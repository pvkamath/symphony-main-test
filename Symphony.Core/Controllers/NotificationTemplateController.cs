﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Data;
using Symphony.Core.Extensions;
using System.Collections;
using System.Reflection;
using SubSonic.Utilities;
using System.Text.RegularExpressions;
using SubSonic;
using log4net;

namespace Symphony.Core.Controllers
{
    /// <summary>
    /// Responsible for managing all template-related things for messaging
    /// </summary>
    /// <remarks>
    /// To add a new system message:
    /// 1) Add an entry in the "Templates" table.
    /// 2) Add the message name specified in the table to the "NotificationType" class
    /// 3) Specify the expected types in the "GetValidTypesForTemplate" method below; this determines 
    /// what shows up in the tree when editing the template, and also what object types should be in
    /// the array of objects passed in when sending the notification. It also indirectly determines
    /// what *recipient groups* are available to receive the message, based on the types selected.
    /// 4) If necessary, add the appropriate objects to GetSampleInstances for the preview.
    /// </remarks>
    public class NotificationTemplateController
    {
        #region Parsing Regexes
        /*
        private static Regex reCollection = new Regex(@"{!foreach (?<placeholder>[\w+|\.|\(|\)]+)}(?<contents>.+){end}", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
        private static Regex reEnd = new Regex(@"{!?end}", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
        private static Regex reItem = new Regex(@"{!(?!foreach)(?<placeholder>[\w|\.|\(|\)|""]+?)}", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
        */
        private static Regex reMethod = new Regex(@"(?<method>.+?)\((?<params>.*?)\)", RegexOptions.Compiled);

        #endregion

        private static ILog Log = LogManager.GetLogger(typeof(NotificationTemplateController));

        /// <summary>
        /// Given a template, returns a list of template parameters that can be used with that template.
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        public static Models.TreeNode GetTemplateParametersForTemplate(Template template)
        {
            Type[] types = GetValidTypesForTemplate(template);
            StringBuilder sb = new StringBuilder();
            Models.TreeNode templateParameters = new Models.TreeNode(template.DisplayName);
            foreach (Type t in types)
            {
                Models.TreeNode tn = GetAllProperties(t);
                templateParameters.Children.Add(tn);
            }
            return templateParameters;
        }

        public static Models.TreeNode GetTemplateParametersForScheduleParameter(ScheduleParameter parameter)
        {
            Type type = GetValidTypeForScheduleParameter(parameter);
            StringBuilder sb = new StringBuilder();
            Models.TreeNode templateParameters = new Models.TreeNode(parameter.DisplayName);
            templateParameters.Children.Add(GetAllProperties(type));
            return templateParameters;
        }

        public static Models.TreeNode GetTemplateParametersForCertificateTemplate()
        {
            var tree = new Models.TreeNode()
            {
                Text = "Root",
                Children = new Models.TreeNodeCollection
                {
                    GetAllProperties(typeof(SalesChannel)),
                    GetAllProperties(typeof(Models.Certificate))
                }
            };

            return tree;
        }

        /// <summary>
        /// Takes a given type and returns a treenode representing a list of all the properties on that type that can be used in a template
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private static Models.TreeNode GetAllProperties(Type t)
        {
            Models.TreeNode root = new Models.TreeNode(t.Name);
            Stack<Type> parentTypes = new Stack<Type>();
            parentTypes.Push(t);
            GetAllProperties(t, root, parentTypes);
            return root;
        }

        /// <summary>
        /// Recursive function that lists out the properties on a specified type and puts them into a nice tree structure.
        /// </summary>
        /// <param name="t">The type to analyze</param>
        /// <param name="node">The parent node</param>
        /// <param name="parentTypes">A list of types that are in the parent path, to avoid infinite recursion</param>
        private static void GetAllProperties(Type t, Models.TreeNode node, Stack<Type> parentTypes)
        {
            if (typeof(ICollection).IsAssignableFrom(t))
            {
                t = GetElementType(t); // make sure its valid...for collections, this will get the element type of each item
            }
            string[] properties = GetValidPropertiesForTemplateType(t);

            if (properties == null)
            {
                throw new Exception("Invalid type found: " + t.ToString());
            }

            Dictionary<string, MethodInfo> objMethodDictionary = t.GetMethods().DistinctBy(m => m.Name).ToDictionary(m => m.Name);
            Dictionary<string, PropertyInfo> objPropertiesDictionary = t.GetProperties().ToDictionary(p => p.Name);

            foreach (string prop in properties)
            {
                string propName = prop;
                if (prop.Contains("("))
                {
                    propName = prop.Substring(0, prop.IndexOf("("));
                    if (!objMethodDictionary.ContainsKey(propName))
                    {
                        continue;
                    }
                }
                else
                {
                    // avoid invalid properties...ones we haven't explicitly allowed, and ones that'll cause an infinite loop
                    if (!objPropertiesDictionary.ContainsKey(propName) || parentTypes.Contains(objPropertiesDictionary[propName].PropertyType) || parentTypes.Contains(GetElementType(objPropertiesDictionary[propName].PropertyType)))
                    {
                        continue;
                    }
                }

                Type propertyType = objMethodDictionary.ContainsKey(propName) ? objMethodDictionary[propName].ReturnType : objPropertiesDictionary[propName].PropertyType;
                string name = Utility.GetProperName(objMethodDictionary.ContainsKey(propName) ? objMethodDictionary[propName].Name : objPropertiesDictionary[propName].Name);

                Models.TreeNode child = new Models.TreeNode(name);
                // save the type with this node
                child.Tag = propertyType;

                if (objMethodDictionary.ContainsKey(propName)) {
                    int startParamIndex = prop.IndexOf("(") + 1;
                    int endParamIndex = prop.IndexOf(")");

                    string parameters = prop.Substring(startParamIndex, endParamIndex - startParamIndex);

                    child.Text = parameters;
                    child.Code = prop;
                    node.Children.Add(child);
                } 
                else if (propertyType.IsPrimitive || propertyType == typeof(string) || propertyType == typeof(DateTime) || propertyType == typeof(decimal))
                {
                    node.Children.Add(child);
                }
                else if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    node.Children.Add(child);
                }
                else
                {
                    // push the type onto a stack so we never end up in a recursive loop
                    parentTypes.Push(t);

                    GetAllProperties(propertyType, child, parentTypes);

                    // once we're out, this type can be removed from our stack
                    parentTypes.Pop();

                    // then we can keep going
                    node.Children.Add(child);
                }
            }
        }

        /// <summary>
        /// Returns a list of types that can be specified as top-level objects in a given template
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        public static Type[] GetValidTypesForTemplate(Template template)
        {
            // pre-defined templates
            switch (template.CodeName)
            {
                case NotificationType.MarkedAsAttended:
                    return new Type[] { typeof(Registration), typeof(ClassX), typeof(User), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.SchedulingChange:
                case NotificationType.NewClassScheduled:
                case NotificationType.ClassCancelled:
                    return new Type[] { typeof(ClassX), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.InstructorRemoved:
                case NotificationType.InstructorAdded:
                    return new Type[] { typeof(ClassX), typeof(User), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.ResourceRequested:
                    return new Type[] { typeof(Resource), typeof(ClassX), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.ClassRoomChange:
                    return new Type[] { typeof(ClassX), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.NewHireStatusChange:
                    return new Type[] { typeof(Data.User), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.StudentUnregistered:
                case NotificationType.StudentRegistered:
                    return new Type[] { typeof(Data.User), typeof(ClassX), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.RegistrationStatusChanged:
                    return new Type[] { typeof(Data.Registration), typeof(ClassX), typeof(User), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.TrainingProgramAssigned:
                    return new Type[] { typeof(Data.TrainingProgram), typeof(Data.Customer), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.MeetingCreatedInternal:
                case NotificationType.MeetingCreatedExternal:
                case NotificationType.MeetingUpdatedInternal:
                case NotificationType.MeetingUpdatedExternal:
                case NotificationType.MeetingCancelledInternal:
                case NotificationType.MeetingCancelledExternal:
                    return new Type[] { typeof(GTMInvitation), typeof(GTMMeeting), typeof(Data.Customer), typeof(Data.SalesChannel) };
                // customer bb
                case NotificationType.UserRegistered:
                    return new Type[] { typeof(Data.User), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.InstructorPermissionGranted:
                    return new Type[] { typeof(Data.User), typeof(Course), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.AssignmentGraded:
                case NotificationType.SingleAssignmentGradedFailed:
                case NotificationType.SingleAssignmentGradedPassed:
                    return new Type[] { typeof(Data.User), typeof(OnlineCourse), typeof(Models.Assignment), typeof(Data.TrainingProgram), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.AssignmentsGradedPassed:
                    return new Type[] { typeof(Data.User), typeof(Data.TrainingProgram), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.AssignmentsGradedFailed:
                    return new Type[] { typeof(Data.User), typeof(Data.TrainingProgram), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.SalesforceNewEnrollment:
                    return new Type[] { typeof(Data.User), typeof(Models.Salesforce.EntitlementList), typeof(Data.Customer), typeof(Data.SalesChannel) };
                case NotificationType.SalesforceNewUser:
                    return new Type[] { typeof(Data.User), typeof(Data.Customer), typeof(Data.SalesChannel) };
            }

            // scheduled templates use the item for which they're scheduled to determine what types can be used in the template
            if (template.IsScheduled)
            {
                ScheduleParameter param = new ScheduleParameter(template.ScheduleParameterID);
                return new Type[] { GetValidTypeForScheduleParameter(param) };
            }
            return new Type[] { };
        }

        /// <summary>
        /// Gets the type that can be used with the specified schedule parameter. Note that this only supports 1 type because
        /// otherwise we can't figure out which object to pull the parameters from.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static Type GetValidTypeForScheduleParameter(ScheduleParameter param)
        {
            switch (param.CodeName)
            {
                case ScheduleParameterType.ClassStartDate:
                case ScheduleParameterType.ClassEndDate:
                    return typeof(Data.ClassX);
                case ScheduleParameterType.TrainingProgramCourseDueDate:
                    return typeof(Data.TrainingProgramToCourseLink);
                case ScheduleParameterType.TrainingProgramStartDate:
                case ScheduleParameterType.TrainingProgramDueDate:
                case ScheduleParameterType.TrainingProgramEndDate:
                    return typeof(Data.TrainingProgram);
                case ScheduleParameterType.TrainingProgramSessionStart:
                case ScheduleParameterType.TrainingProgramSessionEnd:
                    return typeof(Data.TrainingProgramSession);
                case ScheduleParameterType.TrainingProgramAfterCompletion:
                case ScheduleParameterType.TrainingProgramPercentComplete:
                    return typeof(Data.TrainingProgramRollup);
                case ScheduleParameterType.StudentLastCourseActivity:
                case ScheduleParameterType.StudentLastLoggedIn:
                    return typeof(Data.UserLastActivity);
                case ScheduleParameterType.NewHireExpiration:
                    return typeof(Data.User);
            }
            Log.Error("Invalid type found, param: " + param.CodeName + ", " + param.Id);
            return null;
        }

        /// <summary>
        /// For a given type, lists the properties on that type that are valid for use in a template
        /// </summary>
        /// <param name="type">The type to inspect</param>
        /// <returns>A string enumerating the properties that can be used.</returns>
        private static string[] GetValidPropertiesForTemplateType(Type type)
        {
            if (type == typeof(Registration))
            {
                return new string[] { "Attendance", "ClassX", "Score", "Status", "ModifiedOn", "CreatedOn", "UserFullName" };
            }
            else if (type == typeof(ClassX) || type == typeof(Models.ClassroomClass))
            {
                return new string[] { "Room", "CapacityOverride", "ClassDates", "Course", "Description",
                    "InternalCode", "Location", "Name", "Notes", "Resources", "Room", "TimeZone", "ModifiedOn", "CreatedOn", "DirectRegistrationUrl" };
            }
            else if (type == typeof(Resource))
            {
                return new string[] { "Cost", "Name", "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(Room))
            {
                return new string[] { "Capacity", "Name", "NetworkAccessIndicator", "RoomCost", "Venue", "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(Venue))
            {
                return new string[] { "City", "InternalCode", "Name", "Resources", "Rooms", "StateName", "CreatedOn", "ModifiedOn" };
            }
            /*else if (type == typeof(LearningObject))
            {
                return new string[] { "LearningObjectCode", "LearningObjectName","LearningObjectDescription",
                    "Courses", "CreatedOn", "ModifiedOn", "Pages", "VersionID" };
            }
            else if (type == typeof(Artisan.Course))
            {
                return new string[] { "LearningObjects", "CourseCode", "CourseDescription", "CourseName", "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(Artisan.Page))
            {
                return new string[] { "PageName", "ModifiedOn", "CreatedOn" };
            }*/
            else if (type == typeof(User) || type == typeof(Models.User))
            {
                return new string[] { "ID", "Audiences", "JobRole", "Location", "Email", "EmployeeNumber", "FirstName", "FullName",
                    "FullNameReversed", "HireDay", "LastName", "MiddleName", "Notes", "TelephoneNumber", "Username", "Status", "NmlsNumber",
                    "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(UserLastActivity))
            {
                return new string[] {  "Email", "EmployeeNumber", "FirstName", "FullName",
                    "HireDay", "LastName", "MiddleName", "Notes", "TelephoneNumber", "Username",
                    "ModifiedOn", "CreatedOn",
                    "LastLoginDate", "LastCourseWorkDate"};
            }
            else if (type == typeof(TrainingProgram) || type == typeof(Models.TrainingProgram))
            {
                return new string[] {"ID", "Name", "Cost", "Description", "DateCompleted", "EnforceRequiredOrder", "MinimumElectives",
                    "StartDay", "DueDay", "EndDay", "ModifiedOn", "CreatedOn", "RequiredCourses", "ElectiveCourses", "OptionalCourses", "FinalAssessments" };
            }
            else if (type == typeof(TrainingProgramSession))
            {
                return new string[] {"ID", "Name", "Cost", "Description", "EnforceRequiredOrder", "MinimumElectives", 
                    "ModifiedOn", "CreatedOn",
                    "SessionClassID", "SessionClassName", "SessionDailyDuration", "SessionNumberOfDays", "SessionStartDay", "SessionEndDay"};
            }
            else if (type == typeof(TrainingProgramRollup))
            {
                return new string[] { "UserID", "TrainingProgramID", "IsComplete", "IsCompleteOverride", "CompletedDay", "PercentComplete", "User", "TrainingProgram" };
            }
            else if (type == typeof(TrainingProgramCourse))
            {
                return new string[] { "Course", "Classes" };
            }
            else if (type == typeof(GTMInvitation))
            {
                return new string[] { "IsExternal", "CreatedOn", "ModifiedOn" };
            }
            else if (type == typeof(GTMMeeting))
            {
                return new string[] { "Subject", "JoinURL", "ExternalJoinURL", "MaxParticipants", "MeetingType", "StartDate",
                    "EndDate", "PasswordRequired", "ConferenceCallInfo", "IsEditable", "CreatedOn", "ModifiedOn" };
            }
            else if (type == typeof(ClassDate))
            {
                return new string[] { "ClassX", "DurationMinutes", "DurationHours", "DurationText", "StartTime", "StartDay" };
            }
            else if (type == typeof(Course) || type == typeof(Models.Course) || type == typeof(Models.TrainingProgramCourse))
            {
                return new string[]{ "CourseObjective", "CourseTypeDescription", "Credit", "Description", "InternalCode", "Name", "PublicIndicator",
                    "CreatedOn", "ModifiedOn" };
            }
            else if (type == typeof(Data.TimeZone))
            {
                return new string[] { "Description" };
            }
            else if (type == typeof(Location) || type == typeof(Models.Location))
            {
                return new string[] { "Address1", "Address2", "City", "Name", "PostalCode", "State", "TelephoneNumber" };
            }
            else if (type == typeof(JobRole))
            {
                return new string[] { "InternalCode", "Name" };
            }
            else if (type == typeof(Audience))
            {
                return new string[] { "InternalCode", "Name" };
            }
            else if (type == typeof(Status))
            {
                return new string[] { "Description" };
            }
            else if (type == typeof(OnlineCourse))
            {
                return new string[] { "Cost", "Credit", "Description", "Name", "PublicIndicator" };
            }
            else if (type == typeof(TrainingProgramToCourseLink))
            {
                return new string[] { "Cost", "Credit", "Description", "CourseName", "TrainingProgramName", "PublicIndicator", "DueDay" };
            }
            else if (type == typeof(Models.Assignment))
            {
                return new string[] { "Attempt", "Date", "Name", "NumCorrect", "NumQuestions", "Score", "Percent" };
            }
            else if (type == typeof(Customer) || type == typeof(Models.Customer))
            {
                return new string[] { "Name", "SubDomain", "HelpUrl", "Email", "Phone", "Fax", "AssociatedImageData", 
                    "Location", "CourseAlias", "Description", "AccountExecutiveUser", "CustomerCareRepUser", "BaseUrl", "LoginUrl", "LogoData", "LogoHtml", "PrimaryLeader" };
            }
            else if (type == typeof(Models.Salesforce.EntitlementList))
            {
                return new string[] { "Entitlements" };
            }
            else if (type == typeof(Models.Salesforce.Entitlement))
            {
                return new string[] { "SalesforceEntitlemtnId", "SalesforceProductId", "ProductId", "ProductCategory", "EntitlemntId", "EntitlmentPurchaseDate", "EntitlementStartDate", "EntitlementEndDate", "EntitlementDuration", "EntitlementDurationUnits", "IsEnabled", "PaymentRefId", "PartnerId", "PartnerCode", "PartnerDisplayName", "SessionID", "TrainingProgram", "Session" };
            }
            else if (type == typeof(SalesChannel))
            {
                return new string[] { "Logo" };
            }
            else if (type == typeof(Models.Certificate))
            {
                IEnumerable<Models.UserDataField> fields = new CustomerController().GetUserDataFields(0, "", "CodeName", OrderDirection.Ascending, 0, int.MaxValue).Data;
                
                
                return new string[] { "User", "TrainingProgram", "Course", "Class", "Customer", "CustomerLocation", "UserLocation", "TranscriptEntry"}
                    .Concat(fields.Select(f => "GetLicenseDataMeta("+f.CodeName+")")).ToArray();
            }
            else if (type == typeof(Models.TranscriptEntry))
            {
                return new string[] {
                    "Score", "AttemptDate", "AttemptCount", "DateCompleted",
                };
            }
            else if (type == typeof(KeyValuePair<string, string>))
            {
                return new string[] { };
            }
            else if (type == typeof(string))
            {
                return new string[] { };
            }
            return null;
        }

        private static Type GetElementType(Type seqType)
        {
            Type ienum = FindIEnumerable(seqType);
            if (ienum == null) return seqType;
            return ienum.GetGenericArguments()[0];
        }

        private static Type FindIEnumerable(Type seqType)
        {
            if (seqType == null || seqType == typeof(string))
                return null;
            if (seqType.IsArray)
                return typeof(IEnumerable<>).MakeGenericType(seqType.GetElementType());
            if (seqType.IsGenericType)
            {
                foreach (Type arg in seqType.GetGenericArguments())
                {
                    Type ienum = typeof(IEnumerable<>).MakeGenericType(arg);
                    if (ienum.IsAssignableFrom(seqType))
                    {
                        return ienum;
                    }
                }
            }
            Type[] ifaces = seqType.GetInterfaces();
            if (ifaces != null && ifaces.Length > 0)
            {
                foreach (Type iface in ifaces)
                {
                    Type ienum = FindIEnumerable(iface);
                    if (ienum != null) return ienum;
                }
            }
            if (seqType.BaseType != null && seqType.BaseType != typeof(object))
            {
                return FindIEnumerable(seqType.BaseType);
            }
            return null;
        }

        /// <summary>
        /// Parses the specified templateText value using the types that are valid for the specified templateCodeName
        /// </summary>
        /// <param name="templateCodeName">The name of the template</param>
        /// <param name="templateText">The text to parse</param>
        /// <returns>The parsed template</returns>
        public static string PreviewTemplate(int templateId, int scheduleParameterId, string templateText)
        {
            List<object> objects = new List<object>();
            if (scheduleParameterId > 0)
            {
                Type t = GetValidTypeForScheduleParameter(new ScheduleParameter(scheduleParameterId));
                objects.Add(GetObjectInstance(t));
            }
            else
            {
                // get a list of the types that can be used in this template
                Type[] types = GetValidTypesForTemplate(new Template(templateId));

                // for each type that can be used in the template, get an instance
                foreach (Type t in types)
                {
                    objects.Add(GetObjectInstance(t));
                }
            }

            // parse the template, using the sample objects
            string parsed = Parse(templateText, objects.ToArray());

            var c = new SymphonyController();
            var user = new Data.User(c.UserID);
            return NotificationTemplateController.ParseUserTemplateParameters(parsed, user);
        }

        /// <summary>
        /// Creates a nicely loaded object instance
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object GetObjectInstance(Type type)
        {
            Dictionary<Type, object> instances = GetSampleInstances();
            if (instances.ContainsKey(type))
            {
                return instances[type];
            }
            return null;
        }

        /// <summary>
        /// Creates a bunch of sample objects for use in the templates
        /// </summary>
        /// <returns></returns>
        public static Dictionary<Type, object> GetSampleInstances()
        {
            Dictionary<Type, object> sampleInstances = new Dictionary<Type, object>();

            Data.UserStatus us = new Data.UserStatus();
            us.Description = "Active";
            sampleInstances.Add(typeof(UserStatusType), us);

            User u = new User();
            u.FirstName = "John";
            u.LastName = "Doe";
            u.MiddleName = "Q.";
            u.Notes = "Excellent at counting change";
            u.NewHireIndicator = true;
            u.Email = "john.doe@gmail.com";
            u.EmployeeNumber = "867";
            u.HireDate = DateTime.Now.AddDays(-5);
            u.TelephoneNumber = "919-555-5555";
            u.Username = "jdoe";
            u.ModifiedOn = DateTime.Now;
            u.CreatedOn = DateTime.Now.AddSeconds(-500);
            sampleInstances.Add(typeof(User), u);

            Customer c = new Customer();
            c.Name = "Awesome Customer";
            c.SubDomain = "awesomecustomer";
            c.CourseAlias = "Module";
            c.Description = "This is the awesome customer";
            c.Email = "awesome@customer.com";
            c.Fax = "123-456-7890";
            c.HelpUrl = "http://help.example.com";
            c.Phone = "321-654-9870";
            sampleInstances.Add(typeof(Customer), c);

            Status s = new Status();
            s.Description = "Wait List";
            sampleInstances.Add(typeof(Status), s);

            State st = new State();
            st.Name = "NC";
            st.Description = "North Carolina";
            sampleInstances.Add(typeof(State), st);

            Venue venue = new Venue();
            venue.Name = "Green Venue";
            venue.City = "Raleigh";
            venue.InternalCode = "5309";
            venue.ModifiedOn = DateTime.Now;
            venue.CreatedOn = DateTime.Now.AddSeconds(-500);
            sampleInstances.Add(typeof(Venue), venue);

            Room room = new Room();
            room.Name = "Blue Room";
            room.Capacity = 25;
            room.NetworkAccessIndicator = true;
            room.RoomCost = 100;
            room.ModifiedOn = DateTime.Now;
            room.CreatedOn = DateTime.Now.AddSeconds(-500);
            sampleInstances.Add(typeof(Room), room);

            Course course = new Course();
            course.CourseObjective = "To take the course";
            course.Credit = 10;
            course.Description = "A sample course";
            course.InternalCode = "8675309";
            course.Name = "Sample Course";
            course.PublicIndicator = false;
            course.ModifiedOn = DateTime.Now;
            course.CreatedOn = DateTime.Now.AddSeconds(-500);
            sampleInstances.Add(typeof(Course), course);

            PresentationType pt = new PresentationType();
            pt.Description = "Seminar";
            sampleInstances.Add(typeof(PresentationType), pt);

            Resource resource = new Resource();
            resource.Name = "Projector";
            resource.Cost = 5;
            sampleInstances.Add(typeof(Resource), resource);

            Data.TimeZone tz = new Data.TimeZone();
            tz.Description = "EST";
            sampleInstances.Add(typeof(Data.TimeZone), tz);

            ClassX classx = new ClassX();
            classx.Name = "Sample Class";
            classx.CapacityOverride = 30;
            classx.Description = "This class was created for testing";
            classx.InternalCode = "1592";
            classx.Notes = "Some notes about the class go here.";
            classx.ModifiedOn = DateTime.Now;
            classx.CreatedOn = DateTime.Now.AddSeconds(-500);
            classx.CustomerID = 1;
            sampleInstances.Add(typeof(ClassX), classx);

            ClassDate cd = new ClassDate();
            cd.StartDateTime = DateTime.Now.AddDays(10);
            cd.Duration = 90;
            cd.CustomerID = 1;
            ClassDate cd2 = new ClassDate();
            cd2.StartDateTime = DateTime.Now.AddDays(11);
            cd2.Duration = 90;
            cd2.CustomerID = 1;
            sampleInstances.Add(typeof(ClassDate), cd);

            Registration reg = new Registration();
            reg.AttendedIndicator = true;
            reg.Score = "95";
            reg.AttendedIndicator = false;
            reg.CreatedOn = DateTime.Now;
            reg.ModifiedOn = DateTime.Now.AddSeconds(1);
            reg.UserFullName = "Doe, John Q.";
            sampleInstances.Add(typeof(Registration), reg);

            Audience audience = new Audience();
            audience.Name = "My Audience";
            audience.InternalCode = "a1234";
            audience.CreatedOn = DateTime.Now;
            audience.ModifiedOn = DateTime.Now.AddSeconds(1);
            sampleInstances.Add(typeof(Audience), audience);

            JobRole jobrole = new JobRole();
            jobrole.Name = "My Job";
            jobrole.InternalCode = "j1234";
            jobrole.CreatedOn = DateTime.Now;
            jobrole.ModifiedOn = DateTime.Now.AddSeconds(1);
            sampleInstances.Add(typeof(JobRole), jobrole);

            Location location = new Location();
            location.Address1 = "123 Main Street";
            location.Address2 = "Suite 200";
            location.City = "Some City";
            location.State = "Some State";
            location.Name = "My Location";
            location.TelephoneNbr = "919-555-1234";
            location.CreatedOn = DateTime.Now;
            location.ModifiedOn = DateTime.Now.AddSeconds(1);
            sampleInstances.Add(typeof(Location), location);

            TrainingProgram tp = new TrainingProgram();
            tp.Name = "My Training Program";
            tp.Cost = 50;
            tp.Description = "A training program needs a description";
            tp.DueDate = DateTime.Now.AddDays(14);
            tp.EndDate = tp.DueDate;
            tp.StartDate = DateTime.Now;
            tp.EnforceRequiredOrder = true;
            tp.MinimumElectives = 5;
            tp.CreatedOn = DateTime.Now;
            tp.ModifiedOn = DateTime.Now.AddSeconds(1);
            tp.RequiredCourses = new TrainingProgramCourseCollection();
            tp.ElectiveCourses = new TrainingProgramCourseCollection();
            tp.OptionalCourses = new TrainingProgramCourseCollection();
            tp.FinalAssessments = new TrainingProgramCourseCollection();
            tp.CustomerID = 1;
            sampleInstances.Add(typeof(TrainingProgram), tp);

            TrainingProgramCourse requiredCourse = new TrainingProgramCourse();
            requiredCourse.Course = course;

            TrainingProgramCourse electiveCourse = new TrainingProgramCourse();
            electiveCourse.Course = course;

            TrainingProgramCourse optionalCourse = new TrainingProgramCourse();
            optionalCourse.Course = course;

            TrainingProgramCourse finalAssessment = new TrainingProgramCourse();
            finalAssessment.Course = course;

            GTMInvitation invitation = new GTMInvitation();
            invitation.IsExternal = true;
            invitation.CreatedOn = DateTime.Now.AddMinutes(-5);
            invitation.ModifiedOn = DateTime.Now;
            sampleInstances.Add(typeof(GTMInvitation), invitation);

            GTMMeeting meeting = new GTMMeeting();
            meeting.Subject = "A meeting!";
            meeting.JoinURL = "http://someurl.com/join/";
            meeting.MaxParticipants = 10;
            meeting.MeetingType = "Scheduled";
            meeting.StartDateTime = DateTime.Now;
            meeting.EndDateTime = meeting.StartDateTime.AddHours(1);
            meeting.PasswordRequired = false;
            meeting.ConferenceCallInfo = "Dial (507) 552-7185, access code 125-701-754";
            meeting.IsEditable = false;
            meeting.CreatedOn = DateTime.Now.AddMinutes(-5);
            meeting.ModifiedOn = DateTime.Now;
            sampleInstances.Add(typeof(GTMMeeting), meeting);

            ClassInstructor ci = new ClassInstructor();
            ci.ClassX = classx;
            sampleInstances.Add(typeof(ClassInstructor), ci);

            TrainingProgramToCourseLink link = new TrainingProgramToCourseLink();
            link.Cost = 10;
            link.Credit = 3;
            link.Description = "A course, either classsroom or online";
            link.CourseName = "Some Course";
            link.PublicIndicator = false;
            link.TrainingProgramName = "Some Training Program";
            link.DueDate = DateTime.Now;
            sampleInstances.Add(typeof(TrainingProgramToCourseLink), link);

            Models.Assignment assignment = new Models.Assignment()
            {
                Attempt = 2,
                CourseID = 10,
                CourseName = "Assignmnet Test Course",
                Date = DateTime.UtcNow,
                Name = "Test Assignmnet",
                NumCorrect = 6,
                NumQuestions = 10,
                Score = 0.6,
                SectionID = 39,
                TotalQuestions = 10,
                TrainingProgramID = 1,
                UserID = 1
            };
            sampleInstances.Add(typeof(Models.Assignment), assignment);

            Data.OnlineCourse onlineCourse = new OnlineCourse()
            {
                Id = 1,
                CreatedByUserId = 1,
                CreatedByActualUserId = 1,
                Description = "Course Description",
                CreatedBy = "Me",
                Cost = 10,
                ContactEmailOverride = "contact@override.com",
                CertificatePath = "/path",
                CertificateEnabledOverride = false,
                CertificateEnabled = true,
                CategoryID = 1,
                ArtisanCourseID = 10,
                Active = true,
                CreatedOn = DateTime.UtcNow,
                Credit = 10,
                PublicIndicator = false,
                Name = "Test Online Course"
            };
            sampleInstances.Add(typeof(OnlineCourse), onlineCourse);

            // add the class to the class date
            cd.ClassX = classx;
            cd2.ClassX = classx;

            // add the class dates to the class
            classx.Room = room;
            classx.TimeZone = tz;
            classx.Course = course;

            classx.ClassDates.Add(cd);
            classx.ClassDates.Add(cd2);
            classx.Resources.Add(resource);
            classx.Registrations.Add(reg);

            // create the registration status and class
            reg.Status = s;
            reg.ClassX = classx;

            // set the venue for the room
            room.Venue = venue;

            // set the venue state resources and rooms
            venue.State = st;
            venue.Resources.Add(resource);
            venue.Rooms.Add(room);

            // add the audience/jobrole/location
            u.UserStatus = us;
            u.Audiences.Add(audience);
            u.JobRole = jobrole;
            u.Location = location;

            course.PresentationType = pt;

            // resuing the sample class above for each trainingProgramCourse details
            ClassXCollection classes = new ClassXCollection();
            classes.Add(classx);

            requiredCourse.Classes = classes;
            electiveCourse.Classes = classes;
            optionalCourse.Classes = classes;
            finalAssessment.Classes = classes;

            tp.RequiredCourses.Add(requiredCourse);
            tp.ElectiveCourses.Add(electiveCourse);
            tp.OptionalCourses.Add(optionalCourse);
            tp.FinalAssessments.Add(finalAssessment);

            TrainingProgramSession tpSession = new TrainingProgramSession
            {
                CustomerID = 1,
                Name = "Test Training Program Name",
                Cost = 100,
                Description = "Here is your training program description",
                EnforceRequiredOrder = true,
                MinimumElectives = 10,
                ModifiedOn = DateTime.UtcNow,
                CreatedOn = DateTime.UtcNow,
                SessionClassID = 4,
                SessionClassName = "SessionName",
                SessionDailyDuration = 8,
                SessionNumberOfDays = 1,
                MinSessionStartDate = DateTime.UtcNow,
                MaxSessionStartDate = DateTime.UtcNow,
                SessionStartDate = DateTime.UtcNow,
                SessionEndDate = DateTime.UtcNow.AddHours(8)
            };

            sampleInstances.Add(typeof(TrainingProgramSession), tpSession);

            TrainingProgramRollup tpRollup = new TrainingProgramRollup{
                TrainingProgram = tp,
                UserID = u.Id,
                TrainingProgramID = tp.Id,
                IsComplete = true,
                IsCompleteOverride = false,
                DateCompleted = DateTime.UtcNow,
                PercentComplete = 95,
                User = u
            };

            sampleInstances.Add(typeof(TrainingProgramRollup), tpRollup);

            Data.TrainingProgram tp1 = new Data.TrainingProgram();
            tp1.CopyFrom(tp);
            Data.TrainingProgram tp2 = new Data.TrainingProgram();
            tp2.CopyFrom(tp);
            Data.TrainingProgram tp3 = new Data.TrainingProgram();
            tp3.CopyFrom(tp);

            tp1.Name = "Test Training Program 1";
            tp2.Name = "Test Training Program 2";
            tp3.Name = "Test Training Program 3";

            tp1.ElectiveCourses = tp.ElectiveCourses;
            tp1.RequiredCourses = tp.RequiredCourses;
            tp1.OptionalCourses = tp.OptionalCourses;
            tp1.FinalAssessments = tp.FinalAssessments;
            tp2.ElectiveCourses = tp.ElectiveCourses;
            tp2.RequiredCourses = tp.RequiredCourses;
            tp2.OptionalCourses = tp.OptionalCourses;
            tp2.FinalAssessments = tp.FinalAssessments;
            tp3.ElectiveCourses = tp.ElectiveCourses;
            tp3.RequiredCourses = tp.RequiredCourses;
            tp3.OptionalCourses = tp.OptionalCourses;
            tp3.FinalAssessments = tp.FinalAssessments;
            
            Models.Salesforce.EntitlementList entitlements = new Models.Salesforce.EntitlementList
            {
                Entitlements = new List<Models.Salesforce.Entitlement>()
                {
                   new Models.Salesforce.Entitlement{
                       EntitlementDuration = 12,
                       EntitlementDurationUnits = "Months",
                       EntitlementEndDate = DateTime.UtcNow.AddYears(1).ToString(),
                       EntitlementStartDate = DateTime.UtcNow.ToString(),
                       IsEnabled = true,
                       PartnerCode = "1234",
                       Session = classx,
                       TrainingProgram = tp1
                   },
                   new Models.Salesforce.Entitlement{
                       EntitlementDuration = 12,
                       EntitlementDurationUnits = "Months",
                       EntitlementEndDate = DateTime.UtcNow.AddYears(1).ToString(),
                       EntitlementStartDate = DateTime.UtcNow.ToString(),
                       IsEnabled = true,
                       PartnerCode = "1234",
                       Session = classx,
                       TrainingProgram = tp2
                   },
                   new Models.Salesforce.Entitlement{
                       EntitlementDuration = 12,
                       EntitlementDurationUnits = "Months",
                       EntitlementEndDate = DateTime.UtcNow.AddYears(1).ToString(),
                       EntitlementStartDate = DateTime.UtcNow.ToString(),
                       IsEnabled = true,
                       PartnerCode = "1234",
                       Session = classx,
                       TrainingProgram = tp3
                   }
                }
            };

            sampleInstances.Add(typeof(Models.Salesforce.EntitlementList), entitlements);

            //Page page = new Page();
            //page.PageName = "An Artisan page";
            //page.SerializeLinkedTables = true;
            //sampleInstances.Add(typeof(Page), page);

            //LearningObject lo = new LearningObject();
            //lo.LearningObjectCode = "lo123";
            //lo.LearningObjectName = "The name of the LO";
            //lo.LearningObjectDescription = "The description of the LO";
            //lo.CreatedOn = DateTime.Now.AddMinutes(-5);
            //lo.ModifiedOn = DateTime.Now;
            //lo.SerializeLinkedTables = true;
            //sampleInstances.Add(typeof(LearningObject), lo);

            //Artisan.Course acourse = new Artisan.Course();
            //acourse.CourseCode = "c123";
            //acourse.CourseName = "The name of the course";
            //acourse.CourseDescription = "The description of the course";
            //acourse.CreatedOn = DateTime.Now.AddMinutes(-5);
            //acourse.ModifiedOn = DateTime.Now;
            //acourse.SerializeLinkedTables = true;
            //sampleInstances.Add(typeof(Artisan.Course), acourse);

            //lo.CoursesWrite = new BankersEdge.Artisan.CourseCollection();
            //lo.CoursesWrite.Add(acourse);
            //lo.PagesWrite = new PageCollection();
            //lo.PagesWrite.Add(page);

            //acourse.LearningObjectsWrite = new LearningObjectCollection();
            //acourse.LearningObjectsWrite.Add(lo);

            //acourse.CourseAuthorNotesWrite = new CourseAuthorNoteCollection();
            //acourse.CourseAuthorNotesWrite.Add(new CourseAuthorNote()
            //{
            //    Note = "Some note"
            //});

            return sampleInstances;
        }

        /// <summary>
        /// Parses a template
        /// </summary>
        /// <param name="message"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static string Parse(string message, object[] objects)
        {
            return ParseTemplate2(message, objects);//.Replace("{end}", "").Replace("_\r\n", "");
        }

        private static Regex CommandRegex = new Regex(@"\{!(?<cmd>[^}])\}", RegexOptions.Compiled);

        private enum TokenState { Normal, LeftBrace, Command }
        private enum TokenType { Text, Variable, Foreach, End }
        private class Token
        {
            public TokenType Type { get; set; }
            public string Value { get; set; }
        }
        private class Tokenizer
        {
            private string Text { get; set; }
            public int Position { get; private set; }
            public TokenState State { get; private set; }
            public Tokenizer(string text)
            {
                Text = text;
                State = TokenState.Normal;
            }
            public void Reset(int position, TokenState state)
            {
                Position = position;
                State = state;
            }
            public Token Next()
            {
                Token token = null;
                var tokenStart = Position;
                while (Position < Text.Length)
                {
                    var c = Text[Position];
                    switch (State)
                    {
                        case TokenState.Normal:
                            if (c == '{')
                            {
                                State = TokenState.LeftBrace;
                            }
                            break;
                        case TokenState.LeftBrace:
                            if (c == '!')
                            {
                                State = TokenState.Command;
                                token = new Token
                                {
                                    Type = TokenType.Text,
                                    Value = Text.Substring(tokenStart, Position - tokenStart - 1)
                                };
                            }
                            else
                            {
                                State = TokenState.Normal;
                            }
                            break;
                        case TokenState.Command:
                            if (c == '}')
                            {
                                State = TokenState.Normal;
                                var value = Text.Substring(tokenStart, Position - tokenStart);
                                var type = TokenType.Variable;
                                if (value.StartsWith("foreach "))
                                {
                                    type = TokenType.Foreach;
                                    value = value.Split(' ')[1];
                                }
                                else if (value == "end")
                                {
                                    type = TokenType.End;
                                }
                                token = new Token
                                {
                                    Type = type,
                                    Value = value
                                };
                            }
                            break;
                    }
                    Position++;
                    if (token != null)
                    {
                        return token;
                    }
                }
                if (tokenStart != Position)
                {
                    return new Token
                    {
                        Type = TokenType.Text,
                        Value = Text.Substring(tokenStart, Position - tokenStart)
                    };
                }
                return token;
            }
        }

        private static string ParseTemplate2(string message, object[] objects)
        {
            Token token;
            var tokenizer = new Tokenizer(message);
            var output = new StringBuilder();
            while ((token = tokenizer.Next()) != null)
            {
                switch (token.Type)
                {
                    case TokenType.Text:
                        output.Append(ParseText(token.Value, null, objects, tokenizer));
                        break;
                    case TokenType.Variable:
                        output.Append(ParseVariable(token.Value, null, objects, tokenizer));
                        break;
                    case TokenType.Foreach:
                        output.Append(ParseForeach(token.Value, null, objects, tokenizer));
                        break;
                }
            }
            return output.ToString();
        }

        private static string ParseText(string value, object source, object[] objects, Tokenizer tokenizer)
        {
            return value;
        }

        private static string ParseVariable(string value, object source, object[] objects, Tokenizer tokenizer)
        {
            object obj = ExtractObject(value, 1, source, objects);
            if (obj != null)
            {
                return obj.ToString();
            }
            return "[Unknown " + value.Split('.').Last<string>() + "]";
        }

        private static string ParseForeach(string value, object source, object[] objects, Tokenizer tokenizer)
        {
            object collection = ExtractObject(value, 1, source, objects);
            if (collection != null && collection is ICollection)
            {
                Token token;
                var output = new StringBuilder();
                var position = tokenizer.Position;
                var state = tokenizer.State;
                foreach (object item in (ICollection)collection)
                {
                    tokenizer.Reset(position, state);
                    while ((token = tokenizer.Next()) != null)
                    {
                        if (token.Type == TokenType.End)
                        {
                            break;
                        }
                        switch (token.Type)
                        {
                            case TokenType.Text:
                                output.Append(ParseText(token.Value, item, objects, tokenizer));
                                break;
                            case TokenType.Variable:
                                output.Append(ParseVariable(token.Value, item, objects, tokenizer));
                                break;
                            case TokenType.Foreach:
                                output.Append(ParseForeach(token.Value, item, objects, tokenizer));
                                break;
                        }
                    }
                }
                return output.ToString();
            }
            return "[Unknown " + value.Split('.').Last<string>() + "]";
        }

        /// <summary>
        /// Takes a string and replaces the contents in the format {![name]} with the appropriate properties
        /// from the specified objects
        /// </summary>
        /// <param name="message">The template message</param>
        /// <param name="objects">The objects the use in the message</param>
        /// <param name="useWriteProperties">If set to true, any time a [PropertyName]Write property is found, it will be used in place of the [PropertyName]
        /// property. This is to enable unit testing.</param>
        /// <returns>A formatted message</returns>
        /*
        private static string ParseTemplate(string message, object source, object[] objects)
        {
            // then, take care of all the "foreach" loops, so we know those are cleared
            message = reCollection.Replace(message, m =>
            {
                // get the string
                string placeholder = m.Groups["placeholder"].Value;

                object o = ExtractObject(placeholder, 1, null, objects);
                if (o != null && o is ICollection)
                {
                    // loop over the collection, formatting each item
                    StringBuilder sb = new StringBuilder();
                    foreach (object item in (ICollection)o)
                    {
                        sb.Append(ParseTemplate(m.Groups["contents"].Value, item, objects)).Replace("{end}", string.Empty);
                    }
                    return sb.ToString();
                }
                return "[Unknown " + placeholder.Split('.').Last<string>() + "]";
            });

            foreach (Match match in reItem.Matches(message))
            {
                if (message.IndexOf("{!") > message.IndexOf("{end}") && message.IndexOf("{end}") > -1)
                {
                    return reEnd.Replace(message, "", 1);
                }
                message = reItem.Replace(message, m =>
                {
                    // get the string
                    string placeholder = m.Groups["placeholder"].Value;
                    object o = ExtractObject(placeholder, 1, source, objects);

                    if (o != null)
                        return o.ToString();

                    return "[Unknown " + placeholder.Split('.').Last<string>() + "]";
                }, 1);
            }


            return message;
        }
         */

        /// <summary>
        /// Extracts an object from the source properties. If the source is null, it is found by searching
        /// based on the types in the objects list for a type matching the name specified as part of the placeholder.
        /// </summary>
        /// <param name="placeholder">A string indicating the object to be found, such as "ClassX.Name". If the source is specified, the first part
        /// of the property is ignored and the second part is used as the property. If the source is NOT specified (null), then the first part
        /// is used to search the objects list for a corresponding type, and the second part is still used as the property.</param>
        /// <param name="placeholderPartIndex">Since this is a recursive function, this is the index into the placeholder string.</param>
        /// <param name="useWriteProperties">If true, and a "[property]Write" method is found, it will be used in place of the property.
        /// This allows us to use "dummy" values for testing, without having to hit the database.</param>
        /// <param name="source">The object to be analyzed</param>
        /// <param name="objects">A list of possible source objects, if one isn't specified. This allows us to pass in a bunch
        /// of objects for the top level parameters.</param>
        /// <returns>An object instance</returns>
        /// <remarks>
        /// This method is fairly versatile, including the ability to take simple method calls, such as:
        /// ClassX.Cost.ToString("c2"), which will give the class cost nicely formatted to 2 decimals with a currency sign.
        /// </remarks>
        private static object ExtractObject(string placeholder, int placeholderPartIndex, object source, object[] objects)
        {
            string[] parts = placeholder.Split('.');

            // first, cache the incoming objects by their type so we can find them easily

            Hashtable typeCache = new Hashtable();
            foreach (object obj in objects)
            {
                if (obj == null)
                    continue;

                string typeName = obj.GetType().Name;
                if (!typeCache.ContainsKey(typeName))
                {
                    typeCache.Add(typeName, obj);
                }
                else
                {
                    throw new Exception("Message formatting currently doesn't handle multiple objects of the same type in the 'objects' parameter.");
                }
            }

            // if no "source" is specified, locate the source based on its type name
            // this is always the case for the first value in a string. for example,
            // if "Class.StartDateTime.ToShortDateString()" is used,
            // the "Class" parameter isn't an object, so it will be looked up in the objects.
            // from then on, we're dealing with concrete objects and can work off those instances.
            if (source == null)
            {
                // first part is the type, and should match the params object passed in
                string typeName = parts[placeholderPartIndex - 1];
                // allow secondary name, so in the template we can use "Class" instead of "ClassX".
                // NOTE: we're using the default provider here, so this will screw up if we use different
                // suffixes for different providers.
                string amendedTypeName = Utility.GetParameterName(typeName, SubSonic.DataService.Provider);

                // locate the object. if it's not there, bail out
                if (!typeCache.ContainsKey(typeName) && !typeCache.ContainsKey(amendedTypeName))
                    return null;

                source = typeCache[typeName] ?? typeCache[amendedTypeName];
            }

            // locate the value   
            if (source == null)
                return null;

            object result = null;

            if (placeholderPartIndex >= parts.Length)
            {
                return "[Unknown " + placeholder + " - You may have referenced an object where an object property is required]";
            }

            // for now, we only support passing string literal parameters to methods
            // that lets us do formatting via ToString("[my format]"), but that's about it
            if (parts[placeholderPartIndex].IndexOf(")") > 0 || parts[placeholderPartIndex].IndexOf("(") > 0)
            {

                // It is possible for a . to end up in the parameters for a function
                // this will cause additional parts to be created on the split
                // need to check for the closing bracket of the function and 
                // merge any additional parts. 
                int placeholderSeek = 0;
                string part = parts[placeholderPartIndex];
                while (part.IndexOf(")") <= 0 && placeholderPartIndex + placeholderSeek < parts.Length)
                {
                    placeholderSeek++;
                    part += "." + parts[placeholderPartIndex + placeholderSeek];
                }

                if (part.IndexOf(")") <= 0)
                {
                    Log.Error("Could not find closing brace for " + parts[placeholderPartIndex]);
                    throw new FormatException("Error parsing template; could not find method closing brace");
                }

                if (placeholderSeek > 0)
                {
                    List<string> partsList = parts.ToList();
                    partsList.RemoveRange(placeholderPartIndex, placeholderSeek + 1);
                    partsList.Insert(placeholderPartIndex, part);
                    parts = partsList.ToArray();
                }
                
                // extract out the method name and parameters
                Match match = reMethod.Match(parts[placeholderPartIndex]);
                string[] parameters = null;
                Type[] types = new Type[0];
                if (!match.Success)
                {
                    throw new FormatException("Error parsing template; invalid method structure");
                }

                // check for parameters for the method call
                if (!string.IsNullOrEmpty(match.Groups["params"].Value))
                {
                    // for now, we only accept plain string parameters
                    parameters = match.Groups["params"].Value.Split(',').Select(s => s.Trim('"')).ToArray();
                    // we need an array of types to pass to the method invokation to tell it which
                    // overload to use. since we only accept string types, that's relatively easy...
                    types = new Type[parameters.Length];
                    for (int i = 0; i < types.Length; i++)
                    {
                        types[i] = typeof(String);
                    }
                }

                // find the method
                string methodName = match.Groups["method"].Value;
                MethodInfo mi = source.GetType().GetMethod(methodName, types);
                if (mi == null)
                {
                    mi = source.GetType().GetMethod(Utility.GetParameterName(methodName, SubSonic.DataService.Provider), types);
                    if (mi == null)
                        throw new FormatException("Error parsing template; the method " + parts[placeholderPartIndex] + " could not be found on " + source.GetType() + ".");
                }

                // try and invoke it
                try
                {
                    result = mi.Invoke(source, parameters);
                }
                catch (TargetInvocationException ex)
                {
                    // always throws with an inner exception that has the real error
                    throw ex.InnerException;
                }

                // if we're not done...
                if (placeholderPartIndex != parts.Length - 1)
                {
                    // recurse, adding our new object into the mix
                    result = ExtractObject(placeholder, placeholderPartIndex + 1, result, objects);
                }
            }
            else
            {

                // get the property we're looking for
                string propertyName = parts[placeholderPartIndex];

                PropertyInfo pi = source.GetType().GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
                if (pi == null)
                {
                    pi = source.GetType().GetProperty(Utility.GetParameterName(propertyName, SubSonic.DataService.Provider), BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
                    if (pi == null)
                        // If the property doesn't exist, it could be just that the property is not part of this object
                        // Just return null to avoid causing the entire template to fail. This will display unknown in the place
                        // of the value.
                        return null;
                        ///throw new FormatException("Error parsing template; the property " + parts[placeholderPartIndex] + " could not be found on " + source.GetType() + ".");
                }

                // our "SerializeLinkedTables" properties screws with property accessors,
                // so here we'll temporarily set it true, then set it back when we're done
                bool previous = false;
                PropertyInfo serializedLinked = source.GetType().GetProperty("SerializeLinkedTables");
                if (serializedLinked != null)
                {
                    previous = (bool)serializedLinked.GetValue(source, null);
                    // temporarily set it to true
                    serializedLinked.SetValue(source, true, null);
                }
                // get the value
                result = pi.GetValue(source, null);
                // set it back to its previous value
                if (serializedLinked != null)
                {
                    serializedLinked.SetValue(source, previous, null);
                }
                if (placeholderPartIndex != parts.Length - 1)
                {
                    // recurse, adding our new object into the mix
                    result = ExtractObject(placeholder, placeholderPartIndex + 1, result, objects);
                }
            }
            return result;
        }

        /// <summary>
        /// Given a schedule parameter ID, returns a list of recipients that can be used with that parameter
        /// </summary>
        /// <param name="scheduleParameterId"></param>
        /// <returns></returns>
        public static List<Models.RecipientGroup> GetRecipientGroupsForScheduleParameter(ScheduleParameter scheduleParameter)
        {
            return GetRecipientGroupsForType(GetValidTypeForScheduleParameter(scheduleParameter)).OrderBy(r=>r.DisplayName).ToList();
        }

        /// <summary>
        /// Gets a list of possible recipients for a given type
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private static List<Models.RecipientGroup> GetRecipientGroupsForType(Type t)
        {
            return Select.AllColumnsFrom<RecipientGroup>()
                .Where(RecipientGroup.Columns.ValidForType).IsEqualTo(t.Name)
                .ExecuteTypedList<Models.RecipientGroup>();
        }

        public static List<Models.RecipientGroup> GetRecipientGroupsForTemplate(Template template)
        {
            List<Models.RecipientGroup> totalList = new List<Models.RecipientGroup>();
            foreach (Type t in GetValidTypesForTemplate(template))
            {
                foreach (Models.RecipientGroup current in GetRecipientGroupsForType(t))
                {
                    if (!totalList.Any(total => total.ID == current.ID))
                    {
                        totalList.Add(current);
                    }
                }
            }
            return totalList;
        }

        /// <summary>
        /// Gets a list of users for the specified template
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static List<int> GetUsersForTemplate(int templateId, object[] objects)
        {
            List<int> userIds = new List<int>();
            Template template = new Template(templateId);
            TemplatesToRecipientGroupsMapCollection maps = new TemplatesToRecipientGroupsMapCollection()
                .Where(TemplatesToRecipientGroupsMap.Columns.TemplateID, templateId)
                .Load();

            Log.DebugFormat("Found {0} maps for the template.", maps.Count);

            // determine which people are set up to receive notifications for this template
            foreach (TemplatesToRecipientGroupsMap map in maps)
            {
                try
                {
                    RecipientGroup group = new RecipientGroup(map.RecipientGroupID);
                    object currentObject = null;

                    // loop through the objects and find the referenced users
                    foreach (object o in objects)
                    {
                        // templates each have multiple maps. a given map may apply to zero, one, or more objects,
                        // but in all cases right now, only applies to one object, so this'll get hit fairly reguarly
                        // for any templates that take > 1 object
                        if (group.ValidForType != o.GetType().Name)
                        {
                            Log.DebugFormat("The type {0} is not valid for the group with selector {1}, display name {2}", o.GetType(), group.Selector, group.DisplayName);
                            continue;
                        }

                        if (currentObject == null)
                        {
                            currentObject = o;
                        }

                        Log.DebugFormat("Searching for users...");

                        string[] parts = group.Selector.Split('.');

                        for (int i = 0; i < parts.Length; i++)
                        {
                            if (currentObject == null)
                            {
                                // if we ever reach a null object in the chain, just skip it and move on

                                Log.DebugFormat("Null object found.");
                                break;
                            }

                            string currentValue = parts[i];

                            PropertyInfo pi = currentObject.GetType().GetProperty("Id", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                            string id = pi != null ? pi.GetValue(currentObject, null).ToString() : "Unknown";
                            Log.DebugFormat("Object of type {0} found, id: {1}, searching for property {2}", currentObject.GetType(), id, currentValue);

                            if (currentValue.IndexOf("(") == -1)
                            {
                                if (typeof(ICollection).IsAssignableFrom(currentObject.GetType()))
                                {
                                    foreach (object item in (ICollection)currentObject)
                                    {
                                        pi = item.GetType().GetProperty(currentValue, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                        if (pi == null)
                                        {
                                            Log.ErrorFormat("The type {0} does not have a property {1}", item.GetType(), currentValue);
                                            continue;
                                        }
                                        if (i == parts.Length - 1)
                                        {
                                            userIds.Add((int)pi.GetValue(item, null));
                                        }
                                        else
                                        {
                                            currentObject = pi.GetValue(item, null);
                                        }
                                    }
                                }
                                else
                                {
                                    pi = currentObject.GetType().GetProperty(currentValue, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                    if (pi == null)
                                    {
                                        Log.ErrorFormat("The type {0} does not have a property {1}", currentObject.GetType(), currentValue);
                                        continue;
                                    }
                                    if (i == parts.Length - 1)
                                    {
                                        userIds.Add((int)pi.GetValue(currentObject, null));
                                    }
                                    else
                                    {
                                        currentObject = pi.GetValue(currentObject, null);
                                    }
                                }
                            }
                            else
                            {
                                MethodInfo mi = currentObject.GetType().GetMethod(currentValue.Replace("(", "").Replace(")", ""), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                if (mi == null)
                                {
                                    Log.ErrorFormat("The type {0} does not have a method {1}", currentObject.GetType(), currentValue);
                                    continue;
                                }

                                if (i == parts.Length - 1)
                                {
                                    if (typeof(ICollection).IsAssignableFrom(mi.ReturnType))
                                    {
                                        userIds.AddRange((IEnumerable<int>)mi.Invoke(currentObject, null));
                                    }
                                    else
                                    {
                                        userIds.Add((int)mi.Invoke(currentObject, null));
                                    }
                                }
                                else
                                {
                                    currentObject = mi.Invoke(currentObject, null);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // errors here should never prevent things fron continuing
                    Log.Error(ex);
                }
            }
            // make sure if the user id is in the list > 1 time (for ex, both a student and a resource manager),
            // that we only send it 1x to them

            Log.DebugFormat("The following user IDs were found: {0}", string.Join(",", userIds.Distinct().ToList().ConvertAll(s => s.ToString()).ToArray()));
            return userIds.Distinct().ToList();
        }

        public static string ParseUserTemplateParameters(string source, User user) 
        {
            var pattern = "##(?<userProperty>[^\\s#]*)##";
            return Regex.Replace(source, pattern, (Match m) =>
                {
                    try
                    {
                        var propertyName = m.Groups["userProperty"].Value;
                        var property = user.GetType().GetProperty(propertyName);
                        return property.GetValue(user, null).ToString();
                    }
                    catch { return m.Value; }
                });
        }


    }
}
