﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Comparers
{
    public class TrainingProgramCourseComparer : IEqualityComparer<TrainingProgramCourse>
    {
        public bool Equals(TrainingProgramCourse x, TrainingProgramCourse y)
        {
            if (x.CourseID == y.CourseID)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int GetHashCode(TrainingProgramCourse c)
        {
            string id = c.CourseID.ToString();
            return id.GetHashCode();
        }
    }
}
