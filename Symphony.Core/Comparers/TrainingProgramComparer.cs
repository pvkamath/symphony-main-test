﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Comparers
{
    public class TrainingProgramComparer : IEqualityComparer<TrainingProgram>
    {
        public bool Equals(TrainingProgram x, TrainingProgram y)
        {
            if (x.Id == y.Id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int GetHashCode(TrainingProgram c)
        {
            string id = c.Id.ToString();
            return id.GetHashCode();
        }
    }
}
