﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Comparers
{
    public class ExternalSystemCompareer : IEqualityComparer<ExternalSystem>
    {
        public bool Equals(ExternalSystem x, ExternalSystem y)
        {
            if (x.Id == y.Id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int GetHashCode(ExternalSystem e)
        {
            string id = e.Id.ToString();
            return id.GetHashCode();
        }
    }
}
