﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Comparers
{
    public class UpcomingEventComparer : IEqualityComparer<UpcomingEvent>
    {
        public bool Equals(UpcomingEvent x, UpcomingEvent y)
        {
            if (x.EventID == y.EventID)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int GetHashCode(UpcomingEvent e)
        {
            string id = e.EventID.ToString();
            return id.GetHashCode();
        }
    }
}
