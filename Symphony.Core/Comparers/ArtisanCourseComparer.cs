﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Comparers
{
    public class ExternalArtisanCourseComparer : IEqualityComparer<ArtisanCourse>
    {
        public bool Equals(ArtisanCourse x, ArtisanCourse y)
        {
            if (x.ExternalID == y.ExternalID)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int GetHashCode(ArtisanCourse c)
        {
            string id = c.ExternalID.ToString();
            return id.GetHashCode();
        }
    }
}
