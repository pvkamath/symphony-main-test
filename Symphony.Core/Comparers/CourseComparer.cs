﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Comparers
{
    public class CourseComparer : IEqualityComparer<Course>
    {
        public bool Equals(Course x, Course y)
        {
            if (x.Id == y.Id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int GetHashCode(Course c)
        {
            string id = c.Id.ToString();
            return id.GetHashCode();
        }
    }
}
