﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Comparers
{
    public class ArtisanPageComparer 
    {
        public static int CompareArtisanPageSort(ArtisanPage x, ArtisanPage y)
        {
            if (x.Sort > y.Sort)
            {
                return 1;
            }
            if (y.Sort > x.Sort)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
