﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using unoidl.com.sun.star.uno;
using uno.util;
using unoidl.com.sun.star.lang;
using unoidl.com.sun.star.frame;
using unoidl.com.sun.star.beans;
using uno;
using Microsoft.Win32;

namespace Symphony.Core.Converters
{
    public class PowerPointToHtml
    {
        /// <summary>
        /// Takes a powerpoint file and converts to html
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        public static void Convert(string inputFile, string outputFile)
        {
            const string OO_PATH_SUFFIX = @"\bin";
            const string OO_REG_UNO_INSTALL_KEY = @"SOFTWARE\Wow6432Node\LibreOffice\UNO\InstallPath";
            const string OO_REG_UNO_INSTALL_NAME = "LibreOffice 3.3";
            const string OO_REG_INSTALL_KEY = @"SOFTWARE\Wow6432Node\LibreOffice\Layers_\URE\1";
            const string OO_REG_INSTALL_NAME = "UREINSTALLLOCATION";

            //Note - Reg_Retrieve_String is my method to return a string from the Registry!
            string oo_UNO_Path = Registry.LocalMachine.OpenSubKey(OO_REG_UNO_INSTALL_KEY).GetValue(OO_REG_UNO_INSTALL_NAME).ToString();
            string oo_Path_Mod = Registry.LocalMachine.OpenSubKey(OO_REG_INSTALL_KEY).GetValue(OO_REG_INSTALL_NAME).ToString();
            //string oo_Path_Mod = @"C:\Program Files (x86)\LibreOffice 3\URE\"; // key.GetValue(OO_REG_INSTALL_KEY, OO_REG_INSTALL_NAME).ToString();
            oo_Path_Mod += OO_PATH_SUFFIX;

            Environment.SetEnvironmentVariable("UNO_PATH", oo_UNO_Path);
            Environment.SetEnvironmentVariable("PATH", Environment.GetEnvironmentVariable("PATH") + ";" + oo_Path_Mod);
 

            XComponentContext context = Bootstrap.bootstrap();
            XMultiServiceFactory factory = (XMultiServiceFactory)context.getServiceManager();
            
            XComponentLoader loader = (XComponentLoader)factory.createInstance("com.sun.star.frame.Desktop");

            XComponent xComponent = loader.loadComponentFromURL(inputFile, "_blank", 0, new PropertyValue[] { 
                GetPropertyValue("Hidden", true) });

            PropertyValue[] propertyValues = new PropertyValue[]{
                GetPropertyValue("Overwrite", true),
                GetPropertyValue("FilterName", "impress_html_Export")
            };

            // "writer_pdf_Export" => pdf

            XStorable xStorable = xComponent as XStorable;
            xStorable.storeToURL(outputFile, propertyValues);
        }

        private static PropertyValue GetPropertyValue(string name, bool value)
        {
            PropertyValue pv = new PropertyValue();
            pv.Name = name;
            pv.Value = new Any(value);
            return pv;
        }

        private static PropertyValue GetPropertyValue(string name, string value)
        {
            PropertyValue pv = new PropertyValue();
            pv.Name = name;
            pv.Value = new Any(value);
            return pv;
        }
    }
}
