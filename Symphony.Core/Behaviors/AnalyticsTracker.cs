﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using GoogleAnalyticsTracker.Simple;

namespace Symphony.Core.Behaviors
{
    public class AnalyticsTracker : IDispatchMessageInspector
    {
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                var url = HttpContext.Current.Request.Url;
                SimpleTracker tracker = new SimpleTracker("UA-XXXXXX-XX", "www.example.org");
                Task result = tracker.TrackPageViewAsync(url.AbsolutePath, url.OriginalString);

                //result.
                //var url = HttpContext.Current.Request.Url;
                //url.OriginalString
                // TODO: tag in google analytics
            }
            //Console.WriteLine("IDispatchMessageInspector.AfterReceiveRequest called.");
            return null;
        }

        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            //Console.WriteLine("IDispatchMessageInspector.BeforeSendReply called.");
        }
    }
}
