﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name="result")]
    public class Result
    {
        public Result() { }

        public Result(Exception ex)
        {
            Error = ex.Message;
            this.Success = false;
        }

        [DataMember(Name="error")]
        public string Error { get; set; }

        private bool _success = true;
        [DataMember(Name = "success")]
        public bool Success { get { return _success; } set { this._success = value; } }

        [DataMember(Name = "notice")]
        public string Notice { get; set; }
    }
}
