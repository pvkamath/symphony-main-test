﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using SubSonic;
using Symphony.Core;
using System.Reflection;
using Symphony.Core.Extensions;

namespace Symphony.Core.Models
{
    [DataContract(Name = "result")]
    public class PagedResult<ModelType> : Result where ModelType : new()
    {
        public PagedResult(Exception ex) : base(ex) { }
        
        public PagedResult(IEnumerable<ModelType> data, int pageIndex, int pageSize, int totalSize)
        {
            // no ordering
            Data = data;
            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalSize = totalSize;
        }

        public PagedResult(IQueryable<ModelType> data, int pageIndex, int pageSize, string orderBy, OrderDirection orderDir)
        {
            int total = data.Count();
            // auto-handles the order-by
            if (orderDir == OrderDirection.Descending)
            {
                data = data.OrderByDescending(orderBy); 
            }
            else
            {
                data = data.OrderBy(orderBy);
            }
            Data = data.Skip(pageIndex * pageSize).Take(pageSize).ToList();
            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalSize = total;
        }

        public PagedResult(SqlQuery query, int pageIndex, int pageSize, string orderBy, OrderDirection orderDir)
        {
            int total = query.GetRecordCount();
            // need to manually order
            if (query.OrderBys.Count == 0)
            {
                if (string.IsNullOrEmpty(orderBy))
                {
                    orderBy = Utilities.GetSortOrder(typeof(ModelType));
                }
                if (orderDir == OrderDirection.Default)
                {
                    orderDir = Utilities.GetOrderDirection(typeof(ModelType));
                }
                if (orderDir == OrderDirection.Descending)
                {
                    query = query.OrderDesc(orderBy);
                }
                else
                {
                    query = query.OrderAsc(orderBy);
                }
            }
            Data = query.Paged(pageIndex+1, pageSize).ExecuteTypedList<ModelType>();
            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalSize = total;
        }

        /// <summary>
        /// Loops over all the items in Data, finding all properties that are dates and setting the UTC flag indicator on them
        /// </summary>
        public PagedResult<ModelType> WithUtcFlag()
        {
            PropertyInfo[] properties = typeof(ModelType).GetProperties().Where(p => p.PropertyType == typeof(DateTime) || p.PropertyType == typeof(DateTime?)).ToArray();
            foreach (ModelType m in this.Data)
            {
                foreach (PropertyInfo pi in properties)
                {
                    object o = pi.GetValue(m, null);
                    if (o != null)
                    {
                        try
                        {
                            pi.SetValue(m, DateTime.SpecifyKind((DateTime)o, DateTimeKind.Utc), null);
                        }
                        catch { }
                    }
                }
            }
            return this;
        }

        [DataMember(Name = "pageIndex")]
        public int PageIndex { get; set; }

        [DataMember(Name = "pageSize")]
        public int PageSize { get; set; }

        [DataMember(Name = "totalSize")]
        public int TotalSize { get; set; }

        [DataMember(Name = "data")]
        public IEnumerable<ModelType> Data { get; set; }
    }
}
