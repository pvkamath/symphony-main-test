﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using SubSonic;

namespace Symphony.Core.Models
{
    [DataContract(Name="multipleResult")]
    public class MultipleResult<ModelType> : Result where ModelType : new()
    {
        public MultipleResult(Exception ex) : base(ex) { }

        public MultipleResult(SqlQuery query)
        {
            this.Data = query.ExecuteTypedList<ModelType>();
        }

        public MultipleResult(List<ModelType> data)
        {
            this.Data = data;
        }

        [DataMember(Name = "data")]
        public List<ModelType> Data { get; set; }
    }
}
