﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using SubSonic;

namespace Symphony.Core.Models
{
    [DataContract(Name="singleResult")]
    public class SingleResult<ModelType> : SimpleSingleResult<ModelType> where ModelType : new()
    {
        public SingleResult(Exception ex) : base(ex) { }

        public SingleResult(ModelType data) : base(data) { }
        
        public SingleResult(SqlQuery query)
        {
            this.Data = query.ExecuteSingle<ModelType>();
        }
    }
}
