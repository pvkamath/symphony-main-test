﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using SubSonic;

namespace Symphony.Core.Models
{
    [DataContract(Name="singleResult")]
    public class SimpleSingleResult<ModelType> : Result
    {
        public SimpleSingleResult() { }

        public SimpleSingleResult(Exception ex) : base(ex) { }

        public SimpleSingleResult(ModelType data)
        {
            this.Data = data;
        }

        [DataMember(Name = "data")]
        public ModelType Data { get; set; }
    }
}
