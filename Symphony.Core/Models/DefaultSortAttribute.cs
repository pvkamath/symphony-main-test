﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DefaultSortAttribute : Attribute
    {
        public DefaultSortAttribute()
        {
            this.Direction = OrderDirection.Ascending;
        }

        /// <summary>
        /// The name of the column to use if no ordering is specified
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// The direction to sort
        /// </summary>
        public OrderDirection Direction { get; set; }
    }
}
