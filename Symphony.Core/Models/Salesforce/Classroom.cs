﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using SubSonic;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "class")]
    [DefaultSort(ColumnName = "Name")]
    public class Classroom : Model
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }

        [DataMember(Name = "webinarKey", EmitDefaultValue = false)]
        public string WebinarKey { get; set; }

        [DataMember(Name = "description", EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(Name = "internalCode", EmitDefaultValue = false)]
        public string InternalCode { get; set; }

        [DataMember(Name = "room")]
        public Room Room { get; set; }

        [DataMember(Name = "timeZoneId", EmitDefaultValue = false)]
        public int TimeZoneID { get; set; }

        [DataMember(Name = "timeZoneDescription", EmitDefaultValue = false)]
        public string TimeZoneDescription { get; set; }

        [DataMember(Name = "capacityOverride", EmitDefaultValue = false)]
        public int CapacityOverride { get; set; }

        [DataMember(Name = "lockedIndicator", EmitDefaultValue = false)]
        public bool LockedIndicator { get; set; }

        [DataMember(Name = "objective", EmitDefaultValue = false)]
        public string Objective { get; set; }

        [DataMember(Name = "isVirtual", EmitDefaultValue = false)]
        public bool IsVirtual { get; set; }

        [DataMember(Name = "surveyId", EmitDefaultValue = false)]
        public int SurveyID { get; set; }

        [DataMember(Name = "venue", EmitDefaultValue = false)]
        public Venue Venue { get; set; }

        [DataMember(Name = "minClassDate", EmitDefaultValue = false)]
        public DateTime? MinClassDate { get; set; }

        public string ClassDates { get; set; }

        [DataMember(Name = "classDates", EmitDefaultValue = false)]
        public List<DateTime> Dates
        {
            get
            {
                if (string.IsNullOrEmpty(ClassDates))
                {
                    return null;
                }
                List<DateTime> dates = new List<DateTime>();
                foreach (string str in ClassDates.Split(','))
                {
                    dates.Add(DateTime.Parse(str.Trim()));
                }
                return dates;
            }
            set { }
        }


        [DataMember(Name = "dateList")]
        public List<ClassDate> DateList { get; set; }

        [DataMember(Name = "onlineCourseId")]
        public int OnlineCourseID { get; set; }

        [DataMember(Name = "approvalRequired")]
        public bool ApprovalRequired { get; set; }

        [DataMember(Name = "isThirdParty")]
        public bool IsThirdParty { get; set; }

        [DataMember(Name = "totalDuration")]
        public int TotalDuration { get; set; }

        [DataMember(Name = "totalCost")]
        public decimal TotalCost { get; set; }

        [DataMember(Name = "registrations")]
        public Registrations Registrations { get; set; }

        [DataMember(Name = "externalId")]
        public string ExternalID { get; set; }
    }
}
