﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "state")]
    [DefaultSort(ColumnName = "name")]
    public class State : Model
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "abbreviation")]
        public string Abbreviation { get; set; }
    }
}
