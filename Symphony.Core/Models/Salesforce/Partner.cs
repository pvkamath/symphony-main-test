﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "partner")]
    public class Partner : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "partnerId")]
        public int PartnerId { get; set; }

        [DataMember(Name = "partnerCode")]
        public string PartnerCode { get; set; }

        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }
    }
}
