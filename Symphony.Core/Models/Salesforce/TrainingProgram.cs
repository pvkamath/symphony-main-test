﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "trainingProgram")]
    public class TrainingProgram : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "categoryName")]
        public string CategoryName { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public DateTime? EndDate { get; set; }

        [DataMember(Name = "dueDate")]
        public DateTime? DueDate { get; set; }

        [DataMember(Name = "courseCount")]
        public int CourseCount { get; set; }

        [DataMember(Name = "requiredCourses")]
        public List<Course> RequiredCourses { get; set; }

        [DataMember(Name = "electiveCourses")]
        public List<Course> ElectiveCourses { get; set; }

        [DataMember(Name = "optionalCourses")]
        public List<Course> OptionalCourses { get; set; }

        [DataMember(Name = "finalAssessments")]
        public List<Course> FinalAssessments { get; set; }

        [DataMember(Name = "externalId")]
        public string ExternalID { get; set; }

        [DataMember(Name = "sessions")]
        public List<Session> Sessions { get; set; }
        
    }
}
