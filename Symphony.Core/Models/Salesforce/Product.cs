﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "product")]
    [DefaultSort(ColumnName = "name")]
    public class Product : IssueCapableModel
    {
        [DataMember(Name = "productId")]
        public string ProductId { get; set; }

        [DataMember(Name = "sku")]
        public string Sku { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "businessUnit")]
        public string BusinessUnit { get; set; }

        [DataMember(Name = "discipline")]
        public string Discipline { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "private")]
        public bool Private { get; set; }

        [DataMember(Name = "privateCompanyId")]
        public int PrivateCompanyId { get; set; }

        [DataMember(Name = "listPrice")]
        public decimal ListPrice { get; set; }

        [DataMember(Name = "cost")]
        public decimal Cost { get; set; }

        [DataMember(Name = "contentType")]
        public string ContentType { get; set; }

        [DataMember(Name = "deliveryMethod")]
        public string DeliveryMethod { get; set; }

        [DataMember(Name = "deliveryType")]
        public string DeliveryType { get; set; }

        [DataMember(Name = "state")]
        public Salesforce.State State { get; set; }

        [DataMember(Name = "hours")]
        public int Hours { get; set; }

        [DataMember(Name = "sessions")]
        public List<Salesforce.Session> Sessions { get; set; }
    }
}
