﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "products")]
    public class ProductList : Model
    {
        [DataMember(Name = "products")]
        public List<Product> Products { get; set; }

        public void AddProduct(Product p)
        {
            if (this.Products == null)
            {
                this.Products = new List<Product>();
            }

            this.Products.Add(p);
        }
    }
}
