﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "user")]
    [DefaultSort(ColumnName = "LastName")]
    public class User : IssueCapableModel
    {
        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "janrainUserUuid")]
        public string JanrainUserUuid { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "addresses")]
        public List<Salesforce.Address> Addresses { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "mobile")]
        public string Mobile { get; set; }

        [DataMember(Name = "fax")]
        public string Fax { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "salesforceContactId")]
        public string SalesforceContactId { get; set; }

        [DataMember(Name = "salesforceAccountName")]
        public string SalesforceAccountName { get; set; }

        [DataMember(Name = "salesforceAccountId")]
        public string SalesforceAccountId { get; set; }

        [DataMember(Name = "isEnabled")]
        public bool IsEnabled { get; set; }

        [DataMember(Name = "auth")]
        public string Auth { get; set; }

        [DataMember(Name = "isAuthRequired")]
        public bool IsAuthRequired { get; set; }

        [DataMember(Name = "resetPasswordRequired")]
        public bool? ResetPasswordRequired { get; set; }

        [DataMember(Name = "nmlsNumber")]
        public string NmlsNumber { get; set; }
    }
}
