﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "entitlements")]
    public class EntitlementList : Model
    {
        [DataMember(Name = "entitlements")]
        public List<Entitlement> Entitlements { get; set; }
    }
}
