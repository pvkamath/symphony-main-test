﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Extensions;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "course")]
    public class Course : CourseRecord
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "internalCode")]
        public string InternalCode { get; set; }

        [DataMember(Name = "courseObjective")]
        public string CourseObjective { get; set; }

        /// <summary>
        /// This is the date the classroom course began; it's null for online courses
        /// </summary>
        [DataMember(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// This is the date the classroom course ended; it's null for online courses
        /// </summary>
        [DataMember(Name = "endDate")]
        public DateTime? EndDate { get; set; }

        
        [DataMember(Name = "numberOfDays")]
        public int NumberOfDays { get; set; }

        [DataMember(Name = "dailyDuration")]
        public int DailyDuration { get; set; }

        [DataMember(Name = "courseCompletionTypeId")]
        public int CourseCompletionTypeID { get; set; }

        [DataMember(Name = "courseCompletionTypeDescription")]
        public string CourseCompletionTypeDescription { get; set; }

        /// <summary>
        /// For classroom courses with an "Online Test" completion type, the online course from which to draw scores
        /// </summary>
        [DataMember(Name = "onlineCourseId")]
        public int OnlineCourseID { get; set; }

        /// <summary>
        /// Used in classroom when displaying the selected online course so we avoid an extra lookup
        /// </summary>
        [DataMember(Name = "onlineCourseName")]
        public string OnlineCourseName { get; set; }

        /// <summary>
        /// the online course to use as a survey for this course
        /// </summary>
        [DataMember(Name = "surveyId")]
        public int SurveyID { get; set; }

        
        /// <summary>
        /// Name of the survey
        /// </summary>
        [DataMember(Name = "surveyName")]
        public string SurveyName { get; set; }

        
        [DataMember(Name = "presentationTypeId")]
        public int PresentationTypeID { get; set; }

        [DataMember(Name = "presentationTypeDescription")]
        public string PresentationTypeDescription { get; set; }

        [DataMember(Name = "publicIndicator")]
        public bool PublicIndicator { get; set; }

        [DataMember(Name = "hasEnded")]
        public bool HasEnded
        {
            get { return EndDate.HasValue && DateTime.Now.ToUniversalTime() > EndDate.Value.WithUtcFlag(); }
            private set { return; }
        }

        [DataMember(Name = "hasStarted")]
        public bool HasStarted
        {
            get { return StartDate.HasValue && DateTime.Now.ToUniversalTime() > StartDate.Value.WithUtcFlag(); }
            private set { return; }
        }

        [DataMember(Name = "isStartingSoon")]
        public bool IsStartingSoon
        {
            get
            {
                int i;
                if (!int.TryParse(ConfigurationManager.AppSettings["startingSoonMinutes"], out i))
                {
                    i = 15;
                }
                return StartDate.HasValue && DateTime.Now.ToUniversalTime().AddMinutes(i) > StartDate.Value.WithUtcFlag();
            }
            private set { return; }
        }

        /// <summary>
        /// When to allow re-testing
        /// </summary>
        [DataMember(Name = "retestMode")]
        public int RetestMode { get; set; }

        /// <summary>
        /// The number of allowed retakes
        /// </summary>
        [DataMember(Name = "retries")]
        public int? Retries { get; set; }

        [DataMember(Name = "cost")]
        public decimal Cost { get; set; }

        [DataMember(Name = "keywords")]
        public string Keywords { get; set; }

        [DataMember(Name = "isTest")]
        public bool IsTest { get; set; }

        [DataMember(Name = "isSurvey")]
        public bool IsSurvey { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "modifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [DataMember(Name = "version")]
        public int Version { get; set; }

        [DataMember(Name = "webinarRegistrationId")]
        public string WebinarRegistrationID { get; set; }

        [DataMember(Name = "webinarKey")]
        public string WebinarKey { get; set; }

        [DataMember(Name = "webinarLaunchUrl")]
        public string WebinarLaunchURL
        {
            get { return Model.GTWLaunchUrl(WebinarKey, WebinarRegistrationID); }
            set { }
        }

        [DataMember(Name = "categoryId")]
        public int CategoryId { get; set; }

        [DataMember(Name = "categoryName")]
        public string CategoryName { get; set; }

        [DataMember(Name = "artisanCourseId")]
        public int? ArtisanCourseID { get; set; }

        [DataMember(Name = "messageBoardId")]
        public int MessageBoardID { get; set; }

        [DataMember(Name = "proctorRequiredIndicator")]
        public bool? ProctorRequiredIndicator { get; set; }

        [DataMember(Name = "messageBoardName")]
        public string MessageBoardName { get; set; }

        [DataMember(Name = "canMoveForwardIndicator")]
        public bool CanMoveForwardIndicator { get; set; }

        [DataMember(Name = "enforceCanMoveForwardIndicator")]
        public bool? EnforceCanMoveForwardIndicator { get; set; }

        [DataMember(Name = "classes")]
        public List<Classroom> Classes { get; set; }

        [DataMember(Name = "externalId")]
        public string ExternalID { get; set; }
    }
}
