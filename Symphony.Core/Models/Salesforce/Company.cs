﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "company")]
    [DefaultSort(ColumnName = "name")]
    public class Company : IssueCapableModel
    {
        [DataMember(Name = "companyId")]
        public int CompanyId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "addresses")]
        public List<Salesforce.Address> Addresses { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "fax")]
        public string Fax { get; set; }

        [DataMember(Name = "salesforceAccountId")]
        public string SalesforceAccountId { get; set; }

        [DataMember(Name = "salesforceContactId")]
        public string SalesforceContactId { get; set; }

        [DataMember(Name = "isEnabled")]
        public bool? IsEnabled { get; set; }
    }
}
