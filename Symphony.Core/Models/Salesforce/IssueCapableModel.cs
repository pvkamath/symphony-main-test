﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{

    [DataContract(Name = "issueCapableModel")]
    public class IssueCapableModel : Model
    {
        [DataMember(Name = "issues")]
        public List<Salesforce.Issue> Issues { get; set; }

        [DataMember(Name = "success")]
        public bool Success { get; set; }

        public void SetIssue(string type, string code, string message)
        {
            Issue issue = new Issue();
            issue.type = type;
            issue.Code = code;
            issue.Message = message;

            if (Issues == null)
            {
                Issues = new List<Issue>();
            }

            issue.Number = Issues.Count + 1;

            Issues.Add(issue);

            Success = false;
        }
    }
}
