﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using SubSonic;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "class")]
    [DefaultSort(ColumnName = "Name")]
    public class ClassroomClass : Models.ClassroomClass
    {
        
        [DataMember(Name = "roomInternalCode")]
        public string RoomInternalCode { get; set; }


        [DataMember(Name = "venueInternalCode")]
        public string VenueInternalCode { get; set; }

        [DataMember(Name = "roomCost")]
        public decimal RoomCost { get; set; }

        [DataMember(Name = "capacity")]
        public int Capacity { get; set; }

        [DataMember(Name = "networkAccessIndicator")]
        public bool NetworkAccessIndicator { get; set; }

        [DataMember(Name = "stateId")]
        public int StateID { get; set; }

        [DataMember(Name = "stateDescription")]
        public string StateDescription { get; set; }
    }
}
