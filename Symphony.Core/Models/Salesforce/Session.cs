﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "session")]
    [DefaultSort(ColumnName = "sessionId")]
    public class Session : IssueCapableModel
    {
        [DataMember(Name = "sessionId")]
        public int SessionId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "beginDateTime")]
        public string BeginDateTime { get; set; }

        [DataMember(Name = "endDateTime")]
        public string EndDateTime { get; set; }

        [DataMember(Name = "durationInDays")]
        public int DurationInDays { get; set; }

        [DataMember(Name = "private")]
        public bool Private { get; set; }

        [DataMember(Name = "privateCompanyId")]
        public int PrivateCompanyId { get; set; }

        [DataMember(Name = "privateBranchId")]
        public int PrivateBranchId { get; set; }

        [DataMember(Name = "privateBranchName")]
        public string PrivateBranchName { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public Salesforce.State State { get; set; }

        [DataMember(Name = "details")]
        public string Details { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "onlineRegistrationAllowed")]
        public bool OnlineRegistrationAllowed { get; set; }

        [DataMember(Name = "currentEnrollment")]
        public int CurrentEnrollment { get; set; }

        [DataMember(Name = "maxEnrollment")]
        public int MaxEnrollment { get; set; }
    }
}
