﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract]
    public class Registration : Model
    {
        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        [DataMember(Name = "registrationStatusId")]
        public int RegistrationStatusID { get; set; }

        [DataMember(Name = "registrantId")]
        public int RegistrantID { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "middleName")]
        public string MiddleName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "fullName")]
        public string FullName
        {
            get
            {
                return Model.FormatFullName(FirstName, MiddleName, LastName);
            }
            set { }
        }
    }
}
