﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "entitlement")]
    [DefaultSort(ColumnName = "salesforceEntitlementId")]
    public class Entitlement : IssueCapableModel
    {
        [DataMember(Name = "salesforceEntitlementId")]
        public string SalesforceEntitlementId { get; set; }

        [DataMember(Name = "salesforceProductId")]
        public string SalesforceProductId { get; set; }

        [DataMember(Name = "productId")]
        public string ProductId { get; set; }

        [DataMember(Name = "productCategory")]
        public string ProductCategory { get; set; }

        [DataMember(Name = "entitlementId")]
        public int EntitlementId { get; set; }

        [DataMember(Name = "entitlementPurchaseDate")]
        public string EntitlementPurchaseDate { get; set; }

        [DataMember(Name = "entitlementStartDate")]
        public string EntitlementStartDate { get; set; }

        [DataMember(Name = "entitlementEndDate")]
        public string EntitlementEndDate { get; set; }

        [DataMember(Name = "entitlementDuration")]
        public int EntitlementDuration { get; set; }

        [DataMember(Name = "entitlementDurationUnits")]
        public string EntitlementDurationUnits { get; set; }

        [DataMember(Name = "isEnabled")]
        public bool IsEnabled { get; set; }

        [DataMember(Name = "paymentRefId")]
        public int? PaymentRefId { get; set; }

        [DataMember(Name = "partnerId")]
        public int PartnerId { get; set; }

        [DataMember(Name = "partnerCode")]
        public string PartnerCode { get; set; }

        [DataMember(Name = "audienceCodes")]
        public string[] AudienceCodes { get; set; }

        [DataMember(Name = "partnerDisplayName")]
        public string PartnerDisplayName { get; set; }

        [DataMember(Name = "isSuppressEmail")]
        public bool IsSuppressEmail { get; set; }

        [DataMember(Name = "sessionId")]
        public int? SessionId { get; set; }

        // Flag to prevent rollup. Need this for the pass through so we can force an immediate rollup without
        // waiting for hangfire. Otherwise the profession selector may pop up. We could manually generate the rollup
        // first, however we need to run the enrolment first in order to get the training program id. 
        [IgnoreDataMember]
        public bool? IsSuppressRollup { get; set; }

        // Used for notification purposes in symphony only
        [IgnoreDataMember]
        public Data.TrainingProgram TrainingProgram { get; set; }

        // Used for notification purposes in symphony only
        [IgnoreDataMember]
        public Data.ClassX Session { get; set; }

    }
}
