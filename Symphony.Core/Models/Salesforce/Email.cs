﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract(Name = "email")]
    public class Email : Model
    {
        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}
