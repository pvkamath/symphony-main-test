﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models.Salesforce
{
    [DataContract]
    public class Registrations : Model
    {
        [DataMember(Name = "awaitingApproval")]
        public List<Registration> AwaitingApproval { get; set; }

        [DataMember(Name = "registered")]
        public List<Registration> Registered { get; set; }

        [DataMember(Name = "waitList")]
        public List<Registration> WaitList { get; set; }

        [DataMember(Name = "denied")]
        public List<Registration> Denied { get; set; }
    }
}
