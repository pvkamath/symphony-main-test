﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Extensions;

namespace Symphony.Core.Models
{
    [DataContract(Name = "displayDate")]
    public class DisplayDate : DisplayModel
    {
        private DateTime? _date;

        private bool _isAutoSpecifyKind = true;

        [IgnoreDataMember]
        public bool IsAutoSpecificyKind
        {
            get
            {
                return _isAutoSpecifyKind;
            }
            set
            {
                _isAutoSpecifyKind = value;
            }
        }

        [DataMember(Name = "date")]
        public DateTime? Date
        {
            get
            {
                if (_isAutoSpecifyKind)
                {
                    if (_date.HasValue && !AddLocalOffset && _date.Value.Kind != DateTimeKind.Utc)
                    {
                        _date = _date.Value.WithUtcFlag();
                    }
                    else if (_date.HasValue && AddLocalOffset && _date.Value.Kind != DateTimeKind.Unspecified)
                    {
                        _date = DateTime.SpecifyKind(_date.Value, DateTimeKind.Unspecified);
                    }
                }

                return _date;
            }
            set
            {
                _date = value;
            }
        }

        [DataMember(Name = "addLocalOffset")]
        public bool AddLocalOffset
        {
            get
            {
                return Format == null || !Format.HasTimeComponent;
            }
            internal set
            {

            }
        }

        [DataMember(Name = "milliseconds")]
        public double Milliseconds
        {
            get
            {
                return Date.HasValue ? Date.Value.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, Date.Value.Kind)).TotalMilliseconds : 0;
            }
            internal set { }
        }

        [DataMember(Name = "ticks")]
        public long Ticks { 
            get {
                return Date.HasValue ? Date.Value.Ticks : 0;
            }
            internal set {} 
        }

        [DataMember(Name = "relativeMilliseconds")]
        public double RelativeMilliseconds
        {
            get
            {
                return Milliseconds - DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
            }
            internal set { }
        }

        [DataMember(Name = "relativeTicks")]
        public long RelativeTicks
        {
            get
            {
                return Date.HasValue ? Date.Value.Subtract(DateTime.UtcNow).Ticks : 0;
            }
            internal set { }
        }

        [DataMember(Name = "format")]
        public DateFormat Format { get; set; }

        [DataMember(Name = "isActive")]
        public bool IsActive { get; set; }
    }
}
