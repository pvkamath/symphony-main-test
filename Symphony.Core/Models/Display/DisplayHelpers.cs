﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    class DisplayHelpers
    {
        #region Training programs and Courses
        public static DisplayIconState GetCertificateBadge(CertificateType type, int trainingProgramID, int courseOrClassID, int userId = 0) {
            var linkUrl = Url.ViewCertificate;
            int? certificateTemplateId = 0;

            DisplayIconState certificate = new DisplayIconState()
            {
                Icon = IconState.Certificate,
                Messages = new List<Text>() { Text.ViewCertificate },
                Link = new DisplayLink()
                {
                    IsNewWindow = true,
                    BaseUrl = linkUrl,
                    ClassFormat = TextFormat.CertificateLinkClassFormat
                }
            };

            
            certificate.Link.QueryParams.Add("UserID", userId > 0 ? userId.ToString() : SymphonyController.GetUserID().ToString());
            certificate.Link.QueryParams.Add("CertificateTypeID", type.ToString());
            certificate.Link.QueryParams.Add("TrainingProgramID", trainingProgramID.ToString());
            certificate.Link.QueryParams.Add("CourseOrClassID", courseOrClassID.ToString());
            certificate.Link.QueryParams.Add("CertificateTemplateId", certificateTemplateId.ToString());

            return certificate;
        }

        public static DisplayLink GetSimpleCourseLaunchLink(int courseId, int trainingProgramId)
        {
            return new DisplayLink()
            {
                UrlFormat = TextFormat.LaunchLinkUrlFormat,
                IdFormat = TextFormat.LaunchLinkIdFormat,
                ClassFormat = TextFormat.LaunchLinkClassFormat,
                QueryParams = new KeyValueList() {
                    new DisplayKeyValue(){ Key = "CourseID", Value = courseId.ToString() },
                    new DisplayKeyValue(){ Key = "TrainingProgramID", Value = trainingProgramId.ToString() },
                    new DisplayKeyValue(){ Key = "UserID", Value = SymphonyController.GetUserID().ToString() },
                }
            };
        }

        public static DisplayLink GetRegisterLink (int courseId, int classId, long? minTicks, long? maxTicks) {
            if (maxTicks < minTicks)
            {
                maxTicks = DateTime.MaxValue.Ticks;
            }

            DisplayLink link = new DisplayLink()
            {
                BaseUrl = Url.Register,
                Title = Text.Register,
                ClassFormat = TextFormat.RegisterLinkClassFormat,
                QueryParams = new KeyValueList() {
                    new DisplayKeyValue(){ Key = "CourseID", Value = courseId.ToString() },
                    new DisplayKeyValue(){ Key = "UserID", Value = SymphonyController.GetUserID().ToString() },
                }
            };

            if (classId > 0)
            {
                link.QueryParams.Add("ClassID", classId.ToString());
            }

            if (minTicks != null)
            {
                link.QueryParams.Add(new DisplayKeyValue(){ Key = "MinDate", Value = new DateTime(minTicks.Value).ToString("M/d/yyyy") });
            }

            if (maxTicks != null)
            {
                link.QueryParams.Add(new DisplayKeyValue() { Key = "MaxDate", Value = new DateTime(maxTicks.Value).ToString("M/d/yyyy") });
            }

            return link;
        }
        public static DisplayLink GetRegisterLink(int courseId, long? minTicks, long? maxTicks)
        {
            return GetRegisterLink(courseId, 0, minTicks, maxTicks);
        }

        public static DisplayLink GetUnregisterLink(int registrationId)
        {
            return new DisplayLink()
            {
                BaseUrl = Url.Unregister,
                Title = Text.Unregister,
                ClassFormat = TextFormat.UnregisterLinkClassFormat,
                UrlFormat = TextFormat.UnregisterLinkUrlFormat,
                QueryParams = new KeyValueList() {
                    new DisplayKeyValue(){ Key = "RegistrationID", Value = registrationId.ToString() },
                }
            };
        }

        public static DisplayLink GetDetailsLink(long classId, string title)
        {
            return new DisplayLink()
            {
                BaseUrl = Url.ClassDetails,
                Title = Text.Dynamic(title),
                UrlFormat = TextFormat.ClassDetailsLinkUrlFormat,
                ClassFormat = TextFormat.ClassDetailsLinkClassFormat,
                QueryParams = new KeyValueList() {
                    new DisplayKeyValue() { Key = "ClassID", Value = classId.ToString() }
                }
            };
        }

        public static DisplayLink GetGTMLaunchLink(long id, string title, string url)
        {
            return new DisplayLink()
            {
                BaseUrl = Url.Dynamic(url),
                Title = Text.Dynamic(title),
                UrlFormat = TextFormat.GTMLaunchLinkUrlFormat,
                ClassFormat = TextFormat.GTMLaunchLinkClassFormat,
                QueryParams = new KeyValueList() {
                    new DisplayKeyValue() { Key = "EventID", Value = id.ToString() }
                }
            };
        }
        #endregion

        #region Libraries
        public static DisplayIconState GetIsFavoriteIcon(LibraryItem item)
        {
            if (item.IsFavorite)
            {
                return new DisplayIconState()
                {
                    Icon = IconState.FavoriteLibraryItem,
                    Link = new DisplayLink() {
                        BaseUrl = Url.DeleteLibraryFavorite,
                        UrlFormat = TextFormat.LibraryFavoriteFormat,
                        ClassFormat = TextFormat.ToggleFavoriteClassFormat,
                        QueryParams = new KeyValueList() {
                            new DisplayKeyValue() { Key = "itemId", Value = item.LibraryItemID.ToString() },
                            new DisplayKeyValue() { Key = "userId", Value = SymphonyController.GetUserID().ToString() }
                        }
                    },
                    Messages = new List<Text>() {
                        Text.Library_FavoriteItem
                    }
                };
            }
            return new DisplayIconState()
            {
                Icon = IconState.UnFavoriteLibraryItem,
                Link = new DisplayLink()
                {
                    BaseUrl = Url.SaveLibraryFavorite,
                    UrlFormat = TextFormat.LibraryFavoriteFormat,
                    ClassFormat = TextFormat.ToggleFavoriteClassFormat,
                    QueryParams = new KeyValueList() {
                            new DisplayKeyValue() { Key = "itemId", Value = item.LibraryItemID.ToString() },
                            new DisplayKeyValue() { Key = "userId", Value = SymphonyController.GetUserID().ToString() }
                        }
                },
                Messages = new List<Text>() {
                        Text.Library_UnFavoriteItem
                    }
            };
        }

        public static DisplayIconState GetLibraryItemTypeIcon(int libraryItemTypeId)
        {
            IconState icon = libraryItemTypeId == (int)LibraryItemType.OnlineCourse ?
                IconState.OnlineCourseLibraryItem :
                IconState.TrainingProgramLibraryItem;

            Text tooltip = libraryItemTypeId == (int)LibraryItemType.OnlineCourse ?
                Text.Library_OnlineCourseItemType :
                Text.Library_TrainingProgramItemType;

            return new DisplayIconState()
            {
                Icon = icon,
                Messages = new List<Text>()
                {
                    tooltip    
                }
            };
        }

        #endregion

    }
}
