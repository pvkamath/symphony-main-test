﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Extensions;

namespace Symphony.Core.Models
{
    [CollectionDataContract(Name = "KeyValueList", ItemName = "KeyValue")]
    public class KeyValueList : List<DisplayKeyValue>
    {
        public void Add(string key, string value)
        {
            this.Add(new DisplayKeyValue() { Key = key, Value = value });
        }

        public string this[string key]
        {
            get
            {
                var d = AsDictionary();
                if (d.ContainsKey(key))
                {
                    return d[key].Value;
                }
                return null;
            }
            set
            {
                var d = AsDictionary();
                if (d.ContainsKey(key))
                {
                    d[key].Value = value;
                }
                else
                {
                    Add(key, value);
                }
            }
        }

        public Dictionary<string, DisplayKeyValue> AsDictionary()
        {
            return this.DistinctBy(kv => kv.Key).ToList().ToDictionary(k => k.Key);
        }
    }
}
