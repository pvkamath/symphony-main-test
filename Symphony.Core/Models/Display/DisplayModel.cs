﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "displayModel")]
    [KnownType(typeof(DateText))]
    public class DisplayModel : Model
    {
        private List<Text> _messages = new List<Text>();
        [DataMember(Name = "messages")]
        public List<Text> Messages { get { return _messages; } set { _messages = value; } }

        [DataMember(Name = "messageValues")]
        public List<string> MessageStrings
        {
            get
            {
                return Messages.Select(m => m.Value).Where(m => !string.IsNullOrWhiteSpace(m)).ToList();
            }
            internal set { }
        }
    }
}
