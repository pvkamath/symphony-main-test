﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Symphony.Core.Models
{
    [DataContract(Name = "displayLaunchStep")]
    public class DisplayLaunchStep : DisplayModel
    {
        private string _code;

        [DataMember(Name = "code")]
        public string Code
        {
            get
            {
                if (string.IsNullOrEmpty(_code)) 
                {
                    Type t = this.GetType();
                    FieldInfo[] fields = t.GetFields();
                    _code = "unknown";

                    foreach (FieldInfo field in fields)
                    {
                        if (field.FieldType == typeof(DisplayLaunchStep))
                        {
                            DisplayLaunchStep step = (DisplayLaunchStep)field.GetValue(null);
                            if (step == this)
                            {
                                _code = field.Name;
                            }
                        }
                    }
                }
                return _code;
            }
            set {
                _code = value;
            }
        }

        [DataMember(Name = "title")]
        public Text Title { get; set; }

        [DataMember(Name = "content")]
        public JsonString Content { get; set; }

        [DataMember(Name = "isBlocker")]
        public bool IsBlocker { get; set; }

        [DataMember(Name = "jsonFieldName")]
        public string JsonFieldName { get; set; }

        /// <summary>
        /// If this is true, we will skip this launch step in the ui. 
        /// This can be used to ignore a particular step if something
        /// has changed between loading the training program and launching
        /// the course. 
        /// 
        /// For now this is only used on the Profession step. If there
        /// are no professions, this will be true. 
        /// 
        /// </summary>
        [DataMember(Name = "isSkip")]
        public bool IsSkip { get; set; }

        /// <summary>
        /// This is used to generate the content for 
        /// this step of the launch process
        /// 
        /// It can be used instead of any content provided
        /// in the content field.
        /// </summary>
        [DataMember(Name = "contentUrl")]
        public string ContentUrl { get; set; }

        /// <summary>
        /// This is used to provide the url to post data
        /// to for this step
        /// </summary>
        [DataMember(Name = "postUrl")]
        public string PostUrl { get; set; }

        public DisplayLaunchStep GetInstance()
        {
            return Model.Create<DisplayLaunchStep>(this);
        }

        public static DisplayLaunchStep CourseWorkLaunchStep = new DisplayLaunchStep()
        {
            Title = Text.LaunchStep_CourseUnavailable_Title,
            Content = JsonString.LaunchStep_CourseUnavailable,
            IsBlocker = true
        };

        public static DisplayLaunchStep SessionWarningLaunchStep = new DisplayLaunchStep() 
        {
            Title = Text.LaunchStep_SessionExpired_Title,
            Content = JsonString.LaunchStep_SessionExpired,
            IsBlocker = false
        };

        public static DisplayLaunchStep ProctorRequiredLaunchStep = new DisplayLaunchStep()
        {
            Title = Text.LaunchStep_ProctorRequired_Title,
            Content = JsonString.LaunchStep_ProctorRequired,
            IsBlocker = false,
            JsonFieldName = "proctorJsonString",
            PostUrl = Url.SaveProctor.Value
        };

        public static DisplayLaunchStep ValidationRequiredLaunchStep = new DisplayLaunchStep()
        {
            Title = Text.LaunchStep_ValidationRequired_Title,
            Content = JsonString.LaunchStep_ValidationRequired,
            IsBlocker = false,
            JsonFieldName = "validationJsonString",
            PostUrl = Url.SaveValidation.Value
        };

        public static DisplayLaunchStep ProfessionRequiredLaunchStep = new DisplayLaunchStep()
        {
            Title = Text.LaunchStep_ProfessionRequired_Title,
            IsBlocker = false,
            JsonFieldName = "professionJsonString",
            PostUrl = Url.SaveProfession.Value
        };

        public static DisplayLaunchStep AccreditationDisclaimerLaunchStep = new DisplayLaunchStep()
        {
            Title = Text.LaunchStep_Disclaimer_Title,
            IsBlocker = false
        };

        public static DisplayLaunchStep AffidavitRequiredLaunchStep = new DisplayLaunchStep()
        {
            Title = Text.LaunchStep_AffidavitRequired_Title,
            IsBlocker = false,
            JsonFieldName = "affidavitJsonString",
            PostUrl = Url.SaveAffidavit.Value
        };



        public static DisplayLaunchStep PrevalidationRequiredLaunchStep = new DisplayLaunchStep()
        {
            Title = Text.LaunchStep_Prevalidation_Title,
            IsBlocker = false,
            JsonFieldName = "preValidationJsonString",
            Content = JsonString.LaunchStep_PrevalidationRequired,
            PostUrl = Url.SavePreValidation.Value
        };

        public static DisplayLaunchStep ConfirmatiionLaunchStep = new DisplayLaunchStep()
        {
            Title = Text.LaunchStep_Confirmation_Title,
            IsBlocker = false
        };
    }


}
