﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "displayIconState")]
    public class DisplayIconState : DisplayModel
    {
        [DataMember(Name = "icon")]
        public IconState Icon { get; set; }

        [DataMember(Name = "link")]
        public DisplayLink Link { get; set; }

        [DataMember(Name = "isEmpty")]
        public bool IsEmpty
        {
            get
            {
                return Icon == IconState.Empty;
            }
            internal set { }
        }
    }
}
