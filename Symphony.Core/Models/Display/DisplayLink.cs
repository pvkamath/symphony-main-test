﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "displayLink")]
    public class DisplayLink : DisplayModel
    {
        private KeyValueList _queryParams = new KeyValueList();
        private List<string> _pathTokens = new List<string>();

        [DataMember(Name = "isEmpty")]
        public bool IsEmpty
        {
            get
            {
                return BaseUrl == Url.Empty;
            }
            internal set { }
        }

        [DataMember(Name = "isNewWindow")]
        public bool IsNewWindow
        {
            get;
            set; 
        }

        [DataMember(Name = "title")]
        public Text Title { get; set; }

        [DataMember(Name = "baseUrl")]
        public Url BaseUrl { get; set; }

        [DataMember(Name = "queryParams")]
        public KeyValueList QueryParams
        {
            get
            {
                return _queryParams;
            }
            set
            {
                _queryParams = value;
            }
        }

        [DataMember(Name = "urlFormat")]
        public TextFormat UrlFormat { get; set; }

        [DataMember(Name = "idFormat")]
        public TextFormat IdFormat { get; set; }

        [DataMember(Name = "classFormat")]
        public TextFormat ClassFormat { get; set; }

        [DataMember(Name = "domId")]
        public string DomId { 
            get 
            {
                string _domId = IdFormat != null ? IdFormat.Value : "";

                foreach (DisplayKeyValue kv in QueryParams)
                {
                    _domId = _domId.Replace("{" + kv.Key + "}", kv.Value);
                }

                return _domId;
            }
            internal set { }
        }

        [DataMember(Name = "domClass")]
        public string DomClass
        {
            get
            {
                string _domClass = ClassFormat != null ? ClassFormat.Value : "";

                foreach (DisplayKeyValue kv in QueryParams)
                {
                    _domClass = _domClass.Replace("{" + kv.Key + "}", kv.Value);
                }

                return _domClass;
            }
            internal set { }
        }

        [DataMember(Name = "pathTokens")]
        public List<string> PathTokens
        {
            get
            {
                return _pathTokens;
            }
            set
            {
                _pathTokens = value;
            }
        }

        [DataMember(Name = "fullUrl")]
        public string FullUrl {
            get
            {
                if (BaseUrl == Url.Empty)
                {
                    return "";
                }

                string fullUrl = BaseUrl != null ? BaseUrl.Value : "";
                    
                if (UrlFormat == null) {
                    if (fullUrl.EndsWith("?"))
                    {
                        fullUrl = fullUrl.Substring(0, fullUrl.Length - 1);
                    }

                    if (!fullUrl.EndsWith("/")) {
                        fullUrl = fullUrl + "/";
                    }

                    foreach (string token in PathTokens)
                    {
                        fullUrl += token.Replace("/", "") + "/";
                    }

                    if (QueryParams.Count > 0)
                    {
                        List<string> queryParams = new List<string>();
                        foreach (DisplayKeyValue kv in QueryParams)
                        {
                            queryParams.Add(kv.Key + "=" + kv.Value);
                        }

                        fullUrl += "?" + string.Join("&", queryParams);
                    }
                } else {

                    fullUrl = UrlFormat.Value.Replace("{base}", fullUrl);

                    foreach (DisplayKeyValue kv in QueryParams) {
                        fullUrl = fullUrl.Replace("{"+kv.Key+"}", kv.Value);
                    }
                }

                return fullUrl;
            }
            internal set { }
        }

        [DataMember(Name = "absoluteUrl")]
        public string AbsoluteUrl
        {
            get
            {
                Uri absoluteUri;
                Uri.TryCreate(FullUrl, UriKind.Absolute, out absoluteUri);

                if (absoluteUri == null)
                {
                    Uri.TryCreate(System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + FullUrl, UriKind.Absolute, out absoluteUri);
                }

                if (absoluteUri != null) {
                    return absoluteUri.ToString();
                }

                return FullUrl;
            }
            internal set { }
        }
    }
}
