﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Symphony.Core.Models.Reporting
{
    [DataContract]
    public class QlikReportParameters : ReportSystemParameters
    {
        [DataMember(Name="url")]
        public string Url { get; set; }
    }
}
