﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Configuration;

namespace Symphony.Core.Models
{
    [DataContract(Name = "model")]
    public class Model
    {
        //private static Dictionary<Type, PropertyInfo[]> propertyCache = new Dictionary<Type, PropertyInfo[]>();
        //private static object propertyCacheLock = new object();

        /// <summary>
        /// Creates an object of type T and copies any properties from <paramref name="source"/> to the new object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T Create<T>(object source)
        {
            // create an instance of the destination object
            object dest = Activator.CreateInstance<T>();

            // determine the types we're working with
            Type destType = typeof(T);
            Type sourceType = source.GetType();

            // get a list of properties from the source type
            PropertyInfo[] properties = sourceType.GetProperties(BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);

            // loop over the source properties, and if they exist on the destination, set them
            foreach (PropertyInfo pi in properties)
            {
                PropertyInfo thisPi = destType.GetProperty(pi.Name, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
                if (thisPi != null)
                {
                    if (thisPi != null && thisPi.CanWrite && (thisPi.PropertyType == pi.PropertyType || TypeDescriptor.GetConverter(pi.PropertyType).CanConvertTo(thisPi.PropertyType)))
                    {
                        try
                        {
                            if (thisPi.PropertyType == pi.PropertyType)
                            {
                                thisPi.SetValue(dest, pi.GetValue(source, null), null);
                            }
                            else
                            {
                                thisPi.SetValue(dest, TypeDescriptor.GetConverter(pi.PropertyType).ConvertTo(pi.GetValue(source, null), thisPi.PropertyType), null);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.Write("Error converting types..." + ex.Message);
                        }
                    }
                }
            }
            return (T)dest;
        }

        public static string FormatFullName(string first, string middle, string last)
        {
            return (last + ", " + first + " " + middle).Trim();
        }

        public static string GTWLaunchUrl(string webinarKey, string webinarRegistrationId)
        {
            return string.Format(ConfigurationManager.AppSettings["goToWebinarLaunchLink"], webinarKey, webinarRegistrationId);
        }

        /// <summary>
        /// Copies the properties from this object to the destination object.
        /// </summary>
        /// <param name="dest"></param>
        public void CopyTo(object dest)
        {
            // get the source and destination types
            Type sourceType = this.GetType();
            Type destType = dest.GetType();

            // get the properties of the source object
            PropertyInfo[] properties = sourceType.GetProperties(BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);

            // copy the properties to the destination object
            foreach (PropertyInfo pi in properties)
            {
                PropertyInfo thisPi = destType.GetProperty(pi.Name, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
                if (thisPi == null || !thisPi.CanWrite)
                {
                    continue;
                }
                Type destPropertyType = thisPi.PropertyType;
                if (destPropertyType.IsGenericType && destPropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    destPropertyType = destPropertyType.GetGenericArguments()[0];
                }
                if ((destPropertyType == pi.PropertyType || TypeDescriptor.GetConverter(pi.PropertyType).CanConvertTo(destPropertyType)))
                {
                    // ignore a couple special properties
                    if (thisPi.Name == "CreatedOn" || thisPi.Name == "CreatedBy" || thisPi.Name == "ModifiedOn" || thisPi.Name == "ModifiedBy")
                    {
                        continue;
                    }
                    try
                    {
                        thisPi.SetValue(dest, pi.GetValue(this, null), null);
                    }
                    catch (Exception ex)
                    {
                        Console.Write("Error converting types..." + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Copies the properties from the source object to this object.
        /// </summary>
        /// <param name="dest"></param>
        public void CopyFrom(object source)
        {
            // get the source and destination types
            Type sourceType = source.GetType();
            Type destType = this.GetType();

            // get the properties of the source object
            PropertyInfo[] properties = sourceType.GetProperties(BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);

            // copy the properties to the destination object
            foreach (PropertyInfo pi in properties)
            {
                PropertyInfo thisPi = destType.GetProperty(pi.Name, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
                if (thisPi == null || !thisPi.CanWrite)
                {
                    continue;
                }
                Type destPropertyType = thisPi.PropertyType;
                if (destPropertyType.IsGenericType && destPropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    destPropertyType = destPropertyType.GetGenericArguments()[0];
                }
                if ((destPropertyType == pi.PropertyType || TypeDescriptor.GetConverter(pi.PropertyType).CanConvertTo(destPropertyType)))
                {
                    // ignore a couple special properties
                    if (thisPi.Name == "CreatedOn" || thisPi.Name == "CreatedBy" || thisPi.Name == "ModifiedOn" || thisPi.Name == "ModifiedBy")
                    {
                        continue;
                    }
                    try
                    {
                        thisPi.SetValue(this, pi.GetValue(source, null), null);
                    }
                    catch (Exception ex)
                    {
                        Console.Write("Error converting types..." + ex.Message);
                    }
                }
            }
        }

    }
}
