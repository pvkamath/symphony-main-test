﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    public class TreeNodeEventArgs : EventArgs
    {
        public TreeNode Item { get; set; }
    }

    public class TreeNodeCollection : List<TreeNode>
    {
        public EventHandler<TreeNodeEventArgs> ItemAdded;

        public new void Add(TreeNode item)
        {
            base.Add(item);

            if (ItemAdded != null)
                ItemAdded(this, new TreeNodeEventArgs() { Item = item });
        }
    }

    [DataContract(Name="treeNode")]
    public class TreeNode
    {
        public TreeNode(string text)
        {
            this.Text = text;
            Init();
        }
        public TreeNode()
        {
            Init();
        }

        private void Init()
        {
            this.Children = new TreeNodeCollection();
            this.Children.ItemAdded += new EventHandler<TreeNodeEventArgs>(this.ChildAdded);
        }

        private void ChildAdded(object sender, TreeNodeEventArgs e)
        {
            e.Item.Parent = this;
        }

        [DataMember(Name = "children")]
        public TreeNodeCollection Children { get; set; }

        [DataMember(Name="text")]
        public string Text { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "isCollection")]
        public bool IsCollection
        {
            get
            {
                if (Tag != null)
                {
                    return typeof(ICollection).IsAssignableFrom((Type)Tag);
                }
                return false;
            }
            set { }
        }

        [DataMember(Name="singularText")]
        public string SingularText
        {
            get { return SubSonic.Sugar.Strings.PluralToSingular(Text); }
            set { }
        }

        [XmlIgnore]
        public object Tag { get; set; }

        [XmlIgnore]
        public TreeNode Parent { get; set; }

        [XmlIgnore]
        public int Level
        {
            get
            {
                if (this.Parent == null)
                    return 0;

                int i = 1;
                TreeNode parent = this.Parent;
                while ((parent = parent.Parent) != null)
                    i++;
                return i;
            }
        }

        [XmlIgnore]
        public string FullPath
        {
            get
            {
                if (this.Parent == null)
                    return Text;

                TreeNode parent = this.Parent;
                List<string> pathElements = new List<string>();
                pathElements.Add(Text);
                pathElements.Add(parent.Text);
                while ((parent = parent.Parent) != null)
                {
                    pathElements.Add(parent.Text);
                }
                pathElements.Reverse();
                return string.Join(".", pathElements.ToArray());
            }
        }
    }
}
