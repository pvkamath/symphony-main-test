﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="license")]
    public class License : Model
    {
        public License()
        {
            this.DataFields = new List<string>();
        }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "parentId")]
        public int ParentId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "dataFields")]
        public List<string> DataFields { get; set; }

        [DataMember(Name = "userFields")]
        public List<string> UserFields { get; set; }

        [DataMember(Name = "certificateFormFieldJson")]
        public string CertificateFormFieldJson { get; set; }

        [DataMember(Name = "jurisdiction")]
        public string Jurisdiction { get; set; }

        [DataMember(Name = "jurisdictionId")]
        public int JurisdictionID { get; set; }

        [DataMember(Name = "accreditationBoard")]
        public string AccreditationBoard { get; set; }

        [DataMember(Name = "accreditationBoardId")]
        public int AccreditationBoardID { get; set; }

        [DataMember(Name = "profession")]
        public string Profession { get; set; }

        [DataMember(Name = "professionId")]
        public int ProfessionID { get; set; }

        [DataMember(Name = "customerId")]
        public int? CustomerId { get; set; }

        [DataMember(Name = "customerName")]
        public string CustomerName { get; set; }

        [DataMember(Name = "hasUserLicenseNumber")]
        public bool HasUserLicenseNumber { get; set; }

        public string RequiredUserFields { get; set; }

        public string UserLicenseNumber { get; set; }

        [DataMember(Name = "expirationRuleId")]
        public int ExpirationRuleId { get; set; }

        [DataMember(Name = "expirationRuleAfterDays")]
        public int? ExpirationRuleAfterDays { get; set; }

        [DataMember(Name = "licenseAssignments")]
        public List<LicenseAssignment> LicenseAssignments { get; set; }

        [DataMember(Name = "licenseAssignmentTypes")]
        public List<string> LicenseAssignmentTypes { get; set; }

        [DataMember(Name = "renewalLeadTimeInDays")]
        public int? RenewalLeadTimeInDays { get; set; }
    }
}

