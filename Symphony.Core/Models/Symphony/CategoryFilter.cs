﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "categoryFilter")]
    public class CategoryFilter
    {
        [DataMember(Name = "categoryId")]
        public int CategoryID { get; set; }

        [DataMember(Name = "secondaryCategoryId")]
        public int SecondaryCategoryId { get; set; }
    }
}
