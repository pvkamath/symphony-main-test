﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "onlineCourseRegistration")]
    [DefaultSort(ColumnName = "LastName")]
    public class OnlineCourseRegistration : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "courseId")]
        public int CourseID { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "score")]
        public decimal Score { get; set; }

        [DataMember(Name = "attemptCount")]
        public int AttemptCount { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "courseName")]
        public string CourseName { get; set; }

        [DataMember(Name = "trainingProgramName")]
        public string TrainingProgramName { get; set; }

        [DataMember(Name = "testAttemptCount")]
        public int TestAttemptCount { get; set; }

        [DataMember(Name = "canMoveForwardIndicator")]
        public bool CanMoveForwardIndicator { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "middleName")]
        public string MiddleName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "fullName")]
        public string FullName
        {
            get
            {
                return Model.FormatFullName(FirstName, MiddleName, LastName);
            }
            set { }
        }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "expiresAfterMinutes")]
        public double? expiresAfterMinutes { get; set; }

        [DataMember(Name = "remaining")]
        public double? Remaining { get; set; }

        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        [DataMember(Name = "className")]
        public string ClassName { get; set; }

        [DataMember(Name = "classStartTime")]
        public DateTime ClassStartTime { get; set; }

        [DataMember(Name = "success")]
        public bool Success { get; set; }

        [DataMember(Name = "completion")]
        public bool Completion { get; set; }

        [DataMember(Name = "resetNotes")]
        public string ResetNotes { get; set; }

        [DataMember(Name = "highScoreDate")]
        public DateTime? HighScoreDate { get; set; }

        [DataMember(Name = "totalSeconds")]
        public long? TotalSeconds { get; set; }

        [DataMember(Name = "totalSecondsRounded")]
        public long? TotalSecondsRounded { get; set; }

        [DataMember(Name = "totalSecondsLastAttempt")]
        public long? TotalSecondsLastAttempt { get; set; }
    }
}
