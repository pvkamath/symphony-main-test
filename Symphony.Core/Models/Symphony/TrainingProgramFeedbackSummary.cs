﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Attributes;


namespace Symphony.Core.Models
{
    [DataContract(Name = "TrainingProgramFeedbackSummary")]
    public class TrainingProgramFeedbackSummary : Model
    {
        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "scoreAverage")]
        public double ScoreAverage { get; set; }

        [DataMember(Name = "feedbackTotal")]
        public int FeedbackTotal { get; set; }

        [DataMember(Name = "recommendedTotal")]
        public int RecommendedTotal { get; set; }
    }
}
