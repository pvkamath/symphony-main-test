using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "reportParameters")]
    public class ReportParameters : Model
    {
        [DataMember(Name = "courseType")]
        public int CourseType { get; set; }

        [DataMember(Name = "studentname")]
        public string StudentName { get; set; }

        [DataMember(Name = "location")]
        public string Location { get; set; }

        [DataMember(Name = "jobRole")]
        public string Jobrole { get; set; }

        [DataMember(Name = "studentStatus")]
        public string StudentStatus { get; set; }

        [DataMember(Name = "student")]
        public string Student { get; set; }

        [DataMember(Name = "audience")]
        public string Audience { get; set; }

        [DataMember(Name = "trainingProgram")]
        public string TrainingProgram { get; set; }

        [DataMember(Name = "groupBy")]
        public int GroupBy { get; set; }

        [DataMember(Name = "courseRequiredType")]
        public int CourseRequiredType { get; set; }

        [DataMember(Name = "testStatus")]
        public int TestStatus { get; set; }

        [DataMember(Name = "hireDateFrom")]
        public DateTime HireDateFrom { get; set; }

        [DataMember(Name = "hireDateTo")]
        public DateTime HireDateTo { get; set; }
        
        [DataMember(Name = "passDateFrom")]
        public DateTime PassDateFrom { get; set; }
        
        [DataMember(Name = "passDateTo")]
        public DateTime PassDateTo { get; set; }
        
        [DataMember(Name = "trainingProgramDateFrom")]
        public DateTime TrainingProgramDateFrom { get; set; }

        [DataMember(Name = "trainingProgramDateTo")]
        public DateTime TrainingProgramDateTo { get; set; }

        [DataMember(Name = "courseDueDateFrom")]
        public DateTime CourseDueDateFrom { get; set; }

        [DataMember(Name = "courseDueDateTo")]
        public DateTime CourseDueDateTo { get; set; }

        [DataMember(Name = "viewDateRangeFrom")]
        public DateTime ViewDateRangeFrom { get; set; }

        [DataMember(Name = "viewDateRangeTo")]
        public DateTime ViewDateRangeTo { get; set; }

        [DataMember(Name = "classDate")]
        public DateTime ClassDate { get; set; }

        [DataMember(Name = "classStatus")]
        public string ClassSatus { get; set; }

        [DataMember(Name = "test")]
        public string Test { get; set; }

    }
}
