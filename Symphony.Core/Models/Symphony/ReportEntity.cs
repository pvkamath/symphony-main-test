using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;
using System.Text.RegularExpressions;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportEntity")]
    [DefaultSort(ColumnName = "Name")]
    public class ReportEntity : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
		
		[DataMember(Name = "xtype")]
        public string Xtype { get; set; }
		
        [DataMember(Name = "config")]
        public string Config { get; set; }

        [DataMember(Name = "order")]
        public int Order { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "isDeprecated")]
        public bool IsDeprecated { get; set; }

        private string _tagName;

        [DataMember(Name = "tagName")]
        public string TagName
        {
            get
            { 
                if (string.IsNullOrWhiteSpace(_tagName)) {
                    Regex reg = new Regex("[^a-zA-Z0-9]");
                    string tagName = reg.Replace(Name, "");

                    _tagName = tagName.Substring(0, 1).ToLower() + tagName.Substring(1);
                }

                return _tagName;
            }
            set
            {
                _tagName = value;
            }
        }

        [DataMember(Name = "reportEntityTypeId")]
        public int ReportEntityTypeID { get; set; }

        [DataMember(Name = "symphonyTable")]
        public string SymphonyTable { get; set; }

        [DataMember(Name = "symphonyNameColumn")]
        public string SymphonyNameColumn { get; set; }

        [DataMember(Name = "symphonyIdColumn")]
        public string SymphonyIdColumn { get; set; }

        [DataMember(Name = "reportingTable")]
        public string ReportingTable { get; set; }

        [DataMember(Name = "reportingNameColumn")]
        public string ReportingNameColumn { get; set; }

        [DataMember(Name = "reportingIdColumn")]
        public string ReportingIdColumn { get; set; }
    }
}
