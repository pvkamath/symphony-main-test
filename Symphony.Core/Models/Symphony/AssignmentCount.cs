﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name="assignmentCount")]
    public class AssignmentCount : Model
    {
        [DataMember(Name = "totalAssignments")]
        public int TotalAssignments { get; set; }

        [DataMember(Name = "totalAssignmentsComplete")]
        public int TotalAssignmentsComplete { get; set; }

        [DataMember(Name = "totalAssignmentsFailed")]
        public int TotalAssignmentsFailed { get; set; }

        [DataMember(Name = "totalAssignmentsUnmarked")]
        public int TotalAssignmentsUnmarked { get; set; }

        public static AssignmentCount GetTrainingProgramAssignmentCount(TrainingProgram tp) {
            int? _totalAssignments = 0;
            int? _totalAssignmentsComplete = 0;
            int? _totalAssignmentsFailed = 0;
            int? _totalAssignmentsUnmarked = 0;
            
            tp.RequiredCourses.ForEach(c => {
                _totalAssignments += c.AssignmentsInCourse.HasValue ? c.AssignmentsInCourse.Value : 0;
                _totalAssignmentsComplete += c.AssignmentsCompleted.HasValue ? c.AssignmentsCompleted.Value : 0;
                _totalAssignmentsUnmarked += (c.AssignmentsInCourse.HasValue && c.AssignmentsMarked.HasValue) ? (c.AssignmentsInCourse - c.AssignmentsMarked) : 0;
                _totalAssignmentsFailed += (c.AssignmentsMarked.HasValue && c.AssignmentsCompleted.HasValue) ? (c.AssignmentsMarked - c.AssignmentsCompleted) : 0;
            });

            return new AssignmentCount(){
                TotalAssignments = _totalAssignments.HasValue ? _totalAssignments.Value : 0,
                TotalAssignmentsComplete = _totalAssignmentsComplete.HasValue ? _totalAssignmentsComplete.Value : 0,
                TotalAssignmentsFailed = _totalAssignmentsFailed.HasValue ? _totalAssignmentsFailed.Value : 0,
                TotalAssignmentsUnmarked = _totalAssignmentsUnmarked.HasValue ? _totalAssignmentsUnmarked.Value : 0
            };
        }
    }
}
