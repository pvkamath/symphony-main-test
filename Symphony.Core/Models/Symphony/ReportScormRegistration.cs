using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportScormRegistration")]
    [DefaultSort(ColumnName = "TestResult")]
    public class ReportScormRegistration : Model
    {
        [DataMember(Name = "scormRegistrationId")]
        public int ScormRegistrationId { get; set; }

        [DataMember(Name = "testResult")]
        public string TestResult { get; set; }

    }
}
