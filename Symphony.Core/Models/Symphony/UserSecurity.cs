﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Cryptography;
namespace Symphony.Core.Models
{
    [DataContract(Name = "userSecurity")]
    public class UserSecurity : User
    {
        [DataMember(Name = "ssn")]
        public string SSN { get; set; }

        [DataMember(Name = "dob")]
        public string DOB { get; set; }

        public byte[] HashSSN()
        {
            string salt = "h4f0819pFvqrnf42";
            SHA256 sha = new SHA256CryptoServiceProvider();
            byte[] bytes = sha.ComputeHash(System.Text.Encoding.ASCII.GetBytes(SSN + salt));
            
            SSN = String.Empty;

            return bytes;
        }

        public byte[] HashDOB()
        {
            DateTime dob = DateTime.Parse(DOB);
            DOB = string.Empty;
            string salt = "fj4FEJfjkld5432j";
            SHA256 sha = new SHA256CryptoServiceProvider();
            byte[] bytes = sha.ComputeHash(System.Text.Encoding.ASCII.GetBytes(dob.ToString("d") + salt));
            
            return bytes;
        }
    }
}
