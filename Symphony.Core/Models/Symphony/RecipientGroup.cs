﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract]
    public class RecipientGroup
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name="displayName")]
        public string DisplayName { get; set; }

    }
}
