﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Symphony.Core;
using Symphony.Core.Controllers;


namespace Symphony.Core.Models
{
    [DataContract(Name = "upcomingEvent")]
    [DefaultSort(ColumnName = "StartDate")]
    [KnownType(typeof(PublicCourse))]
    [KnownType(typeof(Meeting))]
    [KnownType(typeof(Course))]
    [KnownType(typeof(Class))]
    [KnownType(typeof(TrainingProgram))]
    public class UpcomingEvent
    {
        [DataMember(Name = "eventType")]
        public string EventType { get; set; }

        [DataMember(Name = "eventTypeId")]
        public int EventTypeID { get; set; }

        // not serialized, just used for filtering prior to returning to the client
        public string UniqueID { get; set; }

        [DataMember(Name = "eventId")]
        public long EventID { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "eventName")]
        public string EventName { get; set; }

        [DataMember(Name = "groupId")]
        public int GroupID { get; set; }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "startDate")]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public DateTime? EndDate { get; set; }

        [DataMember(Name = "registrationStatusId")]
        public int RegistrationStatusID { get; set; }

        [DataMember(Name = "registrationId")]
        public int RegistrationID { get; set; }

        [DataMember(Name = "webinarRegistrationId")]
        public string WebinarRegistrationID { get; set; }

        [DataMember(Name = "isAdminRegistration")]
        public bool IsAdminRegistration { get; set; }

        [DataMember(Name = "webinarKey")]
        public string WebinarKey { get; set; }

        [DataMember(Name = "webinarLaunchUrl")]
        public string WebinarLaunchURL
        {
            get { return Model.GTWLaunchUrl(WebinarKey, WebinarRegistrationID); }
            set { }
        }

        [DataMember(Name = "joinUrl")]
        public string JoinURL { get; set; }

        [DataMember(Name = "details")]
        public object Details { get; set; }

        [DataMember(Name = "extendedDetails")]
        public object ExtendedDetails { get; set; }

        [DataMember(Name = "messageBoardId")]
        public int MessageBoardID { get; set; }

        public bool IsClassEvent
        {
            get
            {
                return (this.EventTypeID == (int)Symphony.Core.EventType.TrainingProgramClass ||
                    this.EventTypeID == (int)Symphony.Core.EventType.PublicClass ||
                    this.EventTypeID == (int)Symphony.Core.EventType.RegisteredClass);
            }
        }

        // consider two upcoming events equal if they have the same event id, same group id, and same(ish) event type id
        // this way if we have a class and a meeting (for example) with the same event id, they'll still have different group ids
        // note though that 2 items are considered equal if they have the same id and group, and they're a "class" event
        public override bool Equals(object obj)
        {
            UpcomingEvent ue = (UpcomingEvent)obj;
            return ue.EventID == this.EventID && ue.GroupID == this.GroupID && (IsClassEvent ? ue.IsClassEvent : ue.EventTypeID == this.EventTypeID);
        }

        public override int GetHashCode()
        {
            return unchecked((int)this.EventID) ^ this.GroupID ^ this.EventTypeID;
        }

        #region Display
        [DataMember(Name = "displayStartDate")]
        public DisplayDate DisplayStartDate
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                return new DisplayDate()
                {
                    Date = StartDate,
                    Format = DateFormat.DateTimeTz
                };
            }
            internal set { }
        }
        [DataMember(Name = "displayEventLink")]
        public DisplayLink DisplayEventLink
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayLink link = new DisplayLink()
                {
                    BaseUrl = Url.Empty,
                    Title = Text.Dynamic(EventName)
                };

                Modules modules = new Modules();
                
                if (!string.IsNullOrWhiteSpace(WebinarKey) && Modules.HasModule(SymphonyController.GetCustomerID(), modules.GoToWebinar))
                {
                    if (WebinarRegistrationID == "[NA]")
                    {
                        link.BaseUrl = Url.GTM;
                    }
                    else
                    {
                        link.BaseUrl = Url.Dynamic(WebinarLaunchURL);
                    }
                    link.IsNewWindow = true;

                } else if (EventTypeID == (int)Symphony.Core.EventType.Meeting) {
                    link = DisplayHelpers.GetGTMLaunchLink(EventID, EventName, JoinURL);
                }
                else if (EventTypeID == (int)Symphony.Core.EventType.TrainingProgramClass)
                {
                    Class c = (Class)Details;
                    if (c.RegistrationID > 0)
                    {
                        link = DisplayHelpers.GetDetailsLink(EventID, EventName);
                    }
                }
                else if (new Data.PresentationTypeCollection().Load().Where(p => p.Description == EventType).Count() > 0)
                {
                    if (UserID > 0)
                    {
                        link = DisplayHelpers.GetDetailsLink(EventID, EventName);
                    }
                }

                return link;
            }
            internal set { }
        }
        [DataMember(Name = "displayCourseLink")]
        public DisplayLink DisplayCourseLink
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                switch (EventTypeID)
                {
                    case (int)Symphony.Core.EventType.PublicClass:
                    case (int)Symphony.Core.EventType.RegisteredClass:
                        Course course = (Course)Details;
                        return course.DisplayCourseLink;
                    case (int)Symphony.Core.EventType.TrainingProgramClass:
                        
                        TrainingProgram tp = (TrainingProgram)ExtendedDetails;

                        TrainingProgramCourse tpCourse = tp.RequiredCourses
                            .Concat(tp.ElectiveCourses)
                            .Concat(tp.OptionalCourses)
                            .Concat(tp.FinalAssessments)
                            .Where(c => c.Id == GroupID)
                            .FirstOrDefault();

                        if (tpCourse != null && tpCourse.Id > 0)
                        {
                            tpCourse.TrainingProgram = tp;
                            return tpCourse.DisplayCourseLink;
                        }

                        return new DisplayLink()
                        {
                            BaseUrl = Url.Empty,
                            Title = Text.Empty,
                            Messages = new List<Text>(){Text.Dash}
                        };
                    case (int)Symphony.Core.EventType.Meeting:

                        break;
                    case (int)Symphony.Core.EventType.InstructedClass:
                        return new DisplayLink()
                        {
                            BaseUrl = Url.Empty,
                            Title = Text.Empty,
                            Messages = new List<Text>() { Text.Dash }
                        };
                }
                // Shouldn't get here
                return new DisplayLink()
                {
                    BaseUrl = Url.Empty,
                    Title = Text.Empty,
                    Messages = new List<Text>() { Text.Dash }
                };
            }
            internal set { }
        }
        [DataMember(Name = "displayEventType")]
        public DisplayModel DisplayEventType
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                string eventTypeName = EventType.First().ToString().ToUpper() + EventType.Substring(1);
                Text eventDetails = Text.Event_Details;
                eventDetails.Value = eventDetails.Value.Replace("{eventName}", eventTypeName);

                return new DisplayModel()
                {
                    Messages = new List<Text>()
                    {
                        Text.Dynamic(eventTypeName),
                        eventDetails
                    }
                };
            }
            internal set { }
        }
        #endregion

    }
}
