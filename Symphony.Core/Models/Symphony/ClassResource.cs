﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "classResource")]
    [DefaultSort(ColumnName = "Name")]
    public class ClassResource : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "resourceId")]
        public int ResourceID { get; set; }

        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "cost")]
        public decimal Cost { get; set; }

        [DataMember(Name = "venueId")]
        public int VenueID { get; set; }
        
        [DataMember(Name = "venueName")]
        public string VenueName { get; set; }

        [DataMember(Name = "roomId")]
        public int RoomID { get; set; }

        [DataMember(Name = "roomName")]
        public string RoomName { get; set; }

        [DataMember(Name = "location")]
        public string Location
        {
            get {
                List<string> location = new List<string>();
                if (!string.IsNullOrEmpty(VenueName))
                    location.Add(VenueName);
                if (!string.IsNullOrEmpty(RoomName))
                    location.Add(RoomName);
                return string.Join("/", location.ToArray());
            }
            set { }
        }
    }
}
