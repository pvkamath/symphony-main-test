﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "location")]
    [DefaultSort(ColumnName = "Name")]
    public class Location : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        // used to load, don't serialize it though
        public int ParentLocationId { get; set; }

        [DataMember(Name = "parentId")]
        public int ParentId
        {
            get { return ParentLocationId; }
            set { ParentLocationId = value; }
        }

        [DataMember(Name = "address1")]
        public string Address1 { get; set; }

        [DataMember(Name = "address2")]
        public string Address2 { get; set; }

        [DataMember(Name = "internalCode")]
        public string InternalCode { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "postalCode")]
        public string PostalCode { get; set; }

        [DataMember(Name = "telephoneNumber")]
        public string TelephoneNumber
        {
            get { return TelephoneNbr; }
            set { TelephoneNbr = value; }
        }

        // used to load, don't serialize it though
        public string TelephoneNbr { get; set; }

        [DataMember(Name = "salesforcePartnerCode")]
        public string SalesforcePartnerCode { get; set; }
    }
}
