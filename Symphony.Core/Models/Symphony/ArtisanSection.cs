﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;

namespace Symphony.Core.Models
{
    [DataContract(Name = "artisanSection")]
    public class ArtisanSection : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "sectionId")]
        public int SectionId { get; set; }

        [DataMember(Name = "courseId")]
        public int CourseId { get; set; }

        [DataMember(Name = "sections", EmitDefaultValue = false)]
        public List<ArtisanSection> Sections { get; set; }

        [DataMember(Name = "pages", EmitDefaultValue = false)]
        public List<ArtisanPage> Pages { get; set; }

        [DataMember(Name = "sectionType")]
        public int SectionType { get; set; }

        [DataMember(Name = "objectives")]
        public string Objectives { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "testType")]
        public ArtisanTestType TestType { get; set; }

        [DataMember(Name = "isRandomizeAnswers")]
        public bool IsRandomizeAnswers { get; set; }

        [DataMember(Name = "isRandomizeQuizQuestions")]
        public bool IsRandomizeQuizQuestions { get; set; }

        [DataMember(Name = "isReRandomizeOrder")]
        public bool IsReRandomizeOrder { get; set; }

        [DataMember(Name = "passingScore")]
        public int PassingScore { get; set; }

        [DataMember(Name = "testOut")]
        public bool TestOut { get; set; }

        [DataMember(Name = "clientId")]
        public int? ClientId { get; set; }

        [DataMember(Name = "isQuiz")]
        public bool IsQuiz { get; set; }

        [DataMember(Name = "maxQuestions")]
        public int MaxQuestions { get; set; }

        [DataMember(Name = "maxTime")]
        public int? MaxTime { get; set; }

        [DataMember(Name = "minLoTime")]
        public int MinLoTime { get; set; }

        [DataMember(Name = "minScoTime")]
        public int MinScoTime { get; set; }

        [DataMember(Name = "minimumPageTime")]
        public int? MinimumPageTime { get; set; }

        [DataMember(Name = "maximumQuestionTime")]
        public int? MaximumQuestionTime { get; set; }

        [DataMember(Name = "markingMethod")]
        public int MarkingMethod { get; set; }

        [DataMember(Name = "externalId")]
        public string ExternalID { get; set; }

        /// <summary>
        /// Groups this sections pages into a single page
        /// </summary>
        [DataMember(Name = "isSinglePage")]
        public bool IsSinglePage { get; set; }

        /// <summary>
        /// Lists the pages that were combined so details can be looked up later
        /// </summary>
        [DataMember(Name = "combinedPages")]
        public List<ArtisanPage> CombinedPages { get; set; }

        [DataMember(Name = "finalContentPage")]
        public string FinalContentPage { get; set; }

        /// <summary>
        /// Only used for dealing with TrainingPRO imports
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// Only used for dealing with TrainingPRO imports
        /// this allows calculating the sort value of the 
        /// quiz when added to the course after
        /// </summary>
        public int RelatedQuizes { get; set; }

        public void ClearIds()
        {
            this.Id = -1;
            if (this.Pages != null)
            {
                foreach (var page in this.Pages)
                {
                    page.Id = -1;
                    if (page.Answers != null)
                    {
                        foreach (var answer in page.Answers)
                        {
                            answer.Id = -1;
                        }
                    }
                }
            }
            if (this.Sections != null)
            {
                foreach (var section in this.Sections)
                {
                    section.ClearIds();
                }
            }
        }

        public void TransformQuestionPageReferences(ArtisanCourse originalCourse, ArtisanCourse courseToTransform)
        {
            if (this.Id == 0)
            {
                throw new Exception("This method cannot be run on a section with no id.");
            }
            if (this.Pages != null)
            {
                foreach (var page in this.Pages)
                {
                    if (page.ReferencePageId == 0)
                    {
                        continue;
                    }

                    // first, find the page in the original course with the current "reference id"
                    var originalPage = originalCourse.FindPageByID(page.ReferencePageId);
                    if (originalPage != null)
                    {

                        // second, find the page in the *new* course with the same content
                        var newPage = courseToTransform.FindPageByHtml(originalPage.Html);

                        // find the course in the original course with matching content
                        //var contentPage = originalCourse.FindPageByContent(page);
                        if (newPage != null)
                        {
                            page.ReferencePageId = newPage.Id;
                        }
                    }
                }
            }
            if (this.Sections != null)
            {
                foreach (var section in this.Sections)
                {
                    section.TransformQuestionPageReferences(originalCourse, courseToTransform);
                }
            }
        }

        /// <summary>
        /// Searches the current section and all sub-sections for the first page with matching content
        /// </summary>
        /// <param name="pageToFind"></param>
        /// <returns></returns>
        public ArtisanPage FindPageBy(Func<ArtisanPage, bool> comparator)
        {
            if (this.Pages != null)
            {
                foreach (var page in this.Pages)
                {
                    if (comparator(page))
                    {
                        return page;
                    }
                }
            }
            if (this.Sections != null)
            {
                foreach (var section in this.Sections)
                {
                    var page = section.FindPageBy(comparator);
                    if (page != null)
                    {
                        return page;
                    }
                }
            }
            return null;
        }
    }
}
