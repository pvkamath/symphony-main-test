﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "parameter")]
    [DefaultSort(ColumnName = "value")]
    public class ParameterSetOption : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "parameterSetId")]
        public int ParameterSetID { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }
    }
}
