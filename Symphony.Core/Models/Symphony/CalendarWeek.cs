﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "calendarWeek")]
    public class CalendarWeek
    {
        [DataMember(Name = "sunday")]
        public CalendarDay Sunday { get; set; }

        [DataMember(Name = "monday")]
        public CalendarDay Monday { get; set; }

        [DataMember(Name = "tuesday")]
        public CalendarDay Tuesday { get; set; }

        [DataMember(Name = "wednesday")]
        public CalendarDay Wednesday { get; set; }

        [DataMember(Name = "thursday")]
        public CalendarDay Thursday { get; set; }

        [DataMember(Name = "friday")]
        public CalendarDay Friday { get; set; }

        [DataMember(Name = "saturday")]
        public CalendarDay Saturday { get; set; }

        public CalendarDay this[int day]
        {
            get
            {
                switch (day)
                {
                    case 0: return Sunday;
                    case 1: return Monday;
                    case 2: return Tuesday;
                    case 3: return Wednesday;
                    case 4: return Thursday;
                    case 5: return Friday;
                    case 6: return Saturday;
                    default: throw new Exception("Invalid day. Valid range is 0-6.");
                }
            }
            set
            {
                switch (day)
                {
                    case 0: Sunday = value; break;
                    case 1: Monday = value; break;
                    case 2: Tuesday = value; break;
                    case 3: Wednesday = value; break;
                    case 4: Thursday = value; break;
                    case 5: Friday = value; break;
                    case 6: Saturday = value; break;
                    default: throw new Exception("Invalid day. Valid range is 0-6.");
                }
            }
        }
    }
}
