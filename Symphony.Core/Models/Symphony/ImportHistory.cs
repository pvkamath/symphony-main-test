﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract]
    public class ImportHistory
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "statusCode")]
        public int StatusCode { get; set; }

        [DataMember(Name="startDate")]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public DateTime EndDate { get; set; }

        [DataMember(Name = "lastUpdated")]
        public DateTime LastUpdated { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "errored")]
        public bool Errored { get; set; }

        [DataMember(Name = "sessionUserIDs")]
        public string SessionUserIDs { get; set; }

        [DataMember(Name = "sessionUserJson")]
        public string SessionUserJson { get; set; }
    }
}
