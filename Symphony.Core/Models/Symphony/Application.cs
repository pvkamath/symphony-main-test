﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "application")]
    [DefaultSort(ColumnName = "Name")]
    public class Application : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "authenticationUri")]
        public string AuthenticationUri { get; set; }

        [DataMember(Name = "passUserNamePass")]
        public bool PassUserNamePass { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

    }
}
