﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Symphony.Core.Models
{
    [DataContract]
    public class MetaData
    {
        [DataMember(Name = "display")]
        public string Display { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "keyCode")]
        public string KeyCode { get; set; }
    }
}
