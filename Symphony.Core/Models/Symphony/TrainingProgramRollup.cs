﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    /// <summary>
    /// Represents the TrainingProgramRollup table
    /// </summary>
    [DataContract(Name="TrainingProgramRollup")]
    public class TrainingProgramRollup : TrainingProgramSummary
    {
        [DataMember(Name="id")]
        public int ID { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "isFinalAvailable")]
        public bool IsFinalAvailable { get; set; }

        [DataMember(Name = "finalUnlockedOn")]
        public DateTime? FinalUnlockedOn { get; set; }
    }
}
