﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "artisanMatchingAnswer")]
    public class ArtisanMatchingAnswer
    {
        [DataMember(Name = "leftText")]
        public string LeftText { get; set; }

        [DataMember(Name = "rightText")]
        public string RightText { get; set; }
    }
}
