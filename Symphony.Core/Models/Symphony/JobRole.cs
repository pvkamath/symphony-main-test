﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "location")]
    [DefaultSort(ColumnName = "Name")]
    public class JobRole : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "internalCode")]
        public string InternalCode { get; set; }

        // used to load, don't serialize it though
        public int ParentJobRoleID { get; set; }

        [DataMember(Name = "parentId")]
        public int ParentId
        {
            get { return ParentJobRoleID; }
            set { ParentJobRoleID = value; }
        }
    }
}
