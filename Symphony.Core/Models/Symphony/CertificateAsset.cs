﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;
using System.IO;

namespace Symphony.Core.Models
{
    [DataContract(Name = "certificateAsset")]
    public class CertificateAsset: Model
    {
        [DataMember(Name = "filename")]
        public string Filename { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "path")]
        public string thePath
        {
            get
            {
                var root = "/Uploads/CertificateEditor/Assets/";  // this returns an abs path: (new CertificateController()).GetAssetsPath();
                return root + Id.ToString() + Path.GetExtension(Filename);
            }
            private set { }
        }

        [DataMember(Name = "relativeUrl")]
        public string RelativeUrl
        {
            get
            {   
                //todo: move to a config 
                var root = "/Uploads/CertificateEditor/Assets/";  // this returns an abs path: (new CertificateController()).GetAssetsPath();
                return root + Id.ToString() + Path.GetExtension(Filename);
            }
         
            private set { }
        }
    }
}
