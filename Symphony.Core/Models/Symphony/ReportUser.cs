using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportUser")]
    [DefaultSort(ColumnName = "LastName")]
    public class ReportUser : Model
    {
        [DataMember(Name = "userkey")]
        public int UserKey { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }
		
		[DataMember(Name = "lastName")]
        public string LastName { get; set; }
		
		[DataMember(Name = "middleName")]
        public string MiddleName { get; set; }

        [DataMember(Name = "customerkey")]
        public int CustomerKey { get; set; }

        [DataMember(Name = "userId")]
        public int UserId { get; set; }

        [DataMember(Name = "isImported")]
        public Boolean IsImported { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerId { get; set; }

        [DataMember(Name = "fullName")]
        public string FullName
        {
            get
            {
                return Model.FormatFullName(FirstName, MiddleName, LastName);
            }
            internal set { }
        }
    }
}
