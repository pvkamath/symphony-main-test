using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportJobTriggers")]
    [DefaultSort(ColumnName = "Name")]
    public class ReportJobTriggers : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "misfireInstruction")]
        public string MisfireInstruction { get; set; }
		
		[DataMember(Name = "name")]
        public string Name { get; set; }
		
		[DataMember(Name = "group")]
        public string Group { get; set; }
		
		[DataMember(Name = "description")]
        public string Description { get; set; }
		
		[DataMember(Name = "jobId")]
        public int JobID { get; set; }
		
		[DataMember(Name = "startAt")]
        public DateTime? StartAt { get; set; }
		
		[DataMember(Name = "endAt")]
        public DateTime? EndAt { get; set; }
		
		[DataMember(Name = "cronSchedule")]
        public string CronSchedule { get; set; }

    }
}
