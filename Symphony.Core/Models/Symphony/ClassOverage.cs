﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "classOverage")]
    public class ClassOverage : Model
    {
        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        [DataMember(Name = "className")]
        public string ClassName { get; set; }

        [DataMember(Name = "overage")]
        public int Overage { get; set; }
    }
}
