﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "artisanAnswer")]
    public class ArtisanAnswer : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "pageId")]
        public int PageId { get; set; }

        string _text = string.Empty;

        [DataMember(Name = "text")]
        public string Text
        {
            get { return this._text; }
            set
            {
                this._text = value;
                if (this.Text.StartsWith("{") && this.Text.EndsWith("}"))
                {
                    // Image Question
                    if (this.Text.Contains("\"imageUrl\":"))
                    {
                        try
                        {
                            if (this.Text.Contains("\"dimensions\":{"))
                            {
                                // old format; extract into the new form
                                var temp = Utilities.Deserialize<OldArtisanImageQuestionAnswer>(value);
                                this.ImageQuestionAnswer = new ArtisanImageQuestionAnswer()
                                {
                                    Dimensions = new List<Dimensions>(){ temp.Dimensions },
                                    ImageUrl = temp.ImageUrl
                                };
                            }
                            else
                            {
                                // new format has an array of dimensions
                                this.ImageQuestionAnswer = Utilities.Deserialize<ArtisanImageQuestionAnswer>(value);
                            }
                        }
                        catch (Exception)
                        {
                            // ignore it, set the answer to a default
                            this.ImageQuestionAnswer = new ArtisanImageQuestionAnswer()
                            {
                                ImageUrl = string.Empty,
                                Dimensions = new List<Dimensions>(){ new Dimensions() { Height = 35, Width = 35, X = 0, Y = 0 } }
                            };
                        }
                    }
                    // Matching Question
                    else if (this.Text.Contains("\"leftText\":"))
                    {
                        try
                        {
                            this.MatchingAnswer = Utilities.Deserialize<ArtisanMatchingAnswer>(value);
                        }
                        catch (Exception)
                        {
                            // ignore it, set the answer to a default
                            this.MatchingAnswer = new ArtisanMatchingAnswer()
                            {
                                LeftText = string.Empty,
                                RightText = string.Empty
                            };
                        }
                    }
                    // Category Matching Question
                    else if (this.Text.Contains("\"category\":"))
                    {
                        try
                        {
                            this.CategoryMatchingAnswer = Utilities.Deserialize<ArtisanCategoryMatchingAnswer>(value);
                        }
                        catch (Exception)
                        {
                            // ignore it, set the answer to a default
                            this.CategoryMatchingAnswer = new ArtisanCategoryMatchingAnswer()
                            {
                                Category = string.Empty,
                                Matches = new List<string>()
                            };
                        }
                    }
                }
            }
        }

        [DataMember(Name = "sort")]
        public int Sort { get; set; }

        [DataMember(Name = "isCorrect")]
        public bool IsCorrect { get; set; }

        /// <summary>
        /// Custom feedback for a particular answer that will override the question feedback
        /// </summary>
        [DataMember(Name = "feedback")]
        public string Feedback { get; set; }

        public ArtisanImageQuestionAnswer ImageQuestionAnswer { get; set; }
        public ArtisanMatchingAnswer MatchingAnswer { get; set; }
        public ArtisanCategoryMatchingAnswer CategoryMatchingAnswer { get; set; }
    }
}
