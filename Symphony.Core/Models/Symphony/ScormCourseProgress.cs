﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="scormCourseProgress")]
    public class ScormCourseProgress : Model
    {
        [DataMember(Name = "bookmark")]
        public string Bookmark { get; set; }

        [DataMember(Name = "dataChunk")]
        public string DataChunk { get; set; }
    }
}
