using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportJobs")]
    [DefaultSort(ColumnName = "Name")]
    public class ReportJobs : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "isScheduled")]
        public int IsScheduled { get; set; }

        [DataMember(Name = "userNotes")]
        public string UserNotes { get; set; }
		
		[DataMember(Name = "name")]
        public string Name { get; set; }
		
		[DataMember(Name = "group")]
        public string  Group { get; set; }
		
		[DataMember(Name = "description")]
        public string  Description { get; set; }
		
		[DataMember(Name = "jobType")]
        public string JobType { get; set; }
		

    }
}
