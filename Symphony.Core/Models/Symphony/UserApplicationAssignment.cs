﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "application")]
    [DefaultSort(ColumnName = "Name")]
    public class UserApplicationAssignment : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "credentialState")]
        public string CredentialState { get; set; }

        [DataMember(Name = "credentialId")]
        public int CredentialID { get; set; }

        [DataMember(Name = "credentialUserName")]
        public string CredentialUserName { get; set; }

        [DataMember(Name = "credentialPassword")]
        public string CredentialPassword { get; set; }

        [DataMember(Name = "tileCount")]
        public int TileCount { get; set; }


    }
}
