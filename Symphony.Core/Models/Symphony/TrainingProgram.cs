﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using SubSonic;
using Symphony.Core.Comparers;
using Symphony.Core.Extensions;
using Symphony.Core.Controllers;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Models
{
    [DataContract(Name = "trainingProgram")]
    [DefaultSort(ColumnName = "Name")]
    public class TrainingProgram : Model, IAssociatedImage, ITrainingProgram
    {
        private double percentComplete = -1;

        private List<TrainingProgramCourse> _requiredCourses = new List<TrainingProgramCourse>();
        private List<TrainingProgramCourse> _electiveCourses = new List<TrainingProgramCourse>();
        private List<TrainingProgramCourse> _optionalCourses = new List<TrainingProgramCourse>();
        private List<TrainingProgramCourse> _finalAssessments = new List<TrainingProgramCourse>();
        private List<TrainingProgramCourse> _accreditationSurveys = new List<TrainingProgramCourse>();

        private AssignmentCount _assignmentCount;

        public TrainingProgram() { }

        [DataMember(Name = "associatedImageData")]
        public string AssociatedImageData { get; set; }

        [DataMember(Name = "newHireStartDateOffset")]
        public int? NewHireStartDateOffset { get; set; }

        [DataMember(Name = "newHireDueDateOffset")]
        public int? NewHireDueDateOffset { get; set; }

        [DataMember(Name = "newHireEndDateOffset")]
        public int? NewHireEndDateOffset { get; set; }

        [DataMember(Name = "newHireOffsetEnabled")]
        public bool NewHireOffsetEnabled { get; set; }

        [DataMember(Name = "newHireTransitionPeriod")]
        public int? NewHireTransitionPeriod { get; set; }

        /* info displayed in the portal grid */
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "categoryName")]
        public string CategoryName { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "goalsAndObjectives")]
        public string GoalsAndObjectives { get; set; }

        [DataMember(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public DateTime? EndDate { get; set; }

        [DataMember(Name = "originalReleaseDate")]
        public DateTime? OriginalReleaseDate { get; set; }

        [DataMember(Name = "dueDate")]
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// Is the current date between the start and end date?
        /// Factors in new hires. 
        /// </summary>
        [DataMember(Name = "isActive")]
        public bool IsActive { get; set; }

        [DataMember(Name = "courseCount")]
        public int CourseCount { get; set; }

        [DataMember(Name = "duplicateFromId")]
        public int DuplicateFromID { get; set; }

        /* info displayed after drilling into the TP in the portal */
        [DataMember(Name = "files")]
        public List<TrainingProgramFile> Files { get; set; }

        [DataMember(Name = "books")]
        public List<Book> Books { get; set; }

        [DataMember(Name = "requiredCourses")]
        public List<TrainingProgramCourse> RequiredCourses
        {
            get
            {
                _requiredCourses.ForEach(c => c.TrainingProgram = this);
                return _requiredCourses;
            }
            set
            {
                _requiredCourses = value;
            }
        }

        [DataMember(Name = "electiveCourses")]
        public List<TrainingProgramCourse> ElectiveCourses
        {
            get
            {
                _electiveCourses.ForEach(c => c.TrainingProgram = this);
                return _electiveCourses;
            }
            set
            {
                _electiveCourses = value;
            }
        }

        [DataMember(Name = "optionalCourses")]
        public List<TrainingProgramCourse> OptionalCourses
        {
            get
            {
                _optionalCourses.ForEach(c => c.TrainingProgram = this);
                return _optionalCourses;
            }
            set
            {
                _optionalCourses = value;
            }
        }

        [DataMember(Name = "finalAssessments")]
        public List<TrainingProgramCourse> FinalAssessments
        {
            get
            {
                _finalAssessments.ForEach(c => c.TrainingProgram = this);
                return _finalAssessments;
            }
            set
            {
                _finalAssessments = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether or not the TP has loaded data on completed
        /// accreditation surveys. If false, training program rollup data should be used instead.
        /// </summary>
        public bool HasAccreditationSurveyData { get; set; }

        [DataMember(Name = "accreditationSurveys")]
        public List<TrainingProgramCourse> AccreditationSurveys
        {
            get
            {
                _accreditationSurveys.ForEach(c => c.TrainingProgram = this);
                return _accreditationSurveys;
            }
            set
            {
                _accreditationSurveys = value;
            }
        }

        [DataMember(Name = "minimumElectives")]
        public int MinimumElectives { get; set; }

        [DataMember(Name = "enforceRequiredOrder")]
        public bool EnforceRequiredOrder { get; set; }

        /* additional details only used when editing the TP itself */
        [DataMember(Name = "isLive")]
        public bool IsLive { get; set; }

        [DataMember(Name = "isNewHire")]
        public bool IsNewHire { get; set; }

        [DataMember(Name = "cost")]
        public decimal Cost { get; set; }

        [DataMember(Name = "internalCode")]
        public string InternalCode { get; set; }

        [DataMember(Name = "completed")]
        public bool Completed { get; set; }

        [DataMember(Name = "dateCompleted")]
        public DateTime DateCompleted { get; set; }

        [DataMember(Name = "percentComplete")]
        public int PercentComplete
        {
            get
            {
                if (percentComplete < 0)
                {
                    int electiveCourseWorkComplete = TotalCourseWorkComplete(ElectiveCourses);
                    int electiveCourseWorkRequired = TotalCourseWorkRequired(ElectiveCourses);

                    if (electiveCourseWorkComplete > electiveCourseWorkRequired)
                    {
                        electiveCourseWorkComplete = electiveCourseWorkRequired;
                    }

                    int totalRequiredCourseWork = GetTotalCourseWorkRequired(); // Use training program setting for either number of courses or number of hours

                    // Use training program setting to calculate total completed courses or total completed hours.
                    int totalCompletedCourseWork = TotalCourseWorkComplete(RequiredCourses) +
                                                   electiveCourseWorkComplete +
                                                   TotalCourseWorkComplete(FinalAssessments);

                    percentComplete = totalRequiredCourseWork > 0 ?
                        (double)totalCompletedCourseWork / (double)totalRequiredCourseWork :
                        0;
                }

                return (int)Math.Round(percentComplete * 100);
            }
            internal set
            {

            }
        }

        [DataMember(Name = "allowPrinting")]
        public bool AllowPrinting { get; set; }

        [DataMember(Name = "ownerId")]
        public int OwnerID { get; set; }
        
        [DataMember(Name = "ownerName")]
        public string OwnerName { get; set; }

        [DataMember(Name = "primaryAssignment")]
        public List<HierarchyLink> PrimaryAssignment { get; set; }

        [DataMember(Name = "secondaryAssignment")]
        public List<HierarchyLink> SecondaryAssignment { get; set; }

        [DataMember(Name = "categoryId")]
        public int CategoryID { get; set; }

        [DataMember(Name = "disableScheduled")]
        public bool DisableScheduled { get; set; }

        [DataMember(Name = "messageBoardId")]
        public int MessageBoardID { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "subDomain")]
        public string SubDomain { get; set; }

        [DataMember(Name = "partnerId")]
        public int? PartnerID { get; set; }

        [DataMember(Name = "partnerCode")]
        public string PartnerCode { get; set; }

        [DataMember(Name = "partnerDisplayName")]
        public int PartnerDisplayName { get; set; }

        [DataMember(Name = "isSalesforce")]
        public bool IsSalesforce { get; set; }

        [DataMember(Name = "purchasedFromBundleId")]
        public int? PurchasedFromBundleId { get; set; }

        [DataMember(Name = "purchasedFromBundleName")]
        public string PurchasedFromBundleName { get; set; }

        [DataMember(Name = "leaders")]
        public List<TrainingProgramLeader> Leaders { get; set; }

        [DataMember(Name = "trainingProgramParameterOverrides")]
        public List<TrainingProgramParameterOverride> TrainingProgramParameterOverrides { get; set; }

        [DataMember(Name = "isCreateMessageBoard")]
        public bool IsCreateMessageBoard { get; set; }

        [DataMember(Name = "isDisableTopicCreation")]
        public bool IsDisableTopicCreation { get; set; }

        [DataMember(Name = "messageBoardName")]
        public string MessageBoardName { get; set; }

        [DataMember(Name = "enforceCanMoveForwardIndicator")]
        public bool EnforceCanMoveForwardIndicator { get; set; }

        [DataMember(Name = "surveyId")]
        public int SurveyID { get; set; }

        [DataMember(Name = "isSurveyMandatory")]
        public bool IsSurveyMandatory { get; set; }

        [DataMember(Name = "surveyName")]
        public string SurveyName { get; set; }

        [DataMember(Name = "originalSurveyName")]
        public string OriginalSurveyName { get; set; }

        [DataMember(Name = "originalSurveyId")]
        public int OriginalSurveyID { get; set; }

        [DataMember(Name = "surveyTaken")]
        public bool SurveyTaken { get; set; }

        [DataMember(Name = "surveysRequired")]
        public bool SurveysRequired { get; set; }

        [DataMember(Name = "assignmentsRequired")]
        public bool AssignmentsRequired { get; set; }

        [DataMember(Name = "levelIndentText")]
        public string LevelIndentText { get; set; }

        [DataMember(Name = "isValidationEnabled")]

        public bool? IsValidationEnabled { get; set; }

        [DataMember(Name = "preTestValidation")]
        public bool? PreTestValidation { get; set; }

        [DataMember(Name = "postTestValidation")]
        public bool? PostTestValidation { get; set; }

        [DataMember(Name = "validationInterval")]
        public int? ValidationInterval { get; set; }

        [DataMember(Name = "maxCourseWork")]
        public int? MaxCourseWork { get; set; }

        [DataMember(Name = "externalId")]
        public string ExternalID { get; set; }

        [DataMember(Name = "userStartTime")]
        public DateTime? UserStartTime { get; set; }

        [DataMember(Name = "affidavitId")]
        public int AffidavitID { get; set; }

        [DataMember(Name = "affidavitName")]
        public string AffidavitName { get; set; }

        [DataMember(Name = "trainingProgramAffidavitId")]
        public int TrainingProgramAffidavitID { get; set; }

        [DataMember(Name = "trainingProgramAffidavitName")]
        public string TrainingProgramAffidavitName { get; set; }

        [DataMember(Name = "proctorFormId")]
        public int ProctorFormID { get; set; }

        [DataMember(Name = "proctorFormName")]
        public string ProctorFormName { get; set; }

        [DataMember(Name = "isFinalAffidavitCompleted")]
        public bool IsFinalAffidavitCompleted { get; set; }

        [DataMember(Name = "isTrainingProgramAffidavitCompleted")]
        public bool IsTrainingProgramAffidavitCompleted { get; set; }

        [DataMember(Name = "certificatePath")]
        public string CertificatePath { get; set; }

        [DataMember(Name = "metaDataJson")]
        public string MetaDataJson { get; set; }

        [DataMember(Name = "sku")]
        public string Sku { get; set; }

        [DataMember(Name = "isCourseWorkAllowed")]
        public bool IsCourseWorkAllowed { get; set; }

        [DataMember(Name = "hasUserStarted")]
        public bool HasUserStarted { get; set; }

        [DataMember(Name = "isNmls")]
        public bool IsNmls { get; set; }

        [DataMember(Name = "finalExamScore")]
        public string FinalExamScore
        {
            get
            {
                if (FinalAssessments.Count == 0)
                {
                    return null;
                }

                return FinalAssessments.FirstOrDefault().Score;
            }
            internal set { }
        }

        /// <summary>
        /// True if this is representation of a training program from a different system. 
        /// This will automatically render the certificate regardless if we have data for
        /// the user in individual courses.
        /// </summary>
        [DataMember(Name = "isHistorical")]
        public bool IsHistorical { get; set; }

        /// <summary>
        /// Refers to the completed flag in training program rollup.
        /// If this is set, and it is true, then we can assume that the entire 
        /// training program has been completed since it was previously calculated
        /// as being complete.
        /// </summary>
        [DataMember(Name = "isTrainingProgramRollupComplete")]
        public bool? IsTrainingProgramRollupComplete { get; set; }

        [DataMember(Name = "licenses")]
        public List<TrainingProgramLicense> Licenses { get; set; }
        /// <summary>
        /// Use this to override the license being used as the primary license
        /// </summary>
        public int PrimaryLicenseIdOverride { get; set; }

        /// <summary>
        /// Will return the first license marked as primary
        /// Or the first license in the case where no licenses are marked as primary (Should never happen)
        /// or the license indicated by primaryLicenseIdOverride
        /// or null if no licenses found
        /// </summary>
        public License PrimaryLicense
        {
            get
            {
                if (PrimaryLicenseIdOverride > 0 && this.Licenses != null)
                {
                    List<TrainingProgramLicense> licenses = this.Licenses.Where(l => l.Id == PrimaryLicenseIdOverride).ToList();

                    if (licenses.Count > 0)
                    {
                        return licenses[0];
                    }
                }
                return this.Licenses != null && this.Licenses.Count > 0 ? Licenses[0] : null;
            }
        }

        public string GetPrimaryLicenseApprovalCode()
        {
            if (this.HasLicenses)
            {
                var tpl = Select.AllColumnsFrom<Data.TrainingProgramLicense>()
                    .Where(Data.TrainingProgramLicense.LicenseIDColumn).IsEqualTo(this.PrimaryLicense.Id)
                    .And(Data.TrainingProgramLicense.TrainingProgramIDColumn).IsEqualTo(this.Id)
                    .ExecuteSingle<Data.TrainingProgramLicense>();

                if (tpl != null)
                {
                    return tpl.ApprovalCode;
                }
            }
            return string.Empty;
        }

        public bool HasLicenses
        {
            get
            {
                return this.PrimaryLicense != null;
            }
        }

        /// <summary>
        /// For Salesforce. When purchases are made
        /// we duplicate the training program specifically
        /// for the user. This will link back to what we 
        /// duplicated from.
        /// </summary>
        [DataMember(Name = "parentTrainingProgramId")]
        public int ParentTrainingProgramID { get; set; }

        /// <summary>
        /// Minimum time the user must spend in this training program in order to receive credit.
        /// Stored in minutes.
        /// </summary>
        [DataMember(Name = "minCourseWork")]
        public int? MinCourseWork { get; set; }
        /// <summary>
        /// Total seconds the user has spent in this training program. This will only be present when the GetTrainingProgramsForUser SP is called
        /// Only using a long for consistency with the SCORM db. SCORM reports time tracked using a long in the scorm integration course rollup
        /// so online course rollup uses a long, as well as training program rollup. This is fully overkill as an int should be plenty to 
        /// record seconds since that allows for 68 years. SCORM records using centiseconds, but reports back with seconds so this is probabaly 
        /// why they used a long. Even with an int they could record 248 days of course work for a single course/user. Since it's a long they 
        /// can store 2924712086 years worth of centiseconds.
        /// </summary>
        [DataMember(Name = "totalSeconds")]
        public long TotalSeconds { get; set; }
        
        [DataMember(Name = "isMinCourseWorkComplete")]
        public bool IsMinCourseWorkComplete { get {

            if (MinCourseWork.HasValue && MinCourseWork.Value > 0)
            {
                long totalMinutes = TotalSeconds / 60;
                if (totalMinutes >= MinCourseWork.Value)
                {
                    return true;
                }
                return false;
            }
            return true;

        } internal set {}}

        #region Overrides
        // overrides in the trainingprogram management area
        [DataMember(Name = "showPretestOverride")]
        public bool? ShowPretestOverride { get; set; }

        [DataMember(Name = "showPosttestOverride")]
        public bool? ShowPostTestOverride { get; set; }

        [DataMember(Name = "passingScoreOverride")]
        public int? PassingScoreOverride { get; set; }

        [DataMember(Name = "skinOverride")]
        public int? SkinOverride { get; set; }

        [DataMember(Name = "pretestTypeOverride")]
        public int? PretestTypeOverride { get; set; }

        [DataMember(Name = "posttestTypeOverride")]
        public int? PosttestTypeOverride { get; set; }

        [DataMember(Name = "navigationMethodOverride")]
        public int? NavigationMethodOverride { get; set; }

        [DataMember(Name = "bookmarkingMethodOverride")]
        public int? BookmarkingMethodOverride { get; set; }

        [DataMember(Name = "certificateEnabledOverride")]
        public bool? CertificateEnabledOverride { get; set; }

        [DataMember(Name = "disclaimerOverride")]
        public string DisclaimerOverride { get; set; }

        [DataMember(Name = "contactEmailOverride")]
        public string ContactEmailOverride { get; set; }

        [DataMember(Name = "showObjectivesOverride")]
        public bool? ShowObjectivesOverride { get; set; }

        [DataMember(Name = "randomizeAnswersOverride")]
        public bool? RandomizeAnswersOverride { get; set; }

        [DataMember(Name = "randomizeQuestionsOverride")]
        public bool? RandomizeQuestionsOverride { get; set; }

        [DataMember(Name = "reviewMethodOverride")]
        public int? ReviewMethodOverride { get; set; }

        [DataMember(Name = "markingMethodOverride")]
        public int? MarkingMethodOverride { get; set; }

        [DataMember(Name = "inactivityMethodOverride")]
        public int? InactivityMethodOverride { get; set; }

        [DataMember(Name = "inactivityTimeoutOverride")]
        public int? InactivityTimeoutOverride { get; set; }

        [DataMember(Name = "isForceLogoutOverride")]
        public bool? IsForceLogoutOverride { get; set; }

        [DataMember(Name = "minimumPageTimeOverride")]
        public int? MinimumPageTimeOverride { get; set; }

        [DataMember(Name = "maximumQuestionTimeOverride")]
        public int? MaximumQuestionTimeOverride { get; set; }

        [DataMember(Name = "maxTimeOverride")]
        public int? maxTimeOverride { get; set; }

        [DataMember(Name = "minTimeOverride")]
        public int? MinTimeOverride { get; set; }

        [DataMember(Name = "minLoTimeOverride")]
        public int? MinLoTimeOverride { get; set; }

        [DataMember(Name = "minScoTimeOverride")]
        public int? MinScoTimeOverride { get; set; }

        [DataMember(Name = "theme")]
        public ArtisanTheme Theme { get; set; }

        [DataMember(Name = "pretestTestOutOverride")]
        public bool? PretestTestOutOverride { get; set; }

        [DataMember(Name = "retries")]
        public int? Retries { get; set; }

        [DataMember(Name = "themeFlavorOverrideId")]
        public int? ThemeFlavorOverrideID { get; set; }

        [DataMember(Name = "themeFlavor")]
        public Theme ThemeFlavor { get; set; }
        // end overrides
        #endregion

        #region Sessions
        /// <summary>
        /// Allows adding a course as a session for this training program
        /// This is salesforce specific.
        /// This will allow all users to register for a single course for
        /// a training progarm. Since training programs are duplicated
        /// when purchased, this will allow one point of registration 
        /// for the training program.
        /// </summary>
        [DataMember(Name = "sessionCourseId")]
        public int? SessionCourseID { get; set; }

        /// <summary>
        /// For data binding of the session course id
        /// </summary>
        [DataMember(Name = "sessionCourseName")]
        public string SessionCourseName { get; set; }

        /// <summary>
        /// The session for this training program
        /// This will either be the active session, nearest upcoming session, or nearest past session
        /// 
        /// </summary>
        [DataMember(Name = "session")]
        public ClassroomClass Session { get; set; }

        /// <summary>
        /// True if todays date is between the start and end date of the session
        /// that the user is registered for
        /// </summary>
        [DataMember(Name = "isSessionActive")]
        public bool IsSessionActive { get; set; }

        /// <summary>
        /// True if !managing && !isNewHire && sessionCourseId > 0
        /// </summary>
        [DataMember(Name = "isUsingSessions")]
        public bool IsUsingSessions { get; set; }

        /// <summary>
        /// True if sessionCourseId > 0 and user is registered in one of the sessions
        /// Doesn't matter if the session is active or not. 
        /// </summary>
        [DataMember(Name = "isSessionRegistered")]
        public bool IsSessionRegistered { get; set; }
        #endregion

        // Disables all course timers for all online courses within this training program
        [DataMember(Name = "isCourseTimingDisabled")]
        public bool IsCourseTimingDisabled { get; set; }

        /// <summary>
        /// Only used to determine the total number of training programs found by 
        /// the get training programs for user stored procedure
        /// </summary>
        public int TotalSize { get; set; }

        /// <summary>
        /// How/if courses are unlocked during a training program.
        /// Either they get unlocked after completion preventing additional attempts to increase score and just allow review
        /// or they do not unlock and continue to behave as regular courses. 
        /// </summary>
        [DataMember(Name = "courseUnlockModeId")]
        public int CourseUnlockModeID { get; set; }

        /// <summary>
        /// The amount of time it will take a student to complete all courses
        /// in this training program.
        /// </summary>
        [DataMember(Name = "duration")]
        public int Duration
        {
            get
            {
                return GetTotalCourseWorkRequired(true); // Force time calculation
            }
            set
            {

            }
        }

        /// <summary>
        /// Toggles completion method calculation that takes course
        /// duration into account
        /// </summary>
        [DataMember(Name = "isAdvancedCompletionPercentage")]
        public bool IsAdvancedCompletionPercentage { get; set; }

        [DataMember(Name = "sharedFlag")]
        public bool SharedFlag { get; set; }

        [DataMember(Name = "isCustomNotificationsEnabled")]
        public bool IsCustomNotificationsEnabled { get; set; }

        [DataMember(Name = "notificationTemplates")]
        public List<NotificationTemplate> NotificationTemplates { get; set; }

        [DataMember(Name = "accreditations")]
        public List<TrainingProgramAccreditation> Accreditations { get; set; }

        [DataMember(Name = "professions")]
        public List<Profession> Professions { get; set; }

        [DataMember(Name = "hasAccreditations")]
        public bool HasAccreditations
        {
            get
            {
                return Accreditations != null && Accreditations.Count > 0;
            }
            internal set {}
        }

        [DataMember(Name = "hasProfession")]
        public bool HasProfession
        {
            get
            {
                return TrainingProgramRollup != null && TrainingProgramRollup.ProfessionID > 0;
            }
            internal set {}
        }

        #region HelperFunctions
        /// <summary>
        /// Returns either hours completed, or number of courses completed. Depending on the training program setting.
        /// </summary>
        /// <param name="courses"></param>
        /// <returns></returns>
        public int TotalCourseWorkComplete(List<TrainingProgramCourse> courses, bool? isAdvancedModeOverride = null)
        {
            if (courses == null || courses.Count == 0)
            {
                return 0;
            }

            int count = 0;
            int duration = 0;

            foreach (TrainingProgramCourse course in courses)
            {
                if (course.Passed.HasValue && course.Passed.Value > 0)
                {
                    course.Completed = true;
                }

                if (course.SurveyID > 0 && !course.SurveyTaken && course.IsSurveyMandatory)
                {
                    SurveysRequired = true;
                    course.Completed = false;
                }
                if (course.IsAssignmentRequired)
                {
                    AssignmentsRequired = true;
                    course.Completed = false;
                }

                if (course.Completed)
                {
                    count++;
                    duration += course.Duration;
                }
                else
                {
                    duration += (int)(course.Duration * (course.NavigationPercentage * 0.01));
                }
            }

            bool isAdvanced = isAdvancedModeOverride.HasValue ? isAdvancedModeOverride.Value : IsAdvancedCompletionPercentage;

            return isAdvanced ? duration : count;
        }

        private int GetTotalCourseWorkRequired(bool? modeOverride = null)
        {
            return TotalCourseWorkRequired(RequiredCourses, modeOverride) +
                        TotalCourseWorkRequired(ElectiveCourses, modeOverride) +
                        TotalCourseWorkRequired(FinalAssessments, modeOverride);
        }

        /// <summary>
        /// Returns the total number of hours required, or the total number of courses required depening on tp settings
        /// </summary>
        /// <param name="courses"></param>
        /// <returns></returns>
        private int TotalCourseWorkRequired(List<TrainingProgramCourse> courses, bool? modeOverride = null)
        {
            bool isAdvanced = modeOverride.HasValue ? modeOverride.Value : IsAdvancedCompletionPercentage;

            if (courses == null || courses.Count == 0)
            {
                return 0;
            }

            TrainingProgramCourse first = courses.FirstOrDefault();
            bool isElective = false;

            if (first != null && first.SyllabusTypeID == (int)SyllabusType.Elective)
            {
                isElective = true;
            }

            if (isAdvanced)
            {
                int duration = courses.Sum(c => (c.IsUseAutomaticDuration || c.Duration == 0) ? c.AutomaticDuration : c.Duration);

                return isElective ? (duration / courses.Count) * MinimumElectives : duration;
            }
            else
            {
                return isElective ? MinimumElectives : courses.Count;
            }
        }

        /// <summary>
        /// For salesforce - allows merging courses from another training program
        /// </summary>
        /// <param name="tp">Training program to merge in from</param>
        /// <returns>True if there is any change in the lists of courses, false if the courses have not changed.</returns>
        public bool IncludeCoursesFromTrainingProgram(TrainingProgram tp)
        {
            TrainingProgramCourseComparer comparer = new TrainingProgramCourseComparer();

            // union each group of courses, which will filter out any duplicates
            List<Models.TrainingProgramCourse> requiredCourses =
                  RequiredCourses
                    .Union(tp.RequiredCourses, comparer)
                    .ToList();

            List<Models.TrainingProgramCourse> optionalCourses =
                  OptionalCourses
                    .Union(tp.OptionalCourses, comparer)
                    .ToList();

            List<Models.TrainingProgramCourse> electiveCourses =
                  ElectiveCourses
                    .Union(tp.ElectiveCourses, comparer)
                    .ToList();

            List<Models.TrainingProgramCourse> finalAssessments =
                  FinalAssessments
                    .Union(tp.FinalAssessments, comparer)
                    .ToList();

            if (RequiredCourses.Count == requiredCourses.Count &&
                OptionalCourses.Count == optionalCourses.Count &&
                ElectiveCourses.Count == electiveCourses.Count &&
                FinalAssessments.Count == finalAssessments.Count)
            {
                // there was no change
                return false;
            }

            // Set the new course assignements
            RequiredCourses = requiredCourses;
            OptionalCourses = optionalCourses;
            ElectiveCourses = electiveCourses;
            FinalAssessments = finalAssessments;

            // courses have changed
            return true;
        }
        /// <summary>
        /// For salesforce - removes the courses from this training program that exist in the training program passed in
        /// </summary>
        /// <param name="tp">The training program containing the courses to remove</param>
        /// <returns>True if the list of courses has changed, flase if it has not changed</returns>
        public bool ExcludeCoursesFromTrainingProgram(TrainingProgram tp)
        {
            TrainingProgramCourseComparer comparer = new TrainingProgramCourseComparer();

            List<Models.TrainingProgramCourse> requiredCourses =
                  RequiredCourses
                    .Except(tp.RequiredCourses, comparer)
                    .ToList();

            List<Models.TrainingProgramCourse> optionalCourses =
                  OptionalCourses
                    .Except(tp.OptionalCourses, comparer)
                    .ToList();

            List<Models.TrainingProgramCourse> electiveCourses =
                  ElectiveCourses
                    .Except(tp.ElectiveCourses, comparer)
                    .ToList();

            List<Models.TrainingProgramCourse> finalAssessments =
                  FinalAssessments
                    .Except(tp.FinalAssessments, comparer)
                    .ToList();

            if (RequiredCourses.Count == requiredCourses.Count &&
                OptionalCourses.Count == optionalCourses.Count &&
                ElectiveCourses.Count == electiveCourses.Count &&
                FinalAssessments.Count == finalAssessments.Count)
            {
                // there was no change
                return false;
            }

            // Set the new course assignements
            RequiredCourses = requiredCourses;
            OptionalCourses = optionalCourses;
            ElectiveCourses = electiveCourses;
            FinalAssessments = finalAssessments;

            // courses have changed
            return true;
        }

        private List<TrainingProgramCourse> GetCourseList(int syllabusType)
        {
            List<TrainingProgramCourse> courses = new List<TrainingProgramCourse>();
            switch (syllabusType)
            {
                case (int)SyllabusType.Required:
                    courses = RequiredCourses;
                    break;
                case (int)SyllabusType.Elective:
                    courses = ElectiveCourses;
                    break;
                case (int)SyllabusType.Optional:
                    courses = OptionalCourses;
                    break;
                case (int)SyllabusType.Final:
                    courses = FinalAssessments;
                    break;
                case (int)SyllabusType.Survey:
                    courses = AccreditationSurveys;
                    break;
            }

            return courses;
        }

        public bool HasPreviousRequiredCourse(TrainingProgramCourse course)
        {
            if (!SymphonyController.IsServerSideUiLogic) { return false; }

            List<TrainingProgramCourse> courses = GetCourseList(course.SyllabusTypeID);
            TrainingProgramCourse first = courses.FirstOrDefault();

            if (courses.Count > 0 && first.Id == course.Id && first.CourseTypeID == course.CourseTypeID)
            {
                return false;
            }

            foreach (TrainingProgramCourse c in courses)
            {
                if (c.Id == course.Id && course.CourseTypeID == course.CourseTypeID)
                {
                    return false;
                }

                if (!c.Passed.HasValue || c.Passed.Value == 0)
                {
                    return true;
                }
            }

            return false;
        }

        public bool HasPreviousCourseRequiringApproval(TrainingProgramCourse course)
        {
            if (!SymphonyController.IsServerSideUiLogic) { return false; }

            List<TrainingProgramCourse> courses = GetCourseList(course.SyllabusTypeID);
            TrainingProgramCourse first = courses.FirstOrDefault();

            if (courses.Count > 0 && first.Id == course.Id && first.CourseTypeID == course.CourseTypeID)
            {
                return false;
            }

            foreach (TrainingProgramCourse c in courses)
            {
                if (c.Id == course.Id && course.CourseTypeID == course.CourseTypeID)
                {
                    return false;
                }

                if (!c.CanMoveForwardIndicator && (EnforceCanMoveForwardIndicator || (c.EnforceCanMoveForwardIndicator.HasValue && c.EnforceCanMoveForwardIndicator.Value)))
                {
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Gets the previous course - Can return null
        /// </summary>
        /// <param name="course"></param>
        /// <returns></returns>
        public TrainingProgramCourse GetPreviousClassroomCourse(TrainingProgramCourse course)
        {
            if (!SymphonyController.IsServerSideUiLogic) { return null; }

            List<TrainingProgramCourse> courses = GetCourseList(course.SyllabusTypeID);

            return courses.GetRange(0, courses.IndexOf(course)).Where(c => c.CourseTypeID == course.CourseTypeID).LastOrDefault();
        }
        /// <summary>
        /// Gets the next course - Can return null
        /// </summary>
        /// <param name="course"></param>
        /// <returns></returns>
        public TrainingProgramCourse GetNextClassroomCourse(TrainingProgramCourse course)
        {
            if (!SymphonyController.IsServerSideUiLogic) { return null; }

            List<TrainingProgramCourse> courses = GetCourseList(course.SyllabusTypeID);
            int nextIndex = courses.IndexOf(course) + 1;
            int coursesAfter = courses.Count - nextIndex;

            if (coursesAfter > 0)
            {
                return courses.GetRange(nextIndex, coursesAfter).Where(c => c.CourseTypeID == course.CourseTypeID).FirstOrDefault();
            }

            return null;
        }

        /// <summary>
        /// Gets the certifificate link in the context of the requested user
        /// if this traininging program has a complete entry in the 
        /// training program rollup table. 
        /// 
        /// Returns null if the training program does not have a completed rollup
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DisplayIconState GetCompletedRollup(int userId = 0)
        {
            if (IsHistorical || (IsTrainingProgramRollupComplete.HasValue && IsTrainingProgramRollupComplete.Value == true))
            {
                // Just return a certificate. We have either no course data for this training program 
                // as it was completed in a different lms, or we are using the training program rollup
                // table and the compltion state has already been calculated.
                return DisplayHelpers.GetCertificateBadge(CertificateType.TrainingProgram, Id, 0, userId > 0 ? userId : 0);
            }

            return null;
        }

        #endregion

        /// <summary>
        /// The students current status in the training program
        /// only available when the training program is loaded within
        /// a student context. 
        /// Only available when loading a single training program
        /// </summary>
        [DataMember(Name = "trainingProgramRollup")]
        public TrainingProgramRollup TrainingProgramRollup { get; set; }

        /// <summary>
        /// Are all required courses complete 
        /// </summary>
        [DataMember(Name = "isRequiredComplete")]
        public bool IsRequiredComplete
        {
            get
            {
                return RequiredCourses.Where(c => c.Completed == false).Count() == 0;
            }
            internal set { }
        }
        /// <summary>
        /// Are all elective courses complete 
        /// </summary>
        [DataMember(Name = "isElectiveComplete")]
        public bool IsElectiveComplete
        {
            get
            {
                return ElectiveCourses.Where(c => c.Completed == true).Count() >= MinimumElectives;
            }
            internal set { }
        }
        /// <summary>
        /// Are all final courses complete 
        /// </summary>
        [DataMember(Name = "isFinalComplete")]
        public bool IsFinalComplete
        {
            get
            {
                return FinalAssessments.Where(c => c.Completed == false).Count() == 0;
            }
            internal set { }
        }

        /// <summary>
        /// Are all surveys complete
        /// </summary>
        [DataMember(Name = "isAccreditationSurveyComplete")]
        public bool IsAccreditationSurveyComplete
        {
            get
            {
                if (HasAccreditationSurveyData)
                {
                    return AccreditationSurveys.Where(c => c.Completed == false).Count() == 0;
                }
                else
                {
                    // will be null if query is run for more than a single TP.
                    if (TrainingProgramRollup != null)
                    {
                        return TrainingProgramRollup.IsAccreditationSurveyComplete;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            internal set { }
        }

        /// <summary>
        /// Are electives enabled
        /// </summary>
        [DataMember(Name = "isElectivesAvailable")]
        public bool IsElectivesAvailable
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return false; }
                return !EnforceRequiredOrder || (EnforceRequiredOrder && IsRequiredComplete);
            }
            internal set { }
        }

        /// <summary>
        /// Are optional courses enabled
        /// </summary>
        [DataMember(Name = "isOptionalAvailable")]
        public bool IsOptionalAvailable
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return false; }
                return !EnforceRequiredOrder || (EnforceRequiredOrder && IsRequiredComplete && IsElectiveComplete);
            }
            internal set { }
        }

        [DataMember(Name = "isFinalAvailable")]
        public bool IsFinalAvailable
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return false; }
                return IsRequiredComplete && IsElectiveComplete && RequiredCanMoveForward && IsMinCourseWorkComplete;
            }
            internal set { }
        }

        [DataMember(Name = "isAccreditationSurveyAvailable")]
        public bool IsAccreditationSurveyAvailable
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return false; }
                return IsFinalComplete;
            }
            internal set { }
        }

        [DataMember(Name = "requiredCanMoveForward")]
        public bool RequiredCanMoveForward
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return true; }

                if (RequiredCourses.Count == 0)
                {
                    return true;
                }

                if (FinalAssessments.Count > 0)
                {
                    Course finalCourse = RequiredCourses.LastOrDefault();
                    if ((finalCourse.EnforceCanMoveForwardIndicator.HasValue && finalCourse.EnforceCanMoveForwardIndicator.Value) || EnforceCanMoveForwardIndicator)
                    {
                        return finalCourse.CanMoveForwardIndicator;
                    }
                }

                return true;
            }
            internal set { }
        }
        [DataMember(Name = "assignmentCount")]
        public AssignmentCount AssignmentCounts
        {
            get
            {
                if (_assignmentCount == null)
                {
                    _assignmentCount = AssignmentCount.GetTrainingProgramAssignmentCount(this);
                }
                return _assignmentCount;
            }
            internal set { }
        }
        [DataMember(Name = "isAssignmentsComplete")]
        public bool IsAssignmentsComplete
        {
            get
            {
                return AssignmentCounts.TotalAssignments == AssignmentCounts.TotalAssignmentsComplete;
            }
            internal set { }
        }

        #region DisplayFields
        /* Display fields */

        /// <summary>
        /// Overrides the user id used when generating the cert. 
        /// </summary>
        [IgnoreDataMember]
        public int? UserIDOverride { get; set; }

        [DataMember(Name = "displayDueDate")]
        public DisplayDate DisplayDueDate
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DateTime? date = null;

                if (DueDate.HasValue && DueDate.Value > StartDate.Value && (EndDate.HasValue && DueDate.Value < EndDate.Value))
                {
                    date = DueDate;
                }
                else if (EndDate.HasValue)
                {
                    date = EndDate;
                }
                else
                {
                    return null;
                }

                return new DisplayDate()
                {
                    Date = date.Value,
                    Format = IsUsingSessions ? DateFormat.DateTime : DateFormat.Date,
                    Messages = new List<Text>() { (IsUsingSessions && !IsSessionRegistered ? Text.Session_RegistrationRequired : Text.Empty) }
                };
            }
            internal set { }
        }

        [DataMember(Name = "isSurveyComplete")]
        public bool IsSurveyComplete
        {
            get
            {
                return SurveyTaken && SurveyID > 0 && IsSurveyMandatory ||
                   !IsSurveyMandatory ||
                   SurveyID == 0;
            }
            internal set { }
        }

        [DataMember(Name = "displayCompleted")]
        public DisplayIconState DisplayCompleted
        {
            get
            {
                DisplayIconState completedState = GetCompletedRollup(UserIDOverride.HasValue ? UserIDOverride.Value : 0);
                if (completedState != null)
                {
                    return completedState;
                }
                else
                {
                    completedState = new DisplayIconState();
                }

                completedState.Link = new DisplayLink()
                {
                    BaseUrl = Url.Empty,
                    Title = Text.Empty
                };

                bool courseSurveyComplete = !SurveysRequired;
                bool assignmentsComplete = !AssignmentsRequired;

                if (RequiredCourses.Count > 0 || ElectiveCourses.Count > 0)
                {
                    if (Completed)
                    {
                        bool canMoveForward = true;

                        if (FinalAssessments.Count > 0)
                        {
                            canMoveForward = RequiredCanMoveForward;
                        }

                        if (IsAccreditationSurveyComplete && IsSurveyComplete && courseSurveyComplete && canMoveForward && assignmentsComplete && IsMinCourseWorkComplete)
                        {
                            completedState = DisplayHelpers.GetCertificateBadge(CertificateType.TrainingProgram, Id, 0, UserIDOverride.HasValue ? UserIDOverride.Value : 0);
                        }
                        else if (!assignmentsComplete)
                        {
                            completedState.Messages.Add(Text.TrainingProgram_AssignmentsRequired);
                            completedState.Icon = IconState.Incomplete;
                        }
                        else if (!IsMinCourseWorkComplete)
                        {
                            completedState.Messages.Add(Text.TrainingProgram_MinimumTimeIncomplete);
                            completedState.Icon = IconState.Incomplete;
                        }
                        else if (!IsSurveyComplete)
                        {
                            completedState.Messages.Add(Text.TrainingProgram_SurveysRequired);
                            completedState.Icon = IconState.Incomplete;
                        }
                        else
                        {
                            completedState.Messages.Add(Text.TrainingProgram_SurveysRequired);
                            completedState.Icon = IconState.Incomplete;
                        }
                    }
                    else
                    {
                        completedState.Messages.Add(Text.TrainingProgram_Incomplete);
                        completedState.Icon = IconState.Incomplete;
                    }
                }
                else
                {
                    completedState.Messages.Add(Text.Empty);
                    completedState.Icon = IconState.Empty;
                }

                return completedState;
            }
            internal set { }
        }


        [DataMember(Name = "displaySurvey")]
        public DisplayIconState DisplaySurvey
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayIconState _survey = new DisplayIconState();

                if (SurveyID > 0)
                {
                    if (Completed)
                    {
                        _survey.Icon = IconState.Empty;
                        _survey.Link = DisplayHelpers.GetSimpleCourseLaunchLink(SurveyID, Id);
                        _survey.Link.BaseUrl = Url.Launch;
                        _survey.Link.Title = Text.TakeSurvey;
                    }
                    else
                    {
                        _survey.Icon = IconState.Inactive;
                        _survey.Messages.Add(Text.TrainingProgram_SurveyUnavailable);
                    }
                }
                else
                {
                    _survey.Icon = IconState.Inactive;
                    _survey.Messages.Add(Text.TrainingProgram_NoSurvey);
                }

                return _survey;
            }
            internal set { }
        }

        [DataMember(Name = "displayAccreditationSurvey")]
        public DisplayIconState DisplayAccreditationSurvey
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayIconState _survey = new DisplayIconState();

                if (_accreditationSurveys.Count > 0)
                {
                    if (Completed)
                    {
                        var survey = _accreditationSurveys.FirstOrDefault(ac => ac.Completed == false);

                        if (survey != null)
                        {
                            _survey.Icon = IconState.Empty;
                            _survey.Link = DisplayHelpers.GetSimpleCourseLaunchLink(survey.OnlineCourseID, Id);
                            _survey.Link.BaseUrl = Url.Launch;
                            _survey.Link.Title = Text.TakeSurvey;
                        }
                    }
                    else
                    {
                        _survey.Icon = IconState.Inactive;
                        _survey.Messages.Add(Text.TrainingProgram_SurveyUnavailable);
                    }
                }
                else
                {
                    _survey.Icon = IconState.Inactive;
                    _survey.Messages.Add(Text.TrainingProgram_NoSurvey);
                }

                return _survey;
            }
            internal set { }
        }

        [DataMember(Name = "displayIsElectivesAvailable")]
        public DisplayModel DisplayIsElectivesAvailable
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayModel dv = new DisplayModel();

                if (!IsElectivesAvailable)
                {
                    dv.Messages.Add(Text.TrainingProgram_ElectivesUnavailable);
                }
                return dv;
            }
            internal set { }
        }

        [DataMember(Name = "displayIsOptionalAvailable")]
        public DisplayModel DisplayIsOptionalAvailable
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayModel dv = new DisplayModel();

                if (!IsOptionalAvailable)
                {
                    dv.Messages.Add(Text.TrainingProgram_OptionalUnavailable);
                }
                return dv;
            }
            internal set { }
        }

        [DataMember(Name = "displayIsFinalAvailable")]
        public DisplayModel DisplayIsFinalAvailable
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayModel _display = new DisplayModel();

                if (IsFinalAvailable)
                {
                    return _display;
                }

                _display.Messages.Add(Text.TrainingProgram_FinalDisabled);

                if (!IsRequiredComplete)
                {
                    _display.Messages.Add(Text.TrainingProgram_FinalDisabled_RequiredComplete);
                }

                if (!IsMinCourseWorkComplete)
                {
                    _display.Messages.Add(Text.TrainingProgram_FinalDisabled_MinimumTimeIncomplete);
                }

                if (!IsElectiveComplete)
                {
                    _display.Messages.Add(Text.TrainingProgram_FinalDisabled_ElectivesComplete);
                }

                if (!RequiredCanMoveForward)
                {
                    _display.Messages.Add(Text.TrainingProgram_FinalDisabled_RequiredMoveForward);
                }

                if (!IsAssignmentsComplete)
                {
                    if (AssignmentCounts.TotalAssignmentsFailed > 0)
                    {

                        Text assignmentsFailed = Text.TrainingProgram_FinalDisabled_AssignmentsFailed;
                        string assignmentsFailedMessage = assignmentsFailed.Value.Replace("{!assignmentsFailed}", AssignmentCounts.TotalAssignmentsFailed.ToString());

                        _display.Messages.Add(Text.Dynamic(assignmentsFailedMessage));
                    }
                    if (AssignmentCounts.TotalAssignmentsUnmarked > 0)
                    {
                        Text assignmentsUnmarked = Text.TrainingProgram_FinalDisabled_AssignmentsUnmarked;
                        string assignmentsUnmarkedMessage = assignmentsUnmarked.Value.Replace("{!assignmentsUnmarked}", AssignmentCounts.TotalAssignmentsUnmarked.ToString());

                        _display.Messages.Add(Text.Dynamic(assignmentsUnmarkedMessage));
                    }
                }

                _display.Messages.Add(Text.TrainingProgram_FinalDisabled_Footer);

                return _display;
            }
            internal set { }
        }

        [DataMember(Name = "displayIsAccreditationSurveyAvailable")]
        public DisplayModel DisplayIsAccreditationSurveyAvailable
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayModel _display = new DisplayModel();

                if (IsAccreditationSurveyAvailable)
                {
                    return _display;
                }

                _display.Messages.Add(Text.TrainingProgram_AccreditationSurveyDisabled_Header);

                if (!IsFinalComplete)
                {
                    _display.Messages.Add(Text.TrainingProgram_AccreditationSurveyDisabled_FinalComplete);
                }

                _display.Messages.Add(Text.TrainingProgram_AccreditationSurveyDisabled_Footer);

                return _display;
            }
            internal set { }
        }

        [DataMember(Name = "certificateTemplateId")]
        public int? CertificateTemplateId { get; set; }

        #endregion
    }
}