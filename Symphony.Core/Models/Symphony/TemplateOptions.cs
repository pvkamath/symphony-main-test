﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Data;

namespace Symphony.Core.Models
{
    public class TemplateOptions
    {
        public TemplateOptions()
        {
        }

        public TemplateOptions(Template template)
        {
            Init(template);
        }

        /// <summary>
        /// Special overload to allow explicitly returning a set of information for a given schedule parameter.
        /// This is so the user can change the schedule parameter and we can dynamically get a new list of recipients
        /// and template parameters.
        /// </summary>
        /// <param name="scheduleParameterID"></param>
        public TemplateOptions(int scheduleParameterID)
        {
            if (scheduleParameterID != 0)
            {
                Init(new Template() { ScheduleParameterID = scheduleParameterID });
            }
        }

        public void Init(Template template)
        {
            if (template.ScheduleParameterID > 0)
            {
                PossibleRecipients = Controllers.TemplateController.GetRecipientsForScheduleParameter(template.ScheduleParameterID);
                TemplateParameters = Controllers.TemplateController.GetTemplateParametersForScheduleParameter(template.ScheduleParameterID);
            }
            else
            {
                // for non-scheduled templates, the parameters list needs to be loaded based on the template name
                // instead of the scheduleparameterid
                PossibleRecipients = Controllers.TemplateController.GetRecipientsForTemplate(template);
                TemplateParameters = Controllers.TemplateController.GetTemplateParametersForTemplate(template);   
            }
        }

        public TreeNode TemplateParameters { get; set; }

        public RecipientGroupCollection PossibleRecipients { get; set; }
    }
}
