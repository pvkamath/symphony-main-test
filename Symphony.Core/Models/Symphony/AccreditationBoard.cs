﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    /// <summary>
    /// An AccreditationBoard is an organization that is responsible for accrediting courses created
    /// in Artisan. An "accreditation" is an official endorsement of the course by the board.
    /// </summary>
    [DataContract(Name="accreditationBoard")]
    public class AccreditationBoard : Model
    {
        static AccreditationBoard()
        {
            PagedQueryParams<AccreditationBoard>.RegisterSortableProperties(new Dictionary<string, string>
            {
                { "name", "Name" },
                { "description", "Description" },
                { "primaryContact", "primaryContact" },
                { "phone", "phone" }
            });
        }

        /// <summary>
        /// Gets or sets the unique database identifier for the accreditation board.
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the accreditation board. This is a required property.
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the descirption of the accreidtation board. This is an optional property
        /// that briefly describes the purpose of the board.
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the name of the primary contact for the accreditation board. No validation
        /// is performed on this property and it may be omitted.
        /// </summary>
        [DataMember(Name = "primaryContact")]
        public string PrimaryContact { get; set; }

        /// <summary>
        /// Gets or sets the phone number used to contact the accreditation board. No validation is
        /// performed on this property and it may be omitted.
        /// </summary>
        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the disclaimer for the accreditation board.
        /// </summary>
        [DataMember(Name = "disclaimer")]
        public string Disclaimer { get; set; }

        /// <summary>
        /// Gets or sets the accreditation board's logo, as a base-64 encoded string (data uri).
        /// </summary>
        [DataMember(Name = "logo")]
        public string Logo { get; set; }

        /// <summary>
        /// Gets or sets the professions associated with the accreditation board. When taking a
        /// course accredited by an accreditation board, the user must select from one of these
        /// professions.
        /// </summary>
        [DataMember(Name = "professions")]
        public List<AccreditationBoardProfession> Professions { get; set; }

        /// <summary>
        /// Gets or sets the surveys associated with an accreditation board. These surveys must be
        /// taken upon completing a training program accredited by an accreditation board.
        /// </summary>
        [DataMember(Name = "surveys")]
        public List<AccreditationBoardSurvey> Surveys { get; set; }

        /// <summary>
        /// Get or set associated training program accreditations for this board
        /// </summary>
        [DataMember(Name = "trainingProgramAccreditations")]
        public List<TrainingProgramAccreditation> TrainingProgramAccreditations { get; set; }
        /// <summary>
        /// get or set associated online course accreditations for this board
        /// </summary>
        [DataMember(Name = "onlineCourseAccreditations")]
        public List<OnlineCourseAccreditation> OnlineCourseAccreditations { get; set; }
    }
}
