﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "gtmOrganizer")]
    [DefaultSort(ColumnName = "Email")]
    public class GTMOrganizer : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "organizerKey")]
        public long OrganizerKey { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }
    }
}
