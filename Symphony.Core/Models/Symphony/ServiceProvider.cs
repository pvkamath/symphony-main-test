﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "serviceProvider")]
    public class ServiceProvider : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "artifactUrl")]
        public string ArtifactUrl { get; set; }

        [DataMember(Name = "logoutUrl")]
        public string LogoutUrl { get; set; }

        [DataMember(Name = "method")]
        public string Method { get; set; }

        [DataMember(Name = "assertionUrl")]
        public string AssertionUrl { get; set; }

        [DataMember(Name = "nameIdentifier")]
        public string NameIdentifier { get; set; }

        [DataMember(Name = "isRedirectArtifact")]
        public bool IsRedirectArtifact { get; set; }

        [DataMember(Name = "isDisableTheme")]
        public bool IsDisableTheme { get; set; }
    }
}
