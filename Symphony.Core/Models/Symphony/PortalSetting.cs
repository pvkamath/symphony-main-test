﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "portalSetting")]
    public class PortalSetting : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerId { get; set; }

        [DataMember(Name = "widgetId")]
        public int WidgetId { get; set; }

        [DataMember(Name = "widgetName")]
        public string WidgetName { get; set; }

        [DataMember(Name = "widgetOrderNumber")]
        public int WidgetOrderNumber { get; set; }

        [DataMember(Name = "widgetIsVisible")]
        public bool WidgetIsVisible { get; set; }

    }
}
