﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract]
    public class QuickMessage : Model
    {
        [DataMember(Name = "recipients")]
        public List<HierarchyLink> Recipients { get; set; }

        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "body")]
        public string Body { get; set; }
    }
}
