using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "reportProcedure")]
    [DefaultSort(ColumnName = "fullName")]
    public class ReportProcedure : Model
    {
        [DataMember(Name = "fullName")]
        public string FullName { get; set; }

        [DataMember(Name = "codeName")]
        public string CodeName { get; set; }
    }
}
