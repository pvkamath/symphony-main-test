﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "genericFilter")]
    public class GenericFilter
    {
        [DataMember(Name = "logicOperator")]
        public int LogicOperator { get; set; }

        [DataMember(Name = "comparisonOperator")]
        public int ComparisonOperator { get; set; }

        [DataMember(Name = "isOpenExpression")]
        public bool IsOpenExpression { get; set; }

        [DataMember(Name = "isCloseExpression")]
        public bool IsCloseExpression { get; set; }

        [DataMember(Name = "property")]
        public string Property { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }
    }
}
