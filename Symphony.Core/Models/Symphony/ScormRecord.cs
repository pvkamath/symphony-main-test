﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="scormRecord")]
    public class ScormRecord : Model
    {
        [DataMember(Name = "webPath")]
        public string WebPath { get; set; }

        [DataMember(Name = "scormActivityId")]
        public int ScormActivityID { get; set; }

        //[DataMember(Name = "activityProgressStatus")]
        //public bool ActivityProgressStatus { get; set; }

        [DataMember(Name = "activityAttemptCount")]
        public int ActivityAttemptCount { get; set; }

        [DataMember(Name = "activityCompletionAmount")]
        public decimal AttemptCompletionAmount { get; set; }

        [DataMember(Name = "attemptCompletionStatus")]
        public bool AttemptCompletionStatus { get; set; }

        [DataMember(Name = "activityStartTimeUtc")]
        public DateTime ActivityStartTimeUTC { get; set; }

        [DataMember(Name = "attemptStartTimeUtc")]
        public DateTime AttemptStartTimeUTC { get; set; }

        [DataMember(Name = "activityUpdateTime")]
        public DateTime ActivityUpdateTime { get; set; }

        //[DataMember(Name = "completionStatus")]
        //public bool CompletionStatus { get; set; }

        //[DataMember(Name = "successStatus")]
        //public bool SuccessStatus { get; set; }

        [DataMember(Name = "bookmark")]
        public string Bookmark { get; set; }

        [DataMember(Name = "score")]
        public decimal Score { get; set; }

        //[DataMember(Name = "activityRTSuccessStatus")]
        //public bool ActivityRTSuccessStatus { get; set; }

        [DataMember(Name = "dataChunk")]
        public string DataChunk { get; set; }

        [DataMember(Name = "activityRTUpdateTime")]
        public DateTime ActivityRTUpdateTime { get; set; }

        [DataMember(Name = "interactionIndex")]
        public int InteractionIndex { get; set; }

        [DataMember(Name = "artisanPageID")]
        public string ArtisanPageID { get; set; }

        [DataMember(Name = "typeId")]
        public int TypeID { get; set; }

        [DataMember(Name = "interactionTimeUtc")]
        public DateTime InteractionTimeUTC { get; set; }

        //[DataMember(Name = "interactionResult")]
        //public bool InteractionResult { get; set; }

        [DataMember(Name = "interactionDescription")]
        public string InteractionDescription { get; set; }

        [DataMember(Name = "learnerResponse")]
        public string LearnerResponse { get; set; }

        [DataMember(Name = "responseUpdateTime")]
        public DateTime ReponseUpdateTime { get; set; }

    }
}
