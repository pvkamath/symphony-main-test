﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name="hierarchyLink")]
    public class HierarchyLink
    {
        [DataMember(Name = "hierarchyNodeId")]
        public int HierarchyNodeID { get; set; }

        [DataMember(Name = "hierarchyTypeId")]
        public int HierarchyTypeID { get; set; }
    }
}
