﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    /// <summary>
    /// Summary information of the data in the TrainingProgramRollup table
    /// This is also shared with TrainingProgramSummaryDetail
    /// </summary>
    [DataContract(Name="TrainingProgramSummary")]
    public class TrainingProgramSummary : Model
    {
        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "isCompleteOverride")]
        public bool IsCompleteOverride { get; set; }

        [DataMember(Name = "dateCompleted")]
        public DateTime DateCompleted { get; set; }

        [DataMember(Name = "isSurveyComplete")]
        public bool IsSurveyComplete { get; set; }

        [DataMember(Name = "requiredCompletedCount")]
        public int RequiredCompletedCount { get; set; }

        [DataMember(Name = "electiveCompletedCount")]
        public int ElectiveCompletedCount { get; set; }

        [DataMember(Name = "finalCompletedCount")]
        public int FinalCompletedCount { get; set; }

        [DataMember(Name = "assignmentsCompletedCount")]
        public int AssignmentsCompletedCount { get; set; }

        [DataMember(Name = "finalExamScore")]
        public string FinalExamScore { get; set; }

        [DataMember(Name = "isRequiredComplete")]
        public bool IsRequiredComplete { get; set; }

        [DataMember(Name = "isElectiveComplete")]
        public bool IsElectiveComplete { get; set; }

        [DataMember(Name = "isFinalComplete")]
        public bool IsFinalComplete { get; set; }

        [DataMember(Name = "isComplete")]
        public bool IsComplete { get; set; }

        [DataMember(Name = "totalSeconds")]
        public long TotalSeconds { get; set; }

        [DataMember(Name = "percentComplete")]
        public int PercentComplete { get; set; }

        [DataMember(Name = "professionId")]
        public int? ProfessionID { get; set; }

        [DataMember(Name = "isAccreditationSurveyComplete")]
        public bool IsAccreditationSurveyComplete { get; set; }
    }
}
