﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Extensions;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="transcriptEntryCourse")]
    public class TranscriptEntryCourse : Course
    {
        [DataMember(Name = "courseId")]
        public int CourseID { get; set; }

        [DataMember(Name = "courseName")]
        public string CourseName { get; set; }

        [DataMember(Name = "trainingProgramId")]
        /// <summary>
        /// This value is ONLY valid for online courses. For classroom courses, we don't have a 1-1 association, and this will be 0
        /// </summary>
        public int TrainingProgramID { get; set; }
        [DataMember(Name = "trainingProgramName")]
        /// <summary>
        /// This value is ONLY valid for online courses. For classroom courses, we don't have a 1-1 association, and this will be 0
        /// </summary>
        public string TrainingProgramName { get; set; }

        // If historical, this entry is just a placeholder record for a previously completed
        // training program from an external system. We will not have detailed course transcript
        // information for any historical training programs. 
        [DataMember(Name = "isHistorical")]
        public bool IsHistorical { get; set; }

        /// <summary>
        /// Refers to the completed flag in training program rollup.
        /// If this is set, and it is true, then we can assume that the entire 
        /// training program has been completed since it was previously calculated
        /// as being complete.
        /// </summary>
        [DataMember(Name = "isTrainingProgramRollupComplete")]
        public bool? IsTrainingProgramRollupComplete { get; set; }

        /// <summary>
        /// The parent training program certificate if it exists for this entry
        /// 
        /// Only returns a certificate from the training program rollup table. 
        /// 
        /// </summary>
        [DataMember(Name = "displayTrainingProgramCertificate")]
        public DisplayIconState DisplayTrainingProgramCertificate
        {
            get { 
                return TrainingProgram != null ? TrainingProgram.GetCompletedRollup(TranscriptUserID) : new DisplayIconState(); 
            }
            internal set { }
        }

        /// <summary>
        /// True if this course is used as a session for a training program. 
        /// In this case we probably don't want to display it in the transcript
        /// since it will never have a grade. 
        /// </summary>
        [DataMember(Name = "isSessionCourse")]
        public bool IsSessionCourse
        {
            get;
            set;
        }

    }
}
