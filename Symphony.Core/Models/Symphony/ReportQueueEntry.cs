using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "reportQueueEntry")]
    public class ReportQueueEntry : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "reportId")]
        public int ReportId { get; set; }

        [DataMember(Name = "queueId")]
        public int QueueId { get; set; }

        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                return ((ReportStatus)StatusId).ToString();
            }
            internal set { }
        }

        [DataMember(Name = "statusId")]
        public int StatusId { get; set; }

        [DataMember(Name = "startedOn")]
        public DateTime StartedOn { get; set; }

        [DataMember(Name = "finishedOn")]
        public DateTime FinishedOn { get; set; }

        [DataMember(Name = "reportName")]
        public string ReportName { get; set; }	
		
        [DataMember(Name = "downloadCSV")]
        public bool DownloadCSV { get; set; }

        [DataMember(Name = "downloadXLS")]
        public bool DownloadXLS { get; set; }

        [DataMember(Name = "downloadPDF")]
        public bool DownloadPDF { get; set; }

        [DataMember(Name = "nextRunTime")]
        public DateTime NextRunTime { get; set; }
    }
}
