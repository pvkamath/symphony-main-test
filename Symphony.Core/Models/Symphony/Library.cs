﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Models
{
    [DataContract(Name = "library")]
    [DefaultSort(ColumnName="Name")]
    public class Library : Model, IAssociatedImage
    {
        [DataMember(Name = "associatedImageData")]
        public string AssociatedImageData { get; set; }

        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "cost")]
        public double Cost { get; set; }

        [DataMember(Name = "sku")]
        public string Sku { get; set; }

        [DataMember(Name = "details")]
        public string Details { get; set; }

        [DataMember(Name = "libraryItemTypeId")]
        public int LibraryItemTypeID { get; set; }

        [DataMember(Name = "itemCount")]
        public int ItemCount { get; set; }

        [DataMember(Name = "registrationCount")]
        public int RegistrationCount { get; set; }

        [DataMember(Name = "totalRows")]
        public int TotalRows { get; set; }

        [DataMember(Name = "isShared")]
        public bool IsShared { get; set; }

        // Display
        [DataMember(Name = "displayCreatedOn")]
        public DisplayDate DisplayCreatedOn
        {
            get
            {
                return new DisplayDate()
                {
                    Format = DateFormat.Date,
                    Date = CreatedOn
                };
            }
            internal set { }
        }
        [DataMember(Name = "displayLibraryItemType")]
        public DisplayIconState DisplayLibraryItemType
        {
            get
            {
                return DisplayHelpers.GetLibraryItemTypeIcon(LibraryItemTypeID);
            }
            internal set { }
        }
    }
}
