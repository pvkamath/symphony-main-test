﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "onlineCourseRollup")]
    public class OnlineCourseRollup : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "courseId")]
        public int CourseID { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "score")]
        public decimal Score { get; set; }

        [DataMember(Name = "attemptCount")]
        public int AttemptCount { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "courseName")]
        public string CourseName { get; set; }

        [DataMember(Name = "trainingProgramName")]
        public string TrainingProgramName { get; set; }

        [DataMember(Name = "testAttemptCount")]
        public int TestAttemptCount { get; set; }

        [DataMember(Name = "navigationPercentage")]
        public int NavigationPercentage { get; set; }

        [DataMember(Name = "totalSeconds")]
        public long TotalSeconds { get; set; }

        [DataMember(Name = "totalSecondsPreviousRegistration")]
        public long TotalSecondsPreviousRegistration { get; set; }

        [DataMember(Name = "totalSecondsRounded")]
        public long TotalSecondsRounded { get; set; }

        [DataMember(Name = "totalSecondsLastAttempt")]
        public long TotalSecondsLastAttempt { get; set; }

        [DataMember(Name = "totalSecondsScorm")]
        public long TotalSecondsScorm { get; set; }

        [DataMember(Name = "proctorKey")]
        public string ProctorKey { get; set; }
    }
}
