﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "libraryGrantHierarchy")]
    [DefaultSort(ColumnName="HierarchyTypeID")]
    /*
     [KnownType(typeof(User))]
    [KnownType(typeof(Location))]
    [KnownType(typeof(Audience))]
    [KnownType(typeof(JobRole))]
     */
    public class LibraryGrantHierarchy : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "hierarchyTypeId")]
        public int HierarchyTypeID { get; set; }

        [DataMember(Name = "hierarchyNodeId")]
        public int HierarchyNodeID { get; set; }

        [DataMember(Name = "locationId")]
        public int LocationID { get; set; }

        [DataMember(Name = "jobRoleId")]
        public int JobRoleID { get; set; }

        [DataMember(Name = "audienceId")]
        public int AudienceID { get; set; }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "levelIndentText")]
        public string LevelIndentText { get; set; }

        [DataMember(Name = "parentId")]
        public int ParentID { get; set; }

        [DataMember(Name = "startDate")]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public DateTime EndDate { get; set; }

        [DataMember(Name = "totalRows")]
        public Int64 TotalRows { get; set; }

        /// <summary>
        /// Customer id of the hierarchy item the 
        /// library is assigned to
        /// </summary>
        [DataMember(Name = "hierarchyCustomerId")]
        public int HierarchyCustomerID { get; set; }

        /// <summary>
        /// Customer id of the library
        /// </summary>
        [DataMember(Name = "libraryCustomerId")]
        public int LibraryCustomerID { get; set; }

        /// <summary>
        /// Is this grant for a hierarchy item that 
        /// is not in the same customer as the library
        /// </summary>
        [DataMember(Name = "isExternalGrant")]
        public bool IsExternalGrant { get; set; }

        /// <summary>
        /// Is the grant for the current customer.
        /// This is regardless of whether the grant
        /// is for an external library or not. 
        /// </summary>
        [DataMember(Name = "isCustomerGrant")]
        public bool IsCustomerGrant { get; set; }

        /*
         * In case we need this at some point
        [DataMember(Name = "hierarchyObject")]
        public Model HierarchyObject
        {
            get; set;
        }
         */
    }
}
