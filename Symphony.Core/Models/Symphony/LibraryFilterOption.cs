﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "libraryFilterOption")]
    public class LibraryFilterOption : Model
    {
        
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "value")]
        public int Value { get; set; }

        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

        [DataMember(Name = "data")]
        public List<LibraryFilterOption> Data { get; set; }

        [DataMember(Name = "expanded")]
        public bool Expanded { get { return Data != null && Data.Count > 0; } set { } }

        [DataMember(Name = "leaf")]
        public bool Leaf { get { return !Expanded; } set { } }

        [DataMember(Name = "checked")]
        public bool? Checked { get; set; }
    }
}
