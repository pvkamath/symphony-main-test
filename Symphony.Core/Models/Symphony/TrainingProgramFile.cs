﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name="trainingProgramFile")]
    public class TrainingProgramFile
    {
        [DataMember(Name="id")]
        public int Id { get; set; }

        [DataMember(Name="filename")]
        public string Filename { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name="description")]
        public string Description { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }
    }
}
