﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Attributes;
using System.Reflection;

namespace Symphony.Core.Models
{
    [DataContract(Name = "theme")]
    [DefaultSort(ColumnName = "Name")]
    public class Theme : Model
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "codeName")]
        public string CodeName { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "id")]
        public int? ID
        {   
            get
            {
                return _ID;
            }
            set
            {
                // This futzery is because if an ExtJS model has id 0, the proxy will stupidly send
                // it like /themes/0. The way around that is to make the id property nullable on
                // ExtJS' side. 
                //
                // This needs to be a nullable int so that if ExtJS sends null, the value can be
                // bound. We're assuming this is not nullable in a lot of places though, so we
                // use 0 instead of null for compatibility.
                //
                // This is specific to REST proxies in Ext4.0. Later versions of Ext4 will remove
                // the trailing 0.
                if (value.HasValue)
                {
                    _ID = value.Value;
                }
                else
                {
                    _ID = 0;
                }
            }
        }
        private int _ID;

        [ScssVariableField]
        [DataMember(Name = "primaryColor")]
        public string PrimaryColor { get; set; }

        [ScssVariableField]
        [DataMember(Name = "secondaryColor")]
        public string SecondaryColor { get; set; }

        [ScssVariableField]
        [DataMember(Name = "tertiaryColor")]
        public string TertiaryColor { get; set; }

        [ScssVariableField]
        [DataMember(Name = "quaternary")]
        public string Quaternary { get; set; }

        [ScssVariableField]
        [DataMember(Name = "primaryImage")]
        public string PrimaryImage { get; set; }

        [ScssVariableField]
        [DataMember(Name = "secondaryImage")]
        public string SecondaryImage { get; set; }

        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }

        public List<PropertyInfo> GetScssVariableFields()
        {
            List<PropertyInfo> properties = typeof(Theme).GetProperties()
                .Where(prop => prop.IsDefined(typeof(ScssVariableField), false))
                .ToList();

            return properties;
        }

        public DateTime ModifiedOn { get; set; }
    }
}
