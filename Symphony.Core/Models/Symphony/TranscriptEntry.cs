﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Extensions;

namespace Symphony.Core.Models
{
    [DataContract]
    public class TranscriptEntry
    {
        [DataMember(Name = "courseKey")]
        public string CourseKey
        {
            get
            {
                return CourseID + "-" + CourseTypeID;
            }
            internal set { }
        }

        [DataMember(Name="courseId")]
        public int CourseID { get; set; }

        [DataMember(Name = "courseName")]
        public string CourseName { get; set; }

        [DataMember(Name = "courseTypeId")]
        public int CourseTypeID { get; set; }

        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        [DataMember(Name = "registrationId")]
        public int RegistrationID { get; set; }

        [DataMember(Name = "registrationStatusId")]
        public int RegistrationStatusID { get; set; }

        [DataMember(Name = "attendedIndicator")]
        public bool AttendedIndicator { get; set; }

        [DataMember(Name = "passOrFail")]
        public string PassOrFail { get; set; }

        [DataMember(Name = "passed")]
        public int? Passed { get; set; }

        [DataMember(Name = "completeOrIncomplete")]
        public string CompleteOrIncomplete { get; set; }

        [DataMember(Name = "completed")]
        public int? Completed { get; set; }

        [DataMember(Name = "onlineTestId")]
        public int OnlineTestID { get; set; }

        [DataMember(Name = "surveyId")]
        public int SurveyID { get; set; }

        [DataMember(Name = "surveyTaken")]
        public bool SurveyTaken { get; set; }

        [DataMember(Name = "isPublic")]
        public bool IsPublic { get; set; }

        [DataMember(Name = "score")]
        public string Score { get; set; }

        [DataMember(Name = "attemptDate")]
        public DateTime? AttemptDate { get; set; }

        [DataMember(Name = "attemptTime")]
        public long AttemptTime { get; set; }

        [DataMember(Name = "attemptCount")]
        public int AttemptCount { get; set; }

        [DataMember(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public DateTime? EndDate { get; set; }

        [DataMember(Name = "trainingProgramId")]
        /// <summary>
        /// This value is ONLY valid for online courses. For classroom courses, we don't have a 1-1 association, and this will be 0
        /// </summary>
        public int TrainingProgramID { get; set; }
        [DataMember(Name = "trainingProgramName")]
        /// <summary>
        /// This value is ONLY valid for online courses. For classroom courses, we don't have a 1-1 association, and this will be 0
        /// </summary>
        public string TrainingProgramName { get; set; }

        [DataMember(Name = "webinarRegistrationID")]
        public string WebinarRegistrationID { get; set; }

        [DataMember(Name = "webinarKey")]
        public string WebinarKey { get; set; }

        [DataMember(Name = "canMoveForwardIndicator")]
        public bool CanMoveForwardIndicator { get; set; }

        [DataMember(Name = "hasEnded")]
        public bool HasEnded
        {
            get { return EndDate.HasValue && DateTime.Now.ToUniversalTime() > EndDate.Value.WithUtcFlag(); }
            private set { return; }
        }

        [DataMember(Name = "hasStarted")]
        public bool HasStarted
        {
            get { return StartDate.HasValue && DateTime.Now.ToUniversalTime() > StartDate.Value.WithUtcFlag(); }
            private set { return; }
        }

        /// <summary>
        /// Is an assignment required for this course
        ///     ** Note that this is only true if there is an assignment and it is not marked or the student did not pass
        ///     If a course has an assignment, and the user has completed it successfully this will be false.
        /// </summary>
        [DataMember(Name = "isAssignmentRequired")]
        public bool IsAssignmentRequired { get; set; }

        /// <summary>
        /// In the case where an assignment is required, this will be true if the assignment has been marked, meaning the user
        /// did not score high enough to complete the assignment. 
        /// If false this means the instructor has not yet looked at the assignment. 
        /// </summary>
        [DataMember(Name = "isAssignmentMarked")]
        public bool IsAssignmentMarked { get; set; }

        /// <summary>
        /// OnlineCourseRollupID that contains the online course score
        /// </summary>
        [DataMember(Name = "onlineCourseRollupId")]
        public int OnlineCourseRollupID { get; set; }

        /// <summary>
        /// Number of times the test for this course has been attempted
        /// </summary>
        [DataMember(Name = "testAttemptCount")]
        public int TestAttemptCount { get; set; }

        /// <summary>
        /// Number of assignments in course
        /// </summary>
        [DataMember(Name = "assignmentsInCourse")]
        public int? AssignmentsInCourse { get; set; }

        /// <summary>
        /// Number of assignments marked as completed
        /// </summary>
        [DataMember(Name = "assignmentsCompleted")]
        public int? AssignmentsCompleted { get; set; }

        /// <summary>
        /// Number of assignments marked (passed or failed)
        /// </summary>
        [DataMember(Name = "assignmentsMarked")]
        public int? AssignmentsMarked { get; set; }

        /// <summary>
        /// Position the user is at in the course
        /// </summary>
        [DataMember(Name = "navigationPercentage")]
        public int NavigationPercentage { get; set; }

        // If historical, this entry is just a placeholder record for a previously completed
        // training program from an external system. We will not have detailed course transcript
        // information for any historical training programs. 
        [DataMember(Name = "isHistorical")]
        public bool IsHistorical { get; set; }

        /// <summary>
        /// For training programs only. 
        /// This comes from the trainingProgramRollup table where the completed date of the training 
        /// program will be recorded.
        /// </summary>
        [DataMember(Name = "dateCompleted")]
        public DateTime DateCompleted { get; set; }

        [DataMember(Name = "isTrainingProgramRollupComplete")]
        public bool IsTrainingProgramRollupComplete { get; set; }

        /// <summary>
        /// For courses only - this is the first date the course switched from being incomplete
        /// to complete. This represents the first date the user got to the end of the course
        /// does not matter if they passed or failed. 
        /// </summary>
        [DataMember(Name = "FirstCompletionDate")]
        public DateTime? FirstCompletionDate { get; set; }

        #region versioninginfo
        /// <summary>
        /// The version of symphony.core that the user was using during plaback
        /// </summary>
        [DataMember(Name = "plabackCoreVersion")]
        public string PlaybackCoreVersion { get; set; }
        /// <summary>
        /// The version of core that was delivered in the course
        /// (the version of core that the JS came from)
        /// </summary>
        [DataMember(Name = "deliveredCoreVersion")]
        public string DeliveredCoreVersion { get; set; }
        /// <summary>
        /// The version id of the online course delivered to the student
        /// </summary>
        [DataMember(Name = "deliveredOnlineCourseVersion")]
        public int DeliveredOnlineCourseVersion { get; set; }
        /// <summary>
        /// The artisan course id delivered to the student
        /// </summary>
        [DataMember(Name = "deliveredArtisanCourseID")]
        public int DeliveredArtisanCourseID { get; set; }
        /// <summary>
        /// Current core version used for the latest version of this course
        /// </summary>
        [DataMember(Name = "currentCoreVersion")]
        public string CurrentCoreVersion { get; set; }
        /// <summary>
        /// Current version of the course
        /// </summary>
        [DataMember(Name = "currentCourseVersion")]
        public int CurrentCourseVersion { get; set; }
        /// <summary>
        /// Current artisan course id
        /// </summary>
        [DataMember(Name = "currentArtisanCourseID")]
        public int CurrentArtisanCourseID { get; set; }
        /// <summary>
        /// Current version publish date
        /// </summary>
        [DataMember(Name = "currentArtisanCourseCreatedOn")]
        public DateTime CurrentArtisanCourseCreatedOn { get; set; }
        /// <summary>
        /// Delivered version publish date
        /// </summary>
        [DataMember(Name = "deliveredArtisanCourseCreatedOn")]
        public DateTime DeliveredArtisanCourseCreatedOn { get; set; }
        #endregion
    }
}
