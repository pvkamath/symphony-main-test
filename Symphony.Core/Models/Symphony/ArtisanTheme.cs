﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using System.Web;
using System.IO;

namespace Symphony.Core.Models
{
    [DataContract(Name="artisanTheme")]
    public class ArtisanTheme : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "cssPath")]
        public string CssPath { get; set; }

        [DataMember(Name = "folder")]
        public string Folder { get; set; }

        [DataMember(Name = "skinName")]
        public string SkinName { get; set; }

        [DataMember(Name = "skinCssPath")]
        public string SkinCssPath { 
            get 
            {
                return Folder + "/skins/" + SkinName + "/skin.css";
            }
            set { }
        }

        [DataMember(Name = "isDynamic")]
        public bool IsDynamic { get; set; }

        [DataMember(Name = "scssVariablesTemplatePath")]
        public string ScssVariablesTemplatePath { get; set; }

        public DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Gets the path on disk for the current theme; uses the CSS path if the folder itself isn't specified.
        /// </summary>
        /// <returns></returns>
        internal string GetFolder(string webRoot = null)
        {
            string path = this.CssPath;
            if (!string.IsNullOrEmpty(Folder))
            {
                path = Folder;
            }

            return MapPath(path, webRoot);
        }

        internal string GetScssPath(string webRoot = null)
        {
            if (!IsDynamic)
            {
                return string.Empty;
            }

            return Path.Combine(GetFolder(webRoot), GetSkinFolder(), "main.scss");
        }

        internal string GetScssVariablesTemplatePath(string webRoot = null)
        {
            if (string.IsNullOrWhiteSpace(this.ScssVariablesTemplatePath))
            {
                return string.Empty;
            }

            return MapPath(this.ScssVariablesTemplatePath, webRoot);
        }

        // used for CSS include in package index.html
        internal string GetSkinFolder()
        {
            if (String.IsNullOrEmpty(SkinName))
            {
                return null;
            }
            return "skins/" + SkinName;
        }

        private string MapPath(string path, string webRoot = null)
        {
            if (!string.IsNullOrWhiteSpace(webRoot) || HttpContext.Current == null)
            {
                if (path.StartsWith("/"))
                {
                    path = path.Substring(1);
                }
                return Path.Combine(webRoot, path);
            }

            return HttpContext.Current.Server.MapPath(path);
        }
    }
}
