﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="trainingProgramBundle")]
    public class TrainingProgramBundle : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "sku")]
        public string Sku { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "categoryId")]
        public int CategoryID { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "trainingPrograms")]
        public List<TrainingProgram> TrainingPrograms { get; set; }

        [DataMember(Name = "subDomain")]
        public string SubDomain { get; set; }

        [DataMember(Name = "categoryName")]
        public string CategoryName { get; set; }

        [DataMember(Name = "levelIndentText")]
        public string LevelIndentText { get; set; }

        [DataMember(Name = "cost")]
        public decimal Cost { get; set; }

        [DataMember(Name = "listPrice")]
        public decimal ListPrice { get; set; }

        [DataMember(Name = "contentType")]
        public string ContentType { get; set; }

        [DataMember(Name = "deliveryType")]
        public string DeliveryType { get; set; }

        [DataMember(Name = "deliveryMethod")]
        public string DeliveryMethod { get; set; }

        [DataMember(Name = "hours")]
        public int Hours { get; set; }

    }
}

