﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Models
{
    [DataContract(Name = "customer")]
    [DefaultSort(ColumnName = "Name")]
    public class Customer : Model, IAssociatedImage
    {
        [DataMember(Name = "associatedImageData")]
        public string AssociatedImageData { get; set; }

        [DataMember(Name = "id")]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the id of the theme that the customer is using.
        /// </summary>
        [DataMember(Name = "themeId")]
        public int? ThemeID { get; set; }

        [DataMember(Name = "theme")]
        public Theme Theme { get; set; }

        [DataMember(Name = "customTheme")]
        public Theme CustomTheme { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "modules")]
        public string Modules { get; set; }

        [DataMember(Name = "loginRedirect")]
        public string LoginRedirect { get; set; }

        [DataMember(Name = "locationId")]
        public int LocationID { get; set; }

        [DataMember(Name = "customerLocation")]
        public string customerLocation { get; set; }
        
        [DataMember(Name = "organizerSeats")]
        public int OrganizerSeats { get; set; }

        [DataMember(Name = "newHireTransitionPeriod")]
        public int? NewHireTransitionPeriod { get; set; }

        // used to load, don't serialize it though
        public int ParentCustomerID { get; set; }

        [DataMember(Name = "enforcePasswordReset")]
        public bool EnforcePasswordReset { get; set; }

        [DataMember(Name = "enforcePasswordDays")]
        public int EnforcePasswordDays { get; set; }

        [DataMember(Name = "allowSelfAccountCreation")]
        public bool AllowSelfAccountCreation { get; set; }

        [DataMember(Name = "parentId")]
        public int ParentID
        {
            get { return ParentCustomerID; }
            set { ParentCustomerID = value; }
        }

        [DataMember(Name = "canUsersChangePassword")]
        public bool CanUsersChangePassword { get; set; }

        [DataMember(Name = "salesChannelId")]
        public int SalesChannelID { get; set; }

        [DataMember(Name = "salesChannelName")]
        public string SalesChannelName { get; set; }

        [DataMember(Name = "evaluationIndicator")]
        public bool EvaluationIndicator { get; set; }

        [DataMember(Name = "evaluationEndDate")]
        public DateTime? EvaluationEndDate { get; set; }

        [DataMember(Name = "suspendedIndicator")]
        public bool SuspendedIndicator { get; set; }

        [DataMember(Name = "userLimit")]
        public int UserLimit { get; set; }

        [DataMember(Name = "selfRegistrationIndicator")]
        public bool SelfRegistrationIndicator { get; set; }

        [DataMember(Name = "subdomain")]
        public string SubDomain { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "quickQueryIndicator")]
        public bool QuickQueryIndicator { get; set; }

        [DataMember(Name = "reportBuilderIndicator")]
        public bool ReportBuilderIndicator { get; set; }

        [DataMember(Name = "usernameAlias")]
        public string UsernameAlias { get; set; }

        [DataMember(Name = "audienceAlias")]
        public string AudienceAlias { get; set; }

        [DataMember(Name = "jobRoleAlias")]
        public string JobRoleAlias { get; set; }

        [DataMember(Name = "locationAlias")]
        public string LocationAlias { get; set; }

        [DataMember(Name = "courseAlias")]
        public string CourseAlias { get; set; }

        [DataMember(Name = "newHireDuration")]
        public int NewHireDuration { get; set; }

        [DataMember(Name = "timeZone")]
        public string TimeZone { get; set; }

        [DataMember(Name = "ssoEnabled")]
        public bool SsoEnabled { get; set; }

        [DataMember(Name = "ssoType")]
        public int? SsoType { get; set; }

        [DataMember(Name = "ssoLoginUiType")]
        public int? SsoLoginUiType { get; set; }

        [DataMember(Name = "certificateFileName")]
        public string CertificateFileName { get; internal set; }

        [DataMember(Name = "identityProviderUrl")]
        public string IdentityProviderUrl { get; set; }

        [DataMember(Name = "ssoTimeoutRedirect")]
        public string SSOTimeoutRedirect { get; set; }

        [DataMember(Name = "ssoTimeoutMessage")]
        public string SSOTimeoutMessage { get; set; }

        [DataMember(Name = "hasPasswordMask")]
        public bool HasPasswordMask { get; set; }

        [DataMember(Name = "customerCareRep")]
        public int CustomerCareRep { get; set; }

        [DataMember(Name = "accountExecutive")]
        public int AccountExecutive { get; set; }

        [DataMember(Name = "customerCareRepFullName")]
        public string CustomerCareRepFullName { get; set; }

        [DataMember(Name = "accountExecutiveFullName")]
        public string AccountExecutiveFullName { get; set; }

        [DataMember(Name = "retries")]
        public int? Retries { get; set; }

        [DataMember(Name = "maxCourseWork")]
        public int? MaxCourseWork { get; set; }

        [DataMember(Name = "helpUrl")]
        public string HelpUrl { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "isExternalSystemLoginEnabled")]
        public bool IsExternalSystemLoginEnabled { get; set; }

        [DataMember(Name = "externalSystems")]
        public List<ExternalSystem> ExternalSystems { get; set; }

        [DataMember(Name = "redirectType")]
        public int? RedirectType { get; set; }

        [DataMember(Name = "optInMessage")]
        public string OptInMessage { get; set; }

        [DataMember(Name = "optInTitle")]
        public string OptInTitle { get; set; }

        [DataMember(Name = "optInConfirmText")]
        public string OptInConfirmText { get; set; }

        [DataMember(Name = "optInPreviewText")]
        public string OptInPreviewText { get; set; }

        [DataMember(Name = "optInCancelText")]
        public string OptInCancelText { get; set; }

        [DataMember(Name = "defaultAuthority")]
        public string DefaultAuthority { get; set; }

        [DataMember(Name = "defaultProtocol")]
        public string DefaultProtocol { get; set; }

        [DataMember(Name = "certificateEnabledOverride")]
        public bool? CertificateEnabledOverride { get; set; }

        [DataMember(Name = "passingScoreOverride")]
        public int? PassingScoreOverride { get; set; }

        [DataMember(Name = "maxSignInAttempts")]
        public int? MaxSignInAttempts { get; set; }

        [DataMember(Name = "lockedOutPeriodInMinutes")]
        public int? LockedOutPeriodInMinutes { get; set; }

        [DataMember(Name = "lockedOutMessage")]
        public string LockedOutMessage { get; set; }

        #region salesforce

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "fax")]
        public string Fax { get; set; }

        [DataMember(Name = "salesforceContactId")]
        public string SalesforceContactId { get; set; }

        [DataMember(Name = "salesforceAccountId")]
        public string SalesforceAccountId { get; set; }

        [DataMember(Name = "salesforceAddress")]
        public string SalesforceAddress { get; set; }

        [DataMember(Name = "salesforceIsEnabled")]
        public bool SalesforceIsEnabled { get; set; }

        [DataMember(Name = "isSalesforceCapable")]
        public bool IsSalesforceCapable { get; set; }

        [DataMember(Name = "isCreatedBySalesforce")]
        public bool IsCreatedBySalesforce { get; set; }

        [DataMember(Name = "portalSettings")]
        public List<PortalSetting> PortalSettings { get; set; }

        #endregion
    }
}
