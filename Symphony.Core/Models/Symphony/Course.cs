﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Extensions;
using Symphony.Core.Controllers;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Models
{
    [DataContract(Name="course")]
    public class Course : CourseRecord, IAssociatedImage
    {
        [DataMember(Name = "associatedImageData")]
        public string AssociatedImageData { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "internalCode")]
        public string InternalCode { get; set; }

        [DataMember(Name = "courseObjective")]
        public string CourseObjective { get; set; }

        [DataMember(Name = "score")]
        public string Score { get; set; }

        /// <summary>
        /// The student has met the requirements of the course
        /// </summary>
        [DataMember(Name = "passed")]
        public int? Passed { get; set; }

        /// <summary>
        /// The student does not have any outstanding assignments or surveys to complete for this course
        /// </summary>
        [DataMember(Name = "completed")]
        public bool Completed { get; set; }

        [DataMember(Name = "hasClasses")]
        public bool HasClasses { get; set; }

        /// <summary>
        /// This is the date the classroom course began; it's null for online courses
        /// </summary>
        [DataMember(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// This is the date the classroom course ended; it's null for online courses
        /// </summary>
        [DataMember(Name = "endDate")]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// This is the date the online course was attempted; it's null for classroom courses
        /// </summary>
        [DataMember(Name = "attemptDate")]
        public DateTime? AttemptDate { get; set; }

        /// <summary>
        /// The number of attempts this user has made
        /// </summary>
        [DataMember(Name = "attemptCount")]
        public int AttemptCount { get; set; }
        /// <summary>
        /// Amount of time the user has spent in the course
        /// </summary>
        [DataMember(Name = "attemptTime")]
        public long AttemptTime { get; set; }

        /// <summary>
        /// Number of times the test for this course has been taken
        /// </summary>
        [DataMember(Name = "testAttemptCount")]
        public int TestAttemptCount { get; set; }

        /// <summary>
        /// The class registration id, 0 for online courses
        /// </summary>
        [DataMember(Name = "registrationId")]
        public int RegistrationID { get; set; }

        /// <summary>
        /// The class registration status, 0 for online courses
        /// </summary>
        [DataMember(Name = "registrationStatusId")]
        public int RegistrationStatusID { get; set; }

        /// <summary>
        /// The class registration attended indicator
        /// </summary>
        [DataMember(Name = "attendedIndicator")]
        public bool AttendedIndicator { get; set; }

        /// <summary>
        /// The class id, 0 for online courses
        /// </summary>
        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        /// <summary>
        /// The amount of credit for this online or classroom course
        /// </summary>
        [DataMember(Name = "credit")]
        public decimal Credit { get; set; }

        /// <summary>
        /// Not currently used by anything, just stored.
        /// </summary>
        [DataMember(Name = "perStudentFee")]
        public decimal PerStudentFee { get; set; }

        [DataMember(Name = "numberOfDays")]
        public int NumberOfDays { get; set; }

        [DataMember(Name = "dailyDuration")]
        public int DailyDuration { get; set; }

        [DataMember(Name = "courseCompletionTypeId")]
        public int CourseCompletionTypeID { get; set; }

        [DataMember(Name = "courseCompletionTypeDescription")]
        public string CourseCompletionTypeDescription { get; set; }

        /// <summary>
        /// For classroom courses with an "Online Test" completion type, the online course from which to draw scores
        /// </summary>
        [DataMember(Name = "onlineCourseId")]
        public int OnlineCourseID { get; set; }

        [DataMember(Name = "contentTypeId")]
        public int ContentTypeId { get; set; }

        /// <summary>
        /// If this is an online course and we have user data
        /// then this will point to the onlineCourseRollupID that
        /// contains their score. 
        /// </summary>
        [DataMember(Name = "onlineCourseRollupId")]
        public int OnlineCourseRollupID { get; set; }

        /// <summary>
        /// Used in classroom when displaying the selected online course so we avoid an extra lookup
        /// </summary>
        [DataMember(Name = "onlineCourseName")]
        public string OnlineCourseName { get; set; }

        /// <summary>
        /// the online course to use as a survey for this course
        /// </summary>
        [DataMember(Name = "surveyId")]
        public int SurveyID { get; set; }

        /// <summary>
        /// Is the survey mandatory before certificate is given
        /// </summary>
        [DataMember(Name = "isSurveyMandatory")]
        public bool IsSurveyMandatory { get; set; }

        /// <summary>
        /// Name of the survey
        /// </summary>
        [DataMember(Name = "surveyName")]
        public string SurveyName { get; set; }

        /// <summary>
        /// Has the survey attached to the course been taken yet
        /// </summary>
        [DataMember(Name = "surveyTaken")]
        public bool SurveyTaken { get; set; }

        [DataMember(Name = "presentationTypeId")]
        public int PresentationTypeID { get; set; }

        [DataMember(Name = "presentationTypeDescription")]
        public string PresentationTypeDescription { get; set; }

        [DataMember(Name = "publicIndicator")]
        public bool PublicIndicator { get; set; }

        [DataMember(Name = "hasEnded")]
        public bool HasEnded
        {
            get { return EndDate.HasValue && DateTime.Now.ToUniversalTime() > EndDate.Value.WithUtcFlag(); }
            private set { return; }
        }

        [DataMember(Name = "hasStarted")]
        public bool HasStarted
        {
            get { return StartDate.HasValue && DateTime.Now.ToUniversalTime() > StartDate.Value.WithUtcFlag(); }
            private set { return; }
        }

        [DataMember(Name = "isStartingSoon")]
        public bool IsStartingSoon
        {
            get { 
                int i;
                if (!int.TryParse(ConfigurationManager.AppSettings["startingSoonMinutes"], out i))
                {
                    i = 15;
                }
                return StartDate.HasValue && DateTime.Now.ToUniversalTime().AddMinutes(i) > StartDate.Value.WithUtcFlag(); 
            }
            private set { return; }
        }
        
        /// <summary>
        /// When to allow re-testing
        /// </summary>
        [DataMember(Name = "retestMode")]
        public int RetestMode { get; set; }

        /// <summary>
        /// The number of allowed retakes
        /// </summary>
        [DataMember(Name = "retries")]
        public int? Retries { get; set; }

        [DataMember(Name = "cost")]
        public decimal Cost { get; set; }

        [DataMember(Name = "keywords")]
        public string Keywords { get; set; }

        [DataMember(Name = "isTest")]
        public bool IsTest { get; set; }

        [DataMember(Name = "isSurvey")]
        public bool IsSurvey { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "modifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [DataMember(Name = "version")]
        public int Version { get; set; }

        [DataMember(Name = "webinarRegistrationId")]
        public string WebinarRegistrationID { get; set; }

        [DataMember(Name = "webinarKey")]
        public string WebinarKey { get; set; }

        [DataMember(Name = "webinarLaunchUrl")]
        public string WebinarLaunchURL
        {
            get { return Model.GTWLaunchUrl(WebinarKey, WebinarRegistrationID); }
            set { }
        }

        [DataMember(Name = "categoryId")]
        public int CategoryId { get; set; }

        [DataMember(Name = "categoryName")]
        public string CategoryName { get; set; }

        [DataMember(Name = "externalId")]
        public string ExternalID { get; set; }

        [DataMember(Name = "certificateTemplateId")]
        public int? CertificateTemplateId { get; set; }

        [DataMember(Name = "certificatePath")]
        public string CertificatePath { get; set; }

        // Certificate key value pairs
        [DataMember(Name = "metaDataJson")]
        public string MetaDataJson { get; set; }

        // overrides in the course management area
        [DataMember(Name = "showPretestOverride")]
        public bool? ShowPretestOverride { get; set; }

        [DataMember(Name = "showPosttestOverride")]
        public bool? ShowPostTestOverride { get; set; }

        [DataMember(Name = "passingScoreOverride")]
        public int? PassingScoreOverride { get; set; }

        [DataMember(Name = "skinOverride")]
        public int? SkinOverride { get; set; }

        [DataMember(Name = "pretestTypeOverride")]
        public int? PretestTypeOverride { get; set; }

        [DataMember(Name = "posttestTypeOverride")]
        public int? PosttestTypeOverride { get; set; }

        [DataMember(Name = "navigationMethodOverride")]
        public int? NavigationMethodOverride { get; set; }

        [DataMember(Name = "bookmarkingMethodOverride")]
        public int? BookmarkingMethodOverride { get; set; }

        [DataMember(Name = "certificateEnabled")]
        public bool? CertificateEnabled { get; set; }

        [DataMember(Name = "certificateEnabledOverride")]
        public bool? CertificateEnabledOverride { get; set; }

        [DataMember(Name = "disclaimerOverride")]
        public string DisclaimerOverride { get; set; }

        [DataMember(Name = "contactEmailOverride")]
        public string ContactEmailOverride { get; set; }

        [DataMember(Name = "showObjectivesOverride")]
        public bool? ShowObjectivesOverride { get; set; }

        [DataMember(Name = "randomizeAnswersOverride")]
        public bool? RandomizeAnswersOverride { get; set; }
        
        [DataMember(Name = "randomizeQuestionsOverride")]
        public bool? RandomizeQuestionsOverride { get; set; }

        [DataMember(Name = "reviewMethodOverride")]
        public int? ReviewMethodOverride { get; set; }

        [DataMember(Name = "markingMethodOverride")]
        public int? MarkingMethodOverride { get; set; }

        [DataMember(Name = "inactivityMethodOverride")]
        public int? InactivityMethodOverride { get; set; }

        [DataMember(Name = "inactivityTimeoutOverride")]
        public int? InactivityTimeoutOverride { get; set; }

        [DataMember(Name = "isForceLogoutOverride")]
        public bool? IsForceLogoutOverride { get; set; }

        [DataMember(Name = "minimumPageTimeOverride")]
        public int? MinimumPageTimeOverride { get; set; }

        [DataMember(Name = "maximumQuestionTimeOverride")]
        public int? MaximumQuestionTimeOverride { get; set; }

        [DataMember(Name = "maxTimeOverride")]
        public int? MaxTimeOverride { get; set; }

        [DataMember(Name = "minTimeOverride")]
        public int? MinTimeOverride { get; set; }

        [DataMember(Name = "minLoTimeOverride")]
        public int? MinLoTimeOverride { get; set; }

        [DataMember(Name = "minScoTimeOverride")]
        public int? MinScoTimeOverride { get; set; }

        [DataMember(Name = "theme")]
        public ArtisanTheme Theme { get; set; }

        [DataMember(Name = "pretestTestOutOverride")]
        public bool? PretestTestOutOverride { get; set; }

        [DataMember(Name = "artisanCourseId")]
        public int? ArtisanCourseID { get; set; }

        [DataMember(Name = "publishedArtisanCourseId")]
        public int? PublishedArtisanCourseID { get; set; }

        [DataMember(Name = "messageBoardId")]
        public int MessageBoardID { get; set; }

        [DataMember(Name = "proctorRequiredIndicator")]
        public bool? ProctorRequiredIndicator { get; set; }

        [DataMember(Name = "themeFlavorOverrideId")]
        public int? ThemeFlavorOverrideID { get; set; }

        [DataMember(Name = "themeFlavor")]
        public Theme ThemeFlavor { get; set; }

        [DataMember(Name = "parameters")]
        public string Parameters { get; set; }

        [DataMember(Name = "onlineCourseParameterOverrides")]
        public List<OnlineCourseParameterOverride> OnlineCourseParameterOverrides { get; set; }

        [DataMember(Name = "authors")]
        public List<Author> Authors { get; set; }

        [DataMember(Name = "secondaryCategories")]
        public List<Category> SecondaryCategories { get; set; }

        [DataMember(Name = "accreditations")]
        public List<OnlineCourseAccreditation> Accreditations { get; set; }

        [DataMember(Name = "hasCourseParameters")]
        public bool HasCourseParameters { get; set; }

        public RusticiSoftware.ScormContentPlayer.Logic.ImportResult ImportResult { get; set; }

        [DataMember(Name = "isCreateMessageBoard")]
        public bool IsCreateMessageBoard { get; set; }

        [DataMember(Name = "isDisableTopicCreation")]
        public bool IsDisableTopicCreation { get; set; }

        [DataMember(Name = "messageBoardName")]
        public string MessageBoardName { get; set; }

        [DataMember(Name = "canMoveForwardIndicator")]
        public bool CanMoveForwardIndicator { get; set; }

        [DataMember(Name = "levelIndentText")]
        public string LevelIndentText { get; set; }

        [DataMember(Name = "isValidationEnabled")]
        public bool IsValidationEnabled { get; set; }

        [DataMember(Name = "validationInterval")]
        public int? ValidationInterval { get; set; }

        [DataMember(Name = "isUserValidationAllowed")]
        public bool IsUserValidationAllowed { get; set; }

        [DataMember(Name = "isCourseWorkAllowed")]
        public bool IsCourseWorkAllowed { get; set; }

        [DataMember(Name = "enforceCanMoveForwardIndicator")]
        public bool? EnforceCanMoveForwardIndicator { get; set; }

        [DataMember(Name = "affidavitId")]
        public int AffidavitID { get; set; }

        /// <summary>
        /// Is an assignment required for this course
        ///     ** Note that this is only true if there is an assignment and it is not marked or the student did not pass
        ///     If a course has an assignment, and the user has completed it successfully this will be false.
        /// </summary>
        [DataMember(Name = "isAssignmentRequired")]
        public bool IsAssignmentRequired { get; set; }

        /// <summary>
        /// In the case where an assignment is required, this will be true if the assignment has been marked, meaning the user
        /// did not score high enough to complete the assignment. 
        /// If false this means the instructor has not yet looked at the assignment. 
        /// </summary>
        [DataMember(Name = "isAssignmentMarked")]
        public bool IsAssignmentMarked { get; set; }

        /// <summary>
        /// Will be true if this course contains any assignments
        /// </summary>
        [DataMember(Name = "hasAssignments")]
        public bool HasAssignments { get; set; }

        /// <summary>
        /// Number of assignments in course
        /// </summary>
        [DataMember(Name = "assignmentsInCourse")]
        public int? AssignmentsInCourse { get; set; }

        /// <summary>
        /// Number of assignments marked as completed
        /// </summary>
        [DataMember(Name = "assignmentsCompleted")]
        public int? AssignmentsCompleted { get; set; }

        /// <summary>
        /// Number of assignments marked (passed or failed)
        /// </summary>
        [DataMember(Name = "assignmentsMarked")]
        public int? AssignmentsMarked { get; set; }

        /// <summary>
        /// Flag to indicate whether the user has started the triaining program or not
        /// </summary>
        [DataMember(Name = "hasTrainingProgramStarted")]
        public bool HasTrainingProgramStarted { get; set; }

        /// <summary>
        /// For online courses - the estimated number of hours it will take
        /// for a student to complete the course work.
        /// </summary>
        [DataMember(Name = "duration")]
        public int Duration { get; set; }

        /// <summary>
        /// The automatic duration calculated using the min timer values in the artisan course
        /// </summary>
        [DataMember(Name = "automaticDuration")]
        public int AutomaticDuration { get; set; }

        /// <summary>
        /// Toggle to use the automatic duration calculated by the artisan course
        /// </summary>
        [DataMember(Name = "isUseAutomaticDuration")]
        public bool IsUseAutomaticDuration { get; set; }

        [DataMember(Name = "navigationPercentage")]
        public int NavigationPercentage { get; set; }

        [DataMember(Name = "navigationPercentageDisplay")]
        public string NavigationPercentageDisplay
        {
            get
            {
                if (Completed && Passed.HasValue && Passed.Value == 0)
                {
                    // completed, but not passed
                    return "";
                }
                if (Completed)
                {
                    return "100%";
                }
                else if (NavigationPercentage == 0)
                {
                    return "";
                }

                return NavigationPercentage + "%";
            }
            private set { }
        }

        /// <summary>
        /// The user id that was used to load any transcript information (Score, Attempts, Assignment Info, Version Info, Registration info....)
        /// </summary>
        [DataMember(Name = "transcriptUserId")]
        public int TranscriptUserID
        {
            get;
            set; 
        }

        [DataMember(Name = "isSurveyRequired")]
        public bool IsSurveyRequried
        {
            get
            {
                return (SurveyID > 0 && IsSurveyMandatory && !SurveyTaken);
            }
            internal set { }
        }

        /// <summary>
        /// This is the first date the course switched from being incomplete
        /// to complete. This represents the first date the user got to the end of the course
        /// does not matter if they passed or failed. 
        /// </summary>
        [DataMember(Name = "firstCompletionDate")]
        public DateTime? FirstCompletionDate { get; set; }

        /// <summary>
        /// This is the firstCompletionDate for the previous course if this course is
        /// loaded by a training proram. 
        /// </summary>
        [DataMember(Name = "previousCourseFirstCompletionDate")]
        public DateTime? PreviousCourseFirstCompletionDate { get; set; }


        #region versioninginfo
        /// <summary>
        /// The version of symphony.core that the user was using during plaback
        /// </summary>
        [DataMember(Name = "playbackCoreVersion")]
        public string PlaybackCoreVersion { get; set; }
        /// <summary>
        /// The version of core that was delivered in the course
        /// (the version of core that the JS came from)
        /// </summary>
        [DataMember(Name = "deliveredCoreVersion")]
        public string DeliveredCoreVersion { get; set; }
        /// <summary>
        /// The version id of the online course delivered to the student
        /// </summary>
        [DataMember(Name = "deliveredOnlineCourseVersion")]
        public int DeliveredOnlineCourseVersion { get; set; }
        /// <summary>
        /// The artisan course id delivered to the student
        /// </summary>
        [DataMember(Name = "deliveredArtisanCourseID")]
        public int DeliveredArtisanCourseID { get; set; }
        /// <summary>
        /// The date the artisan course delivered to the student was created
        /// </summary>
        [DataMember(Name = "deliveredArtisanCourseCreatedOn")]
        public DateTime DeliveredArtisanCourseCreatedOn { get; set; }
        /// <summary>
        /// Current core version used for the latest version of this course
        /// </summary>
        [DataMember(Name = "currentCoreVersion")]
        public string CurrentCoreVersion { get; set; }
        /// <summary>
        /// Current version of the course
        /// </summary>
        [DataMember(Name = "currentCourseVersion")]
        public int CurrentCourseVersion { get; set; }
        /// <summary>
        /// Current artisan course id
        /// </summary>
        [DataMember(Name = "currentArtisanCourseID")]
        public int CurrentArtisanCourseID { get; set; }
        /// <summary>
        /// The date the current artisan course created on
        /// </summary>
        [DataMember(Name = "currentArtisanCourseCreatedOn")]
        public DateTime CurrentArtisanCourseCreatedOn { get; set; }
        /// <summary>
        /// Current version of symphony
        /// </summary>
        [DataMember(Name = "symphonyCoreVersion")]
        public string SymphonyCoreVersion
        {
            get
            {
                return new System.Reflection.AssemblyName(System.Reflection.Assembly.GetExecutingAssembly().FullName).Version.ToString();
            }
            internal set { }
        }

        #endregion

        #region Display
        /// <summary>
        /// The training program context this course will be displayed as (if it is displayed in the context of a training program)
        /// </summary>
        [IgnoreDataMember]
        public TrainingProgram TrainingProgram { get; set; }
        /// <summary>
        /// The customer context this course will be displayed as
        /// </summary>
        [IgnoreDataMember]
        public Data.Customer Customer { get; set; }

        [DataMember(Name = "isDisplayAssignmentLaunchLink")]
        public bool IsDisplayAssignmentLaunchLink
        {
            get
            {
                Version deliveredVersion;
                System.Version.TryParse(DeliveredCoreVersion, out deliveredVersion);

                if (deliveredVersion != null && deliveredVersion.Minor > 2733)
                {
                    return true;
                }

                return false;
            }
            internal set
            {

            }
        }
        [DataMember(Name = "displayAssignmentsIcon")]
        public DisplayIconState DisplayAssignmentsIcon
        {
            get
            {
                if (HasAssignments)
                {
                    int trainingProgramId = TrainingProgram != null ? TrainingProgram.Id : 0;
                    int loggedInUserId = SymphonyController.GetUserID();

                    // if a transcriptUserId is set, then we need to use the transcriptUserID instead 
                    // of the logged in user id. This is becuase an instructor or a student could be 
                    // viewing the assignments. 
                    int assignmentUserId = TranscriptUserID > 0 && loggedInUserId != TranscriptUserID ?
                        TranscriptUserID :
                        loggedInUserId;

                    return new DisplayIconState()
                    {
                        Icon = IconState.Assignments,
                        Messages = new List<Text>() { Text.Course_Assignments_View },
                        Link = new DisplayLink()
                        {
                            BaseUrl = Url.LaunchAssignments,
                            ClassFormat = TextFormat.ViewAssignmentsClassFormat,
                            UrlFormat = TextFormat.ViewAssignmentsUrlFormat,
                            QueryParams = new KeyValueList()
                            {
                                new DisplayKeyValue(){ Key = "UserID", Value = assignmentUserId.ToString() },
                                new DisplayKeyValue(){ Key = "CourseID", Value = Id.ToString()},
                                new DisplayKeyValue(){ Key = "TrainingProgramID", Value = trainingProgramId.ToString()}
                            }

                        }
                    };
                }

                return new DisplayIconState()
                {
                    Icon = IconState.Empty,
                    Link = new DisplayLink()
                };
            }
            internal set { }
        }

        [DataMember(Name = "displayScore")]
        public DisplayModel DisplayScore
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }
                
                DisplayModel displayModel = new DisplayModel();

                if (string.IsNullOrWhiteSpace(Score))
                {
                    displayModel.Messages.Add(Text.Dash);
                    displayModel.Messages.Add(Text.Course_Unattempted);
                } else if (CourseTypeID == (int)CourseType.Classroom &&
                    OnlineCourseID > 0 &&
                    !AttendedIndicator)
                {
                    displayModel.Messages.Add(Text.Dash);
                    displayModel.Messages.Add(Text.Course_Unattended);
                }
                else
                { 
                    Text assignmentStatus = DisplayAssignmentStatus.Messages.FirstOrDefault();
                    
                    if (assignmentStatus == Text.Course_AssignmentsUnmarked) {
                        displayModel.Messages.Add(Text.Dash);
                        displayModel.Messages.Add(Text.Course_ScoreRecorded_AssignmentsUnmarked);
                    } else if (assignmentStatus == Text.Course_AssignmentsUnattempted) {
                        displayModel.Messages.Add(Text.Dash);
                        displayModel.Messages.Add(Text.Course_ScoreRecorded_AssignmentsUnattempted);
                    } else if (assignmentStatus == Text.Course_AssignmentsFailed) {
                        displayModel.Messages.Add(Text.Dash);
                        displayModel.Messages.Add(Text.Course_ScoreRecorded_AssignmentsFailed);
                    }
                    else if (IsSurveyRequried)
                    {
                        displayModel.Messages.Add(Text.Dash);
                        displayModel.Messages.Add(Text.Course_ScoreRecorded_SurveyRequired);
                    }
                    else
                    {
                        displayModel.Messages.Add(Text.Dynamic(Score));
                    }
                }

                return displayModel;
            }
            internal set { }
        }
        
        [DataMember(Name = "displayCertificate")]
        public DisplayIconState DisplayCertificate
        {
            get
            {
                bool? tpCertificateEnabledOverride = null;
                bool? customerCertificateEnabledOverride = null;

                if (this.TrainingProgram != null)
                {
                    tpCertificateEnabledOverride = this.TrainingProgram.CertificateEnabledOverride;
                }

                if (this.Customer != null)
                {
                    customerCertificateEnabledOverride = this.Customer.CertificateEnabledOverride;
                }

                bool? certEnabledOverride = tpCertificateEnabledOverride ?? this.CertificateEnabledOverride ?? customerCertificateEnabledOverride;

                bool? certificateEnabled = certEnabledOverride.HasValue ?
                    certEnabledOverride.Value :
                    CertificateEnabled.HasValue ?
                    CertificateEnabled.Value :
                    true;

                if (IsSurvey)
                {
                    CertificateEnabled = false;
                }

                DisplayIconState state = new DisplayIconState()
                {
                    Icon = IconState.Empty,
                    Link = new DisplayLink()
                };

                bool isUnattendedClassroomCourse =
                    CourseTypeID == (int)CourseType.Classroom &&
                    OnlineCourseID > 0 &&
                    !AttendedIndicator;

                if (!IsSurvey && (string.IsNullOrWhiteSpace(Score) || isUnattendedClassroomCourse))
                {
                    return state;
                }
                
                if (Passed.HasValue && Passed.Value > 0)
                {
                    if (IsSurvey)
                    {
                        state.Icon = IconState.Complete;
                        state.Messages = new List<Text> { Text.Course_SurveyComplete };
                    }
                    else if (certificateEnabled.HasValue && !certificateEnabled.Value ||
                        IsAssignmentRequired ||
                        IsSurveyRequried)
                    {
                        state.Icon = IconState.Empty;
                    }
                    else
                    {
                        CertificateType certificateType = CourseTypeID == (int)CourseType.Classroom ? CertificateType.Classroom : CertificateType.Online;
                        int courseOrClassID = CourseTypeID == (int)CourseType.Classroom ? ClassID : Id;

                        state = DisplayHelpers.GetCertificateBadge(certificateType, TrainingProgram != null ? TrainingProgram.Id : 0, courseOrClassID, TranscriptUserID > 0 ? TranscriptUserID : 0);
                    }
                }

                return state;
            }
            internal set
            {

            }
        }

        [DataMember(Name = "displayAssignmentStatus")]
        public DisplayIconState DisplayAssignmentStatus
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayIconState assignmentStatus = new DisplayIconState();

                if (HasAssignments)
                {

                    if (IsAssignmentRequired && IsAssignmentMarked)
                    {
                        assignmentStatus.Messages.Add(Text.Course_AssignmentsFailed);
                        assignmentStatus.Icon = IconState.Incomplete;
                    }
                    else if (IsAssignmentRequired && !IsAssignmentMarked && AssignmentsInCourse.HasValue)
                    {
                        assignmentStatus.Messages.Add(Text.Course_AssignmentsUnmarked);
                        assignmentStatus.Icon = IconState.InactiveStrong;
                    }
                    else if (string.IsNullOrWhiteSpace(Score) || !AssignmentsInCourse.HasValue)
                    {
                        assignmentStatus.Messages.Add(Text.Course_AssignmentsUnattempted);
                        assignmentStatus.Icon = IconState.Inactive;
                    }
                    else
                    {
                        assignmentStatus.Messages.Add(Text.Course_AssignmentsComplete);
                        assignmentStatus.Icon = IconState.Complete;
                    }
                }
                else
                {
                    assignmentStatus.Messages.Add(Text.NotApplicable);
                    assignmentStatus.Icon = IconState.Empty;
                }

                return assignmentStatus;
            }
            internal set { }
        }

        [DataMember(Name = "displayCourseLink")]
        public DisplayLink DisplayCourseLink
        {
            get
            {
                return GetDisplayLink(null, null, null);
            }
            internal set { }
        }

        [DataMember(Name = "displayAssignmentLaunchText")]
        public DisplayModel DisplayAssignmentLaunchText
        {
            get
            {
                return new DisplayModel()
                {
                    Messages = new List<Text>() { Text.Course_Assignments_Launch }
                };
            }
            internal set { }
        }

        /// <summary>
        /// Returns a course launch link
        /// 
        /// TODO: This is really just coppied from the JS implementation
        /// This is messy because we assume there could be information about
        /// the training program. If we detect a training program then we have
        /// to convert the course in to a training program course... it would be 
        /// nicer to have this function know nothing about the Training Program 
        /// and have an override in TrainingProgramCourse that will add 
        /// the training program details. 
        /// 
        /// 
        /// </summary>
        /// <param name="minDate"></param>
        /// <param name="maxDate"></param>
        /// <returns></returns>
        public virtual DisplayLink GetDisplayLink(DateTime? minDate, DateTime? maxDate, List<Text> unavailableReasons)
        {
            int tpId = TrainingProgram != null ? TrainingProgram.Id : 0;
            bool isOnline = CourseTypeID == (int)CourseType.Online;

            DisplayLink link = DisplayHelpers.GetSimpleCourseLaunchLink((isOnline ? Id : OnlineCourseID), tpId);

            if (unavailableReasons != null && unavailableReasons.Count > 0)
            {
                link.Messages = unavailableReasons;
            }

            long minTicks = minDate.HasValue ? minDate.Value.Ticks : DateTime.UtcNow.Ticks;
            long maxTicks = maxDate.HasValue && maxDate.Value.Ticks > minTicks ? maxDate.Value.Ticks : DateTime.MaxValue.Ticks;

            KeyValueList paramDictionary = new KeyValueList();
            paramDictionary.Add("AffidavitID", AffidavitID.ToString());
            paramDictionary.Add("ProctorRequiredIndicator", ProctorRequiredIndicator.ToString());
            paramDictionary.Add("IsCourseWorkAllowed", IsCourseWorkAllowed.ToString());
            paramDictionary.Add("IsUserValidationAllowed", IsUserValidationAllowed.ToString());
            paramDictionary.Add("IsInactiveSession", (TrainingProgram != null && TrainingProgram.IsUsingSessions && !TrainingProgram.IsSessionActive).ToString());
            paramDictionary.Add("IsCourseComplete", (TrainingProgram != null && TrainingProgram.CourseUnlockModeID == (int)CourseUnlockMode.AfterCourseCompletion && Passed == 1 && !IsAssignmentRequired).ToString());

            if (CourseTypeID == (int)CourseType.Online)
            {
                if (Passed == 1 && RetestMode == (int)Symphony.Core.RetestMode.OnlyOnFailure) {
                    link.Title = Text.Completed;
                    link.BaseUrl = Url.Empty;
                    link.Messages.Add(Text.Course_CompletedNoRetry);
                } else {
                    /// Messy stuff now... Don't like this. 
                    /// We convert this into a training program course to see if we have other details

                    if (TrainingProgram != null && this.GetType() == typeof(TrainingProgramCourse)) {
                        TrainingProgramCourse tpCourse = (TrainingProgramCourse)this;

                        // Should have tp course info now
                        if (tpCourse.IsRelativeDueDate) {
                            if (tpCourse.MinutesUntilExpired <= 0 && tpCourse.DenyAccessAfterDueDateIndicator) {
                                link.Messages.Add(Text.Session_DueDatePassed);
                            }
                        }

                        if (tpCourse.IsRelativeActiveAfter) {
                            bool isSessionTimerActive = tpCourse.ActiveAfterMode == (int)RelativeTimeMode.Session && TrainingProgram.IsUsingSessions && TrainingProgram.IsSessionActive;
                            bool isTrainingProgramTimerActive = tpCourse.ActiveAfterMode != (int)RelativeTimeMode.Session && tpCourse.HasTrainingProgramStarted;
                            bool isPrevCourseEndTimerActive = tpCourse.ActiveAfterMode == (int)RelativeTimeMode.PreviousCourseEnd && tpCourse.PreviousCourseFirstCompletionDate.HasValue;
                            
                            bool isTimerActive = isSessionTimerActive || isTrainingProgramTimerActive || isPrevCourseEndTimerActive;

                            DateTime dateActive = DateTime.UtcNow.AddMinutes(isTimerActive ? tpCourse.MinutesUntilActive : tpCourse.ActiveAfterMinutes.Value);

                            DisplayDate displayDate = new DisplayDate() {
                                Date = dateActive,
                                IsActive = isTimerActive,
                                Format = DateFormat.DateTime
                            };
                            if (dateActive > DateTime.UtcNow)
                            {
                                link.Messages.Add(DateText.Dynamic(DynamicDateText.Course_DateAvailableRelative, displayDate));
                            }
                        } else if (tpCourse.ActiveAfterDate.HasValue && tpCourse.ActiveAfterDate.Value > DateTime.UtcNow) {

                            DisplayDate displayDate = new DisplayDate() {
                                Date = tpCourse.ActiveAfterDate,
                                IsActive = false,
                                Format = DateFormat.Date
                            };

                            if (tpCourse.IsUsingSession) {
                                displayDate.IsActive = true;
                                displayDate.Format = DateFormat.DateTime;
                            } 

                            link.Messages.Add(DateText.Dynamic(DynamicDateText.Course_DateAvailable, displayDate));
                        } else if (tpCourse.DueDate.HasValue && !tpCourse.DueDate.Value.IsDefaultDateTime() && tpCourse.DueDate.Value < DateTime.UtcNow && tpCourse.DenyAccessAfterDueDateIndicator) {
                            link.Messages.Add(Text.Session_DueDatePassed);
                        }

                        if (!TrainingProgram.IsSessionRegistered && TrainingProgram.SessionCourseID > 0) {
                            link.Messages = new List<Text>() {
                                Text.Session_RegistrationRequiredDetail
                            };
                        }
                        
                    } 

                    if (link.Messages.Count == 0) {
                        link.Title = Text.Launch;
                        link.BaseUrl = Url.Launch;
                        link.QueryParams.AddRange(paramDictionary);
                    } else {
                        link.Title = Text.Unavailable;
                        link.BaseUrl = Url.Empty;
                    }
                }
            }
            else
            {
                switch (RegistrationStatusID)
                {
                    default:
                        link = DisplayHelpers.GetRegisterLink(Id, minTicks, maxTicks);
                        break;
                    case (int)RegistrationStatusType.AwaitingApproval:
                        link = DisplayHelpers.GetUnregisterLink(RegistrationID);
                        link.Messages.Add(Text.AwaitingApproval);
                        break;
                    case (int)RegistrationStatusType.WaitList:
                        link.BaseUrl = Url.Empty;
                        link.Title = Text.WaitList;
                        break;
                    case (int)RegistrationStatusType.Denied:
                        link.Messages.Add(Text.Registration_Denied);
                        goto default;
                    case (int)RegistrationStatusType.Registered:
                        if (!HasStarted)
                        {
                            if (!string.IsNullOrWhiteSpace(Score))
                            {
                                link.Title = Text.Dash;
                                link.BaseUrl = Url.Empty;
                            }
                            else
                            {
                                link = DisplayHelpers.GetUnregisterLink(RegistrationID);
                            }
                        }
                        else if (!string.IsNullOrWhiteSpace(WebinarKey) && (IsStartingSoon || HasStarted) && !HasEnded)
                        {
                            link.Title = Text.LaunchWebinar;
                            //link.BaseUrl = Url.Dynamic(WebinarLaunchURL);
                        }
                        else if (HasStarted && !HasEnded)
                        {
                            link.Title = Text.InProgress;
                            link.BaseUrl = Url.Empty;
                        }
                        else if (HasStarted && HasEnded)
                        {
                            // Default
                            link.Title = Text.Dash;
                            link.BaseUrl = Url.Empty;

                            if (Passed == 0)
                            {
                                if (OnlineCourseID > 0)
                                {
                                    if (AttendedIndicator)
                                    {
                                        link.Title = Text.TakeTest;
                                        link.BaseUrl = Url.Launch;
                                        link.QueryParams.AddRange(paramDictionary);
                                    }
                                }
                                else if (SurveyID > 0 && !SurveyTaken)
                                {
                                    link = DisplayHelpers.GetSimpleCourseLaunchLink(SurveyID, tpId);
                                    link.BaseUrl = Url.Launch;
                                    link.Title = Text.TakeSurvey;
                                }
                                else
                                {
                                    link = DisplayHelpers.GetRegisterLink(Id, minTicks, maxTicks);
                                }
                            }
                            else
                            {
                                if (SurveyID > 0 && !SurveyTaken)
                                {
                                    link.Title = Text.TakeSurvey;
                                    link.BaseUrl = Url.Launch;
                                    link.QueryParams.Add("CourseID", SurveyID.ToString());
                                    link.QueryParams.Add("TrainingProgramID", tpId.ToString());
                                }
                            }
                        }
                        else
                        {
                            // Should never get here
                            link.Title = Text.Dynamic("?");
                            link.BaseUrl = Url.Empty;
                        }
                        break;
                }
            }

            return link;
        }

        #endregion
    }
}
