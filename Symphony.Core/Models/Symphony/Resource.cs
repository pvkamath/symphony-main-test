﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "resource")]
    [DefaultSort(ColumnName = "Name")]
    public class Resource : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "cost")]
        public decimal Cost { get; set; }

        [DataMember(Name = "venueId")]
        public int VenueID { get; set; }

        [DataMember(Name = "roomId")]
        public int RoomID { get; set; }
    }
}
