using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportCourse")]
    [DefaultSort(ColumnName = "Name")]
    public class ReportCourse : Model
    {
        [DataMember(Name = "coursekey")]
        public int CourseKey { get; set; }

        [DataMember(Name = "coursekeyType")]
        public int CoursekeyType { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "customerkey")]
        public int CustomerKey { get; set; }

        [DataMember(Name = "thirdPartyIndicator")]
        public Boolean ThirdPartyIndicator { get; set; }

        [DataMember(Name = "courseId")]
        public int CourseId { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerId { get; set; }

    }
}
