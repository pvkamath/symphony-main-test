﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Web;
using System.IO;
using Symphony.Core.Controllers;
using Newtonsoft.Json;
using log4net;

namespace Symphony.Core.Models
{
    [DataContract(Name = "certificate")]
    public class Certificate : Model
    {
        public ILog Log = LogManager.GetLogger(typeof(Certificate));
        public User User { get; set; }

        public string Title { get; set; }
        public string TemplatePath { get; set; }
        public TrainingProgram TrainingProgram { get; set; }
        public Course Course { get; set; }
        public ClassroomClass Class { get; set; }
        public Customer Customer { get; set; }
        public Location CustomerLocation { get; set; }
        public Location UserLocation { get; set; }

        public string Url { get; set; }

        public List<UserDataField> UserDataFields { get; set; }
        public string UserDataFieldsJson
        {
            get
            {
                try
                {
                    return JsonConvert.SerializeObject(UserDataFields);
                }
                catch (Exception e)
                {
                    Log.Error("Could not serialize UserDataFields to JSON", e);
                }
                return "";
            }
        }

        public List<string> LicenseDataFieldLabels { get; set; }
        public List<string> LicenseDataFieldNames { get; set; }

        public List<string> LicenseUserFieldLabels { get; set; }
        public List<string> LicenseUserFieldNames { get; set; }

        public TranscriptEntry TranscriptEntry { get; set; }
        public CertificateType Type { get; set; }
        public CourseType CourseType { get; set; }
        
        public Dictionary<string, string> CourseMeta { get; set; }
        public Dictionary<string, string> ClassMeta { get; set; }
        public Dictionary<string, string> TrainingProgramMeta { get; set; }
        public Dictionary<string, string> LicenseDataMeta { get; set; }
        public Dictionary<string, string> LicenseUserMeta { get; set; }


        public string GetTrainingProgramMeta(string key)
        {
            if (TrainingProgramMeta != null && TrainingProgramMeta.ContainsKey(key))
            {
                return TrainingProgramMeta[key];
            }
            return "";
        }

        public string GetLicenseDataMeta(string key)
        {
            if (LicenseDataMeta != null && LicenseDataMeta.ContainsKey(key))
            {
                return LicenseDataMeta[key];
            }
            return "";
        }

        public string GetLicenseUserMeta(string key)
        {
            if (LicenseUserMeta != null && LicenseUserMeta.ContainsKey(key))
            {
                return LicenseUserMeta[key];
            }
            return "";
        }
        

        public string GetClassMeta(string key) {
            if (ClassMeta.ContainsKey(key))
            {
                return ClassMeta[key];
            }
            return "";
        }

        public string GetCourseMeta(string key)
        {
            if (CourseMeta.ContainsKey(key))
            {
                return CourseMeta[key];
            }
            return "";
        }

        public string FormatDate(DateTime date, string format)
        {
            return date.ToString(format);
        }

        public string FormatDate(DateTime date)
        {
            return FormatDate(date, "M/d/yyyy");
        }

        public string UserFullName
        {
            get
            {
                return User.FirstName + " " + User.LastName;
            }
        }

        public string BaseCustomerCSS
        {
            get
            {
                string url = String.Format("/Certificates/{0}/css/base.css", Customer.SubDomain);
                string path = HttpContext.Current.Server.MapPath(url);
                if (File.Exists(path))
                {
                    return url;
                }
                return "";
            }
        }

        public string CertificateTypeCustomerCSS
        {
            get
            {
                string url = String.Format("/Certificates/{0}/css/{1}.css", Customer.SubDomain, Type.ToString());
                string path = HttpContext.Current.Server.MapPath(url);
                if (File.Exists(path))
                {
                    return url;
                }
                return "";
            }
        }

        public string CertificateTemplateCustomerCSS
        {
            get
            {
                string relativePath = CertificateController.GetRelativeTemplatePath(TemplatePath, Customer.SubDomain);
                
                
                if (!string.IsNullOrEmpty(relativePath)) {
                    string url = Path.Combine("/Certificates/", Customer.SubDomain, "css", relativePath, Type.ToString() + ".css")
                                    .Replace('\\', '/');

                    string path = HttpContext.Current.Server.MapPath(url);
                    if (File.Exists(path))
                    {
                        return url;
                    }
                }

                return "";
            }
        }

        public string BaseCustomerJS
        {
            get
            {
                string url = String.Format("/Certificates/{0}/js/base.js", Customer.SubDomain);
                string path = HttpContext.Current.Server.MapPath(url);
                if (File.Exists(path))
                {
                    return url;
                }
                return "";
            }
        }

        public string CertificateTypeCustomerJS
        {
            get
            {
                string url = String.Format("/Certificates/{0}/js/{1}.js", Customer.SubDomain, Type.ToString());
                string path = HttpContext.Current.Server.MapPath(url);
                if (File.Exists(path))
                {
                    return url;
                }
                return "";
            }
        }

        public string CertificateTemplateCustomerJS
        {
            get
            {
                string relativePath = CertificateController.GetRelativeTemplatePath(TemplatePath, Customer.SubDomain);

                if (!string.IsNullOrEmpty(relativePath))
                {
                    string url = Path.Combine("/Certificates/", Customer.SubDomain, "js", relativePath, Type.ToString() + ".js")
                                        .Replace('\\', '/');

                    string path = HttpContext.Current.Server.MapPath(url);
                    if (File.Exists(path))
                    {
                        return url;
                    }

                }

                return "";
            }
        }
    }
}
