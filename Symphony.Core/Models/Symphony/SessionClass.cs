﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    public class SessionClass : Class
    {
        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }
    }
}
