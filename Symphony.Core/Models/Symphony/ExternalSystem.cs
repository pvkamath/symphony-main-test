﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.ExternalSystemIntegration.Handlers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "externalSystem")]
    public class ExternalSystem : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "systemName")]
        public string SystemName { get; set; }

        [DataMember(Name = "systemCodeName")]
        public string SystemCodeName { get; set; }

        [DataMember(Name = "loginUrl")]
        public string LoginUrl { get; set; }

        [DataMember(Name = "isRequired")]
        public bool IsRequired { get; set; }

        [DataMember(Name = "externalSystemTypeName")]
        public string ExternalSystemTypeName { get; set; }

        [DataMember(Name = "customerIdInExternalSystem")]
        public string CustomerIdInExternalSystem { get; set; }

        [DataMember(Name = "isCheckCustomerIdInExternalSystem")]
        public bool IsCheckCustomerIdInExternalSystem { get; set; }


        [IgnoreDataMember]
        public Type ExternalSystemType
        {
            get
            {
                if (!string.IsNullOrEmpty(ExternalSystemTypeName))
                {
                    string externalSystemNamespace = typeof(AbstractExternalSystemHandler).Namespace;
                    string qualfiedExternalSystemTypeName = string.Format("{0}.{1}", externalSystemNamespace, ExternalSystemTypeName);
                    return Type.GetType(qualfiedExternalSystemTypeName);
                }
                return null;
            }
        }

        // Ignoreing this one since it will always be false when not within the context of an AbstractExternalSystemHandler
        /// <summary>
        /// Will be true if using within the context of an AbstractExternalSystemHandler that has been configured with this external systemp and
        /// IsSymphony = true on the handler. 
        /// </summary>
        [IgnoreDataMember]
        public bool IsSymphony { get; set; }
    }
}
