using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "reportTrainingProgram")]
    [DefaultSort(ColumnName = "ReportTrainingPrograms.Name")]
    public class ReportTrainingProgram : Model
    {
        [DataMember(Name = "trainingProgramkey")]
        public int TrainingProgramKey { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "customerkey")]
        public int CustomerKey { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramId { get; set; }

        [DataMember(Name = "isCurrentIndicator")]
        public Boolean IsCurrentIndicator { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerId { get; set; }



    }
}
