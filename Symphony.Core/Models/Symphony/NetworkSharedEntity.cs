﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "networkSharedEntity")]
    public class NetworkSharedEntity : Model
    {
        [DataMember(Name="id")]
        public int ID { get; set; }

        [DataMember(Name = "networkId")]
        public int NetworkID { get; set; }

        [DataMember(Name = "customerNetworkDetailId")]
        public int CustomerNetworkDetailID { get; set; }

        [DataMember(Name = "entityTypeId")]
        public int EntityTypeID { get; set; }

        [DataMember(Name = "entityId")]
        public int EntityID { get; set; }
    }
}
