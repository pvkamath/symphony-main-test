﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "class")]
    [DefaultSort(ColumnName = "ClassName")]
    public class Class : Model
    {
        [DataMember(Name = "registrationId")]
        public int RegistrationID { get; set; }

        [DataMember(Name = "registrationStatusId")]
        public int RegistrationStatusID { get; set; }

        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        [DataMember(Name = "courseId")]
        public int CourseID { get; set; }

        [DataMember(Name = "className")]
        public string ClassName { get; set; }

        [DataMember(Name = "webinarKey")]
        public string WebinarKey { get; set; }

        [DataMember(Name = "courseName")]
        public string CourseName { get; set; }

        [DataMember(Name = "classDescription")]
        public string ClassDescription { get; set; }

        [DataMember(Name = "courseDescription")]
        public string CourseDescription { get; set; }

        [DataMember(Name = "venueName")]
        public string VenueName { get; set; }

        [DataMember(Name = "classRoomName")]
        public string ClassRoomName { get; set; }

        private DateTime? _startDate;
        [DataMember(Name = "startDate")]
        public DateTime? StartDate
        {
            get
            {
                return
                    _startDate.HasValue ? _startDate :
                    Dates != null && Dates.Count > 0 ? (DateTime?)Dates[0] : null;
            }
            set { _startDate = value; }
        }

        private DateTime? _endDate;
        [DataMember(Name = "endDate")]
        public DateTime? EndDate
        {
            get
            {
                return
                    _endDate.HasValue ? _endDate :
                    Dates != null && Dates.Count > 0 ? (DateTime?)Dates[Dates.Count - 1] : null;
            }
            set { _endDate = value; }
        }

        [DataMember(Name = "dates")]
        public List<DateTime> Dates { get; set; }

        [DataMember(Name = "registrationClosed")]
        public bool RegistrationClosed { get; set; }

        [DataMember(Name = "isAdminRegistration")]
        public bool IsAdminRegistration { get; set; }


        #region Display 
        
        [DataMember(Name = "displayStartDate")]
        public DisplayDate DisplayStartDate
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                return new DisplayDate() {
                    Date = StartDate,
                    Format = DateFormat.DateTimeTz
                };
            }
            internal set
            {

            }
        }

        [DataMember(Name = "displayLocation")]
        public DisplayModel DisplayLocation
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                Text message;

                if (!string.IsNullOrWhiteSpace(WebinarKey)) {
                    message = Text.Class_Location_Online;
                } else if (string.IsNullOrWhiteSpace(ClassRoomName) && string.IsNullOrWhiteSpace(VenueName)) {
                    message = Text.Class_Location_Unspecified;
                } else {
                    string location = VenueName;
                    if (!string.IsNullOrWhiteSpace(ClassRoomName)) {
                        location += (!string.IsNullOrWhiteSpace(location) ? ", " : "") + ClassRoomName;
                    }

                    message = Text.Dynamic(location);
                }

                return new DisplayModel()
                {
                    Messages = new List<Text>() {
                        message
                    }
                };
            }
            internal set { }
        }

        [DataMember(Name = "displayRegisterLink")]
        public DisplayLink DisplayRegisterLink
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayLink link = new DisplayLink();
                TimeSpan timeSinceStart = DateTime.UtcNow - (StartDate.HasValue ? StartDate.Value : DateTime.MinValue);

                if (IsAdminRegistration || RegistrationClosed)
                {
                    link.BaseUrl = Url.Empty;
                    link.Messages.Add(Text.Class_Registration_Closed);
                    link.Title = Text.Empty;
                }
                else if (timeSinceStart.Days > 7)
                {
                    link.BaseUrl = Url.Empty;
                    link.Messages.Add(Text.Dash);
                    link.Title = Text.Empty;
                }
                else
                {
                    link = DisplayHelpers.GetRegisterLink(CourseID, ClassID, null, null);
                }

                return link;
            }
            internal set { } 
        }


        #endregion
    }
}
