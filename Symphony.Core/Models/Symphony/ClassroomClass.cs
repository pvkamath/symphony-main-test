﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using SubSonic;

namespace Symphony.Core.Models
{
    [DataContract(Name = "class")]
    [DefaultSort(ColumnName = "Name")]
    public class ClassroomClass : Model
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }

        [DataMember(Name = "webinarKey", EmitDefaultValue = false)]
        public string WebinarKey { get; set; } 

        [DataMember(Name = "description", EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(Name = "internalCode", EmitDefaultValue = false)]
        public string InternalCode { get; set; }

        [DataMember(Name = "roomId", EmitDefaultValue = false)]
        public int RoomID { get; set; }

        [DataMember(Name = "courseId", EmitDefaultValue = false)]
        public int CourseID { get; set; }

        [DataMember(Name = "courseName", EmitDefaultValue = false)]
        public string CourseName { get; set; }

        [DataMember(Name = "numberOfDays")]
        public int NumberOfDays { get; set; }

        [DataMember(Name = "dailyDuration")]
        public int DailyDuration { get; set; }

        [DataMember(Name = "courseCompletionTypeId")]
        public int CourseCompletionTypeID { get; set; }

        [DataMember(Name = "timeZoneId", EmitDefaultValue = false)]
        public int TimeZoneID { get; set; }

        [DataMember(Name = "timeZoneDescription", EmitDefaultValue = false)]
        public string TimeZoneDescription { get; set; }

        [DataMember(Name = "capacityOverride", EmitDefaultValue = false)]
        public int CapacityOverride { get; set; }

        [DataMember(Name = "lockedIndicator", EmitDefaultValue = false)]
        public bool LockedIndicator { get; set; }

        [DataMember(Name = "additionalInstructions", EmitDefaultValue = false)]
        public string AdditionalInstructions { get; set; }

        [DataMember(Name = "notes", EmitDefaultValue = false)]
        public string Notes { get; set; }

        [DataMember(Name = "objective", EmitDefaultValue = false)]
        public string Objective { get; set; }

        [DataMember(Name = "isVirtual", EmitDefaultValue = false)]
        public bool IsVirtual { get; set; }

        [DataMember(Name = "surveyId", EmitDefaultValue = false)]
        public int SurveyID { get; set; }

        [DataMember(Name = "venueId", EmitDefaultValue = false)]
        public int VenueID { get; set; }

        [DataMember(Name = "location", EmitDefaultValue = false)]
        public string Location
        {
            get
            {
                List<string> location = new List<string>();
                if (!string.IsNullOrEmpty(RoomName))
                    location.Add(RoomName);
                if (!string.IsNullOrEmpty(VenueName))
                    location.Add(VenueName);
                if (!string.IsNullOrEmpty(VenueCity))
                    location.Add(VenueCity);
                if (!string.IsNullOrEmpty(StateName))
                    location.Add(StateName);
                return string.Join(", ", location.ToArray());
            }
            set { }
        }

        [DataMember(Name = "roomName", EmitDefaultValue = false)]
        public string RoomName { get; set; }

        [DataMember(Name = "venueName", EmitDefaultValue = false)]
        public string VenueName { get; set; }

        [DataMember(Name = "venueCity", EmitDefaultValue = false)]
        public string VenueCity { get; set; }

        [DataMember(Name = "stateName", EmitDefaultValue = false)]
        public string StateName { get; set; }

        [DataMember(Name = "stateAbbreviation", EmitDefaultValue = false)]
        public string StateAbbreviation { get; set; }

        [DataMember(Name = "minClassDate", EmitDefaultValue = false)]
        public DateTime? MinClassDate { get; set; }

        [DataMember(Name = "maxClassDate", EmitDefaultValue = false)]
        public DateTime? MaxClassDate { get; set; }

        public string ClassDates { get; set; }

        [DataMember(Name = "classDates", EmitDefaultValue = false)]
        public List<DateTime> Dates
        {
            get
            {
                if (string.IsNullOrEmpty(ClassDates))
                {
                    return null;
                }
                List<DateTime> dates = new List<DateTime>();
                foreach (string str in ClassDates.Split(','))
                {
                    dates.Add(DateTime.Parse(str.Trim()));
                }
                return dates;
            }
            set { }
        }

        [DataMember(Name = "classInstructors", EmitDefaultValue = false)]
        public string ClassInstructors { get; set; }

        [DataMember(Name = "classInstructorList")]
        public List<ClassInstructor> ClassInstructorList { get; set; }

        [DataMember(Name = "resourceList")]
        public List<ClassResource> ResourceList { get; set; }

        [DataMember(Name = "dateList")]
        public List<ClassDate> DateList { get; set; }

        [DataMember(Name = "registrationList")]
        public List<Registration> RegistrationList { get; set; }

        [DataMember(Name = "onlineCourseId")]
        public int OnlineCourseID { get; set; }

        [DataMember(Name = "files", EmitDefaultValue = false)]
        public List<CourseFile> Files { get; set; }

        [DataMember(Name = "approvalRequired")]
        public bool ApprovalRequired { get; set; }

        [DataMember(Name = "isThirdParty")]
        public bool IsThirdParty { get; set; }

        [DataMember(Name = "currentEnrollment")]
        public int CurrentEnrollment { get; set; }

        [DataMember(Name = "classDescription")]
        public string ClassDescription { get; set; }

        [DataMember(Name = "totalDuration")]
        public int TotalDuration { get; set; }

        [DataMember(Name = "totalCost")]
        public decimal TotalCost { get; set; }

        [DataMember(Name = "isCreateMessageBoard")]
        public bool IsCreateMessageBoard { get; set; }

        [DataMember(Name = "isDisableTopicCreation")]
        public bool IsDisableTopicCreation { get; set; }

        [DataMember(Name = "messageBoardName")]
        public string MessageBoardName { get; set; }

        [DataMember(Name = "externalId")]
        public string ExternalID { get; set; }

        // Certificate key value pairs
        [DataMember(Name = "metaDataJson")]
        public string MetaDataJson { get; set; }

        // references the proctorRequiredIndicator on the online test for the class, if any.
        [DataMember(Name = "proctorRequiredIndicator")]
        public bool ProctorRequiredIndicator { get; set; }

        // references the validation setting on the online test for the class, if any.
        [DataMember(Name = "isUserValidationAllowed")]
        public bool IsUserValidationAllowed { get; set; }

        // Only administrators can register users for the course
        [DataMember(Name = "isAdminRegistration")]
        public bool IsAdminRegistration { get; set; }
    }
}
