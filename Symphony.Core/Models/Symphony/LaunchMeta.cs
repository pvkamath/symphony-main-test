﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name="launchMeta")]
    public class LaunchMeta
    {
        [DataMember(Name = "affidavitJsonString")]
        public string AffidavitJsonString { get; set; }

        [DataMember(Name = "validationJsonString")]
        public string ValidationJsonString { get; set; }

        [DataMember(Name = "proctorJsonString")]
        public string ProctorJsonString { get; set; }

        [DataMember(Name = "professionJsonString")]
        public string ProfessionJsonString { get; set; }

        [DataMember(Name = "preValidationJsonString")]
        public string PreValidationJsonString { get; set; }

    }
}
