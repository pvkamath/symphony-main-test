﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "messageBoardPost")]
    public class MessageBoardPost : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "topicId")]
        public int TopicID { get; set; }

        [DataMember(Name = "postId")]
        public int PostID { get; set; }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "content")]
        public string Content { get; set; }

        [DataMember(Name = "isPrivate")]
        public bool IsPrivate { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "modifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [DataMember(Name = "createdBy")]
        public string CreatedBy { get; set; }

        [DataMember(Name = "modifiedBy")]
        public string ModifiedBy { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "posts")]
        public int Posts { get; set; }

        [DataMember(Name = "jobRole")]
        public string JobRole { get; set; }

        [DataMember(Name = "replies")]
        public List<MessageBoardPost> Replies { get; set; }

        [DataMember(Name = "isFirst")]
        public bool IsFirst { get; set; }

        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }

        [DataMember(Name = "replyContent")]
        public string ReplyContent { get; set; }

        [DataMember(Name = "replyUsername")]
        public string ReplyUsername { get; set; }
    }
}
