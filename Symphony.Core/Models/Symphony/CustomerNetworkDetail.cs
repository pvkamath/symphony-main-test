﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "customerNetworkDetail")]
    [DefaultSort(ColumnName = "DetailId")]
    public class CustomerNetworkDetail : Model
    {
        /*
         * 
         * There is a misspelling in the database that
         * has been around for a while on this table. 
         * The primary key on this table has been named
         * DetaildId when it should be DetailId.
         * 
         * This is a hack to avoid changing anything on the 
         * table itself. Will allow the model and the data model
         * to interact and maintain the detailId on load.
         * 
         */
        /// <summary>
        /// Misspelled primary key - not returning this as part
        /// of the api request in order to avoid at least some 
        /// confusion on the ui side.
        /// </summary>
        [IgnoreDataMember]
        public int DetaildId
        {
            get;
            set;
        }
        /// <summary>
        /// Wrapper for detaildId so at least it makes sense client side. 
        /// This will return the same value that is in DetaildId
        /// </summary>
        [DataMember(Name="detailId")]
        public int DetailId {
            get
            {
                return DetaildId;
            }
            set
            {
                DetaildId = value;
            }
        }

        [DataMember(Name = "networkId")]
        public int NetworkId { get; set; }

        [DataMember(Name = "sourceCustomerId")]
        public int SourceCustomerId { get; set; }

        [DataMember(Name = "sourceCustomerName")]
        public string SourceCustomerName { get; set; }

        [DataMember(Name = "destinationCustomerId")]
        public int DestinationCustomerId { get; set; }

        [DataMember(Name = "destinationCustomerName")]
        public string DestinationCustomerName { get; set; }

        [DataMember(Name = "allowTrainingProgramSharing")]
        public bool AllowTrainingProgramSharing { get; set; }

        [DataMember(Name = "allowReporting")]
        public bool AllowReporting { get; set; }

        [DataMember(Name = "allowLibraries")]
        public bool AllowLibraries { get; set; }

        /// <summary>
        /// The original id on the client side prior to save
        /// </summary>
        [DataMember(Name = "clientId")]
        public int ClientID { get; set; }

        [DataMember(Name = "networkSharedEntities")]
        public List<NetworkSharedEntity> NetworkSharedEntities { get; set; }

        [DataMember(Name = "trainingProgramIds")]
        public List<int> TrainingProgramIds { get; set; }

    }
}
