﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;

namespace Symphony.Core.Models
{
    [DataContract(Name = "pendingRegistration")]
    public class PendingRegistration : Model
    {
        [DataMember(Name = "classId")]
        public int ClassId { get; set; }

        [DataMember(Name = "registrationId")]
        public int RegistrationId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "studentName")]
        public string StudentName {
            get
            {
                return Model.FormatFullName(this.FirstName, this.MiddleName, this.LastName);
            }
            set { }
        }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "middleName")]
        public string MiddleName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "startDate")]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "location")]
        public string Location { get; set; }

        [DataMember(Name = "status")]
        public RegistrationStatusType Status { get; set; }
    }
}
