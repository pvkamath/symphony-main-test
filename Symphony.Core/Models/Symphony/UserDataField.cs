﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Symphony.Core.Models
{
    [DataContract]
    public class UserDataField : Model
    {
        // These codeName and displayName are referenced in a few different ways
        // depending on if the data is coming from the db, or an extjs config. 
        // These fields will allow everything to remain consistant regarless of how
        // the fields are set.
        private string codeName = "";
        private string displayName = "";

        [DataMember(Name = "userDataFieldId")]
        public int ID { get; set; }
        
        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "xtype")]
        public string Xtype { get; set; }

        [DataMember(Name = "config")]
        public string Config { get; set; }

        [DataMember(Name = "codeName")]
        public string CodeName
        {
            get
            {
                return codeName;
            }
            set
            {
                codeName = value;
            }
        }

        [DataMember(Name = "displayName")]
        public string DisplayName
        {
            get
            {
                return displayName;
            }
            set
            {
                displayName = value;
            }
        }

        [DataMember(Name = "categoryCodeName")]
        public string CategoryCodeName { get; set; }

        [DataMember(Name = "categoryDisplayName")]
        public string CategoryDisplayName { get; set; }

        [DataMember(Name = "userValue")]
        public string UserValue { get; set; }

        [DataMember(Name = "userDataFieldUserMapId")]
        public int UserDataFieldUserMapID { get; set; }

        [DataMember(Name = "isGlobal")]
        public bool IsGlobal { get; set; }

        [DataMember(Name = "isSymphonyField")]
        public bool IsSymphonyField { get; set; }

        [DataMember(Name = "type")]
        public int Type { get; set;}

        [DataMember(Name = "userFieldName")]
        public string UserFieldName { get; set; }

        public int Order { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "html")]
        public string Html { get; set; }

        [DataMember(Name = "allowBlank")]
        public bool AllowBlank { get; set; }

        // Intentionally setting the codeName backing field
        // in order to make this model more seemless with
        // extjs usage
        [DataMember(Name = "name")]
        public string Name
        {
            get
            {
                return codeName;
            }
            set
            {
                codeName = value;
            }
        }

        // Intentionally setting the displayName backing field
        // in order to make this model more seemless with
        // extjs usage
        [DataMember(Name = "_name")]
        public string _Name
        {
            get
            {
                return displayName;
            }
            set
            {
                displayName = value;
            }
        }

        // intentionally setting the displayName backing field
        // in order to make this model more seemless with
        // extjs usage.
        [DataMember(Name = "fieldLabel")]
        public string FieldLabel
        {
            get
            {
                return displayName;
            }
            set
            {
                displayName = value;
            }
        }

    }
}
