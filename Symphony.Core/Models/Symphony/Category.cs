﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="category")]
    public class Category : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "categoryTypeId")]
        public int CategoryTypeID { get; set; }

        // used to load, don't serialize it though
        public int ParentCategoryID { get; set; }

        [DataMember(Name = "parentId")]
        public int ParentID
        {
            get { return ParentCategoryID; }
            set { ParentCategoryID = value; }
        }

        [DataMember(Name = "categoryKey")]
        public string CategoryKey { get; set; }

        [DataMember(Name = "levelIndentText")]
        public string LevelIndentText { get; set; }

        [DataMember(Name = "maxCourseWork")]
        public int? MaxCourseWork { get; set; }

        /// <summary>
        /// If being used as a secondary category - helps link to the course
        /// </summary>
        public int OnlineCourseID { get; set; }
    }
}

