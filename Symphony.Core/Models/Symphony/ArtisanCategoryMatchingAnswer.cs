﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "artisanCategoryMatchingAnswer")]
    public class ArtisanCategoryMatchingAnswer
    {
        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "matches")]
        public List<string> Matches { get; set; }
    }
}
