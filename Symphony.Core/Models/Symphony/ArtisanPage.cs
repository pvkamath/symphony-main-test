﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="artisanPage")]
    public class ArtisanPage : Model
    {
        public bool Processed { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "html")]
        public string Html { get; set; }

        [DataMember(Name = "pageType")]
        public int PageType { get; set; }

        [DataMember(Name = "questionType")]
        public ArtisanQuestionType QuestionType { get; set; }

        [DataMember(Name = "answers")]
        public List<ArtisanAnswer> Answers { get; set; }

        //[DataMember(Name = "sectionId")]
        //public int SectionId { get; set; }

        [DataMember(Name = "courseId")]
        public int CourseId { get; set; }

        [DataMember(Name = "templateId")]
        public int TemplateId { get; set; }

        [DataMember(Name = "cssText")]
        public string CssText { get; set; }

        [DataMember(Name = "correctResponse")]
        public string CorrectResponse { get; set; }

        [DataMember(Name = "incorrectResponse")]
        public string IncorrectResponse { get; set; }

        [DataMember(Name = "sort")]
        public int Sort { get; set; }

        [DataMember(Name = "referencePageId")]
        public int ReferencePageId { get; set; }

        [DataMember(Name = "calculateScore")]
        public bool CalculateScore { get; set; }

        [DataMember(Name = "isGradable")]
        public bool IsGradable { get; set; }

        [DataMember(Name = "clientId")]
        public int? ClientId { get; set; }

        public string Tag { get; set; }

        [DataMember(Name = "externalId")]
        public string ExternalID { get; set; }

        [DataMember(Name = "sectionId")]
        public int SectionID { get; set; }

        [DataMember(Name = "minimumPageTime")]
        public int MinimumPageTime { get; set; }

        /// <summary>
        /// Not stored in db, will only be set by the packager
        /// if a learning object is configured as a group and
        /// this is the placeholder page for the group of pages.
        /// </summary>
        [DataMember(Name = "isGroup")]
        public bool IsGroup { get; set; }

    }
}
