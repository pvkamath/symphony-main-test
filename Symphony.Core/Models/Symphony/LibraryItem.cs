﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "libraryItem")]
    [DefaultSort(ColumnName="Name")]
    [KnownType(typeof(Course))]
    [KnownType(typeof(TrainingProgram))]
    public class LibraryItem : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "libraryItemId")]
        public int LibraryItemID { get; set; }

        [DataMember(Name = "libraryId")]
        public int LibraryID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "categoryId")]
        public int CategoryID { get; set; }

        [DataMember(Name = "duration")]
        public int Duration { get; set; }

        [DataMember(Name = "libraryItemTypeId")]
        public int LibraryItemTypeID { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "courseTypeId")]
        public int CourseTypeID { get; set; }

        [DataMember(Name = "categoryName")]
        public string CategoryName { get; set; }

        [DataMember(Name = "levelIndentText")]
        public string LevelIndentText { get; set; }

        [DataMember(Name = "isInLibrary")]
        public bool IsInLibrary { get; set; }

        [DataMember(Name = "isFavorite")]
        public bool IsFavorite { get; set; }

        [DataMember(Name = "isInProgress")]
        public bool IsInProgress { get; set; }

        [DataMember(Name = "libraryFavoriteId")]
        public int LibraryFavoriteID { get; set; }

        [DataMember(Name = "totalRows")]
        public int TotalRows { get; set; }

        [DataMember(Name = "item")]
        public object Item { get; set; }

        [DataMember(Name = "itemCreatedOn")]
        public DateTime ItemCreatedOn { get; set; }

        [DataMember(Name = "itemModifiedOn")]
        public DateTime ItemModifiedOn { get; set; }

        [DataMember(Name = "courseVersion")]
        public int CourseVersion { get; set; }

        /// Display
        [DataMember(Name = "displayLibraryItemType")]
        public DisplayIconState DisplayLibraryItemType
        {
            get
            {
                return DisplayHelpers.GetLibraryItemTypeIcon(LibraryItemTypeID);
            }
            internal set { }
        }
        [DataMember(Name = "displayCreatedOn")]
        public DisplayDate DisplayCreatedOn
        {
            get
            {
                return new DisplayDate()
                {
                    Date = CreatedOn,
                    Format = DateFormat.Date
                };
            }
            internal set { }
        }
        [DataMember(Name = "displayItemCreatedOn")]
        public DisplayDate DisplayItemCreatedOn
        {
            get
            {
                return new DisplayDate()
                {
                    Date = ItemCreatedOn,
                    Format = DateFormat.Date
                };
            }
            internal set { }
        }
        [DataMember(Name = "displayItemModifiedOn")]
        public DisplayDate DisplayModifiedOn
        {
            get
            {
                return new DisplayDate()
                {
                    Date = ItemModifiedOn,
                    Format = DateFormat.Date
                };
            }
            internal set { }
        }
        [DataMember(Name = "displayIsFavorite")]
        public DisplayIconState DisplayIsFavorite
        {
            get
            {
                return DisplayHelpers.GetIsFavoriteIcon(this);
            }
            internal set { }
        }
        [DataMember(Name = "displayInLibrary")]
        public DisplayIconState DisplayInLibrary
        {
            get
            {
                if (IsInLibrary)
                {
                    return new DisplayIconState()
                    {
                        Icon = IconState.InLibraryItem,
                        Messages = new List<Text>() {
                            Text.Library_InLibrary
                        }
                    };
                }
                return new DisplayIconState()
                {
                    Icon = IconState.Inactive,
                    Messages = new List<Text>()
                    {
                        Text.Empty
                    }
                };
            }
            internal set { }
        }

    }
}
