using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportQueues")]
    [DefaultSort(ColumnName = "StartedOn")]
    public class ReportQueues : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "queueCommand")]
        public string queueCommand { get; set; }

        [DataMember(Name = "parameters")]
        public string Parameters { get; set; }
		
		[DataMember(Name = "reportType")]
        public int ReportType { get; set; }
		
        [DataMember(Name = "isEnabled")]
        public int IsEnabled { get; set; }

        [DataMember(Name = "jobId")]
        public int JobId { get; set; }

        [DataMember(Name = "reportId")]
        public int ReportId { get; set; }

        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                return ((ReportStatus)StatusId).ToString();
            }
            internal set { }
        }

        [DataMember(Name = "statusId")]
        public int StatusId { get; set; }

        [DataMember(Name = "startedOn")]
        public DateTime StartedOn { get; set; }

        [DataMember(Name = "finishedOn")]
        public DateTime FinishedOn { get; set; }

        [DataMember(Name = "reportName")]
        public string ReportName { get; set; }

        [DataMember(Name = "errorMessage")]
        public string ErrorMessage { get; set; }

        [DataMember(Name = "downloadCSV")]
        public bool DownloadCSV { get; set; }

        [DataMember(Name = "downloadXLS")]
        public bool DownloadXLS { get; set; }

        [DataMember(Name = "downloadPDF")]
        public bool DownloadPDF { get; set; }

        [DataMember(Name = "nextRunTime")]
        public DateTime NextRunTime { get; set; }

        [DataMember(Name = "statusDataLength")]
        public int StatusDataLength { get; set; }

    }
}
