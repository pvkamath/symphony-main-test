﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "trainingProgramLeader")]
    [DefaultSort(ColumnName = "LastName")]
    public class TrainingProgramLeader : Model
    {
        #region Organizer Statuses

        public static string GTMOrganizerStatusActive = "Active";
        public static string GTMOrganizerStatusSuspended = "Suspended";
        public static string GTMOrganizerStatusInvited = "Invited";

        #endregion

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "middleName")]
        public string MiddleName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "fullName")]
        public string FullName
        {
            get
            {
                return Model.FormatFullName(FirstName, MiddleName, LastName);
            }
            set { }
        }

        [DataMember(Name = "gtmOrganizerStatus")]
        public string GTMOrganizerStatus { get; set; }
        
        [DataMember(Name = "canHostVirtual")]
        public bool CanHostVirtual
        {
            get { return GTMOrganizerStatus == GTMOrganizerStatusActive; }
            set { }
        }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        [DataMember(Name = "alternateEmail1")]
        public string AlternateEmail1 { get; set; }

        [DataMember(Name = "alternateEmail2")]
        public string AlternateEmail2 { get; set; }

        [DataMember(Name = "addressLine1")]
        public string AddressLine1 { get; set; }

        [DataMember(Name = "addressLine2")]
        public string AddressLine2 { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "zipCode")]
        public string ZipCode { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "homePhone")]
        public string HomePhone { get; set; }

        [DataMember(Name = "workPhone")]
        public string WorkPhone { get; set; }

        [DataMember(Name = "cellPhone")]
        public string CellPhone { get; set; }

        [DataMember(Name = "isPrimary")]
        public bool IsPrimary { get; set; }

        // Display
        [DataMember(Name = "displayVideoChatLink")]
        public DisplayIconState DisplayVideoChatLink
        {
            get
            {
                if (!Modules.HasModule(Symphony.Core.Controllers.SymphonyController.GetCustomerID(), new Modules().VideoChat)) {
                    return null;
                }

                return new DisplayIconState()
                {
                    Icon = IconState.Webcam,
                    Link = new DisplayLink()
                    {
                        BaseUrl = Url.LaunchVideoChat,
                        UrlFormat = TextFormat.LaunchVideoChatUrlFormat,
                        QueryParams = new KeyValueList()
                        {
                            new DisplayKeyValue() { Key = "userId", Value = UserID.ToString() }
                        }
                    }
                };
            }
            internal set { }
        }

        [DataMember(Name = "displayMessageLink")]
        public DisplayIconState DisplayMessageLink
        {
            get
            {
                return new DisplayIconState()
                {
                    Icon = IconState.Message,
                    Link = new DisplayLink()
                    {
                        BaseUrl = Url.SendMessage,
                        UrlFormat = TextFormat.SendMessageUrlFormat,
                        QueryParams = new KeyValueList() {
                            new DisplayKeyValue() { Key = "fromUserId", Value = Symphony.Core.Controllers.SymphonyController.GetUserID().ToString() },
                            new DisplayKeyValue() { Key = "toUserId", Value = UserID.ToString() }
                        }
                    }
                };
            }
            internal set { }
        }
    }
}
