﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "trainingProgramGroup")]
    public class TrainingProgramGroup : Model
    {

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "entitlementId")]
        public int EntitlementID { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "parentTrainingProgramId")]
        public int ParentTrainingProgramID { get; set; }

    }
}
