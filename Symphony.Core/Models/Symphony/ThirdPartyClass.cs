﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "thirdPartyClass")]
    public class ThirdPartyClass : Model
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "date")]
        public DateTime Date { get; set; }

        [DataMember(Name = "courseCompletionTypeId")]
        public int CourseCompletionTypeId { get; set; }
    }
}
