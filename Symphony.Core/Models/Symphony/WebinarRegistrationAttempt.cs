﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;

namespace Symphony.Core.Models
{
    [DataContract(Name = "webinarRegistrationAttempt")]
    public class WebinarRegistrationAttempt : Model
    {
        [DataMember(Name="success")]
        public bool Success { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "webinarKey")]
        public string WebinarKey { get; set; }

        [DataMember(Name = "registrantID")]
        public string RegistrantID { get; set; }

        [DataMember(Name = "joinUrl")]
        public string JoinUrl { get; set; }

        [DataMember(Name = "registrantKey")]
        public string RegistrantKey { get; set; }

        [DataMember(Name = "error")]
        public string Error { get; set; }

    }
}
