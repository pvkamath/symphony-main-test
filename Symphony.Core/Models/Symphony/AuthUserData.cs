﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "authUserData")]
    public class AuthUserData : Model
    {
        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "salesChannelId")]
        public int SalesChannelID { get; set; }

        [DataMember(Name = "applicationName")]
        public string ApplicationName { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "isSalesChannelAdmin")]
        public bool IsSalesChannelAdmin { get; set; }

        [DataMember(Name = "isNewHire")]
        public bool IsNewHire { get; set; }

        [DataMember(Name = "cnrToken")]
        public string CNRToken { get; set; }

        [DataMember(Name = "actualUserId")]
        public int ActualUserID { get; set; }

        [DataMember(Name = "actualCustomerID")]
        public int ActualCustomerID { get; set; }

        [DataMember(Name = "actualSalesChannelID")]
        public int ActualSalesChannelID { get; set; }

        [DataMember(Name = "actualApplicationName")]
        public string ActualApplicationName { get; set; }

        [DataMember(Name = "attributes")]
        public Dictionary<string, string> Attributes { get; set; }
    }
}
