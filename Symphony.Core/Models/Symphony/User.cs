﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Attributes;
using Symphony.Core.Interfaces;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.IO;
using log4net;

namespace Symphony.Core.Models
{
    [DataContract(Name = "user")]
    [DefaultSort(ColumnName = "LastName")]
    public class User : Model, IAssociatedImage
    {
        ILog log = LogManager.GetLogger(typeof(User));

        private List<UserDataField> _userDataFields;

        [DataMember(Name = "associatedImageData")]
        public string AssociatedImageData { get; set; }

        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "collaborator")]
        public bool Collaborator { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [CertificateField]
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [CertificateField]
        [DataMember(Name = "middleName")]
        public string MiddleName { get; set; }

        [CertificateField]
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "fullName")]
        public string FullName
        {
            get
            {
                return Model.FormatFullName(FirstName, MiddleName, LastName);
            }
            set { }
        }

        [DataMember(Name = "loginSystems")]
        public List<LoginSystem> LoginSystems { get; set; }

        [DataMember(Name = "userReportingPermissions")]
        public List<UserReportingPermission> UserReportingPermissions { get; set; }

        //[DataMember(Name = "name")]
        //public string Name
        //{
        //    get
        //    {
        //        return LastName + ", " + FirstName + " " + MiddleName;
        //    }
        //    set { }
        //}

        [CertificateField]
        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "supervisorId")]
        public int SupervisorID { get; set; }

        [DataMember(Name = "enforcePasswordDays")]
        public int EnforcePasswordDays { get; set; }

        [DataMember(Name = "enforcePasswordReset")]
        public bool EnforcePasswordReset { get; set; }

        [DataMember(Name = "lastEnforcedPasswordReset")]
        public DateTime? LastEnforcedPasswordReset { get; set; }

        [DataMember(Name = "isPasswordResetRequired")]
        public bool IsPasswordResetRequired { get; set; }

        [DataMember(Name = "reportingSupervisorId")]
        public int ReportingSupervisorID { get; set; }

        [DataMember(Name = "secondarySupervisorId")]
        public int SecondarySupervisorID { get; set; }

        [DataMember(Name = "supervisor")]
        public string Supervisor { get; set; }

        [DataMember(Name = "reportingSupervisor")]
        public string ReportingSupervisor { get; set; }

        [DataMember(Name = "secondarySupervisor")]
        public string SecondarySupervisor { get; set; }

        [DataMember(Name = "salesChannelId")]
        public int SalesChannelID { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "customerName")]
        public string CustomerName { get; set; }

        [DataMember(Name = "customerSubDomain")]
        public string CustomerSubDomain { get; set; }

        [DataMember(Name = "customerExternalLoginEnabled")]
        public bool CustomerExternalLoginEnabled { get; set; }

        [DataMember(Name = "jobRole")]
        public string JobRole { get; set; }

        [DataMember(Name = "jobRoleId")]
        public int JobRoleID { get; set; }

        [DataMember(Name = "location")]
        public string Location { get; set; }

        [DataMember(Name = "locationId")]
        public int LocationID { get; set; }

        [DataMember(Name = "audiences")]
        public string Audiences { get; set; }

        [DataMember(Name = "applicationPermissions")]
        public List<string> ApplicationPermissions { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [CertificateField]
        [DataMember(Name = "employeeNumber")]
        public string EmployeeNumber { get; set; }

        [DataMember(Name = "newHireIndicator")]
        public bool NewHireIndicator { get; set; }

        [DataMember(Name = "newHireEndDate")]
        public DateTime? NewHireEndDate { get; set; }

        [DataMember(Name = "hireDate")]
        public DateTime? HireDate { get; set; }

        [DataMember(Name = "telephoneNumber")]
        public string TelephoneNumber { get; set; }

        [DataMember(Name = "isExternal")]
        public bool IsExternal { get; set; }

        [DataMember(Name = "statusId")]
        public int StatusID { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [CertificateField]
        [DataMember(Name = "notes")]
        public string Notes { get; set; }

        [DataMember(Name = "loginCounter")]
        public int LoginCounter { get; set; }

        [DataMember(Name = "failedAttempts")]
        public int FailedAttempts { get; set; }

        [DataMember(Name = "isAccountExec")]
        public bool IsAccountExec { get; set; }

        [DataMember(Name = "isCustomerCare")]
        public bool IsCustomerCare { get; set; }

        [DataMember(Name = "isLockedOut")]
        public bool IsLockedOut { get; set; }

        [DataMember(Name = "timeZone")]
        public string TimeZone { get; set; }

        [DataMember(Name = "onlineStatus")]
        public int? OnlineStatus { get; set; }

        [DataMember(Name = "isCreatedBySalesforce")]
        public bool IsCreatedBySalesforce { get; set; }

        [CertificateField]
        [DataMember(Name = "nmlsNumber")]
        public string NmlsNumber { get; set; }


        [CertificateField]
        [DataMember(Name = "socialSecurityNumber")]
        public string SocialSecurityNumber
        {
            get
            {
                if (string.IsNullOrEmpty(EncryptedSSN))
                {
                    return string.Empty;
                }
                return Utilities.RijndaelSimple.Decrypt(EncryptedSSN);
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    EncryptedSSN = Utilities.RijndaelSimple.Encrypt(value);
                }
                else
                {
                    EncryptedSSN = string.Empty;
                }
            }
        }

        [IgnoreDataMember]
        public string EncryptedSSN
        {
            get; set;
        }

        [DataMember(Name = "dateOfBirth")]
        public DateTime? DateOfBirth
        {
            get;
            set;
        }

        #region Author

        [DataMember(Name = "isAuthor")]
        public bool? IsAuthor { get; set; }

        [DataMember(Name = "speciality")]
        public string Speciality { get; set; }

        #endregion Author

        [DataMember(Name = "alternateEmail1")]
        public string AlternateEmail1 { get; set; }

        [DataMember(Name = "alternateEmail2")]
        public string AlternateEmail2 { get; set; }

        [DataMember(Name = "addressLine1")]
        public string AddressLine1 { get; set; }

        [DataMember(Name = "addressLine2")]
        public string AddressLine2 { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "zipCode")]
        public string ZipCode { get; set; }

        [DataMember(Name = "homePhone")]
        public string HomePhone { get; set; }

        [DataMember(Name = "workPhone")]
        public string WorkPhone { get; set; }

        [DataMember(Name = "cellPhone")]
        public string CellPhone { get; set; }

        [DataMember(Name = "userDataFields")]
        public List<UserDataField> UserDataFields {
            get
            {
                if (_userDataFields != null && _userDataFields.Count > 0)
                {
                    return _userDataFields;
                } else if (!string.IsNullOrEmpty(UserDataFieldsXml))
                {
                    try
                    {
                        XmlRootAttribute root = new XmlRootAttribute();
                        root.ElementName = "UserDataFields";
                        XmlSerializer s = new XmlSerializer(typeof(List<UserDataField>), root);
                        List<UserDataField> userDataFields;

                        using (TextReader reader = new StringReader("<UserDataFields>" + UserDataFieldsXml + "</UserDataFields>"))
                        {
                            userDataFields = (List<UserDataField>)s.Deserialize(reader);
                        }

                        if (userDataFields == null)
                        {
                            log.Error("User data fields was null, yet a XML string was found. This should not happen. XML String: " + UserDataFieldsXml);
                            return new List<UserDataField>();
                        }

                        _userDataFields = userDataFields;

                        return userDataFields;
                    } catch(Exception e) {
                        log.Error("Could not deserialize XML: " + UserDataFieldsXml, e);
                    }
                }

                return new List<UserDataField>();
            }
            set
            {
                _userDataFields = value;
            }
        }
        [IgnoreDataMember]
        public string UserDataFieldsXml { get; set; }

        #region Salesforce

        [DataMember(Name = "mobile")]
        public string Mobile { get; set; }

        [DataMember(Name = "fax")]
        public string Fax { get; set; }

        [DataMember(Name = "salesforceContactId")]
        public string SalesforceContactId { get; set; }

        [DataMember(Name = "salesforceAccountName")]
        public string SalesforceAccountName { get; set; }

        [DataMember(Name = "salesforceAccountId")]
        public string SalesforceAccountId { get; set; }

        [DataMember(Name = "janrainUserUuid")]
        public string JanrainUserUuid { get; set; }

        [DataMember(Name = "salesforceAddress")]
        public string SalesforceAddress { get; set; }

        public bool IsJanrainPasswordFail { get; set; }
        #endregion


        internal string GetLicenseFieldValue(string dataField)
        {
            return new UserController().GetCertificateFieldValue(this, dataField);
        }

        [DataMember(Name = "primaryProfessionName")]
        public string PrimaryProfessionName { get; set; }

        [DataMember(Name = "secondaryProfessionName")]
        public string SecondaryProfessionName { get; set; }

        [DataMember(Name = "primaryProfessionId")]
        public int? PrimaryProfessionID { get; set; }

        [DataMember(Name = "secondaryProfessionId")]
        public int? SecondaryProfessionID { get; set; }

        [DataMember(Name = "isRedirectOptIn")]
        public bool? IsRedirectOptIn { get; set; }

        [DataMember(Name = "customer")]
        public Customer Customer { get; set; }

        [DataMember(Name = "salesChannelName")]
        public string SalesChannelName { get; set; }

        [DataMember(Name = "loginRedirect")]
        public string LoginRedirect { get; set; }

        [DataMember(Name = "redirectType")]
        public int? RedirectType { get; set; }

        [DataMember(Name = "isSpoofing")]
        public bool IsSpoofing { get; set; }
    }
}
