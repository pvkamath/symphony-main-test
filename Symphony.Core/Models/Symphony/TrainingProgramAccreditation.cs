﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TrainingProgramAccreditationData = Symphony.Core.Data.TrainingProgramAccreditation;

namespace Symphony.Core.Models
{
    /// <summary>
    /// A training program accreditation is an accreditation granted to an entire training program,
    /// rather than a course.
    /// </summary>
    [DefaultSort(ColumnName = "[dbo].[TrainingProgramAccreditation].[ID]")]
    [DataContract(Name = "trainingProgramAccreditation")]
    public class TrainingProgramAccreditation : Model
    {
        static TrainingProgramAccreditation()
        {
            PagedQueryParams<TrainingProgramAccreditation>.RegisterSortableProperties(new Dictionary<string, string>
            {
                { "trainingProgramName", "[dbo].[TrainingProgram].[Name]" },
                { "status", "status" },
                { "startDate", "startDate" },
                { "expiryDate", "expiryDate" },
                { "creditHours", "creditHours" },
                { "creditHoursLabel", "creditHoursLabel" },
                { "accreditationCode", "accreditationCode" },
                { "courseNumber", "courseNumber" }
            });
        }

        /// <summary>
        /// Gets or sets the unique database identifier for the accreditation.
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the ID of the accreditation board that issued the accreditation.
        /// </summary>
        [DataMember(Name = "accreditationBoardId")]
        public int AccreditationBoardId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the training program that the accreditation is tied to.
        /// </summary>
        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramId { get; set; }

        /// <summary>
        /// Gets or sets the name of the associated training program. This is only populated when
        /// specifically joined to the TrainingProgram table, such as when querying the set of all
        /// accreditations that belong to an AccreditationBoard.
        /// </summary>
        [DataMember(Name = "trainingProgramName")]
        public string TrainingProgramName { get; set; }

        /// <summary>
        /// Gets or sets the status of the accreditation. See <see cref="AccreditationStatus"/> for
        /// possible statuses. This property overrides any value in the StartDate or ExpiryDate
        /// properties.
        /// </summary>
        [DataMember(Name = "status")]
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the date from which the accreditation is valid. This is a UTC date and is
        /// specific to the minute. This property is required if the accreditation is no longer
        /// pending.
        /// </summary>
        [DataMember(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the date after which the accreditation expires. This is a UTC date and is
        /// specific to the minute. This property is required if the accreditation is no longer
        /// pending.
        /// </summary>
        [DataMember(Name = "expiryDate")]
        public DateTime? ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the number of credit hours that the accreditation is worth. Credit hours
        /// define how many "credits" each AccreditationBoard awards to a training program.
        /// 
        /// This has an implication for licenses that are granted based on a number of credits.
        /// Different boards can weight courses differently, so this may vary per course. This
        /// property is required  if the accreditation is no longer pending.
        /// </summary>
        [DataMember(Name = "creditHours")]
        public float? CreditHours { get; set; }

        /// <summary>
        /// Gets or sets the name that the accreditation board uses when describing credit hours.
        /// Different accreditation boards may use different terms when referring to these. For
        /// example: "Credit Hours", "Credits", "Classroom Hours", etc. This property is required
        /// if the accreditation is no longer pending.
        /// </summary>
        [DataMember(Name = "creditHoursLabel")]
        public string CreditHoursLabel { get; set; }

        /// <summary>
        /// Gets or sets the disclaimer that will be shown before starting the training program.
        /// This may or may not have HTML formatting. This property is not required. If no
        /// disclaimer is provided, it will use the disclaimer of the its parent accreditation
        /// board.
        /// </summary>
        [DataMember(Name = "disclaimer")]
        public string Disclaimer { get; set; }

        /// <summary>
        /// Gets or sets a unique accreditation code that the accreditation board assigns to the
        /// accreditation. This is the a number the board assigns us that indicates that the course
        /// is approved.
        /// </summary>
        [DataMember(Name = "accreditationCode")]
        public string AccreditationCode { get; set; }

        /// <summary>
        /// Gets or sets a unique course number for the accreditation. This is a number that the
        /// board uses internally to identify the course.
        /// </summary>
        [DataMember(Name = "courseNumber")]
        public string CourseNumber { get; set; }

        /// <summary>
        /// Gets or sets whether or not the accreditation is deleted.
        /// </summary>
        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the profession id for this accreditation
        /// This is used when returning with a training program
        /// Going to list 1 accreditation entry per profession
        /// </summary>
        [DataMember(Name = "professionId")]
        public int ProfessionID { get; set; }

        /// <summary>
        /// Gets or sets the profession name for this accreditation
        /// This is used when returning with a training program
        /// Going to list 1 accreditation entry per profession
        /// </summary>
        [DataMember(Name = "professionName")]
        public string ProfessionName { get; set; }

        /// <summary>
        /// Converts the TrainingProgrmaAccreditation object to a SubSonic data object.
        /// </summary>
        /// <returns>The SubSonic data object.</returns>
        public TrainingProgramAccreditationData ToDataObject()
        {
            TrainingProgramAccreditationData data;
            if (Id > 0)
            {
                data = new TrainingProgramAccreditationData(Id);
            }
            else
            {
                data = new TrainingProgramAccreditationData();
            }

            data.TrainingProgramID = TrainingProgramId;
            data.AccreditationBoardID = AccreditationBoardId;
            data.Status = Status;
            data.StartDate = StartDate;
            data.ExpiryDate = ExpiryDate;
            data.CreditHours = CreditHours;
            data.CreditHoursLabel = CreditHoursLabel;
            data.AccreditationCode = AccreditationCode;
            data.CourseNumber = CourseNumber;
            data.Disclaimer = Disclaimer;
            data.IsDeleted = IsDeleted;

            return data;
        }

        /// <summary>
        /// Determines whether or not the accreditation model is in a valid state and outputs an
        /// error message if so.
        /// </summary>
        /// <param name="error">Reference to a string variable where an error message can be stored.</param>
        /// <returns>A value indicating whether or not the model is in a valid state.</returns>
        public bool IsValid(out string error)
        {
            string errBase = "TrainingProgramAccreditation object is in an invalid state: ";

            if (TrainingProgramId <= 0)
            {
                error = errBase + "TrainingProgramId must be a positive integer.";
                return false;
            }
            if (AccreditationBoardId <= 0)
            {
                error = errBase + "AccreditationBoardId must be a positive integer.";
                return false;
            }
            if (ExpiryDate.HasValue && StartDate.HasValue && DateTime.Compare(StartDate.Value, ExpiryDate.Value) > 0)
            {
                error = errBase + "ExpiryDate must be later than StartDate.";
                return false;
            }
            if (CreditHours.HasValue && CreditHours.Value <= 0)
            {
                error = errBase + "CreditHours must be a positive integer.";
                return false;
            }

            if (Status != (int)AccreditationStatus.Pending)
            {
                if (!StartDate.HasValue)
                {
                    error = errBase + "StartDate is required for accreditations that are not pending.";
                    return false;
                }
                if (!ExpiryDate.HasValue)
                {
                    error = errBase + "ExpiryDate is required for accreditations that are not pending.";
                    return false;
                }
                if (!CreditHours.HasValue)
                {
                    error = errBase + "CreditHours is required for accreditations that are not pending.";
                    return false;
                }
                if (string.IsNullOrWhiteSpace(CreditHoursLabel))
                {
                    error = errBase + "CreditHoursLabel is required for accreditations that are not pending.";
                    return false;
                }
                if (string.IsNullOrWhiteSpace(AccreditationCode))
                {
                    error = errBase + "AccreditationCode is required for accreditations that are not pending.";
                    return false;
                }
            }

            error = null;
            return true;
        }
    }
}
