﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "parameterValue")]
    [DefaultSort(ColumnName = "ID")]
    public class ParameterValue : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "parameterId")]
        public int ParameterID { get; set; }

        [DataMember(Name = "parameterSetId")]
        public string ParameterSetID { get; set; }

        [DataMember(Name = "parameterSetName")]
        public string ParameterSetName { get; set; }

        [DataMember(Name = "parameterSetOptionId")]
        public int ParameterSetOptionId { get; set; }

        [DataMember(Name = "parameterSetOptionValue")]
        public string ParameterSetOptionValue { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }
    }
}
