﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;
using System.IO;

namespace Symphony.Core.Models
{
    [DataContract(Name = "mediaPlayer")]
    public class MediaPlayer : Model
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
    }
}
