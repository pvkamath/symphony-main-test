﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;

namespace Symphony.Core.Models
{
    [DataContract(Name="loginSystem")]
    public class LoginSystem
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "userId")]
        public int UserId { get; set; }

        [DataMember(Name = "systemName")]
        public string SystemName { get; set; }

        [DataMember(Name = "customerName")]
        public string CustomerName { get; set; }
		
		[DataMember(Name = "systemCodeName")]
		public string SystemCodeName { get; set; }

        [DataMember(Name = "userEmail")]
        public string UserEmail { get; set; }

        [DataMember(Name = "userFullName")]
        public string UserFullName { get; set; }

        [DataMember(Name = "userFirstName")]
        public string UserFirstName { get; set; }

        [DataMember(Name = "userLastName")]
        public string UserLastName { get; set; }

        [DataMember(Name = "janrainId")]
        public string JanrainId { get; set; }

        [DataMember(Name = "customerSubDomain")]
        public string CustomerSubDomain { get; set; }

        [DataMember(Name = "loginForm")]
        public string LoginForm { get; set; }

        [DataMember(Name = "isSymphony")]
        public bool IsSymphony { get; set; }

        [DataMember(Name = "isDisplayed")]
        public bool IsDisplayed { get; set; }

        [DataMember(Name = "isAutoNavigate")]
        public bool IsAutoNavigate { get; set; }

        [DataMember(Name = "formType")]
        public int FormType { get; set; }

        [DataMember(Name = "frameId")]
        public string FrameID { get; set; }

        [DataMember(Name = "formId")]
        public string FormID { get; set; }

        [DataMember(Name = "landingPage")]
        public string LandingPage { get; set; }

        /// <summary>
        /// We previously returned courses only, but now we are 
        /// returning the training programs instead. 
        /// Leaving this in here to ensure any place still using courses
        /// will receive the same data structure
        /// </summary>
        [DataMember(Name = "courses")]
        public List<Course> Courses {
            get
            {
                if (TrainingPrograms != null && TrainingPrograms.Count > 0)
                {
                    return TrainingPrograms.Select(tp => new Course{Name = tp.Name}).ToList<Course>();
                }
                return new List<Course>();
            }
            internal set
            {
            }
        }

        [DataMember(Name = "trainingPrograms")]
        public List<TrainingProgram> TrainingPrograms { get; set; }
    }
}
