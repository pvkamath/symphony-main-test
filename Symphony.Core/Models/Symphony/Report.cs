using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;
using NodaTime;

namespace Symphony.Core.Models
{
    [DataContract(Name="report")]
    [DefaultSort(ColumnName = "Name")]
    public class Report : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "ownerId")]
        public int OwnerId { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerId { get; set; }

		[DataMember(Name = "name")]
        public string Name { get; set; }	
		
        [DataMember(Name = "reportTypeId")]
        public int ReportTypeId { get; set; }

        [DataMember(Name = "notifyWhenReady")]
        public bool NotifyWhenReady { get; set; }

        [DataMember(Name = "downloadCSV")]
        public bool DownloadCSV { get; set; }

        [DataMember(Name = "downloadXLS")]
        public bool DownloadXLS { get; set; }

        [DataMember(Name = "downloadPDF")]
        public bool DownloadPDF { get; set; }
        
        [DataMember(Name = "parameters")]
        public string Parameters { get; set; } // JSON

        [DataMember(Name = "scheduleType")]
        public ReportSchedule ScheduleType { get; set; }

        // 0-23 (12pm-11pm)
        [DataMember(Name = "scheduleHour")]
        public int ScheduleHour { get; set; }

        // 0=Sunday, 1=Monday, ... 6=Saturday
        [DataMember(Name = "scheduleDaysOfWeek")]
        public int[] ScheduleDaysOfWeekArray
        {
            get
            {
                if (!ScheduleDaysOfWeek.HasValue)
                {
                    ScheduleDaysOfWeek = 0;
                }
                return Utilities.FlagsToDays((DaysOfWeek)ScheduleDaysOfWeek);
            }
            set {
                ScheduleDaysOfWeek = (int)Utilities.DaysToFlags(value);
            }
        }

        public int? ScheduleDaysOfWeek { get; set; }

        // 1-31, (-1 = Last day of month)
        [DataMember(Name = "scheduleDayOfMonth")]
        public int? ScheduleDayOfMonth { get; set; }

        // 1-12
        [DataMember(Name = "scheduleMonth")]
        public int? ScheduleMonth { get; set; }

        [DataMember(Name = "isFavorite")]
        public bool IsFavorite { get; set; }

        [DataMember(Name = "ownerName")]
        public string OwnerName { get; set; }

        [DataMember(Name = "reportTypeCode")]
        public string ReportTypeCode { get; set; }

        // TODO: Change this back to a ZonedDateTime? once we figure out how to properly render ZonedDateTime's in JS
        [DataMember(Name = "nextRunTime")]
        public string NextRunTime { get; set; }

        public string GetCronDaysOfWeek()
        {
            return string.Join(",", ScheduleDaysOfWeekArray.Select(d => (d + 1).ToString()).ToArray());
        }

        [DataMember(Name = "reportTemplateId")]
        public int ReportTemplateID { get; set; }

        [DataMember(Name = "xmlParameters")]
        public string XMLParameters { get; set; }

        [DataMember(Name = "absoluteXmlParameters")]
        public string AbsoluteXmlParameters { get; set; }

        [DataMember(Name = "absoluteXmlParametersJson")]
        public string AbsoluteXmlParametersJson { get; set; }

        [DataMember(Name = "absoluteXmlParametersWarning")]
        public string AbsoluteXmlParametersWarning { get; set; }

        [DataMember(Name = "isSymphonyIdMode")]
        public bool? IsSymphonyIdMode { get; set; }
    }
}
