﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web;
using System.IO;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    /// <summary>
    /// Generated certificates are intermediary objects used by the templating engine to generate a
    /// final certificate to display in HTML.
    /// </summary>
    [DataContract(Name = "simpleGeneratedCertificate")]
    public class SimpleGeneratedCertificate : Model
    {
        [DataMember(Name = "body")]
        public string Body { get; set; }
    }
}
