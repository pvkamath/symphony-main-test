﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Models
{
    /// <summary>
    /// Includes details of the training program along with the summary information. 
    /// Want the ID in this case to point to the training program ID.
    /// 
    /// This represents the result set of GetTrainingProgramsForUser
    /// </summary>
    [DataContract(Name="TrainingProgramSummaryDetail")]
    public class TrainingProgramSummaryDetail : TrainingProgramSummary, ITrainingProgram
    {
        [DataMember(Name="id")]
        public int Id { get; set; }

        #region TrainingProgram Details
        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "ownerId")]
        public int OwnerID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "internalCode")]
        public string InternalCode { get; set; }

        [DataMember(Name = "cost")]
        public decimal Cost { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "isNewHire")]
        public bool IsNewHire { get; set; }

        [DataMember(Name = "isLive")]
        public bool IsLive { get; set; }

        [DataMember(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public DateTime? EndDate { get; set; }

        [DataMember(Name = "dueDate")]
        public DateTime? DueDate { get; set; }

        [DataMember(Name = "enforceRequiredOrder")]
        public bool EnforceRequiredOrder { get; set; }

        [DataMember(Name = "minimumElectives")]
        public int MinimumElectives { get; set; }

        [DataMember(Name = "courseCount")]
        public int CourseCount { get; set; }

        public int TotalSize { get; set; }

        [DataMember(Name = "messageBoardId")]
        public int MessageBoardID { get; set; }

        [DataMember(Name = "sku")]
        public string Sku { get; set; }

        [DataMember(Name = "parentTrainingProgramId")]
        public int ParentTrainingProgramID { get; set; }

        [DataMember(Name = "isActive")]
        public bool IsActive { get; set; }

        [DataMember(Name = "sessionCourseId")]
        public int? SessionCourseID { get; set; }

        [DataMember(Name = "session")]
        public ClassroomClass Session { get; set; }

        [DataMember(Name = "isSessionActive")]
        public bool IsSessionActive { get; set; }

        [DataMember(Name = "isSessionRegistered")]
        public bool IsSessionRegistered { get; set; }

        [DataMember(Name = "isUsingSessions")]
        public bool IsUsingSessions { get; set; }

        [DataMember(Name = "sessionCourseName")]
        public string SessionCourseName { get; set; }

        [DataMember(Name = "categoryName")]
        public string CategoryName { get; set; }

        [DataMember(Name = "newHireStartDateOffset")]
        public int? NewHireStartDateOffset { get; set; }

        [DataMember(Name = "newHireDueDateOffset")]
        public int? NewHireDueDateOffset { get; set; }

        [DataMember(Name = "newHireEndDateOffset")]
        public int? NewHireEndDateOffset { get; set; }

        [DataMember(Name = "newHireOffsetEnabled")]
        public bool NewHireOffsetEnabled { get; set; }

        [DataMember(Name = "newHireTransitionPeriod")]
        public int? NewHireTransitionPeriod { get; set; }

        [DataMember(Name = "purchasedFromBundleName")]
        public string PurchasedFromBundleName { get; set; }
        #endregion

        #region Display 
        [DataMember(Name = "displayCompleted")]
        public DisplayIconState DisplayCompleted
        {
            get
            {
                if (IsComplete || IsCompleteOverride)
                {
                    return DisplayHelpers.GetCertificateBadge(CertificateType.TrainingProgram, Id, 0, UserID);
                }
                return new DisplayIconState
                {
                    Link = new DisplayLink
                    {
                        BaseUrl = Url.Empty,
                        Title = Text.Empty
                    },
                    Icon = IconState.Incomplete,
                    Messages = new List<Text>{Text.TrainingProgram_Incomplete}
                };
            }
            internal set { }
        }
        [DataMember(Name = "displayDueDate")]
        public DisplayDate DisplayDueDate
        {
            get
            {
                DateTime? date = null;

                if (DueDate.HasValue && DueDate.Value > StartDate.Value && (EndDate.HasValue && DueDate.Value < EndDate.Value))
                {
                    date = DueDate;
                }
                else if (EndDate.HasValue)
                {
                    date = EndDate;
                }
                else
                {
                    return null;
                }

                return new DisplayDate()
                {
                    Date = date.Value,
                    Format = IsUsingSessions ? DateFormat.DateTime : DateFormat.Date,
                    Messages = new List<Text>() { (IsUsingSessions && !IsSessionRegistered ? Text.Session_RegistrationRequired : Text.Empty) }
                };
            }
            internal set { }
        }
        #endregion
    }
}
