﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "customerUserFilter")]
    public class CustomerUserFilter
    {
        [DataMember(Name = "status")]
        public UserStatusType Status { get; set; }
    }
}
