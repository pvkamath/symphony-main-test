﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="trainingProgramCourse")]
    public class TrainingProgramCourse : Course
    {
        /// <summary>
        /// Simple override for the ID property so we can load direcly from the links table
        /// </summary>
        public int CourseID { get { return this.Id; } set { this.Id = value; } }

        public int TrainingProgramId { get; set; }
        public string TrainingProgramName { get; set; }

        [DataMember(Name="syllabusTypeId")]
        public int SyllabusTypeID { get; set; }

        [DataMember(Name = "sortOrder")]
        public int SortOrder { get; set; }

        [DataMember(Name = "dueDate")]
        public DateTime? DueDate { get; set; }

        [DataMember(Name = "activeAfterDate")]
        public DateTime? ActiveAfterDate { get; set; }

        [DataMember(Name = "activeAfterMinutes")]
        public double? ActiveAfterMinutes { get; set; }

        [DataMember(Name = "expiresAfterMinutes")]
        public double? ExpiresAfterMinutes { get; set; }

        [DataMember(Name = "isRelativeActiveAfter")]
        public bool IsRelativeActiveAfter { get; set; }

        [DataMember(Name = "allowPrinting")]
        public bool? AllowPrinting { get; set; }

        [DataMember(Name = "isRelativeDueDate")]
        public bool IsRelativeDueDate { get; set; }

        [DataMember(Name = "activeAfterMode")]
        public int ActiveAfterMode { get; set; }

        [DataMember(Name = "dueMode")]
        public int DueMode { get; set; }

        [DataMember(Name = "minutesUntilActive")]
        public int MinutesUntilActive { get; set; }

        [DataMember(Name = "minutesUntilExpired")]
        public int MinutesUntilExpired { get; set; }

        [DataMember(Name = "hasLaunched")]
        public bool HasLaunched { get; set; }

        [DataMember(Name = "denyAccessAfterDueDateIndicator")]
        public bool DenyAccessAfterDueDateIndicator { get; set; }

        [DataMember(Name = "activeAfterUnits")]
        public string ActiveAfterUnits { get; set; }

        [DataMember(Name = "expiresAfterUnits")]
        public string ExpiresAfterUnits { get; set; }

        [DataMember(Name = "isUsingSession")]
        public bool IsUsingSession { get; set; }

        [DataMember(Name = "hasAccreditations")]
        public bool HasAccreditations
        {
            get
            {
                return TrainingProgram != null && TrainingProgram.HasAccreditations ||
                       Accreditations != null && Accreditations.Count > 0;
            }
            internal set { }
        }

        #region Display 

        [DataMember(Name = "displayDueDate")]
        public DisplayDate DisplayDueDate
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayDate displayDate = new DisplayDate();
                
                DateTime? dueDate = DueDate;

                if (IsRelativeDueDate)
                {
                    dueDate = DateTime.UtcNow.AddMinutes(MinutesUntilExpired);
                    displayDate.Format = DateFormat.DateTime;

                    if (dueDate < DateTime.UtcNow)
                    {
                        displayDate.Messages.Add(Text.Session_DueDatePassed);
                        displayDate.Format = DateFormat.Empty;
                    }
                }
                else if (!dueDate.HasValue || dueDate.Value == DateTime.MinValue)
                {
                    displayDate.Format = DateFormat.Date;

                    if (TrainingProgram != null && CourseTypeID == (int)CourseType.Online)
                    {
                        dueDate = TrainingProgram.DueDate ?? TrainingProgram.EndDate;
                    }
                }
                else if (CourseTypeID == (int)CourseType.Classroom)
                {
                    
                    
                    if (RegistrationID > 0 && RegistrationStatusID == (int)RegistrationStatusType.Registered && EndDate.HasValue && EndDate.Value != DateTime.MinValue)
                    {
                        displayDate.Format = DateFormat.Date;
                        dueDate = EndDate;
                    }
                    else
                    {
                        displayDate.Format = DateFormat.Empty;
                        displayDate.Messages.Add(Text.Dash);
                    }
                }
                else if (IsUsingSession)
                {
                    if (TrainingProgram != null && TrainingProgram.IsSessionRegistered)
                    {
                        displayDate.Format = DateFormat.DateTime;

                        if (dueDate < DateTime.UtcNow)
                        {
                            displayDate.Messages.Add(Text.Session_DueDatePassed);
                            displayDate.Format = DateFormat.Empty;
                        }
                    }
                    // Making this explicit
                    else if (TrainingProgram != null && !TrainingProgram.IsSessionRegistered)
                    {
                        displayDate.Messages.Add(Text.Session_RegistrationRequiredDetail);
                        displayDate.Format = DateFormat.Empty;
                    }
                    else
                    {
                        // This shouldn't occur, but leaving it in place.
                        // This would mean there is no training program set
                        // on this course. It should be set if there is a
                        // session. 
                        displayDate.Messages.Add(Text.Dash);
                        displayDate.Format = DateFormat.Empty;
                    }
                }
                else if (TrainingProgram != null && TrainingProgram.DisplayDueDate != null && TrainingProgram.DisplayDueDate.Date.HasValue)
                {
                    return TrainingProgram.DisplayDueDate;
                }
                else
                {
                    displayDate.Messages.Add(Text.Dash);
                    displayDate.Format = DateFormat.Empty;
                }

                displayDate.Date = dueDate;

                return displayDate;
            }
            internal set
            {

            }
        }

        public override DisplayLink GetDisplayLink(DateTime? minDate, DateTime? maxDate, List<Text> unavailableReasons)
        {
            if (TrainingProgram != null) {
                bool isRequired = SyllabusTypeID == (int)SyllabusType.Required;
                bool isEnforceOrder = TrainingProgram.EnforceRequiredOrder || TrainingProgram.EnforceCanMoveForwardIndicator;

                unavailableReasons = new List<Text>();
                maxDate = TrainingProgram.DueDate ?? TrainingProgram.EndDate;
                minDate = TrainingProgram.StartDate;
                
                if (isRequired && isEnforceOrder) {
                    if (CourseTypeID == (int)CourseType.Online)
                    {
                        bool previousRequiredCourse = TrainingProgram.HasPreviousRequiredCourse(this);
                        bool previousCourseRequiringApproval = TrainingProgram.HasPreviousCourseRequiringApproval(this);

                        if (previousCourseRequiringApproval)
                        {
                            unavailableReasons.Add(Text.Course_ApprovalRequired);
                        }

                        if (previousRequiredCourse && unavailableReasons.Count == 0)
                        {
                            unavailableReasons.Add(Text.Course_PreviousRequired);
                        }
                    }
                    else 
                    {
                        TrainingProgramCourse previous = TrainingProgram.GetPreviousClassroomCourse(this);
                        TrainingProgramCourse next = TrainingProgram.GetNextClassroomCourse(this);

                        if (previous != null && previous.RegistrationID > 0)
                        {
                            maxDate = next != null ? next.StartDate : maxDate;
                        }
                        else if (previous == null && next != null && next.RegistrationID > 0)
                        {
                            maxDate = next.StartDate;
                        }
                        else if (previous != null && previous.RegistrationID == 0)
                        {
                            unavailableReasons.Add(Text.Course_PreviousRegistrationRequired);
                        }
                    }
                }
            }

            return base.GetDisplayLink(minDate, maxDate, unavailableReasons);
        }

        [DataMember(Name = "displayLaunchSteps")]
        public List<DisplayLaunchStep> DisplayLaunchSteps
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                bool _isSessionExpired = TrainingProgram.IsUsingSessions && !TrainingProgram.IsSessionActive;
                bool _isCompleted = TrainingProgram.CourseUnlockModeID == (int)CourseUnlockMode.AfterCourseCompletion &&
                                    Passed == 1 &&
                                    !IsAssignmentRequired;

                List<DisplayLaunchStep> _launchSteps = new List<DisplayLaunchStep>();
                if (!IsCourseWorkAllowed)
                {
                    _launchSteps.Add(DisplayLaunchStep.CourseWorkLaunchStep);
                }
                if (_isSessionExpired || _isCompleted)
                {
                    _launchSteps.Add(DisplayLaunchStep.SessionWarningLaunchStep);
                }
                if (!IsUserValidationAllowed)
                {
                    _launchSteps.Add(DisplayLaunchStep.ValidationRequiredLaunchStep);
                }
                if (ProctorRequiredIndicator.HasValue && ProctorRequiredIndicator.Value)
                {
                    _launchSteps.Add(DisplayLaunchStep.ProctorRequiredLaunchStep);
                }
                if (AffidavitID > 0)
                {
                    DisplayLaunchStep affidavitStep = DisplayLaunchStep.AffidavitRequiredLaunchStep;
                    AffidavitController ac = new AffidavitController();
                    var result = ac.FindAffidavit(AffidavitID);
                    if (result.Success && result.Data != null) 
                    {
                        affidavitStep.Content = JsonString.Dynamic(result.Data.AffidavitJSON);
                    }
                    _launchSteps.Add(DisplayLaunchStep.AffidavitRequiredLaunchStep);
                }

                return _launchSteps;
            }
            internal set { }
        }

        [DataMember(Name="displayLaunchStepsV2")]
        public List<DisplayLaunchStep> DisplayLaunchStepsV2
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                int _userId = SymphonyController.GetUserID();
                bool _isProfessionRequired = HasAccreditations && TrainingProgram != null && !TrainingProgram.HasProfession;
                bool _hasDisclaimers = HasAccreditations;
                bool _isSessionExpired = TrainingProgram != null && TrainingProgram.IsUsingSessions && !TrainingProgram.IsSessionActive;
                bool _isCompleted = TrainingProgram != null && TrainingProgram.CourseUnlockModeID == (int)CourseUnlockMode.AfterCourseCompletion && 
                                    Passed == 1 && 
                                    !IsAssignmentRequired;

                bool _prevalidationRequired = TrainingProgram != null && TrainingProgram.PreTestValidation.HasValue && TrainingProgram.PreTestValidation.Value;
                
                string launchStepUrlTemplate = "/services/courseassignment.svc/launchstep/{0}/{1}/{2}/{3}";
                
                List<DisplayLaunchStep> _launchSteps = new List<DisplayLaunchStep>();
                if (!IsCourseWorkAllowed)
                {
                    _launchSteps.Add(DisplayLaunchStep.CourseWorkLaunchStep);
                }
                if (_isSessionExpired || _isCompleted)
                {
                    _launchSteps.Add(DisplayLaunchStep.SessionWarningLaunchStep);
                }
                if (!IsUserValidationAllowed)
                {
                    DisplayLaunchStep step = DisplayLaunchStep.ValidationRequiredLaunchStep.GetInstance();
                    step.ContentUrl = string.Format(launchStepUrlTemplate, step.Code, TrainingProgram.Id, Id, _userId);
                    _launchSteps.Add(step);
                }
                if (ProctorRequiredIndicator.HasValue && ProctorRequiredIndicator.Value)
                {
                    DisplayLaunchStep step = DisplayLaunchStep.ProctorRequiredLaunchStep.GetInstance();
                    step.ContentUrl = string.Format(launchStepUrlTemplate, step.Code, TrainingProgram.Id, Id, _userId);
                    _launchSteps.Add(step);
                }
                if (AffidavitID > 0)
                {
                    DisplayLaunchStep step = DisplayLaunchStep.AffidavitRequiredLaunchStep.GetInstance();
                    int courseId = this.SyllabusTypeID == (int)SyllabusType.Final ? Id : 0;

                    step.Content = JsonString.Dynamic(new Data.AffidavitFinalExam(AffidavitID).AffidavitJSON);
                    step.ContentUrl = string.Format(launchStepUrlTemplate, step.Code, TrainingProgram.Id, courseId, _userId);
                    _launchSteps.Add(step);
                }
                if (_isProfessionRequired)
                {
                    DisplayLaunchStep step = DisplayLaunchStep.ProfessionRequiredLaunchStep.GetInstance();
                    step.ContentUrl = string.Format(launchStepUrlTemplate, step.Code, TrainingProgram.Id, Id, _userId);
                    _launchSteps.Add(step);
                }
                if (_hasDisclaimers)
                {
                    DisplayLaunchStep step = DisplayLaunchStep.AccreditationDisclaimerLaunchStep.GetInstance();
                    step.ContentUrl = string.Format(launchStepUrlTemplate, step.Code, TrainingProgram.Id, Id, _userId);
                    _launchSteps.Add(step);
                }

                if (_prevalidationRequired)
                {
                    DisplayLaunchStep step = DisplayLaunchStep.PrevalidationRequiredLaunchStep.GetInstance();
                    step.ContentUrl = string.Format(launchStepUrlTemplate, step.Code, TrainingProgram.Id, Id, _userId);
                    _launchSteps.Add(step);
                }

                // because many of these steps require a response from the server, 
                // we need to block the course from launching until everything is complete
                if (_launchSteps.Count > 0)
                {
                    DisplayLaunchStep step2 = DisplayLaunchStep.ConfirmatiionLaunchStep.GetInstance();
                    step2.ContentUrl = string.Format(launchStepUrlTemplate, step2.Code, TrainingProgram.Id, Id, _userId);
                    _launchSteps.Add(step2);
                }
                return _launchSteps;
            }
            internal set { }
        }

        [DataMember(Name = "displayPrint")]
        public bool DisplayPrint
        {
            get
            {
                // courses in a training program that are completed can potentially be printed
                if (this.TrainingProgram != null)
                {
                    if (this.Completed && this.DeliveredArtisanCourseID > 0)
                    {
                        if (this.AllowPrinting.HasValue)
                        {
                            return this.AllowPrinting.Value;
                        }
                        return this.TrainingProgram.AllowPrinting;
                    }
                    return false;
                }
                return false;
            }
            internal set { }
        }
        [DataMember(Name = "displayPrintIcon")]
        public DisplayIconState DisplayPrintIcon
        {
            get
            {
                if (DisplayPrint)
                {
                    return new DisplayIconState()
                    {
                        Messages = new List<Text>() { Text.Course_Print },
                        Icon = IconState.Print,
                        Link = new DisplayLink()
                        {
                            BaseUrl = Url.PrintCourse,
                            UrlFormat = TextFormat.PrintCourseFormat,
                            Title = Text.Print,
                            IsNewWindow = true,
                            QueryParams = new KeyValueList()
                            {
                                new DisplayKeyValue(){ Key = "ArtisanCourseID", Value = DeliveredArtisanCourseID.ToString() }
                            }
                        }
                    };
                }
                else
                {
                    return new DisplayIconState()
                    {
                        IsEmpty = true,
                        Icon = IconState.Empty
                    };
                }
            }
            internal set { }
        }

        #endregion

    }
}
