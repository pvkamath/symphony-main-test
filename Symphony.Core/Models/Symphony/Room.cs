﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "room")]
    [DefaultSort(ColumnName = "Name")]
    public class Room : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "internalCode")]
        public string InternalCode { get; set; }

        [DataMember(Name = "roomCost")]
        public decimal RoomCost { get; set; }

        [DataMember(Name = "capacity")]
        public int Capacity { get; set; }

        [DataMember(Name = "networkAccessIndicator")]
        public bool NetworkAccessIndicator { get; set; }

        [DataMember(Name = "lockedIndicator")]
        public bool LockedIndicator { get; set; }

        [DataMember(Name = "venueId")]
        public int VenueID { get; set; }
    }
}
