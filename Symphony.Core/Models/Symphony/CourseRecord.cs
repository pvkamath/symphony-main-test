﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name="courseRecord")]
    public class CourseRecord : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "courseTypeId")]
        public int CourseTypeID { get; set; }

        [DataMember(Name = "courseKey")]
        public string CourseKey { 
            get {
                return Id + "-" + CourseTypeID;
            } internal set {}
        }

    }
}
