﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using SubSonic;
using Symphony.Core.Comparers;
using Symphony.Core.Extensions;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "trainingProgram")]
    [DefaultSort(ColumnName = "Name")]
    public class TrainingProgramLookup : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

    }
}
