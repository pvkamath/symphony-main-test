﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "parameter")]
    [DefaultSort(ColumnName = "[Parameter].[Name]")]
    public class Parameter : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "parameterSetId")]
        public int ParameterSetID { get; set; }

        [DataMember(Name = "parameterSetName")]
        public string ParameterSetName { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "defaultValue")]
        public string DefaultValue { get; set; }

        [DataMember(Name = "values")]
        public List<ParameterValue> Values { get; set; }
    }
}
