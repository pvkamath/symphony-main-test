﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;

namespace Symphony.Core.Models
{
    [DataContract(Name = "artisanAsset")]
    public class ArtisanAsset : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "path")]
        public string Path { get; set; }

        [DataMember(Name = "filename")]
        public string Filename { get; set; }

        [DataMember(Name = "assetTypeId")]
        public int AssetTypeId { get; set; }

        [DataMember(Name = "template")]
        public string Template { get; set; }

        [DataMember(Name = "previewTemplate")]
        public string PreviewTemplate { get; set; }

        [DataMember(Name = "keywords")]
        public string Keywords { get; set; }

        [DataMember(Name = "isPublic")]
        public bool IsPublic { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "width")]
        public int? Width { get; set; }

        [DataMember(Name = "height")]
        public int? Height { get; set; }

        [DataMember(Name = "alternateHtml")]
        public string AlternateHtml { get; set; }

        [DataMember(Name = "hasAlternateHtml")]
        public bool HasAlternateHtml { get; set; }
    }
}
