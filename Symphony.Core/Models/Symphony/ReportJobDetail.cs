using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportJobDetail")]   
    public class ReportJobDetail : Model
    {

        public ReportJobDetail()
        {
            this.Job = new ReportJobs();
            this.Queue = new ReportQueues();
            this.Trigger = new ReportJobTriggers();
            this.Parameters = new ReportParameters();
        }

         [DataMember(Name = "job", IsRequired = false)]
         public ReportJobs Job { get ; set; }

         [DataMember(Name = "queue", IsRequired = false)]
         public ReportQueues Queue { get; set; }

         [DataMember(Name = "trigger", IsRequired = false)]
         public ReportJobTriggers Trigger { get; set; }

         [DataMember(Name = "parameters", IsRequired = false)]
         public ReportParameters Parameters { get; set; }

      
    }
}
