﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "classDate")]
    [DefaultSort(ColumnName = "StartDateTime")]
    public class ClassDate : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        [DataMember(Name = "startDateTime")]
        public DateTime StartDateTime { get; set; }

        [DataMember(Name = "duration")]
        public int? Duration { get; set; }

    }
}
