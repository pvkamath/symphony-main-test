﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "bulkDownloadRequest")]
    public class BulkDownloadRequest : Model
    {
        
        [DataMember(Name = "courseIds")]
        public int[] CourseIDs { get; set; }

        [DataMember(Name = "courseOverride")]
        public Course CourseOverride { get; set; }

        [DataMember(Name = "parameterOverrides")]
        public List<ParameterValue> ParameterOverrides { get; set; }

    }
}
