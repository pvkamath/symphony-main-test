using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportTypes")]
    [DefaultSort(ColumnName = "Name")]
    public class ReportTypes : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
		
		[DataMember(Name = "userNotes")]
        public string UserNotes { get; set; }
		
        [DataMember(Name = "isEnabled")]
        public int IsEnabled { get; set; }

        [DataMember(Name = "codeName")]
        public string CodeName { get; set; }
    }
}
