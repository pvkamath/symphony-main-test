﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Attributes;


namespace Symphony.Core.Models
{
    [DataContract(Name = "licenseSummaryReport")]
    public class LicenseSummaryReport
    {
        [DataMember(Name = "data")]
        public Dictionary<string, int> Data { get; set; }
    }
}
