﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract]
    public class PasswordRecover : Model
    {
        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "subdomain")]
        public string Subdomain { get; set; }
    }
}
