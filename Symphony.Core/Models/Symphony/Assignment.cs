﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="assignment")]
    public class Assignment : Model
    {
        
        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "onlineCourseId")]
        public int OnlineCourseID { get; set; }

        [DataMember(Name = "artisanSectionId")]
        public int ArtisanSectionID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "courseName")]
        public string CourseName { get; set; }

        [DataMember(Name = "totalQuestions")]
        public int TotalQuestions { get; set; }

        [DataMember(Name = "submittedOn")]
        public DateTime SubmittedOn { get; set; }

        [DataMember(Name = "questionsAnswered")]
        public int QuestionsAnswered { get; set; }

        [DataMember(Name = "questionsMarked")]
        public int QuestionsMarked { get; set; }

        [DataMember(Name = "questionsCorrect")]
        public int QuestionsCorrect { get; set; }

        [DataMember(Name = "questionsFailed")]
        public int QuestionsFailed { get; set; }

        #region Temporary Backwards Compatibiliity
        // Changed names of some fields due to the 
        // new assignment summary table. 
        // Leaving these here until any dependencies
        // are updated.
        [DataMember(Name = "gradedDate")]
        public DateTime? GradedDate {
            get
            {
                return GradedOn;
            }
            set
            {
                GradedOn = value;
            }
        }
        [DataMember(Name = "date")]
        public DateTime Date
        {
            get
            {
                return SubmittedOn;
            }
            set
            {
                SubmittedOn = value;
            }
        }
        [DataMember(Name = "numQuestions")]
        public int NumQuestions {
            get
            {
                return QuestionsAnswered;
            }
            set
            {
                QuestionsAnswered = value;
            }
        }

        [DataMember(Name = "numCorrect")]
        public int NumCorrect {
            get
            {
                return QuestionsCorrect;
            }
            set
            {
                QuestionsCorrect = value;
            }
        }
        [DataMember(Name = "courseId")]
        public int CourseID {
            get
            {
                return OnlineCourseID;
            }
            set
            {
                OnlineCourseID = value;
            }
        }
        [DataMember(Name = "sectionId")]
        public int SectionID
        {
            get
            {
                return ArtisanSectionID;
            }
            set
            {
                ArtisanSectionID = value;
            }
        }
        [DataMember(Name = "attempt")]
        public int Attempt
        {
            get
            {
                return AttemptID;
            }
            set
            {
                AttemptID = value;
            }
        }
        #endregion

        [DataMember(Name = "gradedOn")]
        public DateTime? GradedOn { get; set; }

        [DataMember(Name = "attemptId")]
        public int AttemptID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "score")]
        public double Score { get; set; }

        [DataMember(Name = "passingScore")]
        public int PassingScore { get; set; }

        [DataMember(Name = "isPassed")]
        public bool IsPassed
        {
            get
            {
                double passing = (double)PassingScore;
                double score = Score;

                if (score >= passing)
                {
                    return true;
                }
                return false;
            }
            internal set { }
        }

        [DataMember(Name = "isMarked")]
        public bool IsMarked { get; set; }

        [DataMember(Name = "isOld")]
        public bool IsOld { get; set; }

        public string Percent
        {
            get
            {
                return Math.Round(Score, 0) + "%";
            }
        }

        [DataMember(Name = "artisanCourseID")]
        public int ArtisanCourseID { get; set; }

        [DataMember(Name = "isOldArtisanCourse")]
        public bool IsOldArtisanCourse { get; set; }

        // Total number of assignments for the course that this 
        // assignment is part of. 
        [DataMember(Name = "totalAssignments")]
        public int TotalAssignments { get; set; }

        [DataMember(Name = "groupId")]
        public int GroupId { get; set; }

        public string GroupKey
        {
            get
            {
                return CourseID + "-" + AttemptID;
            }
            internal set { }
        }


        [DataMember(Name = "assignmentSummaryId")]
        public int AssignmentSummaryID { get; set; }

        #region Display
        [DataMember(Name = "displayAssignmentStatus")]
        public DisplayIconState DisplayAssignmentStatus
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                DisplayIconState state = new DisplayIconState();

                if (IsOld)
                {
                    state.Messages.Add(Text.Course_Assignments_State_Resubmitted);
                    state.Icon = IconState.Retry;
                }
                else if (NumQuestions < TotalQuestions)
                {
                    state.Messages.Add(Text.Course_Assignments_State_Incomplete);
                    state.Icon = IconState.Error;
                }
                else if (IsMarked == false)
                {
                    state.Messages.Add(Text.Course_Assignments_State_Unmarked);
                    state.Icon = IconState.New;
                }
                else if (IsPassed)
                {
                    state.Messages.Add(Text.Course_Assignments_State_Passed);
                    state.Icon = IconState.Complete;
                } else {
                    state.Messages.Add(Text.Course_Assignments_State_Failed);
                    state.Icon = IconState.Incomplete;
                }

                return state;
            }
            internal set { }
        }
        [DataMember(Name = "displaySubmitDate")]
        public DisplayDate DisplaySubmitDate
        {
            get
            {
                if (!SymphonyController.IsServerSideUiLogic) { return null; }

                return new DisplayDate()
                {
                    Date = Date,
                    Format = DateFormat.DateTimeTz
                };
            }
            internal set { }
        }

        [DataMember(Name = "publishedArtisanCourseId")]
        public int PublishedArtisanCourseId { get; set; }
        #endregion
    }
}

