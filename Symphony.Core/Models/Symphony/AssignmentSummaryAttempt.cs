﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "assignmentSummaryAttempt")]
    public class AssignmentSummaryAttempt : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "trainingProgramID")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "onlineCourseID")]
        public int OnlineCourseID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "assignmentSummaryID")]
        public int AssignmentSummaryID { get; set; }

        [DataMember(Name = "attemptID")]
        public int AttemptID { get; set; }

        [DataMember(Name = "questionsAnswered")]
        public int QuestionsAnswered { get; set; }

        [DataMember(Name = "questionsMarked")]
        public int QuestionsMarked { get; set; }

        [DataMember(Name = "questionsCorrect")]
        public int QuestionsCorrect { get; set; }

        [DataMember(Name = "questionsFailed")]
        public int QuestionsFailed { get; set; }

        [DataMember(Name = "totalQuestions")]
        public int TotalQuestions { get; set; }

        [DataMember(Name = "gradedOn")]
        public DateTime? GradedOn { get; set; }

        [DataMember(Name = "sumbmittedOn")]
        public DateTime? SumbmittedOn { get; set; }

        [DataMember(Name = "score")]
        public int Score { get; set; }

        [DataMember(Name = "passingScore")]
        public int PassingScore { get; set; }

        [DataMember(Name = "isMarked")]
        public bool IsMarked { get; set; }

        [DataMember(Name = "isComplete")]
        public bool IsComplete { get; set; }

        [DataMember(Name = "isFailed")]
        public bool IsFailed { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "modifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [DataMember(Name = "createdBy")]
        public string CreatedBy { get; set; }

        [DataMember(Name = "modifiedBy")]
        public string ModifiedBy { get; set; }

    }
}
