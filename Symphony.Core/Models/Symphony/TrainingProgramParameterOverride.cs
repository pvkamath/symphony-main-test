﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "trainingProgramParameterOverride")]
    [DefaultSort(ColumnName = "parameterName")]
    public class TrainingProgramParameterOverride : Model
    {
        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "parameterSetOptionId")]
        public int ParameterSetOptionID { get; set; }

        [DataMember(Name = "parameterSetId")]
        public int ParameterSetID { get; set; }

        [DataMember(Name = "parameterId")]
        public int ParameterID { get; set; }

        [DataMember(Name = "parameterSetOptionValue")]
        public string ParameterSetOptionValue { get; set; }

        [DataMember(Name = "parameterName")]
        public string ParameterName { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "defaultValue")]
        public string DefaultValue { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }
    }
}
