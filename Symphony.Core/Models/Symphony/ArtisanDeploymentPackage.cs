﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract]
    public class ArtisanDeploymentPackage
    {
        public string CourseIdsText { get; set; }
        public string CustomerIdsText { get; set; }

        [DataMember(Name = "courseIds")]
        public List<int> CourseIds 
        {
            get
            {
                List<int> ids = new List<int>();
                string[] idStrs = CourseIdsText.Split(',');
                foreach (string s in idStrs)
                {
                    int i;
                    if (int.TryParse(s.Trim(), out i))
                    {
                        ids.Add(i);
                    }
                }
                return ids;
            }
            set
            {
                string[] intArrayStr = Array.ConvertAll<int, string>(value.ToArray(), new Converter<int, string>(Convert.ToString));
                CourseIdsText = String.Join(",", intArrayStr);
            }
        }

        [DataMember(Name = "customerIds")]
        public List<int> CustomerIds 
        {
            get
            {
                List<int> ids = new List<int>();
                string[] idStrs = CustomerIdsText.Split(',');
                foreach (string s in idStrs)
                {
                    int i;
                    if (int.TryParse(s.Trim(), out i))
                    {
                        ids.Add(i);
                    }
                }
                return ids;
            }
            set
            {
                string[] intArrayStr = Array.ConvertAll<int, string>(value.ToArray(), new Converter<int, string>(Convert.ToString));
                CustomerIdsText = String.Join(",", intArrayStr);
            }
        }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "notes")]
        public string Notes { get; set; }

        [DataMember(Name = "isUpdate")]
        public bool IsUpdate { get; set; }

        [DataMember(Name = "isUpdateOnly")]
        public bool IsUpdateOnly { get; set; }

        [DataMember(Name = "isAutoUpdate")]
        public bool IsAutoUpdate { get; set; }

        [DataMember(Name = "toArtisan")]
        public bool ToArtisan { get; set; }

        [DataMember(Name = "toSymphony")]
        public bool ToSymphony { get; set; }

        [DataMember(Name = "toMars")]
        public bool ToMars { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "importResults")]
        public List<bool> ImportResults { get; set; }

        [DataMember(Name = "importMessages")]
        public List<string> ImportMessages { get; set; }

        [DataMember(Name = "running")]
        public bool Running { get; set; }
    }
}
