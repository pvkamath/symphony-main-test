﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Attributes;


namespace Symphony.Core.Models
{
    [DataContract(Name = "LicenseAssignmentNote")]
    public class LicenseAssignmentNote : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "body")]
        public string Body { get; set; }

        [DataMember(Name = "created")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "modified")]
        public DateTime ModifiedOn { get; set; }

        [DataMember(Name = "licenseAssignmentId")]
        public int LicenseAssignmentId { get; set; }
    }
}
