﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Models
{
    [DataContract(Name = "book")]
    [DefaultSort(ColumnName="Name")]
    public class Book : Model, IAssociatedImage
    {
        [DataMember(Name = "associatedImageData")]
        public string AssociatedImageData { get; set; }

        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramId { get; set; }

        [DataMember(Name = "author")]
        public string Author { get; set; }

        [DataMember(Name = "typeFormat")]
        public string TypeFormat { get; set; }

        [DataMember(Name = "publisher")]
        public string Publisher { get; set; }

        [DataMember(Name = "isbn")]
        public string Isbn { get; set; }

    }
}
