﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    /// <summary>
    /// A certificate template is an html template that is used to generate certificates for
    /// users upon completion of a training program or a course.
    /// </summary>
    [DataContract(Name = "certificateTemplate")]
    public class CertificateTemplate : Model
    {
        static CertificateTemplate()
        {
            PagedQueryParams<CertificateTemplate>.RegisterSortableProperties(new Dictionary<string, string>
            {
                { "name", "Name" },
                { "description", "description" }
            });
        }

        /// <summary>
        /// Gets or sets the internal database id for the certificate template.
        /// </summary>
        [DataMember(Name = "id")]
        public int? ID { get; set; }

        /// <summary>
        /// Gets or sets the customer that the certificate is associated with.
        /// </summary>
        [DataMember(Name = "customerId")]
        public int? CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the type of certificate that the template generates.
        /// </summary>
        [DataMember(Name = "certificateType")]
        public int CertificateType { get; set; }

        /// <summary>
        /// Gets or sets a short name describing the certificate template.
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a detailed description of the certificate template. Generally, this is used
        /// to provide context to admins so that they know how certificates are being used and how
        /// changing a certificate template may affect other customers.
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the width of the certificate. This is used to allow precise sizing of
        /// certificates.
        /// </summary>
        [DataMember(Name = "width")]
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets the height of the certificate. This is used to allow precise sizing of
        /// certificates.
        /// </summary>
        [DataMember(Name = "height")]
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the path of the certificate template. This is used for legacy certificates
        /// only.
        /// </summary>
        [DataMember(Name = "path")]
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the parent node of the certificate template. This is used for displaying
        /// the templates in a tree. A null value indicates a root node.
        /// </summary>
        [DataMember(Name = "parentId")]
        public int? ParentID { get; set; }

        /// <summary>
        /// Gets or sets text describing how indented the template is in a tree hierarchy.
        /// </summary>
        [DataMember(Name = "levelIndentText")]
        public string LevelIndentText { get; set; }

        /// <summary>
        /// Gets or sets the html template of the certificate.
        /// </summary>
        [DataMember(Name = "content")]
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the certificate is deleted.
        /// </summary>
        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }

        [DataMember(Name = "isLegacy")]
        public bool isLegacy { get; set; }

        [DataMember(Name = "certificateTemplateId")]
        public int? CertificateTemplateId { get; set; }

        [DataMember(Name = "bgImage")]
        public string BgImage { get; set; }

        [DataMember(Name = "bgImageId")]
        public int? BgImageId { get; set; }

        [DataMember(Name = "bgImagePath")]
        public string BgImagePath { get; set; }

        
    }
}
