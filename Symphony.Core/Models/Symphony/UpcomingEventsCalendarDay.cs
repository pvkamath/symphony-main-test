﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "upcomingEventsCalendarDay")]
    public class UpcomingEventsCalendarDay : CalendarDay
    {
        [DataMember(Name = "events")]
        public List<UpcomingEvent> Events { get; set; }
    }
}
