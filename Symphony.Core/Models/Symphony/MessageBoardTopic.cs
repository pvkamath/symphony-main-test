﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "messageBoardTopic")]
    public class MessageBoardTopic : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "messageBoardId")]
        public int MessageBoardID { get; set; }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "isSticky")]
        public bool IsSticky { get; set; }

        [DataMember(Name = "isLocked")]
        public bool IsLocked { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "modifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [DataMember(Name = "createdBy")]
        public string CreatedBy { get; set; }

        [DataMember(Name = "modifiedBy")]
        public string ModifiedBy { get; set; }

        [DataMember(Name = "posts")]
        public int Posts { get; set; }

        [DataMember(Name = "lastPostBy")]
        public string LastPostBy { get; set; }

        [DataMember(Name = "lastPost")]
        public DateTime LastPost { get; set; }

        [DataMember(Name = "preview")]
        public string Preview { get; set; }

        [DataMember(Name = "newTopicPost")]
        public string NewTopicPost { get; set; }

        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }
    }
}
