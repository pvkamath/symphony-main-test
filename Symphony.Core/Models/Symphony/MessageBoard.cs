﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "messageBoard")]
    public class MessageBoard : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "modifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [DataMember(Name = "createdBy")]
        public string CreatedBy { get; set; }

        [DataMember(Name = "modifiedBy")]
        public string ModifiedBy { get; set; }

        [DataMember(Name = "messageBoardType")]
        public MessageBoardType MessageBoardType { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int? TrainingProgramId { get; set; }

        [DataMember(Name = "classId")]
        public int? ClassId { get; set; }

        [DataMember(Name = "onlineCourseId")]
        public int? OnlineCourseId { get; set; }

        [DataMember(Name = "courseName")]
        public string CourseName { get; set; }

        [DataMember(Name = "onlineCourseName")]
        public string OnlineCourseName { get; set; }

        [DataMember(Name = "trainingProgramName")]
        public string TrainingProgramName { get; set; }

        [DataMember(Name = "className")]
        public string ClassName { get; set; }

        [DataMember(Name = "posts")]
        public int Posts { get; set; }

        [DataMember(Name = "lastUpdated")]
        public DateTime LastUpdated { get; set; }

        [DataMember(Name = "lastPostBy")]
        public string LastPostBy { get; set; }

        [DataMember(Name = "enrolled")]
        public bool Enrolled { get; set; }

        [DataMember(Name = "isInstructor")]
        public bool IsInstructor { get; set; }

        [DataMember(Name = "isDisableTopicCreation")]
        public bool IsDisableTopicCreation { get; set; }
    }
}
