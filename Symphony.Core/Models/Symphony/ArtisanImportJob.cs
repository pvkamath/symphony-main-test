﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="artisanImportJob")]
    public class ArtisanImportJob : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "course")]
        public string Course { get; set; }

        [DataMember(Name = "section")]
        public string Section { get; set; }

        [DataMember(Name = "coursesProcessed")]
        public int CoursesProcessed { get; set; }

        [DataMember(Name = "sectionsProcessed")]
        public int SectionsProcessed { get; set; }

        [DataMember(Name = "pagesProcessed")]
        public int PagesProcessed { get; set; }

        [DataMember(Name = "linesProcessed")]
        public int LinesProcessed { get; set; }

        [DataMember(Name = "sectionCount")]
        public int SectionCount { get; set; }

        [DataMember(Name = "courseCount")]
        public int CourseCount { get; set; }

        [DataMember(Name = "lines")]
        public int Lines { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "modifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [DataMember(Name = "courseId")]
        public string CourseID { get; set; }

        [DataMember(Name = "running")]
        public bool Running { get; set; }

        [DataMember(Name = "errors")]
        public string Errors { get; set; }

        [DataMember(Name = "pathIssues")]
        public string PathIssues { get; set; }

        [DataMember(Name = "downloadTime")]
        public string DownloadTime { get; set; }

        [DataMember(Name = "processTime")]
        public string ProcessTime { get; set; }

        [DataMember(Name = "artisanTime")]
        public string ArtisanTime { get; set; }

        [DataMember(Name = "totalTime")]
        public string TotalTime { get; set; }

        [DataMember(Name = "totalFiles")]
        public int TotalFiles { get; set; }

        [DataMember(Name = "filesDuplicate")]
        public int FilesDuplicate { get; set; }

        [DataMember(Name = "filesDownloaded")]
        public int FilesDownloaded { get; set; }

        [DataMember(Name = "filesNotFound")]
        public int FilesNotFound { get; set; }

        [DataMember(Name = "filesError")]
        public int FilesError { get; set; }
    }
}
