﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using SubSonic;
using Symphony.Core.Comparers;
using Symphony.Core.Extensions;
using Symphony.Core.Controllers;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Models
{
    [DataContract(Name = "trainingProgramSession")]
    [DefaultSort(ColumnName = "Name")]
    public class TrainingProgramSession : TrainingProgram
    {
        public int SessionClassID { get; set; }

        public string SessionClassName { get; set; }

        public int SessionDailyDuration { get; set; }

        public int SessionNumberOfDays { get; set; }

        public DateTime MinSessionStartDate { get; set; }

        public DateTime MaxSessionStartDate { get; set; }

        public DateTime SessionEndDate
        {
            get
            {
                return MaxSessionStartDate.AddMinutes(SessionDailyDuration).WithUtcFlag();
            }
        }

        public DateTime SessionStartDate
        {
            get
            {
                return MinSessionStartDate.WithUtcFlag();
            }
        }
    }
}