﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "appLinkAuthenticationRepsonse")]
    [DefaultSort(ColumnName = "Name")]
    public class AppLinkAuthenticationResponse : Model
    {

        [DataMember(Name = "success")]
        public bool Success { get; set; }

        [DataMember(Name = "errors")]
        public string[] Errors { get; set; }

        [DataMember(Name = "token")]
        public string Token { get; set; }


    }
}
