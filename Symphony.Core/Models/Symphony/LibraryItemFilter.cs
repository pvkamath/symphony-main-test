﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "libraryItemFilter")]
    public class LibraryItemFilter
    {
        List<Category> _categories = new List<Category>();
        List<Author> _authors = new List<Author>();

        [DataMember(Name = "isShowAll")]
        public bool IsShowAll { get; set; }

        [DataMember(Name = "isHideInLibrary")]
        public bool IsHideInLibrary { get; set; }

        [DataMember(Name = "categoryId")]
        public int CategoryID { get; set; }

        [DataMember(Name = "isFavorite")]
        public bool IsFavorite { get; set; }

        [DataMember(Name = "isInProgress")]
        public bool IsInProgress { get; set; }

        [DataMember(Name = "isCompleted")]
        public bool IsCompleted { get; set; }

        [DataMember(Name = "isNotStarted")]
        public bool IsNotStarted { get; set; }

        [DataMember(Name = "categories")]
        public List<Category> Categories {
            get
            {
                if (_categories == null)
                {
                    _categories = new List<Category>();
                }
                return _categories;
            }
            set
            {
                _categories = value;
            }
        }

        [DataMember(Name = "authors")]
        public List<Author> Authors
        {
            get
            {
                if (_authors == null)
                {
                    _authors = new List<Author>();
                }
                return _authors;
            }
            set
            {
                _authors = value;
            }
        }

        [DataMember(Name = "libraryIds")]
        public int[] LibraryIds { get; set; }

        [DataMember(Name = "libraryItemTypeId")]
        public int? LibraryItemTypeId { get; set; }

        [DataMember(Name = "categoryIds")]
        public int[] CategoryIds { get; set; }

        [DataMember(Name = "secondaryCategoryIds")]
        public int[] SecondaryCategoryIds { get; set; }

        [DataMember(Name = "dateString")]
        public string DateString { get; set; }

        public ZonedDateRange? DateRange
        {
            get
            {
                try
                {
                    if (DateString.IndexOf(",") < 0)
                    {
                        DateString = DateString + ',' + DateString;
                    }

                    ZonedDateRange dateRange = DateRangeManager.Parse(DateString, TimeZoneInfo.Utc.Id);
                    return dateRange;
                }
                catch
                {
                    log4net.LogManager.GetLogger(typeof(LibraryItemFilter)).Debug("Could not parse date string: " + DateString);
                }

                return null;
            }
            internal set { }
        }
    }
}
