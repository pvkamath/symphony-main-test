﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;

namespace Symphony.Core.Models
{
    class AssignedTrainingProgramCourse : Course
    {
        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "key")]
        public string Key { get; set; }

        [DataMember(Name = "syllabusTypeID")]
        public int SyllabusTypeID { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "requiredSyllabusOrder")]
        public int RequiredSyllabusOrder { get; set; }

    }
}
