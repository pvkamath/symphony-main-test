﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "classroomCalendarDay")]
    public class ClassroomCalendarDay : CalendarDay
    {
        [DataMember(Name = "classes")]
        public List<CalendarClass> Classes { get; set; }
    }
}
