﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="artisanTemplate")]
    public class ArtisanTemplate : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "html")]
        public string Html { get; set; }

        [DataMember(Name = "thumbPath")]
        public string ThumbPath { get; set; }

        [DataMember(Name = "pageType")]
        public ArtisanPageType PageType { get; set; }

        [DataMember(Name = "questionType")]
        public ArtisanQuestionType QuestionType { get; set; }

        [DataMember(Name = "cssText")]
        public string CssText { get; set; }
    }
}
