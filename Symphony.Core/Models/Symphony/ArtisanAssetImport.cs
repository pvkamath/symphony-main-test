﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;

namespace Symphony.Core.Models
{
    [DataContract(Name = "artisanAssetImport")]
    public class ArtisanAssetImport : ArtisanAsset
    {
        // Only used for long import script to 
        // load the original name of a file from a SQL
        // statement with out the [PDF] (1 of 12) wrapping
        // so [PDF] document.pdf (1 of 10) = document.pdf
        [DataMember(Name = "originalName")]
        public string OriginalName { get; set; }

        
    }
}
