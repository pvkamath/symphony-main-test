﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Attributes;

namespace Symphony.Core.Models
{
    [DataContract(Name = "userReportingPermissions")]
    public class UserReportingPermission : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "userId")]
        public int UserId { get; set; }

        [DataMember(Name = "permissionTypeId")]
        public int PermissionTypeId { get; set; }

        [DataMember(Name = "permissionType")]
        public string PermissionType { get; set; }

        [DataMember(Name = "permissionTypeCode")]
        public string PermissionTypeCode { get; set; }

        [DataMember(Name = "objectId")]
        public int ObjectId { get; set; }

    }
}
