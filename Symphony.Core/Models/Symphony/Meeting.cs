﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using System.Web;
using Symphony.Web;
using Symphony.Core.GTM;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract]
    [DefaultSort(ColumnName = "StartDateTime", Direction = OrderDirection.Descending)]
    public class Meeting : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "startDateTime")]
        public DateTime StartDateTime { get; set; }

        [DataMember(Name = "endDateTime")]
        public DateTime EndDateTime { get; set; }

        [DataMember(Name = "gtmOrganizerId")]
        public int GTMOrganizerID { get; set; }

        [DataMember(Name = "isOrganizer")]
        public bool IsOrganizer { get; set; }

        [DataMember(Name = "meetingId")]
        public long MeetingID { get; set; }

        [DataMember(Name = "uniqueMeetingId")]
        public string UniqueMeetingID { get; set; }

        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "joinUrl")]
        public string JoinUrl { get; set; }

        [DataMember(Name = "meetingType")]
        public string MeetingType { get; set; }

        [DataMember(Name = "maxParticipants")]
        public int MaxParticipants { get; set; }

        [DataMember(Name = "timeZoneKey")]
        public int TimeZoneKey { get; set; }

        [DataMember(Name = "passwordRequired")]
        public bool PasswordRequired { get; set; }

        [DataMember(Name = "isEditable")]
        public bool IsEditable { get; set; }

        [DataMember(Name = "conferenceCallInfo")]
        public string ConferenceCallInfo { get; set; }

        [DataMember(Name = "internalInvitees")]
        public List<int> InternalInvitees { get; set; }

        [DataMember(Name = "externalInvitees")]
        public List<string> ExternalInvitees { get; set; }

        public string ExternalJoinUrl
        {
            get
            {
                string target = ConfigurationManager.AppSettings["PortalDomain"];
                if (string.IsNullOrEmpty(target))
                {
                    target = "http://portal.betraining.com/";
                }
                string customer = string.Empty;
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    customer = new SymphonyController().ApplicationName;
                }

                if (!target.EndsWith("/"))
                {
                    target += "/";
                }
                //tack on the site & customer id
                target += "site/?customer=" + customer;
                return target;
            }
        }

        public void AddExtraData(MeetingInfo meetingInfo)
        {
            this.MeetingID = meetingInfo.meetingId;

            string uniqueMeetingID = GTMController.GetFieldValue(meetingInfo.meetingFields, "uniqueMeetingId");
            if (!string.IsNullOrEmpty(uniqueMeetingID))
                this.UniqueMeetingID = uniqueMeetingID;

            string joinURL = GTMController.GetFieldValue(meetingInfo.meetingFields, "joinURL");
            if (!string.IsNullOrEmpty(joinURL))
                this.JoinUrl = joinURL;

            string maxParticipants = GTMController.GetFieldValue(meetingInfo.meetingFields, "maxParticipants");
            if (!string.IsNullOrEmpty(maxParticipants))
                this.MaxParticipants = int.Parse(maxParticipants);

            string confCallInfo = GTMController.GetFieldValue(meetingInfo.meetingFields, "conferenceCallInfo");
            if (!string.IsNullOrEmpty(confCallInfo))
                this.ConferenceCallInfo = confCallInfo;
        }



        //public void ApplyDefaults()
        //{
        //    // default to empty subject
        //    if (this.Subject == null)
        //        this.Subject = "";

        //    // default time zone to eastern
        //    if (this.TimeZoneKey == 0)
        //    {
        //        TimeZone timeZone = TimeZone.CurrentTimeZone;
        //        switch (timeZone.StandardName.ToLower())
        //        {
        //            case "pacific standard time":
        //                this.TimeZoneKey = GTMController.TimeZonePacific;
        //                break;
        //            case "mountain standard time":
        //                this.TimeZoneKey = GTMController.TimeZoneMountain;
        //                break;
        //            case "central standard time":
        //                this.TimeZoneKey = GTMController.TimeZoneCentral;
        //                break;
        //            case "eastern standard time":
        //                this.TimeZoneKey = GTMController.TimeZoneEastern;
        //                break;
        //            case "atlantic standard time":
        //                this.TimeZoneKey = GTMController.TimeZoneAtlantic;
        //                break;
        //            default:
        //                this.TimeZoneKey = GTMController.TimeZoneEastern;
        //                break;
        //        }
        //    }

        //    // default start date/time to 5 minutes from now
        //    if (this.StartDateTime == null || this.StartDateTime.Year == 1)
        //        this.StartDateTime = DateTime.Now.ToUniversalTime().AddMinutes(5).ToLocalTime();

        //    // default end date/time to 1 hour after start date/time
        //    if (this.EndDateTime == null || this.EndDateTime.Year == 1)
        //        this.EndDateTime = this.StartDateTime.ToUniversalTime().AddHours(1).ToLocalTime();

        //    // default to empty conference call info ("Free"?)
        //    if (this.ConferenceCallInfo == null)
        //        this.ConferenceCallInfo = "";

        //    // default to no password required
        //    if (this.PasswordRequired == null)
        //        this.PasswordRequired = false;

        //    // default to editable
        //    if (this.IsEditable == null)
        //        this.IsEditable = true;
        //}
    }
}
