﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract]
    public class ScheduleParameter : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name="canFilterComplete")]
        public bool CanFilterComplete { get; set; }

        [DataMember(Name = "codeName")]
        public string CodeName { get; set; }

        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

        [DataMember(Name = "validTemplateParameters")]
        public TreeNode ValidTemplateParameters { get; set; }

        [DataMember(Name = "possibleRecipients")]
        public List<RecipientGroup> PossibleRecipients { get; set; }

        [DataMember(Name = "scheduleParameterOption")]
        public int ScheduleParameterOption { get; set; }

        [DataMember(Name = "isConfigurableForTrainingProgram")]
        public bool IsConfigurableForTrainingProgram { get; set; }
    }
}
