﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;

namespace Symphony.Core.Models
{
    /// <summary>
    /// Can either be a class, or an online course.
    /// Used to list courses an instructor has access to. An instructor
    /// may be assigned directly to a class, or to a training program,
    /// or to both. This allows a list of online classes and regular
    /// classes together.
    /// </summary>
    [DataContract(Name="ClassOrOnlineCourse")]
    public class ClassOrOnlineCourse : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "isVirtual")]
        public bool IsVirtual { get; set; }

        [DataMember(Name = "minClassDate")]
        public DateTime MinClassDate { get; set; }

        [DataMember(Name = "courseTypeId")]
        public int CourseTypeID { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "trainingProgramName")]
        public string TrainingProgramName { get; set; }

        [DataMember(Name = "requiredSyllabusOrder")]
        public int RequiredSyllabusOrder { get; set; }

        [DataMember(Name = "syllabusTypeId")]
        public int SyllabusTypeId { get; set; }

        [DataMember(Name = "messageBoardId")]
        public int MessageBoardId { get; set; }

        [DataMember(Name = "trainingProgramMessageBoardId")]
        public int TrainingProgramMessageBoardID { get; set; }

        [DataMember(Name = "groupByTrainingProgramId")]
        public int GroupByTrainingProgramID { get; set; }

        [DataMember(Name = "parentTrainingProgramId")]
        public int ParentTrainingProgramID { get; set; }

        [DataMember(Name = "hasUnmarkedAssignments")]
        public bool HasUnmarkedAssignments { get; set; }
    }
}
