﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "authenticationUserData")]
    public class AuthenticationTokenResult
    {

        [DataMember(Name = "userId")]
        public string UserId { get; set; }

        [DataMember(Name = "success")]
        public bool Success { get; set; }


        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "loginData")]
        public AuthenticationTokenData LoginData
        {
            get
            {
                return AuthenticationToken.LoginUser;
            }
            set
            {
                AuthenticationToken = new AuthenticationToken(value);
            }
        }

        [IgnoreDataMember]
        public AuthenticationToken AuthenticationToken { get; set; }

        [DataMember(Name = "cookieTokenUrl")]
        public string Token
        {
            get
            {
                return AuthenticationToken.CreateTokenStringFromAuthenticationToken(AuthenticationToken);
            }
            set
            {
                AuthenticationToken = AuthenticationToken.CreateAuthenticationTokenFromTokenString(value);
            }

        }
    }

    [DataContract(Name = "authenticationUserData")]
    public class AuthenticationTokenData
    {
        [DataMember(Name="userName")]
        public string UserName { get; set; }

        [DataMember(Name = "customer")]
        public string Customer { get; set; }

        [DataMember(Name = "expires")]
        public DateTime Expires { get; set; }
    }

    [DataContract(Name = "authenticationToken")]
    public class AuthenticationToken
    {
        public AuthenticationToken(AuthenticationTokenData user)
        {
            LoginUser = user;
            user.Expires = DateTime.Now.AddDays(1);
            Signature = EncryptedUser();
        }

        [DataMember(Name = "signature")]
        public string Signature { get; set; }

        [DataMember(Name = "loginUser")]
        public AuthenticationTokenData LoginUser { get; set; }

        public string EncryptedUser()
        {
            return Utilities.RijndaelSimple.Encrypt(Utilities.Serialize(LoginUser));
        }
        
        public bool isValid()
        {
            return EncryptedUser() == Signature;
        }
        
        public bool isActive() 
        {
            return LoginUser.Expires >= DateTime.Now;
        }
        
        public static AuthenticationToken CreateAuthenticationTokenFromTokenString(string token)
        {
            AuthenticationToken authToken = Utilities.Deserialize<AuthenticationToken>(Utilities.RijndaelSimple.Decrypt(Utilities.Base64UrlDecode(token)));
            if (authToken.isValid() && authToken.isActive())
            {
                return authToken;
            }
            else
            {
                return null;
            }
        }
        public static string CreateTokenStringFromAuthenticationToken(AuthenticationToken token)
        {
            return Utilities.Base64UrlEncode(Utilities.RijndaelSimple.Encrypt(Utilities.Serialize(token)));
        }

    }
}
