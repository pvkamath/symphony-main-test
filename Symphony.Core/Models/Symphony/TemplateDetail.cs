﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Data;

namespace Symphony.Core.Models
{
    public class TemplateDetail
    {
        public TemplateDetail()
        {
        }

        public TemplateDetail(int templateID)
        {
            this.TemplateID = templateID;
        }

        private void Init()
        {
            Template template = new Template(TemplateID);

            this.SelectedRecipients = new List<RecipientGroup>();

            TemplatesToRecipientGroupsMapCollection maps = new TemplatesToRecipientGroupsMapCollection()
                .Where(TemplatesToRecipientGroupsMap.Columns.TemplateID, template.Id)
                .Load();

            foreach (TemplatesToRecipientGroupsMap map in maps)
            {
                RecipientGroup group = new Data.RecipientGroup(map.RecipientGroupID);
                if (group.Id > 0)
                    this.SelectedRecipients.Add(group);
            }

            // load all the possible schedule parameters
            ScheduleParameters = new ScheduleParameterCollection().Load();

            // load the settings for the schedule parameters based on the scheduled parameter ID
            TemplateOptions = new TemplateOptions(template);
        }

        private int _templateID;
        public int TemplateID
        {
            get { return _templateID; }
            set { this._templateID = value; Init(); }
        }


        public ScheduleParameterCollection ScheduleParameters { get; set; }

        public List<RecipientGroup> SelectedRecipients { get; set; }

        public TemplateOptions TemplateOptions { get; set; }

    }
}
