﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "venue")]
    [DefaultSort(ColumnName = "Name")]
    public class Venue : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "internalCode")]
        public string InternalCode { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "stateId")]
        public int StateID { get; set; }

        [DataMember(Name = "stateName")]
        public string StateName { get; set; }

        [DataMember(Name = "stateDescription")]
        public string StateDescription { get; set; }

        [DataMember(Name = "timeZoneId")]
        public int TimeZoneID { get; set; }

        [DataMember(Name = "timeZoneDescription")]
        public string TimeZoneDescription { get; set; }

        [DataMember(Name = "resourceManagers")]
        public List<VenueResourceManager> ResourceManagers { get; set; }

        [DataMember(Name = "resources")]
        public List<Resource> Resources { get; set; }

        [DataMember(Name = "rooms")]
        public List<Room> Rooms { get; set; }
    }
}
