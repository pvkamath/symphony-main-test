﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    /// <summary>
    /// Contains all data required to generate the accreditation statement prior to course launch. 
    /// This includes all accreditation boards assigned to the training program for the user's selected profession
    /// and the trainingprogram details. 
    /// 
    /// This should be used in conjuction with the accreditation razor template for rendering out
    /// all accreditation information. 
    /// 
    /// </summary>
    [DataContract(Name = "AccreditationDocument")]
    public class AccreditationDocument : Model
    {
        public TrainingProgram TrainingProgram { get; set; }
        public List<AccreditationBoard> AccreditationBoards { get; set; }
        public Profession Profession { get; set; }
        public List<Author> Authors { get; set; }
        public string AuthorText
        {
            get {
                string authorText = "";

                if (Authors.Count > 0)
                {
                    Author firstAuthor = Authors.First();
                    Author lastAuthor = Authors.Last();

                    foreach (Author author in Authors)
                    {
                        string authorName = author.FirstName + " " + author.LastName;

                        UserDataField suffixField = author.UserDataFields.Where(f => f.CodeName == "suffix").ToList().FirstOrDefault();
                        if (suffixField != null && !string.IsNullOrEmpty(suffixField.UserValue))
                        {
                            authorName += ", " + suffixField.UserValue;
                        }
                        
                        if (author == firstAuthor)
                        {
                            authorText += authorName;
                        }
                        else if (author == lastAuthor && Authors.Count > 1)
                        {
                            authorText += "; and " + authorName;
                        }
                        else
                        {
                            authorText += "; " + authorName;
                        }
                    }

                }
                return authorText;
            }
        }
    }
}
