﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "licenseAssignment")]
    public class LicenseAssignment : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerId { get; set; }

        [DataMember(Name = "customerName")]
        public string CustomerName { get; set; }

        [DataMember(Name = "userId")]
        public int UserId { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }
        
        [DataMember(Name = "locationId")]
        public int LocationId { get; set; }
        
        [DataMember(Name = "locationName")]
        public string LocationName { get; set; }

        [DataMember(Name = "jobRoleId")]
        public int JobRoleId { get; set; }

        [DataMember(Name = "jobRoleName")]
        public string JobRoleName { get; set; }

        [DataMember(Name = "audienceId")]
        public int AudienceId { get; set; }

        [DataMember(Name = "audienceName")]
        public string AudienceName { get; set; }

        [DataMember(Name = "entityName")]
        public string EntityName { get; set; }
        
        [DataMember(Name = "startDate")]
        public DateTime StartDate { get; set; }
        
        [DataMember(Name = "assignmentStatusId")]
        public int AssignmentStatusId { get; set; }

        [DataMember(Name = "assignmentStatus")]
        public string AssignmentStatus { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramId { get; set; }

        [DataMember(Name = "trainingProgramName")]
        public string TrainingProgramName { get; set; }

        [DataMember(Name = "onlineCourseId")]
        public int OnlineCourseId { get; set; }

        [DataMember(Name = "onlineCourseName")]
        public string OnlineCourseName { get; set; }

        [DataMember(Name = "parentObject")]
        public string ParentObject { get; set; }

        [DataMember(Name = "renewalSubmittedDate")]
        public DateTime? RenewalSubmittedDate { get; set; }

        [DataMember(Name = "licenseAssignmentId")]
        public int LicenseAssignmentId { get; set; }

        [DataMember(Name = "renewalLeadTimeInDays")]
        public int RenewalLeadTimeInDays { get; set; }

        [DataMember(Name = "licenseNumber")]
        public int LicenseNumber { get; set; }

        [DataMember(Name = "licenseName")]
        public string LicenseName { get; set; }

        [DataMember(Name = "expirationRuleId")]
        public int ExpirationRuleId { get; set; }

        [DataMember(Name = "expirationRuleAfterDays")]
        public int ExpirationRuleAfterDays { get; set; }

        [DataMember(Name = "expiryDate")]
        public DateTime ExpiryDate { set; get; }
        
        [DataMember(Name = "expiresOnDate")]
        public DateTime ExpiresOnDate {
            private set { }
            get
            {
                DateTime expiryDate;

                switch (this.ExpirationRuleId)
                {
                    case 1: // Ends after set number of days set on a per license basis
                        expiryDate = this.ExpiryDate;
                        break;
                    case 2: // Ends after date specified on the license
                        var now = DateTime.UtcNow;
                        expiryDate = now.AddDays(this.ExpirationRuleAfterDays - Convert.ToInt32((DateTime.UtcNow - this.StartDate).TotalDays));

                        break;
                    case 3: // Ends year end
                        expiryDate = new DateTime(DateTime.UtcNow.Year + 1, 1, 1);
                        break;
                    default:
                        expiryDate = DateTime.UtcNow;
                        break;
                }

                return expiryDate;
            }
        }


        [DataMember(Name = "daysUntilExpiry")]
        public int DaysUntilExpiry
        {
            get
            {
                int daysUntil = 0;

                switch (this.ExpirationRuleId)
                {
                    case 1: // Ends after set number of days set on a per license basis
                        daysUntil = Convert.ToInt32((this.ExpiryDate - DateTime.UtcNow).TotalDays);
                        break;
                    case 2: // Ends after date specified on the license
                        daysUntil = this.ExpirationRuleAfterDays - Convert.ToInt32((DateTime.UtcNow - this.StartDate).TotalDays);
                        break;
                    case 3: // Ends year end
                        daysUntil = Convert.ToInt32((new DateTime(DateTime.UtcNow.Year + 1, 1, 1) - DateTime.UtcNow).TotalDays);
                        break;
                }

                return daysUntil;
            }

            private set { }
        }

        [DataMember(Name = "renewByDate")]
        public DateTime RenewByDate
        {
            private set { }

            get
            {
                DateTime renewBy = this.ExpiresOnDate.AddDays(-this.RenewalLeadTimeInDays);
                return renewBy;
            }
        }
    }
}
