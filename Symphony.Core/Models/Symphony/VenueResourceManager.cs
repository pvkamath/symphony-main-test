﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "venueResourceManager")]
    [DefaultSort(ColumnName = "Name")]
    public class VenueResourceManager : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "resourceManagerId")]
        public int ResourceManagerID { get; set; }

        [DataMember(Name = "venueId")]
        public int VenueID { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "middleName")]
        public string MiddleName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "fullName")]
        public string FullName
        {
            get
            {
                return Model.FormatFullName(FirstName, MiddleName, LastName);
            }
            set { }
        }
    }
}
