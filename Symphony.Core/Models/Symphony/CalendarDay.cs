﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "calendarDay")]
    [KnownType(typeof(UpcomingEventsCalendarDay))]
    [KnownType(typeof(ClassroomCalendarDay))]
    public class CalendarDay
    {
        [DataMember(Name = "year")]
        public int Year { get; set; }

        [DataMember(Name = "month")]
        public int Month { get; set; }

        [DataMember(Name = "day")]
        public int Day { get; set; }

        [DataMember(Name = "inCurrentMonth")]
        public bool InCurrentMonth { get; set; }
    }
}
