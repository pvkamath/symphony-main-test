using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportJobRole")]
    [DefaultSort(ColumnName = "Name")]
    public class ReportJobRole : Model
    {
        [DataMember(Name = "jobRolekey")]
        public int JobRoleKey { get; set; }

        [DataMember(Name = "Name")]
        public string JobRoleName { get; set; }

        [DataMember(Name = "customerkey")]
        public int CustomerKey { get; set; }

        [DataMember(Name = "jobRoleLevelDetail")]
        public string JobRoleLevelDetail { get; set; }

        [DataMember(Name = "jobRoleId")]
        public int JobRoleId { get; set; }

        [DataMember(Name = "parentJobRoleId")]
        public int ParentJobRoleId { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerId { get; set; }

    }
}
