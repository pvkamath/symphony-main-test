﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "janrainResult")]
    public class JanrainResult : Model
    {
        [DataMember(Name = "stat")]
        public string Stat { get; set; }
    }
}
