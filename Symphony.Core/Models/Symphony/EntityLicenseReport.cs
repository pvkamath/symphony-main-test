﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Attributes;


namespace Symphony.Core.Models
{
    [DataContract(Name = "entityLicenseReport")]
    public class EntityLicenseReport : Model
    {
        [DataMember(Name = "entityName")]
        public string EntityName { get; set; }

        [DataMember(Name = "entityId")]
        public int EntityID { get; set; }

        [DataMember(Name = "entityType")]
        public string EntityType { get; set; }

        [DataMember(Name = "licenseName")]
        public string LicenseName { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "statusId")]
        public int StatusID { get; set; }

        [DataMember(Name = "expirationRuleId")]
        public int ExpirationRuleId { get; set; }

        [DataMember(Name = "expirationRuleAfterDays")]
        public int ExpirationRuleAfterDays { get; set; }

        [DataMember(Name = "renewalLeadTimeInDays")]
        public int RenewalLeadTimeInDays { get; set; }

        [DataMember(Name = "startDate")]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "expiryDate")]
        public DateTime ExpiryDate { get; set; }

        [DataMember(Name = "licenseId")]
        public int LicenseID { get; set; }

        [DataMember(Name = "licenseNumber")]
        public int LicenseNumber { get; set; }

        [DataMember(Name = "expiresOnDate")]
        public DateTime ExpiresOnDate
        {
            private set { }
            get
            {
                DateTime expiryDate;

                switch (this.ExpirationRuleId)
                {
                    case 1: // Ends after set number of days set on a per license basis
                        expiryDate = this.ExpiryDate;
                        break;
                    case 2: // Ends after date specified on the license
                        var now = DateTime.UtcNow;
                        expiryDate = now.AddDays(this.ExpirationRuleAfterDays - Convert.ToInt32((DateTime.UtcNow - this.StartDate).TotalDays));

                        break;
                    case 3: // Ends year end
                        expiryDate = new DateTime(DateTime.UtcNow.Year + 1, 1, 1);
                        break;
                    default:
                        expiryDate = DateTime.UtcNow;
                        break;
                }

                return expiryDate;
            }
        }

        [DataMember(Name = "daysUntilExpiry")]
        public int DaysUntilExpiry
        {
            get
            {
                int daysUntil = 0;
                switch (this.ExpirationRuleId)
                {
                    case 1: // Ends after set number of days set on a per license basis
                        daysUntil = Convert.ToInt32((this.ExpiryDate - DateTime.UtcNow).TotalDays);
                        break;
                    case 2: // Ends after date specified on the license
                        daysUntil = this.ExpirationRuleAfterDays - Convert.ToInt32((DateTime.UtcNow - this.StartDate).TotalDays);
                        break;
                    case 3: // Ends year end
                        daysUntil = Convert.ToInt32((new DateTime(DateTime.UtcNow.Year + 1, 1, 1) - DateTime.UtcNow).TotalDays);
                        break;
                } 
                
                return daysUntil;
            }

            private set { }
        }

        [DataMember(Name = "renewalSubmittedDate", IsRequired = false)]
        public DateTime RenewalSubmittedDate { get; set; }

        [DataMember(Name = "renewByDate")]
        public DateTime RenewByDate
        {
            private set { }

            get
            {
                DateTime renewBy = this.ExpiresOnDate.AddDays(-this.RenewalLeadTimeInDays);
                return renewBy;
            }
        }

        [DataMember(Name = "belongsToType", IsRequired = false)]
        public string BelongsToType { get; set; }

        [DataMember(Name = "belongsToId", IsRequired = false)]
        public string BelongsToId { get; set; }
    }
}
