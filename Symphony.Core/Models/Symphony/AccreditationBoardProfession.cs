﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    /// <summary>
    /// An accreditation board profession is a profession associated with an accreditation board.
    /// When a user takes a training program that is accreditated by an accreditation board, they
    /// will be prompted to select their profession from one of the professions associated with an
    /// accreditation board.
    /// 
    /// This model is shared with the AccreditationBoardProfessionWithUnassigned view. Some of the
    /// properties in this model will only be available if using this view.
    /// </summary>
    [DataContract(Name="accreditationBoardProfession")]
    public class AccreditationBoardProfession : Model
    {
        static AccreditationBoardProfession()
        {
            PagedQueryParams<AccreditationBoardProfession>.RegisterSortableProperties(new Dictionary<string, string>
            {
                { "name", "Name" }
            });
        }

        /// <summary>
        /// Gets or sets the unique database identifier for the accreditation board profession.
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the id of the accreditation board.
        /// </summary>
        [DataMember(Name = "accreditationBoardId")]
        public int AccreditationBoardID { get; set; }

        /// <summary>
        /// Gets or sets the id of the profession associated with the accreditation board.
        /// </summary>
        [DataMember(Name = "professionId")]
        public int ProfessionID { get; set; }

        /// <summary>
        /// Gets or sets the name of the profession.
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the profession is assigned to the 
        /// accreditation board.
        /// </summary>
        [DataMember(Name = "isAssigned")]
        public bool IsAssigned { get; set; }
    }
}
