﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract]
    public class PasswordChange : Model
    {
        [DataMember(Name = "oldPassword")]
        public string OldPassword { get; set; }

        [DataMember(Name = "newPassword")]
        public string NewPassword { get; set; }
    }
}
