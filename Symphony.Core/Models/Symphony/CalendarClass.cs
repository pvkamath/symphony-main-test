﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "calendarClass")]
    public class CalendarClass
    {
        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        [DataMember(Name = "classDateId")]
        public int ClassDateID { get; set; }

        [DataMember(Name = "startDateTime")]
        public DateTime StartDateTime { get; set; }

        [DataMember(Name = "isMinimum")]
        public bool IsMinimum { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "managerUsersAwaitingApproval", EmitDefaultValue = false)]
        public bool ManagerUsersAwaitingApprovalPass { get; set; }

        [DataMember(Name = "managerUsersWaitListed", EmitDefaultValue = false)]
        public bool ManagerUsersWaitListedPass { get; set; }

        [DataMember(Name = "managerUsersDenied", EmitDefaultValue = false)]
        public bool ManagerUsersDeniedPass { get; set; }

        [DataMember(Name = "managerUsersRegistered", EmitDefaultValue = false)]
        public bool ManagerUsersRegisteredPass { get; set; }

        [DataMember(Name = "managerAllClasses", EmitDefaultValue = false)]
        public bool ManagerAllClassesPass { get; set; }

        [DataMember(Name = "instructors", EmitDefaultValue = false)]
        public bool InstructorsPass { get; set; }

        [DataMember(Name = "resourceManagerRequiredResources", EmitDefaultValue = false)]
        public bool ResourceManagerRequiredResourcesPass { get; set; }

        [DataMember(Name = "resourceManagerNoRequiredResources", EmitDefaultValue = false)]
        public bool ResourceManagerNoRequiredResourcesPass { get; set; }

        [DataMember(Name = "supervisorUsersAwaitingApproval", EmitDefaultValue = false)]
        public bool SupervisorUsersAwaitingApprovalPass { get; set; }

        [DataMember(Name = "supervisorUsersWaitListed", EmitDefaultValue = false)]
        public bool SupervisorUsersWaitListedPass { get; set; }

        [DataMember(Name = "supervisorUsersDenied", EmitDefaultValue = false)]
        public bool SupervisorUsersDeniedPass { get; set; }

        [DataMember(Name = "supervisorUsersRegistered", EmitDefaultValue = false)]
        public bool SupervisorUsersRegisteredPass { get; set; }

        public int? ManagerUsersAwaitingApproval { get; set; }

        public int? ManagerUsersWaitListed { get; set; }

        public int? ManagerUsersDenied { get; set; }

        public int? ManagerUsersRegistered { get; set; }

        public int? Instructors { get; set; }

        public int? RequiredResources { get; set; }

        public int? SupervisorUsersAwaitingApproval { get; set; }

        public int? SupervisorUsersWaitListed { get; set; }

        public int? SupervisorUsersDenied { get; set; }

        public int? SupervisorUsersRegistered { get; set; }
    }
}
