﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "customerSettings")]
    public class CustomerSettings : Model
    {
        [DataMember(Name = "showPretestOverride")]
        public bool? ShowPretestOverride { get; set; }

        [DataMember(Name = "showPosttestOverride")]
        public bool? ShowPostTestOverride { get; set; }

        [DataMember(Name = "passingScoreOverride")]
        public int? PassingScoreOverride { get; set; }

        [DataMember(Name = "skinOverride")]
        public int? SkinOverride { get; set; }

        [DataMember(Name = "pretestTypeOverride")]
        public int? PretestTypeOverride { get; set; }

        [DataMember(Name = "posttestTypeOverride")]
        public int? PosttestTypeOverride { get; set; }

        [DataMember(Name = "navigationMethodOverride")]
        public int? NavigationMethodOverride { get; set; }

        [DataMember(Name = "bookmarkingMethodOverride")]
        public int? BookmarkingMethodOverride { get; set; }

        [DataMember(Name = "certificateEnabledOverride")]
        public bool? CertificateEnabledOverride { get; set; }

        [DataMember(Name = "disclaimerOverride")]
        public string DisclaimerOverride { get; set; }

        [DataMember(Name = "contactEmailOverride")]
        public string ContactEmailOverride { get; set; }

        [DataMember(Name = "showObjectivesOverride")]
        public bool? ShowObjectivesOverride { get; set; }

        [DataMember(Name = "pretestTestOutOverride")]
        public bool? PretestTestOutOverride { get; set; }

        [DataMember(Name = "retestMode")]
        public int RetestMode { get; set; }

        [DataMember(Name = "retries")]
        public int? Retries { get; set; }

        [DataMember(Name = "theme")]
        public ArtisanTheme Theme { get; set; }

        [DataMember(Name = "randomizeAnswersOverride")]
        public bool? RandomizeAnswersOverride { get; set; }

        [DataMember(Name = "randomizeQuestionsOverride")]
        public bool? RandomizeQuestionsOverride { get; set; }

        [DataMember(Name = "reviewMethodOverride")]
        public int? ReviewMethodOverride { get; set; }

        [DataMember(Name = "markingMethodOverride")]
        public int? MarkingMethodOverride { get; set; }

        [DataMember(Name = "inactivityMethodOverride")]
        public int? InactivityMethodOverride { get; set; }

        [DataMember(Name = "inactivityTimeoutOverride")]
        public int? InactivityTimeoutOverride { get; set; }

        [DataMember(Name = "isForceLogoutOverride")]
        public bool? IsForceLogoutOverride { get; set; }

        [DataMember(Name = "minimumPageTimeOverride")]
        public int? MinimumPageTimeOverride { get; set; }

        [DataMember(Name = "maximumQuestionTimeOverride")]
        public int? MaximumQuestionTimeOverride { get; set; }

        [DataMember(Name = "maxTimeOverride")]
        public int? MaxTimeOverride { get; set; }

        [DataMember(Name = "minTimeOverride")]
        public int? MinTimeOverride { get; set; }

        [DataMember(Name = "minLoTimeOverride")]
        public int? MinLoTimeOverride { get; set; }

        [DataMember(Name = "minScoTimeOverride")]
        public int? MinScoTimeOverride { get; set; }

        [DataMember(Name = "themeFlavorOverrideId")]
        public int? ThemeFlavorOverrideID { get; set; }

        [DataMember(Name = "themeFlavor")]
        public Theme ThemeFlavor { get; set; }

    }
}
