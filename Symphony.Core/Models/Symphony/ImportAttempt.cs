﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "importAttempt")]
    public class ImportAttempt<T> : Model
    {
        [DataMember(Name="success")]
        public bool Success { get; set; }

        [DataMember(Name = "error")]
        public string Error { get; set; }

        [DataMember(Name = "result")]
        public T Result { get; set; }
    }
}
