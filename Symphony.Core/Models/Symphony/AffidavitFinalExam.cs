﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    /// <summary>
    /// An AffidavitFinalExam is a sworn statement that a student must make before they are allowed
    /// to launch a course. This is a custom JSON component that varies per customer.
    /// </summary>
    [DataContract(Name = "affidavitFinalExam")]
    public class AffidavitFinalExam : Model
    {
        static AffidavitFinalExam()
        {
            PagedQueryParams<AffidavitFinalExam>.RegisterSortableProperties(new Dictionary<string, string>
            {
                { "name", "Name" },
                { "description", "descripton" }
            });
        }

        /// <summary>
        /// Gets or sets the unique database identifier for the Affidavit.
        /// </summary>
        [DataMember(Name = "id")]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name of the Affidavit. This is a required property.
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a brief description for the Affidavit. This is an optional property and has
        /// no effect other than to provide information to content editors.
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the "JSON" that describe the component. This isn't actually JSON, but an
        /// ExtJS config block. This is required.
        /// </summary>
        [DataMember(Name = "affidavitJSON")]
        public string AffidavitJSON { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the affidavit is deleted.
        /// </summary>
        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }
    }
}
