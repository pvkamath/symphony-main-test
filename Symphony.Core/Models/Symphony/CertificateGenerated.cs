﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;
using System.IO;

namespace Symphony.Core.Models
{
    [DataContract(Name = "certificateGenerated")]
    public class CertificateGenerated : Model
    {
        [DataMember(Name = "html")]
        public string Html { get; set; }

        [DataMember(Name = "bgImagePath")]
        public string BgImagePath { get; set; }

        [DataMember(Name = "width")]
        public int Width { get; set; }

        [DataMember(Name = "height")]
        public int Height { get; set; }

        [DataMember(Name = "isPreview")]
        public bool IsPreview { get; set; }
    }
}
