﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "classesFilter")]
    public class ClassesFilter
    {
        [DataMember(Name = "archived")]
        public bool Archived { get; set; }
    }
}
