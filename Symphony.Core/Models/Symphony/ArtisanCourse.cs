﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Models
{
    [DataContract(Name = "artisanCourse")]
    public class ArtisanCourse : Model, IAssociatedImage
    {
        public int CustomerId { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "associatedImageData")]
        public string AssociatedImageData { get; set; }

        [DataMember(Name = "keywords")]
        public string Keywords { get; set; }

        [DataMember(Name = "categoryId")]
        public int CategoryId { get; set; }

        [DataMember(Name = "secondaryCategories")]
        public List<Category> SecondaryCategories { get; set; }

        [DataMember(Name = "accreditations")]
        public List<ArtisanCourseAccreditation> Accreditations { get; set; }

        [DataMember(Name = "accreditation")]
        public string Accreditation { get; set; }

        [DataMember(Name = "authors")]
        public List<Author> Authors { get; set; }

        [DataMember(Name = "sections")]
        public List<ArtisanSection> Sections { get; set; }

        [DataMember(Name = "themeId")]
        public int ThemeId { get; set; }

        [DataMember(Name = "theme")]
        public ArtisanTheme Theme { get; set; }

        [DataMember(Name = "themeFlavorId")]
        public int? ThemeFlavorID { get; set; }

        [DataMember(Name = "themeFlavor")]
        public Theme ThemeFlavor { get; set; }

        [DataMember(Name = "completionMethod")]
        public ArtisanCourseCompletionMethod CompletionMethod { get; set; }

        [DataMember(Name = "navigationMethod")]
        public ArtisanCourseNavigationMethod NavigationMethod { get; set; }

        [DataMember(Name = "bookmarkingMethod")]
        public ArtisanCourseBookmarkingMethod BookmarkingMethod { get; set; }

        [DataMember(Name = "displayTestOnly")]
        public bool DisplayTestOnly { get; set; }

        [DataMember(Name = "displayQuestionFeedback")]
        public bool DisplayQuestionFeedback { get; set; }

        [DataMember(Name = "certificateEnabled")]
        public bool CertificateEnabled { get; set; }

        [DataMember(Name = "contactEmail")]
        public string ContactEmail { get; set; }

        [DataMember(Name = "disclaimer")]
        public string Disclaimer { get; set; }

        [DataMember(Name = "passingScore")]
        public int PassingScore { get; set; }

        [DataMember(Name = "objectives")]
        public string Objectives { get; set; }

        [DataMember(Name = "headerImageUrl")]
        public string HeaderImageURL { get; set; }

        [DataMember(Name = "isPublished")]
        public bool IsPublished { get; set; }

        [DataMember(Name = "isAutoPublish")]
        public bool IsAutoPublish { get; set; }

        [DataMember(Name = "originalCourseId")]
        public int? OriginalCourseId { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name = "modifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [DataMember(Name = "lastPublishedOn")]
        public DateTime? LastPublishedOn { get; set; }

        [DataMember(Name = "timeoutMethod")]
        public ArtisanCourseTimeoutMethod TimeoutMethod { get; set; }

        [DataMember(Name = "isForceLogout")]
        public bool IsForceLogout { get; set; }

        [DataMember(Name = "markingMethod")]
        public ArtisanCourseMarkingMethod MarkingMethod { get; set; }

        [DataMember(Name = "reviewMethod")]
        public ArtisanCourseReviewMethod ReviewMethod { get; set; }

        [DataMember(Name = "minimumPageTime")]
        public int MinimumPageTime { get; set; }

        [DataMember(Name = "maximumQuestionTime")]
        public int MaximumQuestionTime { get; set; }

        [DataMember(Name = "maxTime")]
        public int MaxTime { get; set; }

        [DataMember(Name = "minTime")]
        public int MinTime { get; set; }

        [DataMember(Name = "minLoTime")]
        public int MinLoTime { get; set; }

        [DataMember(Name = "minScoTime")]
        public int MinScoTime { get; set; }

        [DataMember(Name = "inactivityTimeout")]
        public int InactivityTimeout { get; set; }

        [DataMember(Name = "defaultParameters")]
        public List<ParameterValue> DefaultParameters { get; set; }

        [DataMember(Name = "parameters")]
        public string Parameters { get; set; }

        [DataMember(Name = "defaultParametersJson")]
        public string DefaultParametersJSON { get; set; }

        [DataMember(Name = "levelIndentText")]
        public string LevelIndentText { get; set; }

        [DataMember(Name = "externalId")]
        public string ExternalID { get; set; }

        [DataMember(Name = "externalCourseNumber")]
        public string ExternalCourseNumber { get; set; }

        [DataMember(Name = "popupMenu")]
        public string PopupMenu { get; set; }

        /// <summary>
        /// The duration of the course, set in minutes
        /// This is used to calculate the % complete in a training program
        /// </summary>
        [DataMember(Name = "duration")]
        public int Duration { get; set; }

        /// <summary>
        /// Toggles automatic duration calculation - will override the duration field with automatic druation
        /// </summary>
        [DataMember(Name = "isUseAutomaticDuration")]
        public bool IsUseAutomaticDuration { get; set; }

        /// <summary>
        /// Automatic duration calculated from the minimum course timers on the course
        /// </summary>
        [DataMember(Name = "automaticDuration")]
        public int AutomaticDuration { get; set; }

        [DataMember(Name = "finalContentPage")]
        public string FinalContentPage { get; set; }

        /// <summary>
        /// True if there is a page with an embedded iframe containing a pdf that needs
        /// the pdfjs viewer to be coppied to the content of the course.
        /// </summary>
        [IgnoreDataMember]
        public bool IsRequirePdfJs { get; set; }

        /// <summary>
        /// sets all ids on this course and all sections to -1
        /// </summary>
        public void ClearIds()
        {
            this.Id = -1;
            if (this.Sections != null)
            {
                foreach (var section in this.Sections)
                {
                    section.ClearIds();
                }
            }
        }

        /// <summary>
        /// Finds a page in this course with content that matches the specified content
        /// </summary>
        /// <param name="pageToFind"></param>
        /// <returns></returns>
        public ArtisanPage FindPageByHtml(string html)
        {
            foreach (var section in this.Sections)
            {
                var page = section.FindPageBy((page1) =>
                {
                    return page1.Html == html;
                });

                if (page != null)
                {
                    return page;
                }
            }
            return null;
        }

        public ArtisanPage FindPageByID(int id)
        {
            foreach (var section in this.Sections)
            {
                var page = section.FindPageBy((page1) =>
                {
                    return page1.Id == id;
                });

                if (page != null)
                {
                    return page;
                }
            }
            return null;
        }

        public ArtisanPage FindPageByName(string name)
        {
            foreach (var section in this.Sections)
            {
                var page = section.FindPageBy((page1) =>
                {
                    return page1.Name == name;
                });

                if (page != null)
                {
                    return page;
                }
            }
            return null;
        }

        [DataMember(Name = "mode")]
        public string Mode { get; set; }

        // yes, there's a mismatch. the end users always see content type, even though we have ID internally
        [DataMember(Name = "contentType")]
        public int ContentTypeID { get; set; }

        /// <summary>
        /// The media player to use - the name will match up with 
        /// one of the media players in Symphony.Core/Resources/Packager/js/media-players
        /// If the folder cannot be found, it will resort to the video.js player as a default
        /// </summary>
        [DataMember(Name = "mediaPlayer")]
        public string MediaPlayer { get; set; }

        #region Mastery
        [DataMember(Name = "isMasteryEnabled")]
        public bool IsMasteryEnabled {get; set;}

        [DataMember(Name = "masteryLevel")]
        public int MasteryLevel {get; set;}

        [DataMember(Name = "minimumMasteryQuestions")]
        public int? MinimumMasteryQuestions {get; set;}

        [DataMember(Name = "isUspap")]
        public bool IsUspap { get; set; }

        [DataMember(Name = "isAntiGuessing")]
        public bool IsAntiGuessing { get; set; }
        #endregion
    }
}
