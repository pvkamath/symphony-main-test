﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "lockedUser")]
    [KnownType(typeof(User))]
    public class LockedUser : User
    {
        [DataMember(Name="lockedCourseCount")]
        public int LockedCourseCount { get; set; }
    }
}
