﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "courseCompletionType")]
    [DefaultSort(ColumnName = "Description")]
    public class CourseCompletionType
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "codeName")]
        public string CodeName { get; set; }
    }
}
