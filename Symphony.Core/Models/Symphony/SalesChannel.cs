﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "salesChannel")]
    [DefaultSort(ColumnName = "Name")]
    public class SalesChannel : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "isLoginEnabled")]
        public bool IsLoginEnabled { get; set; }

        [DataMember(Name = "logo")]
        public string Logo { get; set; }

        [DataMember(Name="logoImageUrl")]
        public string LogoImageTag {
            get {
                return "<img src='" + Logo + "' />";
            }
            set { }
        }

        [DataMember(Name = "description")]
        public string description { get; set; }

        [DataMember(Name = "themeId")]
        public int ThemeId { get; set; }

        // used to load, don't serialize it though
        public int ParentSalesChannelID { get; set; }

        [DataMember(Name = "parentId")]
        public int ParentID
        {
            get { return ParentSalesChannelID; }
            set { ParentSalesChannelID = value; }
        }
    }
}
