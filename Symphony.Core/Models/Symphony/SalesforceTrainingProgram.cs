﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using SubSonic;

namespace Symphony.Core.Models
{
    [DataContract(Name="salesForceTrainingProgram")]
    public class SalesforceTrainingProgram : TrainingProgram
    {
        [DataMember(Name = "entitlementId")]
        public int EntitlementId { get; set; }

        [DataMember(Name = "salesforceEntitlementId")]
        public string SalesforceEntitlementId { get; set; }

        [DataMember(Name = "salesforceProductId")]
        public string SalesforceProductId { get; set; }

        [DataMember(Name = "salesforceCategory")]
        public string SalesforceCategory { get; set; }

        [DataMember(Name = "salesforceDuration")]
        public int SalesforceDuration { get; set; }

        [DataMember(Name = "salesforceDurationUnits")]
        public string SalesforceDurationUnits { get; set; }

        [DataMember(Name = "salesforcePaymentRefId")]
        public int SalesforcePaymentRefId { get; set; }

        [DataMember(Name = "salesforcePartnerCode")]
        public string SalesforcePartnerCode { get; set; }

        [DataMember(Name = "salesforceStartDate")]
        public DateTime SalesforceStartDate { get; set; }

        [DataMember(Name = "salesforceEndDate")]
        public DateTime SalesforceEndDate { get; set; }

        [DataMember(Name = "salesforcePurchaseDate")]
        public DateTime SalesforcePurchaseDate { get; set; }
    }
}