﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="assignmentResponse")]
    public class AssignmentResponse : Model
    {
        
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "courseId")]
        public int CourseID { get; set; }

        [DataMember(Name = "pageId")]
        public int PageID { get; set; }

        [DataMember(Name = "sectionId")]
        public int SectionID { get; set; }

        [DataMember(Name = "attempt")]
        public int Attempt { get; set; }

        [DataMember(Name = "date")]
        public DateTime Date { get; set; }

        [DataMember(Name = "response")]
        public string Response { get; set; }

        [DataMember(Name = "question")]
        public string Question { get; set; }

        [DataMember(Name = "correctResponse")]
        public string CorrectResponse { get; set; }

        [DataMember(Name = "responseStatus")]
        public int ResponseStatus { get; set; }

        [DataMember(Name = "instructorFeedback")]
        public string InstructorFeedback { get; set; }

        [DataMember(Name = "isAutoResponse")]
        public bool IsAutoResponse { get; set; }
    }
}

