﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="trainingProgramLicense")]
    [DefaultSort(ColumnName = "[dbo].[License].[Id]", Direction = OrderDirection.Ascending)]
    public class TrainingProgramLicense : License
    {
        [DataMember(Name = "approvalCode")]
        public string  ApprovalCode { get; set; }

        [DataMember(Name = "isPrimary")]
        public bool IsPrimary { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }
    }
}

