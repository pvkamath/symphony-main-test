﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "userTileAssignment")]
    [DefaultSort(ColumnName = "Name")]

    public class UserTileAssignment : Model
    {

        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "tileId")]
        public int TileID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "tileUri")]
        public string TileUri { get; set; }

        [DataMember(Name = "applicationId")]
        public int ApplicationId { get; set; }
        
        [DataMember(Name = "authenticationUri")]
        public string AuthenticationUri { get; set; }

        [DataMember(Name = "applicationName")]
        public string ApplicationName { get; set; }

        [DataMember(Name = "PassUserNamePass")]
        public bool PassUserNamePass { get; set; }

        [DataMember(Name = "credentialState")]
        public string CredentialState { get; set; }

        [DataMember(Name = "credentialID")]
        public int CredentialID { get; set; }

        [DataMember(Name = "credentialsUserName")]
        public string CredentialsUserName { get; set; }

        [DataMember(Name = "credentialsPassword")]
        public string CredentialsPassword { get; set; }
        [DataMember(Name = "pass")]
        public string Pass
        {
            get
            {
                if (string.IsNullOrEmpty(CredentialsPassword))
                {
                    return string.Empty;
                }
                return Utilities.RijndaelSimple.Decrypt(CredentialsPassword);
            }
            set
            {
                this.CredentialsPassword = Utilities.RijndaelSimple.Encrypt(value);
            }
        }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "isAssignedByLocation")]
        public bool IsAssignedByLocation { get; set; }
        [DataMember(Name = "isAssignedByJobRole")]
        public bool IsAssignedByJobRole { get; set; }
        [DataMember(Name = "isAssignedByAudience")]
        public bool IsAssignedByAudience { get; set; }
        [DataMember(Name = "isAssignedByUser")]
        public bool IsAssignedByUser { get; set; }
    }
}
