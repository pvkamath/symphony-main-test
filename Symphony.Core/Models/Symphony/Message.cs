﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "message")]
    [DefaultSort(ColumnName = "CreatedOn")]
    public class Message
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name="createdOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember(Name="recipientId")]
        public int RecipientID { get; set; }

        [DataMember(Name = "senderId")]
        public int SenderID { get; set; }

        [DataMember(Name = "isRead")]
        public bool IsRead { get; set; }

        [DataMember(Name = "body")]
        public string Body { get; set; }

        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "sender")]
        public string Sender { get; set; }

        [DataMember(Name = "recipient")]
        public string Recipient { get; set; }
    }
}
