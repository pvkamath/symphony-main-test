﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "appLInk")]
    [DefaultSort(ColumnName = "Name")]
    public class AppLink : Model
    {
        public AppLink()
        {
            this.CredentialsSetup = true;
            this.Hidden = false;
        }

        [DataMember(Name = "label")]
        public string Label { get; set; }

        [DataMember(Name = "linkUrl")]
        public string LinkUrl { get; set; }

        [DataMember(Name = "authenticationUrl")]
        public string AuthenticationUrl { get; set; }
        
        [DataMember(Name = "resourceUrl")]
        public string ResourceUrl { get; set; }

        [DataMember(Name = "appInstanceId")]
        public int AppInstanceId { get; set; }

        [DataMember(Name = "appName")]
        public string AppName { get; set; }

        [DataMember(Name = "hidden")]
        public bool Hidden { get; set; }

        [DataMember(Name = "credentialsSetup")]
        public bool CredentialsSetup { get; set; }

        [DataMember(Name = "tileUri")]
        public string TileUri { get; set; }

        [DataMember(Name = "tileId")]
        public int TileID { get; set; }
        
    }
}
