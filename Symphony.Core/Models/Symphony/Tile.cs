﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "tile")]
    [DefaultSort(ColumnName = "Name")]
    public class Tile : Model
    {

        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "tileUri")]
        public string TileUri { get; set; }

        [DataMember(Name = "applicationId")]
        public int ApplicationId { get; set; }

        [DataMember(Name = "applicationName")]
        public string ApplicationName { get; set; }

        [DataMember(Name = "authenticationUri")]
        public string AuthenticationUri { get; set; }

        [DataMember(Name = "assignment")]
        public List<HierarchyLink> Assignment { get; set; }
    }
}
