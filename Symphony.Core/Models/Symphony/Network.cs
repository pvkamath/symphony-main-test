﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "network")]
    [DefaultSort(ColumnName = "Name")]
    public class Network : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "codeName")]
        public string CodeName { get; set; }

        [DataMember(Name = "customerNetwork")]
        public List<Customer> CustomerNetwork { get; set; }

        [DataMember(Name = "customerNetworkDetails")]
        public List<CustomerNetworkDetail> CustomerNetworkDetails { get; set; }
     }
}
