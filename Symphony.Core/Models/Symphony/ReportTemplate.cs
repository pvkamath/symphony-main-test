using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportTemplate")]
    [DefaultSort(ColumnName = "Name")]
    public class ReportTemplate : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
		
		[DataMember(Name = "description")]
        public string description { get; set; }
		
        [DataMember(Name = "isEnabled")]
        public bool IsEnabled { get; set; }

        [DataMember(Name = "codeName")]
        public string CodeName { get; set; }

        [DataMember(Name = "reportSystem")]
        public int ReportSystem { get; set; }

        [DataMember(Name = "reportSystemParameters")]
        public string ReportSystemParameters { get; set; }

        

        [DataMember(Name = "reportEntities")]
        public List<ReportEntity> ReportEntities { get; set; }

        [DataMember(Name = "reportPermittedSalesChannels")]
        public List<SalesChannel> ReportPermittedSalesChannels { get; set; }

        [DataMember(Name = "reportPermittedCustomers")]
        public List<Customer> ReportPermittedCustomers { get; set; }

    }
}
