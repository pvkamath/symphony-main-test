﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using OnlineCourseAccreditationData = Symphony.Core.Data.OnlineCourseAccreditation;

namespace Symphony.Core.Models
{
    /// <summary>
    /// An OnlineCourseAccreditation is an accreditation granted to an OnlineCourse. These
    /// accreditations are an endorsement of the course by an AccreditationBoard. These
    /// accreditations are imported along with other Artisan data and are tied to a specific course.
    /// 
    /// OnlineCourseAccreditations do not have a status. They are assumed to be valid in the range
    /// defined by the startDate and expiryDate.
    /// </summary>
    [DefaultSort(ColumnName = "[dbo].[OnlineCourseAccreditation].[ID]")]
    [DataContract(Name="onlineCourseAccreditation")]
    public class OnlineCourseAccreditation : Model
    {
        static OnlineCourseAccreditation()
        {
            PagedQueryParams<OnlineCourseAccreditation>.RegisterSortableProperties(new Dictionary<string, string>
            {
                { "name", "name" },
                { "startDate", "startDate" },
                { "expiryDate", "expiryDate" },
                { "creditHours", "creditHours" },
                { "creditHoursLabel", "creditHoursLabel" },
                { "accreditationCode", "accreditationCode" },
                { "CourseNumber", "courseNumber" }
            });
        }

        /// <summary>
        /// Gets or sets the unique database identifier for the accreditation.
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the ID of the AccreditationBoard that issued the accreditation.
        /// </summary>
        [DataMember(Name = "accreditationBoardId")]
        public int AccreditationBoardId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the OnlineCourse that the accreditation is tied to.
        /// </summary>
        [DataMember(Name = "onlineCourseId")]
        public int OnlineCourseId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the Profession that the accreditation is associated with. This is
        /// optional and most accreditations will likely not be associated with a particular
        /// profession.
        /// </summary>
        [DataMember(Name = "professionId")]
        public int? ProfessionId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the original ArtisanCourseAccreditation that this accreditation
        /// was created from from.
        /// </summary>
        [DataMember(Name ="artisanAccreditationId")]
        public int ArtisanCourseAccreditationId { get; set; }

        /// <summary>
        /// Gets or sets the name of the accreditation board that granted the accreditation.
        /// </summary>
        [DataMember(Name = "accreditationBoardName")]
        public string AccreditationBoardName { get; set; }

        /// <summary>
        /// Gets or sets the name of the profession tied to the accreditation.
        /// </summary>
        [DataMember(Name = "professionName")]
        public string ProfessionName { get; set; }

        /// <summary>
        /// Gets or sets the date from which the accreditation is valid. This is a UTC date and is
        /// specific to the minute.
        /// </summary>
        [DataMember(Name = "startDate")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the date after which the accreditation expires. This is a UTC date and is
        /// specific to the minute.
        /// </summary>
        [DataMember(Name = "expiryDate")]
        public DateTime ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the number of credit hours that the accreditation is worth. Credit hours
        /// define how many "credits" each AccreditationBoard awards to a course.
        /// 
        /// This has an implication for licenses that are granted based on a number of credits.
        /// Different boards can weight courses differently, so this may vary per course.
        /// </summary>
        [DataMember(Name = "creditHours")]
        public float CreditHours { get; set; }

        /// <summary>
        /// Gets or sets the name that the AccreditationBoard uses when describing credit hours.
        /// Different accreditation boards may use different terms when referring to these. For
        /// example: "Credit Hours", "Credits", "Classroom Hours", etc.
        /// </summary>
        [DataMember(Name = "creditHoursLabel")]
        public string CreditHoursLabel { get; set; }

        /// <summary>
        /// Gets or sets the disclaimer that will be shown before starting the course. This may
        /// or may not have HTML formatting. This property is not required, but it is usually
        /// provided.
        /// </summary>
        [DataMember(Name = "disclaimer")]
        public string Disclaimer { get; set; }

        /// <summary>
        /// Gets or sets a unique accreditation code that the accreditation board assigns to the
        /// accreditation. This is the a number the board assigns us that indicates that the course
        /// is approved.
        /// </summary>
        [DataMember(Name = "accreditationCode")]
        public string AccreditationCode { get; set; }

        /// <summary>
        /// Gets or sets a unique course number for the accreditation. This is a number that the
        /// board uses internally to identify the course.
        /// </summary>
        [DataMember(Name = "courseNumber")]
        public string CourseNumber { get; set; }

        /// <summary>
        /// Gets or sets whether or not the accreditation is deleted. OnlineCourseAccreditation
        /// objects are soft-deleted.
        /// </summary>
        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Converts the OnlineCourseAccreditation object to a SubSonic data object.
        /// </summary>
        /// <returns>The SubSonic data object.</returns>
        public OnlineCourseAccreditationData ToDataObject()
        {
            OnlineCourseAccreditationData data;
            if (Id > 0)
            {
                data = new OnlineCourseAccreditationData(Id);
            }
            else
            {
                data = new OnlineCourseAccreditationData();
            }

            data.OnlineCourseID = OnlineCourseId;
            data.AccreditationBoardID = AccreditationBoardId;
            data.ArtisanCourseAccreditationID = ArtisanCourseAccreditationId;
            data.ProfessionID = ProfessionId;
            data.StartDate = StartDate;
            data.ExpiryDate = ExpiryDate;
            data.CreditHours = CreditHours;
            data.CreditHoursLabel = CreditHoursLabel;
            data.AccreditationCode = AccreditationCode;
            data.CourseNumber = CourseNumber;
            data.Disclaimer = Disclaimer;
            data.IsDeleted = IsDeleted;

            return data;
        }

        /// <summary>
        /// Determines whether or not the OnlineCourseAccreditation model is in a valid state and
        /// outputs an error message if so.
        /// </summary>
        /// <param name="error">Reference to a string variable where an error message can be stored.</param>
        /// <returns>A value indicating whether or not the model is in a valid state.</returns>
        public bool IsValid(out string error)
        {
            string errBase = "OnlineCourseAccreditation object is in an invalid state: ";

            if (OnlineCourseId <= 0)
            {
                error = errBase + "OnlineCourseId must be a positive integer.";
                return false;
            }
            if (AccreditationBoardId <= 0)
            {
                error = errBase + "AccreditationBoardId must be a positive integer.";
                return false;
            }
            if (ArtisanCourseAccreditationId <= 0)
            {
                error = errBase + "ArtisanCourseAccreditationId must be a positive integer.";
                return false;
            }
            if (DateTime.Compare(StartDate, ExpiryDate) > 0)
            {
                error = errBase + "ExpiryDate must be later than StartDate.";
                return false;
            }
            if (CreditHours <= 0)
            {
                error = errBase + "CreditHours must be a positive integer.";
                return false;
            }
            if (string.IsNullOrWhiteSpace(CreditHoursLabel))
            {
                error = errBase + "CreditHoursLabel cannot be empty.";
                return false;
            }
            if (string.IsNullOrWhiteSpace(AccreditationCode))
            {
                error = errBase + "AccreditationCode cannot be empty.";
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Copies data from an ArtisanCourseAccreditation object.
        /// </summary>
        /// <param name="accreditation">The ArtisanCourseAccreditation to copy data from.</param>
        /// <param name="error">If an error occurs, it will be output here.</param>
        /// <returns>Whether or not the data could be copied successfully.</returns>
        public bool CopyFromArtisan(ArtisanCourseAccreditation accreditation, out string error)
        {
            try
            {
                AccreditationBoardId = accreditation.AccreditationBoardId;
                ProfessionId = accreditation.ProfessionId;
                ArtisanCourseAccreditationId = accreditation.Id;
                StartDate = accreditation.StartDate.GetValueOrDefault();
                ExpiryDate = accreditation.ExpiryDate.GetValueOrDefault();
                CreditHours = accreditation.CreditHours.GetValueOrDefault();
                CreditHoursLabel = accreditation.CreditHoursLabel;
                AccreditationCode = accreditation.AccreditationCode;
                CourseNumber = accreditation.CourseNumber;
                Disclaimer = accreditation.Disclaimer;
                IsDeleted = accreditation.IsDeleted;

                return IsValid(out error);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }
        }
    }
}
