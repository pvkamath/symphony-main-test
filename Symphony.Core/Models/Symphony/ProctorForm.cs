﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    /// <summary>
    /// A ProctorForm is a sworn statement that must be completed by someone who is supervising a
    /// student that is taking a course.
    /// </summary>
    [DataContract(Name = "proctorForm")]
    public class ProctorForm : Model
    {
        static ProctorForm()
        {
            PagedQueryParams<ProctorForm>.RegisterSortableProperties(new Dictionary<string, string>
            {
                { "name", "Name" },
                { "description", "descripton" }
            });
        }

        /// <summary>
        /// Gets or sets the unique database identifier for the ProctorForm.
        /// </summary>
        [DataMember(Name = "id")]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name of the ProctorForm. This is a required property.
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a brief description for the ProctorForm. This is an optional property and has
        /// no effect other than to provide information to content editors.
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the "JSON" that describes the component. This isn't actually JSON, but an
        /// ExtJS config block. This is required.
        /// </summary>
        [DataMember(Name = "proctorJSON")]
        public string ProctorJSON { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the ProctorForm is deleted.
        /// </summary>
        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }
    }
}
