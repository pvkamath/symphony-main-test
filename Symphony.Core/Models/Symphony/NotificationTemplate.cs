﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "notificationTemplate")]
    [DefaultSort(ColumnName = "Subject")]
    public class NotificationTemplate : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "area")]
        public string Area { get; set; }

        [DataMember(Name = "body")]
        public string Body { get; set; }

        [DataMember(Name = "codeName")]
        public string CodeName { get; set; }

        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "enabled")]
        public bool Enabled { get; set; }

        [DataMember(Name = "includeCalendarInvite")]
        public bool IncludeCalendarInvite { get; set; }

        [DataMember(Name = "isScheduled")]
        public bool IsScheduled { get; set; }

        [DataMember(Name="scheduleParameterId")]
        public int ScheduleParameterID { get; set; }

        [DataMember(Name = "scheduleParameter")]
        public ScheduleParameter ScheduleParameter { get; set; }

        [DataMember(Name = "relativeScheduledMinutes")]
        public long RelativeScheduledMinutes { get; set; }

        [DataMember(Name = "percentage")]
        public int Percentage { get; set; }

        [DataMember(Name="filterComplete")]
        public bool FilterComplete { get; set; }

        [DataMember(Name = "validTemplateParameters")]
        public TreeNode ValidTemplateParameters { get; set; }

        [DataMember(Name = "possibleRecipients")]
        public List<RecipientGroup> PossibleRecipients { get; set; }
        
        [DataMember(Name = "selectedRecipients")]
        public List<RecipientGroup> SelectedRecipients { get; set; }

        [DataMember(Name = "ownerId")]
        public int OwnerID { get; set; }

        [DataMember(Name = "ownerName")]
        public string OwnerName { get; set; }

        [DataMember(Name = "ccClassroomSupervisors")]
        public bool CcClassroomSupervisors { get; set; }

        [DataMember(Name = "ccReportingSupervisors")]
        public bool CcReportingSupervisors { get; set; }

        [DataMember(Name = "isConfigurableForTrainingProgram")]
        public bool IsConfigurableForTrainingProgram { get; set; }

        [DataMember(Name = "isUserCreated")]
        public bool IsUserCreated { get; set; }
    }
}