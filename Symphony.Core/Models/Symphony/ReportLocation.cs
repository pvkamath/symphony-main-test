using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportLocation")]
    [DefaultSort(ColumnName = "Name")]
    public class ReportLocation : Model
    {
        [DataMember(Name = "locationkey")]
        public int LocationKey { get; set; }

        [DataMember(Name = "Name")]
        public string LocationName { get; set; }

        [DataMember(Name = "customerkey")]
        public int CustomerKey { get; set; }

        [DataMember(Name = "locationLevelDetail")]
        public string LocationLevelDetail { get; set; }

        [DataMember(Name = "locationId")]
        public int LocationId { get; set; }

        [DataMember(Name = "parentLocationId")]
        public int ParentLocationId { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerId { get; set; }

    }
}
