﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Attributes;
using System.Xml;
using Symphony.Core.Extensions;

using Newtonsoft.Json;

namespace Symphony.Core.Models
{
    public class SessionAssignedUser : User
    {
        string registrationStatusName = "";

        [DataMember(Name = "registrationStatusId")]
        public int? RegistrationStatusID { get; set; }

        [DataMember(Name = "registrationStatusName")]
        public string RegistrationStatusName
        {
            get
            {
                if (string.IsNullOrEmpty(registrationStatusName) && RegistrationStatusID.HasValue)
                {
                    registrationStatusName = Enum.GetName(typeof(RegistrationStatusType), RegistrationStatusID);
                }
                return registrationStatusName;
            }
            set
            {
                registrationStatusName = value;
            }
        }

        [DataMember(Name = "classId")]
        public int? ClassID { get; set; }

        [DataMember(Name = "className")]
        public string ClassName { get; set; }

        [DataMember(Name = "minStartDateTime")]
        public DateTime? MinStartDateTime { get; set; }

        [DataMember(Name = "maxStartDateTime")]
        public DateTime? MaxStartDateTime { get; set; }

        [DataMember(Name = "createdOn")]
        public DateTime? CreatedOn { get; set; }

        [DataMember(Name = "modifiedOn")]
        public DateTime? ModifiedOn { get; set; }

        [DataMember(Name = "sessionStatus")]
        public int SessionStatus { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "trainingProgramName")]
        public string TrainingProgramName { get; set; }

        [DataMember(Name = "isInDateRange")]
        public bool IsInDateRange { get; set; }

        [DataMember(Name = "sort")]
        public long Sort { get; set; }

        [IgnoreDataMember]
        public int TotalRows { get; set; }

        [IgnoreDataMember]
        public string SessionClassDatesXML { get; set; }

        private string SessionClassDatesJSON
        {
            get
            {
                if (string.IsNullOrEmpty(SessionClassDatesXML))
                {
                    return null;
                }

                // Ensure the sessions are always detected as an array
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(SessionClassDatesXML);

                XmlAttribute jsonNS = doc.CreateAttribute("xmlns", "json", "http://www.w3.org/2000/xmlns/");
                jsonNS.Value = "http://james.newtonking.com/projects/json";

                doc.DocumentElement.SetAttributeNode(jsonNS);

                XmlNodeList sessionNodes = doc.SelectNodes("//ClassDates/Class");
                foreach (XmlElement node in sessionNodes)
                {
                    XmlAttribute jsonArray = doc.CreateAttribute("json", "Array", "http://james.newtonking.com/projects/json");
                    jsonArray.Value = "true";
                    node.SetAttributeNode(jsonArray);
                }

                string sessionClassDatesJSON = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.None, true);

                return sessionClassDatesJSON;
            }
        }

        [DataMember(Name = "sessionClasses")]
        public List<SessionClass> SessionClasses
        {
            get
            {
                List<SessionClass> sessionClasses = new List<SessionClass>();

                string sessionClassDatesJSON = SessionClassDatesJSON;
                if (!string.IsNullOrEmpty(sessionClassDatesJSON))
                {
                    Dictionary<string, List<SessionClass>> classDates = JsonConvert.DeserializeObject<Dictionary<string, List<SessionClass>>>(SessionClassDatesJSON);

                    if (classDates.Count > 0)
                    {
                        sessionClasses = classDates.First().Value;
                    }
                }

                return sessionClasses.Where(s => s.TrainingProgramID == TrainingProgramID).ToList().WithUtcOffset();
            }
        }
    }
}
