﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    public class LibraryFilterableCategory : Model
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int CategoryTypeID { get; set; }
        public int SecondaryCategoryID { get; set; }
        public string SecondaryCategoryName { get; set; }
    }
}
