﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "location")]
    [DefaultSort(ColumnName = "Name")]
    public class Audience : Model
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "internalCode")]
        public string InternalCode { get; set; }

        [DataMember(Name = "salesforcePartnerCode")]
        public string SalesforcePartnerCode { get; set; }

        // used to load, don't serialize it though
        public int ParentAudienceID { get; set; }

        [DataMember(Name = "parentId")]
        public int ParentID
        {
            get { return ParentAudienceID; }
            set { ParentAudienceID = value; }
        }
    }
}
