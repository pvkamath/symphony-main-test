﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="artisanImageQuestionAnswer")]
    public class ArtisanImageQuestionAnswer
    {
        [DataMember(Name="dimensions")]
        public List<Dimensions> Dimensions { get; set; }

        [DataMember(Name="imageUrl")]
        public string ImageUrl { get; set; }
    }


    [DataContract(Name = "oldArtisanImageQuestionAnswer")]
    public class OldArtisanImageQuestionAnswer
    {
        [DataMember(Name = "dimensions")]
        public Dimensions Dimensions { get; set; }

        [DataMember(Name = "imageUrl")]
        public string ImageUrl { get; set; }
    }
}
