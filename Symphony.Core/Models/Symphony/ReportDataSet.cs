using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportDataSet")]
    public class ReportDataSet : Model
    {
        [DataMember(Name = "resultSet")]
        public DataSet ResultSet { get; set; }

    }
}
