using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="reportAudience")]
    [DefaultSort(ColumnName = "Name")]
    public class ReportAudience : Model
    {
        [DataMember(Name = "audiencekey")]
        public int AudienceKey { get; set; }

        [DataMember(Name = "name")]
        public string AudienceName { get; set; }

        [DataMember(Name = "customerkey")]
        public int CustomerKey { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerID { get; set; }

        [DataMember(Name = "audienceLevelDetail")]
        public string AudienceLevelDetail { get; set; }

        [DataMember(Name = "audienceId")]
        public int AudienceId { get; set; }

        [DataMember(Name = "parentId")]
        public int ParentAudienceId { get; set; }



    }
}
