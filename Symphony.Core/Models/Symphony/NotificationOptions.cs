﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "notificationOptions")]
    public class NotificationOptions : Model
    {
        public bool Sync { get; set; }

        public byte[] Attachment { get; set; }

        public int[] Users { get; set; }

        public object[] TemplateObjects { get; set; }

        public NotificationDate[] NotificationDates { get; set; }

        public Func<List<int>, List<int>> UserModifier { get; set; }

        public bool IsQueued { get; set; }

        public DateTime? SendTime { get; set; }

        public bool? IsForced { get; set; }

        public Data.Customer CustomerOverride { get; set; }
    }
}

