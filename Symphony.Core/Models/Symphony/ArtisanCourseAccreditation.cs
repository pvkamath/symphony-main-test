﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ArtisanCourseAccreditationData = Symphony.Core.Data.ArtisanCourseAccreditation;

namespace Symphony.Core.Models
{
    /// <summary>
    /// An ArtisanCourseAccreditation is an accreditation granted to a course in Artisan. These
    /// accreditations are an endorsement of the course by an AccreditationBoard. These
    /// accreditations are included in the SCORM meta-data when exported and will transfer over to
    /// OnlineCourse objects that are created from ArtisanCourses.
    /// </summary>
    [DefaultSort(ColumnName = "[dbo].[ArtisanCourseAccreditation].[ID]")]
    [DataContract(Name = "artisanCourseAccreditation")]
    public class ArtisanCourseAccreditation : Model
    {
        static ArtisanCourseAccreditation()
        {
            PagedQueryParams<ArtisanCourseAccreditation>.RegisterSortableProperties(new Dictionary<string, string>
            {
                { "courseName", "[dbo].[ArtisanCourses].[Name]" },
                { "status", "status" },
                { "startDate", "startDate" },
                { "expiryDate", "expiryDate" },
                { "creditHours", "creditHours" },
                { "creditHoursLabel", "creditHoursLabel" },
                { "accreditationCode", "accreditationCode" },
                { "courseNumber", "courseNumber" }
            });
        }

        /// <summary>
        /// Gets or sets the unique database identifier for the accreditation.
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the ID of the AccreditationBoard that issued the accreditation.
        /// </summary>
        [DataMember(Name = "accreditationBoardId")]
        public int AccreditationBoardId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the ArtisanCourse that the accreditation is tied to.
        /// </summary>
        [DataMember(Name = "artisanCourseId")]
        public int ArtisanCourseId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the Profession that the accreditation is associated with. This is
        /// optional and most accreditations will likely not be associated with a particular
        /// profession.
        /// </summary>
        [DataMember(Name = "professionId")]
        public int? ProfessionId { get; set; }

        /// <summary>
        /// Gets or sets the name of the associated course. This is only populated when specifically
        /// joined to the ArtisanCourse table, such as when querying the set of all accreditations
        /// that belong to an AccreditationBoard
        /// </summary>
        [DataMember(Name = "courseName")]
        public string CourseName { get; set; }

        /// <summary>
        /// Gets or sets the status of the accreditation. See <see cref="AccreditationStatus"/> for
        /// possible statuses. This property overrides any value in the StartDate or ExpiryDate
        /// properties.
        /// </summary>
        [DataMember(Name = "status")]
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the date from which the accreditation is valid. This is a UTC date and is
        /// specific to the minute. This property is required if the accreditation is no longer
        /// pending.
        /// </summary>
        [DataMember(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the date after which the accreditation expires. This is a UTC date and is
        /// specific to the minute. This property is required if the accreditation is no longer
        /// pending.
        /// </summary>
        [DataMember(Name = "expiryDate")]
        public DateTime? ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the number of credit hours that the accreditation is worth. Credit hours
        /// define how many "credits" each AccreditationBoard awards to a course.
        /// 
        /// This has an implication for licenses that are granted based on a number of credits.
        /// Different boards can weight courses differently, so this may vary per course. This
        /// property is required  if the accreditation is no longer pending.
        /// </summary>
        [DataMember(Name = "creditHours")]
        public float? CreditHours { get; set; }

        /// <summary>
        /// Gets or sets the name that the AccreditationBoard uses when describing credit hours.
        /// Different accreditation boards may use different terms when referring to these. For
        /// example: "Credit Hours", "Credits", "Classroom Hours", etc. This property is required
        /// if the accreditation is no longer pending.
        /// </summary>
        [DataMember(Name = "creditHoursLabel")]
        public string CreditHoursLabel { get; set; }

        /// <summary>
        /// Gets or sets the disclaimer that will be shown before starting the course. This may
        /// or may not have HTML formatting. This property is not required, but it is usually
        /// provided.
        /// </summary>
        [DataMember(Name = "disclaimer")]
        public string Disclaimer { get; set; }

        /// <summary>
        /// Gets or sets a unique accreditation code that the accreditation board assigns to the
        /// accreditation. This is the a number the board assigns us that indicates that the course
        /// is approved.
        /// </summary>
        [DataMember(Name = "accreditationCode")]
        public string AccreditationCode { get; set; }

        /// <summary>
        /// Gets or sets a unique course number for the accreditation. This is a number that the
        /// board uses internally to identify the course.
        /// </summary>
        [DataMember(Name = "courseNumber")]
        public string CourseNumber { get; set; }

        /// <summary>
        /// Gets or sets whether or not the accreditation is deleted. ArtisanCourseAccreditation
        /// objects are soft-deleted.
        /// </summary>
        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Converts the ArtisanCourseAccreditation object to a SubSonic data object.
        /// </summary>
        /// <returns>The SubSonic data object.</returns>
        public ArtisanCourseAccreditationData ToDataObject()
        {
            ArtisanCourseAccreditationData data;
            if (Id > 0)
            {
                data = new ArtisanCourseAccreditationData(Id);
            }
            else
            {
                data = new ArtisanCourseAccreditationData();
            }

            data.ArtisanCourseID = ArtisanCourseId;
            data.AccreditationBoardID = AccreditationBoardId;
            data.ProfessionID = ProfessionId;
            data.Status = Status;
            data.StartDate = StartDate;
            data.ExpiryDate = ExpiryDate;
            data.CreditHours = CreditHours;
            data.CreditHoursLabel = CreditHoursLabel;
            data.AccreditationCode = AccreditationCode;
            data.CourseNumber = CourseNumber;
            data.Disclaimer = Disclaimer;
            data.IsDeleted = IsDeleted;

            return data;
        }

        /// <summary>
        /// Determines whether or not the ArtisanCourseAccreditation model is in a valid state and
        /// outputs an error message if so.
        /// </summary>
        /// <param name="error">Reference to a string variable where an error message can be stored.</param>
        /// <returns>A value indicating whether or not the model is in a valid state.</returns>
        public bool IsValid(out string error)
        {
            string errBase = "ArtisanCourseAccreditation object is in an invalid state: ";

            if (ArtisanCourseId <= 0)
            {
                error = errBase + "ArtisanCourseId must be a positive integer.";
                return false;
            }
            if (AccreditationBoardId <= 0)
            {
                error = errBase + "AccreditationBoardId must be a positive integer.";
                return false;
            }
            if (ExpiryDate.HasValue && StartDate.HasValue && DateTime.Compare(StartDate.Value, ExpiryDate.Value) > 0)
            {
                error = errBase + "ExpiryDate must be later than StartDate.";
                return false;
            }
            if (CreditHours.HasValue && CreditHours.Value <= 0)
            {
                error = errBase + "CreditHours must be a positive integer.";
                return false;
            }

            if (Status != (int)AccreditationStatus.Pending)
            {
                if (!StartDate.HasValue)
                {
                    error = errBase + "StartDate is required for accreditations that are not pending.";
                    return false;
                }
                if (!ExpiryDate.HasValue)
                {
                    error = errBase + "ExpiryDate is required for accreditations that are not pending.";
                    return false;
                }
                if (!CreditHours.HasValue)
                {
                    error = errBase + "CreditHours is required for accreditations that are not pending.";
                    return false;
                }
                if (string.IsNullOrWhiteSpace(CreditHoursLabel))
                {
                    error = errBase + "CreditHoursLabel is required for accreditations that are not pending.";
                    return false;
                }
                if (string.IsNullOrWhiteSpace(AccreditationCode))
                {
                    error = errBase + "AccreditationCode is required for accreditations that are not pending.";
                    return false;
                }
            }

            error = null;
            return true;
        }
    }
}
