﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "artisanAssetFilter")]
    public class ArtisanAssetFilter
    {
        [DataMember(Name = "assetTypeId")]
        public int AssetTypeId { get; set; }
    }
}
