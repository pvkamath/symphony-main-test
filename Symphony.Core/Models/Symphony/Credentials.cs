﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using System.Security.Cryptography;
using System.IO;

namespace Symphony.Core.Models
{
    [DataContract(Name = "credentials")]
    [DefaultSort(ColumnName = "UserName")]
    public class Credentials : Model
    {
        /*private AesManaged getAES()
        {
            if (this.aes == null)
            {
                int Rfc2898KeygenIterations = 100;
                int AesKeySizeInBits = 128;
                byte[] Password = Encoding.ASCII.GetBytes("Xr@f0rT8%^Np");
                byte[] Salt = Encoding.ASCII.GetBytes("RxHMdKog9eFP"); ;
                System.Random rnd = new System.Random();
                rnd.NextBytes(Salt);

                this.aes = new AesManaged();

                this.aes.Padding = PaddingMode.PKCS7;
                this.aes.KeySize = AesKeySizeInBits;
                int KeyStrengthInBytes = this.aes.KeySize / 8;
                System.Security.Cryptography.Rfc2898DeriveBytes rfc2898 =
                    new System.Security.Cryptography.Rfc2898DeriveBytes(Password, Salt, Rfc2898KeygenIterations);
                this.aes.Key = rfc2898.GetBytes(KeyStrengthInBytes);
                this.aes.IV = rfc2898.GetBytes(KeyStrengthInBytes);
            }
            return this.aes;
        }*/

        private AesManaged aes;

        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "userName")]
        public string UserName { get; set; }

        [IgnoreDataMember]
        public string Password { get; set; }

        [DataMember(Name = "pass")]
        public string Pass
        {
            get
            {
                if (string.IsNullOrEmpty(Password))
                {
                    return string.Empty;
                }
                return Utilities.RijndaelSimple.Decrypt(Password);
                /*byte[] cipherText = System.Convert.FromBase64String(this.Password);
                byte[] plainText;
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, getAES().CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherText, 0, cipherText.Length);
                    }

                    plainText = ms.ToArray();
                }
                return System.Text.Encoding.Unicode.GetString(plainText);*/
            }

            set
            {
                /*byte[] rawPlaintext = System.Text.Encoding.Unicode.GetBytes(value);
                byte[] cipherText;
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, getAES().CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(rawPlaintext, 0, rawPlaintext.Length);
                    }

                    cipherText = ms.ToArray();
                }
                 * System.Convert.ToBase64String(cipherText);
                 */
                if (string.IsNullOrEmpty(value))
                {
                    this.Password = string.Empty;
                }
                else
                {
                    this.Password = Utilities.RijndaelSimple.Encrypt(value);
                }
            }
        }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "applicationId")]
        public int ApplicationID { get; set; }


    }
}