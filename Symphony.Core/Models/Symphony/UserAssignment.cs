﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;
using Symphony.Core.Attributes;

namespace Symphony.Core.Models
{
    public class UserAssignment : User
    {
        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        [DataMember(Name = "className")]
        public string ClassName { get; set; }

        [DataMember(Name = "classStartTime")]
        public DateTime ClassStartTime { get; set; }

        [DataMember(Name = "maxSubmitDate")]
        public DateTime MaxSubmitDate { get; set; }

        [DataMember(Name = "maxGradedDate")]
        public DateTime? MaxGradedDate { get; set; }

        [DataMember(Name = "assignmentsMarked")]
        public int AssignmentsMarked { get; set; }

        [DataMember(Name = "assignmentsInCourse")]
        public int AssignmentsInCourse { get; set; }

        [DataMember(Name = "assignmentsCompleted")]
        public int AssignmentsCompleted { get; set; }

        [DataMember(Name = "isAssignmentsFailed")]
        public int IsAssignmentsFailed { get; set; }

        [DataMember(Name = "isAssignmentsComplete")]
        public int IsAssignmentsComplete { get; set; }

        [DataMember(Name = "isNew")]
        public bool IsNew { get; set; }

        [IgnoreDataMember]
        public int TotalRows { get; set; }

        [DataMember(Name = "userId")]
        public int UserID
        {
            get
            {
                return ID;
            }
            set
            {

            }
        }

    }
}
