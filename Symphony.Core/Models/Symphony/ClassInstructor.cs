﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "classInstructor")]
    [DefaultSort(ColumnName = "LastName")]
    public class ClassInstructor : Model
    {
        #region Organizer Statuses

        public static string GTMOrganizerStatusActive = "Active";
        public static string GTMOrganizerStatusSuspended = "Suspended";
        public static string GTMOrganizerStatusInvited = "Invited";

        #endregion

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "instructorId")]
        public int InstructorID { get; set; }

        [DataMember(Name = "collaborator")]
        public bool Collaborator { get; set; }

        [DataMember(Name = "classId")]
        public int ClassID { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "middleName")]
        public string MiddleName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "fullName")]
        public string FullName
        {
            get
            {
                return Model.FormatFullName(FirstName, MiddleName, LastName);
            }
            set { }
        }

        [DataMember(Name = "gtmOrganizerStatus")]
        public string GTMOrganizerStatus { get; set; }
        
        [DataMember(Name = "canHostVirtual")]
        public bool CanHostVirtual
        {
            get { return GTMOrganizerStatus == GTMOrganizerStatusActive; }
            set { }
        }
    }
}
