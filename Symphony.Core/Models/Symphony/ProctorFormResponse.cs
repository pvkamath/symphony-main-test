﻿using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    [DataContract(Name = "proctorFormResponse")]
    public class ProctorFormResponse : Model
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        [DataMember(Name = "proctorFormId")]
        public int ProctorFormID { get; set; }

        [DataMember(Name = "trainingProgramId")]
        public int TrainingProgramID { get; set; }

        [DataMember(Name = "courseId")]
        public int CourseID { get; set; }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "responseJSON")]
        public string ResponseJSON { get; set; }
    }
}
