﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Web;

namespace Symphony.Core.Models
{
    [DataContract(Name = "authenticationResult")]
    public class AuthenticationResult
    {
        public AuthenticationResult()
        {
            this.Attributes = new Dictionary<string, string>();
        }

        [DataMember(Name = "success")]
        public bool Success { get; set; }

        [DataMember(Name = "userId")]
        public int UserId { get; set; }

        [DataMember(Name = "customerId")]
        public int CustomerId { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "customerSubDomain")]
        public string CustomerSubDomain { get; set; }

        [DataMember(Name = "attributes")]
        public Dictionary<string, string> Attributes { get; set; }

        public AuthUserData UserData { get; set; }

        public List<LoginSystem> AdditionalSystems {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null) return new List<LoginSystem>();
                return (HttpContext.Current.Session["UserLoginSystem"] as List<LoginSystem>) == null ? new List<LoginSystem>() : HttpContext.Current.Session["UserLoginSystem"] as List<LoginSystem>;
            }
        }

        public bool AuthenticatedButNotSymphonyUser
        {
            get { return UserId < 0; }
        }

        public bool HasExternalSystems
        {
            get
            {
                bool onlySymphonySystem = AdditionalSystems.Count == 1 && AdditionalSystems.First().SystemCodeName == "symphony";
                return AdditionalSystems.Count >= 1 && !onlySymphonySystem;
            }
        }
    }
}
