﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name = "libraryCategory")]
    [DefaultSort(ColumnName="Name")]
    public class LibraryCategory : Category
    {
        [DataMember(Name = "totalItems")]
        public int TotalItems { get; set;}

        [DataMember(Name = "items")]
        public List<LibraryItem> Items { get; set; }

        [DataMember(Name = "totalRows")]
        public int TotalRows { get; set; }
    }
}
