﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;

namespace Symphony.Core.Models
{
    [DataContract(Name="assignmentStatus")]
    public class AssignmentStatus : Model
    {
        [DataMember(Name = "courseId")]
        public int CourseID { get; set; }

        [DataMember(Name = "userId")]
        public int UserID { get; set; }

        [DataMember(Name = "isAssignmentsFailed")]
        public bool IsAssignmentsFailed
        {
            get
            {
                return (AssignmentsMarked >= AssignmentsInCourse && AssignmentsCompleted < AssignmentsInCourse);
            }
            internal set {}
        }

        [DataMember(Name = "isAssignmentsComplete")]
        public bool IsAssignmentsComplete
        {
            get
            {
                return (AssignmentsMarked >= AssignmentsInCourse && AssignmentsCompleted >= AssignmentsInCourse);
            }
            internal set {}
        }

        [DataMember(Name = "assignmentsInCourse")]
        public int AssignmentsInCourse { get; set; }

        [DataMember(Name = "assignmentsMarked")]
        public int AssignmentsMarked { get; set; }

        [DataMember(Name = "assignmentsCompleted")]
        public int AssignmentsCompleted { get; set; }

        [DataMember(Name = "assignmentsAttempted")]
        public int AssignmentsAttempted { get; set; }
    }
}

