﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Symphony.Core.Models
{
    /// <summary>
    /// Accreditation board survey is a view model that is a very small subset of an online course.
    /// It is designed to display survey info in the accreditation board form's survey panel that
    /// allows users to assign surveys to a course.
    /// </summary>
    [DefaultSort(ColumnName = "[dbo].[OnlineCourse].[ID]")]
    [DataContract(Name="accreditationBoardSurvey")]
    public class AccreditationBoardSurvey : Model
    {
        static AccreditationBoardSurvey()
        {
            PagedQueryParams<AccreditationBoardSurvey>.RegisterSortableProperties(new Dictionary<string, string>
            {
                { "name", "OnlineCourse.Name" },
                { "category", "Category.Name" }
            });
        }

        /// <summary>
        /// Gets or sets the unique database identifier of the survey. This refers to the online
        /// course.
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the order in which the survey should be taken. Lower values have precedence.
        /// </summary>
        [DataMember(Name = "rank")]
        public int Rank { get; set; }

        /// <summary>
        /// Gets or sets the name of the survey.
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the category of the survey.
        /// </summary>
        [DataMember(Name = "category")]
        public string Category { get; set; }
    }
}
