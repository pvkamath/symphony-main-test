﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using Symphony.Core.Controllers;
using DDay.iCal;
using Symphony.Core.Extensions;

namespace Symphony.Core.Models
{
    [DataContract(Name = "notificationDates")]
    public class NotificationDate : Model
    {
        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public CalendarType Type { get; set; }

        public CalendarMethod Method { get; set; }

        public string Location { get; set; }

        public string InstanceID { get; set; }

        public int Sequence { get; set; }

        /*public Dictionary<string, string> GetNotificationDateVCals(Symphony.Core.Models.NotificationOptions options, string subject, User from, User to)
        {
            Dictionary<string, string> streams = new Dictionary<string, string>();

            if (options.NotificationDates != null)
            {
                foreach (Models.NotificationDate date in options.NotificationDates)
                {
                    string vcalString = BuildVCalFile(date.StartDate, date.EndDate, from, to, subject);

                    string notificationName = date.Name;
                    int occurances = 0;
                    while (streams.ContainsKey(notificationName))
                    {
                        occurances++;
                        notificationName = date.Name + " (" + occurances + ")";
                    }

                    streams.Add(notificationName, vcalString);
                }
            }
            return streams;
        }*/

        /// <summary>
        /// Creates a VCal style string, and returns the ASCII-encoded byte value
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <param name="summary">A string description of the event</param>
        /// <returns>ASCII encoded version of the VCAL attachment</returns>
        public iCalendar BuildVCalFile(Data.User from, Data.User to, string summary, string details)
        {
            /*string dateFormat = "yyyyMMdd";
            string timeFormat = "HHmmss";
            string uid = InstanceID;

            string vcalTemplate = @"
BEGIN:VCALENDAR
PRODID:-//ocl/symphony//NONSGML v1.0//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:REQUEST
BEGIN:VEVENT
DTSTART:{0:" + dateFormat + "}T{1:" + timeFormat + @"}Z
DTEND:{2:" + dateFormat + "}T{3:" + timeFormat + @"}Z
ORGANIZER;CN={4}:mailto:{5}
UID:{6}
ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=
    TRUE;CN={7};X-NUM-GUESTS=0:mailto:{8}
CREATED:20150129T185625Z
DESCRIPTION:{9}
LAST-MODIFIED:20150129T185625Z
LOCATION:{10}
SEQUENCE:{11}
STATUS:CONFIRMED
SUMMARY:{12}
TRANSP:OPAQUE
END:VEVENT
END:VCALENDAR";*/

            iCalendar ical = new iCalendar();
            ical.Version = "2.0";
            ical.Method = "PUBLISH"; 
            
            Event evt = ical.Create<Event>();
            evt.Summary = summary;
            evt.Start = new iCalDateTime(StartDate.WithUtcFlag());
            evt.Duration = EndDate.Subtract(StartDate);
            evt.Summary = summary;
            evt.Description = details;
            evt.Organizer = new Organizer(from.Email)
            {
                CommonName = from.FullName
            };
            evt.UID = InstanceID;
            evt.Status = EventStatus.Confirmed;

            var htmlProp = new CalendarProperty("X-ALT-DESC");
            htmlProp.AddParameter("FMTTYPE", "text/html");
            htmlProp.Value = details;
            evt.AddProperty(htmlProp);

            // alert 60 mins and 24 hours before the event, per BC 8/11/15
            foreach (var minutes in new int[] { 60, 24 * 60 })
            {
                var alarm = ical.Create<Alarm>();
                alarm.Trigger = new Trigger(TimeSpan.FromMinutes(minutes));
                alarm.Action = AlarmAction.Display;
                alarm.Name = summary;
                alarm.Description = details;
                evt.Alarms.Add(alarm);
            }

            return ical;

            //return String.Format(vcalTemplate, StartDate, StartDate, EndDate, EndDate, from.FullName, from.Email, uid, to.FullName, to.Email, summary, Location, Sequence, summary);
        }
    
    }
}

