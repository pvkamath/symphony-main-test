﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Symphony.Core.Models
{
    [DataContract]
    public class UserDataFieldForm : Model
    {
        [DataMember(Name = "items")]
        public List<UserDataField> Items { get; set; }
    }
}
