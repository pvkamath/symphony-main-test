﻿using System;
using System.Collections.Generic;
using log4net;

namespace Symphony.Core.Models
{
    /// <summary>
    /// PagedQueryParams encapasulates the parameters necessary to perform a paged query. It
    /// provides additional validation logic and defaults to safe parameters if invalid input is
    /// entered.
    /// </summary>
    /// <typeparam name="T">Type of </typeparam>
    public class PagedQueryParams<T>
    {
        static PagedQueryParams()
        {
            SortableProperties = new Dictionary<string, string>();
        }

        private ILog Log;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log">The logging instance used to write log messages. If not provided, then
        /// no logs will be emitted.</param>
        /// <param name="searchText">The query parameter to filter results by. Pass null if no
        /// search parameter is desired.</param>
        /// <param name="start">The offset of the first record to return. Values below zero will be
        /// rounded up to zero.</param>
        /// <param name="limit">The total number of records to return. Values below 1 will be 
        /// rounded up to 1.</param>
        /// <param name="sort">The property to sort the results by. This value must have been
        /// registered by RegisterSortableProperties or it will be discarded.</param>
        /// <param name="dir">Indicates how to sort the results, either "asc" or "desc".</param>
        public PagedQueryParams(ILog log, string searchText, int start, int limit, string sort, string dir)
            :this(searchText, start, limit, sort, dir)
        {
            // If a logger is injected, logs appear to come from the class that injected it.
            Log = log;
        }

        public PagedQueryParams(string searchText, int start, int limit, string sort, string dir)
        {
            dir = dir ?? "";
            dir = dir.ToLower();

            if (string.IsNullOrWhiteSpace(searchText))
            {
                searchText = null;
            }
            if (start < 0)
            {
                if (Log != null)
                {
                    Log.WarnFormat("start must be zero or a positive integer - start: {0}", start);
                }
                start = 0;
            }
            if (limit < 1)
            {
                if (Log != null)
                {
                    Log.WarnFormat("limit must be a positive integer - limit: {0}", limit);
                }
                limit = 25;
            }
            if (sort != null && !IsSortableProperty(sort))
            {
                if (Log != null)
                {
                    Log.WarnFormat("sort must be a sortable property - sort: {0}", sort);
                }
                sort = null;
            }

            if (sort != null)
            {
                sort = SortableProperties[sort];
            }

            SearchText = searchText;
            Start = start;
            Limit = limit;
            OrderBy = sort;
            OrderDirection =
                dir == "asc" ? OrderDirection.Ascending :
                dir == "desc" ? OrderDirection.Descending :
                OrderDirection.Default;
        }

        /// <summary>
        /// Gets the optional query parameter that is used to filter results.
        /// </summary>
        public string SearchText { get; private set; }

        /// <summary>
        /// Gets the offset of the first record of results to return.
        /// </summary>
        public int Start { get; private set; }

        /// <summary>
        /// Gets the total number of results to return.
        /// </summary>
        public int Limit { get; private set; }

        /// <summary>
        /// Gets the page of results to return. This begins at 1 and is based on the Start and
        /// Limit properties. It assumes that Limit is the PageSize and that Start will always
        /// be a multiple of Limit.
        /// </summary>
        public int PageIndex
        {
            get
            {
                return (int)Math.Floor((double)(Start / Limit));
            }
        }

        /// <summary>
        /// Gets the size of each page of results to return. This is based on the Limit property.
        /// </summary>
        public int PageSize
        {
            get
            {
                return Limit;
            }
        }

        /// <summary>
        /// Gets a string indicating which properties should be used to sort the data.
        /// </summary>
        public string OrderBy { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to sort in ascending or descending order.
        /// </summary>
        public OrderDirection OrderDirection { get; private set; }

        private static Dictionary<string, string> SortableProperties { get; set; }
        private static object SortablePropertiesLock = new object();

        /// <summary>
        /// Marks the specified properties as "sortable." When this class is initialized with a "sort"
        /// parameter, if the parameter is not registered, then it will be discarded rather than
        /// causing an error in the data layer.
        /// 
        /// Note that this is unique per generic variant of PagedQueryParams{T}, so make sure that
        /// you are calling the correct one.
        /// 
        /// The dictionary provides a mapping of properties to database fields. The key would be
        /// the value passed from ExtJS and the value would be the actual database column.
        /// </summary>
        /// <param name="properties">The set of properties to assign as sortable.</param>
        public static void RegisterSortableProperties(Dictionary<string, string> properties)
        {
            lock (SortablePropertiesLock)
            {
                foreach (var kvp in properties)
                {
                    SortableProperties.Add(kvp.Key, kvp.Value);
                }
            }
        }

        /// <summary>
        /// Determines whether or not a given property is sortable.
        /// </summary>
        /// <param name="property">The property name to check.</param>
        /// <returns>Whether or not the provided property name is sortable.</returns>
        public static bool IsSortableProperty(string property)
        {
            return SortableProperties.ContainsKey(property);
        }

        /// <summary>
        /// Converts the parameters to a human readable representation.
        /// </summary>
        /// <returns>A human readable representation of the parameters.</returns>
        public override string ToString()
        {
            string dir = OrderDirection == OrderDirection.Default ? "Default (0)" :
                OrderDirection == OrderDirection.Ascending ? "Ascending (1)" :
                OrderDirection == OrderDirection.Descending ? "Descending (2)" : "Unknown";

            return string.Format("{{SearchText={0}, Start={1}, Limit={2}, OrderBy={3}, OrderDirection={4}}}", SearchText, Start, Limit, OrderBy, dir);
        }
    }
}
