﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Symphony.Core
{
    /// <summary>
    /// Lists all the possible Symphony roles
    /// </summary>
    public class Roles
    {
        public static string GTMOrganizer = "Collaboration - GTM Organizer";
        public static string GTWOrganizer = "Collaboration - GTW Organizer";
        public static string ClassroomResourceManager = "Classroom - Resource Manager";
        public static string ClassroomSupervisor = "Classroom - Supervisor";
        public static string ClassroomInstructor = "Classroom - Instructor";
        public static string ClassroomManager = "Classroom - ILT Manager";
        public static string ArtisanAuthor = "Artisan - Author";
        public static string ArtisanPackager = "Artisan - Packager";
        public static string ArtisanViewer = "Artisan - Viewer";
        public static string CustomerManager = "Customer - Manager";
        public static string CustomerJobRoleManager = "Customer - Job Role Manager";
        public static string CustomerLocationManager = "Customer - Location Manager";
        public static string CustomerUser = "Customer - User";
        public static string CustomerAdministrator = "Customer - Administrator";
        public static string CourseAssignmentAdministrator = "CourseAssignment - Training Administrator";
        public static string CourseAssignmentTrainingManager = "CourseAssignment - Training Manager";
        public static string SuperHero = "SuperHero";
        public static string ReportingAnalyst = "Reporting - Analyst";
        public static string ReportingJobRoleAnalyst = "Reporting - Job Role Analyst";
        public static string ReportingLocationAnalyst = "Reporting - Location Analyst";
        public static string ReportingAudienceAnalyst = "Reporting - Audience Analyst";
        public static string ReportingDeveloper = "Reporting - Report Developer";
        //public static string SuperUser = "Super User";
        public static string ReportingSupervisor = "Reporting - Supervisor";

        public static string[] GetAllRoles()
        {
            var fields = typeof(Roles).GetFields(BindingFlags.Static | BindingFlags.Public);
            List<string> roles = new List<string>();
            foreach (var fieldInfo in fields)
            {
                roles.Add(fieldInfo.GetValue(null).ToString());
            }
            return roles.ToArray();
        }
    }
}
