﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Symphony.Core
{
    public class Skins
    {
        public const string QueryParameter = "skin";
        public const string QuerySkinPathTpl = "/skins/{0}/";

        public static bool HasQuerySkin()
        {
            return HttpContext.Current.Request.QueryString.AllKeys.Contains(QueryParameter);
        }

        public static string GetQuerySkin() {
            if (!HasQuerySkin()) {
                return "";
            }

            return HttpContext.Current.Request.QueryString[QueryParameter];
        }

        public static string BuildQuerySkin(string prefix = "")
        {
            if (!HasQuerySkin())
            {
                return "";
            }

            return prefix + QueryParameter + "=" + GetQuerySkin();
        }

        public static string GetSkinFolder()
        {
            if (!HasQuerySkin())
            {
                return "";
            }

            return String.Format(QuerySkinPathTpl, GetQuerySkin());
        }
    }
}
