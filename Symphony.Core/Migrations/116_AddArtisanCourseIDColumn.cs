﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration116 : Migration
    {
        public override void Up()
        {
            AddColumn("OnlineCourse", "ArtisanCourseID", DbType.Int32);
        }
    }
}