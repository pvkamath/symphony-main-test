﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration399 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgram", "ParentTrainingProgramID", DbType.Int32, 0, true);
        }
    }
}