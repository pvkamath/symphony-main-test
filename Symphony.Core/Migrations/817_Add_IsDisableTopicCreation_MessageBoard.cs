﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration817 : Migration
    {
        public override void Up()
        {
            AddColumn("MessageBoard", "IsDisableTopicCreation", DbType.Boolean, 0, false, "0");
        }
    }
}