﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration551 : Migration
    {
        public override void Up()
        {
            AddColumn("OnlineCourse", "IsUseAutomaticDuration", DbType.Boolean, 0, false, "0");
            AddColumn("OnlineCourse", "AutomaticDuration", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanCourses", "IsUseAutomaticDuration", DbType.Boolean, 0, false, "0");
            AddColumn("ArtisanCourses", "AutomaticDuration", DbType.Int32, 0, false, "0");
        }
    }
}