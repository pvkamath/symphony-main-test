﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration203 : Migration
    {

        public override void Up()
        {
            StringBuilder sb = new StringBuilder(318);
            sb.AppendLine("CREATE view [dbo].[TileApplication] as");
            sb.AppendLine("  SELECT");
            sb.AppendLine("    t.ID,");
            sb.AppendLine("    t.Name,");
            sb.AppendLine("    t.TileUri,");
            sb.AppendLine("    t.ApplicationID,");
            sb.AppendLine("    a.Name AS ApplicationName,");
            sb.AppendLine("    t.CreatedOn,");
            sb.AppendLine("    t.CreatedBy,");
            sb.AppendLine("    t.ModifiedOn,");
            sb.AppendLine("    t.ModifiedBy");
            sb.AppendLine("FROM");
            sb.AppendLine("  Tiles AS t ");
            sb.AppendLine("  INNER JOIN");
            sb.AppendLine("    Applications AS a");
            sb.AppendLine("    ON a.ID = t.ApplicationID");
            Execute(sb.ToString());

        }

        public override void Down()
        {
            Execute(@"DROP VIEW [dbo].[TileApplication]");
        }
    }
}