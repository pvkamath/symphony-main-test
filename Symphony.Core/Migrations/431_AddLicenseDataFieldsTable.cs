﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration431 : Migration
    {

        public override void Up()
        {
            TableSchema.Table dataFields = CreateTableWithKey("LicenseDataFields", "ID");
            dataFields.AddColumn("Name", DbType.String, 512, false, "''");
            AddSubSonicStateColumns(dataFields);

            TableSchema.Table LicenseDataFieldsToLicensesMap = CreateTableWithKey("LicenseDataFieldsToLicensesMap", "ID");
            LicenseDataFieldsToLicensesMap.AddColumn("LicenseID", DbType.Int32, 0, false);
            LicenseDataFieldsToLicensesMap.AddColumn("LicenseDataFieldID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(LicenseDataFieldsToLicensesMap);

            TableSchema.Table LicenseDataFieldsToUsersMap = CreateTableWithKey("LicenseDataFieldsToUsersMap", "ID");
            LicenseDataFieldsToUsersMap.AddColumn("UserID", DbType.Int32, 0, false);
            LicenseDataFieldsToUsersMap.AddColumn("LicenseDataFieldID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(LicenseDataFieldsToUsersMap);
        }

    }
}