﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration455 : Migration
    {
       
        public override void Up()
        {
            AddColumn("Customer", "HelpUrl", DbType.String, 2048, true);
        }

    }
}