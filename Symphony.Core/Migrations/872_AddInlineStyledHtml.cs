﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration872 : Migration
    {

        public override void Up()
        {
            Execute(@"
                alter table ArtisanTemplates
                add InlineStyledHtml nvarchar(max) null;

                alter table ArtisanPages
                add InlineStyledHtml nvarchar(max) null;
                ");
        }
    }
}
