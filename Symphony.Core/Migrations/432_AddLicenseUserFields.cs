﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration432 : Migration
    {

        public override void Up()
        {
            Execute("alter table Licence add RequiredUserFields nvarchar(max) not null default ''");
        }

    }
}