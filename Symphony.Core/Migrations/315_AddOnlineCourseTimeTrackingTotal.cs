﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration315: Migration
    {
        public override void Up()
        {
            Execute(@"create view [dbo].[OnlineCourseTimeTrackingTotal]
as
select o.*, uSum.UserDuration, coalesce(tp.MaxCourseWork, c.MaxCourseWork, cu.MaxCourseWork) * 3600 as MaxCourseWork
from OnlineCourseTimeTracking o
join
	(
		select sum(ou.Duration) as UserDuration, ou.UserID, ou.[Date]
		from OnlineCourseTimeTracking ou
		group by ou.UserID, ou.[Date]
	) uSum on uSum.UserID = o.UserID and uSum.[Date] = o.[Date]
left join Category c on o.CategoryID = c.ID
left join TrainingProgram tp on o.TrainingProgramID = tp.ID
join [User] u on u.ID = o.UserID
join Customer cu on cu.ID = u.CustomerID;");
        }
    }
}