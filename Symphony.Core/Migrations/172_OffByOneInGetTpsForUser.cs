﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration172 : Migration
    {
        public override void Up()
        {
            Execute(@"
alter procedure [dbo].[GetTrainingProgramsForUser](@userId int, @newHire bit, @searchText nvarchar(50), @orderBy nvarchar(50), @orderDir varchar(4), @pageIndex int, @pageSize int)
as
begin
	set @orderDir = lower(@orderDir);
	set @orderBy = lower(@orderBy);
	-- create a date time that represents today without the time
	declare @today datetime
	declare @hireDate datetime
	declare @newHireDuration int
	declare @newHireTransitionPeriod int
	
	set @today = (select cast(floor(cast(getdate() as decimal(12, 5))) as datetime));
	set @hireDate = (select HireDate from [user] where id = @userId);
	set @newHireDuration = (select NewHireDuration from Customer where ID = (select CustomerID from [user] where id = @userId));
	set @newHireTransitionPeriod = (select NewHireTransitionPeriod from Customer where ID = (select CustomerID from [user] where id = @userId));
	
	-- if the user's new hire date + transition > enddate, exclude it
	
	-- get all the tp details
	with TP as(
		select
			TP.ID, TP.CustomerID, TP.OwnerID, TP.Name, TP.InternalCode, TP.Cost, TP.[Description], 
			TP.IsNewHire, TP.IsLive, TP.EnforceRequiredOrder, 
			TP.MinimumElectives, TP.FinalAssessmentCourseID, TP.FinalAssessmentCourseTypeID, 
			TP.ModifiedBy, TP.CreatedBy, TP.ModifiedOn, TP.CreatedOn,
			TP.NewHireOffsetEnabled, TP.NewHireStartDateOffset, TP.NewHireDueDateOffset, TP.NewHireEndDateOffset,
			TP.NewHireTransitionPeriod,
			
			case when @newHire = 1 
				then dateadd(d, 
						case when NewHireOffsetEnabled = 1 
							then NewHireStartDateOffset 
							else 0
						end, @hireDate) 
				else StartDate end as StartDate,
			case when @newHire = 1 
				then dateadd(d, 
						case when NewHireOffsetEnabled = 1 
							then NewHireDueDateOffset 
							else @newHireDuration
						end, @hireDate) 
				else DueDate end as DueDate,
				
			case when @newHire = 1 
				then dateadd(d, 
						case when NewHireOffsetEnabled = 1 
							then NewHireEndDateOffset 
							else @newHireDuration
						end, @hireDate) 
				else EndDate end as EndDate,
				
			T.CourseCount,
			ROW_NUMBER() over (order by TrainingProgramID) as MyRow
		from
			TrainingProgram tp
		join
			(
				-- make sure we only ever see 1 instance of a given tp
				select
					distinct(X.TrainingProgramID) as TrainingProgramID, 
					(count(distinct cl.ID)) as CourseCount
				from
					(
						-- get the assigned tps
						select
							l.TrainingProgramID, 
							tp.Name,
							tp.MinimumElectives
						from
							hierarchytotrainingprogramlink l
						join
							(

								select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
								
								union
								
								select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
								
								union
								
								select f.ID, f.TypeID from useraudience 
								cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
								where userid = @userid
								
								union
								
								select
									@userId, 4 -- 'user' type id
							) as H
						on
							h.TypeID = l.HierarchyTypeID
							and
							h.ID = l.HierarchyNodeID
						join
							TrainingProgram tp
						on
							tp.ID = l.TrainingProgramID
						group by
							l.trainingprogramid, tp.name, tp.MinimumElectives
							
					) as X
				left join
					dbo.TrainingProgramToCourseLink cl
				on
					X.TrainingProgramID = cl.TrainingProgramID
				group by
					X.TrainingProgramID, X.MinimumElectives
			)
		as T
		on
			tp.ID = T.TrainingProgramID
		where
			Name like '%' + @searchText + '%'
		and
			IsNewHire = @newHire
		and 
			IsLive = 1
	)
	select
		TP.ID, TP.CustomerID, TP.OwnerID, TP.Name, TP.InternalCode, TP.Cost, TP.[Description], TP.IsNewHire, TP.IsLive, 
		TP.StartDate as StartDate,
		TP.EndDate as EndDate,
		-- new hire end date is the due date
		TP.DueDate as DueDate,
		TP.EnforceRequiredOrder, TP.MinimumElectives, TP.FinalAssessmentCourseID, 
		TP.FinalAssessmentCourseTypeID, TP.ModifiedBy, TP.CreatedBy, TP.ModifiedOn, TP.CreatedOn,
		TP.CourseCount,
		(select max(myrow) from TP) as TotalSize
	from
		TP
	join
		Customer
	on
		Customer.id = Customerid
	where
		myrow between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
	and 
		StartDate <= @today
	and
		EndDate >= @today
	and
		EndDate >= case when @newHire = 0 then dateadd(d, coalesce(Customer.NewHireTransitionPeriod, TP.NewHireTransitionPeriod,0), @hireDate) else @today end
	group by
		TP.ID, TP.CustomerID, TP.OwnerID, TP.Name, TP.InternalCode, TP.Cost, TP.[Description], 
		TP.IsNewHire, TP.IsLive, TP.StartDate, TP.EndDate, TP.DueDate, TP.EnforceRequiredOrder, 
		TP.MinimumElectives, TP.FinalAssessmentCourseID, TP.FinalAssessmentCourseTypeID, 
		TP.ModifiedBy, TP.CreatedBy, TP.ModifiedOn, TP.CreatedOn, TP.CourseCount
	order by
		case when @orderBy = 'Name' and @orderDir = 'asc' then TP.Name end,
		case when @orderBy = 'Name' and @orderDir = 'desc' then TP.Name end desc,
		case when @orderBy = 'Description' and @orderDir = 'asc' then TP.[Description] end,
		case when @orderBy = 'Description' and @orderDir = 'desc' then TP.[Description] end desc,
		case when @orderBy = 'StartDate' and @orderDir = 'asc' then TP.StartDate end,
		case when @orderBy = 'StartDate' and @orderDir = 'desc' then TP.StartDate end desc,
		case when @orderBy = 'EndDate' and @orderDir = 'asc' then TP.EndDate end,
		case when @orderBy = 'EndDate' and @orderDir = 'desc' then TP.EndDate end desc,
		case when @orderBy = 'DueDate' and @orderDir = 'asc' then TP.DueDate end,
		case when @orderBy = 'DueDate' and @orderDir = 'desc' then TP.DueDate end desc,
		case when 1 = 1 then StartDate end
	
end
");
        }
    }
}