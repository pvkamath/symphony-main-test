﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration200 : Migration
    {
        public override void Up()
        {
            Execute(@"
                UPDATE ArtisanAssetTypes SET HasAlternateHtml = 1 WHERE Extensions = 'swf'
            ");
        }
    }
}