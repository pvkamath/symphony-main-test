﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration350: Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgram", "AffidavitID", DbType.Int32, 0, true);
        }
    }
}