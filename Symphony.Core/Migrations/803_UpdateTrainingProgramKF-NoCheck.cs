﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration803 : Migration
    {
        public override void Up()
        {
            Execute(@"
                ALTER TABLE AssignmentSummaryAttempts  
                    NOCHECK CONSTRAINT fk_AssignmentSummaryAttemptsTrainingProgramID_TrainingProgramID");
        }
    }
}