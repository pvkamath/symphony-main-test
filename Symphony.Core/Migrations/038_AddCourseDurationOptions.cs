﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration038 : Migration
    {
        public override void Up()
        {
            Execute("alter table CourseDurationUnit alter column factor decimal(11, 10)");
            Execute("insert into CourseDurationUnit (description, factor) values ('Hours', 1.00)");
            Execute("insert into CourseDurationUnit (description, factor) values ('Minutes', 0.0166666667)");
        }
    }
}