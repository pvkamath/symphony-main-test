﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration037 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(566);
            sb.AppendFormat(@"CREATE TABLE [dbo].[ArtisanAnswers]({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[ID] [int] IDENTITY(1,1) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[PageID] [int] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[Text] [nvarchar](2000) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[Sort] [int] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[IsCorrect] [bit] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[CreatedOn] [datetime] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[CreatedBy] [nvarchar](50) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[ModifiedOn] [datetime] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[ModifiedBy] [nvarchar](50) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@" CONSTRAINT [PK_ArtisanAnswers] PRIMARY KEY CLUSTERED {0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[ID] ASC{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ");
            sb.AppendFormat(@"ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]{0}", Environment.NewLine);
            sb.AppendFormat(@") ON [PRIMARY]");

            Execute(sb.ToString());
        }
    }
}