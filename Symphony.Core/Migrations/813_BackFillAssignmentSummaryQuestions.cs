﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration813 : Migration
    {
        public override void Up()
        {
            // Re-running this migration as there was an issue found with the initial backfill

            // Backfills the assignments using all courses that have assignments
            // that do not appear in the assignments table yet. This would be for
            // any deployed course that has not yet had a user take the assignment
            Execute(@"
INSERT INTO AssignmentSummaryQuestions
    (
        AssignmentSummaryID,
        ArtisanSectionID,
        ArtisanPageID,
        Question,
        CorrectResponse,
        Sort,
        CreatedOn,
        ModifiedOn,
        CreatedBy,
        ModifiedBy
    )
SELECT
	a.ID as AssignmentSummaryID,
	a.ArtisanSectionID,
	p.ID as ArtisanPageID,
	p.Html as Question,
	aa.Text as CorrectResponse,
    sp.Sort,
	a.CreatedOn,
	a.ModifiedOn,
	a.CreatedBy,
	a.ModifiedBy
FROM
	AssignmentSummary a
JOIN
	ArtisanSectionPages sp
	ON
	sp.SectionID = a.ArtisanSectionID
JOIN
	ArtisanPages p
	ON
	p.ID = sp.PageID and p.QuestionType = 8
JOIN
	ArtisanAnswers aa
	ON
	aa.PageID = p.ID");

        }
    }
}