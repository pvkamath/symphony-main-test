﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration282 : Migration
    {
        public override void Up()
        {
            Execute(@"CREATE view [dbo].[OnlineCourseOverrideDetail]
as
(
	SELECT ocpo.OnlineCourseID as OnlineCourseID,
           ocpo.ParameterSetOptionID as ParameterSetOptionID, 
           pso.ParameterSetID as ParameterSetID, 
           pso.Value as ParameterSetOptionValue,
           p.ID as ParameterID,
           p.Name as ParameterName, 
           p.Code as Code, 
           p.DefaultValue,
	        CASE WHEN pv.Value IS NULL THEN
		        p.DefaultValue
	        ELSE
		        pv.Value
	        END as Value
	FROM OnlineCourseParameterOverride ocpo
    JOIN ParameterSetOption pso on pso.ID = ocpo.ParameterSetOptionID
    JOIN Parameter p on p.ParameterSetID = pso.ParameterSetID
    LEFT JOIN ParameterValue pv on p.ID = pv.ParameterID AND pv.ParameterSetOptionID = ocpo.ParameterSetOptionID
);"); 
        }
    }
}