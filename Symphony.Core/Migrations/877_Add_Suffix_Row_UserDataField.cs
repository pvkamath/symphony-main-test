﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration877 : Migration
    {

        public override void Up()
        {
            Execute (@"INSERT INTO UserDataField (
                CustomerID, 
                Xtype,  
                CodeName, 
                DisplayName,
                CategoryCodeName,
                CategoryDisplayName
            )
            VALUES(
                0, -- global
                'textfield',
                'suffix', 
                'Suffix',
                'other',
                'Other'
            )");
        }
    }
}
