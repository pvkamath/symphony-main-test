﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration561 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.ReportTemplatePermSalesChannels
(
	Id INT IDENTITY(1, 1) NOT NULL,
	ReportTemplateId INT NOT NULL,
	SalesChannelId INT NOT NULL
)
");

            Execute(@"
ALTER TABLE dbo.ReportTemplatePermSalesChannels
ADD CONSTRAINT PK_ReportTemplatePermSalesChannels__Id PRIMARY KEY (Id)
");

            Execute(@"
ALTER TABLE dbo.ReportTemplatePermSalesChannels
ADD CONSTRAINT FK_ReportTemplatePermSalesChannels_ReportTemplate__Id FOREIGN KEY (ReportTemplateId) REFERENCES dbo.ReportTemplate (ID)
");


            Execute(@"
ALTER TABLE dbo.ReportTemplatePermSalesChannels
ADD CONSTRAINT FK_ReportTemplatePermSalesChannels_SalesChannel__Id FOREIGN KEY (SalesChannelId) REFERENCES dbo.SalesChannel(ID)
");


            Execute(@"
CREATE TABLE dbo.ReportTemplatePermCustomers
(
	Id INT IDENTITY(1, 1) NOT NULL,
	ReportTemplateId INT,
	CustomerId INT
)
");

            Execute(@"
ALTER TABLE dbo.ReportTemplatePermCustomers
ADD CONSTRAINT PK_ReportTemplatePermCustomers__Id PRIMARY KEY (Id)

");

            Execute(@"
ALTER TABLE dbo.ReportTemplatePermCustomers
ADD CONSTRAINT FK_ReportTemplatePermCustomers_ReportTemplate__Id FOREIGN KEY (ReportTemplateId) REFERENCES dbo.ReportTemplate (ID)
");

            Execute(@"
ALTER TABLE dbo.ReportTemplatePermCustomers
ADD CONSTRAINT FK_ReportTemplatePermCustomers_Customer__Id FOREIGN KEY (CustomerId) REFERENCES dbo.Customer(ID)
");


            
            
        }
    }
}
