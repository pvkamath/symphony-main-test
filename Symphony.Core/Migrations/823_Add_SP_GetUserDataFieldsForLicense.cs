﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration823 : Migration
    {
        public override void Up()
        {
            Execute(@"
                create procedure GetUserDataFieldsForLicense (@licenseId int) 
                as
                begin
	                select
		                null as ID, 
		                'textfield' as Xtype, 
		                null as Config, 
		                item as CodeName, 
		                null as DisplayName, 
		                6 as [Type],  
		                null as Validator
	                from 
		                License l
		                cross apply fSplit(RequiredUserFields,',')
	                where
		                l.ID = @licenseID
	                union
	                select
		                f.ID, 
		                f.Xtype, 
		                f.Config as Config, 
		                f.CodeName, 
		                f.DisplayName,  
		                7 as [Type],  
		                f.validator as Validator
	                from
		                UserDataFieldLicenseMap l
	                join
		                UserDataField f on f.ID = l.UserDataFieldID
	                where l.LicenseID = @licenseID
	                order by [Type] asc
                end
            ");
        }
    }
}