﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration006 : Migration
    {
        public override void Up()
        {
            Execute("alter table customer add OrganizerSeats int not null default 0");
        }
    }
}