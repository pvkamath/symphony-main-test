﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration180 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "IdentityProviderURL", DbType.String, Int32.MaxValue, true);
        }
    }
}