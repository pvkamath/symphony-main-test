﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration220 : Migration
    {
        public override void Up()
        {

            TableSchema.Table reportTypes = CreateTable("ReportTypes");
            reportTypes.AddPrimaryKeyColumn("ID");
            reportTypes.AddColumn("Name", DbType.String, 64, false);
            reportTypes.AddColumn("UserNotes", DbType.String, 63, false);
            reportTypes.AddColumn("IsEnabled", DbType.Int32, 0, false);
            AddSubSonicStateColumns(reportTypes);

            TableSchema.Table reportsQueues = CreateTable("ReportQueues");
            reportsQueues.AddPrimaryKeyColumn("ID");
            reportsQueues.AddColumn("QueueCommand", DbType.String, 2000, false);
            reportsQueues.AddColumn("ReportType", DbType.Int32, 0, false);
            reportsQueues.AddColumn("IsEnabled", DbType.Int32, 0, false);
            AddSubSonicStateColumns(reportsQueues);

            TableSchema.Table reportJobs = CreateTable("ReportJobs");
            reportJobs.AddPrimaryKeyColumn("ID");
            reportJobs.AddColumn("QueueID", DbType.Int32, 0, false);
            reportJobs.AddColumn("CustomerID", DbType.Int32, 0, false);
            reportJobs.AddColumn("UserNotes", DbType.String, 256, false);
            // quartz parameters
            // key field = (name, group)
            reportJobs.AddColumn("Name", DbType.String, 64, false);
            reportJobs.AddColumn("Group", DbType.String, 64, false);
            reportJobs.AddColumn("Description", DbType.String, 128, false);
            reportJobs.AddColumn("JobType", DbType.String, 64, false);
            AddSubSonicStateColumns(reportJobs);

            TableSchema.Table reportJobTriggers = CreateTable("ReportJobTriggers");
            reportJobTriggers.AddPrimaryKeyColumn("ID");
            reportJobTriggers.AddColumn("Name", DbType.String, 64, false);
            reportJobTriggers.AddColumn("Group", DbType.String, 64, false);
            reportJobTriggers.AddColumn("Description", DbType.String, 128, false);
            // easier for us to link back to job with the jobID
            // but the Quartz standard is to us the (jobname, jobgroup)
            reportJobTriggers.AddColumn("JobID", DbType.Int32, 0, false);
            // key field = (jobname, jobgroup) from the above QuartzJobs table
            reportJobTriggers.AddColumn("JobName", DbType.String, 64, false);
            reportJobTriggers.AddColumn("JobGroup", DbType.String, 64, false);
            reportJobTriggers.AddColumn("MisfireInstruction", DbType.String, 64, false);
            reportJobTriggers.AddColumn("RepeatCount", DbType.Int32, 0, false);
            reportJobTriggers.AddColumn("RepeatInterval", DbType.Int32, 0, false);
            reportJobTriggers.AddColumn("StartAt", DbType.DateTime);
            reportJobTriggers.AddColumn("EndAt", DbType.DateTime);
            //cron expression "5 01 * * * ?"
            reportJobTriggers.AddColumn("CronSchedule", DbType.String, 64, false);
            AddSubSonicStateColumns(reportJobTriggers);
        }
    }
}