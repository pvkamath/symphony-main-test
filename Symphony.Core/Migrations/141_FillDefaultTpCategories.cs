﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration141 : Migration
    {
        public override void Up()
        {
            Execute(@"declare @customerId int
                    declare @categoryId int

                    declare category_cursor cursor for
                    select id from Customer

                    open category_cursor
                    fetch next from category_cursor into @customerId

                    while @@FETCH_STATUS = 0
                    begin
	                    set @categoryId = (select max(ID) from Category where CustomerID = @customerId AND Name = 'Default')
	                    if(@categoryId is null)
	                    begin
	                      insert Category(customerid, name, description, createdon, modifiedon, createdby, modifiedby, categorytypeid)
                               values (@customerId, 'Default', 'Default',getutcdate(),getutcdate(),'','',0)

                              select @categoryId = scope_identity()
	                    end
	                    update TrainingProgram set CategoryID = @categoryId where CustomerID = @customerId

	                    fetch next from category_cursor into @customerId
                    end
                    close category_cursor
                    deallocate category_cursor");
        }
    }
}