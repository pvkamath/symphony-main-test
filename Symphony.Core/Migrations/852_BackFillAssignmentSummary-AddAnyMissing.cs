﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration852 : Migration
    {
        public override void Up()
        {
            // Running backfill migration again. 
            // Adjusting the join to account for re-deployed courses
            // only adding in summaries that don't already exist. 
            Execute(@"
INSERT INTO AssignmentSummary
(
    ArtisanCourseID,
    ArtisanSectionID,
    Name,
    NumQuestions,
    PassingScore,
    CreatedOn,
    ModifiedOn,
    CreatedBy,
    ModifiedBy
)
SELECT
	sp.CourseID as ArtisanCourseID,
	sp.SectionID as ArtisanSectionID,
	s.Name,
	count(distinct p.ID) as NumQuestions,
	coalesce(nullif(oc.passingScoreOverride, 0), nullif(s.passingScore, 0), ac.PassingScore) as PassingScore,
    max(oc.CreatedOn) as CreatedOn,
	max(oc.ModifiedOn) as ModifiedOn,
	'migration' as CreatedBy,
	ac.ModifiedBy
FROM 
    OnlineCourseAssignments a
JOIN
    (
        SELECT
            max(a.Attempt) as Attempt,
            UserID,
            TrainingProgramID,
            CourseID,
            PageID
        FROM
            OnlineCourseAssignments a
        GROUP BY
            UserID,
            TrainingProgramID,
            CourseID,
            PageID
    ) maxAttempt
    ON
        maxAttempt.UserID = a.UserID and
        maxAttempt.TrainingProgramID = a.TrainingProgramID and
        maxAttempt.CourseID = a.CourseID and
        maxAttempt.PageID = a.PageID and
        maxAttempt.Attempt = a.Attempt
JOIN
	OnlineCourse oc
ON
    -- Since surveys create duplicate online courses that both point to the same artisan courses
    -- it is possible for duplicate artisan/section ids to apear in this query. Excluding any
    -- duplicate from online courses as they will just point to the same artisan course anyway
	oc.ID = a.CourseID and oc.DuplicateFromID = 0 
-- joining artisan section pages first instead of the artisan course
-- as we can get to the actual published course the user took by
-- looking up the course for the page in the assignment
JOIN
	ArtisanSectionPages sp
ON
	sp.PageID = a.PageID
JOIN
	ArtisanCourses ac
ON
	ac.ID = sp.CourseID
JOIN
	ArtisanPages p
ON
	p.ID = sp.PageID and p.QuestionType = 8
JOIN
	ArtisanSections s
ON
	s.ID = sp.SectionID
-- Exclude any assignments that already appear in the summary table
WHERE a.ID in (
	select
		a.ID
	from
		OnlineCourseAssignments a
	join
		ArtisanSectionPages sp
		on
		sp.PageID = a.PageID
	left join
		AssignmentSummary s
		on
		s.ArtisanCourseID = sp.CourseID
	where
		s.ID is null
)
GROUP BY
	sp.SectionID, sp.CourseID, s.Name, ac.PassingScore, ac.CreatedBy, ac.ModifiedBy, oc.PassingScoreOverride, ac.PassingScore, s.PassingScore ");

        }
    }
}