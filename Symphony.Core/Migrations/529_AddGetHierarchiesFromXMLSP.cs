﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration529 : Migration
    {

        public override void Up()
        {
            Execute(@"
CREATE PROCEDURE [dbo].[GetHierarchiesFromXML]
	@xmlString XML
AS
BEGIN
select 
	parsed.HierarchyName,
	branch.id
from
	fParseHierarchyXML(@xmlString) parsed
join 
	HierarchyType t on t.CodeName = parsed.HierarchyName
cross apply fSplit(parsed.HierarchyIds, ',') split
cross apply fGetHierarchyBranch(split.item, t.id) branch
END;
");
        }

    }
}
