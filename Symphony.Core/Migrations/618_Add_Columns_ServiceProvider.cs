﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration618 : Migration
    {
        public override void Up()
        {
            AddColumn("ServiceProvider", "NameIdentifier", DbType.String, 2084);
            AddColumn("ServiceProvider", "AssertionUrl", DbType.String, 2084);
        }
    }
}
