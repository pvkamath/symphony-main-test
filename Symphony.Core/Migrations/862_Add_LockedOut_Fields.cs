﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration862 : Migration
    {
        /*
         * 
        [DataMember(Name = "maxSignInAttempts")]
        public int? MaxSignInAttempts { get; set; }

        [DataMember(Name = "lockedOutPeriodInMinutes")]
        public int? LockedOutPeriodInMinutes { get; set; }

        [DataMember(Name = "lockedOutMessage")]
        public string LockedOutMessage { get; set; }
         */
        public override void Up()
        {
            AddColumn("Customer", "MaxSignInAttempts", DbType.Int32);
            AddColumn("Customer", "LockedOutPeriodInMinutes", DbType.Int32);
            AddColumn("Customer", "LockedOutMessage", DbType.String);
        }
    }
}
