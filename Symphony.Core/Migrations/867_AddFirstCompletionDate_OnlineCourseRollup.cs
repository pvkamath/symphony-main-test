﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration867 : Migration
    {

        public override void Up()
        {
            AddColumn("OnlineCourseRollup", "FirstCompletionDate", DbType.DateTime, 0, true);
        }
    }
}
