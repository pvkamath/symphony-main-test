﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration361 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "MinLoTime", DbType.Int32, 0, true);
            AddColumn("ArtisanSections", "MinScoTime", DbType.Int32, 0, true);
            AddColumn("ArtisanCourses", "MinTime", DbType.Int32, 0, true);
            AddColumn("ArtisanCourses", "MinLoTime", DbType.Int32, 0, true);
            AddColumn("ArtisanCourses", "MinScoTime", DbType.Int32, 0, true);
            AddColumn("OnlineCourse", "MinLoTimeOverride", DbType.Int32, 0, true);
            AddColumn("OnlineCourse", "MinScoTimeOverride", DbType.Int32, 0, true);
            AddColumn("OnlineCourse", "MinTimeOverride", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "MinLoTimeOverride", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "MinScoTimeOverride", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "MinTimeOverride", DbType.Int32, 0, true);
            AddColumn("Customer", "MinLoTimeOverride", DbType.Int32, 0, true);
            AddColumn("Customer", "MinScoTimeOverride", DbType.Int32, 0, true);
            AddColumn("Customer", "MinTimeOverride", DbType.Int32, 0, true);

            
        }
    }
}