﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration083 : Migration
    {
        public override void Up()
        {
            RemoveColumn("ArtisanPages", "Sort");
        }
    }
}