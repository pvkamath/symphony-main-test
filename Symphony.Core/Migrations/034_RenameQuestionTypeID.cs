﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration034 : Migration
    {
        public override void Up()
        {
            Execute("sp_RENAME 'ArtisanTemplates.[QuestionTypeID]' , 'QuestionType', 'COLUMN'");
        }
    }
}