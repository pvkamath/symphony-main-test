﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration072 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(626);
            sb.AppendFormat(@"CREATE TABLE [dbo].[ArtisanSectionPages]({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[ID] [int] IDENTITY(1,1) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[SectionID] [int] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[PageID] [int] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@" CONSTRAINT [PK_ArtisanSectionsPages] PRIMARY KEY CLUSTERED {0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[ID] ASC{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ");
            sb.AppendFormat(@"ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]{0}", Environment.NewLine);
            sb.AppendFormat(@") ON [PRIMARY]{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"ALTER TABLE [dbo].[ArtisanSectionPages] ADD  CONSTRAINT [DF_ArtisanSectionsPages_SectionID]  ");
            sb.AppendFormat(@"DEFAULT ((0)) FOR [SectionID]{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"ALTER TABLE [dbo].[ArtisanSectionPages] ADD  CONSTRAINT [DF_ArtisanSectionsPages_PageID]  DEFAULT ");
            sb.AppendFormat(@"((0)) FOR [PageID]{0}", Environment.NewLine);
            Execute(sb.ToString());
        }
    }
}