﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration767 : Migration
    {
        public override void Up()
        {
            AddColumn("Themes", "IsCustomTheme", DbType.Boolean, 0, false, "0");
        }
    }
}