﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration209 : Migration
    {

        public override void Up()
        {
            Execute(@"CREATE PROCEDURE [dbo].[DeleteApplication] (@applicationId int) AS
BEGIN
DELETE FROM 
	HierarchyToTileLinks
	WHERE HierarchyToTileLinks.TileID IN (select ID
		FROM Tiles
		WHERE Tiles.ApplicationID = @applicationId)
DELETE
FROM Tiles
WHERE Tiles.ApplicationID = @applicationId
DELETE
FROM Credentials
WHERE Credentials.ApplicationID = @applicationId
DELETE FROM Applications
	WHERE Applications.ID = @applicationId
END");
        }

        public override void Down()
        {
            Execute(@"DROP PROCEDURE [dbo].[DeleteApplication]");
        }
    }
}