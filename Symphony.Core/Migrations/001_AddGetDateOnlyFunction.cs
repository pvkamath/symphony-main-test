﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration001 : Migration
    {
        public override void Up()
        {
            // create a function to get the date portion of a datetime field
            StringBuilder sb = new StringBuilder(175);
            sb.AppendFormat(@"create function [dbo].[fGetDateOnly]{0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}@DateTime datetime{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"){0}", Environment.NewLine);
            sb.AppendFormat(@"returns datetime{0}", Environment.NewLine);
            sb.AppendFormat(@"as{0}", Environment.NewLine);
            sb.AppendFormat(@"begin{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}return (select cast(floor(cast(@DateTime as decimal(12, 5))) as datetime));{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"end");

            Execute(sb.ToString());
        }
    }
}