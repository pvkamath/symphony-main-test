﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration723 : Migration
    {

        public override void Up()
        {
            // Using Int64 to keep things consistant with SCORM database. They use a long when returning the 
            // total seconds tracked for the course. This seems unreasonable since an Int32 can store 68 years
            // worth of seconds, and it likely wouldn't be possible to spend 68 years taking one course.
            AddColumn("OnlineCourseRollup", "TotalSecondsPreviousRegistration", DbType.Int64, 0, false, "0");
            AddColumn("OnlineCourseRollup", "CurrentRegistrationID", DbType.String, 100, false, "0");
            AddColumn("TrainingProgramRollup", "TotalSeconds", DbType.Int64, 0, false, "0");
            AddColumn("TrainingProgramRollup", "PercentComplete", DbType.Int32, 0, false, "0");
            AddColumn("TrainingProgram", "MinCourseWork", DbType.Int32, 0, true);
        }

    }
}
