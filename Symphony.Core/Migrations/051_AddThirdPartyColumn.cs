﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration051 : Migration
    {
        public override void Up()
        {
            AddColumn("Class", "IsThirdParty", DbType.Boolean, 0, false, "0");
            AddColumn("Course", "IsThirdParty", DbType.Boolean, 0, false, "0");            
        }
    }
}