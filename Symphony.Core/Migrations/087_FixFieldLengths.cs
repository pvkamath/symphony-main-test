﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration087 : Migration
    {
        public override void Up()
        {
            AlterColumn("ArtisanCourses", "Name", DbType.String, 512);
            AlterColumn("ArtisanCourses", "Description", DbType.String, 1024);
            AlterColumn("ArtisanCourses", "Keywords", DbType.String, 1024);

            AlterColumn("ArtisanSections", "Name", DbType.String, 512);

            AlterColumn("ArtisanPages", "Name", DbType.String, 512);

            AlterColumn("ArtisanAssets", "Name", DbType.String, 512);
            AlterColumn("ArtisanAssets", "Description", DbType.String, 1024);
        }
    }
}