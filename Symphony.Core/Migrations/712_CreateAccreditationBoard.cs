﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration712 : Migration
    {
        public override void Up()
        {
            TableSchema.Table board = CreateTableWithKey("AccreditationBoard", "ID");

            board.AddColumn("Name", DbType.String, 128, false);
            board.AddColumn("PrimaryContact", DbType.String, 64, true);
            board.AddColumn("Phone", DbType.String, 32, true);

            AddSubSonicStateColumns(board);
        }
    }
}
