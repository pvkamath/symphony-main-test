﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration761 : Migration
    {
        public override void Up()
        {

            /* As far sas I can tell, this doesn't need to be a view, however 
             * since things rely on this just updating to add AllowLibraries */
            Execute(@"
ALTER VIEW [dbo].[CustomerNetworkDetailsInfo]
AS

SELECT
	cnd.DetaildId,
	cnd.NetworkId,
	cnd.SourceCustomerId,
	cs.Name AS [SourceCustomerName],
	cnd.DestinationCustomerId,
	cd.Name AS [DestinationCustomerName],
	cnd.AllowTrainingProgramSharing,
	cnd.AllowReporting,
    cnd.AllowLibraries
FROM
	dbo.CustomerNetworkDetail cnd WITH (NOLOCK)
	INNER JOIN dbo.Customer cs WITH (NOLOCK) ON cs.ID = cnd.SourceCustomerId
	INNER JOIN dbo.Customer cd WITH (NOLOCK) ON cd.ID = cnd.DestinationCustomerId

;
");
        }

    }
}