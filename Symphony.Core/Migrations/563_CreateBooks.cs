using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration563 : Migration
    {

        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.Books
(
	Id INT IDENTITY(1, 1) NOT NULL,
	Name VARCHAR(200) NOT NULL,
	TypeFormat VARCHAR(50),
	Author VARCHAR(100) NOT NULL,
	ISBN VARCHAR(50),
	Publisher VARCHAR(100)
)
");

            Execute(@"
ALTER TABLE dbo.Books
ADD CONSTRAINT PK_Books__Id PRIMARY KEY (Id)
");


            Execute(@"
CREATE TABLE dbo.TrainingProgramBooks
(
    Id INT IDENTITY(1, 1) NOT NULL,
    TrainingProgramId INT NOT NULL,
    BookId INT NOT NULL
)
");

            Execute(@"
ALTER TABLE dbo.TrainingProgramBooks
ADD CONSTRAINT PK_TrainingProgramBooks__Id PRIMARY KEY (Id)
");

            Execute(@"
ALTER TABLE dbo.TrainingProgramBooks
ADD CONSTRAINT FK_TrainingProgramBooks_TrainingPrograms FOREIGN KEY (TrainingProgramId) REFERENCES dbo.TrainingProgram(ID)
");

            Execute(@"
ALTER TABLE dbo.TrainingProgramBooks
ADD CONSTRAINT FK_TrainingProgramBooks_Books FOREIGN KEY (BookId) REFERENCES dbo.Books(ID)
");


		}
	}
}