﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration042 : Migration
    {
        public override void Up()
        {
            Execute("insert ArtisanAssets values (1, 'Snowman', 'This is a snowman', '/images/snowman.gif', 1, '1/1/1', '1/1/1', 'jerod', 'jerod')");
        }
    }
}