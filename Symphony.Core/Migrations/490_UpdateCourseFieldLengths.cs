﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration490 : Migration
    {

        public override void Up()
        {
            Execute(@"
DROP INDEX name_index on Class
DROP INDEX course_name_index on Course

alter table Class alter column [Description] nvarchar(max);
alter table Course alter column [CourseObjective] nvarchar(max);
alter table Course alter column [Description] nvarchar(max);

CREATE NONCLUSTERED INDEX [name_index] ON [dbo].[Class] 
(
	[Name] ASC
)
INCLUDE ( [Description],
[InternalCode]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [course_name_index] ON [dbo].[Course] 
(
	[Name] ASC
)
INCLUDE ( [Description]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

");
        }

    }
}
