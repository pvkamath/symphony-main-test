﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration815 : Migration
    {

        public override void Up()
        {
            // Clear assignment summary attempts when assignments are cleared. 
            Execute(@"
ALTER PROCEDURE [dbo].[CourseReset]
	@onlineCourseRollupId INT = 0,
	@resetAssignments BIT = 0
AS
	DECLARE @userId INT
	DECLARE @courseId INT
	DECLARE @trainingProgramId INT
	DECLARE @retCode INT
	SET @retCode = 0

	SET @userId = 0
	SET @courseId = 0
	SET @trainingProgramId = 0

	SELECT
		@userId = r.UserID,
		@courseId = r.CourseID,
		@trainingProgramId = r.TrainingProgramID
		FROM
			OnlineCourseRollup r
	WHERE r.ID = @onlineCourseRollupId

	IF (@resetAssignments = 1)
	BEGIN
		UPDATE dbo.OnlineCourseAssignments
		SET
			IsReset = 1
		WHERE
			UserID = @userId AND
			TrainingProgramID = @trainingProgramId AND
			CourseID = @courseId

		INSERT INTO dbo.OnlineCourseRollupArchive (UserID, CourseID, TrainingProgramID, Score, TotalSeconds, Success, Completion, ModifiedOn, ModifiedBy, AttemptCount, HighScoreDate, OriginalRegistrationID, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, TestAttemptCount, ResetNotes, LastResetDate, CanMoveForwardIndicator, NavigationPercentage)
		SELECT
			UserID, CourseID, TrainingProgramID, Score, TotalSeconds, Success, Completion, ModifiedOn, ModifiedBy, AttemptCount, HighScoreDate, OriginalRegistrationID, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, TestAttemptCount, ResetNotes, LastResetDate, CanMoveForwardIndicator, NavigationPercentage
		FROM
			dbo.OnlineCourseRollup WITH (NOLOCK)
		WHERE
			UserID = @userId AND
			CourseID = @courseId AND
			TrainingProgramID = @trainingProgramId

		INSERT INTO dbo.OnlineCourseAssignmentsArchive (UserID, TrainingProgramID, CourseID, PageID, Attempt, Date, ResponseStatus, IsCorrect, Response, InstructorFeedback, IsAutoResponse, GradedDate, IsReset)
		SELECT
			UserID, TrainingProgramID, CourseID, PageID, Attempt, Date, ResponseStatus, IsCorrect, Response, InstructorFeedback, IsAutoResponse, GradedDate, IsReset
		FROM dbo.OnlineCourseAssignments WITH (NOLOCK)
		WHERE 
			UserID = @userId AND
			CourseID = @courseId AND
			TrainingProgramID = @trainingProgramId

        -- Not soft deleting or archiving these here since they
		-- are generated from the OnlineCourseAssignments table. 
		DELETE FROM dbo.AssignmentSummaryAttempts
		WHERE
			UserID = @userId AND
			OnlineCourseID = @courseId AND
			TrainingProgramID = @trainingProgramId

		DELETE FROM dbo.OnlineCourseAssignments
		WHERE
			UserID = @userId AND
			CourseID = @courseId AND
			TrainingProgramID = @trainingProgramId

		UPDATE dbo.OnlineCourseRollup
		SET
			Score = NULL,
			Success = NULL,
			TotalSeconds = 0,
			Completion = NULL,
			AttemptCount = 0,
			HighScoreDate = NULL,
			TestAttemptCount = 0,
			CanMoveForwardIndicator = 0,
			NavigationPercentage =  0,
            TotalSecondsPreviousRegistration = 0,
            TotalSecondsLastAttempt = 0,
            TotalSecondsRounded = 0,
            ArtisanAuditID = NULL
		WHERE
			UserID = @userId AND
			CourseID = @courseId AND
			TrainingProgramID = @trainingProgramId

	END;

	IF (@courseId > 0)
	BEGIN
		UPDATE OnlineTraining.dbo.ScormRegistration
		SET course_id = 0
		WHERE [user_id] = @userId
		AND course_id = @courseId
		AND (CASE
			WHEN training_program_id IS NULL THEN 0
			WHEN training_program_id IS NOT NULL THEN training_program_id
		END) = @trainingProgramId
	END
");
        }

    }
}