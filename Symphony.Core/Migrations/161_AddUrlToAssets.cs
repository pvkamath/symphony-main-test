﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration161 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanAssets", "Url", DbType.String, 2048, true);
        }
    }
}