﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration739 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourseAccreditation", "ProfessionID", DbType.Int32, 0, true);
            AddColumn("OnlineCourseAccreditation", "ProfessionID", DbType.Int32, 0, true);

            Execute("ALTER TABLE ArtisanCourseAccreditation"
                + " ADD CONSTRAINT fk_Profession_ID_ArtisanCourseAccreditation_ProfessionID FOREIGN KEY(ProfessionID)"
                    + " REFERENCES Profession(ID)"
                );

            Execute("ALTER TABLE OnlineCourseAccreditation"
                + " ADD CONSTRAINT fk_Profession_ID_OnlineCourseAccreditation_ProfessionID FOREIGN KEY(ProfessionID)"
                    + " REFERENCES Profession(ID)"
                );
        }
    }
}