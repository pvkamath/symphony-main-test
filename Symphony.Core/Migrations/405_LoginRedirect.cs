﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration405 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "LoginRedirect", DbType.String, 2048, true);
        }
    }
}