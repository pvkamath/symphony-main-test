﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration044 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(1265);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"insert templates {0}", Environment.NewLine);
            sb.AppendFormat(@"{0}([subject], body, codename, displayname, [description], priority, customerid, ", Environment.NewLine);
            sb.AppendFormat(@"[enabled],modifiedby,createdby,modifiedon,createdon,area,isscheduled,scheduleparameterid, ");
            sb.AppendFormat(@"relativescheduledminutes,filtercomplete){0}", Environment.NewLine);
            sb.AppendFormat(@"values {0}", Environment.NewLine);
            sb.AppendFormat(@"{0}('New Hire Status Chat', 'Your new hire status has changed', 'NewHireStatusChange','New Hire ", Environment.NewLine);
            sb.AppendFormat(@"Status Change', 'Occurs when a student''s goes from new hire to non-new hire.',1,0,0, ");
            sb.AppendFormat(@"'jerod','jerod','1/1/1','1/1/1','',0,0,0,0){0}", Environment.NewLine);
            sb.AppendFormat(@"  {0}", Environment.NewLine);
            sb.AppendFormat(@"declare @customerId int{0}", Environment.NewLine);
            sb.AppendFormat(@"  {0}", Environment.NewLine);
            sb.AppendFormat(@"declare customer_cursor cursor for{0}", Environment.NewLine);
            sb.AppendFormat(@"select id from customer{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"open customer_cursor;{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"fetch next from customer_cursor{0}", Environment.NewLine);
            sb.AppendFormat(@"into @customerId{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"while(@@FETCH_STATUS = 0){0}", Environment.NewLine);
            sb.AppendFormat(@"begin{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}insert templates {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}([subject], body, codename, displayname, [description], priority, customerid, ", Environment.NewLine);
            sb.AppendFormat(@"[enabled],modifiedby,createdby,modifiedon,createdon,area,isscheduled,scheduleparameterid, ");
            sb.AppendFormat(@"relativescheduledminutes,filtercomplete){0}", Environment.NewLine);
            sb.AppendFormat(@"{0}values {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}('New Hire Status Chat', 'Your new hire status has changed', 'NewHireStatusChange','New Hire ", Environment.NewLine);
            sb.AppendFormat(@"Status Change', 'Occurs when a student''s goes from new hire to non-new hire.',1,@customerId,0, ");
            sb.AppendFormat(@"'jerod','jerod','1/1/1','1/1/1','',0,0,0,0){0}", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}fetch next from customer_cursor{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}into @customerId{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"end{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"close customer_cursor{0}", Environment.NewLine);
            sb.AppendFormat(@"deallocate customer_cursor");


            Execute(sb.ToString());
        }
    }
}