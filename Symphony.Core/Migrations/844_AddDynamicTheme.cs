﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration844 : Migration
    {
        public override void Up()
        {
            Execute(@"INSERT INTO [Symphony].[dbo].[ArtisanThemes]
           ([Name]
           ,[Description]
           ,[CssPath]
           ,[CreatedOn]
           ,[ModifiedOn]
           ,[CreatedBy]
           ,[ModifiedBy]
           ,[CustomerID]
           ,[Folder]
           ,[SkinName]
           ,[IsDynamic])
     VALUES
           ('OnCourse (Dynamic)'
           ,'A theme that allows custom color and image choices'
           ,'/skins/artisan_content/default/css/theme.css'
           ,GETUTCDATE()
           ,GETUTCDATE()
           ,'admin'
           ,'admin'
           ,1
           ,'/skins/artisan_content/default'
           ,'oncourse_dynamic'
           ,1)
");
        }
    }
}
