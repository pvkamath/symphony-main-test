﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration646 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER FUNCTION [dbo].[fGetItemsForLibrary_Search]
(
	@libraryId INT,
	@libraryItemTypeId INT = null,
	@customerId INT,
	@isShowAll bit,
	@isHideInLibrary bit,
	@categoryId int = null,
	@secondaryCategoryIdsCS nvarchar(max),
    @secondaryCategorySearch nvarchar(2000),
    @authorIdsCS nvarchar(max),
    @authorSearch nvarchar(2000),
	@search nvarchar(50), 
	@orderBy nvarchar(50), 
	@orderDir varchar(4),
	@isFavorite int,
	@isInProgress int, 
	@isNotStarted int,
	@userId int,
	@startDate datetime = null,
	@endDate datetime = null
)
RETURNS @Tr TABLE (
	ID int,
	LibraryItemID int,
	LibraryID int,
	Name nvarchar(2000),
	[Description] nvarchar(max),
	CategoryID int,
	Duration int,
	LibraryItemTypeID int, 
	CreatedOn datetime,
	CourseTypeID int,
	CategoryName nvarchar(max),
	LevelIndentText nvarchar(max),
	IsInLibrary bit,
	IsFavorite bit,
	IsInProgress bit,
	LibraryFavoriteID int,
	r int,
	ItemCreatedOn datetime,
	ItemModifiedOn datetime,
	CourseVersion int
)
AS
BEGIN

	
	if (@secondaryCategorySearch is null or @secondaryCategorySearch = '' or len(@secondaryCategorySearch) = 0)
	begin
		set @secondaryCategorySearch = '""""'
	end
	
	if (@authorSearch is null or @authorSearch = '' or len(@authorSearch) = 0)
	begin
		set @authorSearch = '""""'
	end
	

	declare @authorIds table (id int)
	declare @secondaryCategoryIds table (id int)
	
	insert into @secondaryCategoryIds
		select CAST(LTRIM(RTRIM(item)) as int)
		from fSplit(@secondaryCategoryIdsCS,',');
	insert into @authorIds
		select CAST(LTRIM(RTRIM(item)) as int)
		from fSplit(@authorIdsCS,',');
	
	insert into @Tr
	select
		item.ID,
		item.LibraryItemID,
		item.LibraryID,
		item.Name, 
		item.[Description],
		item.CategoryID,
		item.Duration,
		item.LibraryItemTypeID,
		item.CreatedOn,
		item.CourseTypeID,
		item.CategoryName,
		item.LevelIndentText, 
		item.IsInLibrary,
		item.IsFavorite,
		item.IsInProgress,
		item.LibraryFavoriteID,
		row_number() over 
		(
			order by
				case when @orderBy = 'CourseTypeID' and @orderDir = 'asc' then item.CourseTypeID end, 
				case when @orderBy = 'CourseTypeID' and @orderDir = 'desc' then item.CourseTypeID end desc,
				case when @orderBy = 'Name' and @orderDir = 'asc' then item.Name end,
				case when @orderBy = 'Name' and @orderDir = 'desc' then item.Name end desc,
				case when @orderBy = 'Category' and @orderDir = 'asc' then item.LevelIndentText end,
				case when @orderBy = 'Category' and @orderDir = 'desc' then item.LevelIndentText end desc,
				case when @orderBy = 'CreatedOn' and @orderDir = 'asc' then item.CreatedOn end,
				case when @orderBy = 'CreatedOn' and @orderDir = 'desc' then item.CreatedOn end desc,
				case when @orderBy = 'DisplayInLibrary' and @orderDir = 'asc' then item.IsInLibrary end,
				case when @orderBy = 'DisplayInLibrary' and @orderDir = 'desc' then item.IsInLibrary end desc,
				case when @orderBy = 'ItemCreatedOn' and @orderDir = 'asc' then item.ItemCreatedOn end,
				case when @orderBy = 'ItemCreatedOn' and @orderDir = 'desc' then item.ItemCreatedOn end desc,
				case when @orderBy = 'ItemModifiedOn' and @orderDir = 'asc' then item.ItemModifiedOn end,
				case when @orderBy = 'ItemModifiedOn' and @orderDir = 'desc' then item.ItemModifiedOn end desc,
				case when @orderBy = 'CourseVersion' and @orderDir = 'asc' then item.CourseVersion end,
				case when @orderBy = 'CourseVersion' and @orderDir = 'desc' then item.CourseVersion end desc
		)  as r,
		item.ItemCreatedOn,
		item.ItemModifiedOn,
		item.CourseVersion
	from fGetItemsForLibrary(
		@libraryId, 
		@libraryItemTypeId, 
		@customerID, 
		@isShowAll, 
		@isHideInLibrary,
		@isFavorite,
		@isInProgress, 
		@isNotStarted,
		@userId
	) item
	left join 
		OnlineCourseAuthorLink al
	on 
		al.OnlineCourseId = item.Id and item.LibraryItemTypeId = 1
	left join
		[User] u
	on 
		u.Id = al.UserID
	left join
		OnlineCourseSecondaryCategoryLink scl
	on
		scl.OnlineCourseId = item.Id and item.LibraryItemTypeId = 1
	left join
		SecondaryCategory sc
	on
		sc.Id = scl.SecondaryCategoryId
	where
		(item.Name like '%' + @search + '%' or item.LevelIndentText like '%' + @search + '%') and
		(@categoryId > 0 and item.CategoryID = @categoryID or (@categoryId is null or @categoryId = 0)) and
		(
			(@authorIdsCS is null or len(@authorIdsCS) = 0) or
			(@authorIdsCS is not null and u.Id in (select id from @authorIds))
		) and
		(
			(@secondaryCategoryIdsCS is null or len(@secondaryCategoryIdsCS) = 0) or
			(@secondaryCategoryIdsCS is not null and sc.Id in (select id from @secondaryCategoryIds))
		) and
		( @secondaryCategorySearch = '""""' or CONTAINS(sc.Name, @secondaryCategorySearch)) and
		( @authorSearch = '""""' or CONTAINS((u.firstName, u.lastName), @authorSearch)) and
		( @startDate is null or item.CreatedOn > @startDate) and
		( @endDate is null or item.CreatedOn < @endDate)
	group by 
		item.ID,
		item.LibraryItemID,
		item.LibraryID,
		item.Name,
		item.[Description],
		item.CategoryID,
		item.Duration,
		item.LibraryItemTypeID, 
		item.CreatedOn,
		item.CourseTypeID,
		item.CategoryName,
		item.LevelIndentText,
		item.IsInLibrary,
		item.IsFavorite,
		item.LibraryFavoriteID,
		item.IsInProgress,
		item.ItemCreatedOn,
		item.ItemModifiedOn,
		item.CourseVersion
	return
END
;

");
        }
    }
}
