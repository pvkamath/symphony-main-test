﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration691 : Migration
    {
        public override void Up()
        {
            AddColumn("ReportEntity", "ReportEntityTypeID", DbType.Int32, 0, true);

            TableSchema.Table reportEntityType = CreateTableWithKey("ReportEntityType", "ID");
            reportEntityType.AddColumn("Name", DbType.String, 32, false);
            reportEntityType.AddColumn("SymphonyTable", DbType.String, 128, true);
            reportEntityType.AddColumn("SymphonyIdColumn", DbType.String, 128, true);
            reportEntityType.AddColumn("SymphonyNameColumn", DbType.String, 128, true);
            reportEntityType.AddColumn("ReportingTable", DbType.String, 128, true);
            reportEntityType.AddColumn("ReportingIdcolumn", DbType.String, 128, true);
            reportEntityType.AddColumn("ReportingNameColumn", DbType.String, 128, true);
            AddSubSonicStateColumns(reportEntityType);

        }
    }
}
