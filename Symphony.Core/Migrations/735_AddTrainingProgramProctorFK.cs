﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration735 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE TrainingProgram"
                + " ADD CONSTRAINT fk_ProctorForm_ID_TrainingProgram_ProctorFormID FOREIGN KEY(ProctorFormID)"
                    + " REFERENCES ProctorForm(ID)"
                );
        }
    }
}