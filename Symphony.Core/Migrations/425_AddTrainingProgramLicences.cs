﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration425 : Migration
    {

        public override void Up()
        {
            TableSchema.Table trainingProgramLicences = CreateTableWithKey("TrainingProgramLicence", "ID");
            trainingProgramLicences.AddColumn("LicenceID", DbType.Int32, 0, false);
            trainingProgramLicences.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            trainingProgramLicences.AddColumn("ApprovalCode", DbType.String, 64, true, "''");

        }

        public override void Down()
        {

        }
    }
}