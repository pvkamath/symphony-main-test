﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration879 : Migration
    {

        public override void Up()
        {
            Execute(@"
ALTER procedure [dbo].[GetTrainingProgramsForUser_WithInProgressSort](@userId int, @newHire bit, @searchText nvarchar(50), @orderBy nvarchar(50), @orderDir varchar(4), @pageIndex int, @pageSize int, @startDate datetime = null, @endDate datetime = null)
as
begin
	set @orderDir = lower(@orderDir);
	set @orderBy = lower(@orderBy);
	-- create a date time that represents today without the time
	declare @today datetime
	declare @hireDate datetime
	declare @newHireEndDate datetime
	declare @newHireDuration int
	declare @newHireTransitionPeriod int
	declare @isUseDateRange bit;

	--set @pageIndex = @pageIndex + 1
	
	set @today = (select cast(floor(cast(getdate() as decimal(12, 5))) as datetime));
	set @hireDate = (select HireDate from [user] with (nolock) where id = @userId);
	set @newHireEndDate = (select NewHireEndDate from [user] with (nolock) where id = @userId);
	set @newHireDuration = (select NewHireDuration from Customer with (nolock) where ID = (select CustomerID from [user] where id = @userId));
	set @newHireTransitionPeriod = (select NewHireTransitionPeriod from Customer with (nolock) where ID = (select CustomerID from [user] where id = @userId));
	set @isUseDateRange = case when @startDate is not null and @endDate is not null then 1 else 0 end;
	
	--if(@newHireTransitionPeriod is null or @newHireTransitionPeriod = 0)
	--begin
		if(@newHireEndDate is null and @newHire = 0)
		begin
			set @newHireEndDate = @hireDate
		end;
	--end;
	
	
	-- if the user's new hire date + transition > enddate, exclude it
	
	-- get all the tp details
	with TP as(
		select
			TPa.ID, TPa.CustomerID, TPa.OwnerID, TPa.Name, TPa.InternalCode, TPa.Cost, TPa.[Description], 
			TPa.IsNewHire, TPa.IsLive, TPa.EnforceRequiredOrder, 
			TPa.MinimumElectives, TPa.FinalAssessmentCourseID, TPa.FinalAssessmentCourseTypeID, 
			TPa.ModifiedBy, TPa.CreatedBy, TPa.ModifiedOn, TPa.CreatedOn,
			TPa.NewHireOffsetEnabled, TPa.NewHireStartDateOffset, TPa.NewHireDueDateOffset, TPa.NewHireEndDateOffset,
			TPa.NewHireTransitionPeriod, TPa.IsSalesforce, TPa.PurchasedFromBundleName,
			TPa.SortName, TPa.[Order], TPa.StartDate, TPa.DueDate, TPa.EndDate,
			TPa.CourseCount,
			TPa.MessageBoardID,
			TPa.Sku,
			TPa.ParentTrainingProgramID,
			cast(
                case when 
					TPa.StartDate <= @today
				and
					TPa.EndDate >= @today
				and 
					TPa.EndDate >= case when @newHire = 0 then dateadd(d, coalesce(TPa.NewHireTransitionPeriod, TPa.CustNewHireTransitionPeriod, 0), @newHireEndDate) else @today end
				then 1
				else 0
			    end as bit
            ) as IsActive,
			case 
				when @isUseDateRange = 0 then 1
				when @startDate <= TPa.StartDate and @endDate >= TPa.EndDate then 1
				else 0
			end as IsInDateRange,
			TPa.SessionCourseID,
			TPa.CategoryName,
			tpr.TrainingProgramID,
			tpr.UserID,
			tpr.TotalSeconds,
            tpr.IsCompleteOverride,
            tpr.DateCompleted,
            tpr.IsSurveyComplete,
            tpr.RequiredCompletedCount,
            tpr.ElectiveCompletedCount,
            tpr.FinalCompletedCount,
            tpr.AssignmentsCompletedCount,
            tpr.FinalExamScore,
            tpr.IsRequiredComplete,
            tpr.IsElectiveComplete,
            tpr.IsFinalComplete,
            tpr.IsAssignmentsComplete,
            tpr.IsComplete,
            tpr.PercentComplete,
            tpr.ProfessionID,
            tpr.IsAccreditationSurveyComplete,
            case
				when tpr.PercentComplete is null then 0
				when tpr.PercentComplete >= 0 and tpr.PercentComplete <= 99 then PercentComplete
				else -1 
			end as InProgressOrder
		from (
			select
				TP.ID, TP.CustomerID, TP.OwnerID, TP.Name, TP.InternalCode, TP.Cost, TP.[Description], 
				TP.IsNewHire, TP.IsLive, TP.EnforceRequiredOrder, 
				TP.MinimumElectives, TP.FinalAssessmentCourseID, TP.FinalAssessmentCourseTypeID, 
				TP.ModifiedBy, TP.CreatedBy, TP.ModifiedOn, TP.CreatedOn,
				TP.NewHireOffsetEnabled, TP.NewHireStartDateOffset, TP.NewHireDueDateOffset, TP.NewHireEndDateOffset,
				TP.NewHireTransitionPeriod, TP.IsSalesforce, tpb.Name as PurchasedFromBundleName,
				case when tpb.Name is not null
					then tpb.Name
					else tp.Name
				end as SortName,
				tpbm.[Order] as [Order],
				case when @newHire = 1 
					then dateadd(d, 
							case when tp.NewHireOffsetEnabled = 1 
								then tp.NewHireStartDateOffset 
								else 0
							end, @hireDate) 
					else tp.StartDate end as StartDate,
				case when @newHire = 1 
					then dateadd(d, 
							case when tp.NewHireOffsetEnabled = 1 
								then tp.NewHireDueDateOffset 
								else @newHireDuration
							end, @hireDate) 
					else tp.DueDate end as DueDate,
					
				case when @newHire = 1 
					then dateadd(d, 
							case when tp.NewHireOffsetEnabled = 1 
								then tp.NewHireEndDateOffset 
								else @newHireDuration
							end, @hireDate) 
					else tp.EndDate end as EndDate,
				
				T.CourseCount,
				(
					SELECT TOP 1 mb.ID
					FROM MessageBoard as mb with (nolock) 
					WHERE mb.TrainingProgramID = TP.ID
				) as MessageBoardID,
				CASE 
					WHEN tp.IsSalesforce = 1 and tp.PurchasedFromBundleID is null THEN parentTp.Sku
					WHEN tp.IsSalesforce = 1 and tp.PurchasedFromBundleID is not null THEN tpb.Sku
					WHEN tp.IsSalesforce = 0 THEN tp.Sku
				END as Sku,
				tp.ParentTrainingProgramID,
				cust.NewHireTransitionPeriod as CustNewHireTransitionPeriod,
				CASE
					WHEN tp.parentTrainingProgramID > 0 then parentTp.SessionCourseID else tp.SessionCourseID 
				END as SessionCourseID,
				cat.Name as CategoryName
			from
				TrainingProgram tp with (nolock)
			join
				(
					-- make sure we only ever see 1 instance of a given tp
					select
						distinct(X.TrainingProgramID) as TrainingProgramID, 
                        -- If # of exams > 1, only consider 1 as the course count, this should appear to the user as if there is only
                        -- 1 exam to complete, the rest are just retry variations
						(count(distinct cl.ID)) + (case when (count(distinct clX.ID)) > 0 then 1 else 0 end)  as CourseCount
					from
						(
							-- get the assigned tps
							select
								l.TrainingProgramID, 
								tp.Name,
								tp.MinimumElectives
							from
								hierarchytotrainingprogramlink l with (nolock)
							join
								(

									select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user]  with (nolock) where id = @userid), 1)
									
									union
									
									select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user]  with (nolock) where id = @userid), 2)
									
									union
									
									select f.ID, f.TypeID from useraudience with (nolock)
									cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
									where userid = @userid
									
									union
									
									select
										@userId, 4 -- 'user' type id
								) as H
							on
								h.TypeID = l.HierarchyTypeID
								and
								h.ID = l.HierarchyNodeID
							join
								TrainingProgram tp with (nolock)
							on
								tp.ID = l.TrainingProgramID
							group by
								l.trainingprogramid, tp.name, tp.MinimumElectives
								
						) as X
					left join
						dbo.TrainingProgramToCourseLink cl with (nolock)
					on
						X.TrainingProgramID = cl.TrainingProgramID and cl.SyllabusTypeID != 4
					left join
						dbo.TrainingProgramToCourseLink clX with (nolock) 
					on
						X.TrainingProgramID = clX.TrainingProgramID and clX.SyllabusTypeID = 4
					group by
						X.TrainingProgramID, X.MinimumElectives
				)
			as T
			on
				tp.ID = T.TrainingProgramID
			left join
				dbo.TrainingProgram parentTp with (nolock)
			on
				parentTp.ID = tp.ParentTrainingProgramID
			left join
				dbo.TrainingProgramBundle tpb  with (nolock)
			on
				tpb.ID = tp.PurchasedFromBundleID
			left join
				dbo.TrainingProgramBundleMap tpbm  with (nolock)
			on
				tpbm.TrainingProgramID = tp.ParentTrainingProgramID
				and tpbm.ID = tpb.ID
			left join
				dbo.Customer cust  with (nolock)
			on 
				cust.ID = tp.CustomerID
			left join
				dbo.Category cat with (nolock)
			on
				cat.ID = tp.CategoryID
			where
				(
					tp.Name like '%' + @searchText + '%'
					or tpb.Name like '%' + @searchText + '%'
					or CASE 
						WHEN tp.IsSalesforce = 1 and tp.PurchasedFromBundleID is null THEN parentTp.Sku
						WHEN tp.IsSalesforce = 1 and tp.PurchasedFromBundleID is not null THEN tpb.Sku
						WHEN tp.IsSalesforce = 0 THEN tp.Sku
					END like '%' + @searchText + '%'
				)
			and
				tp.IsNewHire = @newHire
			and 
				tp.IsLive = 1
		) TPa
		left join 
			TrainingProgramRollup tpr with (nolock)
		on 
			tpr.TrainingProgramID = TPa.ID and tpr.UserID = @userId
	)
	select 
		d.ID, d.CustomerID, d.OwnerID, d.Name, d.InternalCode, d.Cost, d.[Description], d.IsNewHire, d.IsLive, 
		d.StartDate as StartDate,
		d.EndDate as EndDate,
		-- new hire end date is the due date
		d.DueDate as DueDate,
		d.EnforceRequiredOrder, d.MinimumElectives, d.FinalAssessmentCourseID, 
		d.FinalAssessmentCourseTypeID, d.ModifiedBy, d.CreatedBy, d.ModifiedOn, d.CreatedOn,
		d.CourseCount,
		max(TotalSize) as TotalSize,
		d.MessageBoardID,
		d.Sku,
		d.ParentTrainingProgramID,
		d.IsActive,
        d.TotalSeconds,
        d.IsCompleteOverride,
        d.DateCompleted,
        d.IsSurveyComplete,
        d.RequiredCompletedCount,
        d.ElectiveCompletedCount,
        d.FinalCompletedCount,
        d.AssignmentsCompletedCount,
        d.FinalExamScore,
        d.IsRequiredComplete,
        d.IsElectiveComplete,
        d.IsFinalComplete,
        d.IsAssignmentsComplete,
        d.IsComplete,
        d.PercentComplete,
        d.ProfessionID,
        d.IsAccreditationSurveyComplete,
        d.SessionCourseID,
        d.CategoryName
	from
	(
		select
			TP.ID, TP.CustomerID, TP.OwnerID, TP.Name, TP.InternalCode, TP.Cost, TP.[Description], TP.IsNewHire, TP.IsLive, 
			TP.StartDate as StartDate,
			TP.EndDate as EndDate,
			-- new hire end date is the due date
			TP.DueDate as DueDate,
			TP.EnforceRequiredOrder, TP.MinimumElectives, TP.FinalAssessmentCourseID, 
			TP.FinalAssessmentCourseTypeID, TP.ModifiedBy, TP.CreatedBy, TP.ModifiedOn, TP.CreatedOn,
			TP.CourseCount,
			(select count(*) from TP where 
				(@isUseDateRange = 0 and TP.IsActive = 1)
				or
				(@isUseDateRange = 1 and TP.IsInDateRange = 1)
			) as TotalSize,
			row_number() over (order by 
				case when @orderBy = 'Name' and @orderDir = 'asc' then TP.SortName end, 
				case when @orderBy = 'Name' and @orderDir = 'desc' then TP.SortName end desc,
				case when @orderBy = 'Description' and @orderDir = 'asc' then TP.[Description] end,
				case when @orderBy = 'Description' and @orderDir = 'desc' then TP.[Description] end desc,
				case when @orderBy = 'StartDate' and @orderDir = 'asc' then TP.StartDate end,
				case when @orderBy = 'StartDate' and @orderDir = 'desc' then TP.StartDate end desc,
				case when @orderBy = 'EndDate' and @orderDir = 'asc' then TP.EndDate end,
				case when @orderBy = 'EndDate' and @orderDir = 'desc' then TP.EndDate end desc,
				case when @orderBy = 'DueDate' and @orderDir = 'asc' then TP.DueDate end,
				case when @orderBy = 'DueDate' and @orderDir = 'desc' then TP.DueDate end desc,
				case when @orderBy = 'Sku' and @orderDir = 'desc' then TP.Sku end desc,			
				case when @orderBy = 'Sku' and @orderDir = 'asc' then TP.Sku end asc,
				case when @orderBy = 'PurchasedFromBundleName' and @orderDir = 'desc' then TP.PurchasedFromBundleName end desc,			
				case when @orderBy = 'PurchasedFromBundleName' and @orderDir = 'asc' then TP.PurchasedFromBundleName end asc,
				case when @orderBy = 'IsSalesforce' and @orderDir = 'desc' then TP.IsSalesforce end desc,			
				case when @orderBy = 'IsSalesforce' and @orderDir = 'asc' then TP.IsSalesforce end asc,
				case when @orderBy = 'Completed' and @orderDir = 'desc' then coalesce(nullif(TP.IsCompleteOverride,0), TP.IsComplete) end desc,
				case when @orderBy = 'Completed' and @orderDir = 'asc' then coalesce(nullif(TP.IsCompleteOverride,0), TP.IsComplete) end asc,
				case when @orderBy = 'IsActive' and @orderDir = 'asc' then TP.IsActive end asc,
				case when @orderBy = 'IsActive' and @orderDir = 'desc' then TP.IsActive end desc,
				case when @orderBy = 'InProgress' and @orderDir = 'asc' then TP.InProgressOrder end asc,
				case when @orderBy = 'InProgress' and @orderDir = 'desc' then TP.InProgressOrder end desc,
				case when 1 = 1 then StartDate end, TP.PurchasedFromBundleName, TP.[Order])  as r,
			TP.MessageBoardID,
			TP.Sku,
			TP.IsSalesforce,
			TP.ParentTrainingProgramID,
			TP.IsActive,
			TP.IsInDateRange,
            TP.TotalSeconds,
            TP.IsCompleteOverride,
            TP.DateCompleted,
            TP.IsSurveyComplete,
            TP.RequiredCompletedCount,
            TP.ElectiveCompletedCount,
            TP.FinalCompletedCount,
            TP.AssignmentsCompletedCount,
            TP.FinalExamScore,
            TP.IsRequiredComplete,
            TP.IsElectiveComplete,
            TP.IsFinalComplete,
            TP.IsAssignmentsComplete,
            TP.IsComplete,
            TP.PercentComplete,
            TP.ProfessionID,
            TP.IsAccreditationSurveyComplete,
            TP.SessionCourseID,
            TP.CategoryName
		from
			TP with (nolock) 
		where
			(@isUseDateRange = 0 and TP.IsActive = 1)
				or
			(@isUseDateRange = 1 and TP.IsInDateRange = 1)
		group by
			TP.ID, TP.CustomerID, TP.OwnerID, TP.Name, TP.InternalCode, TP.Cost, TP.[Description], 
			TP.IsNewHire, TP.IsLive, TP.StartDate, TP.EndDate, TP.DueDate, TP.EnforceRequiredOrder, 
			TP.MinimumElectives, TP.FinalAssessmentCourseID, TP.FinalAssessmentCourseTypeID, 
			TP.ModifiedBy, TP.CreatedBy, TP.ModifiedOn, TP.CreatedOn, TP.CourseCount, TP.MessageBoardID, TP.Sku, TP.PurchasedFromBundleName, TP.IsSalesforce, TP.SortName, TP.[Order], TP.ParentTrainingProgramID,
			TP.TrainingProgramID, TP.UserID, TP.IsCompleteOverride, TP.IsComplete, TP.IsActive, TP.IsInDateRange, TP.TotalSeconds,
			TP.DateCompleted,
            TP.IsSurveyComplete,
            TP.RequiredCompletedCount,
            TP.ElectiveCompletedCount,
            TP.FinalCompletedCount,
            TP.AssignmentsCompletedCount,
            TP.FinalExamScore,
            TP.IsRequiredComplete,
            TP.IsElectiveComplete,
            TP.IsFinalComplete,
            TP.IsAssignmentsComplete,
            TP.IsComplete,
            TP.PercentComplete,
            TP.ProfessionID,
            TP.IsAccreditationSurveyComplete,
            TP.SessionCourseID,
            TP.CategoryName,
            TP.InProgressOrder
	) as d
	where
		d.r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
	
	group by
		d.ID, d.CustomerID, d.OwnerID, d.Name, d.InternalCode, d.Cost, d.[Description], 
		d.IsNewHire, d.IsLive, d.StartDate, d.EndDate, d.DueDate, d.EnforceRequiredOrder, 
		d.MinimumElectives, d.FinalAssessmentCourseID, d.FinalAssessmentCourseTypeID, 
		d.ModifiedBy, d.CreatedBy, d.ModifiedOn, d.CreatedOn, d.CourseCount, d.MessageBoardID, d.Sku, d.r, d.ParentTrainingProgramID, d.IsActive,  d.TotalSeconds,
		d.IsCompleteOverride,
        d.DateCompleted,
        d.IsSurveyComplete,
        d.RequiredCompletedCount,
        d.ElectiveCompletedCount,
        d.FinalCompletedCount,
        d.AssignmentsCompletedCount,
        d.FinalExamScore,
        d.IsRequiredComplete,
        d.IsElectiveComplete,
        d.IsFinalComplete,
        d.IsAssignmentsComplete,
        d.IsComplete,
        d.PercentComplete,
        d.ProfessionID,
        d.IsAccreditationSurveyComplete,
        d.SessionCourseID,
        d.CategoryName
	order by d.r
	
end;");
        }
    }
}
