﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration636 : Migration
    {
        public override void Up()
        {
            AddColumn("Library", "Sku", DbType.String, 256, true);
        }
    }
}
