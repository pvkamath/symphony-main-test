﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration364 : Migration
    {
        public override void Up()
        {
            Execute(@"select 
                        s.ID as SectionID, 
                        o.Name as CourseName, 
                        a.CourseID, 
                        a.TrainingProgramID, 
                        a.Attempt, 
                        s.Name, 
                        max(a.[Date]) as [Date], 
                        a.UserID, 
                        count(distinct a.PageId) as NumQuestions,
                        pCount.totalQuestions as TotalQuestions,
                        c.NumCorrect,
                        cast(c.NumCorrect as float) / cast(pCount.totalQuestions as float) as Score,
                        case
							when c.MaxResponse > 1 then 1
							when c.MaxResponse <= 1 then 0
						end as IsMarked
                    from OnlineCourseAssignments a
                    join ArtisanPages p on p.ID = a.PageID
                    join ArtisanSectionPages sp on p.ID = sp.PageID
                    join ArtisanSections s on s.ID = sp.SectionID
                    join OnlineCourse o on a.CourseID = o.ID
                    join (
						select 
							artSP.SectionID, 
							count(distinct artSP.PageID) as totalQuestions
						from ArtisanSectionPages artSP
						join ArtisanPages artP on artP.ID = artSP.PageID and artP.QuestionType = 8
						group by artSP.SectionID
                    ) pCount on pCount.SectionID = s.ID
                    join (
						select 
							ia.UserID, 
							ia.CourseID, 
							ia.TrainingProgramID, 
							ia.Attempt,
							isp.SectionID, 
							sum(cast(ia.IsCorrect AS INT)) as NumCorrect,
							max(ia.ResponseStatus) as MaxResponse 
						from OnlineCourseAssignments ia
						join ArtisanSectionPages isp on isp.PageID = ia.PageID
						group by isp.SectionID, ia.Attempt, ia.CourseID, ia.TrainingProgramID, ia.UserID
					) c on c.UserID = a.UserID and 
						   c.CourseID = a.CourseID and 
						   c.TrainingProgramID = a.TrainingProgramID and
						   c.SectionID = s.ID and
						   c.Attempt = a.Attempt
                    group by s.ID, s.Name, a.Attempt, a.CourseID, o.Name, a.TrainingProgramID, a.UserID, c.NumCorrect, c.MaxResponse, pCount.totalQuestions
");
        }
    }
}