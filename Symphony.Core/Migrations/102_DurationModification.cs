﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration102 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER TABLE dbo.ClassDate DROP CONSTRAINT
	                    DF_ClassDate_Duration");

            Execute(@"UPDATE ClassDate SET Duration = Duration * 60;
                      ALTER TABLE ClassDate ALTER COLUMN Duration int;");

            Execute(@"ALTER TABLE dbo.Course ADD
	                    NumberOfDays int NULL,
	                    DailyDuration int NULL");
        }
    }
}