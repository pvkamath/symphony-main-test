﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration692 : Migration
    {
        public override void Up()
        {
            string[,] columnDefinitions = new string[,] {
                // Name              //ReportingTable             //ReportingIdColumn     //ReportingNameColumn               //SymphonyTable     //SymphonyIdColumn    //SymphonyNameColumn
                { "location",       "[ReportLocations]",        "[LocationKey]",       "[LocationLevelDetail]",                    "[Location]",       "[ID]",               "[Name]" },
                { "jobrole",        "[ReportJobRoles]",         "[JobRoleKey]",        "[JobRoleLevelDetail]",                     "[JobRole]",        "[ID]",               "[Name]" },
                { "audience",       "[ReportAudiences]",        "[AudienceKey]",       "[AudienceLevelDetail]",                    "[Audience]",       "[ID]",               "[Name]" },
                { "user",           "[ReportUsers]",            "[UserKey]",           "[LastName] + '', '' + [FirstName]",        "[User]",           "[ID]",               "[LastName] + '', '' + [FirstName]" },
                { "trainingprogram","[ReportTrainingPrograms]", "[TrainingProgramKey]","[Name]",                                   "[TrainingProgram]","[ID]",               "[Name]" },
                { "course",         "[ReportCourses]",          "[CourseKey]",         "[Name]",                                   "[Course]",         "[ID]",               "[Name]" },
                { "customer",       "[ReportCustomers]",        "[CustomerKey]",       "[Name]",                                   "[Customer]",       "[ID]",               "[Name]" }
            };

			Execute("declare @now datetime; set @now = getutcdate(); ");
			
            string query = "insert ReportEntityType (Name, ReportingTable, ReportingIdColumn, ReportingNameColumn, SymphonyTable, SymphonyIdColumn, SymphonyNameColumn, CreatedBy, ModifiedBy, CreatedOn, ModifiedOn) values ";
            string values = "";
            int columnLength = columnDefinitions.GetLength(0);

            for (int i = 0; i < columnLength; i++) {
                values = ((values.Length > 0 ? ", " : "") + string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9}, {10});",
                    columnDefinitions[i,0],
                    columnDefinitions[i,1],
                    columnDefinitions[i,2],
                    columnDefinitions[i,3],
                    columnDefinitions[i,4],
                    columnDefinitions[i,5],
                    columnDefinitions[i,6],
                    "admin",
                    "admin",
                    "getutcdate()",
                    "getutcdate()"));
				//throw new Exception(query + values);	
				Execute(query + values);
				
				values = "";
            }
            
            //query = query + values;

            //throw new Exception(query);

            //Execute(query);

        }
    }
}
