﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration407 : Migration
    {
        public override void Up()
        {
            Execute(@"EXECUTE sp_refreshview N'CustomerDetails';");
        }
    }
}