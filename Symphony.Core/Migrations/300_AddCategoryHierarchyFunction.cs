﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration300 : Migration
    {
        public override void Up()
        {
            Execute(@"CREATE FUNCTION [dbo].[fGetCategoryHierarchyBranch]
(   
    --the node that'll be considered the root node of the branch
    @iCategoryID int
)
RETURNS TABLE
AS
RETURN
(
WITH Category_CTE (ID, ParentCategoryID, Name, CustomerID, Level, LevelIndentText, LevelText, RootCategoryID)
AS
(
-- Anchor member definition
    SELECT
            c.ID,
            c.ParentCategoryID,
            c.Name,
            c.CustomerID,
            0 AS Level,
            CAST(c.Name as nvarchar(max)) as LevelIndentText,
            CAST(c.Name as nvarchar(max)) as LevelText,
            c.ID as RootCategoryID
        FROM
            Category c
      WHERE
            --Note by Jerod
            --Added a case in here...this way, passing 0 will get the full tree, while passing
            --the ID of any node will get that node's branch.
            --Note that this means if you have 1 root node in the tree, passing that node id or passing 0 will give the same result set
            1 = case when @iCategoryID = 0 then
                    case --allow 0 or nulls as root nodes
                        when ParentCategoryID IS NULL then 1
                        when ParentCategoryID = 0 then 1
                        else 0
                    end
                else
                    case
                        when ID = @iCategoryID then 1
                        else 0
                    end
                end
    UNION ALL
-- Recursive member definition
    SELECT
            c.ID,
            c.ParentCategoryID,
            c.Name,
            c.CustomerID,           
            Level + 1,
            CAST(LevelIndentText + ' > ' + c.Name as nvarchar(max)),
            --CAST(replicate('&nbsp;&nbsp;&nbsp;&nbsp;',Level+1) + c.Name as nvarchar(max)),
            CAST(c.Name as nvarchar(max)) as LevelText,
            RootCategoryID
    FROM
            Category c
    INNER JOIN Category_CTE cc ON
            cc.ID = c.ParentCategoryID
)
SELECT
      a.Level,
      a.LevelText,
      a.LevelIndentText,
      a.RootCategoryID,
      b.*
FROM
      Category_CTE a
JOIN
      Category b
ON
      a.ID = b.ID
)");
        }
    }
}