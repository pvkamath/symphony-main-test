﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration623 : Migration
    {
        public override void Up()
        {
            TableSchema.Table library = CreateTableWithKey("Library", "ID");
            library.AddColumn("CustomerID", DbType.Int32, 0, false);
            library.AddColumn("Name", DbType.String, 256, false);
            library.AddColumn("Cost", DbType.Double, 0, false);
            library.AddLongText("Details", true, "''");
            library.AddLongText("Description", true, "''");
            library.AddColumn("LibraryItemTypeID", DbType.Int32, 0, false);
            library.AddColumn("IsDeleted", DbType.Boolean, 0, false, "0");
            AddSubSonicStateColumns(library);

            TableSchema.Table libraryGrant = CreateTableWithKey("LibraryGrant", "ID");
            libraryGrant.AddColumn("HierarchyNodeID", DbType.Int32, 0, false);
            libraryGrant.AddColumn("HierarchyTypeID", DbType.Int32, 0, false);
            libraryGrant.AddColumn("LibraryID", DbType.Int32, 0, false);
            libraryGrant.AddColumn("StartDate", DbType.DateTime, 0, false);
            libraryGrant.AddColumn("EndDate", DbType.DateTime, 0, false);
            AddSubSonicStateColumns(libraryGrant);

            TableSchema.Table libraryItem = CreateTableWithKey("LibraryItem", "ID");
            libraryItem.AddColumn("ItemID", DbType.Int32, 0, false);
            libraryItem.AddColumn("LibraryID", DbType.Int32, 0, false);
            libraryItem.AddColumn("LibraryItemTypeID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(libraryItem);

            TableSchema.Table libraryItemType = CreateTableWithKey("LibraryItemType", "ID");
            libraryItemType.AddColumn("Name", DbType.String, 64, false);
            AddSubSonicStateColumns(libraryItemType);

            TableSchema.Table libraryFavorite = CreateTableWithKey("LibraryFavorite", "ID");
            libraryFavorite.AddColumn("LibraryItemID", DbType.Int32, 0, false);
            libraryFavorite.AddColumn("UserID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(libraryFavorite);


        }
    }
}
