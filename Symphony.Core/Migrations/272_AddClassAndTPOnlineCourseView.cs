﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration272 : Migration
    {
        /// <summary>
        /// Combines classroom courses with classroom courses AND online courses that are linked to a trainging program
        /// This is to be filtered later by classes and training programs the user instructs
        /// Note that if the user is an instructor for a class, AND the user is a leader for a training program that contains
        /// the class, then after you filter, this class will appear twice, once as a regular class from the first select
        /// and once joined with the training program in the second select
        /// This allows the Instructs tab to have a seperate section to list classes the current user actually is an instructor
        /// for.
        /// </summary>
        public override void Up()
        {

            Execute(@"CREATE VIEW [dbo].[ClassesAndTPOnlineCourses]
AS
SELECT 
        c.[ID],
        c.[CustomerID],
        c.[LockedIndicator],
        c.[Name],
        c.[IsVirtual],
        cd.[MinStartDateTime] as MinClassDate,
        1 as CourseTypeID,
        0 as TrainingProgramID,
        null as TrainingProgramName
    FROM [dbo].[Class] c
    LEFT JOIN [ClassDateMinMax] cd ON cd.[ClassID] = c.[ID]
    UNION
    SELECT
        CASE WHEN tpl.[CourseTypeID] = 2 THEN o.[ID] ELSE cl.[ID] END,
        tpl.[CustomerID],
        CASE WHEN tpl.[CourseTypeID] = 2 THEN CASE WHEN o.[Active] = 1 THEN 0 ELSE 1 END ELSE cl.[LockedIndicator] END,
        CASE WHEN tpl.[CourseTypeID] = 2 THEN o.[Name] ELSE cl.[Name] END,
        CAST(CASE WHEN tpl.[CourseTypeID] = 2 THEN 1 ELSE 2 END as bit),
        CASE WHEN tpl.[CourseTypeID] = 2 THEN tpl.[ActiveAfterDate] ELSE cd.[MinStartDateTime] END,
        tpl.[CourseTypeID],
        tpl.[TrainingProgramID],
        tp.[Name] as TrainingProgramName
    FROM [dbo].[TrainingProgramToCourseLink] tpl
    LEFT JOIN [OnlineCourse] o ON o.ID = tpl.[CourseID] AND tpl.[CourseTypeID] = 2
    LEFT JOIN [Class] cl ON cl.[CourseID] = tpl.[CourseID] AND tpl.[CourseTypeID] = 1
    LEFT JOIN [ClassDateMinMax] cd ON cd.[ClassID] = cl.[ID]
    INNER JOIN [dbo].[TrainingProgram] tp ON tp.[ID] = tpl.[TrainingProgramID]");

        }
    }
}