﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration086 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "TestType", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanSections", "PassingScore", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanSections", "TestOut", DbType.Boolean, 0, false, "0");
        }
    }
}