﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration810 : Migration
    {
        public override void Up()
        {
            Execute(@"
    delete from AssignmentSummaryQuestions;
    delete from AssignmentSummaryAttempts;
    delete from AssignmentSummary;
");
        }
    }
}