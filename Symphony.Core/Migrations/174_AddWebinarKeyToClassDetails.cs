﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration174 : Migration
    {
        public override void Up()
        {
            Execute(@"alter view [dbo].[ClassroomClassDetails] as
select
	ROW_NUMBER() OVER (order by cClass.ID) as ID, 
	cCourse.ID as CourseID, 
	cClass.ID as ClassID, 
	cClass.[Name] as ClassName, 
	cCourse.[Name] as CourseName, 
	cVenue.[Name] as VenueName, 
	cClass.Description as ClassDescription, 
	cRoom.[Name] as ClassRoomName, 
	cClass.WebinarKey, 
	vClassDMM.MinStartDateTime as MinClassDate, 	
	isnull(replace(replace((
		select     
			cUser.FirstName + '+' + cUser.LastName as [data()]
		from         
			dbo.ClassInstructors as cClassI 
		inner join
            dbo.[User] as cUser 
		on 
			cUser.ID = cClassI.InstructorID
        where     
			cClassI.ClassID = cClass.ID 
		for xml path('')), ' ', ', '), '+', ' '), '')
	as ClassInstructors, 
	isnull(replace((
		select     
			cClassD.StartDateTime as [data()]
        from         
			dbo.ClassDate as cClassD
        where     
			(cClassD.ClassID = cClass.ID)
        order by 
			StartDateTime ASC 
		for xml path('')), ' ', ', '), '') 
	as ClassDates
	from
		dbo.Class as cClass 
	inner join
		dbo.Course as cCourse 
	on 
		cCourse.ID = cClass.CourseID 
	left join
		dbo.Room as cRoom 
	on 
		cRoom.ID = cClass.RoomID 
	left join
		dbo.Venue as cVenue 
	on 
		cVenue.ID = cRoom.VenueID 
	inner join
		dbo.ClassDate as cClassD 
	on 
		cClassD.ClassID = cClass.ID 
	inner join
		dbo.ClassDateMinMax as vClassDMM 
	on 
		vClassDMM.ClassID = cClass.ID 
		and 
		vClassDMM.MinStartDateTime = cClassD.StartDateTime");
        }
    }
}