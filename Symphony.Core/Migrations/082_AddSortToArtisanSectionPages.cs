﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration082 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSectionPages", "Sort", DbType.Int32, 0, false, "0");
        }
    }
}