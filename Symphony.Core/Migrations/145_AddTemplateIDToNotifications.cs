﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration145 : Migration
    {
        public override void Up()
        {
            Execute("alter table Notifications add TemplateID int not null default 0");
        }
    }
}