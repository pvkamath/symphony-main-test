﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration341 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgram", "ExternalID", DbType.String, 32, true);
            AddColumn("Course", "ExternalID", DbType.String, 32, true);
            AddColumn("OnlineCourse", "ExternalID", DbType.String, 32, true);
            AddColumn("Class", "ExternalID", DbType.String, 32, true);
        }
    }
}