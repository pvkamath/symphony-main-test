﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration212 : Migration
    {

        public override void Up()
        {
            Execute(@" ALTER PROCEDURE [dbo].[GetUserTilesForApp] (@userId int, @applicationId int)
AS
BEGIN
	WITH Assignments as (SELECT distinct
		htl.TileID
		FROM HierarchyToTileLinks htl 
		JOIN (
			select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
			union
			select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
			union
			select f.ID, f.TypeID from useraudience 
				cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
			where userid = @userid
			union
			select @userid, 4
		) as H on h.id = htl.HierarchyNodeID and h.TypeID = htl.HierarchyTypeID
	),
		UserAppCred as (
			select app.ID
				 , app.Name
				 , app.AuthenticationUri
				 , app.PassUserNamePass
				 , cred.ID as CredentialsId
				 , case 
						when app.PassUserNamePass = 0 then 'none'
						when app.PassUserNamePass = 1 and cred.ID is null then 'missing'
						else 'complete'
					end as CredentialState
				 , cred.UserName as CredentialsUserName
				 , cred.Password as CredentialsPassword
				 , @userId as UserID
				from Applications as app
					left join (select Credentials.ID
				 , Credentials.ApplicationID
				 , Credentials.UserName
				 , Credentials.Password
				 , @userId as userId
				from Credentials
				right join [User] as usr on usr.ID = Credentials.UserID
			where usr.ID = @userId) as cred on app.ID = cred.ApplicationID
		)

	select Tiles.ID 
		 , assign.TileID
		 , Tiles.Name
		 , Tiles.TileUri
		 , app.ID as ApplicationId
		 , app.Name as ApplicationName
		 , app.PassUserNamePass
		 , app.CredentialState
		 , app.CredentialsId
		 , app.CredentialsUserName
		 , app.CredentialsPassword
		 , @userId as UserID
	from Tiles
		join UserAppCred as app
			on app.ID = tiles.ApplicationID
		left join Assignments as assign on assign.TileID = Tiles.ID
	where assign.TileID is not null 
		and (
				0 = @applicationID
			or	Tiles.ApplicationID = @applicationID
		)	
END");
        }

        public override void Down()
        {
            Execute(@"ALTER PROCEDURE [dbo].[GetUserTilesForApp] (@userId int, @applicationId int)
AS
BEGIN
	WITH Assignments as (
		SELECT tl.ID
			 , tl.Name as Name
			 , tl.TileUri
			 , ap.ID as ApplicationId
			 , ap.Name as ApplicationName
			 , ap.PassUserNamePass 
			 , htl.HierarchyNodeID
			 , htl.HierarchyTypeID
			 , tl.CreatedBy
			 , tl.CreatedOn
			 , tl.ModifiedBy
			 , tl.ModifiedOn
			 , ROW_NUMBER() over (order by tl.applicationId) as MyRow
		  FROM Tiles as tl
		  	JOIN Applications as ap on ap.ID = tl.ApplicationID
		  	JOIN HierarchyToTileLinks htl on tl.id = htl.TileID
			JOIN (
				select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
				union
				select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
				union
				select f.ID, f.TypeID from useraudience 
					cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
				where userid = @userid
				union
				select @userid, 4
			) as H on h.id = htl.HierarchyNodeID and h.TypeID = htl.HierarchyTypeID
	)
	select distinct
			a.ID
			, a.Name
			, a.TileUri
			, a.ApplicationId
			, a.ApplicationName
			, a.PassUserNamePass
			, case 
				when a.PassUserNamePass = 0 then 'none'
				when a.PassUserNamePass = 1 and cr.ID is null then 'missing'
				else 'complete'
			end as CredentialState
			, cr.ID as CredentialID
			, cr.UserName as CredentialUserName
			, case when cr.Password is null 
				then null
				else '*****'
				end as CredentialPassword
			, @userId as UserID
			, cast(case when (select count(*) from Assignments as au where au.id = a.id and au.HierarchyTypeID = 1) > 0 then 1 else 0 end as bit) as IsAssignedByLocation
			, cast(case when (select count(*) from Assignments as au where au.id = a.id and au.HierarchyTypeID = 2) > 0 then 1 else 0 end as bit) as IsAssignedByJobRole
			, cast(case when (select count(*) from Assignments as au where au.id = a.id and au.HierarchyTypeID = 3) > 0 then 1 else 0 end as bit) as IsAssignedByAudience
			, cast(case when (select count(*) from Assignments as au where au.id = a.id and au.HierarchyTypeID = 4) > 0 then 1 else 0 end as bit) as IsAssignedByUser

	from Assignments as a
		left join Credentials as cr on cr.ApplicationID = a.ApplicationId
	where 
		(
			cr.UserID = @userId 
			or cr.UserID is NULL
		)
	and a.applicationId = @applicationId");
        }
    }
}