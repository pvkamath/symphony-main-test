﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration482 : Migration
    {

        public override void Up()
        {
            Execute(@"
    insert into ExternalSystem (SystemName, SystemCodeName, LoginUrl)
    values ('ProSchools', 'proschools', 'https://www.proschools.com/customer/account/login/'),
           ('TrainingPro', 'trainingpro', 'https://www.trainingpro.com/'),
           ('Career WebSchool', 'careerwebschool', 'https://www.ordercourses.com/computaught/showcase/viewStudentLogin.do')
");
        }

    }
}