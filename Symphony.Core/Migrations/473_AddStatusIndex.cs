﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;
using Symphony.Core;

namespace Symphony.Core.Migrations
{
    public class Migration473 : Migration
    {
        public override void Up()
        {
            Execute(@"CREATE NONCLUSTERED INDEX [status_index] ON [dbo].[Notifications] 
(
	[Status] ASC
) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
        }

    }
}