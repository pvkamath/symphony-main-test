﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration138 : Migration
    {
        public override void Up()
        {
            Execute(@"UPDATE ArtisanThemes 
                      SET CssPath='/skins/artisan_content/default/css/theme.css',
	                      Folder='/skins/artisan_content/default',
	                      SkinName='be_default' 
                      WHERE Name='BE Default'
                      
                      UPDATE ArtisanThemes 
                      SET CssPath='/skins/artisan_content/default/css/theme.css',
	                      Folder='/skins/artisan_content/default',
	                      SkinName='be_smooth' 
                      WHERE Name='BE Smooth'
                      
                      UPDATE ArtisanThemes 
                      SET CssPath='/skins/artisan_content/default/css/theme.css',
	                      Folder='/skins/artisan_content/default',
	                      SkinName='aslan' 
                      WHERE Name='Aslan'");
        }
    }
}