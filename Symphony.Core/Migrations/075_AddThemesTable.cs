﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration075 : Migration
    {
        public override void Up()
        {
            TableSchema.Table table = CreateTableWithKey("ArtisanThemes", "ID");
            table.AddColumn("CustomerID", DbType.Int32, 0, false, "0");
            table.AddColumn("Name", DbType.String, 64, false);
            table.AddColumn("Description", DbType.String, 64, false);
            table.AddColumn("CssPath", DbType.String, 128, false);
            AddSubSonicStateColumns(table);
        }
    }
}