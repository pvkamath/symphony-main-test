﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration864 : Migration
    {

        public override void Up()
        {
            Execute(@"
IF (NOT EXISTS(SELECT * FROM Templates WHERE CodeName = 'SingleAssignmentGradedFailed'))
BEGIN
    INSERT INTO dbo.Templates (
        Subject, 
        Body, 
        CodeName, 
        DisplayName, 
        Description, 
        Priority, 
        CustomerID, 
        Enabled, 
        ModifiedBy, 
        CreatedBy, 
        ModifiedOn, 
        CreatedOn, 
        Area, 
        IsScheduled, 
        ScheduleParameterID, 
        RelativeScheduledMinutes, 
        FilterComplete, 
        OwnerID, 
        CcClassroomSupervisors, 
        CcReportingSupervisors, 
        CreatedByUserId, 
        ModifiedByUserId, 
        CreatedByActualUserId, 
        ModifiedByActualUserId, 
        IncludeCalendarInvite,
        Percentage,
        IsConfigurableForTrainingProgram,
        IsUserCreated)
    SELECT TOP 1 
        'You have failed {!Assignment.Name}, please try again.', 
        'Hi {!User.Firstname},<br/><br/>Unfortunately your instructor for the course ""{!OnlineCourse.Name}"" in the training program ""{!TrainingProgram.Name}"" given you a failing grade on your assignment. You scored {!Assignment.Percent} ({!Assignment.NumCorrect} out of {!Assignment.NumQuestions}). Please log in to Symphony to review your assignment and the instructor feedback, then attempt your assignment again.', 
        'SingleAssignmentGradedFailed', 
        'Single Assignment Graded - Failed', 
        'Sent each time an instructor assigns a failing score to an assignment.', 
        1, 
        0, 
        0, 
        'admin', 
        'admin', 
        GETUTCDATE(), 
        GETUTCDATE(), 
        '', 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        null, 
        null, 
        0,
        null,
        1,
        0
     FROM dbo.Templates

    INSERT INTO dbo.Templates (Subject, Body, CodeName, DisplayName, Description, Priority, CustomerID, Enabled, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, RelativeScheduledMinutes, FilterComplete, OwnerID, CcClassroomSupervisors, CcReportingSupervisors, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, IncludeCalendarInvite, Percentage, IsConfigurableForTrainingProgram, IsUserCreated)
    SELECT
	    tp.Subject,
	    tp.Body,
	    tp.CodeName,
	    tp.DisplayName,
	    tp.Description,
	    tp.Priority,
	    c.Id,
	    tp.Enabled,
	    tp.ModifiedBy,
	    tp.CreatedBy,
	    tp.ModifiedOn,
	    tp.CreatedOn,
	    tp.Area,
	    tp.IsScheduled,
	    tp.ScheduleParameterID,
	    tp.RelativeScheduledMinutes,
	    tp.FilterComplete,
	    tp.OwnerID,
	    tp.CcClassroomSupervisors,
	    tp.CcReportingSupervisors,
	    tp.CreatedByUserId,
	    tp.ModifiedByUserId,
	    tp.CreatedByActualUserId,
	    tp.ModifiedByActualUserId,
	    tp.IncludeCalendarInvite,
        tp.Percentage,
        tp.IsConfigurableForTrainingProgram,
        tp.IsUserCreated
    FROM
	    Customer c
	    INNER JOIN dbo.Templates tp ON tp.CustomerID = 0 AND tp.CodeName = 'SingleAssignmentGradedFailed'
END
        ");
        }

    }
}
