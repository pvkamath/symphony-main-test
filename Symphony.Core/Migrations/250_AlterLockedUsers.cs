﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration250 : Migration
    {
        public override void Up()
        {
            Execute(@"
                    ALTER view [dbo].[LockedUsers] as
                    select
	                    [user].*, coalesce(LockedCourseCount, 0) as LockedCourseCount
                    from
	                    (
	                    select
		                    u.id, sum(case when ocr.testattemptcount >= (oc.retries + 1) then 1 else 0 end) as LockedCourseCount
	                    from
		                    [user] u
	                    join
		                    onlinecourserollup ocr
	                    on
		                    ocr.userid = u.id
	                    join
		                    onlinecourse oc
	                    on
		                    oc.id = ocr.courseid
	                    left join
		                    onlineTraining.dbo.ScormRegistration sr
	                    on
		                    sr.scorm_registration_id = ocr.originalregistrationid and sr.course_id = ocr.courseid
	                    group by
		                    u.id
	                    ) as userids
                    right join
	                    [user]
                    on
	                    [user].id = userids.id");
        }
    }
}