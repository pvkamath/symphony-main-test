﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration641 : Migration
    {
        public override void Up()
        {
            Execute(@"

ALTER TABLE dbo.License
ADD ExpirationRuleId INT NULL

ALTER TABLE dbo.License
ADD ExpirationRuleAfterDays INT NULL


");
        }
    }
}