﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration153 : Migration
    {
        public override void Up()
        {
            Execute("alter table Notifications add DeletedFromHistory bit not null default 0");
        }
    }
}