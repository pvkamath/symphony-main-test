﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration012 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(641);
            sb.AppendFormat(@"CREATE TABLE [dbo].[ImportJobQueue]({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[Id] [int] IDENTITY(1,1) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[JobTypeCode] [int] NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[Dsc] [varchar](20) NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[StartDate] [datetime] NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[EndDate] [datetime] NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[StatusDate] [datetime] NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[StatusCode] [int] NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[StatusUpdateCount] [int] NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[StatusMsg] [varchar](50) NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[ErrorMsg] [varchar](50) NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[WorkingDir] [varchar](200) NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[CustomerID] [int] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@" CONSTRAINT [PK_ImportJobQueue] PRIMARY KEY CLUSTERED {0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[Id] ASC{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ");
            sb.AppendFormat(@"ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]{0}", Environment.NewLine);
            sb.AppendFormat(@") ON [PRIMARY]");


            Execute(sb.ToString());
        }
    }
}