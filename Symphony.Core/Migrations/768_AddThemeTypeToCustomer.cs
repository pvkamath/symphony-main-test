﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration768 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "ThemeTypeID", DbType.Int32, 0, false, "1");
        }
    }
}