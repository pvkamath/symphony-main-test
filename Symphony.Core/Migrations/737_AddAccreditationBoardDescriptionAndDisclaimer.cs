﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration737 : Migration
    {
        public override void Up()
        {
            AddColumn("AccreditationBoard", "Description", DbType.String, 32768, true);
            AddColumn("AccreditationBoard", "Disclaimer", DbType.String, 32768, true);
        }
    }
}