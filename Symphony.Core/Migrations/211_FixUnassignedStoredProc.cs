﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration211 : Migration
    {

        public override void Up()
        {
            Execute(@"ALTER PROCEDURE [dbo].[GetUserUnassignedTiles] (@userId int)
AS
BEGIN
with Assignments as (SELECT distinct
	htl.TileID
	FROM HierarchyToTileLinks htl 
	JOIN (
		select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
		union
		select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
		union
		select f.ID, f.TypeID from useraudience 
			cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
		where userid = @userid
		union
		select @userid, 4
	) as H on h.id = htl.HierarchyNodeID and h.TypeID = htl.HierarchyTypeID
),
	UserAppCred as (
		select app.ID
			 , app.Name
			 , app.AuthenticationUri
			 , app.PassUserNamePass
			 , cred.ID as CredentialsId
			 , case 
					when app.PassUserNamePass = 0 then 'none'
					when app.PassUserNamePass = 1 and cred.ID is null then 'missing'
					else 'complete'
				end as CredentialState
			 , cred.UserName as CredentialsUserName
			 , cred.Password as CredentialsPassword
			 , @userId as UserID
			from Applications as app
				left join (select Credentials.ID
			 , Credentials.ApplicationID
			 , Credentials.UserName
			 , Credentials.Password
			 , @userId as userId
			from Credentials
			right join [User] as usr on usr.ID = Credentials.UserID
		where usr.ID = @userId) as cred on app.ID = cred.ApplicationID
	)

select Tiles.ID 
	 , Tiles.Name
	 , Tiles.TileUri
	 , app.ID as ApplicationId
	 , app.Name as ApplicationName
	 , app.PassUserNamePass
	 , app.CredentialState
	 , app.CredentialsId
	 , app.CredentialsUserName
	 , app.CredentialsPassword
	 , @userId as UserID
from Tiles
	join UserAppCred as app
		on app.ID = tiles.ApplicationID
	left join Assignments as assign on assign.TileID = Tiles.ID
where assign.TileId is null
END;");
        }

        public override void Down()
        {
            Execute(@"ALTER PROCEDURE [dbo].[GetUserUnassignedTiles] (@userId int)
AS
BEGIN
	WITH Assignments as (
		SELECT htl.TileID as ID
			FROM HierarchyToTileLinks htl 
			JOIN (
				select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
				union
				select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
				union
				select f.ID, f.TypeID from useraudience 
					cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
				where userid = @userid
				union
				select @userid, 4
			) as H on h.id = htl.HierarchyNodeID and h.TypeID = htl.HierarchyTypeID
	)
	select distinct
			  tl.ID
			, tl.Name
			, tl.TileUri
			, ap.ID as ApplicationId
			, ap.Name as ApplicationName
			, ap.PassUserNamePass
			, case 
				when ap.PassUserNamePass = 0 then 'none'
				when ap.PassUserNamePass = 1 and cr.ID is null then 'missing'
				else 'complete'
			end as CredentialState
			, cr.ID as CredentialID
			, cr.UserName as CredentialUserName
			, case when cr.Password is null 
				then null
				else '*****'
				end as CredentialPassword
			, @userId as UserID
	from Tiles as tl
		JOIN Applications as ap on ap.ID = tl.ApplicationID
		LEFT JOIN Credentials as cr on cr.ApplicationID = ap.ID
	where 
		(
			    cr.UserID = @userId 
			 or cr.UserID is NULL
		)
		and tl.ID not in (select id from Assignments
		union
		select -1
		)
END;
");
        }
    }
}