﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration848 : Migration
    {
       
        public override void Up()
        {
            AddColumn("ArtisanCourses", "BookmarkingMethod", DbType.Int32, 0, false, "1");
            AddColumn("OnlineCourse", "BookmarkingMethodOverride", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "BookmarkingMethodOverride", DbType.Int32, 0, true);
            AddColumn("Customer", "BookmarkingMethodOverride", DbType.Int32, 0, true);
        }
    }
}
