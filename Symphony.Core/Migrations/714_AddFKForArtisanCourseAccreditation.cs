﻿using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration714 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE ArtisanCourseAccreditation"
                + " ADD CONSTRAINT fk_AccreditationBoard_ID_ArtisanCourseAccreditation_AccreditiationBoardID FOREIGN KEY(AccreditationBoardID)"
                    + " REFERENCES AccreditationBoard(ID)"
                );

            Execute("ALTER TABLE ArtisanCourseAccreditation"
                + " ADD CONSTRAINT fk_ArtisanCourses_ID_ArtisanCourseAccreditation_ArtisanCourseID FOREIGN KEY(ArtisanCourseID)"
                    + " REFERENCES ArtisanCourses(ID)"
                );
        }
    }
}
