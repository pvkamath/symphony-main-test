﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration603 : Migration
    {

        public override void Up()
        {
            Execute(@"
                ALTER procedure [dbo].[GetTrainingProgramsForUsers](@userIdsCommaSeparated nvarchar(max))
as
begin
	declare @today datetime
	declare @userIds table (id int)
	
	-- Convert the comma separted userids to a table
	insert into @userIds
	select CAST(LTRIM(RTRIM(item)) as int)
	from fSplit(@userIdsCommaSeparated,',');
			
	set @today = (select cast(floor(cast(getdate() as decimal(12, 5))) as datetime));
	select 
		distinct
		allTps.TrainingProgramID as ID,
		allTps.Name
	from (
		select
			utps.UserID,
			utps.NewHireIndicator,
			utps.NewHireEndDate,
			utps.HireDate,
			utps.TrainingProgramID,
			utps.NewHireOffsetEnabled,
			utps.NewHireStartDateOffset,
			utps.NewHireEndDateOffset,
			utps.NewHireDueDateOffset,
			utps.TpNewHireTransitionPeriod,
			utps.NewHireDuration,
			utps.NewHireTransitionPeriod,
			utps.Name,
			case when utps.NewHireIndicator = 1 
				then dateadd(d, 
						case when utps.NewHireOffsetEnabled = 1 
							then utps.NewHireStartDateOffset 
							else 0
						end, utps.HireDate) 
				else utps.StartDate end as StartDate,
			case when utps.NewHireIndicator = 1 
				then dateadd(d, 
						case when utps.NewHireOffsetEnabled = 1 
							then utps.NewHireDueDateOffset 
							else utps.NewHireDuration
						end, utps.HireDate) 
				else utps.DueDate end as DueDate,
			case when utps.NewHireIndicator = 1 
				then dateadd(d, 
						case when utps.NewHireOffsetEnabled = 1 
							then utps.NewHireEndDateOffset 
							else utps.NewHireDuration
						end, utps.HireDate) 
				else utps.EndDate end as EndDate
		from (
			select 
				distinct
				u.ID as UserID,
				u.NewHireIndicator,
				u.NewHireEndDate,
				u.HireDate,
				tp.ID as TrainingProgramID,
				tp.Name,
				tp.StartDate,
				tp.EndDate,
				tp.DueDate,
				tp.NewHireOffsetEnabled,
				tp.NewHireStartDateOffset,
				tp.NewHireEndDateOffset,
				tp.NewHireDueDateOffset,
				tp.NewHireTransitionPeriod as TpNewHireTransitionPeriod,
				c.NewHireDuration,
				c.NewHireTransitionPeriod
			from [User] u
			left join UserAudience ua on ua.UserID = u.ID
			cross apply fGetHierarchyParentsWithDefault(u.LocationID, 1) as l
			cross apply fGetHierarchyParentsWithDefault(u.JobRoleID, 2) as j
			cross apply fGetHierarchyParentsWithDefault(ua.AudienceID, 3) as a
			left join HierarchyToTrainingProgramLink hl on
				(l.ID = hl.HierarchyNodeID and hl.HierarchyTypeID = 1) or
				(j.ID = hl.HierarchyNodeID and hl.HierarchyTypeID = 2) or
				(a.ID = hl.HierarchyNodeID and hl.HierarchyTypeID = 3) or
				(u.ID = hl.HierarchyNodeID and hl.HierarchyTypeID = 4)
			join TrainingProgram tp on tp.ID = hl.TrainingProgramID
			join Customer c on c.ID = u.CustomerID
			where u.ID in (select id from @userIds) and tp.SessionCourseID > 0
		) utps
	) allTps
	where
		(
			allTps.EndDate >= @today
		and
			allTps.EndDate >= case when allTps.NewHireIndicator = 0 then dateadd(d, coalesce(allTps.TpNewHireTransitionPeriod, allTps.NewHireTransitionPeriod, 0), coalesce(allTps.NewHireEndDate, allTps.HireDate)) else @today end
		)	
end
");
        }

    }
}
