﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration644 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE FUNCTION [dbo].[fGetHierarchyParentsForUser]
(   
    @userId int
)
RETURNS @tbl TABLE (ID int not null, TypeID int not null)
AS
begin
	insert @tbl
	select 
			ID, TypeID 
		from fGetHierarchyParents(
			(
				select LocationID 
				from [User] u 
				where u.ID = @userID
			), 1)
	union
		select 
			ID, TypeID 
		from fGetHierarchyParents(
			(
				select JobRoleID 
				from [User] u 
				where u.ID = @userID
			), 2)
	union
		select 
			f.ID, f.TypeID 
		from UserAudience a 
		cross apply dbo.[fGetHierarchyParents](a.AudienceID, 3) as f
		where a.UserID = @userid
	union
		select @userID, 4
	return
end	


");
        }
    }
}