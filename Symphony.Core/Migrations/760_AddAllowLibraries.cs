﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration760 : Migration
    {
        public override void Up()
        {
            AddColumn("CustomerNetworkDetail", "AllowLibraries", DbType.Boolean, 0, true, "0");
        }

    }
}