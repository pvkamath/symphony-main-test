﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration307 : Migration
    {
        public override void Up()
        {
            AddColumn("User", "DOB", DbType.String, 10, true);
            Execute(@"ALTER TABLE [User] ADD SSN Binary(20) NULL");
            AddColumn("OnlineCourse", "IsValidationEnabled", DbType.Boolean, 0, true);
            AddColumn("OnlineCourse", "ValidationInterval", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "IsValidationEnabled", DbType.Boolean, 0, true);
            AddColumn("TrainingProgram", "ValidationInterval", DbType.Int32, 0, true);
        }
    }
}