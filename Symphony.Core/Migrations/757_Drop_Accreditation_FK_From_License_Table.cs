﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration757 : Migration
    {

        public override void Up()
        {
            Execute(@"
                ALTER TABLE [dbo].[License] DROP CONSTRAINT [fk_AccreditationBoard_ID_License_AccreditiationBoardID]
            ");
        }
    }
}
