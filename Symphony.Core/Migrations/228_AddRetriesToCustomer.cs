﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration228 : Migration
    {

        public override void Up()
        {
            AddColumn("Customer", "OnlineCourseRetries", DbType.Int32, 0, true);
        }

        public override void Down()
        {
            
        }
    }
}