﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration316: Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER TABLE [User] 
ALTER COLUMN SSN Binary(32) NULL;
ALTER TABLE [User]
DROP COLUMN DOB;
ALTER TABLE [User] ADD DOB Binary(32) NULL;
");
        }
    }
}