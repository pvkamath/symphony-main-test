﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration832 : Migration
    {
        /// <summary>
        /// Get the parent training programs for the specified customers
        /// with session start/end dates within the given range
        /// Used for notifications
        /// 
        /// Simplifies from migration 831
        /// </summary>
        public override void Up()
        {
            Execute(@"
                alter procedure [dbo].[GetParentTrainingProgramsWithSessions] 
                (
                    -- Returns the parent training program with session information
                    -- for all training programs within the specified customer
                    -- the parent training program may be in another customer
                    -- Used for the session start/end notifications and returns
                    -- training programs with sessions that match a window of time
                    -- to avoid returning all training programs for the customer

                    @customerId int, -- Will return the parent training program even if the parent training program is not in the current customer
                    @minReferenceDate datetime, -- To apply to the session start/end time to avoid returning all past sessions
                    @maxReferenceDate datetime, -- To apply to the session start/end time to avoid returning all past sessions
                    @isUseStartDate bit -- indicataor to determine whether the date range is applied to start time or end time
                )
                AS
                BEGIN
	                select 
	                    tp.ID,
	                    tp.CustomerID,
	                    tp.Name,
	                    tp.Cost,
	                    tp.[Description],
	                    tp.IsNewHire,
	                    tp.IsLive,
	                    tp.StartDate,
	                    tp.EndDate,
	                    tp.DueDate,
	                    tp.EnforceRequiredOrder,
	                    tp.MinimumElectives,
	                    tp.ModifiedOn,
	                    tp.CreatedOn,
	                    tp.EnforceCanMoveForwardIndicator,
	                    tp.Sku,
	                    tp.ParentTrainingProgramID,
	                    tp.IsCustomNotificationsEnabled,
	                    tp.SessionCourseID,
	                    minStart.MinStartDateTime as 'MinSessionStartDate',
	                    maxStart.MaxStartDateTime as 'MaxSessionStartDate',
	                    cl.ID as 'SessionClassID',
	                    cl.Name as 'SessionClassName',
	                    c.Name as 'SessionCourseName',
	                    c.DailyDuration as 'SessionDailyDuration',
	                    c.NumberOfDays as 'SessionNumberOfDays'
                    from (
	                    select 
		                    distinct coalesce(ptp.ID, tp.ID) as 'ID'
	                    from 
		                    TrainingProgram tp
	                    left join
		                    TrainingProgram ptp
		                    on
		                    tp.ParentTrainingProgramID > 0 and ptp.ID = tp.ParentTrainingProgramID
	                    where 
		                    tp.CustomerID = @customerId
                    ) as ParentIds
                    join
	                    TrainingProgram tp
	                    on 
	                    tp.ID = ParentIds.ID
                    join
	                    Course c on c.ID = tp.SessionCourseID
                    join
                        Class cl on cl.CourseID = tp.SessionCourseID
                    join 
                        ClassMinStartDateTime minStart on minStart.ClassID = cl.ID
                    join 
                        ClassMaxStartDateTime maxStart on maxStart.ClassID = cl.ID
                    where tp.CustomerID = @customerId
                    and
                    (
	                    (
		                    @isUseStartDate = 1 and 
		                    minStart.MinStartDateTime > @minReferenceDate and 
		                    minStart.MinStartDateTime < @maxReferenceDate
	                    ) 
	                    or
	                    (
		                    @isUseStartDate = 0 and 
		                    dateadd(minute, c.DailyDuration, maxStart.MaxStartDateTime) > @minReferenceDate and 
		                    dateadd(minute, c.DailyDuration, maxStart.MaxStartDateTime) < @maxReferenceDate
	                    )
                    )
                END
");
        }
    }
}
