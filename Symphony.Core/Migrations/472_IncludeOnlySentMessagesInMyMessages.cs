﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;
using Symphony.Core;

namespace Symphony.Core.Migrations
{
    public class Migration472 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER VIEW [dbo].[MyMessages] AS
                    SELECT
	                    n.ID, n.RecipientID, n.SenderID, n.Subject, n.Body, n.IsRead, n.AttachmentID, n.Priority, n.CreatedOn, n.ModifiedOn,
	                    n.Subject as SearchText,
	                    CASE 
		                    WHEN u.ID IS NOT NULL THEN u.FirstName + ' ' + u.LastName
		                    ELSE '(No reply)'
	                    END AS Sender
                    FROM
	                    Notifications as n
                    LEFT JOIN
	                    dbo.[User] u
                    ON
	                    u.ID = n.SenderID
                    WHERE
	                    n.IsDeleted = 0
	                  and n.[Status] = 2;");
        }

    }
}