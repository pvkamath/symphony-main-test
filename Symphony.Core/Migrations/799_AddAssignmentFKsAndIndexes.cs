﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration799 : Migration
    {
        public override void Up()
        {
            Execute(@"
                ALTER TABLE AssignmentSummary
                    ADD CONSTRAINT fk_AssignmentSummaryAritsanCourse_ArtisanCoursesID FOREIGN KEY(ArtisanCourseID)
                    REFERENCES ArtisanCourses(ID)");

            Execute(@"
                ALTER TABLE AssignmentSummary
                    ADD CONSTRAINT fk_AssignmentSummaryArtisanSectionID_ArtisanSectionsID FOREIGN KEY(ArtisanSectionID)
                    REFERENCES ArtisanSections(ID)");

            Execute(@"
                ALTER TABLE AssignmentSummaryQuestions
                    ADD CONSTRAINT fk_AssignmentSummaryQuestionsAssignmentSummaryID_AssignmentSummaryID FOREIGN KEY(AssignmentSummaryID)
                    REFERENCES AssignmentSummary(ID)");

            Execute(@"
                ALTER TABLE AssignmentSummaryQuestions
                    ADD CONSTRAINT fk_AssignmentSummaryQuestionsArtisanSectionID_ArtisanSectionsID FOREIGN KEY(ArtisanSectionID)
                    REFERENCES ArtisanSections(ID)");

            Execute(@"
                ALTER TABLE AssignmentSummaryQuestions
                    ADD CONSTRAINT fk_AssignmentSummaryQuestionsArtisanPageID_ArtisanPagesID FOREIGN KEY(ArtisanPageID)
                    REFERENCES ArtisanPages(ID)");

            Execute(@"
                ALTER TABLE AssignmentSummaryAttempts
                    ADD CONSTRAINT fk_AssignmentSummaryAttemptsTrainingProgramID_TrainingProgramID FOREIGN KEY(TrainingProgramID)
                    REFERENCES TrainingProgram(ID)");

            Execute(@"
                ALTER TABLE AssignmentSummaryAttempts
                    ADD CONSTRAINT fk_AssignmentSummaryAttemptsUserID_UserID FOREIGN KEY(UserID)
                    REFERENCES [User](ID)");

            Execute(@"
                ALTER TABLE AssignmentSummaryAttempts
                    ADD CONSTRAINT fk_AssignmentSummaryAttemptsAssignmentID_AssignmentSummaryID FOREIGN KEY(AssignmentSummaryID)
                    REFERENCES AssignmentSummary(ID)");

            Execute(@"
                CREATE UNIQUE INDEX idx_AssignmentSummary_ArtisanCourseIDArtisanSectionID 
                    ON AssignmentSummary (ArtisanCourseID, ArtisanSectionID)");

            Execute(@"
                CREATE UNIQUE INDEX idx_AssignmentSummaryAttempts
                    ON AssignmentSummaryAttempts (TrainingProgramID, OnlineCourseID, UserID, AssignmentSummaryID, AttemptID)");

            Execute(@"
                CREATE UNIQUE INDEX idx_AssignmentSummaryQuestions
                    ON AssignmentSummaryQuestions (AssignmentSummaryID, ArtisanSectionID, ArtisanPageID)");

            Execute(@"
                CREATE UNIQUE INDEX idx_AssignmentSummary_ArtisanSectionID
                    ON AssignmentSummary (ArtisanSectionID)");

            Execute(@"
                CREATE UNIQUE INDEX idx_AssignmentSummaryQuestions_ArtisanPageID
                    ON AssignmentSummaryQuestions (ArtisanPageID)");
        }
    }
}