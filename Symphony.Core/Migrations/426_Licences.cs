﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration426 : Migration
    {

        public override void Up()
        {
            TableSchema.Table rootLicenceTable = CreateTableWithKey("Licence", "ID");
            rootLicenceTable.AddColumn("ParentID", DbType.Int32, 0, false);
            rootLicenceTable.AddColumn("JurisdictionID", DbType.Int32, 0, false);
            rootLicenceTable.AddColumn("ProfessionID", DbType.Int32, 0, false);
            rootLicenceTable.AddColumn("Category", DbType.String, 255, false);
            rootLicenceTable.AddColumn("Name", DbType.String, 100, false);
            rootLicenceTable.AddColumn("Description", DbType.String, 1500, false);
            AddSubSonicStateColumns(rootLicenceTable);

            TableSchema.Table professionTable = CreateTableWithKey("Profession", "ID");
            professionTable.AddColumn("Name", DbType.String, 255, false);
            AddSubSonicStateColumns(professionTable);

            TableSchema.Table jurisdictionTable = CreateTableWithKey("Jurisdiction", "ID");
            jurisdictionTable.AddColumn("Name", DbType.String, 255, false);
            AddSubSonicStateColumns(jurisdictionTable);
        
            TableSchema.Table licenceGrants = CreateTableWithKey("LicenceGrants", "ID");
            licenceGrants.AddColumn("LicenceID", DbType.Int32, 0, false);
            licenceGrants.AddColumn("UserID", DbType.Int32, 0, false);
            licenceGrants.AddColumn("LocationID", DbType.Int32, 0, false);
            licenceGrants.AddColumn("JobRoleID", DbType.Int32, 0, false);
            licenceGrants.AddColumn("StartDate", DbType.Date, 0, false);
            licenceGrants.AddColumn("ExpiryDate", DbType.Date, 0, false);
            licenceGrants.AddColumn("LicenceName", DbType.String, 255, false);
            licenceGrants.AddColumn("LicenceNumber", DbType.String, 255, false);
            AddSubSonicStateColumns(licenceGrants);
            
        }

        public override void Down()
        {

        }
    }
}