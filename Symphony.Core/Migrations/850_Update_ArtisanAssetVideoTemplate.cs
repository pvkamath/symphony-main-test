﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration850 : Migration
    {
        public override void Up()
        {
            Execute(@"
            update ArtisanAssetTypes 
            set Template = '<div class=""video-container"" style=""<tpl if=""width == null"">width:640px;<tpl else>width:{width}px;</tpl><tpl if=""height == null"">height:480px;<tpl else>height:{height}px</tpl>""><video id=""video-{id}"" class=""video-js vjs-default-skin"" controls preload=""auto"" width=""<tpl if=""width == null"">640<tpl else>{width}</tpl>"" height=""<tpl if=""height == null"">480<tpl else>{height}</tpl>"" data-setup=""{}""><source src=""{path}"" type=""video/mp4""></video></div>' 
            where Extensions = 'mp4'");

        }
    }
}
