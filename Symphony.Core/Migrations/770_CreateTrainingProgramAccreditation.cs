﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration770 : Migration
    {
        public override void Up()
        {
            TableSchema.Table accreditation = CreateTableWithKey("TrainingProgramAccreditation", "ID");

            accreditation.AddColumn("AccreditationBoardID", DbType.Int32, 0, false);
            accreditation.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            accreditation.AddColumn("Status", DbType.Int32, 0, false);
            accreditation.AddColumn("StartDate", DbType.DateTime, 0, true);
            accreditation.AddColumn("ExpiryDate", DbType.DateTime, 0, true);
            accreditation.AddColumn("CreditHours", DbType.Single, 0, true);
            accreditation.AddColumn("CreditHoursLabel", DbType.String, 32, true, "'Credit Hours'");
            accreditation.AddColumn("Disclaimer", DbType.String, 32768, true);
            accreditation.AddColumn("AccreditationCode", DbType.String, 32, true);
            accreditation.AddColumn("IsDeleted", DbType.Boolean, 0, false, "0");

            AddSubSonicStateColumns(accreditation);
        }
    }
}
