﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration786 : Migration
    {
        public override void Up()
        {
            Execute(@"
                ALTER table Customer
                    DROP CONSTRAINT fk_Theme_ID_Customer_CustomThemeID
            ");

            RemoveColumn("Themes", "IsCustomTheme");
            RemoveColumn("Customer", "CustomThemeID");
        }
    }
}
