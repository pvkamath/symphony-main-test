﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration139 : Migration
    {
        public override void Up()
        {
            Execute(@"EXEC sp_rename 'CourseCategories', 'Category'");
        }
    }
}