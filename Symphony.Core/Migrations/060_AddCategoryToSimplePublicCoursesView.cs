﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration060 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(715);
            sb.AppendFormat(@"ALTER view [dbo].[SimplePublicCourses]{0}", Environment.NewLine);
            sb.AppendFormat(@"as{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}select{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}o.ID, o.Name, o.[Description], o.Credit, '0' as InternalCode, o.CustomerID, 2 as CourseTypeID, 0 ", Environment.NewLine);
            sb.AppendFormat(@"as CourseCompletionTypeID, 0 as HasSurvey, 0 as OnlineCourseID, Keywords, c.Name as ");
            sb.AppendFormat(@"CategoryName{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}from{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}OnlineCourse as o{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}inner join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}CourseCategories as c{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}o.CategoryID = c.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}where{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}publicindicator = 1{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}union{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}select{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}co.ID, co.Name, co.[Description], Credit, InternalCode, co.CustomerID, 1 as CourseTypeID, ", Environment.NewLine);
            sb.AppendFormat(@"CourseCompletionTypeId, HasSurvey, OnlineCourseID, '' as Keywords, c.Name as CategoryName{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}from{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Course as co{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}inner join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}CourseCategories c{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}co.CategoryID = c.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}where{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}publicindicator = 1{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);

            Execute(sb.ToString());
        }
    }
}