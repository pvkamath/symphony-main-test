﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration865 : Migration
    {

        public override void Up()
        {
            Execute(@"
                update Templates
                set Description = 'Sent when student has passed all required assignments for the training program.'
                where CodeName = 'AssignmentsGradedPassed'

                update Templates
                set Description = 'Sent when all assignments for the training program have been graded and at least one assignment has been failed.'
                where CodeName = 'AssignmentsGradedFailed'

                update Templates
                set Description = 'Sent whenever an instructor grades an assignment regardless of the grade (passed/failed). Do not use this notification in conjunction with Single Assignment Graded Passed or Single Assignment Graded Failed.'
                where CodeName = 'AssignmentGraded'

                update Templates
                set DisplayName = REPLACE(REPLACE('All ' + DisplayName, 'Failed', ' - Failed'), 'Passed', ' - Passed')
                where CodeName = 'AssignmentsGradedPassed' or CodeName = 'AssignmentsGradedFailed'

                update Templates
                set DisplayName = REPLACE(DisplayName, 'Single ', '')
                where CodeName = 'SingleAssignmentGradedPassed' or CodeName = 'SingleAssignmentGradedFailed'
                
        ");
        }

    }
}
