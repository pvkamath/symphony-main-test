﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration467 : Migration
    {

        public override void Up()
        {
            Execute(@"ALTER TABLE Templates ALTER COLUMN Subject varchar(2000)");
            Execute(@"ALTER TABLE Templates ALTER COLUMN Body varchar(max)");
            Execute(@"ALTER TABLE TrainingProgram ALTER COLUMN Name varchar(2000)");
        }

    }
}