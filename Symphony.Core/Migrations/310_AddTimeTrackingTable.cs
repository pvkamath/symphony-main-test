﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration310: Migration
    {
        public override void Up()
        {
            TableSchema.Table onlineCourseTimeTracking = CreateTableWithKey("OnlineCourseTimeTracking", "ID");
            onlineCourseTimeTracking.AddColumn("UserID", DbType.Int32, 0, false);
            onlineCourseTimeTracking.AddColumn("Duration", DbType.Int64, 0, false);
            onlineCourseTimeTracking.AddColumn("Date", DbType.Date, 0, false);
            onlineCourseTimeTracking.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            onlineCourseTimeTracking.AddColumn("CategoryID", DbType.Int32, 0, false);
        }
    }
}