﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration738 : Migration
    {
        public override void Up()
        {
            AddColumn("License", "AccreditationBoardID", DbType.Int32, 0, true);

            Execute("ALTER TABLE License"
                + " ADD CONSTRAINT fk_AccreditationBoard_ID_License_AccreditiationBoardID FOREIGN KEY(AccreditationBoardID)"
                    + " REFERENCES AccreditationBoard(ID)"
                );
        }
    }
}