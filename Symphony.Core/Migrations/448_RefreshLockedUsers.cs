﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration448 : Migration
    {

        public override void Up()
        {
            Execute(@"EXECUTE sp_refreshview N'LockedUsers';");
        }

    }
}