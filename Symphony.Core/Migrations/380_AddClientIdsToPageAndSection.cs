﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration380 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "ClientID", DbType.Int32, 0, true);
            AddColumn("ArtisanPages", "ClientID", DbType.Int32, 0, true);
        }
    }
}