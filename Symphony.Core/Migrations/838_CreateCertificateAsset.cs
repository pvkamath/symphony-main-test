﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration838 : Migration
    {
        public override void Up()
        {
            TableSchema.Table tbl = CreateTableWithKey("CertificateAsset", "ID");
            tbl.AddColumn("Name", DbType.String);
            tbl.AddColumn("Filename", DbType.String);
            tbl.AddColumn("Description", DbType.String);
            tbl.AddColumn("IsDeleted", DbType.Boolean, 0, false, "0");

            AddSubSonicStateColumns(tbl);
        }
    }
}
