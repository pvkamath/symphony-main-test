﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration195 : Migration
    {
        public override void Up()
        {
            Execute(@"
                    DECLARE @sqlstr nvarchar(max);

                    DECLARE cur Cursor
                    FOR
	                    SELECT 'ALTER TABLE Symphony.dbo.[' + name + '] ADD CreatedByUserId int NOT NULL DEFAULT(0), ModifiedByUserId int NOT NULL DEFAULT(0), CreatedByActualUserId int NULL, ModifiedByActualUserId int NULL' as sqlstr
	                    FROM Symphony.sys.tables 
	                    WHERE name NOT LIKE 'aspnet_%' AND name NOT LIKE 'SubSonic%'
	                    ORDER BY name
                    OPEN cur

                    FETCH NEXT FROM cur INTO @sqlstr
                    WHILE @@FETCH_STATUS = 0
                    BEGIN
                        EXEC sp_executesql @sqlstr
                        FETCH NEXT FROM cur INTO @sqlstr
                    END
      
                    CLOSE cur 
                    DEALLOCATE cur ");
        }
    }
}