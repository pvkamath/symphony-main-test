﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration143 : Migration
    {
        public override void Up()
        {
            Execute("insert ScheduleParameters (DisplayName, CanFilterComplete,CodeName) values ('New Hire - Expiration', 0, 'newhireexpiration')");
        }
    }
}