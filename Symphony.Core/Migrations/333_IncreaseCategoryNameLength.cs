﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration333: Migration
    {
        public override void Up()
        {
            Execute(@"ALTER TABLE [Category] ALTER COLUMN [Name] varchar(512)");
        }
    }
}