﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration580 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE TABLE dbo.ArtisanCourseAuthorsLink
(
	Id INT IDENTITY(1, 1) NOT NULL,
	UserId INT NOT NULL,
	ArtisanCourseId INT NOT NULL
)

ALTER TABLE dbo.ArtisanCourseAuthorsLink
ADD CONSTRAINT PK_ArtisanCourseAuthorsLink__Id PRIMARY KEY (Id)

ALTER TABLE dbo.ArtisanCourseAuthorsLink
ADD CONSTRAINT FK_ArtisanCourseAuthorsLink_Users__Id FOREIGN KEY (UserId) REFERENCES dbo.[User](ID)

ALTER TABLE dbo.ArtisanCourseAuthorsLink
ADD CONSTRAINT FK_ArtisanCourseAuthorsLink_ArtisanCourses__Id FOREIGN KEY (ArtisanCourseId) REFERENCES dbo.ArtisanCourses(ID)

");
        }
    }
}
