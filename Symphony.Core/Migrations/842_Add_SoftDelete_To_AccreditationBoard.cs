﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration842 : Migration
    {
        public override void Up()
        {
            AddColumn("AccreditationBoard", "IsDeleted", DbType.Boolean, 0, false, "0");
        }
    }
}