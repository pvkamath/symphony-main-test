﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration830 : Migration
    {
        /// <summary>
        /// Update training program users view to call the function instead
        /// so we can ensure any existing places where this view is used will
        /// remain consistant with the stored procedure version. 
        /// </summary>
        public override void Up()
        {
            Execute(@"
                ALTER view [dbo].[TrainingProgramUsers] as
                    -- this view is going to get us a list of all the users in a given training program

                    SELECT     
	                    --ROW_NUMBER() OVER (ORDER BY TrainingProgramID) AS ID, 
	                    1 as ID,
	                    CustomerID, 
	                    TrainingProgramID, 
	                    TrainingProgramName, 
	                    TrainingProgramStartDate, 
	                    TrainingProgramDueDate, 
	                    TrainingProgramEndDate, 
	                    TrainingProgramDescription, 
	                    EnforceRequiredOrder, 
	                    IsNewHire, 
	                    IsUserNewHire,
	                    SourceNodeID, 
	                    HierarchyTypeID, 
	                    FirstName, 
	                    LastName, 
	                    UserID,
                        ((SELECT     COUNT(1)
                            FROM         TrainingProgramToCourseLink
                            WHERE     T .TrainingProgramID = TrainingProgramID AND SyllabusTypeID IN (1, 4)) + MinimumElectives) 
                        AS MinimumCourseCount, 
                        MinimumElectives,
                        ParentTrainingProgramID
                    FROM         
	                    fGetTrainingProgramUsers(null, null) 
                    AS T
                    -- where 
                    --		(	
                    --			-- all the single-hierarchy TP users
                    --			(	select 
                    --				count(httpl.ID) 
                    --				from HierarchyToTrainingProgramLink httpl 
                    --				where httpl.TrainingProgramID = T.TrainingProgramID 
                    --			)  = 1 
                    --			-- all the HierarchyTypes of User
                    --			 OR
                    --			
                    --				T.HierarchyTypeID = 4
                    --			
                    --			-- the first instance of each user in both hierarchies of a TP
                    --			--OR
                    --			--(
                    --			--	select Count(tpui.UserID) as UserIDCount 
                    --			--						from TrainingProgramUsersInclusive tpui 
                    --			---						where tpui.TrainingProgramID = T.TrainingProgramID
                    --			--						and tpui.userID = T.UserID
                    --			--) = 2
                    --		)
                    ;;;

");
        }
    }
}
