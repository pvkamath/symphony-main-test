﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration295 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "MaximumQuestionTime", DbType.Int32, 0, false, "0");
        }
    }
}