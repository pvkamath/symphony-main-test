﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration265 : Migration
    {
        public override void Up()
        {

            Execute(
@"CREATE VIEW [dbo].[AssignedTrainingProgramCoursesDetail]
AS
SELECT [available].[ID], 
       [available].[CourseTypeID], 
       [available].[Name], 
       [available].[CustomerID], 
       [available].[Key], 
       [tp].[SyllabusTypeID], 
       [tp].[TrainingProgramID], 
       [tp].[RequiredSyllabusOrder],
       [ac].[Description],
	   [ac].[Fee],
	   [ac].[Credit],
	   [ac].[PublicIndicator],
	   [ac].[ModifiedBy],
	   [ac].[CreatedBy],
	   [ac].[ModifiedOn],
	   [ac].[CreatedOn],
	   [ac].[CodeName],
       [c].[NumberOfDays],
       [c].[DailyDuration]
FROM   [dbo].[TrainingProgramToCourseLink] AS tp 
INNER JOIN [dbo].[AvailableTrainingProgramCourses] AS available ON tp.CourseID = available.ID AND tp.CourseTypeID = available.CourseTypeID
INNER JOIN [dbo].[AllCourses] ac ON ac.CourseID = available.ID AND ac.CourseTypeID = available.CourseTypeID
LEFT JOIN [dbo].[Course] c ON c.ID = ac.CourseID");  
        }
    }
}