﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration031 : Migration
    {
        public override void Up()
        {
            TableSchema.Table sections = CreateTable("ArtisanSections");
            sections.AddPrimaryKeyColumn("ID");
            sections.AddColumn("Name", DbType.String, 50, false);
            sections.AddColumn("SectionID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(sections);

            TableSchema.Table pages = CreateTable("ArtisanPages");
            pages.AddPrimaryKeyColumn("ID");
            pages.AddColumn("Name", DbType.String, 50, false);
            pages.AddColumn("Html", DbType.String, 9000, false);
            pages.AddColumn("PageType", DbType.Int32, 0, false);
            pages.AddColumn("QuestionType", DbType.Int32, 0, false);
            pages.AddColumn("SectionID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(pages);
        }

        public override void Down()
        {
            DropTable("ArtisanSections");
            DropTable("ArtisanPages");
        }
    }
}