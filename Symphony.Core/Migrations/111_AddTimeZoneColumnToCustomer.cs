﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration111 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "TimeZone", DbType.String, 100, false, "'UTC'");
        }
    }
}