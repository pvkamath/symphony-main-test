using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration489 : Migration
    {

        public override void Up()
        {
            Execute(@"
			
CREATE TABLE [dbo].[Workflows](
	[ScreenScrapingId] [int] IDENTITY(1, 1) NOT NULL,
	[ReportName] [varchar](200) NOT NULL,
	[ActionName] [varchar](200) NULL,
	[ActionOrder] [int] NULL,
	[ActionParameter1] [varchar](1000) NULL,
	[ActionParameter2] [varchar](2000) NULL,
	[ActionParameter3] [varchar](200) NULL,
	[ActionParameter4] [varchar](200) NULL,
	[ActionParameter5] [varchar](200) NULL,
 CONSTRAINT [PK_Workflows__ScreenScrapingId] PRIMARY KEY CLUSTERED 
(
	[ScreenScrapingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
");

Execute(@"INSERT INTO [dbo].[Workflows] ([ReportName], [ActionName], [ActionOrder], [ActionParameter1], [ActionParameter2], [ActionParameter3], [ActionParameter4], [ActionParameter5]) VALUES (N'ProSchools', N'HTTP_REQUEST', 1, N'https://store.proschools.com/cas/login', N'', N'', N'', N'SaveProSchool_LT_Value')

");
			
        }

    }
}