using SubSonic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Symphony.Core.Migrations
{
    public class Migration511 : Migration
    {
        public override void Up()
		{
            Execute(@"
CREATE FUNCTION dbo.fGetRelatedCustomers
(
	@CustomerId INT
)
RETURNS @Tr TABLE (CustomerId INT, NetworkId INT)
AS
BEGIN
	INSERT INTO @Tr (CustomerId, NetworkId)
	SELECT
		cn.CustomerId, cn.NetworkId
	FROM
		dbo.CustomerNetwork cn WITH (NOLOCK)
		INNER JOIN (SELECT NetworkId FROM dbo.CustomerNetwork WHERE CustomerId = @CustomerId) n ON n.NetworkId = cn.NetworkId
	WHERE 
		cn.CustomerId != @CustomerId
	RETURN;
END
");
			
		}
	}
}