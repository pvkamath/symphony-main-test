﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration871 : Migration
    {

        public override void Up()
        {
            AddColumn("TrainingProgram", "PostTestValidation", DbType.Boolean);
            AddColumn("TrainingProgram", "PreTestValidation", DbType.Boolean);
        }
    }
}
