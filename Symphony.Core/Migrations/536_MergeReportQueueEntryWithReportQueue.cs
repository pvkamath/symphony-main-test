﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration536 : Migration
    {
        public override void Up()
        {
            AddColumn("ReportQueues", "CustomerID", DbType.Int32, 0, false, "0");
            AddColumn("ReportQueues", "ReportID", DbType.Int32, 0, false, "0");
            AddColumn("ReportQueues", "StatusID", DbType.Int32, 0, false, "0");
            AddColumn("ReportQueues", "StartedOn", DbType.DateTime, 0, true);
            AddColumn("ReportQueues", "FinishedOn", DbType.DateTime, 0, true);
            AddColumn("ReportQueues", "ReportName", DbType.String, 64, false, "''");
            AddColumn("ReportQueues", "DownloadCSV", DbType.Boolean, 0, false, "0");
            AddColumn("ReportQueues", "DownloadXLS", DbType.Boolean, 0, false, "0");
            AddColumn("ReportQueues", "DownloadPDF", DbType.Boolean, 0, false, "0");

            Execute("ALTER TABLE ReportQueues ADD ReportData nvarchar(max)");
        }
    }
}