﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration205 : Migration
    {

        public override void Up()
        {
            Execute(@"CREATE PROCEDURE [dbo].[GetUserUnassignedTiles] (@userId int)
AS
BEGIN
	WITH Assignments as (
		SELECT htl.TileID as ID
			FROM HierarchyToTileLinks htl 
			JOIN (
				select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
				union
				select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
				union
				select f.ID, f.TypeID from useraudience 
					cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
				where userid = @userid
				union
				select @userid, 4
			) as H on h.id = htl.HierarchyNodeID and h.TypeID = htl.HierarchyTypeID
	)
	select distinct
			  tl.ID
			, tl.Name
			, tl.TileUri
			, ap.ID as ApplicationId
			, ap.Name as ApplicationName
			, ap.PassUserNamePass
			, case 
				when ap.PassUserNamePass = 0 then 'none'
				when ap.PassUserNamePass = 1 and cr.ID is null then 'missing'
				else 'complete'
			end as CredentialState
			, cr.ID as CredentialID
			, cr.UserName as CredentialUserName
			, case when cr.Password is null 
				then null
				else '*****'
				end as CredentialPassword
			, @userId as UserID
	from Tiles as tl
		JOIN Applications as ap on ap.ID = tl.ApplicationID
		LEFT JOIN Credentials as cr on cr.ApplicationID = ap.ID
	where 
		(
			    cr.UserID = @userId 
			 or cr.UserID is NULL
		)
		and tl.ID not in (select id from Assignments)
END");

        
      
        }

        public override void Down()
        {
            Execute(@"DROP PROCEDURE [dbo].[GetUserUnassignedTiles]");
        }
    }
}