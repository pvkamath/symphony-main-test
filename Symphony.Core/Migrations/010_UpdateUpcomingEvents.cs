﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration010 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(1816);
            sb.AppendFormat(@"ALTER view [dbo].[SimpleUpcomingEvents] {0}", Environment.NewLine);
            sb.AppendFormat(@"as{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}-- get the classroom classes{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}select{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.ID as EventID,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}course.ID as CourseID,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.WebinarKey,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}reg.WebinarRegistrationID,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}isnull(p.[Description],'Class') as EventType,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.Name as EventName,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}reg.RegistrantID as UserID,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}StartDateTime as StartDate,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}DATEADD(hh, Duration, StartDateTime) as EndDate{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}from{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Registration reg{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Class c{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}reg.ClassID = c.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Course course{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}course.ID = c.CourseID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}left join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}PresentationType p{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}course.PresentationTypeID = p.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}-- we can show multiple items in the upcoming events because{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}-- each class is...well, upcoming{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}ClassDate cd{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}cd.ClassID = c.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}-- only show classes for which they're actually registered{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}where{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}RegistrationStatusID = 4{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}union{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}-- get the meetings{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}select{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}m.ID as EventID, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}0 as CourseID, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'' as WebinarKey,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'' as WebinarRegistrationID,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'meeting' as EventType,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}m.[Subject] as EventName, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}m.UserID, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}m.StartDateTime as StartDate, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}m.EndDateTime as EndDate{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}from{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}MyGTMMeetings m{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}union{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}-- get classes being taught{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}select{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.ID as EventID,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}course.ID as CourseID,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.WebinarKey,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'[NA]' as WebinarRegistrationID,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'[Instructor] ' + isnull(p.[Description],'Class') as EventType,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.Name as EventName,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}ci.InstructorID as UserID,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}StartDateTime as StartDate,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}DATEADD(hh, Duration, StartDateTime) as EndDate{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}from{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}ClassInstructors ci{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Class c{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}ci.ClassID = c.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Course course{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}course.ID = c.CourseID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}left join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}PresentationType p{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}course.PresentationTypeID = p.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}-- we can show multiple items in the upcoming events because{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}-- each class is...well, upcoming{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}ClassDate cd{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}cd.ClassID = c.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);

            Execute(sb.ToString());
        }
    }
}