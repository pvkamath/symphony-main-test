﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration749 : Migration
    {

        public override void Up()
        {
            Execute(@"
                update Templates set IsUserCreated = 0 where IsScheduled = 1
            ");
        }

    }
}