﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration196 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "AccountExecutive", DbType.Int32, 0, true);
            AddColumn("Customer", "CustomerCareRep", DbType.Int32, 0, true);
        }
    }
}