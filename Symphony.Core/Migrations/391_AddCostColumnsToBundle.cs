﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration391 : Migration
    {
        public override void Up()
        {
            Execute(@"Alter Table TrainingProgramBundle Add [Cost] [decimal](10, 2) NOT NULL");
            Execute(@"Alter Table TrainingProgramBundle Add [ListPrice] [decimal](10, 2) NOT NULL");
        }
    }
}