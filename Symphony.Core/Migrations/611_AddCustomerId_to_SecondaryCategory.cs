﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration611 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER TABLE dbo.SecondaryCategory
ADD CustomerId INT NULL

ALTER TABLE dbo.SecondaryCategory
ADD CONSTRAINT FK_SecondaryCategory_Customer__Id FOREIGN KEY (CustomerId) REFERENCES dbo.Customer(ID)

");
        }
    }
}
