﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration538 : Migration
    {

        public override void Up()
        {
            Execute(@"
CREATE PROCEDURE [dbo].[GetReportProcedures] (
	@database nvarchar(100) = 'Symphony',
    @prefix nvarchar(60) = 'report_'
) AS
BEGIN
    DECLARE @sql nvarchar(max) 
set @sql	= 'select 
			r.[SPECIFIC_NAME] as FullName, replace(r.[SPECIFIC_NAME], ''' + @prefix + ''', '''') as CodeName 
			from ' + @database + '.[information_schema].[routines] r
			where [ROUTINE_TYPE] = ''PROCEDURE''
            and [SPECIFIC_NAME] like ''' + @prefix + '%'''

    EXEC sp_executesql @sql;
END;
");
        }

    }
}
