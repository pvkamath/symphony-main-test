﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration758 : Migration
    {
        public override void Up()
        {
            TableSchema.Table entityShare = CreateTableWithKey("NetworkSharedEntity", "ID");
            entityShare.AddColumn("EntityID", DbType.Int32, 0, false);
            entityShare.AddColumn("EntityTypeID", DbType.Int32, 0, false);
            entityShare.AddColumn("CustomerNetworkDetailID", DbType.Int32, 0, false);
            entityShare.AddColumn("NetworkID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(entityShare);

            Execute(@"
            ALTER TABLE dbo.NetworkSharedEntity
            ADD CONSTRAINT FK_NetworkSharedEntity_CustomerNetworkDetailID FOREIGN KEY (CustomerNetworkDetailID) REFERENCES dbo.CustomerNetworkDetail(DetaildId)
            ");
            Execute(@"
            ALTER TABLE dbo.NetworkSharedEntity
            ADD CONSTRAINT FK_NetworkSharedEntity_NetworkID FOREIGN KEY (NetworkID) REFERENCES dbo.Network(Id)
            ");

        }

    }
}