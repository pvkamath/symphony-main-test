﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration291 : Migration
    {
        // Sets the status of the course to incomplete if there is a required survey that is not taken for the course
        // adds the issurveymandatory flag to transacript entry
        public override void Up()
        {
            Execute(@"ALTER VIEW [dbo].[ClassesAndTPOnlineCourses]
AS
SELECT 
        c.[ID],
        c.[CustomerID],
        c.[LockedIndicator],
        c.[Name],
        c.[IsVirtual],
        cd.[MinStartDateTime] as MinClassDate,
        1 as CourseTypeID,
        0 as TrainingProgramID,
        null as TrainingProgramName,
        0 as RequiredSyllabusOrder,
        0 as SyllabusTypeID
    FROM [dbo].[Class] c
    LEFT JOIN [ClassDateMinMax] cd ON cd.[ClassID] = c.[ID]
    UNION
    SELECT
        CASE WHEN tpl.[CourseTypeID] = 2 THEN o.[ID] ELSE cl.[ID] END,
        tpl.[CustomerID],
        CASE WHEN tpl.[CourseTypeID] = 2 THEN CASE WHEN o.[Active] = 1 THEN 0 ELSE 1 END ELSE cl.[LockedIndicator] END,
        CASE WHEN tpl.[CourseTypeID] = 2 THEN o.[Name] ELSE cl.[Name] END,
        CAST(CASE WHEN tpl.[CourseTypeID] = 2 THEN 1 ELSE 2 END as bit),
        CASE WHEN tpl.[CourseTypeID] = 2 THEN tpl.[ActiveAfterDate] ELSE cd.[MinStartDateTime] END,
        tpl.[CourseTypeID],
        tpl.[TrainingProgramID],
        tp.[Name] as TrainingProgramName,
        tpl.[RequiredSyllabusOrder] as RequiredSyllabusOrder,
        tpl.[SyllabusTypeID] as SyllabusTypeID
    FROM [dbo].[TrainingProgramToCourseLink] tpl
    LEFT JOIN [OnlineCourse] o ON o.ID = tpl.[CourseID] AND tpl.[CourseTypeID] = 2
    LEFT JOIN [Class] cl ON cl.[CourseID] = tpl.[CourseID] AND tpl.[CourseTypeID] = 1
    LEFT JOIN [ClassDateMinMax] cd ON cd.[ClassID] = cl.[ID]
    INNER JOIN [dbo].[TrainingProgram] tp ON tp.[ID] = tpl.[TrainingProgramID]
;"); 
        }
    }
}