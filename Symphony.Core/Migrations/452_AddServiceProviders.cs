﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration452 : Migration
    {

        public override void Up()
        {
            TableSchema.Table serviceProviders = CreateTableWithKey("ServiceProvider", "ID");
            serviceProviders.AddColumn("Name", DbType.String, 512, false);
            serviceProviders.AddColumn("Url", DbType.String, 2084, false);
            serviceProviders.AddColumn("ArtifactUrl", DbType.String, 2084, false);
            serviceProviders.AddColumn("LogoutUrl", DbType.String, 2084, false);
            serviceProviders.AddColumn("Method", DbType.String, 1024, false);
            AddSubSonicStateColumns(serviceProviders);
        }

    }
}