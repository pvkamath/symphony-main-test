﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration346: Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgramToCourseLink", "ActiveAfterMinutes", DbType.Double, 0, true);
            AddColumn("TrainingProgramToCourseLink", "ExpiresAfterMinutes", DbType.Double, 0, true);
        }
    }
}