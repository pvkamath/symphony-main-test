﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration270 : Migration
    {
        public override void Up()
        {
            AddColumn("Registration", "CanMoveForwardIndicator", DbType.Boolean, 0, false, "0");
            AddColumn("OnlineCourseRollup", "CanMoveForwardIndicator", DbType.Boolean, 0, false, "0");
        }
    }
}