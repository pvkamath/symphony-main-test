﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration889 : Migration
    {
        public override void Up()
        {
            Execute(@"
IF NOT EXISTS(SELECT * FROM dbo.syscolumns WITH (nolock) WHERE [name] = 'OriginalReleaseDate' AND id = (SELECT [id] FROM dbo.sysobjects WITH (nolock) WHERE [name] = 'TrainingProgram'))
BEGIN    
    ALTER TABLE dbo.TrainingProgram ADD OriginalReleaseDate datetime
END");

        }
    }
}