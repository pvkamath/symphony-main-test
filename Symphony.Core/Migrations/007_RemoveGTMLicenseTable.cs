﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration007 : Migration
    {
        public override void Up()
        {
            

            // fixes the GTMOrganizerCount, which was previously pointed at "CustomerBlackBox.dbo"
            StringBuilder sb = new StringBuilder(244);
            sb.AppendFormat(@"ALTER VIEW [dbo].[GTMOrganizerCount]{0}", Environment.NewLine);
            sb.AppendFormat(@"AS{0}", Environment.NewLine);
            sb.AppendFormat(@"SELECT     {0}", Environment.NewLine);
            sb.AppendFormat(@"{0}COUNT(gtmO.UserID) AS OrganizerCount, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}cUser.CustomerID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"FROM         {0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[User] AS cUser LEFT OUTER JOIN{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"dbo.GTMOrganizer AS gtmO {0}", Environment.NewLine);
            sb.AppendFormat(@"{0}ON gtmO.UserID = cUser.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"GROUP BY cUser.CustomerID");

            Execute(sb.ToString());
        }
    }
}