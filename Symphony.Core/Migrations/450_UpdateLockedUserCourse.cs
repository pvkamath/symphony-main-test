﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration450 : Migration
    {

        public override void Up()
        {
            Execute(@"ALTER view [dbo].[LockedUserCourses] as
                select
	                tp.name as TrainingProgramName, 
	                oc.name as CourseName, 
	                u.username as Username,
	                case when ocr.testattemptcount >= (coalesce(oc.retries, customer.Retries) + 1) and ocr.trainingprogramid > 0 then 1 else 0 end as IsLockedOut,
	                ocr.*
                from
	                onlinecourse oc
                left join
	                onlinecourserollup ocr
                on
	                oc.id = ocr.courseid
                join
	                [customer]
                on
	                customer.id = oc.customerid
                left join
	                onlineTraining.dbo.ScormRegistration sr
                on
	                sr.scorm_registration_id = ocr.originalregistrationid and sr.course_id = ocr.courseid
                left join
	                trainingprogram tp
                on
	                tp.id = ocr.trainingprogramid
                join
	                [user] u
                on
	                u.id = ocr.userid
	
                ;");
        }

    }
}