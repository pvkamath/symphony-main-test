﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    class Migration577 : Migration
    {
        public override void Up()
        {

            

            Execute(@"

CREATE TABLE dbo.AuthorDetails
(
	Id INT IDENTITY(1, 1) NOT NULL,
	UserId INT NOT NULL,
    Speciality VARCHAR(4000) NULL,
	Notes VARCHAR(4000)
)

ALTER TABLE dbo.AuthorDetails
ADD CONSTRAINT PK_AuthorDetails__Id PRIMARY KEY (Id)

");
        }
    }
}
