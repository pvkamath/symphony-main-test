﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration231 : Migration
    {

        public override void Up()
        {
            Execute(@"
create view LockedUserCourses as
select
	tp.name as TrainingProgramName, ocr.*, oc.name as CourseName, u.username as Username
from
	onlinecourserollup ocr
join
	onlinecourse oc
on
	oc.id = ocr.courseid
join
	[customer]
on
	customer.id = oc.customerid
join
	onlineTraining.dbo.ScormRegistration sr
on
	sr.scorm_registration_id = ocr.originalregistrationid and sr.course_id = ocr.courseid
join
	trainingprogram tp
on
	tp.id = ocr.trainingprogramid
join
	[user] u
on
	u.id = ocr.userid
where
	ocr.testattemptcount >= (coalesce(oc.retries, customer.OnlineCourseRetries) + 1) and ocr.trainingprogramid > 0
	
");
        }

        public override void Down()
        {
            
        }
    }
}