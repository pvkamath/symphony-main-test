﻿using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration716 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE OnlineCourseAccreditation"
                + " ADD CONSTRAINT fk_AccreditationBoard_ID_OnlineCourseAccreditation_AccreditiationBoardID FOREIGN KEY(AccreditationBoardID)"
                    + " REFERENCES AccreditationBoard(ID)"
                );

            Execute("ALTER TABLE OnlineCourseAccreditation"
                + " ADD CONSTRAINT fk_OnlineCourse_ID_OnlineCourseAccreditation_OnlineCourseID FOREIGN KEY(OnlineCourseID)"
                    + " REFERENCES OnlineCourse(ID)"
                );

            Execute("ALTER TABLE OnlineCourseAccreditation"
                + " ADD CONSTRAINT fk_ArtisanCourseAccreditation_ID_OnlineCourseAccreditation_ArtisanCourseAccreditationID FOREIGN KEY(ArtisanCourseAccreditationID)"
                    + " REFERENCES ArtisanCourseAccreditation(ID)"
                );
        }
    }
}