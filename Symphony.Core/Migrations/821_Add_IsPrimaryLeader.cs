﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration821 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgramLeader", "IsPrimary", DbType.Boolean, 0, false, "0");
        }
    }
}