﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration337: Migration
    {
        public override void Up()
        {
            Execute(@"ALTER TABLE [ArtisanCourses] ALTER COLUMN [Name] varchar(512)");
        }
    }
}