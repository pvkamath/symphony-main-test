﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration582 : Migration
    {
        public override void Up()
        {
            Execute(@"

ALTER TABLE [dbo].[ArtisanCourses] DROP CONSTRAINT [DF_ArtisanCourses_Objectives]

ALTER TABLE dbo.ArtisanCourses
ALTER COLUMN Objectives VARCHAR(3000)

ALTER TABLE [dbo].[ArtisanCourses] ADD CONSTRAINT [DF_ArtisanCourses_Objectives]  DEFAULT ('') FOR [Objectives]

");
            
        }
    }
}
