﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration257 : Migration
    {
        public override void Up()
        {
            TableSchema.Table messageBoard = CreateTableWithKey("MessageBoard", "ID");
            messageBoard.AddColumn("Name", DbType.String, 256, false);
            AddSubSonicStateColumns(messageBoard);

            TableSchema.Table messageBoardTopic = CreateTableWithKey("MessageBoardTopic", "ID");
            messageBoardTopic.AddColumn("MessageBoardID", DbType.Int32, 0, false);
            messageBoardTopic.AddColumn("UserID", DbType.Int32, 0, false);
            messageBoardTopic.AddColumn("Title", DbType.String, 512, false);
            messageBoardTopic.AddColumn("IsSticky", DbType.Boolean, 0, false);
            messageBoardTopic.AddColumn("IsLocked", DbType.Boolean, 0, false);
            AddSubSonicStateColumns(messageBoardTopic);

            TableSchema.Table messageBoardPost = CreateTableWithKey("MessageBoardPost", "ID");
            messageBoardPost.AddColumn("TopicID", DbType.Int32, 0, false);
            messageBoardPost.AddColumn("UserID", DbType.Int32, 0, false);
            messageBoardPost.AddColumn("PostID", DbType.Int32, 0, true);
            messageBoardPost.AddColumn("IsPrivate", DbType.Boolean, 0, false);
            messageBoardPost.AddColumn("Content", DbType.String);
            AddSubSonicStateColumns(messageBoardPost);
        }
    }
}