﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration330: Migration
    {
        public override void Up()
        {
            Execute(@"CREATE PROCEDURE [dbo].[AddFinal](@CID int, @CODE nvarchar(255))
as
begin
DECLARE @PSCID int
DECLARE @STYPE int
DECLARE @QSETID int
DECLARE @PASS int
DECLARE @QNAME nvarchar(10)
set @STYPE = 6

	SELECT 
			@PSCID = c.course_id,
			@QSETID = e.QSET_ID_1,
			@PASS = e.FINAL_PASSPERCENT
		from Analyst.dbo.ps_courses c  
		left join Analyst.dbo.ps_final_exams e on e.course_id = c.course_id
		where c.course_code = @CODE
	IF (@PASS is NULL)
		SET @PASS = 70
	
	IF (@QSETID is not NULL)
	BEGIN
		exec Symphony.dbo.CreateQuizExam @CID, @PSCID, @QSETID, 'Final Exam', @PASS, @STYPE, '', @CODE
	END
END

;


");


        }
    }
}