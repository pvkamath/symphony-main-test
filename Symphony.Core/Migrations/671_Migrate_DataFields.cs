﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration671 : Migration
    {
        
        public override void Up()
        {
            Execute(@"
                -- Copy LicenseDataFields to UserDataField
                set identity_insert UserDataField on

                insert into UserDataField (ID, CustomerID, Xtype, Config, CodeName, DisplayName, CategoryCodeName, CategoryDisplayName, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy)
                select 
                    ID, 
                    0, 
                    null, 
                    null, 
                    lower(replace(Name, ' ', '')), 
                    Name, 
                    null,
                    null,
                    CreatedOn, 
                    ModifiedOn, 
                    CreatedBy, 
                    ModifiedBy
                from LicenseDataFields
	
                set identity_insert UserDataField off

                -- Copy LicenseDataFieldsToLicensesMap to UserDataFieldLicenseMap
                set identity_insert UserDataFieldLicenseMap on

                insert into UserDataFieldLicenseMap (ID, LicenseID, UserDataFieldID, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy)
                select
	                ID, 
	                LicenseID, 
	                LicenseDataFieldID,
	                CreatedOn,
	                ModifiedOn, 
	                CreatedBy,
	                ModifiedBy
                from 
	                LicenseDataFieldsToLicensesMap

                set identity_insert UserDataFieldLicenseMap off
                
                -- Copy LicenseDataFieldsToUsersMap to UserDataFieldUserMap
                set identity_insert UserDataFieldUserMap on

                insert into UserDataFieldUserMap (ID, UserID, UserDataFieldID, UserValue, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy)
                select
	                ID,
	                UserID,
	                LicenseDataFieldID,
	                UserValue,
	                CreatedOn,
	                ModifiedOn,
	                CreatedBy,
	                ModifiedBy
                from
	                LicenseDataFieldsToUsersMap
	
                set identity_insert UserDataFieldUserMap off
");
        }
    }
}
