﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration256 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgramToCourseLink", "ActiveAfterDate", DbType.DateTime, 0, true, "null");
        }
    }
}