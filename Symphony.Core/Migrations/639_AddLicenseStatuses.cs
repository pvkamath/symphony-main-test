﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration639 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE TABLE dbo.LicenseStatus
(
	Id INT IDENTITY(1, 1) NOT NULL,
	StatusName VARCHAR(50) NOT NULL
)

INSERT INTO dbo.LicenseStatus (StatusName)
VALUES ('Pending Course Work')

INSERT INTO dbo.LicenseStatus (StatusName)
VALUES ('Pending Regulatory')

INSERT INTO dbo.LicenseStatus (StatusName)
VALUES ('Regulatory Approved')

INSERT INTO dbo.LicenseStatus (StatusName)
VALUES ('Assigned')

INSERT INTO dbo.LicenseStatus (StatusName)
VALUES ('Revoked')


ALTER TABLE dbo.LicenseStatus
ADD CONSTRAINT PK_LicenseStatus__Id PRIMARY KEY (Id)

ALTER TABLE dbo.LicenseAssignments
ADD AssignmentStatusId INT NULL

ALTER TABLE dbo.LicenseAssignments
ADD TrainingProgramId INT NULL

ALTER TABLE dbo.LicenseAssignments
ADD OnlineCourseId INT NULL

ALTER TABLE dbo.LicenseAssignments
ADD CONSTRAINT FK_LicenseAssignments_LicenseStatus__AssignmentStatusId FOREIGN KEY (AssignmentStatusId) REFERENCES dbo.LicenseStatus(Id)

");
        }
    }
}