﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration663 : Migration
    {
        public override void Up()
        {
            AddColumn("LicenseAssignments", "ConfirmationNumber", DbType.String, 32, false, "''");
        }
    }
}
