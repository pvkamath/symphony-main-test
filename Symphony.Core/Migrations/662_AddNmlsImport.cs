﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration662 : Migration
    {
        public override void Up()
        {
            TableSchema.Table table1 = CreateTableWithKey("NmlsMessages", "ID");
            table1.AddLongText("Body", false, "''");
            table1.AddLongText("Subject", false, "''");
            table1.AddLongText("From", false, "''");
            table1.AddLongText("To", false, "''");
            AddSubSonicStateColumns(table1);

            TableSchema.Table table = CreateTableWithKey("NmlsImports", "ID");
            table.AddColumn("NmlsMessageID", DbType.Int32, 0, false, "0");
            table.AddColumn("ConfirmationID", DbType.String, 64, false, "''");
            table.AddColumn("CourseID", DbType.String, 64, false, "''");
            table.AddColumn("CourseName", DbType.String, 512, false, "''");
            table.AddColumn("CompletionDate", DbType.String, 64, false, "''");
            table.AddColumn("Hours", DbType.Int32, 0, false, "0");
            table.AddColumn("Location", DbType.String, 512, false, "''");
            table.AddColumn("StudentName", DbType.String, 256, false, "''");
            table.AddColumn("MLOID", DbType.String, 64, false, "''");
            table.AddColumn("Fee", DbType.Int32, 0, false, "0");

            table.AddColumn("UserID", DbType.Int32, 0, false, "0");
            table.AddColumn("TrainingProgramID", DbType.Int32, 0, false, "0");
            
            table.AddColumn("ImportStatus", DbType.Int32, 0, false, "0");
            table.AddColumn("ImportError", DbType.String, 512, false, "''");
            AddSubSonicStateColumns(table);
        }
    }
}
