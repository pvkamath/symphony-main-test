﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration755 : Migration
    {

        public override void Up()
        {
            Execute(@"
                update Templates
                    set Body = REPLACE(Body, '{!Assignment.Score}%', '{!Assignment.Percent}')
                    where Body like '%{!Assignment.Score}[%]%'
            ");
        }

    }
}