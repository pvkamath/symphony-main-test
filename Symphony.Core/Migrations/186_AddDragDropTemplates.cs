﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration186 : Migration
    {
        public override void Up()
        {
            Execute(@"
declare @id1 int
set @id1 = (SELECT top 1 ID FROM ArtisanQuestionTypes WHERE Name = 'Matching')

declare @id2 int
set @id2 = (SELECT top 1 ID FROM ArtisanQuestionTypes WHERE Name = 'CategoryMatching')
                INSERT INTO [Symphony].[dbo].[ArtisanTemplates]
                       ([CustomerID]
                       ,[Name]
                       ,[Description]
                       ,[Html]
                       ,[ThumbPath]
                       ,[PageType]
                       ,[QuestionType]
                       ,[CssText])
                 VALUES
                       (0
                       ,'Matching'
                       ,'Matching question template with drag and drop interaction.'
                       ,'<div id=""Main"" class=""Main"">    <div class=""MainWrapper"">      <div id=""Content"" class=""Content"">        <div class=""ContentWrapper""></div>      </div>    </div>  </div>'
                       ,'/images/artisan/templates/matching_icon.gif'
                       ,2
                       ,@id1
                       ,'')
                
                INSERT INTO [Symphony].[dbo].[ArtisanTemplates]
                       ([CustomerID]
                       ,[Name]
                       ,[Description]
                       ,[Html]
                       ,[ThumbPath]
                       ,[PageType]
                       ,[QuestionType]
                       ,[CssText])
                 VALUES
                       (0
                       ,'Category Matching'
                       ,'Category matching question template with drag and drop interaction.'
                       ,'<div id=""Main"" class=""Main"">    <div class=""MainWrapper"">      <div id=""Content"" class=""Content"">        <div class=""ContentWrapper""></div>      </div>    </div>  </div>'
                       ,'/images/artisan/templates/cat_matching_icon.gif'
                       ,2
                       ,@id2
                       ,'')
            ");
        }
    }
}