﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration384 : Migration
    {
        public override void Up()
        {
            AddColumn("SalesforcePartner", "LocationID", DbType.Int32, 0, true);
            AddColumn("SalesforcePartner", "CustomerID", DbType.Int32, 0, true);
            AddColumn("Location", "SalesforcePartnerCode", DbType.String, 128, true);
        }
    }
}