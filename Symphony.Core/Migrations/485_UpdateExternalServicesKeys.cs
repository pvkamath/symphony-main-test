﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration485 : Migration
    {
        public override void Up()
        {
            Execute(@"declare @TableName sysname = 'CustomerExternalSystem'

declare @PrimaryKeyName sysname = (
    select name
    from sys.key_constraints
    where type = 'PK' and parent_object_id = object_id(@TableName))

execute ('alter table ' + @TableName + ' drop constraint ' + @PrimaryKeyName) ;");

            Execute(@"Alter Table CustomerExternalSystem ADD
CONSTRAINT [PK_CustomerExternalSystem_1] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[ExternalSystemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

ALTER TABLE [dbo].[CustomerExternalSystem]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerExternalSystem_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([ID])
NOT FOR REPLICATION 
");

            Execute(@"ALTER TABLE [dbo].[CustomerExternalSystem] NOCHECK CONSTRAINT [FK_CustomerExternalSystem_Customer]");

            Execute(@"ALTER TABLE [dbo].[CustomerExternalSystem]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerExternalSystem_ExternalSystem] FOREIGN KEY([ExternalSystemID])
REFERENCES [dbo].[ExternalSystem] ([ID])
NOT FOR REPLICATION 
");

            Execute(@"ALTER TABLE [dbo].[CustomerExternalSystem] NOCHECK CONSTRAINT [FK_CustomerExternalSystem_ExternalSystem]");
        }
    }
}