﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration215 : Migration
    {

        public override void Up()
        {
            Execute(@"ALTER PROCEDURE [dbo].[GetUserTilesForApp] (@userId int, @applicationId int = 0, @tileId int = 0 )
AS
BEGIN
	WITH Assignments as (SELECT distinct
		htl.TileID, 
		htl.HierarchyNodeID, 
		htl.HierarchyTypeID
		FROM HierarchyToTileLinks htl 
		JOIN (
			select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
			union
			select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
			union
			select f.ID, f.TypeID from useraudience 
				cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
			where userid = @userid
			union
			select @userid, 4
		) as H on h.id = htl.HierarchyNodeID and h.TypeID = htl.HierarchyTypeID
	),
		UserAppCred as (
			select app.ID
				 , app.Name
				 , app.AuthenticationUri
				 , app.PassUserNamePass
				 , cred.ID as CredentialsId
				 , case 
						when app.PassUserNamePass = 0 then 'none'
						when app.PassUserNamePass = 1 and cred.ID is null then 'missing'
						else 'complete'
					end as CredentialState
				 , cred.UserName as CredentialsUserName
				 , cred.Password as CredentialsPassword
				 , @userId as UserID
				from Applications as app
					left join (select Credentials.ID
				 , Credentials.ApplicationID
				 , Credentials.UserName
				 , Credentials.Password
				 , @userId as userId
				from Credentials
				right join [User] as usr on usr.ID = Credentials.UserID
			where usr.ID = @userId) as cred on app.ID = cred.ApplicationID
		)

	select distinct
		 Tiles.ID 
		 , assign.TileID
		 , Tiles.Name
		 , Tiles.TileUri
		 , app.ID as ApplicationId
		 , app.Name as ApplicationName
		 , app.PassUserNamePass
		 , app.CredentialState
		 , app.CredentialsId
		 , app.CredentialsUserName
		 , app.CredentialsPassword
		 , @userId as UserID
		 , cast(case when (select count(*) from Assignments as au join  Tiles as tl on tl.ID = au.TileID where tl.id = Tiles.id and au.HierarchyTypeID = 1) > 0 then 1 else 0 end as bit) as IsAssignedByLocation
		 , cast(case when (select count(*) from Assignments as au join  Tiles as tl on tl.ID = au.TileID where tl.id = Tiles.id and au.HierarchyTypeID = 2) > 0 then 1 else 0 end as bit) as IsAssignedByJobRole
		 , cast(case when (select count(*) from Assignments as au join  Tiles as tl on tl.ID = au.TileID where tl.id = Tiles.id and au.HierarchyTypeID = 3) > 0 then 1 else 0 end as bit) as IsAssignedByAudience
		 , cast(case when (select count(*) from Assignments as au join  Tiles as tl on tl.ID = au.TileID where tl.id = Tiles.id and au.HierarchyTypeID = 4) > 0 then 1 else 0 end as bit) as IsAssignedByUser

	from Tiles
		join UserAppCred as app
			on app.ID = tiles.ApplicationID
		left join Assignments as assign on assign.TileID = Tiles.ID
	where assign.TileID is not null 
		and (
				0 = @applicationID
			or	Tiles.ApplicationID = @applicationID
		)	
		and (
				0 = @tileId
			or  Tiles.ID = @tileId
		)
END");
        }

        public override void Down()
        {
            Execute(@" ALTER PROCEDURE [dbo].[GetUserTilesForApp] (@userId int, @applicationId int)
AS
BEGIN
	WITH Assignments as (SELECT distinct
		htl.TileID, 
		htl.HierarchyNodeID, 
		htl.HierarchyTypeID
		FROM HierarchyToTileLinks htl 
		JOIN (
			select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
			union
			select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
			union
			select f.ID, f.TypeID from useraudience 
				cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
			where userid = @userid
			union
			select @userid, 4
		) as H on h.id = htl.HierarchyNodeID and h.TypeID = htl.HierarchyTypeID
	),
		UserAppCred as (
			select app.ID
				 , app.Name
				 , app.AuthenticationUri
				 , app.PassUserNamePass
				 , cred.ID as CredentialsId
				 , case 
						when app.PassUserNamePass = 0 then 'none'
						when app.PassUserNamePass = 1 and cred.ID is null then 'missing'
						else 'complete'
					end as CredentialState
				 , cred.UserName as CredentialsUserName
				 , cred.Password as CredentialsPassword
				 , @userId as UserID
				from Applications as app
					left join (select Credentials.ID
				 , Credentials.ApplicationID
				 , Credentials.UserName
				 , Credentials.Password
				 , @userId as userId
				from Credentials
				right join [User] as usr on usr.ID = Credentials.UserID
			where usr.ID = @userId) as cred on app.ID = cred.ApplicationID
		)

	select distinct
		 Tiles.ID 
		 , assign.TileID
		 , Tiles.Name
		 , Tiles.TileUri
		 , app.ID as ApplicationId
		 , app.Name as ApplicationName
		 , app.PassUserNamePass
		 , app.CredentialState
		 , app.CredentialsId
		 , app.CredentialsUserName
		 , app.CredentialsPassword
		 , @userId as UserID
		 , cast(case when (select count(*) from Assignments as au join  Tiles as tl on tl.ID = au.TileID where tl.id = Tiles.id and au.HierarchyTypeID = 1) > 0 then 1 else 0 end as bit) as IsAssignedByLocation
		 , cast(case when (select count(*) from Assignments as au join  Tiles as tl on tl.ID = au.TileID where tl.id = Tiles.id and au.HierarchyTypeID = 2) > 0 then 1 else 0 end as bit) as IsAssignedByJobRole
		 , cast(case when (select count(*) from Assignments as au join  Tiles as tl on tl.ID = au.TileID where tl.id = Tiles.id and au.HierarchyTypeID = 3) > 0 then 1 else 0 end as bit) as IsAssignedByAudience
		 , cast(case when (select count(*) from Assignments as au join  Tiles as tl on tl.ID = au.TileID where tl.id = Tiles.id and au.HierarchyTypeID = 4) > 0 then 1 else 0 end as bit) as IsAssignedByUser

	from Tiles
		join UserAppCred as app
			on app.ID = tiles.ApplicationID
		left join Assignments as assign on assign.TileID = Tiles.ID
	where assign.TileID is not null 
		and (
				0 = @applicationID
			or	Tiles.ApplicationID = @applicationID
		)	
END");

        }
    }
}