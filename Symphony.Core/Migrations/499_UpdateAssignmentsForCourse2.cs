﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration499 : Migration
    {
        // Adds count of assignments completed, and assignments marked
        public override void Up()
        {
            Execute(@"ALTER VIEW [dbo].[AssignmentsForCourse] AS
-- Rolls up status of each assignment for the user so we can easily determine if the assignments for a particular course have been completed by a user
select 
	asgnLastAttempt.CourseID,
	asgnLastAttempt.UserID,
	asgnLastAttempt.TrainingProgramID,
	asgnLastAttempt.AssignmentsAttempted,
	asgnLastAttempt.AssignmentsInCourse,
    sum(asgnLastAttempt.IsMarked) as AssignmentsMarked,
	sum(case when asgnLastAttempt.Score >= asgnLastAttempt.PassingScore and asgnLastAttempt.Score > 0 then 1 else 0 end) as AssignmentsCompleted,
	(
		case 
			when 
				sum(asgnLastAttempt.IsMarked) = asgnLastAttempt.AssignmentsInCourse and 
				sum(case when asgnLastAttempt.Score >= asgnLastAttempt.PassingScore and asgnLastAttempt.Score > 0 then 1 else 0 end) < asgnLastAttempt.AssignmentsInCourse 
			then 
				1 -- Number of marked assignments is equal to number of assignments, but number of passed assignmnets < total assignments
			else 
				0 -- Either not all assignments have been marked yet, or all assignmnets have been marked and they have been passed (in which case IsAssignmentsPassed will be 1)
		end
	) as IsAssignmentsFailed,
    (
		case 
			when 
				sum(case when asgnLastAttempt.Score >= asgnLastAttempt.PassingScore and asgnLastAttempt.Score > 0 then 1 else 0 end) < asgnLastAttempt.AssignmentsInCourse 
			then 
				0 -- Number of passed assignments is less than number of assignments in course
			else 
				1 -- number of passed assignments is equal to the number of assignments in course
		end
	) as IsAssignmentsComplete
from 
	AssignmentsLastAttempt asgnLastAttempt
group by
	asgnLastAttempt.CourseID,
	asgnLastAttempt.TrainingProgramID,
	asgnLastAttempt.UserID,
	asgnLastAttempt.AssignmentsAttempted,
	asgnLastAttempt.AssignmentsInCourse
;;

");


        }

    }
}