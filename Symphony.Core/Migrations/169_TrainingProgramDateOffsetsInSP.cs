﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration169 : Migration
    {
        public override void Up()
        {
            Execute(@"
alter procedure [dbo].[GetTrainingProgramsForUser](@userId int, @newHire bit, @searchText nvarchar(50), @orderBy nvarchar(50), @orderDir varchar(4), @pageIndex int, @pageSize int)
as
begin
	set @orderDir = lower(@orderDir);
	set @orderBy = lower(@orderBy);
	-- create a date time that represents today without the time
	declare @today datetime
	set @today = (select cast(floor(cast(getdate() as decimal(12, 5))) as datetime));
	declare @hireDate datetime
	set @hireDate = (select HireDate from [user] where id = @userId);
	declare @newHireDuration int

	set @newHireDuration = (select NewHireDuration from Customer where ID = (select CustomerID from [user] where id = @userId));
	
	
	-- get all the tp details
	with temp as(
		select
			ID, CustomerID, OwnerID, Name, InternalCode, Cost, Description, 
			IsNewHire, IsLive, EnforceRequiredOrder, 
			MinimumElectives, FinalAssessmentCourseID, FinalAssessmentCourseTypeID, 
			ModifiedBy, CreatedBy, ModifiedOn, CreatedOn,
			NewHireOffsetEnabled, NewHireStartDateOffset, NewHireDueDateOffset,NewHireEndDateOffset,
						
			case when @newHire = 1 
				then dateadd(d, 
						case when NewHireOffsetEnabled = 1 
							then NewHireEndDateOffset 
							else @newHireDuration
						end, @hireDate) 
				else EndDate end as EndDate,
				
			case when @newHire = 1 
				then dateadd(d, 
						case when NewHireOffsetEnabled = 1 
							then NewHireStartDateOffset 
							else 0
						end, @hireDate) 
				else StartDate end as StartDate,
			
			case when @newHire = 1 
				then dateadd(d, 
						case when NewHireOffsetEnabled = 1 
							then NewHireDueDateOffset 
							else @newHireDuration
						end, @hireDate) 
				else DueDate end as DueDate,
			
			T.CourseCount,
			ROW_NUMBER() over (order by TrainingProgramID) as MyRow
		from
			TrainingProgram tp
		join
			(
				-- make sure we only ever see 1 instance of a given tp
				select
					distinct(X.TrainingProgramID) as TrainingProgramID, 
					(count(distinct cl.ID)) as CourseCount
				from
					(
						-- get the assigned tps
						select
							l.TrainingProgramID, 
							tp.Name,
							tp.MinimumElectives
						from
							hierarchytotrainingprogramlink l
						join
							(

								select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
								
								union
								
								select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
								
								union
								
								select f.ID, f.TypeID from useraudience 
								cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
								where userid = @userid
								
								union
								
								select
									@userId, 4 -- 'user' type id
							) as H
						on
							h.TypeID = l.HierarchyTypeID
							and
							h.ID = l.HierarchyNodeID
						join
							TrainingProgram tp
						on
							tp.ID = l.TrainingProgramID
						group by
							l.trainingprogramid, tp.name, tp.MinimumElectives
							
					) as X
				left join
					dbo.TrainingProgramToCourseLink cl
				on
					X.TrainingProgramID = cl.TrainingProgramID
				group by
					X.TrainingProgramID, X.MinimumElectives
			)
		as T
		on
			tp.ID = T.TrainingProgramID
		where
			Name like '%' + @searchText + '%'
		and
			IsNewHire = @newHire
		and 
			IsLive = 1
	)
	select
		ID, CustomerID, OwnerID, Name, InternalCode, Cost, Description, IsNewHire, IsLive, 
		StartDate as StartDate,
		EndDate as EndDate,
		-- new hire end date is the due date
		DueDate as DueDate,
		EnforceRequiredOrder, MinimumElectives, FinalAssessmentCourseID, 
		FinalAssessmentCourseTypeID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn,
		CourseCount,
		(select max(myrow) from temp) as TotalSize
	from
		temp
	where
		myrow between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize + 1)
	and
		EndDate >= @today 
	and 
		StartDate <= @today
	group by
		ID, CustomerID, OwnerID, Name, InternalCode, Cost, Description, 
		IsNewHire, IsLive, StartDate, EndDate, DueDate, EnforceRequiredOrder, 
		MinimumElectives, FinalAssessmentCourseID, FinalAssessmentCourseTypeID, 
		ModifiedBy, CreatedBy, ModifiedOn, CreatedOn,	CourseCount
	order by
		case when @orderBy = 'Name' and @orderDir = 'asc' then Name end,
		case when @orderBy = 'Name' and @orderDir = 'desc' then Name end desc,
		case when @orderBy = 'Description' and @orderDir = 'asc' then [Description] end,
		case when @orderBy = 'Description' and @orderDir = 'desc' then [Description] end desc,
		case when @orderBy = 'StartDate' and @orderDir = 'asc' then StartDate end,
		case when @orderBy = 'StartDate' and @orderDir = 'desc' then StartDate end desc,
		case when @orderBy = 'EndDate' and @orderDir = 'asc' then EndDate end,
		case when @orderBy = 'EndDate' and @orderDir = 'desc' then EndDate end desc,
		case when @orderBy = 'DueDate' and @orderDir = 'asc' then DueDate end,
		case when @orderBy = 'DueDate' and @orderDir = 'desc' then DueDate end desc,
		case when 1 = 1 then StartDate end
	
end;
");
        }
    }
}