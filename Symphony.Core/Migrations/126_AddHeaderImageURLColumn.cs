﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration126 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "HeaderImageURL", DbType.String, Int32.MaxValue, true);
        }
    }
}