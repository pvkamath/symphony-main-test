﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration008 : Migration
    {
        public override void Up()
        {
	    Execute("update	customer set customer.OrganizerSeats = l.OrganizerCount from GTMLicense l join Customer c on l.CustomerID = c.ID");
            Execute("drop table GTMLicense");
            Execute("alter table Registration add WebinarRegistrationID nvarchar(30) not null default ''");
        }
    }
}