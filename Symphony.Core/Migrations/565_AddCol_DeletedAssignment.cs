﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration565 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER TABLE dbo.OnlineCourseAssignments
ADD IsReset BIT NULL
");
 
        }
    }
}
