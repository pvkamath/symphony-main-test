﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration885 : Migration
    {

        public override void Up()
        {
            Execute(@"
CREATE FUNCTION [dbo].[fGetUserDataFieldXml]
(
	@userID int
)
RETURNS nvarchar(max)
AS
BEGIN
	return ( 
	    select
            udf.ID,
            udf.CustomerID,
            udf.Xtype,
            udf.Config,
            udf.CodeName,
            udf.DisplayName,
            udf.CategoryCodeName,
            udf.CategoryDisplayName,
            udfv.UserValue,
            case when udf.CustomerID = 0 then 1 else 0 end as IsGlobal,
            udf.Validator
        from
            UserDataField udf with (nolock)
        inner join
            UserDataFieldUserMap udfv on udfv.UserDataFieldID = udf.ID
		where
			udfv.UserID = @userID
        for xml path ('UserDataField'))
END
");
        }
    }
}
