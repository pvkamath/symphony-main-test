﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration504 : Migration
    {

        public override void Up()
        {
            // Retrieves responses for an assignment with forced grading.

            Execute(@"
ALTER PROCEDURE [dbo].[GetAssignmentWithForcedStatus] (
	@userId int,
	@courseID int,
	@trainingProgramID int,
	@responseStatus int,
	@isCorrect int,
	@response as nvarchar(max),
	@feedback as nvarchar(max)
) AS
BEGIN
select
	coalesce(oa1.ID, 0) as ID,
	coalesce(oa1.UserID, @userID) as UserID,
	coalesce(oa1.TrainingProgramID, @trainingProgramID) as TrainingProgramID, 
	coalesce(oa1.CourseID, @courseID) as CourseID, 
	coalesce(oa1.PageID, sp1.PageID) as PageID,
    sa.SectionID as SectionID,
	sa.Attempt as Attempt,
	coalesce(oa1.Date, getutcdate()) as Date,
	@responseStatus as ResponseStatus,
	@isCorrect as IsCorrect,
	coalesce(oa1.Response, @response) as Response,
	coalesce(oa1.InstructorFeedback, @feedback) as InstructorFeedback,
    cast(coalesce(oa1.IsAutoResponse, 1) as bit) as IsAutoResponse
from
(
	select 
		max(oa.Attempt) as Attempt,
		sp.SectionID
	from OnlineCourseAssignments oa
	inner join ArtisanPages p on p.ID = oa.PageID
	inner join ArtisanSectionPages sp on p.ID = sp.PageID
	where oa.TrainingProgramID = @trainingProgramID
	  and oa.UserID = @userID
	  and oa.CourseID = @courseID
	group by sp.SectionID
) sa
inner join ArtisanSectionPages sp1 on sp1.SectionID = sa.SectionID
left join OnlineCourseAssignments oa1 on oa1.UserID = @userID and oa1.CourseID = @courseID and oa1.TrainingProgramID = @trainingProgramID and oa1.PageID = sp1.PageID and oa1.Attempt = sa.Attempt
END;
");
        }

    }
}
