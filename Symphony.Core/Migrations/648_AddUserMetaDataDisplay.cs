﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration648 : Migration
    {
        public override void Up()
        {
            RemoveColumn("UserMetaData", "Key");
            AddColumn("UserMetaData", "Display", DbType.String, 256, false, "''");
        }
    }
}
