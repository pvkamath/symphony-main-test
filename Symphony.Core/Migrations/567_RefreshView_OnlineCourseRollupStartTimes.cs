﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration567 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER VIEW [dbo].OnlineCourseRollupStartTimes
AS

SELECT
	r.ID,
	r.UserID,
	r.CourseID,
	r.TrainingProgramID,
	r.Score,
	r.TotalSeconds,
	r.Success,
	r.Completion,
	r.ModifiedOn,
	r.ModifiedBy,
	r.AttemptCount,
	r.HighScoreDate,
	r.OriginalRegistrationID,
	r.CreatedByUserId,
	r.ModifiedByUserId,
	r.CreatedByActualUserId,
	r.ModifiedByActualUserId,
	r.TestAttemptCount,
	r.ResetNotes,
	r.LastResetDate,
	r.CanMoveForwardIndicator,
	r.NavigationPercentage,
	cst.StartTime,
	tl.ExpiresAfterMinutes,
	tl.ExpiresAfterMinutes - DATEDIFF(MINUTE, cst.StartTime, GETDATE()) AS Remaining
	FROM
		OnlineCourseRollup r
		LEFT JOIN CourseStartTime cst
			ON r.CourseID = cst.OnlineCourseID
			AND r.TrainingProgramID = cst.TrainingProgramID
			AND r.UserID = cst.UserID
		LEFT JOIN TrainingProgramToCourseLink tl
			ON r.TrainingProgramID = tl.TrainingProgramID
			AND r.CourseID = tl.CourseID


");
        }
    }
}
