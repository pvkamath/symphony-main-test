﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration286 : Migration
    {
        // Includes the CanMoveForwardIndicator from OnlineCourseRollup and Registrations
        // which is used by the training program popup in the portal to prevent progression
        // through the training program if the instructor has not flagged the student as 
        // being able to move forward.
        public override void Up()
        {
            Execute(@"ALTER procedure [dbo].[GetTranscript](@userId int)
                    as
                    begin

	                    select
		                    oc.ID as CourseID,
		                    oc.[Name] as CourseName,
		                    2 as CourseTypeID,
		                    0 as ClassID,
		                    0 as RegistrationID,
		                    0 as RegistrationStatusID,
		                    cast (0 as bit) as AttendedIndicator,
		                    case 
			                    when Success is null then 'Unknown'
			                    when Success = 0 then 'Failed'
			                    when Success = 1 then 'Passed'
		                    end
		                    as PassOrFail,
		                    Success as Passed,
		                    case 
			                    when Completion is null then 'Unknown'
			                    when Completion = 0 then 'Incomplete'
			                    when Completion = 1 then 'Completed'
		                    end
		                    as CompleteOrIncomplete,
		                    Completion as Completed,
		                    0 as OnlineTestID,
		                    0 as SurveyID,
		                    oc.PublicIndicator as IsPublic,
		                    cast(floor(round(cro.Score,0)) as nvarchar(50)) as Score, 
		                    coalesce(HighScoreDate, cro.Modifiedon, null) as AttemptDate,
		                    cro.TotalSeconds as AttemptTime,
		                    cro.AttemptCount as AttemptCount,
		                    null as StartDate,
		                    coalesce(HighScoreDate, cro.Modifiedon, null) as EndDate,
		                    cro.TrainingProgramID,
		                    '' as WebinarRegistrationID,
		                    '' as WebinarKey,
		                    cro.[CanMoveForwardIndicator] as CanMoveForwardIndicator
	                    from
		                    OnlineCourseRollup as cro 
	                    left outer join                  
		                    onlineCourse as oc 
	                    on
		                    oc.ID = cro.CourseID
	                    where
		                    cro.UserID = @userID
	                    and
		                    -- there shouldn't be any rollups without courses, but you never know...
		                    oc.ID is not null

	                    UNION

	                    select
		                    CourseID,
		                    CourseName,
		                    1 as CourseTypeID,
		                    ClassID,
		                    RegistrationID,
		                    RegistrationStatusID,
		                    cast(AttendedIndicator as bit) as AttendedIndicator,
		                    case 
			                    when Passed = 0 and ScoreReported = 1 then 'Failed'
			                    when Passed = 1 and ScoreReported = 1 then 'Passed'
			                    else 'Unknown'
		                    end as PassOrFail,
		                    Passed,
		                    case ScoreReported
			                    when 1 then 'Complete'
			                    else 'Incomplete'
		                    end as CompleteOrIncomplete,
		                    ScoreReported as Completed,
		                    OnlineTestID,
		                    SurveyID,
		                    PublicIndicator,
		                    Score,
		                    AttemptDate,
		                    AttemptTime,
		                    AttemptCount,
		                    StartDate,
		                    EndDate,
		                    0 as TrainingProgramID,
		                    WebinarRegistrationID,
		                    WebinarKey,
		                    CanMoveForwardIndicator
	                    from
	                    (
		                    select
			                    cCourse.ID as CourseID,
			                    cCourse.[Name] as CourseName,
			                    cClass.ID as ClassID,
			                    cClass.WebinarKey as WebinarKey,
			                    cReg.ID as RegistrationID, 
			                    cReg.RegistrationStatusID as RegistrationStatusID,
			                    cReg.AttendedIndicator as AttendedIndicator,
			                    cReg.WebinarRegistrationID as WebinarRegistrationID,
			                    -- Whether or not the user has passed changes based on course type and completion type.
			                    -- This is where minimum passing scores are set for classroom courses.

                    -- Online courses have passing scores on a per-course basis, and the 'passed' flag will be set as appropriate.
			                    case
				                    when cCourseCT.CodeName = 'attendance' and cReg.AttendedIndicator = 1 
					                    then 1
				                    when cCourseCT.CodeName = 'attendance'
					                    then 0

                    when cCourseCT.CodeName = 'numeric' and ( len(cReg.Score) = 3 or cReg.Score >= cast(isnull(cPass.PassingScore, 70) as varchar(15)) )
					                    then 1
				                    when cCourseCT.CodeName = 'numeric' 
					                    then 0
				                    when cCourseCT.CodeName = 'letter' and (cReg.Score not in ('A','B','C','D'))
					                    then 0 
				                    when cCourseCT.CodeName = 'letter' 
					                    then 1 
				                    when cCourseCT.CodeName = 'onlinecourse' AND ocr.Success = 1
					                    then 1 
				                    when cCourseCT.CodeName = 'onlinecourse' 
					                    then 0 
			                    end as Passed,
			                    -- To determine if a user has attended a course with attendance based passing,

                    -- we have a bit of a conundrum - there's no way to know if they DIDN'T attend, since the default value for
                    -- attended is 0. Therefore, (and per BE's instructions) we assume that if the registration was modified after the 
                    -- last day of the class, then whatever indicator is in the db was the 'reported' value. So, if the date modified is
                    -- after the last day of the class, or the AttendedIndicator is true, the instructor reported a score.
                    -- For online courses, the 'Complete' flag will be set to '2' once a score has been sent to the server.
			                    case 

                    when cCourseCT.CodeName = 'attendance' and (cReg.ModifiedOn > vClassDMM.MaxStartDateTime or cReg.AttendedIndicator = 1) 
					                    then 1 
				                    when cCourseCT.CodeName = 'attendance' 
					                    then 0 
				                    when cCourseCT.CodeName = 'onlinecourse' and (ocr.Completion = 1 or ocr.Score > 0)
					                    then 1 
				                    when cCourseCT.CodeName = 'onlinecourse'
					                    then 0 
				                    when cReg.Score = '' 
					                    then 0
				                    else 1
			                    end as ScoreReported,
			                    cCourse.OnlineCourseID as OnlineTestID,
			                    cClass.SurveyID as SurveyID,
			                    cCourse.PublicIndicator as PublicIndicator,
			                    -- Calculate the score
			                    --    -if the score is based on attendance, they get 100 if they attended

                    --    -if the score is entered by the instructor, use that. Since this can be a letter or a number, we use nvarchar
			                    --    -if the score is based on taking an online course, use that score
			                    case 
				                    when cCourseCT.CodeName = 'attendance' and cReg.AttendedIndicator = 1
					                    then '100'
				                    when ocr.ID is null
					                    then cast(cReg.Score as nvarchar(50))
				                    else 
					                    cast(floor(round(ocr.Score,0)) as nvarchar(50)) 
			                    end as Score,
			                    null as AttemptDate,
			                    0 as AttemptTime,
			                    0 as AttemptCount,
			                    vClassDMM.MinStartDateTime as StartDate,
			                    vClassDMM.MaxStartDateTime as EndDate,
			                    cReg.CanMoveForwardIndicator as CanMoveForwardIndicator
		                    from         
			                    dbo.Registration as cReg 
		                    inner join
			                    dbo.[Status] as stat on cReg.RegistrationStatusID = stat.ID
		                    inner join
			                     dbo.Class as cClass ON cClass.ID = cReg.ClassID 
		                    inner join
			                    dbo.Course as cCourse ON cCourse.ID = cClass.CourseID 
		                    inner join
			                    dbo.CourseCompletionType as cCourseCT ON cCourseCT.ID = cCourse.CourseCompletionTypeID 
		                    inner join
			                    dbo.ClassDateMinMax as vClassDMM ON vClassDMM.ClassID = cClass.ID 
		                    left join
			                    dbo.PassingScore as cPass ON cPass.CustomerID = cClass.CustomerID 
		                    --left join
		                    --	dbo.[User] as cUser ON cUser.ID = cReg.RegistrantID 
		                    -- this join simply grabs the score from the user if they had an online course to take for this class
		                    left join
			                    dbo.OnlineCourseRollup as ocr ON 
				                    ocr.CourseID = cCourse.OnlineCourseID AND 
				                    ocr.UserID = @userid
		                    where
			                    cReg.RegistrantID = @userid
	                    ) as X
                    end;"); 
        }
    }
}