﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration787 : Migration
    {
        public override void Up()
        {
            RemoveColumn("Customer", "ThemeTypeID");
        }
    }
}
