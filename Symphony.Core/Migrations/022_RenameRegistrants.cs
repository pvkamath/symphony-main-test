﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration022 : Migration
    {
        public override void Up()
        {
            Execute("update [RecipientGroups] set DisplayName = 'All Registrants' where DisplayName = 'Registrants'");
        }
    }
}