﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration694 : Migration
    {
        public override void Up()
        {
            AddColumn("ReportQueues", "ErrorMessage", DbType.String, 1024, false, "''");
        }
    }
}
