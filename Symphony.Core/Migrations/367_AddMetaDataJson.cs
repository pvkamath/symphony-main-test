﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration367 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE TrainingProgram ADD MetaDataJson nvarchar(max) NULL");
            Execute("ALTER TABLE Course ADD MetaDataJson nvarchar(max) NULL");
            Execute("ALTER TABLE Class ADD MetaDataJson nvarchar(max) NULL");
            Execute("ALTER TABLE OnlineCourse ADD MetaDataJson nvarchar(max) NULL");
        }
    }
}