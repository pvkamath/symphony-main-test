﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration057 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(560);
            sb.AppendFormat(@"CREATE TABLE [dbo].[CourseCategories]({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[ID] [int] IDENTITY(1,1) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[CustomerID] [int] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[Name] [nvarchar](50) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[Description] [nvarchar](200) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[CreatedOn] [datetime] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[ModifiedOn] [datetime] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[CreatedBy] [nvarchar](50) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[ModifiedBy] [nvarchar](50) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@" CONSTRAINT [PK_CourseCategories] PRIMARY KEY CLUSTERED {0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[ID] ASC{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ");
            sb.AppendFormat(@"ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]{0}", Environment.NewLine);
            sb.AppendFormat(@") ON [PRIMARY]");
            Execute(sb.ToString());
        }
    }
}