﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration124 : Migration
    {
        public override void Up()
        {
            Execute(@"UPDATE ArtisanAssetTypes SET Template='<object classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"" codebase=""https://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0"" width=""100%"" height=""100%"" align=""top""><param name=""allowScriptAccess"" value=""sameDomain"" /><param name=""wmode"" value=""transparent""><param name=""movie"" value=""{path}"" /><param name=""quality"" value=""high"" /><param name=""bgcolor"" value=""#ffffff"" /><embed src=""{path}"" quality=""high"" bgcolor=""#ffffff"" width=""100%"" height=""100%"" name=""{element_id}"" align=""top"" allowScriptAccess=""sameDomain"" type=""application/x-shockwave-flash"" wmode=""transparent"" pluginspage=""http://www.macromedia.com/go/getflashplayer"" /></object>' WHERE Extensions = 'swf'

                    UPDATE ArtisanAssetTypes SET Template='<a href=""{path}"" id=""{id}"" target=""_blank"">{name}</a>' WHERE Extensions = 'pdf|doc|ppt|xls|docx|pptx|xlsx'");
        }
    }
}