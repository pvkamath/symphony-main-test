﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration875 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgramRollup", "PostValidationComplete", DbType.Boolean);
        }
    }
}
