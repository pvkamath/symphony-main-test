﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration886 : Migration
    {

        public override void Up()
        {
            Execute(@"
            ALTER VIEW [dbo].[FastSearchableUsers] as
                SELECT
	                dbo.fGetUserAudiences(u.ID) as Audiences,
	                l.Name as Location, 
	                j.Name as JobRole,
                    c.EnforcePasswordDays,
                    c.EnforcePasswordReset,
	                u.ID,
	                u.Username, 
	                u.CustomerID, 
	                u.SalesChannelID, 
	                u.LocationID, 
	                u.EmployeeNumber, 
	                u.FirstName, 
	                u.MiddleName, 
	                u.LastName, 
	                u.HireDate, 
	                u.NewHireIndicator, 
	                u.NewHireEndDate, 
	                u.Email, 
	                u.StatusID, 
	                u.JobRoleID, 
	                u.Notes,
	                u.LoginCounter, 
	                u.EncryptedSSN,
                    u.LastEnforcedPasswordReset,
                    u.IsAccountExec, 
                    u.IsCustomerCare, 
                    u.ModifiedBy, 
                    u.CreatedBy, 
                    u.ModifiedOn, 
                    u.CreatedOn, 
                    u.TelephoneNumber, 
                    u.SupervisorID, 
                    u.[Password], 
                    u.IsExternal, 
                    u.SecondaryLocationID, 
                    u.[Username]+' ' +u.[FirstName] +' ' + u.[MiddleName] +' ' + u.[LastName] + ' ' + COALESCE(u.NMLSNumber, '') + COALESCE(u.EmployeeNumber, '') as Search,
                    u.ReportingSupervisorID,
	                isnull(us.[Description],'') as Status,
	                u.FirstName + ' ' + u.LastName as FullName,
	                isnull(su.FirstName + ' ' + su.LastName,'') as Supervisor,
	                isnull(ssu.FirstName + ' ' + ssu.LastName,'') as SecondarySupervisor,
	                isnull(rsu.FirstName + ' ' + rsu.LastName,'') as ReportingSupervisor,
                    u.TimeZone,
                    u.IsCreatedBySalesforce,
                    u.mobile,
                    u.fax,
                    u.janrainUserUuid,
                    u.salesforceAccountId,
                    u.salesforceAccountName,
                    u.salesforceContactId,
                    u.DateOfBirth,
                    u.NmlsNumber,
	                u.IsAuthor,
	                u.AlternateEmail1,
	                u.AlternateEmail2,
	                u.AddressLine1,
	                u.AddressLine2,
	                u.City,
	                u.[State],
	                u.ZipCode,
                    u.HomePhone,
                    u.WorkPhone,
                    u.CellPhone,
                    u.OnlineStatus,
                    u.RedirectType,
                    u.LoginRedirect,
                    dbo.fGetUserDataFieldXml(u.ID) as UserDataFieldsXml,
                    u.PrimaryProfessionID,
                    u.SecondaryProfessionID,
                    pri_p.Name as PrimaryProfessionName,
                    sec_p.Name as SecondaryProfessionName,
                    u.AssociatedImageData
                from
	                [user] u WITH (NOLOCK)
                left join
	                [user] su WITH (NOLOCK)
                on
	                u.SupervisorID = su.ID
                left join
	                [user] rsu WITH (NOLOCK)
                on
	                u.ReportingSupervisorID = rsu.ID
                left join
	                [user] ssu WITH (NOLOCK)
                on
	                u.SecondarySupervisorID = ssu.ID
                left join
	                Customer c WITH (NOLOCK)
                on
	                c.ID = u.CustomerID
                left join
	                location l WITH (NOLOCK)
                on
	                l.ID = u.LocationID
                left join
	                jobrole j WITH (NOLOCK)
                on
	                j.ID = u.JobRoleID
                left join
	                UserStatus us WITH (NOLOCK)
                on
	                us.ID = u.StatusID
	            left join
					Profession pri_p with (nolock)
				on
					pri_p.ID = u.PrimaryProfessionID
				left join
					Profession sec_p with (nolock)
				on
					sec_p.ID = u.SecondaryProfessionID
");
        }
    }
}
