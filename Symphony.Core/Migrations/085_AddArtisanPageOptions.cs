﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration085 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanPages", "CalculateScore", DbType.Boolean, 0, false, "0");
        }
    }
}