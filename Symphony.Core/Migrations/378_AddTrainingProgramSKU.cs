﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration378 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgram", "Sku", DbType.String, 32, true);
        }
    }
}