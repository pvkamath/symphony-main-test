﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration837 : Migration
    {
        public override void Up()
        {
            AddColumn("SalesChannel", "Description", DbType.String);
            AddColumn("SalesChannel", "ThemeId", DbType.Int32);

            // not sure how to do a MAX via SubSonic...
            Execute(@"ALTER TABLE SalesChannel
                ADD Logo VARCHAR(max);"
            );
        }
    }
}
