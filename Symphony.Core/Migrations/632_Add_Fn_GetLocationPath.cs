﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration632 : Migration
    {
        public override void Up()
        {
            
            Execute(@"
CREATE FUNCTION [dbo].[fGetLocationPath]
(   
	@locationID int
)
RETURNS TABLE
AS
RETURN
(
WITH Location_CTE (ID, ParentLocationID, Level, LevelIndentText)
AS
(
-- Anchor member definition
    SELECT
            l.ID,
            l.ParentLocationID,
            0 as Level,
            CAST(l.Name as nvarchar(max)) as LevelIndentText
        FROM
            Location l
      WHERE
            l.ID = @locationID
    UNION ALL
-- Recursive member definition
    SELECT
            l.ID,
            l.ParentLocationID,
            ll.Level + 1,
            CAST(l.Name + ' > ' + LevelIndentText as nvarchar(max))
    FROM
            Location l
    INNER JOIN Location_CTE ll ON
            ll.ParentLocationID = l.ID
)
SELECT top 1
	  a.ID,
      a.Level,
      a.LevelIndentText
FROM
      Location_CTE a
order by
	a.Level desc
)


;
");
        }
    }
}
