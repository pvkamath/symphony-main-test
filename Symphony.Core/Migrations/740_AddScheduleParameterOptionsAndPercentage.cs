﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration740 : Migration
    {

        public override void Up()
        {
            AddColumn("ScheduleParameters", "ScheduleParameterOption", DbType.Int32, 0, false, "1");
            AddColumn("Templates", "Percentage", DbType.Int32, 0, true);
        }

    }
}