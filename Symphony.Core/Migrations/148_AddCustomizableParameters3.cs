﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration148 : Migration
    {
        public override void Up()
        {
            Execute("alter table OnlineCourse add PretestTestOutOverride bit null");
            Execute("alter table OnlineCourse add RetestMode int not null default 0");
            Execute("alter table OnlineCourse add Retries int null");
        }
    }
}