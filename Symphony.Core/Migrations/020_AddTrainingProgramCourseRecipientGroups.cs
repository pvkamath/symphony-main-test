﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration020 : Migration
    {
        public override void Up()
        {
            Execute("insert recipientgroups (selector, displayname, validfortype) values ('GetAssignedUsers()','Assigned Users','TrainingProgramToCourseLink')");
        }
    }
}