﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration283 : Migration
    {
        public override void Up()
        {
            Execute(@"alter table customer alter column modules nvarchar(200)");
        }
    }
}