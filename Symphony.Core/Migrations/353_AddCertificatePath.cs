﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration353 : Migration
    {
        public override void Up()
        {
            AddColumn("OnlineCourse", "CertificatePath", DbType.String, 260, true);
            AddColumn("Course", "CertificatePath", DbType.String, 260, true);
            AddColumn("TrainingProgram", "CertificatePath", DbType.String, 260, true);
        }
    }
}