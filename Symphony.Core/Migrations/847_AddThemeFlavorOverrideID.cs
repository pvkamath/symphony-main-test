﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration847 : Migration
    {
        public override void Up()
        {
            AddColumn("OnlineCourse", "ThemeFlavorOverrideID", DbType.Int32, 0, true);
            AddColumn("Customer", "ThemeFlavorOverrideID", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "ThemeFlavorOverrideID", DbType.Int32, 0, true);
        }
    }
}
