﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration653 : Migration
    {

        public override void Up()
        {
            Execute(@"
    update AffidavitFinalExam set
    AffidavitJSON = '{""width"":600,""height"":700,""items"":[{""html"":""<h1>Rules of Conduct for NMLS Approved Pre-Licensure (PE) and Continuing Education (CE) Courses</h1><p><em style=\""font-style: italic\"">The Secure and Fair Enforcement for Mortgage Licensing Act (SAFE Act), requires that state-licensed MLOs complete pre-licensing (PE) and continuing education (CE) courses as a condition to be licensed. The SAFE Act also  requires that all education completed as a condition for state licensure be NMLS approved. Since 2009 NMLS has  established course design, approval, and delivery standards which NMLS approved course providers are required to  meet. To further ensure students meet the education requirements of the SAFE Act, NMLS has established a Rules  of Conduct (ROC). The ROC, which have been approved by the NMLS Mortgage Testing & Education Board, and the  NMLS Policy Committee, both of which are comprised of state regulators, are intended to stress that NMLS  approved education be delivered and completed with integrity.</em></p><h1>Rules of Conduct</h1><p>As an individual completing either pre-licensure education (PE) or continuing education (CE), I agree to abide by the following rules of conduct:</p><ol style=\""list-style-type: decimal\""><li>I attest that I am the person who I say I am and that all my course registration information is  accurate.</li><li>I acknowledge that I will be required to show a current government issued form of identification prior to and during the course, and/or be required to answer questions that are intended to verify/validate my identity prior to, and during the course. </li><li>I understand that the SAFE Act and state laws require me to spend a specific amount of time in  specific subject areas. Accordingly, I will not attempt to circumvent the requirements of any  NMLS approved course.  </li><li>I will not divulge my login ID or password or other login credential(s) to another individual for any  online course. </li><li>I will not seek or attempt to seek outside assistance to complete the course. </li><li>I will not give or attempt to give assistance to any person who is registered to take an NMLS  approved pre-licensure or continuing education course. </li><li>I will not engage in any conduct that creates a disturbance or interferes with the administration  of the course or other students’ learning.  </li><li>I will not engage in any conduct that would be contrary to good character or reputation, or  engage in any behavior that would cause the public to believe that I would not operate in the  mortgage loan business lawfully, honestly or fairly. </li><li>I will not engage in any conduct that is dishonest, fraudulent, or would adversely impact the  integrity of the course(s) I am completing and the conditions for which I am seeking licensure or  renewal of licensure.</li></ol><p>I understand that NMLS approved course providers are not authorized by NMLS to grant exceptions to these rules  and that I alone am responsible for my conduct under these rules. I also understand that these rules are in  addition to whatever applicable rules my course provider may have. </p><p>I understand that the course provider or others may report any alleged violations to NMLS and that NMLS may  conduct an investigation into alleged violations and that it may report alleged violations to the state(s) in which I  am seeking licensure or maintain licenses, or to other states. </p><p>I further understand that the results of any investigation into my alleged violation(s) may subject me to disciplinary  actions by the state(s) or the State Regulatory Registry (SRR), including removal of any course from my NMLS  record, and/or denial or revocation of my license(s).</p>""},{""xtype"":""textfield"",""name"":""fullName"",""fieldLabel"":""Full Name"",""anchor"":""-10""},{""xtype"":""textfield"",""name"":""nmlsID"",""fieldLabel"":""NMLS ID (If Known)"",""anchor"":""-10""},{""layout"":{""align"":""middle"",""pack"":""center"",""type"":""hbox""},""items"":[{""xtype"":""button"",""cls"":""x-btn-large"",""text"":""I certify the statement above""}]}]}'
    where Name = 'NMLS Rules of Conduct'
");
        }

    }
}
