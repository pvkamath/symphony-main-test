﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration418 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "IsSinglePage", DbType.Boolean, 0, true);
        }
    }
}