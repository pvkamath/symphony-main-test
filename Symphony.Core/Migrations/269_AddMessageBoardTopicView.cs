﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration269 : Migration
    {
        public override void Up()
        {
     Execute(@"create view [dbo].[MessageBoardTopicsDetail] as
SELECT COUNT([dbo].[MessageBoardPost].[ID]) as 'Posts',
    MAX([dbo].[MessageBoardPost].[CreatedOn]) as 'LastPost',
    (SELECT TOP 1 [dbo].[MessageBoardPost].[CreatedBy]
        FROM [dbo].[MessageBoardPost]
        WHERE [dbo].[MessageBoardPost].[TopicID] = [dbo].[MessageBoardTopic].[ID]
        ORDER BY [dbo].[MessageBoardPost].[CreatedOn] DESC
        ) as 'LastPostBy',
    (SELECT TOP 1 [dbo].[MessageBoardPost].[Content]
        FROM [dbo].[MessageBoardPost]
        WHERE [dbo].[MessageBoardPost].[TopicID] = [dbo].[MessageBoardTopic].[ID]
        ORDER BY [dbo].[MessageBoardPost].[ID] ASC
        ) as 'Preview',
    [dbo].[MessageBoardTopic].[ID], 
    [dbo].[MessageBoardTopic].[MessageBoardID],
    [dbo].[MessageBoardTopic].[UserID],
    [dbo].[MessageBoardTopic].[Title],
    [dbo].[MessageBoardTopic].[IsSticky],
    [dbo].[MessageBoardTopic].[IsLocked],
    [dbo].[MessageBoardTopic].[CreatedOn],
    [dbo].[MessageBoardTopic].[ModifiedOn],
    [dbo].[MessageBoardTopic].[CreatedBy],
    [dbo].[MessageBoardTopic].[ModifiedBy]
        FROM [dbo].[MessageBoardTopic]
    LEFT OUTER JOIN [dbo].[MessageBoardPost] ON [dbo].[MessageBoardTopic].[ID] = [dbo].[MessageBoardPost].[TopicID]
    GROUP BY [dbo].[MessageBoardTopic].[ID],
        [dbo].[MessageBoardTopic].[MessageBoardID],
        [dbo].[MessageBoardTopic].[UserID],
        [dbo].[MessageBoardTopic].[Title],
        [dbo].[MessageBoardTopic].[IsSticky],
        [dbo].[MessageBoardTopic].[IsLocked],
        [dbo].[MessageBoardTopic].[CreatedOn],
        [dbo].[MessageBoardTopic].[ModifiedOn],
        [dbo].[MessageBoardTopic].[CreatedBy],
        [dbo].[MessageBoardTopic].[ModifiedBy]
");
            
        }
    }
}