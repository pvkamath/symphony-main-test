﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration388 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "EnforcePasswordReset", DbType.Boolean, 0, false, "0");
            AddColumn("Customer", "EnforcePasswordDays", DbType.Int32, 0, false, "30");
            AddColumn("User", "LastEnforcedPasswordReset", DbType.DateTime, 0, true);
            
            Execute(@"EXECUTE sp_refreshview N'CustomerDetails';");
        }
    }
}