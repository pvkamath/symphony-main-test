﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration422 : Migration
    {

        public override void Up()
        {
            AddColumn("User", "NmlsNumber", DbType.String, 32, true, null);
            AddColumn("TrainingProgram", "IsNmls", DbType.Boolean, 0, false, "0");
            
        }

    }
}