﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration084 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "CompletionMethod", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanCourses", "NavigationMethod", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanCourses", "DisplayTestOnly", DbType.Boolean, 0, false, "0");
            AddColumn("ArtisanCourses", "DisplayQuestionFeedback", DbType.Boolean, 0, false, "0");
            AddColumn("ArtisanCourses", "CertificateEnabled", DbType.Boolean, 0, false, "0");
            AddColumn("ArtisanCourses", "ContactEmail", DbType.String, 100, false, "''");
            AddColumn("ArtisanCourses", "Disclaimer", DbType.String, 9000, false, "''");
            AddColumn("ArtisanCourses", "PassingScore", DbType.Int32, 0, false, "0");
        }
    }
}