﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration218 : Migration
    {

        public override void Up()
        {
            Execute(@"alter PROCEDURE [dbo].[GetUserApplications] (@userId int)
AS
BEGIN
	WITH Assignments as (SELECT distinct
	    htl.TileID
	    FROM HierarchyToTileLinks htl 
	    JOIN (
		    select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
		    union
		    select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
		    union
		    select f.ID, f.TypeID from useraudience 
			    cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
		    where userid = @userid
		    union
		    select @userid, 4
	    ) as H on h.id = htl.HierarchyNodeID and h.TypeID = htl.HierarchyTypeID
    ),
	    UserAppCred as (
		    select app.ID
			     , app.Name
			     , app.AuthenticationUri
			     , app.PassUserNamePass
			     , cred.ID as CredentialId
			     , case 
					    when app.PassUserNamePass = 0 then 'none'
					    when app.PassUserNamePass = 1 and cred.ID is null then 'missing'
					    else 'complete'
				    end as CredentialState
			     , cred.UserName as CredentialUserName
			     , cred.Password as CredentialPassword
			     , @userId as UserID
			    from Applications as app
				    left join (select Credentials.ID
			     , Credentials.ApplicationID
			     , Credentials.UserName
			     , Credentials.Password
			     , @userId as userId
			    from Credentials
			    right join [User] as usr on usr.ID = Credentials.UserID
		    where usr.ID = @userId) as cred on app.ID = cred.ApplicationID
	    )

    select distinct
	     app.ID as ID
	     , app.Name as Name
	     , app.CredentialState
	     , app.CredentialId
	     , app.CredentialUserName
	     , app.CredentialPassword
	     , count(Tiles.ID) as TileCount
    from Tiles
	    join UserAppCred as app
		    on app.ID = tiles.ApplicationID
	    left join Assignments as assign on assign.TileID = Tiles.ID
    where assign.TileID is not null 
    group by app.ID	 , app.Name 
	     , app.CredentialState
	     , app.CredentialId
	     , app.CredentialUserName
	     , app.CredentialPassword
	
END");
        }

        public override void Down()
        {
            
        }
    }
}