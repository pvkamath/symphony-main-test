﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration245 : Migration
    {
        public override void Up()
        {
            AddColumn("ReportTypes", "CodeName", DbType.String, 50, true);
            RemoveColumn("ReportTypes", "Enum");
        }
    }
}