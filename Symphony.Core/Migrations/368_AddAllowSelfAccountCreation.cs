﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration368 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "AllowSelfAccountCreation", DbType.Boolean, 0, false, "0");
            Execute(@"EXECUTE sp_refreshview N'CustomerDetails';");
        }
    }
}