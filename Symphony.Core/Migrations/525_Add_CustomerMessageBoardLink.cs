using SubSonic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Symphony.Core.Migrations
{
    class Migration525 : Migration
    {

        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.CustomerMessageBoardLink
(
	Id INT IDENTITY(1, 1) NOT NULL,
	CustomerId INT,
	MessageBoardId INT
)
");

            Execute(@"
ALTER TABLE dbo.CustomerMessageBoardLink
ADD CONSTRAINT PK_CustomerMessageBoardLink__Id PRIMARY KEY (Id)			
");

            Execute(@"
ALTER TABLE dbo.CustomerMessageBoardLink
ADD CONSTRAINT FK_CustomerMessageBoardLink_Customers FOREIGN KEY (CustomerId) REFERENCES dbo.Customer(ID)
");

            Execute(@"
ALTER TABLE dbo.CustomerMessageBoardLink
ADD CONSTRAINT FK_CustomerMessageBoardLink_MessageBoard FOREIGN KEY (MessageBoardId) REFERENCES dbo.MessageBoard(ID)
");


		}
	}
}