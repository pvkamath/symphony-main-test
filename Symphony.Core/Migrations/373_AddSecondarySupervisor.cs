﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration373 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER TABLE dbo.[User] ADD
	                    SecondarySupervisorID int NOT NULL DEFAULT 0
                    
                    ALTER TABLE dbo.[User] WITH NOCHECK ADD CONSTRAINT
	                    FK_User_SecondarySupervisor FOREIGN KEY
	                    (
	                    SecondarySupervisorID
	                    ) REFERENCES dbo.[User]
	                    (
	                    ID
	                    ) ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION 
                    	
                    ALTER TABLE dbo.[User]
	                    NOCHECK CONSTRAINT FK_User_SecondarySupervisor");
        }
    }
}