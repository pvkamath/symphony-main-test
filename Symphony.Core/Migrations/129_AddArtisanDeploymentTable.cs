﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration129 : Migration
    {
        public override void Up()
        {
            TableSchema.Table table = CreateTableWithKey("ArtisanDeployment", "ID");
            table.AddColumn("Name", DbType.String, 64, false);
            table.AddColumn("CourseIdsText", DbType.String, 2048, false);
            table.AddColumn("CustomerIdsText", DbType.String, 2048, false);
            table.AddColumn("IsUpdate", DbType.Boolean, 0, false, "0");
            table.AddColumn("ToArtisan", DbType.Boolean, 0, false, "0");
            table.AddColumn("ToSymphony", DbType.Boolean, 0, false, "0");
            table.AddColumn("ToMars", DbType.Boolean, 0, false, "0");
            AddSubSonicStateColumns(table);
        }
    }
}