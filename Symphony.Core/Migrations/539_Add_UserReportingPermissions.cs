﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration539 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.UserReportingPermissionsType
(
	Id INT IDENTITY(1, 1) NOT NULL,
	Name VARCHAR(50),
    Code VARCHAR(50)
)
");

            Execute(@"
ALTER TABLE dbo.UserReportingPermissionsType
ADD CONSTRAINT PK_UserReportingPermissionsType__Id PRIMARY KEY (Id)
");

            Execute(@"
CREATE TABLE dbo.UserReportingPermissions
(
	Id INT IDENTITY(1, 1) NOT NULL,
	UserId INT NOT NULL,
	PermissionTypeId INT NOT NULL,
    ObjectId INT NOT NULL
)
");

            Execute(@"
ALTER TABLE dbo.UserReportingPermissions
ADD CONSTRAINT PK_UserReportingPermissions__Id PRIMARY KEY (Id)

");

            Execute(@"
ALTER TABLE dbo.UserReportingPermissions
ADD CONSTRAINT FK_UserReportingPermissions__User_Id FOREIGN KEY (UserId) REFERENCES dbo.[User](ID)
");

            Execute(@"
ALTER TABLE dbo.UserReportingPermissions
ADD CONSTRAINT FK_UserReportingPermissions__UserReportingPermissionsType_Id FOREIGN KEY (PermissionTypeId) REFERENCES dbo.[UserReportingPermissionsType](ID)
");

            Execute(@"
INSERT INTO dbo.UserReportingPermissionsType (Name, Code)
VALUES ('JobRole', 'jobrole');

INSERT INTO dbo.UserReportingPermissionsType (Name, Code)
VALUES ('Location', 'location');

INSERT INTO dbo.UserReportingPermissionsType (Name, Code)
VALUES ('Audience', 'audience');
");

        }
    }
}
