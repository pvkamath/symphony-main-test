﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration238 : Migration
    {
        public override void Up()
        {
            AddColumn("ReportJobs", "IsScheduled", DbType.Int32, 0, false, "0");
            AddColumn("ReportTypes", "Enum", DbType.Int32, 0, false, "0");
            AddColumn("ReportQueues", "JobId", DbType.Int32, 0, false, "0");
            // queueId is not needed in this table
            RemoveColumn("ReportJobs", "QueueID");
        }
    }
}