﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration088 : Migration
    {
        public override void Up()
        {
            string prefix = "<div id=\"Main\" class=\"Main\"><div class=\"MainWrapper\"><div id=\"Content\" class=\"Content\"><div class=\"ContentWrapper\">";
            string postfix = "</div></div></div></div>";

            Execute("update ArtisanTemplates set html = '" + prefix + "' + html + '" + postfix + "' where PageType = 1");
        }
    }
}