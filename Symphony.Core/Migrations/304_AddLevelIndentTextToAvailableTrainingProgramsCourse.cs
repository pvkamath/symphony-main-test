﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration304 : Migration
    {
        public override void Up()
        {
            // Adds LevelIndentText to available training program courses in order to
            // show the category in the training program course assignment
            Execute(@"ALTER view [dbo].[AvailableTrainingProgramCourses]
as
(
	SELECT 
		oc.ID, 
		oc.CustomerID, 
		oc.[Name],
		oc.[Description], 
		Cost AS Fee, 
		Credit, 
		PublicIndicator, 
		ct.ID AS CourseTypeID,
		oc.ModifiedBy, 
		oc.CreatedBy, 
		oc.ModifiedOn, 
		oc.CreatedOn,
		cast(0 as bit) as IsThirdParty,
		cast(oc.ID as varchar(20)) + '-' + cast(ct.ID as varchar(20)) as [Key],
		oc.ProctorRequiredIndicator as ProctorRequiredIndicator,
		ac.[Parameters] as [Parameters],
		ch.LevelIndentText as LevelIndentText
	FROM dbo.OnlineCourse as oc JOIN CourseType as ct ON ct.CodeName = 'online'
	LEFT JOIN dbo.ArtisanCourses ac on oc.ArtisanCourseID = ac.ID
	LEFT JOIN dbo.CategoryHierarchy ch on oc.CategoryID = ch.ID
	-- don't allow surveys to be directly assigned
	WHERE IsSurvey = 0
	UNION ALL
	SELECT 
		c.ID, 
		c.CustomerID, 
		c.Name, 
		c.[Description], 
		PerStudentFee AS Fee, 
		Credit, 
		PublicIndicator,
		ct.ID AS CourseTypeID, 
		c.ModifiedBy, 
		c.CreatedBy, 
		c.ModifiedOn, 
		c.CreatedOn,
		c.IsThirdParty,
		cast(c.ID as varchar(20)) + '-' + cast(ct.ID as varchar(20)) as [Key],
		null as ProctorRequiredIndicator,
		'' as [Parameters],
		ch.LevelIndentText as LevelIndentText
	FROM dbo.Course as c JOIN CourseType as ct ON ct.CodeName = 'classroom'
	LEFT JOIN dbo.CategoryHierarchy ch on c.CategoryID = ch.ID
	-- don't allow public classroom courses to be directly assigned
	WHERE PublicIndicator = 0
);");
        }
    }
}