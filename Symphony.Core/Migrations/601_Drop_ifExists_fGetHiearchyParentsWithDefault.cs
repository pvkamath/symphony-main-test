﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration601 : Migration
    {
        public override void Up()
        {
            Execute(@"
IF OBJECT_ID('dbo.fGetHierarchyParentsWithDefault') IS NOT NULL
    DROP FUNCTION fGetHierarchyParentsWithDefault");
        }
    }
}
