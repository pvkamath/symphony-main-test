﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration800 : Migration
    {
        public override void Up()
        {
            // Backfills based on assignments users have taken
            // This is the only way we can backfill the assignments table with
            // assignments for courses that may have been redeployed.
            Execute(@"
INSERT INTO AssignmentSummary
    (
        ArtisanCourseID,
        ArtisanSectionID,
        Name,
        NumQuestions,
        PassingScore,
        CreatedOn,
        ModifiedOn,
        CreatedBy,
        ModifiedBy
    )
SELECT
	sp.CourseID as ArtisanCourseID,
	sp.SectionID as ArtisanSectionID,
	s.Name,
	count(distinct p.ID) as NumQuestions,
	coalesce(nullif(oc.passingScoreOverride, 0), nullif(s.passingScore, 0), ac.PassingScore) as PassingScore,
    max(oc.CreatedOn) as CreatedOn,
	max(oc.ModifiedOn) as ModifiedOn,
	ac.CreatedBy,
	ac.ModifiedBy
FROM 
    OnlineCourseAssignments a
JOIN
    (
        SELECT
            max(a.Attempt) as Attempt,
            UserID,
            TrainingProgramID,
            CourseID,
            PageID
        FROM
            OnlineCourseAssignments a
        GROUP BY
            UserID,
            TrainingProgramID,
            CourseID,
            PageID
    ) maxAttempt
    ON
        maxAttempt.UserID = a.UserID and
        maxAttempt.TrainingProgramID = a.TrainingProgramID and
        maxAttempt.CourseID = a.CourseID and
        maxAttempt.PageID = a.PageID and
        maxAttempt.Attempt = a.Attempt
JOIN
	OnlineCourse oc
ON
    -- Since surveys create duplicate online courses that both point to the same artisan courses
    -- it is possible for duplicate artisan/section ids to apear in this query. Excluding any
    -- duplicate from online courses as they will just point to the same artisan course anyway
	oc.ID = a.CourseID and oc.DuplicateFromID = 0 
JOIN
	ArtisanCourses ac
ON
	ac.ID = oc.PublishedArtisanCourseID
JOIN
	ArtisanSectionPages sp
ON
	sp.CourseID = ac.ID
JOIN
	ArtisanPages p
ON
	p.ID = sp.PageID and p.QuestionType = 8
JOIN
	ArtisanSections s
ON
	s.ID = sp.SectionID
GROUP BY
	sp.SectionID, sp.CourseID, s.Name, ac.PassingScore, ac.CreatedBy, ac.ModifiedBy, oc.PassingScoreOverride, ac.PassingScore, s.PassingScore");

        }
    }
}