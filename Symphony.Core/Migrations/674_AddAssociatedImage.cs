﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration674 : Migration
    {
        public override void Up()
        {
            Execute("alter table Customer add AssociatedImageData nvarchar(max)");
            Execute("alter table OnlineCourse add AssociatedImageData nvarchar(max)");
            Execute("alter table ArtisanCourses add AssociatedImageData nvarchar(max)");
            Execute("alter table [User] add AssociatedImageData nvarchar(max)");
            Execute("alter table TrainingProgram add AssociatedImageData nvarchar(max)");
            Execute("alter table Course add AssociatedImageData nvarchar(max)");
            Execute("alter table [Class] add AssociatedImageData nvarchar(max)");
            Execute("alter table Room add AssociatedImageData nvarchar(max)");
            Execute("alter table Venue add AssociatedImageData nvarchar(max)");
            Execute("alter table Library add AssociatedImageData nvarchar(max)");
            Execute("alter table Books add AssociatedImageData nvarchar(max)");
        }
    }
}
