﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration370 : Migration
    {
        public override void Up()
        {
            // This function had to be added at the server already
            // so dropping it here first if it exists. 
            // The next migration will create it. 

            Execute(@"
IF object_id('dbo.fGetCategoryHierarchyPath') IS NOT NULL
BEGIN 
    DROP FUNCTION dbo.fGetCategoryHierarchyPath
END");
        }
    }
}