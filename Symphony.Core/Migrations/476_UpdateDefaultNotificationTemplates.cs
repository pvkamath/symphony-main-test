﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration476 : Migration
    {

        public override void Up()
        {
            Execute(@"
    UPDATE Templates SET Body = 'Hi {!User.Firstname},

Your instructor for the course ""{!OnlineCourse.Name}"" in the training program ""{!TrainingProgram.Name}"" has graded your assignment.

You scored {!Assignment.Score}% ({!Assignment.NumCorrect} out of {!Assignment.NumQuestions}).

Please log in to Symphony to review your assignment and the instructor feedback.',
                        Subject = 'The assignment ""{!Assignment.Name}"" has been graded'
where Templates.CodeName = 'AssignmentGraded'");

            Execute(@"
    UPDATE Templates SET Body = 'Hi {!User.FirstName},
An instructor has given you permission to move forward from the course ""{!Course.Name}"". You may now log into Symphony and continue with your training.',
                        Subject = 'Permission to move forward from ""{!Course.Name}"" granted'
where Templates.CodeName = 'InstructorPermissionGranted'");
        }

    }
}
