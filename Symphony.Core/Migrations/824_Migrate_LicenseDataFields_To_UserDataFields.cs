﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration824 : Migration
    {
        /// <summary>
        /// Copies over the existing LicenseDataFieldsToUsersMap rows to the UserDataFieldUserMap table. 
        /// Any UserDataFieldUserMap fields that exist only in the UserDataFieldUserMap table and not the LicenseDataFieldsToUsersMap
        /// will be moved to the end of UserDataFieldUserMap in order to keep the ids consistant that have been in use.
        /// </summary>
        public override void Up()
        {
            Execute(@"
                -- Update any matching values in UserDataFieldUserMap with newer values in LicenseDataFieldsToUsersMap if they exist
                update UserDataFieldUserMap
	                set CreatedOn = l.CreatedOn,
		                ModifiedOn = l.ModifiedOn,
		                UserValue = l.UserValue
                from 
	                UserDataFieldUserMap m
                join 
	                LicenseDataFieldsToUsersMap l
	                on
	                l.UserID = m.UserID and l.LicenseDataFieldID = m.UserDataFieldID
                where 
	                l.UserValue != m.UserValue
	                and l.ModifiedOn > m.ModifiedOn

                -- Create table for field info in UserDataFieldUserMap
                create table #tempDataFields 
                (
	                ID int,
	                UserID int,
	                UserDataFieldID int,
	                UserValue nvarchar(1024),
	                CreatedOn datetime,
	                ModifiedOn datetime,
	                CreatedBy nvarchar(64),
	                ModifiedBy nvarchar(64)
                )
                -- Copy any new data in UserDataFieldMap to the temp table.
                insert into #tempDataFields (ID, UserID, UserDataFieldID, UserValue, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy)
                select 
                    m.ID,
	                m.UserID, 
	                m.UserDataFieldID,
	                m.UserValue,
	                m.CreatedOn,
	                m.ModifiedOn,
	                m.CreatedBy,
	                m.ModifiedBy
                from LicenseDataFieldsToUsersMap l
                left join 
	                UserDataFieldUserMap m on m.ID = l.ID
                where 
	                (m.UserID != l.UserID) 
	                or (m.UserDataFieldID != l.LicenseDataFieldID)

                -- Delete the user data fields in the UserDataFieldMap table (We will add them back in after)
                -- This allows all the LicenseDataFieldMap IDs to match up with UserDataFieldUserMaps
                delete from UserDataFieldUserMap where ID in 
                (
	                select ID from #tempDataFields
                )

                -- Copy over all the new records in LicenseDataFields, retaining the ids.
                set identity_insert [dbo].[UserDataFieldUserMap] on
                insert into UserDataFieldUserMap (ID, UserID, UserDataFieldID, UserValue, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy)
                select
	                l.ID,
	                l.UserID, 
	                l.LicenseDataFieldID,
	                l.UserValue,
	                l.CreatedOn,
	                l.ModifiedOn,
	                l.CreatedBy,
	                l.ModifiedBy
                from LicenseDataFieldsToUsersMap l
                where l.ID > (
	                select max(m.ID) from UserDataFieldUserMap m
                )
                set identity_insert [dbo].[UserDataFieldUserMap] off

                -- Add the temporary records back in
                insert into UserDataFieldUserMap (UserID, UserDataFieldID, UserValue, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy)
                select
	                t.UserID, 
	                t.UserDataFieldID,
	                t.UserValue,
	                t.CreatedOn,
	                t.ModifiedOn,
	                t.CreatedBy,
	                t.ModifiedBy
                from #tempDataFields t

                if(object_id('tempdb..#tempDataFields') is not null)
                begin
                    drop table #tempDataFields
                end
");
        }
    }
}
