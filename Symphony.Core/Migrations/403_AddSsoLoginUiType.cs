﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration403 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "SsoLoginUiType", DbType.Int32, 0, true, "1");
            Execute(@"EXECUTE sp_refreshview N'CustomerDetails';");
        }
    }
}