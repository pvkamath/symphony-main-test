﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration293 : Migration
    {
        public override void Up()
        {
            Execute(@"-- TODO: exclude null course ids? this means (at least for classroom courses) that the course has no scheduled dates yet

ALTER view [dbo].[TrainingProgramCoursesAndClasses] as
-- This query will give us a list of all the courses that fall within a given training program.
-- Use this in conjunction with a query to give all the users that have the training program assigned to them
-- to determine the total list of courses a user must take.

--the outer select is simply to coalesce the online and classroom courses into a single set of columns
select
	ROW_NUMBER() OVER (ORDER BY TrainingProgramID, ClassStartDate) AS ID, 
	CustomerID,
	TrainingProgramID,
	TrainingProgramName,
	SyllabusTypeID,
	ActiveAfterDate,
	ProgramStartDate,
	ProgramDueDate,
	ProgramEndDate,
	coalesce(ClassroomCourseID, OnlineCourseID, null) as CourseID,
	CourseTypeID,
	coalesce(ClassroomCourseName, OnlineCourseName, null) as CourseName,
	coalesce(ClassStartDate, null) as ClassStartDate,
	coalesce(ClassEndDate, null) as ClassEndDate,
	ClassID,
	EnforceRequiredSortOrder,
	coalesce(SortOrder,0) as SortOrder
from
	(
	-- first, get the courses for the training program
	select
		link.CourseTypeID,
		link.TrainingProgramID,
		link.SyllabusTypeID,
		link.ActiveAfterDate as ActiveAfterDate, 
		program.CustomerID as CustomerID,
		program.[Name] as TrainingProgramName,
		program.StartDate as ProgramStartDate,
		program.DueDate as ProgramDueDate,
		program.EndDate as ProgramEndDate,
		classroomCourse.CourseID as ClassroomCourseID,
		classroomCourse.[Name] as ClassroomCourseName,
		classroomCourse.StartDate as ClassStartDate,
		classroomCourse.StartDate as ClassEndDate,
		classroomCourse.ClassID as ClassID,
		onlineCourse.ID as OnlineCourseID,
		onlineCourse.[Name] as OnlineCourseName,
		program.EnforceRequiredOrder as EnforceRequiredSortOrder,
		link.RequiredSyllabusOrder as SortOrder
	from
		TrainingProgram program
	join
		TrainingProgramToCourseLink link
	on
		program.ID = link.TrainingProgramID
	-- second, find the details for the courses
	left join
		dbo.OnlineCourse onlineCourse
	on
		onlineCourse.ID = link.CourseID and link.CourseTypeID = 2
	left join
		(
			-- join only to classroom courses that have scheduled dates
			-- this way we can filter them to the training program
			select
				Course.[Name], 
				Course.ID as CourseID,
				Class.ID as ClassID,
				ClassDate.StartDateTime as StartDate
			from
				dbo.Course as Course
			left join
				dbo.Class as Class
			on
				Course.ID = Class.CourseID
			left join
				dbo.ClassDate as ClassDate
			on
				Class.ID = ClassDate.ClassID
			group by
				Course.[Name],
				Course.ID,
				Class.ID,
				ClassDate.StartDateTime
		) as classroomCourse
	on
		classroomCourse.CourseID = link.CourseID and link.CourseTypeID = 1
	-- make sure we exclude training programs that haven't been entered completely yet
	--where
	--	program.StartDate != '1900/01/01'
	--and
	--	program.EndDate != '1900/01/01'
	-- and make sure we only include training programs that are enabled
	and
		program.IsLive = 1

) as Schedule

where
	-- filter classroom courses by the training program date
	(
		CourseTypeID = 1 
	)
	-- online courses always come in
	or
	(
		CourseTypeID = 2
	);");
        }
    }
}
