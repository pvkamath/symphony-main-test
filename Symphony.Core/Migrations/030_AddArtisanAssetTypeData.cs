﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration030 : Migration
    {
        public override void Up()
        {
            Execute("insert into [ArtisanAssetTypes] ([Name], [Description], [Extensions], [Template]) VALUES ('Image', 'A computer-basic graphic.', 'png|jpeg|jpg|gif|bmp', '')");
            Execute("insert into [ArtisanAssetTypes] ([Name], [Description], [Extensions], [Template]) VALUES ('Flash', 'A Flash-based element.', 'swf', '')");
            Execute("insert into [ArtisanAssetTypes] ([Name], [Description], [Extensions], [Template]) VALUES ('HTML', 'Html or text-based content.', 'htm|html|txt', '')");
            Execute("insert into [ArtisanAssetTypes] ([Name], [Description], [Extensions], [Template]) VALUES ('Document', 'A downloadable PDF, Word document, PowerPoint presentation, or Excel spreadsheet.', 'pdf|doc|ppt|xls|docx|pptx|xlsx', '')");
        }
    }
}