﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration266 : Migration
    {
        public override void Up()
        {
        // Based off of ClassroomClassDetails view
        // Adds additional fields and registration counts
        // Keeping this seperate to avoid grabbing the extra
        // data when it is not needed elsewhere in symphony
        // when using this specific view.
     Execute(@"create view [dbo].[SalesforceClass] as
select
    cClass.ID as ID, 
	cCourse.ID as CourseID, 
	cClass.[Name] as ClassName, 
    cClass.[LockedIndicator] as LockedIndicator,
	cCourse.[Name] as CourseName, 
	cVenue.[Name] as VenueName, 
    cVenue.[City] as VenueCity,
    cState.[Description] as StateName,
    cState.[Name] as StateAbbreviation,
	cClass.Description as ClassDescription, 
	cRoom.[Name] as ClassRoomName, 
	cClass.WebinarKey, 
	vClassDMM.MinStartDateTime as MinClassDate, 	
	isnull(replace(replace((
		select     
			cUser.FirstName + '+' + cUser.LastName as [data()]
		from         
			dbo.ClassInstructors as cClassI 
		inner join
            dbo.[User] as cUser 
		on 
			cUser.ID = cClassI.InstructorID
        where     
			cClassI.ClassID = cClass.ID 
		for xml path('')), ' ', ', '), '+', ' '), '')
	as ClassInstructors, 
	isnull(replace((
		select     
			cClassD.StartDateTime as [data()]
        from         
			dbo.ClassDate as cClassD
        where     
			(cClassD.ClassID = cClass.ID)
        order by 
			StartDateTime ASC 
		for xml path('')), ' ', ', '), '') 
	as ClassDates,
    (
        select count(r.ID) as registrationCount from dbo.Registration r
        where r.ClassID = cClass.ID
        and r.RegistrationStatusID = 4
    ) as CurrentEnrollment,
    cClass.[CapacityOverride] as CapacityOverride,
    COALESCE((
        select sum(res.Cost) as cost
        from
            dbo.ClassResources as cr
        inner join
            dbo.Resource as res
        on 
            res.ID = cr.ResourceID
        where
            cr.ClassID = cClass.ID
        ), 0) + COALESCE(cRoom.[RoomCost], 0) as TotalCost,
    (
        select sum(cd.Duration) as classDuration
        from
            dbo.ClassDate as cd
        where
            cd.ClassID = cClass.ID
    ) as TotalDuration
	from
		dbo.Class as cClass 
	inner join
		dbo.Course as cCourse 
	on 
		cCourse.ID = cClass.CourseID 
	left join
		dbo.Room as cRoom 
	on 
		cRoom.ID = cClass.RoomID 
	left join
		dbo.Venue as cVenue 
	on 
		cVenue.ID = cRoom.VenueID 
    left join
        dbo.States as cState
    on
        cState.ID = cVenue.StateID
	inner join
		dbo.ClassDate as cClassD 
	on 
		cClassD.ClassID = cClass.ID 
	inner join
		dbo.ClassDateMinMax as vClassDMM 
	on 
		vClassDMM.ClassID = cClass.ID 
		and 
		vClassDMM.MinStartDateTime = cClassD.StartDateTime
");
            
        }
    }
}