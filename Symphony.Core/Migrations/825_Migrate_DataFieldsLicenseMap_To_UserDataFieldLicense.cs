﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration825 : Migration
    {
        /// <summary>
        /// Copy over the new rows from LicenseDataFieldsToLicenseMap to UserDataFieldLicenseMap
        /// </summary>
        public override void Up()
        {
            Execute(@"
                set identity_insert [dbo].[UserDataFieldLicenseMap] on
                   
                    insert into UserDataFieldLicenseMap (ID, LicenseID, UserDataFieldID, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy)
                    select
                        l.ID,
                        l.LicenseID, 
                        l.LicenseDataFieldID,
                        l.CreatedOn,
                        l.ModifiedOn,
                        l.CreatedBy,
                        l.ModifiedBy
                    from LicenseDataFieldsToLicensesMap l
                    where l.ID > (
                    	select max(m.ID) from UserDataFieldLicenseMap m
                    )

              set identity_insert [dbo].[UserDataFieldLicenseMap] off

              -- Clear out any duplicate mappings
                ;WITH CTE AS(
	                SELECT *, RN = ROW_NUMBER()OVER(PARTITION BY LicenseID, UserDataFieldID ORDER BY ModifiedOn Desc)
	                FROM UserDataFieldLicenseMap
                )
                DELETE FROM CTE WHERE RN > 1
");
        }
    }
}
