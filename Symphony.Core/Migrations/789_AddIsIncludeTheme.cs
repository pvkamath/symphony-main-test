﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration789 : Migration
    {
        public override void Up()
        {
            AddColumn("ServiceProvider", "IsDisableTheme", DbType.Boolean, 0, false, "0");
        }
    }
}
