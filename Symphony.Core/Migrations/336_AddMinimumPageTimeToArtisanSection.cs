﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration336: Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "MinimumPageTime", DbType.Int32, 0, true);
            AddColumn("ArtisanSections", "MaximumQuestionTime", DbType.Int32, 0, true);
        }
    }
}