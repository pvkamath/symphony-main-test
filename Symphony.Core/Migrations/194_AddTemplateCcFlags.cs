﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration194 : Migration
    {
        public override void Up()
        {
            AddColumn("Templates", "CcClassroomSupervisors", DbType.Boolean, 0, false, "0");
            AddColumn("Templates", "CcReportingSupervisors", DbType.Boolean, 0, false, "0");
        }
    }
}