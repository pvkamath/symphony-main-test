﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration576 : Migration
    {
        public override void Up()
        {
            Execute(@"

ALTER TABLE dbo.ReportQueues
ADD ResultDataLength INT NULL

ALTER TABLE dbo.ReportQueues
ADD ResultRowCount INT NULL

");
        }
    }
}
