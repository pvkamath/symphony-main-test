﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration528 : Migration
    {

        public override void Up()
        {
            Execute(@"
create function [dbo].[fGetHierarchyNodeIdsFromXML](@xmlString xml, @hierarchyName nvarchar(40)) 
returns @hierarchyIds TABLE (
	HierarchyNodeId int
) 
as
begin
	declare @hierarchyTypeId int;
	
	select @hierarchyTypeid = id from HierarchyType where CodeName = @hierarchyName;

	insert 
		@hierarchyIds
	select
		ID
	from 
		fSplit(
			(
				select
					HierarchyIds
				from
					fParseHierarchyXML(@xmlString)
				where
					HierarchyName = @hierarchyName
			), ','
		)
	cross apply fGetHierarchyBranch(item, @hierarchyTypeId)
	where
		item > 0
	
	return
end
");
        }

    }
}
