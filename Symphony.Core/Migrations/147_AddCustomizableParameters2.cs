﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration147 : Migration
    {
        public override void Up()
        {
            Execute("alter table OnlineCourse add NavigationMethodOverride int null");
            Execute("alter table OnlineCourse add DisclaimerOverride nvarchar(50) null ");
            Execute("alter table OnlineCourse add ContactEmailOverride nvarchar(50) null");
            Execute("alter table OnlineCourse add ShowObjectivesOverride bit null");
        }
    }
}