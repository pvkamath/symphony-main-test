﻿using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration717 : Migration
    {
        public override void Up()
        {
            Execute("CREATE UNIQUE INDEX ux_OnlineCourseAccreditatino_ArtisanCourseAccreditationID_OnlineCourseID ON OnlineCourseAccreditation (ArtisanCourseAccreditationID, OnlineCourseID)");
        }
    }
}