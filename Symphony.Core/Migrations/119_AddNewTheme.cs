﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration119 : Migration
    {
        public override void Up()
        {
            Execute(@"UPDATE [SYMPHONY].[dbo].[ArtisanThemes]
                       SET [Name] = 'BE Default'
                          ,[Description] = 'BankersEdge Default Theme'
                          ,[CssPath] = '/skins/artisan_content/be_default/css/main.css'
                          ,[Folder] = '/skins/artisan_content/be_default/css'
                     WHERE ID = 3;
                    
                    INSERT [dbo].[ArtisanThemes] (
                        [Name], 
                        [Description], 
                        [CssPath], 
                        [CreatedBy], 
                        [ModifiedBy], 
                        [CustomerID], 
                        [Folder]) 
                    VALUES (N'BE Smooth', 
                            N'BankersEdge Smooth Theme', 
                            N'/skins/artisan_content/be_smooth/css/main.css', 
                            N'system', 
                            N'system', 
                            0, 
                            N'/skins/artisan_content/be_smooth/css');");
        }
    }
}