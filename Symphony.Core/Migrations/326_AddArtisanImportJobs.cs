﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration326: Migration
    {
        public override void Up()
        {
            TableSchema.Table importJob = CreateTableWithKey("ArtisanImportJob", "ID");
            importJob.AddColumn("Course", DbType.String, 512, true);
            importJob.AddColumn("Section", DbType.String, 512, true);
            importJob.AddColumn("CoursesProcessed", DbType.Int32, 0, true);
            importJob.AddColumn("SectionsProcessed", DbType.Int32, 0, true);
            importJob.AddColumn("PagesProcessed", DbType.Int32, 0, true);
            importJob.AddColumn("Running", DbType.Boolean, 0, true);
            importJob.AddColumn("CourseID", DbType.String, 128, true);
            AddSubSonicStateColumns(importJob);
        }
    }
}