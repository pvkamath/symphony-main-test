﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration669 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "DefaultProtocol", DbType.String, 8, false, "''");
            AddColumn("Customer", "DefaultAuthority", DbType.String, 128, false, "''");
        }
    }
}
