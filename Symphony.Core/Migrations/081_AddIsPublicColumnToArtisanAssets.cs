﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration081 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanAssets", "IsPublic", DbType.Boolean, 0, false, "0");
        }
    }
}