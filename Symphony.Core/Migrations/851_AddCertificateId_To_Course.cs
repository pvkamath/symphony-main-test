﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration851 : Migration
    {

        public override void Up()
        {
            AddColumn("Course", "CertificateTemplateId", DbType.Int32);
        }
    }
}
