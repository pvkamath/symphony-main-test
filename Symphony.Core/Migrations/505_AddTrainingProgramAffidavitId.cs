﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration505 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgram", "TrainingProgramAffidavitID", DbType.Int32, 0, true);
        }
    }
}