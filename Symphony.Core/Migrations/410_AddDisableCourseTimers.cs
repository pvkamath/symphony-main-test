﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration410 : Migration
    {
        public override void Up()
        {
           // Adds flag to training programs to disable all course timers for any artisan course in the training program
            AddColumn("TrainingProgram", "IsCourseTimingDisabled", DbType.Boolean, 0, false, "0");
        }
    }
}