﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration531 : Migration
    {

        public override void Up()
        {
            Execute(@"
    
        update ReportEntity set Description = 'Allows filtering the report data by the specified Audiences' where ID = 1
        update ReportEntity set Description = 'Allows specifying a class date for the report' where ID = 2
        update ReportEntity set Description = 'Allows specifying a classroom for the report' where ID = 3
        update ReportEntity set Description = 'Held: Class has occured. Not Held: Class is scheduled in the future. All: All classes' where ID = 4
        update ReportEntity set Description = 'Search for classes that have specific users registered' where ID = 5
        update ReportEntity set Description = 'Select courses that have a due date within the specified range' where ID = 6
        update ReportEntity set Description = 'Choose between either public courses, or training program courses only' where ID = 7
        update ReportEntity set Description = 'Student status in the course, either Passed, Failed, or All' where ID = 8
        update ReportEntity set Description = 'Use only specific courses for the report' where ID = 9
        update ReportEntity set Description = 'Either classroom, online, or both' where ID = 10
        update ReportEntity set Description = 'Either classroom, online, or both when assigned as a required course in a training program' where ID = 11
        update ReportEntity set Description = 'A generic date range field' where ID = 12
        update ReportEntity set Description = 'Group data by one of the available hierarchies' where ID = 13
        update ReportEntity set Description = 'Group data by a specific hierarchy' where ID = 14
        update ReportEntity set Description = 'Filter data by hire date of the user' where ID = 15
        update ReportEntity set Description = 'Filter courses by specific instructors' where ID = 16
        update ReportEntity set Description = 'Filter users by job role' where ID = 17
        update ReportEntity set Description = 'Filter users by location' where ID = 18
        update ReportEntity set Description = 'Filter data by the students pass date of the course' where ID = 19
        update ReportEntity set Description = 'Select a permission level to filter users by' where ID = 20
        update ReportEntity set Description = 'Allows specifying a set of users to use for the report' where ID = 21
        update ReportEntity set Description = 'Allows specifying user status - Active, Inactive, LOA, Deleted, Pending' where ID = 22
        update ReportEntity set Description = 'Select students with a particular set of supervisors' where ID = 23
        update ReportEntity set Description = 'Date in which a test was completed' where ID = 24
        update ReportEntity set Description = 'Date range for a training program start date' where ID = 25
        update ReportEntity set Description = 'A set of training programs to report on' where ID = 26
        update ReportEntity set Description = '' where ID = 27
        update ReportEntity set Description = 'Customers to load data from' where ID = 28


");


        }

    }
}
