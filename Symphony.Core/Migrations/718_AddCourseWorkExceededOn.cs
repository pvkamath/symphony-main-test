﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration718 : Migration
    {

        public override void Up()
        {
            AddColumn("OnlineCourseRollup", "CourseWorkExceededOn", DbType.DateTime, 0, true);
        }

    }
}
