﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration790 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "IsRandomizeQuizQuestions", DbType.Boolean, 0, false, "1");
        }
    }
}