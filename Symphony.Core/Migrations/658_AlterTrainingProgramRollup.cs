﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration658 : Migration
    {

        public override void Up()
        {
            AddColumn("TrainingProgramRollup", "FinalExamScore", DbType.String, 10, true);
            AddColumn("TrainingProgramRollup", "IsRequiredComplete", DbType.Boolean, 0, true);
            AddColumn("TrainingProgramRollup", "IsElectiveComplete", DbType.Boolean, 0, true);
            AddColumn("TrainingProgramRollup", "IsFinalComplete", DbType.Boolean, 0, true);
            AddColumn("TrainingProgramRollup", "IsAssignmentsComplete", DbType.Boolean, 0, true);
            AddColumn("TrainingProgramRollup", "IsComplete", DbType.Boolean, 0, true);

            AddColumn("TrainingProgramRollup", ReservedColumnName.CREATED_ON, DbType.DateTime, 0, false, "getdate()");
            AddColumn("TrainingProgramRollup", ReservedColumnName.MODIFIED_ON, DbType.DateTime, 0, false, "getdate()");
            AddColumn("TrainingProgramRollup", ReservedColumnName.CREATED_BY, DbType.String);
            AddColumn("TrainingProgramRollup", ReservedColumnName.MODIFIED_BY, DbType.String);
        }
    }
}
