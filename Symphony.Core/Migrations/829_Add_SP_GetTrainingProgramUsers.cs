﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration829 : Migration
    {
        /// <summary>
        /// Create an SP wrapper for the fGetTrainingProgramUsers function
        /// This will return the same data as the TrainingProgramUsers view
        /// but for one training program. 
        /// 
        /// This also incldues users assigned to child training programs
        /// </summary>
        public override void Up()
        {
            Execute(@"
                CREATE procedure [dbo].[GetTrainingProgramUsers] (@trainingProgramId int, @isNewHire bit = null)
                as
                begin
	                select 
		                1 as ID,
                        CustomerID, 
                        TrainingProgramID, 
                        TrainingProgramName, 
                        TrainingProgramStartDate, 
                        TrainingProgramDueDate, 
                        TrainingProgramEndDate, 
                        TrainingProgramDescription, 
                        EnforceRequiredOrder, 
                        IsNewHire, 
                        IsUserNewHire,
                        SourceNodeID, 
                        HierarchyTypeID, 
                        FirstName, 
                        LastName, 
                        UserID,
                        ((SELECT     COUNT(1)
                            FROM         TrainingProgramToCourseLink
                            WHERE     T .TrainingProgramID = TrainingProgramID AND SyllabusTypeID IN (1, 4)) + MinimumElectives) 
                        AS MinimumCourseCount, 
                        MinimumElectives,
                        ParentTrainingProgramID
	                from
		                fGetTrainingProgramUsers(@trainingProgramId, @isNewHire) T
	
end");
        }
    }
}
