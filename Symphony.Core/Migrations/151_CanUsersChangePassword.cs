﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration151 : Migration
    {
        public override void Up()
        {
            Execute("alter table Customer add CanUsersChangePassword bit not null default 0");
        }
    }
}