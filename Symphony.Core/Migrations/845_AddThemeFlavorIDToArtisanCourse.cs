﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration845 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "ThemeFlavorID", DbType.Int32, 0, true);
        }
    }
}
