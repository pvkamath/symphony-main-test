﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration607 : Migration
    {
        public override void Up()
        {

            AddColumn("OnlineCourseRollup", "CoreVersion", DbType.String, 64);
            AddColumn("OnlineCourseRollup", "ArtisanAuditID", DbType.Int32, 0);
            
        }
    }
}
