﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration797 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(432);
            sb.AppendFormat(@"DECLARE @application nvarchar(20){0}", Environment.NewLine);
            sb.AppendFormat(@"DECLARE @applications cursor{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"SET @applications = CURSOR FOR{0}", Environment.NewLine);
            sb.AppendFormat(@"     SELECT subdomain FROM customer{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"OPEN @applications{0}", Environment.NewLine);
            sb.AppendFormat(@" {0}", Environment.NewLine);
            sb.AppendFormat(@"FETCH NEXT FROM @applications INTO @application{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"WHILE (@@FETCH_STATUS = 0) {0}", Environment.NewLine);
            sb.AppendFormat(@"BEGIN{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}exec aspnet_Roles_CreateRole @application, 'Reporting - Audience Analyst'{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}FETCH NEXT FROM @applications INTO @application{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"END{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"CLOSE @applications{0}", Environment.NewLine);
            sb.AppendFormat(@"DEALLOCATE @applications{0}", Environment.NewLine);

            Execute(sb.ToString());
           
        }
    }
}