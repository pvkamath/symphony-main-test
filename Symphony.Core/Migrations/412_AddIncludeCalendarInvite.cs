﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration412 : Migration
    {

        public override void Up()
        {
            AddColumn("Templates", "IncludeCalendarInvite", DbType.Boolean, 0, false, "0");
        }

        public override void Down()
        {

        }
    }
}