﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration121 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanAssets", "IsGrouped", DbType.Boolean, 0, false, "0");
        }
    }
}