﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration221 : Migration
    {
        public override void Up()
        {
            /*Execute(@"exec('CREATE SCHEMA [MYREPORTSCHEMA] AUTHORIZATION [dbo]
create VIEW [dbo].[ReportRefreshStatuses]
AS
SELECT [ID]
      ,[StartedOn]
      ,[CompletedOn]
  FROM [SymphonyRptReplication].[dbo].[RptRefreshStatus]

create VIEW [dbo].[ReportQueueStatuses]
AS
SELECT [ID]
      ,[QueueID]
      ,[CustomerID]
      ,[StatusType]
      ,[ErrorType]
      ,[CompletedDate]
      ,[IsEnabled]
      ,[CreatedOn]
      ,[ModfiedOn]
  FROM [SymphonyRptReplication].[dbo].[RptQueueStatus]

create VIEW [dbo].[ReportUserToAudiences]
AS
SELECT [UserToAudienceKey]
      ,[UserKey] 
      ,[AudienceKey]
      ,[UserNumber] as UserID
      ,[UserName] 
      ,[CustomerNumber] as CustomerID
      ,[AudienceLevel]
      ,[AudienceLevelDetail]
  FROM [SymphonyRptReplication].[dbo].[UserToAudience]
create VIEW [dbo].[ReportUsers]
AS
SELECT [UserKey]
      ,[UserNumber] as UserID
      ,[FirstName]
      ,[MiddleName]
      ,[LastName]
      ,[UserName]
      ,[CustomerKey]
      ,[LocationKey]
      ,[JobRoleKey]
      ,[UserPermission]
      ,[UserPermissionsKey]
      ,[CustomerNumber] as CustomerID
      ,isnull([IsImported],0) as [IsImported]
  FROM [SymphonyRptReplication].[dbo].[User]

create VIEW [dbo].[ReportTrainingPrograms]
AS
SELECT [TrainingProgramKey]
      ,[TrainingProgramNumber] as TrainingProgramID
      ,[CustomerKey] 
      ,[CustomerNumber] as CustomerID
      ,[TrainingProgramName] as Name
      ,[IsCurrentIndicator]
      ,isnull([IsImported],0) as [IsImported]
  FROM [SymphonyRptReplication].[dbo].[TrainingProgram]
WHERE IsCurrentIndicator = 1

create VIEW [dbo].[ReportLocations]
AS
SELECT [LocationKey]
      ,[LocationName] as Name
      ,[CustomerKey]
      ,[LocationLevelDetail]
      ,[LocationNumber] as LocationID
      ,[ParentLocationNumber] as ParentLocationID
      ,[CustomerNumber] as CustomerID
  FROM [SymphonyRptReplication].[dbo].[Location]

CREATE VIEW [dbo].[ReportJobRoles]
AS
SELECT [JobRoleKey]
      ,[JobRoleName] as Name
      ,[CustomerKey]
      ,[JobRoleLevelDetail]
      ,[JobRoleNumber] as JobRoleID
      ,[ParentJobRoleNumber] as ParentJobRoleID
      ,[CustomerNumber] as CustomerID
  FROM [SymphonyRptReplication].[dbo].[JobRole]
create VIEW [dbo].[ReportCustomers]
AS
SELECT [CustomerKey]
      ,[CustomerNumber] as CustomerID
      ,[CustomerName] as Name
FROM [SymphonyRptReplication].[dbo].[Customer]

create View [dbo].[ReportCourses]
as
SELECT [CourseKey]
      ,[CourseNumber] as CourseID
      ,[CourseTypeKey] -- 1 as classroom, 2 as online
      ,[CourseName] as Name
      ,[CustomerKey]
      ,[ExternalCourseIdentifier]
      ,[ExternalCourseName]
      ,isnull([ThirdPartyIndicator],0) as [ThirdPartyIndicator]
      ,[CreatedBy] 
      ,[CreatedDateTime] as CreatedOn
      ,[ModifiedBy]
      ,[ModifiedDateTime] as ModifiedOn
      ,[CustomerNumber] as CustomerID
  FROM [SymphonyRptReplication].[dbo].[Course]

create View [dbo].[ReportAudiences]
as
SELECT [AudienceKey]
      ,[AudienceName] as Name
      ,[AudienceNumber] as AudienceID
      ,[InternalCode]
      ,[AudienceLevelDetail]
      ,[ParentAudienceNumber] as ParentID
      ,[SortOrder]
      ,[CustomerKey]
      ,[CreatedBy]
      ,[CreatedDateTime] as CreatedOn
      ,[ModifiedBy]
      ,[ModifiedDateTime] as ModifiedOn
  FROM [SymphonyRptReplication].[dbo].[Audience]
GO')");*/
        }
    }
}