﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration616 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE TABLE dbo.NetworkSharedTrainingPrograms
(
	Id INT IDENTITY(1,1) NOT NULL,
	CustomerNetworkDetailId INT,
	TrainingProgramId INT
)

ALTER TABLE dbo.NetworkSharedTrainingPrograms
ADD CONSTRAINT PK_NetworkSharedTrainingPrograms__Id PRIMARY KEY (Id)

ALTER TABLE dbo.NetworkSharedTrainingPrograms
ADD CONSTRAINT FK_NetworkSharedTrainingPrograms_CustomerNetworkDetail__DetailsId FOREIGN KEY (CustomerNetworkDetailId) REFERENCES dbo.CustomerNetworkDetail (DetaildId)

ALTER TABLE dbo.NetworkSharedTrainingPrograms
ADD CONSTRAINT FK_NetworkSharedTrainingPrograms_TrainingProgram__Id FOREIGN KEY (TrainingProgramId) REFERENCES dbo.TrainingProgram(ID)


");
        }
    }
}
