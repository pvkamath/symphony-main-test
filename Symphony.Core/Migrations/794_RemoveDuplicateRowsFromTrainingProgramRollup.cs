﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration794 : Migration
    {
        public override void Up()
        {
            Execute(@"
                WITH CTE AS(
	                SELECT *, RN = ROW_NUMBER()OVER(PARTITION BY TrainingProgramID, UserID ORDER BY TrainingProgramID, UserID)
	                FROM TrainingProgramRollup
                )
                DELETE FROM CTE WHERE RN > 1
            ");
        }
    }
}