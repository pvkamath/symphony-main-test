﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration526 : Migration
    {

        public override void Up()
        {
            Execute(@"Alter table ReportQueues alter column QueueCommand nvarchar(max) not null");
        }

    }
}
