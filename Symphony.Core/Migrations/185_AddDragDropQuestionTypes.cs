﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration185 : Migration
    {
        public override void Up()
        {
            Execute(@"
                INSERT INTO ArtisanQuestionTypes (Name,DisplayName) VALUES ('Matching', 'Matching')
                INSERT INTO ArtisanQuestionTypes (Name,DisplayName) VALUES ('CategoryMatching', 'Category Matching')
            ");
        }
    }
}