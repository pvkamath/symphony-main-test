﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration216 : Migration
    {

        public override void Up()
        {
            Execute(@"ALTER view [dbo].[TileApplication] as
	SELECT
	t.ID,
	t.Name,
	t.TileUri,
	t.ApplicationID,
	a.Name AS ApplicationName,
	a.AuthenticationUri,
	t.CreatedOn,
	t.CreatedBy,
	t.ModifiedOn,
	t.ModifiedBy
FROM		
	Tiles AS t 
	INNER JOIN
	Applications AS a
	ON a.ID = t.ApplicationID");

        }

        public override void Down()
        {
            Execute(@"ALTER view [dbo].[TileApplication] as
	SELECT
	t.ID,
	t.Name,
	t.TileUri,
	t.ApplicationID,
	a.Name AS ApplicationName,
	t.CreatedOn,
	t.CreatedBy,
	t.ModifiedOn,
	t.ModifiedBy
FROM		
	Tiles AS t 
	INNER JOIN
	Applications AS a
	ON a.ID = t.ApplicationID");
        }
    }
}