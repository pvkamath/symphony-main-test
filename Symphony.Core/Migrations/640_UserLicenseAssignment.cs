﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration640 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE VIEW dbo.UserLicenseAssignment
AS
SELECT 
la.LicenseID,
f.ID,
f.LastName,
f.FirstName,
la.StartDate,
la.ExpiryDate,
la.AssignmentStatusId,
ls.StatusName AS 'AssignmentStatus',
'' as 'ParentObject'
FROM 
	dbo.FastSearchableUsers f WITH (NOLOCK)
	INNER JOIN dbo.LicenseAssignments la WITH (NOLOCK)
		ON la.UserID = f.ID
	INNER JOIN dbo.LicenseStatus ls WITH (NOLOCK)
		ON ls.Id = la.AssignmentStatusId

UNION

SELECT
la.LicenseID,
f.ID,
f.LastName,
f.FirstName,
la.StartDate,
la.ExpiryDate,
la.AssignmentStatusId,
ls.StatusName AS 'AssignmentStatus',
'Place' AS 'ParentObject'
FROM
	dbo.FastSearchableUsers f WITH (NOLOCK)
	INNER JOIN dbo.LicenseAssignments la WITH (NOLOCK)
		ON la.LocationID = f.LocationID
	INNER JOIN dbo.LicenseStatus ls WITH (NOLOCK)
		ON ls.Id = la.AssignmentStatusId

UNION

SELECT
la.LicenseID,
f.ID,
f.LastName,
f.FirstName,
la.StartDate,
la.ExpiryDate,
la.AssignmentStatusId,
ls.StatusName AS 'AssignmentStatus',
'JobRole' AS 'ParentObject'
FROM
	dbo.FastSearchableUsers f WITH (NOLOCK)
	INNER JOIN dbo.LicenseAssignments la WITH (NOLOCK)
		ON la.JobRoleID = f.JobRoleID
	INNER JOIN dbo.LicenseStatus ls WITH (NOLOCK)
		ON ls.Id = la.AssignmentStatusId

UNION

SELECT
la.LicenseID,
f.ID,
f.LastName,
f.FirstName,
la.StartDate,
la.ExpiryDate,
la.AssignmentStatusId,
ls.StatusName AS 'AssignmentStatus',
'Audience' AS 'ParentObject'
FROM
	dbo.FastSearchableUsers f WITH (NOLOCK)
	INNER JOIN dbo.Audience a WITH (NOLOCK)
		ON PATINDEX('%' + a.Name + '%', f.Audiences) > 0
	INNER JOIN dbo.LicenseAssignments la
		ON la.AudienceId = a.ID
	INNER JOIN dbo.LicenseStatus ls WITH (NOLOCK)
		ON ls.Id = la.AssignmentStatusId



");
        }
    }
}