﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration178 : Migration
    {
        public override void Up()
        {
            Execute(@"
alter table TemplateScheduleHistory alter column ObjectID nvarchar(50)
");
        }
    }
}