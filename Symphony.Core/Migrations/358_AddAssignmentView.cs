﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration358 : Migration
    {
        public override void Up()
        {
            Execute(@"CREATE VIEW [dbo].[Assignments] AS
                                        select 
                        s.ID as SectionID, 
                        o.Name as CourseName, 
                        a.CourseID, 
                        a.TrainingProgramID, 
                        a.Attempt, 
                        s.Name, 
                        max(a.[Date]) as [Date], 
                        a.UserID, 
                        count(distinct a.PageId) as NumQuestions,
                        c.NumCorrect,
                        cast(c.NumCorrect as float) / cast(count(distinct a.PageId) as float) as Score,
                        case
							when c.MaxResponse > 1 then 1
							when c.MaxResponse <= 1 then 0
						end as IsMarked
                    from OnlineCourseAssignments a
                    join ArtisanPages p on p.ID = a.PageID
                    join ArtisanSectionPages sp on p.ID = sp.PageID
                    join ArtisanSections s on s.ID = sp.SectionID
                    join OnlineCourse o on a.CourseID = o.ID
                    join (
						select 
							ia.UserID, 
							ia.CourseID, 
							ia.TrainingProgramID, 
							ia.Attempt,
							isp.SectionID, 
							sum(cast(ia.IsCorrect AS INT)) as NumCorrect,
							max(ia.ResponseStatus) as MaxResponse 
						from OnlineCourseAssignments ia
						join ArtisanSectionPages isp on isp.PageID = ia.PageID
						group by isp.SectionID, ia.Attempt, ia.CourseID, ia.TrainingProgramID, ia.UserID
					) c on c.UserID = a.UserID and 
						   c.CourseID = a.CourseID and 
						   c.TrainingProgramID = a.TrainingProgramID and
						   c.SectionID = s.ID and
						   c.Attempt = a.Attempt
                    group by s.ID, s.Name, a.Attempt, a.CourseID, o.Name, a.TrainingProgramID, a.UserID, c.NumCorrect, c.MaxResponse");
        }
    }
}