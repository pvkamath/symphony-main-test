﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration050 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(1023);
            sb.AppendFormat(@"create procedure GetUsersByHierarchy{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}@hierarchyTypeId int,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}@hierarchyNodeId int{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"as{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}declare @hierarchyTypeCodeName as nvarchar(50){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}SELECT @hierarchyTypeCodeName = CodeName FROM HierarchyType WHERE ID = @hierarchyTypeId{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}IF (@hierarchyTypeCodeName = 'location'){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}BEGIN{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}select{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}u.*{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}from{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}dbo.[fGetLocationHierarchyBranch](@hierarchyNodeId) as a{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}[User] u{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}a.ID = u.LocationID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}END{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}IF (@hierarchyTypeCodeName = 'jobrole'){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}BEGIN{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}select {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}u.*{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}from {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}dbo.[fGetJobRoleHierarchyBranch](@hierarchyNodeId) as a{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}[user] u{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}a.ID = u.JobRoleID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}END{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}IF (@hierarchyTypeCodeName = 'audience'){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}BEGIN{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}select {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}u.*{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}from {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}dbo.[fGetAudienceHierarchyBranch](@hierarchyNodeId) as a{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}UserAudience ua{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}ua.AudienceID = a.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}join{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}[user] u{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}on{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}ua.UserID = u.ID{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}END{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}IF (@hierarchyTypeCodeName = 'user'){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}BEGIN{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}select {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}u.*{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}from {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}[User] as u{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}where {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}u.ID = @hierarchyNodeId{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}END{1}", "\t", Environment.NewLine);
            Execute(sb.ToString());
        }
    }
}