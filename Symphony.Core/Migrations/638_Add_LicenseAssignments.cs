﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration638 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE TABLE [dbo].LicenseAssignments
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LicenseID] [int] NOT NULL,
    [CustomerId] [int] NULL,
	[UserID] [int] NULL,
	[LocationID] [int] NULL,
	[JobRoleID] [int] NULL,
	[AudienceId] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[ExpiryDate] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](64) NULL,
	[ModifiedBy] [nvarchar](64) NULL
)

ALTER TABLE [dbo].[LicenseAssignments] ADD  CONSTRAINT [DF_LicenseAssignments_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]

ALTER TABLE [dbo].[LicenseAssignments] ADD  CONSTRAINT [DF_LicenseAssignments_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]

ALTER TABLE dbo.LicenseAssignments ADD CONSTRAINT PK_LicenseAssignments__Id PRIMARY KEY (ID)

ALTER TABLE dbo.LicenseAssignments 
ADD CONSTRAINT FK_LicenseAssignments_Customer__Id FOREIGN KEY (CustomerId) REFERENCES dbo.Customer(ID)

ALTER TABLE dbo.LicenseAssignments 
ADD CONSTRAINT FK_LicenseAssignments_License__Id FOREIGN KEY (LicenseID) REFERENCES dbo.License(ID)

ALTER TABLE dbo.LicenseAssignments
ADD CONSTRAINT FK_LicenseAssignments_Users__Id FOREIGN KEY (UserID) REFERENCES dbo.[User](ID)

ALTER TABLE dbo.LicenseAssignments
ADD CONSTRAINT FK_LicenseAssignments_Location__Id FOREIGN KEY (LocationID) REFERENCES dbo.Location(ID)

ALTER TABLE dbo.LicenseAssignments
ADD CONSTRAINT FK_LicenseAssignments_JobRole__Id FOREIGN KEY (JobRoleID) REFERENCES dbo.JobRole(ID)

ALTER TABLE dbo.LicenseAssignments
ADD CONSTRAINT FK_LicenseAssignments_Audience__Id FOREIGN KEY (AudienceId) REFERENCES dbo.Audience(ID)


");
        }
    }
}