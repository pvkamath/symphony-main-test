﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration021 : Migration
    {
        public override void Up()
        {
            Execute("create table [ArtisanCourses] ([ID] int primary key identity, [CustomerID] int not null, [Name] nvarchar(50) not null, [Description] nvarchar(100) not null, [Keywords] nvarchar(100) not null)");
        }
    }
}