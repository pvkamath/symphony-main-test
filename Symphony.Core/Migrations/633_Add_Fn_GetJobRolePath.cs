﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration633 : Migration
    {
        public override void Up()
        {
            
            Execute(@"
CREATE FUNCTION [dbo].[fGetJobRolePath]
(   
	@jobRoleId int
)
RETURNS TABLE
AS
RETURN
(
WITH JobRole_CTE (ID, ParentJobRoleID, Level, LevelIndentText)
AS
(
-- Anchor member definition
    SELECT
            j.ID,
            j.ParentJobRoleID,
            0 as Level,
            CAST(j.Name as nvarchar(max)) as LevelIndentText
        FROM
            JobRole j
      WHERE
            j.ID = @jobRoleId
    UNION ALL
-- Recursive member definition
    SELECT
            j.ID,
            j.ParentJobRoleID,
            jj.Level + 1,
            CAST(j.Name + ' > ' + LevelIndentText as nvarchar(max))
    FROM
            JobRole j
    INNER JOIN JobRole_CTE jj ON
            jj.ParentJobRoleID = j.ID
)
SELECT top 1
	  a.ID,
      a.Level,
      a.LevelIndentText
FROM
      JobRole_CTE a
order by
	a.Level desc
)


;
");
        }
    }
}
