﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration079 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanAssets", "Keywords", DbType.String, 9000, false, "''");
        }
    }
}