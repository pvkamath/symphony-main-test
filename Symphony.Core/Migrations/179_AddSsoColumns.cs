﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration179 : Migration
    {
        public override void Up()
        {
            Execute(@"
                ALTER TABLE Customer ADD SsoEnabled bit DEFAULT 0 NOT NULL;
                ALTER TABLE Customer ADD CertificateFile varbinary(max);
                ALTER TABLE Customer ADD CertificateFileName nvarchar(64);
            ");
        }
    }
}