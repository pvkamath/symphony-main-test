﻿using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration781 : Migration
    {
        public override void Up()
        {
            Execute(@"
    CREATE VIEW TrainingProgramAccreditationBoards AS
        select  
	        ab.ID as AccreditationBoardID, 
            ab.Name, 
            ab.PrimaryContact, 
            ab.Phone, 
            ab.CreatedOn as AccreditationBoardCreatedOn,   
            ab.ModifiedOn as AccreditationBoardModifiedOn, 
            ab.CreatedBy as AccreditationBoardCreatedBy, 
            ab.[Description], 
            ab.[Disclaimer], 
            ab.Logo,
	        acc.ID as AccreditationID, 
            acc.[Status],
            acc.TrainingProgramID, 
            acc.StartDate, 
            acc.ExpiryDate, 
            acc.CreditHours, 
            acc.CreditHoursLabel, 
            acc.Disclaimer as DisclaimerOverride, 
            acc.AccreditationCode, 
            acc.CreatedOn as AccreditationCreatedOn, 
            acc.ModifiedOn as AccreditationModifiedOn, 
            acc.CreatedBy as AccreditationCreatedBy, 
            acc.ModifiedBy as AccreditationModifiedBy,
            p.ProfessionID        
        from 
	        TrainingProgramAccreditation acc
        join 
	        AccreditationBoard ab on ab.ID = acc.AccreditationBoardID
        left join
	        AccreditationBoardProfession p on p.AccreditationBoardID = ab.ID
        where
	        acc.IsDeleted = 0
");
        }
    }
}
