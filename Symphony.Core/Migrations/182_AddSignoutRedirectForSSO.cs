﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration182 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "SSOTimeoutRedirect", DbType.String, 2048);
            AddColumn("Customer", "SSOTimeoutMessage", DbType.String, 2048);
        }
    }
}