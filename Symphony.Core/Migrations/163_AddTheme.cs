﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration163 : Migration
    {
        public override void Up()
        {
            Execute(@"insert ArtisanThemes (name, description, csspath, createdon, modifiedon, createdby, modifiedby, customerid, folder, skinname) values
('Franklin Square','Franklin Square','/skins/artisan_content/default/css/theme.css',getutcdate(),getutcdate(),'system','system',1,'/skins/artisan_content/default','franklin_square')
            ");
        }
    }
}