﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration024 : Migration
    {
        public override void Up()
        {
            Execute("update [Templates] set [Subject] = replace([Subject], '{end}', '{!end}')");
            Execute("update [Templates] set [Body] = replace([Body], '{end}', '{!end}')");
        }
    }
}