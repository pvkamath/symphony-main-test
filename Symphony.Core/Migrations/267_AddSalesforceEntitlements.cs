﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration267 : Migration
    {
        public override void Up()
        {
              TableSchema.Table salesforceEntitlements = CreateTable("SalesforceEntitlements");
              salesforceEntitlements.AddPrimaryKeyColumn("ID");
              salesforceEntitlements.AddColumn("UserId", DbType.Int32, 0, false);
              salesforceEntitlements.AddColumn("TrainingProgramId", DbType.Int32, 0, false);
              salesforceEntitlements.AddColumn("ParentTrainingProgramID", DbType.Int32, 0, false);
              salesforceEntitlements.AddColumn("SalesforceEntitlementId", DbType.String, 32, false);
              salesforceEntitlements.AddColumn("SalesforceProductId", DbType.String, 32, false);
              salesforceEntitlements.AddColumn("SalesforceCategory", DbType.String, 256, false);
              salesforceEntitlements.AddColumn("SalesforceDuration", DbType.Int32, 0, false);
              salesforceEntitlements.AddColumn("SalesforceDurationUnits", DbType.String, 32, false);
              salesforceEntitlements.AddColumn("SalesforcePaymentRefId", DbType.Int32, 0, true);
              salesforceEntitlements.AddColumn("SalesforcePartnerCode", DbType.String, 64, true);
              salesforceEntitlements.AddColumn("SalesforceStartDate", DbType.DateTime, 0, false);
              salesforceEntitlements.AddColumn("SalesforceEndDate", DbType.DateTime, 0, false);
              salesforceEntitlements.AddColumn("SalesforcePurchaseDate", DbType.DateTime, 0, false);
              salesforceEntitlements.AddColumn("IsEnabled", DbType.Boolean, 0, false);
              AddSubSonicStateColumns(salesforceEntitlements);
        }
    }
}