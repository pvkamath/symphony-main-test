﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration721 : Migration
    {

        public override void Up()
        {
            AddColumn("OnlineCourseRollup", "CreatedOn", DbType.DateTime);
        }

    }
}
