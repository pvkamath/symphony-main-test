﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration535 : Migration
    {

        public override void Up()
        {
            // Retrieve scorm data for user

            Execute(@"
ALTER PROCEDURE [dbo].[GetScormData]
	                (	
		                @userId int,
		                @courseId int,
		                @trainingProgramId int
	                )
	                as
select 
	p.web_path as WebPath,
	a.scorm_activity_id as ScormActivityID,
	a.activity_progress_status as ActivityProgressStatus,
	a.activity_attempt_count as ActivityAttemptCount,
	a.attempt_completion_amount as AttemptCompletionAmount,
	a.attempt_completion_status as AttemptCompletionStatus,
	a.activity_start_timestamp_utc as ActivityStartTimeUTC,
	a.attempt_start_timestamp_utc as AttemptStartTimeUTC,
	a.update_dt as ActivityUpdateTime,
	art.completion_status as CompletionStatus,
	art.success_status as SuccessStatus,
	art.location as Bookmark,
	art.location_null as IsBookmarkNull,
	art.score_raw as Score,
	art.score_max as ScoreMax,
	art.score_min as ScormMin,
	art.success_status as ActivityRTSuccessStatus,
	art.suspend_data as DataChunk,
	art.suspend_data_null as IsDataChunkNull,
	art.update_dt as ActivityRTUpdateTime,
	art_int.interaction_index as InteractionIndex,
	art_int.interaction_id as ArtisanPageID,
	art_int.[type] as TypeID,
	art_int.timestamp_utc as InteractionTimeUTC,
	art_int.result as InteractionResult,
	art_int.description as InteractionDescription,
	art_int_resp.learner_response as LearnerResponse,
	art_int_resp.update_dt as ResponseUpdateTime
from
	[OnlineTraining].[dbo].[ScormRegistration] r
left join 
	[OnlineTraining].[dbo].[ScormPackage] p
	on
	p.scorm_package_id = r.scorm_package_id
left join 
	[OnlineTraining].[dbo].[ScormActivity] a
	on 
	a.scorm_registration_id = r.scorm_registration_id
left join
	[OnlineTraining].[dbo].ScormObject o
	on
	o.scorm_object_id = a.scorm_object_id
left join 
	[OnlineTraining].[dbo].[ScormActivityRT] art
	on
	art.scorm_activity_id = a.scorm_activity_id
left join
	[OnlineTraining].[dbo].[ScormActivityRTInteraction] art_int
	on
	art_int.scorm_activity_id = art.scorm_activity_id
left join [OnlineTraining].[dbo].[ScormActivityRTIntLearnerResp] art_int_resp
	on
	art_int_resp.scorm_activity_id = art_int.scorm_activity_id
	and
	art_int_resp.interaction_index = art_int.interaction_index
where
	r.user_id = @userId
and
	r.course_id = @courseId
and
	r.training_program_id = @trainingProgramId
and
	o.scorm_object_type_id = 2
order by p.update_dt desc;
");
        }

    }
}
