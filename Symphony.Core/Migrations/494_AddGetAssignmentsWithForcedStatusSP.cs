﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration494 : Migration
    {

        public override void Up()
        {
            // Retrieves responses for an assignment with forced grading.

            Execute(@"
CREATE PROCEDURE [dbo].[GetAssignmentWithForcedStatus] (
	@userId int,
	@courseID int,
	@trainingProgramID int,
	@responseStatus int,
	@isCorrect int,
	@response as nvarchar(max),
	@feedback as nvarchar(max)
) AS
BEGIN
select 
	coalesce(oa.ID, 0) as ID,
	coalesce(oa.UserID, @userID) as UserID,
	coalesce(oa.TrainingProgramID, @trainingProgramID) as TrainingProgramID, 
	coalesce(oa.CourseID, @courseID) as CourseID, 
	coalesce(oa.PageID, p.ID) as PageID,
    sp.SectionID as SectionID,
	coalesce(max(oa.Attempt), 1) as Attempt,
	coalesce(oa.Date, getutcdate()) as Date,
	@responseStatus as ResponseStatus,
	@isCorrect as IsCorrect,
	coalesce(oa.Response, @response) as Response,
	coalesce(oa.InstructorFeedback, @feedback) as InstructorFeedback 
from OnlineCourse o
inner join ArtisanCourses a on o.PublishedArtisanCourseID = a.ID
inner join ArtisanSections s on s.CourseID = a.ID
inner join ArtisanSectionPages sp on s.ID = sp.SectionID
inner join ArtisanPages p on p.ID = sp.PageID
left join OnlineCourseAssignments oa on oa.PageID = sp.PageID and oa.UserID = @userID
where o.ID = @courseID
and p.QuestionType = 8
group by sp.SectionID, p.ID, oa.ID, oa.UserID, oa.TrainingProgramID, oa.CourseID, oa.PageID, oa.Date, oa.ResponseStatus, oa.IsCorrect, oa.Response, oa.InstructorFeedback
END;
");
        }

    }
}
