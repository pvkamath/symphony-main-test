﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration501 : Migration
    {

        public override void Up()
        {
            Execute(@"Alter table OnlineCourseRollup drop constraint DF_OnlineCourseRollup_ResetNotes");
            Execute(@"Alter table OnlineCourseRollup alter column ResetNotes nvarchar(max) not null");
            Execute(@"ALTER TABLE [dbo].[OnlineCourseRollup] ADD  CONSTRAINT [DF_OnlineCourseRollup_ResetNotes]  DEFAULT ('') FOR [ResetNotes]");
        }

    }
}
