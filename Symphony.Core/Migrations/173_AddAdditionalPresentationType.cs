﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration173 : Migration
    {
        public override void Up()
        {
            Execute(@"insert presentationtype ([description]) values ('Webinar')");
        }
    }
}