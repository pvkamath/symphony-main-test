﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration715 : Migration
    {
        public override void Up()
        {
            TableSchema.Table accreditation = CreateTableWithKey("OnlineCourseAccreditation", "ID");
            TableSchema.Table board = GetTable("AccreditationBoard");

            accreditation.AddColumn("AccreditationBoardID", DbType.Int32, 0, false);
            accreditation.AddColumn("ArtisanCourseAccreditationID", DbType.Int32, 0, false);
            accreditation.AddColumn("OnlineCourseID", DbType.Int32, 0, false);
            accreditation.AddColumn("StartDate", DbType.DateTime, 0, false);
            accreditation.AddColumn("ExpiryDate", DbType.DateTime, 0, false);
            accreditation.AddColumn("CreditHours", DbType.Single, 0, false);
            accreditation.AddColumn("CreditHoursLabel", DbType.String, 32, false, "'Credit Hours'");
            accreditation.AddColumn("Disclaimer", DbType.String, 32768, true);
            accreditation.AddColumn("AccreditationCode", DbType.String, 32, false);
            accreditation.AddColumn("IsDeleted", DbType.Boolean, 0, false, "0");

            AddSubSonicStateColumns(accreditation);
        }
    }
}
