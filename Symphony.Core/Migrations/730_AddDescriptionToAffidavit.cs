﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration730 : Migration
    {
        public override void Up()
        {
            AddColumn("AffidavitFinalExam", "Description", DbType.String, 512, true); 
        }
    }
}