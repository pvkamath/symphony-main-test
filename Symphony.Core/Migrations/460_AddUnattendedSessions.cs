﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration460 : Migration
    {

        public override void Up()
        {
            Execute(@"CREATE VIEW [dbo].[UnattendedSessions] AS
                    -- Retrieves a list of users for current sessions that 
                    -- have started the session prior to the session expiry.
                    -- Will include the next session, if it exists, and
                    -- the date of a future session the user may be registered for.
                    --
                    -- If minMax.ClassID == Null, there is no upcoming session, set this user to denied to remove from the list
                    -- If upcoming.ClassStartTime == nextClassStart.NextStartDate, user is already registered for the next session
                    --          but in either Denied, WaitList, or Awaiting Approval. Shouldn't need to do anything automatic here.
                    -- If duplicate rows exist for a user, with only the minMax.ClassID changing, there are multiple upcoming sessions
                    --          that have the same start date. In this case the user should be registered for one of these sessions to take
                    --          them off this list.
                    -- If minMax.ClassID != null and no duplicate rows, we should be able to safely create a registration for this class
                    --          to advance the user into the next session
                    select 
	                    tpStart.SessionCourseID,            -- Course id of the training program session
	                    tpStart.SessionTimeout,             -- Minutes until the user can be rolled forward
	                    tpStart.RegistrantID,               -- User id
	                    tpStart.ClassStartTime,             -- Start time of the class
	                    tpStart.TimeSinceStart,             -- Time since the start of the class in minutes
	                    nextClassStart.NextStartDate,       -- next class start date we can register the user for
	                    minMax.ClassID,                     -- Next class id that we can potentially register the user for
	                    upcoming.ClassStartTime as Upcoming -- Future date of a course user has a registration for
                    from (
	                    -- Gets the max session for each training program session
	                    -- that all users are registered for. 
	                    select 
		                    t.SessionCourseID, 
		                    t.SessionTimeout, 
		                    r.RegistrantID, 
		                    max(st.TrainingProgramStartTime) TrainingProgramStartTime, -- Because a session could be assigned to multiple child training programs
																					   -- this would create null start dates for the other training programs the
																					   -- the user is assigned to, so take the max, to ensure we get a start time
																					   -- if one exists for any of the training programs.
		                    max(d.MinStartDateTime) as ClassStartTime, -- Only look at the latest registration for each user
																	   -- Future registrations are filtered in the final where clause
		                    DATEDIFF(Minute, max(d.MinStartDateTime), GETUTCDATE()) as TimeSinceStart
	                    from 
		                    TrainingProgram t
	                    join Class c on c.CourseID = t.SessionCourseID
	                    join ClassDateMinMax d on d.ClassID = c.ID
	                    join Registration r on r.ClassID = c.ID
	                    left join (
		                    -- Joining on the training program start time
		                    -- based on the minimum course start time for
		                    -- the training program/user.
		                    select 
			                    min(cst.StartTime) as TrainingProgramStartTime,
			                    cst.TrainingProgramID,
			                    cst.UserID
		                    from CourseStartTime cst
		                    group by cst.TrainingProgramID, cst.UserID
	                    ) st on 
		                    st.UserID = r.RegistrantID and 
		                    t.ID = st.TrainingProgramID
	                    where 
		                    -- Only focus on training programs with sessions
		                    -- and users that have the registered status
		                    -- (Ignore awaiting approval, wait list, and denied)
		                    t.SessionCourseID > 0 and 
		                    r.RegistrationStatusID = 4 and
                            t.SessionTimeout > 0 -- Otherwise users will be bumped as soon as they are registered!
                                                 -- So if no session timeout is set, we do not do any rollovers
	                    group by t.SessionCourseID, t.SessionTimeout, r.RegistrantID
                    ) tpStart
                    left join (
	                    -- Join the next class on
	                    -- This has the minimum class start date
	                    -- that is greater than today's date
	                    select 
		                    nextClass.CourseID,
		                    nextClass.CustomerID,
		                    min(nextMinMax.MinStartDateTime) NextStartDate
	                    from Class nextClass
	                    join ClassDateMinMax nextMinMax
	                    on nextMinMax.ClassID = nextClass.ID
	                    where nextMinMax.MinStartDateTime > GETUTCDATE()
	                    group by nextClass.CourseID, nextClass.CustomerID
                    ) nextClassStart on nextClassStart.CourseID = tpStart.SessionCourseID
	                    -- Self join the ClassDateMinMax back to itself so we can
	                    -- determine the class id that has the next date.
	                    -- Joining on MinStartDate, CustomerID, and ClassID in classes for the session.
	                    -- Note, that if there are two classes for the same session
	                    -- that start at the same time, this will create a duplicate row (Two class IDs). 
	                    -- When processing, the user should only roll forward to one of the classes
                    left join 
	                    ClassDateMinMax minMax
                    on 
	                    minMax.CustomerID = nextClassStart.CustomerID and
	                    minMax.MinStartDateTime = nextClassStart.NextStartDate and
	                    minMax.ClassID in (
		                    select 
			                    cl.ID
		                    from 
			                    Class cl
		                    where
			                    cl.CourseID = tpStart.SessionCourseID
	                    )
	                    -- Finally join on the next upcoming course the user might have a registration for.
	                    -- If the user has a future course with a registration, they would have already been excluded
	                    -- however, they might have a registration that is Denied, Awaiting Approval, or Wait List. 
	                    -- I'm thinking it's possible that users may be preassigned a denied registration to stop them from rolling forward 
	                    -- at a certain point. We don't care about previous denied, as long as the most recent one is not.
	                    -- If it's awaiting approval, or wait list, we shouldn't roll forward either since they are already
	                    -- waiting on this course. 
	                    -- If Upcoming == NextStartDate we don't want to roll forward to this class, something will have
	                    -- to be done manually at this point.
                    left join (
	                    select 
		                    t.SessionCourseID, 
		                    t.SessionTimeout, 
		                    r.RegistrantID, 
		                    min(d.MinStartDateTime) as ClassStartTime
	                    from 
		                    TrainingProgram t
	                    join Class c on C.CourseID = t.SessionCourseID
	                    join ClassDateMinMax d on d.ClassID = c.ID
	                    join Registration r on r.ClassID = c.ID
	                    where d.MinStartDateTime > GETUTCDATE()
	                    group by t.SessionCourseID, t.SessionTimeout, r.RegistrantID
                    ) upcoming on upcoming.SessionCourseID = tpStart.SessionCourseID and upcoming.RegistrantID = tpStart.RegistrantID
                    where 
	                    tpStart.ClassStartTime < GETUTCDATE() and 
	                    tpStart.TrainingProgramStartTime is null and
	                    tpStart.TimeSinceStart > tpStart.SessionTimeout");
        }

    }
}