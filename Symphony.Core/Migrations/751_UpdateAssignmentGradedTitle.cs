﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration751 : Migration
    {
        public override void Up()
        {
            Execute(@"
            update Templates set DisplayName = 'Assignment Graded' where DisplayName = 'Assignment graded'
            ");
        }

    }
}