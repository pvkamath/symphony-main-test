﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration343 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER view [dbo].[SimplePublicCourses]
as
    select
        o.ID, o.Name, o.[Description], o.Credit, '0' as InternalCode, o.CustomerID, 2 as CourseTypeID, 0 as CourseCompletionTypeID, 0 as HasSurvey, 0 as OnlineCourseID, Keywords, c.Name as CategoryName, cast(0 as bit) as IsThirdParty,
        o.Retries, o.RetestMode, (SELECT TOP 1 mb.ID FROM MessageBoard AS mb WHERE mb.OnlineCourseID = o.ID) as MessageBoardID, COALESCE(o.CertificateEnabledOverride, cu.CertificateEnabledOverride, o.CertificateEnabled) as CertificateEnabled
    from
        OnlineCourse as o
    left join
        Category as c
    on
        o.CategoryID = c.ID
    left join
		Customer as cu
	on 
		o.CustomerID = cu.ID
    where
        publicindicator = 1

    union

    select
        co.ID, co.Name, co.[Description], Credit, InternalCode, co.CustomerID, 1 as CourseTypeID, CourseCompletionTypeId, HasSurvey, OnlineCourseID, '' as Keywords, c.Name as CategoryName, co.IsThirdParty,
        0, 0, 0, COALESCE(cu.CertificateEnabledOverride, co.CertificateEnabled) as CertificateEnabled
    from
        Course as co
    left join
        Category c
    on
        co.CategoryID = c.ID
    left join
		Customer as cu
	on 
		co.CustomerID = cu.ID
    where
        publicindicator = 1
;;");


        }
    }
}