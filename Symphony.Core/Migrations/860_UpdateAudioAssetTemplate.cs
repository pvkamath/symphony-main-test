﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration860 : Migration
    {

        public override void Up()
        {
            Execute(@"
    update ArtisanAssetTypes 
    set Template = '<div id=""{element_id}"" class=""wrapper"" style=""position: relative; width: 100%; height: 100%;""><audio id=""audio-{id}"" class=""video-js vjs-default-skin"" controls preload=""auto""><source src=""{path}"" type=""audio/mp3""></source><object width=""100%"" height=""100%"" align=""top"" classid=""clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B"" codebase=""http://www.apple.com/qtactivex/qtplugin.cab""><param name=""src"" value=""{path}"" /><param name=""autoplay"" value=""false"" /><param name=""controller"" value=""true"" /><param name=""bgcolor"" value=""#ffffff"" /><embed src=""{path}"" autostart=""false"" bgcolor=""#ffffff"" controller=""true"" width=""100%"" height=""100%"" name=""{element_id}""/></object></audio></div>'
    where Name = 'Audio'");
        }
    }
}
