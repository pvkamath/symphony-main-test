﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration023 : Migration
    {
        public override void Up()
        {
            Execute("delete [RecipientGroups] where [DisplayName] = 'Registrant'");
        }
    }
}