﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration596 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE PROCEDURE [dbo].[GetLatestAssignmentResponses] (@userId int, @trainingProgramId int, @courseId int)
as 
begin
	select o.* from
	(
	select MAX(o.Attempt) as MaxAttempt, PageID from OnlineCourseAssignments o
	where o.UserID = @userId
			and o.TrainingProgramID = @trainingProgramId
			and o.CourseID = @courseId
	group by PageID
	) m join OnlineCourseAssignments o on
	o.UserID = @userId
	and
	o.TrainingProgramID = @trainingProgramId
	and 
	o.CourseID = @courseId
	and 
	o.PageID = m.PageID
	and
	o.Attempt = m.MaxAttempt
end

");
            
        }
    }
}
