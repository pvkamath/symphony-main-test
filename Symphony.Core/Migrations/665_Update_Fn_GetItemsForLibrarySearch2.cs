﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration665 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER FUNCTION [dbo].[fGetItemsForLibrary_Search]
(
	@libraryIdsCS nvarchar(max),
	@libraryItemTypeId INT = null,
	@customerId INT,
	@isShowAll bit,
	@isHideInLibrary bit,
	@categoryIdsCS nvarchar(max),
	@secondaryCategoryIdsCS nvarchar(max),
    @secondaryCategorySearch nvarchar(2000),
    @authorIdsCS nvarchar(max),
    @authorSearch nvarchar(2000),
	@search nvarchar(50), 
	@orderBy nvarchar(50), 
	@orderDir varchar(4),
	@isFavorite int,
	@isInProgress int, 
	@isNotStarted int,
	@userId int,
	@startDate datetime = null,
	@endDate datetime = null
)
RETURNS @Tr TABLE (
	ID int,
	LibraryItemID int,
	--LibraryID int,
	Name nvarchar(2000),
	[Description] nvarchar(max),
	CategoryID int,
	Duration int,
	LibraryItemTypeID int, 
	CreatedOn datetime,
	CourseTypeID int,
	CategoryName nvarchar(max),
	LevelIndentText nvarchar(max),
	IsInLibrary bit,
	IsFavorite bit,
	IsInProgress bit,
	LibraryFavoriteID int,
	r int,
	ItemCreatedOn datetime,
	ItemModifiedOn datetime,
	CourseVersion int
)
AS
BEGIN

	
	if (@secondaryCategorySearch is null or @secondaryCategorySearch = '' or len(@secondaryCategorySearch) = 0)
	begin
		set @secondaryCategorySearch = '""""'
	end
	
	if (@authorSearch is null or @authorSearch = '' or len(@authorSearch) = 0)
	begin
		set @authorSearch = '""""'
	end
	

	declare @authorIds table (id int)
	declare @secondaryCategoryIds table (id int)
	declare @categoryIds table (id int)
	
	insert into @secondaryCategoryIds
		select CAST(LTRIM(RTRIM(item)) as int)
		from fSplit(@secondaryCategoryIdsCS,',');
	insert into @authorIds
		select CAST(LTRIM(RTRIM(item)) as int)
		from fSplit(@authorIdsCS,',');
	insert into @categoryIds
		select CAST(LTRIM(RTRIM(item)) as int)
		from fSplit(@categoryIdsCS,',');
	
	insert into @Tr
	select 
		ID,
		LibraryItemID,
		Name, 
		[Description],
		CategoryID,
		Duration,
		LibraryItemTypeID,
		CreatedOn,
		CourseTypeID,
		CategoryName,
		LevelIndentText, 
		IsInLibrary,
		IsFavorite,
		IsInProgress,
		LibraryFavoriteID,
		row_number() over 
		(
			order by
				case when @orderBy = 'CourseTypeID' and @orderDir = 'asc' then CourseTypeID end, 
				case when @orderBy = 'CourseTypeID' and @orderDir = 'desc' then CourseTypeID end desc,
				case when @orderBy = 'Name' and @orderDir = 'asc' then Name end,
				case when @orderBy = 'Name' and @orderDir = 'desc' then Name end desc,
				case when @orderBy = 'Category' and @orderDir = 'asc' then LevelIndentText end,
				case when @orderBy = 'Category' and @orderDir = 'desc' then LevelIndentText end desc,
				case when @orderBy = 'CreatedOn' and @orderDir = 'asc' then CreatedOn end,
				case when @orderBy = 'CreatedOn' and @orderDir = 'desc' then CreatedOn end desc,
				case when @orderBy = 'DisplayInLibrary' and @orderDir = 'asc' then IsInLibrary end,
				case when @orderBy = 'DisplayInLibrary' and @orderDir = 'desc' then IsInLibrary end desc,
				case when @orderBy = 'ItemCreatedOn' and @orderDir = 'asc' then ItemCreatedOn end,
				case when @orderBy = 'ItemCreatedOn' and @orderDir = 'desc' then ItemCreatedOn end desc,
				case when @orderBy = 'ItemModifiedOn' and @orderDir = 'asc' then ItemModifiedOn end,
				case when @orderBy = 'ItemModifiedOn' and @orderDir = 'desc' then ItemModifiedOn end desc,
				case when @orderBy = 'CourseVersion' and @orderDir = 'asc' then CourseVersion end,
				case when @orderBy = 'CourseVersion' and @orderDir = 'desc' then CourseVersion end desc,
				case when @orderBy = 'IsInProgress' and @orderDir = 'asc' then IsInProgress end,
				case when @orderBy = 'IsInProgress' and @orderDir = 'desc' then IsInProgress end desc,
				case when @orderBy = 'IsFavorite' and @orderDir = 'asc' then IsFavorite end,
				case when @orderBy = 'IsFavorite' and @orderDir = 'desc' then IsFavorite end desc
		) as r,
		ItemCreatedOn,
		ItemModifiedOn,
		CourseVersion
	from 
		(
		select
			item.ID,
			max(item.LibraryItemID) as LibraryItemID,
			--item.LibraryID,
			item.Name, 
			item.[Description],
			item.CategoryID,
			item.Duration,
			item.LibraryItemTypeID,
			max(item.CreatedOn) as CreatedOn,
			item.CourseTypeID,
			item.CategoryName,
			item.LevelIndentText, 
			cast(max(cast(item.IsInLibrary as int)) AS bit) as IsInLibrary,
			cast(max(cast(item.IsFavorite as int)) AS bit) as IsFavorite,
			cast(max(cast(item.IsInProgress as int)) AS bit) as IsInProgress,
			cast(max(cast(item.LibraryFavoriteID as int)) AS bit) as LibraryFavoriteID,
			item.ItemCreatedOn as ItemCreatedOn,
			item.ItemModifiedOn as ItemModifiedOn,
			item.CourseVersion
		from fGetItemsForLibrary(
			@libraryIdsCS, 
			@libraryItemTypeId, 
			@customerID, 
			@isShowAll, 
			@isHideInLibrary,
			@isFavorite,
			@isInProgress, 
			@isNotStarted,
			@userId
		) item
		left join 
			OnlineCourseAuthorLink al
		on 
			al.OnlineCourseId = item.Id and item.LibraryItemTypeId = 1
		left join
			[User] u
		on 
			u.Id = al.UserID
		left join
			OnlineCourseSecondaryCategoryLink scl
		on
			scl.OnlineCourseId = item.Id and item.LibraryItemTypeId = 1
		left join
			SecondaryCategory sc
		on
			sc.Id = scl.SecondaryCategoryId
		where
			(item.Name like '%' + @search + '%' or item.LevelIndentText like '%' + @search + '%') and
			(
				(@categoryIdsCS is not null and len(@categoryIdsCS) != 0 and item.CategoryID in (select id from @categoryIds)) or 
				(@categoryIdsCS is null or len(@categoryIdsCS) = 0)
			) and
			(
				(@authorIdsCS is null or len(@authorIdsCS) = 0) or
				(@authorIdsCS is not null and u.Id in (select id from @authorIds))
			) and
			(
				(@secondaryCategoryIdsCS is null or len(@secondaryCategoryIdsCS) = 0) or
				(@secondaryCategoryIdsCS is not null and sc.Id in (select id from @secondaryCategoryIds))
			) and
			( @secondaryCategorySearch = '""""' or CONTAINS(sc.Name, @secondaryCategorySearch)) and
			( @authorSearch = '""""' or CONTAINS((u.firstName, u.lastName), @authorSearch)) and
			( @startDate is null or item.CreatedOn > @startDate) and
			( @endDate is null or item.CreatedOn < @endDate)
		group by 
			item.ID,
			--item.LibraryItemID,
			--item.LibraryID,
			item.Name,
			item.[Description],
			item.CategoryID,
			item.Duration,
			item.LibraryItemTypeID, 
			--item.CreatedOn,
			item.CourseTypeID,
			item.CategoryName,
			item.LevelIndentText,
			--item.IsInLibrary,
			--item.IsFavorite,
			--item.LibraryFavoriteID,
			--item.IsInProgress,
			item.ItemCreatedOn,
			item.ItemModifiedOn,
			item.CourseVersion
		) d
		
		return
END
;

");
        }
    }
}
