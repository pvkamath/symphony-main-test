﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration071 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "SectionType", DbType.Int32, 0, false, "1");
        }
    }
}