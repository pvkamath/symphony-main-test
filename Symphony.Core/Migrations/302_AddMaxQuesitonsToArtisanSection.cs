﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration302 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "MaxQuestions", DbType.Int32, 0, false, "20");
        }
    }
}