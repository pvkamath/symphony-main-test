﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration572 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE TABLE [dbo].OnlineCourseAssignmentsArchive
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[TrainingProgramID] [int] NOT NULL,
	[CourseID] [int] NOT NULL,
	[PageID] [int] NOT NULL,
	[Attempt] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[ResponseStatus] [int] NOT NULL,
	[IsCorrect] [bit] NOT NULL,
	[Response] [nvarchar](max) NOT NULL,
	[InstructorFeedback] [nvarchar](max) NULL,
	[IsAutoResponse] [bit] NOT NULL,
	[GradedDate] [datetime] NULL,
	[IsReset] [bit] NULL,
	DateCreated DATETIME,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].OnlineCourseAssignmentsArchive ADD  CONSTRAINT [DF_OnlineCourseAssignmentsArchive__ResponseStatus]  DEFAULT ((1)) FOR [ResponseStatus]


ALTER TABLE [dbo].OnlineCourseAssignmentsArchive ADD  CONSTRAINT [DF_OnlineCourseAssignmentsArchive__IsCorrect]  DEFAULT ((0)) FOR [IsCorrect]


ALTER TABLE [dbo].OnlineCourseAssignmentsArchive ADD  CONSTRAINT [DF_OnlineCourseAssignmentsArchive__Response]  DEFAULT ('') FOR [Response]


ALTER TABLE [dbo].OnlineCourseAssignmentsArchive ADD  CONSTRAINT [DF_OnlineCourseAssignmentsArchive__IsAutoResponse]  DEFAULT ((0)) FOR [IsAutoResponse]

ALTER TABLE dbo.OnlineCourseAssignmentsArchive
ADD CONSTRAINT PK_OnlineCourseAssignmentsArchive__DateCrated DEFAULT (GETDATE()) FOR DateCreated


");
        }
    }
}
