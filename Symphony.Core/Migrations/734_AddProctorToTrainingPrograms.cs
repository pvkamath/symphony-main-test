﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration734 : Migration
    {
        public override void Up()
        {
            Execute(@"
                SET IDENTITY_INSERT ProctorForm ON;

                INSERT INTO ProctorForm(ID, Name, Description, ProctorJSON)
                VALUES(1, 'Default',  '', '{""width"":420,""height"":260,""items"":[{""_name"":""Proctor Label"",""xtype"":""component"",""html"":""Please have your proctor enter their information."",""margin"":{""left"":10,""bottom"":10}},{""_name"":""Name"",""xtype"":""textfield"",""name"":""Name"",""fieldLabel"":""Name"",""anchor"":"" - 10""},{""_name"":""Code"",""xtype"":""textfield"",""name"":""Code"",""fieldLabel"":""Code"",""anchor"":"" - 10""},{""_name"":""Location"",""xtype"":""textfield"",""name"":""Location"",""fieldLabel"":""Location"",""anchor"":"" - 10""},{""_name"":""I am an authorized proctor"",""xtype"":""checkboxgroup"",""labelPad"":12,""margin"":{""left"":0},""items"":[{""xtype"":""checkbox"",""name"":"""",""boxLabel"":""I am an authorized proctor""}]},{""_name"":""Confirm"",""_showCancelButton"":true,""xtype"":""container"",""margin"":{""top"":6,""bottom"":6},""layout"":{""align"":""middle"",""pack"":""center"",""type"":""hbox""},""items"":[{""xtype"":""button"",""itemId"":""cancel"",""cls"":""x - btn - large"",""text"":""Cancel"",""minWidth"":90,""margin"":{""right"":12}},{""xtype"":""button"",""itemId"":""confirm"",""cls"":""x - btn - large"",""text"":""Confirm"",""minWidth"":90}]}]}')
            ");

            AddColumn("TrainingProgram", "ProctorFormID", DbType.Int32, 0, false, "1");
        }
    }
}