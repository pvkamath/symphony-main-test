﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration537 : Migration
    {
        public override void Up()
        {
            Execute(@"
update
	q
set
	q.CustomerID = e.CustomerID,
	q.ReportID = e.ReportID,
    q.StatusID = e.StatusID,
	q.StartedOn = e.StartedOn,
	q.FinishedOn = e.FinishedOn,
	q.DownloadCSV = e.DownloadCSV,
	q.DownloadXLS = e.DownloadXLS,
	q.DownloadPDF = e.DownloadPDF,
	q.ReportData = e.ReportData
from 
	ReportQueues q
inner join
	ReportQueueEntries e
on
	q.ID = e.QueueID
");
        }
    }
}