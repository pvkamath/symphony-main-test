﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration778 : Migration
    {
        public override void Up()
        {
            TableSchema.Table SAMLCache = CreateTableWithKey("SAMLCache", "ID");

            SAMLCache.AddColumn("Key", DbType.String);
            SAMLCache.AddColumn("Value", DbType.String);
            SAMLCache.AddColumn("ValidDurationInMS", DbType.Int64);

            AddSubSonicStateColumns(SAMLCache);
        }
    }
}
