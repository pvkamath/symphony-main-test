﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration376 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE Customer ALTER COLUMN Modules NVARCHAR(512) NULL;");
        }
    }
}