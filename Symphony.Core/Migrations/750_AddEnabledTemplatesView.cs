﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration750 : Migration
    {
        public override void Up()
        {
            Execute(@"
            create view EnabledTemplates as
                select 
		             t.[ID]
		            ,t.[Subject]
		            ,t.[Body]
		            ,t.[CodeName]
		            ,t.[DisplayName]
		            ,t.[Description]
		            ,t.[Priority]
		            ,t.[CustomerID]
		            ,t.[ModifiedBy]
		            ,t.[CreatedBy]
		            ,t.[ModifiedOn]
		            ,t.[CreatedOn]
		            ,t.[Area]
		            ,t.[SearchText]
		            ,t.[IsScheduled]
		            ,t.[ScheduleParameterID]
		            ,t.[RelativeScheduledMinutes]
		            ,t.[FilterComplete]
		            ,t.[OwnerID]
		            ,t.[CcClassroomSupervisors]
		            ,t.[CcReportingSupervisors]
		            ,t.[CreatedByUserId]
		            ,t.[ModifiedByUserId]
		            ,t.[CreatedByActualUserId]
		            ,t.[ModifiedByActualUserId]
		            ,t.[IncludeCalendarInvite]
		            ,t.[Percentage]
		            ,t.[IsConfigurableForTrainingProgram]
		            ,t.[IsUserCreated]
		            ,t.[Enabled]
		            ,case when count(tpt.TemplateID) > 0 then 1 else 0 end as IsEnabledByTrainingProgram
	            from [Templates] t
	            left join TrainingProgramTemplates tpt on tpt.TemplateID = t.ID
	            where [Enabled] = 1 or tpt.TemplateID is not null
	            group by
		             t.[ID]
		            ,t.[Subject]
		            ,t.[Body]
		            ,t.[CodeName]
		            ,t.[DisplayName]
		            ,t.[Description]
		            ,t.[Priority]
		            ,t.[CustomerID]
		            ,t.[Enabled]
		            ,t.[ModifiedBy]
		            ,t.[CreatedBy]
		            ,t.[ModifiedOn]
		            ,t.[CreatedOn]
		            ,t.[Area]
		            ,t.[SearchText]
		            ,t.[IsScheduled]
		            ,t.[ScheduleParameterID]
		            ,t.[RelativeScheduledMinutes]
		            ,t.[FilterComplete]
		            ,t.[OwnerID]
		            ,t.[CcClassroomSupervisors]
		            ,t.[CcReportingSupervisors]
		            ,t.[CreatedByUserId]
		            ,t.[ModifiedByUserId]
		            ,t.[CreatedByActualUserId]
		            ,t.[ModifiedByActualUserId]
		            ,t.[IncludeCalendarInvite]
		            ,t.[Percentage]
		            ,t.[IsConfigurableForTrainingProgram]
		            ,t.[IsUserCreated]
            ");
        }

    }
}