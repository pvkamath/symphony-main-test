﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration634 : Migration
    {
        public override void Up()
        {
            
            Execute(@"
CREATE FUNCTION [dbo].[fGetHierarchyPath] 
(	
	-- Add the parameters for the function here
	@HierarchyNodeID int,
	@HierarchyTypeID int
)
RETURNS @HTable TABLE
(ID int not null, TypeID int not null, LevelIndentText nvarchar(2000), [Level] int)
AS
BEGIN
	DECLARE @HierarchyTypeCodeName AS nvarchar(50)

	SELECT @HierarchyTypeCodeName = CodeName FROM HierarchyType WHERE ID = @HierarchyTypeID
	
	IF (@HierarchyTypeCodeName = 'location')
	BEGIN
		INSERT @HTable SELECT ID, @HierarchyTypeID, LevelIndentText, [Level] FROM dbo.fGetLocationPath(@HierarchyNodeID)
	END
	IF (@HierarchyTypeCodeName = 'jobrole')
	BEGIN
		INSERT @HTable SELECT ID, @HierarchyTypeID, LevelIndentText, [Level] FROM dbo.fGetJobRolePath(@HierarchyNodeID)
	END
	IF (@HierarchyTypeCodeName = 'audience')
	BEGIN
		INSERT @HTable SELECT ID, @HierarchyTypeID, LevelIndentText, [Level] FROM dbo.fGetAudiencePath(@HierarchyNodeID)
	END
	IF (@HierarchyTypeCodeName = 'user')
	BEGIN
		INSERT @HTable SELECT  @HierarchyNodeID, @HierarchyTypeID, FirstName + ' ' + LastName, 1 FROM [User] where ID = @HierarchyNodeID
	END
	RETURN
END


");
        }
    }
}
