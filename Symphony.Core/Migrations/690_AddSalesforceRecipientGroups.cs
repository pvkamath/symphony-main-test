﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration690 : Migration
    {

        public override void Up()
        {
            Execute(@"
    INSERT INTO TemplatesToRecipientGroupsMap 
        (
            RecipientGroupID,
            TemplateID,
            CreatedByUserID,
            ModifiedByUserID,
            CreatedByActualUserID,
            ModifiedByActualUserID
        ) 
        SELECT 12, (SELECT TOP 1 ID FROM Templates WHERE CodeName = 'SalesforceNewUser' AND CustomerID = 0), 0, 0, 0, 0
        UNION ALL
        SELECT 12, (SELECT TOP 1 ID FROM Templates WHERE CodeName = 'SalesforceNewEnrollment' AND CustomerID = 0), 0, 0, 0, 0
");
        }

    }
}
