﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration856 : Migration
    {
        public override void Up()
        {
            Execute(@"
                CREATE FUNCTION [dbo].[fGetHierarchyBranch] 
                (	
	                -- Add the parameters for the function here
	                @HierarchyNodeID int,
	                @HierarchyTypeID int
                )
                RETURNS TABLE
                AS
                RETURN (
	                SELECT
		                ID as ID, 1 as TypeID
	                FROM
		                dbo.fGetLocationHierarchyBranch(@HierarchyNodeID)
	                WHERE
		                @HierarchyTypeID = 1
	                UNION
	                SELECT
		                ID, 2
	                FROM
		                dbo.fGetJobRoleHierarchyBranch(@HierarchyNodeID)
	                WHERE
		                @HierarchyTypeID = 2
	                UNION
	                SELECT
		                ID, 3
	                FROM
		                dbo.fGetAudienceHierarchyBranch(@HierarchyNodeID)
	                WHERE
		                @HierarchyTypeID = 3
	                UNION
	                SELECT
		                @HierarchyNodeID, 4
	                WHERE
		                @HierarchyTypeID = 4
                )

");
        }
    }
}
