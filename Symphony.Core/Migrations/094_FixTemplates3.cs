﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration094 : Migration
    {
        public override void Up()
        {
            Execute("update artisantemplates set html = '' where pagetype = 2");
        }
    }
}