﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration065 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(1289);
            sb.AppendFormat(@"update{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}Templates{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"set{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[Subject] = 'New Hire Status Change',{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}Body = 'The new hire status for {{!User.FullName}} has expired.  Additional training programs may ", Environment.NewLine);
            sb.AppendFormat(@"now be available based on new training assignments.'{0}", Environment.NewLine);
            sb.AppendFormat(@"where{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}CodeName = 'NewHireStatusChange'{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"and{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}CustomerID = 0{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"update {0}", Environment.NewLine);
            sb.AppendFormat(@"{0}Templates{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"set{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}Body = '{{!Registration.UserFullName}} has attended the {{!ClassX.Course.Name}} on {{!foreach ", Environment.NewLine);
            sb.AppendFormat(@"ClassX.ClassDates}} {{!ClassDate.StartDate}} at {{!ClassDate.StartTime}} {{!end}} and has received ");
            sb.AppendFormat(@"a score of {{score}}.'{0}", Environment.NewLine);
            sb.AppendFormat(@"where{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}CodeName = 'MarkedAsAttended'{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"and{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}CustomerID = 0{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"insert into{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}Templates {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}({1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}[Subject], {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Body, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}CodeName, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}DisplayName, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}[Description], {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Priority, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}CustomerID, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}[Enabled], {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}ModifiedBy, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}CreatedBy,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}CreatedOn,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}ModifiedOn{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"values{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}({1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'Registration Change for {{!Registration.UserFullName}}',{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'{{!User.FullName}}''s registration status has been changed to ", Environment.NewLine);
            sb.AppendFormat(@"{{!Registration.Status.Description}} for the {{!ClassX.Course.Name}} on {{!foreach ");
            sb.AppendFormat(@"ClassX.ClassDates}} {{!ClassDate.StartDate}} at {{!ClassDate.StartTime}} {{!end}}.',{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'RegistrationStatusChanged',{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'Registration Status Changed',{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'Occurs when a user''s registration status is changed in classroom',{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}1,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}0,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}1,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'admin',{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}'admin',{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}GETDATE(),{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}GETDATE(){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0})", Environment.NewLine);
            Execute(sb.ToString());
        }
    }
}