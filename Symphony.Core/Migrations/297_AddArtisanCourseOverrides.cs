﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration297 : Migration
    {
        public override void Up()
        {
            AddColumn("OnlineCourse", "RandomizeQuestionsOverride", DbType.Boolean, 0, true);
            AddColumn("OnlineCourse", "RandomizeAnswersOverride", DbType.Boolean, 0, true);
            AddColumn("OnlineCourse", "ReviewMethodOverride", DbType.Int32, 0, true);
            AddColumn("OnlineCourse", "MarkingMethodOverride", DbType.Int32, 0, true);
            AddColumn("OnlineCourse", "InactivityMethodOverride", DbType.Int32, 0, true);
            AddColumn("OnlineCourse", "InactivityTimeoutOverride", DbType.Int32, 0, true);
            AddColumn("OnlineCourse", "MinimumPageTimeOverride", DbType.Int32, 0, true);
            AddColumn("OnlineCourse", "MaximumQuestionTimeOverride", DbType.Int32, 0, true);

            AddColumn("Customer", "RandomizeQuestionsOverride", DbType.Boolean, 0, true);
            AddColumn("Customer", "RandomizeAnswersOverride", DbType.Boolean, 0, true);
            AddColumn("Customer", "ReviewMethodOverride", DbType.Int32, 0, true);
            AddColumn("Customer", "MarkingMethodOverride", DbType.Int32, 0, true);
            AddColumn("Customer", "InactivityMethodOverride", DbType.Int32, 0, true);
            AddColumn("Customer", "InactivityTimeoutOverride", DbType.Int32, 0, true);
            AddColumn("Customer", "MinimumPageTimeOverride", DbType.Int32, 0, true);
            AddColumn("Customer", "MaximumQuestionTimeOverride", DbType.Int32, 0, true);
        }
    }
}