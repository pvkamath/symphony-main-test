﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration117 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER view [dbo].[TrainingProgramUsers] as
                    -- this view is going to get us a list of all the users in a given training program

                    SELECT     
	                    --ROW_NUMBER() OVER (ORDER BY TrainingProgramID) AS ID, 
	                    1 as ID,
	                    CustomerID, 
	                    TrainingProgramID, 
	                    TrainingProgramName, 
	                    TrainingProgramStartDate, 
	                    TrainingProgramDueDate, 
	                    TrainingProgramEndDate, 
	                    TrainingProgramDescription, 
	                    EnforceRequiredOrder, 
	                    IsNewHire, 
	                    IsUserNewHire,
	                    SourceNodeID, 
	                    HierarchyTypeID, 
	                    FirstName, 
	                    LastName, 
	                    UserID,
                        ((SELECT     COUNT(1)
                            FROM         TrainingProgramToCourseLink
                            WHERE     T .TrainingProgramID = TrainingProgramID AND SyllabusTypeID IN (1, 4)) + MinimumElectives) 
                        AS MinimumCourseCount, 
                        MinimumElectives
                    FROM         
	                    (SELECT     
	                    program.CustomerID AS CustomerID, 
	                    program.ID AS TrainingProgramID, 
	                    program.[Name] AS TrainingProgramName, 
                        case 


                    when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0)
		                    then
			                    program.StartDate 
		                    else 



                    cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, '1/1/1900') as datetime)
		                    end as TrainingProgramStartDate,	
	                    case 


                    when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0) 
		                    then
			                    program.EndDate 
		                    else 



                    dateadd(d,customer.NewHireDuration, cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, '1/1/1900') as datetime))
		                    end as TrainingProgramEndDate,
	                    case 


                    when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0) 
		                    then
			                    program.DueDate 
		                    else



                    dateadd(d,customer.NewHireDuration, cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, '1/1/1900') as datetime))
		                    end as TrainingProgramDueDate, 
		                    program.Description AS TrainingProgramDescription, 
                                                                  
		                    program.MinimumElectives AS MinimumElectives, 
		                    program.EnforceRequiredOrder AS EnforceRequiredOrder, 
		                    program.IsNewHire AS IsNewHire,
		                    FullList.ID AS SourceNodeID, 
		                    link.HierarchyTypeID, 

                    COALESCE (locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) AS IsUserNewHire,
                    COALESCE (locationUsers.FirstName, jobroleUsers.FirstName, audienceUsers.FirstName, users.FirstName, NULL) AS FirstName, 


                    COALESCE (locationUsers.LastName, jobroleUsers.LastName, audienceUsers.LastName, users.LastName, NULL) AS LastName, 
		                    COALESCE (locationUsers.ID, jobroleUsers.ID, audienceUsers.ID, users.ID, NULL) AS UserID
                    FROM
	                    TrainingProgram program 
                    JOIN
	                    dbo.HierarchyToTrainingProgramLink link 
                    ON 
	                    link.TrainingProgramID = program.ID 
                    CROSS apply 
	                    dbo.fGetHierarchyBranch(link.HierarchyNodeID, link.HierarchyTypeID) AS FullList 
                    LEFT JOIN
                          dbo.[User] locationUsers ON FullList.ID = locationUsers.LocationID AND HierarchyTypeID = 1
                    LEFT JOIN
                          dbo.[User] jobroleUsers ON FullList.ID = jobroleUsers.JobRoleID AND HierarchyTypeID = 2 
                    LEFT JOIN
                          dbo.UserAudience ua ON FullList.ID = ua.AudienceID AND HierarchyTypeID = 3 
                    LEFT JOIN
                          dbo.[User] audienceUsers ON ua.UserID = audienceUsers.ID 
                    LEFT JOIN
                          dbo.[User] users ON users.ID = FullList.ID AND link.HierarchyTypeID = 4
                    left join
	                    dbo.Customer customer
                    on
	                    program.CustomerID = customer.ID
                    WHERE      

                    COALESCE (locationUsers.ID, jobroleUsers.ID, audienceUsers.ID, users.ID, NULL) IS NOT NULL AND program.IsLive = 1) 
                    AS T
                    -- where 
                    --		(	
                    --			-- all the single-hierarchy TP users
                    --			(	select 
                    --				count(httpl.ID) 
                    --				from HierarchyToTrainingProgramLink httpl 
                    --				where httpl.TrainingProgramID = T.TrainingProgramID 
                    --			)  = 1 
                    --			-- all the HierarchyTypes of User
                    --			 OR
                    --			
                    --				T.HierarchyTypeID = 4
                    --			
                    --			-- the first instance of each user in both hierarchies of a TP
                    --			--OR
                    --			--(
                    --			--	select Count(tpui.UserID) as UserIDCount 
                    --			--						from TrainingProgramUsersInclusive tpui 
                    --			---						where tpui.TrainingProgramID = T.TrainingProgramID
                    --			--						and tpui.userID = T.UserID
                    --			--) = 2
                    --		)
                    ;;");

        }
    }
}