﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration661 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER procedure [dbo].[GetUsersWithAssignmentsForTrainingProgram]
	(@trainingProgramId int, @sessionId int, @search nvarchar(50), @orderBy nvarchar(50), @orderDir varchar(4), @pageIndex int, @pageSize int)
as
begin
	declare @totalAssignments int 
	set @totalAssignments = 0;

	declare @totalMarkedTable table
	(
		TotalMarked int,
		UserID int
	)

	insert into @totalMarkedTable
	select
		COUNT(distinct sp.SectionID),	
		oa.UserID
	from OnlineCourseAssignments oa
	join ArtisanSectionPages sp on sp.PageID = oa.PageID
	where oa.TrainingProgramID in (
		select ID from TrainingProgram tp where
		tp.ID = @trainingProgramId or tp.ParentTrainingProgramID = @trainingProgramId
	) and oa.ResponseStatus > 1
	group by oa.UserID

	select 
		@totalAssignments = SUM(TotalAssignments)
	from TrainingProgramToCourseLink tpl
	join OnlineCourse o on tpl.CourseID = o.ID
	join (
	 select 
		c.ID as ArtisanCourseID, 
		count(distinct ssp.SectionID) as TotalAssignments
	from 
		ArtisanCourses c with (nolock)
	join 
		ArtisanSectionPages ssp 
	on 
		c.ID = ssp.CourseID
	join 
		ArtisanPages p 
	on 
		p.ID = ssp.PageID
	where 
		p.QuestionType = 8 and c.IsPublished = 1
	group by 
		c.ID
	) t on t.ArtisanCourseID = o.PublishedArtisanCourseID
	where tpl.TrainingProgramID = @trainingProgramId
	and tpl.CourseTypeID = 2

	;with UserAssignments as (select
		0 as AssignmentsAttempted, --Not needed
		tm.TotalMarked as AssignmentsMarked,-- number of assignments marked
		@totalAssignments as AssignmentsInCourse, -- Number of assignments in training program
		0 as AssignmentsCompleted, -- Not needed
		0 as IsAssignmentsFailed, -- Not needed
		0 as IsAssignmentsComplete, -- Not Needed
		MAX(o.GradedDate) as MaxGradedDate,
		MAX(o.[Date]) as MaxSubmitDate,
		MIN(o.ResponseStatus) as IsMarked,
		u.ID,
		u.FirstName,
		u.LastName,
		u.MiddleName,
		cl.ID as ClassID,
		cl.Name as ClassName,
		cmm.MinStartDateTime as ClassStartTime,
		row_number() over 
		(
			order by max(cmm.MinStartDateTime) desc, u.LastName, u.FirstName asc	
		)  as r
	from OnlineCourseAssignments o
	join TrainingProgram tp on o.TrainingProgramID = tp.ID
	left join Class cl on cl.CourseID = @sessionId
	left join Registration r on r.ClassID = cl.ID and r.RegistrantID = o.UserID
	left join ClassMinStartDateTime cmm on cl.ID = cmm.ClassID
	join [User] u on u.ID = o.UserID
	left join @totalMarkedTable tm on tm.UserID = u.ID
	where 
		(tp.ID = @trainingProgramId or tp.ParentTrainingProgramID = @trainingProgramId)
		and ( cmm.MinStartDateTime < DATEADD(day, 1, GETUTCDATE()) or cmm.MinStartDateTime is null)
		and (r.RegistrationStatusID = 4 or r.RegistrationStatusID is null)
		and (u.FirstName like '%' + @search + '%' or u.LastName like '%' + @search + '%' or cl.Name like '%' + @search + '%')
	group by
		u.ID,
		u.FirstName,
		u.LastName,
		u.MiddleName,
		cl.ID,
		cl.Name,
		cmm.MinStartDateTime,
		tm.TotalMarked
	)

	select 
		AssignmentsAttempted, --Not needed
		AssignmentsMarked,-- number of assignments marked
		AssignmentsInCourse, -- Number of assignments in training program
		AssignmentsCompleted, -- Not needed
		IsAssignmentsFailed, -- Not needed
		IsAssignmentsComplete, -- Not Needed
		MaxSubmitDate,
		MaxGradedDate,
		IsMarked,
		ID,
		FirstName,
		LastName,
		MiddleName,
		ClassID,
		ClassName,
		ClassStartTime,
		r,
		(select count(*) from UserAssignments) as TotalRows
	from
		UserAssignments
	where 
		r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
	order by r asc
		
end
");
        }
    }
}
