﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration762 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER procedure [dbo].[Library_GetLibraries]
	(
	@customerId int,
	@search nvarchar(50), 
	@orderBy nvarchar(50), 
	@orderDir varchar(4), 
	@pageIndex int, 
	@pageSize int,
	@isAllowSharing bit = 0
	)
as
begin 
	;with Libraries as (
        select 
	        l.Id,
            l.Name,
            l.LibraryItemTypeID,
            l.Details,
            l.[Description],
            l.CustomerID,
            l.Cost,
            l.Sku,
            l.CreatedOn,
            l.ModifiedOn,
            (select 
				count(li.ID) 
			from LibraryItem li
			left join
				OnlineCourse oc on li.ItemID = oc.ID and li.LibraryItemTypeID = 1
			left join 
				TrainingProgram tp on li.ItemID = tp.ID and li.LibraryItemTypeID = 2
			where 
				li.LibraryID = l.id and 
				li.LibraryItemTypeID = l.LibraryItemTypeID and
				coalesce(oc.CustomerID, tp.CustomerID) = l.CustomerID
			) as ItemCount,
            (select count(ID) from LibraryGrant where LibraryID = l.id) as RegistrationCount,
            cast(case when l.CustomerID != @customerId then 1 else 0 end as bit) as IsShared
        from 
		    Library l
		left join
			NetworkSharedEntity n 
				on n.EntityTypeID = 2 and n.EntityID = l.ID and @isAllowSharing = 1
		left join
			CustomerNetworkDetail d 
				on d.DetaildId = n.CustomerNetworkDetailId and @isAllowSharing = 1
	    where
		    l.CustomerID = @customerId or
		    (
				d.AllowLibraries = 1
				and
				d.DestinationCustomerID = @customerId
				and
				d.AllowLibraries = 1
		    )
	    group by
		    l.Id,
		    l.Name,
		    l.LibraryItemTypeID,
		    l.Details,
		    l.[Description],
		    l.Cost,
            l.Sku,
		    l.CustomerID, 
		    l.CreatedOn,
		    l.ModifiedOn
    )

    select
	    L.*,
	    (select count(id) from Libraries) as TotalRows
    from
	    ( 
		    select
			    Id,
			    Name,
			    LibraryItemTypeID,
			    CreatedOn,
			    ModifiedOn,
			    Details,
				[Description],
				CustomerID,
				Cost,
                Sku,
			    ItemCount,
			    RegistrationCount,
			    row_number() over 
			    (
				    order by
						case when @orderBy = 'Name' and @orderDir = 'asc' then Name end,
						case when @orderBy = 'Name' and @orderDir = 'desc' then Name end desc,
						case when @orderBy = 'LibraryItemTypeID' and @orderDir = 'asc' then LibraryItemTypeID end,
						case when @orderBy = 'LibraryItemTypeID' and @orderDir = 'desc' then LibraryItemTypeID end desc,
						case when @orderBy = 'CreatedOn' and @orderDir = 'asc' then CreatedOn end,
						case when @orderBy = 'CreatedOn' and @orderDir = 'desc' then CreatedOn end desc,
						case when @orderBy = 'ModifiedOn' and @orderDir = 'asc' then ModifiedOn end,
						case when @orderBy = 'ModifiedOn' and @orderDir = 'desc' then ModifiedOn end desc,
						case when @orderBy = 'ItemCount' and @orderDir = 'asc' then ItemCount end,
						case when @orderBy = 'ItemCount' and @orderDir = 'desc' then ItemCount end desc,
						case when @orderBy = 'RegistrationCount' and @orderDir = 'asc' then RegistrationCount end,
						case when @orderBy = 'RegistrationCount' and @orderDir = 'desc' then RegistrationCount end desc,
						case when @orderBy = 'IsShared' and @orderDir = 'asc' then IsShared end,
						case when @orderBy = 'IsShared' and @orderDir = 'desc' then IsShared end desc
			    ) as r,
			    IsShared
		    from Libraries
		    where Name like '%' + @search + '%'
	    ) L
    where
	    L.r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
end
");
        }
    }
}
