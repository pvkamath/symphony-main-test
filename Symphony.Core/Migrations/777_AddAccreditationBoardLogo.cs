﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration777 : Migration
    {
        public override void Up()
        {
            AddColumn("AccreditationBoard", "Logo", DbType.String, 32768);
        }
    }
}
