﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration136 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER VIEW [dbo].[LatestPublishedCourses] as
                        SELECT c.*
                        FROM ArtisanCourses c
                        INNER JOIN 
                        (
                          SELECT OriginalCourseId, CustomerID, MAX(CreatedOn) 'maxdate' 
                          FROM ArtisanCourses 
                          WHERE OriginalCourseId != 0 and OriginalCourseID  is not null and IsPublished = 1
                          GROUP BY CustomerID, OriginalCourseId
                        ) c2 
                        ON c.OriginalCourseId = c2.OriginalCourseId AND c.CreatedOn = c2.maxdate and c.CustomerID = c2.CustomerID
                        WHERE IsPublished = 1;
            ");
        }
    }
}