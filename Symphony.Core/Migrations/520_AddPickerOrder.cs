﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration520 : Migration
    {

        public override void Up()
        {
            AddColumn("ReportEntity", "DefaultOrder", DbType.Int32, 0, false, "1000");
            AddColumn("ReportTemplateEntity", "Order", DbType.Int32, 0, false, "0"); 
        }

    }
}