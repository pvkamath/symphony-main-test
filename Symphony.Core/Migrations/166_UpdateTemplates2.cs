﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration166 : Migration
    {
        public override void Up()
        {
            Execute(@"update ArtisanAssetTypes set Template = 
                '<div class=""video-container"" style=""width:{width}px;height:{height}px""><video id=""video-{id}"" class=""video-js vjs-default-skin"" controls preload=""auto"" width=""{width}"" height=""{height}"" data-setup=""{}""><source src=""{path}"" type=""video/mp4""></video></div>'
            where Name = 'Video'");
        }
    }
}