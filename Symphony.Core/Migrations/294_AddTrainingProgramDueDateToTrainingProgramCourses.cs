﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration294 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER view [dbo].[TrainingProgramCourses] as
-- this query simply collapses out the individual classes from the [TrainingProgramCoursesAndClasses] view
-- so we get a list of only the actual courses
select
	ROW_NUMBER() over (order by TrainingProgramID, SortOrder) as ID,
	CustomerID,
	TrainingProgramID,
	TrainingProgramName,
	SyllabusTypeID,
	ActiveAfterDate,
	ProgramStartDate,
    ProgramDueDate,
	ProgramEndDate,
	CourseID,
	CourseTypeID,
	CourseName,
	EnforceRequiredSortOrder,
	SortOrder
from
	[TrainingProgramCoursesAndClasses]
group by
	CustomerID,
	TrainingProgramID,
	TrainingProgramName,
	SyllabusTypeID,
	ActiveAfterDate,
	ProgramStartDate,
    ProgramDueDate,
	ProgramEndDate,
	CourseID,
	CourseTypeID,
	CourseName,
	EnforceRequiredSortOrder,
	SortOrder
;
");
        }
    }
}
