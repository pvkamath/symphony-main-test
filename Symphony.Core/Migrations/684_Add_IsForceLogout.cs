﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration684 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "IsForceLogout", DbType.Boolean, 0, true);
            AddColumn("OnlineCourse", "IsForceLogoutOverride", DbType.Boolean, 0, true);
            AddColumn("TrainingProgram", "IsForceLogoutOverride", DbType.Boolean, 0, true);
            AddColumn("Customer", "IsForceLogoutOverride", DbType.Boolean, 0, true);
        }
    }
}