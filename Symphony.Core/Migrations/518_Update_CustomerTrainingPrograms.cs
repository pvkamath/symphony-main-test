﻿using SubSonic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Symphony.Core.Migrations
{
    public class Migration518
 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER VIEW [dbo].[CustomerTrainingPrograms]
AS  
SELECT DISTINCT
	[ID], [CustomerID], [OwnerID], [Name], [InternalCode], [Cost], [Description], [IsNewHire],
	[IsLive], [StartDate], [EndDate], [DueDate], [EnforceRequiredOrder], [MinimumElectives], [FinalAssessmentCourseID],
	[FinalAssessmentCourseTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn], [CategoryID],
	[NewHireStartDateOffset], [NewHireDueDateOffset], [NewHireEndDateOffset], 
	[NewHireOffsetEnabled], [NewHireTransitionPeriod], [DisableScheduled], 
	[CreatedByUserId], [ModifiedByUserId], [CreatedByActualUserId], [ModifiedByActualUserId], [PartnerID],
	[IsSalesforce], [EnforceCanMoveForwardIndicator],[SurveyID], [IsSurveyMandatory], [IsValidationEnabled],
	[ValidationInterval], [MaxCourseWork], [ShowPretestOverride], [ShowPostTestOverride],
	[PassingScoreOverride], [SkinOverride], [PreTestTypeOverride], [PostTestTypeOverride],
	[NavigationMethodOverride], [DisclaimerOverride], [ContactEmailOverride], [ShowObjectivesOverride],
	[PretestTestOutOverride], [Retries], [RandomizeQuestionsOverride], [RandomizeAnswersOverride], [ReviewMethodOverride],
	[MarkingMethodOverride], [InactivityMethodOverride], [InactivityTimeoutOverride], [MinimumPageTimeOverride],
	[MaximumQuestionTimeOverride], [CertificateEnabledOverride], [ExternalID], [AffidavitID], [MaxTimeOverride], 
	[CertificatePath], [MinLoTimeOverride], [MinScoTimeOverride], [MinTimeOverride], [MetaDataJson], [Sku],
	[PurchasedFromBundleID], [ParentTrainingProgramID], [IsCourseTimingDisabled], 
	[SessionCourseID], [IsNmls], [SessionTimeout], [CourseUnlockModeID], [SharedFlag], [SourceCustomerId],
    [TrainingProgramAffidavitID]
FROM     
(
	SELECT ROW_NUMBER() OVER ( ORDER BY Name ASC) AS Row, 
		[dbo].[TrainingProgram].[ID], [dbo].[TrainingProgram].[CustomerID], [dbo].[TrainingProgram].[OwnerID], [dbo].[TrainingProgram].[Name], 
		[dbo].[TrainingProgram].[InternalCode], [dbo].[TrainingProgram].[Cost], [dbo].[TrainingProgram].[Description], [dbo].[TrainingProgram].[IsNewHire], 
		[dbo].[TrainingProgram].[IsLive], [dbo].[TrainingProgram].[StartDate], [dbo].[TrainingProgram].[EndDate], [dbo].[TrainingProgram].[DueDate], 
		[dbo].[TrainingProgram].[EnforceRequiredOrder], [dbo].[TrainingProgram].[MinimumElectives], [dbo].[TrainingProgram].[FinalAssessmentCourseID], 
		[dbo].[TrainingProgram].[FinalAssessmentCourseTypeID], [dbo].[TrainingProgram].[ModifiedBy], [dbo].[TrainingProgram].[CreatedBy], 
		[dbo].[TrainingProgram].[ModifiedOn], [dbo].[TrainingProgram].[CreatedOn], [dbo].[TrainingProgram].[CategoryID], 
		[dbo].[TrainingProgram].[NewHireStartDateOffset], [dbo].[TrainingProgram].[NewHireDueDateOffset], [dbo].[TrainingProgram].[NewHireEndDateOffset], 
		[dbo].[TrainingProgram].[NewHireOffsetEnabled], [dbo].[TrainingProgram].[NewHireTransitionPeriod], [dbo].[TrainingProgram].[DisableScheduled], 
		[dbo].[TrainingProgram].[CreatedByUserId], [dbo].[TrainingProgram].[ModifiedByUserId], [dbo].[TrainingProgram].[CreatedByActualUserId], 
		[dbo].[TrainingProgram].[ModifiedByActualUserId], [dbo].[TrainingProgram].[PartnerID], [dbo].[TrainingProgram].[IsSalesforce], 
		[dbo].[TrainingProgram].[EnforceCanMoveForwardIndicator], [dbo].[TrainingProgram].[SurveyID], [dbo].[TrainingProgram].[IsSurveyMandatory], [dbo].[TrainingProgram].[IsValidationEnabled], 
		[dbo].[TrainingProgram].[ValidationInterval], [dbo].[TrainingProgram].[MaxCourseWork], [dbo].[TrainingProgram].[ShowPretestOverride], [dbo].[TrainingProgram].[ShowPostTestOverride], 
		[dbo].[TrainingProgram].[PassingScoreOverride], [dbo].[TrainingProgram].[SkinOverride], [dbo].[TrainingProgram].[PreTestTypeOverride], [dbo].[TrainingProgram].[PostTestTypeOverride], 
		[dbo].[TrainingProgram].[NavigationMethodOverride], [dbo].[TrainingProgram].[DisclaimerOverride], [dbo].[TrainingProgram].[ContactEmailOverride], [dbo].[TrainingProgram].[ShowObjectivesOverride], 
		[dbo].[TrainingProgram].[PretestTestOutOverride], [dbo].[TrainingProgram].[Retries], 
		[dbo].[TrainingProgram].[RandomizeQuestionsOverride], [dbo].[TrainingProgram].[RandomizeAnswersOverride], [dbo].[TrainingProgram].[ReviewMethodOverride], [dbo].[TrainingProgram].[MarkingMethodOverride], 
		[dbo].[TrainingProgram].[InactivityMethodOverride], [dbo].[TrainingProgram].[InactivityTimeoutOverride], [dbo].[TrainingProgram].[MinimumPageTimeOverride], 
		[dbo].[TrainingProgram].[MaximumQuestionTimeOverride], [dbo].[TrainingProgram].[CertificateEnabledOverride], [dbo].[TrainingProgram].[ExternalID], 
		[dbo].[TrainingProgram].[AffidavitID], [dbo].[TrainingProgram].[MaxTimeOverride], [dbo].[TrainingProgram].[CertificatePath], [dbo].[TrainingProgram].[MinLoTimeOverride], 
		[dbo].[TrainingProgram].[MinScoTimeOverride], [dbo].[TrainingProgram].[MinTimeOverride], [dbo].[TrainingProgram].[MetaDataJson], [dbo].[TrainingProgram].[Sku], 
		[dbo].[TrainingProgram].[PurchasedFromBundleID], [dbo].[TrainingProgram].[ParentTrainingProgramID], [dbo].[TrainingProgram].[IsCourseTimingDisabled], 
		[dbo].[TrainingProgram].[SessionCourseID], [dbo].[TrainingProgram].[IsNmls], [dbo].[TrainingProgram].[SessionTimeout], [dbo].[TrainingProgram].[CourseUnlockModeID],
		CAST(0 AS BIT) AS 'SharedFlag', 0 AS 'SourceCustomerId', 
        [dbo].[TrainingProgram].[TrainingProgramAffidavitID]
	FROM [dbo].[TrainingProgram]
	UNION
		SELECT ROW_NUMBER() OVER ( ORDER BY Name ASC) AS Row, 
		tp.[ID], alltp.[InitCustomerId] AS 'CustomerId', tp.[OwnerID], tp.[Name], 
		tp.[InternalCode], tp.[Cost], tp.[Description], tp.[IsNewHire], 
		tp.[IsLive], tp.[StartDate], tp.[EndDate], tp.[DueDate], 
		tp.[EnforceRequiredOrder], tp.[MinimumElectives], tp.[FinalAssessmentCourseID], 
		tp.[FinalAssessmentCourseTypeID], tp.[ModifiedBy], tp.[CreatedBy], 
		tp.[ModifiedOn], tp.[CreatedOn], tp.[CategoryID], 
		tp.[NewHireStartDateOffset], tp.[NewHireDueDateOffset], tp.[NewHireEndDateOffset], 
		tp.[NewHireOffsetEnabled], tp.[NewHireTransitionPeriod], tp.[DisableScheduled], 
		tp.[CreatedByUserId], tp.[ModifiedByUserId], tp.[CreatedByActualUserId], 
		tp.[ModifiedByActualUserId], tp.[PartnerID], tp.[IsSalesforce], 
		tp.[EnforceCanMoveForwardIndicator], tp.[SurveyID], tp.[IsSurveyMandatory], tp.[IsValidationEnabled], 
		tp.[ValidationInterval], tp.[MaxCourseWork], tp.[ShowPretestOverride], tp.[ShowPostTestOverride], 
		tp.[PassingScoreOverride], tp.[SkinOverride], tp.[PreTestTypeOverride], tp.[PostTestTypeOverride], 
		tp.[NavigationMethodOverride], tp.[DisclaimerOverride], tp.[ContactEmailOverride], tp.[ShowObjectivesOverride], 
		tp.[PretestTestOutOverride], tp.[Retries], 
		tp.[RandomizeQuestionsOverride], tp.[RandomizeAnswersOverride], tp.[ReviewMethodOverride], tp.[MarkingMethodOverride], 
		tp.[InactivityMethodOverride], tp.[InactivityTimeoutOverride], tp.[MinimumPageTimeOverride], 
		tp.[MaximumQuestionTimeOverride], tp.[CertificateEnabledOverride], tp.[ExternalID], 
		tp.[AffidavitID], tp.[MaxTimeOverride], tp.[CertificatePath], tp.[MinLoTimeOverride], 
		tp.[MinScoTimeOverride], tp.[MinTimeOverride], tp.[MetaDataJson], tp.[Sku], 
		tp.[PurchasedFromBundleID], tp.[ParentTrainingProgramID], tp.[IsCourseTimingDisabled], 
		tp.[SessionCourseID], tp.[IsNmls], tp.[SessionTimeout], tp.[CourseUnlockModeID],
		CAST(1 AS BIT) AS 'SharedFlag', alltp.ReleatedCustomerId as 'SourceCustomerId',
        tp.[TrainingProgramAffidavitID]
	FROM
		dbo.TrainingProgram tp
		INNER JOIN (
		SELECT 
			tpshared.ID, g.InitCustomerId, g.ReleatedCustomerId 
		FROM 
			[dbo].[TrainingProgram] tpshared
			CROSS APPLY dbo.fGetRelatedTrainingPrograms(tpshared.CustomerId) g
				) alltp ON alltp.ReleatedCustomerId = tp.CustomerID
) TP
");
        }
    }
}
