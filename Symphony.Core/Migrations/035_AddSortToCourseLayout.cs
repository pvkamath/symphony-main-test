﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration035 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "Sort", DbType.Int32, 0, false);
            AddColumn("ArtisanPages", "Sort", DbType.Int32, 0, false);
        }
    }
}