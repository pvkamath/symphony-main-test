﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration356 : Migration
    {
        public override void Up()
        {
            Execute(@"insert into ArtisanQuestionTypes (
                        Name, 
                        DisplayName, 
                        ModifiedBy, 
                        CreatedBy, 
                        ModifiedOn, 
                        CreatedOn, 
                        CreatedByUserId, 
                        ModifiedByUserId)   
                values (
                    'LongAnswer', 
                    'Long Answer', 
                    '',
                    '', 
                    '" + DateTime.Now.ToString("yyyy-MM-dd") + @"',
                    '" + DateTime.Now.ToString("yyyy-MM-dd") + @"', 
                    0, 
                    0)");
            Execute(@"insert into ArtisanTemplates (
                CustomerID, 
                Name, 
                Description, 
                Html, 
                ThumbPath, 
                PageType, 
                QuestionType, 
                ModifiedBy,  
                CreatedBy,   
                ModifiedOn,  
                CreatedOn,  
                CssText,  
                CreatedByUserId,  
                ModifiedByUserId) values ( 
                    0,  
                    'Long Answer',  
                    'Text-based question template with a long answer response field. Gradeable by instructor only.', 
                    '',
                    '/images/artisan/templates/long_answer_icon.gif',  
                    2,  
                    (SELECT ID From ArtisanQuestionTypes where Name = 'LongAnswer'),  
                    '',  
                    '',
                    '" + DateTime.Now.ToString("yyyy-MM-dd") + @"',
                    '" + DateTime.Now.ToString("yyyy-MM-dd") + @"',
                    '',
                    0,
                    0
            )");
        
        }
    }
}