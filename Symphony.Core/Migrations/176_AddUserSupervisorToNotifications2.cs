﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration176 : Migration
    {
        public override void Up()
        {
            Execute(@"
insert RecipientGroups (Selector, DisplayName, ValidForType) values ('GetAssignedUsersSupervisors()','Assigned Users Classroom Supervisors', 'TrainingProgramToCourseLink')
insert RecipientGroups (Selector, DisplayName, ValidForType) values ('GetAssignedUsersReportingSupervisors()','Assigned Users Reporting Supervisors', 'TrainingProgramToCourseLink')
");
        }
    }
}