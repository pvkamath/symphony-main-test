﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration523 : Migration
    {

        public override void Up()
        {
            Execute(@"
    update ReportEntity set [DefaultOrder] = ID + 1000;
    update ReportEntity set [DefaultOrder] = 1000 where xtype = 'reporting.customerpicker';

");
        }

    }
}