﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration045 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(594);
            sb.AppendFormat(@"CREATE TABLE [dbo].[PublicDocuments]({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[ID] [int] IDENTITY(1,1) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[Filename] [nvarchar](100) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[Name] [nvarchar](50) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[Description] [nvarchar](500) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[CustomerID] [int] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[Uploaded] [datetime] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[CreatedOn] [datetime] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[ModifiedOn] [datetime] NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[CreatedBy] [nvarchar](50) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[ModifiedBy] [nvarchar](50) NOT NULL,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@" CONSTRAINT [PK_PublicDocuments] PRIMARY KEY CLUSTERED {0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[ID] ASC{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ");
            sb.AppendFormat(@"ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]{0}", Environment.NewLine);
            sb.AppendFormat(@") ON [PRIMARY]{0}", Environment.NewLine);

            sb.AppendFormat(@"CREATE NONCLUSTERED INDEX [IX_CustomerID] ON [dbo].[PublicDocuments] {0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[CustomerID] ASC{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ");
            sb.AppendFormat(@"DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]{0}", Environment.NewLine);

            sb.AppendFormat(@"CREATE NONCLUSTERED INDEX [IX_Name] ON [dbo].[PublicDocuments] {0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[Name] ASC{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ");
            sb.AppendFormat(@"DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]{0}", Environment.NewLine);


            sb.AppendFormat(@"CREATE NONCLUSTERED INDEX [IX_Description] ON [dbo].[PublicDocuments] {0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[Description] ASC{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ");
            sb.AppendFormat(@"DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]{0}", Environment.NewLine);

            Execute(sb.ToString());
        }
    }
}