﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration059 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(901);
            sb.AppendFormat(@"if((select count(*) from CourseCategories) = 0){0}", Environment.NewLine);
            sb.AppendFormat(@"begin {0}", Environment.NewLine);
            sb.AppendFormat(@"{0}declare @customerId int{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}declare @categoryId int{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}declare category_cursor cursor for{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}select{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}id{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}from{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}Customer{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}open category_cursor{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}fetch next from category_cursor into @customerId{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}while @@FETCH_STATUS = 0{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}begin{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}insert into {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}CourseCategories (Name, [Description], CreatedBy, ModifiedBy, CustomerID, CreatedOn, ModifiedOn) {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}values {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}('Default', 'Default Category', 'system', 'system', @customerId, GETDATE(), GETDATE()){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}set @categoryId = (select ID from CourseCategories where CustomerID = @customerId) {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}update Course set CategoryID = @categoryId where CustomerID = @customerId{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}update OnlineCourse set CategoryID = @categoryId where CustomerID = @customerId{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}fetch next from category_cursor into @customerId{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}end{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}close category_cursor{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}deallocate category_cursor{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"end");
            Execute(sb.ToString());
        }
    }
}