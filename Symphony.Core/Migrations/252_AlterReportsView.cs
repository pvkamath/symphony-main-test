﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration252 : Migration
    {
        public override void Up()
        {
          /*  Execute(@"
--exec('CREATE SCHEMA [MYREPORTSCHEMA2] AUTHORIZATION [dbo]

alter VIEW [dbo].[ReportQueueStatuses]
AS
SELECT [ID]
      ,[ReportId]  
      ,[QueueId]   -- number of time submitted
      ,[ReportName]
      ,[CustomerID]
      ,[DownloadCSV]
      ,[DownloadXLS]
      ,[DownloadPDF]
      ,[StatusType] as StatusId 
      ,[ErrorType]
      ,[CompletedDate]  as FinishedOn
      ,[IsEnabled]
      ,[CreatedOn] as StartedOn
      ,[ModfiedOn]
  FROM [SymphonyRptReplication].[dbo].[RptQueueStatus]  

--GO')
");*/
        }
    }
}