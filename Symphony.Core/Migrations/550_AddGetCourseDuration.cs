﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration550 : Migration
    {

        public override void Up()
        {
            // Navigation percentage
            Execute(@"
CREATE procedure [dbo].[GetCourseDuration](@artisanCourseID int)
as
begin
    declare @totalMinCourseTime int
	declare @totalMinScoTime int
	declare @totalMinLoTime int
	declare @totalMinSectionPageTime int


	select
		@totalMinCourseTime = max(sectionTimes.MinCourseTime),
		@totalMinScoTime = sum(
			case
				when (
					sectionTimes.SectionType = 1
				) then
					sectionTimes.MinScoTime
				else
					0
			end
		),
		@totalMinLoTime = sum(
			case
				when (
					(sectionTimes.SectionType = 2 or 
					 sectionTimes.sectionType = 5 or 
					 sectionTimes.sectionType = 6)
					and sectionTimes.Pages > 0
				) then
					sectionTimes.MinLoTime
				else
					0
			end
		),
		@totalMinSectionPageTime = sum(
			case
				when (
					(sectionTimes.SectionType = 2 or 
					 sectionTimes.sectionType = 5 or 
					 sectionTimes.sectionType = 6)
					and sectionTimes.Pages > 0
				) then
					sectionTimes.MinSectionPageTime
				else
					0
			end
		)
	from
	(
		select
			pageTimes.SectionID,
			pageTimes.IsQuiz,
			pageTimes.SectionType,
			pageTimes.MaxQuestions,
			pageTimes.IsSinglePage,
			pageTimes.MinLoTime,
			pageTimes.MinScoTime,
			pageTimes.MinTime as MinCourseTime,
			count(pageTimes.PageID) as Pages,
			case 
				when 
				(
					(pageTimes.IsQuiz = 1 or pageTimes.SectionType = 5 or pageTimes.SectionType = 6)
					and pageTimes.MaxQuestions > 0
				)
				then
					pageTimes.MaxQuestions * pageTimes.SectionPageTime
				else
					sum(pageTimes.PageTime)
			end as MinSectionPageTime
		from
		(
			select 
				a.MinTime,
				s.ID as SectionID,
				s.IsQuiz,
				s.TestType,
				s.SectionType,
				s.MaxQuestions,
				s.IsSinglePage,
				coalesce(
					NULLIF(s.MinLoTime, 0),
					NULLIF(a.MinLoTime, 0),
					0
				) as MinLoTime,
				coalesce(
					NULLIF(s.MinScoTime, 0),
					NULLIF(a.MinScoTime, 0),
					0
				) as MinScoTime,
				p.ID as PageID,
				coalesce(
					NULLIF(s.MinimumPageTime, 0),
					NULLIF(a.MinimumPageTime, 0),
					0
				) as SectionPageTime,
				coalesce(
					NULLIF(p.MinimumPageTime, 0),
					NULLIF(s.MinimumPageTime, 0),
					NULLIF(a.MinimumPageTime, 0),
					0
				) as PageTime
			from ArtisanCourses a
			join ArtisanSections s on s.CourseID = a.ID
			left join ArtisanSectionPages sp on sp.SectionID = s.ID
			left join ArtisanPages p on p.ID = sp.PageID 
			where a.ID = @artisanCourseID
		) pageTimes
		group by pageTimes.MinTime, pageTimes.SectionID, pageTimes.IsQuiz, pageTimes.TestType, pageTimes.SectionType, pageTimes.MaxQuestions, pageTimes.IsSinglePage, pageTimes.SectionPageTime, pageTimes.MinLoTime, pageTimes.MinScoTime
	) sectionTimes

	select ceiling(max(T.Total) / 60.00) as CourseDuration
	from (
		select @totalMinCourseTime as Total
		union
		select @totalMinScoTime as Total
		union
		select @totalMinLoTime as Total
		union
		select @totalMinSectionPageTime as Total
	) as T
end;
");
        }

    }
}
