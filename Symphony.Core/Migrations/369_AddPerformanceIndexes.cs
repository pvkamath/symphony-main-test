﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration369 : Migration
    {
        public override void Up()
        {
            Execute(@"
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IDX_Performance' AND object_id = OBJECT_ID('OnlineCourse'))
BEGIN
	CREATE NONCLUSTERED INDEX [IDX_Performance] ON [dbo].[OnlineCourse] 
	(
		[ID] ASC,
		[CustomerID] ASC,
		[Name] ASC,
		[Cost] ASC,
		[Credit] ASC,
		[PublicIndicator] ASC,
		[IsSurvey] ASC,
		[IsTest] ASC,
		[InternalCode] ASC,
		[ProctorRequiredIndicator] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IDX_Keywords' AND object_id = OBJECT_ID('ArtisanCourses'))
BEGIN
	CREATE NONCLUSTERED INDEX [IDX_Keywords] ON [dbo].[ArtisanCourses] 
	(
		[ID] ASC,
		[CustomerID] ASC,
		[Name] ASC,
		[IsPublished] ASC,
		[IsDeleted] ASC,
		[IsAutoPublish] ASC
	)
	INCLUDE ( [Keywords]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END

");
        }
    }
}