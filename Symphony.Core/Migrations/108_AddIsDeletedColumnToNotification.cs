﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration108 : Migration
    {
        public override void Up()
        {
            AddColumn("Notifications", "IsDeleted", DbType.Boolean, 0, false, "0");
        }
    }
}