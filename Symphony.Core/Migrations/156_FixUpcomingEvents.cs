﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration156 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER view [dbo].[SimpleUpcomingEvents] 
as
	
	
	-- get the meetings
	select
		u.customerid as CustomerID,		
		m.ID as EventID, 
		m.ID as GroupID, 
		'' as WebinarKey,
		'' as WebinarRegistrationID,
		'meeting' as EventType,
		2 as EventTypeID,
		m.[Subject] as EventName, 
		m.UserID, 
		m.StartDateTime as StartDate, 
		m.EndDateTime as EndDate,
		m.JoinURL as JoinURL
	from
		MyGTMMeetings m
	join
		[user] u
	on
		u.Id = m.UserID
		
	union
	
	-- get classes being taught
	select
		c.CustomerID as CustomerID,
		c.ID as EventID,
		course.ID as GroupID,
		c.WebinarKey,
		'[NA]' as WebinarRegistrationID,
		'[Instructor] ' + isnull(p.[Description],'Class') as EventType,
		3 as EventTypeID,
		c.Name as EventName,
		ci.InstructorID as UserID,
		StartDateTime as StartDate,
		DATEADD(minute, Duration, StartDateTime) as EndDate,
		'' as JoinURL
	from
		ClassInstructors ci
	join
		Class c
	on
		ci.ClassID = c.ID
	join
		Course course
	on
		course.ID = c.CourseID
	left join
		PresentationType p
	on
		course.PresentationTypeID = p.ID
	-- we can show multiple items in the upcoming events because
	-- each class is...well, upcoming
	join
		ClassDate cd
	on
		cd.ClassID = c.ID
		
	union
			
	-- add public classroom courses that the user is *not* registered for
	select
		c.CustomerID as CustomerID,
		c.ID as EventID,
		course.ID as GroupID,
		'',
		'[NA]' as WebinarRegistrationID,
		isnull(p.[Description],'Public Class') as EventType,
		4 as EventTypeID,
		c.Name as EventName,
		coalesce(reg.RegistrantID,0) as UserID,
		StartDateTime as StartDate,
		DATEADD(minute, Duration, StartDateTime) as EndDate,
		'' as JoinURL
	from
		Class c
	left join
		Registration reg
	on
		reg.ClassID = c.ID
	join
		Course course
	on
		course.ID = c.CourseID
	left join
		PresentationType p
	on
		course.PresentationTypeID = p.ID
	-- we can show multiple items in the upcoming events because
	-- each class is...well, upcoming
	join
		ClassDate cd
	on
		cd.ClassID = c.ID
	-- only show classes for which they're not registered
	--where
	--	RegistrationStatusID != 4 or RegistrationStatusID is null
	and
		course.PublicIndicator = 1
");
        }
    }
}