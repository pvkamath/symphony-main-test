﻿using SubSonic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Symphony.Core.Migrations
{
    class Migration508 : Migration
    {
        public override void Up()
        {
			Execute(@"
CREATE FUNCTION [dbo].[fGetRelatedTrainingPrograms]
(
	@CustomerId INT
)
RETURNS @Tr TABLE (TrainingProgramId INT, InitCustomerId INT, ReleatedCustomerId INT)
AS
BEGIN
	INSERT INTO @Tr (TrainingProgramId, InitCustomerId, ReleatedCustomerId)
	SELECT
		tp.ID, @CustomerId, tp.CustomerID
	FROM
		dbo.TrainingProgram tp
		INNER JOIN dbo.CustomerNetworkDetail cnd ON cnd.DestinationCustomerId = @CustomerId AND cnd.SourceCustomerId = tp.CustomerID AND cnd.AllowTrainingProgramSharing = 1
		
	RETURN;
END
			");

        }
    }
}
