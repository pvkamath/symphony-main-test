﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration199 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanAssetTypes", "HasAlternateHtml", DbType.Boolean, 0, false, "0");

            AddColumn("ArtisanAssets", "AlternateHtml", DbType.String, 9000, true);
        }
    }
}