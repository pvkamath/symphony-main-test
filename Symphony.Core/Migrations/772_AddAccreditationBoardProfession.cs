﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration772 : Migration
    {
        public override void Up()
        {
            TableSchema.Table accreditation = CreateTableWithKey("AccreditationBoardProfession", "ID");

            accreditation.AddColumn("AccreditationBoardID", DbType.Int32, 0, false);
            accreditation.AddColumn("ProfessionID", DbType.Int32, 0, false);

            Execute("ALTER TABLE AccreditationBoardProfession"
                + " ADD CONSTRAINT fk_AccreditationBoard_ID_AccreditationBoardProfession_AccreditationBoardID FOREIGN KEY(AccreditationBoardID)"
                    + " REFERENCES AccreditationBoard(ID)"
                );

            Execute("ALTER TABLE AccreditationBoardProfession"
                + " ADD CONSTRAINT fk_Profession_ID_AccreditationBoardProfession_ProfessionID FOREIGN KEY(ProfessionID)"
                    + " REFERENCES Profession(ID)"
                );

            AddSubSonicStateColumns(accreditation);
        }
    }
}
