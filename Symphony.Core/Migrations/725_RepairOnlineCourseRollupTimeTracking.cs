﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration725 : Migration
    {
       
        public override void Up()
        {
            // This will fix our existing rollups to ensure that what we have recorded in our online course rollup table
            // matches to what is reported in the scorm database
            Execute(@"
-- UPDATES TOTAL TIME
-- Set OnlineCourseRollup.TotalTime to the total time the user has spent in this course across all deployed versions
update
	rl
set
	rl.TotalSeconds = sc.TotalSecondsTracked
from
	OnlineCourseRollup rl
join 
(
	select t.course_id, t.training_program_id, t.user_id, t.TotalTimeTracked / 100 as TotalSecondsTracked from
	(
		select
			sr.course_id,
			sr.training_program_id,
			sr.user_id,
			sum(art.total_time_tracked) as TotalTimeTracked
		from
		OnlineTraining.dbo.ScormRegistration sr
		join OnlineTraining.dbo.ScormActivity a on a.scorm_registration_id = sr.scorm_registration_id
		join OnlineTraining.dbo.ScormActivityRT art on art.scorm_activity_id = a.scorm_activity_id
		group by sr.course_id, sr.training_program_id, sr.user_id
	) t
) sc on sc.course_id = rl.CourseID and sc.training_program_id = rl.TrainingProgramID and sc.user_id = rl.UserID

-- UPDATE PREVIOUS TIME
-- Set OnlineCourseRollup.TotalSecondsPreviousRegistration to the total time the user has spent in previous versions of the course
update
	rl
set
	rl.TotalSecondsPreviousRegistration = sc.TotalSecondsTracked
from
	OnlineCourseRollup rl
join
(
	select 
		t.course_id, t.training_program_id, t.user_id, t.TotalTimeTracked / 100 as TotalSecondsTracked
	from
	(
		select
			sr.course_id,
			sr.training_program_id,
			sr.user_id,
			sum(art.total_time_tracked) as TotalTimeTracked
		from
		OnlineTraining.dbo.ScormRegistration sr
		join OnlineTraining.dbo.ScormActivity a on a.scorm_registration_id = sr.scorm_registration_id
		join OnlineTraining.dbo.ScormActivityRT art on art.scorm_activity_id = a.scorm_activity_id
		-- Exclude the max registration id for each
		where sr.scorm_registration_id not in (
			select
				max(sr.scorm_registration_id) as CurrentRegistrationID
			from
				OnlineTraining.dbo.ScormRegistration sr
			group by
				sr.training_program_id, sr.course_id, sr.user_id
		)
		group by sr.course_id, sr.training_program_id, sr.user_id
	) t
) sc on sc.course_id = rl.CourseID and sc.training_program_id = rl.TrainingProgramID and sc.user_id = rl.UserID
-- UPDATE CURRENT REGISTRATION ID
-- Set OnlineCourseRollup.CurrentRegistrationID to max registration id for the course.
update
	rl
set
	rl.CurrentRegistrationID = sr.CurrentRegistrationID
from
	OnlineCourseRollup rl
join
(
	select
		sr.training_program_id,
		sr.course_id, 
		sr.user_id,
		max(sr.scorm_registration_id) as CurrentRegistrationID
	from
		OnlineTraining.dbo.ScormRegistration sr
	group by
		sr.training_program_id, sr.course_id, sr.user_id
) sr on rl.TrainingProgramID = sr.training_program_id and rl.CourseID = sr.course_id and rl.UserID = sr.user_id
-- UPDATE TRAINING PROGRAM ROLLUP
-- Set TrainingProgramRollup.TotalSeconds to the total amount of time spent in required courses
update
	tpr
set
	tpr.TotalSeconds = rl.TotalSeconds
from
	TrainingProgramRollup tpr
join (
	select
		rl.UserID,
		rl.TrainingProgramID,
		sum(rl.TotalSeconds) as TotalSeconds
	from OnlineCourseRollup rl
	join TrainingProgramToCourseLink tl
		on tl.CourseTypeID = 2
		and tl.SyllabusTypeID = 1
		and tl.CourseID = rl.CourseID
		and tl.TrainingProgramID = rl.TrainingProgramID
	group by
		rl.UserID,
		rl.TrainingProgramID
) rl on rl.UserID = tpr.UserID and rl.TrainingProgramID = tpr.TrainingProgramID



");
        }

    }
}