﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration642 : Migration
    {
        public override void Up()
        {
            Execute(@"

ALTER FUNCTION [dbo].fGetLicenseHierarchyBranch
(   
    --the node that'll be considered the root node of the branch
    @iLicenseID int
)
RETURNS TABLE
AS
RETURN
(
WITH License_CTE (ID, ParentID, Name, Level, LevelIndentText, LevelText, RootLicenseID)
AS
(
-- Anchor member definition
    SELECT
            l.ID,
            l.ParentID,
            l.Name,
            0 AS Level,
            CAST(l.Name as nvarchar(max)) as LevelIndentText,
            CAST(l.Name as nvarchar(max)) as LevelText,
            l.ID as RootLicenseID
        FROM
            License l
      WHERE
            --Note by Jerod
            --Added a case in here...this way, passing 0 will get the full tree, while passing
            --the ID of any node will get that node's branch.
            --Note that this means if you have 1 root node in the tree, passing that node id or passing 0 will give the same result set
            1 = case when @iLicenseID = 0 then
                    case --allow 0 or nulls as root nodes
                        when l.ParentID IS NULL then 1
                        when l.ParentID = 0 then 1
                        else 0
                    end
                else
                    case
                        when ID = @iLicenseID then 1
                        else 0
                    end
                end
    UNION ALL
-- Recursive member definition
    SELECT
            l.ID,
            l.ParentID,
            l.Name,
            Level + 1,
            CAST(LevelIndentText + ' > ' + l.Name as nvarchar(max)),
            --CAST(replicate('&nbsp;&nbsp;&nbsp;&nbsp;',Level+1) + c.Name as nvarchar(max)),
            CAST(l.Name as nvarchar(max)) as LevelText,
            ll.RootLicenseID
    FROM
            License l
    INNER JOIN License_CTE ll ON
            ll.ID = l.ParentID
)
SELECT
	a.[Level],
	a.LevelText,
	a.LevelIndentText,
	a.RootLicenseID,
	b.ID,
	b.ParentID,
	b.JurisdictionID,
	b.ProfessionID,
	b.Category,
	b.Name,
	b.Description,
	b.CreatedOn,
	b.ModifiedOn,
	b.CreatedBy,
	b.ModifiedBy,
	b.RequiredUserFields,
	b.HasUserLicenseNumber,
	b.CustomerId,
	b.ExpirationRuleId,
	b.ExpirationRuleAfterDays
FROM
      License_CTE a
JOIN
      License b
ON
      a.ID = b.ID
);


");
        }
    }
}