﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration631 : Migration
    {
        public override void Up()
        {
            
            Execute(@"
CREATE FUNCTION [dbo].[fGetAudiencePath]
(   
	@audienceId int
)
RETURNS TABLE
AS
RETURN
(
WITH Audience_CTE (ID, ParentAudienceID, Level, LevelIndentText)
AS
(
-- Anchor member definition
    SELECT
            a.ID,
            a.ParentAudienceID,
            0 as Level,
            CAST(a.Name as nvarchar(max)) as LevelIndentText
        FROM
            Audience a
      WHERE
            a.ID = @audienceID
    UNION ALL
-- Recursive member definition
    SELECT
            a.ID,
            a.ParentAudienceID,
            aa.Level + 1,
            CAST(a.Name + ' > ' + LevelIndentText as nvarchar(max))
    FROM
            Audience a
    INNER JOIN Audience_CTE aa ON
            aa.ParentAudienceID = a.ID
)
SELECT top 1
	  a.ID,
      a.Level,
      a.LevelIndentText
FROM
      Audience_CTE a
order by
	a.Level desc
)


;
");
        }
    }
}
