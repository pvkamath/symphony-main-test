﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration444 : Migration
    {

        public override void Up()
        {
            Execute(@"
    execute sp_rename 'Licence', 'License';
    execute sp_rename 'LicenceGrants', 'LicenseGrants';
    execute sp_rename 'LicenseGrants.LicenceName', 'LicenseName', 'COLUMN';
    execute sp_rename 'LicenseGrants.LicenceNumber', 'LicenseNumber', 'COLUMN';

");
        }

    }
}