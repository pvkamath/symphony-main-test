﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration210 : Migration
    {

        public override void Up()
        {
            Execute(@"ALTER PROCEDURE [dbo].[GetUserApplications] (@userId int)
AS
BEGIN
	WITH Assignments as (
			SELECT tl.ID
				 , tl.Name as Name
				 , tl.TileUri
				 , ap.ID as ApplicationId
				 , ap.Name as ApplicationName
				 , ap.PassUserNamePass 
				 , htl.HierarchyNodeID
				 , htl.HierarchyTypeID
				 , tl.CreatedBy
				 , tl.CreatedOn
				 , tl.ModifiedBy
				 , tl.ModifiedOn
				 , ROW_NUMBER() over (order by tl.applicationId) as MyRow
			  FROM Tiles as tl
			  	JOIN Applications as ap on ap.ID = tl.ApplicationID
			  	JOIN HierarchyToTileLinks htl on tl.id = htl.TileID
				JOIN (
					select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
					union
					select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
					union
					select f.ID, f.TypeID from useraudience 
						cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
					where userid = @userid
					union
					select @userid, 4
				) as H on h.id = htl.HierarchyNodeID and h.TypeID = htl.HierarchyTypeID
		)
		select distinct
				  a.ApplicationId as ID
				, a.ApplicationName as Name
				, case 
					when a.PassUserNamePass = 0 then 'none'
					when a.PassUserNamePass = 1 and cr.ID is null then 'missing'
					else 'complete'
				end as CredentialState
				,  (select count(*) from Assignments as au where au.ApplicationId = a.ApplicationId) as TileCount
                , cr.ID as CredentialID
				, cr.UserName as CredentialUserName
				, cr.Password as CredentialPassword
				, (select count(*) from Assignments as au where au.ApplicationId = a.ApplicationId) as TileCount
		from Assignments as a
			left join Credentials as cr on cr.ApplicationID = a.ApplicationId
		where 
			(
				cr.UserID = @userId 
				or cr.UserID is NULL
			)
END");
        }

        public override void Down()
        {
            Execute(@"ALTER PROCEDURE [dbo].[GetUserApplications] (@userId int)
AS
BEGIN
	WITH Assignments as (
			SELECT tl.ID
				 , tl.Name as Name
				 , tl.TileUri
				 , ap.ID as ApplicationId
				 , ap.Name as ApplicationName
				 , ap.PassUserNamePass 
				 , htl.HierarchyNodeID
				 , htl.HierarchyTypeID
				 , tl.CreatedBy
				 , tl.CreatedOn
				 , tl.ModifiedBy
				 , tl.ModifiedOn
				 , ROW_NUMBER() over (order by tl.applicationId) as MyRow
			  FROM Tiles as tl
			  	JOIN Applications as ap on ap.ID = tl.ApplicationID
			  	JOIN HierarchyToTileLinks htl on tl.id = htl.TileID
				JOIN (
					select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
					union
					select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
					union
					select f.ID, f.TypeID from useraudience 
						cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
					where userid = @userid
					union
					select @userid, 4
				) as H on h.id = htl.HierarchyNodeID and h.TypeID = htl.HierarchyTypeID
		)
		select distinct
				  a.ApplicationId as ID
				, a.ApplicationName as Name
				, case 
					when a.PassUserNamePass = 0 then 'none'
					when a.PassUserNamePass = 1 and cr.ID is null then 'missing'
					else 'complete'
				end as CredentialState
				,  (select count(*) from Assignments as au where au.ApplicationId = a.ApplicationId) as TileCount
                , cr.ID as CredentialID
				, cr.UserName as CredentialUserName
				, case when cr.Password is null 
					then null
					else '*****'
					end as CredentialPassword
				,  (select count(*) from Assignments as au where au.ApplicationId = a.ApplicationId) as TileCount
		from Assignments as a
			left join Credentials as cr on cr.ApplicationID = a.ApplicationId
		where 
			(
				cr.UserID = @userId 
				or cr.UserID is NULL
			)
END");
        }
    }
}