﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration140 : Migration
    {
        public override void Up()
        {
            AddColumn("Category", "CategoryTypeID", DbType.Int32, 0, false, "0");

            AddColumn("TrainingProgram", "CategoryID", DbType.Int32, 0, false, "0");
        }
    }
}