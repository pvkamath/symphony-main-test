﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration709 : Migration
    {
        public override void Up()
        {
            TableSchema.Table licenseAssignmentNotesTable = CreateTableWithKey("LicenseAssignmentNote", "ID");
            licenseAssignmentNotesTable.AddColumn("Body", DbType.String);
            licenseAssignmentNotesTable.AddColumn("LicenseAssignmentID", DbType.Int32);

            AddSubSonicStateColumns(licenseAssignmentNotesTable);

            Execute(@"
                ALTER TABLE dbo.LicenseAssignmentNote
                ADD CONSTRAINT FK_LicenseNoteLicenseAssignmentId FOREIGN KEY (LicenseAssignmentId) REFERENCES dbo.LicenseAssignments(ID)
            ");
        }
    }
}
