﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration743 : Migration
    {

        public override void Up()
        {
            TableSchema.Table trainingProgramNotifications = CreateTableWithKey("TrainingProgramTemplates", "ID");
            trainingProgramNotifications.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            trainingProgramNotifications.AddColumn("TemplateID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(trainingProgramNotifications);

            
        }

    }
}