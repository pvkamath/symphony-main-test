﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration162 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE ArtisanAssets ALTER COLUMN Filename NVARCHAR(MAX) NULL;");
        }
    }
}