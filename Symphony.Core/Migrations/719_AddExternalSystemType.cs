﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration719 : Migration
    {

        public override void Up()
        {
            AddColumn("ExternalSystem", "ExternalSystemTypeName", DbType.String, 100, true);
        }

    }
}
