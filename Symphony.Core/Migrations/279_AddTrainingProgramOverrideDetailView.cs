﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration279 : Migration
    {
        public override void Up()
        {
            Execute(@"CREATE view [dbo].[TrainingProgramParameterOverrideDetail]
as
(
	SELECT tpo.TrainingProgramID as TrainingProgramID,
           tpo.ParameterSetOptionID as ParameterSetOptionID, 
           pso.ParameterSetID as ParameterSetID, 
           pso.Value as ParameterSetOptionValue,
           p.ID as ParameterID,
           p.Name as ParameterName, 
           p.Code as Code, 
           p.DefaultValue,
	        CASE WHEN pv.Value IS NULL THEN
		        p.DefaultValue
	        ELSE
		        pv.Value
	        END as Value
	FROM TrainingProgramParameterOverride tpo
    JOIN ParameterSetOption pso on pso.ID = tpo.ParameterSetOptionID
    JOIN Parameter p on p.ParameterSetID = pso.ParameterSetID
    LEFT JOIN ParameterValue pv on p.ID = pv.ParameterID AND pv.ParameterSetOptionID = tpo.ParameterSetOptionID
);"); 
        }
    }
}