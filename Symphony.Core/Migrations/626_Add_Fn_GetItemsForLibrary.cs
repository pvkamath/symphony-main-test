﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration626 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE FUNCTION [dbo].[fGetItemsForLibrary]
(
	@libraryId INT,
	@libraryItemTypeId INT = null,
	@customerId INT,
	@isShowAll bit,
	@isHideInLibrary bit,
	@isFavorite int,
	@isInProgress int, 
	@isNotStarted int,
	@userId int
)
RETURNS @Tr TABLE (
	ID int,
	LibraryItemID int,
	LibraryID int,
	Name nvarchar(2000),
	[Description] nvarchar(max),
	CategoryID int,
	Duration int,
	LibraryItemTypeID int, 
	CreatedOn datetime,
	CourseTypeID int,
	LibraryFavoriteID int,
	CategoryName nvarchar(max),
	LevelIndentText nvarchar(max),
	IsInLibrary bit,
	IsFavorite bit,
	IsInProgress bit,
	ItemCreatedOn datetime,
	ItemModifiedOn datetime,
	CourseVersion int
)
AS
BEGIN
	insert into @Tr
	select 
		item.ID,
		item.LibraryItemID,
		item.LibraryID,
		item.Name, 
		item.[Description],
		item.CategoryID,
		item.Duration,
		item.LibraryItemTypeID,
		item.CreatedOn,
		item.CourseTypeID,
		item.LibraryFavoriteID,
		cat.LevelText, 
		cat.LevelIndentText, 
		case when item.LibraryItemID is null then 0 else 1 end as IsInLibrary,
		case when item.LibraryFavoriteID is null then 0 else 1 end as IsFavorite,
		case when item.MaxOnlineCourseRollupID is null then 0 else 1 end as IsInProgress,
		item.ItemCreatedOn,
		item.ItemModifiedOn,
		item.CourseVersion
	from
	(
		select
			tp.ID as ID,
			li.ID as LibraryItemID,
			li.LibraryID as LibraryID,
			tp.Name as Name,
			tp.[Description] as [Description],
			tp.CategoryID as CategoryID,
			tp.Duration,
			2 as LibraryItemTypeID,
			li.CreatedOn,
			-1 as CourseTypeID,
			lf.ID as LibraryFavoriteID,
			max(ocr.ID) as MaxOnlineCourseRollupID,
			tp.CreatedOn as ItemCreatedOn,
			tp.ModifiedOn as ItemModifiedOn,
			null as CourseVersion
		from
			TrainingProgram tp
		left join
			LibraryItem li on
				li.ItemID = tp.ID and
				li.LibraryItemTypeID = 2 and
				li.LibraryID = @libraryID
		left join
			LibraryFavorite lf on
				lf.LibraryItemID = li.ID and
				lf.UserID = @userId
		left join
			OnlineCourseRollup ocr on
				ocr.UserID = @userId and
				ocr.TrainingProgramID = li.ItemID
		where
			((li.LibraryID = @libraryId and @isShowAll = 0)  or @isShowAll = 1) and
			((li.ID is null and @isShowAll = 1 and @isHideInLIbrary = 1) or (@isShowAll = 0 or @isHideInLibrary = 0)) and
			tp.CustomerID = @customerID 
			and tp.ParentTrainingProgramID = 0
		group by
			tp.ID,
			li.ID,
			li.LibraryID,
			tp.Name,
			tp.[Description],
			tp.CategoryID,
			tp.Duration,
			li.CreatedOn,
			lf.ID,
			tp.CreatedOn,
			tp.ModifiedOn
	union
		select
			o.ID as ID,
			li.ID as LibraryItemID,
			li.LibraryID as LibraryID,
			o.Name as Name,
			o.[Description] as [Description],
			o.CategoryID as CategoryID,
			case when o.IsUseAutomaticDuration = 1 then AutomaticDuration else Duration end as Duration,
			1 as LibraryItemTypeID,
			li.CreatedOn,
			2 as CourseTypeID,
			lf.ID as LibraryFavoriteID,
			max(ocr.ID) as MaxOnlineCourseRollupID,
			o.CreatedOn as ItemCreatedOn,
			o.ModifiedOn as ItemModifiedOn,
			o.[Version] as CourseVersion
		from
			OnlineCourse o
		left join
			LibraryItem li on
				o.ID = li.ItemID and 
				li.LibraryItemTypeID = 1 and
				li.LibraryID = @libraryID
		left join
			LibraryFavorite lf on
				lf.LibraryItemID = li.ID and
				lf.UserID = @userId
		left join
			OnlineCourseRollup ocr on
				ocr.CourseID = li.ItemID and
				ocr.UserID = @userID and
				(ocr.TrainingProgramID = 0 or ocr.TrainingProgramID is null)
		where
			((li.LibraryID = @libraryId and @isShowAll = 0)  or @isShowAll = 1) and
			((li.ID is null and @isShowAll = 1 and @isHideInLIbrary = 1) or (@isShowAll = 0 or @isHideInLibrary = 0)) and
			o.CustomerID = @customerID
		group by
			o.ID,
			li.ID,
			li.LibraryID,
			o.Name,
			o.[Description],
			o.CategoryID,
			li.CreatedOn,
			o.IsUseAutomaticDuration,
			o.AutomaticDuration,
			o.Duration,
			lf.ID,
			o.CreatedOn,
			o.ModifiedOn,
			o.[Version]
	) item
	cross apply fGetCategoryHierarchyPath(item.CategoryID) as cat
	where (@libraryItemTypeId is null or @libraryItemTypeId > 0 and item.LibraryItemTypeID = @libraryItemTypeId) and
		(@IsFavorite = 1 and item.LibraryFavoriteID > 0 or @isFavorite  != 1) and
		(@IsInProgress = 1 and item.MaxOnlineCourseRollupID > 0 or @isInProgress != 1) and
		(@IsNotStarted = 1 and (item.MaxOnlineCourseRollupID is null or item.MaxOnlineCourseRollupID = 0) or @isNotStarted = 0)
	return
END
;
");
        }
    }
}
