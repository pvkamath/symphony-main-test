﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration477 : Migration
    {

        public override void Up()
        {
            AddColumn("TrainingProgram", "CourseUnlockModeID", DbType.Int32, 0, false, "0");
        }

    }
}