﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration019 : Migration
    {
        public override void Up()
        {
            // add the default template
            AddTemplate(0);

            DbConnection connection = Provider.CreateConnection();
            DbCommand command = connection.CreateCommand();
            command.CommandText = "select id from customer";
            DbDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                // add the template for all the other customers
                AddTemplate(reader.GetInt32(0));   
            }
        }

        private void AddTemplate(int customerId)
        {

            StringBuilder sb = new StringBuilder(614);
            sb.AppendFormat(@"insert templates{0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"[Subject],{0}", Environment.NewLine);
            sb.AppendFormat(@"Body,{0}", Environment.NewLine);
            sb.AppendFormat(@"CodeName,{0}", Environment.NewLine);
            sb.AppendFormat(@"DisplayName,{0}", Environment.NewLine);
            sb.AppendFormat(@"[Description],{0}", Environment.NewLine);
            sb.AppendFormat(@"Priority,{0}", Environment.NewLine);
            sb.AppendFormat(@"CustomerID,{0}", Environment.NewLine);
            sb.AppendFormat(@"[Enabled],{0}", Environment.NewLine);
            sb.AppendFormat(@"ModifiedBy,{0}", Environment.NewLine);
            sb.AppendFormat(@"CreatedBy,{0}", Environment.NewLine);
            sb.AppendFormat(@"ModifiedOn,{0}", Environment.NewLine);
            sb.AppendFormat(@"CreatedOn,{0}", Environment.NewLine);
            sb.AppendFormat(@"Area,IsScheduled,ScheduleParameterID,RelativeScheduledMinutes,FilterComplete){0}", Environment.NewLine);
            sb.AppendFormat(@"values{0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"'Registration Change for {{!Registration.UserFullName}}',{0}", Environment.NewLine);
            sb.AppendFormat(@"'Your status has been changed to {{!Registration.Status.Description}}',{0}", Environment.NewLine);
            sb.AppendFormat(@"'RegistrationStatusChanged',{0}", Environment.NewLine);
            sb.AppendFormat(@"'Registration Status Changed',{0}", Environment.NewLine);
            sb.AppendFormat(@"'Occurs when a user''s registration status is changed in classroom',{0}", Environment.NewLine);
            sb.AppendFormat(@"1,{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}, --customer id{1}", customerId, Environment.NewLine);
            sb.AppendFormat(@"0, -- enabled{0}", Environment.NewLine);
            sb.AppendFormat(@"'admin',{0}", Environment.NewLine);
            sb.AppendFormat(@"'admin',{0}", Environment.NewLine);
            sb.AppendFormat(@"getdate(),{0}", Environment.NewLine);
            sb.AppendFormat(@"getdate(),{0}", Environment.NewLine);
            sb.AppendFormat(@"'',{0}", Environment.NewLine);
            sb.AppendFormat(@"0, 0, 0, 0)");

            Execute(sb.ToString());
        }
    }
}