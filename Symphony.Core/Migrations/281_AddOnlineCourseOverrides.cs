﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration281 : Migration
    {
        public override void Up()
        {
            TableSchema.Table onlineCourseParameterOverride = CreateTable("OnlineCourseParameterOverride");
            onlineCourseParameterOverride.AddPrimaryKeyColumn("ID");
            onlineCourseParameterOverride.AddColumn("OnlineCourseID", DbType.Int32, 0, false);
            onlineCourseParameterOverride.AddColumn("ParameterSetOptionID", DbType.Int32, 0, false);

            AddSubSonicStateColumns(onlineCourseParameterOverride);
        }
    }
}