﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration766 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "CustomThemeID", DbType.Int32, 0, true, null);

            Execute(@"
                alter table Customer
                    add constraint fk_Theme_ID_Customer_CustomThemeID foreign key(CustomThemeID)
                    references Themes(ID)
            ");
        }
    }
}