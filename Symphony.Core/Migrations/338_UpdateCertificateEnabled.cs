﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration338: Migration
    {
        public override void Up()
        {
            Execute(@"ALTER TABLE [ArtisanCourses] DROP CONSTRAINT [DF_ArtisanCourses_CertificateEnabled]");
            Execute(@"ALTER TABLE [ArtisanCourses] ADD CONSTRAINT [DF_ArtisanCourses_CertificateEnabled] DEFAULT (1) FOR [CertificateEnabled]");
            Execute(@"UPDATE [ArtisanCourses] SET CertificateEnabled = 1");

            AddColumn("OnlineCourse", "CertificateEnabledOverride", DbType.Boolean, 0, true);
            AddColumn("TrainingProgram", "CertificateEnabledOverride", DbType.Boolean, 0, true);
            AddColumn("Customer", "CertificateEnabledOverride", DbType.Boolean, 0, true);
        }
    }
}