﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration181 : Migration
    {
        public override void Up()
        {
            Execute(@"
                IF NOT EXISTS(SELECT * FROM ArtisanThemes WHERE Name='Zoologic')
                BEGIN
	                INSERT ArtisanThemes (Name, Description, CssPath, CustomerID, Folder, SkinName, CreatedBy, ModifiedBy) 
	                VALUES ('Zoologic', 'Zoologic Theme', '/skins/artisan_content/zoologic/css/theme.css', 0, '/skins/artisan_content/zoologic', 'be_smooth', 'system', 'system')
                END
            ");
        }
    }
}