﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration822 : Migration
    {
        public override void Up()
        {
            Execute(@"
                ALTER TABLE License 
                ADD CertificateFormFieldJson nvarchar(max)
            ");
        }
    }
}