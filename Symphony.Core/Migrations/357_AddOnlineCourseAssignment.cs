﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration357 : Migration
    {
        public override void Up()
        {


            TableSchema.Table onlineCourseAssignments = CreateTableWithKey("OnlineCourseAssignments", "ID");
            onlineCourseAssignments.AddColumn("UserID", DbType.Int32, 0, false);
            onlineCourseAssignments.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            onlineCourseAssignments.AddColumn("CourseID", DbType.Int32, 0, false);
            onlineCourseAssignments.AddColumn("PageID", DbType.Int32, 0, false);
            onlineCourseAssignments.AddColumn("Attempt", DbType.Int32, 0, false);
            onlineCourseAssignments.AddColumn("Date", DbType.DateTime, 0, false);
            onlineCourseAssignments.AddColumn("ResponseStatus", DbType.Int32, 0, false, "1");
            onlineCourseAssignments.AddColumn("IsCorrect", DbType.Boolean, 0, false, "0");
            onlineCourseAssignments.AddLongText("Response", false, "''");

        
        }
    }
}