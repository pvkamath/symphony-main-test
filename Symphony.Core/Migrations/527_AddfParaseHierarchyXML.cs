﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration527 : Migration
    {

        public override void Up()
        {
            Execute(@"
create function [dbo].[fParseHierarchyXML](@xmlString xml) 
returns @hierarchies TABLE (
	HierarchyName nvarchar(40),
	HierarchyIds nvarchar(max)
) 
as
begin

	with hg as (
		select 
			h.CustomerID,
			h.HierarchyName,
			case 
				when h.HierarchyName = 'audience' then 
				(
					case when h.HierarchyIds = 'all' then
						(select CAST(id as nvarchar(10)) + ',' AS 'data()' 
						from Audience
						where CustomerID = h.CustomerID
						for xml path(''))
					when h.HierarchyIds = 'none' then
						'-1'
					else 
						h.HierarchyIds
					end
				)
				when h.HierarchyName = 'location' then 
				(
					case when h.HierarchyIds = 'all' then
						(select CAST(id as nvarchar(10)) + ',' AS 'data()' 
						from Location
						where CustomerID = h.CustomerID
						for xml path(''))
					when h.HierarchyIds = 'none' then
						'-1'
					else 
						h.HierarchyIds
					end
				)
				when h.HierarchyName = 'jobrole' then 
				(
					case when h.HierarchyIds = 'all' then
						(select CAST(id as nvarchar(10)) + ',' AS 'data()' 
						from JobRole
						where CustomerID = h.CustomerID
						for xml path(''))
					when h.HierarchyIds = 'none' then
						'-1'
					else 
						h.HierarchyIds
					end
				)
				end as HierarchyIds
		from (
			select
				c.value('../customerId[1]','varchar(200)') as CustomerID,
				c.value('local-name(.)[1]','varchar(200)') as HierarchyName,
				c.value('(.)[1]','varchar(200)') as HierarchyIds
			from @xmlString.nodes('//parameters/userHierarchyAccess') as x(Rec)
			cross apply @xmlString.nodes('//parameters/userHierarchyAccess/customer/*') as i(c)
		) h
	)
	
	insert  
		@hierarchies 
	select 
		HierarchyName,
		case 
			when 
				right(rtrim(HierarchyIds),1) = ',' 
			then 
				substring(rtrim(HierarchyIds),1,len(rtrim(HierarchyIds))-1)
			else 
				HierarchyIds
		end as HierachyIds
	from (
		select
			hGrouped.HierarchyName,
			(
				select 
					case
						when hg.HierarchyIds is null then ''
						when right(rtrim(hg.HierarchyIds),1) = ',' then hg.HierarchyIds
						else hg.HierarchyIds + ','
					end	 AS 'data()' 
				from hg
				where hg.HierarchyName = hGrouped.HierarchyName
				for xml path('')
			) as HierarchyIds
		from
			hg as hGrouped
		where hGrouped.HierarchyName != 'customerId'
		group by hGrouped.HierarchyName
	) cleaned
	return 
end
");
        }

    }
}
