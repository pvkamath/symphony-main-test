﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration558 : Migration
    {

        public override void Up()
        {
            Execute(@"
                CREATE procedure [dbo].[ClearRegistrations] (
                    @registrationIdsCommaSeparated nvarchar(max), -- Registration ids to clear
                    @userName nvarchar(64) -- The username that made the change
                )
                as
                begin
					declare @registrationIds table (id int)			-- List of registration ids to use
					
					-- Convert the comma separted registration ids to a table
					insert into @registrationIds
					select CAST(LTRIM(RTRIM(item)) as int)
					from fSplit(@registrationIdsCommaSeparated,',');
					
					
					delete from Registration
					where 
						ID in (
							select 
								clearedRegistrations.ID 
							from 
								Registration activeRegistrations
							join 
								Registration clearedRegistrations 
							on 
								clearedRegistrations.ClassID = -activeRegistrations.ClassID and 
								clearedRegistrations.RegistrantID = -activeRegistrations.RegistrantID and
								clearedRegistrations.ClassID < 0 and
								clearedRegistrations.RegistrantID < 0
							where activeRegistrations.ID in (select id from @registrationIds)
						) and
						ClassID < 0 and 
						RegistrantID < 0
					
					-- set the class and registrant id to negative in order to easily restore the registration if needed
					-- also will allow history
					update Registration 
					set
						ClassID = -ClassID,
						RegistrantID = -RegistrantID,
						ModifiedOn = GETUTCDATE(),
						ModifiedBy = @userName
					where
						ID in (select id from @registrationIds)
                end
");
        }

    }
}
