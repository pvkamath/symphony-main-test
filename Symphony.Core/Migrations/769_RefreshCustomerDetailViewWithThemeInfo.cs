﻿using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration769: Migration
    {
        public override void Up()
        {
            Execute(@"ALTER view [dbo].[CustomerDetails] as
                    select
	                   c.[ID]
					  ,c.[SalesChannelID]
					  ,c.[Name]
					  ,c.[ParentCustomerID]
					  ,c.[EvaluationIndicator]
					  ,c.[EvaluationEndDate]
					  ,c.[SuspendedIndicator]
					  ,c.[UserLimit]
					  ,c.[SelfRegistrationIndicator]
					  ,c.[SelfRegistrationPassword]
					  ,c.[SelfRegistrationPasswordSalt]
					  ,c.[StrongPasswordLength]
					  ,c.[PasswordMaskID]
					  ,c.[LockoutCount]
					  ,c.[SubDomain]
					  ,c.[UserNameAlias]
					  ,c.[LocationAlias]
					  ,c.[JobRoleAlias]
					  ,c.[AudienceAlias]
					  ,c.[ModifiedBy]
					  ,c.[CreatedBy]
					  ,c.[ModifiedOn]
					  ,c.[CreatedOn]
					  ,c.[QuickQueryIndicator]
					  ,c.[ReportBuilderIndicator]
					  ,c.[NewHireDuration]
					  ,c.[Modules]
					  ,c.[OrganizerSeats]
					  ,c.[TimeZone]
					  ,c.[CanUsersChangePassword]
					  ,c.[NewHireTransitionPeriod]
					  ,c.[SsoEnabled]
					  ,c.[CertificateFile]
					  ,c.[CertificateFileName]
					  ,c.[IdentityProviderURL]
					  ,c.[SSOTimeoutRedirect]
					  ,c.[SSOTimeoutMessage]
					  ,c.[HasPasswordMask]
					  ,c.[CreatedByUserId]
					  ,c.[ModifiedByUserId]
					  ,c.[CreatedByActualUserId]
					  ,c.[ModifiedByActualUserId]
					  ,c.[AccountExecutive]
					  ,c.[CustomerCareRep]
					  ,c.[OnlineCourseRetries]
					  ,c.[ShowPretestOverride]
					  ,c.[ShowPostTestOverride]
					  ,c.[PassingScoreOverride]
					  ,c.[SkinOverride]
					  ,c.[PreTestTypeOverride]
					  ,c.[PostTestTypeOverride]
					  ,c.[NavigationMethodOverride]
					  ,c.[DisclaimerOverride]
					  ,c.[ContactEmailOverride]
					  ,c.[ShowObjectivesOverride]
					  ,c.[PretestTestOutOverride]
					  ,c.[RetestMode]
					  ,c.[Retries]
					  ,c.[Phone]
					  ,c.[Fax]
					  ,c.[SalesforceContactId]
					  ,c.[SalesforceAccountId]
					  ,c.[SalesforceIsEnabled]
					  ,c.[IsSalesforceCapable]
					  ,c.[SalesforceAddress]
					  ,c.[RandomizeQuestionsOverride]
					  ,c.[RandomizeAnswersOverride]
					  ,c.[ReviewMethodOverride]
					  ,c.[MarkingMethodOverride]
					  ,c.[InactivityMethodOverride]
					  ,c.[InactivityTimeoutOverride]
					  ,c.[MinimumPageTimeOverride]
					  ,c.[MaximumQuestionTimeOverride]
					  ,c.[MaxCourseWork]
					  ,c.[SsoType]
					  ,c.[CertificateEnabledOverride]
					  ,c.[MaxTimeOverride]
					  ,c.[MinLoTimeOverride]
					  ,c.[MinScoTimeOverride]
					  ,c.[MinTimeOverride]
					  ,c.[AllowSelfAccountCreation]
					  ,c.[LocationID]
					  ,c.[EnforcePasswordReset]
					  ,c.[EnforcePasswordDays]
					  ,c.[IsCreatedBySalesforce]
					  ,c.[CourseAlias]
					  ,c.[SsoLoginUiType]
					  ,c.[LoginRedirect]
					  ,c.[HelpUrl]
					  ,c.[Email]
					  ,c.[IsExternalSystemLoginEnabled]
					  ,c.[Description]
					  ,c.[DefaultProtocol]
					  ,c.[DefaultAuthority]
					  ,c.[AssociatedImageData]
					  ,c.[RedirectType]
					  ,c.[OptInTitle]
					  ,c.[OptInMessage]
					  ,c.[OptInConfirmText]
					  ,c.[OptInPreviewText]
					  ,c.[OptInCancelText]
					  ,c.[IsForceLogoutOverride]
					  ,c.[ThemeID]
                      ,c.[CustomThemeID]
                      ,c.[ThemeTypeID],
	                    u1.FirstName + ' ' + u1.LastName as AccountExecutiveFullName, 
	                    u2.FirstName + ' ' + u2.LastName as CustomerCareRepFullName,
	                    Location.Name as CustomerLocation
                    from
	                    Customer c
                    left join
	                    [User] u1
                    on
	                    c.AccountExecutive = u1.id
                    left join
	                    [User] u2
                    on
	                    c.CustomerCareRep = u2.id
                    left join 
	                    Location
                    on c.LocationID = Location.ID
;;");
        }
    }
}