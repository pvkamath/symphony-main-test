﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration446 : Migration
    {

        public override void Up()
        {
            Execute(@"
    execute sp_rename 'TrainingProgramLicence', 'TrainingProgramLicense';
    execute sp_rename 'TrainingProgramLicense.LicenceID', 'LicenseID';

");
        }

    }
}