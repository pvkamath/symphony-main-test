﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration058 : Migration
    {
        public override void Up()
        {
            AddColumn("Course", "CategoryID", DbType.Int32, 0, false, "0");
            AddColumn("OnlineCourse", "CategoryID", DbType.Int32, 0, false, "0");   
        }
    }
}