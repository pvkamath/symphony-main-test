﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration700 : Migration
    {
        public override void Up()
        {
            AddColumn("User", "LoginRedirect", DbType.String, 2048, true);
            AddColumn("User", "RedirectType", DbType.Int32, 0, true);
        }
    }
}
