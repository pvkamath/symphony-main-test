﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration686 : Migration
    {
        public override void Up()
        {
            Execute(@"
    CREATE NONCLUSTERED INDEX [idx_customer_validation] ON [dbo].[TrainingProgram] 
    (
	    [CustomerID],
	    [IsValidationEnabled]
    ) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 85) ON [PRIMARY]
    CREATE NONCLUSTERED INDEX [idx_customer_validation] ON [dbo].[OnlineCourse] 
    (
	    [CustomerID],
	    [IsValidationEnabled]
    ) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 85) ON [PRIMARY]

");
        }
    }
}
