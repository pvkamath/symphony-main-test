﻿using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration780 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgramRollup", "ProfessionID", System.Data.DbType.Int32, 0, true);
        }
    }
}
