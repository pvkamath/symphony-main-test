﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration522 : Migration
    {

        public override void Up()
        {
            Execute(@"
CREATE PROCEDURE [dbo].[report_None]
	@xmlString XML
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

select @xmlString;

END;
");
        }

    }
}
