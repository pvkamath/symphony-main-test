﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration329: Migration
    {
        public override void Up()
        {
            Execute(@"CREATE PROCEDURE [dbo].[AddQuiz](@CID int, @CODE nvarchar(255), @CHAPTER_TITLE nvarchar(255))
as
begin
DECLARE @PSCID int
DECLARE @CH_ID int
DECLARE @S_ID int
DECLARE @STYPE int
DECLARE @QSETID int
DECLARE @PASS int
DECLARE @QNAME nvarchar(10)

	SELECT 
		@PSCID = c.course_id, 
		@CH_ID = s.chapter_id, 
		@S_ID = s.section_id, 
		@STYPE = s.section_testtype,
		@QSETID = s.qset_id,
		@PASS = s.section_exam_passpercent,
		@QNAME = CASE s.section_testtype WHEN 2 THEN 'Quiz' ELSE 'Final Exam' END
		from Analyst.dbo.ps_courses c
		left join Analyst.dbo.ps_sections s on c.course_id = s.course_id
		left join Analyst.dbo.ps_chapters ch on ch.chapter_id = s.chapter_id and ch.course_id = c.course_id
		where c.course_code = @CODE AND ch.chapter_title like '%' + @CHAPTER_TITLE + '%'
	IF (@QSETID is not NULL and @STYPE is not null)
	BEGIN
	exec Symphony.dbo.CreateQuizExam @CID, @PSCID, @QSETID, @QNAME, @PASS, @STYPE, @CHAPTER_TITLE, @CODE
	END
END

");


        }
    }
}