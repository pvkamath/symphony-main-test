﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration627 : Migration
    {
        public override void Up()
        {
            // Transactions will fail here because you can't create these inside
            // a transaction.

            DataService.ExecuteQuery(new QueryCommand(@"
    CREATE FULLTEXT CATALOG Symphony

	CREATE FULLTEXT INDEX ON SecondaryCategory
	(
		Name
	)
	KEY INDEX PK_SecondaryCategory__Id ON Symphony

	CREATE FULLTEXT INDEX ON [User]
	(
		FirstName, MiddleName, LastName
	)
	KEY INDEX PK_User ON Symphony
	", Provider.Name));
        }
    }
}
