﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration693 : Migration
    {
        public override void Up()
        {
          Execute(@"
-- Location = 1
update ReportEntity set ReportEntityTypeID = 1 where Xtype = 'reporting.locationpicker' or Xtype = 'reporting.classlocationpicker';
-- JobRole = 2
update ReportEntity set ReportEntityTypeID = 2 where Xtype = 'reporting.jobrolepicker';
-- Audience = 3
update ReportEntity set ReportEntityTypeID = 3 where Xtype = 'reporting.audiencepicker';
-- User = 4
update ReportEntity set ReportEntityTypeID = 4 where Xtype = 'reporting.instructorpicker' or Xtype = 'reporting.supervisorpicker' or Xtype = 'reporting.studentpicker';
-- TP = 5
update ReportEntity set ReportEntityTypeID = 5 where Xtype = 'reporting.trainingprogrampicker';
-- Course = 6
update ReportEntity set ReportEntityTypeID = 6 where Xtype = 'reporting.coursepicker';
-- Cusotmer = 7
update ReportEntity set ReportEntityTypeID = 7 where Xtype = 'reporting.customerpicker';
");
        }
    }
}
