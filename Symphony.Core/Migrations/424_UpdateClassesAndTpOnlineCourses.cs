﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration424 : Migration
    {

        public override void Up()
        {
            // Allows sharing training program message boards
            // from parent to child when listing training programs
            // in the instructor/student portal
            //
            // Also fixes a bug where wrong training program
            // message board would be used in the student 
            // or instructor portals
            Execute(@"
ALTER VIEW [dbo].[ClassesAndTPOnlineCourses]
AS
SELECT 
        c.[ID],
        c.[CustomerID],
        c.[LockedIndicator],
        c.[Name],
        c.[IsVirtual],
        cd.[MinStartDateTime] as MinClassDate,
        1 as CourseTypeID,
        0 as TrainingProgramID,
        null as TrainingProgramName,
        0 as RequiredSyllabusOrder,
        0 as SyllabusTypeID,
        mb.[ID] as MessageBoardID,
        0 as TrainingProgramMessageBoardID
    FROM [dbo].[Class] c
    LEFT JOIN [ClassDateMinMax] cd ON cd.[ClassID] = c.[ID]
    LEFT JOIN [MessageBoard] mb ON mb.[ClassID] = c.[ID]
    UNION
    SELECT
        CASE WHEN tpl.[CourseTypeID] = 2 THEN o.[ID] ELSE cl.[ID] END,
        tpl.[CustomerID],
        CASE WHEN tpl.[CourseTypeID] = 2 THEN CASE WHEN o.[Active] = 1 THEN 0 ELSE 1 END ELSE cl.[LockedIndicator] END,
        CASE WHEN tpl.[CourseTypeID] = 2 THEN o.[Name] ELSE cl.[Name] END,
        CAST(CASE WHEN tpl.[CourseTypeID] = 2 THEN 1 ELSE 2 END as bit),
        CASE WHEN tpl.[CourseTypeID] = 2 THEN tpl.[ActiveAfterDate] ELSE cd.[MinStartDateTime] END,
        tpl.[CourseTypeID],
        tpl.[TrainingProgramID],
        tp.[Name] as TrainingProgramName,
        tpl.[RequiredSyllabusOrder] as RequiredSyllabusOrder,
        tpl.[SyllabusTypeID] as SyllabusTypeID,
        CASE WHEN tpl.[CourseTypeID] = 2 THEN omb.[ID] ELSE cmb.[ID] END as MessageBoardID,
        CASE WHEN tp.[ParentTrainingProgramID] > 0 THEN ptpmb.ID ELSE tpmb.ID END as TrainingProgramMessageBoardID
    FROM [dbo].[TrainingProgramToCourseLink] tpl
    LEFT JOIN [OnlineCourse] o ON o.ID = tpl.[CourseID] AND tpl.[CourseTypeID] = 2
    LEFT JOIN [Class] cl ON cl.[CourseID] = tpl.[CourseID] AND tpl.[CourseTypeID] = 1
    LEFT JOIN [ClassDateMinMax] cd ON cd.[ClassID] = cl.[ID]
    LEFT JOIN [MessageBoard] omb ON o.ID = omb.[OnlineCourseID]
    LEFT JOIN [MessageBoard] cmb ON cl.ID = cmb.[ClassID]
    LEFT JOIN [MessageBoard] tpmb ON tpl.[TrainingProgramID] = tpmb.[TrainingProgramID]
    INNER JOIN [dbo].[TrainingProgram] tp ON tp.[ID] = tpl.[TrainingProgramID]
    LEFT JOIN [MessageBoard] ptpmb ON tp.[ParentTrainingProgramID] = ptpmb.[TrainingProgramID]
;;
");
        }

    }
}