﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration191 : Migration
    {
        public override void Up()
        {
            AddColumn("User", "NewHireEndDate", DbType.DateTime, 0, true, "null");
        }
    }
}