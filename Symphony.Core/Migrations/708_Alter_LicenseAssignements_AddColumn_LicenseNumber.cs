﻿using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    class Migration708 : Migration
    {

        public override void Up()
        {
            Execute(@"
                ALTER TABLE LicenseAssignments
                ADD LicenseNumber TEXT 
            ");
        }

    }
}
