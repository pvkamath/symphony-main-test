﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration776 : Migration
    {
        public override void Up()
        {
            TableSchema.Table board = CreateTableWithKey("AccreditationBoardSurveyAssignment", "ID");

            board.AddColumn("AccreditationBoardID", DbType.Int32, 0, false);
            board.AddColumn("OnlineCourseID", DbType.Int32, 0, false);
            board.AddColumn("Rank", DbType.Int32, 0, false);

            AddSubSonicStateColumns(board);

            Execute("ALTER TABLE AccreditationBoardSurveyAssignment"
                + " ADD CONSTRAINT fk_AccreditationBoard_ID_AccreditationBoardSurveyAssignment_AccreditiationBoardID FOREIGN KEY(AccreditationBoardID)"
                    + " REFERENCES AccreditationBoard(ID)"
                );

            Execute("ALTER TABLE AccreditationBoardSurveyAssignment"
                + " ADD CONSTRAINT fk_TrainingProgram_ID_AccreditationBoardSurveyAssignment_OnlineCourseID FOREIGN KEY(OnlineCourseID)"
                    + " REFERENCES OnlineCourse(ID)"
                );
        }
    }
}
