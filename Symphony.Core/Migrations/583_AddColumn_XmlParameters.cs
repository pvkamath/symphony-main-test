﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration583 : Migration
    {
        public override void Up()
        {
            Execute(@"
alter table ReportQueues add XMLParameters nvarchar(max);
            ");
        }
    }
}
