﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration884 : Migration
    {

        public override void Up()
        {
            Execute(@"
CREATE FUNCTION [dbo].[fGetUserAudiences]
(
	@userID int
)
RETURNS nvarchar(max)
AS
BEGIN
	return 
	coalesce(substring((
        select
            (', ' + a.Name)
        from
            Audience a
        inner join
            [UserAudience] ua
        on
            a.ID = ua.AudienceID
        where
            ua.UserID = @userID
        for xml path ('')
    ),3, 1000),'')
END
");
        }
    }
}
