﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration695 : Migration
    {
        public override void Up()
        {
            AddColumn("CustomerExternalSystem", "CustomerIdInExternalSystem", DbType.String, 64, true);
            AddColumn("CustomerExternalSystem", "IsCheckCustomerIdInExternalSystem", DbType.Boolean, 0, false, "0");
        }
    }
}
