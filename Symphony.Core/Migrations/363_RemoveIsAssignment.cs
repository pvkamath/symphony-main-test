﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration363 : Migration
    {
        public override void Up()
        {
            RemoveColumn("ArtisanSections", "IsAssignment");
        }
    }
}