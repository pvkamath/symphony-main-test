﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration365 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE ArtisanAnswers ALTER COLUMN Text NVARCHAR(MAX) NOT NULL;");
        }
    }
}