﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration439 : Migration
    {

        public override void Up()
        {
            AddColumn("User", "DateOfBirth", DbType.Date, 0, true);
        }

    }
}