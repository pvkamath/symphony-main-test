﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration427 : Migration
    {

        public override void Up()
        {
            // long outstanding bug, we exclude autopublish from the duplicates list, but didn't exclude them here, so they didn't show at all
            Execute(@"
ALTER VIEW [dbo].[LatestPublishedCourses] as
SELECT c.*
FROM ArtisanCourses c
INNER JOIN 
(
    SELECT OriginalCourseId, CustomerID, MAX(CreatedOn) 'maxdate' 
    FROM ArtisanCourses 
    WHERE OriginalCourseId != 0 and OriginalCourseID  is not null and IsPublished = 1 and isautopublish = 0
    GROUP BY CustomerID, OriginalCourseId
) c2 
ON c.OriginalCourseId = c2.OriginalCourseId AND c.CreatedOn = c2.maxdate and c.CustomerID = c2.CustomerID
WHERE IsPublished = 1 and isautopublish = 0;
");
        }

        public override void Down()
        {

        }
    }
}