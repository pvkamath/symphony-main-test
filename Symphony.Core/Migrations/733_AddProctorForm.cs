﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration733 : Migration
    {
        public override void Up()
        {
            TableSchema.Table table = CreateTableWithKey("ProctorForm", "ID");

            table.AddColumn("Name", DbType.String, 512, false);
            table.AddColumn("Description", DbType.String, 512, false);
            table.AddLongText("ProctorJSON", false, "'{}'");
            table.AddColumn("IsDeleted", DbType.Boolean, 0, false, "0");
            AddSubSonicStateColumns(table);
        }
    }
}