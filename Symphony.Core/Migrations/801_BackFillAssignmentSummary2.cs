﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration801 : Migration
    {
        public override void Up()
        {
            // Backfills the assignments using all courses that have assignments
            // that do not appear in the assignments table yet. This would be for
            // any deployed course that has not yet had a user take the assignment
            Execute(@"
INSERT INTO AssignmentSummary
    (
        ArtisanCourseID,
        ArtisanSectionID,
        Name,
        NumQuestions,
        PassingScore,
        CreatedOn,
        ModifiedOn,
        CreatedBy,
        ModifiedBy
    )
SELECT 
	ac.ID as ArtisanCourseID,
	s.ID as ArtisanSectionID,
	s.Name,
	count(p.ID) as NumQuestions,
	ac.PassingScore,
	ac.CreatedOn,
	ac.ModifiedOn,
	ac.CreatedBy,
	ac.ModifiedBy
FROM OnlineCourse oc
JOIN 
	ArtisanCourses ac
	ON
	ac.ID = oc.PublishedArtisanCourseID
JOIN
	ArtisanSectionPages sp
	ON
	sp.CourseID = ac.ID
JOIN
	ArtisanSections s
	on
	sp.SectionID = s.ID
JOIN
	ArtisanPages p
	ON
	p.ID = sp.PageID and p.QuestionType = 8
WHERE oc.PublishedArtisanCourseID NOT IN (
	SELECT ArtisanCourseID FROM AssignmentSummary
)
GROUP BY ac.ID, s.ID, s.Name, ac.PassingScore, ac.CreatedOn, ac.ModifiedOn, ac.CreatedBy, ac.ModifiedBy");

        }
    }
}