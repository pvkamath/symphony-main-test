﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration569 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE TABLE [dbo].OnlineCourseRollupArchive (
	[ID] [INT] IDENTITY (1, 1) NOT NULL,
	[UserID] [INT] NOT NULL,
	[CourseID] [INT] NOT NULL,
	[TrainingProgramID] [INT] NOT NULL,
	[Score] [DECIMAL](10, 2) NULL,
	[TotalSeconds] [BIGINT] NOT NULL,
	[Success] [BIT] NULL,
	[Completion] [BIT] NULL,
	[ModifiedOn] [DATETIME] NOT NULL,
	[ModifiedBy] [NVARCHAR](50) NOT NULL,
	[AttemptCount] [INT] NOT NULL,
	[HighScoreDate] [DATETIME] NULL,
	[OriginalRegistrationID] [NVARCHAR](100) NOT NULL,
	[CreatedByUserId] [INT] NOT NULL,
	[ModifiedByUserId] [INT] NOT NULL,
	[CreatedByActualUserId] [INT] NULL,
	[ModifiedByActualUserId] [INT] NULL,
	[TestAttemptCount] [INT] NOT NULL,
	[ResetNotes] [NVARCHAR](MAX) NOT NULL,
	[LastResetDate] [DATETIME] NULL,
	[CanMoveForwardIndicator] [BIT] NOT NULL,
	[NavigationPercentage] [INT] NOT NULL,
	DateCreated DATETIME NULL
	CONSTRAINT [PK_OnlineCourseRollupArchive] PRIMARY KEY CLUSTERED
	(
	[ID] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].OnlineCourseRollupArchive 
ADD CONSTRAINT [DF_OnlineCourseRollupArchive_AttemptCount] DEFAULT ((0)) FOR [AttemptCount]

ALTER TABLE [dbo].OnlineCourseRollupArchive 
ADD CONSTRAINT DF_OnlineCourseRollupArchiveArchive__OriginalRegistrationID DEFAULT ('') FOR [OriginalRegistrationID]

ALTER TABLE [dbo].OnlineCourseRollupArchive 
ADD CONSTRAINT DF_OnlineCourseRollupArchiveArchive__CreatedByUserId DEFAULT ((0)) FOR [CreatedByUserId]

ALTER TABLE [dbo].OnlineCourseRollupArchive 
ADD DEFAULT ((0)) FOR [ModifiedByUserId]

ALTER TABLE [dbo].OnlineCourseRollupArchive 
ADD CONSTRAINT [DF_OnlineCourseRollupArchive_TestAttemptCount] DEFAULT ((0)) FOR [TestAttemptCount]

ALTER TABLE [dbo].OnlineCourseRollupArchive 
ADD CONSTRAINT [DF_OnlineCourseRollupArchive_ResetNotes] DEFAULT ('') FOR [ResetNotes]

ALTER TABLE [dbo].OnlineCourseRollupArchive ADD CONSTRAINT [DF_OnlineCourseRollupArchive_CanMoveForwardIndicator] DEFAULT ((0)) FOR [CanMoveForwardIndicator]

ALTER TABLE [dbo].OnlineCourseRollupArchive 
ADD CONSTRAINT [DF_OnlineCourseRollupArchive_NavigationPercentage] DEFAULT ((0)) FOR [NavigationPercentage]

ALTER TABLE dbo.OnlineCourseRollupArchive
ADD CONSTRAINT DF_OnlineCourseRollupArchiveArchive__DateCreated DEFAULT (GETDATE()) FOR DateCreated

");
        }
    }
}
