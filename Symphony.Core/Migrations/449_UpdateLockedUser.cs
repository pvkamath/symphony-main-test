﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration449 : Migration
    {

        public override void Up()
        {
            Execute(@"ALTER view [dbo].[LockedUsers] as
                    select
	                    [user].*, coalesce(LockedCourseCount, 0) as LockedCourseCount
                    from
	                    (
	                    select
		                    u.id, sum(case when ocr.testattemptcount >= (coalesce(oc.retries,c.Retries) + 1) then 1 else 0 end) as LockedCourseCount
	                    from
		                    [user] u
	                    join
		                    onlinecourserollup ocr
	                    on
		                    ocr.userid = u.id
	                    join
		                    onlinecourse oc
	                    on
		                    oc.id = ocr.courseid
	                    left join
		                    onlineTraining.dbo.ScormRegistration sr
	                    on
		                    sr.scorm_registration_id = ocr.originalregistrationid and sr.course_id = ocr.courseid
		                left join
							Customer c
						on
							c.ID = u.CustomerID
	                    group by
		                    u.id
	                    ) as userids
                    right join
	                    [user]
                    on
	                    [user].id = userids.id;");
        }

    }
}