﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration783 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgramAccreditation", "CourseNumber", DbType.String, 256, true);
            AddColumn("OnlineCourseAccreditation", "CourseNumber", DbType.String, 256, true);
            AddColumn("ArtisanCourseAccreditation", "CourseNumber", DbType.String, 256, true);
        }
    }
}
