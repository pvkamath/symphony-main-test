﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration747 : Migration
    {

        public override void Up()
        {
            Execute(@"
                update ScheduleParameters
                set
                    IsConfigurableForTrainingProgram = 1
                where
                    CodeName in (
                        'trainingprogramstartdate',
                        'trainingprogramenddate',
                        'trainingprogramduedate',
                        'trainingprogramcourseduedate',
                        'trainingprogrampercentcomplete',
                        'trainingprogramaftercompletion'
                    )
            ");

            Execute(@"
                update Templates
                set 
                    IsConfigurableForTrainingProgram = 1
                where
                    CodeName in (
                        'TrainingprogramAssigned',
                        'AssignmentGraded',
                        'AssignmentsGradedPassed',
                        'AssignmentsGradedFailed'
                    )
            ");
        }

    }
}