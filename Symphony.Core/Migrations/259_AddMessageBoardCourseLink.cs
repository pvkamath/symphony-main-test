﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration259 : Migration
    {
        public override void Up()
        {
            AddColumn("MessageBoard", "MessageBoardType", DbType.Int32, 0, false, "0");
            AddColumn("MessageBoard", "TrainingProgramId", DbType.Int32, 0, true);
            AddColumn("MessageBoard", "ClassId", DbType.Int32, 0, true);
            AddColumn("MessageBoard", "OnlineCourseId", DbType.Int32, 0, true);
        }
    }
}