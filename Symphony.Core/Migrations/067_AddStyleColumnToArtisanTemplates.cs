﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration067 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanTemplates", "CssText", DbType.String, Int32.MaxValue, false, "''");
        }
    }
}