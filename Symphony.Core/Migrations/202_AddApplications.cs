﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration202 : Migration
    {

        public override void Up()
        {

            //TableSchema.Table customer = GetTable("Customer");
            //TableSchema.Table user = GetTable("User");
            //TableSchema.Table hierarchyTypes = GetTable("HierarchyType");


            TableSchema.Table applications = CreateTable("Applications");
            applications.AddPrimaryKeyColumn("ID");
            applications.AddColumn("Name", DbType.String, 50, false);
            applications.AddColumn("AuthenticationUri", DbType.String, 255, false);
            applications.AddColumn("PassUserNamePass", DbType.Boolean);
            AddSubSonicStateColumns(applications);
            //CreateForeignKey(customer.GetColumn("ID"), applications.GetColumn("CustomerId"));

            TableSchema.Table tiles = CreateTable("Tiles");
            tiles.AddPrimaryKeyColumn("ID");
            tiles.AddColumn("ApplicationID", DbType.Int32, 0, false);
            tiles.AddColumn("Name", DbType.String, 50, false);
            tiles.AddColumn("TileUri", DbType.String, 255, false);
            AddSubSonicStateColumns(tiles);
            //CreateForeignKey(applications.GetColumn("ID"), tiles.GetColumn("ApplicationID"));

            TableSchema.Table tileHierarchyLinks = CreateTable("HierarchyToTileLinks");
            tileHierarchyLinks.AddPrimaryKeyColumn("ID");
            tileHierarchyLinks.AddColumn("TileID", DbType.Int32, 0, false);
            tileHierarchyLinks.AddColumn("HierarchyNodeID", DbType.Int32, 0, false);
            tileHierarchyLinks.AddColumn("HierarchyTypeID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(tileHierarchyLinks);
            //CreateForeignKey(tiles.GetColumn("ID"), tileHierarchyLinks.GetColumn("TileID"));
            //CreateForeignKey(hierarchyTypes.GetColumn("ID"), tileHierarchyLinks.GetColumn("HierarchyTypeID"));


            TableSchema.Table credentials = CreateTable("Credentials");
            credentials.AddPrimaryKeyColumn("ID");
            credentials.AddColumn("ApplicationID", DbType.Int32, 0, false);
            credentials.AddColumn("UserID", DbType.Int32, 0, false);
            credentials.AddColumn("UserName", DbType.String, 150, false);
            credentials.AddColumn("Password", DbType.String, 150, false);
            //CreateForeignKey(applications.GetColumn("ID"), credentials.GetColumn("ApplicationID"));
            //CreateForeignKey(user.GetColumn("ID"), credentials.GetColumn("UserID"));


        }

        public override void Down()
        {
            //TableSchema.Table applications = GetTable("Applications");
            //TableSchema.Table credentials = GetTable("Credentials");
            //TableSchema.Table customer = GetTable("Customer");
            //TableSchema.Table hierarchyTypes = GetTable("HierarchyType");
            //TableSchema.Table tileHierarchyLinks = GetTable("HierarchyToTileLinks");
            //TableSchema.Table tiles = GetTable("Tiles");
            //TableSchema.Table user = GetTable("User");

            //DropForeignKey(applications.GetColumn("ID"), credentials.GetColumn("ApplicationID"));
            //DropForeignKey(applications.GetColumn("ID"), tiles.GetColumn("ApplicationID"));
            //DropForeignKey(customer.GetColumn("ID"), applications.GetColumn("CustomerId"));
            //DropForeignKey(hierarchyTypes.GetColumn("ID"), tileHierarchyLinks.GetColumn("HierarchyTypeID"));
            //DropForeignKey(tiles.GetColumn("ID"), tileHierarchyLinks.GetColumn("TileID"));
            //DropForeignKey(user.GetColumn("ID"), credentials.GetColumn("UserID"));
            DropTable("Applications");
            DropTable("Credentials");
            DropTable("HierarchyToTileLinks");
            DropTable("Tiles");         
        }
    }
}