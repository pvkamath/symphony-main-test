﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration816 : Migration
    {

        public override void Up()
        {
            // Fix assignment totals
            Execute(@"
ALTER procedure [dbo].[GetTranscript](@userId int)
                    as
                    begin

	                    select
		                    oc.ID as CourseID,
		                    oc.[Name] as CourseName,
		                    2 as CourseTypeID,
		                    0 as ClassID,
		                    0 as RegistrationID,
		                    0 as RegistrationStatusID,
		                    cast (0 as bit) as AttendedIndicator,
		                    case 
			                    when Success is null then 'Unknown'
			                    when Success = 0 then 'Failed'
			                    when Success = 1 then 'Passed'
		                    end
		                    as PassOrFail,
		                    Success as Passed,
		                    case 
			                    when Completion is null then 'Unknown'
			                    when Completion = 0 then 'Incomplete'
			                    when Completion = 1 then 'Completed'
		                    end
		                    as CompleteOrIncomplete,
		                    Completion as Completed,
		                    0 as OnlineTestID,
		                    0 as SurveyID,
		                    oc.PublicIndicator as IsPublic,
		                    cast(floor(round(cro.Score,0)) as nvarchar(50)) as Score, 
		                    coalesce(HighScoreDate, cro.Modifiedon, null) as AttemptDate,
		                    cro.TotalSeconds as AttemptTime,
		                    cro.AttemptCount as AttemptCount,
		                    null as StartDate,
		                    coalesce(HighScoreDate, cro.Modifiedon, null) as EndDate,
		                    cro.TrainingProgramID,
		                    '' as WebinarRegistrationID,
		                    '' as WebinarKey,
		                    cro.[CanMoveForwardIndicator] as CanMoveForwardIndicator,
		                    cast(0 as bit) as SurveyTaken,
		                    cast(0 as bit) as IsSurveyMandatory,
                            -- IsAssignmentsComplete and IsAssignmentsFailed will both be null if no assignments exist for the course
		                    cast(case 
								when (asgnForCourse.IsAssignmentsComplete is null or asgnForCourse.IsAssignmentsFailed is null) and courseDetails.TotalAssignments >= 1 then 1
                                when (asgnForCourse.IsAssignmentsComplete is null or asgnForCourse.IsAssignmentsFailed is null) and (courseDetails.TotalAssignments is null or courseDetails.TotalAssignments < 1) then 0
								when asgnForCourse.IsAssignmentsComplete = 0 and courseDetails.TotalAssignments >= 1 then 1 
								when asgnForCourse.IsAssignmentsComplete = 1 then 0 
							end as bit) as IsAssignmentRequired,
							cast(case
								when asgnForCourse.IsAssignmentsComplete = 1 or asgnForCourse.IsAssignmentsFailed = 1 then 1 else 0
							end as bit) as IsAssignmentMarked,
		                    asgnForCourse.IsAssignmentsComplete,
		                    asgnForCourse.IsAssignmentsFailed,
		                    asgnForCourse.AssignmentsMarked,
		                    coalesce(asgnForCourse.AssignmentsCompleted, 0) as AssignmentsCompleted, -- avoid nulls if the user has not submitted any assignments
		                    courseDetails.TotalAssignments as AssignmentsInCourse,
		                    cro.ID as OnlineCourseRollupID,
                            cro.TestAttemptCount as TestAttemptCount,
                            cro.NavigationPercentage as NavigationPercentage,
                            cro.CoreVersion as PlaybackCoreVersion,
                            aa.CoreVersion as DeliveredCoreVersion,
                            aa.OnlineCourseVersion as DeliveredOnlineCourseVersion,
                            aa.ArtisanCourseID as DeliveredArtisanCourseID,
                            caa.CoreVersion as CurrentCoreVersion,
                            caa.OnlineCourseVersion as CurrentCourseVersion,
                            caa.ArtisanCourseID as CurrentArtisanCourseID,
                            tp.Name as TrainingProgramName,
                            cast(0 as bit) as IsHistorical,
                            tpr.DateCompleted,
                            tpr.IsComplete as IsTrainingProgramRollupComplete,
                            aa.CreatedOn as DeliveredArtisanCourseCreatedOn,
                            caa.CreatedOn as CurrentArtisanCourseCreatedOn
	                    from
		                    OnlineCourseRollup as cro with (nolock)
	                    left outer join                  
		                    onlineCourse as oc with (nolock)
	                    on
		                    oc.ID = cro.CourseID
		                left join
							ArtisanAudit aa with (nolock)
						on
							aa.ID = cro.ArtisanAuditID
						left join
	                   	(
	                   		select
	                   			a.TrainingProgramID,
	                   			a.OnlineCourseID,
	                   			a.UserID,
	                   			a.ArtisanCourseID,
	                   			sum(a.IsMarked) as AssignmentsMarked,
	                   			sum(a.IsComplete) as AssignmentsCompleted,
	                   			sum(a.IsFailed) as AssignmentsFailed,
	                   			count(a.ArtisanCourseID) as AssignmentsInCourse,
	                   			count(distinct a.AssignmentSummaryID) as AssignmentsAttempted,
	                   			case when sum(a.IsFailed) > 0 then 1 else 0 end as IsAssignmentsFailed,
	                   			case when sum(a.IsComplete) >= count(a.ArtisanCourseID) then 1 else 0 end as IsAssignmentsComplete
	                   		from
	                   		(
	                   			select
	                   				a.TrainingProgramID,
	                   				a.OnlineCourseID,
	                   				a.UserID,
	                   				a.AssignmentSummaryID,
	                   				s.ArtisanCourseID,
	                   				min(cast(a.IsMarked as int)) as IsMarked,
	                   				max(cast(a.IsComplete as int)) as IsComplete,
	                   				min(cast(a.IsFailed as int)) as IsFailed
	                   			from
	                   				AssignmentSummaryAttempts a
	                   			join 
	                   				AssignmentSummary s on s.ID = a.AssignmentSummaryID
	                   			where 
	                   				a.UserID = @userId
	                   			group by
	                   				a.TrainingProgramID,
	                   				a.OnlineCourseID,
	                   				a.UserID,
	                   				a.AssignmentSummaryID,
	                   				s.ArtisanCourseID
	                   		) a
	                   		group by
	                   			a.TrainingProgramID,
	                   			a.OnlineCourseID,
	                   			a.UserID,
	                   			a.ArtisanCourseID
	                   	) asgnForCourse 
	                   	on 
	                   		asgnForCourse.OnlineCourseID = oc.ID and
	                   		asgnForCourse.ArtisanCourseID = coalesce(aa.ArtisanCourseID, oc.PublishedArtisanCourseID) and --Fall back to online course PublishedArtisanCourseID if there is no ArtisanAuditID (Clear it on reset assignments in order to grab the latest)
	                   		asgnForCourse.UserID = @userId
	                   	left join
							(
								select 
									s.ArtisanCourseID,
									count(s.ArtisanSectionID) as TotalAssignments
								from
									AssignmentSummary s with (nolock)
								group by s.ArtisanCourseID
							) courseDetails
                        on 
                            courseDetails.ArtisanCourseID = coalesce(aa.ArtisanCourseID, oc.PublishedArtisanCourseID) -- Fall back to online course PublishedArtisanCourseID if there is no ArtisanAuditID
						left join
							ArtisanAudit caa with (nolock)
						on
							caa.OnlineCourseID = oc.ID and 
							caa.ArtisanCourseID = oc.PublishedArtisanCourseID and
							caa.OnlineCourseVersion = oc.[Version]
						left join 
                            TrainingProgram tp with (nolock)
                        on
                            tp.ID = cro.TrainingProgramID
                        left join
							TrainingProgramRollup tpr with (nolock)
						on
							tp.ID = tpr.TrainingProgramID and
							tpr.UserID = @userId
                        where
		                    cro.UserID = @userID
		                and
		                    -- there shouldn't be any rollups without courses, but you never know...
		                    oc.ID is not null
		                UNION
		                select
							0 as CourseID,
							'' as CourseName,
							2 as CourseTypeID,
							0 as ClassID,
							0 as RegistrationID,
							0 as RegistrationStatusID,
							cast(0 as bit) as AttendedIndicator,
							'' as PassOrFail,
							0 as Passed,
							'' as CompleteOrIncomplete,
							0 as Completed,
							0 as OnlineTestID,
							0 as SurveyID,
							cast(0 as bit) as PublicIndicator,
							'' as Score,
							'' as AttemptDate,
		                    0 as AttemptTime,
		                    0 as AttemptCount,
							'' as StartDate,
						    '' as EndDate,
						    tpr.TrainingProgramID as TrainingProgramID,
						    '' as WebinarRegistrationID,
						    '' as WebinarKey,
						    cast(0 as bit) as CanMoveForwardIndicator,
						    cast(0 as bit) as SurveyTaken,
						    cast(0 as bit) as IsSurveyMandatory,
						    cast(0 as bit) as IsAssignmentRequired,
						    cast(0 as bit) as IsAssignmentMarked,
						    cast(0 as bit) as IsAssignmentsComplete,
		                    cast(0 as bit) as IsAssignmentsFailed,
		                    0 as AssignmentsMarked,
		                    0 as AssignmentsCompleted,
		                    0 as AssignmentsInCourse,
		                    0 as OnlineCourseRollupID,
                            0 as TestAttemptCount,
                            0 as NavigationPercentage,
                            '' as PlaybackCoreVersion,
                            '' as DeliveredCoreVersion,
                            0 as DeliveredOnlineCourseVersion,
                            0 as DeliveredArtisanCourseID,
                            '' as CurrentCoreVersion,
                            0 as CurrentCourseVersion,
                            0 as CurrentArtisanCourseID,
                            tp.Name as TrainingProgramName,
                            cast(1 as bit) as IsHistorical,
                            tpr.DateCompleted as DateCompleted,
                            cast(1 as bit) as IsTrainingProgramRollupComplete,
                            null as DeliveredArtisanCourseCreatedOn,
                            null as CurrentArtisanCourseCreatedOn
                        from 
							TrainingProgramRollup tpr with (nolock)
						join
							TrainingProgram tp with (nolock)
						on
							tp.ID = tpr.TrainingProgramID
						where 
							tpr.UserID = @userID and
							tpr.IsCompleteOverride = 1 and
							tpr.TrainingProgramID not in (select TrainingProgramID from OnlineCourseRollup with (nolock) where UserID = @userId)
						UNION
	                    select
		                    CourseID,
		                    CourseName,
		                    1 as CourseTypeID,
		                    ClassID,
		                    RegistrationID,
		                    RegistrationStatusID,
		                    cast(AttendedIndicator as bit) as AttendedIndicator,
		                    case 
			                    when Passed = 0 and ScoreReported = 1 then 'Failed'
			                    when Passed = 1 and ScoreReported = 1 then 'Passed'
			                    else 'Unknown'
		                    end as PassOrFail,
		                    Passed,
		                    case 
			                    when ScoreReported = 1 and
									 (IsSurveyMandatory = 1 and SurveyTaken = 1 or IsSurveyMandatory = 0) then 'Complete'
			                    else 'Incomplete'
		                    end as CompleteOrIncomplete,
		                    ScoreReported as Completed,
		                    OnlineTestID,
		                    SurveyID,
		                    PublicIndicator,
		                    Score,
		                    AttemptDate,
		                    AttemptTime,
		                    AttemptCount,
		                    StartDate,
		                    EndDate,
		                    0 as TrainingProgramID,
		                    WebinarRegistrationID,
		                    WebinarKey,
		                    CanMoveForwardIndicator,
		                    cast(SurveyTaken as bit) as SurveyTaken,
		                    IsSurveyMandatory,
		                    cast(0 as bit) as IsAssignmentRequired,
		                    cast(0 as bit) as IsAssignmentMarked,
		                    0 as IsAssignmentsComplete,
		                    0 as IsAssignmentsFailed,
		                    0 as AssignmentsMarked,
		                    0 as AssignmentsCompleted,
		                    0 as AssignmentsInCourse,
		                    0 as OnlineCourseRollupID,
                            0 as TestAttemptCount,
                            0 as NavigationPercentage,
                            '' as PlaybackCoreVersion,
                            '' as DeliveredCoreVersion,
                            0 as DeliveredOnlineCourseVersion,
                            0 as DeliveredArtisanCourseID,
                            '' as CurrentCoreVersion,
                            0 as CurrentCourseVersion,
                            0 as CurrentArtisanCourseID,
                            '' as TrainingProgramName,
                            cast(0 as bit) as IsHistorical,
                            null as DateCompleted,
                            cast(0 as bit) as IsTrainingProgramRollupComplete,
                            null as DeliveredArtisanCourseCreatedOn,
                            null as CurrentArtisanCourseCreatedOn
	                    from
	                    (
		                    select
			                    cCourse.ID as CourseID,
			                    cCourse.[Name] as CourseName,
			                    cClass.ID as ClassID,
			                    cClass.WebinarKey as WebinarKey,
			                    cReg.ID as RegistrationID, 
			                    cReg.RegistrationStatusID as RegistrationStatusID,
			                    cReg.AttendedIndicator as AttendedIndicator,
			                    cReg.WebinarRegistrationID as WebinarRegistrationID,
			                    -- Whether or not the user has passed changes based on course type and completion type.
			                    -- This is where minimum passing scores are set for classroom courses.

                    -- Online courses have passing scores on a per-course basis, and the 'passed' flag will be set as appropriate.
			                    case
				                    when cCourseCT.CodeName = 'attendance' and cReg.AttendedIndicator = 1 
					                    then 1
				                    when cCourseCT.CodeName = 'attendance'
					                    then 0
									when cCourseCT.CodeName = 'numeric' and 
											( len(cReg.Score) = 3 or 
											cReg.Score >= cast(isnull(cPass.PassingScore, 70) as varchar(15)) )
					                    then 1
				                    when cCourseCT.CodeName = 'numeric' 
					                    then 0
				                    when cCourseCT.CodeName = 'letter' and (cReg.Score not in ('A','B','C','D'))
					                    then 0 
				                    when cCourseCT.CodeName = 'letter' 
					                    then 1 
				                    when cCourseCT.CodeName = 'onlinecourse' AND ocr.Success = 1
					                    then 1 
				                    when cCourseCT.CodeName = 'onlinecourse' 
					                    then 0 
			                    end as Passed,
			                    -- To determine if a user has attended a course with attendance based passing,

                    -- we have a bit of a conundrum - there's no way to know if they DIDN'T attend, since the default value for
                    -- attended is 0. Therefore, (and per BE's instructions) we assume that if the registration was modified after the 
                    -- last day of the class, then whatever indicator is in the db was the 'reported' value. So, if the date modified is
                    -- after the last day of the class, or the AttendedIndicator is true, the instructor reported a score.
                    -- For online courses, the 'Complete' flag will be set to '2' once a score has been sent to the server.
			                    case 

                    when cCourseCT.CodeName = 'attendance' and (cReg.ModifiedOn > vClassDMM.MaxStartDateTime or cReg.AttendedIndicator = 1) 
					                    then 1 
				                    when cCourseCT.CodeName = 'attendance' 
					                    then 0 
				                    when cCourseCT.CodeName = 'onlinecourse' and (ocr.Completion = 1 or ocr.Score > 0)
					                    then 1 
				                    when cCourseCT.CodeName = 'onlinecourse'
					                    then 0 
				                    when cReg.Score = '' 
					                    then 0
				                    else 1
			                    end as ScoreReported,
			                    cCourse.OnlineCourseID as OnlineTestID,
			                    cClass.SurveyID as SurveyID,
			                    cCourse.PublicIndicator as PublicIndicator,
			                    -- Calculate the score
			                    --    -if the score is based on attendance, they get 100 if they attended

                    --    -if the score is entered by the instructor, use that. Since this can be a letter or a number, we use nvarchar
			                    --    -if the score is based on taking an online course, use that score
			                    case 
				                    when cCourseCT.CodeName = 'attendance' and cReg.AttendedIndicator = 1
					                    then '100'
				                    when ocr.ID is null
					                    then cast(cReg.Score as nvarchar(50))
				                    else 
					                    cast(floor(round(ocr.Score,0)) as nvarchar(50)) 
			                    end as Score,
			                    null as AttemptDate,
			                    0 as AttemptTime,
			                    0 as AttemptCount,
			                    vClassDMM.MinStartDateTime as StartDate,
			                    vClassDMM.MaxStartDateTime as EndDate,
			                    cReg.CanMoveForwardIndicator as CanMoveForwardIndicator,
			                    case when ocrSurvey.Completion = 1 then 1 else 0 end as SurveyTaken,
			                    cCourse.IsSurveyMandatory as IsSurveyMandatory
		                    from         
			                    dbo.Registration as cReg  with (nolock)
		                    inner join
			                    dbo.[Status] as stat with (nolock) on cReg.RegistrationStatusID = stat.ID
		                    inner join
			                     dbo.Class as cClass with (nolock) ON cClass.ID = cReg.ClassID 
		                    inner join
			                    dbo.Course as cCourse with (nolock) ON cCourse.ID = cClass.CourseID 
		                    inner join
			                    dbo.CourseCompletionType as cCourseCT with (nolock) ON cCourseCT.ID = cCourse.CourseCompletionTypeID 
		                    inner join
			                    dbo.ClassDateMinMax as vClassDMM with (nolock) ON vClassDMM.ClassID = cClass.ID 
		                    left join
			                    dbo.PassingScore as cPass with (nolock) ON cPass.CustomerID = cClass.CustomerID 
		                    --left join
		                    --	dbo.[User] as cUser ON cUser.ID = cReg.RegistrantID 
		                    -- this join simply grabs the score from the user if they had an online course to take for this class
		                    left join
			                    dbo.OnlineCourseRollup as ocr with (nolock) ON 
				                    ocr.CourseID = cCourse.OnlineCourseID AND 
				                    ocr.UserID = @userid
				            -- Add on the rollup again to determine if the student has taken the survey
				            left join
								dbo.OnlineCourseRollup as ocrSurvey with (nolock) ON
									ocrSurvey.CourseID = cClass.SurveyID AND
									ocrSurvey.UserID = @userid
		                    where
			                    cReg.RegistrantID = @userid
	                    ) as X
	                    order by TrainingProgramName asc
                    end;
");
        }

    }
}
