﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration253 : Migration
    {
        public override void Up()
        {
            Execute(@"exec('create PROCEDURE [dbo].[GetReportDataSet] 
	@QueueID	int = 0
AS

exec SymphonyReporting.dbo.[GetReportDataSet]
	@QueueID = @QueueID')");
        }
    }
}