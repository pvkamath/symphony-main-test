﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration284 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER TABLE ParameterValue ADD CONSTRAINT uc_ParameterOption UNIQUE (ParameterID,ParameterSetOptionID)");
        }
    }
}