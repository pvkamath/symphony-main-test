﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration033 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanPages", "CourseID", DbType.Int32, 0, false);
            AddColumn("ArtisanSections", "CourseID", DbType.Int32, 0, false);
        }
    }
}