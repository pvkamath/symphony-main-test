﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration578 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE TABLE dbo.SecondaryCategory
(
	Id INT IDENTITY(1, 1) NOT NULL,
	Name VARCHAR(200)
)

ALTER TABLE dbo.SecondaryCategory
ADD CONSTRAINT PK_SecondaryCategory__Id PRIMARY KEY (Id)

ALTER TABLE dbo.AuthorDetails
ADD CONSTRAINT FK_AuthorDetails_Users__Id FOREIGN KEY (UserId) REFERENCES dbo.[User] (ID)


");
            Execute(@"

ALTER TABLE dbo.[User]
ADD IsAuthor BIT NULL

");

        }
    }
}
