﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration679 : Migration
    {
       
        public override void Up()
        {
            // Repairs ordering for GetTrainingProgramsForUser, adds order by completed state
            Execute(@"ALTER procedure [dbo].[GetTrainingProgramsForUser](@userId int, @newHire bit, @searchText nvarchar(50), @orderBy nvarchar(50), @orderDir varchar(4), @pageIndex int, @pageSize int)
as
begin
	set @orderDir = lower(@orderDir);
	set @orderBy = lower(@orderBy);
	-- create a date time that represents today without the time
	declare @today datetime
	declare @hireDate datetime
	declare @newHireEndDate datetime
	declare @newHireDuration int
	declare @newHireTransitionPeriod int

	--set @pageIndex = @pageIndex + 1
	
	set @today = (select cast(floor(cast(getdate() as decimal(12, 5))) as datetime));
	set @hireDate = (select HireDate from [user] where id = @userId);
	set @newHireEndDate = (select NewHireEndDate from [user] where id = @userId);
	set @newHireDuration = (select NewHireDuration from Customer where ID = (select CustomerID from [user] where id = @userId));
	set @newHireTransitionPeriod = (select NewHireTransitionPeriod from Customer where ID = (select CustomerID from [user] where id = @userId));


	--if(@newHireTransitionPeriod is null or @newHireTransitionPeriod = 0)
	--begin
		if(@newHireEndDate is null and @newHire = 0)
		begin
			set @newHireEndDate = @hireDate
		end;
	--end;
	
	
	-- if the user's new hire date + transition > enddate, exclude it
	
	-- get all the tp details
	with TP as(
		select
			TP.ID, TP.CustomerID, TP.OwnerID, TP.Name, TP.InternalCode, TP.Cost, TP.[Description], 
			TP.IsNewHire, TP.IsLive, TP.EnforceRequiredOrder, 
			TP.MinimumElectives, TP.FinalAssessmentCourseID, TP.FinalAssessmentCourseTypeID, 
			TP.ModifiedBy, TP.CreatedBy, TP.ModifiedOn, TP.CreatedOn,
			TP.NewHireOffsetEnabled, TP.NewHireStartDateOffset, TP.NewHireDueDateOffset, TP.NewHireEndDateOffset,
			TP.NewHireTransitionPeriod, TP.IsSalesforce, tpb.Name as PurchasedFromBundleName,
			case when tpb.Name is not null
				then tpb.Name
				else tp.Name
			end as SortName,
			tpbm.[Order] as [Order],
			case when @newHire = 1 
				then dateadd(d, 
						case when tp.NewHireOffsetEnabled = 1 
							then tp.NewHireStartDateOffset 
							else 0
						end, @hireDate) 
				else tp.StartDate end as StartDate,
			case when @newHire = 1 
				then dateadd(d, 
						case when tp.NewHireOffsetEnabled = 1 
							then tp.NewHireDueDateOffset 
							else @newHireDuration
						end, @hireDate) 
				else tp.DueDate end as DueDate,
				
			case when @newHire = 1 
				then dateadd(d, 
						case when tp.NewHireOffsetEnabled = 1 
							then tp.NewHireEndDateOffset 
							else @newHireDuration
						end, @hireDate) 
				else tp.EndDate end as EndDate,
				
			T.CourseCount,
			ROW_NUMBER() over (order by T.TrainingProgramID) as MyRow,
			(
				SELECT TOP 1 mb.ID
				FROM MessageBoard as mb
				WHERE mb.TrainingProgramID = TP.ID
			) as MessageBoardID,
			CASE 
				WHEN tp.IsSalesforce = 1 and tp.PurchasedFromBundleID is null THEN parentTp.Sku
			    WHEN tp.IsSalesforce = 1 and tp.PurchasedFromBundleID is not null THEN tpb.Sku
			    WHEN tp.IsSalesforce = 0 THEN tp.Sku
			END as Sku,
			tp.ParentTrainingProgramID
		from
			TrainingProgram tp
		join
			(
				-- make sure we only ever see 1 instance of a given tp
				select
					distinct(X.TrainingProgramID) as TrainingProgramID, 
					(count(distinct cl.ID)) as CourseCount
				from
					(
						-- get the assigned tps
						select
							l.TrainingProgramID, 
							tp.Name,
							tp.MinimumElectives
						from
							hierarchytotrainingprogramlink l
						join
							(

								select ID, TypeID from dbo.[fGetHierarchyParents]((select locationid from [user] where id = @userid), 1)
								
								union
								
								select ID, TypeID from dbo.[fGetHierarchyParents]((select jobroleid from [user] where id = @userid), 2)
								
								union
								
								select f.ID, f.TypeID from useraudience 
								cross apply dbo.[fGetHierarchyParents](audienceid, 3) as f
								where userid = @userid
								
								union
								
								select
									@userId, 4 -- 'user' type id
							) as H
						on
							h.TypeID = l.HierarchyTypeID
							and
							h.ID = l.HierarchyNodeID
						join
							TrainingProgram tp
						on
							tp.ID = l.TrainingProgramID
						group by
							l.trainingprogramid, tp.name, tp.MinimumElectives
							
					) as X
				left join
					dbo.TrainingProgramToCourseLink cl
				on
					X.TrainingProgramID = cl.TrainingProgramID
				group by
					X.TrainingProgramID, X.MinimumElectives
			)
		as T
		on
			tp.ID = T.TrainingProgramID
		left join
			dbo.TrainingProgram parentTp
		on
			parentTp.ID = tp.ParentTrainingProgramID
		left join
			dbo.TrainingProgramBundle tpb
		on
			tpb.ID = tp.PurchasedFromBundleID
		left join
			dbo.TrainingProgramBundleMap tpbm
		on
			tpbm.TrainingProgramID = tp.ParentTrainingProgramID
			and tpbm.ID = tpb.ID
		where
			(
				tp.Name like '%' + @searchText + '%'
				or tpb.Name like '%' + @searchText + '%'
				or CASE 
					WHEN tp.IsSalesforce = 1 and tp.PurchasedFromBundleID is null THEN parentTp.Sku
					WHEN tp.IsSalesforce = 1 and tp.PurchasedFromBundleID is not null THEN tpb.Sku
					WHEN tp.IsSalesforce = 0 THEN tp.Sku
				END like '%' + @searchText + '%'
			)
		and
			tp.IsNewHire = @newHire
		and 
			tp.IsLive = 1
	)
	select 
		d.ID, d.CustomerID, d.OwnerID, d.Name, d.InternalCode, d.Cost, d.[Description], d.IsNewHire, d.IsLive, 
		d.StartDate as StartDate,
		d.EndDate as EndDate,
		-- new hire end date is the due date
		d.DueDate as DueDate,
		d.EnforceRequiredOrder, d.MinimumElectives, d.FinalAssessmentCourseID, 
		d.FinalAssessmentCourseTypeID, d.ModifiedBy, d.CreatedBy, d.ModifiedOn, d.CreatedOn,
		d.CourseCount,
		max(TotalSize) as TotalSize,
		d.MessageBoardID,
		d.Sku,
		d.ParentTrainingProgramID
	from
	(
		select
			TP.ID, TP.CustomerID, TP.OwnerID, TP.Name, TP.InternalCode, TP.Cost, TP.[Description], TP.IsNewHire, TP.IsLive, 
			TP.StartDate as StartDate,
			TP.EndDate as EndDate,
			-- new hire end date is the due date
			TP.DueDate as DueDate,
			TP.EnforceRequiredOrder, TP.MinimumElectives, TP.FinalAssessmentCourseID, 
			TP.FinalAssessmentCourseTypeID, TP.ModifiedBy, TP.CreatedBy, TP.ModifiedOn, TP.CreatedOn,
			TP.CourseCount,
			(select count(*) from 
				TP 
			join
				Customer
			on
				Customer.id = Customerid
			where
				StartDate <= @today
			and
				EndDate >= @today
			and
				EndDate >= case when @newHire = 0 then dateadd(d, coalesce(TP.NewHireTransitionPeriod, Customer.NewHireTransitionPeriod, 0), @newHireEndDate) else @today end
			) as TotalSize,
			row_number() over (order by 
				case when @orderBy = 'Name' and @orderDir = 'asc' then TP.SortName end, 
				case when @orderBy = 'Name' and @orderDir = 'desc' then TP.SortName end desc,
				case when @orderBy = 'Description' and @orderDir = 'asc' then TP.[Description] end,
				case when @orderBy = 'Description' and @orderDir = 'desc' then TP.[Description] end desc,
				case when @orderBy = 'StartDate' and @orderDir = 'asc' then TP.StartDate end,
				case when @orderBy = 'StartDate' and @orderDir = 'desc' then TP.StartDate end desc,
				case when @orderBy = 'EndDate' and @orderDir = 'asc' then TP.EndDate end,
				case when @orderBy = 'EndDate' and @orderDir = 'desc' then TP.EndDate end desc,
				case when @orderBy = 'DueDate' and @orderDir = 'asc' then TP.DueDate end,
				case when @orderBy = 'DueDate' and @orderDir = 'desc' then TP.DueDate end desc,
				case when @orderBy = 'Sku' and @orderDir = 'desc' then TP.Sku end desc,			
				case when @orderBy = 'Sku' and @orderDir = 'asc' then TP.Sku end asc,
				case when @orderBy = 'PurchasedFromBundleName' and @orderDir = 'desc' then TP.PurchasedFromBundleName end desc,			
				case when @orderBy = 'PurchasedFromBundleName' and @orderDir = 'asc' then TP.PurchasedFromBundleName end asc,
				case when @orderBy = 'IsSalesforce' and @orderDir = 'desc' then TP.IsSalesforce end desc,			
				case when @orderBy = 'IsSalesforce' and @orderDir = 'asc' then TP.IsSalesforce end asc,
				case when @orderBy = 'Completed' and @orderDir = 'desc' then coalesce(nullif(tpr.IsCompleteOverride,0), tpr.IsComplete) end desc,
				case when @orderBy = 'Completed' and @orderDir = 'asc' then coalesce(nullif(tpr.IsCompleteOverride,0), tpr.IsComplete) end asc,
				case when 1 = 1 then StartDate end, TP.PurchasedFromBundleName, TP.[Order])  as r,
			TP.MessageBoardID,
			TP.Sku,
			TP.IsSalesforce,
			TP.ParentTrainingProgramID
		from
			TP
		join
			Customer
		on
			Customer.id = Customerid
		--where
		--	myrow between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
		--and 
		left join 
			TrainingProgramRollup tpr
		on 
			tpr.TrainingProgramID = TP.ID and tpr.UserID = @userId
		where
			StartDate <= @today
		and
			EndDate >= @today
		and
			EndDate >= case when @newHire = 0 then dateadd(d, coalesce(TP.NewHireTransitionPeriod, Customer.NewHireTransitionPeriod, 0), @newHireEndDate) else @today end
		
		group by
			TP.ID, TP.CustomerID, TP.OwnerID, TP.Name, TP.InternalCode, TP.Cost, TP.[Description], 
			TP.IsNewHire, TP.IsLive, TP.StartDate, TP.EndDate, TP.DueDate, TP.EnforceRequiredOrder, 
			TP.MinimumElectives, TP.FinalAssessmentCourseID, TP.FinalAssessmentCourseTypeID, 
			TP.ModifiedBy, TP.CreatedBy, TP.ModifiedOn, TP.CreatedOn, TP.CourseCount, TP.MessageBoardID, TP.Sku, TP.PurchasedFromBundleName, TP.IsSalesforce, TP.SortName, TP.[Order], TP.ParentTrainingProgramID,
			tpr.TrainingProgramID, tpr.UserID, tpr.IsCompleteOverride, tpr.IsComplete
	) as d
	where
		d.r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
	
	group by
		d.ID, d.CustomerID, d.OwnerID, d.Name, d.InternalCode, d.Cost, d.[Description], 
		d.IsNewHire, d.IsLive, d.StartDate, d.EndDate, d.DueDate, d.EnforceRequiredOrder, 
		d.MinimumElectives, d.FinalAssessmentCourseID, d.FinalAssessmentCourseTypeID, 
		d.ModifiedBy, d.CreatedBy, d.ModifiedOn, d.CreatedOn, d.CourseCount, d.MessageBoardID, d.Sku, d.r, d.ParentTrainingProgramID
	order by d.r
	
end;");
        }

    }
}