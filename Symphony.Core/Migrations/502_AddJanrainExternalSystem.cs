using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration502 : Migration
    {

        public override void Up()
		{
            Execute(@"
INSERT INTO dbo.ExternalSystem (SystemName, SystemCodeName, LoginUrl, IsRequired)
VALUES (N'Janrain', N'janrain', N'https://oncourselearning.janraincapture.com', 0);
");
		}
	}
}