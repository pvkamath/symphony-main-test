﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration670 : Migration
    {
        
        public override void Up()
        {
            TableSchema.Table userDataFields = CreateTableWithKey("UserDataField", "ID");
            userDataFields.AddColumn("CustomerID", DbType.Int32, 0, false);
            userDataFields.AddColumn("Xtype", DbType.String, 64, true);
            userDataFields.AddColumn("Config", DbType.String, 2000, true);
            userDataFields.AddColumn("CodeName", DbType.String, 64, false);
            userDataFields.AddColumn("DisplayName", DbType.String, 128, false);
            userDataFields.AddColumn("CategoryCodeName", DbType.String, 128, true);
            userDataFields.AddColumn("CategoryDisplayName", DbType.String, 128, true);
            userDataFields.AddColumn("Validator", DbType.String, 128, true);
            AddSubSonicStateColumns(userDataFields);

            TableSchema.Table licenseUserDataFieldsMap = CreateTableWithKey("UserDataFieldLicenseMap", "ID");
            licenseUserDataFieldsMap.AddColumn("LicenseID", DbType.Int32, 0, false);
            licenseUserDataFieldsMap.AddColumn("UserDataFieldID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(licenseUserDataFieldsMap);

            TableSchema.Table userDataFieldsMap = CreateTableWithKey("UserDataFieldUserMap", "ID");
            userDataFieldsMap.AddColumn("UserID", DbType.Int32, 0, false);
            userDataFieldsMap.AddColumn("UserDataFieldID", DbType.Int32, 0, false);
            userDataFieldsMap.AddColumn("UserValue", DbType.String, 1024, false);
            AddSubSonicStateColumns(userDataFieldsMap);
        }
    }
}
