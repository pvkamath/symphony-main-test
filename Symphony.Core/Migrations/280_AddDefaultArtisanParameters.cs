﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration280 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE [dbo].[ArtisanCourses] ADD [DefaultParametersJSON] nvarchar(MAX) DEFAULT '' NOT NULL"); 
        }
    }
}