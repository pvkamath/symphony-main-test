﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration132 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "OriginalCourseId", DbType.Int32);
        }
    }
}