﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration883 : Migration
    {

        public override void Up()
        {
            Execute(@"
update 
	Templates
set 
	IsConfigurableForTrainingProgram = 0
where 
	CodeName = 'SalesforceNewEnrollment'

delete 
from 
	TrainingProgramTemplates
where TemplateID in (
	select 
		ID
	from 
		Templates
	where
		CodeName = 'SalesforceNewEnrollment'
)
");
        }
    }
}
