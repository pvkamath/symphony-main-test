﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration581 : Migration
    {
        public override void Up()
        {
            Execute(@"

ALTER TABLE dbo.ArtisanCourses
ADD Accreditation VARCHAR(3000) NULL

");
            
        }

    }
}
