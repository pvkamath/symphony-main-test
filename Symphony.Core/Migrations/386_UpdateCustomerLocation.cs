﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration386 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE Customer DROP COLUMN CustomerLocation;");
            AddColumn("Customer", "LocationID", DbType.Int32, 0, true);
        }
    }
}