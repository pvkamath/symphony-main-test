﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration005 : Migration
    {
        public override void Up()
        {
            Execute("alter table class add WebinarKey nvarchar(30) not null default ''");
        }
    }
}