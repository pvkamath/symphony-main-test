﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration798 : Migration
    {
        public override void Up()
        {
            TableSchema.Table assignmentTable = CreateTableWithKey("AssignmentSummary", "ID");
            assignmentTable.AddColumn("ArtisanCourseID", DbType.Int32, 0, false);
            assignmentTable.AddColumn("ArtisanSectionID", DbType.Int32, 0, false);
            assignmentTable.AddColumn("Name", DbType.String, 512, false);
            assignmentTable.AddColumn("NumQuestions", DbType.Int32, 0, false);
            assignmentTable.AddColumn("PassingScore", DbType.Int32, 0, false);
            AddSubSonicStateColumns(assignmentTable);

            TableSchema.Table assignmentQuestionsTable = CreateTableWithKey("AssignmentSummaryQuestions", "ID");
            assignmentQuestionsTable.AddColumn("AssignmentSummaryID", DbType.Int32, 0, false);
            assignmentQuestionsTable.AddColumn("ArtisanSectionID", DbType.Int32, 0, false);
            assignmentQuestionsTable.AddColumn("ArtisanPageID", DbType.Int32, 0, false);
            assignmentQuestionsTable.AddLongText("Question", false, "");
            assignmentQuestionsTable.AddLongText("CorrectResponse", false, "");
            assignmentQuestionsTable.AddColumn("Sort", DbType.Int32, 0, false);
            AddSubSonicStateColumns(assignmentQuestionsTable);

            TableSchema.Table assignmentAttemptsTable = CreateTableWithKey("AssignmentSummaryAttempts", "ID");
            assignmentAttemptsTable.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            assignmentAttemptsTable.AddColumn("OnlineCourseID", DbType.Int32, 0, false);
            assignmentAttemptsTable.AddColumn("UserID", DbType.Int32, 0, false);
            assignmentAttemptsTable.AddColumn("AssignmentSummaryID", DbType.Int32, 0, false);
            assignmentAttemptsTable.AddColumn("AttemptID", DbType.Int32, 0, false);
            assignmentAttemptsTable.AddColumn("QuestionsAnswered", DbType.Int32, 0, false, "0");
            assignmentAttemptsTable.AddColumn("QuestionsMarked", DbType.Int32, 0, false, "0");
            assignmentAttemptsTable.AddColumn("QuestionsCorrect", DbType.Int32, 0, false, "0");
            assignmentAttemptsTable.AddColumn("QuestionsFailed", DbType.Int32, 0, false, "0");
            assignmentAttemptsTable.AddColumn("TotalQuestions", DbType.Int32, 0, false, "0");
            assignmentAttemptsTable.AddColumn("GradedOn", DbType.DateTime, 0, true);
            assignmentAttemptsTable.AddColumn("SubmittedOn", DbType.DateTime, 0, true);
            assignmentAttemptsTable.AddColumn("Score", DbType.Int32, 0, false, "0");
            assignmentAttemptsTable.AddColumn("PassingScore", DbType.Int32, 0, false, "0");
            assignmentAttemptsTable.AddColumn("IsMarked", DbType.Boolean, 0, false, "0");
            assignmentAttemptsTable.AddColumn("IsComplete", DbType.Boolean, 0, false, "0");
            assignmentAttemptsTable.AddColumn("IsFailed", DbType.Boolean, 0, false, "0");
            AddSubSonicStateColumns(assignmentAttemptsTable);

        }
    }
}