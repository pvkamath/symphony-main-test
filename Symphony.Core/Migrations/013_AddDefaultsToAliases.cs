﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration013 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(326);
            sb.AppendFormat(@"ALTER TABLE dbo.Customer{0}", Environment.NewLine);
            sb.AppendFormat(@"ADD CONSTRAINT def_location_alias{0}", Environment.NewLine);
            sb.AppendFormat(@"DEFAULT 'Location'{0}", Environment.NewLine);
            sb.AppendFormat(@"FOR LocationAlias{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"ALTER TABLE dbo.Customer{0}", Environment.NewLine);
            sb.AppendFormat(@"ADD CONSTRAINT def_jobrole_alias{0}", Environment.NewLine);
            sb.AppendFormat(@"DEFAULT 'Job Role'{0}", Environment.NewLine);
            sb.AppendFormat(@"FOR JobRoleAlias{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"ALTER TABLE dbo.Customer{0}", Environment.NewLine);
            sb.AppendFormat(@"ADD CONSTRAINT def_audience_alias{0}", Environment.NewLine);
            sb.AppendFormat(@"DEFAULT 'Audience'{0}", Environment.NewLine);
            sb.AppendFormat(@"FOR AudienceAlias");

            Execute(sb.ToString());
        }
    }
}