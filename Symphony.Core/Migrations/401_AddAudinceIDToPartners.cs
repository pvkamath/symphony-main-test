﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration401 : Migration
    {
        public override void Up()
        {
            AddColumn("SalesforcePartner", "AudienceID", DbType.Int32, 0, true);
        }
    }
}