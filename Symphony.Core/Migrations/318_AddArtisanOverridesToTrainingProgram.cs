﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration318: Migration
    {
        public override void Up()
        {

            Execute(@"ALTER TABLE TrainingProgram
            ADD
    [ShowPretestOverride] [bit] NULL,
	[ShowPostTestOverride] [bit] NULL,
	[PassingScoreOverride] [int] NULL,
	[SkinOverride] [int] NULL,
	[PreTestTypeOverride] [int] NULL,
	[PostTestTypeOverride] [int] NULL,
	[NavigationMethodOverride] [int] NULL,
	[DisclaimerOverride] [nvarchar](50) NULL,
	[ContactEmailOverride] [nvarchar](50) NULL,
	[ShowObjectivesOverride] [bit] NULL,
	[PretestTestOutOverride] [bit] NULL,
	[Retries] [int] NULL,
	[RandomizeQuestionsOverride] [bit] NULL,
	[RandomizeAnswersOverride] [bit] NULL,
	[ReviewMethodOverride] [int] NULL,
	[MarkingMethodOverride] [int] NULL,
	[InactivityMethodOverride] [int] NULL,
	[InactivityTimeoutOverride] [int] NULL,
	[MinimumPageTimeOverride] [int] NULL,
	[MaximumQuestionTimeOverride] [int] NULL;");
        }
    }
}