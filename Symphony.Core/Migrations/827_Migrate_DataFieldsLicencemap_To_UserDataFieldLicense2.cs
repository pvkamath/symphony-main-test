﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration827 : Migration
    {
        public override void Up()
        {
            Execute(@"
                insert into UserDataFieldLicenseMap (LicenseID, UserDataFieldID, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy)
                select 
                	ldf.LicenseID, 
                    ldf.LicenseDataFieldID,
                    ldf.CreatedOn,
                    ldf.ModifiedOn,
                    ldf.CreatedBy,
                    ldf.ModifiedBy
                from LicenseDataFieldsToLicensesMap ldf
                left join UserDataFieldLicenseMap udf
                	on udf.LicenseID = ldf.LicenseID and
                	   udf.UserDataFieldID = ldf.LicenseDataFieldID
                where udf.ID is null
");
        }
    }
}
