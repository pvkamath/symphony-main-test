﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    class Migration588 : Migration
    {
        public override void Up()
        {
            Execute(@"

ALTER VIEW [dbo].[Assignments]
AS
select 
    s.ID as SectionID, 
    o.Name as CourseName, 
    a.CourseID, 
    a.TrainingProgramID, 
    a.Attempt, 
    s.Name, 
    max(a.[Date]) as [Date],
    max(a.GradedDate) as GradedDate, 
    a.UserID, 
    count(distinct a.PageId) as NumQuestions,
    pCount.totalQuestions as TotalQuestions,
    c.NumCorrect,
    cast(c.NumCorrect as float) / cast(pCount.totalQuestions as float) as Score,
    case
		when c.MarkedResponses = count(distinct a.PageId) then 1
		else 0
	end as IsMarked,
    sp.CourseID as ArtisanCourseID -- Get the actual published course id for this page from ArtisanSectionPages
from OnlineCourseAssignments a with (nolock)
join ArtisanPages p on p.ID = a.PageID
join ArtisanSectionPages sp on p.ID = sp.PageID
join ArtisanSections s on s.ID = sp.SectionID
join OnlineCourse o on a.CourseID = o.ID
join (
	select 
		artSP.SectionID, 
		count(distinct artSP.PageID) as totalQuestions
	from ArtisanSectionPages artSP with (nolock)
	join ArtisanPages artP on artP.ID = artSP.PageID and artP.QuestionType = 8
	group by artSP.SectionID
) pCount on pCount.SectionID = s.ID
join (
	select 
		ia.UserID, 
		ia.CourseID, 
		ia.TrainingProgramID, 
		ia.Attempt,
		isp.SectionID, 
		sum(cast(ia.IsCorrect AS INT)) as NumCorrect,
		sum(
			case
				when ia.ResponseStatus > 1 then 1
				when ia.ResponseStatus <= 1 then 0
			end
		) as MarkedResponses
	from OnlineCourseAssignments ia with (nolock)
	join ArtisanSectionPages isp on isp.PageID = ia.PageID
	group by isp.SectionID, ia.Attempt, ia.CourseID, ia.TrainingProgramID, ia.UserID
) c on c.UserID = a.UserID and 
		c.CourseID = a.CourseID and 
		c.TrainingProgramID = a.TrainingProgramID and
		c.SectionID = s.ID and
		c.Attempt = a.Attempt
WHERE
	ISNULL(a.IsReset, 0) != 1
group by s.ID, s.Name, a.Attempt, a.CourseID, o.Name, a.TrainingProgramID, a.UserID, c.NumCorrect, c.MarkedResponses, pCount.totalQuestions, sp.CourseID; -- Adding a group by course id - shouldn't change anything with grouping since we are grouping by sections already anyway 


");
        }
    }
}
