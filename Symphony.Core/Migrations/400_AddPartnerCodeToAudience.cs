﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration400 : Migration
    {
        public override void Up()
        {
            AddColumn("Audience", "SalesforcePartnerCode", DbType.String, 128, true);
        }
    }
}