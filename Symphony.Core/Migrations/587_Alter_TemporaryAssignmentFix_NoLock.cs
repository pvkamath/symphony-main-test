﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration587 : Migration
    {
        public override void Up()
        {
            // Temporary fix for a long running query - adds sp that combines all assignment views in order to filter the data sooner
            Execute(@"
ALTER procedure [dbo].[GetUsersWithAssignmentsForTrainingProgram]
	(@trainingProgramId int, @sessionId int, @search nvarchar(50), @orderBy nvarchar(50), @orderDir varchar(4), @pageIndex int, @pageSize int)
as
begin

with AssignmentsViewCTE as (
	-- Coppied from the Assignments view which is the first view that references OnlineCourseAssignments
	-- Filterining added here in order to reduce the amount of data on subsequent views
	select 
		s.ID as SectionID, 
		o.Name as CourseName, 
		a.CourseID, 
		a.TrainingProgramID, 
		a.Attempt, 
		s.Name, 
		max(a.[Date]) as [Date],
		max(a.GradedDate) as GradedDate, 
		a.UserID, 
		count(distinct a.PageId) as NumQuestions,
		pCount.totalQuestions as TotalQuestions,
		c.NumCorrect,
		cast(c.NumCorrect as float) / cast(pCount.totalQuestions as float) as Score,
		case
			when c.MarkedResponses = count(distinct a.PageId) then 1
			else 0
		end as IsMarked,
	    sp.CourseID as ArtisanCourseID
	from OnlineCourseAssignments a with (nolock)
	join ArtisanPages p on p.ID = a.PageID
	join ArtisanSectionPages sp on p.ID = sp.PageID
	join ArtisanSections s on s.ID = sp.SectionID
	join OnlineCourse o on a.CourseID = o.ID
	join (
		select 
			artSP.SectionID, 
			count(distinct artSP.PageID) as totalQuestions
		from ArtisanSectionPages artSP with (nolock)
		join ArtisanPages artP on artP.ID = artSP.PageID and artP.QuestionType = 8
		group by artSP.SectionID
	) pCount on pCount.SectionID = s.ID
	join (
		select 
			ia.UserID, 
			ia.CourseID, 
			ia.TrainingProgramID, 
			ia.Attempt,
			isp.SectionID, 
			sum(cast(ia.IsCorrect AS INT)) as NumCorrect,
			sum(
				case
					when ia.ResponseStatus > 1 then 1
					when ia.ResponseStatus <= 1 then 0
				end
			) as MarkedResponses
		from OnlineCourseAssignments ia with (nolock)
		join ArtisanSectionPages isp on isp.PageID = ia.PageID
		group by isp.SectionID, ia.Attempt, ia.CourseID, ia.TrainingProgramID, ia.UserID
	) c on c.UserID = a.UserID and 
		   c.CourseID = a.CourseID and 
		   c.TrainingProgramID = a.TrainingProgramID and
		   c.SectionID = s.ID and
		   c.Attempt = a.Attempt
	-- filtering
	where a.TrainingProgramID in (select ID from TrainingProgram where ID = @trainingProgramId or ParentTrainingProgramID = @trainingProgramId)
	AND ISNULL(a.IsReset, 0) != 1
	group by s.ID, s.Name, a.Attempt, a.CourseID, o.Name, a.TrainingProgramID, a.UserID, c.NumCorrect, c.MarkedResponses, pCount.totalQuestions, sp.CourseID
), 
AssignmentsLastAttemptViewCTE as
(
	-- Copy pasted from the assignmentsLastAttemptView, only modification is to use AssignmentsViewCTE instead of the Assignments view
	-- Rolls up the latest attempt for each assignment
	select 
		lastAttempt.Attempt, -- number of the attempt
		lastAttempt.CourseID,
		lastAttempt.SectionID,
		lastAttempt.TrainingProgramID,
		lastAttempt.UserID,
		asg.Score * 100 as Score,
		asg.IsMarked,
		-- Add the total number of assignments attempted for the course, this is for convenience to find any courses where # assignments attempted < total assignments in course
		( 
			select 
				count(distinct AssignmentsViewCTE.SectionID) as AssignmentsAttempted
			from
				AssignmentsViewCTE
			where 
				AssignmentsViewCTE.CourseID = lastAttempt.CourseID and
				AssignmentsViewCTE.UserID = lastAttempt.UserID
			group by
				AssignmentsViewCTE.CourseID, 
				AssignmentsViewCTE.UserID
		) as AssignmentsAttempted,
		-- The total assignments in the course
		courseDetails.TotalAssignments as AssignmentsInCourse,
		case 
			when s.PassingScore > 0 then s.PassingScore
			else coalesce(o.PassingScoreOverride, c.PassingScoreOverride, courseDetails.PassingScore)
		end as PassingScore,
		asg.[Date] as [Date],
		asg.GradedDate as [GradedDate]
	from
	(
	-- first find the lastest attempt
		select 
			max(AssignmentsViewCTE.Attempt) as Attempt,
			AssignmentsViewCTE.CourseID, 
			AssignmentsViewCTE.TrainingProgramID, 
			AssignmentsViewCTE.SectionID, 
			AssignmentsViewCTE.UserID
		from 
			AssignmentsViewCTE
		group by 
			AssignmentsViewCTE.CourseID, 
			AssignmentsViewCTE.TrainingProgramID, 
			AssignmentsViewCTE.SectionID, 
			AssignmentsViewCTE.UserID
	) lastAttempt
	join -- join back to assignments to include the details of that attempt
		AssignmentsViewCTE asg 
	on 
		asg.Attempt = lastAttempt.Attempt and 
		asg.CourseID = lastAttempt.CourseID and
		asg.SectionID = lastAttempt.SectionID and
		asg.TrainingProgramID = lastAttempt.TrainingProgramID and
		asg.UserID = lastAttempt.UserID
	join  
		OnlineCourse o with (nolock)
	on 
		o.ID = lastAttempt.CourseID
	join AssignmentsArtisanCourseDetails courseDetails
	on
		courseDetails.ArtisanCourseID = asg.ArtisanCourseID
	join -- get the passing score for the assignment
		ArtisanSections s with (nolock)
	on 
		lastAttempt.SectionID = s.ID
	join -- customer override of passing score
		Customer c with (nolock)
	on 
		c.ID = o.CustomerID
), 
AssignmentsForCourseViewCTE as
(
	-- Copy pasted from the AssignmentsForCourse view. This uses the AssignmentsLastAttemptViewCTE instead of the AssignmentsForCourse view
	-- Rolls up status of each assignment for the user so we can easily determine if the assignments for a particular course have been completed by a user
	select 
		asgnLastAttempt.CourseID,
		asgnLastAttempt.UserID,
		asgnLastAttempt.TrainingProgramID,
		asgnLastAttempt.AssignmentsAttempted,
		asgnLastAttempt.AssignmentsInCourse,
		sum(asgnLastAttempt.IsMarked) as AssignmentsMarked,
		sum(case when asgnLastAttempt.Score >= asgnLastAttempt.PassingScore and asgnLastAttempt.Score > 0 then 1 else 0 end) as AssignmentsCompleted,
		(
			case 
				when 
					sum(asgnLastAttempt.IsMarked) = asgnLastAttempt.AssignmentsInCourse and 
					sum(case when asgnLastAttempt.Score >= asgnLastAttempt.PassingScore and asgnLastAttempt.Score > 0 then 1 else 0 end) < asgnLastAttempt.AssignmentsInCourse 
				then 
					1 -- Number of marked assignments is equal to number of assignments, but number of passed assignmnets < total assignments
				else 
					0 -- Either not all assignments have been marked yet, or all assignmnets have been marked and they have been passed (in which case IsAssignmentsPassed will be 1)
			end
		) as IsAssignmentsFailed,
		(
			case 
				when 
					sum(case when asgnLastAttempt.Score >= asgnLastAttempt.PassingScore and asgnLastAttempt.Score > 0 then 1 else 0 end) < asgnLastAttempt.AssignmentsInCourse 
				then 
					0 -- Number of passed assignments is less than number of assignments in course
				else 
					1 -- number of passed assignments is equal to the number of assignments in course
			end
		) as IsAssignmentsComplete,
		max(asgnLastAttempt.[Date]) as [Date],
		max(asgnLastAttempt.[GradedDate]) as [GradedDate]
	from 
		AssignmentsLastAttemptViewCTE asgnLastAttempt
	group by
		asgnLastAttempt.CourseID,
		asgnLastAttempt.TrainingProgramID,
		asgnLastAttempt.UserID,
		asgnLastAttempt.AssignmentsAttempted,
		asgnLastAttempt.AssignmentsInCourse
), AssignmentTotals as (
	select
		SUM(a.AssignmentsAttempted) as AssignmentsAttempted,
		SUM(a.AssignmentsMarked) as AssignmentsMarked,
		SUM(a.AssignmentsInCourse) as AssignmentsInCourse,
		SUM(a.AssignmentsCompleted) as AssignmentsCompleted,
		MAX(a.IsAssignmentsFailed) as IsAssignmentsFailed,
		MIN(a.IsAssignmentsComplete) as IsAssignmentsComplete,
		MAX(a.[Date]) as MaxSubmitDate,
		MAX(a.GradedDate) as MaxGradedDate,
		u.[ID],
		u.FirstName,
		u.LastName, 
		u.MiddleName,
		cl.ID as ClassID,
		cl.Name as ClassName,
		cmm.MinStartDateTime as ClassStartTime,
		row_number() over 
		(
			order by cmm.MinStartDateTime desc, u.LastName, u.FirstName asc	
		)  as r
	from AssignmentsForCourseViewCTE a
		join [User] u  with (nolock) on u.ID = a.UserID
		join TrainingProgram tp  with (nolock) on a.TrainingProgramID = tp.ID
		left join Course co  with (nolock) on tp.SessionCourseID = co.ID and co.ID = @sessionId
		left join Class cl  with (nolock) on cl.CourseID = co.ID and cl.CourseID = @sessionId
		left join Registration r  with (nolock) on r.RegistrantID = u.ID and r.ClassID = cl.ID
		left join ClassMinStartDateTime cmm  with (nolock) on cmm.ClassID = cl.ID
	where
		(@sessionId = 0 or @sessionId is null or r.RegistrationStatusID = 4)
		and (@sessionId = 0 or @sessionId is null or cmm.MinStartDateTime < DATEADD(day, 1, GETUTCDATE()))
		and (u.FirstName like '%' + @search + '%' or u.LastName like '%' + @search + '%' or cl.Name like '%' + @search + '%')
	group by
		u.[ID],
		u.FirstName,
		u.MiddleName, 
		u.LastName,
		cl.ID,
		cl.Name,
		cmm.MinStartDateTime
)	

select 
	t.AssignmentsAttempted,
	t.AssignmentsMarked,
	t.AssignmentsInCourse,
	t.AssignmentsCompleted,
	t.IsAssignmentsFailed,
	t.IsAssignmentsComplete,
	t.MaxSubmitDate,
	t.MaxGradedDate,
	t.ID,
	t.FirstName,
	t.LastName,
	t.MiddleName,
	t.ClassID,
	t.ClassName,
	t.ClassStartTime,
	t.r,
	(select count(*) from AssignmentTotals) as TotalRows
from
	AssignmentTotals t
where 
	t.r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
end

");
        }
    }
}
