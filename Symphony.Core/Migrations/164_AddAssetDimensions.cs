﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration164 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanAssets", "Width", DbType.Int32, 0, true);
            AddColumn("ArtisanAssets", "Height", DbType.Int32, 0, true);
        }
    }
}