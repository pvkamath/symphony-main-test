﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration390 : Migration
    {
        public override void Up()
        {
            TableSchema.Table trainingProgramBundle = CreateTable("TrainingProgramBundle");
            trainingProgramBundle.AddPrimaryKeyColumn("ID");
            trainingProgramBundle.AddColumn("Sku", DbType.String, 32, false);
            trainingProgramBundle.AddColumn("Name", DbType.String, 512, false);
            trainingProgramBundle.AddLongText("Description", true, "''");
            trainingProgramBundle.AddColumn("CustomerID", DbType.Int32, 0, false);
            trainingProgramBundle.AddColumn("CategoryID", DbType.Int32, 0, true, "0");
            trainingProgramBundle.AddColumn("Status", DbType.String, 32, false, "'In-Development'");
            trainingProgramBundle.AddColumn("ContentType", DbType.String, 50, false, "''");
            trainingProgramBundle.AddColumn("DeliveryMethod", DbType.String, 50, false, "''");
            trainingProgramBundle.AddColumn("Hours", DbType.Int32, 0, false, "0");
            trainingProgramBundle.AddColumn("IsDeleted", DbType.Boolean, 0, false, "0");
            AddSubSonicStateColumns(trainingProgramBundle);

            TableSchema.Table trainingProgramBundleMap = CreateTable("TrainingProgramBundleMap");
            trainingProgramBundleMap.AddPrimaryKeyColumn("ID");
            trainingProgramBundleMap.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            trainingProgramBundleMap.AddColumn("TrainingProgramBundleID", DbType.Int32, 0, false);
          
        }
    }
}