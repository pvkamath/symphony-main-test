﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration443 : Migration
    {

        public override void Up()
        {
            Execute(@"CREATE VIEW [dbo].[LicenseHierarchy] AS
                    select 
						l.ID,
						l.ParentID,
						l.Name,
						l.[Level],
						l.LevelIndentText,
						l.LevelText,
						l.RootLicenseID,
						l.JurisdictionID,
						l.ProfessionID,
						l.Category,
						l.[Description],
						l.CreatedOn,
						l.ModifiedOn,
						l.CreatedBy,
						l.ModifiedBy,
						l.RequiredUserFields,
						l.HasUserLicenseNumber,
						j.Name as Jurisdiction,
						p.Name as Profession
					from fGetLicenseHierarchyBranch(0) l
                    left join Jurisdiction j on j.ID = l.JurisdictionID
                    left join Profession p on p.ID = l.ProfessionID");
        }

    }
}