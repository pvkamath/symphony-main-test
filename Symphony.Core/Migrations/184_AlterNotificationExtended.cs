﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration184 : Migration
    {
        public override void Up()
        {
            Execute(@"
                alter view [dbo].[NotificationExtended] as
                select
	                n.ID, SenderID, RecipientID, [Subject], Body, IsRead, AttachmentID, [Priority], 
	                n.ModifiedBy, n.CreatedBy, n.ModifiedOn, n.CreatedOn, n.IsDeleted, TemplateID, GroupID, DeletedFromHistory,
	                sender.LastName + ', ' + sender.FirstName as Sender,
	                coalesce(recipient.LastName + ', ' + recipient.FirstName,'') as Recipient
                from
	                Notifications n
                left join
	                [User] sender
                on
	                sender.ID = SenderID
                join
	                [User] recipient
                on
	                recipient.ID = RecipientID
            ");
        }
    }
}