﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration759 : Migration
    {
        public override void Up()
        {
            AddColumn("Network", ReservedColumnName.CREATED_ON, DbType.DateTime, 0, false, "getdate()");
            AddColumn("Network", ReservedColumnName.MODIFIED_ON, DbType.DateTime, 0, false, "getdate()");
            AddColumn("Network", ReservedColumnName.CREATED_BY, DbType.String);
            AddColumn("Network", ReservedColumnName.MODIFIED_BY, DbType.String);
        }

    }
}