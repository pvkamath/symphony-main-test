﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration624 : Migration
    {
        public override void Up()
        {
            Execute(@"
    CREATE NONCLUSTERED INDEX [idx_LibraryGrant_Dates]
    ON [dbo].[LibraryGrant] ([StartDate],[EndDate])
    INCLUDE ([HierarchyNodeID],[HierarchyTypeID],[LibraryID])

    CREATE NONCLUSTERED INDEX [idx_LibraryGrant_NodeDates]
    ON [dbo].[LibraryGrant] ([HierarchyNodeID],[HierarchyTypeID],[StartDate],[EndDate])
    INCLUDE ([LibraryID])

    CREATE NONCLUSTERED INDEX [idx_LibraryItem_LibraryID]
    ON [dbo].[LibraryItem] ([LibraryID])
    INCLUDE ([ID])

    CREATE NONCLUSTERED INDEX [idx_LibraryItem_ItemTypeID]
    ON [dbo].[LibraryItem] ([LibraryID],[LibraryItemTypeID])
    INCLUDE ([ItemID])

    CREATE NONCLUSTERED INDEX [idx_OnlineCourse_CustomerID]
    ON [dbo].[OnlineCourse] ([CustomerID])

    CREATE NONCLUSTERED INDEX [idx_LibraryGrant_LibraryID]
    ON [dbo].[LibraryGrant] ([LibraryID])
");
        }
    }
}
