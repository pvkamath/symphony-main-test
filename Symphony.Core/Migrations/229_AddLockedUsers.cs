﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration229 : Migration
    {

        public override void Up()
        {
            Execute(@"
create view LockedUsers as
select
	[user].*, LockedCourseCount
from
	(
	select
		u.id, sum(case when ocr.testattemptcount >= (oc.retries + 1) then 1 else 0 end) as LockedCourseCount
	from
		[user] u
	join
		onlinecourserollup ocr
	on
		ocr.userid = u.id
	join
		onlinecourse oc
	on
		oc.id = ocr.courseid
    join
	    [customer]
    on
	    customer.id = oc.customerid
	join
		onlineTraining.dbo.ScormRegistration sr
	on
		sr.scorm_registration_id = ocr.originalregistrationid and sr.course_id = ocr.courseid
	where
		ocr.testattemptcount >= (coalesce(oc.retries, customer.OnlineCourseRetries) + 1) and ocr.trainingprogramid > 0
	group by
		u.id
	) as userids
join
	[user]
on
	[user].id = userids.id
");
        }

        public override void Down()
        {
            
        }
    }
}