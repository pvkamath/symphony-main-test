﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration621 : Migration
    {
        public override void Up()
        {
            AddColumn("ReportTemplate", "ReportSystem", DbType.Int32, 0, false, "0");
            AddColumn("ReportTemplate", "ReportSystemParameters", DbType.String, 2048, true);
        }
    }
}
