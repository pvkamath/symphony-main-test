﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration814 : Migration
    {
        public override void Up()
        {
            // Re-running this migration as there was an issue found with the initial backfill

            // Backfills the assignments the assignment attempts
            // loads the assignment attempts from a slightly modified
            // Assignments View to add some additional data.
            // 

            // All of the records in OnlineCourseAssignments are mistakenly
            // saved using local server time instead of utc time. 
            // We will convert them to UTC for the summary.

            TimeSpan utcDiff = DateTime.UtcNow.Subtract(DateTime.Now);
            double utcMinutesDiff = utcDiff.TotalMinutes;

            Execute(@"
INSERT INTO AssignmentSummaryAttempts
    (
      TrainingProgramID,
      OnlineCourseID,
      UserID,
      AssignmentSummaryID,
      AttemptID,
      QuestionsAnswered,
      QuestionsCorrect,
      QuestionsMarked,
      QuestionsFailed,
      TotalQuestions,
      GradedOn,
      Score,
      PassingScore,
      CreatedOn,
      ModifiedOn,
      SubmittedOn,
      IsMarked,
      IsComplete,
      IsFailed
    )
select
	a.TrainingProgramID,
	a.CourseID as OnlineCourseID,
	a.UserID,
	s.ID as AssignmentSummaryID,
	a.Attempt as AttemptID,
	a.NumQuestions as QuestionsAnswered,
	a.NumCorrect as QuestionsCorrect,
	a.MarkedResponses as QuestionsMarked,
	a.MarkedResponses - a.NumCorrect as QuestionsFailed,
    a.TotalQuestions,
	a.GradedDate as GradedOn,
	a.Score * 100 as Score,
    a.PassingScore,
	dateadd(minute, " + utcMinutesDiff + @", a.[Date]) as CreatedOn,
    dateadd(minute, " + utcMinutesDiff + @", a.[Date]) as ModifiedOn,
    dateadd(minute, " + utcMinutesDiff + @", a.[Date]) as SubmittedOn,
    a.IsMarked,
    case when a.IsMarked = 1 and a.Score * 100 >= a.PassingScore then 1 else 0 end as IsComplete,
    case when a.IsMarked = 1 and a.Score * 100 < a.PassingScore then 1 else 0 end as IsFailed
from (
	-- Coppied out from the assignments view
	-- adds count of marked responses
	select 
		s.ID as SectionID, 
		o.Name as CourseName, 
		a.CourseID, 
		a.TrainingProgramID, 
		a.Attempt, 
		s.Name, 
		max(a.[Date]) as [Date],
		max(a.GradedDate) as GradedDate, 
		a.UserID, 
		count(distinct a.PageId) as NumQuestions,
		pCount.totalQuestions as TotalQuestions,
		c.NumCorrect,
		cast(c.NumCorrect as float) / cast(pCount.totalQuestions as float) as Score,
		case
			when c.MarkedResponses = count(distinct a.PageId) then 1
			else 0
		end as IsMarked,
		sp.CourseID as ArtisanCourseID, -- Get the actual published course id for this page from ArtisanSectionPages
		c.MarkedResponses,
		coalesce(NULLIF(o.PassingScoreOverride, 0), NULLIF(cust.PassingScoreOverride,0), NULLIF(s.PassingScore, 0), ac.PassingScore) as PassingScore
	from OnlineCourseAssignments a with (nolock)
	join ArtisanPages p on p.ID = a.PageID
	join ArtisanSectionPages sp on p.ID = sp.PageID
	join ArtisanSections s on s.ID = sp.SectionID
	join OnlineCourse o on a.CourseID = o.ID
	join ArtisanCourses ac on ac.ID = o.PublishedArtisanCourseID
	join (
		select 
			artSP.SectionID, 
			count(distinct artSP.PageID) as totalQuestions
		from ArtisanSectionPages artSP with (nolock)
		join ArtisanPages artP on artP.ID = artSP.PageID and artP.QuestionType = 8
		group by artSP.SectionID
	) pCount on pCount.SectionID = s.ID
	join (
		select 
			ia.UserID, 
			ia.CourseID, 
			ia.TrainingProgramID, 
			ia.Attempt,
			isp.SectionID, 
			sum(cast(ia.IsCorrect AS INT)) as NumCorrect,
			sum(
				case
					when ia.ResponseStatus > 1 then 1
					when ia.ResponseStatus <= 1 then 0
				end
			) as MarkedResponses
		from OnlineCourseAssignments ia with (nolock)
		join ArtisanSectionPages isp on isp.PageID = ia.PageID
		group by isp.SectionID, ia.Attempt, ia.CourseID, ia.TrainingProgramID, ia.UserID
	) c on c.UserID = a.UserID and 
			c.CourseID = a.CourseID and 
			c.TrainingProgramID = a.TrainingProgramID and
			c.SectionID = s.ID and
			c.Attempt = a.Attempt
	join
		[User] u on u.ID = a.UserID
	join 
		Customer cust on cust.ID = u.CustomerID
	WHERE
		ISNULL(a.IsReset, 0) != 1
	group by s.ID, s.Name, a.Attempt, a.CourseID, o.Name, a.TrainingProgramID, a.UserID, c.NumCorrect, c.MarkedResponses, pCount.totalQuestions, sp.CourseID, s.PassingScore, o.PassingScoreOverride, cust.PassingScoreOverride, ac.PassingScore)  a
JOIN
	AssignmentSummary s
	ON
	s.ArtisanCourseID = a.ArtisanCourseID and
	s.ArtisanSectionID = a.SectionID");

        }
    }
}