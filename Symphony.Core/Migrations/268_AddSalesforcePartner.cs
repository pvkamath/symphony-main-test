﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration268 : Migration
    {
        public override void Up()
        {
            TableSchema.Table salesforcePartner = CreateTable("SalesforcePartner");
            salesforcePartner.AddPrimaryKeyColumn("ID");
            salesforcePartner.AddColumn("PartnerId", DbType.Int32, 0, false);
            salesforcePartner.AddColumn("PartnerCode", DbType.String, 128, false);
            salesforcePartner.AddColumn("DisplayName", DbType.String, 128, false);
            AddSubSonicStateColumns(salesforcePartner);
        }
    }
}