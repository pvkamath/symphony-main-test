﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration264 : Migration
    {
        public override void Up()
        {
            AddColumn("User", "Mobile", DbType.String, 16, true);
            AddColumn("User", "Fax", DbType.String, 16, true);
            AddColumn("User", "SalesforceContactId", DbType.String, 18, true);
            AddColumn("User", "SalesforceAccountName", DbType.String, 256, true);
            AddColumn("User", "SalesforceAccountId", DbType.String, 18, true);
            AddColumn("User", "JanrainUserUuid", DbType.String, 36, true);
            AddColumn("User", "SalesforceIsEnabled", DbType.Boolean, 0, true);
            Execute(@"ALTER TABLE [dbo].[User] ADD SalesforceAddress nvarchar(MAX)");

            AddColumn("Customer", "Phone", DbType.String, 16, true);
            AddColumn("Customer", "Fax", DbType.String, 16, true);
            AddColumn("Customer", "SalesforceContactId", DbType.String, 18, true);
            AddColumn("Customer", "SalesforceAccountId", DbType.String, 18, true);
            AddColumn("Customer", "SalesforceIsEnabled", DbType.Boolean, 0, true);
            AddColumn("Customer", "IsSalesforceCapable", DbType.Boolean, 0, false, "'0'");
            Execute(@"ALTER TABLE [dbo].[Customer] ADD SalesforceAddress nvarchar(MAX)");

            AddColumn("TrainingProgram", "PartnerID", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "IsSalesforce", DbType.Boolean, 0, false, "'0'");

            Execute(@"EXECUTE sp_refreshview N'CustomerDetails';");

        }
    }
}