﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration597 : Migration
    {
        public override void Up()
        {

            Execute(@"
ALTER VIEW [dbo].FastSearchableUsers as
select
	coalesce(substring((
		select
			(', ' + a.Name)
		from
			Audience a
		inner join
			[UserAudience] ua
		on
			a.ID = ua.AudienceID
		where
			ua.UserID = u.ID
		for xml path ('')
	),3, 1000),'') as Audiences,
	l.Name as Location, 
	j.Name as JobRole,
    c.EnforcePasswordDays,
    c.EnforcePasswordReset,
	u.ID,
	u.Username, 
	u.CustomerID, 
	u.SalesChannelID, 
	u.LocationID, 
	u.EmployeeNumber, 
	u.FirstName, 
	u.MiddleName, 
	u.LastName, 
	u.HireDate, 
	u.NewHireIndicator, 
	u.NewHireEndDate, 
	u.Email, 
	u.StatusID, 
	u.JobRoleID, 
	u.Notes,
	u.LoginCounter, 
	u.EncryptedSSN,
    u.LastEnforcedPasswordReset,
	    u.IsAccountExec, 
	    u.IsCustomerCare, 
	    u.ModifiedBy, 
	    u.CreatedBy, 
	    u.ModifiedOn, 
	    u.CreatedOn, 
	    u.TelephoneNumber, 
	    u.SupervisorID, 
	    u.[Password], 
	    u.IsExternal, 
	    u.SecondaryLocationID, 
	    u.[Username]+' ' +u.[FirstName] +' ' + u.[MiddleName] +' ' + u.[LastName] + ' ' + COALESCE(u.NMLSNumber, '') as Search,
	    u.ReportingSupervisorID,
	isnull(us.[Description],'') as Status,
	u.FirstName + ' ' + u.LastName as FullName,
	isnull(su.FirstName + ' ' + su.LastName,'') as Supervisor,
	isnull(ssu.FirstName + ' ' + ssu.LastName,'') as SecondarySupervisor,
	isnull(rsu.FirstName + ' ' + rsu.LastName,'') as ReportingSupervisor,
    u.TimeZone,
    u.IsCreatedBySalesforce,
    u.mobile,
    u.fax,
    u.janrainUserUuid,
    u.salesforceAccountId,
    u.salesforceAccountName,
    u.salesforceContactId,
    u.DateOfBirth,
    u.NmlsNumber,
	u.IsAuthor
from
	[user] u
left join
	[user] su
on
	u.SupervisorID = su.ID
left join
	[user] rsu
on
	u.ReportingSupervisorID = rsu.ID
left join
	[user] ssu
on
	u.SecondarySupervisorID = ssu.ID
left join
	Customer c
on
	c.ID = u.CustomerID
left join
	location l
on
	l.ID = u.LocationID
left join
	jobrole j
on
	j.ID = u.JobRoleID
left join
	UserStatus us
on
	us.ID = u.StatusID
;

");

        }
    }
}
