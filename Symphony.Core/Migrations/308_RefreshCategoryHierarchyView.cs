﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration308 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER VIEW [dbo].[CategoryHierarchy] AS
                    select * from [fGetCategoryHierarchyBranch](0);");
        }
    }
}