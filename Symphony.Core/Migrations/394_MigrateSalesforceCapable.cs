﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration394 : Migration
    {
        public override void Up()
        {
            Execute(@"update [Customer] set [Modules] = [Modules] + ',SALESFORCE' where [IsSalesforceCapable] = 1");
        }
    }
}