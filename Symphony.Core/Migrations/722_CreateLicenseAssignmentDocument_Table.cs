﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration722 : Migration
    {
        public override void Up()
        {
            TableSchema.Table licenseAssignmentDocmentsTable = CreateTableWithKey("LicenseAssignmentDocument", "ID");
            licenseAssignmentDocmentsTable.AddColumn("LicenseAssignmentID", DbType.Int32);
            licenseAssignmentDocmentsTable.AddColumn("FileName", DbType.String);
            licenseAssignmentDocmentsTable.AddColumn("FileType", DbType.String);
            licenseAssignmentDocmentsTable.AddColumn("Description", DbType.String);

            AddSubSonicStateColumns(licenseAssignmentDocmentsTable);

            Execute(@"
                ALTER TABLE dbo.LicenseAssignmentDocument
                ADD CONSTRAINT FK_LicenseAssignmentDocument FOREIGN KEY (LicenseAssignmentId) REFERENCES dbo.LicenseAssignments(ID)
            ");
        }
    }
}
