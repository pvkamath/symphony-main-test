﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration395 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER TABLE [dbo].[TrainingProgramBundle]  WITH NOCHECK ADD  CONSTRAINT [FK_TrainingProgramBundle_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([ID])
NOT FOR REPLICATION ");

            Execute(@"ALTER TABLE [dbo].[TrainingProgramBundle] NOCHECK CONSTRAINT [FK_TrainingProgramBundle_Customer]");
        }
    }
}