﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration348 : Migration
    {
        public override void Up()
        {
            TableSchema.Table affidavit = CreateTableWithKey("AffidavitFinalExam", "ID");
            affidavit.AddColumn("Name", DbType.String, 512, false);
            affidavit.AddLongText("AffidavitJSON", false, "'{}'");
            AddSubSonicStateColumns(affidavit);
        }
    }
}