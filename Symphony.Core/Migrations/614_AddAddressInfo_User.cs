﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration614 : Migration
    {
        public override void Up()
        {
            Execute(@"

ALTER TABLE dbo.[User]
ADD AddressLine1 VARCHAR(100) NULL

ALTER TABLE dbo.[User]
ADD AddressLine2 VARCHAR(100) NULL

ALTER TABLE dbo.[User]
ADD City VARCHAR(100) NULL

ALTER TABLE dbo.[User]
ADD [State] VARCHAR(50) NULL

ALTER TABLE dbo.[User]
ADD ZipCode VARCHAR(10) NULL

ALTER TABLE dbo.[User]
ADD AlternateEmail1 VARCHAR(50) NULL

ALTER TABLE dbo.[User]
ADD AlternateEmail2 VARCHAR(50) NULL

ALTER TABLE dbo.[User]
ADD HomePhone VARCHAR(30) NULL

ALTER TABLE dbo.[User]
ADD WorkPhone VARCHAR(30) NULL

ALTER TABLE dbo.[User]
ADD CellPhone VARCHAR(30) NULL


");
        }
    }
}
