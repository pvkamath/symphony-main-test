﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration736 : Migration
    {
        public override void Up()
        {
            TableSchema.Table affidavit = CreateTableWithKey("ProctorFormResponse", "ID");
            affidavit.AddColumn("ProctorFormID", DbType.Int32, 0, false);
            affidavit.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            affidavit.AddColumn("CourseID", DbType.Int32, 0, false);
            affidavit.AddColumn("UserID", DbType.Int32, 0, false);
            affidavit.AddLongText("ResponseJSON", false, "'{}'");
            AddSubSonicStateColumns(affidavit);

            Execute("ALTER TABLE ProctorFormResponse"
                + " ADD CONSTRAINT fk_ProctorForm_ID_ProctorFormResponse_ProctorFormID FOREIGN KEY(ProctorFormID)"
                    + " REFERENCES ProctorForm(ID)"
                );

            Execute("ALTER TABLE ProctorFormResponse"
                + " ADD CONSTRAINT fk_TrainingProgram_ID_ProctorFormResponse_TrainingProgramID FOREIGN KEY(TrainingProgramID)"
                    + " REFERENCES TrainingProgram(ID)"
                );

            Execute("ALTER TABLE ProctorFormResponse"
                + " ADD CONSTRAINT fk_OnlineCourse_ID_ProctorFormResponse_CourseID FOREIGN KEY(CourseID)"
                    + " REFERENCES OnlineCourse(ID)"
                );

            Execute("ALTER TABLE ProctorFormResponse"
                + " ADD CONSTRAINT fk_User_ID_ProctorFormResponse_UserID FOREIGN KEY(UserID)"
                    + " REFERENCES [User](ID)"
                );
        }
    }
}