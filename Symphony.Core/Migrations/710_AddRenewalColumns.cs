﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration710 : Migration
    {
        public override void Up()
        {
            AddColumn("License", "RenewalLeadTimeInDays", DbType.Int32);
            AddColumn("licenseAssignments", "RenewalSubmittedDate", DbType.DateTime);
        }
    }
}
