﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration077 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("set identity_insert ArtisanThemes on");
            sb.AppendLine("insert ArtisanThemes (id, name, description, cssPath, createdon, modifiedon, createdby, modifiedby) values (1,'Default','Default Theme', '/skins/artisan_content/default/default.css', '1/1/1','1/1/1','system','system')");
            sb.AppendLine("insert ArtisanThemes (id, name, description, cssPath, createdon, modifiedon, createdby, modifiedby) values (2,'Simple Blue','Simple Blue Theme', '/skins/artisan_content/simple_blue/simple_blue.css', '1/1/1','1/1/1','system','system')");
            sb.AppendLine("set identity_insert ArtisanThemes off");
            Execute(sb.ToString());
        }
    }
}