﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration419 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgram", "SessionCourseID", DbType.Int32, 0, true);
        }
    }
}