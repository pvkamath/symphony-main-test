﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration655 : Migration
    {

        public override void Up()
        {
            Execute(@"
    CREATE NONCLUSTERED INDEX [idx_TrainingProgramRollup_TrainingProgramID]
    ON [dbo].[TrainingProgramRollup] ([TrainingProgramID])


    CREATE NONCLUSTERED INDEX [idx_TrainingProgramRollup_UserID]
    ON [dbo].[TrainingProgramRollup] ([UserID])


    CREATE NONCLUSTERED INDEX [idx_TrainingProgramRollup_TrainingProgramIDUserID]
    ON [dbo].[TrainingProgramRollup] ([TrainingProgramID],[UserID])

");
        }

    }
}
