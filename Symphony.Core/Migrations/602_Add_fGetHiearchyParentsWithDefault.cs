﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration602 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE FUNCTION [dbo].[fGetHierarchyParentsWithDefault]
(   
    --the node that'll be considered the root node of the branch
    @ID int,
    @TypeID int
)
RETURNS @tbl TABLE (ID int not null, TypeID int not null)
AS
begin
	declare @parents TABLE (ID int not null, TypeID int not null);
	declare @count int;
	
	insert @parents
	select ID, TypeID from fGetHierarchyParents(@ID, @TypeID);
	
	select @count = count(ID) from @parents;
	
	if (@count > 0) 
		begin
			insert @tbl
			select ID, TypeID from @parents
		end
	else
		begin
			insert @tbl
			select 0 as ID, 0 as TypeID
		end

	return
end	

");
        }
    }
}
