﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration328: Migration
    {
        public override void Up()
        {
            Execute(@"CREATE PROCEDURE [dbo].[CreateQuizExam](@CID int, @PSCID int, @QSETID int, @QNAME nvarchar(10), @PASS int, @STYPE int, @CHAPTER nvarchar(255), @CODE nvarchar(255))
as
begin

	set rowcount 0
DECLARE @SID int
DECLARE @SECTID int
DECLARE @PTSECTID int
DECLARE @PID int
DECLARE @QNAME2 varchar(40)
DECLARE @MAXQUESTIONS int
DECLARE @SEQ int
DECLARE @PSSECTIONORDER int 
DECLARE @ArtisanType int

print @QNAME
set @QNAME2 = CASE WHEN @STYPE < 3 THEN @CHAPTER ELSE 'Final Exam' END
set @MAXQUESTIONS = 20
SET @SID = 0
SET @PSSECTIONORDER = 0 -- Chapter #
 
-- Get Max Sequence for Course
select @SEQ =  CASE WHEN MAX(P.SORT) IS NULL then 1 else MAX(P.SORT) + 1 end from  Symphony.dbo.ArtisanSections as S
inner join Symphony.dbo.ArtisanSectionPages as P on S.ID = P.SectionID
where S.CourseID = @CID
 
 
-- Load Questions into Temp
                select Q.*, FG = 0  into #PSQ from Analyst.dbo.ps_question_sets as QS
                 inner join Analyst.dbo.ps_questions as Q on QS.Q_ID = Q.Q_ID
                where QS.QSET_ID in (CONVERT(CHAR(20),@QSETID) )   
 
 
DECLARE @R_CT int
DECLARE @IN_Q_ID int
DECLARE @IN_Q_TEXT nvarchar(150)
DECLARE @IN_A0 nvarchar(150)
DECLARE @IN_A1 nvarchar(150)
DECLARE @IN_A2 nvarchar(150)
DECLARE @IN_A3 nvarchar(150)
DECLARE @IN_CORRECT nvarchar(150)
DECLARE @IN_EXPLAIN nvarchar(max)
 
-- Finals do not need the Section/LO
set @ArtisanType = 6 -- assume it's a post test
if @STYPE < 3
BEGIN
				set @ArtisanType = 2 -- in this case it's a quiz so update to 2
                insert into Symphony.dbo.ArtisanSections ( [Name], SectiONID, CreatedOn, ModifiedOn, CreatedBy, MOdifiedBy,
                CourseID, Sort, SectionType, Objectives, TestType, PassingScore, TestOut, Description, CreatedByUserId, CreatedByActualUserId, 
                ModifiedByActualUserId, ExternalID )
                values ( @QNAME, 0, getdate(), getdate(), 'system', 'system', 
                  @CID, @SEQ, 1, '', 0, 0, 0, '', 0,0,
                0, @CODE) 
                
                SET @SID = SCOPE_IDENTITY()
               
                -- update the keywords so show the PS course and section rank/order
                update Symphony.dbo.ArtisanCourses set Keywords = Keywords + ',PSID' + RTRIM(CONVERT(VARCHAR(10),@PSCID)) + '-' + RTRIM(CONVERT(VARCHAR(10),@PSSECTIONORDER)) where ID = @CID and Keywords not like '%FINAL%'
  END             
    ELSE
        SET @SID = 0
        
        -- update the keywords so show the PS course and section rank/order
        update Symphony.dbo.ArtisanCourses set Keywords = Keywords + ',PSID' + RTRIM(CONVERT(VARCHAR(10),@PSCID)) + '-FINAL' where ID = @CID and Keywords not like '%FINAL%'
 
                insert into Symphony.dbo.ArtisanSections ( [Name], SectiONID, CreatedOn, ModifiedOn, CreatedBy, MOdifiedBy,
                CourseID, Sort, SectionType, Objectives, TestType, PassingScore, TestOut, Description, CreatedByUserId, CreatedByActualUserId, 
                ModifiedByActualUserId, IsQuiz, MaxQuestions, MarkingMethod, ExternalID )
                values ( @QNAME2, @SID, getdate(), getdate(), 'system', 'system', 
                  @CID, @SEQ + 1, @ArtisanType, '', 3, @PASS, 0, '', 0,0,
                0 , 1, @MAXQUESTIONS, 1, @CODE) 
                
                SET @SECTID = SCOPE_IDENTITY()
                                                                set @SEQ = @SEQ + 1
               
                --SET @SECTID = 46082
               
                                                               
SET ROWCOUNT 1
SET @R_CT = 1
WHILE(@R_CT > 0 )
BEGIN
                select @IN_Q_ID = Q_ID, @IN_Q_TEXT = Q_QUESTION,
                                @IN_A0  = Q_ANS0,
                                @IN_A1 = Q_ANS1,
                                @IN_A2 = Q_ANS2,
                                @IN_A3 = Q_ANS3,
                                @IN_CORRECT = Q_CORRECT_ANS,
                                @IN_EXPLAIN = Q_EXPLAIN from #PSQ where FG = 0                     
                 
                 SET @R_CT = @@ROWCOUNT
                if( @R_CT = 0 )
                BEGIN
                                BREAK
                END
                update #PSQ set FG = 1 where Q_ID = @IN_Q_ID
               
    set @SEQ = @SEQ + 1
 
                insert into Symphony.dbo.ArtisanPages ( [Name], HTML, PageType, QuestionType, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy, CourseID, TemplateID, CorrectResponse,
       IncorrectResponse, CalculateScore, IsGradable, ReferencePageID, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, ExternalID )
        values ( CONVERT(CHAR(30),@IN_Q_ID), @IN_Q_TEXT, 2, 1, getdate(), getdate(), 'System', 'System',@CID, 0, 'Correct. ' + @IN_EXPLAIN,
      'Incorrect. ' + @IN_EXPLAIN, 0, 0, 0, 0, 0, 0,0, @CODE )
      
       SET @PID = SCOPE_IDENTITY();
                       
      insert into Symphony.dbo.ArtisanSectionPages ( SectioNID, PageID, Sort, CreatedByUserId, ModifiedByUserId,
         CreatedByActualUserId, ModifiedByActualUserId, ExternalID )
       values ( @SECTID, @PID, @SEQ , 0,0,0,0, @CODE )
                     
         insert into Symphony.dbo.ArtisanAnswers ( PageID, [Text], Sort, IsCorrect, CreatedOn, CreatedBy,
         ModifiedOn, ModifiedBy, CreatedByUserID, ModifiedByUserID, CreatedByActualUserId, ModifiedByActualUserId, ExternalID )
          values ( @PID, @IN_A0, 0, case when @IN_CORRECT = '0' then 1 else 0 end, getdate(), '', 
         getdate(),'', 0, 0, 0, 0, @CODE )
         
         insert into Symphony.dbo.ArtisanAnswers ( PageID, [Text], Sort, IsCorrect, CreatedOn, CreatedBy,
         ModifiedOn, ModifiedBy, CreatedByUserID, ModifiedByUserID, CreatedByActualUserId, ModifiedByActualUserId, ExternalID )
          values ( @PID, @IN_A1, 1, case when @IN_CORRECT = '1' then 1 else 0 end, getdate(), '', 
         getdate(),'', 0, 0, 0, 0, @CODE )
        
         insert into Symphony.dbo.ArtisanAnswers ( PageID, [Text], Sort, IsCorrect, CreatedOn, CreatedBy,
         ModifiedOn, ModifiedBy, CreatedByUserID, ModifiedByUserID, CreatedByActualUserId, ModifiedByActualUserId, ExternalID )
          values ( @PID, @IN_A2, 2, case when @IN_CORRECT = '2' then 1 else 0 end, getdate(), '', 
         getdate(),'', 0, 0, 0, 0, @CODE ) 
         
          insert into Symphony.dbo.ArtisanAnswers ( PageID, [Text], Sort, IsCorrect, CreatedOn, CreatedBy,
         ModifiedOn, ModifiedBy, CreatedByUserID, ModifiedByUserID, CreatedByActualUserId, ModifiedByActualUserId, ExternalID )
          values ( @PID, @IN_A3, 3, case when @IN_CORRECT = '3' then 1 else 0 end, getdate(), '', 
         getdate(),'', 0, 0, 0, 0, @CODE ) 
                                  
               
                 
END
SET ROWCOUNT 0
--select * from #PSQ
 
drop table #PSQ
END;");


        }
    }
}