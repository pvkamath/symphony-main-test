﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration393 : Migration
    {
        public override void Up()
        {
            AddColumn("SalesforceEntitlements", "BundleID", DbType.Int32, 0, true);
            AddColumn("SalesforceEntitlements", "BundleSku", DbType.String, 32, true);
        }
    }
}