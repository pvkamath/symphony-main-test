﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration258 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER TABLE MessageBoardPost
                    ALTER COLUMN Content nvarchar(MAX)");  
        }
    }
}