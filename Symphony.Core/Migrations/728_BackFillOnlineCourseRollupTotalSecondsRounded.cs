﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration728 : Migration
    {

        public override void Up()
        {
            Execute(@"
                update OnlineCourseRollup 
                set 
                    TotalSecondsRounded = ceiling(cast(TotalSeconds as float) / cast(60 as float)) * 60,
                    TotalSecondsScorm = TotalSeconds
            ");
        }

    }
}
