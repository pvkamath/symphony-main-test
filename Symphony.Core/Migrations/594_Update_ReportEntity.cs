﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration594 : Migration
    {
        public override void Up()
        {
            Execute("update ReportEntity set IsDeprecated = 1 where Name = 'Course Due Date'");
            Execute("update ReportEntity set IsDeprecated = 1 where Name = 'Date Range'");
            Execute("update ReportEntity set IsDeprecated = 1 where Name = 'Hire Date'");
            Execute("update ReportEntity set IsDeprecated = 1 where Name = 'Pass Date'");
            Execute("update ReportEntity set IsDeprecated = 1 where Name = 'Test Date'");
            Execute("update ReportEntity set IsDeprecated = 1 where Name = 'Training Program Date'");
            Execute("update ReportEntity set IsDeprecated = 1 where Name = 'View Date'");
            Execute("update ReportEntity set IsDeprecated = 1 where Name = 'Course Required Type'");
            Execute("update ReportEntity set IsDeprecated = 1 where Name = 'Class Date'");
            Execute("update ReportEntity set IsDeprecated = 1 where Name = 'Class Location'");

            Execute(@"insert into ReportEntity (Name, Xtype, Config, DefaultOrder, Description) 
                    values (
                        'Search',
                        'reporting.search',
                        '',
                        0,
                        'Generic search field'
                    )");

            Execute(@"insert into ReportEntity (Name, Xtype, Config, DefaultOrder, Description) 
                    values (
                        'Date',
                        'reporting.datepicker',
                        '',
                        0,
                        'Generic date range field'
                    )");
        }
    }
}
