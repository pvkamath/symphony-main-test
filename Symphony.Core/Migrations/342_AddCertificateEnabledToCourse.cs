﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration342 : Migration
    {
        public override void Up()
        {
            AddColumn("Course", "CertificateEnabled", DbType.Boolean, 0, false, "1");
            AddColumn("OnlineCourse", "CertificateEnabled", DbType.Boolean, 0, false, "1");
        }
    }
}