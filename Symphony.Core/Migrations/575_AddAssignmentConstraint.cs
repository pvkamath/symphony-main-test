﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration575 : Migration
    {
        public override void Up()
        {
            // Add assignment unique key
            Execute(@"
alter table OnlineCourseAssignments
add constraint uc_Response unique (UserID, TrainingProgramID, CourseID, PageID, Attempt)
");
        }
    }
}
