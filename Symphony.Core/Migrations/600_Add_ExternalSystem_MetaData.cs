﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration600 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE TABLE dbo.ExternalSystemMetaDataRecords
(
	Id INT IDENTITY(1, 1) NOT NULL,
	ExternalSystemCode VARCHAR(50),
    ExternalSourceCourseId INT NOT NULL,
	Name VARCHAR(1000),
	Value VARCHAR(8000)
)


ALTER TABLE dbo.ExternalSystemMetaDataRecords
ADD CONSTRAINT PK_ExternalSystemMetaDataRecords__Id PRIMARY KEY (Id)

");
        }
    }
}
