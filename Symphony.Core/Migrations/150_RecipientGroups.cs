﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration150 : Migration
    {
        public override void Up()
        {
            Execute(@"insert RecipientGroups (Selector, DisplayName, ValidForType) values ('GetAssignedUsersSupervisors()','Assigned Users Classroom Supervisors','TrainingProgram')");
            Execute(@"insert RecipientGroups (Selector, DisplayName, ValidForType) values ('GetInviteeSupervisor()','Invitee Classroom Supervisor','GTMInvitation')");
            Execute(@"insert RecipientGroups (Selector, DisplayName, ValidForType) values ('GetAllRegistrantsSupervisors()','All Registrants Classroom Supervisors','ClassX')");

            Execute(@"update RecipientGroups set Selector='UserID' where Selector='ID' and ValidForType='GTMInvitation'");

            Execute(@"insert RecipientGroups (Selector, DisplayName, ValidForType) values ('GetAssignedUsersReportingSupervisors()','Assigned Users Reporting Supervisors','TrainingProgram')");
            Execute(@"insert RecipientGroups (Selector, DisplayName, ValidForType) values ('GetInviteeReportingSupervisor()','Invitee Reporting Supervisor','GTMInvitation')");
            Execute(@"insert RecipientGroups (Selector, DisplayName, ValidForType) values ('GetAllRegistrantsReportingSupervisors()','All Registrants Reporting Supervisors','ClassX')");
        }
    }
}