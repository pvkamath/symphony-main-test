﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration090 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "Objectives", DbType.String, 1024, false, "''");
        }
    }
}