﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration074 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "Objectives", DbType.String, 9000, false, "''");
        }
    }
}