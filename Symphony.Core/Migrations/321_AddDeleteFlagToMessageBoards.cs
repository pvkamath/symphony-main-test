﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration321: Migration
    {
        public override void Up()
        {
            AddColumn("MessageBoardPost", "IsDeleted", DbType.Boolean, 0, false, "0");
            AddColumn("MessageBoardTopic", "IsDeleted", DbType.Boolean, 0, false, "0");
        }
    }
}