﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration255 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "TimeoutMethod", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanCourses", "MarkingMethod", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanCourses", "InactivityTimeout", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanCourses", "MinimumPageTime", DbType.Int32, 0, false, "0");
        }
    }
}