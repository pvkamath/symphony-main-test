﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration696 : Migration
    {
        public override void Up()
        {
            TableSchema.Table table = CreateTableWithKey("Themes", "ID");
            table.AddColumn("PrimaryColor", DbType.String, 10, false, "''");
            table.AddColumn("SecondaryColor", DbType.String, 10, false, "''");
            table.AddColumn("TertiaryColor", DbType.String, 10, false, "''");
            table.AddLongText("PrimaryImage", false, "''");
            table.AddLongText("SecondaryImage", false, "''");
            table.AddColumn("Name", DbType.String, 256, false, "''");
            table.AddColumn("CodeName", DbType.String, 64, false);
            AddSubSonicStateColumns(table);

            AddColumn("Customer", "ThemeID", DbType.Int32, 0, true); 
        }
    }
}
