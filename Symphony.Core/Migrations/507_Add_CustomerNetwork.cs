﻿using SubSonic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Symphony.Core.Migrations
{
    class Migration507 : Migration
    {

        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.Network
(
	Id INT IDENTITY(1, 1) NOT NULL,
    Name VARCHAR(50) NOT NULL,
	CodeName VARCHAR(50)
)
");
            Execute(@"
ALTER TABLE dbo.Network
ADD CONSTRAINT PK_Network__Id PRIMARY KEY (Id)
");

            Execute(@"
CREATE TABLE dbo.CustomerNetwork
(
	CustomerNetworkId INT IDENTITY(1,1) NOT NULL,
	NetworkId INT NOT NULL,
	CustomerId INT NOT NULL
)
");

            Execute(@"
ALTER TABLE dbo.CustomerNetwork
ADD CONSTRAINT FK_CustomerNetwork_Network__Id FOREIGN KEY (NetworkId) REFERENCES dbo.Network(Id)
");

            Execute(@"
ALTER TABLE dbo.CustomerNetwork
ADD CONSTRAINT PK_CustomerNetwork__ID PRIMARY KEY (CustomerNetworkId)
");
            Execute(@"
ALTER TABLE dbo.CustomerNetwork
ADD CONSTRAINT FK_CustomerNetwork_Customer__ID FOREIGN KEY (CustomerId) REFERENCES dbo.Customer(ID)
");

            Execute(@"
CREATE TABLE dbo.CustomerNetworkDetail
(
	DetaildId INT IDENTITY(1,1) NOT NULL,
	NetworkId INT NOT NULL,
	SourceCustomerId INT NOT NULL,
	DestinationCustomerId INT NOT NULL,
	AllowTrainingProgramSharing BIT NULL,
	AllowReporting BIT NULL
)
");
            Execute(@"
ALTER TABLE dbo.CustomerNetworkDetail
ADD CONSTRAINT PK_CustomerNetworkDetail__DetailId PRIMARY KEY (DetaildId)
");
            Execute(@"
ALTER TABLE dbo.CustomerNetworkDetail
ADD CONSTRAINT FK_CustomerNetworkDetail_Network__Id FOREIGN KEY (NetworkId) REFERENCES dbo.Network(Id)
");
            Execute(@"
ALTER TABLE dbo.CustomerNetworkDetail
ADD CONSTRAINT FK_CustomerNetworkDetail_Customer__SourceCustomerId FOREIGN KEY (SourceCustomerId) REFERENCES dbo.Customer(ID)
");
            Execute(@"
ALTER TABLE dbo.CustomerNetworkDetail
ADD CONSTRAINT FK_CustomerNetworkDetail_Customer__DestinationCustomerId FOREIGN KEY (DestinationCustomerId) REFERENCES dbo.Customer(ID)
");

        }

    }
}
