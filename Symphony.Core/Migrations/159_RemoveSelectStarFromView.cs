﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration159 : Migration
    {
        public override void Up()
        {
            Execute(@"
alter view [dbo].[SearchableUsers] as
select
	isnull(EmployeeNumber,'') + ' ' +isnull(JobRole,'') + ' ' + isnull(Location,'') + ' ' + isnull(Username,'') + ' ' + isnull(Audiences,'') + ' ' + isnull(FirstName,'') + ' ' + isnull(LastName,'') + ' ' + isnull(Supervisor,'') + isnull(Status,'') as SearchText,
    Audiences, Location, JobRole, ID, Username, CustomerID, SalesChannelID, LocationID, EmployeeNumber, FirstName, MiddleName, LastName, HireDate, NewHireIndicator, Email, StatusID, JobRoleID, Notes, LoginCounter, IsAccountExec, IsCustomerCare, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, TelephoneNumber, SupervisorID, Password, IsExternal, SecondaryLocationID, Search, ApplicationName, Status, Supervisor, HasSupervisorRole, CanManageCustomers, CanOwnTrainingPrograms
from
(
select
	replace((
		select
			a.Name as [data()]
		from
			Audience a
		inner join
			[UserAudience] ua
		on
			a.ID = ua.AudienceID
		where
			ua.UserID = u.ID
		for xml path ('')
	),' ',', ') as Audiences,
	l.Name as Location, 
	j.Name as JobRole,
	u.*,
	app.ApplicationName,
	isnull(us.Description,'') as Status,
	isnull(su.FirstName + ' ' + su.LastName,'') as Supervisor,
	cast(
		(select 
			case when count(1) > 0 then 1 else 0 end
		from
			aspnet_UsersInRoles aspUIR
		join
			aspnet_Roles aspR
		on
			aspUIR.RoleID = aspR.RoleID
		where
			UserID = au.UserID and aspR.LoweredRoleName = 'classroom - supervisor'
		) 
	as bit) as HasSupervisorRole,
	cast(
		(select 
			case when count(1) > 0 then 1 else 0 end
		from
			aspnet_UsersInRoles aspUIR
		join
			aspnet_Roles aspR
		on
			aspUIR.RoleID = aspR.RoleID
		where
			UserID = au.UserID 
		and 
			aspR.LoweredRoleName in ('customer - administrator','customer - limited administrator','customer - manager','customer - limited manager')
		) 
	as bit) as CanManageCustomers,
	cast(
		(select 
			case when count(1) > 0 then 1 else 0 end
		from
			aspnet_UsersInRoles aspUIR
		join
			aspnet_Roles aspR
		on
			aspUIR.RoleID = aspR.RoleID
		where
			UserID = au.UserID 
		and 
			aspR.LoweredRoleName in ('courseassignment - training administrator','courseassignment - training manager')
		) 
	as bit) as CanOwnTrainingPrograms

from
	[user] u
left join
	[user] su
on
	u.SupervisorID = su.ID
left join
	Customer c
on
	c.ID = u.CustomerID
left join
	location l
on
	l.ID = u.LocationID
left join
	jobrole j
on
	j.ID = u.JobRoleID
left join
	UserStatus us
on
	us.ID = u.StatusID
left join
	aspnet_Applications app
on
	app.ApplicationName = c.SubDomain
left join
	aspnet_Users au
on
	au.LoweredUserName = lower(u.Username)
and
	au.ApplicationID = app.ApplicationID
) as Users

");
        }
    }
}