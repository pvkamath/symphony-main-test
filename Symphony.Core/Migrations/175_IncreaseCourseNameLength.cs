﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration175 : Migration
    {
        public override void Up()
        {
            AlterColumn("Course", "Name", DbType.String, 128);
            AlterColumn("Class", "Name", DbType.String, 128);
        }
    }
}