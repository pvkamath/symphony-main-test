﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration122 : Migration
    {
        public override void Up()
        {
            Execute("insert ArtisanAssetTypes (Name, Description, Extensions, Template, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy, PreviewTemplate) values ('Asset Group', 'A group of assets; for now, html/htm is supported', 'zip','',getutcdate(), getutcdate(), 'admin','admin','')");
        }
    }
}