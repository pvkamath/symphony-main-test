﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration049 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(1082);
            sb.AppendFormat(@"/* ExecuteTransaction() */ create view [dbo].[PendingRegistrations] as{0}", Environment.NewLine);
            sb.AppendFormat(@"select{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}Class.ID as ClassId,{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}Registration.ID as RegistrationId, {1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}class.CustomerID as CustomerId,{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}Class.Name,{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}ClassDateMinMax.MinStartDateTime as StartDate,{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}case {1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}when Class.IsVirtual = 1 then 'Virtual'{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}else{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Room.Name + ', ' + Venue.Name + ', ' + Venue.City + ', ' + States.Name end{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}as Location,{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[User].FirstName,{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[User].MiddleName,{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}[User].LastName,{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"{0}Registration.RegistrationStatusID as status{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"from{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}Registration{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"inner join{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}Class on Registration.ClassID = class.ID{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"inner join{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}[User] on [User].ID = Registration.RegistrantID{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"inner join{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}ClassDateMinMax on class.ID = ClassDateMinMax.ClassID{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"inner join{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}Room on class.RoomID = Room.ID{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"inner join{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}Venue on room.VenueID = venue.ID{0}{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"inner join{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}States on Venue.StateID = States.ID{1}","\t", Environment.NewLine);
            sb.AppendFormat(@"where {0}", Environment.NewLine);
            sb.AppendFormat(@"{0}Registration.RegistrationStatusID <> 4{1}","\t", Environment.NewLine);
            Execute(sb.ToString());
        }
    }
}