﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration377 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "InternalID", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanSections", "InternalParentID", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanPages", "InternalID", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanPages", "InternalSectionID", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanSectionPages", "InternalSectionID", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanSectionPages", "InternalPageID", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanSectionPages", "CourseID", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanAnswers", "InternalPageID", DbType.Int32, 0, false, "0");
            AddColumn("ArtisanAnswers", "CourseID", DbType.Int32, 0, false, "0");
        }
    }
}