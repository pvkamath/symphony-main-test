﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration332: Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "SsoType", DbType.Int32, 0, true);
        }
    }
}