﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration289 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgram", "IsSurveyMandatory", DbType.Boolean, 0, false, "0");
            AddColumn("Course", "IsSurveyMandatory", DbType.Boolean, 0, false, "0");
        }
    }
}