using SubSonic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Symphony.Core.Migrations
{
    public class Migration510 : Migration
    {
        public override void Up()
		{
            Execute(@"
CREATE VIEW dbo.CustomerNetworkDetailsInfo
AS

SELECT
	cnd.DetaildId,
	cnd.NetworkId,
	cnd.SourceCustomerId,
	cs.Name AS [SourceCustomerName],
	cnd.DestinationCustomerId,
	cd.Name AS [DestinationCustomerName],
	cnd.AllowTrainingProgramSharing,
	cnd.AllowReporting
FROM
	dbo.CustomerNetworkDetail cnd WITH (NOLOCK)
	INNER JOIN dbo.Customer cs WITH (NOLOCK) ON cs.ID = cnd.SourceCustomerId
	INNER JOIN dbo.Customer cd WITH (NOLOCK) ON cd.ID = cnd.DestinationCustomerId

");
			
		}
	}
}