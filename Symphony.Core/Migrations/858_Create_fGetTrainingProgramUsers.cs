﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration858 : Migration
    {
        public override void Up()
        {
            Execute(@"
                CREATE FUNCTION [dbo].[fGetTrainingProgramUsers] 
                (	
	                -- Add the parameters for the function here
	                @trainingProgramID int = null,
	                @isNewHire int = null
                )
                RETURNS TABLE
                AS
                RETURN
                (
            		SELECT     
						program.CustomerID AS CustomerID, 
						program.ID AS TrainingProgramID, 
						program.[Name] AS TrainingProgramName, 
        
						--TrainingProgramStartDate
						case when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0)
							then program.StartDate 
							else cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, '1/1/1900') as datetime)
						end as TrainingProgramStartDate,
						--TrainingProgramEndDate	
						case when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0) 
							then program.EndDate 
							else dateadd(
								d,customer.NewHireDuration, 
								cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, '1/1/1900') as datetime)
							)
						end as TrainingProgramEndDate,
						--TrainingProgramDueDate
						case when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0) 
							then program.DueDate
							else dateadd(
								d,customer.NewHireDuration, 
								cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, '1/1/1900') as datetime)
							)
						end as TrainingProgramDueDate, 
		
						program.Description AS TrainingProgramDescription, 
						program.MinimumElectives AS MinimumElectives, 
						program.EnforceRequiredOrder AS EnforceRequiredOrder, 
						program.IsNewHire AS IsNewHire,
						FullList.ID AS SourceNodeID, 
						link.HierarchyTypeID, 
						COALESCE (locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) AS IsUserNewHire,
						COALESCE (locationUsers.FirstName, jobroleUsers.FirstName, audienceUsers.FirstName, users.FirstName, NULL) AS FirstName, 
						COALESCE (locationUsers.LastName, jobroleUsers.LastName, audienceUsers.LastName, users.LastName, NULL) AS LastName, 
						COALESCE (locationUsers.ID, jobroleUsers.ID, audienceUsers.ID, users.ID, NULL) AS UserID,
						program.ParentTrainingProgramID
					FROM
						TrainingProgram program with (nolock)
					JOIN
						dbo.HierarchyToTrainingProgramLink link with (nolock)
					ON 
						link.TrainingProgramID = program.ID 
					CROSS apply 
						dbo.fGetHierarchyBranch(link.HierarchyNodeID, link.HierarchyTypeID) AS FullList 
					LEFT JOIN
						  dbo.[User] locationUsers with (nolock) ON FullList.ID = locationUsers.LocationID AND HierarchyTypeID = 1
					LEFT JOIN
						  dbo.[User] jobroleUsers with (nolock) ON FullList.ID = jobroleUsers.JobRoleID AND HierarchyTypeID = 2 
					LEFT JOIN
						  dbo.UserAudience ua with (nolock) ON FullList.ID = ua.AudienceID AND HierarchyTypeID = 3 
					LEFT JOIN
						  dbo.[User] audienceUsers with (nolock) ON ua.UserID = audienceUsers.ID 
					LEFT JOIN
						  dbo.[User] users with (nolock) ON users.ID = FullList.ID AND link.HierarchyTypeID = 4
					left join
						dbo.Customer customer with (nolock)
					on
						program.CustomerID = customer.ID
					WHERE      
						COALESCE (locationUsers.ID, jobroleUsers.ID, audienceUsers.ID, users.ID, NULL) IS NOT NULL AND program.IsLive = 1 
						and (
							(@trainingProgramID > 0 and (@trainingProgramID = program.ID or @trainingProgramID = program.parentTrainingProgramID))
						)
						and (
							@isNewHire is null or
							@isNewHire = COALESCE (locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0)
						)
				)
");
        }
    }
}
