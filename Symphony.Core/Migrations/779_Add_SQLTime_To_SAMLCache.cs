﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration779 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER TABLE SAMLCache
                ADD SQLTime DateTime NOT NULL
                DEFAULT GETUTCDATE();

                ALTER TABLE SAMLCache
                ALTER COLUMN [Value] nvarchar(max)");
        }
    }
}
