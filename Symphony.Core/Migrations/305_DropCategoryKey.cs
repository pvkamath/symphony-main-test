﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration305 : Migration
    {
        public override void Up()
        {
            // Removing category key as we wont end up needing this
            RemoveColumn("Category", "CategoryKey");
        }
    }
}