﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration249 : Migration
    {
        public override void Up()
        {
            Execute("alter table Customer add ShowPretestOverride bit");
            Execute("alter table Customer add ShowPostTestOverride bit");
            Execute("alter table Customer add PassingScoreOverride int");
            Execute("alter table Customer add SkinOverride int");
            Execute("alter table Customer add PreTestTypeOverride int");
            Execute("alter table Customer add PostTestTypeOverride int");
            Execute("alter table Customer add NavigationMethodOverride int null");
            Execute("alter table Customer add DisclaimerOverride nvarchar(50) null ");
            Execute("alter table Customer add ContactEmailOverride nvarchar(50) null");
            Execute("alter table Customer add ShowObjectivesOverride bit null");
            Execute("alter table Customer add PretestTestOutOverride bit null");
            Execute("alter table Customer add RetestMode int not null default 0");
            Execute("alter table Customer add Retries int null");
        }
    }
}