﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration112 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER TABLE dbo.ClassDate WITH NOCHECK ADD CONSTRAINT
	                    FK_ClassDate_Customer FOREIGN KEY
	                    (
	                    CustomerID
	                    ) REFERENCES dbo.Customer
	                    (
	                    ID
	                    ) ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION 
	                     NOT FOR REPLICATION

                    ALTER TABLE dbo.ClassDate
	                    NOCHECK CONSTRAINT FK_ClassDate_Customer");
        }
    }
}