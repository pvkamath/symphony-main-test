﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration032 : Migration
    {
        public override void Up()
        {
            DropTable("ArtisanCourses");
            TableSchema.Table courses = CreateTable("ArtisanCourses");
            courses.AddPrimaryKeyColumn("ID");
            courses.AddColumn("CustomerID", DbType.Int32, 0, false);
            courses.AddColumn("Name", DbType.String, 50, false);
            courses.AddColumn("Description", DbType.String, 100, false);
            courses.AddColumn("Keywords", DbType.String, 100, false);
            courses.AddColumn("CategoryID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(courses);
        }
    }
}