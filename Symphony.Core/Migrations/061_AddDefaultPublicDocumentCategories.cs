﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration061 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(661);
            sb.AppendFormat(@"declare @customerId int{0}", Environment.NewLine);
            sb.AppendFormat(@"declare {0}", Environment.NewLine);
            sb.AppendFormat(@"{0}category_cursor cursor{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"for{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}select {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}id{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}from{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Customer{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"open {0}", Environment.NewLine);
            sb.AppendFormat(@"{0}category_cursor{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"fetch{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}next{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"from{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}category_cursor{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"into{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}@customerId{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"while @@FETCH_STATUS = 0{0}", Environment.NewLine);
            sb.AppendFormat(@"begin{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}if((select COUNT(*) from PublicDocumentCategories where CustomerID = @customerId) = 0){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}begin{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}insert into PublicDocumentCategories{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}(Name, [Description], CreatedBy, ModifiedBy, CustomerID, CreatedOn, ModifiedOn){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}values{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{0}('Default', 'Default Category', 'system', 'system', @customerId, GETDATE(), GETDATE()){1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}end{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}fetch next from category_cursor into @customerId{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"end{0}", Environment.NewLine);
            sb.AppendFormat(@"{0}", Environment.NewLine);
            sb.AppendFormat(@"close category_cursor{0}", Environment.NewLine);
            sb.AppendFormat(@"deallocate category_cursor");
            Execute(sb.ToString());
        }
    }
}