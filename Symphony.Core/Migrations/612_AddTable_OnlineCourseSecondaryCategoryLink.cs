﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration612 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.OnlineCourseSecondaryCategoryLink
(
	Id INT IDENTITY(1, 1) NOT NULL,
	OnlineCourseId INT NOT NULL,
	SecondaryCategoryId INT NOT NULL
)

ALTER TABLE dbo.OnlineCourseSecondaryCategoryLink
ADD CONSTRAINT PK_OnlineCourseSecondaryCategoryLink__Id PRIMARY KEY (Id)

ALTER TABLE dbo.OnlineCourseSecondaryCategoryLink
ADD CONSTRAINT FK_OnlineCourseSecondaryCategoryLink_OnlineCourse__Id FOREIGN KEY (OnlineCourseId) REFERENCES dbo.OnlineCourse(ID)

ALTER TABLE dbo.OnlineCourseSecondaryCategoryLink
ADD CONSTRAINT FK_OnlineCourseSecondaryCategoryLink_SecondaryCategory__Id FOREIGN KEY (SecondaryCategoryId) REFERENCES dbo.SecondaryCategory(Id)
");
        }
    }
}
