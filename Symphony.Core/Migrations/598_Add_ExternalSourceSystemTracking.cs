﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration598 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.ExternalSystemSourceTracking
(
	Id INT IDENTITY(1, 1),
	ExternalSystemName VARCHAR(50),
	SourceId INT,
	ArtisanCourseId INT
)

ALTER TABLE dbo.ExternalSystemSourceTracking
ADD CONSTRAINT PK_ExternalSystemSourceTracking__id PRIMARY KEY (Id)

");
        }
    }
}