﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration462 : Migration
    {

        public override void Up()
        {
            Execute(@"
IF NOT EXISTS(SELECT * FROM sys.indexes where name = 'IX_ArtisanPages_Courses' AND [object_id] = OBJECT_ID('ArtisanPages'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_ArtisanPages_Courses] ON [dbo].[ArtisanPages] 
	(
		[CourseID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 85) ON [PRIMARY]
END

IF NOT EXISTS(SELECT * FROM sys.indexes where name = 'IX_ArtisanSections_Pages' AND [object_id] = OBJECT_ID('ArtisanSectionPages'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_ArtisanSections_Pages] ON [dbo].[ArtisanSectionPages] 
	(
		[PageID] ASC,
		[SectionID] ASC,
		[CourseID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 85) ON [PRIMARY]
END

IF NOT EXISTS(SELECT * FROM sys.indexes where name = 'IX_ArtisanSections_Sections' AND [object_id] = OBJECT_ID('ArtisanSectionPages'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_ArtisanSections_Sections] ON [dbo].[ArtisanSectionPages] 
	(
		[SectionID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 85) ON [PRIMARY]
END");
        }

    }
}