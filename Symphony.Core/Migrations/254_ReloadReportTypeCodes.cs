﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration254 : Migration
    {
        public override void Up()
        {
            Execute(@"
                DELETE FROM ReportTypes

                SET IDENTITY_INSERT ReportTypes ON
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (1, 'Assigned Courses by Audience', 'coursesbyaudience', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (2, 'Assigned Courses by Course', 'coursesbycourse', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (3, 'Assigned Courses by Student', 'coursesbystudent', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (4, 'Audience Listing', 'audiencelisting', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (5, 'Classroom Roster', 'classroomroster', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (6, 'Classroom Training Summary', 'classroomtraining', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (7, 'Course Assignment', 'courseassignment', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (8, 'Course Completion', 'coursecompletion', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (9, 'Course Feedback', 'coursefeedback', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (10, 'Course Summary', 'coursesummary', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (11, 'Course Totals', 'coursetotals', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (12, 'Course Utilization', 'courseutilization', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (13, 'Online Course Schedules', 'onlinecourseschedules', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (14, 'Room Utilization', 'roomutilization', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (15, 'Student Detail', 'studentdetail', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (16, 'Student Listing', 'studentlisting', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (17, 'Student Locked Test', 'studentlockedtest', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (18, 'Student Summary', 'studentsummary', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (19, 'Student Test Analysis', 'studenttestanalysis', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (20, 'Student Test Attempt', 'studenttestattempt', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (21, 'Student Test Review', 'studenttestreview', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (22, 'Student Time Spent', 'studenttimespent', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (23, 'Student Transcript', 'studenttranscript', '', 1);
                INSERT INTO ReportTypes (ID, Name, CodeName, UserNotes, IsEnabled) VALUES (24, 'Survey', 'survey', '', 1);
                SET IDENTITY_INSERT ReportTypes OFF
            ");
        }
    }
}