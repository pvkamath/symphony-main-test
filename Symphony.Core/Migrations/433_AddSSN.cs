﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration433 : Migration
    {

        public override void Up()
        {
            Execute("alter table [User] add EncryptedSSN nvarchar(256) not null default ''");
        }

    }
}