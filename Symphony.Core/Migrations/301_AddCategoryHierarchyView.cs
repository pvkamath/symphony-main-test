﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration301 : Migration
    {
        public override void Up()
        {
            Execute(@"CREATE VIEW [dbo].[CategoryHierarchy] AS
                    select * from [fGetCategoryHierarchyBranch](0)");
        }
    }
}