﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration248 : Migration
    {
        public override void Up()
        {
            TableSchema.Table reportQE = CreateTable("ReportQueueEntries");
            reportQE.AddPrimaryKeyColumn("ID");
            reportQE.AddColumn("CustomerID", DbType.Int32, 0, false);
            reportQE.AddColumn("ReportID", DbType.Int32, 0, false);
            reportQE.AddColumn("StatusID", DbType.Int32, 0, false);
            reportQE.AddColumn("StartedOn", DbType.DateTime, 0, true, "null");
            reportQE.AddColumn("FinishedOn", DbType.DateTime, 0, true, "null");
            reportQE.AddColumn("ReportName", DbType.String, 64, false);
            reportQE.AddColumn("DownloadCSV", DbType.Boolean, 0, false, "1");
            reportQE.AddColumn("DownloadXLS", DbType.Boolean, 0, false, "1");
            reportQE.AddColumn("DownloadPDF", DbType.Boolean, 0, false, "1");
            AddSubSonicStateColumns(reportQE);
        }
    }
}