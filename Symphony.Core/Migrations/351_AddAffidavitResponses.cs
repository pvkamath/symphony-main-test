﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration351 : Migration
    {
        public override void Up()
        {
            TableSchema.Table affidavit = CreateTableWithKey("AffidavitFinalExamResponses", "ID");
            affidavit.AddColumn("AffidavitID", DbType.Int32, 0, false);
            affidavit.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            affidavit.AddColumn("CourseID", DbType.Int32, 0, false);
            affidavit.AddColumn("UserID", DbType.Int32, 0, false);
            affidavit.AddLongText("ResponseJSON", false, "'{}'");
            AddSubSonicStateColumns(affidavit);
        }
    }
}