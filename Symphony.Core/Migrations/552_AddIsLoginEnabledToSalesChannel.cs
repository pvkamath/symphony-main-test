﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration552 : Migration
    {
        public override void Up()
        {
            AddColumn("SalesChannel", "IsLoginEnabled", DbType.Boolean, 0, true, "0");
        }
    }
}
