﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration026 : Migration
    {
        public override void Up()
        {
            Execute("update [ArtisanTemplates] set [ThumbPath] = REPLACE([ThumbPath], '/Applications/BankersEdge/resources/images/thumbIcons/', '/images/artisan/templates/')");
        }
    }
}