﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration726 : Migration
    {

        public override void Up()
        {
            AddColumn("OnlineCourseRollup", "LastLaunchedOn", DbType.DateTime, 0, true);
            AddColumn("OnlineCourseRollup", "TotalSecondsRounded", DbType.Int64, 0, false, "0");
            AddColumn("OnlineCourseRollup", "TotalSecondsLastAttempt", DbType.Int64, 0, false, "0");
            AddColumn("OnlineCourseRollup", "TotalSecondsScorm", DbType.Int64, 0, false, "0");
        }

    }
}
