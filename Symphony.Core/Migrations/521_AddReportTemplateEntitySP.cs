﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration521 : Migration
    {

        public override void Up()
        {
            Execute(@"
CREATE PROCEDURE [dbo].[GetReportEntities] (
	@templateId int,
    @hideUnused bit
) AS
BEGIN
select 
	re.ID,
	re.Name, 
	re.Xtype,
	re.Config, 
	COALESCE(rte.[order], re.DefaultOrder) as [Order]
from 
	ReportEntity re
left join 
	ReportTemplateEntity rte
on
	rte.ReportEntityID = re.ID
	and
	rte.ReportTemplateID = @templateId
where
    case 
		when rte.ReportTemplateID is null
		then 0 
		else 1 
	end >= @hideUnused
order by
	[Order] ASC
END;
");
        }

    }
}
