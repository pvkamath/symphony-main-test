﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration323: Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgramToCourseLink", "EnforceCanMoveForwardIndicator", DbType.Boolean, 0, true);
        }
    }
}