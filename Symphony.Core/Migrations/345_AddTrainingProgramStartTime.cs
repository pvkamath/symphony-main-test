﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration345: Migration
    {
        public override void Up()
        {
            TableSchema.Table courseStartTime = CreateTableWithKey("CourseStartTime", "ID");
            courseStartTime.AddColumn("TrainingProgramID", DbType.Int32, 0, true);
            courseStartTime.AddColumn("OnlineCourseID", DbType.Int32, 0, false);
            courseStartTime.AddColumn("UserID", DbType.Int32, 0, false);
            courseStartTime.AddColumn("StartTime", DbType.DateTime, 0, false);

            AddSubSonicStateColumns(courseStartTime);
        }
    }
}