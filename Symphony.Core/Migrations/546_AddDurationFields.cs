﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration546 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "Duration", DbType.Int32, 0, true);
            AddColumn("OnlineCourse", "Duration", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "Duration", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "IsAdvancedCompletionPercentage", DbType.Boolean, 0, true);
        }
    }
}