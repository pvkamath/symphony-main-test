﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration133 : Migration
    {
        public override void Up()
        {
            Execute(@"CREATE VIEW [dbo].[LatestPublishedCourses] as
                      SELECT c.*
                      FROM ArtisanCourses c
                      JOIN (SELECT OriginalCourseId, MAX(CreatedOn) 'maxdate' 
                            FROM ArtisanCourses GROUP BY OriginalCourseId) c2
	                    ON c.OriginalCourseId = c2.OriginalCourseId
	                    AND c.CreatedOn = c2.maxdate
                      WHERE IsPublished = 1
                    ");
        }
    }
}