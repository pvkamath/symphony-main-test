﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration070 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanPages", "CorrectResponse", DbType.String, Int32.MaxValue, false, "''");
            AddColumn("ArtisanPages", "IncorrectResponse", DbType.String, Int32.MaxValue, false, "''");
        }
    }
}