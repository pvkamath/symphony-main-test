﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration699: Migration
    {
        public override void Up()
        {
            Execute(@"  
                update Templates set DisplayName = 'Fulfillment - New User' where CodeName = 'SalesforceNewUser';
                update Templates set DisplayName = 'Fulfillment - New Enrollment' where CodeName = 'SalesforceNewEnrollment';
            ");
        }
    }
}