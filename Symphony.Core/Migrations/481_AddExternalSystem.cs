﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration481 : Migration
    {

        public override void Up()
        {
            TableSchema.Table externalSystem = CreateTableWithKey("ExternalSystem", "ID");
            externalSystem.AddColumn("SystemName", DbType.String, 256, false);
            externalSystem.AddColumn("SystemCodeName", DbType.String, 256, false);
            externalSystem.AddColumn("LoginUrl", DbType.String, 256, false);

            TableSchema.Table customerExternalSystem = CreateTableWithKey("CustomerExternalSystem", "ID");
            customerExternalSystem.AddColumn("CustomerID", DbType.Int32, 0, false);
            customerExternalSystem.AddColumn("ExternalSystemID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(customerExternalSystem);
        }

    }
}