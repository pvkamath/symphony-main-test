﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration605 : Migration
    {
        public override void Up()
        {

            TableSchema.Table artisanAudit = CreateTableWithKey("ArtisanAudit", "ID");
            artisanAudit.AddColumn("OnlineCourseID", DbType.Int32, 0, false);
            artisanAudit.AddColumn("ArtisanCourseID", DbType.Int32, 0, false);
            artisanAudit.AddColumn("OnlineCourseVersion", DbType.Int32, 0, false);
            artisanAudit.AddColumn("PackageGuid", DbType.String, 256, false);
            artisanAudit.AddColumn("CoreVersion", DbType.String, 64, false);
            AddSubSonicStateColumns(artisanAudit);
            
        }
    }
}
