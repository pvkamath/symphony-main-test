﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration379 : Migration
    {
        public override void Up()
        {
            Execute(@"create unique nonclustered index ix_sku
                        on TrainingProgram (Sku)
                        where Sku is not null");
        }
    }
}