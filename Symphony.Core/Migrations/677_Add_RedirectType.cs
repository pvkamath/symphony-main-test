﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration677 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "RedirectType", DbType.Int32, 0, true);
            AddColumn("User", "IsRedirectOptIn", DbType.Boolean, 0, true);

            AddColumn("Customer", "OptInTitle", DbType.String, 2000, true);
            Execute(@"ALTER TABLE Customer ADD OptInMessage nvarchar(max)");
            AddColumn("Customer", "OptInConfirmText", DbType.String, 200, true);
            AddColumn("Customer", "OptInPreviewText", DbType.String, 200, true);
            AddColumn("Customer", "OptInCancelText", DbType.String, 200, true);

            Execute(@"EXECUTE sp_refreshview N'CustomerDetails';");

        }
    }
}
