﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration876 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgramRollup", "PreValidationComplete", DbType.Boolean);
        }
    }
}
