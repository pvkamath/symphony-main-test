﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    class Migration589 : Migration
    {
        public override void Up()
        {
            Execute(@"

ALTER VIEW [dbo].[AssignmentsLastAttempt] AS
-- Rolls up the latest attempt for each assignment
select 
	lastAttempt.Attempt, -- number of the attempt
	lastAttempt.CourseID,
	lastAttempt.SectionID,
	lastAttempt.TrainingProgramID,
	lastAttempt.UserID,
	asg.Score * 100 as Score,
	asg.IsMarked,
	-- Add the total number of assignments attempted for the course, this is for convenience to find any courses where # assignments attempted < total assignments in course
	( 
		select 
			count(distinct Assignments.SectionID) as AssignmentsAttempted
		from
			Assignments
		where 
			Assignments.CourseID = lastAttempt.CourseID and
			Assignments.UserID = lastAttempt.UserID
		group by
			Assignments.CourseID, 
			Assignments.UserID
	) as AssignmentsAttempted,
	-- The total assignments in the course
	courseDetails.TotalAssignments as AssignmentsInCourse,
	case 
		when s.PassingScore > 0 then s.PassingScore
		else coalesce(o.PassingScoreOverride, c.PassingScoreOverride, courseDetails.PassingScore)
	end as PassingScore,
	asg.[Date] as [Date],
	asg.GradedDate as [GradedDate]
from
(
-- first find the lastest attempt
	select 
		max(Assignments.Attempt) as Attempt,
		Assignments.CourseID, 
		Assignments.TrainingProgramID, 
		Assignments.SectionID, 
		Assignments.UserID
	from 
		Assignments
	group by 
		Assignments.CourseID, 
		Assignments.TrainingProgramID, 
		Assignments.SectionID, 
		Assignments.UserID
) lastAttempt
join -- join back to assignments to include the details of that attempt
	Assignments asg 
on 
	asg.Attempt = lastAttempt.Attempt and 
	asg.CourseID = lastAttempt.CourseID and
	asg.SectionID = lastAttempt.SectionID and
	asg.TrainingProgramID = lastAttempt.TrainingProgramID and
	asg.UserID = lastAttempt.UserID
join  
	OnlineCourse o with (nolock)
on 
	o.ID = lastAttempt.CourseID
join AssignmentsArtisanCourseDetails courseDetails
on
	courseDetails.ArtisanCourseID = asg.ArtisanCourseID
join -- get the passing score for the assignment
	ArtisanSections s with (nolock)
on 
	lastAttempt.SectionID = s.ID
join -- customer override of passing score
	Customer c with (nolock)
on 
	c.ID = o.CustomerID

");
        }
    }
}
