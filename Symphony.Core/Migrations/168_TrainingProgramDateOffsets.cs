﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration168 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgram", "NewHireStartDateOffset", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "NewHireDueDateOffset", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "NewHireEndDateOffset", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "NewHireOffsetEnabled", DbType.Boolean, 0, false, "0");
        }
    }
}