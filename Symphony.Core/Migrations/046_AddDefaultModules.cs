﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration046 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(135);
            sb.AppendFormat(@"alter table customer alter column modules nvarchar(100){0}", Environment.NewLine);
            sb.AppendFormat(@"update customer set Modules = Modules + ',CLASSROOM,CUSTOMER,COURSEASSIGNMENT'");

            Execute(sb.ToString());
        }
    }
}