﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration247 : Migration
    {
        public override void Up()
        {
            TableSchema.Table reportTypes = CreateTable("Reports");
            reportTypes.AddPrimaryKeyColumn("ID");
            reportTypes.AddColumn("Name", DbType.String, 64, false);
            reportTypes.AddColumn("ReportTypeID", DbType.Int32, 0, false);
            reportTypes.AddColumn("NotifyWhenReady", DbType.Boolean, 0, false, "0");
            reportTypes.AddColumn("DownloadCSV", DbType.Boolean, 0, false, "1");
            reportTypes.AddColumn("DownloadXLS", DbType.Boolean, 0, false, "1");
            reportTypes.AddColumn("DownloadPDF", DbType.Boolean, 0, false, "1");
            reportTypes.AddColumn("Parameters", DbType.String, 9000, false, "''");
            reportTypes.AddColumn("ScheduleType", DbType.Int32, 0, false);
            reportTypes.AddColumn("ScheduleHour", DbType.Int32, 0, true);
            reportTypes.AddColumn("ScheduleDaysOfWeek", DbType.Int32, 0, true);
            reportTypes.AddColumn("ScheduleDayOfMonth", DbType.Int32, 0, true);
            reportTypes.AddColumn("ScheduleMonth", DbType.Int32, 0, true);
            reportTypes.AddColumn("OwnerID", DbType.Int32, 0, false);
            reportTypes.AddColumn("CustomerID", DbType.Int32, 0, false);
            reportTypes.AddColumn("IsFavorite", DbType.Boolean, 0, false, "0");
            AddSubSonicStateColumns(reportTypes);
        }
    }
}