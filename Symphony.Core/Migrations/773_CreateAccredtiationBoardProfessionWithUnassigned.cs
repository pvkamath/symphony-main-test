﻿using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration773 : Migration
    {
        public override void Up()
        {
            Execute(@"CREATE VIEW AccreditationBoardProfessionWithUnassigned AS
                SELECT
                    NULL ID,
                    ab.ID AccreditationBoardID,
                    p.ID ProfessionID,
                    p.Name,
                    CAST(0 AS BIT) IsAssigned
                FROM Profession p
                    CROSS JOIN AccreditationBoard ab

                EXCEPT 

                SELECT
                    NULL ID,
                    abp.AccreditationBoardID,
                    abp.ProfessionID,
                    p.Name,
                    CAST(0 AS BIT) IsAssigned
                FROM Profession p
                    JOIN AccreditationBoardProfession abp ON abp.ProfessionID = p.ID
                    JOIN AccreditationBoard ab ON ab.ID = abp.AccreditationBoardID

                UNION

                SELECT
                    abp.ID,
                    abp.AccreditationBoardID,
                    abp.ProfessionID,
                    p.Name,
                    CAST(1 AS BIT) IsAssigned
                FROM Profession p
                    JOIN AccreditationBoardProfession abp ON abp.ProfessionID = p.ID
                    JOIN AccreditationBoard ab ON ab.ID = abp.AccreditationBoardID
            ");
        }
    }
}
