﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration680 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE procedure [dbo].[Library_GetItemsForLibrary_v2]
	(
	@libraryIdsCS nvarchar(max),
	@userId int = NULL,
	@customerId int,
	@isShowAll bit,
	@isHideInLibrary bit,	 
    @categoryIdsCS nvarchar(max),
    @secondaryCategoryIds nvarchar(max),
    @secondaryCategorySearch nvarchar(max),
    @authorIds nvarchar(max),
    @authorSearch nvarchar(max),
	@search nvarchar(50), 
	@orderBy nvarchar(50), 
	@orderDir varchar(4), 
	@pageIndex int, 
	@pageSize int,
	@isFavorite bit,
	@isInProgress bit, 
	@isNotStarted bit,
	@startDate datetime = null,
	@endDate dateTime = null,
    @libraryItemTypeId int = null
	)
as
begin 
	--if @isShowAll = 1
	--begin 
	--	select @libraryItemTypeId = LibraryItemTypeID from Library where ID = @libraryId
	--end

	;with Items as (
	select
		item.ID,
		item.LibraryItemID,
		--item.LibraryID,
		item.Name, 
		item.[Description],
		item.CategoryID,
		item.Duration,
		item.LibraryItemTypeID,
		item.CreatedOn,
		item.CourseTypeID,
		item.CategoryName,
		item.LevelIndentText, 
		item.IsInLibrary,
		item.IsFavorite,
		item.IsInProgress,
		item.LibraryFavoriteID,
		item.r,
		item.ItemCreatedOn,
		item.ItemModifiedOn,
		item.CourseVersion
	from fGetItemsForLibrary_Search
	(
		@libraryIdsCS, 
		@libraryItemTypeId, 
		@customerID, 
		@isShowAll, 
		@isHideInLibrary, 
		@categoryIdsCS,
		@secondaryCategoryIds,
		@secondaryCategorySearch,
		@authorIds,
		@authorSearch, 
		@search, 
		@orderBy, 
		@orderDir,
		@isFavorite,
		@isInProgress, 
		@isNotStarted,
		@userId,
		@startDate,
		@endDate
	) item
)
select 
	I.*,
	(select max(r) from Items) as TotalRows
from Items I
where
	I.r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
end
");
        }
    }
}
