﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration114 : Migration
    {
        public override void Up()
        {
            Execute(@"UPDATE [SYMPHONY].[dbo].[TrainingProgram]
                      SET
	                    StartDate=DATEADD(dd, DATEDIFF(dd, 0, StartDate), 0),
	                    EndDate=DATEADD(dd, DATEDIFF(dd, 0, EndDate), 0),
	                    DueDate=DATEADD(dd, DATEDIFF(dd, 0, DueDate), 0);");

            Execute(@"UPDATE [SYMPHONY].[dbo].[TrainingProgramToCourseLink]
                      SET
	                    DueDate=DATEADD(dd, DATEDIFF(dd, 0, DueDate), 0);");
        }
    }
}