﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration617 : Migration
    {
        public override void Up()
        {
            Execute(@"

ALTER TABLE dbo.License
ADD CustomerId INT NULL

ALTER TABLE dbo.License
ADD CONSTRAINT FK_License_Customer__Id FOREIGN KEY (CustomerId) REFERENCES dbo.Customer(ID)

");
        }
    }
}