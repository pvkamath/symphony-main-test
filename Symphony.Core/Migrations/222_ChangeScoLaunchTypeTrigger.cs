﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration222 : Migration
    {
        public override void Up()
        {
            // Because of the nested Dynamic SQL, make sure all quotes are DOUBLE-escaped

            Execute(@"
                DECLARE @sql nvarchar(4000);
                DECLARE @BigSQL nvarchar(4000);
                DECLARE @dbName varchar(100);

                SET @dbName = 'OnlineTraining';

                SET @sql = '
	                IF OBJECTPROPERTY(OBJECT_ID(''''ScormWindowSizeTrigger''''), ''''IsTrigger'''') = 1
                    BEGIN
                        DROP TRIGGER [dbo].[ScormWindowSizeTrigger]
                    END'
                SET @BigSQL = 'USE ' + @dbName + '; EXEC sp_executesql N''' + @sql + '''';
                EXEC (@BigSQL)

                SET @sql = '
	                CREATE TRIGGER ScormWindowSizeTrigger ON [dbo].[ScormPackageProperties] AFTER INSERT,UPDATE
                        AS
                        BEGIN
                            UPDATE
                                spp
                            SET
                                required_width = 0,
                                required_height = 0,
                                required_fullscreen = 0,
                                desired_height = 800,
                                desired_width = 1024,
                                desired_fullscreen = 0,
                                sco_launch_type = 1
                            FROM
                                inserted i
                            JOIN
                                ScormPackageProperties spp
                            ON
                                i.scorm_package_id = spp.scorm_package_id
                        END'
                SET @BigSQL = 'USE ' + @dbName + '; EXEC sp_executesql N''' + @sql + '''';
                EXEC (@BigSQL)
            ");
        }
    }
}