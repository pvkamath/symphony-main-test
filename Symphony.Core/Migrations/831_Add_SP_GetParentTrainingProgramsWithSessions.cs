﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration831 : Migration
    {
        /// <summary>
        /// Get the parent training programs for the specified customers
        /// with session start/end dates within the given range
        /// Used for notifications
        /// </summary>
        public override void Up()
        {
            Execute(@"
                create procedure [dbo].[GetParentTrainingProgramsWithSessions] 
                (
                    -- Returns the parent training program with session information
                    -- for all training programs within the specified customer
                    -- the parent training program may be in another customer
                    -- Used for the session start/end notifications and returns
                    -- training programs with sessions that match a window of time
                    -- to avoid returning all training programs for the customer

                    @customerId int, -- Will return the parent training program even if the parent training program is not in the current customer
                    @minReferenceDate datetime, -- To apply to the session start/end time to avoid returning all past sessions
                    @maxReferenceDate datetime, -- To apply to the session start/end time to avoid returning all past sessions
                    @isUseStartDate bit -- indicataor to determine whether the date range is applied to start time or end time
                )
                AS
                BEGIN
	                with TPs as (
		                select
		                   distinct 
		                   ptp.[ID]
		                  ,ptp.[CustomerID]
		                  ,ptp.[OwnerID]
		                  ,ptp.[Name]
		                  ,ptp.[InternalCode]
		                  ,ptp.[Cost]
		                  ,ptp.[Description]
		                  ,ptp.[IsNewHire]
		                  ,ptp.[IsLive]
		                  ,ptp.[StartDate]
		                  ,ptp.[EndDate]
		                  ,ptp.[DueDate]
		                  ,ptp.[EnforceRequiredOrder]
		                  ,ptp.[MinimumElectives]
		                  ,ptp.[FinalAssessmentCourseID]
		                  ,ptp.[FinalAssessmentCourseTypeID]
		                  ,ptp.[ModifiedBy]
		                  ,ptp.[CreatedBy]
		                  ,ptp.[ModifiedOn]
		                  ,ptp.[CreatedOn]
		                  ,ptp.[CategoryID]
		                  ,ptp.[NewHireStartDateOffset]
		                  ,ptp.[NewHireDueDateOffset]
		                  ,ptp.[NewHireEndDateOffset]
		                  ,ptp.[NewHireOffsetEnabled]
		                  ,ptp.[NewHireTransitionPeriod]
		                  ,ptp.[DisableScheduled]
		                  ,ptp.[CreatedByUserId]
		                  ,ptp.[ModifiedByUserId]
		                  ,ptp.[CreatedByActualUserId]
		                  ,ptp.[ModifiedByActualUserId]
		                  ,ptp.[PartnerID]
		                  ,ptp.[IsSalesforce]
		                  ,ptp.[EnforceCanMoveForwardIndicator]
		                  ,ptp.[SurveyID]
		                  ,ptp.[IsSurveyMandatory]
		                  ,ptp.[IsValidationEnabled]
		                  ,ptp.[ValidationInterval]
		                  ,ptp.[MaxCourseWork]
		                  ,ptp.[ShowPretestOverride]
		                  ,ptp.[ShowPostTestOverride]
		                  ,ptp.[PassingScoreOverride]
		                  ,ptp.[SkinOverride]
		                  ,ptp.[PreTestTypeOverride]
		                  ,ptp.[PostTestTypeOverride]
		                  ,ptp.[NavigationMethodOverride]
		                  ,ptp.[DisclaimerOverride]
		                  ,ptp.[ContactEmailOverride]
		                  ,ptp.[ShowObjectivesOverride]
		                  ,ptp.[PretestTestOutOverride]
		                  ,ptp.[Retries]
		                  ,ptp.[RandomizeQuestionsOverride]
		                  ,ptp.[RandomizeAnswersOverride]
		                  ,ptp.[ReviewMethodOverride]
		                  ,ptp.[MarkingMethodOverride]
		                  ,ptp.[InactivityMethodOverride]
		                  ,ptp.[InactivityTimeoutOverride]
		                  ,ptp.[MinimumPageTimeOverride]
		                  ,ptp.[MaximumQuestionTimeOverride]
		                  ,ptp.[CertificateEnabledOverride]
		                  ,ptp.[ExternalID]
		                  ,ptp.[AffidavitID]
		                  ,ptp.[MaxTimeOverride]
		                  ,ptp.[CertificatePath]
		                  ,ptp.[MinLoTimeOverride]
		                  ,ptp.[MinScoTimeOverride]
		                  ,ptp.[MinTimeOverride]
		                  ,ptp.[MetaDataJson]
		                  ,ptp.[Sku]
		                  ,ptp.[PurchasedFromBundleID]
		                  ,ptp.[ParentTrainingProgramID]
		                  ,ptp.[IsCourseTimingDisabled]
		                  ,ptp.[SessionCourseID]
		                  ,ptp.[IsNmls]
		                  ,ptp.[SessionTimeout]
		                  ,ptp.[CourseUnlockModeID]
		                  ,ptp.[TrainingProgramAffidavitID]
		                  ,ptp.[Duration]
		                  ,ptp.[IsAdvancedCompletionPercentage]
		                  ,ptp.[AssociatedImageData]
		                  ,ptp.[IsForceLogoutOverride]
		                  ,ptp.[AllowPrinting]
		                  ,ptp.[MinCourseWork]
		                  ,ptp.[ProctorFormID]
		                  ,ptp.[IsCustomNotificationsEnabled]
		                from 
			                TrainingProgram tp
		                join
			                TrainingProgram ptp
			                on
			                tp.ParentTrainingProgramID > 0 and ptp.ID = tp.ParentTrainingProgramID
			                or
			                tp.ParentTrainingProgramID = 0 and ptp.ID = tp.ID
		                where 
			                tp.CustomerID = @customerId
	                )
	                select 
		                   TPs.[ID]
		                  ,TPs.[CustomerID]
		                  ,TPs.[OwnerID]
		                  ,TPs.[Name]
		                  ,TPs.[InternalCode]
		                  ,TPs.[Cost]
		                  ,TPs.[Description]
		                  ,TPs.[IsNewHire]
		                  ,TPs.[IsLive]
		                  ,TPs.[StartDate]
		                  ,TPs.[EndDate]
		                  ,TPs.[DueDate]
		                  ,TPs.[EnforceRequiredOrder]
		                  ,TPs.[MinimumElectives]
		                  ,TPs.[FinalAssessmentCourseID]
		                  ,TPs.[FinalAssessmentCourseTypeID]
		                  ,TPs.[ModifiedBy]
		                  ,TPs.[CreatedBy]
		                  ,TPs.[ModifiedOn]
		                  ,TPs.[CreatedOn]
		                  ,TPs.[CategoryID]
		                  ,TPs.[NewHireStartDateOffset]
		                  ,TPs.[NewHireDueDateOffset]
		                  ,TPs.[NewHireEndDateOffset]
		                  ,TPs.[NewHireOffsetEnabled]
		                  ,TPs.[NewHireTransitionPeriod]
		                  ,TPs.[DisableScheduled]
		                  ,TPs.[CreatedByUserId]
		                  ,TPs.[ModifiedByUserId]
		                  ,TPs.[CreatedByActualUserId]
		                  ,TPs.[ModifiedByActualUserId]
		                  ,TPs.[PartnerID]
		                  ,TPs.[IsSalesforce]
		                  ,TPs.[EnforceCanMoveForwardIndicator]
		                  ,TPs.[SurveyID]
		                  ,TPs.[IsSurveyMandatory]
		                  ,TPs.[IsValidationEnabled]
		                  ,TPs.[ValidationInterval]
		                  ,TPs.[MaxCourseWork]
		                  ,TPs.[ShowPretestOverride]
		                  ,TPs.[ShowPostTestOverride]
		                  ,TPs.[PassingScoreOverride]
		                  ,TPs.[SkinOverride]
		                  ,TPs.[PreTestTypeOverride]
		                  ,TPs.[PostTestTypeOverride]
		                  ,TPs.[NavigationMethodOverride]
		                  ,TPs.[DisclaimerOverride]
		                  ,TPs.[ContactEmailOverride]
		                  ,TPs.[ShowObjectivesOverride]
		                  ,TPs.[PretestTestOutOverride]
		                  ,TPs.[Retries]
		                  ,TPs.[RandomizeQuestionsOverride]
		                  ,TPs.[RandomizeAnswersOverride]
		                  ,TPs.[ReviewMethodOverride]
		                  ,TPs.[MarkingMethodOverride]
		                  ,TPs.[InactivityMethodOverride]
		                  ,TPs.[InactivityTimeoutOverride]
		                  ,TPs.[MinimumPageTimeOverride]
		                  ,TPs.[MaximumQuestionTimeOverride]
		                  ,TPs.[CertificateEnabledOverride]
		                  ,TPs.[ExternalID]
		                  ,TPs.[AffidavitID]
		                  ,TPs.[MaxTimeOverride]
		                  ,TPs.[CertificatePath]
		                  ,TPs.[MinLoTimeOverride]
		                  ,TPs.[MinScoTimeOverride]
		                  ,TPs.[MinTimeOverride]
		                  ,TPs.[MetaDataJson]
		                  ,TPs.[Sku]
		                  ,TPs.[PurchasedFromBundleID]
		                  ,TPs.[ParentTrainingProgramID]
		                  ,TPs.[IsCourseTimingDisabled]
		                  ,TPs.[SessionCourseID]
		                  ,TPs.[IsNmls]
		                  ,TPs.[SessionTimeout]
		                  ,TPs.[CourseUnlockModeID]
		                  ,TPs.[TrainingProgramAffidavitID]
		                  ,TPs.[Duration]
		                  ,TPs.[IsAdvancedCompletionPercentage]
		                  ,TPs.[AssociatedImageData]
		                  ,TPs.[IsForceLogoutOverride]
		                  ,TPs.[AllowPrinting]
		                  ,TPs.[MinCourseWork]
		                  ,TPs.[ProctorFormID]
		                  ,TPs.[IsCustomNotificationsEnabled]
		                  ,minStart.MinStartDateTime as 'MinSessionStartDate'
		                  ,maxStart.MaxStartDateTime as 'MaxSessionStartDate'
		                  ,cl.ID as 'SessionClassID'
		                  ,cl.Name as 'SessionClassName'
		                  ,c.Name as 'SessionCourseName'
		                  ,c.DailyDuration as 'SessionDailyDuration'
		                  ,c.NumberOfDays as 'SessionNumberOfDays'
	                from TPs
	                join
		                Course c on c.ID = TPs.SessionCourseID
	                join
		                Class cl on cl.CourseID = c.ID
	                join 
		                ClassMinStartDateTime minStart on minStart.ClassID = cl.ID
	                join 
		                ClassMaxStartDateTime maxStart on maxStart.ClassID = cl.ID
	                where
		                (
			                @isUseStartDate = 1 and 
			                minStart.MinStartDateTime > @minReferenceDate and 
			                minStart.MinStartDateTime < @maxReferenceDate
		                ) 
		                or
		                (
			                @isUseStartDate = 0 and 
			                dateadd(minute, c.DailyDuration, maxStart.MaxStartDateTime) > @minReferenceDate and 
			                dateadd(minute, c.DailyDuration, maxStart.MaxStartDateTime) < @maxReferenceDate
		                )
                END
");
        }
    }
}
