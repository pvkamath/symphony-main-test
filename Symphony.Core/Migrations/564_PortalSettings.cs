using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration564 : Migration
    {

        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.PortalSettings
(
	Id INT IDENTITY(1, 1) NOT NULL,
	CustomerId INT NOT NULL,
	WidgetId INT,
	WidgetName VARCHAR(100) NOT NULL,
	WidgetOrderNumber INT,
	WidgetIsVisible BIT
)
");

            Execute(@"
ALTER TABLE dbo.PortalSettings
ADD CONSTRAINT PK_PortalSettings__Id PRIMARY KEY (Id)
");


            Execute(@"
ALTER TABLE dbo.PortalSettings
ADD CONSTRAINT FK_PortalSettings_Customer FOREIGN KEY (CustomerId) REFERENCES dbo.Customer(ID)
");

		}
	}
}