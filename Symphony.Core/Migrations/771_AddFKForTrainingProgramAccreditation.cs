﻿using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration771 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE TrainingProgramAccreditation"
                + " ADD CONSTRAINT fk_AccreditationBoard_ID_TrainingProgramAccreditation_AccreditiationBoardID FOREIGN KEY(AccreditationBoardID)"
                    + " REFERENCES AccreditationBoard(ID)"
                );

            Execute("ALTER TABLE TrainingProgramAccreditation"
                + " ADD CONSTRAINT fk_TrainingProgram_ID_TrainingProgramAccreditation_TrainingProgramID FOREIGN KEY(TrainingProgramID)"
                    + " REFERENCES TrainingProgram(ID)"
                );
        }
    }
}
