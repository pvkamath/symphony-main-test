﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration261 : Migration
    {
        public override void Up()
        {
            Execute(@"
                alter view [dbo].[SimplePublicCourses]
as
    select
        o.ID, o.Name, o.[Description], o.Credit, '0' as InternalCode, o.CustomerID, 2 as CourseTypeID, 0 as CourseCompletionTypeID, 0 as HasSurvey, 0 as OnlineCourseID, Keywords, c.Name as CategoryName, cast(0 as bit) as IsThirdParty,
        o.Retries, o.RetestMode, (SELECT TOP 1 mb.ID FROM MessageBoard AS mb WHERE mb.OnlineCourseID = o.ID) as MessageBoardID
    from
        OnlineCourse as o
    inner join
        Category as c
    on
        o.CategoryID = c.ID
    where
        publicindicator = 1

    union

    select
        co.ID, co.Name, co.[Description], Credit, InternalCode, co.CustomerID, 1 as CourseTypeID, CourseCompletionTypeId, HasSurvey, OnlineCourseID, '' as Keywords, c.Name as CategoryName, co.IsThirdParty,
        0, 0, 0
    from
        Course as co
    inner join
        Category c
    on
        co.CategoryID = c.ID
    where
        publicindicator = 1
");
        }
    }
}