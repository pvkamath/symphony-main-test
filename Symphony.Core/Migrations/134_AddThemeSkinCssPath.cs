﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration134 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanThemes", "SkinCssPath", DbType.String, 256, false, "''");
        }
    }
}