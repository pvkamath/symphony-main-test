﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration657 : Migration
    {

        public override void Up()
        {
            // Navigation percentage
            Execute(@"insert ReportEntity (Name, XType, Config, DefaultOrder, Description, IsDeprecated) values ('State','reporting.statepicker', '', 1030, 'Generic state picker', 0)");
        }

    }
}
