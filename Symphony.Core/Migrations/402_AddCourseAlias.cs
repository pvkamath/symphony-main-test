﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration402 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "CourseAlias", DbType.String, 50, true);
        }
    }
}