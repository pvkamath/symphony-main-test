﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration237 : Migration
    {

        public override void Up()
        {
            Execute(@"CREATE FUNCTION [dbo].[GetUserPermission] 
(
	-- Add the parameters for the function here
	@userID int,
	@customerID int
)
RETURNS nvarchar(500)
AS
BEGIN
declare @permissions nvarchar(500)
select
	@permissions = student.[Permissions]
from (
select
	u.LastName + ', ' + u.FirstName as [Name],	
	u.ID as UserID,
	u.CustomerID,
	isnull(replace(replace((
		select
			replace(aspR.RoleName,' ','+') as [data()]
		from
			Symphony.dbo.aspnet_UsersInRoles aspUIR
		join
			Symphony.dbo.aspnet_Roles aspR
		on
			aspUIR.RoleID = aspR.RoleID
		where
			UserID = au.UserID
			and (aspR.RoleName like 'customer%' or aspR.RoleName like 'reporting%')
		for xml path ('')
	),' ',', '),'+',' '),'') as [Permissions],
		u.createdOn as CreatedDateTime,
	u.ModifiedOn as ModifiedDateTime
from
	Symphony.dbo.[user] u
left join
	Symphony.dbo.[user] su
on
	u.SupervisorID = su.ID
left join
	Symphony.dbo.Customer c
on
	c.ID = u.CustomerID

left join
	Symphony.dbo.aspnet_Applications app
on
	app.ApplicationName = c.SubDomain
left join
	Symphony.dbo.aspnet_Users au
on
	au.LoweredUserName = lower(u.Username)
and
	au.ApplicationID = app.ApplicationID
) student

where 	student.UserID = @userID
	 and student.CustomerID = @customerID

RETURN isnull(@permissions, '')

END");
        }
    }
}