﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration352 : Migration
    {
        public override void Up()
        {
            AddColumn("OnlineCourse", "MaxTimeOverride", DbType.Int32, 0, true);
            AddColumn("Customer", "MaxTimeOverride", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "MaxTimeOverride", DbType.Int32, 0, true);
            AddColumn("ArtisanCourses", "MaxTime", DbType.Int32, 0, true);
        }
    }
}