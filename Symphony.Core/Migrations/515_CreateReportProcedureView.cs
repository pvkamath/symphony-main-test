﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration515 : Migration
    {

        public override void Up()
        {
            Execute(@"
    CREATE VIEW [dbo].[ReportProcedures] AS
    select r.[SPECIFIC_NAME] as FullName, replace(r.[SPECIFIC_NAME], 'report_', '') as CodeName from [information_schema].[routines] r
    where [ROUTINE_TYPE] = 'PROCEDURE'
    and [SPECIFIC_NAME] like 'report_%'
");
        }

    }
}