﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration869 : Migration
    {

        public override void Up()
        {
            AddColumn("TrainingProgramRollup", "IsFinalAvailable", DbType.Boolean, 0, false, "0");
            AddColumn("TrainingProgramRollup", "FinalUnlockedOn", DbType.DateTime, 0, true);
        }
    }
}
