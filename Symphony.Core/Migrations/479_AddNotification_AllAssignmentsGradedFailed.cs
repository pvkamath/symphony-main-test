﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration479 : Migration
    {

        public override void Up()
        {
            Execute(@"
IF (NOT EXISTS(SELECT * FROM Templates WHERE CodeName = 'AssignmentsGradedFailed'))
BEGIN
    INSERT INTO dbo.Templates (Subject, Body, CodeName, DisplayName, Description, Priority, CustomerID, Enabled, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, RelativeScheduledMinutes, FilterComplete, OwnerID, CcClassroomSupervisors, CcReportingSupervisors, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, IncludeCalendarInvite)
    SELECT TOP 1 'Assignments Graded Failed', 'Assignments Graded Failed', 'AssignmentsGradedFailed', 'Assignments Graded Failed', 'Student has failed one or more assignments', Priority, 0, Enabled, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, RelativeScheduledMinutes, FilterComplete, OwnerID, CcClassroomSupervisors, CcReportingSupervisors, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, IncludeCalendarInvite FROM dbo.Templates

    INSERT INTO dbo.Templates (Subject, Body, CodeName, DisplayName, Description, Priority, CustomerID, Enabled, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, RelativeScheduledMinutes, FilterComplete, OwnerID, CcClassroomSupervisors, CcReportingSupervisors, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, IncludeCalendarInvite)
    SELECT
	    tp.Subject,
	    tp.Body,
	    tp.CodeName,
	    tp.DisplayName,
	    tp.Description,
	    tp.Priority,
	    c.Id,
	    tp.Enabled,
	    tp.ModifiedBy,
	    tp.CreatedBy,
	    tp.ModifiedOn,
	    tp.CreatedOn,
	    tp.Area,
	    tp.IsScheduled,
	    tp.ScheduleParameterID,
	    tp.RelativeScheduledMinutes,
	    tp.FilterComplete,
	    tp.OwnerID,
	    tp.CcClassroomSupervisors,
	    tp.CcReportingSupervisors,
	    tp.CreatedByUserId,
	    tp.ModifiedByUserId,
	    tp.CreatedByActualUserId,
	    tp.ModifiedByActualUserId,
	    tp.IncludeCalendarInvite
    FROM
	    Customer c
	    INNER JOIN dbo.Templates tp ON tp.CustomerID = 0 AND tp.CodeName = 'AssignmentsGradedFailed'

END
        ");
        }

    }
}
