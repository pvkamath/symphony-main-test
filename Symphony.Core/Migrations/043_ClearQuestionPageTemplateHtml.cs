﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration043 : Migration
    {
        public override void Up()
        {
            Execute("update ArtisanTemplates set Html = '' where PageType = 2");
        }
    }
}