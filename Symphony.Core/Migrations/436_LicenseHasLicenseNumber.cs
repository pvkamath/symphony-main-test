﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration436 : Migration
    {

        public override void Up()
        {
            AddColumn("Licence", "HasUserLicenseNumber", DbType.Boolean, 0, false, "0");

            TableSchema.Table table = CreateTableWithKey("UserLicenseNumbers", "ID");
            table.AddColumn("UserID", DbType.Int32, 0, false);
            table.AddColumn("LicenseID", DbType.Int32, 0, false);
            table.AddColumn("LicenseNumber", DbType.String, 64, false, "''");

            AddSubSonicStateColumns(table);
        }

    }
}