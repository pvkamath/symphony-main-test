﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration836 : Migration
    {
        /// <summary>
        /// Migration to update the scorm database
        /// </summary>
        public override void Up()
        {
            Execute(@"
create procedure [dbo].[UpdateScormActivity]
(	
    @registrationId int,
    @bookmark ntext,
    @datachunk ntext
)
as
update OnlineTraining.dbo.ScormActivityRT
set
	location = @bookmark,
	suspend_data = @datachunk
where
	scorm_activity_id in
	(
		select scorm_activity_id 
		from OnlineTraining.dbo.ScormRegistration
		where scorm_registration_id = @registrationId
	);
");
        }
    }
}
