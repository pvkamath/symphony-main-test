﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration826 : Migration
    {
        /// <summary>
        /// Clean up assignment totals
        /// </summary>
        public override void Up()
        {
            Execute(@"
                update AssignmentSummary
	                set NumQuestions = s.ActualNumQuestions
                from
	                AssignmentSummary x
                join (
	                select
		                a.ArtisanCourseID,
		                a.ArtisanSectionID,
		                count(p.ID) as ActualNumQuestions
	                from AssignmentSummary a
	                join ArtisanSectionPages s on a.ArtisanSectionID = s.SectionID
	                join ArtisanPages p on s.PageID = p.ID and p.QuestionType = 8
	                group by a.ArtisanCourseID, a.ArtisanSectionID, a.Name, a.NumQuestions
                ) s
                on s.ArtisanCourseID = x.ArtisanCourseID and s.ArtisanSectionID = x.ArtisanSectionID and s.ActualNumQuestions != x.NumQuestions
");
        }
    }
}
