﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration103 : Migration
    {
        public override void Up()
        {
            Execute(@"
                    UPDATE Course SET
	                    NumberOfDays = CASE
		                    WHEN cdu.Description = 'Days' THEN c.CourseDurationUnitCount
		                    WHEN cdu.Description = 'Half-days' THEN c.CourseDurationUnitCount
		                    WHEN cdu.Description = 'Hours' THEN 1
		                    WHEN cdu.Description = 'Minutes' THEN 1
	                    END,
	                    DailyDuration = CASE
		                    WHEN cdu.Description = 'Days' THEN 480
		                    WHEN cdu.Description = 'Half-days' THEN 240
		                    WHEN cdu.Description = 'Hours' THEN (c.CourseDurationUnitCount * 60)
		                    WHEN cdu.Description = 'Minutes' THEN c.CourseDurationUnitCount
	                    END
                    FROM Course AS c
                    INNER JOIN CourseDurationUnit AS cdu
                    ON c.CourseDurationUnitID = cdu.ID

                    ALTER TABLE dbo.Course DROP CONSTRAINT
	                    FK_Course_CourseDurationUnit

                    ALTER TABLE dbo.Course DROP COLUMN
	                    CourseDurationUnitID, CourseDurationUnitCount");
        }
    }
}