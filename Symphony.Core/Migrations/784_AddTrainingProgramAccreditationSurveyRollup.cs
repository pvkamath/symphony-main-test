﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration784 : Migration
    {
        public override void Up()
        {
            TableSchema.Table rollup = CreateTableWithKey("TrainingProgramAccreditationSurveyRollup", "ID");

            rollup.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            rollup.AddColumn("OnlineCourseID", DbType.Int32, 0, false);
            rollup.AddColumn("UserID", DbType.Int32, 0, false);
            rollup.AddColumn("ProfessionID", DbType.Int32, 0, true);
            rollup.AddColumn("IsSurveyComplete", DbType.Boolean, 0, false, "0");

            Execute("ALTER TABLE TrainingProgramAccreditationSurveyRollup"
                + " ADD CONSTRAINT fk_TrainingProgram_ID_TrainingProgramAccreditationSurveyRollup_TrainingProgramID FOREIGN KEY(TrainingProgramID)"
                    + " REFERENCES TrainingProgram(ID)"
                );

            Execute("ALTER TABLE TrainingProgramAccreditationSurveyRollup"
                + " ADD CONSTRAINT fk_OnlineCourse_ID_TrainingProgramAccreditationSurveyRollup_OnlineCourseID FOREIGN KEY(OnlineCourseID)"
                    + " REFERENCES OnlineCourse(ID)"
                );

            Execute("ALTER TABLE TrainingProgramAccreditationSurveyRollup"
                + " ADD CONSTRAINT fk_User_ID_TrainingProgramAccreditationSurveyRollup_UserID FOREIGN KEY(UserID)"
                    + " REFERENCES [User](ID)"
                );

            Execute("ALTER TABLE TrainingProgramAccreditationSurveyRollup"
                + " ADD CONSTRAINT fk_Profession_ID_TrainingProgramAccreditationSurveyRollup_ProfessionID FOREIGN KEY(ProfessionID)"
                    + " REFERENCES Profession(ID)"
                );

            Execute("CREATE UNIQUE INDEX ux_TrainingProgramAccreditationSurveyRollup_TrainingProgramID_OnlineCourseID_UserID_ProfessionID"
                + " ON TrainingProgramAccreditationSurveyRollup("
                    + "TrainingProgramID,"
                    + "OnlineCourseID,"
                    + "UserID,"
                    + "ProfessionID"
                + ")"
                );
        }
    }
}
