﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration381 : Migration
    {
        public override void Up()
        {
            Execute("ALTER TABLE ArtisanDeployment ADD Info NVARCHAR(max) NULL;");
        }
    }
}