﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration668 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE procedure [dbo].[Library_GetFilterableCategoriesForLibraries]
	(
	@libraryIdsCS nvarchar(max)
	)
as
begin 
	declare @libraryIds table (id int)
	
	insert into @libraryIds
		select CAST(LTRIM(RTRIM(item)) as int)
		from fSplit(@libraryIdsCS,',');

	select 
		c.ID as CategoryID, 
		c.Name as CategoryName, 
		c.CategoryTypeID, 
		sc.SecondaryCategoryID, 
		sc.Name as SecondaryCategoryName
	from 
		(
		select 
			c.ID, 
			c.Name, 
			c.CategoryTypeID
		from 
			LibraryItem li
		join 
			OnlineCourse o 
		on 
			li.ItemID = o.ID and 
			li.LibraryItemTypeID = 1
		join 
			Category c 
		on 
			o.CategoryID = c.ID
		where 
			li.LibraryID in (select id from @libraryIds)
		group by 
			c.ID, 
			c.Name, 
			c.CategoryTypeID
		) c
	left join 
		(
		select
			o.CategoryID, 
			scl.SecondaryCategoryID, 
			sc.Name
		from 
			LibraryItem li
		join 
			OnlineCourse o 
		on 
			li.ItemID = o.ID and 
			li.LibraryItemTypeID = 1
		join 
			OnlineCourseSecondaryCategoryLink scl 
		on 
			scl.OnlineCourseId = o.Id
		join 
			SecondaryCategory sc 
		on 
			sc.Id = scl.SecondaryCategoryId
		where 
			li.LibraryID in (select id from @libraryIds)
		group by 
			o.CategoryID, 
			scl.SecondaryCategoryID, 
			sc.Name
		) sc 
	on 
		sc.CategoryID = c.ID
	order by 
		c.Name, sc.Name asc
end
");
        }
    }
}
