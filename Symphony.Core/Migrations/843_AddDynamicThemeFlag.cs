﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration843 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanThemes", "IsDynamic", DbType.Boolean, 0, false, "0");
        }
    }
}
