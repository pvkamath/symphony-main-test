﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration753 : Migration
    {
        public override void Up()
        {
            Execute(@"
        drop view CourseProblems;
        drop view FullGTMInvitations;
        drop view OnlineTranscript;
        drop view PublicCourses;
        drop view StudentTranscript;
        drop view StudentTranscriptLatestAttempts;
            ");
        }

    }
}