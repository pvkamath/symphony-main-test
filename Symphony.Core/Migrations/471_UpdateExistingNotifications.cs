﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;
using Symphony.Core;

namespace Symphony.Core.Migrations
{
    public class Migration471 : Migration
    {
        public override void Up()
        {
            Execute(String.Format("Update Notifications set Status = 2"));
        }

    }
}