﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration513 : Migration
    {

        public override void Up()
        {
            Execute(@"ALTER TABLE [dbo].[ReportTemplateEntity] WITH NOCHECK 
                        ADD CONSTRAINT [FK_ReportTemplateEntity_ReportTemplate] FOREIGN KEY([ReportTemplateID])
                        REFERENCES [dbo].[ReportTemplate] ([ID])
                        NOT FOR REPLICATION");

            Execute(@"ALTER TABLE [dbo].[ReportTemplateEntity] WITH NOCHECK 
                        ADD CONSTRAINT [FK_ReportTemplateEntity_ReportEntity] FOREIGN KEY([ReportEntityID])
                        REFERENCES [dbo].[ReportEntity] ([ID])
                        NOT FOR REPLICATION");
        }

    }
}
