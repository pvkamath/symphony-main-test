﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration818 : Migration
    {
        public override void Up()
        {
            Execute(@"
    create procedure [dbo].[GetTrainingProgramsWithUnmarkedAssignments]
	(
	    @trainingProgramIdsCommaSeparated nvarchar(max)
	)
    as
    begin 
	    declare @trainingProgramIds table (id int)
	
	    insert into @trainingProgramIds
		    select cast(ltrim(rtrim(item)) as int)
		    from fSplit(@trainingProgramIdsCommaSeparated,',');

        select
            distinct coalesce(nullif(tp.ParentTrainingProgramID, 0), tp.ID)
        from
            OnlineCourseAssignments o
        join
            TrainingProgram tp on tp.ID = o.TrainingProgramID
        join
            Course c on c.ID = tp.SessionCourseID
        join
            Class cl on cl.CourseID = c.ID
        join
            Registration r on r.ClassID = cl.ID and r.RegistrantID = o.UserID and r.RegistrationStatusID = 4
        join
            ClassMinStartDateTime mn on mn.ClassID = cl.ID
        join
            ClassMaxStartDateTime mx on mx.ClassID = cl.ID
        where
            o.ResponseStatus = 1
            and 
            mn.MinStartDateTime <= getutcdate()
            and
            dateadd(minute, c.DailyDuration, mx.MaxStartDateTime) >= getutcdate()
            and
            coalesce(nullif(tp.ParentTrainingProgramID, 0), tp.ID) in (select id from @trainingProgramIds)
    end
");
        }
    }
}