﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration118 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "Description", DbType.String, Int32.MaxValue, false, "''");
        }
    }
}