﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration144 : Migration
    {
        public override void Up()
        {
            Execute("alter table Templates add OwnerID int not null default 0");
        }
    }
}