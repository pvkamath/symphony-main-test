﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration292 : Migration
    {
        public override void Up()
        {
            Execute(@"
                DROP INDEX [body_index] ON [dbo].[Notifications] WITH ( ONLINE = OFF )
                ALTER TABLE Notifications ALTER COLUMN Body varchar(MAX)
                CREATE NONCLUSTERED INDEX [body_index] ON [dbo].[Notifications] 
(
	[Subject] ASC
)
INCLUDE ( [Body]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
");
        }
    }
}
