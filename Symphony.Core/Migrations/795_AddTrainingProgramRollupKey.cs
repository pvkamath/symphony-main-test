﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration795 : Migration
    {
        public override void Up()
        {
            Execute(@"
                ALTER TABLE TrainingProgramRollup
                ADD CONSTRAINT uc_TrainingProgramUser
                UNIQUE (TrainingProgramID, UserID)
            ");
        }
    }
}