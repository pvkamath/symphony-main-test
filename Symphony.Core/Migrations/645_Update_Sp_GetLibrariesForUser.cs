﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration645 : Migration
    {
        public override void Up()
        {
            Execute(@"
alter procedure [dbo].[Library_GetLibrariesForUser]
	(@userId int, @search nvarchar(50), @orderBy nvarchar(50), @orderDir varchar(4), @pageIndex int, @pageSize int)
as
begin 
	;with Libraries as (
		select 
			l.ID,
			l.CustomerID,
			l.Name,
			l.Cost,
			l.Details,
			l.[Description],
			l.LibraryItemTypeID,
			l.IsDeleted,
			l.CreatedOn,
			l.ModifiedOn,
			l.CreatedBy,
			l.ModifiedBy,
            l.Sku,
			lg.StartDate,
			lg.EndDate,
			count(distinct li.ID) as ItemCount,
			row_number() over 
			(
				order by
					case when @orderBy = 'Name' and @orderDir = 'asc' then l.Name end, 
					case when @orderBy = 'Name' and @orderDir = 'desc' then l.Name end desc,
					case when @orderBy = 'Cost' and @orderDir = 'asc' then l.Cost end,
					case when @orderBy = 'Cost' and @orderDir = 'desc' then l.Cost end desc,
					case when @orderBy = 'ItemCount' and @orderDir = 'asc' then count(li.ID) end,
					case when @orderBy = 'ItemCount' and @orderDir = 'desc' then count(li.ID) end desc,
					case when @orderBy = 'StartDate' and @orderDir = 'asc' then lg.StartDate end,
					case when @orderBy = 'StartDate' and @orderDir = 'desc' then lg.StartDate end desc,
					case when @orderBy = 'EndDate' and @orderDir = 'asc' then lg.EndDate end,
					case when @orderBy = 'EndDate' and @orderDir = 'desc' then lg.EndDate end desc
			)  as r
		from
		fGetHierarchyParentsForUser(@userId) h
		join LibraryGrant lg on
			lg.HierarchyNodeID = h.ID and
			lg.HierarchyTypeID = h.TypeID and
			lg.StartDate <= GetUtcDate() and
			lg.EndDate >= GetUtcDate()
		join Library l on
			l.ID = lg.LibraryID
		left join LibraryItem li on
			li.LibraryID = l.ID and 
			li.LibraryItemTypeID = l.LibraryItemTypeID
		left join TrainingProgram tp on
			tp.ID = li.ItemID and li.LibraryItemTypeID = 2 and tp.CustomerID = l.CustomerID
		left join OnlineCourse oc on
			oc.ID = li.ItemID and li.LibraryItemTypeID = 1 and oc.CustomerID = l.CustomerID
		where 
			l.Name like '%' + @search + '%' and
			coalesce(oc.CustomerID, tp.CustomerID) = l.CustomerID
		group by
			l.ID,
			l.CustomerID,
			l.Name,
			l.Cost,
			l.Details,
			l.[Description],
			l.LibraryItemTypeID,
			l.IsDeleted,
			l.CreatedOn,
			l.ModifiedOn,
			l.CreatedBy,
			l.ModifiedBy,
            l.Sku,
			lg.StartDate,
			lg.EndDate
	)
	select
		L.*,
		cast((select max(r) from Libraries) as int) as TotalRows
	from 
		Libraries L
	where
		L.r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
end
");
        }
    }
}
