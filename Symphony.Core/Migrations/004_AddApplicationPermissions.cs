﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration004 : Migration
    {
        public override void Up()
        {
            Execute("alter table customer add Modules nvarchar(50) not null default 'GTM,ARTISAN,REPORTING,PORTAL'");
        }
    }
}