﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration451 : Migration
    {

        public override void Up()
        {
            Execute(@"ALTER PROCEDURE [dbo].[CourseReset]
	                (	
		                @onlineCourseRollupId int = 0
	                )
	                as
	                declare @userId int
	                declare @courseId int
	                declare @trainingProgramId int
	                declare @retCode int
	                set @retCode = 0

	                set @userId = 0
	                set @courseId = 0
	                set @trainingProgramId = 0

	                select 
		                @userId = r.UserID, 
		                @courseId = r.CourseID, 
		                @trainingProgramID = r.TrainingProgramID
	                from 
		                OnlineCourseRollup r
	                where
		                r.Id = @onlineCourseRollupId
	
	                if (@courseId > 0)
	                begin
		                update 
			                onlineTraining.dbo.ScormRegistration
		                set
			                course_id = 0
		                where 
			                [user_id] = @userId
		                and 
			                course_id = @courseId
		                and 
			                (case 
				                when training_program_id is null then 0
				                when training_program_id is not null then training_program_id
			                end) = @trainingProgramId 
	                end
	
                ;");
        }

    }
}