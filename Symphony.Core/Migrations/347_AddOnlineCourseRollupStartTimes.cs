﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration347 : Migration
    {
        public override void Up()
        {
            /* Adds view to include the start time and the expiration time
             * of online courses with the online course rollups
             */
            Execute(@"create view [dbo].[OnlineCourseRollupStartTimes]
as
(
	select 
		r.*, 
		cst.StartTime, 
		tl.ExpiresAfterMinutes,
        tl.ExpiresAfterMinutes - datediff(minute, cst.StartTime, getdate()) as Remaining
	from OnlineCourseRollup r
	left join CourseStartTime cst on 
		r.CourseID = cst.OnlineCourseID and
		r.TrainingProgramID = cst.TrainingProgramID and
		r.UserID = cst.UserID
	left join TrainingProgramToCourseLink tl on
		r.TrainingProgramID = tl.TrainingProgramID and
		r.CourseID = tl.CourseID
);");


        }
    }
}