﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration666 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER procedure [dbo].[Library_GetCategoriesForLibrary]
	(
	@libraryIdsCS nvarchar(max),
	@libraryItemTypeId int = NULL,
	@userId int = NULL,
	@isShowAll bit,
	@customerId int, 
	@search nvarchar(50), 
	@orderBy nvarchar(50), 
	@orderDir varchar(4), 
	@pageIndex int, 
	@pageSize int,
	@isHideInLibrary bit,
	@categoryId int,
	@secondaryCategoryIds nvarchar(max),
	@secondaryCategorySearch nvarchar(max),
	@authorIds nvarchar(max),
    @authorSearch nvarchar(max),
    @isFavorite bit,
    @isInProgress bit,
	@isNotStarted bit,
	@startDate datetime = null,
	@endDate datetime = null
	)
as
begin 
	;with Items as (
	select
		item.CategoryID as ID,
		item.CategoryName as Name, 
		item.LevelIndentText,
		count(item.ID) as TotalItems,
		row_number() over 
		(
			order by
				case when @orderBy = 'TotalItems' and @orderDir = 'asc' then count(item.ID) end, 
				case when @orderBy = 'TotalItems' and @orderDir = 'desc' then count(item.ID) end desc,
				case when @orderBy = 'CategoryName' and @orderDir = 'asc' then item.CategoryName end,
				case when @orderBy = 'CategoryName' and @orderDir = 'desc' then item.CategoryName end desc,
				case when @orderBy = 'LevelIndentText' and @orderDir = 'asc' then item.LevelIndentText end,
				case when @orderBy = 'LevelIndentText' and @orderDir = 'desc' then item.LevelIndentText end desc
		)  as r
	from fGetItemsForLibrary_Search(
		@libraryIdsCS, 
		@libraryItemTypeId, 
		@customerID, 
		@isShowAll, 
		@isHideInLibrary, 
		@categoryId,
		@secondaryCategoryIds,
		@secondaryCategorySearch,
		@authorIds,
		@authorSearch, 
		@search, 
		@orderBy, 
		@orderDir,
		@isFavorite,
		@isInProgress, 
		@isNotStarted,
		@userId,
		@startDate,
		@endDate) item
	group by
		item.CategoryID,
		item.CategoryName,
		item.LevelIndentText
)
select 
	I.*,
	(select count(r) from Items) as TotalRows
from Items I
where
	I.r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
end
;
");
        }
    }
}
