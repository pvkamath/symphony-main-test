﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration089 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanThemes", "Folder", DbType.String, 256, false, "''");
        }
    }
}