﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration754 : Migration
    {

        public override void Up()
        {
            Execute(@"
                update Templates
                set 
                    IsConfigurableForTrainingProgram = 1
                where
                    CodeName in (
                        'SalesforceNewEnrollment'
                    )
            ");
        }

    }
}