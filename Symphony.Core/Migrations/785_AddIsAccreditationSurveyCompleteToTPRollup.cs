﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration785 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgramRollup", "IsAccreditationSurveyComplete", DbType.Boolean, 0, false, "1");
        }
    }
}
