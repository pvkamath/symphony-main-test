﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration275 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgramToCourseLink", "ProctorRequiredIndicator", DbType.Boolean, 0, true, "null");
        }
    }
}