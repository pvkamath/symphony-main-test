﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration688 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgram", "AllowPrinting", DbType.Boolean, 0, true);
            AddColumn("TrainingProgramToCourseLink", "AllowPrinting", DbType.Boolean, 0, false, "0");
        }
    }
}
