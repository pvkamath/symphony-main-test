﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration170 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "NewHireTransitionPeriod", DbType.Int32, 0, true);
            AddColumn("TrainingProgram", "NewHireTransitionPeriod", DbType.Int32, 0, true);
        }
    }
}