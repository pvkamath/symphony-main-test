﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration371 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE FUNCTION [dbo].[fGetCategoryHierarchyPath]
(   
    --the node that'll be considered the leaf node of the branch
    @iCategoryID int
)
RETURNS TABLE
AS
RETURN
(
WITH Category_CTE (ID, ParentCategoryID, Name, CustomerID, Level, LevelIndentText, LevelText, RootCategoryID)
AS
(
-- Anchor member definition
    SELECT
            c.ID,
            c.ParentCategoryID,
            c.Name,
            c.CustomerID,
            0 as Level,
            CAST(c.Name as nvarchar(max)) as LevelIndentText,
            CAST(c.Name as nvarchar(max)) as LevelText,
            c.ID as RootCategoryID
        FROM
            Category c
      WHERE
            c.ID = @iCategoryID
    UNION ALL
-- Recursive member definition
    SELECT
            c.ID,
            c.ParentCategoryID,
            c.Name,
            c.CustomerID,           
            Level + 1,
            CAST(c.Name + ' > ' + LevelIndentText as nvarchar(max)),
            --CAST(replicate('&nbsp;&nbsp;&nbsp;&nbsp;',Level+1) + c.Name as nvarchar(max)),
            CAST(c.Name as nvarchar(max)) as LevelText,
            RootCategoryID
    FROM
            Category c
    INNER JOIN Category_CTE cc ON
            cc.ParentCategoryID = c.ID
)
SELECT top 1
      a.Level,
      a.LevelText,
      a.LevelIndentText,
      a.RootCategoryID,
      b.*
FROM
      Category_CTE a
JOIN
      Category b
ON
      a.ID = b.ID
order by
	Level desc
	

)


");
        }
    }
}