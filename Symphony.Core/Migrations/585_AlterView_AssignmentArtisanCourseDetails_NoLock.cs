﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    class Migration585 : Migration
    {
        public override void Up()
        {
            Execute(@"

ALTER VIEW [dbo].[AssignmentsArtisanCourseDetails] AS
    -- Lists artisan course ids with assignments, number of assignmnets, and the passing score
        select 
			c.ID as ArtisanCourseID, 
			c.PassingScore,
			count(distinct ssp.SectionID) as TotalAssignments
		from 
			ArtisanCourses c with (nolock)
		join 
			ArtisanSectionPages ssp 
		on 
			c.ID = ssp.CourseID
		join 
			ArtisanPages p 
		on 
			p.ID = ssp.PageID
		where 
			p.QuestionType = 8
		group by 
			c.ID, c.PassingScore

");
        }
    }
}
