﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration331: Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanImportJob", "CourseCount", DbType.Int32, 0, true);
            AddColumn("ArtisanImportJob", "SectionCount", DbType.Int32, 0, true);
            AddColumn("ArtisanImportJob", "Lines", DbType.Int32, 0, true);
            AddColumn("ArtisanImportJob", "LinesProcessed", DbType.Int32, 0, true);
            Execute(@"ALTER TABLE ArtisanImportJob ADD PathIssues nvarchar(max)");
            AddColumn("ArtisanImportJob", "DownloadTime", DbType.String, 20, true);
            AddColumn("ArtisanImportJob", "ProcessTime", DbType.String, 20, true);
            AddColumn("ArtisanImportJob", "ArtisanTime", DbType.String, 20, true);
            AddColumn("ArtisanImportJob", "TotalTime", DbType.String, 20, true);
            AddColumn("ArtisanImportJob", "TotalFiles", DbType.Int32, 0, true);
            AddColumn("ArtisanImportJob", "FilesDuplicate", DbType.Int32, 0, true);
            AddColumn("ArtisanImportJob", "FilesDownloaded", DbType.Int32, 0, true);
            AddColumn("ArtisanImportJob", "FilesNotFound", DbType.Int32, 0, true);
            AddColumn("ArtisanImportJob", "FilesError", DbType.Int32, 0, true);
            AddColumn("ArtisanImportJob", "HTMLFiles", DbType.Int32, 0, true);
            AddColumn("ArtisanImportJob", "CoursesSkipped", DbType.Int32, 0, true);
            AddColumn("ArtisanImportJob", "LinesSkipped", DbType.Int32, 0, true);
        }
    }
}