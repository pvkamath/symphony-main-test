﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration619 : Migration
    {
        public override void Up()
        {
            AddColumn("Customer", "Description", DbType.String, 1024, false, "''");
        }
    }
}
