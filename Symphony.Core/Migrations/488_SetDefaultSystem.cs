﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration488 : Migration
    {

        public override void Up()
        {
            Execute(@"update ExternalSystem set IsRequired = 1 where SystemCodeName = 'symphony'");
        }

    }
}