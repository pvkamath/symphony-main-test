﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration015 : Migration
    {
        public override void Up()
        {
            Execute("alter table ImportJobQueue add [Description] nvarchar(500) not null default ''");
            Execute("alter table ImportJobQueue add [OutputFile] nvarchar(500) not null default ''");
        }
    }
}