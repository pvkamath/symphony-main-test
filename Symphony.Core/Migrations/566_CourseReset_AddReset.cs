﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    class Migration566 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER PROCEDURE [dbo].CourseReset
	@onlineCourseRollupId INT = 0,
	@resetAssignments BIT = 0
AS
	DECLARE @userId INT
	DECLARE @courseId INT
	DECLARE @trainingProgramId INT
	DECLARE @retCode INT
	SET @retCode = 0

	SET @userId = 0
	SET @courseId = 0
	SET @trainingProgramId = 0

	SELECT
		@userId = r.UserID,
		@courseId = r.CourseID,
		@trainingProgramId = r.TrainingProgramID
		FROM
			OnlineCourseRollup r
	WHERE r.ID = @onlineCourseRollupId

	IF (@resetAssignments = 1)
	BEGIN
		UPDATE dbo.OnlineCourseAssignments
		SET
			IsReset = 1
		WHERE
			UserID = @userId AND
			TrainingProgramID = @trainingProgramId AND
			CourseID = @courseId
	END;

	IF (@courseId > 0)
	BEGIN
		UPDATE OnlineTraining.dbo.ScormRegistration
		SET course_id = 0
		WHERE [user_id] = @userId
		AND course_id = @courseId
		AND (CASE
			WHEN training_program_id IS NULL THEN 0
			WHEN training_program_id IS NOT NULL THEN training_program_id
		END) = @trainingProgramId
	END

");
 
        }
    }
}
