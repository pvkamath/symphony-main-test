﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration732 : Migration
    {
        public override void Up()
        {
            AddColumn("Themes", "Quaternary", DbType.String, 10, false, "''");
        }
    }
}