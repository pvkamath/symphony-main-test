﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration025 : Migration
    {
        public override void Up()
        {
            Execute("create table [ArtisanTemplates] ([ID] int primary key identity, [CustomerID] int not null, [Name] nvarchar(50) not null, [Description] nvarchar(255) not null, [Html] [nvarchar](max) NOT NULL, [ThumbPath] [nvarchar](255) NOT NULL, [PageType] int NOT NULL, [QuestionTypeID] [int] NOT NULL, [ModifiedBy] [nvarchar](50) NOT NULL, [CreatedBy] [nvarchar](50) NOT NULL, [ModifiedOn] [datetime] NOT NULL, [CreatedOn] [datetime] NOT NULL)");
            Execute("create table [ArtisanQuestionTypes] ([ID] int primary key identity, [Name] nvarchar(50) not null, [DisplayName] nvarchar(50) not null, [ModifiedBy] [nvarchar](50) NOT NULL, [CreatedBy] [nvarchar](50) NOT NULL, [ModifiedOn] [datetime] NOT NULL, [CreatedOn] [datetime] NOT NULL)");

            #region Templates Data 

            StringBuilder artisanTemplatesData = new StringBuilder(53626);
            artisanTemplatesData.AppendFormat(@"SET IDENTITY_INSERT [dbo].[ArtisanTemplates] ON{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (2, 0, N'Template 1', N'Text area spanning the top row with a left column text area and ");
            artisanTemplatesData.AppendFormat(@"right ccolumn image on the second row.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .ArtisanContentTemplate01_template320x175 img{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{  {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  height: 171px;  {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  max-width: 316px;  {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:13px; overflow:auto; max-height:230px;"" class=""element2 element_text"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 2: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:13px"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""overflow:auto; float:left; max-height:175px; width:330px"" class=""element3 ");
            artisanTemplatesData.AppendFormat(@"element_text"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 3: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""float:right; height:175px; width:320px"" class=""element4 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate01_template320x175"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 4: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/1.png', 1, 0, N'administrator', ");
            artisanTemplatesData.AppendFormat(@"N'0', CAST(0x00009A3F0123E8BF AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn])
VALUES (3, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 2', N'Text area spanning the top row with a left column image and right column text area ");
            artisanTemplatesData.AppendFormat(@"on the second row.', N'
<style type=""text/css"">
 .ArtisanContentTemplate02_template320x175 img 
 ");
            artisanTemplatesData.AppendFormat(@"{{
  height: 171px;
  max-width: 316px;
 }}
</style>
<div style=""margin-left:50px; ");
            artisanTemplatesData.AppendFormat(@"margin-top:13px; border:1px #fff solid; width:700px;"">
 <div style=""max-height:30px; ");
            artisanTemplatesData.AppendFormat(@"overflow:hidden;"" class=""element1 element_text"">
  Element 1: Text
 </div>
 <div ");
            artisanTemplatesData.AppendFormat(@"style=""margin-top:13px; overflow:auto; max-height:230px;"" class=""element2 element_text"">
  ");
            artisanTemplatesData.AppendFormat(@"Element 2: Text
 </div>
 <div style=""margin-top:13px"">
  <div style=""float:left; height:175px; ");
            artisanTemplatesData.AppendFormat(@"width:320px"" class=""element3 element_media ArtisanContentTemplate02_template320x175"">
   Element ");
            artisanTemplatesData.AppendFormat(@"3: Media
  </div>
  <div style=""overflow:auto; float:right; max-height:175px; width:330px"" ");
            artisanTemplatesData.AppendFormat(@"class=""element4 element_text"">
   Element 4: Text
  </div>
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/2.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A3F01248E46 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn])
VALUES (4, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 3', N'Text area spanning the top row with an image spanning the second row.', N'
<style ");
            artisanTemplatesData.AppendFormat(@"type=""text/css"">
 .ArtisanContentTemplate03_template700x175 img 
 {{
  height: 171px;
  ");
            artisanTemplatesData.AppendFormat(@"max-width: 696px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff ");
            artisanTemplatesData.AppendFormat(@"solid; width:700px;"">
 <div style=""max-height:30px; vertical-align:top; overflow:hidden;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element1 element_text"">
  Element 1: Text
 </div>
 <div style=""margin-top:13px; ");
            artisanTemplatesData.AppendFormat(@"overflow:auto; max-height:230px; vertical-align:top;"" class=""element2 element_text"">
  Element ");
            artisanTemplatesData.AppendFormat(@"2: Text
 </div>
 <div style=""height:175px; width:700px; text-align:center; margin-top:13px;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element3 element_media ArtisanContentTemplate03_template700x175"">
  Element 3: Media
 ");
            artisanTemplatesData.AppendFormat(@"</div>
</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/3.png', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009A3F012629E3 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (5, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 4', N'Image spanning the top row with a text area spanning the second row.', N'
<style ");
            artisanTemplatesData.AppendFormat(@"type=""text/css"">
 .ArtisanContentTemplate04_template700x175 img 
 {{
  height: 171px;
  ");
            artisanTemplatesData.AppendFormat(@"max-width: 696px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff ");
            artisanTemplatesData.AppendFormat(@"solid; width:700px;"">
 <div style=""max-height:30px; overflow:hidden;"" class=""element1 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
  Element 1: Text
 </div>
 <div style=""height:175px; width:700px; ");
            artisanTemplatesData.AppendFormat(@"text-align:center; margin-top:13px;"" class=""element2 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate04_template700x175"">
  Element 2: Media
 </div>
 <div ");
            artisanTemplatesData.AppendFormat(@"style=""margin-top:13px; overflow:auto; max-height:230px"" class=""element3 element_text"">
  ");
            artisanTemplatesData.AppendFormat(@"Element 3: Text
 </div>
</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/4.png', 1, ");
            artisanTemplatesData.AppendFormat(@"0, N'administrator', N'0', CAST(0x00009A3F0125FAD7 AS DateTime), CAST(0x00008EAC00000000 AS ");
            artisanTemplatesData.AppendFormat(@"DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (6, 0, N'Template 5', N'Three text area columns spanning the entire height of the template ");
            artisanTemplatesData.AppendFormat(@"with column independent scrolling.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div class=""ArtisanContentTemplate05"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""max-height:30px; overflow:hidden;margin-bottom:15px;"" class=""element1 element_text"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""margin-top:13px"">  {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div style=""float:left; width:213px; overflow:auto; max-height:420px; ");
            artisanTemplatesData.AppendFormat(@"left:0px;margin-right:30px;"" class=""element2 element_text"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 2: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div style=""float:left; width:213px; overflow:auto; max-height:420px;margin-right:30px;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element3 element_text"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 3: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div style=""float:left;max-height:420px; overflow:auto; width:213px"" class=""element4 ");
            artisanTemplatesData.AppendFormat(@"element_text"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 4: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/5.png', 1, 0, N'administrator', ");
            artisanTemplatesData.AppendFormat(@"N'0', CAST(0x00009A3F01267F7D AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (7, 0, N'Template 6', N'Image spanning the entire page.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .ArtisanContentTemplate06_template700x420 img{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  height: 416px;   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  max-width: 696px;  {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:13px; height:420px; width:700px; text-align:center"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_media ArtisanContentTemplate06_template700x420"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 2: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/6.png', 1, 0, N'administrator', ");
            artisanTemplatesData.AppendFormat(@"N'0', CAST(0x00009A3F0126F425 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (8, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 7', N'Text area spanning the entire page.', N'
<div style=""margin-left:50px; ");
            artisanTemplatesData.AppendFormat(@"margin-top:13px; border:1px #fff solid; width:700px;"">
 <div style=""max-height:30px; ");
            artisanTemplatesData.AppendFormat(@"overflow:hidden;"" class=""element1 element_text"">
  Element 1: Text
 </div>
 <div ");
            artisanTemplatesData.AppendFormat(@"style=""margin-top:13px; overflow:auto; max-height:420px;"" class=""element2 element_text"">
  ");
            artisanTemplatesData.AppendFormat(@"Element 2: Text
 </div>
</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/7.png', 1, ");
            artisanTemplatesData.AppendFormat(@"0, N'administrator', N'0', CAST(0x00009A3F01276D32 AS DateTime), CAST(0x00008EAC00000000 AS ");
            artisanTemplatesData.AppendFormat(@"DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (9, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 8', N'Text area on the left column and image on the right column.', N'
<style ");
            artisanTemplatesData.AppendFormat(@"type=""text/css"">
 .ArtisanContentTemplate08_template320x420 img 
 {{
  max-height: 416px;
  ");
            artisanTemplatesData.AppendFormat(@"width: 316px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; ");
            artisanTemplatesData.AppendFormat(@"width:700px;"">
 <div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
 ");
            artisanTemplatesData.AppendFormat(@" Element 1: Text
 </div>
 <div style=""margin-top:13px"">
  <div style=""overflow:auto; float:left; ");
            artisanTemplatesData.AppendFormat(@"max-height:420px; width:330px"" class=""element2 element_text"">
   Element 3: Text
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""float:right; height:420px; width:320px; text-align:left"" class=""element3 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate08_template320x420"">
    Element 3: Media
  </div>
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/8.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A3F0127EAA3 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (10, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 9', N'Image on the left column and text area on the right column.', N'
<style ");
            artisanTemplatesData.AppendFormat(@"type=""text/css"">
 .ArtisanContentTemplate09_template320x420 img 
 {{
  max-height: 396px;
  ");
            artisanTemplatesData.AppendFormat(@"width: 316px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; ");
            artisanTemplatesData.AppendFormat(@"width:700px;"">
 <div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
 ");
            artisanTemplatesData.AppendFormat(@" Element 1: Text
 </div>
 <div style=""margin-top:13px"">
  <div style=""float:left; height:420px; ");
            artisanTemplatesData.AppendFormat(@"width:320px"" class=""element2 element_media ArtisanContentTemplate09_template320x420"">
   Element ");
            artisanTemplatesData.AppendFormat(@"2: Media
  </div>
  <div style=""overflow:auto; float:right; max-height:420px; width:330px"" ");
            artisanTemplatesData.AppendFormat(@"class=""element3 element_text"">
   Element 3: Text
  </div>
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/9.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A3F01288CC2 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (11, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 10', N'Text area spanning the top row with a left column text area and right column ");
            artisanTemplatesData.AppendFormat(@"image on the second row and a text area spanning the third row.', N'
<style type=""text/css"">
 ");
            artisanTemplatesData.AppendFormat(@".ArtisanContentTemplate10_template320x140 img 
 {{
  height: 136px;
  max-width: 316px;
 ");
            artisanTemplatesData.AppendFormat(@"}}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
  Element 1: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""margin-top:13px; overflow:auto; max-height:125px;"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
  Element 2: Text
 </div>
 <div style=""margin-top:13px"">
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""overflow:auto; float:left; max-height:140px; width:330px;"" class=""element3 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 3: Text
  </div>
  <div style=""float:right; height:140px; width:320px"" ");
            artisanTemplatesData.AppendFormat(@"class=""element4 element_media ArtisanContentTemplate10_template320x140"">
   Element 4: Media
  ");
            artisanTemplatesData.AppendFormat(@"</div>
 </div>
 <div style=""clear:both; padding-top:13px; overflow:auto; max-height:125px; ");
            artisanTemplatesData.AppendFormat(@"width:700px"" class=""element5 element_text"">
  Element 5: Text
 </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/10.png', 1, 0, N'administrator', ");
            artisanTemplatesData.AppendFormat(@"N'0', CAST(0x00009A3F012A228C AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (12, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 11', N'Text area spanning the top row with a left column image and right column text ");
            artisanTemplatesData.AppendFormat(@"area on the second row and a text area spanning the third row.', N'
<style type=""text/css"">
 ");
            artisanTemplatesData.AppendFormat(@".ArtisanContentTemplate11_template320x140 img 
 {{
  height: 136px;
  max-width: 316px;
 ");
            artisanTemplatesData.AppendFormat(@"}}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
  Element 1: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""margin-top:13px; overflow:auto; max-height:125px;"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
  Element 2: Text
 </div>
 <div style=""margin-top:13px"">
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""float:left; height:140px; width:320px"" class=""element3 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate11_template320x140"">
   Element 3: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""overflow:auto; float:right; max-height:140px; width:330px;"" class=""element4 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 4: Text
  </div>
 </div>
 <div style=""clear:both; padding-top:13px; ");
            artisanTemplatesData.AppendFormat(@"overflow:auto; max-height:125px; width:700px"" class=""element5 element_text"">
  Element 5: Text
 ");
            artisanTemplatesData.AppendFormat(@"</div>
</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/11.png', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009A3F0129E5A3 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (13, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 12', N'Three equal columns spanning the page. Each column has an image in the top row ");
            artisanTemplatesData.AppendFormat(@"and a text area in the bottom row.', N'
<style type=""text/css"">
 ");
            artisanTemplatesData.AppendFormat(@".ArtisanContentTemplate12_template210x130 img 
 {{
  height: 126px;
  max-width: 206px;
 ");
            artisanTemplatesData.AppendFormat(@"}}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
  Element 1: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""margin-top:13px; display:block; position:relative;"">
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""float:left; width:210px; overflow:hidden; max-height:130px"" class=""element2 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate12_template210x130"">
   Element 2: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""float:right; width:210px; overflow:hidden; max-height:130px"" class=""element3 ");
            artisanTemplatesData.AppendFormat(@"element_media ArtisanContentTemplate12_template210x130"">
   Element 3: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""padding-left:30px; max-height:130px; overflow:hidden; width:210px;"" class=""element4 ");
            artisanTemplatesData.AppendFormat(@"element_media ArtisanContentTemplate12_template210x130"">
   Element 4: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""clear:both; margin-top:13px"">
   <div style=""float:left; width:210px; overflow:auto; ");
            artisanTemplatesData.AppendFormat(@"max-height:275px"" class=""element5 element_text"">
    Element 5: Text
   </div>
   <div ");
            artisanTemplatesData.AppendFormat(@"style=""float:right; width:210px; overflow:auto; max-height:275px"" class=""element6 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
    Element 6: Text
   </div>
   <div style=""padding-left:30px; max-height:275px; ");
            artisanTemplatesData.AppendFormat(@"overflow:auto; width:210px;"" class=""element7 element_text"">
    Element 7: Text
   </div>
  ");
            artisanTemplatesData.AppendFormat(@"</div>
 </div>
</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/12.png', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009A3F012CEEE4 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (14, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 13', N'Three equal columns spanning the page. Each column has a text area in the top row ");
            artisanTemplatesData.AppendFormat(@"and an image in the bottom row.', N'
<style type=""text/css"">
 ");
            artisanTemplatesData.AppendFormat(@".ArtisanContentTemplate13_template210x130 img 
 {{
  height: 126px;
  max-width: 206px;
 ");
            artisanTemplatesData.AppendFormat(@"}}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
  Element 1: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""margin-top:13px"">
   <div style=""float:left; width:210px; ");
            artisanTemplatesData.AppendFormat(@"overflow:auto; max-height:275px"" class=""element2 element_text"">
    Element 2: Text
   </div>
   ");
            artisanTemplatesData.AppendFormat(@"<div style=""float:right; width:210px; overflow:auto; max-height:275px"" class=""element4 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
    Element 4: Text
   </div>
   <div style=""padding-left:30px; max-height:275px; ");
            artisanTemplatesData.AppendFormat(@"overflow:auto; width:210px;"" class=""element3 element_text"">
    Element 3: Text
   </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""clear:both; padding-top:13px"">
   <div style=""float:left; width:210px; overflow:hidden; ");
            artisanTemplatesData.AppendFormat(@"max-height:130px"" class=""element5 element_media ArtisanContentTemplate13_template210x130"">
    ");
            artisanTemplatesData.AppendFormat(@"Element 5: Media
   </div>
   <div style=""float:right; width:210px; overflow:hidden; ");
            artisanTemplatesData.AppendFormat(@"max-height:130px"" class=""element7 element_media ArtisanContentTemplate13_template210x130"">
    ");
            artisanTemplatesData.AppendFormat(@"Element 7: Media
   </div>
   <div style=""padding-left:30px; max-height: 130px; overflow: hidden; ");
            artisanTemplatesData.AppendFormat(@"width: 210px;"" class=""element6 element_media ArtisanContentTemplate13_template210x130"">
    ");
            artisanTemplatesData.AppendFormat(@"Element 6: Media
   </div>
  </div>
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/13.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A3F012EF191 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (15, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 14', N'Two column layout with text spanning the entire height of the left column and a ");
            artisanTemplatesData.AppendFormat(@"two row right column containing an image on the first row and a text area on the bottom row.', ");
            artisanTemplatesData.AppendFormat(@"N'
<style type=""text/css"">
 .ArtisanContentTemplate14_template320x210 img 
 {{
  height: 206px;
  ");
            artisanTemplatesData.AppendFormat(@"max-width: 316px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff ");
            artisanTemplatesData.AppendFormat(@"solid; width:700px;"">
 <div style=""max-height:30px; overflow:hidden;"" class=""element1 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
  Element 1: Text
 </div>
 <div style=""overflow:auto; float:left; ");
            artisanTemplatesData.AppendFormat(@"max-height:420px; width:330px; margin-top:13px"" class=""element2 element_text"">
  Element 2: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""float:right; width:320px; margin-top:13px;"">
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""height:210px; width:320px"" class=""element3 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate14_template320x210"">
   Element 3: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""margin-top:13px; overflow:auto; max-height:197px; width:320px"" class=""element4 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 4: Text
  </div>
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/14.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A3F012F8585 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (16, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 15', N'Two column layout with a two row left column containing an image on the first row ");
            artisanTemplatesData.AppendFormat(@"and a text area on the bottom row and a right column text area.', N'
<style type=""text/css"">
 ");
            artisanTemplatesData.AppendFormat(@".ArtisanContentTemplate15_template320x210 img 
 {{
  height: 206px;
  max-width: 316px;
 ");
            artisanTemplatesData.AppendFormat(@"}}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
  Element 1: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""float:left; width:320px; margin-top:13px;"">
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""height:210px; width:320px"" class=""element2 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate15_template320x210"">
   Element 2: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""margin-top:13px; overflow:auto; max-height:197px; width:320px"" class=""element3 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 3: Text
  </div>
 </div>
 <div style=""overflow:auto; float:right; ");
            artisanTemplatesData.AppendFormat(@"max-height:420px; width:330px; margin-top:13px"" class=""element4 element_text"">
  Element 4: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/15.png', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009A3F0130C1C1 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (17, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 16', N'Two column layout with a two row left column containing a text area on the first ");
            artisanTemplatesData.AppendFormat(@"row and an image on the bottom row and a right column text area', N'
<style type=""text/css"">
 ");
            artisanTemplatesData.AppendFormat(@".ArtisanContentTemplate16_template320x210 img 
 {{
  height: 206px;
  max-width: 316px;
 ");
            artisanTemplatesData.AppendFormat(@"}}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
  Element 1: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""float:left; width:320px; margin-top:13px;"">
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""overflow:auto; max-height:197px; width:320px"" class=""element2 element_text"">
   Element ");
            artisanTemplatesData.AppendFormat(@"2: Text
  </div>
  <div style=""margin-top:13px; height:210px; width:320px"" class=""element3 ");
            artisanTemplatesData.AppendFormat(@"element_media ArtisanContentTemplate16_template320x210"">
   Element 3: Media
  </div>
 </div>
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""overflow:auto; float:right; max-height:420px; width:330px; margin-top:13px"" ");
            artisanTemplatesData.AppendFormat(@"class=""element4 element_text"">
  Element 4: Text
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/16.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A3F01316289 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (18, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 17', N'Two column layout with text spanning the entire height of the left column and a ");
            artisanTemplatesData.AppendFormat(@"two row right column containing a text area on the first row and an image on the bottom row.', ");
            artisanTemplatesData.AppendFormat(@"N'
<style type=""text/css"">
 .ArtisanContentTemplate17_template320x210 img 
 {{
  height: 206px;
  ");
            artisanTemplatesData.AppendFormat(@"max-width: 316px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff ");
            artisanTemplatesData.AppendFormat(@"solid; width:700px;"">
 <div style=""max-height:30px; overflow:hidden;"" class=""element1 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
  Element 1: Text
 </div>
 <div style=""overflow:auto; float:left; ");
            artisanTemplatesData.AppendFormat(@"max-height:420px; width:330px; margin-top:13px"" class=""element2 element_text"">
  Element 2: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""float:right; width:320px; margin-top:13px;"">
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""overflow:auto; max-height:197px; width:320px"" class=""element3 element_text"">
   Element ");
            artisanTemplatesData.AppendFormat(@"3: Text
  </div>
  <div style=""height:210px; width:320px; margin-top:13px;"" class=""element4 ");
            artisanTemplatesData.AppendFormat(@"element_media ArtisanContentTemplate17_template320x210"">
   Element 4: Media
  </div>
 </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/17.png', 1, 0, N'administrator', ");
            artisanTemplatesData.AppendFormat(@"N'0', CAST(0x00009A3F0131EF4E AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (19, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 18', N'Two column layout with two rows in each column, each with a landscape image in ");
            artisanTemplatesData.AppendFormat(@"the top row and a text area in the bottom row.', N'
<style type=""text/css"">
 ");
            artisanTemplatesData.AppendFormat(@".ArtisanContentTemplate18_template320x175 img 
 {{
  height:171px;
  max-width: 316px;
 ");
            artisanTemplatesData.AppendFormat(@"}}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
  Element 1: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""margin-top:13px; float:left;"">
  <div style=""height:175px; ");
            artisanTemplatesData.AppendFormat(@"width:320px"" class=""element2 element_media ArtisanContentTemplate18_template320x175"">
   Element ");
            artisanTemplatesData.AppendFormat(@"2: Media
  </div>
  <div style=""overflow:auto; max-height:230px; width:320px; margin-top:13px;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element3 element_text"">
   Element 3: Text
  </div>
 </div>
 <div style=""margin-top:13px; ");
            artisanTemplatesData.AppendFormat(@"float: right;"">
  <div style=""height:175px; width:320px"" class=""element4 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate18_template320x175"">
   Element 4: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""overflow:auto; max-height:230px; width:320px; margin-top: 13px;"" class=""element5 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 5: Text
  </div>
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/18.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A3F01337A50 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (20, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 19', N'Two column layout with two rows in each column, each with a portrait image in the ");
            artisanTemplatesData.AppendFormat(@"top row and a text area in the bottom row.', N'
<style type=""text/css"">
 ");
            artisanTemplatesData.AppendFormat(@".ArtisanContentTemplate19_template320x255 img 
 {{
  height: 255px;
  max-width: 320px;
 ");
            artisanTemplatesData.AppendFormat(@"}}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
  Element 1: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""margin-top:13px; float:left;"">
  <div style=""height:255px; ");
            artisanTemplatesData.AppendFormat(@"width:320px"" class=""element2 element_media ArtisanContentTemplate19_template320x255"">
   Element ");
            artisanTemplatesData.AppendFormat(@"2: Media
  </div>
  <div style=""overflow:auto; max-height:150px; width:320px; margin-top:13px;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element3 element_text"">
   Element 3: Text
  </div>
 </div>
 <div style=""margin-top:13px; ");
            artisanTemplatesData.AppendFormat(@"float:right;"">
  <div style=""height:255px; width:320px;"" class=""element4 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate19_template320x255"">
   Element 4: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""overflow:auto; max-height:150px; width:320px; margin-top:13px;"" class=""element5 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 5: Text
  </div>
 </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/19.png', 1, 0, N'administrator', ");
            artisanTemplatesData.AppendFormat(@"N'0', CAST(0x00009A3F01342972 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (21, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 20', N'Two column layout with two rows in each column, each with a text area in the top ");
            artisanTemplatesData.AppendFormat(@"row and a landscape image on the bottom row.', N'
<style type=""text/css"">
 ");
            artisanTemplatesData.AppendFormat(@".ArtisanContentTemplate20_template320x175 img 
 {{
  height:171px;
  max-width: 316px;
 ");
            artisanTemplatesData.AppendFormat(@"}}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">
  Element 1: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""margin-top:13px"">
  <div style=""float:left; overflow:auto; ");
            artisanTemplatesData.AppendFormat(@"max-height:230px; width:320px"" class=""element2 element_text"">
   Element 2: Text
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""float:right; overflow:auto; max-height:230px; width:320px"" class=""element3 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 3: Text
  </div>
 </div>
 <div style=""clear: both; margin-top: 13px"">
 ");
            artisanTemplatesData.AppendFormat(@" <div style=""float:left; overflow:hidden; height:175px; width:320px; margin-top:13px;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element4 element_media ArtisanContentTemplate20_template320x175"">
   Element 4: Media
  ");
            artisanTemplatesData.AppendFormat(@"</div>
  <div style=""float:right; overflow:hidden; height:175px; width:320px; margin-top:13px;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element5 element_media ArtisanContentTemplate20_template320x175"">
   Element 5: Media
  ");
            artisanTemplatesData.AppendFormat(@"</div>
 </div>
</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/20.png', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009A3F0134FF97 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (22, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 21', N'Two column layout with two rows in each column, each with a text area in the top ");
            artisanTemplatesData.AppendFormat(@"row and a portrait image on the bottom row.', N'
<style type=""text/css"">
 ");
            artisanTemplatesData.AppendFormat(@".ArtisanContentTemplate21_template320x255 img 
 {{
  height: 251px;
  max-width: 316px;
 ");
            artisanTemplatesData.AppendFormat(@"}}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""max-height:30px; vertical-align:top; overflow:hidden;"" class=""element1 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
  Element 1: Text
 </div>
 <div style=""margin-top:13px"">
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""float:left; overflow:auto; max-height:150px; width:320px;"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 2: Text
  </div>
  <div style=""float:right; overflow:auto; ");
            artisanTemplatesData.AppendFormat(@"max-height:150px; width:320px"" class=""element3 element_text"">
   Element 3: Text
  </div>
 ");
            artisanTemplatesData.AppendFormat(@"</div>
 <div style=""clear:both; margin-top: 13px"">
  <div style=""float:left; overflow:hidden; ");
            artisanTemplatesData.AppendFormat(@"width:320px; height:255px; margin-top:13px;"" class=""element4 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate21_template320x255"">
   Element 4: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""float:right; overflow:hidden; height:255px; width:320px; margin-top:13px;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element5 element_media ArtisanContentTemplate21_template320x255"">
   Element 5: Media
  ");
            artisanTemplatesData.AppendFormat(@"</div>
 </div>
</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/21.png', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Administrator', N'0', CAST(0x00009A8500AEAA6F AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (23, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 22', N'Three column layout with a landscape image on the first row and a text area on ");
            artisanTemplatesData.AppendFormat(@"the second row of the left column and text area spanning the center and right column.', N'
<style ");
            artisanTemplatesData.AppendFormat(@"type=""text/css"">
 .ArtisanContentTemplate22_template210x130 img 
 {{
  height: 126px;
  ");
            artisanTemplatesData.AppendFormat(@"max-width: 206px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff ");
            artisanTemplatesData.AppendFormat(@"solid; width:700px;"">
 <div style=""max-height:30px; overflow:hidden;"" class=""element1 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
  Element 1: Text
 </div>
 <div style=""float:left; width:214px; ");
            artisanTemplatesData.AppendFormat(@"margin-top:13px;"">
  <div style=""height:130px; width:210px"" class=""element2 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate22_template210x130"">
   Element 2: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""margin-top:15px; overflow:auto; max-height:300px; width:210px"" class=""element3 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 3: Text
  </div>
 </div>
 <div style=""overflow:auto; float:right; ");
            artisanTemplatesData.AppendFormat(@"max-height:445px; width:214px;margin-top:13px"" class=""element4 element_text"">
  Element 4: Text
 ");
            artisanTemplatesData.AppendFormat(@"</div>
 <div style=""margin-left:243px"">
  <div style=""overflow:auto; max-height:445px; ");
            artisanTemplatesData.AppendFormat(@"width:214px; margin-top:13px"" class=""element5 element_text"">
   Element 5: Text
  </div>
 ");
            artisanTemplatesData.AppendFormat(@"</div>
</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/22.png', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009A3F01370876 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (24, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 23', N'Three column layout with a text area on the first row and a landscape image on ");
            artisanTemplatesData.AppendFormat(@"the second row of the left column and text area spanning the center and right column.', N'
<style ");
            artisanTemplatesData.AppendFormat(@"type=""text/css"">
 .ArtisanContentTemplate23_template210x130 img 
 {{
  height: 126px;
  ");
            artisanTemplatesData.AppendFormat(@"max-width: 206px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff ");
            artisanTemplatesData.AppendFormat(@"solid; width:700px;"">
 <div style=""max-height:30px; overflow:hidden;"" class=""element1 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
  Element 1: Text
 </div>
 <div style=""float:left; width:214px; ");
            artisanTemplatesData.AppendFormat(@"margin-top:13px;"">
  <div style=""overflow:auto; max-height:300px; width:210px;"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 2: Text
  </div>
  <div style=""height:130px; width:210px; ");
            artisanTemplatesData.AppendFormat(@"margin-top:15px;"" class=""element3 element_media ArtisanContentTemplate23_template210x130"">
   ");
            artisanTemplatesData.AppendFormat(@"Element 3: Media
  </div>
 </div>
 <div style=""overflow:auto; float:right; max-height:445px; ");
            artisanTemplatesData.AppendFormat(@"width:214px;margin-top:13px"" class=""element5 element_text"">
  Element 5: Text
 </div>
 <div ");
            artisanTemplatesData.AppendFormat(@"style=""margin-left:243px;"">
  <div style=""overflow:auto; max-height:445px; width:214px; ");
            artisanTemplatesData.AppendFormat(@"margin-top:13px"" class=""element4 element_text"">
   Element 4: Text
  </div>
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/23.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A8C00B89D20 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (25, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 24', N'Three column layout with a text area  in the first column, a landscape image at ");
            artisanTemplatesData.AppendFormat(@"the top row of and a text area second row of the middle column and a text area in the right ");
            artisanTemplatesData.AppendFormat(@"column.', N'
<style type=""text/css"">
 .ArtisanContentTemplate24_template210x130 img 
 {{
  ");
            artisanTemplatesData.AppendFormat(@"height: 126px;
  max-width: 206px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; ");
            artisanTemplatesData.AppendFormat(@"border:1px #fff solid; width:700px;"">
 <div style=""max-height:30px; overflow: hidden;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element1 element_text"">
  Element 1: Text
 </div>
 <div style=""overflow:auto; float:left; ");
            artisanTemplatesData.AppendFormat(@"max-height:445px; width:214px; margin-top:13px"" class=""element2 element_text"">
  Element 2: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""overflow:auto; float:right; max-height:445px; width:214px; ");
            artisanTemplatesData.AppendFormat(@"margin-top:13px"" class=""element3 element_text"">
  Element 3: Text
 </div>
 <div ");
            artisanTemplatesData.AppendFormat(@"style=""margin-left:243px; width:214px; margin-top:13px"">
  <div style=""height:130px; ");
            artisanTemplatesData.AppendFormat(@"width:210px"" class=""element4 element_media ArtisanContentTemplate24_template210x130"">
   Element ");
            artisanTemplatesData.AppendFormat(@"4: Media
  </div>
  <div style=""margin-top:15px; overflow:auto; max-height:300px; width:210px"" ");
            artisanTemplatesData.AppendFormat(@"class=""element5 element_text"">
   Element 5: Text
  </div>
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/24.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A3F013882D1 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (26, 0, N'Template 25', N'Three column layout with a text area  in the first column, a text ");
            artisanTemplatesData.AppendFormat(@"area at the top row and a landscape imagein the second row of the middle column and a text area in ");
            artisanTemplatesData.AppendFormat(@"the right column.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .ArtisanContentTemplate25_template210x130 img {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  height: 126px;{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  max-width: 206px;{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""overflow:auto; float:left; max-height:445px; width:214px; margin-top:13px"" ");
            artisanTemplatesData.AppendFormat(@"class=""element2 element_text"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 2: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""overflow:auto; float:right; max-height:445px; width:214px; margin-top:13px"" ");
            artisanTemplatesData.AppendFormat(@"class=""element3 element_text"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 3: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-left:243px; width:214px; margin-top:13px;"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""overflow:auto; max-height:300px; width:210px"" class=""element4 element_text"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 4: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""height:130px; width:210px; margin-top:15px"" class=""element5 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate25_template210x130"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 5: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/25.png', 1, 0, N'Administrator', ");
            artisanTemplatesData.AppendFormat(@"N'0', CAST(0x00009A8500B1254F AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (27, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 26', N'Three column layout with a text area in the left and center column, and a ");
            artisanTemplatesData.AppendFormat(@"landscape image in the top row and a text area on the bottom row of the right column.', N'
<style ");
            artisanTemplatesData.AppendFormat(@"type=""text/css"">
 .ArtisanContentTemplate26_template210x130 img 
 {{
  height: 126px;
  ");
            artisanTemplatesData.AppendFormat(@"max-width: 206px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff ");
            artisanTemplatesData.AppendFormat(@"solid; width:700px;"">
 <div style=""max-height:30px; overflow: hidden;"" class=""element1 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
  Element 1: Text
 </div>
 <div style=""overflow:auto; float:left; ");
            artisanTemplatesData.AppendFormat(@"max-height:445px; width:214px; margin-top:13px"" class=""element2 element_text"">
  Element 2: ");
            artisanTemplatesData.AppendFormat(@"Text
 </div>
 <div style=""float:right; width:214px; margin-top:13px;"">
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""height:130px; width:210px"" class=""element3 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate26_template210x130"">
   Element 3: Media
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""margin-top:15px; overflow:auto; max-height:300px; width:210px"" class=""element4 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 4: Text
  </div>
 </div>
 <div style=""margin-left:243px"">
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""overflow:auto; max-height:445px; width: 214px; margin-top:13px"" class=""element5 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
   Element 5: Text
  </div>
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/26.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A3F0139B7DD AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (28, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Template 27', N'Three column layout with a text area in the left and center column, and a text ");
            artisanTemplatesData.AppendFormat(@"area in the top row and a landscape image on the bottom row of the right column.', N'
<style ");
            artisanTemplatesData.AppendFormat(@"type=""text/css"">
 .ArtisanContentTemplate27_template210x130 img 
 {{
  height: 126px;
  ");
            artisanTemplatesData.AppendFormat(@"max-width: 206px;
 }}
</style>
<div style=""margin-left:50px; margin-top:13px; border:1px #fff ");
            artisanTemplatesData.AppendFormat(@"solid; width:700px;"">
 <div style=""max-height:30px; overflow:hidden;"" class=""element1 ");
            artisanTemplatesData.AppendFormat(@"element_text"">
  Element 1: Text
 </div>
 <div style=""overflow:auto; float:left; ");
            artisanTemplatesData.AppendFormat(@"max-height:445px; width:214px;margin-top:13px"" class=""element2 element_text"">
  Element 2: Text
 ");
            artisanTemplatesData.AppendFormat(@"</div>
 <div style=""float:right; width:214px; margin-top:13px;"">
  <div style=""overflow:auto; ");
            artisanTemplatesData.AppendFormat(@"max-height:300px; width:210px"" class=""element3 element_text"">
   Element 3: Text
  </div>
  <div ");
            artisanTemplatesData.AppendFormat(@"style=""height:130px; width:210px; margin-top:15px; overflow:hidden"" class=""element4 ");
            artisanTemplatesData.AppendFormat(@"element_media ArtisanContentTemplate27_template210x130"">
    Element 4: Media
  </div>
 </div>
 ");
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left:243px"">
  <div style=""overflow:auto; max-height:445px; width:214px; ");
            artisanTemplatesData.AppendFormat(@"margin-top:13px"" class=""element5 element_text"">
   Element 5: Text
  </div>
 </div>
</div>', ");
            artisanTemplatesData.AppendFormat(@"N'/Applications/BankersEdge/resources/images/thumbIcons/27.png', 1, 0, N'administrator', N'0', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A8C00B8707D AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) 
VALUES (29, 0, ");
            artisanTemplatesData.AppendFormat(@"N'Image Question', N'Image Question', N'
<style type=""text/css"">
 .imagequestiontemplate img
 {{
 ");
            artisanTemplatesData.AppendFormat(@" max-height:400px;
  max-width:500px;
 }}
</style>
<div class=""element1 element_media ");
            artisanTemplatesData.AppendFormat(@"imagequestiontemplate"" id=""wrapper"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" Element 1: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div id=""wrapper2"" class=""element2 element_imagequestion"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" Element 2: Image Question{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/hotspot_icon.gif', 2, 5, N'', N'', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x000099FC00A55820 AS DateTime), CAST(0x000099FC00A55820 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (33, -1, N'Flash Question', N'Flash-file question template will accept a parameterized swf ");
            artisanTemplatesData.AppendFormat(@"file built to interface with the course player.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div id=""wrapper"" class=""element1 element_flashquestion"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{element1_flashquestion}}{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/flash_icon.gif', -1, 0, N'', N'', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A4300F7A670 AS DateTime), CAST(0x00009A4300F7A670 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (34, 0, N'Multiple Choice', N'Text-based question template with a single correct answer.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .question ol li{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{ {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  list-style-type: lower-alpha;{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div class=""element1 element_question question"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" Element 1: Question{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/mc_icon.gif', 2, 1, N'', N'', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009A6500D0C80C AS DateTime), CAST(0x00009A6500D0C80C AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (35, 0, N'True/False', N'Text-based question template with a true or false answer.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .question ol li{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{ {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  list-style-type: lower-alpha;{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div class=""element1 element_question question"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" Element 1: Question{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/truefalse_icon.gif', 2, 2, N'', ");
            artisanTemplatesData.AppendFormat(@"N'', CAST(0x00009A6500D0C80C AS DateTime), CAST(0x00009A6500D0C80C AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (36, 0, N'All that Apply', N'Text-based question template with multiple correct answers.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .question ol li{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{ {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  list-style-type: lower-alpha;{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div class=""element1 element_question question"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" Element 1: Question{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/check_all_icon.gif', 2, 4, N'', ");
            artisanTemplatesData.AppendFormat(@"N'', CAST(0x00009A6500D0C80C AS DateTime), CAST(0x00009A6500D0C80C AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (37, 0, N'Fill in the Blank', N'Text-based question template with a placeholder for a single ");
            artisanTemplatesData.AppendFormat(@"or multiple word answer.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .question ol li{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{ {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  list-style-type: lower-alpha;{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div class=""element1 element_question question"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" Element 1: Question{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/fill_in_blank_icon.gif', 2, 3, ");
            artisanTemplatesData.AppendFormat(@"N'', N'', CAST(0x00009A6500D0C80C AS DateTime), CAST(0x00009A6500D0C80C AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (38, 0, N'Template 28', N'Text area spanning the top row with a left column text area and ");
            artisanTemplatesData.AppendFormat(@"right ccolumn image on the second row.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .ArtisanContentTemplate28_template320x60 img     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  height: 60px !important;    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  max-width: 318px;    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""max-height:30px; overflow:hidden font-size: 14px; font-weight:bold;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element1 element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:25px; overflow:auto; width:690px; max-height:250px;"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 2: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:20px; width: 690px"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""float:left; height:205px; width:318px; border:solid #000 1px"">      {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div class=""element3 element_media ArtisanContentTemplate28_template320x60"">         {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 3: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>       {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div style=""overflow:auto; max-height:125px; width:298px; margin:9px"" class=""element4 ");
            artisanTemplatesData.AppendFormat(@"element_text"">         {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 4: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""overflow:auto; float:right; max-height:205px; width:330px"" class=""element5 ");
            artisanTemplatesData.AppendFormat(@"element_text"">     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 5: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/callout_temp_5.gif', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009AF900EFD906 AS DateTime), CAST(0x00009AF900EFD906 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (40, 0, N'Template 29', N'Text area spanning the top row with a left column image and right ");
            artisanTemplatesData.AppendFormat(@"column text area on the second row.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .ArtisanContentTemplate29_template250x100 img     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  height: 98px !important;     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  max-width: 249px;    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""max-height:30px; overflow:hidden font-size: 14px; font-weight:bold;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element1 element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:25px; overflow:auto; width:690px; max-height:175px;"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_text"">     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 2: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""height:100px; width:690px; border:solid #000 1px; margin-top:20px"">      {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""float:left; width: 249px; height: 98px"" class=""element3 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate29_template250x100"">       {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 3: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>      {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""float:right; overflow:auto; max-height:88px; width:428px; padding:5px"" ");
            artisanTemplatesData.AppendFormat(@"class=""element4 element_text"">       {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 4: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""overflow:auto; max-height:175px; width:690px; margin-top: 20px"" class=""element5 ");
            artisanTemplatesData.AppendFormat(@"element_text"">     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 5: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>  ', N'/Applications/BankersEdge/resources/images/thumbIcons/callout_temp_3.gif', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009AF900EFD96D AS DateTime), CAST(0x00009AF900EFD96D AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (42, 0, N'Template 30', N'Text area spanning the top row with an image spanning the second ");
            artisanTemplatesData.AppendFormat(@"row.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .ArtisanContentTemplate30_template250x100 img     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  height: 98px !important;     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  max-width: 249px;    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""max-height:30px; overflow:hidden font-size: 14px;"" class=""element1 element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:25px; overflow:auto; width:690px; max-height:325px;"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 2: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""height:100px; width:690px; border:solid #000 1px; margin-top:20px"">     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""float:left; width: 249px; height: 98px"" class=""element3 element_media ");
            artisanTemplatesData.AppendFormat(@"ArtisanContentTemplate30_template250x100"">       {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 3: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>      {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""float:right; overflow:auto; max-height:88px; width:428px; padding:5px"" ");
            artisanTemplatesData.AppendFormat(@"class=""element4 element_text"">       {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 4: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div> ', N'/Applications/BankersEdge/resources/images/thumbIcons/callout_temp_2.gif', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009AF900EFD98A AS DateTime), CAST(0x00009AF900EFD98A AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (44, 0, N'Template 31', N'Image spanning the top row with a text area spanning the second ");
            artisanTemplatesData.AppendFormat(@"row.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .ArtisanContentTemplate31_template320x60 img     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  height: 60px !important;     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  max-width: 318px;    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""max-height:30px; overflow:hidden font-size: 14px; font-weight:bold;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element1 element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:25px; overflow:auto; width:690px; max-height:250px;"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 2: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:20px; width:690px"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""float:left; height:205px; width:318px; border:solid #000 1px"">      {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div class=""element3 element_media ArtisanContentTemplate31_template320x60"">        {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 3: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>       {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div style=""overflow:auto; max-height:125px; width:298px; margin:9px"" class=""element4 ");
            artisanTemplatesData.AppendFormat(@"element_text"">        {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 4: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""float:right; height:205px; width:318px; border:solid #000 1px"">      {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div class=""element5 element_media ArtisanContentTemplate31_template320x60"">        {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 5: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>       {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div style=""overflow:auto; max-height:125px; width:298px; margin:9px"" class=""element6 ");
            artisanTemplatesData.AppendFormat(@"element_text"">         {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 6: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/callout_temp_1.gif', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009AF900EFD9A1 AS DateTime), CAST(0x00009AF900EFD9A1 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (45, 0, N'Template 32', N'Three text area columns spanning the entire height of the template ");
            artisanTemplatesData.AppendFormat(@"with column independent scrolling.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .ArtisanContentTemplate32_template320x60 img     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  height: 60px !important;     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  max-width: 318px;    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""max-height:30px; overflow:hidden font-size: 14px; font-weight:bold;"" ");
            artisanTemplatesData.AppendFormat(@"class=""element1 element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:25px; overflow:auto; width:690px; max-height:250px;"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 2: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:20px; width: 690px"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""overflow:auto; float:left; max-height:175px; width:330px"" class=""element3 ");
            artisanTemplatesData.AppendFormat(@"element_text"">     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   Element 3: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""float:right; height:175px; width:318px; border:solid #000 1px"">      {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div class=""element4 element_media ArtisanContentTemplate32_template320x60"">        {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 4: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>       {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   <div style=""overflow:auto; max-height:95px; width:298px; margin:9px"" class=""element5 ");
            artisanTemplatesData.AppendFormat(@"element_text"">        {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 5: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"   </div>     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div> ', N'/Applications/BankersEdge/resources/images/thumbIcons/callout_temp_4.gif', 1, 0, ");
            artisanTemplatesData.AppendFormat(@"N'administrator', N'0', CAST(0x00009AF900EFD906 AS DateTime), CAST(0x00009AF900EFD906 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (54, 0, N'Template 33', N'Image spanning the entire page.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">  {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .ArtisanContentTemplate33_template700x420 img    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  height: 496px;   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  max-width: 711px;   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>  {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>   {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:13px; height:500px; width:715px; text-align:center"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_media ArtisanContentTemplate33_template700x420"">     {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 2: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>  {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/6.png', 1, 0, N'administrator', ");
            artisanTemplatesData.AppendFormat(@"N'0', CAST(0x00009A1600000BB8 AS DateTime), CAST(0x00009A1600000BB8 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (55, 0, N'Template 34', N'Text area spanning the entire page.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left: 50px; margin-top: 13px; border: 1px #fff solid; width: 700px;"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""max-height: 30px; overflow: hidden;"" class=""element1 element_text"">    {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  <div style=""margin-top: 13px; overflow: auto; height: 371px;"" class=""element2 element_text"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"    Element 2: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  </div>  {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/7.png', 1, 0, N'administrator', ");
            artisanTemplatesData.AppendFormat(@"N'0', CAST(0x00009A3F01276D32 AS DateTime), CAST(0x00008EAC00000000 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"INSERT [dbo].[ArtisanTemplates] ([ID], [CustomerID], [Name], [Description], [Html], [ThumbPath], ");
            artisanTemplatesData.AppendFormat(@"[PageType], [QuestionTypeID], [ModifiedBy], [CreatedBy], [ModifiedOn], [CreatedOn]) {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"VALUES (56, 0, N'Template 6 (624X432)', N'Image spanning the entire page.', N'{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<style type=""text/css"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" .ArtisanContentTemplate06_template624x432 img {0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" {{{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  height: 432px;{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  width: 624px;{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" }}{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</style>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"<div style=""margin-left:50px; margin-top:13px; border:1px #fff solid; width:700px;"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""max-height:30px; overflow:hidden;"" class=""element1 element_text"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 1: Text{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" <div style=""margin-top:8px; height:432px; width:700px; text-align:center"" class=""element2 ");
            artisanTemplatesData.AppendFormat(@"element_media ArtisanContentTemplate06_template624x432"">{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"  Element 2: Media{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@" </div>{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"</div>', N'/Applications/BankersEdge/resources/images/thumbIcons/6.png', 1, 0, N'', N'', ");
            artisanTemplatesData.AppendFormat(@"CAST(0x00009BFA00DDEB23 AS DateTime), CAST(0x00009BFA00DDEB23 AS DateTime)){0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"SET IDENTITY_INSERT [dbo].[ArtisanTemplates] OFF{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanTemplates] ADD  CONSTRAINT [DF_ArtisanTemplates_CustomerID]  DEFAULT ");
            artisanTemplatesData.AppendFormat(@"((0)) FOR [CustomerID]{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanTemplates] ADD  CONSTRAINT [DF_ArtisanTemplates_PageType]  DEFAULT ('') ");
            artisanTemplatesData.AppendFormat(@"FOR [PageType]{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanTemplates] ADD  CONSTRAINT [DF_ArtisanTemplates_Name]  DEFAULT ('') FOR [Name]{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanTemplates] ADD  CONSTRAINT [DF_ArtisanTemplates_Description]  DEFAULT ");
            artisanTemplatesData.AppendFormat(@"('') FOR [Description]{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanTemplates] ADD  CONSTRAINT [DF_ArtisanTemplates_ThumbPath]  DEFAULT ('') ");
            artisanTemplatesData.AppendFormat(@"FOR [ThumbPath]{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanTemplates] ADD  CONSTRAINT [DF_ArtisanTemplates_ModifiedBy]  DEFAULT ('') ");
            artisanTemplatesData.AppendFormat(@"FOR [ModifiedBy]{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanTemplates] ADD  CONSTRAINT [DF_ArtisanTemplates_CreatedBy]  DEFAULT ('') ");
            artisanTemplatesData.AppendFormat(@"FOR [CreatedBy]{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanTemplates] ADD  CONSTRAINT [DF_ArtisanTemplates_ModifiedOn]  DEFAULT ");
            artisanTemplatesData.AppendFormat(@"(getdate()) FOR [ModifiedOn]{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanTemplates] ADD  CONSTRAINT [DF_ArtisanTemplates_CreatedOn]  DEFAULT ");
            artisanTemplatesData.AppendFormat(@"(getdate()) FOR [CreatedOn]{0}", Environment.NewLine);
            artisanTemplatesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanTemplates] ADD  CONSTRAINT [DF_ArtisanTemplates_QuestionTypeID]  DEFAULT ");
            artisanTemplatesData.AppendFormat(@"((0)) FOR [QuestionTypeID]{0}", Environment.NewLine);

            #endregion

            Execute(artisanTemplatesData.ToString());

            #region Question Types Data

            StringBuilder artisanQuestionTypesData = new StringBuilder(1864);
            artisanQuestionTypesData.AppendFormat(@"SET IDENTITY_INSERT [dbo].[ArtisanQuestionTypes] ON{0}", Environment.NewLine);
            artisanQuestionTypesData.AppendFormat(@"INSERT [dbo].[ArtisanQuestionTypes] ([ID], [Name], [DisplayName], [ModifiedBy], [CreatedBy], ");
            artisanQuestionTypesData.AppendFormat(@"[ModifiedOn], [CreatedOn]) VALUES (1, N'MultipleChoice', N'Multiple Choice', N'', N'', ");
            artisanQuestionTypesData.AppendFormat(@"CAST(0x000099E700F7BDE0 AS DateTime), CAST(0x000099E700F7BDE0 AS DateTime)){0}", Environment.NewLine);
            artisanQuestionTypesData.AppendFormat(@"INSERT [dbo].[ArtisanQuestionTypes] ([ID], [Name], [DisplayName], [ModifiedBy], [CreatedBy], ");
            artisanQuestionTypesData.AppendFormat(@"[ModifiedOn], [CreatedOn]) VALUES (2, N'TrueFalse', N'True and False', N'', N'', ");
            artisanQuestionTypesData.AppendFormat(@"CAST(0x000099E700F7BDE0 AS DateTime), CAST(0x000099E700F7BDE0 AS DateTime)){0}", Environment.NewLine);
            artisanQuestionTypesData.AppendFormat(@"INSERT [dbo].[ArtisanQuestionTypes] ([ID], [Name], [DisplayName], [ModifiedBy], [CreatedBy], ");
            artisanQuestionTypesData.AppendFormat(@"[ModifiedOn], [CreatedOn]) VALUES (3, N'FillInTheBlank', N'Fill in the Blank', N'', N'', ");
            artisanQuestionTypesData.AppendFormat(@"CAST(0x000099EA016208D0 AS DateTime), CAST(0x000099EA016208D0 AS DateTime)){0}", Environment.NewLine);
            artisanQuestionTypesData.AppendFormat(@"INSERT [dbo].[ArtisanQuestionTypes] ([ID], [Name], [DisplayName], [ModifiedBy], [CreatedBy], ");
            artisanQuestionTypesData.AppendFormat(@"[ModifiedOn], [CreatedOn]) VALUES (4, N'AllThatApply', N'All that Apply', N'', N'', ");
            artisanQuestionTypesData.AppendFormat(@"CAST(0x000099EA017C2080 AS DateTime), CAST(0x000099EA017C2080 AS DateTime)){0}", Environment.NewLine);
            artisanQuestionTypesData.AppendFormat(@"INSERT [dbo].[ArtisanQuestionTypes] ([ID], [Name], [DisplayName], [ModifiedBy], [CreatedBy], ");
            artisanQuestionTypesData.AppendFormat(@"[ModifiedOn], [CreatedOn]) VALUES (5, N'Image', N'Image Recognition', N'', N'', ");
            artisanQuestionTypesData.AppendFormat(@"CAST(0x000099F200FB9640 AS DateTime), CAST(0x000099F200FB9640 AS DateTime)){0}", Environment.NewLine);
            artisanQuestionTypesData.AppendFormat(@"SET IDENTITY_INSERT [dbo].[ArtisanQuestionTypes] OFF{0}", Environment.NewLine);
            artisanQuestionTypesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanQuestionTypes] ADD  CONSTRAINT [DF_QuestionType_ModifiedBy]  DEFAULT ('') ");
            artisanQuestionTypesData.AppendFormat(@"FOR [ModifiedBy]{0}", Environment.NewLine);
            artisanQuestionTypesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanQuestionTypes] ADD  CONSTRAINT [DF_QuestionType_CreatedBy]  DEFAULT ('') ");
            artisanQuestionTypesData.AppendFormat(@"FOR [CreatedBy]{0}", Environment.NewLine);
            artisanQuestionTypesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanQuestionTypes] ADD  CONSTRAINT [DF_QuestionType_ModifiedOn]  DEFAULT ");
            artisanQuestionTypesData.AppendFormat(@"(getdate()) FOR [ModifiedOn]{0}", Environment.NewLine);
            artisanQuestionTypesData.AppendFormat(@"ALTER TABLE [dbo].[ArtisanQuestionTypes] ADD  CONSTRAINT [DF_QuestionType_CreatedOn]  DEFAULT ");
            artisanQuestionTypesData.AppendFormat(@"(getdate()) FOR [CreatedOn]{0}", Environment.NewLine);

            #endregion

            Execute(artisanQuestionTypesData.ToString());
        }
    }
}