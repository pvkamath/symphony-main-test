﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration123 : Migration
    {
        public override void Up()
        {
            Execute(@"update ArtisanAssetTypes set template = '<iframe src=""{path}"" style=""height:100%;width:100%;border:0;margin:0;padding:0"" frameBorder=""0"" />' where name='html'");
        }
    }
}