﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration320: Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "MaxTime", DbType.Int32, 0, true);
        }
    }
}