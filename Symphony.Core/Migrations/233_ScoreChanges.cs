﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration233 : Migration
    {

        public override void Up()
        {
            Execute(@"
create  procedure dbo.GetScoreChanges(@registrationId int)
as
begin

	declare @normalized_measure decimal(10,7)
	declare @prev_normalized_measure decimal(10,7)
	declare @count int

	declare history_cursor cursor for
	select normalized_measure from onlinetraining.dbo.scormlaunchhistory where scorm_registration_id = @registrationId order by exit_time asc

	open history_cursor

	fetch next from history_cursor 
	into @normalized_measure


	set @prev_normalized_measure = null;
	set @count = 0;

	while @@FETCH_STATUS = 0
	begin
		
		if(@prev_normalized_measure is null)
		begin
			print 'Loop: null x ' + cast(@normalized_measure * 100 as nvarchar)
		end
		else
		begin
			print 'Loop: ' + cast(@prev_normalized_measure * 100 as nvarchar) + ' x ' + cast(@normalized_measure * 100 as nvarchar)
		end

		if((@prev_normalized_measure is null and @normalized_measure > 0) or (@prev_normalized_measure != @normalized_measure))
		begin
			set @count = @count + 1;
			if(@prev_normalized_measure is null)
			begin
				print 'Change: null to ' + cast(@normalized_measure * 100 as nvarchar)
			end
			else
			begin
				print 'Change: ' + cast(@prev_normalized_measure * 100 as nvarchar) + ' to ' + cast(@normalized_measure * 100 as nvarchar)
			end
		end
		set @prev_normalized_measure = @normalized_measure

		fetch next from history_cursor 
		into @normalized_measure
	end
	close history_cursor;
	deallocate history_cursor;




	print 'Total: ' + cast(@count as nvarchar)

	select @count
end
	
");
        }

        public override void Down()
        {
            
        }
    }
}