﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration223 : Migration
    {
        public override void Up()
        {
            Execute(@"UPDATE [OnlineTraining].[dbo].ScormPackageProperties SET sco_launch_type = 1");
        }
    }
}