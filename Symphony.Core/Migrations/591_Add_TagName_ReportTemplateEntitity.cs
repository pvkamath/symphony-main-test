﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration591 : Migration
    {
        public override void Up()
        {
            AddColumn("ReportTemplateEntity", "TagName", DbType.String, 64, true);
        }
    }
}
