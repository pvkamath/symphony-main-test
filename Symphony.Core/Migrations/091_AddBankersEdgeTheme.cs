﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration091 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(478);
            sb.AppendFormat(@"SET IDENTITY_INSERT [dbo].[ArtisanThemes] ON{0}", Environment.NewLine);
            sb.AppendFormat(@"INSERT [dbo].[ArtisanThemes] ([ID], [Name], [Description], [CssPath], [CreatedOn], [ModifiedOn], ");
            sb.AppendFormat(@"[CreatedBy], [ModifiedBy], [CustomerID], [Folder]) VALUES (3, N'BankersEdge', N'Default BE', ");
            sb.AppendFormat(@"N'/skins/artisan_content/bankersedge/css/main.css', CAST(0x0000901A00000000 AS DateTime), ");
            sb.AppendFormat(@"CAST(0x0000901A00000000 AS DateTime), N'system', N'system', 0, ");
            sb.AppendFormat(@"N'/skins/artisan_content/bankersedge/css'){0}", Environment.NewLine);
            sb.AppendFormat(@"SET IDENTITY_INSERT [dbo].[ArtisanThemes] OFF");

            Execute(sb.ToString());
        }
    }
}