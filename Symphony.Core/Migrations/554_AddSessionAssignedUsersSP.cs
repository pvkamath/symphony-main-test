﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration554 : Migration
    {

        public override void Up()
        {
            Execute(@"
                CREATE procedure [dbo].[GetSessionAssignedUsers](
                    @trainingProgramIdsCommaSeparated nvarchar(max), -- Training program ids to list users for
                    
                    @sessionCourseID int = null,				     -- Session course id to override for all training programs - Only allowing 1 
																	 -- since passing in multiple wont work as we wont know which training program
																	 -- to map them to
																	 
                    @searchText nvarchar(50),						 -- Search by user/course
                    @orderBy nvarchar(50),							 -- Fullname, Username, MinStartDateTime, MaxStartDateTime
                    @orderDir varchar(4),							 
                    @pageIndex int, 
                    @pageSize int,
                    @minStartDateTime DateTime = null,				 -- Minimum date for session start date
                    @maxStartDateTime DateTime = null,				 -- Maximum date for session start date
                    @minEndDateTime DateTime = null,				 -- Minimum date for session end date
                    @maxEndDateTime DateTime = null,				 -- Maximum date for session end date
                    @showActive bit = null,							 -- Null - Show all, 0 - Show inactive only, 1 show Active only
                    @showInRangeOnly bit = null,					 -- Null or 0 show all, 1 show within search date
                    @registrationStatus int = null,					 -- Null or 0 show all, Otherwise match the status id (1,2,3,4)
					@userIdsCommaSeparated nvarchar(max)			 -- Comma separated list of user ids to filter by
                )
                as
                begin
					declare @sessionCourseIds table (id int)			-- List of session course ids to use for the training programs
					declare @trainingProgramIds table (id int)			-- List of training programs to use
					declare @userIds table(id int)						-- List of user ids to filter by
					
					declare @trainingProgramUsers table (				-- List of users assigned to the training programs
						UserID int, 
						TrainingProgramID int
					)	
					
					declare @tpId int									-- Temp value for the training program to grab users for
					declare @uId int									-- Temp value for the user id to grab training programs for
					declare @trainingProgramIdCount int					-- number of training program ids
					declare @userIdCount int							-- number of user ids to determine if we need to filter by user id
					
					declare @sessionClassDatesXML xml					-- Upcoming session class dates. For conveniently grabbing the next available
																		-- sessions in the ui for assigning users to a classw
					
					-- Convert the comma separted training program ids to a table
					insert into @trainingProgramIds
					select CAST(LTRIM(RTRIM(item)) as int)
					from fSplit(@trainingProgramIdsCommaSeparated,',');
					
					-- Convert the comma separted userids to a table
					insert into @userIds
					select CAST(LTRIM(RTRIM(item)) as int)
					from fSplit(@userIdsCommaSeparated,',');
					
					-- get totals
					select @userIdCount = COUNT(*) from @userIds;
					select @trainingProgramIdCount = COUNT(*) from @trainingProgramIds;

					-- For each training program id, grab the users
					-- Appears to be faster to pass in a single id at a time when using this view
					-- instead of doing a select in statement
					declare tpIDCursor cursor for
						select 
							ID 
						from 
							@trainingProgramIds
					
					open tpIDCursor
						fetch next from tpIDCursor into @tpId
						while @@FETCH_STATUS = 0
						begin
							insert into @trainingProgramUsers 
							select 
								tpu.UserID, tpu.TrainingProgramID
							from 
								TrainingProgramUsers tpu 
							where tpu.TrainingProgramID = @tpId
						
							fetch next from tpIDCursor into @tpId
						end
					close tpIDCursor
					deallocate tpIDCursor
					
					-- if no session ids, calculate the ids from the training programs
                    if (@sessionCourseId is null or @sessionCourseId < 1) 
                    begin
                        insert into @sessionCourseIds (ID)
                        select tp.SessionCourseID from TrainingProgram tp where tp.ID in (select t.ID from @trainingProgramIds t)
                    end
                    else 
					begin
						insert into @sessionCourseIds (ID) values (@sessionCourseId)
					end
                    ;
					
					select @sessionClassDatesXML = (
						select
							c.ID as ClassID,
							c.Name as ClassName,
							cmm.MinStartDateTime as StartDate,
							dateadd(minute, co.DailyDuration, cmm.MaxStartDateTime) as EndDate,
							co.ID as CourseID,
							co.Name as CourseName,
							tp.ID as TrainingProgramID
						from Course co
						join Class c on c.CourseID = co.ID
						join ClassDateMinMax cmm on cmm.ClassID = c.ID
						left join TrainingProgram tp on tp.SessionCourseID = co.ID -- Left join since we might not have actually assigned this course to the tp yet
						where cmm.MinStartDateTime > GETUTCDATE()
						and 
							(
								(@sessionCourseID is not null and @sessionCourseID > 0 and co.ID = @sessionCourseID) or 
								(@sessionCourseID is null or @sessionCourseID < 1) and co.ID in (select id from @sessionCourseIds)
							)
						order by cmm.MinStartDateTime asc
						for xml path('Class'), root('ClassDates')
					);
					
					-- Users with assigned sessions, these could be past, present, or future for any of the training programs passed in
					with Assigned as 
					(
						select 
							c.ID as ClassID,
							c.Name as ClassName,
							cmm.MinStartDateTime,
							dateadd(minute, co.DailyDuration, cmm.MaxStartDateTime) as 'MaxStartDateTime',
							r.RegistrantID,
							r.RegistrationStatusID,
							s.[Description] as RegistrationStatusName,
							t.ID as TrainingProgramID,
							r.CreatedOn,
							r.ModifiedOn
						from
							[Class] c
						join 
							ClassDateMinMax cmm 
						on
							cmm.ClassID = c.ID
						join 
							[Course] co
						on
							co.ID = c.CourseID
						left join 
							Registration r
						on 
							r.ClassID = c.ID
						join 
							[Status] s
						on
							s.ID = r.RegistrationStatusID
						-- Filter by training programs passed in
						-- as multiple training programs can use the same session course
						join 
							TrainingProgram t
						on 
							t.SessionCourseID = co.ID
						join 
							@trainingProgramIds ti
						on 
							ti.id = t.ID
						where
							c.CourseID in (select s.ID from @sessionCourseIds s)
					),
					-- Users with assigned sessions for the date range
					SessionAssignedUsers as
                    (
						select 
							cr.RegistrationStatusID,
							cr.RegistrationStatusName,
							cr.ClassID, 
							cr.ClassName,
							cr.MinStartDateTime, 
							cr.MaxStartDateTime,
							cr.SessionStatus,
							cr.InDateRange,
							cr.CreatedOn,
							cr.ModifiedOn,
							tpu.TrainingProgramId,
                            tp.Name as TrainingProgramName,
							u.ID,
                            u.CustomerID,
							u.Username,
							u.FirstName,
							u.MiddleName,
							u.LastName,
							u.Email,
							u.Search,
							@sessionClassDatesXML as SessionClassDatesXML,
							row_number() over (order by
                                tp.Name asc,
								cr.InDateRange asc,
								cr.SessionStatus asc,
								cr.MinStartDateTime asc,
								case when @orderBy = 'FullName' and @orderDir = 'asc' then u.LastName end,
								case when @orderBy = 'FullName' and @orderDir = 'desc' then u.LastName end desc,
								case when @orderBy = 'FullName' and @orderDir = 'asc' then u.FirstName end,
								case when @orderBy = 'FullName' and @orderDir = 'desc' then u.FirstName end desc,
								case when @orderBy = 'Username' and @orderDir = 'asc' then u.Username end,
								case when @orderBy = 'Username' and @orderDir = 'desc' then u.Username end desc,
								case when @orderBy = 'MinStartDateTime' and @orderDir = 'asc' then cr.MinStartDateTime end,
								case when @orderBy = 'MinStartDateTime' and @orderDir = 'desc' then cr.MinStartDateTime end desc,
								case when @orderBy = 'MaxStartDateTime' and @orderDir = 'asc' then cr.MaxStartDateTime end,
								case when @orderBy = 'MaxStartDateTime' and @orderDir = 'desc' then cr.MaxStartDateTime end desc,
								case when @orderBy = 'RegistrationStatusName' and @orderDir = 'asc' then cr.RegistrationStatusName end,
								case when @orderBy = 'RegistrationStatusName' and @orderDir = 'desc' then cr.RegistrationStatusName end desc,
								case when @orderBy = 'CreatedOn' and @orderDir = 'asc' then cr.CreatedOn end,
								case when @orderBy = 'CreatedOn' and @orderDir = 'desc' then cr.CreatedOn end desc,
								case when @orderBy = 'ModifiedOn' and @orderDir = 'asc' then cr.ModifiedOn end,
								case when @orderBy = 'ModifiedOn' and @orderDir = 'desc' then cr.ModifiedOn end desc,
								case when @orderBy != 'FullName' then u.LastName end,
								case when @orderBy != 'FullName' then u.FirstName end
							
							) as r
						from 
							@trainingProgramUsers tpu
						join 
							[User] u 
						on 
							u.ID = tpu.UserID
						-- Users that have sessions for the time range
						left join (
							select
								a.ClassID,
								a.ClassName,
								a.MinStartDateTime,
								a.MaxStartDateTime,
								a.RegistrantID,
								a.RegistrationStatusID,
								a.RegistrationStatusName,
								a.CreatedOn,
								a.ModifiedOn,
								case when (@minStartDateTime is null 
									or (
										@minStartDateTime < a.MinStartDateTime 
										and @maxStartDateTime > a.MinStartDateTime)
									)
								and (@maxEndDateTime is null 
									or (
										@minEndDateTime < a.MaxStartDateTime
										and @maxEndDateTime > a.MaxStartDateTime
									)
								) then 1 else 0 end as InDateRange,
								case 
									when a.MaxStartDateTime < GETUTCDATE() then 1
									when a.MinStartDateTime < GETUTCDATE() and a.MaxStartDateTime > GETUTCDATE() then 2
									when a.MinStartDateTime > GETUTCDATE() then 3 
								end as SessionStatus,
								a.TrainingProgramID
							from
								Assigned a	
						) cr 
						on 
							cr.RegistrantID = u.ID and cr.TrainingProgramID = tpu.TrainingProgramID
                        join 
                            TrainingProgram tp
                        on
                            tp.ID = tpu.TrainingProgramID
						where 
							-- filter users if necessary
							(@userIdCount = 0 or tpu.UserID in (select id from @userIds)) 
							-- Searchable params
							and (u.Search like '%' + @searchText + '%'
							or u.Email like '%' + @searchText + '%'
							or cr.ClassName like '%' + @searchText + '%')
							-- Display params
							and (
								@showActive is null or
								(@showActive = 0 and cr.ClassID is null) or
								(@showActive = 1 and cr.ClassID is not null)
							)
							and (
								@showInRangeOnly is null or
								(@showInRangeOnly = 0 and (cr.InDateRange = 0 or cr.ClassID is null)) or
								(@showInRangeOnly = 1 and cr.InDateRange = 1)
							)
							and (
								@registrationStatus is null or
								@registrationStatus = 0 or
								@registrationStatus = cr.RegistrationStatusID
							)
                    )

                    -- Get the data, grab the original row count, restrict to the page required, set order
					select
						su.RegistrationStatusID,
						su.RegistrationStatusName,
						su.ClassID, 
						su.ClassName,
						su.MinStartDateTime, 
						su.MaxStartDateTime,
						su.SessionStatus,
						su.TrainingProgramID,
                        su.TrainingProgramName,
                        su.CreatedOn,
                        su.ModifiedOn,
						cast(su.InDateRange as bit) as IsInDateRange,
						su.SessionClassDatesXML,
                        su.ID,
                        su.CustomerID,
						su.FirstName,
						su.LastName,
						su.MiddleName,
						su.Email,
						su.Username,
						su.r as Sort,
						(select count(*) from SessionAssignedUsers) as TotalRows
					from 
						SessionAssignedUsers as su
					where su.r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
					order by su.r
                end
");
        }

    }
}
