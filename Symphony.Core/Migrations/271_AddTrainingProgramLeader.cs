﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration271 : Migration
    {
        public override void Up()
        {
            TableSchema.Table trainingProgramLeader = CreateTable("TrainingProgramLeader");
            trainingProgramLeader.AddPrimaryKeyColumn("ID");
            trainingProgramLeader.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            trainingProgramLeader.AddColumn("UserID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(trainingProgramLeader);
        }
    }
}