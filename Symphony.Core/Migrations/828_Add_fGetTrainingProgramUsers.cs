﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration828 : Migration
    {
        /// <summary>
        /// This is from the TrainingProgramUser view
        /// Copying to a function so it can be accessed for
        /// individual trainingprograms with better performance
        /// than the view. 
        /// 
        /// This also includes users assigned to child training programs
        /// </summary>
        public override void Up()
        {
            Execute(@"
                CREATE FUNCTION [dbo].[fGetTrainingProgramUsers] 
                (	
	                -- Add the parameters for the function here
	                @trainingProgramID int = null,
	                @isNewHire int = null
                )
                RETURNS @HTable TABLE
                (
	                CustomerID int not null,
	                TrainingProgramID int not null,
	                TrainingProgramName varchar(2000) not null,
	                TrainingProgramStartDate datetime null,
	                TrainingProgramEndDate datetime null,
	                TrainingProgramDueDate datetime null,
	                TrainingProgramDescription nvarchar(max) not null,
	                MinimumElectives int null,
	                EnforceRequiredOrder bit not null,
	                IsNewHire bit not null,
	                SourceNodeID int not null,
	                HierarchyTypeID int not null,
	
	                IsUserNewHire int null,
	                FirstName nvarchar(50) null,
	                LastName nvarchar(50) null,
	                UserID int null,
	                ParentTrainingProgramID int null
                )
                AS
                BEGIN
	                INSERT INTO @HTable
	                SELECT     
                        program.CustomerID AS CustomerID, 
                        program.ID AS TrainingProgramID, 
                        program.[Name] AS TrainingProgramName, 
        
                        --TrainingProgramStartDate
                        case when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0)
			                then program.StartDate 
			                else cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, '1/1/1900') as datetime)
		                end as TrainingProgramStartDate,
		                --TrainingProgramEndDate	
                        case when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0) 
			                then program.EndDate 
			                else dateadd(
				                d,customer.NewHireDuration, 
				                cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, '1/1/1900') as datetime)
			                )
                        end as TrainingProgramEndDate,
                        --TrainingProgramDueDate
                        case when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0) 
			                then program.DueDate
			                else dateadd(
				                d,customer.NewHireDuration, 
				                cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, '1/1/1900') as datetime)
			                )
		                end as TrainingProgramDueDate, 
		
                        program.Description AS TrainingProgramDescription, 
                        program.MinimumElectives AS MinimumElectives, 
                        program.EnforceRequiredOrder AS EnforceRequiredOrder, 
                        program.IsNewHire AS IsNewHire,
                        FullList.ID AS SourceNodeID, 
                        link.HierarchyTypeID, 
		                COALESCE (locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) AS IsUserNewHire,
		                COALESCE (locationUsers.FirstName, jobroleUsers.FirstName, audienceUsers.FirstName, users.FirstName, NULL) AS FirstName, 
		                COALESCE (locationUsers.LastName, jobroleUsers.LastName, audienceUsers.LastName, users.LastName, NULL) AS LastName, 
		                COALESCE (locationUsers.ID, jobroleUsers.ID, audienceUsers.ID, users.ID, NULL) AS UserID,
		                program.ParentTrainingProgramID
                    FROM
                        TrainingProgram program 
                    JOIN
                        dbo.HierarchyToTrainingProgramLink link 
                    ON 
                        link.TrainingProgramID = program.ID 
                    CROSS apply 
                        dbo.fGetHierarchyBranch(link.HierarchyNodeID, link.HierarchyTypeID) AS FullList 
                    LEFT JOIN
                          dbo.[User] locationUsers ON FullList.ID = locationUsers.LocationID AND HierarchyTypeID = 1
                    LEFT JOIN
                          dbo.[User] jobroleUsers ON FullList.ID = jobroleUsers.JobRoleID AND HierarchyTypeID = 2 
                    LEFT JOIN
                          dbo.UserAudience ua ON FullList.ID = ua.AudienceID AND HierarchyTypeID = 3 
                    LEFT JOIN
                          dbo.[User] audienceUsers ON ua.UserID = audienceUsers.ID 
                    LEFT JOIN
                          dbo.[User] users ON users.ID = FullList.ID AND link.HierarchyTypeID = 4
                    left join
                        dbo.Customer customer
                    on
                        program.CustomerID = customer.ID
                    WHERE      
		                COALESCE (locationUsers.ID, jobroleUsers.ID, audienceUsers.ID, users.ID, NULL) IS NOT NULL AND program.IsLive = 1 
		                and (
			                (@trainingProgramID > 0 and (@trainingProgramID = program.ID or @trainingProgramID = program.parentTrainingProgramID))
			                or
			                ((@trainingProgramID is null or @trainingProgramID = 0) and program.ID > 0)
		                )
		                and (
			                @isNewHire is null or
			                @isNewHire = COALESCE (locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0)
		                )
	                RETURN
                END");
        }
    }
}
