﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration579 : Migration
    {
        public override void Up()
        {
            Execute(@"

CREATE TABLE dbo.ArtisanCourseSecondaryCategoryLink
(
	Id INT IDENTITY(1, 1) NOT NULL,
	ArtisanCourseId INT NOT NULL,
	SecondaryCategoryId INT NOT NULL
)

ALTER TABLE dbo.ArtisanCourseSecondaryCategoryLink
ADD CONSTRAINT PK_ArtisanCourseSecondaryCategoryLink__Id PRIMARY KEY (Id)

ALTER TABLE dbo.ArtisanCourseSecondaryCategoryLink
ADD CONSTRAINT FK_ArtisanCourseSecondaryCategoryLink_ArtisanCourse__Id FOREIGN KEY (ArtisanCourseId) REFERENCES dbo.ArtisanCourses(ID)

ALTER TABLE dbo.ArtisanCourseSecondaryCategoryLink
ADD CONSTRAINT FK_ArtisanCourseSecondaryCategoryLink_SecondaryCategory__Id FOREIGN KEY (SecondaryCategoryId) REFERENCES dbo.SecondaryCategory(Id)


");
        }
    }
}
