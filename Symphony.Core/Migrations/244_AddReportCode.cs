﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration244 : Migration
    {
        public override void Up()
        {
            AddColumn("ReportJobs", "ReportCode", DbType.String, 100, true);

            AddColumn("ReportJobTriggers", "ScheduleType", DbType.Int32);
            AddColumn("ReportJobTriggers", "ScheduleMinute", DbType.String, 100, true);
            AddColumn("ReportJobTriggers", "ScheduleHour", DbType.Int32);
            AddColumn("ReportJobTriggers", "ScheduleDayOfMonth", DbType.Int32);
            AddColumn("ReportJobTriggers", "ScheduleLastDayOfMonth", DbType.Boolean, 0, true, "0");
            AddColumn("ReportJobTriggers", "ScheduleDayOfWeek", DbType.Int32);
        }
    }
}