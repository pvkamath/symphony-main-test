﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration029 : Migration
    {
        public override void Up()
        {
            TableSchema.Table assetTypes = CreateTable("ArtisanAssetTypes");
            assetTypes.AddPrimaryKeyColumn("ID");
            assetTypes.AddColumn("Name", DbType.String, 50, false);
            assetTypes.AddColumn("Description", DbType.String, 255, false);
            assetTypes.AddColumn("Extensions", DbType.String, 255, false);
            assetTypes.AddColumn("Template", DbType.String, 9000, false);
            AddSubSonicStateColumns(assetTypes);
        }

        public override void Down()
        {
            DropTable("ArtisanAssetTypes");
        }
    }
}