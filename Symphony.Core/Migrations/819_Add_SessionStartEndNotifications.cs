﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration819 : Migration
    {

        public override void Up()
        {
            Execute(@"
                insert into ScheduleParameters
                (DisplayName, CanFilterComplete, CodeName, CreatedByUserId, ModifiedByUserId, ScheduleParameterOption, IsConfigurableForTrainingProgram)
                values
                ('Training Program - Session Start', 0, 'trainingprogramsessionstart', 0, 0, 1, 1),
                ('Training Program - Session End', 0, 'trainingprogramsessionend', 0, 0, 1, 1)
		    ");

            Execute(@"
                insert into RecipientGroups
                (Selector, DisplayName, ValidForType, CreatedByUserId, ModifiedByUserId)
                values
                ('GetAssignedUsers()','Assigned Users','TrainingProgramSession', 0, 0),
                ('GetAssignedUsersSupervisors()','Assigned Users Classroom Supervisors','TrainingProgramSession', 0, 0),
                ('GetAssignedUsersReportingSupervisors()','Assigned Users Reporting Supervisors','TrainingProgramSession', 0, 0)");
        }

    }
}