﻿using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration820 : Migration
    {
        public override void Up()
        {
            Execute(@"
    create procedure [dbo].[GetUsersForTrainingProgramSession]
	(
        @trainingProgramId int,
        @classId int, 
        @excludeComplete bit
	)
    as
    begin 
	    select
	        r.RegistrantID
        from
	        Registration r
        join
	        Class cl on cl.ID = r.ClassID
        join
	        TrainingProgram tp on tp.SessionCourseID = cl.CourseID
        left join
	        TrainingProgramRollup tpr on 
                tpr.TrainingProgramID = tp.ID and 
                tpr.UserID = r.RegistrantID	
        where
	        r.RegistrationStatusID = 4
	        and 
	        ClassID = @classId
	        and
	        tp.ID = @trainingProgramId
	        and
	        (@excludeComplete = 1 and (tpr.IsComplete != 1 or tpr.IsComplete is null) or @excludeComplete = 0)
    end
");
        }
    }
}