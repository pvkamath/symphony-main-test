﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration309: Migration
    {
        public override void Up()
        {
            TableSchema.Table affidavit = CreateTableWithKey("Affidavit", "ID");
            affidavit.AddColumn("UserID", DbType.Int32, 0, false);
            affidavit.AddColumn("QuestionType", DbType.String, 10, false);
            affidavit.AddColumn("Result", DbType.Boolean, 0, false);
            affidavit.AddColumn("CourseID", DbType.Int32, 0, false);
            affidavit.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            affidavit.AddColumn("Attempt", DbType.Int32, 0, false);
            AddSubSonicStateColumns(affidavit);
        }
    }
}