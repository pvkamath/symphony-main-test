﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration190 : Migration
    {
        public override void Up()
        {
            AddColumn("TrainingProgram", "DisableScheduled", DbType.Boolean, 0, false, "0");
        }
    }
}