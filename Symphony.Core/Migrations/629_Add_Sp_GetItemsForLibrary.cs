﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration629 : Migration
    {
        public override void Up()
        {
            Execute(@"
create procedure [dbo].[Library_GetItemsForLibrary]
	(
	@libraryId int,
	@userId int = NULL,
	@customerId int,
	@isShowAll bit,
	@isHideInLibrary bit,	 
    @categoryId int,
    @secondaryCategoryIds nvarchar(max),
    @secondaryCategorySearch nvarchar(max),
    @authorIds nvarchar(max),
    @authorSearch nvarchar(max),
	@search nvarchar(50), 
	@orderBy nvarchar(50), 
	@orderDir varchar(4), 
	@pageIndex int, 
	@pageSize int,
	@isFavorite bit,
	@isInProgress bit, 
	@isNotStarted bit,
	@startDate datetime = null,
	@endDate dateTime = null
	)
as
begin 
	declare @libraryItemTypeId int;
	
	if @isShowAll = 1
	begin 
		select @libraryItemTypeId = LibraryItemTypeID from Library where ID = @libraryId
	end

	;with Items as (
	select
		item.ID,
		item.LibraryItemID,
		item.LibraryID,
		item.Name, 
		item.[Description],
		item.CategoryID,
		item.Duration,
		item.LibraryItemTypeID,
		item.CreatedOn,
		item.CourseTypeID,
		item.CategoryName,
		item.LevelIndentText, 
		item.IsInLibrary,
		item.IsFavorite,
		item.IsInProgress,
		item.LibraryFavoriteID,
		item.r,
		item.ItemCreatedOn,
		item.ItemModifiedOn,
		item.CourseVersion
	from fGetItemsForLibrary_Search
	(
		@libraryId, 
		@libraryItemTypeId, 
		@customerID, 
		@isShowAll, 
		@isHideInLibrary, 
		@categoryId,
		@secondaryCategoryIds,
		@secondaryCategorySearch,
		@authorIds,
		@authorSearch, 
		@search, 
		@orderBy, 
		@orderDir,
		@isFavorite,
		@isInProgress, 
		@isNotStarted,
		@userId,
		@startDate,
		@endDate
	) item
)
select 
	I.*,
	(select max(r) from Items) as TotalRows
from Items I
where
	I.r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
end
");
        }
    }
}
