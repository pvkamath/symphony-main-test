﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration731 : Migration
    {
        public override void Up()
        {
            AddColumn("AffidavitFinalExam", "IsDeleted", DbType.Boolean, 0, false, "0"); 
        }
    }
}