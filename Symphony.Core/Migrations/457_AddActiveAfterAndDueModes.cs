﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration457 : Migration
    {

        public override void Up()
        {
            AddColumn("TrainingProgramToCourseLink", "ActiveAfterMode", DbType.Int32, 0, true);
            AddColumn("TrainingProgramToCourseLink", "DueMode", DbType.Int32, 0, true);
        }

    }
}