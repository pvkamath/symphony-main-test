﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration741 : Migration
    {

        public override void Up()
        {
            Execute(@"
                insert into ScheduleParameters
                (DisplayName, CanFilterComplete, CodeName, CreatedByUserId, ModifiedByUserId, ScheduleParameterOption)
                values
                ('Training Program - Percent Complete', 0, 'trainingprogrampercentcomplete', 0, 0, 2)
				
				insert into ScheduleParameters
                (DisplayName, CanFilterComplete, CodeName, CreatedByUserId, ModifiedByUserId, ScheduleParameterOption)
                values
                ('Training Program - Completion', 0, 'trainingprogramaftercompletion', 0, 0, 3)
				
				insert into ScheduleParameters
                (DisplayName, CanFilterComplete, CodeName, CreatedByUserId, ModifiedByUserId, ScheduleParameterOption)
                values
                ('Student - Last Logged In', 0, 'studentlastloggedin', 0, 0, 3)
				
				insert into ScheduleParameters
                (DisplayName, CanFilterComplete, CodeName, CreatedByUserId, ModifiedByUserId, ScheduleParameterOption)
                values
                ('Student - Last Course Activity', 0, 'studentlastcourseactivity', 0, 0, 3)");

            Execute(@"
                insert into RecipientGroups
                (Selector, DisplayName, ValidForType, CreatedByUserId, ModifiedByUserId)
                values
                ('UserID','User','TrainingProgramRollup', 0, 0)
				
				insert into RecipientGroups
                (Selector, DisplayName, ValidForType, CreatedByUserId, ModifiedByUserId)
                values
                ('ID', 'User', 'UserLastActivity', 0, 0)");
        }

    }
}