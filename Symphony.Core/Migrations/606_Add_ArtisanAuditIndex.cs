﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration606 : Migration
    {
        public override void Up()
        {

            Execute(@"
                CREATE NONCLUSTERED INDEX idx_audit ON ArtisanAudit
                (
                    OnlineCourseId ASC,
                    ArtisanCourseId ASC
                ) 
                CREATE NONCLUSTERED INDEX idx_audit_path ON ArtisanAudit
                (
                    OnlineCourseId ASC,
                    PackageGuid ASC
                )
");
            
        }
    }
}
