﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration296 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanSections", "IsRandomizeAnswers", DbType.Boolean, 0, false, "0");
            AddColumn("ArtisanSections", "IsReRandomizeOrder", DbType.Boolean, 0, false, "0");
            AddColumn("ArtisanCourses", "ReviewMethod", DbType.Int32, 0, false, "0");
        }
    }
}