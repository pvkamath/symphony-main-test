﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration435 : Migration
    {

        public override void Up()
        {
            AddColumn("LicenseDataFieldsToUsersMap", "UserValue", DbType.String, 1024, false, "''");
        }

    }
}