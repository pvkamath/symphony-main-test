﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration167 : Migration
    {
        public override void Up()
        {
            Execute(@"update ArtisanAssetTypes set Template = 
                '<img src=""{path}"" alt=""{description}"" title=""{name}"" style=""<tpl if=\""width == null\"">max-width:100%;</tpl><tpl if=\""width!=null\"">width:{width}px;</tpl><tpl if=\""height == null\"">max-height:100%;</tpl><tpl if=\""height!=null\"">height:{height}px;</tpl>"" />'
            where Name = 'Image'");
        }
    }
}