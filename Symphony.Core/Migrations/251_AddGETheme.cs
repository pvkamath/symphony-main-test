﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration251 : Migration
    {
        public override void Up()
        {
            Execute(@"
insert artisanthemes (name, description, csspath, customerid, folder, skinname)
values ('GE','GE','/skins/artisan_content/default/css/theme.css', 0, '/skins/artisan_content/default', 'ge')

                    ;");
        }
    }
}