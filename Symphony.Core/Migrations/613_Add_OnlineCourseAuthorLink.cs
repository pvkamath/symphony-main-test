﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration613 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.OnlineCourseAuthorLink
(
	Id INT IDENTITY(1, 1) NOT NULL,
	UserId INT NOT NULL,
	OnlineCourseId INT NOT NULL
)

ALTER TABLE dbo.OnlineCourseAuthorLink
ADD CONSTRAINT PK_OnlineCourseAuthorLink PRIMARY KEY (Id)

ALTER TABLE dbo.OnlineCourseAuthorLink
ADD CONSTRAINT FK_OnlineCourseAuthorLink_Users__Id FOREIGN KEY (UserId) REFERENCES dbo.[User](ID)

ALTER TABLE dbo.OnlineCourseAuthorLink
ADD CONSTRAINT FK_OnlineCourseAuthorLink_OnlineCourse__Id FOREIGN KEY (OnlineCourseId) REFERENCES dbo.OnlineCourse(ID)

");
        }
    }
}
