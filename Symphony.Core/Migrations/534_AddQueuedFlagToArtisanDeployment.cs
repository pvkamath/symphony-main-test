﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration534 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanDeployment", "Queued", DbType.Boolean, 0, false, "0");
            AddColumn("ArtisanDeployment", "PackageLocation", DbType.String, 256, true);
            AddColumn("ArtisanDeployment", "ServerRoot", DbType.String, 256, true);
        }
    }
}