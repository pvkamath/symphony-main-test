﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration620 : Migration
    {
        public override void Up()
        {
            TableSchema.Table tags = CreateTableWithKey("UserMetaData", "ID");
            tags.AddColumn("UserID", DbType.Int32, 0, false);
            tags.AddColumn("Key", DbType.String, 64, false);
            tags.AddColumn("KeyCode", DbType.String, 64, false);
            tags.AddLongText("Value", false, "''");
            AddSubSonicStateColumns(tags);
        }
    }
}
