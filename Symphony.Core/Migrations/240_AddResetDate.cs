﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration240 : Migration
    {
        public override void Up()
        {
            AddColumn("OnlineCourseRollup", "LastResetDate", DbType.DateTime, 0, true);
        }
    }
}