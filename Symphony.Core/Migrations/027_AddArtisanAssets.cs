﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration027 : Migration
    {
        public override void Up()
        {
            TableSchema.Table assets = CreateTable("ArtisanAssets");
            assets.AddPrimaryKeyColumn("ID");
            assets.AddColumn("CustomerID", DbType.Int32, 0, false);
            assets.AddColumn("Name", DbType.String, 50, false);
            assets.AddColumn("Description", DbType.String, 255, false);
            assets.AddColumn("Path", DbType.String, 255, false);
            assets.AddColumn("AssetTypeID", DbType.Int32, 0, false);
            AddSubSonicStateColumns(assets);
        }

        public override void Down()
        {
            DropTable("ArtisanAssets");
        }
    }
}