﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration230 : Migration
    {

        public override void Up()
        {
            AddColumn("OnlineCourseRollup", "ResetNotes", DbType.String, 2000, false, "''");
        }

        public override void Down()
        {
            
        }
    }
}