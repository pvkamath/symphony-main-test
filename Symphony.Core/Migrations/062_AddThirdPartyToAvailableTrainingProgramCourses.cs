﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration062 : Migration
    {
        public override void Up()
        {
            StringBuilder sb = new StringBuilder(1035);
            sb.AppendFormat(@"ALTER view [dbo].[AvailableTrainingProgramCourses]{0}", Environment.NewLine);
            sb.AppendFormat(@"as{0}", Environment.NewLine);
            sb.AppendFormat(@"({0}", Environment.NewLine);
            sb.AppendFormat(@"{0}SELECT {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}oc.ID, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}CustomerID, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}oc.[Name],{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}oc.[Description], {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Cost AS Fee, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Credit, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}PublicIndicator, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}ct.ID AS CourseTypeID,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}oc.ModifiedBy, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}oc.CreatedBy, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}oc.ModifiedOn, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}oc.CreatedOn,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}cast(0 as bit) as IsThirdParty,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}cast(oc.ID as varchar(20)) + '-' + cast(ct.ID as varchar(20)) as [Key]{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}FROM dbo.OnlineCourse as oc JOIN CourseType as ct ON ct.CodeName = 'online'{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}-- don't allow surveys to be directly assigned{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}WHERE IsSurvey = 0{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}UNION ALL{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}SELECT {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.ID, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}CustomerID, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.Name, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.[Description], {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}PerStudentFee AS Fee, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}Credit, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}PublicIndicator,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}ct.ID AS CourseTypeID, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.ModifiedBy, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.CreatedBy, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.ModifiedOn, {1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.CreatedOn,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}c.IsThirdParty,{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}{0}cast(c.ID as varchar(20)) + '-' + cast(ct.ID as varchar(20)) as [Key]{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}FROM dbo.Course as c JOIN CourseType as ct ON ct.CodeName = 'classroom'{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}-- don't allow public classroom courses to be directly assigned{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@"{0}WHERE PublicIndicator = 0{1}", "\t", Environment.NewLine);
            sb.AppendFormat(@");");

            Execute(sb.ToString());
        }
    }
}