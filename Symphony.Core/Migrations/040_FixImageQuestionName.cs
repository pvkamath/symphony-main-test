﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration040 : Migration
    {
        public override void Up()
        {
            Execute("update artisanquestiontypes set name = 'ImageQuestion' where name = 'Image'");
        }
    }
}