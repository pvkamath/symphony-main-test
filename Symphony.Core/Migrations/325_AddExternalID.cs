﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration325: Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanAnswers", "ExternalID", DbType.String, 128, true);
            AddColumn("ArtisanAssets", "ExternalID", DbType.String, 128, true);
            AddColumn("ArtisanAssetTypes", "ExternalID", DbType.String, 128, true);
            AddColumn("ArtisanCourses", "ExternalID", DbType.String, 128, true);
            AddColumn("ArtisanDeployment", "ExternalID", DbType.String, 128, true);
            AddColumn("ArtisanPages", "ExternalID", DbType.String, 128, true);
            AddColumn("ArtisanQuestionTypes", "ExternalID", DbType.String, 128, true);
            AddColumn("ArtisanSectionPages", "ExternalID", DbType.String, 128, true);
            AddColumn("ArtisanSections", "ExternalID", DbType.String, 128, true);
            AddColumn("ArtisanTemplates", "ExternalID", DbType.String, 128, true);
            AddColumn("ArtisanThemes", "ExternalID", DbType.String, 128, true);
        }
    }
}