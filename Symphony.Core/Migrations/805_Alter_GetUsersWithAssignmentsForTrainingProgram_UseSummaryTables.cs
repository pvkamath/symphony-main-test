﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration805 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER procedure [dbo].[GetUsersWithAssignmentsForTrainingProgram]
	(@trainingProgramId int, @sessionId int, @search nvarchar(50), @orderBy nvarchar(50), @orderDir varchar(4), @pageIndex int, @pageSize int)
as
begin
	declare @totalAssignments int 
	set @totalAssignments = 0;

	declare @totalMarkedTable table
	(
		TotalMarked int,
		UserID int
	)

	insert into @totalMarkedTable
	select
		COUNT(distinct q.ArtisanSectionID),	
		oa.UserID
	from OnlineCourseAssignments oa
    join AssignmentSummaryQuestions q on q.ArtisanPageID = oa.PageID
	where oa.TrainingProgramID in (
		select ID from TrainingProgram tp where
		tp.ID = @trainingProgramId or tp.ParentTrainingProgramID = @trainingProgramId
	) and oa.ResponseStatus > 1
	group by oa.UserID

	select 
		@totalAssignments = SUM(TotalAssignments)
	from TrainingProgramToCourseLink tpl
	join OnlineCourse o on tpl.CourseID = o.ID
	join (
	    select 
		    a.ArtisanCourseID as ArtisanCourseID, 
		    count(distinct a.ArtisanSectionID) as TotalAssignments
	    from 
		    AssignmentSummary a
	    group by 
		    a.ArtisanCourseID
	) t on t.ArtisanCourseID = o.PublishedArtisanCourseID
	where tpl.TrainingProgramID = @trainingProgramId
	and tpl.CourseTypeID = 2

	;with UserAssignments as (select
		0 as AssignmentsAttempted, --Not needed
		tm.TotalMarked as AssignmentsMarked,-- number of assignments marked
		@totalAssignments as AssignmentsInCourse, -- Number of assignments in training program
		0 as AssignmentsCompleted, -- Not needed
		0 as IsAssignmentsFailed, -- Not needed
		0 as IsAssignmentsComplete, -- Not Needed
		MAX(o.GradedDate) as MaxGradedDate,
		MAX(o.[Date]) as MaxSubmitDate,
		MIN(o.ResponseStatus) as IsMarked,
		u.ID,
		u.FirstName,
		u.LastName,
		u.MiddleName,
		r.ClassID as ClassID,
		r.ClassName as ClassName,
		cmm.MinStartDateTime as ClassStartTime,
		row_number() over 
		(
			order by max(cmm.MinStartDateTime) desc, u.LastName, u.FirstName asc	
		)  as r
	from OnlineCourseAssignments o
	join TrainingProgram tp on o.TrainingProgramID = tp.ID
	left join (
		select
			cl.ID as ClassID,
			r.RegistrantID,
			cl.CourseID,
			cl.Name as ClassName,
			r.RegistrationStatusID
		from
			Class cl 
		join
			Registration r
			on
			r.ClassID = cl.ID
		where
			cl.CourseID = @sessionId
	) r on r.RegistrantID = o.UserID and r.CourseID = @sessionId
	left join ClassMinStartDateTime cmm on r.ClassID = cmm.ClassID
	join [User] u on u.ID = o.UserID
	left join @totalMarkedTable tm on tm.UserID = u.ID
	where 
		(tp.ID = @trainingProgramId or tp.ParentTrainingProgramID = @trainingProgramId)
		and ( cmm.MinStartDateTime < DATEADD(day, 1, GETUTCDATE()) or cmm.MinStartDateTime is null)
		and (r.RegistrationStatusID = 4 or r.RegistrationStatusID is null)
		and (u.FirstName like '%' + @search + '%' or u.LastName like '%' + @search + '%' or r.ClassName like '%' + @search + '%')
	group by
		u.ID,
		u.FirstName,
		u.LastName,
		u.MiddleName,
		r.ClassID,
		r.ClassName,
		cmm.MinStartDateTime,
		tm.TotalMarked
	)

	select 
		AssignmentsAttempted, --Not needed
		AssignmentsMarked,-- number of assignments marked
		AssignmentsInCourse, -- Number of assignments in training program
		AssignmentsCompleted, -- Not needed
		IsAssignmentsFailed, -- Not needed
		IsAssignmentsComplete, -- Not Needed
		MaxSubmitDate,
		MaxGradedDate,
		IsMarked,
		ID,
		FirstName,
		LastName,
		MiddleName,
		ClassID,
		ClassName,
		ClassStartTime,
		r,
		(select count(*) from UserAssignments) as TotalRows
	from
		UserAssignments
	where 
		r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
	order by r asc
		
end
");
        }
    }
}
