﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration208 : Migration
    {

        public override void Up()
        {
            Execute(@"CREATE PROCEDURE [dbo].[DeleteTile] (@tileId int) AS
BEGIN
DELETE FROM [dbo].[HierarchyToTileLinks] 
      WHERE HierarchyToTileLinks.TileID = @tileId
DELETE FROM [dbo].[Tiles]
	WHERE Tiles.id = @tileId

END");
        }

        public override void Down()
        {
            Execute(@"DROP PROCEDURE [dbo].[DeleteTile]");
        }
    }
}