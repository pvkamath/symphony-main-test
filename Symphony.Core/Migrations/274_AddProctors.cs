﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration274 : Migration
    {
        public override void Up()
        {
              TableSchema.Table proctors = CreateTable("Proctor");
              proctors.AddPrimaryKeyColumn("ID");
              proctors.AddColumn("UserID", DbType.Int32, 0, false);
              proctors.AddColumn("OnlineCourseID", DbType.Int32, 0, false);
              proctors.AddColumn("TrainingProgramID", DbType.Int32, 0, true);
              proctors.AddColumn("Name", DbType.String, 80, false);
              proctors.AddColumn("Location", DbType.String, 256, false);
              proctors.AddColumn("ProctorCode", DbType.String, 80, false);
                
              AddSubSonicStateColumns(proctors);
        }
    }
}