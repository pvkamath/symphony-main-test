﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration841 : Migration
    {
        public override void Up()
        {
            Execute(@"
                CREATE VIEW [dbo].[CertificateTemplateHierarchy] AS
                select * from dbo.fGetCertificateTemplateHierarchyBranch(0)"
            );
        }
    }
}

