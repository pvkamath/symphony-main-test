﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration808 : Migration
    {
        public override void Up()
        {
            AddColumn("Themes", "IsDeleted", DbType.Boolean, 0, false, "0");
        }
    }
}