﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration873 : Migration
    {

        public override void Up()
        {
            Execute(@"
ALTER procedure [dbo].[UpdateScormActivity]
(	
    @registrationId int,
    @bookmark ntext,
    @datachunk ntext
)
as
    update OnlineTraining.dbo.ScormActivityRT
    set
	    location = @bookmark,
	    suspend_data = @datachunk
    where
	    scorm_activity_id in
	    (
		    select sa.scorm_activity_id 
		    from OnlineTraining.dbo.ScormActivity sa
		    where sa.scorm_registration_id = @registrationId
	    )
;
                ");
        }
    }
}
