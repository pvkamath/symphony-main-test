﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration746 : Migration
    {

        public override void Up()
        {
            AddColumn("Templates", "IsConfigurableForTrainingProgram", DbType.Boolean, 0, false, "0");
            AddColumn("ScheduleParameters", "IsConfigurableForTrainingProgram", DbType.Boolean, 0, false, "0");
        }

    }
}