using SubSonic;
using System.Data;

namespace Symphony.Core.Migrations
{
    public class Migration888 : Migration
    {
        public override void Up()
        {
            Execute(@"
IF EXISTS(SELECT * FROM dbo.syscolumns WITH (nolock) WHERE [name] = 'DF_TrainingProgram_Goal' AND id = (SELECT [id] FROM dbo.sysobjects WITH (nolock) WHERE [name] = 'TrainingProgram'))
BEGIN
    ALTER TABLE dbo.TrainingProgram DROP CONSTRAINT DF_TrainingProgram_Goal;
END
IF EXISTS(SELECT * FROM dbo.syscolumns WITH (nolock) WHERE [name] = 'Goal' AND id = (SELECT [id] FROM dbo.sysobjects WITH (nolock) WHERE [name] = 'TrainingProgram'))
BEGIN
	ALTER TABLE dbo.TrainingProgram DROP COLUMN Goal;
END
");

            Execute(@"
IF EXISTS(SELECT * FROM dbo.syscolumns WITH (nolock) WHERE [name] = 'DF_TrainingProgram_Objective' AND id = (SELECT [id] FROM dbo.sysobjects WITH (nolock) WHERE [name] = 'TrainingProgram'))
BEGIN
    ALTER TABLE dbo.TrainingProgram DROP CONSTRAINT DF_TrainingProgram_Objective;
END
IF EXISTS(SELECT * FROM dbo.syscolumns WITH (nolock) WHERE [name] = 'Objective' AND id = (SELECT [id] FROM dbo.sysobjects WITH (nolock) WHERE [name] = 'TrainingProgram'))
BEGIN
    ALTER TABLE dbo.TrainingProgram DROP COLUMN Objective;
END
");
            Execute(@"ALTER TABLE [dbo].[TrainingProgram] ADD GoalsAndObjectives NVARCHAR(MAX)");

        }
    }
}
