﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration517 : Migration
    {

        public override void Up()
        {
            Execute(@"
    IF NOT EXISTS (
      SELECT * 
      FROM   sys.columns 
      WHERE  object_id = OBJECT_ID(N'[dbo].[ReportQueueEntries]') 
             AND name = 'QueueID'
    )
    BEGIN
        ALTER TABLE ReportQueueEntries
        ADD QueueID int not null default 0
    END
");
        }

    }
}