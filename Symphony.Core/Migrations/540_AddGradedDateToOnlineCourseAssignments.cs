﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration540 : Migration
    {
        public override void Up()
        {
            AddColumn("OnlineCourseAssignments", "GradedDate", DbType.DateTime, 0, true);
        }
    }
}