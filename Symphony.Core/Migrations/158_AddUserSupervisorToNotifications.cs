﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration158 : Migration
    {
        public override void Up()
        {
            Execute(@"
update RecipientGroups set DisplayName = 'Classroom Supervisor' where selector = 'SupervisorID'
insert RecipientGroups (Selector, DisplayName, ValidForType) values ('ReportingSupervisorID','Reporting Supervisor', 'User')
");
        }
    }
}