﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration461 : Migration
    {

        public override void Up()
        {
            Execute("update Timezone set Description = 'Alaskan' where Description = 'Alaska'");
            Execute("update Timezone set Description = 'Hawaiian' where Description = 'Hawaii'");
        }

    }
}