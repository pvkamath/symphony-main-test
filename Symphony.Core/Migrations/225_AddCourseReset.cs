﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration225 : Migration
    {

        public override void Up()
        {
            Execute(@"
IF not EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'CourseReset')
                    AND type IN ( N'P', N'PC' ) ) 
   exec('CREATE PROCEDURE [dbo].[CourseReset]
	(	@userID int = 12847,
		@scormRegistrationID int = 125329
	)
	as
	declare @retCode int
	set @retCode = 0

	IF EXISTS (select * from onlineTraining.dbo.ScormRegistration	
		where user_id = @userID
			and scorm_registration_id = @scormRegistrationID)
	BEGIN
	update onlineTraining.dbo.ScormRegistration
		set course_id = 0
		where user_id = @userID
			and scorm_registration_id = @scormRegistrationID
	END')
");
        }

        public override void Down()
        {
            
        }
    }
}