﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration689 : Migration
    {

        public override void Up()
        {
            Execute(@"
                IF (NOT EXISTS(SELECT * FROM Templates WHERE CodeName = 'SalesforceNewUser'))
                BEGIN
                    INSERT INTO dbo.Templates (
                        Subject, 
                        Body, 
                        CodeName, 
                        DisplayName, 
                        Description, 
                        Priority, 
                        CustomerID, 
                        Enabled, 
                        ModifiedBy, 
                        CreatedBy, 
                        ModifiedOn, 
                        CreatedOn, 
                        Area, 
                        IsScheduled, 
                        ScheduleParameterID, 
                        RelativeScheduledMinutes, 
                        FilterComplete, 
                        OwnerID, 
                        CcClassroomSupervisors, 
                        CcReportingSupervisors, 
                        CreatedByUserId, 
                        ModifiedByUserId, 
                        CreatedByActualUserId, 
                        ModifiedByActualUserId, 
                        IncludeCalendarInvite
                    ) VALUES (
                        'Welcome to {!Customer.Name}',
                        'You have a new user account created in {!Customer.Name}',
                        'SalesforceNewUser',
                        'Salesforce - New User',
                        'When a new user is registered in Symphony through the Salesforce API',
                        1,
                        0, 
                        0, 
                        'admin',
                        'admin',
                        GETUTCDATE(),
                        GETUTCDATE(),
                        '',
                        0, 
                        0,
                        0,
                        0, 
                        0, 
                        0,
                        0, 
                        0, 
                        0,
                        null,
                        null,
                        0 
                    );
    
                    INSERT INTO dbo.Templates (Subject, Body, CodeName, DisplayName, Description, Priority, CustomerID, Enabled, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, RelativeScheduledMinutes, FilterComplete, OwnerID, CcClassroomSupervisors, CcReportingSupervisors, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, IncludeCalendarInvite)
                    SELECT
	                    tp.Subject,
	                    tp.Body,
	                    tp.CodeName,
	                    tp.DisplayName,
	                    tp.Description,
	                    tp.Priority,
	                    c.Id,
	                    tp.Enabled,
	                    tp.ModifiedBy,
	                    tp.CreatedBy,
	                    tp.ModifiedOn,
	                    tp.CreatedOn,
	                    tp.Area,
	                    tp.IsScheduled,
	                    tp.ScheduleParameterID,
	                    tp.RelativeScheduledMinutes,
	                    tp.FilterComplete,
	                    tp.OwnerID,
	                    tp.CcClassroomSupervisors,
	                    tp.CcReportingSupervisors,
	                    tp.CreatedByUserId,
	                    tp.ModifiedByUserId,
	                    tp.CreatedByActualUserId,
	                    tp.ModifiedByActualUserId,
	                    tp.IncludeCalendarInvite
                    FROM
	                    Customer c
	                    INNER JOIN dbo.Templates tp ON tp.CustomerID = 0 AND tp.CodeName = 'SalesforceNewUser'

                END");

            Execute(@"
                IF (NOT EXISTS(SELECT * FROM Templates WHERE CodeName = 'SalesforceNewEnrollment'))
                BEGIN
                    INSERT INTO dbo.Templates (
                        Subject, 
                        Body, 
                        CodeName, 
                        DisplayName, 
                        Description, 
                        Priority, 
                        CustomerID, 
                        Enabled, 
                        ModifiedBy, 
                        CreatedBy, 
                        ModifiedOn, 
                        CreatedOn, 
                        Area, 
                        IsScheduled, 
                        ScheduleParameterID, 
                        RelativeScheduledMinutes, 
                        FilterComplete, 
                        OwnerID, 
                        CcClassroomSupervisors, 
                        CcReportingSupervisors, 
                        CreatedByUserId, 
                        ModifiedByUserId, 
                        CreatedByActualUserId, 
                        ModifiedByActualUserId, 
                        IncludeCalendarInvite
                    ) VALUES (
                        'Thank you for your order!',
                        'You have been in enrolled in a new course.',
                        'SalesforceNewEnrollment',
                        'Salesforce - New Enrollment',
                        'When a new user is registered in a training program through the Salesforce API',
                        1,
                        0, 
                        0, 
                        'admin',
                        'admin',
                        GETUTCDATE(),
                        GETUTCDATE(),
                        '',
                        0, 
                        0,
                        0,
                        0, 
                        0, 
                        0,
                        0, 
                        0, 
                        0,
                        null,
                        null,
                        0 
                    );
    
                    INSERT INTO dbo.Templates (Subject, Body, CodeName, DisplayName, Description, Priority, CustomerID, Enabled, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, RelativeScheduledMinutes, FilterComplete, OwnerID, CcClassroomSupervisors, CcReportingSupervisors, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, IncludeCalendarInvite)
                    SELECT
	                    tp.Subject,
	                    tp.Body,
	                    tp.CodeName,
	                    tp.DisplayName,
	                    tp.Description,
	                    tp.Priority,
	                    c.Id,
	                    tp.Enabled,
	                    tp.ModifiedBy,
	                    tp.CreatedBy,
	                    tp.ModifiedOn,
	                    tp.CreatedOn,
	                    tp.Area,
	                    tp.IsScheduled,
	                    tp.ScheduleParameterID,
	                    tp.RelativeScheduledMinutes,
	                    tp.FilterComplete,
	                    tp.OwnerID,
	                    tp.CcClassroomSupervisors,
	                    tp.CcReportingSupervisors,
	                    tp.CreatedByUserId,
	                    tp.ModifiedByUserId,
	                    tp.CreatedByActualUserId,
	                    tp.ModifiedByActualUserId,
	                    tp.IncludeCalendarInvite
                    FROM
	                    Customer c
	                    INNER JOIN dbo.Templates tp ON tp.CustomerID = 0 AND tp.CodeName = 'SalesforceNewEnrollment'

                END");
        }

    }
}
