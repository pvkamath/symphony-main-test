﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration470 : Migration
    {

        public override void Up()
        {
            AddColumn("Notifications", "Status", DbType.Int32, 0, false, "1");
            AddColumn("Notifications", "SendDate", DbType.DateTime, 0, true);
            Execute(@"Alter Table Notifications Add CCEmails nvarchar(max)");
            Execute(@"Alter Table Notifications Add ICal nvarchar(max)");
        }

    }
}