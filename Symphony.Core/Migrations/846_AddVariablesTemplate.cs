﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration846 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanThemes", "ScssVariablesTemplatePath", DbType.String, 256, true);
        }
    }
}
