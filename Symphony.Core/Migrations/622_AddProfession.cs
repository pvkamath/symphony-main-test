﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration622 : Migration
    {
        public override void Up()
        {
            AddColumn("User", "PrimaryProfessionID", DbType.Int32, 0, true);
            AddColumn("User", "SecondaryProfessionID", DbType.Int32, 0, true);
        }
    }
}
