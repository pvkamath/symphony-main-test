﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration512 : Migration
    {

        public override void Up()
        {
            TableSchema.Table reportEntity = CreateTableWithKey("ReportEntity", "ID");
            reportEntity.AddColumn("Name", DbType.String, 64, false);
            reportEntity.AddColumn("Xtype", DbType.String, 128, false);
            reportEntity.AddLongText("Config", true, "");

            TableSchema.Table reportTemplateEntity = CreateTableWithKey("ReportTemplateEntity", "ID");
            reportTemplateEntity.AddColumn("ReportTemplateID", DbType.Int32, 0, false);
            reportTemplateEntity.AddColumn("ReportEntityID", DbType.Int32, 0, false);

            TableSchema.Table reportTemplate = CreateTableWithKey("ReportTemplate", "ID");
            reportTemplate.AddColumn("Name", DbType.String, 64, false);
            reportTemplate.AddColumn("CodeName", DbType.String, 64, false);
            reportTemplate.AddLongText("Description", true, "");
            reportTemplate.AddColumn("IsEnabled", DbType.Boolean, 0, false);
            AddSubSonicStateColumns(reportTemplate);

            AddColumn("Reports", "ReportTemplateID", DbType.Int32, 0, false, "0");
            Execute(@"ALTER TABLE [dbo].[Reports] 
                        ADD XMLParameters nvarchar(max) DEFAULT '' NOT NULL");

            Execute(@"ALTER TABLE [dbo].[Reports] WITH NOCHECK 
                        ADD CONSTRAINT [FK_Report_ReportTemplate] FOREIGN KEY([ReportTemplateID])
                        REFERENCES [dbo].[ReportTemplate] ([ID])
                        NOT FOR REPLICATION");

        }

    }
}
