﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration466 : Migration
    {

        public override void Up()
        {
            Execute(@"IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Notifications]') AND name = N'body_index')
DROP INDEX [body_index] ON [dbo].[Notifications] WITH ( ONLINE = OFF )");


            Execute(@"ALTER TABLE Notifications ALTER COLUMN Subject varchar(2000)");

            Execute(@"CREATE NONCLUSTERED INDEX [body_index] ON [dbo].[Notifications] 
(
	[Subject] ASC
)
INCLUDE ( [Body]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
");
        }

    }
}