﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration135 : Migration
    {
        public override void Up()
        {
            Execute(@"
insert artisanthemes (name, description, csspath, createdon, modifiedon, createdby, modifiedby, customerid, folder, skincsspath)
values ('Aslan', 'Aslan Theme', '/skins/artisan_content/aslan/css/main.css', getutcdate(), getutcdate(), 'system','system',1,'/skins/artisan_content/aslan/css','')
");
        }
    }
}