﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration375: Migration
    {
        public override void Up()
        {
            // Switches SupervisorID to GetSupervisors for getting the supervisor of a user
            Execute(@"update [RecipientGroups] set [Selector] = 'GetSupervisors()' where [Selector] = 'SupervisorID' and [ValidForType] = 'User'");
        }
    }
}