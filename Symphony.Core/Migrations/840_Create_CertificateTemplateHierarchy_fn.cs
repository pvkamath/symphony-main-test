﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration840 : Migration
    {
        public override void Up()
        {
            Execute(@"
                CREATE FUNCTION [dbo].[fGetCertificateTemplateHierarchyBranch]
                (   
                    --the node that'll be considered the root node of the branch
                    @iCertificateTemplateID int
                )
                RETURNS TABLE
                AS
                RETURN
                (
                WITH CertificateTemplate_CTE (ID, ParentId, Name, CustomerID, Level, LevelIndentText, LevelText, RootCertificateTemplateID)
                AS
                (
                -- Anchor member definition
                    SELECT
                            l.ID,
                            l.ParentID,
                            l.Name,
                            l.CustomerID,
                            0 AS Level,
                            CAST(l.Name as nvarchar(max)) as LevelIndentText,
                            CAST(l.Name as nvarchar(max)) as LevelText,
                            l.ID as RootCertificateTemplateID
                        FROM
                            CertificateTemplate l
                      WHERE
                                  --Note by Jerod
                            --Added a case in here...this way, passing '0' will get the full tree, while passing
                            --the ID of any node will get that node's branch.
                            --Note that this means if you have 1 root node in the tree, passing that node id or passing 0 will give the same result set
                            1 = case when @iCertificateTemplateID = 0 then
                                    case --allow 0 or nulls as root nodes
                                        when ParentID IS NULL then 1
                                        when ParentID = 0 then 1
                                        else 0
                                    end
                                else
                                    case
                                        when ID = @iCertificateTemplateID then 1
                                        else 0
                                    end
                                end
                    UNION ALL
                -- Recursive member definition
                    SELECT
                            l.ID,
                            l.ParentID,
                            l.Name,
                            l.CustomerID,           
                            Level + 1,
                            CAST(LevelIndentText + ' > ' + l.Name as nvarchar(max)),
                            CAST(l.Name as nvarchar(max)) as LevelText,
                            RootCertificateTemplateID
                    FROM
                            CertificateTemplate l
                    INNER JOIN CertificateTemplate_CTE lc ON
                            lc.ID = l.ParentID
                )
                SELECT
                      a.Level,
                      a.LevelText,
                      a.LevelIndentText,
                      a.RootCertificateTemplateID,
                      b.*
                FROM
                      CertificateTemplate_CTE a
                JOIN
                      CertificateTemplate b
                ON
                      a.ID = b.ID

                )"
            );
        }
    }
}

