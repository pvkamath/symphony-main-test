﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration146 : Migration
    {
        public override void Up()
        {
            Execute("alter table OnlineCourse add ShowPretestOverride bit");
            Execute("alter table OnlineCourse add ShowPostTestOverride bit");
            Execute("alter table OnlineCourse add PassingScoreOverride int");
            Execute("alter table OnlineCourse add SkinOverride int");
            Execute("alter table OnlineCourse add PreTestTypeOverride int");
            Execute("alter table OnlineCourse add PostTestTypeOverride int");
        }
    }
}