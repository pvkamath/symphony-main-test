﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration654 : Migration
    {

        public override void Up()
        {
            TableSchema.Table trainingProgramRollup = CreateTableWithKey("TrainingProgramRollup", "ID");
            trainingProgramRollup.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            trainingProgramRollup.AddColumn("UserID", DbType.Int32, 0, false);
            trainingProgramRollup.AddColumn("IsCompleteOverride", DbType.Boolean, 0, false, "0");
            trainingProgramRollup.AddColumn("DateCompleted", DbType.DateTime, 0, true);
            trainingProgramRollup.AddColumn("IsSurveyComplete", DbType.Boolean, 0, false, "0");
            trainingProgramRollup.AddColumn("RequiredCompletedCount", DbType.Int32, 0, false, "0");
            trainingProgramRollup.AddColumn("ElectiveCompletedCount", DbType.Int32, 0, false, "0");
            trainingProgramRollup.AddColumn("FinalCompletedCount", DbType.Int32, 0, false, "0");
            trainingProgramRollup.AddColumn("AssignmentsCompletedCount", DbType.Int32, 0, false, "0");
        }

    }
}
