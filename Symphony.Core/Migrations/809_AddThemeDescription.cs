﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration809 : Migration
    {
        public override void Up()
        {
            AddColumn("Themes", "Description", DbType.String, 1024, false, "''");
        }
    }
}