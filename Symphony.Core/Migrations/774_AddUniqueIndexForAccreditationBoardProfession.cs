﻿using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration774 : Migration
    {
        public override void Up()
        {
            Execute("CREATE UNIQUE INDEX ux_AccreditationBoardProfession_AccreditationBoardID_ProfessionID "
                + " ON AccreditationBoardProfession(AccreditationBoardID, ProfessionID);");
        }
    }
}
