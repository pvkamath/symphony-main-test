﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration165 : Migration
    {
        public override void Up()
        {
            Execute(@"update ArtisanAssetTypes set Template = 
                '<img src=""{path}"" alt=""{description}"" title=""{name}"" style=""<tpl if=\""width == null\"">max-width:100%</tpl><tpl if=\""width!=null\"">width:{width}px</tpl>;<tpl if=\""height == null\"">max-height:100%</tpl><tpl if=\""height!=null\"">height:{height}px</tpl>"" />'
            where Name = 'Image'");

            Execute(@"insert ArtisanAssetTypes (name, description, extensions, template, previewtemplate) values ('Video','MP4 Video','mp4','<div class=""video-container"" style=""width:500px;height:500px""><video id=""video-{id}"" class=""video-js vjs-default-skin"" controls preload=""auto"" width=""500"" height=""500"" data-setup=""{}""><source src=""{path}"" type=""video/mp4""></video></div>','<img style=""width:60px"" src=""/images/video-play.png"" />');");

            Execute(@"update ArtisanAssetTypes set Extensions = 'mp3|wav' where Name='audio'");
        }
    }
}