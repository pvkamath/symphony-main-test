﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration480 : Migration
    {

        public override void Up()
        {
            Execute(@"
     UPDATE Templates SET Body = 'Hi {!User.Firstname},

Your assignments for ""{!TrainingProgram.Name}"" are complete. You may now take the final exam.',
                        Subject = 'Assignments for ""{!TrainingProgram.Name}"" are complete!'
where Templates.CodeName = 'AssignmentsGradedPassed'");

            Execute(@"
    UPDATE Templates SET Body = 'Hi {!User.FirstName},
Your assignments for ""{!TrainingProgram.Name}"" have been marked, but you have not satisfied the requirements. Please log in to Symphony to review your assignments and try again. The final exam will not be available until you complete your assignments.',
                        Subject = 'Assignments for ""{!TrainingProgram.Name}"" are incomplete.'
where Templates.CodeName = 'AssignmentsGradedFailed'");
        }

    }
}
