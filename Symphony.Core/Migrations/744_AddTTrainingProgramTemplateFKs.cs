﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration744 : Migration
    {

        public override void Up()
        {
            Execute(@"
                alter table TrainingProgramTemplates
                    add constraint fk_TrainingProgram_ID_TrainingProgramTemplate_TrainingProgramID foreign key(TrainingProgramID)
                    references TrainingProgram(ID)
            ");

            Execute(@"
                alter table TrainingProgramTemplates
                    add constraint fk_Templates_ID_TrainingProgramTemplate_TemplateID foreign key(TemplateID)
                    references Templates(ID)
            ");
        }

    }
}