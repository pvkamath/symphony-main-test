﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration486 : Migration
    {

        public override void Up()
        {
            Execute(@"
    insert into ExternalSystem (SystemName, SystemCodeName, LoginUrl)
    values ('Symphony', 'symphony', 'http://symphony.oncourselearning.com')
");
        }

    }
}