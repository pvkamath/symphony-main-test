﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration383 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanPages", "MinimumPageTime", DbType.Int32, 0, true);
            AddColumn("ArtisanPages", "MaximumQuestionTime", DbType.Int32, 0, true);
        }
    }
}