﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration277 : Migration
    {
        public override void Up()
        {
            TableSchema.Table parameter = CreateTable("Parameter");
            parameter.AddPrimaryKeyColumn("ID");
            parameter.AddColumn("ParameterSetID", DbType.Int32, 0, false);
            parameter.AddColumn("Name", DbType.String, 255, false);
            parameter.AddColumn("Code", DbType.String, 255, false);
            parameter.AddLongText("Description", false, "''");
            parameter.AddLongText("DefaultValue", false, "''");

            AddSubSonicStateColumns(parameter);

            TableSchema.Table parameterValue = CreateTable("ParameterValue");
            parameterValue.AddPrimaryKeyColumn("ID");
            parameterValue.AddColumn("ParameterID", DbType.Int32, 0, false);
            parameterValue.AddColumn("ParameterSetOptionID", DbType.Int32, 0, false);
            parameterValue.AddLongText("Value", false, "''");

            AddSubSonicStateColumns(parameterValue);

            TableSchema.Table parameterSet = CreateTable("ParameterSet");
            parameterSet.AddPrimaryKeyColumn("ID");
            parameterSet.AddColumn("Name", DbType.String, 255, false);
            parameterSet.AddLongText("Description", false, "''");

            AddSubSonicStateColumns(parameterSet);

            
            TableSchema.Table parameterSetOption = CreateTable("ParameterSetOption");
            parameterSetOption.AddPrimaryKeyColumn("ID");
            parameterSetOption.AddColumn("ParameterSetID", DbType.Int32, 0, false);
            parameterSetOption.AddColumn("Value", DbType.String, 512, false);

            AddSubSonicStateColumns(parameterSetOption);

            TableSchema.Table trainingProgramOverride = CreateTable("TrainingProgramParameterOverride");
            trainingProgramOverride.AddPrimaryKeyColumn("ID");
            trainingProgramOverride.AddColumn("TrainingProgramID", DbType.Int32, 0, false);
            trainingProgramOverride.AddColumn("ParameterSetOptionID", DbType.Int32, 0, false);

            AddSubSonicStateColumns(trainingProgramOverride);

            Execute("ALTER TABLE [dbo].[ArtisanCourses] ADD [Parameters] nvarchar(MAX) DEFAULT '' NOT NULL"); 

        }
    }
}