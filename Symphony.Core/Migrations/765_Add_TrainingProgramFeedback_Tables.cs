﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration765 : Migration
    {

        public override void Up()
        {
            // Add tables for Training Program Feedback
            TableSchema.Table TrainingProgramFeedback = CreateTableWithKey("TrainingProgramFeedback", "ID");
            TrainingProgramFeedback.AddColumn("TrainingProgramID", DbType.Int32);
            TrainingProgramFeedback.AddColumn("UserID", DbType.Int32);
            TrainingProgramFeedback.AddColumn("Score", DbType.Int32);
            TrainingProgramFeedback.AddColumn("Comment", DbType.String);
            TrainingProgramFeedback.AddColumn("Recommended", DbType.Boolean);

            // B/C 1 rating per user per TP, lock it down:
            Execute(@"
                ALTER TABLE TrainingProgramFeedback
                ADD CONSTRAINT tpf_unique UNIQUE(UserID, TrainingProgramID)
            ");
        }
    }
}
