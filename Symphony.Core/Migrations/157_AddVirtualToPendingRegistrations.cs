﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration157 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER view [dbo].[PendingRegistrations] as
select
	Class.ID as ClassId,
	Registration.ID as RegistrationId, 
	class.CustomerID as CustomerId,
	Class.Name,
	ClassDateMinMax.MinStartDateTime as StartDate,
	case 
		when Class.IsVirtual = 1 then 'Virtual'
	else
		Room.Name + ', ' + Venue.Name + ', ' + Venue.City + ', ' + States.Name end
	as Location,
	[User].FirstName,
	[User].MiddleName,
	[User].LastName,
	Registration.RegistrationStatusID as status
from
	Registration
inner join
	Class on Registration.ClassID = class.ID
inner join
	[User] on [User].ID = Registration.RegistrantID
inner join
	ClassDateMinMax on class.ID = ClassDateMinMax.ClassID
left join
	Room on class.RoomID = Room.ID
left join
	Venue on room.VenueID = venue.ID	
left join
	States on Venue.StateID = States.ID
where 
	Registration.RegistrationStatusID = 1
");
        }
    }
}