﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration319: Migration
    {
        public override void Up()
        {
            AddColumn("Parameter", "CustomerID", DbType.Int32, 0, true);
            AddColumn("ParameterSet", "CustomerID", DbType.Int32, 0, true);
        }
    }
}