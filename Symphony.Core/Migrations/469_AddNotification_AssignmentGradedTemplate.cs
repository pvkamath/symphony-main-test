﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration469 : Migration
    {

        public override void Up()
        {
            Execute(@"

    INSERT INTO dbo.Templates (Subject, Body, CodeName, DisplayName, Description, Priority, CustomerID, Enabled, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, RelativeScheduledMinutes, FilterComplete, OwnerID, CcClassroomSupervisors, CcReportingSupervisors, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, IncludeCalendarInvite)
    SELECT TOP 1 'Assignment graded', 'Assignment graded', 'AssignmentGraded', 'Assignment graded', 'Assignment graded by instructor', Priority, 0, Enabled, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, RelativeScheduledMinutes, FilterComplete, OwnerID, CcClassroomSupervisors, CcReportingSupervisors, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, IncludeCalendarInvite FROM dbo.Templates

    INSERT dbo.Templates (Subject, Body, CodeName, DisplayName, Description, Priority, CustomerID, Enabled, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, RelativeScheduledMinutes, FilterComplete, OwnerID, CcClassroomSupervisors, CcReportingSupervisors, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId, IncludeCalendarInvite)
    SELECT
	    tp.Subject,
	    tp.Body,
	    tp.CodeName,
	    tp.DisplayName,
	    tp.Description,
	    tp.Priority,
	    c.Id,
	    tp.Enabled,
	    tp.ModifiedBy,
	    tp.CreatedBy,
	    tp.ModifiedOn,
	    tp.CreatedOn,
	    tp.Area,
	    tp.IsScheduled,
	    tp.ScheduleParameterID,
	    tp.RelativeScheduledMinutes,
	    tp.FilterComplete,
	    tp.OwnerID,
	    tp.CcClassroomSupervisors,
	    tp.CcReportingSupervisors,
	    tp.CreatedByUserId,
	    tp.ModifiedByUserId,
	    tp.CreatedByActualUserId,
	    tp.ModifiedByActualUserId,
	    tp.IncludeCalendarInvite
    FROM
	    Customer c
	    INNER JOIN dbo.Templates tp ON tp.CustomerID = 0 AND tp.CodeName = 'AssignmentGraded'

    DECLARE @TemplateId INT
    DECLARE @RecipientGroupID INT
    SELECT @TemplateId = Id FROM dbo.Templates WHERE CodeName = 'AssignmentGraded' AND CustomerID = 0
    SELECT @RecipientGroupID = Id FROM dbo.RecipientGroups WHERE Selector = 'ID'
    INSERT INTO dbo.TemplatesToRecipientGroupsMap (RecipientGroupID, TemplateID, CreatedByUserId, ModifiedByUserId, CreatedByActualUserId, ModifiedByActualUserId)
    VALUES (@RecipientGroupID, @TemplateId, DEFAULT, DEFAULT, 0, 0);

        ");
        }

    }
}
