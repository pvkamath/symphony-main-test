﻿using System.Data;
using SubSonic;

namespace Symphony.Core.Migrations
{
    public class Migration839 : Migration
    {
        public override void Up()
        {
            TableSchema.Table template = CreateTableWithKey("CertificateTemplate", "ID");

            template.AddColumn("CustomerID", DbType.Int32, 0, true);
            template.AddColumn("CertificateType", DbType.Int32, 0, false);
            template.AddColumn("Name", DbType.String, 128, false);
            template.AddColumn("Description", DbType.String, 1024, false, "''");
            template.AddColumn("Width", DbType.Int32, 0, false);
            template.AddColumn("Height", DbType.Int32, 0, false);
            template.AddColumn("Content", DbType.String, 32768, false);
            template.AddColumn("ParentID", DbType.Int32, 0, false, "0");
            template.AddColumn("BgImageId", DbType.Int32);
            template.AddColumn("BgImage", DbType.String);
            template.AddColumn("BgImagePath", DbType.String);
            
            AddSubSonicStateColumns(template);
            template.AddColumn("IsDeleted", DbType.Boolean, 0, false, "0");

            AddColumn("TrainingProgram", "CertificateTemplateID", DbType.Int32, 0, true);
            AddColumn("OnlineCourse", "CertificateTemplateID", DbType.Int32, 0, true);

            Execute("ALTER TABLE CertificateTemplate"
                + " ADD CONSTRAINT chk_CertificateTemplate_CertificateType"
                    + " CHECK (CertificateType > 0)"
                );

            Execute("ALTER TABLE CertificateTemplate"
                + " ADD CONSTRAINT chk_CertificateTemplate_Width"
                    + " CHECK (Width > 0)"
                );

            Execute("ALTER TABLE CertificateTemplate"
                + " ADD CONSTRAINT chk_CertificateTemplate_Height"
                    + " CHECK (Height > 0)"
                );

            Execute("ALTER TABLE CertificateTemplate"
                + " ADD CONSTRAINT fk_Customer_ID_CertificateTemplate_CustomerID FOREIGN KEY(CustomerID)"
                    + " REFERENCES Customer(ID)"
                );

            Execute("ALTER TABLE TrainingProgram"
                + " ADD CONSTRAINT fk_CertificateTemplate_ID_TrainingProgram_CertificateTemplateID FOREIGN KEY(CertificateTemplateID)"
                    + " REFERENCES CertificateTemplate(ID)"
                );

            Execute("ALTER TABLE OnlineCourse"
                + " ADD CONSTRAINT fk_CertificateTemplate_ID_OnlineCourse_CertificateTemplateID FOREIGN KEY(CertificateTemplateID)"
                    + " REFERENCES CertificateTemplate(ID)"
                );
        }
    }
}