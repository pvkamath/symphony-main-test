﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration720 : Migration
    {

        public override void Up()
        {
            Execute(@"
                if not exists (select SystemCodeName from ExternalSystem where SystemCodeName = 'healthcare')
                begin
                    insert into ExternalSystem (SystemName, SystemCodeName, LoginUrl, IsRequired, ExternalSystemTypeName)
                        values ('HealthCare', 'healthcare', 'https://www.nurse.com/signup/', 0, 'HealthCare')
                end
            ");

            Execute(@"
                update ExternalSystem set ExternalSystemTypeName = 'ProSchools' where SystemCodeName = 'proschools';
                update ExternalSystem set ExternalSystemTypeName = 'TrainingPro' where SystemCodeName = 'trainingpro';
                update ExternalSystem set ExternalSystemTypeName = 'Symphony' where SystemCodeName = 'symphony';
                update ExternalSystem set ExternalSystemTypeName = 'CareerWebSchool' where SystemCodeName = 'careerwebschool';
            ");
        }

    }
}
