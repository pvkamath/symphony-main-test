﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration052 : Migration
    {
        public override void Up()
        {
            AddColumn("Class", "ApprovalRequired", DbType.Boolean, 0, false, "1");   
        }
    }
}