﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration387 : Migration
    {
        public override void Up()
        {
            Execute(@"ALTER view [dbo].[CustomerDetails] as
                        select
	                    customer.*, 
	                    u1.FirstName + ' ' + u1.LastName as AccountExecutiveFullName, 
	                    u2.FirstName + ' ' + u2.LastName as CustomerCareRepFullName,
	                    Location.Name as CustomerLocation
                    from
	                    Customer
                    left join
	                    [User] u1
                    on
	                    Customer.AccountExecutive = u1.id
                    left join
	                    [User] u2
                    on
	                    Customer.CustomerCareRep = u2.id
                    left join 
	                    Location
                    on Customer.LocationID = Location.ID;
;;");
        }
    }
}