﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration235 : Migration
    {
        public override void Up()
        {
            Execute(@"
                UPDATE oc
                  SET oc.PublishedArtisanCourseID = 
	                (SELECT TOP 1 ac.ID FROM ArtisanCourses as ac
		                WHERE ac.OriginalCourseId = oc.ArtisanCourseID 
		                  AND ac.IsPublished = 1 
		                  AND ac.IsDeleted = 0
		                ORDER BY ac.ModifiedOn DESC)
                  FROM OnlineCourse as oc
                  WHERE oc.ArtisanCourseID IS NOT NULL
            ");
        }
    }
}