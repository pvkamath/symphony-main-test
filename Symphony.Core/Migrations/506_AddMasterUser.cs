﻿using SubSonic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Symphony.Core.Migrations
{
    class Migration506 : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.MasterUser
(
	MasterUserId INT IDENTITY(1,1) NOT NULL,
	UserId INT
)
");

            Execute(@"
ALTER TABLE dbo.MasterUser
ADD CONSTRAINT PK_MasterUser__MasterUserId PRIMARY KEY (MasterUserId)
");

            Execute(@"
ALTER TABLE dbo.MasterUser
ADD CONSTRAINT FK_MasterUser_User__Id FOREIGN KEY (MasterUserId) REFERENCES dbo.[User](ID)
");    
        }
    }
}
