﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration673 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "IsMasteryEnabled", DbType.Boolean, 0, false, "0");
            AddColumn("ArtisanCourses", "MasteryLevel", DbType.Int32, 0, true);
            AddColumn("ArtisanCourses", "MinimumMasteryQuestions", DbType.Int32, 0, true);
            AddColumn("ArtisanCourses", "IsUspap", DbType.Boolean, 0, false, "0");
            AddColumn("ArtisanCourses", "IsAntiGuessing", DbType.Boolean, 0, false, "0");
            AddColumn("ArtisanCourses", "Mode", DbType.String, 64, true);
        }
    }
}
