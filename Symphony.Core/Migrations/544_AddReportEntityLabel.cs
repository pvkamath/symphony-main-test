﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration544 : Migration
    {
        public override void Up()
        {
            AddColumn("ReportTemplateEntity", "Label", DbType.String, 64, true);

            Execute(@"
alter table ReportTemplateEntity
add  Description nvarchar(max) null
");
        }
    }
}