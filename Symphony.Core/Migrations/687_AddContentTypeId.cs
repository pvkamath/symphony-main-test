﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration687 : Migration
    {
        public override void Up()
        {
            AddColumn("ArtisanCourses", "ContentTypeID", DbType.Int32, 0, false, "0");
            AddColumn("OnlineCourse", "ContentTypeID", DbType.Int32, 0, false, "0");
        }
    }
}
