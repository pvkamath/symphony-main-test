﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;

namespace Symphony.Core.Migrations
{
    public class Migration239 : Migration
    {
        public override void Up()
        {
            AddColumn("ReportQueues", "Parameters", DbType.String, 2000, false);
        }
    }
}