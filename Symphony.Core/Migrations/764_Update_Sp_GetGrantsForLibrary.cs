﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration764 : Migration
    {
        public override void Up()
        {
            Execute(@"
ALTER procedure [dbo].[Library_GetGrantsForLibrary]
	(
	@libraryId int,
	@search nvarchar(50), 
	@orderBy nvarchar(50), 
	@orderDir varchar(4), 
	@pageIndex int, 
	@pageSize int,
	@customerId int = 0,
	@showCustomerOnly bit = 0
	)
as
begin 
	with G as (
		select 
			lg.ID,
			lg.HierarchyTypeID,
			lg.HierarchyNodeID,
			l.ID as LocationID,
			j.ID as JobRoleID,
			a.ID as AudienceID,
			u.ID as UserID,
			coalesce(l.Name, j.Name, a.Name, u.FirstName + ' ' + u.LastName) as Name,
			coalesce(l.ParentLocationID, j.ParentJobRoleID, a.ParentAudienceID, 0) as ParentID,
			lg.StartDate,
			lg.EndDate,
			row_number() over
			(
				order by
					case when @orderBy = 'HierarchyTypeID' and @orderDir = 'asc' then lg.HierarchyTypeID end,
					case when @orderBy = 'HierarchyTypeID' and @orderDir = 'desc' then lg.HierarchyTypeID end desc,
					case when @orderBy != 'HierarchyTypeID' then lg.HierarchyTypeID end,
					coalesce(l.ParentLocationID, j.ParentJobRoleID, a.ParentAudienceID, 0),		
					case when @orderBy = 'Name' and @orderDir = 'asc' then coalesce(l.Name, j.Name, a.Name, u.FirstName + ' ' + u.LastName) end,
					case when @orderBy = 'Name' and @orderDir = 'desc' then coalesce(l.Name, j.Name, a.Name, u.FirstName + ' ' + u.LastName) end desc,
					case when @orderBy = 'StartDate' and @orderDir = 'asc' then lg.StartDate end,
					case when @orderBy = 'StartDate' and @orderDir = 'desc' then lg.StartDate end,
					case when @orderBy = 'EndDate' and @orderDir = 'asc' then lg.EndDate end,
					case when @orderBy = 'EndDate' and @orderDir = 'desc' then lg.EndDate end
					
			) as r,
			coalesce(l.CustomerID, j.CustomerID, a.CustomerID, u.CustomerID) as HierarchyCustomerID,
			lib.CustomerID as LibraryCustomerID
		from LibraryGrant lg
		left join Location l on l.ID = lg.HierarchyNodeID and lg.HierarchyTypeID = 1
		left join JobRole j on j.ID = lg.HierarchyNodeID and lg.HierarchyTypeID = 2
		left join Audience a on a.ID = lg.HierarchyNodeID and lg.HierarchyTypeID = 3
		left join [User] u on u.ID = lg.HierarchyNodeID and lg.HierarchyTypeID = 4
		join HierarchyType h on h.ID = lg.HierarchyTypeID
		join Library lib on lib.ID = lg.LibraryID
		join Customer c on c.ID = lib.CustomerID
		where
			(
				l.Name like '%' + @search + '%' or
				a.Name like '%' + @search + '%' or 
				j.Name like '%' + @search + '%' or
				u.Search like '%' + @search + '%' or
				u.Email like '%' + @search + '%' or
				h.Name like '%' + @search + '%' or
				(c.UserNameAlias like '%' + @search + '%' and lg.HierarchyTypeID = 4) or
				(c.LocationAlias like '%' + @search + '%' and lg.HierarchyTypeID = 1) or
				(c.JobRoleAlias like '%' + @search + '%' and lg.HierarchyTypeID = 2) or
				(c.AudienceAlias like '%' + @search + '%' and lg.HierarchyTypeID = 3)
			) and
			lg.LibraryID = @libraryId
			and 
			(
				@showCustomerOnly = 0 or
				coalesce(l.CustomerID, j.CustomerID, a.CustomerID, u.CustomerID) = @customerId
			)
	)

	select 
		G.*,
		p.Level,
		P.LevelIndentText,
		(select max(G.r) from G) as TotalRows,
		cast(case when G.LibraryCustomerID != G.HierarchyCustomerID then 1 else 0 end as bit) as IsExternalGrant,
		cast(case when G.HierarchyCustomerID = @customerId then 1 else 0 end as bit) as IsCustomerGrant
	from G
	cross apply fGetHierarchyPath(G.HierarchyNodeID, G.HierarchyTypeID) p
	where G.r between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize)
	order by 
		case when @orderBy = 'HierarchyTypeID' and @orderDir = 'asc' then G.HierarchyTypeID end,
		case when @orderBy = 'HierarchyTypeID' and @orderDir = 'desc' then G.HierarchyTypeID end desc,
		case when @orderBy != 'HierarchyTypeID' then G.HierarchyTypeID end,
		p.LevelIndentText asc,
		G.ParentID,		
		case when @orderBy = 'Name' and @orderDir = 'asc' then G.Name end,
		case when @orderBy = 'Name' and @orderDir = 'desc' then G.Name end desc,
		case when @orderBy = 'StartDate' and @orderDir = 'asc' then G.StartDate end,
		case when @orderBy = 'StartDate' and @orderDir = 'desc' then G.StartDate end,
		case when @orderBy = 'EndDate' and @orderDir = 'asc' then G.EndDate end,
		case when @orderBy = 'EndDate' and @orderDir = 'desc' then G.EndDate end
end
;
");
        }
    }
}
