﻿using System.Data;
using System.IO;
using SubSonic;
using System.Text;
using System;
using System.Data.Common;

namespace Symphony.Core.Migrations
{
    public class Migration514 : Migration
    {

        public override void Up()
        {
            Execute(@"
    insert into ReportEntity (Name, Xtype, Config)
    values  ('Audience', 'reporting.audiencepicker', ''),
            ('Class Date', 'reporting.classdatepicker', ''),
            ('Class Location', 'reporting.classlocationpicker', ''),
            ('Class Status', 'reporting.classstatuspicker', ''),
            ('Contains Users', 'reporting.containsuserspicker', ''),
            ('Course Due Date', 'reporting.courseduedatepicker', ''),
            ('Course Filter', 'reporting.coursefilterpicker', ''),
            ('Course Pass Type', 'reporting.coursepasstypepicker', ''),
            ('Course', 'reporting.coursepicker', ''),
            ('Course Required Type', 'reporting.courserequiredtypepicker', ''),
            ('Course Type', 'reporting.coursetypepicker', ''),
            ('Date Range', 'reporting.daterangepicker', ''),
            ('Group By', 'reporting.groupbypicker', ''),
            ('Group By Selection', 'reporting.groupbyselectionpicker', ''),
            ('Hire Date', 'reporting.hiredatepicker', ''),
            ('Instructor', 'reporting.instructorpicker', ''),
            ('Job Role', 'reporting.jobrolepicker', ''),
            ('Location', 'reporting.locationpicker', ''),
            ('Pass Date', 'reporting.passdatepicker', ''),
            ('Permission', 'reporting.permissionpicker', ''),
            ('Student', 'reporting.studentpicker', ''),
            ('Student Status', 'reporting.studentstatuspicker', ''),
            ('Supervisor', 'reporting.supervisorpicker', ''),
            ('Test Date', 'reporting.testdatepicker', ''),
            ('Training Program Date', 'reporting.trainingprogramdatepicker', ''),
            ('Training Program', 'reporting.trainingprogrampicker', ''),
            ('View Date', 'reporting.viewdatepicker', '')

");
        }

    }
}