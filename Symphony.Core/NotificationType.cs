﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core
{
    /// <summary>
    /// The various notifications in the system
    /// </summary>
    public class NotificationType
    {
        // standard
        public const string MarkedAsAttended = "MarkedAsAttended";
        public const string SchedulingChange = "SchedulingChange";
        public const string InstructorRemoved = "InstructorRemoved";
        public const string InstructorAdded = "InstructorAdded";
        public const string ResourceRequested = "ResourceRequested";
        public const string ClassCancelled = "ClassCancelled";
        public const string ClassRoomChange = "ClassRoomChange";
        public const string NewClassScheduled = "NewClassScheduled";
        public const string LearningObjectDeletedCopy = "LearningObjectDeletedCopy";
        public const string LearningObjectDeletedCourse = "LearningObjectDeletedCourse";
        public const string StudentRegistered = "StudentRegistered";
        public const string StudentUnregistered = "StudentUnregistered";
        public const string RegistrationStatusChanged = "RegistrationStatusChanged";
        public const string TrainingProgramAssigned = "TrainingProgramAssigned";
        public const string MeetingCreatedInternal = "MeetingCreatedInternal";
        public const string MeetingCreatedExternal = "MeetingCreatedExternal";
        public const string MeetingUpdatedInternal = "MeetingUpdatedInternal";
        public const string MeetingUpdatedExternal = "MeetingUpdatedExternal";
        public const string MeetingCancelledInternal = "MeetingCancelledInternal";
        public const string MeetingCancelledExternal = "MeetingCancelledExternal";
        public const string UserRegistered = "UserRegistered";
        public const string NewHireStatusChange = "NewHireStatusChange";

        public const string AssignmentGraded = "AssignmentGraded";
        public const string AssignmentsGradedPassed = "AssignmentsGradedPassed";
        public const string AssignmentsGradedFailed = "AssignmentsGradedFailed";

        public const string InstructorPermissionGranted = "InstructorPermissionGranted";
        
        public const string SalesforceNewUser = "SalesforceNewUser";
        public const string SalesforceNewEnrollment = "SalesforceNewEnrollment";

        public const string SingleAssignmentGradedPassed = "SingleAssignmentGradedPassed";
        public const string SingleAssignmentGradedFailed = "SingleAssignmentGradedFailed";


        public const string Certificate = "Certificate";
    }
}
