﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using log4net;
using Symphony.Core.Models;
using Symphony.Core.Controllers;
using System.Threading;
using SubSonic;
using Newtonsoft.Json;
using System.Data;

namespace Symphony.Core.Jobs
{
    public class CizerReportJob : IJob
    {
        ILog Log = LogManager.GetLogger(typeof(CizerReportJob));

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                int reportId = Convert.ToInt32(context.JobDetail.Key.Name);
                ExecuteReport(reportId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public int ExecuteReport(int reportId)
        {
            try
            {
                Report report = (new ReportController()).GetReport(0, 0, reportId).Data;

                // Save it in the Queue
                int queueId = (new ReportController()).SubmitJobToRunInCizer(report);

                // Job Save successfully submit it to run in Cizer

                // Submit to Cizer use Report.Parameters / Report.ReportTypeCode
                // translate the parameters to the QueueCommand
                Data.ReportQueue queueEntry = Select.AllColumnsFrom<Data.ReportQueue>()
                    .Where(Data.ReportQueue.IdColumn).IsEqualTo(queueId)
                    .ExecuteSingle<Data.ReportQueue>();

                if (queueEntry == null)
                {
                    queueEntry = new Data.ReportQueue();
                }
                queueEntry.CustomerID = report.CustomerId;
                queueEntry.ReportID = report.Id;
                queueEntry.ReportName = report.Name;
                queueEntry.StartedOn = DateTime.UtcNow;
                queueEntry.FinishedOn = null;
                queueEntry.DownloadCSV = report.DownloadCSV;
                queueEntry.DownloadXLS = report.DownloadXLS;
                queueEntry.DownloadPDF = report.DownloadPDF;
                queueEntry.StatusID = (int)ReportStatus.NotStarted;

                //*** Simulation Code - manually runs the report when running in debug mode ***//

                // Run Report
#if DEBUG

                Data.ReportQueue queue = new Data.ReportQueue(queueId);
                
                try
                {
                    DataSet reportDataSet = DataService.GetDataSet(new QueryCommand(queue.QueueCommand));
                    queueEntry.ReportData = JsonConvert.SerializeObject(reportDataSet);
                    queueEntry.StatusID = (int)ReportStatus.Ready;
                }
                catch (Exception ex)
                {
                    queueEntry.StatusID = (int)ReportStatus.Failed;
                    queueEntry.ErrorMessage = ex.Message;
                }

#endif
                //** End sim **//

                queueEntry.Save();


                return queueId;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return -1;
            }
        }

        public int SubmitReportToRunInCizer(Report report)
        {
            int queueId;
            queueId = 0;


            return queueId;
        }
    }
}
