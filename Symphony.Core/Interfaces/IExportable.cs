﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core.Interfaces
{
    interface IExportable
    {
        string Export();
    }
}
