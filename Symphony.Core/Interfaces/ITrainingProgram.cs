﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Interfaces
{
    public interface ITrainingProgram
    {
        int Id { get; set; }

        int CustomerID { get; set; }

        int OwnerID { get; set; }

        string Name { get; set; }

        string InternalCode { get; set; }

        decimal Cost { get; set; }

        string Description { get; set; }

        bool IsNewHire { get; set; }

        bool IsLive { get; set; }

        DateTime? StartDate { get; set; }

        DateTime? EndDate { get; set; }

        DateTime? DueDate { get; set; }

        bool EnforceRequiredOrder { get; set; }

        int MinimumElectives { get; set; }

        int CourseCount { get; set; }

        int TotalSize { get; set; }

        int MessageBoardID { get; set; }

        string Sku { get; set; }

        int ParentTrainingProgramID { get; set; }

        bool IsActive { get; set; }

        int? SessionCourseID { get; set; }

        ClassroomClass Session { get; set; }

        bool IsSessionActive { get; set; }

        bool IsSessionRegistered { get; set; }

        bool IsUsingSessions { get; set; }

        string SessionCourseName { get; set; }

        string CategoryName { get; set; }

        int? NewHireStartDateOffset { get; set; }

        int? NewHireDueDateOffset { get; set; }

        int? NewHireEndDateOffset { get; set; }

        bool NewHireOffsetEnabled { get; set; }

        int? NewHireTransitionPeriod { get; set; }

        string PurchasedFromBundleName { get; set; }
    }
}
