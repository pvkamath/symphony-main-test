﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Symphony.Core
{
    /// <summary>
    /// Available application modules
    /// </summary>
    [DataContract]
    public class Modules
    {
        [DataMember(Name = "GoToMeeting")]
        public string GoToMeeting { get { return "GTM"; } private set {} }
        [DataMember(Name = "GoToWebinar")]
        public string GoToWebinar { get { return "GTW"; } private set {} }
        [DataMember(Name = "Artisan")]
        public string Artisan { get { return "ARTISAN"; } private set {} }
        [DataMember(Name = "Reporting")]
        public string Reporting { get { return "REPORTING"; } private set {} }
        [DataMember(Name = "Portal")]
        public string Portal { get { return "PORTAL"; } private set {} }
        [DataMember(Name = "Classroom")]
        public string Classroom { get { return "CLASSROOM"; } private set {} }
        [DataMember(Name = "Customer")]
        public string Customer { get { return "CUSTOMER"; } private set {} }
        [DataMember(Name = "CourseAssignment")]
        public string CourseAssignment { get { return "COURSEASSIGNMENT"; } private set {} }
        [DataMember(Name = "MessageBoards")]
        public string MessageBoards { get { return "MESSAGEBOARDS"; } private set {} }
        [DataMember(Name = "StudentPortal")]
        public string StudentPortal { get { return "STUDENTPORTAL"; } private set {} }
        [DataMember(Name = "InstructorPortal")]
        public string InstructorPortal { get { return "INSTRUCTORPORTAL"; } private set {} }
        [DataMember(Name = "VideoChat")]
        public string VideoChat { get { return "VIDEOCHAT"; } private set {} }
        [DataMember(Name = "AdvancedTrainingProgram")]
        public string AdvancedTrainingProgram { get { return "ADVANCEDTRAININGPROGRAM"; } private set {} }
        [DataMember(Name = "Certificates")]
        public string Certificates { get { return "CERTIFICATES"; } private set {} }
        [DataMember(Name = "Affidavit")]
        public string Affidavit { get { return "AFFIDAVIT"; } private set {} }
        [DataMember(Name = "Parameters")]
        public string Parameters { get { return "PARAMETERS"; } private set {} }
        [DataMember(Name = "Validation")]
        public string Validation { get { return "VALIDATION"; } private set {} }
        [DataMember(Name = "Proctor")]
        public string Proctor { get { return "PROCTOR"; } private set {} }
        [DataMember(Name = "Surveys")]
        public string Surveys { get { return "SURVEYS"; } private set {} }
        [DataMember(Name = "Salesforce")]
        public string Salesforce { get { return "SALESFORCE"; } private set { } }
        [DataMember(Name = "Licenses")]
        public string Licenses { get { return "LICENSES"; } private set { } }
        [DataMember(Name = "BulkDownload")]
        public string BulkDownload { get { return "BULKDOWNLOAD"; } private set { } }
        [DataMember(Name = "Books")]
        public string Books { get { return "BOOKS"; } private set { } }
        [DataMember(Name = "Libraries")]
        public string Libraries { get { return "LIBRARIES"; } private set { } }
        [DataMember(Name = "AdvancedAdministration")]
        public string AdvancedAdministration { get { return "ADVADMIN"; } private set { } }

        public static bool HasModule(Data.Customer customer, string module) {
            return customer.Modules.Contains(module);
        }

        public static bool HasModule(int CustomerID, string module) {
            var customer = new Data.Customer(CustomerID);
            return HasModule(customer, module);
        }
    }
}
