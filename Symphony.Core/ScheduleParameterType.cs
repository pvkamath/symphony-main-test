﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core
{
    /// <summary>
    /// What a scheduled template is linked to
    /// </summary>
    public class ScheduleParameterType
    {
        public const string TrainingProgramStartDate = "trainingprogramstartdate";
        public const string TrainingProgramEndDate = "trainingprogramenddate";
        public const string TrainingProgramDueDate = "trainingprogramduedate";
        public const string ClassStartDate = "classstartdate";
        public const string ClassEndDate = "classenddate";
        public const string TrainingProgramCourseDueDate = "trainingprogramcourseduedate";
        public const string NewHireExpiration = "newhireexpiration";
        public const string TrainingProgramAfterCompletion = "trainingprogramaftercompletion";
        public const string TrainingProgramPercentComplete = "trainingprogrampercentcomplete";
        public const string StudentLastLoggedIn = "studentlastloggedin";
        public const string StudentLastCourseActivity = "studentlastcourseactivity";
        public const string TrainingProgramSessionStart = "trainingprogramsessionstart";
        public const string TrainingProgramSessionEnd = "trainingprogramsessionend";
    }
}
