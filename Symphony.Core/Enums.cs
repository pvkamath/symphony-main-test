using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core
{
    /*
     * Please note that these enums *must* match up with the values stored in the corresponding database tables
     */

    public enum OrderDirection
    {
        Default = 0,
        Ascending = 1,
        Descending = 2
    }

    // not serialized to the db
    public enum ArtisanPageType
    {
        Content = 1,
        Question = 2,
        Objectives = 3,
        Introduction = 4,
        Survey = 5
    }

    public enum ArtisanContentType
    {
        Unknown = 0,
        Video = 1,
        Audio = 2
    }

    public enum ReportSystem
    {
        Default = 0,
        Qlik = 1
    }

    public enum RetestMode
    {
        AlwaysAllow = 0,
        OnlyOnFailure = 1
    }

    /// <summary>
    /// Asset types, from table "ArtisanAssetTypes"
    /// </summary>
    public enum ArtisanAssetType
    {
        Image = 1,
        Flash = 2,
        Html = 3,
        Document = 4
    }

    public enum EventType
    {
        RegisteredClass = 1,
        Meeting = 2,
        InstructedClass = 3,
        PublicClass = 4,
        TrainingProgramClass = 5
    }

    // not serialized to the db
    public enum ArtisanQuestionType
    {
        [EnumRecord("multiplechoice", "Multiple Choice")]
        MultipleChoice = 1,
        [EnumRecord("truefalse", "True and False")]
        TrueFalse = 2,
        [EnumRecord("fillintheblank", "Fill in the Blank")]
        FillInTheBlank = 3,
        [EnumRecord("allthatapply", "All that Apply")]
        AllThatApply = 4,
        [EnumRecord("imagequestion", "Image Recognition")]
        ImageQuestion = 5,
        [EnumRecord("matching", "Matching")]
        Matching = 6,
        [EnumRecord("categorymatching", "Category Matching")]
        CategoryMatching = 7,
        [EnumRecord("longanswer", "Long Answer")]
        LongAnswer = 8
    }

    /// <summary>
    /// Hierarchy types, from table "HierarchyType"
    /// </summary>
    public enum HierarchyType
    {
        Location = 1,
        JobRole = 2,
        Audience = 3,
        User = 4
    }

    // not serialized to the db
    public enum ImportType
    {
        User = 1,
        JobRole = 2,
        Location = 3,
        Audience = 4,
        Order = 5
    }

    /// <summary>
    /// Course Types, from table "CourseType"
    /// </summary>
    public enum CourseType
    {
        Classroom = 1,
        Online = 2
    }

    /// <summary>
    /// Course completion types
    /// </summary>
    public enum CourseCompletionType
    {
        Attendance = 1,
        [EnumRecord("numeric")]
        NumericGrade = 2,
        [EnumRecord("letter")]
        LetterGrade = 3,
        [EnumRecord("onlineCourse")]
        OnlineTest = 4
    }

    /// <summary>
    /// User statuses, active, disabled, etc
    /// </summary>
    public enum UserStatusType
    {
        Active = 1,
        Inactive = 2,
        LeaveOfAbsence = 3,
        Deleted = 4,
        ApprovalPending = 5
    }

    /// <summary>
    /// Registration status, from table "Status"
    /// </summary>
    public enum RegistrationStatusType
    {
        AwaitingApproval = 1,
        WaitList = 2,
        Denied = 3,
        Registered = 4
    }

    /// <summary>
    /// Registration type, from table "RegistrationType"
    /// </summary>
    public enum RegistrationType
    {
        Manual = 1,
        Self = 2
    }

    /// <summary>
    /// For a training program-course relationship
    /// </summary>
    public enum SyllabusType
    {
        Required = 1,
        Elective = 2,
        Optional = 3,
        Final = 4,
        Survey = 5
    }

    /// <summary>
    /// Which section the category was created in
    /// </summary>
    public enum CategoryType
    {
        ClassroomCourse = 1,
        OnlineCourse = 2,
        TrainingProgram = 3,
        Artisan = 4
    }

    /// <summary>
    /// Section type for the sections in Artisan
    /// </summary>
    public enum ArtisanSectionType
    {
        Sco = 1,
        LearningObject = 2,
        Objectives = 3,
        Survey = 4,
        Pretest = 5,
        Posttest = 6
    }

    /// <summary>
    /// Completion method for Artisan courses
    /// </summary>
    public enum ArtisanCourseCompletionMethod
    {
        Navigation = 1,
        Test = 2,
        InlineQuestions = 3,
        TestForceRestart = 4,
        Survey = 5
    }

    /// <summary>
    /// Inactivity logout method for Artisan courses
    /// </summary>
    public enum ArtisanCourseTimeoutMethod
    {
        None = 0,
        ShowAlert = 1,
        Automatic = 2
    }

    /// <summary>
    /// Marking method for Artisan courses
    /// </summary>
    public enum ArtisanCourseMarkingMethod
    {
        ShowAnswers = 1,
        HideAnswers = 2,
        HideAnswersAndFeedback = 3,
        HideAllFeedback = 4
    }

    /// <summary>
    /// Review method for Artisan courses
    /// </summary>
    public enum ArtisanCourseReviewMethod
    {
        ShowReview = 1,
        HideReview = 2
    }

    /// <summary>
    /// Test method for Artisan courses
    /// </summary>
    public enum ArtisanTestType
    {
        GameBoard = 1,
        GameWheel = 2,
        Sequential = 3,
        Random = 4
    }

    public enum ArtisanCourseNavigationMethod
    {
        Sequential = 1,
        FreeForm = 2,
        FullSequential = 3,
        FreeFormWithQuiz = 4
    }

    public enum ArtisanCourseBookmarkingMethod
    {
        Standard = 1, 
        Automatic = 2
    }

    [Flags]
    public enum ArtisanDeploymentTarget
    {
        Symphony = 1,
        Mars = 2
    }

    public enum ReportSchedule
    {
        None = 0,
        Daily = 1,
        Weekly = 2,
        Monthly = 3,
        Yearly = 4
    }

    [Flags]
    public enum DaysOfWeek
    {
        None = 0x0,
        Sunday = 0x1,
        Monday = 0x2,
        Tuesday = 0x4,
        Wednesday = 0x8,
        Thursday = 0x10,
        Friday = 0x20,
        Saturday = 0x40
    }

    public enum ReportStatus
    {
        NotStarted = 0,
        Running = 1,
        Failed = 2,
        Ready = 3
    }

    /// <summary>
    /// Type of link between messageboard and course
    /// </summary>
    public enum MessageBoardType
    {
        TrainingProgram = 1,
        Classroom = 2,
        Online = 3
    }

    public enum CertificateType
    {
        TrainingProgram = 1, 
        Classroom = 2, 
        Online = 3
    }

    public enum OnlineStatus
    {
        Online = 1,
        Busy = 2,
        Away = 3,
        Offline = 4
    }

    public enum SsoType
    {
        Saml = 1,
        Janrain = 2
    }

    public enum SsoLoginUiType
    {
        Standard = 1,
        Mixed = 2, 
        Forced = 3
    }

    public enum AssignmentResponseStatus
    {
        Unmarked = 1,
        Correct = 2,
        Incorrect = 3
    }

    public enum TrainingProgramBundleStatus
    {
        InDevelopment = 1,
        Active = 2,
        InActive = 3
    }

    // All day event vs timed event
    public enum CalendarType
    {
        AllDay = 1,
        Timed = 2
    }

    // Operation to perform for the calendar item
    public enum CalendarMethod
    {
        Create = 1,
        Update = 2,
        Delete = 3
    }

    // Mode for relative active/due date
    public enum RelativeTimeMode
    {
        Course = 1,
        TrainingProgram = 2,
        Session = 3,
        PreviousCourseEnd = 4,
        FinalUnlocked = 5
    }

    /// <summary>
    /// Status of the notification - Queued, Sent, Cancelled
    /// </summary>
    public enum NotificationStatus
    {
        Queued = 1,
        Sent = 2,
        Canceled = 3
    }

    // How/if courses are unlocked after completion
    public enum CourseUnlockMode
    {
        None = 0,
        AfterCourseCompletion = 1
    }

    public enum SessionStatus
    {
        Unregistered = 0,
        Expired = 1,
        Active = 2,
        Future = 3
    }

    public enum LibraryItemType
    {
        OnlineCourse = 1,
        TrainingProgram = 2
    }

    public enum LibraryFilterType
    {
        InLibrary = 1,
        NotInLibrary = 2,
        All = 3
    }

    public enum LicenseExpirationRule
    {
        Individual = 1,
        DaysAfterStart = 2,
        EndOfYear = 3
    }

    public enum ArtisanMasteryLevel
    {
        LevelOne = 1,
        LevelTwo = 2
    }

    public enum RedirectType
    {
        Symphony = 1,
        Simple = 2,
        OptIn = 3,
        Force = 4
    }

    public enum ComparisonOperators
    {
        EqualTo = 1,
        NotEqualTo = 2,
        GreaterThan = 3,
        LessThan = 4,
        GreaterThanOrEqual = 5,
        LessThanOrEqual = 6,
        In = 7,
        Like = 8
    }

    public enum LogicOperators
    {
        And = 1,
        Or = 2
    }

    public enum AccreditationStatus
    {
        Pending = 1,
        Accredited = 2,
        Expired = 3
    }

    public enum ExternalSystemFormType
    {
        Submit = 1,
        IFrame = 2,
        Eval = 3
    }

    public enum ScheduleParameterOptions
    {
        BeforeOrAfter = 1,
        Percentage = 2,
        AfterOnly = 3
    }

    public enum EntityType
    {
        TrainingProgram = 1,
        Library = 2
    }

    public enum FieldType
    {
        Label = 1,
        TextField = 2,
        DateField = 3,
        ConfirmationButton = 4,
        CheckboxField = 5,
        SymphonyUserField = 6,
        SymphonyDataField = 7, 
        ProctorCodeField = 8
    }
}
