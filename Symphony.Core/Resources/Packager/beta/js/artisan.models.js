(function () {
    var models = Artisan.Utilities.ns('Artisan.Models');

    //var API = parent;
    var LMS = parent.objLMS;

    ArtisanSectionType =
    {
        Sco: 1,
        LearningObject: 2,
        Objectives: 3,
        Survey: 4,
        Pretest: 5,
        Posttest: 6
    };

    ArtisanQuestionType =
    {
        MultipleChoice: 1,
        TrueFalse: 2,
        FillInTheBlank: 3,
        AllThatApply: 4,
        ImageQuestion: 5,
        Matching: 6,
        CategoryMatching: 7,
        LongAnswer: 8
    };

    AssignmentResponseStatus =
    {
        Unmarked: 1,
        Correct: 2,
        Incorrect: 3
    }

    ArtisanTestType =
    {
        GameBoard: 1,
        GameWheel: 2,
        Sequential: 3,
        Random: 4
    };

    ArtisanCourseCompletionMethod =
    {
        Navigation: 1,
        Test: 2,
        InlineQuestions: 3,
        TestForceRestart: 4,
        Survey: 5
    };

    ArtisanCourseNavigationMethod =
    {
        Sequential: 1,
        FreeForm: 2,
        FullSequential: 3,
        FreeFormWithQuiz: 4
    };

    ArtisanCourseBookmarkingMethod =
    {
        Standard: 1,
        Automatic: 2
    };

    ArtisanPageType =
    {
        Content: 1,
        Question: 2,
        Objectives: 3,
        Introduction: 4,
        Survey: 5
    };

    ArtisanCourseTimeoutMethod = {
        None: 0,
        ShowAlert: 1,
        Automatic: 2
    };

    ArtisanCourseMarkingMethod = {
        ShowAnswers: 1,
        HideAnswers: 2,
        HideAnswersAndFeedback: 3,
        HideAllFeedback: 4
    };

    ArtisanCourseReviewMethod = {
        ShowReview: 1,
        HideReview: 2
    };

    ArtisanMasteryLevel = {
        LevelOne: 1,
        LevelTwo: 2
    }

    GzipPrefix = "gz:";

    Artisan.Models.Timer = Class.extend({
        init: function (type) {
            this.interval = null;
            this.timeout = null;
            this.running = false;
            this.timeRemaining = null;
            this.timeoutFunction = null;
            this.id = null;

            if (type) {
                type = type.toLowerCase();
            }

            if (type != 'lo' && type != 'sco' && type != 'test') {
                type = 'Course';
            } else {
                type = type.charAt(0).toUpperCase() + type.slice(1);
            }

            this.type = type;

            if (!type) {
                this.typeChar = 'c';
            } else {
                this.typeChar = this.type.toLowerCase().charAt(0);
            }
        },

        getTimeRemaining: function () {
            var minutes = Math.floor(this.timeRemaining / 60),
                seconds = this.timeRemaining % 60;

            return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
        },

        isRunning: function () {
            return this.running;
        },

        startTimeout: function (duration) {
            var me = this;

            // Set by the training program to disable any course timing functions
            if (Artisan.IsCourseTimingDisabled) {
                return;
            }

            me.timeout = setTimeout(function () {
                if (typeof (me.timeoutFunction) === 'function') {
                    me.timeoutFunction();
                }
                me.stop();
            }, duration * 1000);
        },

        startInterval: function () {
            var me = this;

            me.clearInterval();

            // Set by the training program to disable any course timing functions
            if (Artisan.IsCourseTimingDisabled) {
                this.stop();
                return;
            }

            me.interval = setInterval(function () {
                if (me.isRunning()) {
                    me.timeRemaining--;

                    if (typeof (me.intervalFunction) === 'function') {
                        me.intervalFunction(me);
                    }


                    Artisan.App.course.record.addTime(me.typeChar, me.id, 1);

                    if (Artisan.App.course.record.getTime(me.typeChar, me.id) % 30 == 0) {
                        Artisan.App.course.record.saveDataChunk();
                    }
                }

                me.updateDisplay();
            }, 1000);
        },

        clearInterval: function () {
            clearInterval(this.interval);
            this.interval = null;
        },

        pause: function () {
            Artisan.App.course.record.saveDataChunk();
            this.running = false;
            clearTimeout(this.timeout);
            this.timeout = null;
        },

        resume: function () {
            // Set by the training program to disable any course timing functions
            if (Artisan.IsCourseTimingDisabled) {
                this.stop();
                return;
            }

            if (!this.running) {
                startTimeout(this.timeRemaining);
                this.running = true;
            }
        },

        updateDisplay: function () {
            // Set by the training program to disable any course timing functions
            if (Artisan.IsCourseTimingDisabled && this.isRunning()) {
                this.stop();
                this.timeRemaining = -1;
            }

            var display = $('#' + this.type + '-time-remaining'),
                counterText = display.find('.counter-text'),
                timerDisplays = $('#timer-displays');

            if (this.config && this.config.title) {
                display.find('.type').html(this.config.title);
            }

            if (this.timeRemaining > 0 && this.running) {
                display.addClass('running');
                timerDisplays.addClass('active');
            } else {
                if (this.timeRemaining >= 0 && display.hasClass('running')) {
                    var me = this;
                    // If it's 0, or manually stopped
                    // show the last value for 1 second
                    // So timer will actually show 0, then disapear
                    //
                    // Double checks to make sure the timer hasn't been reset between starting this timeout
                    // and it actually firing
                    setTimeout(function () {
                        if (me.timeRemaining <= 0) {
                            display.removeClass('running');
                        }
                    }, 1000);
                } else {
                    // if already negative, remove it right away.
                    display.removeClass('running');
                }
            }

            counterText.html(this.getTimeRemaining());

            // hide the entire panel if no displays are active
            var activeDisplays = timerDisplays.find('.running');
            if (!activeDisplays.length) {
                timerDisplays.removeClass('active');
            }
        },

        start: function (id, timeoutFunction, duration, intervalFunction, config) {
            // Set by the training program to disable any course timing functions
            if (Artisan.IsCourseTimingDisabled) {
                this.stop();
                return;
            }

            var me = this,
                timeUsed;

            Artisan.App.course.record.saveDataChunk();

            me.stop();

            me.id = id;
            timeUsed = Artisan.App.course.record.getTime(me.typeChar, me.id);
            duration = duration - timeUsed;
            me.timeRemaining = duration;

            if (me.timeRemaining > 0) {
                me.timeoutFunction = timeoutFunction;
                me.startTimeout(me.timeRemaining)
                me.running = true;
            }

            if (config) {
                me.config = config;
            }

            me.startInterval();

            me.updateDisplay();
        },

        autoLogout: function (time) {
            if (this.type && this.id) {
                Artisan.App.course.record.addTime(this.typeChar, this.id, -time);
                Artisan.App.course.record.saveDataChunk();
            }
        },

        stop: function () {
            Artisan.App.course.record.saveDataChunk();

            this.running = false;

            clearTimeout(this.timeout);
            this.timeout = null;

            this.timeRemaining = 0;
            this.updateDisplay();
        }
    });

    // a Container is an object with sections and pages
    Artisan.Models.Container = Class.extend({
        init: function (config, parent) {
            this.config = config;
            this.sections = [];
            this.pages = [];
            this.parent = parent;
            if (config.sections) {
                for (var i = 0; i < config.sections.length; i++) {
                    this.sections.push(new models.Container(config.sections[i], this));
                }
            }
            if (config.pages) {
                for (var i = 0; i < config.pages.length; i++) {
                    if (config.pages[i].isReview) {
                        this.pages.push(new models.ReviewPage(this, config.pages[i].sectionId, config.pages[i].record));
                    } else {
                        this.pages.push(new models.Page(this, config.pages[i]));
                    }
                }
            }
        },
        isMenuVisible: function () {
            return (this.config && this.config.menuVisible === false ? false : true);
        },
        getName: function () {
            return this.config.name;
        },
        getId: function () {
            return this.config.id;
        },
        getSectionAt: function (index) {
            return this.sections[index];
        },
        getSection: function (id) {
            if (this.getId() == id) {
                return this;
            }
            for (var i = 0; i < this.sections.length; i++) {
                if (this.sections[i].getId() == id) {
                    return this.sections[i];
                } else if (this.sections[i].sections) {
                    var result = this.sections[i].getSection(id);
                    if (result) {
                        return result;
                    }
                }
            }
            return null;
        },
        getSectionByName: function (name) {
            if (this.getName() == name) {
                return this;
            }
            for (var i = 0; i < this.sections.length; i++) {
                if (this.sections[i].getName() == name) {
                    return this.sections[i];
                } else if (this.sections[i].sections) {
                    var result = this.sections[i].getSectionByName(name);
                    if (result) {
                        return result;
                    }
                }
            }
            return null;
        },
        getFirstSectionWithPages: function () {
            if (this.hasPages()) {
                return this;
            }
            if (this.sections && this.sections.length) {
                for (var i = 0; i < this.sections.length; i++) {
                    var result = this.sections[i].getFirstSectionWithPages();
                    if (result) {
                        return result;
                    }
                }
            }
            return null;
        },
        getLastSectionWithPages: function () {
            // Ignore survey section, this must be last
            if (this.config.sectionType && this.config.sectionType == ArtisanSectionType.Survey) {
                return null;
            }

            if (this.hasPages()) {
                return this;
            }
            if (this.sections && this.sections.length) {
                for (var i = 0; i < this.sections.length; i++) {
                    var result = this.sections[this.sections.length - 1 - i].getLastSectionWithPages();
                    if (result) {
                        return result;
                    }
                }
            }
            return null;
        },
        getSections: function () {
            return this.sections;
        },
        getParent: function () {
            return this.parent;
        },
        hasPages: function () {
            return (this.pages ? this.pages.length > 0 : false);
        },
        getPage: function (id) {
            var page = null;
            if (this.pages) {
                for (var i = 0; i < this.pages.length; i++) {
                    if (this.pages[i].getId() == id) {
                        page = this.pages[i];
                        break;
                    }
                }
            }
            if (!page && this.sections) {
                for (var i = 0; i < this.sections.length; i++) {
                    page = this.sections[i].getPage(id);
                    if (page) {
                        break;
                    }
                }
            }
            return page;
        },
        getPageAt: function (index) {
            return this.pages[index];
        },
        getLastPage: function () {
            return this.getPageAt(this.pages.length - 1);
        },
        getFirstPage: function () {
            return this.getPageAt(0);
        },
        getFirstQuestionPage: function () {
            for (var i = 0; i < this.pages.length; i++) {
                var page = this.getPageAt(i);
                if (page.isQuestion()) {
                    return page;
                }
            }
            return null;
        },
        getNextQuestionPage: function (id) {
            for (var i = 0; i < this.pages.length; i++) {
                var page = this.getPageAt(i);
                if (page && page.getId() == id) {
                    for (var j = i; j < this.pages.length; j++) {
                        page = this.getPageAt(j);
                        if (page.isQuestion()) {
                            return page;
                        }
                    }
                }
            }
            return null;
        },
        getNextPage: function (id) {
            for (var i = 0; i < this.pages.length; i++) {
                var page = this.getPageAt(i);
                if (page && page.getId() == id) {
                    return this.getPageAt(i + 1);
                }
            }
        },
        getPreviousPage: function (id) {
            for (var i = 0; i < this.pages.length; i++) {
                var page = this.getPageAt(i);
                if (page && page.getId() == id) {
                    return this.getPageAt(i - 1);
                }
            }
        },
        isTestSection: function () {
            return this.config.sectionType == ArtisanSectionType.Posttest || this.config.sectionType == ArtisanSectionType.Pretest;
        },
        isPretest: function () {
            return this.config.sectionType == ArtisanSectionType.Pretest;
        },
        isTestForceRestart: function () {
            var parent = this.getParent();

            if (!parent) {
                parent = this;
            }

            while (parent && !parent.config.hasOwnProperty('completionMethod')) {
                parent = parent.getParent();
            }

            return parent.config.completionMethod == ArtisanCourseCompletionMethod.TestForceRestart &&
                   this.isTestSection() &&
                   !this.isPretest();
        },
        getTotalPageCount: function () {
            return this.getAllPages().length;
        },
        getTotalPageIndex: function (learningObjectId, pageId, randomizedLearningObjectOverride) {
            var allPages = this.getAllPages(null, randomizedLearningObjectOverride);
            for (var i = 0; i < allPages.length; i++) {
                if (allPages[i].parent.getId() == learningObjectId && allPages[i].getId() == pageId) {
                    return i;
                }
            }
            return -1;
        },
        getAllPages: function (section, randomizedLearningObjectOverride) {
            var total = [];
            if (!section) {
                section = this;
            }
            if (section.pages && section.pages.length) {
                if (randomizedLearningObjectOverride && section.getId() == randomizedLearningObjectOverride.getId()) {
                    total = total.concat(randomizedLearningObjectOverride.pages);
                } else {
                    total = total.concat(section.pages);
                }
            }
            // loop through all sub-sections to get their pages
            if (section.sections && section.sections.length) {
                for (var i = 0; i < section.sections.length; i++) {
                    total = total.concat(this.getAllPages(section.sections[i], randomizedLearningObjectOverride));
                }
            }
            return total;
        },
        isQuizComplete: function () {
            var lo = this;

            if (lo.config.isQuiz) {
                var answeredQuestions = Artisan.App.course.record.answeredQuestions;
                var id = lo.getId();
                var answeredQuizQestions = $.grep(answeredQuestions, function (question) { return question.learningObjectId == id && question.isCorrect });
                var pages = lo.config.isSinglePage ? lo.config.combinedPages : lo.pages;
                var questionCount = $.grep(pages, function (page) { return page.config && page.config.questionType > 0 || page.questionType > 0 }).length;
                var maxQuestions = lo.config.maxQuestions;
                var totalQuestions = maxQuestions > 0 ? maxQuestions : questionCount;

                if (answeredQuizQestions.length >= totalQuestions) {
                    return true;
                }
            }

            return false;
        },
        isMasterySection: function () {
            return this.getId() === masterySectionId;
        }
    });

    // a content or question object
    Artisan.Models.Page = Class.extend({
        init: function (parent, config) {
            this.parent = parent;
            this.config = config;
            if (!this.config.id)
                this.config.id = Artisan.Models.Page.generateId();
        },
        getHtml: function () {
            if (!this.isContentPage()) {
                var template = $('.artisan-page-wrap');

                // remove any sub #main wrappers that might have been 
                // added in the build process (content pages have
                // the #main wrapper embedded into their template)
                // will only be found if isSinglePage is configured
                // for the section and this page is a group of pages
                if (this.parent.config.isSinglePage) {
                    var content = $('<div>').html(this.config.html);
                    var wrappers = content.find('#Main .MainWrapper');



                    if (wrappers.length) {
                        $.each(wrappers, function (i, o) {
                            var w = $(o);
                            w.parent().replaceWith(w.find('.ContentWrapper').children());
                        });

                        this.config.html = content.html();
                    }
                }

                return template.html().replace('{!content}', this.config.html);
            }
            else { // content pages already have wrapper divs
                return this.config.html;
            }
        },
        getRawHtml: function () {
            return this.config.html;
        },
        getHtmlContent: function () {
            return Artisan.Utilities.extractContent(this.config.html);
        },
        isGradable: function () {
            return this.config.isGradable;
        },
        isGroup: function () {
            return this.config.isGroup;
        },
        isInQuiz: function () {
            return this.parent.config.isQuiz
        },
        getName: function () {
            return this.config.name;
        },
        getId: function () {
            return this.config.id;
        },
        getQuestionType: function () {
            return this.config.questionType;
        },
        getReferencePageId: function () {
            return this.config.referencePageId;
        },
        getIncorrectFeedback: function (userAnswers) {
            if (!userAnswers) {
                return this.config.incorrectResponse;
            }

            var answerSpecificFeedback = this.getAnswerSpecificFeedback(userAnswers);

            return answerSpecificFeedback ? answerSpecificFeedback : this.config.incorrectResponse;
        },
        getCorrectFeedback: function (userAnswers) {
            if (!userAnswers) {
                return this.config.correctResponse;
            }

            var answerSpecificFeedback = this.getAnswerSpecificFeedback(userAnswers);

            return answerSpecificFeedback ? answerSpecificFeedback : this.config.correctResponse;
        },
        getAnswerSpecificFeedback: function (userAnswers) {
            var answerSpecificFeedback = "";

            var answers = $.grep(this.config.answers, function (answer) {
                return $.inArray(answer.id.toString(), userAnswers) > -1;
            });

            for (var i = 0; i < answers.length; i++) {

                if (answers[i].feedback) {
                    answerSpecificFeedback += (answerSpecificFeedback ? "<br/>" : "") + answers[i].feedback;
                }
            }

            return answerSpecificFeedback;
        },
        checkAnswers: function (userAnswers) {
            switch (this.config.questionType) {
                case ArtisanQuestionType.LongAnswer:
                    var results = { correct: [], incorrect: [] };
                    // Just assume this is correct for now.
                    results.correct.push(this.config.answers[0]);
                    break;
                case ArtisanQuestionType.MultipleChoice:
                case ArtisanQuestionType.TrueFalse:
                case ArtisanQuestionType.AllThatApply:
                    var results = { correct: [], incorrect: [] };
                    for (var i = 0; i < this.config.answers.length; i++) {
                        var found = false;
                        for (var j = 0; j < userAnswers.length; j++) {
                            if (userAnswers[j] == this.config.answers[i].id) {
                                if (this.config.answers[i].isCorrect) {
                                    // found in the list from the user, and correct
                                    results.correct.push(this.config.answers[i]); // Answer selected by the user, and the answer *should have been* selected � GREEN CHECK
                                } else {
                                    // found in the list from the user, but not correct
                                    results.incorrect.push(this.config.answers[i]); // Answer selected by the user, and the answer *should not have been* selected � RED X
                                }
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            if (this.config.answers[i].isCorrect) {
                                // not found, but should have been
                                results.incorrect.push(this.config.answers[i]);
                            } else {
                                // not found, and shouldn't have been
                                results.correct.push(this.config.answers[i]); // Answer *not* selected by the user, and the answer *should not have been* selected - NOTHING
                            }
                        }
                    }
                    break;
                case ArtisanQuestionType.FillInTheBlank:
                    var results = { correct: [], incorrect: [] };
                    for (var i = 0; i < this.config.answers.length; i++) {
                        var found = false;
                        for (var j = 0; j < userAnswers.length; j++) {
                            if (Artisan.Utilities.trim(userAnswers[j]).toLowerCase() == Artisan.Utilities.trim(this.config.answers[i].text).toLowerCase()) {
                                results.correct.push(this.config.answers[i]);
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            results.incorrect.push(this.config.answers[i]);
                        }
                    }
                    break;
                case ArtisanQuestionType.ImageQuestion:
                    var results = { correct: [], incorrect: [] };
                    var answers = JSON.parse(this.config.answers[0].text).dimensions;

                    for (var i = 0; i < answers.length; i++) {
                        var answer = answers[i];
                        var found = false;
                        for (var j = 0; j < userAnswers.length; j++) {
                            var userAnswer = userAnswers[j];

                            if ((userAnswer.x >= answer.x && userAnswer.x <= answer.x + answer.width)
					            &&
					            (userAnswer.y >= answer.y && userAnswer.y <= answer.y + answer.height)) {

                                results.correct.push(answer);
                                userAnswer.found = true;
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            // they missed it
                            results.incorrect.push(answer);
                        }
                    }
                    // flag the ones they added that weren't correct
                    for (var i = 0; i < userAnswers.length; i++) {
                        var userAnswer = userAnswers[i];
                        if (!userAnswer.found) {
                            results.incorrect.push(userAnswer);
                        }
                    }
                    break;
                case ArtisanQuestionType.Matching:
                    var results = { correct: [], incorrect: [] };
                    for (var i = 0; i < userAnswers.length; i++) {
                        // userAnswers and this.config.answers are parallel arrays
                        if (userAnswers[i].answerId == userAnswers[i].userAnswer) {
                            results.correct.push(this.config.answers[i]);
                        }
                        else {
                            results.incorrect.push(this.config.answers[i]);
                        }
                    }
                    break;
                case ArtisanQuestionType.CategoryMatching:
                    var results = { correct: [], incorrect: [] };
                    // Loop through the categories
                    for (var i = 0; i < this.config.answers.length; i++) {
                        var correct = true;
                        for (var j = 0; j < userAnswers.length; j++) {
                            var matchIds = userAnswers[j].matchId.split('-');
                            var ansId = matchIds[0];
                            var matchIndex = matchIds[1];

                            // Check all items that belong in this category to make sure none are missing
                            var belongsInThisCategory = (this.config.answers[i].id == ansId);

                            // Check all items dragged into this category to make they all belong
                            var draggedToThisCategory = (this.config.answers[i].id == userAnswers[j].userAnswer);

                            if (belongsInThisCategory || draggedToThisCategory) {
                                if (ansId != userAnswers[j].userAnswer) {
                                    correct = false;
                                    break;
                                }
                            }
                        }

                        if (correct) {
                            results.correct.push(this.config.answers[i]);
                        }
                        else {
                            results.incorrect.push(this.config.answers[i]);
                        }
                    }
                    break;
            }
            return results;
        },
        isAllThatApply: function () {
            return this.config.questionType == ArtisanQuestionType.AllThatApply;
        },
        isMultipleChoice: function () {
            return this.config.questionType == ArtisanQuestionType.MultipleChoice;
        },
        isTrueFalse: function () {
            return this.config.questionType == ArtisanQuestionType.TrueFalse;
        },
        isFillInTheBlank: function () {
            return this.config.questionType == ArtisanQuestionType.FillInTheBlank;
        },
        isImageQuestion: function () {
            return this.config.questionType == ArtisanQuestionType.ImageQuestion;
        },
        isMatching: function () {
            return this.config.questionType == ArtisanQuestionType.Matching;
        },
        isCategoryMatching: function () {
            return this.config.questionType == ArtisanQuestionType.CategoryMatching;
        },
        isLongAnswer: function () {
            return this.config.questionType == ArtisanQuestionType.LongAnswer;
        },
        isQuestion: function () {
            return (this.config.questionType ? true : false);
        },

        isIntroduction: function () {
            return this.config.isIntro;
        },
        isGameStart: function () {
            return this.config.isGameStart;
        },
        isReviewPage: function () {
            return this.isReview;
        },
        isContentPage: function () {
            return (this.config.pageType == ArtisanPageType.Content);
        },
        isSurveyPage: function () {
            return (this.config.pageType == ArtisanPageType.Survey);
        },
        getCourse: function () {
            var entity = this.parent;
            while (entity.parent != null) {
                entity = entity.parent;
            }
            return entity;
        },
        getAnswerIndexFromSortIndex: function (sort) {
            var indexes = this.getAnswerIndexesFromSortIndexes([sort]);
            if (indexes.length > 0) {
                return indexes[0];
            }
            return -1;
        },
        getAnswerIndexesFromSortIndexes: function (sortArray) {
            var result = [];

            for (var j = 0; j < sortArray.length; j++) {
                var sort = sortArray[j];
                var sortFound = false;

                for (var i = 0; i < this.config.answers.length; i++) {
                    var answer = this.config.answers[i];
                    if (answer.sort == sort) {
                        if (answer.isRandomized && answer.randomizedSortIndex >= 0 && answer.randomizedSortIndex < this.config.answers.length) {
                            // On randomize, we are storing the new index position on the answer config object. So use this instead.
                            result.push(answer.randomizedSortIndex);
                        } else {
                            result.push(i);
                        }
                        sortFound = true;
                        break;
                    }
                }

                if (sortFound) {
                    continue;
                }

                if (sort >= 0 && sort < this.config.answers.length) {
                    result.push(sort);
                    continue;
                }

                if (sort == this.config.answers.length) {
                    result.push(this.config.answers.length - 1);
                    continue;
                }

                result.push(-1);
            }

            return result;
        }
    });

    // static members for Page class
    Artisan.Models.Page.idCounter = -1;
    Artisan.Models.Page.generateId = function () {
        return Artisan.Models.Page.idCounter--;
    };

    // a special type of Page
    Artisan.Models.ReviewPage = Artisan.Models.Page.extend({
        init: function (parent, sectionId, record) {
            this.isReview = true;
            this.sectionId = sectionId; // needed so we know which answers to review
            this.record = record;
            this._super(parent, {}); // no config to pass in
        },

        getHtml: function () {
            return $('.artisan-page-wrap').html().replace('{!content}', this.getRawHtml());
        },
        getRawHtml: function () {
            var section = this.record.course.getSection(this.sectionId);
            if (!section || !section.pages)
                return;

            var isPretest = section.config.sectionType == ArtisanSectionType.Pretest;
            var isPosttest = section.config.sectionType == ArtisanSectionType.Posttest;
            var isQuiz = section.config.isQuiz;
            var isNavigationCompletion = this.record.course.isNavigationMethod();
            var isSurveyCompletion = this.record.course.isSurveyMethod();
            var isTestOut = section.config.testOut;
            var isCertificateEnabeled = this.record.course.config.certificateEnabled;

            var templateCls = (isPretest ?
                                (isTestOut ? 'artisan-review-pretest-testout' : 'artisan-review-pretest') :
                                (isQuiz && !Artisan.App.state.isPreviousPageReview ? 'artisan-review-quiz' : 'artisan-review-posttest'));



            if (isNavigationCompletion && !isSurveyCompletion && (!isQuiz || Artisan.App.state.isPreviousPageReview)) {
                // override only if this is the last review page and the last section
                // of the course. Otherwise it might be a review for a quiz
                templateCls = 'artisan-review-navigation';
            } else if (isSurveyCompletion && (!isQuiz || Artisan.App.state.isPreviousPageReview)) {
                // override only if this is the last review page and the last section
                // of the course. Otherwise it might be a review for a quiz, though
                // for surveys.... there really shouldn't be a quiz in the course!
                templateCls = 'artisan-review-survey';
            }

            // Show nocert version if no cert is enabled
            // Should only be needed for artisan-review-pretest-testout, artisan-review-posttest, and artisan-review-navigation
            // artisan-review-quiz and artisan-review-pretest never show certs
            if (templateCls !== 'artisan-review-pretest' && templateCls !== 'artisan-review-quiz' && !isCertificateEnabeled) {
                templateCls += '-nocert';
            }

            var template = $('.' + templateCls).html();
            if (section.config.finalContentPage) {
                template = template.replace('{!final_content_page}', section.config.finalContentPage);
            } else {
                if (section.parent.config.finalContentPage) {
                    template = template.replace('{!final_content_page}', section.parent.config.finalContentPage);
                } else {
                    if (section.parent.parent.config.finalContentPage) {
                        template = template.replace('{!final_content_page}', section.parent.parent.config.finalContentPage);
                    } else {
                        template = template.replace('<div class="p final-content-page">{!final_content_page}</div>', '');
                    }
                }
            }

            this.record.buildAnswerCache();

            var reviewData = this.record.calculateReview(this.sectionId);

            // Insert by Nu
            if (isPretest) {
                LMS.SetSpeedPreference(reviewData.score);
            }

            var tempPreTestScore = 0;
            tempPreTestScore = LMS.GetSpeedPreference();
            if (!(tempPreTestScore != undefined || tempPreTestScore != null || tempPreTestScore != "")) {
                tempPreTestScore = 0;
            }

            if (tempPreTestScore < 0) {  	//just in case it's negative
                tempPreTestScore = 0;
            }
            var isMasteryComplete = this.record.course.hasMastery() && this.record.isMastered();
            var isMasteryCompleteOrDisabled = !this.record.course.hasMastery() || isMasteryComplete;

            // If mastery is not enabled, save the score based on the usual logic
            // If mastery is enabled, only save the score when mastery is complete
            if (!(isPretest && !isTestOut) && !(isQuiz && !Artisan.App.state.isPreviousPageReview) && isMasteryCompleteOrDisabled || isMasteryComplete) { // don't set score or status if it's a non-testout pretest, or quiz
                // OK, so, apparently some LMSes see the min/max as not pass/fail values, but as min/max the *student* got and then somehow add them together???
                // soooo, we set to 100, 0 to ensure that'll always set to 100
                LMS.SetScore(reviewData.score, 100, 0);

                // ok, we abuse the speed preference here
                LMS.SetSpeedPreference(tempPreTestScore);

                if (reviewData.passed) {
                    LMS.SetPassed();
                } else {
                    LMS.SetFailed();
                }

                LMS.CommitData();
            }

            var reviewQuestionsTemplate = $('.artisan-review-missed-questions').html() || '';

            var templateParams = {};
            var href = decodeURIComponent(window.top.location.href);
            var params = href.slice(href.indexOf('?') + 1).split('&');

            for (var i = 0; i < params.length; i++) {
                var param = params[i].split('=');
                if (param && param.length && param.length >= 2) {
                    var innerParams = param[1].split('!');

                    for (var j = 0; j < innerParams.length; j++) {
                        var innerParam = innerParams[j].split('|');
                        templateParams[innerParam[0]] = innerParam[1];
                    }
                }
            }

            var certificateUrl = "certificate.html?score={!score}&name={!user}&course={!course}";
            if (Artisan.App.course.config.useSymphonyCertificate) {
                certificateUrl = Artisan.App.course.config.certificateUrl;
            }

            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!score}', reviewData.score);
            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!user}', LMS.GetStudentName() || 'Student Name');
            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!course}', this.getCourse().getName());
            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!course_id}', templateParams.CourseId);
            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!training_program_id}', templateParams.TrainingProgramId || 0);
            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!user_id}', templateParams.UserId || 0);

            var templates = [template, reviewQuestionsTemplate];
            for (var i = 0; i < templates.length; i++) {
                var t = templates[i];
                t = Artisan.Utilities.replaceAll(t, '{!answered_count}', reviewData.answered);
                t = Artisan.Utilities.replaceAll(t, '{!correct_count}', reviewData.correct);
                t = Artisan.Utilities.replaceAll(t, '{!total_count}', reviewData.total);
                t = Artisan.Utilities.replaceAll(t, '{!score}', reviewData.score);
                t = Artisan.Utilities.replaceAll(t, '{!passing_score}', reviewData.passingScore);
                t = Artisan.Utilities.replaceAll(t, '{!user}', LMS.GetStudentName() || 'Student Name');
                t = Artisan.Utilities.replaceAll(t, '{!course}', this.getCourse().getName());
                t = Artisan.Utilities.replaceAll(t, '{!certificate_url}', certificateUrl);
                templates[i] = t;
            }
            template = templates[0];
            reviewQuestionsTemplate = templates[1];

            if (isPretest) {
                template = Artisan.Utilities.replaceAll(template, '{!review_questions}', '');
            } else {
                template = Artisan.Utilities.replaceAll(template, '{!review_questions}', reviewQuestionsTemplate);
            }

            // Delete the answers so test can be taken again
            if (isQuiz && !Artisan.App.state.isPreviousPageReview && !isMasteryComplete) {
                // this will not reset the section if it is a mastery section. 
                this.record.resetQuiz(section.config.id, reviewData.passed);
            } else {
                //this.record.reset();

                // This should send all the data to scorm
                // and trigger the course complete flags
                if (!isPretest && window.top.API && window.top.API.LMSFinish) {

                    if (this.record.course.hasMastery() && this.record.isMastered() || !this.record.course.hasMastery()) {
                        var api = window.top.API;
                        var control = window.top.Control;

                        var historyAtts = { ev: 'ApiTerminate' };
                        if (api.Activity) {
                            historyAtts.ai = api.Activity.ItemIdentifier;
                        }

                        api.WriteHistoryLog("", historyAtts);
                        api.CloseOutSession();
                        api.SetDirtyData();
                        control.Comm.SaveDataNow(true);
                        api.Initialized = false;
                        api.ScoCalledFinish = true;
                    }

                    // Turn off any other controls so we cannot go back 
                    // to the rest of the course. If we do go back
                    // scorm will no longer work since we've closed everything
                    //
                    // Disabling during mastery too, should be clearner overall that way. 
                    $('.artisan-previous').addClass('disabled');
                    $('#artisan-menu').hide();
                }
            }

            return template;
        },
        getHtmlContent: function () {
            return this.getHtml();
        },
        isPassed: function () {
            var reviewData = this.record.calculateReview(this.sectionId);
            return reviewData.passed;
        }
    });


    // global id counter
    var id = -1;
    var masterySectionId = -999;

    // a special type of Container
    Artisan.Models.Course = Artisan.Models.Container.extend({
        init: function (config, dataChunk) {
            id = -1; // Make sure the id always starts at -1 when the course is built.

            if (Artisan.IsScormDisabled) {
                LMS = new parent.LMSStandardAPI('NONE');
            }

            this.record = new Artisan.Models.CourseRecord(this, dataChunk);

            for (var i = 0; i < config.sections.length; i++) {
                // the item we're adding is a single-level SCO (such as pre-test, objectives, etc)
                if (config.sections[i].pages && config.sections[i].pages.length > 0) {

                    var sectionType = config.sections[i].sectionType;
                    var testType = config.sections[i].testType;

                    // if pretest or posttest, adjust for test type and add a review page
                    if (sectionType == ArtisanSectionType.Pretest || sectionType == ArtisanSectionType.Posttest) {

                        // recordthe fact that the first page of a test is an intro page
                        config.sections[i].pages[0].isIntro = true;

                        var isReRandomize = config.sections[i].isReRandomizeOrder;

                        // Make sure that this is a clean attempt - only want to re-randomize fresh attempts at the test
                        // otherwise we can possibly run into issues where the student can score more than 100%
                        if (isReRandomize) {
                            if (this.record.answeredQuestions.length > 0) {
                                isReRandomize = false;
                            }
                        }

                        // if Random, GameBoard, or GameWheel, use 20 random questions
                        if (testType == ArtisanTestType.Random || testType == ArtisanTestType.GameBoard || testType == ArtisanTestType.GameWheel) {
                            var origPages = Artisan.Utilities.clone(config.sections[i].pages);
                            var questionPages = [];

                            if (sectionType == ArtisanSectionType.Pretest && this.record.pretestOrder && !isReRandomize) {
                                $.each(this.record.pretestOrder, function (index, id) {
                                    var page = $.grep(config.sections[i].pages, function (p) { return p.id == id; })[0];
                                    questionPages.push(page);
                                });
                            }
                            else if (sectionType == ArtisanSectionType.Posttest && this.record.posttestOrder && !isReRandomize) {
                                $.each(this.record.posttestOrder, function (index, id) {
                                    var page = $.grep(config.sections[i].pages, function (p) { return p.id == id; })[0];
                                    questionPages.push(page);
                                });
                            }
                            else {
                                this.randomizePages(config.sections[i]);

                                var questionPages = config.sections[i].pages.slice(config.sections[i].pages[0].isIntro ? 1 : 0,
                                                                             config.sections[i].pages[config.sections[i].pages.length - 1].isReview ? config.sections[i].pages.length - 1 : config.sections[i].pages.length);

                                if (sectionType == ArtisanSectionType.Pretest)
                                    this.record.pretestOrder = $.map(questionPages, function (p) { return p.id; });
                                else if (sectionType == ArtisanSectionType.Posttest)
                                    this.record.posttestOrder = $.map(questionPages, function (p) { return p.id; });
                            }
                            config.sections[i].pages = []; // rebuild the pages array with randomly ordered pages
                            config.sections[i].pages.push(origPages[0]); // start with the intro page
                            if (testType == ArtisanTestType.GameBoard)
                                config.sections[i].pages.push({ html: "Select a question from the Gameboard to get started!", isGameStart: true });
                            if (testType == ArtisanTestType.GameWheel)
                                config.sections[i].pages.push({ html: "Click the Spin the Wheel button to get started!", isGameStart: true });
                            for (var j = 0; j < questionPages.length; j++) {
                                config.sections[i].pages.push(questionPages[j]); // add the random/trimmed question pages
                            }
                        }

                        // add the test review page
                        config.sections[i].pages.push(new models.ReviewPage(config.sections[i], config.sections[i].id, this.record));
                    }

                    // We do a special wrap;
                    // since the item we're adding is a single-level SCO (such as pre-test, objectives, etc), 
                    // then we'll put it in a container SCO with the same name
                    // this way, we can always assume we're dealing with at least 2 levels of nesting
                    var original = config.sections[i];
                    config.sections[i] = Artisan.Utilities.clone(config.sections[i]);
                    delete config.sections[i].pages;
                    config.sections[i].sections = [original];
                    config.sections[i].isWrapper = true;
                    config.sections[i].id = id--;
                } else {
                    // Look for quizes
                    var courseRecord = this.record;
                    var me = this;
                    var sectionSearch = function (section) {
                        for (var j = 0; j < section.sections.length; j++) {
                            var innerSection = section.sections[j];

                            if (innerSection.sections && innerSection.sections.length) {
                                sectionSearch(innerSection);
                            }

                            if (innerSection.pages && innerSection.pages.length > 0 && innerSection.isQuiz) {
                                innerSection.pages[0].isIntro = true;
                                innerSection.pages.push(new models.ReviewPage(innerSection, innerSection.id, courseRecord));

                                if (innerSection.isRandomizeQuizQuestions) {
                                    me.randomizePages(innerSection);
                                }
                            }
                        }
                    }
                    if (config.sections[i].sections && config.sections[i].sections.length) {
                        sectionSearch(config.sections[i]);
                    }
                }
            }

            this._super(config);

            // if completion is Inline Questions then add a review page to the end of the last section
            // we don't need to worry about a posttest being at the end because it would be removed at package time
            if (config.completionMethod == ArtisanCourseCompletionMethod.InlineQuestions || config.completionMethod == ArtisanCourseCompletionMethod.Navigation || config.completionMethod == ArtisanCourseCompletionMethod.Survey) {
                var section = this.getLastSectionWithPages();
                if (section != null) {
                    section.pages.push(new models.ReviewPage(section, section.getId(), this.record));
                }
            }
        },
        buildMasterySection: function (isResume) {
            if (!this.masterySection) {
                var masterySco = new Artisan.Models.Container({
                    id: -99999,
                    name: 'Mastery',
                    sectionType: ArtisanSectionType.Sco,
                    sections: [{
                        isMastery: true,
                        id: masterySectionId,
                        menuVisible: false,
                        isStateless: true,
                        isQuiz: true,
                        learningObjectId: masterySectionId,
                        name: 'Mastery'
                    }]
                }, this);

                this.masterySection = masterySco.sections[0];

                this.sections.push(masterySco);
            }
            this.masterySection.pages = [];

            this.buildMasteryPages(isResume);
        },
        buildMasteryPages: function (isResume) {
            var pages = this.getQuestionPages(this, true), // All gradable questions in the course
                // Passed mastery questions from the previous mastery round (if there is one)
                passedMasteryQuestions = $.grep(this.record.answeredQuestions, function (f) { return f.learningObjectId == masterySectionId && f.isCorrect }),
                // All questions from the attempt(s) that were previously failed (Mastery Level one - any questions that was not correct, Mastery Level Two - any question that was not correct in at least 1 attempt)
                questionPagesToShow = [],
                // Number of times a question was answered correctly. This will update with the current mastery round, if it hasn't been counted yet. 
                passedQuestionAttempts = this.record.getPassedQuestionAttempts(),
                // Mastery Level 1 - All attempt counts need to be at least 1
                // Mastery Level 2 - All attempt counts need to be at least 2
                requiredCount = this.isMasteryLevelTwo() ? 2 : 1,
                // the left over pages that arn't required
                leftOverPages = [],
                // minimum pages to display
                minimum = this.getMinimumMasteryQuestions();

            if (isResume && this.record.masteryQuestionOrder && this.record.masteryQuestionOrder.length > 0) {
                // When resuming, use the same page order as stored in the boomker, if the order exists.
                // This will alow resuming to a random page and keeping the mastery section in the same order.
                for (var questionIndex = 0; questionIndex < this.record.masteryQuestionOrder.length; questionIndex++) {
                    questionPagesToShow.push(this.getPage(this.record.masteryQuestionOrder[questionIndex]));
                }
            } else {
                // can't have questions in there 2x, so limit to # of pages left
                if (minimum > pages.length) {
                    minimum = pages.length;
                }

                // Grab all the pages where we have not answered the question correctly the required number of times
                for (var i = 0; i < pages.length; i++) {
                    if (!passedQuestionAttempts[pages[i].getId()] || passedQuestionAttempts[pages[i].getId()].attempts < requiredCount) {
                        questionPagesToShow.push(pages[i]);
                    } else {
                        leftOverPages.push(pages[i]);
                    }
                }

                // Grab from the left over pages to reach the minimum
                if (questionPagesToShow.length < minimum) {
                    i = questionPagesToShow.length;

                    var min = 0;
                    while (i < minimum) {
                        var max = leftOverPages.length;
                        var idx = Math.floor(Math.random() * (max - min) + min);
                        var page = leftOverPages.splice(idx, 1)[0];
                        questionPagesToShow.push(page);
                        i++;
                    }
                }
            }

            // now we build a one-off learning object for this temporary mastery thing
            var masterySection = this.getMasterySection();

            this.record.masteryQuestionOrder = [];

            for (var j = 0; j < questionPagesToShow.length; j++) {
                var pageConfig = Artisan.Utilities.clone(questionPagesToShow[j].config);

                // non-gradable so it doesn't interfere with inline questions
                pageConfig.isGradable = false;

                // ensure we re-randomize
                pageConfig.randomizedOrder = null;

                masterySection.pages.push(new Artisan.Models.Page(masterySection, pageConfig));
                this.record.masteryQuestionOrder.push(questionPagesToShow[j].getId());
            }

            // We need a review page at the end of the mastery section in order to trigger processing. 
            var courseReviewPage = new models.ReviewPage(masterySection, masterySection.getId(), this.record);

            masterySection.pages.push(courseReviewPage);
        },
        // Takes a section config object. If calling this a Artisan.Models.Container
        // pass in the Artisan.Models.Container.config parameters. This will rebuild
        // the configuration, then you need to recreate the Artisan.Models.Container.
        randomizePages: function (sectionConfig, max) {
            max = max ? max : sectionConfig.maxQuestions ? sectionConfig.maxQuestions : 20;

            var hasIntro = sectionConfig.pages[0].isIntro;
            var hasReview = sectionConfig.pages[sectionConfig.pages.length - 1].isReview;
            var currentPages = sectionConfig.pages.slice(hasIntro ? 1 : 0, hasReview ? sectionConfig.pages.length - 1 : sectionConfig.pages.length);
            var originalPages, usedPages;

            if (sectionConfig.originalPages) {
                originalPages = sectionConfig.originalPages;
            } else {
                originalPages = currentPages;
                sectionConfig.originalPages = originalPages;
            }

            if (sectionConfig.usedPages) {
                usedPages = sectionConfig.usedPages;
            } else {
                usedPages = {};
            }

            originalPages.sort(Artisan.Utilities.randomSort);
            var usedPagesArray = [];
            var newPages = $.map(originalPages, function (p) {
                if (!usedPages.hasOwnProperty(p.id)) {
                    return p;
                } else {
                    usedPagesArray.push(p);
                }
            });

            if (newPages.length > max) {
                newPages = newPages.slice(0, max);
            } else {
                var usedPagesRequired = max - newPages.length;
                for (var i = 0; i < usedPagesRequired && i < usedPagesArray.length; i++) {
                    newPages.push(usedPagesArray[i]);
                }
                newPages.sort(Artisan.Utilities.randomSort);
            }

            $.each(newPages, function (i, p) {
                if (!usedPages.hasOwnProperty(p.id)) {
                    usedPages[p.id] = true;
                }
            });

            if (hasIntro) {
                newPages.unshift(sectionConfig.pages[0]);
            }

            if (hasReview) {
                newPages.push(new models.ReviewPage(sectionConfig, sectionConfig.id, this.record));
            }

            if (this.config && (this.config.completionMethod == ArtisanCourseCompletionMethod.InlineQuestions || this.config.completionMethod == ArtisanCourseCompletionMethod.Navigation || this.config.completionMethod == ArtisanCourseCompletionMethod.Survey)) {
                var section = this.getLastSectionWithPages();
                if (section != null && section.getId() == sectionConfig.id) {
                    newPages.push(new models.ReviewPage(section, section.getId(), this.record));
                }
            }

            sectionConfig.usedPages = usedPages;
            sectionConfig.pages = newPages;
        },
        isSequentialNavigation: function () {
            return this.config.navigationMethod == ArtisanCourseNavigationMethod.Sequential || this.config.navigationMethod == ArtisanCourseNavigationMethod.FullSequential;
        },
        isFullSequentialNavigation: function () {
            return this.config.navigationMethod == ArtisanCourseNavigationMethod.FullSequential;
        },
        isFreeFormWithQuizNavigation: function () {
            return this.config.navigationMethod == ArtisanCourseNavigationMethod.FreeFormWithQuiz;
        },
        isStandardBookmarking: function () {
            return this.config.bookmarkingMethod == ArtisanCourseBookmarkingMethod.Standard;
        },
        isAutomaticBookmarking: function () {
            return this.config.bookmarkingMethod == ArtisanCourseBookmarkingMethod.Automatic;
        },
        hasAntiGuessing: function () {
            return this.config.isAntiGuessing;
        },
        hasMastery: function () {
            return this.config.isMasteryEnabled;
        },
        getMasterySection: function () {
            return this.masterySection;
        },
        getRebuiltMasterySection: function (isResume) {
            this.buildMasterySection(isResume);
            return this.masterySection;
        },
        isMasteryLevelOne: function () {
            return this.config.masteryLevel == ArtisanMasteryLevel.LevelOne;
        },
        isMasteryLevelTwo: function () {
            return this.config.masteryLevel == ArtisanMasteryLevel.LevelTwo;
        },
        isUspap: function () {
            return this.config.isUspap;
        },
        getMinimumMasteryQuestions: function () {
            return this.config.minimumMasteryQuestions || 5;
        },
        getPassingScore: function () {
            return this.config.passingScore || 80;
        },
        setPassingScore: function (val) {
            this.config.passingScore = val;
        },
        setNavigationMethod: function (val) {
            this.config.navigationMethod = val;
        },
        setCompletionMethod: function (val) {
            this.config.completionMethod = val;
        },
        getSco: function (id) {
            return this.getSection(id);
        },
        getFirstScoWithContent: function () {
            return this.getFirstSectionWithPages().getParent();
        },
        getLastScoWithContent: function () {
            return this.getLastSectionWithPages().getParent();
        },
        getScos: function () {
            return this._getScos(this);
        },
        _getScos: function (section) {
            var result = [];
            if (!section.hasPages()) {
                var sco = section;
                result.push(sco);
                if (section.sections) {
                    sco.scos = [];
                    for (var i = 0; i < section.sections.length; i++) {
                        sco.scos = sco.scos.concat(this._getScos(section.sections[i]));
                    }
                }
            }
            return result;
        },
        getLearningObjects: function (scoId) {
            var sco = this.getSection(scoId);
            var result = [];
            for (var i = 0; i < sco.sections.length; i++) {
                var section = sco.sections[i];
                if (section.hasPages()) {
                    result.push(section);
                }
            }
            return result;
        },
        getLearningObject: function (learningObjectId) {
            return this.getSection(learningObjectId);
        },
        getNextLearningObject: function (learningObjectId) {
            return this._getNextSection(this, learningObjectId, false, 'forward', function (section) {
                return section.hasPages();
            });
        },
        getPreviousLearningObject: function (learningObjectId) {
            return this._getNextSection(this, learningObjectId, false, 'reverse', function (section) {
                return section.hasPages();
            });
        },
        getNextSco: function (scoId) {
            return this._getNextSection(this, scoId, false, 'forward', function (section) {
                return !section.hasPages();
            });
        },
        getPreviousSco: function (scoId) {
            return this._getNextSection(this, scoId, false, 'reverse', function (section) {
                return !section.hasPages();
            });
        },
        getQuestionPageCount: function (section, gradableOnly, excludeQuiz) {
            if (!section) {
                section = this.config;
            }
            if (typeof gradableOnly == 'undefined') {
                gradableOnly = this.config.completionMethod == ArtisanCourseCompletionMethod.InlineQuestions;
            }
            if (typeof excludeQuiz == 'undefined') {
                excludeQuiz = false;
            }

            var count = 0;
            // add all pages from this section
            if (section.pages && section.pages.length && (excludeQuiz ? !section.isQuiz : true)) {
                count += $.grep(section.pages, function (p) { return p.questionType && (gradableOnly ? p.isGradable : true) }).length;
            }
            // loop through all sub-sections to get their pages
            if (section.sections && section.sections.length) {
                for (var i = 0; i < section.sections.length; i++) {
                    count += this.getQuestionPageCount(section.sections[i], gradableOnly, excludeQuiz);
                }
            }
            return count;
        },
        getQuestionPages: function (section, gradableOnly, excludeQuiz) {
            if (!section) {
                section = this.config;
            }
            if (typeof gradableOnly == 'undefined') {
                gradableOnly = this.config.completionMethod == ArtisanCourseCompletionMethod.InlineQuestions;
            }
            if (typeof excludeQuiz == 'undefined') {
                excludeQuiz = false;
            }

            var all = [];
            // add all pages from this section
            if (section.pages && section.pages.length && (excludeQuiz ? !section.isQuiz : true)) {
                all = all.concat($.grep(section.pages, function (p) { return p.getQuestionType() && (gradableOnly ? p.isGradable() : true) }));
            }
            // loop through all sub-sections to get their pages
            if (section.sections && section.sections.length) {
                for (var i = 0; i < section.sections.length; i++) {
                    var temp = this.getQuestionPages(section.sections[i], gradableOnly, excludeQuiz);
                    all = all.concat(temp);
                }
            }
            return all;
        },
        // used for review
        getFirstQuestionPage: function (section) {
            if (section.sectionType == ArtisanSectionType.Posttest || section.sectionType == ArtisanSectionType.Pretest) {
                // only look within the current section for post/pre tests
                return section.getFirstQuestionPage();
            } else {
                var total = this.getAllQuestionPages(this, true);
                return total[0];
            }
        },
        getNextQuestionPage: function (section, pageId) {
            if (section.sectionType == ArtisanSectionType.Posttest || section.sectionType == ArtisanSectionType.Pretest) {
                // only look within the current section for post/pre tests
                return section.getNextQuestionPage(pageId);
            } else {
                var total = this.getAllQuestionPages(this, true);
                for (var i = 0; i < total.length; i++) {
                    if (total[i].getId() == pageId) {
                        return total[i + 1];
                    }
                }
                return null;
            }
        },

        getAllQuestionPages: function (section, gradableOnly) {
            var total = [];
            if (!section) {
                section = this.config;
            }
            if (section.pages && section.pages.length) {
                for (var i = 0; i < section.pages.length; i++) {
                    var p = section.pages[i];
                    // question page
                    if (p.isQuestion()) {
                        // only track gradable
                        if (gradableOnly) {
                            if (p.isGradable()) {
                                total.push(p);
                            }
                        } else {
                            total.push(p);
                        }
                    }
                }
            }
            // loop through all sub-sections to get their pages
            if (section.sections && section.sections.length) {
                for (var i = 0; i < section.sections.length; i++) {
                    total = total.concat(this.getAllQuestionPages(section.sections[i], gradableOnly));
                }
            }
            return total;
        },
        _getNextSection: function (parentSection, sectionId, takeNext, direction, validator) {
            var len = parentSection.sections.length;
            for (var i = 0; i < len; i++) {
                // simple toggle to either search from the top to bottom (forward) or bottom to top (reverse) so "next" and "previous" use the same codebase
                var section = direction == 'forward' ? parentSection.sections[i] : parentSection.sections[len - i - 1];
                // if we copy an object, it'll show in our list here with the same id twice (think pre-test), so keep going if we run into the same id twice
                if (takeNext && validator(section)) {
                    return section;
                }
                if (section.getId() == sectionId) {
                    takeNext = true;
                }
                var found = this._getNextSection(section, sectionId, takeNext, direction, validator);
                if (found) {
                    return found;
                }
            }
            return null;
        },
        isNavigationMethod: function () {
            return (this.config.completionMethod == ArtisanCourseCompletionMethod.Navigation || this.config.completionMethod == ArtisanCourseCompletionMethod.Survey);
        },
        isSurveyMethod: function() {
            return (this.config.completionMethod == ArtisanCourseCompletionMethod.Survey);
        },
        isTestMethod: function () {
            return (this.config.completionMethod == ArtisanCourseCompletionMethod.Test || this.config.completionMethod == ArtisanCourseCompletionMethod.TestForceRestart);
        },
        isTestForceRestartMethod: function () {
            return (this.config.completionMethod == ArtisanCourseCompletionMethod.TestForceRestart);
        },
        isInlineQuestionsMethod: function () {
            return (this.config.completionMethod == ArtisanCourseCompletionMethod.InlineQuestions);
        },
        hasSurvey: function () {
            // Survey is always at the end
            return (this.sections && this.sections.length > 0 && this.sections[this.sections.length - 1].config.sectionType == ArtisanSectionType.Survey);
        },
        getSurveyPage: function () {
            if (this.hasSurvey() == false) {
                return null;
            }

            // Survey is always at the end, and has exactly one page
            return this.sections[this.sections.length - 1].sections[0].pages[0];
        },
        getSurveySection: function () {
            if (this.hasSurvey() == false) {
                return null;
            }

            // Survey is always at the end
            return this.sections[this.sections.length - 1];
        },
        getPopupMenu: function () {
            var popupMenu = $(this.config.popupMenu),
                title = popupMenu.attr('data-title'),
                link = $('<a>'),
                disabled = popupMenu.attr('data-disabled'),
                container = $('<div>');

            if (!title ||
                !popupMenu ||
                (disabled && disabled.toLowerCase() == 'true')) {
                return "";
            }

            link.html(title)
				.addClass('button artisan-popup-menu')
				.attr('href', '#');

            container.addClass('popup-menu-wrapper')
                .append(link)
                .append(popupMenu);

            popupMenu.hide();

            link.click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                if (popupMenu && popupMenu.toggle) {
                    popupMenu.toggle();
                }
            });

            $(document).click(function () {
                if (popupMenu && popupMenu.hide) {
                    popupMenu.hide();
                }
            });

            return container;
        },
        updateLockedStatus: function () {
            this._updateLockedStatus(this, false);
        },
        _updateLockedStatus: function (section, isLocked) {
            if (section.hasPages()) {
                section.isLocked = isLocked;

                if (!isLocked) {
                    if (this.isFullSequentialNavigation()) {
                        var previous = this.getPreviousLearningObject(section.getId());
                        var isUnlockedOverride = !section.getParent().isLocked && !previous;

                        if (!this.record.hasVisitedLo(section.getId()) && !isUnlockedOverride) {
                            section.isLocked = true;
                            isLocked = true;
                        }
                    } else if (this.isFreeFormWithQuizNavigation()) {
                        if (section.config.isQuiz && !section.isQuizComplete()) {
                            isLocked = true;
                        }
                    }
                }

                return isLocked;
            } else if (section.sections && section.sections.length) {
                section.isLocked = isLocked;

                if (this.isSequentialNavigation()) {
                    var previous = this.getPreviousSco(section.getId());

                    if (!this.record.hasVisitedSco(section.getId()) && previous) {
                        isLocked = true;
                        section.isLocked = true;
                    }
                }

                for (var i = 0; i < section.sections.length; i++) {
                    isLocked = this._updateLockedStatus(section.sections[i], isLocked);
                }

                return isLocked;
            }
        },
        // Check if a section can be unlocked based on the courses current 
        // navigation settings. 
        //
        // Note that we do not do anything here for freeFormWithQuiz
        // we don't need to do anything in this case since we no not need 
        // on the fly unlocking 
        unlockSection: function (section) {
            debugger;
            if (section.isLocked) {
                if (section.hasPages()) { // Learning Object
                    if (this.isFullSequentialNavigation()) {
                        // If full sequential, we can unlock the learning object
                        // if the last page of the previous learning object has been visited
                        // or when there is no previous learning object
                        var previousLearningObject = this.getPreviousLearningObject(section);
                        if (!previousLearningObject) {
                            section.isLocked = false; // first learning object of the course, it must be unlocked
                        } else if (this.record.hasVisitedLo(previousLearningObject.getId())) {
                            var previousPage = previousLearningObject.getLastPage();
                            if (this.record.hasVisitedPage(previousPage.getId())) {
                                section.isLocked = false; // page prior to this learning object has been visited. We can unlock
                            }
                        }
                    } else if (this.isSequentialNavigation()) {
                        // sequential is simple since we ony apply this to the scos
                        // if the sco is unlocked, then all learning objects are unlocked
                        if (!section.parent.isLocked) {
                            section.isLocked = false;
                        }
                    }
                } else { // SCO
                    if (this.isFullSequentialNavigation() || this.isSequentialNavigation()) {
                        // doesn't really matter which sequential mode this is. 
                        // The next sco will unlock when the last page of the last learning object
                        // has been visited.
                        var previousSco = this.getPreviousSco(section.getId());
                        if (!previousSco) {
                            // if it's the first sco, always unlock
                            section.isLocked = false;
                        } else if (this.record.hasVisitedSco(previousSco.getId())) {
                            var lastLearningObject = previousSco.getLastSectionWithPages();
                            var lastPage = lastLearningObject.getLastPage();
                            if (this.record.hasVisitedPage(lastPage.getId())) {
                                section.isLocked = false;
                            }
                        }
                    }
                }
            }

            // The section was either unlocked or already unlocked
            if (!section.isLocked) {
                return true;
            }

            return false;
        }
    });

    // the CourseRecord class is for recording everything a student does while taking a course
    // (unlike the Course class, which only contains information about a course independent from student interaction)
    Artisan.Models.CourseRecord = Class.extend({
        init: function (course, dataChunk) {
            this.clear();
            this.course = course;
            this.attempt = 1;
            this.previousAttemptState = null;
            if (dataChunk) {
                this.parseDataChunk(dataChunk);
            }
        },
        clear: function () {
            // be sure to mirro these properties in the saveAttempt() call
            this.answeredQuestions = [];
            this.visitedScos = {};
            this.visitedLos = {};
            this.visitedPages = {};
            this.timeSpent = {};
            this.pretestOrder = null;
            this.posttestOrder = null;
            this.needMasteryQuestionIds = null;
            this.currentMasteryQuestionId = 0;
        },
        clearMastery: function () {
            this.answeredQuestions = Artisan.Utilities.filter(this.answeredQuestions, function (x) { return x.learningObjectId != masterySectionId; });
            this.passedQuestionAttempts.isCurrentMasteryCounted = false;
        },
        answeredQuestions: null,
        pretestOrder: null,
        posttestOrder: null,
        visitedScos: null,
        visitedLos: null,
        visitedPages: null,
        timeSpent: null,
        needMasteryQuestionIds: null,
        passedQuestionAttempts: {}, // needs to persist throughout the entire course and be saved to datachunk.
        masteryMode: null,
        masteryAttempts: 0,
        masteryQuestionOrder: null, // will be reset each time we rebuild the mastery section to the current mastery order
                                    // will only be used if mastery section is rebuilt with isResume flag.  
                                    // no need to clear this
                                    // Saved to data chunk.
        getMasteryMode: function () {
            return this.masteryMode;
        },
        setMasteryMode: function (mode) {
            this.masteryMode = mode;
        },
        getMasteryAttempts: function () {
            return this.masteryAttempts;
        },
        incrementMasteryAttempts: function () {
            this.masteryAttempts++;
        },
        setMasteryAttempts: function (attempts) {
            this.masteryAttempts = attempts;
        },
        setNeedMasteryQuestionIds: function (ids) {
            this.needMasteryQuestionIds = ids;
        },
        getNeedMasteryQuestionIds: function () {
            return this.needMasteryQuestionIds;
        },
        saveAttempt: function () {
            // clone the current record
            //var record = new Artisan.Models.CourseRecord();
            var state = this.buildDataChunk();

            // clear this record
            this.clear();

            // save the previous attempt
            this.previousAttemptState = state;
            this.attempt++;

            // reset the attempt counted flag, so we actually include the second attempt in the final count. 
            // Since we don't reset the counts, and we count on every attempt, we don't need to worry
            // about counting the previous attempt anymore. 
            this.passedQuestionAttempts.isCurrentAttemptCounted = false;
        },
        // Get an object containing all of the passed questions and the number of times they have been passed
        // {
        //   isCurrentAttemptCounted: true,
        //   isPreviousAttemptCounted: true,
        //   isCurrentMasteryCounted: true,
        //   32908: { question: { }, attempts: 1 },
        //   20842: { question: { }, attempts: 2 }
        //   ...
        // }
        getPassedQuestionAttempts: function () {
            // Count the number of passed questions in the current record
            // We only want to do this once per attempt, and only on the review page!
            // This way whenever we call this function, we will get an accurate count of the 
            // current attempt. 
            // The flags will have to be reset once this attempt is complete.
            //
            var state = Artisan.App.getState();
            var updateCount = state.page && state.page.isReviewPage();
            var updateCountMastery = updateCount && state.learningObject.isMasterySection(); // mastery should only be counted when we are on the mastery section
                                                                                             // otherwise it is possible to count a mastery section prior to completion
                                                                                             // setting the flag to counted.

            if (!this.passedQuestionAttempts.isCurrentAttemptCounted && updateCount) {
                this.passedQuestionAttempts.isCurrentAttemptCounted = this.countAttempt(this.passedQuestionAttempts);
            }

            // Count the number of passed questions in the current mastery round
            // We only want to do this once per round. ClearMastery will reset this flag. 
            if (!this.passedQuestionAttempts.isCurrentMasteryCounted && updateCountMastery) {
                this.passedQuestionAttempts.isCurrentMasteryCounted = this.countAttempt(this.passedQuestionAttempts, masterySectionId);
            }

            return this.passedQuestionAttempts;
        },
        countAttempt: function (result, learningObjectId) {
            var currentPassedQuestions;
            // Only add from a specific learning object
            if (typeof (learningObjectId) != 'undefined') {
                currentPassedQuestions = $.grep(this.answeredQuestions, function (f) { return f.learningObjectId == masterySectionId && f.isCorrect });
            } else { // Add from the entire course
                currentPassedQuestions = this.getPassedQuestions();
                if (this.course.hasAntiGuessing()) {
                    var totalQuestions = this.course.getQuestionPageCount(null, true, true);

                    if ((currentPassedQuestions.length / totalQuestions) < 0.5) {
                        return false;
                    }
                }
            }
            for (var i = 0; i < currentPassedQuestions.length; i++) {
                if (!result[currentPassedQuestions[i].id]) {
                    result[currentPassedQuestions[i].id] = {
                        question: currentPassedQuestions[i],
                        attempts: 0
                    }
                };
                result[currentPassedQuestions[i].id].attempts++;
            }

            return true;
        },
        hasPreviousAttemptRecord: function () {
            return this.previousAttemptState != null;
        },
        getPreviousAttemptRecord: function () {
            var record = new Artisan.Models.CourseRecord();
            record.init(this.course, this.previousAttemptState);
            return record;
        },
        getIsMasteryActive: function () {
            var bookmark = this.parseBookmark(this.getBookmark());
            var state = Artisan.App.getState();

            return (bookmark.scoId == masterySectionId || bookmark.loId == masterySectionId) ||
                   ((state.sco && state.sco.getId() == masterySectionId) || (state.learningObject && state.learningObject.getId() == masterySectionId))
        },
        getCurrentMasteryQuestionId: function () {
            return this.currentMasteryQuestionId;
        },
        setCurrentMasteryQuestionId: function (v) {
            this.currentMasteryQuestionId = v;
        },
        setNeedMasteryQuestionIds: function (v) {
            this.needMasteryQuestionIds = v;
        },
        isMastered: function () {
            return this.isMasteryLevelOneComplete || this.isMasteryLevelTwoComplete;
        },
        recordInteraction: function (state, userAnswers, pageContext) {
            var page = pageContext ? pageContext : state.page;
            var id = page.getId();
            var learningObjectId = state.learningObject.getId();
            var results = page.checkAnswers(userAnswers);
            var isCorrect = (!results.incorrect.length);
            var correctAnswers = $.grep(page.config.answers, function (a) { return a.isCorrect == true; });
            var response;

            var QuestionForSectionType = state.learningObject.config.sectionType;

            var PrefixID;
            if (QuestionForSectionType == 6) {
                PrefixID = 'P'  // Post-test
            }
            else if (QuestionForSectionType == 5) {
                PrefixID = 'R'  // Pre-test
            }
            else {
                PrefixID = 'I'
            }

            if (page.isAllThatApply()) {
                // translate each id to its corresponding index char
                lmsResponses = [];
                $(userAnswers).each(function (index, ua) {
                    var ans = $.grep(page.config.answers, function (a) { return a.id == ua; })[0];
                    lmsResponses.push({ 'Short': Artisan.Utilities.indexToChar(ans.sort), 'Long': ans.text });
                });

                // translate each id to its corresponding index char
                var correctResponse = [];
                $(correctAnswers).each(function (index, ca) {
                    correctResponse.push({ 'Short': Artisan.Utilities.indexToChar(ca.sort), 'Long': ca.text });
                });

                var user_answers = $.map(lmsResponses, function (r) { return r.Short });
                var correct_answers = $.map(correctResponse, function (r) { return r.Short });


                LMS.RecordFillInInteraction(id, user_answers, isCorrect, correct_answers, QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId, new Date());

                //only store the chars in our array
                response = $.map(lmsResponses, function (r) { return r.Short });
            }
            else if (page.isMultipleChoice() || page.isTrueFalse()) {
                var responseId = (userAnswers[0] ? userAnswers[0] : '');
                var selectedAnswer = $.grep(page.config.answers, function (a) { return a.id == responseId; })[0]; // can only have one selected answer
                var correctAnswer = correctAnswers[0]; // can only have one correct answer
                isCorrect = (correctAnswer.id == responseId);

                if (page.isMultipleChoice()) {
                    var selectedAnswerChar = Artisan.Utilities.indexToChar(selectedAnswer.sort);
                    var correctAnswerChar = Artisan.Utilities.indexToChar(correctAnswer.sort);
                    //var selectedAnswerRI = API.CreateResponseIdentifier(selectedAnswerChar, selectedAnswer.text);
                    //var correctAnswerRI = API.CreateResponseIdentifier(correctAnswerChar, correctAnswer.text);

                    LMS.RecordFillInInteraction(id, selectedAnswerChar, isCorrect, correctAnswerChar, QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId, new Date());

                    response = selectedAnswerChar;
                }
                else if (page.isTrueFalse()) {
                    var blnResponse = (selectedAnswer.text == 'True');
                    var blnCorrectResponse = (correctAnswer.text == 'True');

                    LMS.RecordFillInInteraction(id, blnResponse, isCorrect, blnCorrectResponse, QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId, new Date());

                    response = blnResponse;
                }
            }
            else if (page.isImageQuestion()) {
                var strResponse = JSON.stringify(userAnswers); // (userAnswers[0] ? userAnswers[0] : '');
                var strCorrectResponse = correctAnswers[0].text;
                var strIsCorrectAnswer;
                if (isCorrect) {
                    strIsCorrectAnswer = 'Correct';
                }
                else {
                    strIsCorrectAnswer = 'Incorrect';
                }
                LMS.RecordFillInInteraction(id, 'image response - ' + strIsCorrectAnswer, isCorrect, 'Correct', QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId, new Date());

                response = JSON.parse(strResponse);
            }
            else if (page.isFillInTheBlank()) {
                var strResponse = userAnswers.join(',');

                var correctResponses = [];
                $(correctAnswers).each(function (index, ca) {
                    correctResponses.push(ca.text);
                });
                var strCorrectResponse = correctResponses.join(',');

                LMS.RecordFillInInteraction(id, strResponse, isCorrect, strCorrectResponse, QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId, new Date());

                response = userAnswers;
            }
            else if (page.isMatching()) {
                // Compress responses for data chunk
                var correctResponse = {};
                var userResponse = {};
                $(page.config.answers).each(function (index, ans) {
                    correctResponse[ans.id] = ans.id;
                    userResponse[ans.id] = $.map(userAnswers, function (a, i) {
                        if (a.answerId == ans.id) { return a.userAnswer; }
                    });
                });

                LMS.RecordFillInInteraction(id, JSON.stringify(userResponse), isCorrect, JSON.stringify(correctResponse), QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId, new Date());

                response = userResponse;
            }
            else if (page.isCategoryMatching()) {
                // Compress responses for data chunk
                var correctResponse = {};
                var userResponse = {};
                $(page.config.answers).each(function (index, ans) {
                    correctResponse[ans.id] = $.map(userAnswers, function (a, i) {
                        if (a.matchId.split('-')[0] == ans.id) { return a.matchId; }
                    });
                    userResponse[ans.id] = $.map(userAnswers, function (a, i) {
                        if (a.userAnswer == ans.id) { return a.matchId; }
                    });
                });

                LMS.RecordFillInInteraction(id, JSON.stringify(userResponse), isCorrect, JSON.stringify(correctResponse), QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId, new Date());

                response = userResponse;
            }
            else if (page.isLongAnswer()) {
                var strResponse = userAnswers.join(',');

                var correctResponses = [];
                $(correctAnswers).each(function (index, ca) {
                    correctResponses.push(ca.text);
                });
                var strCorrectResponse = correctResponses.join(',');

                LMS.RecordFillInInteraction(id, strResponse, isCorrect, strCorrectResponse, QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId, new Date());
                response = userAnswers;

            }

            // create the answer to be stored locally
            var answerObj = {
                id: id,
                isGradable: page.isGradable(),
                learningObjectId: learningObjectId,
                response: response,
                isCorrect: isCorrect,
                questionType: page.config.questionType
            };

            // check to see if this question has been answered before; this deals with mal-formed courses that have questions that are identical,
            // i.e., have the same ID and everything, and are both gradeable or weird situations where something gets half-saved, etc
            var hit = false;
            for (var i = 0; i < this.answeredQuestions.length; i++) {
                if (answerObj.id == this.answeredQuestions[i].id && answerObj.learningObjectId == this.answeredQuestions[i].learningObjectId) {
                    Artisan.Utilities.extend(this.answeredQuestions[i], answerObj);
                    hit = true;
                    break;
                }
            }

            if (!hit) {
                // no hit on the existing questions, so throw it on the end
                this.answeredQuestions.push(answerObj);
            }

            return isCorrect;
        },
        getGradableQuestions: function (isCorrect) {
            if (this.course.isNavigationMethod()) {
                return [];
            }

            var result = [];
            for (var i = 0; i < this.answeredQuestions.length; i++) {
                var q = this.answeredQuestions[i];
                // only add it if:
                if (this.course.isInlineQuestionsMethod()) {
                    if (q.isGradable && (q.isCorrect === isCorrect || typeof (isCorrect) == 'undefined')) {
                        result.push(q);
                    }
                } else {
                    var section = this.course.getSection(q.learningObjectId);
                    var isPosttest = section.config.sectionType == ArtisanSectionType.Posttest;

                    if (section.isTestSection() && isPosttest && (q.isCorrect === isCorrect || typeof (isCorrect) == 'undefined')) {
                        result.push(q);
                    }
                }
            }
            return result;
        },
        getFailedQuestions: function () {
            return this.getGradableQuestions(false);
        },
        getPassedQuestions: function () {
            return this.getGradableQuestions(true);
        },
        setBookmark: function (state) {
            if (this.bookmarkDisabled) {
                return;
            }

            var bookmark = this.buildBookmark(state);

            this.saveDataChunk();
            LMS.SetBookmark(bookmark);
        },
        getBookmark: function () {
            return LMS.GetBookmark();
        },
        clearBookmark: function () {
            LMS.SetBookmark(''); // has to be an empty string
        },
        disableBookmark: function () {
            this.bookmarkDisabled = true;
            this.clearBookmark();
        },
        buildBookmark: function (state) {
            var bookmark = this.parseBookmark(this.getBookmark());

            // Never let the percent decrease
            var percent = (state.progress && state.progress.coursePercent && state.progress.coursePercent > bookmark.percent)
                ? state.progress.coursePercent
                : bookmark.percent;

            var newBookmark = [
                state.sco.getId(),
                state.learningObject.getId(),
                state.page.getId(),
                state.gameTotal,
                percent
            ].join('|');

            if (this.course.isAutomaticBookmarking()) {
                // If we are using automatic bookmarking
                // only use the new bookmark when the overall progress has actually increased
                // otherwise we revert back to the old bookmark. This prevents
                // bookmarking pages when the user moves backwards through the course which is
                // what is desired for automatic bookmarking - return the user to the furthest
                // location in the course they reached. 
                newBookmark = (state.progress && state.progress.coursePercent && state.progress.coursePercent > bookmark.percent) ?
                    newBookmark :
                    this.getBookmark();
            }

            return newBookmark;
        },
        serializeBookmark: function (o) {
            return [
                o.scoId,
                o.loId,
                o.pageId,
                o.gameTotal,
                o.percent
            ].join('|');
        },
        parseBookmark: function (bookmarkString) {
            if (!bookmarkString) {
                return {
                    scoId: 0,
                    loId: 0,
                    pageId: 0,
                    gameTotal: 0,
                    percent: 0
                };
            }

            var params = bookmarkString.split('|');

            var bookmark = {
                scoId: params[0] ? params[0] : 0,
                loId: params[1] ? params[1] : 0,
                pageId: params[2] ? params[2] : 0,
                gameTotal: params[3] ? Number(params[3]) : 0,
                percent: params[4] ? Number(params[4]) : 0
            };

            return bookmark;
        },
        buildAnswerCache: function () {
            // Store the answers for the questions review
            this._answeredQuestionsCache = this.answeredQuestions.slice(0); // copy
        },
        reset: function () {
            // We only want to reset the questions that are not long answer questions
            // we want to lock out the users that have completed the assignment until
            // the instructor has marked them as failed.
            var answeredLongAnswerQuestions = [];
            for (var i = 0; i < this.answeredQuestions.length; i++) {
                if (this.answeredQuestions[i].questionType && this.answeredQuestions[i].questionType == ArtisanQuestionType.LongAnswer) {
                    answeredLongAnswerQuestions.push(this.answeredQuestions[i]);
                }
            }

            // regular answers will be cleared out for the next attempt, long answers will stick around
            this.answeredQuestions = answeredLongAnswerQuestions;

            this.clearBookmark();
            this.saveDataChunk();
            LMS.CommitData();
        },
        resetQuiz: function (learningObjectId, passed) {
            // We don't want to do anything to the quiz results if it is a mastery course
            // and we are currently in the mastery section. 
            if (this.course.hasMastery() && learningObjectId == masterySectionId) {
                return;
            }

            if (!passed) {
                this.answeredQuestions = $.grep(this.answeredQuestions, function (q) { return q.learningObjectId != learningObjectId });
            } else {
                for (var i = 0; i < this.answeredQuestions.length; i++) {
                    if (this.answeredQuestions[i].learningObjectId == learningObjectId) {
                        this.answeredQuestions[i].isCompletedQuizQuestion = true;
                    }
                }
            }
        },
        saveDataChunk: function () {
            LMS.SetDataChunk(this.buildDataChunk());
        },
        buildDataChunk: function () {
            var compressedData = {
                v: {},      // visited scos, only applies to sequential navigation courses
                vl: {},     // visited los, only applies to full sequential navigation courses
                vp: {},     // visited pages, only applies for turning off timeouts if the page has been visited
                q: {},      // answered question data
                pre: null,  // pretest question order (if random)
                pst: null,   // posttest question order (if random)
                t: {}, // timing values for course/sco/lo
                pa: null, // previous attempt,
                ma: 0, // mastery learning object attempts,
                a: 0, // mastery pass
                m: null, // mastery mode
                mq: null, // mastery questions answered
                mqo: null // mastery question order
            };
            $.each(this.answeredQuestions, function (index, question) {
                var idKey = question.learningObjectId + '-' + question.id;
                compressedData.q[idKey] = { r: question.response, c: question.isCorrect, g: question.isGradable, t: question.questionType };
            });

            compressedData.v = this.visitedScos;
            compressedData.vl = this.visitedLos;
            compressedData.vp = this.visitedPages;

            compressedData.pre = this.pretestOrder;
            compressedData.pst = this.posttestOrder;

            compressedData.t = this.timeSpent;
            compressedData.pa = this.previousAttemptState;
            compressedData.m = this.masteryMode;
            compressedData.ma = this.masteryAttempts;
            compressedData.a = this.attempt;
            compressedData.mq = this.getPassedQuestionAttempts();
            compressedData.mqo = this.masteryQuestionOrder;

            var jsonString = JSON.stringify(compressedData);
            var compressed;

            if (Artisan.Utilities.compressJSON && window.LZString) {
                compressed = GzipPrefix + Artisan.Utilities.compressJSON(jsonString);
            } else {
                compressed = jsonString;
            }

            if (compressed && compressed.length && compressed.length >= 64000 && !Artisan.App.maxSizeAlertShown) {
                alert('Your course data has exceeded the maximum size. As a result your progress will no longer be saved if you exit prior to completion.');
                Artisan.App.maxSizeAlertShown = true;
            }

            return compressed;
        },
        parseDataChunk: function (data) {
            var me = this;
            if (!data) {
                return;
            }

            if (data == "undefined") {
                return;
            }

            var prefix = data.substring(0, GzipPrefix.length);

            if (prefix == GzipPrefix && Artisan.Utilities.decompressJSON && window.LZString) {
                window.top.testData = data;
                data = Artisan.Utilities.decompressJSON(data.substr(GzipPrefix.length));

                // junk data bug fix; old courses accidentally got stored w/ all "u" values
                if (!data || data.match(/^u+$/g)) {
                    return;
                }
            }

            try {
                var dataObj = JSON.parse(data);
            } catch (d) {
                return;
            }

            if (dataObj) {
                if (dataObj.q) { // answered question data
                    me.answeredQuestions = [];
                    $.each(dataObj.q, function (idKey, value) {
                        var separatorIndex = idKey.lastIndexOf('-');
                        var loIdStr = idKey.substr(0, separatorIndex);
                        var pgIdString = idKey.substr(separatorIndex + 1);

                        var ids = idKey.split('-');
                        var hit = false;
                        var answerObj = {
                            id: parseInt(pgIdString),
                            learningObjectId: parseInt(loIdStr),
                            response: value.r,
                            isCorrect: value.c,
                            isGradable: value.g,
                            isMarked: true, // Default to true, can only be false on assignment questions
                            questionType: value.t
                        };

                        if (Artisan.App.assignmentResults && answerObj.questionType == ArtisanQuestionType.LongAnswer) {
                            for (var i = 0; i < Artisan.App.assignmentResults.length; i++) {
                                if (Artisan.App.assignmentResults[i].pageId == answerObj.id) {
                                    answerObj.isCorrect = (Artisan.App.assignmentResults[i].responseStatus == AssignmentResponseStatus.Correct); // correct
                                    answerObj.instructorFeedback = Artisan.App.assignmentResults[i].instructorFeedback;
                                    answerObj.isMarked = (Artisan.App.assignmentResults[i].responseStatus != AssignmentResponseStatus.Unmarked);
                                }
                            }
                        }

                        // retry only means retry the ones we got wrong
                        /*if(!answerObj.isCorrect){
							// if they got it wrong then we check to see if they should be allowed to try again
							if (value.t === ArtisanQuestionType.LongAnswer && Artisan.App.isAllowRetryAssignment) {
								return;
							}
						}*/

                        // ensure no duplicates
                        for (var i = 0; i < me.answeredQuestions.length; i++) {
                            if (answerObj.id == me.answeredQuestions[i].id && answerObj.learningObjectId == me.answeredQuestions[i].learningObjectId) {
                                Artisan.Utilities.extend(me.answeredQuestions[i], answerObj);
                                hit = true;
                                break;
                            }
                        }
                        if (!hit) {
                            me.answeredQuestions.push(answerObj);
                        }
                    });
                }
                if (dataObj.pre) { // pretest question order (if random)
                    me.pretestOrder = dataObj.pre;
                }
                if (dataObj.pst) { // posttest question order (if random)
                    me.posttestOrder = dataObj.pst;
                }

                if (dataObj.v) { // visited scos
                    me.visitedScos = dataObj.v;
                }
                if (dataObj.vl) { //visited los
                    me.visitedLos = dataObj.vl;
                }
                if (dataObj.vp) { // visited pages
                    me.visitedPages = dataObj.vp;
                }
                if (dataObj.t) { // time spent per course/sco/lo
                    me.timeSpent = dataObj.t;
                }
                if (dataObj.pa) {
                    me.previousAttemptState = dataObj.pa;
                }
                if (dataObj.m) {
                    me.masteryMode = dataObj.m;
                }
                if (dataObj.ma) {
                    me.masteryAttempts = dataObj.ma;
                }
                if (dataObj.a) {
                    me.attempt = dataObj.a;
                }
                if (dataObj.mq) {
                    me.passedQuestionAttempts = dataObj.mq;
                }
                if (dataObj.mqo) {
                    me.masteryQuestionOrder = dataObj.mqo
                }
            }
        },
        // To be called every time a question is answered in a post test
        // ONLY WHEN TestForceRestart is enabled. This way we keep track
        // of the current score as the student progresses through the test
        // Then if the student is booted from the course or leaves we will
        // have a score saved.
        calculateInProgressPostTestScore: function (sectionId) {
            this.buildAnswerCache();

            var reviewData = this.calculateReview(sectionId);

            // Coppied from the review getHtml method. 
            // This will get called every 30 seconds while in a post test

            // OK, so, apparently some LMSes see the min/max as not pass/fail values, but as min/max the *student* got and then somehow add them together???
            // soooo, we set to 100, 0 to ensure that'll always set to 100
            LMS.SetScore(reviewData.score, 100, 0);

            // ok, we abuse the speed preference here
            LMS.SetSpeedPreference(0);

            if (reviewData.passed) {
                LMS.SetPassed();
            } else {
                LMS.SetFailed();
            }

            // this is abuse, but seems to force things to save properly
            // per Nu, 5/9/2014
            LMS.CommitData();

        },
        calculateReview: function (section, forceSection) {
            var total, answeredQuestions;
            var passingScore = this.course.getPassingScore();
            if (section) {
                if (!isNaN(parseFloat(section)) && isFinite(section)) {
                    // number
                    section = this.course.getSection(section);
                }
            }

            var isQuizReview = !Artisan.App.state.isPreviousPageReview && section && section.config.isQuiz;
            if (!this._answeredQuestionsCache || this._answeredQuestionsCache.length == 0) {
                this.buildAnswerCache();
            }

            // If it's navigation mode, and we arn't at a quiz review page
            if (this.course.isNavigationMethod() && !isQuizReview) {
                return {
                    total: 1,
                    answered: 1,
                    correct: 1,
                    score: 100,
                    passingScore: passingScore,
                    passed: true
                }
            }

            if (section && (this.course.isTestMethod() || isQuizReview || forceSection)) { // pretest or posttest or quiz
                var pagesToUse = section.config.isSinglePage && isQuizReview ? section.config.combinedPages : section.pages;

                if (!section.pages) {
                    return;
                }

                if (section.config.passingScore > 0 && isQuizReview) { // only quizzes can override test scores
                    passingScore = section.config.passingScore;
                }
                // A map of the pages actually used in this quiz or post test in order to 
                // only count recorded answers for questions that were actually delivered
                // Should fix:https://oncourselearning.atlassian.net/browse/SYM-1063
                var validQuestionPages = {};
                for (var i = 0; i < pagesToUse.length; i++) {
                    var pageId = section.config.isSinglePage ? pagesToUse[i].id : pagesToUse[i].getId();
                    validQuestionPages[pageId] = true;
                }

                total = $.grep(pagesToUse, function (p) { return p.config ? p.config.questionType : p.questionType }).length;
                answeredQuestions = $.grep(this._answeredQuestionsCache, function (q) { return q.learningObjectId == sectionId && validQuestionPages[q.id] });
            }
            else { // inline questions review
                total = this.course.getQuestionPageCount(null, true, true);
                answeredQuestions = $.grep(this.answeredQuestions, function (q) { return q.isGradable; });
            }

            var answered = answeredQuestions.length;
            var correct = $.grep(answeredQuestions, function (q) { return q.isCorrect == true }).length;
            var score = Math.round((correct / total) * 100);

            // Make sure score is never over 100. 
            score = score > 100 ? 100 : score;

            var result = {
                total: total,
                answered: answered,
                correct: correct,
                score: score,
                passingScore: passingScore,
                passed: score >= passingScore
            };

            Artisan.App.fire('reviewcalculated', this, result);

            return result;
        },
        calculateMasteryReview: function () {
            //this.calculateReview(this.course.getMasterySection(), true);
            //TODO: calculate review; this depends on the mastery "level", 
            // meaning they have to get each question right at least 1x or 2x
            // first, get the 2x version working
        },
        setAttempt: function (v) {
            this.attempt = v;
        },
        getAttempt: function () {
            return this.attempt;
        },
        addVisitedSco: function (scoId) {
            this.visitedScos[scoId] = 1;
        },
        hasVisitedSco: function (scoId) {
            return this.visitedScos[scoId];
        },
        addVisitedLo: function (loId) {
            this.visitedLos[loId] = 1;
        },
        hasVisitedLo: function (loId) {
            return this.visitedLos[loId];
        },
        addVisitedPage: function (pageId) {
            this.visitedPages[pageId] = 1;
        },
        hasVisitedPage: function (pageId) {
            return this.visitedPages[pageId];
        },

        // Time helpers for data chunk
        setLoTime: function (id) {
            this.setTime('l', id);
        },
        setCourseTime: function (id) {
            this.setTime('c', id);
        },
        setScoTime: function (id) {
            this.setTime('s', id);
        },
        getLoTime: function (id) {
            return this.getTime('l', id);
        },
        getCourseTime: function (id) {
            return this.getTime('c', id);
        },
        getScoTime: function (id, time) {
            return this.getTime('s', id, time);
        },
        addLoTime: function (id, time) {
            return this.addTime('l', id, time);
        },
        addCourseTime: function (id, time) {
            return this.addTime('c', id, time);
        },
        addScoTime: function (id, time) {
            return this.addTime('s', id, time);
        },
        addTime: function (type, id, time) {

            var currentTime = this.getTime(type, id);

            currentTime += time;

            this.setTime(type, id, currentTime);
        },
        setTime: function (type, id, time) {

            this.timeSpent[type + id] = time;
        },
        getTime: function (type, id) {
            if (!this.timeSpent[type + id]) {
                return 0;
            }

            return this.timeSpent[type + id];
        }
    });
}());