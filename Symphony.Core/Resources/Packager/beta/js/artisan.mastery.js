var beginningMastery = false;

var artisanMasteryTypes = {
    antiGuessing: 1,
    masteryLevelOne: 2,
    masteryLevelTwo: 3
}

// "level" is 1 or 2 - indicates if they have to get each question right 1x or 2x to have "mastered" the content
// "isResume" mastery is started from a bookmarked state. Don't reset the state or increment the attempt count, will use the page order from prev attempt.
Artisan.App.beginMasteryMode = function (level, isResume) {
    beginningMastery = true; // to prevent further calls to process mastery on state update when we select the mastery sco

    // select the new section
    var record = Artisan.App.course.record;
    var masterySection = Artisan.App.course.getRebuiltMasterySection(isResume);

    if (!isResume) {
        record.incrementMasteryAttempts();
        record.setMasteryMode(level);
        record.clearMastery();
    }

    Artisan.App.selectSco(Artisan.App.course.getLastScoWithContent());
    Artisan.App.selectLearningObject(masterySection);
    Artisan.App.selectPage(masterySection.getFirstPage());

    beginningMastery = false;
};

Artisan.App.getPassedQuestionStats = function () {
    var record = Artisan.App.course.record,
        passedQuestions = record.getPassedQuestionAttempts(),
        gradableQuesitons = record.getGradableQuestions(),
        result = {
            correctTwice: 0, // questions answered correctly twice or more
            correctOnce: 0, // questions answered correctly exactly once
            incorrect: 0, // questions never answered correctly
            levelOneCompletedCount: 0, // questions answered correctly one or more times
            levelTwoCompletedCount: 0 // questions answered correctly two or more times
        }

    for (var i = 0; i < gradableQuesitons.length; i++) {
        var question = passedQuestions[gradableQuesitons[i].id];
        if (question) {
            if (question.attempts >= 2) {
                result.correctTwice++;
            } else if (question.attempts == 1) {
                result.correctOnce++;
            } else if (question.attempts == 0) {
                result.incorrect++;
            }

            if (question.attempts >= 1) {
                result.levelOneCompletedCount++;
            }
            if (question.attempts >= 2) {
                result.levelTwoCompletedCount++;
            }
        } else {
            result.incorrect++;
        }
    }

    return result;
}

Artisan.App.on('reviewcalculated', function (result) {
    var course = Artisan.App.course;
    var record = course.record;

    if (course.hasMastery()) {
        // checking if the number of successful attempts for each mode has been reached. 
        // using the score will not work for mastery level 2, so might as well keep this consistent. 
        var passedQuestionsStats = Artisan.App.getPassedQuestionStats(),
            gradableQuestions = record.getGradableQuestions();

        if (course.isMasteryLevelTwo()) {
            if (passedQuestionsStats.levelTwoCompletedCount >= gradableQuestions.length && record.hasPreviousAttemptRecord()) {
                record.isMasteryLevelTwoComplete = true;
            } else if (passedQuestionsStats.levelOneCompletedCount >= gradableQuestions.length && !record.hasPreviousAttemptRecord()) {
                // If we have satisfied level one mastery on the first attempt, set mastery level two complete. 
                record.isMasteryLevelTwoComplete = true;
            }
        } else if (course.isMasteryLevelOne()) {
            if (passedQuestionsStats.levelOneCompletedCount >= gradableQuestions.length) {
                record.isMasteryLevelOneComplete = true;
            }
        }

    }

    if (record.isMasteryLevelOneComplete || record.isMasteryLevelTwoComplete) {
        result.answered = result.total;
        result.correct = result.total;
        result.score = 100;
        result.passed = true;
        result.isMastered = true;
    }
});

Artisan.App.on('aftermovenext', function (nextPage, nextLo, nextSco) {
    Artisan.App.handleUspap(nextPage, nextLo, nextSco);
});
Artisan.App.on('aftermoveprevious', function (previousPage, previousLo, previousSco) {
    Artisan.App.handleUspap(previousPage, previousLo, previousSco, true);
});
Artisan.App.on('aftermovetobeginning', function (page, lo, sco) {
    Artisan.App.handleUspap(page, lo, sco);
});
// Checks the page to see if it has been answered. Moves either next or previous based on the 
// direction the student is travelling in the course if the student has previously answered
// the question correctly. 
Artisan.App.handleUspap = function (page, lo, sco, isPrevious) {
    var course = Artisan.App.course;
    var record = course.record;

    if (course.hasMastery() && course.isUspap() && record.getAttempt() == 2 && !record.getIsMasteryActive()) {
        var passedQuestionAttempts = Artisan.App.course.record.getPassedQuestionAttempts(),
            skipPage = false,
            checkPage;

        if (page) {
            checkPage = page;
        } else if (lo) {
            if (isPrevious) {
                checkPage = lo.getLastPage();
            } else {
                checkPage = lo.getFirstPage();
            }
        } else if (sco) {
            if (isPrevious) {
                var lastLo = sco.getLastSectionWithPages();
                checkPage = lastLo.getLastPage();
            } else {
                var firstLo = sco.getFirstSectionWithPages();
                checkPage = firstLo.getFirstPage();
            }
        }

        if (checkPage && passedQuestionAttempts[checkPage.getId()] && passedQuestionAttempts[checkPage.getId()].attempts > 0) {

            // fake a correct answer so it will be added to the review.
            var existingResponse = $.grep(record.answeredQuestions, function (q) { return q.id == checkPage.getId() });
            if (existingResponse.length == 0) {
                record.answeredQuestions.push({
                    id: checkPage.getId(),
                    isGradable: true,
                    learningObjectId: checkPage.parent.getId(),
                    isCorrect: true,
                    questionType: checkPage.config.questionType
                });
            }

            if (isPrevious) {
                Artisan.App.movePrevious();
            } else {
                Artisan.App.moveNext();
            }
        }

        return true;
    }
};

Artisan.App.initMastery = function () {
    // No longer a need for any initializtion. Leaving this hook in place in case we need this in the future. 
}

Artisan.App.resumeMastery = function (state) {
    var lo = Artisan.App.course.getRebuiltMasterySection(true);
    var sco = lo.parent;

    state.sco = sco;
    state.learningObject = lo;
    state.page = lo.pages[0];

    Artisan.App.processMastery(state, null, true);
}

Artisan.App.processMastery = function (state, o, isResume) {
    if (!beginningMastery) { // We don't want to recalculate while we are already starting a mastery mode.

        var record = Artisan.App.course.record;

        if (state.page && state.page.isReviewPage() || isResume === true) {
            var stats = record.calculateReview();
            if (!record.isMastered()) {
                if (Artisan.App.course.hasAntiGuessing() && stats.score < 50 && !record.getIsMasteryActive()) {
                    if (window.console) {
                        console.log('Restarting due to anti-guessing policy');
                    }

                    // restart
                    Artisan.App.createMasteryReviewPage(artisanMasteryTypes.antiGuessing, function () {
                        record.reset();

                        Artisan.App.resetState();
                        Artisan.App.startCourse();
                        Artisan.App.moveToBeginning();
                        return false; // Because we don't want the default move next to run
                    });

                } else if (Artisan.App.course.hasMastery()) {
                    if (Artisan.App.course.isMasteryLevelTwo()) {
                        if (stats.score == 100 && record.getAttempt() == 1) {
                            // mastered, bail out as long as it's on attempt 1
                        } else {
                            // mastery 2, if they don't get 100%, they have to go through a second attempt
                            if (record.getAttempt() == 1) {

                                var firstLo = Artisan.App.course.getFirstSectionWithPages();

                                var passedQuestionAttempts = record.getPassedQuestionAttempts();
                                // If it's uspap, don't bother showing the first attempt through the content
                                // if they got it right in the first try, update the count to be 2 since they 
                                // wont get another shot at it the second time around. 
                                if (Artisan.App.course.isUspap()) {
                                    for (var index in passedQuestionAttempts) {
                                        if (passedQuestionAttempts.hasOwnProperty(index) &&
                                            parseInt(index, 10) > 0 &&
                                            passedQuestionAttempts[index]) {
                                            if (passedQuestionAttempts[index].attempts < 2) {
                                                passedQuestionAttempts[index].attempts = 2;
                                            }
                                        }
                                    }
                                }

                                Artisan.App.createMasteryReviewPage(artisanMasteryTypes.masteryLevelTwo, function () {
                                    // save the attempt and increment the count
                                    record.saveAttempt();

                                    Artisan.App.resetState();
                                    Artisan.App.startCourse();
                                    Artisan.App.moveToBeginning();
                                    return false; // Because we don't want the default move next to run
                                })

                            } else if (record.getAttempt() == 2) {
                                // they've gone through 2x, but have not yet mastered everything
                                // move to the "mastery mode"
                                Artisan.App.createMasteryReviewPage(artisanMasteryTypes.masteryLevelTwo, function () {
                                    Artisan.App.beginMasteryMode(2, isResume);
                                    return false; // Because we don't want the default move next to run
                                });
                            } else {
                                alert('Unexpected state encountered!');
                            }
                        }
                    } else if (Artisan.App.course.isMasteryLevelOne()) {
                        if (stats.score == 100) {
                            // --- We shouldn't ever get here due to the mastery result being calculated earlier now.
                            // mastered
                        } else {
                            // they've gone through 1x, but have not yet mastered everything
                            // move to the "mastery mode"
                            Artisan.App.createMasteryReviewPage(artisanMasteryTypes.masteryLevelOne, function () {
                                Artisan.App.beginMasteryMode(1, isResume);
                                return false; // Because we don't want the default move next to run
                            });
                        }
                    }
                }
            }

            // save
            Artisan.App.saveProgress();
        } 
    }
}

Artisan.App.saveProgress = function () {
    Artisan.App.setBookmark();
    Artisan.App.course.record.saveDataChunk();
}

Artisan.App.on('submitquestion', function (state, userAnswers, pageContext) {
    var record = Artisan.App.course.record;
    if (record.getIsMasteryActive()) {
        // once in mastery mode, stats are calculated based on the mastery section
        var stats = record.calculateMasteryReview();

        if (Artisan.App.onLastPage()) {
            // last page of the mastery section
            if (Artisan.App.course.isMasteryLevelOne()) {
                if (stats.score == 100) {
                    // mastered
                } else {
                    // they've gone through 1x, but have not yet mastered everything
                    // move to the "mastery mode"
                    Artisan.App.beginMasteryMode(1);
                }
            }
        }
    }
});

Artisan.App.on('statechange', function (state, o) {
    // when we get to the last page in the course,
    // if mastery is not active, move into mastery mode
    Artisan.App.processMastery(state, o);
});

// Updates the next button on the review page:
// Removes exit class, sets text to "Next"
// stores handler to hook into the move next
// function to override the default functionality
// This is used to override the exit call on the review page
// to restart the course instead.
Artisan.App.createMasteryReviewPage = function (type, moveNextHandler) {
    var record = Artisan.App.course.record;

    if (typeof (moveNextHandler) === 'function') {
        var state = Artisan.App.getState(),
            masteryAttempts = record.getMasteryAttempts(),
            masteryPass = record.getAttempt(),
            templateCls = '.artisan-review-mastery-',
            passedQuestionStats = Artisan.App.getPassedQuestionStats();

        switch (type) {
            case artisanMasteryTypes.antiGuessing:
                templateCls += 'antiguessing';

                // The results from mastery tracking get ignored entirely when anti-guessing is activated
                // so we will always see either the last attempt, or 0/11 if we don't adjust.
                // Just count the current attempt in this case. Not including the overall mastery status since we
                // are just displaying why this attempt is being blown away.
                var totalQuestions = Artisan.App.course.getQuestionPageCount(null, true, true);
                var passedQuestions = record.getPassedQuestions();
                // The anti guessing review page only shows correct_count and incorrect_count so just stuffing these
                // values into the count previously calculated.
                passedQuestionStats.levelOneCompletedCount = passedQuestions.length; 
                passedQuestionStats.incorrect = totalQuestions - passedQuestionStats.levelOneCompletedCount;

                break;
            case artisanMasteryTypes.masteryLevelOne:
                templateCls += "lvl1";
                break;
            case artisanMasteryTypes.masteryLevelTwo:
                templateCls += "lvl2";
                if (masteryPass == 2 && masteryAttempts == 0) {
                    templateCls += "-start";
                }
                break;
        }

        if (type != artisanMasteryTypes.antiGuessing && masteryAttempts >= 1) {
            templateCls += "-retry";
        }

        var template = $(templateCls).html();

        template = Artisan.Utilities.replaceAll(template, '{!correct_count_twice}', passedQuestionStats.correctTwice);
        template = Artisan.Utilities.replaceAll(template, '{!correct_count_once}', passedQuestionStats.correctOnce);
        template = Artisan.Utilities.replaceAll(template, '{!correct_count}', passedQuestionStats.levelOneCompletedCount);
        template = Artisan.Utilities.replaceAll(template, '{!incorrect_count}', passedQuestionStats.incorrect);
        template = Artisan.Utilities.replaceAll(template, '{!attempt}', masteryAttempts);

        var reviewHtml = $('.artisan-page-wrap').html().replace('{!content}', template);
        $('#artisan-page').html(reviewHtml);
        
        if (!Artisan.App.course.isUspap()) {
            $('#artisan-page .uspap').hide();
        }

        Artisan.App.applyInserts();
        
        $('.artisan-next').removeClass('disabled').removeClass('exit').text('Next');

        // set handler to run once
        Artisan.App.moveNextOnce = function (state, nextPage, nextLo, nextSco) {
            Artisan.App.moveNextOnce = null;
            return moveNextHandler(state, nextPage, nextLo, nextSco);
        }
    }
}

Artisan.App.on('beforemovenext', function (state, nextPage, nextLo, nextSco) {
    if (Artisan.App.moveNextOnce && typeof (Artisan.App.moveNextOnce) === 'function') {
        var result = Artisan.App.moveNextOnce(state, nextPage, nextLo, nextSco)
        Artisan.App.saveProgress();
        return result;
    }
});
