window.Artisan = window.Artisan || {};
Artisan.Utilities = {
    ns: function (ns) {
        var parts = ns.split('.');
        var current = window;
        for (var i = 0; i < parts.length; i++) {
            var n = parts[i];
            current[n] = current[n] || {};
            current = current[n];
        }
        return current;
    },
    contentSelector: '#Content .ContentWrapper',
    extractContent: function (html) {
        var f = $('<div>');
        f.hide();
        $(document.body).append(f);
        f.html(html);
        var content = $(Artisan.Utilities.contentSelector, f).html();
        f.remove();
        return content;
    },
    escapeRe: function (str) {
        return String(str).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
    },
    replaceAll: function (text, pattern, replacement) {
        var result = text, last = text;
        while (true) {
            result = result.replace(pattern, replacement);
            if (last === result) {
                break;
            }
            last = result;
        }
        return result;
    },
    extend: function (dest, src) {
        for (var prop in src) {
            dest[prop] = src[prop];
        }
        return dest;
    },
    clone: function (obj) {
        return jQuery.extend(true, {}, (obj));
    },
    trim: function (s) {
        if (s) {
            return s.replace(/^\s+|\s+$/, '');
        }
        return s;
    },
    charsets: {
        'upper-alpha': {
            size: 26,
            unicodeStart: 65 // 65 = 'A'
        },
        'lower-alpha': {
            size: 26,
            unicodeStart: 97 // 97 = 'a'
        },
        'decimal': { // this will only work for single-digit numbers
            size: 9,
            unicodeStart: 49 // 49 = '1'
        }
    },
    indexToChar: function (index, charsetName) {
        if (!charsetName)
            charsetName = 'upper-alpha';

        var charset = this.charsets[charsetName]
        if (!charset)
            throw new Error("Invalid charset specified");

        if (index < 0 || index > charset.size - 1)
            throw new Error("Index out of bounds for specified charset");

        var ch = String.fromCharCode(index + charset.unicodeStart);
        return ch;
    },
    charToIndex: function (ch, charsetName) {
        if (!charsetName)
            charsetName = 'upper-alpha';

        var charset = this.charsets[charsetName]
        if (!charset)
            throw new Error("Invalid charset specified");

        var code = ch.charCodeAt(0);

        var index = code - charset.unicodeStart;
        return index;
    },
    indiciesToChars: function (indicies, charsetName) {
        var chars = [];
        for (var i = 0; i < indicies.length; i++) {
            chars.push(this.indexToChar(indicies[i], charsetName));
        }
        return chars;
    },
    charsToIndicies: function (chars, charsetName) {
        var indicies = [];
        for (var i = 0; i < chars.length; i++) {
            indicies.push(this.charToIndex(chars[i], charsetName));
        }
        return indicies;
    },
    // This function is typically used as a parameter to Array.sort()
    randomSort: function () {
        return (Math.round(Math.random()) - 0.5);
        //return 0.5 - Math.random();
    },
	filter: function(arr, iterator) {
		if (arr == null)
		  throw new TypeError();

		var object = Object(arr);
		var results = [], context = arguments[1], value;

		for (var i = 0, length = object.length >>> 0; i < length; i++) {
		  if (i in object) {
			value = object[i];
			if (iterator.call(context, value, i, object)) {
			  results.push(value);
			}
		  }
		}
		return results;
	},
	without: function(arr){
		var values = slice.call(arguments, 1);
		return arr.select(function(value) {
		  return !values.include(value);
		});
	},
    // NOTE: this is for reference only, use swfobject.getObjectById(swfId);
    getSwfObj: function (swfId) {
        if (navigator.appName.indexOf("Microsoft") != -1) {
            return window[swfId];
        } else {
            return document[swfId];
        }
    },
    _scriptQueue: [],
    _processing: false,
    _processQueue: function () {
        if (Artisan.Utilities._processing) {
            return;
        }
        Artisan.Utilities._processing = true;
        var script = Artisan.Utilities._scriptQueue.shift();
        if (script) {
            var url = script.url;
            // TODO: modify this so it also works with URLs that already have parameters
            if (script.cacheBust) {
                url += '?cache=' + parseInt(Math.random() * 99999999);
            }

            Artisan.Utilities.getScript(url, function () {
                Artisan.Utilities._processing = false;
                Artisan.Utilities._processQueue();
            });
        }
        else {
            Artisan.Utilities._processing = false;
            if (Artisan.Utilities.loadScriptsComplete) {
                Artisan.Utilities.loadScriptsComplete();
            }
        }
    },
    loadScripts: function (scripts, callback) {
        this._scriptQueue = this._scriptQueue.concat(scripts);
        this._processQueue();
    },
    _scriptId: 0,
    getCss: function (url, callback, debug) {
        return this.getScript(url, callback, debug, 'css');
    },
    getScript: function (url, callback, debug, type) {
        var script = type == 'css' ? document.createElement('link') : document.createElement('script'),
			head = document.head || document.getElementsByTagName('head')[0] || document.documentElement;

        if (type == 'css') {
            script.rel = 'stylesheet';
            script.type = 'text/css';
            script.href = url;
        } else {
            script.src = url;
        }
        if (debug === true) {
            script.src += '&debug=true';
        }

        script.id = 'artisan_script_' + (++this._scriptId);

        // add a monitor if they've defined the callback
        if (callback) {
            var timeout = window.setTimeout(function () {
                if (!script.loaded) {
                    //alert('We\'re sorry, but the content could not be loaded. Please refresh.');
                }
            }, 30000);
        }

        // when the script loads, clear the timeout and invoke the callback
        script.onload = script.onreadystatechange = function (_, isAbort) {

            if (!script.readyState || /loaded|complete/.test(script.readyState)) {
                window.clearTimeout(timeout);

                //try {
                    // Callback if not abort
                    if (!isAbort) {
                        if (callback) { callback(); }
                    }

                    // Handle memory leak in IE
                    script.onload = script.onreadystatechange = null;

                    // Remove the script
                    if (head && script.parentNode) {
                        if (type != 'css') {
                            head.removeChild(script);
                        }
                    }

                    // Dereference the script
                    script = undefined;
                //} catch (e) {
                //    throw e;
                //}
            }
        };

        // Use insertBefore instead of appendChild to circumvent an IE6 bug.
        // This arises when a base node is used, per Jquery bugs (#2709 and #4378).
        head.insertBefore(script, head.firstChild);
    },
    getUrlFilename: function (url) {
        var end = (url.indexOf("?") == -1) ? url.length : url.indexOf("?");
        return url.substring(url.lastIndexOf("/") + 1, end);
    },
    compressJSON: function (str) {
        return window.LZString.compressToEncodedURIComponent(str);
    },
    decompressJSON: function (str) {
        return window.LZString.decompressFromEncodedURIComponent(str);
    },
    getConfiguration: function (window) {
        if (window.location.search.indexOf('registration=') > 0) {
            var queryString = window.location.search.substr(1);
            var parts = queryString.split('&');

            for (var i = 0; i < parts.length; i++) {
                var keyValues = parts[i].split('=');
                if (keyValues.length > 1 && keyValues[0] == 'registration') {
                    return keyValues[1];
                }
            }

            if (window.parent != window) {
                return Artisan.Utilities.getConfiguration(window.parent);
            }

        } else if (window.parent != window) {
            return Artisan.Utilities.getConfiguration(window.parent);
        }
        return null;
    },
    getArtisanCourseHookHandler: function () {
        var href = window.location.href;
        var configuration = Artisan.Utilities.getConfiguration(window);
        var courseHookScript = 'js/artisan_course_hook.js?_dt=' + (new Date()).getTime() + '&href=' + window.location.href;

        if (configuration) {
            courseHookScript += '&registration=' + configuration;
        }

        Artisan.Utilities.getScript(courseHookScript);
    },
    format: function(template) {
        var result = template;
        for (var i = 1; i < arguments.length; i++) {
            result = result.replace("{" + (i-1) + "}", arguments[i]);
        }
        return result;
    }
};

/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 */
// Inspired by base2 and Prototype
(function(){
  var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;

  // The base Class implementation (does nothing)
  this.Class = function(){};
 
  // Create a new Class that inherits from this class
  Class.extend = function(prop) {
    var _super = this.prototype;
   
    // Instantiate a base class (but only create the instance,
    // don't run the init constructor)
    initializing = true;
    var prototype = new this();
    initializing = false;
   
    // Copy the properties over onto the new prototype
    for (var name in prop) {
      // Check if we're overwriting an existing function
      prototype[name] = typeof prop[name] == "function" &&
        typeof _super[name] == "function" && fnTest.test(prop[name]) ?
        (function(name, fn){
          return function() {
            var tmp = this._super;
           
            // Add a new ._super() method that is the same method
            // but on the super-class
            this._super = _super[name];
           
            // The method only need to be bound temporarily, so we
            // remove it when we're done executing
            var ret = fn.apply(this, arguments);       
            this._super = tmp;
           
            return ret;
          };
        })(name, prop[name]) :
        prop[name];
    }
   
    // The dummy class constructor
    function Class() {
      // All construction is actually done in the init method
      if ( !initializing && this.init )
        this.init.apply(this, arguments);
    }
   
    // Populate our constructed prototype object
    Class.prototype = prototype;
   
    // Enforce the constructor to be what we expect
    Class.constructor = Class;

    // And make this class extendable
    Class.extend = arguments.callee;
   
    return Class;
  };
})();

(function(){
    /* 
      Simple jquery drag/drop/resize, modified from http://dev.iceburg.net/jquery/jqDnR/  
      Sample usage:
        $('#element').jqDrag({
            handle: '#element_handle',
            start: function(e){
                console.log('started');
            },
            drag: function(e){
                console.log('moved');
            },
            stop: function(e) {
                console.log('stopped');
            }
        });
    */
    $.fn.jqDrag = function(o) {
        return i(this, o);
    };
    $.jqDnR = {
        dnr: {},
        e: 0,
        drag: function(v) {
            E.css({ left: M.X + v.pageX - M.pX, top: M.Y + v.pageY - M.pY });
            v.data.moved = true;
            if (v.data.move) {
                v.data.move(v);
            }
            return false;
        },
        stop: function(v) {
            E.css('opacity', M.o);
            $(document.body).unbind('mousemove', J.drag).unbind('mouseup', J.stop);
            if (v.data.stop) {
                v.data.stop(v);
            }
        }
    };
    var J = $.jqDnR, M = J.dnr, E = J.e;
    var i = function(e, h, o) {

        return e.each(function() {
            h = (o.handle) ? $(o.handle) : e;
            h.bind('mousedown', { e: e, o: o }, function(v) {
                var d = v.data, p = {}; E = d.e;
                if (E.css('position') != 'relative') { try { E.position(p); } catch (e) { } }
                M = { X: p.left || f('left') || 0, Y: p.top || f('top') || 0, W: f('width') || E[0].scrollWidth || 0, H: f('height') || E[0].scrollHeight || 0, pX: v.pageX, pY: v.pageY, k: d.k, o: E.css('opacity') };
                E.css({ opacity: 0.8 }); $(document.body).mousemove(o, $.jqDnR.drag).mouseup(o, $.jqDnR.stop);
                return false;
            });
            if (o.start) {
                o.start();
            }
        });
    };
    var f = function(k) { return parseInt(E.css(k)) || false; };

}());