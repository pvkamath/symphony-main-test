﻿Adding a new HTML5 media player

1. Create a new folder under Symphony.Core/Resources/Packager/js/media_players - use the name of the player for the name of the folder
2. Copy all resources required for the player to the new folder

3. !!! Ensure ALL file names referenced only have ONE "." !!!
    - Files names like video.min.js should be renamed to video_min.js
	- Since these are embedded resources all directory separators get replaced with "."
	  if the file name has more than one extension, it will be impossible to determine
	  the folder structure wrequired
	- Files without extensions cannot be used

3. Add additional file to the new folder you created  "InitTemplate.html"
4. In InitTemplate, add the lines required to include the media template

Example init for MediaElement:
```
<script src="js/media-players/MediaElement/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="js/media-Players/MediaElement/mediaelementplayer.css" />
```

5. Update properties for all files in the new folder ensure Build Action = Embedded Resource
6. In Artisan.Utilities update two functions:
      startVideo
	  destroyVideo

	Make sure each function dectects the presence of the player and initializes the video
	elements and destroys them appropriatley