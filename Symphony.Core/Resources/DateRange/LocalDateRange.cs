﻿using NodaTime;
using System;

namespace Symphony.Core
{
    public struct LocalDateRange
    {
        public LocalDate StartDate;
        public LocalDate EndDate;

        public LocalDateRange(LocalDate startDate, LocalDate endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}
