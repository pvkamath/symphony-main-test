﻿using NodaTime;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Symphony.Core
{
    public static class DateRangeManager
    {
        public static ZonedDateRange Parse(string dateRangeExpression, string timeZoneId)
        {
            return new ZonedDateRange(Parse(dateRangeExpression), timeZoneId);
        }

        public static LocalDateRange Parse(string dateRangeExpression)
        {
            LocalDate startDate, endDate;

            // Remove all whitespace
            dateRangeExpression = Regex.Replace(dateRangeExpression, @"\s+", "");

            // Split into start and end date strings
            string[] dates = dateRangeExpression.Split(',');
            if (dates.Length != 2)
            {
                throw new Exception("Invalid date range expression string. Must contain exactly a start date and an end date.");
            }

            if (string.IsNullOrEmpty(dates[0]) || string.IsNullOrEmpty(dates[1]))
            {
                throw new Exception("Invalid date range expression string. Must contain exactly a start date and an end date.");
            }

            // Parse start date
            try
            {
                startDate = ParseDateExpression(dates[0]);
            }
            catch (Exception ex)
            {
                throw new Exception("Invalid date range expression string. The start date format is invalid.", ex);
            }

            // Parse end date
            try
            {
                endDate = ParseDateExpression(dates[1]);
            }
            catch (Exception ex)
            {
                throw new Exception("Invalid date range expression string. The end date format is invalid.", ex);
            }

            return new LocalDateRange(startDate, endDate);
        }

        private static LocalDate ParseDateExpression(string dateExpression)
        {
            // Split date string into Day/Month/Year parts
            string[] dateParts = dateExpression.Split('/');
            if (dateParts.Length != 3)
            {
                throw new Exception("Invalid date expression string. Date must contain exactly a day/month/year.");
            }

            if (string.IsNullOrEmpty(dateParts[0]) || string.IsNullOrEmpty(dateParts[1]) || string.IsNullOrEmpty(dateParts[2]))
            {
                throw new Exception("Invalid date expression string. Date must contain exactly a day/month/year.");
            }

            string dayExpression = dateParts[0];
            string monthExpression = dateParts[1];
            string yearExpression = dateParts[2];

            int day, month, year;

            #region Parse Year
            // Supported values:
            //   C       : Current year
            //   C-3     : 3 years ago
            //   2013    : Specific year

            // First check if its an explicit static year value
            DateTime tempDate;
            if (DateTime.TryParseExact(yearExpression, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tempDate))
            {
                year = tempDate.Year;
            }
            // Next check if it is a special code value
            else if (Regex.IsMatch(yearExpression, @"^(C)$"))
            {
                year = DateTime.Today.Year;
            }
            // Next check if it is a special code value with an positive offset
            else if (Regex.IsMatch(yearExpression, @"^(C)(\+)\d+$"))
            {
                var match = Regex.Match(yearExpression, @"\d+$");
                int offset = Convert.ToInt32(match.Value);
                year = DateTime.Today.Year + offset;
            }
            // Next check if it is a special code value with an negative offset
            else if (Regex.IsMatch(yearExpression, @"^(C)(\-)\d+$"))
            {
                var match = Regex.Match(yearExpression, @"\d+$");
                int offset = Convert.ToInt32(match.Value);
                year = DateTime.Today.Year - offset;
            }
            else
            {
                throw new Exception("Invalid year value in date expression string.");
            }
            #endregion

            #region Parse Month
            // Supported values:
            //   C       : Current month
            //   FY      : First month of year
            //   LY      : Last month of year
            //   C-3     : 3 months ago
            //   11      : Specific month (1-12)

            // First check if its an explicit static month value (1-12)
            int tempMonth;
            if (Int32.TryParse(monthExpression, out tempMonth) && tempMonth >= 1 && tempMonth <= 12)
            {
                month = tempMonth;
            }
            // Next check if it is a special code value
            else if (Regex.IsMatch(monthExpression, @"^(C|FY|LY)$"))
            {
                month = ParseMonthCode(monthExpression);
            }
            // Next check if it is a special code value with an offset
            else if (Regex.IsMatch(monthExpression, @"^(?<code>C|FY|LY)(?<op>\+|\-)(?<offset>\d+)$"))
            {
                var match = Regex.Match(monthExpression, @"^(?<code>C|FY|LY)(?<op>\+|\-)(?<offset>\d+)$");
                int monthStartingPoint = ParseMonthCode(match.Groups["code"].Value);
                int offset = Convert.ToInt32(match.Groups["offset"].Value);
                // Convert offset to negative number if op is minus
                if (match.Groups["op"].Value == "-")
                {
                    offset = -System.Math.Abs(offset);
                }
                tempDate = new DateTime(year, monthStartingPoint, 1).AddMonths(offset);
                month = tempDate.Month;
                year = tempDate.Year;
            }
            else
            {
                throw new Exception("Invalid month value in date expression string.");
            }
            #endregion

            #region Parse Day
            // Supported values:
            //   C       : Current day
            //   FW      : First day of week
            //   LW      : Last day of week
            //   FM      : First day of month
            //   LM      : Last day of month
            //   C-3     : 3 days ago
            //   11      : Specific day (1-31)

            // First check if its an explicit static day value (1-31)
            int tempDay;
            if (Int32.TryParse(dayExpression, out tempDay) && tempDay >= 1 && tempDay <= 31)
            {
                day = tempDay;
            }
            // Next check if it is a special code value
            else if (Regex.IsMatch(dayExpression, @"^(C|FW|LW|FM|LM)$"))
            {
                day = ParseDayCode(dayExpression, ref month, ref year);
            }
            // Next check if it is a special code value with an offset
            else if (Regex.IsMatch(dayExpression, @"^(?<code>C|FW|LW|FM|LM)(?<op>\+|\-)(?<offset>\d+)$"))
            {
                var match = Regex.Match(dayExpression, @"^(?<code>C|FW|LW|FM|LM)(?<op>\+|\-)(?<offset>\d+)$");
                int dayStartingPoint = ParseDayCode(match.Groups["code"].Value, ref month, ref year);
                int offset = Convert.ToInt32(match.Groups["offset"].Value);
                // Convert offset to negative number if op is minus
                if (match.Groups["op"].Value == "-")
                {
                    offset = -System.Math.Abs(offset);
                }
                tempDate = new DateTime(year, month, dayStartingPoint).AddDays(offset);
                day = tempDate.Day;
                month = tempDate.Month;
                year = tempDate.Year;
            }
            else
            {
                throw new Exception("Invalid day value in date expression string.");
            }
            #endregion

            return new LocalDate(year, month, day);
        }

        private static int ParseMonthCode(string monthCode)
        {
            int month = 0;

            switch (monthCode)
            {
                case "C":
                    month = DateTime.Today.Month;
                    break;
                case "FY":
                    month = 1;
                    break;
                case "LY":
                    month = 12;
                    break;
            }

            return month;
        }

        private static int ParseDayCode(string dayCode, ref int month, ref int year)
        {
            int day = 0;
            DateTime d;

            switch (dayCode)
            {
                case "C":
                    day = DateTime.Today.Day;
                    break;
                case "FW":
                    d = DateTime.Today.AddDays(0 - Convert.ToInt32(DateTime.Today.DayOfWeek));
                    year = d.Year;
                    month = d.Month;
                    day = d.Day;
                    break;
                case "LW":
                    d = DateTime.Today.AddDays(6 - Convert.ToInt32(DateTime.Today.DayOfWeek));
                    year = d.Year;
                    month = d.Month;
                    day = d.Day;
                    break;
                case "FM":
                    day = 1;
                    break;
                case "LM":
                    day = DateTime.DaysInMonth(year, month);
                    break;
            }

            return day;
        }
    }
}
