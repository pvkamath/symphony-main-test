﻿using NodaTime;
using System;

namespace Symphony.Core
{
    public struct ZonedDateRange
    {
        public ZonedDateTime StartDate;
        public ZonedDateTime EndDate;

        public ZonedDateRange(LocalDateRange localDateRange, string timeZoneId)
            : this(localDateRange, timeZoneId, DateTimeZoneProviders.Bcl)
        {
        }

        public ZonedDateRange(LocalDateRange localDateRange, string timeZoneId, IDateTimeZoneProvider timeZoneProvider)
        {
            var timeZone = timeZoneProvider[timeZoneId];
            StartDate = timeZone.AtStartOfDay(localDateRange.StartDate);
            // For EndDate add 1 day & subtract 1 tick (to include the end day)
            EndDate = timeZone.AtStartOfDay(localDateRange.EndDate.PlusDays(1)).Minus(Duration.FromTicks(1));
        }

        public ZonedDateRange(ZonedDateTime startDate, ZonedDateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}
