<xsl:stylesheet version="1.0"
    xmlns="urn:schemas-microsoft-com:office:spreadsheet"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:user="urn:my-scripts"
 xmlns:symphony = "urn:symphony"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" > 
 
<xsl:template match="/">
  <xsl:text disable-output-escaping="yes">&lt;&#63;mso-application progid="Excel.Sheet"&#63;&gt;</xsl:text>
  <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
    xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:x="urn:schemas-microsoft-com:office:excel"
    xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
    xmlns:html="http://www.w3.org/TR/REC-html40">
    <Styles>
      <Style ss:ID="sMultiLine">
        <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
      </Style>
      <Style ss:ID="sShortDate">
        <NumberFormat ss:Format="[$-409]m/d/yy\ h:mm\ AM/PM;@"/>
      </Style>
    </Styles>
    <xsl:apply-templates/>
  </Workbook>
</xsl:template>


<xsl:template match="/*">
  <Worksheet>
  <xsl:attribute name="ss:Name">
  <xsl:value-of select="local-name(/*/*)"/>
  </xsl:attribute>
    <Table x:FullColumns="1" x:FullRows="1">
      <Row>
        <xsl:for-each select="*[position() = 1]/*">
          <Cell><Data ss:Type="String">
          <xsl:value-of select="local-name()"/>
          </Data></Cell>
        </xsl:for-each>
      </Row>
      <xsl:apply-templates/>
    </Table>
  </Worksheet>
</xsl:template>


<xsl:template match="/*/*">
  <Row>
    <xsl:apply-templates/>
  </Row>
</xsl:template>


<xsl:template match="/*/*/*">
  <Cell>
    <!-- if there are newlines (found as #10;), then add the multi-line style which'll tag em with word wrapping -->
    <xsl:if test="contains(.,'#10;')">
      <xsl:attribute name="ss:StyleID">sMultiLine</xsl:attribute>
    </xsl:if>
    <xsl:if test="symphony:GetColumnType(local-name()) = 'DateTime'">
      <xsl:attribute name="ss:StyleID">sShortDate</xsl:attribute>
    </xsl:if>
    <Data>
      <xsl:attribute name="ss:Type">
        <xsl:value-of select="symphony:GetColumnType(local-name())"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="symphony:GetColumnType(local-name()) = 'DateTime'">
          <xsl:value-of select="symphony:FormatDate(.)"/>
        </xsl:when>
        <xsl:otherwise>
          <!-- the format string is necessary because it cleans up invalid characters -->
          <xsl:value-of select="symphony:FormatString(.)"/>
        </xsl:otherwise>
      </xsl:choose>
    </Data>
  </Cell>
</xsl:template>


</xsl:stylesheet>
