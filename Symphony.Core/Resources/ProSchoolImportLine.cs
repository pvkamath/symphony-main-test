﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Resources
{
    // Only used for importing proschool csv
    class ProSchoolImportLine
    {
        public string CourseTitle { get; set; }
        public string CourseCode { get; set; }
        public string ChapterTitle { get; set; }
        public string SectionTitle { get; set; }
        public string ContentPath { get; set; }
        public string FilePath { get; set; }
        public List<ArtisanPage> Pages { get; set; }
        public bool PathProblem { get; set; }
        public bool DownloadCompleted { get; set; }
        public bool Processed { get; set; }
        public bool IsDuplicate { get; set; }
        public bool FileNotFound { get; set; }
        public bool IsHtml { get; set; }
        public ProSchoolImportLine DuplicateOf { get; set; }
    }
}
