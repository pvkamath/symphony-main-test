﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symphony.ExternalSystemIntegration.Models
{
    public class ExternalCourse
    {
        public string Code { get; set; }
        public int Id { get; set; }
        public bool Complete { get; set; }
        public string Title { get; set; }
    }
}
