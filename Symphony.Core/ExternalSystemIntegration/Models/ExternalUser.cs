﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symphony.ExternalSystemIntegration.Models
{
    public class ExternalUser
    {
        public string UserName { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SystemCodeName { get; set; }
        /// <summary>
        /// If true, this indicates that their must be another system 
        /// other than symphony where this user can log in. 
        /// </summary>
        public bool RequiresValidation { get; set; }
        /// <summary>
        /// Specifically for the Symphony login handler. If this is set then it will 
        /// point to a different subdomain on login.
        /// </summary>
        public string Subdomain { get; set; }
    }
}
