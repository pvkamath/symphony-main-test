﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.ExternalSystemIntegration.Handlers;
using Symphony.Core.Models;

namespace Symphony.ExternalSystemIntegration.Models
{
    public class ExternalLoginResult
    {
        public ExternalUser User { get; set; }
        public List<TrainingProgram> TrainingPrograms { get; set; }

        public string Form { get; set; }
        public string FormID { get; set; }
        public string FrameID { get; set; }
        public int FormType { get; set; }
        public string LandingPage { get; set; }

        public AbstractExternalSystemHandler System { get; set; }

        public bool IsLoggedIn
        {
            get
            {
                if (User != null && User.Id > 0)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
