﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.ExternalSystemIntegration.HttpController
{
    public class Workflow
    {

        public int ScreenScrapingId { get; set; }
        public string ReportName { get; set; }
        public string ActionName { get; set; }
        public int? ActionOrder { get; set; }
        public string ActionParameter1 { get; set; }
        public string ActionParameter2 { get; set; }
        public string ActionParameter3 { get; set; }
        public string ActionParameter4 { get; set; }
        public string ActionParameter5 { get; set; }
    }
}
