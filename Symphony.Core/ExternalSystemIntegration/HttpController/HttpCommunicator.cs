﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Web;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data.SqlClient;
using SubSonic;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using log4net;

namespace Symphony.ExternalSystemIntegration.HttpController
{
    public class SystemLoginResult
    {
        private string _SystemName;
        public string SystemName
        {
            get { return _SystemName; }
            set { _SystemName = value; }
        }

        private string _ResultItemName;
        public string ResultItemName
        {
            get { return _ResultItemName; }
            set { _ResultItemName = value; }
        }

        private string _ResultItemValue;
        public string ResultItemValue
        {
            get { return _ResultItemValue; }
            set { _ResultItemValue = value; }
        }
    }

    public class StepResults
    {

        private int _ReferenceStepId;
        private string _StepResult1;
        private string _StepResult2;
        private string _StepResult3;
        private string _StepResult4;

        public int ReferenceStepId
        {
            get { return _ReferenceStepId; }
            set { _ReferenceStepId = value; }
        }

        public string StepResult1
        {
            get { return _StepResult1; }
            set { _StepResult1 = value; }
        }

        public string StepResult2
        {
            get { return _StepResult2; }
            set { _StepResult2 = value; }
        }

        public string StepResult3
        {
            get { return _StepResult3; }
            set { _StepResult3 = value; }
        }

        public string StepResult4
        {
            get { return _StepResult4; }
            set { _StepResult4 = value; }
        }

    }

    public class WorkFlowStep
    {
        public string ReportName;
        public string ActionName;
        public int ActionOrder;
        public string AP1;
        public string AP2;
        public string AP3;
        public string AP4;
        public string AP5;
    }

    public class HttpCommunicator
    {

        private static ILog Log = LogManager.GetLogger(typeof(HttpCommunicator));

        private int _NumberOfFilesExtracted = 0;
        private int _CurrentStepId = 0;

        private const string CONST_RESULT_FROM_PREVIOUS_STEP = "{@@PREVIOUS_STEP:GET_STEP_RESULT:4}";

        List<WorkFlowStep> listOfSteps = new List<WorkFlowStep>();
        List<StepResults> listResultCalls = new List<StepResults>();
        public Dictionary<string, string> listOfNameValueResults = new Dictionary<string, string>();
        public Dictionary<string, string> externalParameters = new Dictionary<string, string>();

        public bool CommunicateToSystem(string SystemName, List<string> ExternalParameters)
        {
            this.SystemName = SystemName;
            int i = 1;
            if (ExternalParameters != null)
            {
                foreach (string externalParameter in ExternalParameters)
                {
                    externalParameters.Add(string.Format("@@External{0}", i++), externalParameter);
                }
            }
            bool ret = false;
            try
            {
                //DB db = new DB();
                LoadSteps();
                if (ExternalParameters != null)
                {
                    for (var m = 0; m < listOfSteps.Count; m++)
                    {
                        for (var j = 0; j < ExternalParameters.Count; j++)
                        {
                            var extParam = ExternalParameters[j];
                            listOfSteps[m].AP1 = listOfSteps[m].AP1.Replace(string.Format("@@External{0}", j), extParam);
                            listOfSteps[m].AP2 = listOfSteps[m].AP2.Replace(string.Format("@@External{0}", j), extParam);
                        }
                    }
                }
                int countOfSteps = listOfSteps.Count;
                int stepId = 0;
                while (stepId < countOfSteps)
                {
                    string stepName = listOfSteps[stepId].ActionName;

                    if (stepName == "HTTP_REQUEST") HandleHttpRequest(stepId);

                    if (stepName == "EXTRACT_TOKEN") ExtractToken(stepId);
                    if (stepName == "BUILD_EXPRESSION") BuildExpression(stepId);
                    if (stepName == "URL_ENCODE") UrlEncode(stepId);
                    if (stepName == "REPLACE") ValueReplace(stepId);
                    if (stepName == "TAKE_MATCH") TakeMatch(stepId);

                    stepId++;
                    _CurrentStepId = stepId;
                }
                ret = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return ret;

        }

        #region Private Variables

        private bool AllowDataBaseLog = false;
        private string DownloadedFilesLocation = "";
        private string OutputFile = "";
        private string DropFolder = "";
        private string SystemName = "";

        private bool FlagToWaitBetweenSteps = false;

        private string _URLLogin = "";
        private string _URLLoginValidate = "";
        private string _URLloginPost = "";

        private string _URLContentInit = "";
        private string _URLContentIntitPost = "";

        private string _URLContentFileLink = "";
        private string _URLContentFileLinkPost = "";

        private string _URLContentFileCSV = "";
        private string _URLContentFileCSVPost = "";

        private string _AccountForm = "";
        private string _URLCheckAccount = "";

        private string _LoginValidateStr = "";
        private CookieCollection oCookies = new CookieCollection();

        private bool _LoggedOn = false;

        private string _AuthBasicLogin;
        public string AuthBasicLogin
        {
            get { return _AuthBasicLogin; }
            set { _AuthBasicLogin = value; }
        }

        private string _AuthBasicPwd;
        public string AuthBasicPwd
        {
            get { return _AuthBasicPwd; }
            set { _AuthBasicPwd = value; }
        }

        private string _ExtractedFileName;
        public string ExtractedFileName
        {
            get { return _ExtractedFileName; }
            set { _ExtractedFileName = value; }
        }
        private string _ChangedFileName;
        public string ChangedFileName
        {
            get { return _ChangedFileName; }
            set { _ChangedFileName = value; }
        }

        public int NumberOfFilesExtracted
        {
            get
            {
                return _NumberOfFilesExtracted;
            }
        }

        public string URLContentInit
        {
            get { return _URLContentInit; }
            set { _URLContentInit = value; }
        }

        public string URLContentIntitPost
        {
            get { return _URLContentIntitPost; }
            set { _URLContentIntitPost = value; }
        }

        public string URLContentFileLink
        {
            get { return _URLContentFileLink; }
            set { _URLContentFileLink = value; }
        }

        public string URLContentFileLinkPost
        {
            get { return _URLContentFileLinkPost; }
            set { _URLContentFileLinkPost = value; }
        }

        public string URLContentFileCSV
        {
            get { return _URLContentFileCSV; }
            set { _URLContentFileCSV = value; }
        }

        public string URLContentFileCSVPost
        {
            get { return _URLContentFileCSVPost; }
            set { _URLContentFileCSVPost = value; }
        }

        #endregion

        #region Properties

        public bool LoggedOn
        {
            get { return _LoggedOn; }
        }

        public string URLLogin
        {
            get { return _URLLogin; }
            set { _URLLogin = value; }
        }

        public string URLLoginValidate
        {
            get { return _URLLoginValidate; }
            set { _URLLoginValidate = value; }
        }

        public string URLloginPost
        {
            get { return _URLloginPost; }
            set { _URLloginPost = value; }
        }

        public string GetAccountForm
        {
            get { return _AccountForm; }
            set { _AccountForm = value; }
        }

        public string URLCheckAccount
        {
            get { return _URLCheckAccount; }
            set { _URLCheckAccount = value; }
        }

        public string LoginValidateString
        {
            get { return _LoginValidateStr; }
            set { _LoginValidateStr = value; }
        }

        #endregion

        private int DbCheck(int stepId)
        {
            int ret = 0;
            string finalOutput = "";
            WorkFlowStep ws = listOfSteps[stepId];
            string strSource = ws.AP1;
            if (strSource == CONST_RESULT_FROM_PREVIOUS_STEP) strSource = listResultCalls[stepId - 1].StepResult4;
            //DB db = new DB();
            try
            {
                ret = 0; //db.GetOneValue<int>(strSource);
                //Log.AddLog("DB Check returned : " + ret.ToString());
            }
            catch (Exception ex)
            {

            }
            StepResults itemUrl = new StepResults { ReferenceStepId = stepId, StepResult1 = "", StepResult2 = "", StepResult3 = "", StepResult4 = finalOutput };
            listResultCalls.Add(itemUrl);
            return ret;
        }

        private void ValueReplace(int stepId)
        {
            string finalOutput = "";
            WorkFlowStep ws = listOfSteps[stepId];
            string strSource = ws.AP1;
            string strWhat = ws.AP2;
            if (strWhat.Contains("@@") == true) 
            {
                int stepIdToExtract = -1;
                string part2step = strWhat.Split(':')[1];
                int.TryParse(part2step, out stepIdToExtract);
                strWhat = GetValueForStep(strWhat, stepId); 
            }
            string strTo = ws.AP3;
            if (strTo.Contains("@@") == true)
            {
                int stepIdToExtract = -1;
                string part3step = strTo.Split(':')[1];
                int.TryParse(part3step, out stepIdToExtract);
                strTo = GetValueForStep(strTo, stepId);
            }
            try
            {
                if (strSource == CONST_RESULT_FROM_PREVIOUS_STEP) strSource = listResultCalls[stepId - 1].StepResult4;
                finalOutput = strSource.Replace(strWhat, strTo);
            }
            catch (Exception ex)
            {
                //Log.AddLog("Error : " + ex.Message);
            }
            StepResults itemUrl = new StepResults { ReferenceStepId = stepId, StepResult1 = "", StepResult2 = "", StepResult3 = "", StepResult4 = finalOutput };
            listResultCalls.Add(itemUrl);
        }

        private void TakeMatch(int stepId)
        {
            string mmValue = "";
            WorkFlowStep ws = listOfSteps[stepId];
            string strSource = ws.AP1;
            string regWhat = ws.AP2;
            int strSeqValue = Convert.ToInt32(ws.AP3) - 1;
            string strExtra1 = ws.AP4.Replace("{", "").Replace("}", "");
            string strExtra2 = ws.AP5;
            if (strSource == CONST_RESULT_FROM_PREVIOUS_STEP) strSource = listResultCalls[stepId - 1].StepResult4;
            if (strSource.Contains("@@") == true && !strSource.Contains("@@SPLITVALUE@@")) strSource = GetValueForStep(strSource, stepId);
            if (regWhat.Contains("@@") == true) regWhat = GetValueForStep(regWhat, stepId);
            MatchCollection mc = Regex.Matches(strSource, regWhat);
            try
            {
                if (mc.Count > strSeqValue && strSeqValue >= 0)
                {
                    Match mm = mc[strSeqValue];
                    mmValue = mm.Value;
                }

                if (ws.AP4.Contains("FIND") == true)
                {
                    string matchToFindValue = ws.AP5;
                    mmValue = "";
                    foreach (Match mf in mc)
                    {

                        if (mf.Value.Contains(matchToFindValue) == true)
                        {
                            mmValue = mf.Value;
                            break;
                        }
                    }
                }

                if (strSeqValue < 0)
                {
                    if (strExtra1.Contains("CONTAINS") == true)
                    {
                        string matchToFindValue = GetValueForStep(strExtra2, stepId);
                        foreach (Match mf in mc)
                        {
                            mmValue = mf.Value;
                            if (mmValue.Contains(matchToFindValue) == true) break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.AddLog("Error : " + ex.Message);
            }

            StepResults itemUrl = new StepResults { ReferenceStepId = stepId, StepResult1 = "", StepResult2 = "", StepResult3 = "", StepResult4 = mmValue };
            listResultCalls.Add(itemUrl);

        }

        private string GetValueForStep(string StepAndValues, int stepId)
        {
            StepAndValues = StepAndValues.Replace("{", "").Replace("}", "");
            string ret = "";
            string[] mix = StepAndValues.Split(':');
            try
            {
                string step = mix[1];
                int stepVal = Convert.ToInt32(step) - 1; // -1 to shif steps to comply C# list index starting with 0
                if (stepVal < 0) stepVal = stepId + stepVal;
                string stepRes = mix[3];
                switch (stepRes)
                {
                    case "1":
                        ret = listResultCalls[stepVal].StepResult1;
                        break;
                    case "2":
                        ret = listResultCalls[stepVal].StepResult2;
                        break;
                    case "3":
                        ret = listResultCalls[stepVal].StepResult3;
                        break;
                    case "4":
                        ret = listResultCalls[stepVal].StepResult4;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                //Log.AddLog("Error : " + ex.Message);
            }
            return ret;
        }

        private void HoldOnMinutes(int stepId)
        {
            WorkFlowStep ws = listOfSteps[stepId];
            string strWait = ws.AP1;
            int waitInMin = Convert.ToInt32(strWait) * 60 * 1000;

            string finalOutput = "WAITING";
            if (this.AllowDataBaseLog == true)
            {
                //DB db = new DB();
                string finalOutputLower = finalOutput.ToLower();
                if (finalOutputLower.IndexOf("userid") > 0)
                {
                    finalOutputLower = finalOutputLower.Substring(0, finalOutputLower.IndexOf("userid"));
                }
                //db.LogIntoDB("ScreenScraper", finalOutputLower, "HOLD_ON_MIN", ws.AP1, ws.AP2, Storage.ReportName);
            }

            StepResults itemExpression = new StepResults { ReferenceStepId = stepId, StepResult1 = "HOLD_ON_MIN", StepResult2 = "", StepResult3 = "", StepResult4 = finalOutput };
            listResultCalls.Add(itemExpression);

            System.Threading.Thread.Sleep(waitInMin);

        }

        private string BuildRequestDataCustom(string tr)
        {
            List<string> listOfValues = new List<string>();
            string ret = "";
            //fileName=C0092R8T.REJECTS.030213110427.001.CSV&downloadType=Rejects&downloadCreationDateTime=03%2F02%2F2013+11%3A04%3A27.000&downloadId=354373
            switch (this.SystemName)
            {
                case "FNMA_Upload_LAR_ACK":
                case "FNMA_Rejects_From_SURF":
                case "FNMA_MBS_Reclass":
                    string requestData = "fileName={0}&downloadType={1}&downloadCreationDateTime={2}&downloadId={3}";
                    string regTR = @"value=""[\s\S]*?""";
                    MatchCollection mc = Regex.Matches(tr, regTR);
                    int paramPosition = 0;
                    foreach (Match mm in mc)
                    {
                        string paramReplaceElement = "{" + paramPosition.ToString().Trim() + "}";
                        string mmvalue = HttpUtility.UrlEncode(mm.Value.Replace("value=", "").Replace("\"", ""));
                        requestData = requestData.Replace(paramReplaceElement, mmvalue);
                        paramPosition++;
                    }
                    ret = requestData;
                    //ret = Uri.EscapeDataString(requestData);
                    break;
                default:
                    break;
            }
            return ret;
        }

        private bool CheckMatchValue(string MValue, string KeyWord, string[] allDays)
        {
            bool ret = false;
            //ret = MValue.Contains(KeyWord) == true && (MValue.Contains(TodayValue) == true || MValue.Contains(YesterdayValue) == true);

            foreach (string dvalue in allDays)
            {
                if (dvalue == null) continue;
                ret = MValue.Contains(KeyWord) && MValue.Contains(dvalue);
                if (ret == true)
                    break;
            }
            if (this.SystemName == "Upload_LAR")
            {
                ret = MValue.Contains(KeyWord);
            }
            return ret;
        }

        private void BuildExpression(int stepId)
        {
            try
            {
                string finalOutput = "";
                WorkFlowStep ws = listOfSteps[stepId];
                string strSource = ws.AP2;
                string strParameter = "";
                string[] arrayAP1 = ws.AP1.Replace("{", "").Replace("}", "").Split(':');
                if (ws.AP1 == CONST_RESULT_FROM_PREVIOUS_STEP) strParameter = listResultCalls[stepId - 1].StepResult4;
                if (ws.AP2.Contains("@@STEP") == true) strSource = GetValueForStep(ws.AP2, stepId);
                if (ws.AP1.Contains("@@STEP") == true) strParameter = GetValueForStep(ws.AP1, stepId);
                string strExtra1 = ws.AP3;
                /*
                 * if (strExtra1 == "SQL_CALL_CYCLE")
                    finalOutput = BUILD_EXPRESSION_SQL_CYCLE(stepId);
                 */
                if (strExtra1 == "CONCATENATE")
                    finalOutput = strSource + strParameter;
                if (strExtra1 == "CONCATENATE_REVERSE")
                    finalOutput = strParameter + strSource;

                if (strSource.Contains("{Year}"))
                {
                    strSource = strSource.Replace("{Year}", DateTime.Now.Year.ToString()).Replace("{Month}", DateTime.Now.Month.ToString("00"));
                }

                if (strExtra1 == "PART")
                    finalOutput = strParameter.Remove(0, (strParameter.Length - Convert.ToInt32(strSource)));


                //if (strSource.Contains("@@Login") == true) strSource = GetWebSiteLogin(strSource, ws.AP3);

                if (strExtra1 == "" || strExtra1.Contains("ENC") || strExtra1.Contains("REG")) finalOutput = string.Format(strSource, strParameter);
                if (this.AllowDataBaseLog == true)
                {
                    //DB db = new DB();
                    string finalOutputLower = finalOutput.ToLower();
                    if (finalOutputLower.IndexOf("userid") > 0)
                    {
                        finalOutputLower = finalOutputLower.Substring(0, finalOutputLower.IndexOf("userid"));
                    }
                    //db.LogIntoDB("ScreenScraper", ResizeStringBy(finalOutputLower, 200), "BUILD_EXPRESSION", ws.AP1, ResizeStringBy(ws.AP2, 200), Storage.ReportName);
                }

                StepResults itemExpression = new StepResults { ReferenceStepId = stepId, StepResult1 = "RESULT OF BUILD_EXPRESSION", StepResult2 = strParameter, StepResult3 = "", StepResult4 = finalOutput };
                listResultCalls.Add(itemExpression);
                strParameter = ws.AP2;
            }
            catch (Exception ex)
            {
                //Log.AddLog("Error : " + ex.Message);
            }
        }

        private void UrlEncode(int stepId)
        {
            try
            {

                WorkFlowStep ws = listOfSteps[stepId];
                // string strSource = ws.AP1;
                string strParameter = "";

                strParameter = listResultCalls[stepId - 1].StepResult4;
                string finalOutput = System.Web.HttpUtility.UrlEncode(strParameter);

                if (this.AllowDataBaseLog == true)
                {
                    //DB db = new DB();
                    string finalOutputLower = finalOutput.ToLower();
                    if (finalOutputLower.IndexOf("userid") > 0)
                    {
                        finalOutputLower = finalOutputLower.Substring(0, finalOutputLower.IndexOf("userid"));
                    }
                    //db.LogIntoDB("ScreenScraper", ResizeStringBy(finalOutputLower, 200), "URL_ENCODE", ws.AP1, ResizeStringBy(ws.AP2, 200), Storage.ReportName);
                }

                StepResults itemExpression = new StepResults { ReferenceStepId = stepId, StepResult1 = "RESULT OF Url Encode", StepResult2 = strParameter, StepResult3 = "", StepResult4 = finalOutput };
                listResultCalls.Add(itemExpression);
                strParameter = ws.AP2;
            }
            catch (Exception ex)
            {
                //Log.AddLog("Error : " + ex.Message);
            }
        }

        private void ExtractToken(int stepId)
        {

            WorkFlowStep ws = listOfSteps[stepId];
            string regSource = "";
            string regPattern = "";
            string[] arrayAP1 = ws.AP1.Replace("{", "").Replace("}", "").Split(':');
            //Log.AddLog("wsAP1: " + ws.AP1);
            regPattern = ws.AP2;
            if (ws.AP1 == CONST_RESULT_FROM_PREVIOUS_STEP) regSource = listResultCalls[stepId - 1].StepResult4;
            if (regSource != "" && regPattern != "")
            {
                Match m = Regex.Match(regSource, regPattern);
                string mvalue = m.Groups[m.Groups.Count - 1].Value;
                if (string.IsNullOrEmpty(mvalue) == false)
                {
                    StepResults itemToken = new StepResults { ReferenceStepId = stepId, StepResult1 = m.Value, StepResult2 = regPattern, StepResult3 = "", StepResult4 = mvalue };
                    if (this.AllowDataBaseLog == true)
                    {
                        //DB db = new DB();
                        //db.LogIntoDB("ScreenScraper", mvalue, "EXTRACT_TOKEN", ws.AP1, ws.AP2, Storage.ReportName);
                    }
                    listResultCalls.Add(itemToken);
                }
            }
        }

        private void HandleHttpRequest(int stepId)
        {
            WorkFlowStep ws = listOfSteps[stepId];
            string url = ws.AP1;
            string urlParameters = ws.AP2;
            string extra1 = ws.AP3;
            string extra2 = ws.AP4;
            string extra3 = ws.AP5;
            Cookie customCookie = null;

            if (extra1.Contains("AUTHORIZATION_HEADER_BASIC") == true)
            {
                string[] auth = ws.AP4.Split(':');
                _AuthBasicLogin = auth[0];
                _AuthBasicPwd = auth[1];
            }
            if (extra1.Contains("EXTRACT_FILE") == true)
            {
                ExtractedFileName = "EXTRACT_FILE";

                if (extra2.Contains("@@STEP:") == true)
                    ChangedFileName = GetValueForStep(extra2, 0);
                else
                    ChangedFileName = extra2;

            }
            if (extra1.Contains("ADD_CUSTOM_COOKIE") == true)
            {
                extra2 = extra2.Replace("{", "").Replace("}", "");
                string[] acookie = extra2.Split(':');
                string cookiename = acookie[0];
                string cookievalue = acookie[1];
                string cookiepath = acookie[2];
                string cookiedomain = acookie[3];

                customCookie = new Cookie(cookiename, cookievalue, cookiepath, cookiedomain);
            }

            if (url == CONST_RESULT_FROM_PREVIOUS_STEP) url = listResultCalls[stepId - 1].StepResult4;
            if (url.Contains("@@STEP:") == true) url = GetValueForStep(url, 4);
            if (urlParameters.Contains("@@STEP:") == true) urlParameters = GetValueForStep(ws.AP2, 4).Replace("&", "");
            if (urlParameters == CONST_RESULT_FROM_PREVIOUS_STEP) urlParameters = listResultCalls[stepId - 1].StepResult4;
            if (urlParameters == "" && ws.AP3 == "CLEAR_COOKIES") oCookies = new CookieCollection();

            if (extra2.Contains("ADD_REPORT_DATE") == true)
            {
                urlParameters = extra2 + urlParameters;
            }

            string response = "";
            if (extra3 == "SaveProSchool_LT_Value")
            {
                response = GetPage(url, urlParameters, null, new List<string>() { extra3 });
            }
            else
            {
                if (customCookie == null)
                {
                    if (extra2 == "json")
                    {
                        response = GetPage(URL: url, RequestData: urlParameters, jsonFlag: true);
                    }
                    else
                    {
                        response = GetPage(url, urlParameters);
                    }
                }
                else
                {
                    response = GetPage(url, urlParameters, customCookie);
                }
            }
            StepResults itemUrl = new StepResults { ReferenceStepId = stepId, StepResult1 = url, StepResult2 = urlParameters, StepResult3 = "", StepResult4 = response };
            if (this.AllowDataBaseLog == true)
            {
                //DB db = new DB();
                //db.LogIntoDB("ScreenScraper", ResizeStringBy(response, 180), "HTTP_REQUEST", ResizeStringBy(ws.AP1, 180), ResizeStringBy(ws.AP2, 180), Storage.ReportName);
            }
            listResultCalls.Add(itemUrl);
            if (extra3 == "SaveResult")
            {
                bool loginWasSuccessful = response.ToLower().Contains("<title>cas success</title>");
                listOfNameValueResults.Add("LoginSuccess", loginWasSuccessful.ToString());
            }
        }

        private string ResizeStringBy(string SourceString, int SizeStringTo)
        {
            string ret = "";
            if (SourceString.Length > SizeStringTo) SourceString = SourceString.Substring(0, SizeStringTo - 1);
            ret = SourceString;
            return ret;
        }

        private void LoadSteps()
        {
            List<Workflow> listOfWorkflowSteps = new CodingHorror().ExecuteTypedList<Workflow>(string.Format(@"SELECT ScreenScrapingId, ReportName, ActionName, 
                ActionOrder, ActionParameter1, ActionParameter2, ActionParameter3, ActionParameter4, ActionParameter5  FROM dbo.Workflows WITH (nolock) 
                WHERE ReportName = @SystemName ORDER BY ActionOrder"), this.SystemName);
            foreach (Workflow workflow in listOfWorkflowSteps)
            {
                WorkFlowStep ws = new WorkFlowStep();
                ws.ReportName = this.SystemName;
                ws.ActionName =workflow.ActionName;
                ws.ActionOrder = workflow.ActionOrder.Value;
                ws.AP1 = workflow.ActionParameter1;
                ws.AP2 = workflow.ActionParameter2;
                ws.AP3 = workflow.ActionParameter3;
                ws.AP4 = workflow.ActionParameter4;
                ws.AP5 = workflow.ActionParameter5;
                listOfSteps.Add(ws);
            }
        }

        private string GetEvaluatedParameter(List<string> listParameters, string ParamValue)
        {
            string ret = ParamValue;
            int i = 1;
            foreach (string p in listParameters)
            {
                if (ret.Contains("@@") == true)
                {
                    int zz = 0;
                }
                ret = ret.Replace("{@@p" + i.ToString().Trim() + "}", p);
                i++;
            }
            return ret;
        }

        public void HoldOn()
        {
            Random rnd = new Random(DateTime.Now.TimeOfDay.Seconds);
            int interval = 0;
            if (this.FlagToWaitBetweenSteps == false)
            {
                interval = 0;
            }
            if (this.FlagToWaitBetweenSteps == true)
            {
                interval = Convert.ToInt32(rnd.Next(10) * 1000) + 2000;
            }

            System.Threading.Thread.Sleep(interval);
        }

        public bool AlwaysAccept(object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string GetPage(string URL, string RequestData = "", Cookie customCookie = null, List<string> SaveValues = null, bool jsonFlag = false)
        {
            string ret = "";
            //On Error Resume Next
            string logCookieValues = "";
            //' *** Establish request by assigning Url

            HttpWebRequest loHttp = WebRequest.Create(URL.Trim()) as HttpWebRequest;

            //loHttp.Proxy = null;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AlwaysAccept);

            if (string.IsNullOrEmpty(AuthBasicLogin) == false)
            {
                loHttp.Credentials = new NetworkCredential(AuthBasicLogin, AuthBasicPwd);
            }

            //' *** Set any header related and operational properties
            loHttp.Timeout = 12 * 10000; //' 10 secs
            loHttp.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.2; WOW64; Trident/7.0; .NET4.0E; .NET4.0C; .NET CLR 3.5.30729; .NET CLR 2.0.50727; .NET CLR 3.0.30729; InfoPath.2)";
            //loHttp.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0";
            loHttp.KeepAlive = true;

            loHttp.Headers.Add("Accept-Language", "en-US");

            // loHttp.Headers.Add("Accept-Encoding", "gzip,deflate,sdch");

            loHttp.Headers.Add("Cache-Control", "no-cache");
            //'loHttp.Headers.Add("Accept", "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-excel, application/msword, application/vnd.ms-powerpoint, */*")
            loHttp.Headers.Add("Accept-Charset", "text/html,iso-8859-1,*,utf-8");


            //' *** reuse cookies if available
            loHttp.CookieContainer = new CookieContainer();
            CustomCookies(ref this.oCookies, customCookie);
            if ((this.oCookies) != null)
            {
                if (this.oCookies.Count > 0)
                {
                    loHttp.CookieContainer.Add(this.oCookies);
                }
            }

            //' *** Send data if there if there is data to send 
            if (RequestData.Trim() != string.Empty)
            {

                ASCIIEncoding Encoding = new ASCIIEncoding();
                byte[] data = Encoding.GetBytes(RequestData);

                loHttp.Method = "POST";
                loHttp.AllowAutoRedirect = true;
                loHttp.ContentType = "application/x-www-form-urlencoded;text/html; charset=ISO-8859-1";
                if (jsonFlag)
                {
                    loHttp.ContentType = "application/json";
                }

                loHttp.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

                // loHttp.ServicePoint.Expect100Continue = false;
                Stream newStream = loHttp.GetRequestStream();
                //' *** Send the data.
                newStream.Write(data, 0, data.Length);
                newStream.Close();
            }

            if (loHttp.ServicePoint.Expect100Continue)
                loHttp.ServicePoint.Expect100Continue = false;
            // ' *** Return the Response data

            HttpWebResponse loWebResponse = loHttp.GetResponse() as HttpWebResponse;

            //' ** If the server returns any cookies
            //' ** add them to our cookies collection
            if (loWebResponse.Cookies.Count > 0)
            {
                if (this.oCookies != null)
                {
                    this.oCookies = loWebResponse.Cookies;
                }
            }
            else
            {
                //' ** If we already have cookies update the list
                foreach (Cookie oRespCookie in loWebResponse.Cookies)
                {

                    bool bMatch = false;
                    foreach (Cookie oReqCookie in this.oCookies)
                    {
                        if (oReqCookie.Name == oRespCookie.Name)
                        {
                            oReqCookie.Value = oRespCookie.Value;
                            bMatch = true;
                            break;
                        }
                    }
                    if (bMatch == false) { this.oCookies.Add(oRespCookie); }
                }
            }


            if (loHttp.CookieContainer.Count > 0)
            {
                if (this.oCookies != null)
                {
                    //' ** If we already have cookies update the list
                    foreach (Cookie oRespCookie in loHttp.CookieContainer.GetCookies(loHttp.RequestUri))  //'loWebResponse.Cookies
                    {
                        logCookieValues += oRespCookie.Value;
                        bool bMatch = false;
                        foreach (Cookie oReqCookie in this.oCookies)
                        {
                            if (oReqCookie.Name == oRespCookie.Name)
                            {
                                oReqCookie.Value = oRespCookie.Value;
                                bMatch = true;
                                //'Exit For
                            }
                        }
                        if (bMatch == false) { this.oCookies.Add(oRespCookie); }
                    }
                }
                else
                {
                    foreach (Cookie oRespCookie in loHttp.CookieContainer.GetCookies(loHttp.RequestUri))
                    {
                        this.oCookies.Add(oRespCookie);
                    }
                }
            }

            System.Text.Encoding enc = System.Text.Encoding.GetEncoding(1252);   //' Windows-1252 or iso-

            StreamReader loResponseStream = new StreamReader(loWebResponse.GetResponseStream(), enc);
            if (loWebResponse.ContentEncoding.Length > 0) enc = System.Text.Encoding.GetEncoding(loWebResponse.ContentEncoding);
            if (ExtractedFileName == "EXTRACT_FILE")
            {

                ExtractedFileName = loWebResponse.Headers["Content-Disposition"];
                if (!string.IsNullOrEmpty(ExtractedFileName))
                {


                    ExtractedFileName = ExtractedFileName.Replace("filename=", "");
                    if (RequestData.Contains("ADD_REPORT_DATE"))
                    {
                        string[] s = ExtractedFileName.Split('.');
                        ExtractedFileName = s[0] + RequestData.Replace("ADD_REPORT_DATE", ".") + "000000." + s[1];
                    }

                    if (IsHtmlEncoded(ExtractedFileName))
                    {
                        ExtractedFileName = HttpUtility.UrlDecode(ExtractedFileName);
                        ExtractedFileName = ExtractedFileName.Replace("\"", "").Replace("attachment; ", "");

                    }

                    if (ChangedFileName.ToUpper().Contains(".TXT") || ChangedFileName.ToUpper().Contains(".CSV") ||
                        ChangedFileName.ToUpper().Contains(".PDF"))
                    {

                        ExtractedFileName = ChangedFileName;
                        ChangedFileName = "";
                    }

                    string fullFilePath = Path.Combine(this.DownloadedFilesLocation, ExtractedFileName);

                    FileStream fs = System.IO.File.Create(fullFilePath);
                    ReadWriteStream(loResponseStream.BaseStream, fs);

                    ExtractedFileName = ""; //Clear the field

                }
            }
            else
            {
                ret = loResponseStream.ReadToEnd();
            }
            loResponseStream.Close();
            loWebResponse.Close();
            if (SaveValues != null)
            {
                if (SaveValues.Count > 0)
                {
                    listOfNameValueResults.Add("JSESSION", logCookieValues);
                    if (SaveValues.Contains("SaveProSchool_LT_Value") == true)
                    {
                        string strSeqValue = "name=\"lt\" value=\"[\\s\\S]*?\" />";
                        MatchCollection mc = Regex.Matches(ret, strSeqValue);
                        if (mc.Count > 0)
                        {
                            Match mm = mc[0];
                            var mmValue = mm.Value;
                            MatchCollection mmQuoteOnly = Regex.Matches(mmValue, @"(?<="")(?:\\.|[^""\\])*(?="")");
                            if (mmQuoteOnly.Count == 3)
                            {
                                string ltvalue = mmQuoteOnly[2].Value;
                                listOfNameValueResults.Add("LT_VALUE", ltvalue);
                            }
                        }

                    }
                }
            }
            return ret;
            //Err.Clear()
        }

        private bool IsHtmlEncoded(string text)
        {
            return (HttpUtility.UrlDecode(text) != text);
        }

        private void CustomCookies(ref CookieCollection cookieCollection, Cookie newCustomCookie)
        {
            if (newCustomCookie == null) return;
            cookieCollection.Add(newCustomCookie);
        }

        public string GetFile(string URL, string RequestData = "")
        {
            string ret = "";
            if (string.IsNullOrEmpty(URL) == true) return "";
            string _FileInZipName = string.Empty;
            //On Error Resume Next

            //' *** Establish request by assigning Url
            try
            {
                HttpWebRequest loHttp = WebRequest.Create(URL.Trim()) as HttpWebRequest;
                //Dim loHttp As HttpWebRequest = 

                //' *** Set any header related and operational properties
                loHttp.Timeout = 6 * 10000; //' 10 secs
                loHttp.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0; MASMJS)";
                loHttp.KeepAlive = true;

                loHttp.Headers.Add("Accept-Language", "en-US");
                loHttp.Headers.Add("Cache-Control", "no-cache");
                //'loHttp.Headers.Add("Accept", "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-excel, application/msword, application/vnd.ms-powerpoint, */*")
                loHttp.Headers.Add("Accept-Charset", "text/html,iso-8859-1,*,utf-8");

                //' *** reuse cookies if available
                loHttp.CookieContainer = new CookieContainer();
                if ((this.oCookies) != null)
                {
                    if (this.oCookies.Count > 0)
                    {
                        loHttp.CookieContainer.Add(this.oCookies);
                    }
                }

                //' *** Send data if there if there is data to send 
                if (RequestData.Trim() != string.Empty)
                {

                    ASCIIEncoding Encoding = new ASCIIEncoding();
                    byte[] data = Encoding.GetBytes(RequestData);

                    loHttp.Method = "POST";
                    loHttp.AllowAutoRedirect = true;
                    //'If InStr(RequestData, "---------------------------7d431a16820334") > 0 Then
                    //'    loHttp.ContentType = "multipart/form-data; boundary=---------------------------7d431a16820334"
                    //'Else
                    loHttp.ContentType = "application/x-www-form-urlencoded;text/html; charset=ISO-8859-1";
                    //'End If

                    loHttp.ContentLength = data.Length;
                    Stream newStream = loHttp.GetRequestStream();
                    //' *** Send the data.
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();
                }

                // ' *** Return the Response data
                HttpWebResponse loWebResponse = loHttp.GetResponse() as HttpWebResponse;
                string fileName = loWebResponse.Headers["Content-Disposition"];
                if (fileName.Contains("attachment; filename="))
                {
                    fileName = fileName.Replace("attachment; filename=", "");
                    if (this.AllowDataBaseLog == true)
                    {
                        //DB db = new DB();
                        //db.LogIntoDB("ScreenScraper", fileName, "GET_FILE", "", "", Storage.ReportName);
                    }

                }
                if (fileName.Contains("filename=") == true) fileName = fileName.Replace("filename=", "");
                //' ** If the server returns any cookies
                //' ** add 'em to our cookies collection
                if (loWebResponse.Cookies.Count > 0)
                {
                    if (this.oCookies != null)
                    {
                        this.oCookies = loWebResponse.Cookies;
                    }
                }
                else
                {
                    //' ** If we already have cookies update the list
                    foreach (Cookie oRespCookie in loWebResponse.Cookies)
                    {
                        bool bMatch = false;
                        foreach (Cookie oReqCookie in this.oCookies)
                        {
                            if (oReqCookie.Name == oRespCookie.Name)
                            {
                                oReqCookie.Value = oRespCookie.Value;
                                bMatch = true;
                                break;
                            }
                        }
                        if (bMatch == false) { this.oCookies.Add(oRespCookie); }
                    }
                }


                if (loHttp.CookieContainer.Count > 0)
                {
                    if (this.oCookies != null)
                    {
                        //' ** If we already have cookies update the list
                        foreach (Cookie oRespCookie in loHttp.CookieContainer.GetCookies(loHttp.RequestUri))  //'loWebResponse.Cookies
                        {
                            bool bMatch = false;
                            foreach (Cookie oReqCookie in this.oCookies)
                            {
                                if (oReqCookie.Name == oRespCookie.Name)
                                {
                                    oReqCookie.Value = oRespCookie.Value;
                                    bMatch = true;
                                    //'Exit For
                                }
                            }
                            if (bMatch == false) { this.oCookies.Add(oRespCookie); }
                        }
                    }
                    else
                    {
                        foreach (Cookie oRespCookie in loHttp.CookieContainer.GetCookies(loHttp.RequestUri))
                        {
                            this.oCookies.Add(oRespCookie);
                        }
                    }
                }

                System.Text.Encoding enc = System.Text.Encoding.GetEncoding(1252);   //' Windows-1252 or iso-
                if (loWebResponse.ContentEncoding.Length > 0) enc = System.Text.Encoding.GetEncoding(loWebResponse.ContentEncoding);

                StreamReader loResponseStream = new StreamReader(loWebResponse.GetResponseStream(), enc);
                string fileExt = Path.GetExtension(fileName);
                string fullFilePath = Path.Combine(this.DownloadedFilesLocation, fileName);
                FileStream fs = System.IO.File.Create(fullFilePath);
                ReadWriteStream(loResponseStream.BaseStream, fs);
                if (string.IsNullOrEmpty(this.OutputFile) == false)
                {
                    File.Copy(fullFilePath, this.OutputFile, true);
                }
                if (string.IsNullOrEmpty(this.DropFolder) == false)
                {
                    string fullFileNameForDrop = Path.Combine(this.DropFolder, fileName);
                    File.Copy(fullFilePath, fullFileNameForDrop);
                }

                loResponseStream.Close();
                loWebResponse.Close();
                _NumberOfFilesExtracted++;
            }
            catch (Exception ex)
            {
                //Log.AddLog("Error : " + ex.Message);
            }

            return ret;
        }

        private void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 256;
            Byte[] buffer = new Byte[Length];

            int bytesRead = readStream.Read(buffer, 0, Length);
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Close();
            writeStream.Close();
        }

        private string CorrectStep(string strStep, int intCycle, int numOfRows)
        {
            MatchCollection mc = Regex.Matches(strStep, @"@@STEP:[\s\S]*?:");


            Match mm = mc[0];
            string mmValue = mm.Value;
            mmValue = mmValue.Replace("@@STEP:", "").Replace(":", "");

            int correctedStep = Convert.ToInt32(mmValue) + ((numOfRows) * (intCycle - 1));

            return strStep.Replace(mmValue, correctedStep.ToString());

        }

        private string ReplaceRegex(string strSource, string strFindLink, string strReplacement)
        {
            MatchCollection mc = Regex.Matches(strSource, strFindLink);


            Match mm = mc[0];
            string mmValue = mm.Value;

            return strSource.Replace(mmValue, strReplacement);

        }

        private string PadCurrencyNumber(string strSource)
        {
            strSource = strSource.Replace("$", "#");
            MatchCollection mc = Regex.Matches(strSource, @"#[\s\S]*?<");


            Match mm = mc[0];
            string mmValue = mm.Value;

            string strPadNumber = mmValue.Replace("#", "").Replace("<", "");
            strPadNumber = Convert.ToDouble(strPadNumber).ToString("###,###,##0.00").PadLeft(20, ' ');

            return strSource.Replace(mmValue, strPadNumber + "<");

        }


    }

}
