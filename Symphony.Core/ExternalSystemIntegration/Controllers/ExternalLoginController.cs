﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data = Symphony.Core.Data;
using Symphony.Core.Models;
using log4net;
using SubSonic;
using ExternalLoginHandlers = Symphony.ExternalSystemIntegration.Handlers;
using Symphony.ExternalSystemIntegration.Models;
using System.Text.RegularExpressions;
using Symphony.Core.Controllers;
using System.Configuration;
using System.Web;
using Symphony.Core;

namespace Symphony.ExternalSystemIntegration.Controllers
{
    public class ExternalLoginController
    {
        ILog Log = LogManager.GetLogger(typeof(ExternalLoginController));

        private ExternalLoginHandlers.ExternalSystemHandlerTaskRunner loginTasks;
        private List<LoginSystem> currentLoginSystems = new List<LoginSystem>();

        private List<ExternalLoginHandlers.AbstractExternalSystemHandler> GetExternalSystemHandlers(Data.Customer cust, User user, string password, bool isPreAuthenticated)
        {
            List<ExternalLoginHandlers.AbstractExternalSystemHandler> handlersList = new List<ExternalLoginHandlers.AbstractExternalSystemHandler>();

            ExternalSystem symphonyExternalSystem = new ExternalSystem();

            List<ExternalSystem> externalSystems = new Select(
                Data.ExternalSystem.IdColumn.QualifiedName,
                Data.ExternalSystem.IsRequiredColumn.QualifiedName,
                Data.ExternalSystem.LoginUrlColumn.QualifiedName,
                Data.ExternalSystem.SystemCodeNameColumn.QualifiedName,
                Data.ExternalSystem.SystemNameColumn.QualifiedName,
                Data.ExternalSystem.ExternalSystemTypeNameColumn.QualifiedName,
                Data.CustomerExternalSystem.CustomerIdInExternalSystemColumn.QualifiedName,
                Data.CustomerExternalSystem.IsCheckCustomerIdInExternalSystemColumn.QualifiedName
            ).From<Data.CustomerExternalSystem>()
            .InnerJoin(Data.ExternalSystem.IdColumn, Data.CustomerExternalSystem.ExternalSystemIDColumn)
            .Where(Data.CustomerExternalSystem.CustomerIDColumn).IsEqualTo(cust.Id)
            .ExecuteTypedList<ExternalSystem>();

            if (externalSystems != null && externalSystems.Count > 0)
            {
                foreach (ExternalSystem externalSystem in externalSystems)
                {
                    ExternalLoginHandlers.AbstractExternalSystemHandler ex = ExternalLoginHandlers.AbstractExternalSystemHandler.GetExternalSystem(externalSystem, user, cust, password, isPreAuthenticated);
                    if (ex.IsSymphony)
                    {
                        symphonyExternalSystem = externalSystem;
                    }
                    handlersList.Add(ex);
                }
            }

            if (!string.IsNullOrEmpty(user.JanrainUserUuid) && symphonyExternalSystem != null)
            {
                List<User> listOfusersSameJanrainId = new Select(
                        Data.User.IdColumn,
                        Data.User.FirstNameColumn,
                        Data.User.LastNameColumn,
                        Data.User.UsernameColumn,
                        Data.User.JanrainUserUuidColumn
                    )
                    .From<Data.User>()
                    .InnerJoin(Data.Customer.IdColumn, Data.User.CustomerIDColumn)
                    .IncludeColumn(Data.Customer.IdColumn.QualifiedName, "customerID")
                    .IncludeColumn(Data.Customer.NameColumn.QualifiedName, "customerName")
                    .IncludeColumn(Data.Customer.SubDomainColumn.QualifiedName, "customerSubdomain")
                    .IncludeColumn(Data.Customer.IsExternalSystemLoginEnabledColumn.QualifiedName, "customerExternalLoginEnabled")
                    .Where(Data.Customer.IdColumn).IsNotEqualTo(cust.Id)
                    .And(Data.User.JanrainUserUuidColumn).IsEqualTo(user.JanrainUserUuid)
                    .And(Data.User.IdColumn).IsNotEqualTo(user.ID)
                    .And(Data.Customer.IsExternalSystemLoginEnabledColumn).IsEqualTo(true)
                    .ExecuteTypedList<User>();

                ExternalSystem symphonyExternalSystemConfig = symphonyExternalSystem;

                if (symphonyExternalSystemConfig != null && symphonyExternalSystemConfig.Id > 0)
                {
                    foreach (User u in listOfusersSameJanrainId)
                    {
                        ExternalLoginHandlers.Symphony symphonyExternalSystemHandler = new ExternalLoginHandlers.Symphony(symphonyExternalSystemConfig, user, new Data.Customer(u.CustomerID), password, isPreAuthenticated, u);
                        handlersList.Add(symphonyExternalSystemHandler);
                    }
                }
            }

            return handlersList;
        }

        public void UpdateLoginSystems(Data.Customer cust, User user, string password, bool isPreAuthenticated)
        {
            RunLoginTasks(cust, user, false, password, isPreAuthenticated);
        }

        public void RefreshSymphonyLoginSystems(Data.Customer cust, User user, string password, bool isPreAuthenticated)
        {
            RunLoginTasks(cust, user, true, password, isPreAuthenticated);
        }

        private void RunLoginTasks(Data.Customer cust, User user, bool isSymphonyRefreshOnly, string password, bool isPreAuthenticated)
        {
            if (user == null)
            {
                return;
            }
            if (!IsValidEmailAddress(user.Username)) return; // Only want to process external systems when the user has a valid email address as the username.

            try
            {
                if (!cust.IsExternalSystemLoginEnabled) return;

                loginTasks = new ExternalLoginHandlers.ExternalSystemHandlerTaskRunner();
                List<ExternalLoginHandlers.AbstractExternalSystemHandler> loginHandlers = GetExternalSystemHandlers(cust, user, password, isPreAuthenticated);

                if (isSymphonyRefreshOnly)
                {
                    loginHandlers = loginHandlers.Where(l => l.IsSymphony).ToList();
                }

                if (loginHandlers.Count > 0)
                {
                    foreach (ExternalLoginHandlers.AbstractExternalSystemHandler handler in loginHandlers)
                    {
                        loginTasks.AddRegisteredTask(handler);
                    }

                    loginTasks.RunTasks();
                    if (!isSymphonyRefreshOnly) // Because it will only have symphony systems in this case. Next time you hit multi login you will automatically be redirected to symphony
                    {
                        HttpContext.Current.Session["UserLoginSystem"] = user.LoginSystems;
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public bool CheckForSymphonyRedirect(User user)
        {
           
                if (user.LoginSystems != null && user.LoginSystems.Count > 0)
                {
                    List<LoginSystem> displayedSystems = user.LoginSystems.Where(s => s.IsDisplayed).ToList();
                    if (displayedSystems.Count == 0 || displayedSystems.Sum(d => d.TrainingPrograms != null ? d.TrainingPrograms.Count : 0) == 0)
                    {
                        return true;
                    }

                    return false;
                }
                // Default to redirect to symphony if there are no external systems
                return true;
            
        }

        private static string GetActualUserNameAsEmail(User User)
        {
            if (IsValidEmailAddress(User.Username)) return User.Username;
            if (IsValidEmailAddress(User.Email)) return User.Email;

            //TODO: DS: Additional logic to search email address?

            return "";
        }

        private static bool IsValidEmailAddress(string EmailAddress)
        {
            string patternRegexEmail = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            // from microsoft: https://msdn.microsoft.com/en-us/library/01escwtf(v=vs.110).aspx
            return System.Text.RegularExpressions.Regex.IsMatch(EmailAddress, patternRegexEmail);
        }
        

        public void SetNewPassword(string emailAddress, string password)
        {
            if (!IsValidEmailAddress(emailAddress)) return;
            if (password == null) return;
            if (string.IsNullOrEmpty(password.Trim())) return;

            try
            {
                /*
                 * Please look for comm
                if (prosch.LoginController.IsValidUser(emailAddress) != null)
                    prosch.LoginController.SetNewPassword(emailAddress, password);
                 * */
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex);
                //throw new Exception("Did not update password in external system!");
            }
        }

    }

    public class SystemLoginResult
    {
        private string _SystemName;
        public string SystemName
        {
            get { return _SystemName; }
            set { _SystemName = value; }
        }

        private string _ResultItemName;
        public string ResultItemName
        {
            get { return _ResultItemName; }
            set { _ResultItemName = value; }
        }

        private string _ResultItemValue;
        public string ResultItemValue
        {
            get { return _ResultItemValue; }
            set { _ResultItemValue = value; }
        }
    }
}
