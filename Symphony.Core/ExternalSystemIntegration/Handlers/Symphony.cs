﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using Symphony.Core.Controllers;
using Symphony.ExternalSystemIntegration.Models;
using Data = Symphony.Core.Data;
using Symphony.Core;
using SubSonic;
using Symphony.ExternalSystemIntegration.Controllers;
using System.Web;

namespace Symphony.ExternalSystemIntegration.Handlers
{
    public class Symphony : AbstractExternalSystemHandler
    {
        private User _userOverride;

        public Symphony(ExternalSystem externalSystemConfig, User user, Data.Customer customer, string password, bool isPreAuthenticated)
            : base(externalSystemConfig, user, customer, password, isPreAuthenticated)
        {
        }

        public Symphony(ExternalSystem externalSystemConfig, User user, Data.Customer customer, string password, bool isPreAuthenticated, User userOverride = null)
            : base(externalSystemConfig, user, customer, password, isPreAuthenticated)
        {
            if (userOverride != null && userOverride.ID > 0)
            {
                _userOverride = userOverride;
            }
        }

        public override bool IsSymphony
        {
            get { return true; }
        }

        public override bool IsDisplayed
        {
            get
            {
                if (LoginResult != null && LoginResult.TrainingPrograms != null && LoginResult.TrainingPrograms.Count > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public override string ConnectionString
        {
            get { return ""; }
        }

        public override string GetExternalUserPasswordParallel()
        {
            // Don't need to worry about the password for symphony, just return whatever
            // they typed in originally
            return _password;
        }

        public override void LoginParallel()
        {
            // If the user has been authenticated by symphony, then we will have a user id > 0
            // Otherwise we need to look up a matching user based on the user name provided
            // in the current customer. If that user exists, we will use that user as the 
            // user we are attempting to log in as. 
            // The user does not match credentials of a symphony user so we will need to either create
            // a new symphony user, or look up a symphony user with a username that matches the email
            // address provided.
 
            //
            // NOTE! Even if we find a user here, this may be temporary. If they do not exist as a 
            // valid user in an external system this login result MUST be disregarded 
            //
            if (User.ID <= 0)
            {
                User existingUser = Select.AllColumnsFrom<Data.User>()
                    .InnerJoin(Data.Customer.IdColumn, Data.User.CustomerIDColumn)
                    .Where(Data.User.UsernameColumn).IsEqualTo(User.Username)
                    .And(Data.Customer.IdColumn).IsEqualTo(Customer.Id)
                    .And(Data.User.StatusIDColumn).IsEqualTo(UserStatusType.Active)
                    .ExecuteSingle<User>();

                if (existingUser != null && existingUser.ID > 0)
                {
                    // Found user with matching username for the current customer. User this user as the symphony login
                    LoginResult.User = new ExternalUser()
                    {
                        Id = existingUser.ID,
                        UserName = existingUser.Username,
                        FirstName = existingUser.FirstName,
                        LastName = existingUser.LastName,
                        SystemCodeName = ExternalSystemConfig.SystemCodeName,
                        RequiresValidation = true
                    };
                }
            }
            else
            {
                // User is already logged in as a valid smphony user
                LoginResult.User = new ExternalUser()
                {
                    Id = User.ID,
                    UserName = User.Username,
                    FirstName = User.FirstName,
                    LastName = User.LastName,
                    SystemCodeName = ExternalSystemConfig.SystemCodeName
                };
            }

            // Update the user details for the result if there is an override
            // An override will allow listing matching users in different 
            // symphony customers as login options. 
            if (_userOverride != null && _userOverride.ID > 0 && LoginResult.User != null)
            {
                LoginResult.User.Id = _userOverride.ID;
                LoginResult.User.UserName = _userOverride.Username;
                LoginResult.User.FirstName = _userOverride.FirstName;
                LoginResult.User.LastName = _userOverride.LastName;
                LoginResult.User.Subdomain = _userOverride.CustomerSubDomain;
            }
        }

        public override void LoadTrainingProgramsParallel()
        {
            List<TrainingProgram> trainingPrograms = new List<TrainingProgram>();
            
            if (LoginResult.User != null)
            {
                bool isNewHire = _userOverride != null && _userOverride.ID > 0 ? _userOverride.NewHireIndicator : User.NewHireIndicator;
                trainingPrograms = Data.SPs.GetTrainingProgramsForUser(LoginResult.User.Id, isNewHire, "", "IsActive", "desc", 0, int.MaxValue, new DateTime(1900, 1, 1), new DateTime(9999, 12, 31))
                        .ExecuteTypedList<TrainingProgram>();
            }

            LoginResult.TrainingPrograms = trainingPrograms;
        }

        public override void LoadFormParallel()
        {
            LoginResult.FormType = (int)ExternalSystemFormType.Eval;

            string userName = HttpUtility.UrlEncode(User.Username);
            string subDomain = HttpUtility.UrlEncode(Customer.SubDomain);

            LoginResult.Form = string.Format("window.location.href = '/Multilogin.aspx?n={0}&d={1}{2}'", userName, subDomain, Skins.BuildQuerySkin("&"));
        }
        /// <summary>
        /// Either creates a valid user to log in as or validates a user with 
        /// matching username in the current customer based on being
        /// authenticated in an external system
        /// </summary>
        /// <param name="taskRunner"></param>
        public override void CompleteLogin(ExternalSystemHandlerTaskRunner taskRunner)
        {
            List<ExternalSystem> externalSystemsForUser = taskRunner.ExternalSystemsForUser;
            List<ExternalUser> externalUsers = taskRunner.ExternalUsers;

            bool userAuthenticatedByExternalSystem = externalSystemsForUser.Count > 0;

            if (LoginResult.User == null && User.ID <= 0 && userAuthenticatedByExternalSystem)
            {
                // Create a symphony user to log in as since this user exists in an external system
                if (string.IsNullOrEmpty(User.EmployeeNumber))
                {
                    User.EmployeeNumber = Guid.NewGuid().ToString();
                }

                User.Notes += "\r\n- User has access to external systems - ";
                foreach (ExternalSystem system in externalSystemsForUser)
                {
                    User.Notes += string.Format("\r\nID: {0}, Name: {1}", system.Id, system.SystemName);
                }

                ExternalUser firstExternalUser = externalUsers.FirstOrDefault();
                if (firstExternalUser != null && firstExternalUser.Id > 0)
                {
                    User.FirstName = firstExternalUser.FirstName;
                    User.LastName = firstExternalUser.LastName;
                    User.MiddleName = "";
                    User.Email = firstExternalUser.UserName;
                    User.StatusID = (int)UserStatusType.Active;
                }

                User.Password = Guid.NewGuid().ToString();

                User newUser = new CustomerController().SaveUser(Customer.Id, 0, 0, User, true).Data;

                User.ID = newUser.ID;

                LoginResult.User = new ExternalUser()
                {
                    Id = User.ID,
                    UserName = User.Username,
                    FirstName = User.FirstName,
                    LastName = User.LastName,
                    SystemCodeName = ExternalSystemConfig.SystemCodeName
                };
            }
            else if (LoginResult.User != null && LoginResult.User.RequiresValidation) // We have found a symphony user, but they may not be authenticated by symphony.
            {
                if (!userAuthenticatedByExternalSystem)
                {
                    // We have a user in symphony that matches the email address but the password was not valid in any external system.
                    // Do not allow login to symphony
                    LoginResult.User = null;
                }
                else
                {
                    // If we have no user set, but user was authenticated by an external system, then update the login target user with the rest of the user details
                    if (User.ID <= 0 && LoginResult.User != null && LoginResult.User.Id > 0 && userAuthenticatedByExternalSystem)
                    {
                        Data.User dataUser = new Data.User(LoginResult.User.Id);
                        if (dataUser != null && dataUser.Id > 0)
                        {
                            User.CopyFrom(dataUser);
                        }
                    }
                }
            }

            base.CompleteLogin(taskRunner);
        }
    }
}
