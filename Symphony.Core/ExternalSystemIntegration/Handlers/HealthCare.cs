﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Symphony.ExternalSystemIntegration.Models;
using System.Data.Common;
using System.Data.SqlClient;
using Symphony.Core.Models;
using Data = Symphony.Core.Data;
using Symphony.Core;

namespace Symphony.ExternalSystemIntegration.Handlers
{
    public class HealthCare : AbstractExternalSystemHandler
    {
        ExternalSystem ExternalSystemConfig = null;

        public HealthCare(ExternalSystem externalSystemConfig, User user, Data.Customer customer, string password, bool isPreAuthenticated)
            : base(externalSystemConfig, user, customer, password, isPreAuthenticated)
        {
            this.ExternalSystemConfig = externalSystemConfig;
        }

        public override bool IsSymphony
        {
            get { return false; }
        }

        public override string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Symphony.HealthCare"]))
                {
                    throw new Exception("No health care db connection string specified.");
                }
                return ConfigurationManager.AppSettings["Symphony.HealthCare"];
            }
        }

        private void ApplyCustomerParams(SqlCommand cmd, string customerIdInExternalSystem, bool checkCustomerIdInExternalSystem, string tableName = null)
        {
            if (checkCustomerIdInExternalSystem)
            {
                string tablePrepend = "";

                if (!string.IsNullOrEmpty(tableName))
                {
                    tablePrepend = tableName + ".";
                }

                if (customerIdInExternalSystem == null)
                {
                    cmd.CommandText += " AND " + tablePrepend + "facilityid is null";
                }
                else
                {
                    cmd.CommandText += " AND " + tablePrepend + "facilityid = @facility";
                    cmd.Parameters.AddWithValue("@facility", customerIdInExternalSystem);
                }
            }
        }

        public override string GetExternalUserPasswordParallel()
        {
            string userPassword = "";
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand("select cpw from UserAccount where cemail = @username"))
                {
                    ApplyCustomerParams(cmd, ExternalSystemConfig.CustomerIdInExternalSystem, ExternalSystemConfig.IsCheckCustomerIdInExternalSystem);
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@username", User.Username);

                    var result = cmd.ExecuteScalar();
                    if (result != null)
                    {
                        userPassword = result.ToString();
                    }
                }

                conn.Close();
            }
            return userPassword;
        }

        public override void LoadFormParallel()
        {
            var healthCareDomain = ConfigurationManager.AppSettings["Symphony.HealthCare.Domain"];
            var healthCareLandingPage = ConfigurationManager.AppSettings["Symphony.HealthCare.LandingPage"];


            LoginResult.FormID = "loginForm";
            LoginResult.FormType = (int)ExternalSystemFormType.Submit;

            string form = string.Format(@"
                <form id=""{0}"" method=""post"" action=""{1}"">
                    <input id=""UserName"" maxlength=""100"" name=""UserName"" type=""text"" value=""{2}"">
                    <input id=""Password"" maxlength=""50"" name=""signInPassword"" type=""password"" value=""{3}"">
                    <input id=""source"" name=""source"" type=""hidden""/>
                    <input id=""ReturnUrl"" name=""ReturnUrl"" type=""hidden"" value=""{4}""/>
                </form>",
                LoginResult.FormID, healthCareDomain, User.Username, this._password, healthCareLandingPage
            );

            LoginResult.Form = form;
        }

        public override void LoadTrainingProgramsParallel()
        {
            List<TrainingProgram> trainingPrograms = new List<TrainingProgram>();

            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                string sqlCommand =
                    @"select
                        t.topicid as id,
                        tp.course_number as sku,
                        t.name as name
                    from
                        Test t with (nolock)
                    join
                        UserAccount u with (nolock) on u.iid = t.userid
                    join
                        Topic tp on tp.topicid = t.topicid
                    where
                        u.cemail = @username
                    and
                        t.[status] = 'I'";

                using (var cmd = new SqlCommand(sqlCommand))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@username", User.Username);
                    ApplyCustomerParams(cmd, ExternalSystemConfig.CustomerIdInExternalSystem, ExternalSystemConfig.IsCheckCustomerIdInExternalSystem, "u");
                    SqlDataReader result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        int id = Convert.ToInt32(result["id"].ToString());
                        string sku = result["sku"].ToString();
                        string name = result["name"].ToString();

                        if (id > 0)
                        {
                            trainingPrograms.Add(new TrainingProgram()
                            {
                                Id = id,
                                Sku = sku,
                                Name = name
                            });
                        }
                    }
                }
            }
            LoginResult.TrainingPrograms = trainingPrograms;
        }

        public override void LoginParallel()
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand("select iid, cusername, cfirstname, clastname from UserAccount where cemail = @username"))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@username", User.Username);
                    ApplyCustomerParams(cmd, ExternalSystemConfig.CustomerIdInExternalSystem, ExternalSystemConfig.IsCheckCustomerIdInExternalSystem);

                    if (!_isPreAuthenticated)
                    {
                        cmd.CommandText += " and cpw = @password";
                        cmd.Parameters.AddWithValue("@password", this._password);
                    }

                    SqlDataReader result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        int userId = 0;
                        int.TryParse(result["iid"].ToString(), out userId);
                        if (userId > 0)
                        {
                            string firtname = result["cfirstname"].ToString();
                            string lastname = result["clastname"].ToString();

                            LoginResult.User = new ExternalUser()
                            {
                                Id = userId,
                                UserName = User.Username,
                                FirstName = firtname,
                                LastName = lastname,
                                SystemCodeName = this.GetType().ToString()
                            };
                        }
                    }
                }

                conn.Close();
            }
        }

    }
}
