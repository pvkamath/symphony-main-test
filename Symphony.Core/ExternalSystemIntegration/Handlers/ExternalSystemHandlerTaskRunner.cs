﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Symphony.ExternalSystemIntegration.Models;
using Symphony.Core.Models;
using System.Web;

namespace Symphony.ExternalSystemIntegration.Handlers
{
    public class ExternalSystemHandlerTaskRunner
    {
        private List<Task<ExternalLoginResult>> externalLoginTasks = new List<Task<ExternalLoginResult>>();
        private List<ExternalSystem> externalSystems = new List<ExternalSystem>();

        public void AddRegisteredTask(AbstractExternalSystemHandler handler) {
            
            externalSystems.Add(handler.ExternalSystemConfig);

            externalLoginTasks.Add(Task.Factory.StartNew<ExternalLoginResult>((ctx) => {
                HttpContext.Current = (HttpContext)ctx;

                return handler.RunHandlerParallel();
            }, HttpContext.Current));

        }

        public int TaskCount
        {
            get
            {
                return externalLoginTasks.Count();
            }
        }

        public void RunTasks()
        {
            if (externalLoginTasks.Count > 0)
            {
                Task.WaitAll(externalLoginTasks.ToArray());
                foreach (var task in externalLoginTasks)
                {
                    task.Result.System.CompleteLogin(this);
                }
            }
        }

        public ExternalSystem SymphonyExternalSystem
        {
            get
            {
                if (externalLoginTasks.Count > 0)
                {
                    return externalSystems.Where(e => e.IsSymphony).FirstOrDefault();
                }
                return null;
            }
        }

        public List<ExternalSystem> ExternalSystemsForUser
        {
            get
            {
                if (externalLoginTasks.Count > 0)
                {
                    return externalLoginTasks
                        .Where(t => t.IsCompleted && t.Result.System != null && !t.Result.System.IsSymphony && t.Result.User != null && t.Result.User.Id > 0)
                        .Select(t => t.Result.System.ExternalSystemConfig)
                        .ToList();

                }
                return new List<ExternalSystem>();
            }
        }

        public List<ExternalUser> ExternalUsers
        {
            get
            {
                if (externalLoginTasks.Count > 0)
                {
                    return externalLoginTasks
                        .Where(t => t.IsCompleted && t.Result.User != null && t.Result.System != null && !t.Result.System.IsSymphony && t.Result.User.Id > 0)
                        .Select(t => t.Result.User)
                        .ToList();

                }
                return new List<ExternalUser>();
            }
        }

        public List<ExternalLoginResult> ExternalLoginResults
        {
            get
            {
                if (externalLoginTasks.Count > 0)
                {
                    return externalLoginTasks
                        .Where(t => t.IsCompleted && t.Result.User != null && t.Result.System != null && t.Result.User.Id > 0)
                        .Select(t => t.Result)
                        .ToList();
                }
                return new List<ExternalLoginResult>();
            }
        }

    }
}
