﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using Data = Symphony.Core.Data;
using MySql.Data.MySqlClient;
using Symphony.ExternalSystemIntegration.HttpController;
using System.Configuration;
using Symphony.Core;

namespace Symphony.ExternalSystemIntegration.Handlers
{
    public class ProSchools : AbstractExternalSystemHandler
    {
        public ProSchools(ExternalSystem externalSystemConfig, User user, Data.Customer customer, string password, bool isPreAuthenticated)
            : base(externalSystemConfig, user, customer, password, isPreAuthenticated)
        {
        }

        public override string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Symphony.ProSchools"]))
                {
                    throw new Exception("No proschools db connection string specified.");
                }
                return ConfigurationManager.AppSettings["Symphony.ProSchools"];
            }
        }

        public override string GetExternalUserPasswordParallel()
        {
            string userPassword = "";
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                using (var cmd = new MySqlCommand("select STUDENT_PASSWORD from students where STUDENT_USERNAME = @username"))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@username", User.Username);

                    var result = cmd.ExecuteScalar();
                    if (result != null)
                    {
                        userPassword = result.ToString();
                    }
                }
            }
            return userPassword;
        }

        public override void LoginParallel()
        {
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand("SELECT STUDENT_ID, STUDENT_FIRSTNAME, STUDENT_LASTNAME FROM students WHERE STUDENT_USERNAME=@username"))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@username", User.Username);

                    if (!_isPreAuthenticated)
                    {
                        cmd.CommandText += "  AND STUDENT_PASSWORD=@password";
                        cmd.Parameters.AddWithValue("@password", this._password);
                    }


                    MySqlDataReader result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        int userId = 0;
                        int.TryParse(result["STUDENT_ID"].ToString(), out userId);
                        if (userId > 0)
                        {
                            string firtname = result["STUDENT_FIRSTNAME"].ToString();
                            string lastname = result["STUDENT_LASTNAME"].ToString();

                            LoginResult.User = new Models.ExternalUser()
                            {
                                Id = userId,
                                UserName = User.Username,
                                FirstName = firtname,
                                LastName = lastname
                            };
                        }
                    }
                }

                conn.Close();
            }
        }

        public override void LoadFormParallel()
        {
            var proschoolsDomain = ConfigurationManager.AppSettings["Symphony.Proschools.Domain"];
            HttpCommunicator communicator = new HttpCommunicator();
            communicator.CommunicateToSystem("proschools", null);
            List<SystemLoginResult> systemCommunicationRestuls = new List<SystemLoginResult>();
            
            foreach (string onekey in communicator.listOfNameValueResults.Keys)
            {
                systemCommunicationRestuls.Add(new SystemLoginResult { SystemName = "proschools", ResultItemName = onekey, ResultItemValue = communicator.listOfNameValueResults[onekey] });
            }

            LoginResult.FormID = "signInForm";
            LoginResult.FrameID = "frmProSchools";
            LoginResult.FormType = (int)ExternalSystemFormType.IFrame;
            LoginResult.LandingPage = ConfigurationManager.AppSettings["Symphony.Proschools.LandingPage"];

            StringBuilder sb = new StringBuilder()
                .Append(@"<iframe id='" + LoginResult.FrameID + "' width='0px' height='0px'></iframe>")
                .Append(@"<form id='" + LoginResult.FormID + "' class='clsPSLM' method='post' action='https://")
                .Append(proschoolsDomain)
                .Append(@"/cas/login;jsessionid=" + systemCommunicationRestuls.Where(t => t.ResultItemName == "JSESSION").First().ResultItemValue + @"?service=https%3A%2F%2Fstore.proschools.com%2Fep5sf%2Fj_spring_security_check.ep&parentUrl=https://www.proschools.com/customer/account/login/'>")
                .Append(@"<input type=""hidden"" name=""lt"" value='" + systemCommunicationRestuls.Where(t => t.ResultItemName == "LT_VALUE").First().ResultItemValue + @"' />")
                .Append(@"<input type='hidden' name='_eventId' value='submit' />")
                .Append(@"<input id='email' type='text' name='username' value='" + User.Username + @"'  />")
                .Append(@"<input id='password' type='password' name='password' value='" + this._password + @"'  />")
                .Append(@"</form>");

            LoginResult.Form = sb.ToString();
        }

        public override void LoadTrainingProgramsParallel()
        {
            List<TrainingProgram> results = new List<TrainingProgram>();
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                using (var cmd = new MySqlCommand(
@"select rc.course_id, c.course_code, c.course_title from regcourses rc left 
    join courses c on c.course_id = rc.course_id 
    join students s on s.student_id = rc.student_id          
where student_email = @useremail and c.course_code is not null"
))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@useremail", User.Username);
                    cmd.CommandTimeout = 1000;

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var id = reader.GetInt32(0);
                            var code = reader.GetString(1);
                            var title = reader.GetString(2);
                            results.Add(new TrainingProgram()
                            {
                                Sku = code,
                                Name = title,
                                Id = id
                            });
                        }
                    }
                }
            }
            LoginResult.TrainingPrograms = results;
        }
    }
}
