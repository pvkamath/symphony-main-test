﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using Data = Symphony.Core.Data;
using Symphony.ExternalSystemIntegration.Controllers;
using Symphony.ExternalSystemIntegration.Models;
using SubSonic;
using log4net;

namespace Symphony.ExternalSystemIntegration.Handlers
{
    public abstract class AbstractExternalSystemHandler
    {
        private ExternalSystem _externalSystemConfig;
        private User _user;
        private Data.Customer _customer;
        private Models.ExternalLoginResult _loginResult;
        protected string _password;
        protected bool _isPreAuthenticated;
        protected ILog Log = LogManager.GetLogger(typeof(AbstractExternalSystemHandler));
        
        public ExternalSystem ExternalSystemConfig
        {
            get
            {
                return _externalSystemConfig;
            }
        }

        public User User
        {
            get { return _user; }
        }

        public Data.Customer Customer
        {
            get { return _customer; }
        } 

        public Models.ExternalLoginResult LoginResult
        {
            get
            {
                return _loginResult;
            }
        }
        
        public AbstractExternalSystemHandler(ExternalSystem externalSystemConfig, User user, Data.Customer customer, string password, bool isPreAuthenticated)
        {
            this._externalSystemConfig = externalSystemConfig;
            this._externalSystemConfig.IsSymphony = this.IsSymphony;
            this._password = password;
            this._isPreAuthenticated = isPreAuthenticated;

            this._user = user;
            this._customer = customer;

            this._loginResult = new Models.ExternalLoginResult()
            {
                System = this
            };


        }

        public Models.ExternalLoginResult RunHandlerParallel()
        {
            try
            {
                if (this._isPreAuthenticated)
                {
                    this._password = GetExternalUserPasswordParallel();
                }

                LoginParallel();

                if (this.LoginResult.IsLoggedIn) LoadFormParallel();
                if (this.LoginResult.IsLoggedIn) LoadTrainingProgramsParallel();
            }
            catch (Exception e)
            {
                Log.Error("Error With External System: " + this.ExternalSystemConfig.SystemCodeName + " Message: " + e);
            }

            return this.LoginResult;
        }

        public virtual bool IsSymphony { 
            get {
                return false;
            }
        }

        public virtual bool IsDisplayed
        {
            get
            {
                return true;
            }
        }

        public abstract string ConnectionString { get; }

        /// <summary>
        /// Threaded
        /// </summary>
        /// <returns></returns>
        public abstract string GetExternalUserPasswordParallel();
        public abstract void LoginParallel();
        public abstract void LoadFormParallel();
        public abstract void LoadTrainingProgramsParallel();

        public virtual void CompleteLogin(ExternalSystemHandlerTaskRunner taskRunner)
        {
            if (LoginResult != null && LoginResult.User != null && LoginResult.User.Id > 0)
            {
                LoginSystem userLoginSystem = new LoginSystem()
                {
                    Id = ExternalSystemConfig.Id,
                    SystemName = ExternalSystemConfig.SystemName,
                    CustomerName = Customer.Name,
                    CustomerSubDomain = string.IsNullOrEmpty(LoginResult.User.Subdomain) ? Customer.SubDomain : LoginResult.User.Subdomain,
                    UserFullName = string.Format("{0} {1}", LoginResult.User.FirstName, LoginResult.User.LastName),
                    UserFirstName = LoginResult.User.FirstName,
                    UserLastName = LoginResult.User.LastName,
                    SystemCodeName = ExternalSystemConfig.SystemCodeName,
                    UserEmail = LoginResult.User.UserName,
                    IsSymphony = IsSymphony,
                    JanrainId = User.JanrainUserUuid,
                    UserId = LoginResult.User.Id,
                    TrainingPrograms = LoginResult.TrainingPrograms,
                    LoginForm = LoginResult.Form,
                    FormType = LoginResult.FormType,
                    FormID = LoginResult.FormID,
                    FrameID = LoginResult.FrameID,
                    IsDisplayed = IsDisplayed,
                    LandingPage = LoginResult.LandingPage
                };

                List<ExternalLoginResult> loginResults = taskRunner.ExternalLoginResults;
                if (loginResults.Count > 0)
                {
                    int displayedLogins = loginResults.Where(r => r.System.IsDisplayed).Count();
                    if (displayedLogins == 1 && this.IsDisplayed)
                    {
                        userLoginSystem.IsAutoNavigate = true;
                    }
                }

                if (User.LoginSystems == null)
                {
                    User.LoginSystems = new List<LoginSystem>();
                }

                if (User.LoginSystems.Count(u => u.IsSymphony == true && u.CustomerSubDomain == Customer.SubDomain && u.CustomerName == Customer.Name && u.UserEmail == User.Username) == 0)
                {
                    User.LoginSystems.Add(userLoginSystem);
                }
                else
                {
                    List<LoginSystem> existingSystems = User.LoginSystems.Where(u => u.SystemCodeName == userLoginSystem.SystemCodeName && u.UserId == userLoginSystem.UserId && u.CustomerSubDomain == userLoginSystem.CustomerSubDomain).ToList();
                    if (existingSystems.Count > 0)
                    {
                        LoginSystem existing = existingSystems.FirstOrDefault();
                        if (existing != null)
                        {
                            // Just update training programs and display in this case. 
                            existing.TrainingPrograms = userLoginSystem.TrainingPrograms;
                            existing.IsDisplayed = userLoginSystem.IsDisplayed;

                            // If there was a system that was set for automatic navigation
                            // other than the current system, and now the current system has
                            // course work, turn off auto navigation
                            bool isAutoNavigate = User.LoginSystems.Where(u => u.IsAutoNavigate && u.SystemCodeName != userLoginSystem.SystemCodeName).Count() > 0;
                            if (isAutoNavigate && existing.TrainingPrograms.Count > 0)
                            {
                                User.LoginSystems.ForEach(u => u.IsAutoNavigate = false);
                            }

                        }
                    } 
                    else if (!userLoginSystem.IsSymphony)
                    {
                        User.LoginSystems.Add(userLoginSystem);
                    }
                }
            }
        }

        public static AbstractExternalSystemHandler GetExternalSystem(ExternalSystem externalSystemConfig, User user, Data.Customer customer, string password, bool isPreAuthenticated)
        {
            if (externalSystemConfig.ExternalSystemType == null)
            {
                throw new Exception(string.Format("Configuration Error: The type {0} could not be found. Please ensure a valid ExternalSystemTypeName is set for the external system with id {1}",
                    externalSystemConfig.ExternalSystemTypeName, externalSystemConfig.Id));
            }

            AbstractExternalSystemHandler extSys = (AbstractExternalSystemHandler)Activator.CreateInstance(externalSystemConfig.ExternalSystemType, externalSystemConfig, user, customer, password, isPreAuthenticated);
            return extSys;      
        }
    }
}
