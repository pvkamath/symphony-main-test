﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using Data = Symphony.Core.Data;
using System.Configuration;
using System.Data.SqlClient;
using Symphony.Core;

namespace Symphony.ExternalSystemIntegration.Handlers
{
    public class TrainingPro : AbstractExternalSystemHandler
    {
        public TrainingPro(ExternalSystem externalSystemConfig, User user, Data.Customer customer, string password, bool isPreAuthenticated)
            : base(externalSystemConfig, user, customer, password, isPreAuthenticated)
        {
        }

        public override string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Symphony.TrainingPro"]))
                {
                    throw new Exception("No TrainingPro db connection string specified.");
                }
                return ConfigurationManager.AppSettings["Symphony.TrainingPro"];
            }
        }

        private void ApplyCustomerParams(SqlCommand cmd, string customerIdInExternalSystem, bool checkCustomerIdInExternalSystem)
        {
            if (checkCustomerIdInExternalSystem)
            {
                if (customerIdInExternalSystem == null)
                {
                    cmd.CommandText += " AND CompanyID is null";
                }
                else
                {
                    cmd.CommandText += " AND CompanyID = @company";
                    cmd.Parameters.AddWithValue("@company", customerIdInExternalSystem);
                }
            }
        }

        public override string GetExternalUserPasswordParallel()
        {
            string userPassword = "";
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand("select Password from dbo.[Users] where UserName = @username"))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@username", User.Username);

                    var result = cmd.ExecuteScalar();
                    if (result != null)
                    {
                        userPassword = result.ToString();
                    }
                }
            }
            return userPassword;
        }

        public override void LoginParallel()
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand("SELECT UserID, FirstName, LastName FROM dbo.[Users] WHERE UserName = @username AND UserStatus = 1"))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@username", User.Username);

                    if (!_isPreAuthenticated)
                    {
                        cmd.CommandText += " AND Password = @password";
                        cmd.Parameters.AddWithValue("@password", this._password);
                    }


                    ApplyCustomerParams(cmd, ExternalSystemConfig.CustomerIdInExternalSystem, ExternalSystemConfig.IsCheckCustomerIdInExternalSystem);

                    SqlDataReader result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        int userId = 0;
                        int.TryParse(result["UserID"].ToString(), out userId);
                        if (userId > 0)
                        {
                            string firtname = result["FirstName"].ToString();
                            string lastname = result["LastName"].ToString();
                            LoginResult.User = new Models.ExternalUser()
                            {
                                Id = userId,
                                UserName = User.Username,
                                FirstName = firtname,
                                LastName = lastname
                            };
                        }
                    }
                }

                conn.Close();
            }
        }

        public override void LoadFormParallel()
        {
            var trainingproDomain = ConfigurationManager.AppSettings["Symphony.TrainingPro.Domain"];

            LoginResult.FormID = "ltp2";
            LoginResult.FormType = (int)ExternalSystemFormType.Submit;
            
            StringBuilder sb = new StringBuilder()
            .Append(string.Format("<form id=\"{0}\" name=\"LoginForm\" method=\"POST\" action=\"{1}\" >", LoginResult.FormID, trainingproDomain))
            .Append("<input type=\"hidden\" name=\"referrer\" value=\"\" />")
            .Append(string.Format("<input type=\"text\" name=\"User_Login\" value=\"{0}\" />", User.Username))
            .Append(string.Format("<input type=\"password\" name=\"User_Password\" value=\"{0}\" />", this._password))
            .Append("</form>");

            LoginResult.Form = sb.ToString();
        }

        public override void LoadTrainingProgramsParallel()
        {
            List<TrainingProgram> results = new List<TrainingProgram>();
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(
@"
SELECT 
    c.CourseID,
    CAST(c.CourseID AS VARCHAR(10)) AS [Code],
    c.Name
FROM
	dbo.Courses c WITH (NOLOCK)
	INNER JOIN dbo.UsersCourses cu WITH (NOLOCK) ON cu.CourseID = c.CourseID
	INNER JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = cu.UserID
WHERE 
    u.UserName = @useremail 
and
    cu.coursestatus not in (2,3) -- disabled = 2, deleted = 3
"
))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@useremail", User.Username);
                    cmd.CommandTimeout = 1000;

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var id = reader.GetInt32(0);
                            var code = reader.GetString(1);
                            var title = reader.GetString(2);
                            results.Add(new TrainingProgram()
                            {
                                Sku = code,
                                Name = title,
                                Id = id
                            });
                        }
                    }
                }
            }
            LoginResult.TrainingPrograms = results;
        }
    }
}
