﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Symphony.ExternalSystemIntegration.Models;
using System.Data.Common;
using System.Data.SqlClient;
using Symphony.Core.Models;
using Data = Symphony.Core.Data;
using Symphony.Core;

namespace Symphony.ExternalSystemIntegration.Handlers
{
    public class CWS : AbstractExternalSystemHandler
    {
        public CWS(ExternalSystem externalSystemConfig, User user, Data.Customer customer, string password, bool isPreAuthenticated)
            : base(externalSystemConfig, user, customer, password, isPreAuthenticated)
        {
            
        }
        
        public override bool IsSymphony
        {
            get { return false; }
        }

        public override string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Symphony.CWS"]))
                {
                    throw new Exception("No CWS db connection string specified.");
                }
                return ConfigurationManager.AppSettings["Symphony.CWS"];
            }
        }

        public override string GetExternalUserPasswordParallel()
        {
            string userPassword = "";
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand("GetPassword"))
                {
                    cmd.Connection = conn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@username", User.Username);
                    cmd.Parameters.AddWithValue("@customerId", ExternalSystemConfig.CustomerIdInExternalSystem);

                    var result = cmd.ExecuteScalar();
                    if (result != null)
                    {
                        userPassword = result.ToString();
                    }
                }

                conn.Close();
            }
            return userPassword;
        }

        public override void LoadFormParallel()
        {
            // https://www.ordercourses.com/computaught/showcase/studentLogin.do;jsessionid=YzbkepMZTLLWgwMEMnVFA2zH
            var cwsDomain = ConfigurationManager.AppSettings["Symphony.CWS.Domain"];
            var cwsLandingPage = ConfigurationManager.AppSettings["Symphony.CWS.LandingPage"];


            LoginResult.FormID = "showcasePlayerForm";
            LoginResult.FormType = (int)ExternalSystemFormType.Submit;

            string form = string.Format(@"
                <form id=""{0}"" method=""post"" action=""{1}"">
                    <input id=""userName"" maxlength=""100"" name=""userName"" type=""text"" value=""{2}"">
                    <input id=""passWord"" maxlength=""50"" name=""passWord"" type=""password"" value=""{3}"">
                    <input id=""submitButton"" name=""submitButton"" type=""hidden"" value=""login""/>
                </form>",
                LoginResult.FormID, cwsDomain, User.Username, this._password
            );

            LoginResult.Form = form;
       } 

        public override void LoadTrainingProgramsParallel()
        {
            List<TrainingProgram> trainingPrograms = new List<TrainingProgram>();

            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand("GetActiveCourses"))
                {
                    cmd.Connection = conn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@username", User.Username);
                    cmd.Parameters.AddWithValue("@customerId", ExternalSystemConfig.CustomerIdInExternalSystem);

                    SqlDataReader result = cmd.ExecuteReader();
                    int i = 0;
                    while (result.Read())
                    {
                        //TODO: terry is adjusting to add sku/id
                        // for now we just add sku == name and id is a random number
                        i++;
                        trainingPrograms.Add(new TrainingProgram()
                        {
                            Id = i,
                            Sku = Convert.ToString(result[0]),
                            Name = Convert.ToString(result[1])
                        }); 
                    }
                }
            }
            LoginResult.TrainingPrograms = trainingPrograms;
        }

        public override void LoginParallel()
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand("GetUser"))
                {
                    cmd.Connection = conn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@username", User.Username);
                    cmd.Parameters.AddWithValue("@customerid", ExternalSystemConfig.CustomerIdInExternalSystem);

                    if (!_isPreAuthenticated)
                    {
                        cmd.Parameters.AddWithValue("@password", this._password);
                    }

                    SqlDataReader result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        int userId = 0;
                        int.TryParse(result["USERID"].ToString(), out userId);
                        if (userId > 0)
                        {
                            LoginResult.User = new ExternalUser()
                            {
                                Id = userId,
                                UserName = User.Username,
                                FirstName = Convert.ToString(result["FIRSTNAME"]).Trim(),
                                LastName = Convert.ToString(result["LASTNAME"]).Trim(),
                                SystemCodeName = this.GetType().ToString()
                            };
                        }
                    }
                }

                conn.Close();
            }
        }

    }
}
