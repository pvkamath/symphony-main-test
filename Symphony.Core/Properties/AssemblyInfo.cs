﻿/*
 * 
 *  
 *   888       888       d88888888888b. 888b    8888888888888b    888 .d8888b.  
 *   888   o   888      d88888888   Y88b8888b   888  888  8888b   888d88P  Y88b 
 *   888  d8b  888     d88P888888    88888888b  888  888  88888b  888888    888 
 *   888 d888b 888    d88P 888888   d88P888Y88b 888  888  888Y88b 888888        
 *   888d88888b888   d88P  8888888888P" 888 Y88b888  888  888 Y88b888888  88888 
 *   88888P Y88888  d88P   888888 T88b  888  Y88888  888  888  Y88888888    888 
 *   8888P   Y8888 d8888888888888  T88b 888   Y8888  888  888   Y8888Y88b  d88P 
 *   888P     Y888d88P     888888   T88b888    Y8888888888888    Y888 "Y8888P88  
 *  
 *              ###############################################
 *               WARNING - IN ORDER TO MAKE CHANGES TO THIS FILE 
 *               ENSURE YOU ARE **ONLY** EDITING AssemblyInfoTemplate.cs
 *               IT WILL !!**OVERRWRITE**!! AssemblyInfo.cs during
 *               build.
 *              ##############################################
 * 
 */
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Symphony.Data")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Symphony.Data")]
[assembly: AssemblyCopyright("Copyright ©  2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("b14cd992-e8cd-4398-9d7b-bc3adebcaca3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

