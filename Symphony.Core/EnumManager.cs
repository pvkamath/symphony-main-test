﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Globalization;

namespace Symphony.Core
{
    //TODO: remove enums from the database altogether and specify display names in enum attributes
    public class EnumManager
    {
        private static List<EnumRecord> EnumRecords = new List<EnumRecord>();

        public static string SerializePrefix { get; set; }

        public static void RegisterEnum<T>()
            where T : struct, IConvertible, IFormattable, IComparable
        {
            RegisterEnum<T>(true);
        }

        public static void RegisterEnum<T>(bool serializeToJson)
            where T : struct, IConvertible, IFormattable, IComparable
        {
            EnumRecords.Add(new EnumRecord(typeof(T), serializeToJson));
        }

        public static string SerializeToJson()
        {
            return string.Join(Environment.NewLine, EnumRecords
                .Where(x => x.SerializeToJson)
                .ToList()
                .ConvertAll<string>(SerializeToJson).ToArray());
        }

        public static string SerializeToJson(EnumRecord record)
        {
            var pairs = new List<string>();
            foreach (var item in record.Items)
            {
                pairs.Add(string.Format(@"""{0}"": {1}", item.CodeName, item.Value));
            }
            return SerializePrefix + record.Type.Name + " = { " + string.Join(", ", pairs.ToArray()) + " };";
        }
    }

    public class EnumRecord
    {
        public Type Type { get; set; }
        public bool SerializeToJson { get; set; }
        public List<EnumRecordItem> Items { get; set; }

        public EnumRecord(Type type, bool serializeToJson)
        {
            if (!type.IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type.");
            }
            Type = type;
            SerializeToJson = serializeToJson;
            Items = new List<EnumRecordItem>();
            foreach (var field in type.GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                var item = new EnumRecordItem();
                item.Value = (int)field.GetRawConstantValue();
                foreach (var attribute in field.GetCustomAttributes(typeof(EnumRecordAttribute), true))
                {
                    var enumRecordAttribute = (EnumRecordAttribute)attribute;
                    item.CodeName = enumRecordAttribute.CodeName;
                    item.DisplayName = enumRecordAttribute.DisplayName;
                }
                
                if (string.IsNullOrEmpty(item.CodeName))
                {
                    item.CodeName = field.Name.Substring(0, 1).ToLowerInvariant() + field.Name.Substring(1);
                }
                if (string.IsNullOrEmpty(item.DisplayName))
                {
                    item.DisplayName = String.Empty; //TODO: add default DisplayName here
                }

                Items.Add(item);
            }
        }

        public string GetCodeName<T>(T t) where T : IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type.");
            }

            EnumRecordItem item = Items.First(i => { return i.Value == t.ToInt32(CultureInfo.InvariantCulture); });
            return item.CodeName;
        }

        public string GetDisplayName<T>(T t) where T : IConvertible
        {
            if (!typeof(T).IsEnum) 
            {
                throw new ArgumentException("T must be an enumerated type.");
            }

            EnumRecordItem item = Items.First(i => { return i.Value == t.ToInt32(CultureInfo.InvariantCulture); });
            return item.DisplayName;
        }
    }

    public class EnumRecordItem
    {
        public string CodeName { get; set; }
        public string DisplayName { get; set; }
        public int Value { get; set; }
    }

    public class EnumRecordAttribute : Attribute
    {
        public string CodeName { get; set; }
        public string DisplayName { get; set; }

        public EnumRecordAttribute(string codeName)
        {
            CodeName = codeName;
            DisplayName = String.Empty;
        }
        public EnumRecordAttribute(string codeName, string displayName)
        {
            CodeName = codeName;
            DisplayName = displayName;
        }
    }
}
