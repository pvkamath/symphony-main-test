﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Symphony.Tests
{
    [TestClass]
    public class Salesforce
    {
        [TestMethod]
        public void PostEntitlementsTest()
        {
            
            var controller = new Symphony.Core.Controllers.Salesforce.EntitlementController();
            //controller.UserID = 147;
            Symphony.Core.Controllers.Salesforce.SalesforceController.OverrideTargetCustomerID(1);

            controller.PostEntitlements(9762, new Core.Models.Salesforce.EntitlementList()
            {
                Entitlements = new System.Collections.Generic.List<Core.Models.Salesforce.Entitlement>()
                {
                    new Symphony.Core.Models.Salesforce.Entitlement(){
                        ProductId = "jerod",
                        EntitlementPurchaseDate = "2016-03-09",
                        EntitlementDuration = 12,
                        EntitlementDurationUnits = "Months",
                        IsEnabled = true
                    }
                }
            });
        }
    }
}
