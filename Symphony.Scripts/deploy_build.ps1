#Requires -RunAsAdministrator

# update path
$env:Path += ";C:\Program Files\TortoiseSVN\bin"

$ErrorActionPreference = 'stop'

function Is-Numeric ($Value) {
    return $Value -match "^[\d\.]+$"
}

$Modes = @('Test','Production')
$Environemnts = @('BE','OCL','HealthCare','SoundCheck','Sandbox')

$Configs = @();
$i = 0
$j = 0
$count = 0
foreach($Mode in $Modes){
	$i++;
	foreach($Environment in $Environemnts){
		$j++;
		$count++;
		Echo "$count) $Mode - $Environment"
		$Configs += "$Mode - $Environment"
	}
}

$Selections = Read-Host -Prompt "Select a config (1- $count)"

foreach($Selection in $Selections.Split(',')){
	if(!(Is-Numeric $Selection)){
		Echo "Invalid selection"
		exit
	}

	$Config = $Configs[$Selection-1]
	$ConfigClean = $Config.Replace(" ","")
	"$Config selected! ($ConfigClean)"

	$now = get-date
	$fdate="" + $now.Year + "-" + $now.Month + "-" + $now.Day + "_" + $now.Hour + "-" + $now.Minute + "-" + $now.Second
	$deployments="C:\Symphony\Deployments\$ConfigClean"
	$root="$deployments\$fdate"
	# deliberately no trailing slash so we can append the symphony/rustici paths for the vdirs
	$web_path="$root\web"
	$console_path="$root\console\"
	$migrations_path="$root\migrations\"
	$scripts_path="$root\scripts\"


	# update the working copy
	echo "Updating SVN..."
	svn update ./Builds --username=server.deploy --password=server123!!
	if($LASTEXITCODE){
		Write-Host "Failed to update SVN"
		Exit
	}


	# copy the web content to the target folder
	echo "Copying website..."
	cmd /c xcopy .\Builds\$ConfigClean\web $web_path\ /S /Y
	if($LASTEXITCODE){
		Write-Host "Failed to copy site"
		Exit
	}


	# copy the scheduled task content to the target folder
	echo "Copying taskrunner..."
	cmd /c xcopy .\Builds\$ConfigClean\console $console_path /S /Y
	if($LASTEXITCODE){
		Write-Host "Failed to copy taskrunner"
		Exit
	}


	# move migrations
	echo "Copying migrations..."
	cmd /c xcopy .\Builds\$ConfigClean\migrations $migrations_path /S /Y
	if($LASTEXITCODE){
		Write-Host "Failed to copy migrations"
		Exit
	}


	# move scripts
	echo "Copying scripts..."
	cmd /c xcopy .\Builds\$ConfigClean\scripts $scripts_path /S /Y
	if($LASTEXITCODE){
		Write-Host "Failed to copy scripts"
		Exit
	}


	echo "Running migrations..."
	$invocation = (Get-Variable MyInvocation).Value
	$directorypath = Split-Path $invocation.MyCommand.Path
	cd $scripts_path
	cmd /c migrate_build.bat
	if($LASTEXITCODE){
		Write-Host "Failed to run migrations"
		Exit
	}
	cd $directorypath

	# switch IIS to use the new folder
	echo "Re-pointing IIS..."
	cmd /c "$env:windir\system32\inetsrv\appcmd" set vdir "$ConfigClean/" -physicalPath:$web_path\Symphony.Web
	if($LASTEXITCODE){
		Write-Host "Failed to point IIS (Symphony)"
		Exit
	}
	cmd /c "$env:windir\system32\inetsrv\appcmd" set vdir "$ConfigClean/scorm/" -physicalPath:$web_path\Rustici.Web
	if($LASTEXITCODE){
		Write-Host "Failed to point IIS (Rustici)"
		Exit
	}

	#TODO: update tasks
}