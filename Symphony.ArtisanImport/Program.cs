﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Text;
using Symphony.Core.Controllers;
using Symphony.Core.Models;
using Symphony.Core;
using BankersEdge.CourseGenerator;
using BankersEdge.Artisan;
using System.IO;
using HtmlAgilityPack;
using System.Net;

namespace Symphony.ArtisanImport
{
    class Program
    {
        static ContentAuthor2DataContext db = new ContentAuthor2DataContext();
        static ArtisanController controller = new ArtisanController();

        static void Main(string[] args)
        {
            try
            {
                foreach (int companyId in new int[] { 79, 103, 144 })
                {
                    Console.WriteLine("Please enter a company ID: ");
                    Console.WriteLine(companyId);
                    //string strCompanyId = Console.ReadLine();
                    //int companyId = Convert.ToInt32(strCompanyId);

                    // Get List of courses for this company
                    var courses =
                        from cp in db.CoursePackages
                        join c in db.Courses on cp.CourseID equals c.ID
                        where c.BusinessEntityID == companyId
                            && c.IsDeleted == false
                        select cp;

                    Console.WriteLine("\n{0} courses found for company ID {1}, would you like to import them? (Y/n/[course id])", courses.Count(), companyId);
                    //string sResponse = Console.ReadLine();
                    string sResponse = "Y";


                    int iCourseId = 0;
                    if (sResponse.ToUpperInvariant() != "Y")
                    {
                        if (!int.TryParse(sResponse, out iCourseId))
                        {
                            return;
                        }
                    }

                    Console.WriteLine("\n");

                    // Loop through courses and import each with related info
                    foreach (CoursePackage oldCoursePackage in courses)
                    {
                        try
                        {
                            if (iCourseId > 0 && oldCoursePackage.CourseID != iCourseId)
                            {
                                continue;
                            }
                            Console.WriteLine("Attempting import of course {0} (id: {1})...", oldCoursePackage.Course.CourseName, oldCoursePackage.Course.ID);

                            ArtisanCourse course = new ArtisanCourse();
                            //course.CategoryId = 
                            //course.CertificateEnabled = 
                            course.CompletionMethod = (ArtisanCourseCompletionMethod)oldCoursePackage.CompletionTypeID; // do these match up?
                            course.ContactEmail = oldCoursePackage.EmailAddresses;
                            course.Description = oldCoursePackage.Course.CourseDescription;
                            course.Disclaimer = oldCoursePackage.Disclaimer;
                            course.DisplayQuestionFeedback = oldCoursePackage.DisplayQAFeedback;
                            course.DisplayTestOnly = oldCoursePackage.DisplayTestOnly;
                            //course.Keywords = 
                            course.Name = oldCoursePackage.Course.CourseName;
                            course.NavigationMethod = (ArtisanCourseNavigationMethod)oldCoursePackage.NavigationTypeID; // do these match?
                            course.Objectives = oldCoursePackage.Course.Objectives;
                            course.PassingScore = Convert.ToInt32(oldCoursePackage.PassingScore);
                            course.Sections = new List<ArtisanSection>();
                            course.ThemeId = 4; // hard-coded for BE-smooth


                            // Package up course using old system so we can grab what we need from it
                            CourseGenerator cg = new CourseGenerator();
                            string url = cg.GenerateCourse(oldCoursePackage.CourseID, PackageTypeCode.SCORM12, false);//.Replace(Asset.GetBaseURL(false), "");

                            string scormPath = ConfigurationManager.AppSettings["ScormPath"];
                            //string newPath = Path.Combine(scormPath, url.Replace("http://service.betraining.com/scorm/", ""));

                            string courseRootPath = Path.Combine(scormPath, String.Format("{0}\\scorm", oldCoursePackage.CourseID));
                            string contentPath = Path.Combine(courseRootPath, "content");

                            DirectoryInfo contentDir = new DirectoryInfo(contentPath);

                            // Loop through all sections
                            ArtisanSection postTest = null;
                            foreach (DirectoryInfo di in contentDir.GetDirectories())
                            {
                                switch (di.Name)
                                {
                                    case "0": // objectives
                                        course.Objectives = oldCoursePackage.SummaryMessage;
                                        break;
                                    case "Assets":
                                        // TODO: figure out what to do with this
                                        break;
                                    case "pretest":
                                        var pretest = ProcessDirectory(di, ArtisanSectionType.Pretest);
                                        pretest.Name = "Pretest"; // could this ever be different?
                                        course.Sections.Insert(0, pretest);
                                        break;
                                    case "posttest":
                                        postTest = ProcessDirectory(di, ArtisanSectionType.Posttest);
                                        postTest.Name = "Posttest"; // could this ever be different?
                                        break;
                                    default: // main course folder
                                        if (di.Name != oldCoursePackage.CourseID.ToString())
                                        {
                                            throw new Exception("Unknown course content sub-folder encountered");
                                        }
                                        var courseDir = ProcessDirectory(di, null);

                                        foreach (var subSec in courseDir.Sections)
                                        {
                                            // Must add SCO wrapper if top-level LO is found
                                            if (subSec.SectionType == (int)ArtisanSectionType.LearningObject)
                                            {
                                                ArtisanSection scoWrap = new ArtisanSection();
                                                scoWrap.SectionType = (int)ArtisanSectionType.Sco;
                                                scoWrap.Name = subSec.Name;
                                                scoWrap.Sections = new List<ArtisanSection>();
                                                scoWrap.Sections.Add(subSec);
                                                course.Sections.Add(scoWrap);
                                            }
                                            else
                                            {
                                                course.Sections.Add(subSec);
                                            }
                                        }
                                        break;

                                }
                            }

                            // add the post-test last
                            course.Sections.Add(postTest);

                            //int newCourseId = -1; // SaveCourse expects -1 for a new course

                            //controller.SaveCourse(companyId, userId, newCourseId, course);
                            string courseJson = Symphony.Core.Utilities.Serialize(course);
                            string folder = @"c:\temp\exports\" + companyId + "\\";
                            if (!Directory.Exists(folder))
                            {
                                Directory.CreateDirectory(folder);
                            }
                            File.WriteAllText(folder + oldCoursePackage.Course.ID + ".json", courseJson);


                            Console.WriteLine("Course {0} (id: {1}) successfully imported.\n", oldCoursePackage.Course.CourseName, oldCoursePackage.Course.ID);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("***" + ex.Message + "***");
                        }
                    }
                }

                // Flush any extra keypresses
                while (Console.KeyAvailable)
                    Console.ReadKey(false);

                Console.WriteLine("\nPress any key to exit.");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("***" + ex.Message + "***");
                throw new Exception(ex.Message); // rethrow exception for debugging purposes
            }
        }

        static ArtisanSection ProcessDirectory(DirectoryInfo dirInfo, ArtisanSectionType? type)
        {
            FileInfo[] files = dirInfo.GetFiles();
            DirectoryInfo[] subDirs = dirInfo.GetDirectories();

            ArtisanSection section = new ArtisanSection();

            // If we are in a test, we only have to deal with question pages
            if (type == ArtisanSectionType.Pretest || type == ArtisanSectionType.Posttest)
            {
                section.SectionType = (int)type;
                section.Pages = new List<ArtisanPage>();

                // Loop through files (pages)
                foreach (FileInfo fi in files)
                {
                    ArtisanPage page = ProcessPage(fi);
                    if (page != null)
                    {
                        section.Pages.Add(page);
                    }
                }
            }
            else
            {
                int sectionId = Convert.ToInt32(dirInfo.Name);

                if (files.Length > 0)
                {
                    if (subDirs.Length > 0)
                    {
                        throw new Exception("Both files and sub-directories detected in a section");
                    }
                    section.SectionType = (int)ArtisanSectionType.LearningObject;
                    LearningObject oldLearningObj = db.LearningObjects.SingleOrDefault(lo => lo.ID == sectionId);
                    section.Name = oldLearningObj.LearningObjectName;
                    section.Description = oldLearningObj.LearningObjectDescription;
                    section.Objectives = oldLearningObj.Objectives;
                    section.Pages = new List<ArtisanPage>();

                    // Loop through files (pages)
                    foreach (FileInfo fi in files)
                    {
                        ArtisanPage page = ProcessPage(fi);
                        if (page != null)
                        {
                            section.Pages.Add(page);
                        }
                    }

                }
                else if (subDirs.Length > 0)
                {
                    section.SectionType = (int)ArtisanSectionType.Sco;
                    Course oldSCO = db.Courses.SingleOrDefault(c => c.ID == sectionId);
                    section.Name = oldSCO.CourseName;
                    section.Description = oldSCO.CourseDescription;
                    section.Objectives = oldSCO.Objectives;
                    section.Sections = new List<ArtisanSection>();

                    // Loop though subdirectories
                    foreach (DirectoryInfo di in subDirs)
                    {
                        ArtisanSection subSection = ProcessDirectory(di, null);
                        if (subSection != null)
                        {
                            section.Sections.Add(subSection);
                        }
                    }
                }
                else
                {
                    throw new Exception("Empty section folder detected");
                }
            }
            return section;
        }

        private class OldArtisanImageContent
        {
            public int x1 { get; set; }
            public int x2 { get; set; }
            public int y1 { get; set; }
            public int y2 { get; set; }
        }

        static ArtisanPage ProcessPage(FileInfo fileInfo)
        {
            if (fileInfo.Extension != ".html")
            {
                throw new Exception("Non-HTML file encountered");
            }
            string fileIdStr = Path.GetFileNameWithoutExtension(fileInfo.FullName);

            int pageId;
            if (Int32.TryParse(fileIdStr, out pageId))
            {
                Page oldPage = db.Pages.SingleOrDefault(p => p.ID == pageId);

                ArtisanPage newPage = new ArtisanPage();
                newPage.Name = oldPage.PageName;
                newPage.Sort = oldPage.LearningObjectPages.FirstOrDefault(p => p.PageID == oldPage.ID).SortOrder;

                if (oldPage.PageType.TypeName == "Content")
                {
                    newPage.PageType = (int)ArtisanPageType.Content;
                    string pageHtml = File.ReadAllText(fileInfo.FullName);
                    newPage.Html = string.Format(ArtisanController.WrapperMarkup, pageHtml);
                }
                else if (oldPage.PageType.TypeName == "Question")
                {
                    newPage.PageType = (int)ArtisanPageType.Question;
                    newPage.Answers = new List<ArtisanAnswer>();

                    // grab the question and all the answers from the db
                    var pageQuestionElement = oldPage.PageElements.FirstOrDefault(pe => pe.QuestionID > 0);
                    if (pageQuestionElement == null)
                        throw new Exception("No elements found for question page");

                    var question = pageQuestionElement.Question;
                    newPage.QuestionType = (ArtisanQuestionType)question.QuestionTypeID; // IDs appear to map perfectly in db
                    newPage.CorrectResponse = question.CorrectFeedback;
                    newPage.IncorrectResponse = question.IncorrectFeedback;
                    newPage.Html = question.Text;
                    foreach (var oldAnswer in question.Answers)
                    {
                        ArtisanAnswer newAnswer = new ArtisanAnswer();
                        newAnswer.Text = oldAnswer.Text;
                        newAnswer.IsCorrect = oldAnswer.IsCorrect;
                        newAnswer.Sort = oldAnswer.OrderNum;

                        if (newPage.QuestionType == ArtisanQuestionType.ImageQuestion)
                        {
                            OldArtisanImageContent oldAnswerText = Utilities.Deserialize<OldArtisanImageContent>(oldAnswer.Text);
                            Symphony.Core.Models.ArtisanImageQuestionAnswer newAnswerText = new ArtisanImageQuestionAnswer()
                            {
                                Dimensions = new Dimensions()
                                {
                                    X = oldAnswerText.x1,
                                    Y = oldAnswerText.y1,
                                    Width = oldAnswerText.x2 - oldAnswerText.x1,
                                    Height = oldAnswerText.y2 - oldAnswerText.y1,
                                },
                                ImageUrl = string.Empty //TODO: add image question url
                            };
                            newAnswer.Text = Utilities.Serialize(newAnswerText);
                        }
                        else if (newPage.QuestionType == ArtisanQuestionType.FillInTheBlank)
                        {
                            newAnswer.Text = newAnswer.Text.Replace("_______________", "__________"); // slightly shorter version in the new system
                        }
                        newPage.Answers.Add(newAnswer);
                    }
                }
                else
                {
                    throw new Exception("Invalid page type encountered");
                }

                return newPage;
            }

            return null;
        }
    }
}
