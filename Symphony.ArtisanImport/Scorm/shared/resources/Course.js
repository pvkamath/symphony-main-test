// Course.js

Course = Class.create({
	//function used to initialize the Course object.
	initialize: function(id, title, bookmarkEnabled, notesEnabled, helpEnabled, debugEnabled, certificateEnabled, navigationType, completionType, preTestType, preTestScore, preTestLabel, testOutEnabled, postTestType, postTestScore, postTestLabel, isPreview, feedbackEnabled, courseType) {
		this.id = id;
		this.title = title;
		this.bookmarkEnabled = bookmarkEnabled;
		this.notesEnabled = notesEnabled;
		this.helpEnabled = helpEnabled;
		this.debugEnabled = debugEnabled;
		this.certificateEnabled = certificateEnabled;
		this.navigationType = navigationType;
		this.completionType = completionType;
		this.preTestType = preTestType;
		this.preTestScore = preTestScore;
		this.preTestLabel = preTestLabel;
		this.testOutEnabled = testOutEnabled;
		this.postTestType = postTestType;
		this.postTestScore = postTestScore;
		this.postTestLabel = postTestLabel;		
		this.isPreview = isPreview;
		this.feedbackEnabled = feedbackEnabled;
		this.scos = new Array();
		this.scosHash = {};
		this.completed = false;
		this.courseType = courseType;
		
		if (!this.isPreview && API.GetStudentName) {
			studentName = API.GetStudentName();
		} else {
			this.isPreview = true;
			studentName = 'Preview Mode';
		}
		studentName = studentName.replace(/^,|,$/g,"");
	},
	//function used to add a new Sco object to the Scos array of the Course.
	addSco: function(sco) {
		this.scosHash[sco.id] = true;
		if(sco.id == 'objectives'){
			this.scos.unshift(sco);
		}else if(sco.id == 'pretest'){
			if(this.scosHash['objectives']){
				this.scos.splice(1,0,sco)
			}else{
				this.scos.splice(0,0,sco)
			}
		}else if(sco.id == 'posttest'){
			this.scos.push(sco);
		}else{
			if(this.scosHash['posttest']){
				this.scos.splice(this.scos.length-1,0,sco)
			}else{
				this.scos.push(sco);
			}
		}
	},
	//function used to create a new Sco object within the Course.
	newSco: function(id, title, url, scoType, masteryScore) {
		var sco = new Sco(id, title, url, scoType, masteryScore);
		this.addSco(sco);
		return sco;
	},
	//function used to find a specific Sco.
	findSco: function(id) {
		for (var i = 0; i < this.scos.length; i++)	{
			var sco = this.scos[i];
			if (sco.id == id)
				return sco;
		}
		return null;
	},
	//function used to determine if the current Sco is the first Sco.
	isFirstSco: function() {
		for (var i = 0; i < this.scos.length; i++) {
			if (this.scos[i].id == parent.currentSco) {
				if (i == 0) {
					return true;
				} else {
					return false;
				}
			}
		}
	},
	//function used to return the id of the previous Sco.
	findPreviousSco: function() {
		for (var i = 0; i < this.scos.length; i++) {
			if (this.scos[i].id == parent.currentSco) {
				if(this.scos[i-1]){
					return this.scos[i-1].id;
				}else{
					return 0;
				}
			}
		}
	},
	//function used to return the id of the next Sco.
	findNextSco: function() {
		for (var i = 0; i < this.scos.length; i++) {
			if (this.scos[i].id == parent.currentSco) {
				if(this.scos[i+1]){
					return this.scos[i+1].id;
				}else{
					return 0;
				}
			}
		}
	},	
	//function used to determine if the current Sco is the last Sco.
	isLastSco: function() {
		for (var i = 0; i < this.scos.length; i++) {
			if (this.scos[i].id == parent.currentSco) {
				if (i < this.scos.length - 1) {
					return false;
				} else {
					return true;
				}
			}
		}
	}
});