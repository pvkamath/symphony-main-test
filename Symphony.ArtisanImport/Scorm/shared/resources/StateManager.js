var StateManager = Class.create({
	Registry: {
		'PreAnswered': 'A',
		'PreCorrect': 'B',
		'PreSelected': 'C',
		'PreMasteryScore': 'D',
		'PreMaxQuestions': 'E',
		'PreQuestionIndex': 'F',
		'PreJeopardyValue': 'G',
		
		'PostAnswered': 'H',
		'PostCorrect': 'I',
		'PostSelected': 'J',
		'PostMasteryScore': 'K',
		'PostMaxQuestions': 'L',
		'PostQuestionIndex': 'M',
		'PostJeopardyValue': 'N',

		'PageIndex': 'O',
		'FlatScoStatus': 'P',
		'QuestionsSent': 'Q',
		'CurrentSco': 'R',
		'objectives': 'objectives',
		'pretest': 'pretest',
		'posttest': 'posttest',
		
		'ReviewMode': 'S'
	},
	//function used to initialize the StateManager object.
	initialize: function() {
		this.stateManagerItems = new Array(); //used to track array of StateManagerItems
		this.gameValue = 0; //used to track the running value for any game SCO
		
		this.pageIndex = 0; //new Array(); //used to track the index for the currently displayed Page
		this.questions = new Array(); //used to track array of Questions
		this.questions.selected = new Array(); //used to track array of selected Questions
		this.questions.answered = new Array();
		this.questions.correct = new Array();
		this.questionIndex = 0; //used to track the current Question
		this.maxCount = 0; //used to track the maximum number of Questions for a Test		

		this.questionsP = new Array();
		this.questionsP.selected = new Array();
		this.questionsP.answered = new Array();
		this.questionsP.correct = new Array();
		this.masteryScoreP = CourseTree.preTestScore;
		this.questionIndexP = 0; //used to track the current Question for a Pre-Test
		this.maxCountP = 0;

		this.navigationType = (CourseTree.navigationType == NavigationType.FreeForm) ? 0 : -1;
		this.tree = '';
		this.jeopardyValueP = 0;
		this.jeopardyValue = 0;
		this.bookmark = '';
		this.getFromLms();		
	},
	initializeStateManagerItems: function(){
		this.addStateManagerItem('PostAnswered', ''); //Questions Answered array
		this.addStateManagerItem('PostCorrect', ''); //Questions Correct array
		this.addStateManagerItem('PostSelected', ''); //Questions Selected array
		this.addStateManagerItem('PreAnswered', ''); //Questions Answered array for Pre-Test
		this.addStateManagerItem('PreCorrect', ''); //Questions Correct array for Pre-Test
		this.addStateManagerItem('PreSelected', ''); //Questions Selected array for Pre-Test
		this.addStateManagerItem('PostMasteryScore', ''); //Mastery Score
		this.addStateManagerItem('PreMasteryScore', ''); //Mastery Score for Pre-Test
		this.addStateManagerItem('PageIndex', ''); //Page Index array
		this.addStateManagerItem('PostMaxQuestions', ''); //Maximum number of Questions
		this.addStateManagerItem('PreMaxQuestions', ''); //Maximum number of Questions for Pre-Test
		this.addStateManagerItem('FlatScoStatus', this.tree); //Flattened out status of Scos
		this.addStateManagerItem('PostQuestionIndex', ''); //Current Question index
		this.addStateManagerItem('PreQuestionIndex', ''); //Current Question index for Pre-Test
		this.addStateManagerItem('PreJeopardyValue', 0); //Current Jeopardy Value
		this.addStateManagerItem('PostJeopardyValue', 0); //Current Jeopardy Value for Pre-Test
		this.addStateManagerItem('QuestionsSent', ''); //Questions sent array
		this.addStateManagerItem('ReviewMode', 'false'); //Explicit Flag used to put the course in review mode or not
	},
		// This is only called for tests
	buildStateManagerItemValues: function() {
		if (this.hasPreTest()) {
			this.masteryScoreP = CourseTree.preTestScore;
			this.questionsP = this.getGradedQuestions('pretest');
			switch (CourseTree.preTestType) {
				case TestType.Random:
				case TestType.Jeopardy:
					this.maxCountP = this.questionsP.length > 20 ? 20 : this.questionsP.length;
					this.questionsP.selected = this.randomize(this.questionsP, this.maxCountP);	
					break;
				case TestType.Sequential:
					this.maxCountP = this.questionsP.length;
					this.questionsP.selected = this.questionsP;
					break;
			}
		}
		
		if(this.hasPostTest()){
			this.questions = this.getGradedQuestions('posttest');
			this.masteryScore = CourseTree.postTestScore;
			switch (CourseTree.postTestType) {
				case TestType.Random:
				case TestType.Jeopardy:
					this.maxCount = this.questions.length > 20 ? 20 : this.questions.length;
					this.questions.selected = this.randomize(this.questions, this.maxCount);			
					break;
				case TestType.Sequential:
					this.maxCount = this.questions.length;
					this.questions.selected = this.questions;
					break;
			} 
		}
				
		this.storePreAndPostTestValues();
	},		
	storePreAndPostTestValues: function(){
			//store the values in the data chunk
		if (this.hasPreTest()) {
			this.updateStateManagerItem('PreSelected', this.questionsP.selected.join(';'));
			this.updateStateManagerItem('PreMasteryScore', this.masteryScoreP);
			this.updateStateManagerItem('PreMaxQuestions', this.maxCountP);
			this.updateStateManagerItem('PreQuestionIndex', this.questionIndexP);					
		}
		
		//we always store the post values, because they can be used for post-tests OR inline questions
		this.updateStateManagerItem('PostSelected', this.questions.selected.join(';'));
		this.updateStateManagerItem('PostMasteryScore', this.masteryScore);
		this.updateStateManagerItem('PostMaxQuestions', this.maxCount);
		this.updateStateManagerItem('PostQuestionIndex', this.questionIndex);	
	},
	continueCourse: function(){
		//the registry contains the "readable" keywords, keyed to the acutal stored value.
		//we need to do the opposite here, so we can convert in the other direction.
		var reverseHash = {};
		for(var prop in this.Registry){
			reverseHash[this.Registry[prop]] = prop;
		}
		var lmsData = API.GetDataChunk().split('|');
		for (var i = 0; i < lmsData.length; i++) {
			var nameValuePair = lmsData[i].split(':');
			var key = nameValuePair[0];
			var value =  nameValuePair[1];
			if(reverseHash[key]){
				key = reverseHash[key];
			}
			//once we have the appropriate "readable" word, we can continue.
			this.addStateManagerItem(key,value);
			switch (key) {
				case 'PostAnswered':
					this.questions.answered = value ? value.split(';').collect(function(s) {return parseInt(s);}) : [];
					break;
				case 'PostCorrect':
					this.questions.correct = value ? value.split(';').collect(function(s) {return parseInt(s);}) : [];
					break;
				case 'PostSelected':
					this.questions.selected = value ? value.split(';').collect(function(s) {return parseInt(s);}) : [];
					break;
				case 'PreAnswered':
					this.questionsP.answered = value ? value.split(';').collect(function(s) {return parseInt(s);}) : [];
					break;
				case 'PreCorrect':
					this.questionsP.correct = value ? value.split(';').collect(function(s) {return parseInt(s);}) : [];
					break;
				case 'PreSelected':
					this.questionsP.selected = value ? value.split(';').collect(function(s) {return parseInt(s);}) : [];
					break;
				case 'PostMasteryScore':
					this.masteryScore = value ? parseInt(value) : 0;
					break;
				case 'PreMasteryScore':
					this.masteryScoreP = value ? parseInt(value) : 0;
					break;
				case 'PageIndex':
					this.pageIndex = value;
					break;
				case 'PostMaxQuestions':
					this.maxCount = parseInt(value);
					break;
				case 'PreMaxQuestions':
					this.maxCountP = parseInt(value);
					break;
				case 'FlatScoStatus':
					this.tree = value;
					break;
				case 'PostQuestionIndex':
					//HACK: since this could be set incorrectly in pretest/testout scenarios... do a sanity check
					if (this.questions.answered.length == 0) {
						this.questionIndex = 0;
					} else {
						this.questionIndex = value ? parseInt(value) : 0;
					}
					break;	
				case 'PreQuestionIndex':
					this.questionIndexP = value ? parseInt(value) : 0;
					break;
				case 'PreJeopardyValue':
					this.jeopardyValueP = value ? parseInt(value) : 0;

					//display Zero if you are in a preTest and it has not been completed
					//if( !(this.questionsP.selected.length == this.questionsP.answered.length) ) {
					//	this.jeopardyValueP = 0;
					//}					
					
					// set this to zero if you really wanted the $$ on the screen to appear zero
					this.jeopardyValueP = 0;
					break;
				case 'PostJeopardyValue':
					//this.jeopardyValue = value ? parseInt(value) : 0;
					// set this to zero if you really wanted the $$ on the screen to appear zero
					this.jeopardyValue = 0;
					break;
				case 'CurrentSco':
					parent.currentSco = value;
					break;
				case 'ReviewMode':
					if (value == 'true') {
						parent.blnReviewModeSoReadOnly = true;
						this.reviewMode = true;
					} else {
						parent.blnReviewModeSoReadOnly = false; // this enables continued course taking if the passing grade was from a pretest with testout enabled
					}
					break;
			}
		}
	},
		//function used to start a Sequential and Random Test.
	sequentialTestBegin: function() {
		$('introduction').hide();
		$('noPageNumber').hide();
		$('pageNumbers').show();
		
		if (this.inPreTest()) {
			Ext.get('pageNumber').update(Number(this.questionIndexP) + 1);
			Ext.get('pageCount').update(this.maxCountP);			
		} else if (this.inPostTest()) {
			Ext.get('pageNumber').update(Number(this.questionIndex) + 1);
			Ext.get('pageCount').update(this.maxCount);			
		}
		LoadQuestion();		
	},
		jeopardyTestBegin: function() {
		$('introduction').hide();
		$('jeopardyGame').show();
		var questions;
		var answered;
		if (this.inPreTest()) {
			answered = this.questionsP.answered || []; //findStateManagerItemValue('PreAnswered').split(';');
			questions = this.questionsP.selected;//findStateManagerItemValue('PreSelected').split(';');
		} else if (this.inPostTest()) {
			answered = this.questions.answered || [];//findStateManagerItemValue('PostAnswered').split(';');
			questions = this.questions.selected;//findStateManagerItemValue('PostSelected').split(';');
		}
		var counter = 0;
		for (var i = 0; i < 20; i++) { //loop through the columns
			if (counter < questions.length) {   		
				var btnOn = 'jeopardyButton' + i;
				var btnOff = 'jeopardyButtonOff' + i;
				if (answered.any(function(ans){ return questions[i] == ans; })) {
					$(btnOff).show();
				} else {
					$(btnOn).show();
				}
				counter++;
			}
		}
		this.displayJeopardyScore();
	},
	findCurrentSco: function(){
		return CourseTree.findSco(parent.currentSco);
	},
	inPreTest: function(){
		return parent.currentSco == 'pretest';
	},
	inPostTest: function(){
		return parent.currentSco == 'posttest';
	},
		//function used to return the current Question object.
	getQuestion: function() {
		if (this.inPreTest()) {
			return this.findCurrentSco().findPage(this.questionsP.selected[this.questionIndexP]);
		} else {
			return this.findCurrentSco().findPage(this.questions.selected[this.questionIndex]);
		}		
	},
		//function used to return the URL for the current Question,
	getQuestionUrl: function() {
		if (this.inPreTest()) {
			this.questionsP.selected = this.findStateManagerItemValue('PreSelected').split(';');
			return parent.currentScoUrl + this.questionsP.selected[this.questionIndexP] + ".html";
		} else {
			this.questions.selected = this.findStateManagerItemValue('PostSelected').split(';');
			return parent.currentScoUrl + this.questions.selected[this.questionIndex] + ".html";
		}
	},
	
	getQuestionIndex: function() {
		if (this.inPreTest()) {
			return this.questionIndexP;
		} else {
			return this.questionIndex;
		}		
	},
	
	setQuestionIndex: function(index) {
		if (this.inPreTest()) {
			this.questionIndexP = index;
			this.updateStateManagerItem('PreQuestionIndex', index);
		} else {
			this.questionIndex = index;
			this.updateStateManagerItem('PostQuestionIndex', index);
		}
	},
		//function used to add a new StateManagerItem object to the StateManagerItems.
	addStateManagerItem: function(id, value) {
		this.updateStateManagerItem(id, value);
	},
	//function used to create a new StateManagerItem object.
	updateStateManagerItem: function(id, value) {
		if (!CourseTree.isPreview) {
			var nameValuePair = this.findStateManagerItem(id);
			if (!nameValuePair){
				var sid = this.Registry[id] ? this.Registry[id] : id;
				if(sid == undefined){
					throw new Error('Invalid id specified to be stored in the state manager. Expected a value, got "undefined"');
				}
				var stateManagerItem = new StateManagerItems(sid, value);
				this.stateManagerItems.push(stateManagerItem);	
			}
			else{
				nameValuePair.data = value;
			}
		}
	},	
	//function used to find a specific StateManagerItem object.
	findStateManagerItem: function(id) {
		if (!this.Registry[id] && parseInt(id) == NaN){
			throw new Error("Bad Registry key "+id);
		}
		var rId = this.Registry[id] ? this.Registry[id] : id;
		for (var i = 0; i < this.stateManagerItems.length; i++) {
			if (this.stateManagerItems[i].id == rId) {
				return this.stateManagerItems[i];
			}
		}
		return null;
	},	
	//function used to find the value of a specific StateManagerItem.	
	findStateManagerItemValue: function(id) {
		var item = this.findStateManagerItem(id);
		if (!item){
			return null;
			throw new Error("The id " + id + " could not be found");
		}
		return item.data;
	},
		//function used to find if the Course has a Pre-Test.
	hasPreTest: function() {
		for (var i = 0; i < CourseTree.scos.length; i++) {
			if (CourseTree.scos[i].id == 'pretest') {
				return true;
			}
		}
		return false;
	},
		//function used to find if the Course has a Post-Test.
	hasPostTest: function() {
		for (var i = 0; i < CourseTree.scos.length; i++) {
			if (CourseTree.scos[i].id == 'posttest') {
				return true;
			}
		}
		return false;
	},
		//function used to parse the LMS Bookmark.
	parseBookmark: function() {
		this.bookmark = parent.GetBookMark();
	},
	//function used to determine if the Test has been started.
	isTestStarted: function() {
		if (this.inPreTest()) {
			return (this.questionsP.answered != undefined && this.questionsP.answered.length > 0);
		} else if (this.inPostTest()) {
			return (this.questions.answered != undefined && this.questions.answered.length > 0);
		} else {
			return false;
		}
	},
	
	isTestComplete: function() {
		if (this.inPreTest()) {	
			return (this.questionsP.answered.length == this.questionsP.selected.length);	
		} else {			
			return (this.questions.answered.length == this.questions.selected.length);	
		}
	},
	
	//function used to return all graded Question objects for a specific Sco.
	getGradedQuestions: function(id) {
		var questions = new Array();
		for (var i = 0; i < CourseTree.findSco(id).pages.length; i++) {
			if (CourseTree.findSco(id).pages[i].pageType == PageType.Question && CourseTree.findSco(id).pages[i].calculateGrade) {
				questions.push(CourseTree.findSco(id).pages[i].id);
			}
		}
		return questions;
	},
	//function used to return the count of all graded Questions for a specific Sco.
	getGradedQuestionCount: function() {
		var counter = 0;
		for (var i = 0; i < this.findCurrentSco().pages.length; i++) {
			if (this.findCurrentSco().pages[i].pageType == PageType.Question && this.findCurrentSco().pages[i].calculateGrade) {
				counter ++;
			}
		}

		return counter;
	},
		
	//function used to return the current Page object or Question object.
	getCurrentPage: function() {
		if (isTestSco()) {
			return this.getQuestion();
		} else {
			return this.findCurrentSco().findPage(parent.currentPage);
		}
	},
	//function used to set a specific Page as complete and update the LMS.
	setPageComplete: function(id, value) {
		if (parent.currentPage != 'test') {
			this.addStateManagerItem(id, value);		
		}
		if (isTestSco()) {
			var q, item;
			if(inPreTest()){
				item = 'PreAnswered';
				q = this.questionsP.answered;
			}else{
				item = 'PostAnswered';
				q = this.questions.answered;
			}
			q.push(id);
			this.updateStateManagerItem(item, q.join(";"));
		} else {
			if (this.findCurrentSco().findPage(parent.currentPage).pageType == PageType.Question ||
				CourseTree.completionType == CompletionType.FreeFormNav ||
				CourseTree.completionType == CompletionType.SequentialNav) {

				if (CourseTree.isFirstSco() && this.findCurrentSco().isFirstPage()) {
					$('navigationBack').hide();
				} else {
					$('navigationBack').show();
				}
				$('navigationNext').show();
				if ((CourseTree.completionType == CompletionType.SequentialNav) &&
					(this.isCourseComplete && !this.isCourseComplete()) && CourseTree.isLastSco() && this.findCurrentSco().isLastPage()){
					$('navigationNext').hide();
				}
			}
		}
		this.saveToLms();
	},

	//function used to store a correctly answered Question.
	addCorrectQuestion: function() {
		var item;
		var q;
		if (this.inPreTest()) {
			if(!this.questionsP.correct){
				this.questionsP.correct = [];
			}
			q = this.questionsP.correct;
			item = 'PreCorrect';
		} else if (this.inPostTest() || CourseTree.completionType == CompletionType.InlineQuestions) {
			if(!this.questions.correct){
				this.questions.correct = [];
			}
			q = this.questions.correct;
			item = 'PostCorrect';
		}
		if (CourseTree.isPreview && CourseTree.completionType == CompletionType.InlineQuestions) {
		    q.push(this.getCurrentPage().id);
		} else {
		    q.push(this.getQuestion().id);
		}
		var value = q.join(';');
		this.updateStateManagerItem(item, value);
		this.saveToLms();
	},	
	//function used to load the current Page in the current Sco.
	loadPageContent: function() {
		if (parent.currentPage == '') {
			parent.currentPage = this.findCurrentSco().pages[0].id;	
		}
		
		if (!isTestSco() && parent.currentPage != 'review') {
			if (this.getCurrentPage().pageType == PageType.Content) {
				if (!this.isPageComplete(parent.currentPage)) {
					this.setPageComplete(parent.currentPage, '');
				}
				if (this.findCurrentSco().isLastPage(parent.currentPage)) {
					this.setScoComplete();
				}
			}
		}
	},
	
	//function used to send the current StateManagerItems to the LMS.
	saveToLms: function() {
		if (!CourseTree.isPreview) {
			//always store the current sco
			this.updateStateManagerItem('CurrentSco',parent.currentSco);
			API.SetDataChunk(this.flattenArray());
		}
	},	
		//function used to convert the StateManagerItems array into a flat string for the LMS.
	flattenArray: function() {
		var data = '';
		for (var i = 0; i < this.stateManagerItems.length; i++) { 
			if (i > 0) {
				data += '|';
			}
			data += this.stateManagerItems[i].id + ':' + this.stateManagerItems[i].data;
		}
		return data;
	},
	//function used to build a Question array in a random format.
	randomize: function(questions, max) {
		return questions.sortBy(Math.random).slice(0,max);
	},
	//function used to build a Question array in a sequential format.
	noRandomize: function(questions, max) {
		return questions;	
	},
		//function used to calculate the score of a Test.
	calculateScore: function() {
		var item;
		if (this.inPreTest()){
			item = {
				correct: "PreCorrect",
				answered: "PreAnswered"
			};
		} else {
			item = {
				correct: "PostCorrect",
				answered: "PostAnswered"
			};
		}
		var correct = this.findStateManagerItemValue(item.correct);
		var graded = this.findStateManagerItemValue(item.answered);
		correct = ( correct == "" ? [] : correct.split(';'));
		graded = ( graded == "" ? [] : graded.split(';'));
		
		if (graded.length == 0) {
			return 100;
		}
		if (correct.length == 0) {
			return 0;
		}
		
		var score = parseFloat((correct.length / graded.length) * 100);
		if (score > 100) {
			score = 100;
		}
		return score;	
	},		
		updateJeopardyTotal: function(value, correct) {
		var actualValue = correct ? value : -value;
		if (this.inPreTest()) {
			this.jeopardyValueP += actualValue;
			this.updateStateManagerItem('PreJeopardyValue', this.jeopardyValueP);
		} else if (this.inPostTest()) {
			this.jeopardyValue += actualValue;
			this.updateStateManagerItem('PostJeopardyValue', this.jeopardyValue);		
		}
		this.saveToLms();
	},
		//function used to calculate the current Jeopardy value for the current Test.
	calculateJeopardyTotal: function(correct) {
		var multiplier = 1;
		var value = 100;
		switch (this.getQuestionIndex()) {
			case 0:
			case 5:
			case 10:
			case 15:
				multiplier = 1;
				break;
			case 1:
			case 6:
			case 11:
			case 16:
				multiplier = 2;
				break;
			case 2:
			case 7:
			case 12:
			case 17:
				multiplier = 3;
				break;
			case 3:
			case 8:
			case 13:
			case 18:
				multiplier = 4;
				break;
			case 4:
			case 9:
			case 14:
			case 19:
				multiplier = 5;
				break;
		}
		value = value * multiplier;
		this.updateJeopardyTotal(value, correct);
		this.displayJeopardyScore();
	},
	displayJeopardyScore: function(){
		var jeopardyValue = this.getJeopardyValue();
		//everything correct = (100+200+300+400+500) * 4 = 1500 * 4 = 6000 = 4 digits
		var padded = jeopardyValue.abs().toPaddedString(4);
		var thousands = padded.substring(0,1);
		var hundreds = padded.substring(1,4);
		
		$('jeopardyTotalMinus').style.background = 'url(../resources/images/jeopardy.total.minus.' + (jeopardyValue < 0 ? 'on' : 'off') + '.gif)';
		$('jeopardyTotalThousand').style.background = 'url(../resources/images/jeopardy.total.' + thousands + 'k.gif)';
		$('jeopardyTotalHundred').style.background = 'url(../resources/images/jeopardy.total.' + hundreds + '.gif)';
	},
	getJeopardyValue: function() {
		if (this.inPreTest()) {
			return this.jeopardyValueP || 0;
		} else if (this.inPostTest()) {
			return this.jeopardyValue || 0;
		}
	},
			//function used to update the current Question index.
	updateQuestionIndex: function(increment) {
		if (this.inPreTest()) {
			if (!(increment < 0 && this.questionIndexP == 0)) {
				this.questionIndexP += increment;
				this.updateStateManagerItem('PreQuestionIndex', this.questionIndexP);
			}
		} else {
			if (!(increment < 0 && this.questionIndex == 0)) {
				this.questionIndex += increment;
				this.updateStateManagerItem('PostQuestionIndex', this.questionIndex);
			}
		}
	},

	//function used when the "BACK" button is clicked within the UI.
	clickBack: function() {
		if (!isTestSco()) {
			if (!this.isPageComplete(parent.currentPage)) {
				this.setPageComplete(parent.currentPage, '');
			}
			if(this.getCurrentPage().pageType == PageType.Question && !this.findCurrentSco().isFirstPage()){
				this.updateQuestionIndex(-1);
			}
			if (this.findCurrentSco().isFirstPage()) {
				this.previousSco();
			}
			else {
				this.updatePageIndex(-1);
				this.navigatePage();
			}			
		}	
	},
	
	//function used when the "NEXT" button is clicked within the UI.
	clickNext: function() {
		if (isTestSco()) {
			if (parent.currentPage == 'review') {
				this.nextSco();
			} else {
				this.updateQuestionIndex(1);
				Ext.get('pageNumber').update(Number(this.getPageIndex()) + 1);
				$('navigationNext').hide();
				this.navigatePage();
			}
		} else {
			if (!this.isPageComplete(parent.currentPage)) {
				this.setPageComplete(parent.currentPage, '');
			}
			//if the current page is a question page (and we're in an inline question type SCO, increment
			if(CourseTree.completionType == CompletionType.InlineQuestions && this.getCurrentPage().pageType == PageType.Question && !this.findCurrentSco().isLastPage()){
				this.updateQuestionIndex(1);
			}
			if (this.findCurrentSco().isLastPage()) {
				//we show the inline review differently if its scorm2k4 vs 1.2....2k4 is at the end of each sco, and 1.2 is at the end of the last sco.
				if(this.shouldShowInlineReview()){
					parent.currentPage = 'review';
					this.updateStateManagerItem('PageIndex', parent.currentSco + "#0");
					LoadContent();
					//this.saveToLms();
				}else{
					this.nextSco();
				}
			} else {
				this.updatePageIndex(1);
				this.navigatePage();	
			}
		}
	},
	
	//function used to set the current Page and load in the UI.
	navigatePage: function() {
		if (isTestSco()) {
			Ext.get('pageNumber').update(Number(this.getQuestionIndex()) + 1);
			LoadQuestion();	
		} else {
			parent.currentPage = this.findCurrentSco().getPageByIndex(this.getPageIndex()).id;
			LoadContent();
		}
		this.saveToLms()
	},
		//function used to set Header and Footer UI elements.
	setPageElements: function() {
		Ext.get('courseName').update(this.findCurrentSco().title);
		//Set header button visibility
		if (emailAddress.length == 0) {
			$('EmailLink').style.display = 'none';
		}
		if (!CourseTree.helpEnabled) {
			$('HelpLink').style.display = 'none';
		}
		if (isTestSco()) {			
			$('navigationBack').hide();
			$('navigationNext').hide();
			$('pageNumbers').hide();
			$('noPageNumber').show();			
		} else {
			Ext.get('pageNumber').update(Number(this.getPageIndex()) + 1);
			Ext.get('pageCount').update(this.findCurrentSco().pages.length);
			$('pageNumbers').show();
			$('noPageNumber').hide();			
			
			//Set navigation button visibility
			if(parent.currentPage == 'review'){
				$('navigationBack').hide();
				$('navigationNext').hide();	
			}else{
				if (this.getCurrentPage().pageType == PageType.Question && !this.isPageComplete(parent.currentPage)) {
					$('navigationBack').hide();
					$('navigationNext').hide();				
				} else {
					$('navigationBack').show();
					$('navigationNext').show();
					
					if (CourseTree.isFirstSco() && this.findCurrentSco().isFirstPage()) {
						$('navigationBack').hide();
						$('navigationNext').show();				
					}
					
					if (CourseTree.isLastSco() && this.findCurrentSco().isLastPage()) {
						$('navigationBack').show();
						if ((CourseTree.completionType != CompletionType.InlineQuestions && CourseTree.completionType != CompletionType.FreeFormNav &&
							CourseTree.completionType != CompletionType.SequentialNav) ||
							(this.isCourseComplete && !this.isCourseComplete())) {
							$('navigationNext').hide();	
						}
					}
				}
			}			
		}
	},
	
	// search through the stateManagerItems list for the ID (probably the PageId).  If found, isComplete is true
	isComplete: function(id){
		for (var i = 0; i < this.stateManagerItems.length; i++) {
			if (this.stateManagerItems[i].id == id) {
				return true;
			}
		}
		return false;
	},

	//function used to determine if the current Page was previously completed.
	isPageComplete: function() {
		var page = this.getCurrentPage();
		if(!page){
			return false;
		}
		var cId = page.id;
		if (isTestSco()) {
			var q = this.inPreTest() ? this.questionsP.answered : this.questions.answered;
			if(!q) return false;
			return q.any(function(q){
				return q == cId;
			});
		} else {
			return this.isComplete(cId);
		}
	},	
	
	getCurrentUserAnswers: function(){
		var data = DataChunk.findStateManagerItemValue(parent.currentPage);
		var state = [];
		if(data) {
			state = data.split(';');
		}
		return state;
	},
	
	getFromLms: function() { throw new Error("getFromLms Not Implemented"); },
	buildPageIndex: function() { throw new Error("buildPageIndex Not Implemented"); },
	getPageIndex: function() { throw new Error("getPageIndex Not Implemented"); },
	updatePageIndex: function(pageIncrement) { throw new Error("updatePageIndex Not Implemented"); },
	loadSco: function() { throw new Error("loadSco Not Implemented"); },
	sendInteractionsToLms: function(answer) { throw new Error("sendInteractionsToLms Not Implemented"); },
	addSentQuestion: function(id) { throw new Error("addSentQuestion Not Implemented"); },
	isScoComplete: function() { throw new Error("isScoComplete Not Implemented"); },
	setScoComplete: function() { throw new Error("setScoComplete Not Implemented"); },
	previousSco: function() { throw new Error("previousSco Not Implemented"); },
	navigateTest: function() { throw new Error("navigateTest Not Implemented"); },
	nextSco: function() { throw new Error("nextSco Not Implemented"); },
	getMasteryScore: function() { throw new Error("getMasteryScore Not Implemented"); },
	loadContentFromTree: function() { throw new Error("loadContentFromTree Not Implemented"); },
	reBuildTreeLinks: function() { throw new Error("reBuildTreeLinks Not Implemented"); },
	shouldShowInlineReview: function(){	throw new Error("shouldShowInlineReview Not Implemented"); },
	
	hasPassed: function(){
		var userScore = this.calculateScore();
		var masteryScore = this.getMasteryScore();
		if (CourseTree.completionType == CompletionType.FreeFormNav || CourseTree.completionType == CompletionType.SequentialNav) {
			masteryScore = 100;
		}
		return userScore >= masteryScore;
	},
	
	//function used to store an answered Question.
	addAnsweredQuestion: function() {
		var item = '';
		var q;
		if (this.inPreTest()) {
			item = "PreAnswered";
			q = this.questionsP;
		} else {
			item = "PostAnswered";
			q = this.questions;
		}
		var ans = this.findStateManagerItemValue(item);
		q.answered = ans ? ans.split(';') : [];
		q.answered.push(q.selected[this.getQuestionIndex()]);
		var value = q.answered.join(';');
		this.updateStateManagerItem(item, value);
		this.saveToLms();
	},
	
	//function used to return the Test type for the current Sco.
	findTestType: function() {
		if (this.inPreTest()) {
			return CourseTree.preTestType;
		} else if (this.inPostTest()) {
			return CourseTree.postTestType;
		}
	},
	
	isQuestionCorrect: function(id, preTest) {
		if ((!preTest && this.questions.correct == undefined) ||(preTest && this.questionsP.correct == undefined)) {
			return false;
		} else {
			var qColl = preTest ? this.questionsP : this.questions;
			for (var i = 0; i < qColl.correct.length; i++) {
				if (id == qColl.correct[i]) {
					return true;
				}
			}
		}
		return false;
	}
});

StateManagerItems = Class.create({
	initialize: function(id, data) {
		this.id = id;
		this.data = data;
	}
});

//factory class for initializing the StateManager based on the CourseTree.courseType
var StateManagerFactory = Class.create({
	loadFactory: function() {
		var factory;
		switch(CourseTree.courseType) {
			case CourseFormat.AICC:
				factory = new StateManagerAICC();
				break;
			case CourseFormat.SCORM:
				factory = new StateManagerSCORM12();
				break;
			case CourseFormat.SCORM2004:
				factory = new StateManagerSCORM2004();
				break;
		}
		return factory;
	}
});
