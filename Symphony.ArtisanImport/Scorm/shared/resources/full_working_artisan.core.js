/* set global parameters for the course content frame */
var API = parent;
var studentName = '';

var TestType = {
	Jeopardy: 1,
	Sequential: 2,
	Random: 3
};

var CompletionType = {
	FreeFormNav: 1,
	SequentialNav: 2,
	Test: 4,
	InlineQuestions: 5
};

var QuestionType = {
	MultipleChoice: 1,
	TrueFalse: 2,
	FillInTheBlank: 3,
	AllThatApply: 4,
	Image: 5
};

var MessageType = {
	PreTestIntroHeader: 3,
	PreTestIntroSingle: 4,
	PreTestIntroMultiple: 5,	
	PreTestFailedSingleNoTest: 6,
	PreTestPassedSingleNoTest: 7,
	PreTestFailedMultipleNoTest: 8,
	PreTestPassedMultipleNoTest: 9,
	PreTestFailedSingleTest: 10,
	PreTestPassedSingleTest: 11,	
	PreTestFailedMultipleTest: 12,
	PreTestPassedMultipleTest: 13,	
	PostTestIntroHeader: 14,
	PostTestIntroSingleNoPre: 15,
	PostTestIntroMultipleNoPre: 16,	
	PostTestIntroSinglePre: 17,	
	PostTestIntroMultiplePre: 18,	
	PostTestFailedSingle: 19,
	PostTestPassedSingle: 20,	
	PostTestFailedMultiple: 21,
	PostTestPassedMultiple: 22,
	ExitButton: 23,
	NotesButton: 24,
	HelpButton: 25,
	EmailButton: 26	
};

Course = Class.create();
Course.prototype = {
	initialize: function(id, title, bookmarkEnabled, notesEnabled, helpEnabled, debugEnabled, certificateEnabled, navigationType, completionType, preTestType, preTestScore, preTestLabel, testOutEnabled, postTestType, postTestScore, postTestLabel, isPreview, feedbackEnabled) {
		this.id = id;
		this.title = title;
		this.bookmarkEnabled = bookmarkEnabled;
		this.notesEnabled = notesEnabled;
		this.helpEnabled = helpEnabled;
		this.debugEnabled = debugEnabled;
		this.certificateEnabled = certificateEnabled;
		this.navigationType = navigationType;
		this.completionType = completionType;
		this.preTestType = preTestType;
		this.preTestScore = preTestScore;
		this.preTestLabel = preTestLabel;
		this.testOutEnabled = testOutEnabled;
		this.postTestType = postTestType;
		this.postTestScore = postTestScore;
		this.postTestLabel = postTestLabel;		
		this.isPreview = isPreview;
		this.feedbackEnabled = feedbackEnabled;
		this.scos = new Array();
		this.completed = false;
		
		if (!this.isPreview && API.GetStudentName) {
			studentName = API.GetStudentName();
		} else {
			this.isPreview = true;
			studentName = 'Preview Mode';
		}
	},
		
	addSco: function(sco) {
		this.scos.push(sco);
	},
	
	newSco: function(id, title, url, scoType, masteryScore) {
		var sco = new Sco(id, title, url, scoType, masteryScore);
		this.addSco(sco);
		return sco;
	},
	
	findSco: function(id) {
		for (var i = 0; i < this.scos.length; i++)	{
			var sco = this.scos[i];
			if (sco.id == id)
				return sco;
		}
		return null;
	},
	
	hasPreTest: function() {
		for (var i = 0; i < this.scos.length; i++) {
			if (this.scos[i].id == 'pretest') {
				return true;
			}
		}
		return false;
	}
}

Sco = Class.create();
Sco.prototype = {
	initialize: function(id, title, url, scoType, masteryScore) {
		this.id = id;
		switch (id) {
			case 'pretest':
				this.title = CourseTree.preTestLabel;
				this.masteryScore = CourseTree.preTestScore;			
				break;
			case 'posttest':
				this.title = CourseTree.postTestLabel;
				this.masteryScore = CourseTree.postTestScore;
				break;
			case 'objectives':
				this.title = 'Objectives';
				this.masteryScore = 0;
				break;
			default:
				this.title = title;
				this.masteryScore = masteryScore;
				break;
		}
		this.url = url;
		this.scoType = scoType;
		this.pages = new Array();
		this.scoredQuestionCount = 0;
	},
	
	getIndexById: function(pageid) {
		for (var i = 0; i < this.pages.length; i++)	{
			if (this.pages[i].id == pageid) {
				return i;
			}
		}
		return null;
	},
	
	getPageByIndex: function(index) {
	    return this.pages[index];
	},
	
	getCurrentPage: function() {
	    return this.pages[DataChunk.currentPageIndex];
	},
			
	setCurrentPage: function() {
		if (this.id == 'pretest' || this.id == 'posttest' || parent.currentSco == 'objectives') {
			Ext.get('pageNumber').update(DataChunk.currentPageIndex + 1);
		} else {
			DataChunk.currentPageIndex = this.getIndexById(parent.currentPage);
			Ext.get('pageNumber').update(DataChunk.currentPageIndex + 1);				
		}
	},

	setPageCount: function() {
		if (this.id == 'posttest' || this.id == 'pretest') {
			Ext.get('pageCount').update(DataChunk.maxCount);
		} else {
			Ext.get('pageCount').update(this.pages.length);
		}
	},
		
	addPage: function(page) {
		this.pages.push(page);
	},
	
	newPage: function(id, title, pageType, calculateGrade, questionType, questionText) {
		var page = new Page(id, title, pageType, calculateGrade, questionType, questionText);
		this.addPage(page);
		return page;
	},
	
	findPage: function(id) {
		for (var i = 0; i < this.pages.length; i++)	{
			var page = this.pages[i];
			if (page.id == id)
				return page;
		}
		return null;
	},
	
	isLastPage: function(page) {
		if (this.pages[this.pages.length - 1].id == page) {
			return true;
		}
		return false;
	},
	
	getScoredQuestions: function() {
		var questions = new Array();
		for (var i = 0; i < this.pages.length; i++) {
			if (this.pages[i].pageType == 2 && this.pages[i].calculateGrade) {
				questions.push(this.pages[i].id);
			}
		}
		return questions;
	},
	
	getScoredQuestionCount: function() {
		var counter = 0;
		for (var i = 0; i < this.pages.length; i++) {
			if (this.pages[i].pageType == 2 && this.pages[i].calculateGrade) {
				counter ++;
			}
		}
//		DataChunk.updateStateData('QC', counter);
		return counter;
	}
}

Page = Class.create();
Page.prototype = {
	initialize: function(id, title, pageType, calculateGrade, questionType, questionText) {
		this.id = id;
		this.title = title;
		this.calculateGrade = calculateGrade;
		this.correct = false;
		this.completed = false;
		this.answers = new Array();
		this.pageType = pageType;
		this.questionType = questionType;		
		this.questionText = questionText;
	},
	
	addAnswer: function(answer) {
		this.answers.push(answer);
	},

	newAnswer: function(id, text, isCorrect) {
		var answer = new Answer(id, this.id, text, isCorrect);
		this.addAnswer(answer);
		return answer;
	},
	
	findAnswers: function() {
		var answers = DataChunk.findStateDetail(this.id).data.split(';');
		var responseIdentifier = new Array();
		for (var i = 0; i < answers.length; i++) {
			for (var j = 0; j < this.answers.length; j++) {
				var answer = this.answers[j];
				if (answer.id == answers[i]) {
					responseIdentifier.push(parent.CreateResponseIdentifier(this.convertNumberToLetter(j), answer.text));
				}
			}
		}
		return responseIdentifier;
	},
	
	convertNumberToLetter: function(index) {
		var letter = '';
		switch (index) {
			case 0:
				letter = 'a';
				break;
			case 1:
				letter = 'b';
				break;
			case 2:
				letter = 'c';
				break;
			case 3:
				letter = 'd';
				break;
			case 4:
				letter = 'e';
				break;
			case 5:
				letter = 'f';
				break;
			case 6:
				letter = 'g';
				break;
			case 7:
				letter = 'h';
				break;
			case 8:
				letter = 'i';
				break;				
		}
		return letter;
	},
	
	findCorrectAnswers: function() {
		var responseIdentifier = new Array();
		for (var i = 0; i < this.answers.length; i++) {
			if (this.answers[i].isCorrect) {
				switch (this.questionType) {
					case QuestionType.TrueFalse:
						return true;
						break;
					case QuestionType.FillInTheBlank:
						return this.answers[i].text;
						break;
					default:
						responseIdentifier.push(parent.CreateResponseIdentifier(this.convertNumberToLetter(i), this.answers[i].text));					
						break;
				}
			}
		}
		switch (this.questionType) {
			case QuestionType.TrueFalse:
				return false;
				break;
			case QuestionType.FillInTheBlank:
				return '';
				break;	
			default:
				return responseIdentifier;	
				break;
		}
	}
}

Answer = Class.create();
Answer.prototype = {
	initialize: function(id, pageid, text, isCorrect) {
		this.id = id;
		this.pageid = pageid;
		this.text = text;
		this.isCorrect = isCorrect;
	}
}

StateManager = Class.create();
StateManager.prototype = {
	initialize: function() {
		this.stateData = new Array();
		this.questions = new Array();
		this.questions.selected = new Array();
		this.questions.answered = new Array();
		this.questions.correct = new Array();
		this.questions.sent = new Array();
		this.masteryScore = CourseTree.findSco(parent.currentSco).masteryScore;
		this.maxCount = 0;
		this.testType = 0;
		this.currentPageIndex = 0;
		this.scorePerQuestion = 0;
		this.correctCount = 0;
		this.jeopardyValue = 0;
		this.displayFeedback = CourseTree.feedbackEnabled;
		this.displayReview = CourseTree.reviewEnabled;
		this.randomized = false;
		this.testStarted = false;
		if (parent.currentSco == 'pretest') {
			this.masteryScore = CourseTree.preTestScore;
			this.testType = CourseTree.preTestType;
		}
		if (parent.currentSco == 'posttest') {
			this.masteryScore = CourseTree.postTestScore;
			this.testType = CourseTree.postTestType;
		}
		if(parent.currentSco == 'objectives') {
			this.masteryScore = 0;
		}
		if (this.testType != TestType.Sequential) {
			this.randomized = true;
		} else {
			maxCount = this.questions.length;
		}
		this.getFromLms();

	},
	
	addDetail: function(id, data) {
		var stateDetail = new StateManagerDetail(id, data);
		this.stateData.push(stateDetail);
	},
	
	setComplete: function(id, data) {
		this.addDetail(id, data);	
		
		if (parent.currentSco == 'pretest' || parent.currentSco == 'posttest') {
			//if (this.testType == TestType.Jeopardy) {
				var answered = this.findStateData('QA');
				if (answered == null || answered.length == 0) {
					answered = '' + id;
				} else {
					answered += ';' + id;
				}
				this.questions.answered = answered.split(';');
				this.updateStateData('QA', answered);
			//}
		}
	},
	
	isCompleted: function(id) {
		for (var i = 0; i < this.stateData.length; i++) {
			if (this.stateData[i].id == id ) {
				return true;
			}
		}
		return false;
	},
	
	findStateData: function(id) {
		for (var i = 0; i < this.stateData.length; i++) {
			if (this.stateData[i].id == id) {
				return this.stateData[i].data;
			}
		}
		return null;
	},
	
	findStateDetail: function(id) {
		for (var i = 0; i < this.stateData.length; i++) {
			if (this.stateData[i].id == id) {
				return this.stateData[i];
			}
		}
		return null;
	},
	
	updateStateData: function(id, value) {
		if (!CourseTree.isPreview) {
			var nameValuePair = this.findStateDetail(id);
			nameValuePair.data = value;
		}
	},
	
	updatePageIndex: function(index) {
		this.currentPageIndex = index;
		this.updateStateData('CP', this.currentPageIndex);
	},
	
	calculateScore: function() {
		var score = parseFloat(100 / parseInt(this.findStateData('QC'))) * parseInt(this.findStateData('CC'));
		if(score > 100) score = 100;
		return score;
	},
	
	calculateJeopardyValue: function(correct) {
		var multiplier = 1;
		var value = 100;
		switch (this.currentPageIndex) {
			case 0:
			case 5:
			case 10:
			case 15:
				multiplier = 1;
				break;
			case 1:
			case 6:
			case 11:
			case 16:
				multiplier = 2;
				break;
			case 2:
			case 7:
			case 12:
			case 17:
				multiplier = 3;
				break;
			case 3:
			case 8:
			case 13:
			case 18:
				multiplier = 4;
				break;
			case 4:
			case 9:
			case 14:
			case 19:
				multiplier = 5;
				break;
		}
		value = value * multiplier;
		if (correct) {
			this.jeopardyValue = this.jeopardyValue + value;
		} else {
			this.jeopardyValue = this.jeopardyValue - value;		
		}
		this.updateStateData('JV', this.jeopardyValue);
	},
	
	randomize: function(Q, M) {
	    var J, K, L = Q.length, T;
		for (K = L; K > L - M; K--) {
			J =  Math.floor(K * (Math.random() % 1));
			T = Q[K - 1];
			Q[K - 1] = Q[J];
			Q[J] = T;
		}
		var questions = Q.slice(L - M);
		var selected = '';
		for (var i = 0; i < questions.length; i ++) {
			if (i > 0) {
				selected += ';';
			}
			selected += questions[i];
		}

		this.addDetail('QS', selected);
		this.questions.selected = selected.split(';');
   	},
   	
   	buildStateData: function() {
		this.addDetail('MS', this.masteryScore); //masteryScore
		this.addDetail('QC', 0); //number of questions which are graded
		this.addDetail('CC', this.correctCount); //number of correct graded questions
		this.addDetail('CP', this.currentPageIndex); //current page index
		this.addDetail('TS', this.testStarted); //test started
		this.addDetail('JV', this.jeopardyValue); //jeopardy value
		this.addDetail('QL', ''); //questions sent to LMS as interactions
		this.addDetail('CQ', ''); //questions which were answered correctly
		this.addDetail('QA', ''); //question answers

		if (parent.currentSco == 'pretest' || parent.currentSco == 'posttest') {
			switch (this.testType) {
				case TestType.Jeopardy: //jeopardy style
					this.maxCount = 20;
					if (this.questions.length < 20) {
						this.maxCount = this.questions.length;
					}
					this.randomize(this.questions, this.maxCount);
					this.updateStateData('QC', this.maxCount);
					this.populateJeopardyGame();					
					break;
				case TestType.Sequential: //sequential style
					this.maxCount = this.questions.length;
					this.questions.selected = this.questions; //sequential = all questions
					this.addDetail('QS', this.questions.selected.join(";"));
					this.updateStateData('QC', this.maxCount);
					break;
				case TestType.Random: //random selection sequential style
					this.maxCount = 20;
					if (this.questions.length < 20) {
						this.maxCount = this.questions.length;
					}
					this.updateStateData('QC', this.maxCount);
					this.randomize(this.questions, this.maxCount);
					break;
			}
		} else {
			if (CourseTree.completionType == CompletionType.InlineQuestions && parent.currentSco != 'objectives') {
				this.questions.selected = this.questions;
				this.addDetail('QS', this.questions.selected.join(";"));
				this.updateStateData('QC', this.questions.selected.length);
			}
		}
   	},
   	
   	rebuildStateData: function() {
		var dataChunk = API.GetDataChunk();
		if(dataChunk == "") return this.buildStateData();
		var lmsData = dataChunk.split('|');
		for (var i = 0; i < lmsData.length; i++) {
			var nameValuePair = lmsData[i].split(':');
			this.addDetail(nameValuePair[0], nameValuePair[1]);
			if (nameValuePair[0] == 'QS') {
				this.questions.selected = nameValuePair[1].split(';').collect(function(s){ return parseInt(s); });
				this.maxCount = this.questions.selected.length;
			}else if(nameValuePair[0] == 'QA') {
				this.questions.answered = nameValuePair[1].split(';').collect(function(s){ return parseInt(s); });
			}else if(nameValuePair[0] == 'CP'){
				this.currentPageIndex = parseInt(nameValuePair[1]);
			}else if(nameValuePair[0] == 'QC'){
				this.questions.correct = nameValuePair[1].split(';').collect(function(s){ return parseInt(s); });
			}else if(nameValuePair[0] == 'QL'){
				this.questions.sent = nameValuePair[1].split(';').collect(function(s){ return parseInt(s); });
			}else if(nameValuePair[0] == 'JV'){
				this.jeopardyValue = parseInt(nameValuePair[1]);
			}else if(nameValuePair[0] == 'CC'){
				this.correctCount = parseInt(nameValuePair[1]);
			}
		}
   	},
   	
   	populateJeopardyGame: function() {
   		var counter = 0;
   	},
	
	getFromLms: function() {
		var randomTest = false;
		this.questions = CourseTree.findSco(parent.currentSco).getScoredQuestions();
		if (!CourseTree.isPreview) {
			switch (API.GetStatus()) {
				case 1: // passed
				case 2: // completed
				case 3: // failed
					this.rebuildStateData();
					// for any Sco that has been completed, only reset the Sco if there are gradable questions in it. The Pre-Test only allows for one completion.
					/*if (this.questions.length > 0 && parent.currentSco != 'pretest') {
						API.SetDataChunk('');
						API.ResetStatus();
						if (parent.currentSco != 'posttest') {
							API.SetBookmark('');
						} else {
							this.buildStateData();
						}
					}else{
						this.rebuildStateData();
					}*/
					break;
				case 4: // incomplete
					this.rebuildStateData();
					break;
 				case 6: // not attempted
					this.buildStateData();
 					break;
			}
			this.saveToLms();
		}
	},
		
	saveToLms: function() {
		if (!CourseTree.isPreview) {	
			API.SetDataChunk(this.flattenArray());

			if (CourseTree.bookmarkEnabled && parent.currentSco != 'pretest' && parent.currentSco != 'posttest' && parent.currentSco != 'objectives' && this.currentPageIndex > 0) {
				if(parent.currentScoUrl && parent.currentPage)
					API.SetBookmark(parent.currentScoUrl + parent.currentPage + ".html");
			}
		}
	},
	
	flattenArray: function() {
		var data = '';
		for (var i = 0; i < this.stateData.length; i++) {
			if (i > 0) {
				data += '|';
			}
			data += this.stateData[i].id + ':' + this.stateData[i].data;
		}
		return data;
	},
	
	getQuestion: function() {
		parent.currentPage = this.questions.selected[this.currentPageIndex];
		return parent.currentScoUrl + this.questions.selected[this.currentPageIndex] + '.html';
	},
	
	completeSco: function() {
		if(CourseTree.isPreview) return;
		switch(CourseTree.completionType) {
			case CompletionType.FreeFormNav:			
			case CompletionType.SequentialNav:
				parent.SCORM2004_CallSetValue("cmi.completion_status", "completed");
				parent.SCORM2004_CallSetValue("cmi.success_status", "passed");
				parent.SCORM2004_CallCommit();
				break;
			case CompletionType.Test:
				if (parent.currentSco != 'pretest' && parent.currentSco != 'posttest') {
					parent.SCORM2004_CallSetValue("cmi.completion_status", "completed");
					parent.SCORM2004_CallSetValue("cmi.success_status", "passed");
					parent.SCORM2004_CallCommit();
				}
				break;
			case CompletionType.InlineQuestions:
				if (parent.currentSco == 'objectives'){
					parent.SCORM2004_CallSetValue("cmi.completion_status", "completed");
					parent.SCORM2004_CallSetValue("cmi.success_status", "passed");
					parent.SCORM2004_CallCommit();					
				}else if (CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).pageType == 1) {
					if (CourseTree.findSco(parent.currentSco).getScoredQuestionCount() == 0) {
						parent.SCORM2004_CallSetValue("cmi.completion_status", "completed");
						parent.SCORM2004_CallSetValue("cmi.score.raw", 100);
						parent.SCORM2004_CallSetValue("cmi.score.scaled", 1);
						parent.SCORM2004_CallCommit();
					}  else {
						this.completeTest();
					}
				} else {
					this.completeTest();
				}
				break;				
		}
		this.saveToLms();
	},
	
	completeTest: function() {
		if(CourseTree.isPreview) return;
		if (DataChunk.calculateScore() >= DataChunk.findStateData('MS')) {
			parent.SCORM2004_CallSetValue("cmi.completion_status", "completed");
			parent.SCORM2004_CallSetValue("cmi.success_status", "passed");
		}
		var score = this.calculateScore();
		parent.SCORM2004_CallSetValue("cmi.score.raw", score);
		parent.SCORM2004_CallSetValue("cmi.score.scaled", score / 100);
		parent.SCORM2004_CallCommit();
		this.saveToLms();
	},
	
	sendInteractionsToLms: function(answer) {
		var page = CourseTree.findSco(parent.currentSco).findPage(parent.currentPage);
		var answers = CourseTree.findSco(parent.currentSco).findPage(this.findStateDetail(page.id).id).findAnswers();
		var correctAnswers = CourseTree.findSco(parent.currentSco).findPage(page.id).findCorrectAnswers();
		var result = false;
		switch (page.questionType) {
			case QuestionType.MultipleChoice:			
			case QuestionType.AllThatApply:
				result = parent.SCORM2004_RecordMultipleChoiceInteraction(page.id, answers, this.isQuestionCorrect(page.id), correctAnswers, page.questionText, 100 / this.questions.selected.length, 0, '', Date());			
				break;
			case QuestionType.TrueFalse:
				answers = (this.isQuestionCorrect(page.id) == correctAnswers) ? correctAnswers : !correctAnswers;
				result = parent.SCORM2004_RecordTrueFalseInteraction(page.id, answers, this.isQuestionCorrect(page.id), correctAnswers, page.questionText, 100 / this.questions.selected.length, 0, '', Date());
				break;
			case QuestionType.FillInTheBlank:
				result = parent.SCORM2004_RecordFillInInteraction(page.id, answer, this.isQuestionCorrect(page.id), correctAnswers, page.questionText, 100 / this.questions.selected.length, 0, '', Date());
				break;
			case QuestionType.Image:
			
				break;
		}
		if (result) {
			this.addSentQuestion(page.id);
		}
	},
	
	addSentQuestion: function(id) {
		if (this.questions.sent == undefined) {
			this.questions.sent = new Array();
		}
		var value = '';
		this.questions.sent.push(id);
		for (var i = 0; i < this.questions.sent.length; i++) {
			if (value.length > 0) {
				value += ';';
			}
			value += this.questions.sent[i];
		}
		this.updateStateData('QL', value);
	},
	
	addCorrectQuestion: function(id) {
		if (this.questions.correct == undefined) {
			this.questions.correct = new Array();
		}
		var value = ''
		this.questions.correct.push(id);
		for (var i = 0; i < this.questions.correct.length; i++) {
			if (value.length > 0) {
				value += ';';
			}
			value += this.questions.correct[i];
		}
		if (value.length > 0) {
			this.updateStateData('CQ', value);
		}
	},
	
	isQuestionCorrect: function(id) {
		if (this.questions.correct == undefined) {
			return false;
		} else {
			for (var i = 0; i < this.questions.correct.length; i++) {
				if (id == this.questions.correct[i]) {
					return true;
				}
			}
		}
		return false;
	}
}

StateManagerDetail = Class.create();
StateManagerDetail.prototype = {
	initialize: function(id, data) {
		this.id = id;
		this.data = data;
	}
}

CustomMessage = Class.create();
CustomMessage.prototype = {
	initialize: function() {
		this.messages = new Array();
	},
	
	addMessage: function(message) {
		this.messages.push(message);
	},
	
	newMessage: function(id, text) {
		var message = new CustomMessages(id, text);
		this.addMessage(message);
		return message;
	},
	
	findMessage: function(id) {
		for (var i = 0; i < this.messages.length; i++)	{
			var message = this.messages[i];
			if (message.id == id)
				return message.text;
		}
		return null;
	}	
}

CustomMessages = Class.create();
CustomMessages.prototype = {
	initialize: function(id, text) {
		this.id = id;
		this.text = text;
	}
}

//stub out placeholder text values for all generic text messages. This can be edited in future from package tool.
var Messages = new CustomMessage();
objMessage = Messages.newMessage(3, 'Introduction');
objMessage = Messages.newMessage(4, 'This is a Single Question Pre-Test');
objMessage = Messages.newMessage(5, 'This is a Multiple Question Pre-Test');
objMessage = Messages.newMessage(6, 'You have failed a single question pre test. This test was for practice only, and will not affect your final score.');
objMessage = Messages.newMessage(7, 'You have passed a single question pre test. This test was for practice only, and will not affect your final score.');
objMessage = Messages.newMessage(8, 'You have failed a multiple question pre test. This test was for practice only, and will not affect your final score.');
objMessage = Messages.newMessage(9, 'You have passed a multiple question pre test. This test was for practice only, and will not affect your final score.');
objMessage = Messages.newMessage(10, 'You have failed a single question pre test. This course has the test out feature but you must continue.');
objMessage = Messages.newMessage(11, 'You have passed a single question pre test. This course has the test out feature so you are done.');
objMessage = Messages.newMessage(12, 'You have failed a multiple question pre test. This course has the test out feature but you must continue.');
objMessage = Messages.newMessage(13, 'You have passed a multiple question pre test. This course has the test out feature so you are done.');
objMessage = Messages.newMessage(14, 'Introduction');
objMessage = Messages.newMessage(15, 'This is a Single Question Post Test with no Pre-Test');
objMessage = Messages.newMessage(16, 'This is a Multiple Question Post Test with no Pre-Test');
objMessage = Messages.newMessage(17, 'This is a Single Question Post Test with a Pre-Test');
objMessage = Messages.newMessage(18, 'This is a Multiple Question Post Test with a Pre-Test');
objMessage = Messages.newMessage(19, 'You have failed a single question post test. This course is not completed.');
objMessage = Messages.newMessage(20, 'You have successfully completed a single question post test. This course is now completed.');
objMessage = Messages.newMessage(21, 'You have failed a multiple question post test. This course is not completed.');
objMessage = Messages.newMessage(22, 'You have successfully passed a multiple question post test. This course is now completed.');
