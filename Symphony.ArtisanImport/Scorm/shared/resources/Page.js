// Page.js

Page = Class.create({
	//function used to initialize the Page object.
	initialize: function(id, title, pageType, calculateGrade, questionType, questionText) {
		this.id = id;
		this.title = title;
		this.calculateGrade = calculateGrade;
		this.answers = new Array();
		this.pageType = pageType;
		this.questionType = questionType;		
		this.questionText = questionText;
	},	
    
	//function used to add a new Answer object to the Answers array of the Page.
	addAnswer: function(answer) {
		this.answers.push(answer);
	},
    
	//function used to create a new Answer object within the Page.
	newAnswer: function(id, text, isCorrect) {
		var answer = new Answer(id, this.id, text, isCorrect);
		this.addAnswer(answer);
		return answer;
	},
    
	//function used to return a list of Answer objects for the Page.
	// findAnswers: function() {
		// //var answers = DataChunk.findStateManagerItemValue(this.id).data.split(';');
		// var responseIdentifier = new Array();
		// //for (var i = 0; i < answers.length; i++) {
			// for (var j = 0; j < this.answers.length; j++) {
				// var answer = this.answers[j];
				// //if (answer.id == answers[i]) {
					// responseIdentifier.push(parent.CreateResponseIdentifier(this.convertNumberToLetter(j), answer.text));
				// //}
			// }
		// //}
		// return responseIdentifier;
	// },
	
    convertAnswers: function(answersStr){
        var answers = answersStr.split(';');
		var responseIdentifier = new Array();
		for (var i = 0; i < answers.length; i++) {
			for (var j = 0; j < this.answers.length; j++) {
				var answer = this.answers[j];
				if (answer.id == answers[i]) {
                    responseIdentifier.push(parent.CreateResponseIdentifier(this.convertNumberToLetter(j), answer.text));					
				}
			}
		}
		return responseIdentifier;
    },
    
    //function used to return the correct Answers for the Page.
    findCorrectAnswers: function() {
		var responseIdentifier = new Array();
		for (var i = 0; i < this.answers.length; i++) {
			if (this.answers[i].isCorrect) {
				switch (this.questionType) {
					case QuestionType.TrueFalse:
						return true;
						break;
                    case QuestionType.Image:
                        var coords = this.answers[i].text.evalJSON();
                        return "["+coords.x1+","+coords.y1+"]-["+coords.x2+","+coords.y2+"]";
                        break;
                    case QuestionType.FillInTheBlank:
                        responseIdentifier.push(this.answers[i].text);
                        break;
					default:
						responseIdentifier.push(parent.CreateResponseIdentifier(this.convertNumberToLetter(i), this.answers[i].text));					
						break;
				}
			}
		}

		return responseIdentifier;	
	},
    
    findAllAnswers: function(){
        var responseIdentifier = new Array();
		for (var i = 0; i < this.answers.length; i++) {
            switch (this.questionType) {
                case QuestionType.TrueFalse:
                    responseIdentifier.push(this.answers[i].isCorrect);
                    break;
                case QuestionType.Image:
                    var coords = this.answers[i].text.evalJSON();
                    responseIdentifier.push("["+coords.x1+","+coords.y1+"]-["+coords.x2+","+coords.y2+"]");
                    break;
                case QuestionType.FillInTheBlank:
                    responseIdentifier.push(this.answers[i].text);
                    break;
                default:
                    var text = this.answers[i].isCorrect ? this.answers[i].text + "***" : this.answers[i].text ;
                    responseIdentifier.push(parent.CreateResponseIdentifier(this.convertNumberToLetter(i), text));					
                    break;
            }
		}

		return responseIdentifier;	
    },
    
	//function used to convert the Answer index to a letter.
	convertNumberToLetter: function(index) {
		return String.fromCharCode(97 + parseInt(index)); // 0='a', 1='b',.....
	}
});

Answer = Class.create({
	//function used to initialize the Answer object.
	initialize: function(id, pageid, text, isCorrect) {
		this.id = id;
		this.pageid = pageid;
		this.text = text;
		this.isCorrect = isCorrect;
	}
});