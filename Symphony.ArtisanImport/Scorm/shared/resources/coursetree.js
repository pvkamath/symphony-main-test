Ext.namespace('Ext.ux');
Ext.ux.JSONTree = function(config){
	this.options = config;
	this.treeviewShown = false;
	
	this.options = Object.extend({
        async: false,
        items: [],
        idField: "Id",
        parentIDField: "ParentId",
        displayField: "Name",
        ignoreEmptyItems: false
    },this.options);
    this.options.treeOptions = Object.extend({ 
        loader: new Ext.tree.TreeLoader(),
        rootText: "Root",
        rootNodeOptions: { iconCls: "course"}, //options for the root node of the tree
        nodeOptions: { iconCls: "course" }, //options that get passed into the nodes of the tree
        rootVisible: false,
        rootContextMenu: false,
        enableDD: true
    },this.options.treeOptions || {});
	
	Ext.ux.JSONTree.superclass.constructor.call(this, this.options.treeOptions);
        
    //build the root node
    this.root = new Ext.tree.TreeNode(Object.extend({
        text: this.options.treeOptions.rootText || "",
        draggable:false,
        id: 'source'
    }, this.options.treeOptions.rootNodeOptions || {}));

    //attach the root to the tree
    this.setRootNode(this.root);
    
    //build the sub-nodes
    this.loadJSON(this.options.items);
};

Ext.extend(Ext.ux.JSONTree, Ext.tree.TreePanel, {

	clearTree : function(){
	    var children = [];
		this.root.eachChild(function(child) {
		    children.push(child);			    
		}, this);
		children.each(function(child) {
		    this.root.removeChild(child);
		});
	},
	
	//a helper function for re-building with a new set of items
	createTree : function(items){
		var options = this.options;
	    var treeItems = this.buildCourseTreeJSON(items);

	    for(var i = 0; i < treeItems.length; i++)
	        this.root.appendChild(this.getLoader().createNode(treeItems[i]));

	    if(!this.treeViewShown)
	        this.render();
	    //this.root.expand();
	    //if(this.root.firstChild && !options.treeOptions.rootVisible)
	    //    this.root.firstChild.expand();
		this.expandAll();
	},
	
	//allows node creation easily from external functions
	buildNode : function(item, options){
	    var node = this.getLoader().createNode(this.buildNodeJSON(item));
	    Object.extend(node,options || {});
	    return node;
	},
	//attribute is the extended node attribute, property is the property on the attribute, and value is the value of the property
	//nodes should be null initially, or the nodes to search.
	//example would be: findNodeByAttribute("item","Id","5");
	//which will return the node that has an "item" attribute with an "Id" property equal to "5".
	findNodeByAttribute : function(attribute, property, value, nodes){
		if(!nodes) nodes = this.root.childNodes;
		var result = nodes.find(function(node){
			if(node.attributes[attribute] && node.attributes[attribute][property] == value)
				return node;
			else if(node.childNodes != null)
				return this.findNodeByAttribute(attribute, property, value, node.childNodes);
		}.bind(this));
		return result;
	},
	findNodeById : function(id,nodes){
		if(!nodes) nodes = this.root.childNodes;
		var result = nodes.find(function(node){
			if(node.id == id)
				return node;
			else if(node.childNodes != null)
				return this.findNodeById(id, node.childNodes);
		}.bind(this));
		return result;
	},
	buildNodeJSON : function(item){
		var options = this.options;
	    return Object.extend({ id: item[options.idField], text: item[options.displayField], item: item },options.treeOptions.nodeOptions);
	},
	hasObjective : function(){
	    return (CourseTree.scos.any(function(itr){ return itr.id == "objectives"; }));
	},
	hasPreTest : function(){
	    return CourseTree.preTestType > 0;
	},
	hasPostTest : function(){
	    return CourseTree.postTestType > 0;
	},
	loadJSON : function(items){
	    this.clearTree();
	    this.createTree(items);
	    this.fireEvent("treeViewShown", {sender: this }); 
	    this.treeViewShown = true;
	},
	injectElectives : function(tree){	    
	    if(this.hasPreTest())
	        this.injectPreTest(tree);
	    if(this.hasPostTest())
	        this.injectPostTest(tree);
	    if(this.hasObjective())
	       this.injectObjective(tree);
	
	},
	injectPreTest : function(arr){
	    arr.unshift({ title : "Pre Test", id : "pretest", text : "Pre Test", leaf : true, cls: "learningobject", item : { Id : "pretest" }});
	},
	injectPostTest : function(arr){
	    arr.push({ title : "Post Test", id : "posttest", text : "Post Test", leaf : true, cls: "learningobject", item : { Id : "posttest" }});	
	},
	injectObjective : function(arr){
	    arr.unshift({ title : "Objective", id : "objectives", text : "Objectives", leaf : true, cls: "learningobject", item : { Id : "objectives" }});	
	},
	buildCourseTreeJSON : function(courseTreeObj){
        var treeJSON = [];
        var treeObjects = [];
        //ok, we're now gonna loop through the list of courses...
		if (!courseTreeObj.testonly) {
	        if (courseTreeObj.CourseHierarchy.length > 1) {
	            for (var i = 1; i < courseTreeObj.CourseHierarchy.length; i++) {
	                var courseNode = courseTreeObj.CourseHierarchy[i];
	                
	                //if this is a new course...
	                if (!treeObjects.find(function(o){ return o === courseNode; })) {
	                    //create the Ext tree node structure. this is going to be a 1st level node
	                    var node = { id: courseNode.Id, text: courseNode.CourseName, leaf: false, course: courseNode, item: courseNode, allowDrop: true, cls: "course", isTarget: true };
	                    //this is a recursive function, it'll be called with the courseNode and node
	                    (function recurseChildren(seedCourse, seedNode) {
	                        treeObjects.push(seedCourse);
	                        //grab all the sub-courses for this node
	                        var childrenCourses = courseTreeObj.CourseHierarchy.findAll(function(c) {
	                            return c.ParentCourseID == seedCourse.Id;
	                        });
	                        //if we have sub-courses...
	                        if (childrenCourses && childrenCourses.length > 0) {
	                            //not a leaf
	                            seedNode.children = [];
	                            seedNode.leaf = false;
	                            childrenCourses.each(function(childCourse) {
	                                var newCourseNode = { id: childCourse.Id, text: childCourse.CourseName, leaf: true, course: childCourse, item: childCourse, allowDrop: true, cls: "course", isTarget: true };
	                                seedNode.children.push(newCourseNode);
	                                //build the sub-courses
	                                recurseChildren(childCourse, newCourseNode); //recurse 1 level
	                            });
	                        } else if (courseTreeObj.LearningObjectHierarchy) { 
	                            //build the learning object list
	                            var childrenLearningObjects = courseTreeObj.LearningObjectHierarchy.findAll(function(lo) {
	                                return lo.CourseID == seedCourse.Id;
	                            });
	                            
	                            if (childrenLearningObjects && childrenLearningObjects.length > 0) {
	                                childrenLearningObjects = childrenLearningObjects.sortBy(function(clo) {
	                                    return clo.SortOrder;
	                                });
	                                seedNode.children = [];
	                                seedNode.leaf = false;
	                                childrenLearningObjects.each(function(childLO) {                                        
	                                    //ok, we could have children pages too...
	                                    /*var childrenPagesNodes = [];
	                                    var childrenPages = courseTreeObj.PageHierarchy.each(function(p) {
	                                        if(p.LearningObjectID == childLO.Id){
	                                            var disabledPage = true;
	                                            if ( p.PageTypeID == 2 ) { disabledPage = false };
	                                            //todo: swap the icon class to be a content/question page as appropriate
	                                            childrenPagesNodes.push({ id: childLO.CourseID + "-" + childLO.Id + "-" + p.Id, text: p.PageName, leaf: true, page: p, item: p, iconCls: "page-content", disabled: disabledPage });
	                                        }
	                                    });*/
	                                    var newLONode = { id: childLO.Id + "-" + childLO.Id, text: childLO.LearningObjectName, leaf: true, courseLearningObject: childLO, item: childLO, cls: "learningobject", disabled: false };
	                                    seedNode.children.push(newLONode);
	                                });
	                            }else{
	                                //no child learning objects...we still don't want it to act like a leaf though.
	                                seedNode.children = [];
	                                seedNode.leaf = false;
	                            }
	                        }
	                    })(courseNode, node); //kick off the recursion with self call (js is so cool)
	                    treeJSON.push(node);
	                }
	            } //end for each course hierarchy item
	            
	        } else if (courseTreeObj.LearningObjectHierarchy) { //just add the LOs (flat 1 level tree)                            
	            //TODO: query should limit this collection for us, for now (build 5), limiting via script
	            var childrenLearningObjects = courseTreeObj.LearningObjectHierarchy.findAll(function(lo) {
	                return lo.CourseID == courseTreeObj.CourseHierarchy[0].Id;
	            });
	            childrenLearningObjects.each(function(childLO) {
	                var newLONode = { id: childLO.CourseID + "-" + childLO.Id, text: childLO.LearningObjectName, item: childLO, leaf: true, courseLearningObject: childLO, cls: "learningobject" };
	                treeJSON.push(newLONode);
	            });
	        }
		}
		
		if(CourseTree){
	        this.injectElectives(treeJSON);
        }
        return treeJSON;
    }
});