// initialize the Sco's StateManager object.
var DataChunk = new StateManager();
var answers = '';
var numberCorrect = 0;
var questionWin;
var templateClass = 'TemplateHolder';

// if course is not in debug mode, disable the right click functionality.
if (!CourseTree.debugEnabled) {
	document.oncontextmenu = function()
	{
		return false;
	}
}

/************************************************************************************************/

/* Ext.onReady
	This is executed on initial page load and is used to set up the page's
	layout and notes dialog window. It uses ExtJs 2.0 the framework.
*/
Ext.onReady(function(){
	// initialize the ExtJs cookie provider.
	Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

	// create the notes dialog and set its properties assuming notes is available.
	var notesWin;
	if (CourseTree.notesEnabled) {
		var link = Ext.get('NotesLink');
		link.on('click', function() {
			if (!notesWin) {
				notesWin = new Ext.Window({
					el: 'notesWindow',
					modal: true,
					plain: false,
					width: 500,
					height: 250,
					draggable: false,
					resizable: false,
					autoDestroy: true,
					closable: false,
					title: 'Enter Notes',
					
					// define the notes dialog textarea.
					items: new Ext.form.TextArea({
						id: 'Notes',
						name: 'Notes',
						width: 490,
						height: 248,
						maxLengthText: '4000 character limit exceeded.',
						value: '',
						maxLength: 4000
					}),
									
					// define the buttons available within the notes dialog and wire the appropriate functions.
					buttons: [{
						text: 'Save',
						handler: function() {
							SaveNotes();
						}
					}, {
						text: 'Delete',
						handler: function() {
							DeleteNotes();
						}
					}, {
						text: 'Close',
						handler: function() {
							notesWin.hide();
						}
					}]
				});
			}
	
			// wire the notes dialog to call the LoadNotes() function.
			notesWin.on('show', function() {
				LoadNotes();
			});
	
			notesWin.show();
			
		});
	} else {
		$('NotesLink').style.display = 'none';	
	}
	
	var cls = '';
	if(parent.currentSco != 'objectives')
	    cls = templateClass;
	    
	var centerPanel = new Ext.Panel ({
		id:'center',
		text: 'Loading...',
		cls: cls,
		border:false,
		discardUrl:false,
		scripts:true,					
		autoLoad: LoadInitialContent()
	});	
	centerPanel.on('render', function() { 
	    centerPanel.getUpdater().on('update',function(el, transport){
	        FixIEStyles(document, transport.responseText);
		    var placeMarkerFn = QuestionLoadCallback(centerPanel, false, null);
		    DisableAnsweredElements(placeMarkerFn);
	    });	    
	});
	// initialize the page laout into three sections. Set their properties and auto load the content via AJAX.
	var viewport = new Ext.Viewport({
		layout:'border',
		border:false,
		items:[
			new Ext.BoxComponent({ // raw
				region:'north',
				el: 'north',
				height:65,
				border:false
			}),{
				region:'south',
				contentEl: 'south',
				split:false,
				collapsible: false,
				height: 38,
				title:'',
				margins:'0 0 0 0',
				border:false
			}, {
				region:'center',
				items: centerPanel
			}
		]
	});	
});

/************************************************************************************************/

/* LoadNotes()
	This function makes a call to the Lms to load any learner notes for the current Sco.
*/
function LoadNotes() {
	if (!CourseTree.isPreview) {
		$('Notes').value = API.GetComments();
	}
}

/* SaveNotes()
	This function is executed from the notes dialog window and saves the entire
	string back to the Lms for later use.
*/
function SaveNotes() {
	if (!CourseTree.isPreview) {
		// only 4000 characters are allowed. Inform the learner thta some data may not be saved.
		if ($('Notes').value.length > 4000) {
			alert('4000 character limit exceeded. Only the first 4000 characters will be saved.');
		}
		
		if (API.IsLoaded()) {
			API.WriteComment($('Notes').value);
		}
	}
}

/* DeleteNotes()
	This function is executed from the notes dialog window and removes any Lms
	saved learniner comments and clears all text from the dialog.
*/
function DeleteNotes() {
	if (!CourseTree.isPreview) {
		if (API.IsLoaded()) {
			API.WriteComment('');
			$('Notes').value = '';
		}
	}
}

/* SendMail()
	This function is executed when the "Email" button is pressed. The recipient list 
	and subject line are prepopulated based on parameters.
*/
function SendMail() {
	document.location = 'mailto:' + emailAddress + '?subject=Question pertaining to ' + courseName;
}

/* LoadHelp()
	This function is executed when the header "Help" button is pressed. It opens a
	window and displays course help.
*/
function LoadHelp() {
	var dlg = new Ext.Window({
		modal: true,
		width: 500,
		height: 250,
		title: "Help",
		autoScroll: true,
		autoLoad: '../resources/help.html'
	});
	dlg.on("show",function(){
		dlg.setPosition(160,50);
		dlg.setSize(700,590);
	});
	dlg.show();
}

/* ExitCourse()
	This function is executed when the header "Exit" button is pressed. It sends
	control back to the Lms and tells the Lms to close the course window with the
	current status.
*/
function ExitCourse() {
	if (!CourseTree.isPreview) {
		try{
			API.parent.Control.ReturnToLms();
		}catch(e){
			API.ConcedeControl();
		}
	}
}

/************************************************************************************************/

/* LoadInitialContent()
	This is the initial function call executed on initialization of the page. It calls
	functions which set page properties based on parameters, and loads the initial content.
*/
function LoadInitialContent() {
	if (!CourseTree.isPreview) {
		// check to see if bookmarking is enabled.
		if (CourseTree.bookmarkEnabled) {
			// bookmarking is enabled, but there is no bookmark stored. Load the first page of the Sco.
			if (API.GetBookmark().length == 0) {
				LoadSco();
			// bookmarking is enabled and a bookmark is returned. Load the bookmarked page.
			} else {
				ParseBookmark();
			}
		// no bookmarking is enabled, load the first page on the Sco always.
		} else {
			LoadSco();
		}
	} else {
		$('navigationBack').hide();
		$('ExitLink').hide();
		LoadSco();
	}
	Cleanup();
	SetSharedContent();	
	return parent.currentScoUrl + parent.currentPage + ".html";	
}

function Cleanup(){
	if($('markerDiv')){ $('markerDiv').remove(); }
	if($('areaDiv')){ $('areaDiv').remove(); }
}

function DisableAnsweredElements(placeMarkerFunction) {	
	// disable the next and back buttons if this is a question page
	if (!DataChunk.isCompleted(parent.currentPage) && CourseTree.findSco(parent.currentSco).getCurrentPage().answers.length > 0) {
	    $('navigationNext').hide();
	    $('navigationBack').hide();
	}else{
		$('navigationNext').show();
		if (!isTestSco()) {
			$('navigationBack').show();
		}
	}
	if (isTestSco()) {
		var prepend = '';
		var pretest = '';
		var questions = '';
		var passed = '';
		testout = '';
		if (parent.currentSco == 'posttest') {
			if (CourseTree.hasPreTest()) {
				pretest = 'Pre';
			} else {
				pretest = 'NoPre';
			}
		} else {
			if (CourseTree.testOutEnabled) {
				testout = 'Test';
			} else {
				testout = 'NoTest';
			}
		}
		prepend = (parent.currentSco == 'pretest') ? 'PreTest' : 'PostTest';
		questions = (CourseTree.findSco(parent.currentSco).getScoredQuestionCount() == 1) ? 'Single' : 'Multiple';
		
		if (parent.currentPage == 'test') {
			$('introHeader').innerHTML = Messages.findMessage(MessageType[prepend + 'IntroHeader']);
			$('introText').innerHTML = Messages.findMessage(MessageType[prepend + 'Intro' + questions + pretest]);
		    $('navigationNext').hide();
		} else if (parent.currentPage == 'review') {
			passed = (DataChunk.calculateScore() >= DataChunk.findStateData('MS')) ? 'Passed' : 'Failed';
			$('reviewText').innerHTML = Messages.findMessage(MessageType[prepend + passed + questions + testout]);

			//display certificate link
			if ( (parent.currentSco == 'posttest' && passed == 'Passed') || (parent.currentSco == 'pretest' && passed == 'Passed' && testout == 'Test')) {
				$('certificate').show();
			} else {
				$('certificate').hide();
			}

			//set the score for the test.
			var scoreUS = parseInt(DataChunk.calculateScore());
			var scorePS = parseInt(DataChunk.masteryScore);
			if(!scoreUS) scoreUS = 0;

			var len = 3;
			var scoreStringUS = scoreUS.toPaddedString(len);
			var scoreStringPS = scorePS.toPaddedString(len);
			for(var i = 0; i < len; i++){
				var revStringUS = 'url(../resources/images/review.' + i + '.' + scoreStringUS.charAt(i) + '.gif)';
				var revStringPS = 'url(../resources/images/review.' + i + '.' + scoreStringPS.charAt(i) + '.gif)';
				$('reviewus' + i).style.background = revStringUS;
				$('reviewps' + i).style.background = revStringPS;
			}
			$('navigationNext').show();
		}
	}
	
    var currentPage = CourseTree.findSco(parent.currentSco).getCurrentPage();
    var state = [];
    if (!CourseTree.isPreview){
        data = DataChunk.findStateData(parent.currentPage);
        if(data)
            state = data.split(';');
    }
    if (DataChunk.isCompleted(parent.currentPage) || CourseTree.isPreview) {
        var arr = Element.select('ul','.answerElToDisable');
        arr.each( function( el , index ) {
            if(!CourseTree.isPreview)
    	        el.disable();
    	        
            if (currentPage.answers[index] && el.name.toLowerCase() != 'answersubmitbtn') {
        		for (var i = 0; i < state.length; i++) {
        			if (currentPage.answers[index].id == state[i]) {
        				el.checked = el.value = state[i];
        			}
        		}
            }
        });
        //HACK: this will detect if the user has 2 entries for an answer, which is only the case for an image question
        if(currentPage.answers && currentPage.answers.length > 0 && currentPage.answers[0].userAnswer && currentPage.answers[0].userAnswer.length == 2 && placeMarkerFunction){
            placeMarkerFunction(currentPage.answers[0].userAnswer[0],currentPage.answers[0].userAnswer[1]);
        }
		
		var incorrectfeedback = "incorrectAnswer";
	    var correctFeedback = "correctAnswer";
		if(DataChunk.isQuestionCorrect(parent.currentPage) && isTestSco()){
			DisplayFeedback(correctFeedback);
		}else{
			DisplayFeedback(incorrectfeedback);
		}
    }
}
function DisplayFeedback( feedback ) {
    HideAllFeedback();
    if ( CourseTree.feedbackEnabled) {
	var el = $( feedback + "FeedbackDiv" );
	el.up().setStyle({position:'relative'});
	if(isPreTest()){
		el.innerHTML = (feedback == 'correctAnswer' ? 'Correct' : 'Incorrect');
	}
        el.show();
    }   
}

function HideAllFeedback() {
    $("correctAnswerFeedbackDiv").hide();
    $("incorrectAnswerFeedbackDiv").hide();
}

function CheckAnswer( btn , list , choices ) {
    if (!CourseTree.isPreview && $(btn).disable) $(btn).disable();
    var feedback = "incorrectAnswer";
    var correctFeedback = "correctAnswer";
    answers = '';
    //determine question type
    var IsCorrect;
    switch (list) {
        case "multipleChoiceList":
        case "trueFalseList":
        case "allThatApplyList":
            IsCorrect = function(pageAnswer, answer) {  
                if (answer.checked) {
                	if (answers.length > 0) {
						answers += ';' + pageAnswer.id;
					} else {
						answers += pageAnswer.id;				
					}             		
                }
                return pageAnswer.isCorrect == answer.checked;
            };
            break;
        case "fillInTheBlankList":
            IsCorrect = function(pageAnswer,answer) {
            	if (answers.length > 0) {
            		answers += ';' + pageAnswer.text;
            	} else {
            		answers += pageAnswer.text;
            	}
                return pageAnswer.text.toLowerCase().strip() == answer.value.toLowerCase().strip();
            };
            break;
        case "flashList":
            IsCorrect = function(pageAnswer,answer) {
                pageAnswer.userAnswer = answer.value;
                return pageAnswer == answer.value;
            };            
            break;
        case "hotspotList":
        	//update StateManager if record does not already exist
			if (!DataChunk.isCompleted(parent.currentPage)) {
				DataChunk.setComplete(parent.currentPage, answers);
			}
	        $('navigationNext').show();
	        $('navigationBack').show();
            IsCorrect = (GetCurrentPage().answers[0].isCorrect && (GetCurrentPage().answers[0].userAnswer != null)); // ? correctFeedback : feedback; //image questions are managed live
            break;
    }  
    
    //confirm if answers are correct
    var currentPage = GetCurrentPage();
    var n = 0;
    var answersArr = [];
    if($(list)){
        answersArr = $(list).getElementsByClassName(choices);
	    if(answersArr){
            answersArr.each( function( answerEl , index ) {
                var answ = currentPage.answers[index]
                if (!CourseTree.isPreview) answerEl.disable();
                if ( IsCorrect( answ , answerEl)  ) {
                    n = n + 1;
                    PlaceTick(answerEl,list,answ,true);
                }else{
                    PlaceTick(answerEl,list,answ,false);
                }
            });
	    }
    }else{
		//put a div around the correct area here...
		if(!isPreTest()){
			var coords = currentPage.answers[0].text.evalJSON(true);
			var div = document.createElement('div');
			div.id = 'areaDiv';
			//offsets account for margin around the image container
			var xOffset = -5;
			var yOffset = -5;
			$$('.imagequestiontemplate')[0].appendChild(div);
			div = $(div.id);
			div.setStyle({
			    position:'absolute',
			    top: (coords.y1 + yOffset) + 'px',
			    left: (coords.x1 + xOffset) + 'px',
			    width: (coords.x2 - coords.x1) + 'px',
			    height: (coords.y2-coords.y1) + 'px',
			    border: '2px dashed ' + (IsCorrect === true ? 'green' : 'red') 
			});
		}
    }
    var responseCorrect = false;
    if ( (n == answersArr.length && answersArr.length == currentPage.answers.length) || (list == "hotspotList" && IsCorrect === true) ) {
        feedback = correctFeedback;
		numberCorrect ++;
		if (CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).calculateGrade) {
			DataChunk.correctCount++;
			DataChunk.updateStateData('CC', DataChunk.correctCount);
			DataChunk.addCorrectQuestion(currentPage.id);
			responseCorrect = true;		
		}
    }
    
    if (DataChunk.testType == TestType.Jeopardy) {
    	DataChunk.calculateJeopardyValue(responseCorrect);
		ShowJeopardyScore(DataChunk.jeopardyValue);
    }
    
    //update StateManager if record does not already exist    
    var tempAnswers = '';
    tempAnswers = (CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).questionType == QuestionType.FillInTheBlank) ? '1' : answers;
    
    if (isTestSco()) {
    	if (!DataChunk.isCompleted(DataChunk.questions.selected[DataChunk.currentPageIndex])) {
    		DataChunk.setComplete(DataChunk.questions.selected[DataChunk.currentPageIndex], tempAnswers);    		
    	}
		if (DataChunk.testType == TestType.Jeopardy) {
			$('jeopardyCloseButton').enabled = true;
			$('jeopardyButton' + DataChunk.currentPageIndex).hide();
			$('jeopardyButtonOff' + DataChunk.currentPageIndex).show();	
		}    	
    } else {
    	if (!DataChunk.isCompleted(parent.currentPage)) {
			DataChunk.setComplete(parent.currentPage, tempAnswers);
		}
    }
    
    //send the interaction to the LMS
    if (isTestSco()) {
    	DataChunk.sendInteractionsToLms(answers);
    	if (DataChunk.testType != TestType.Jeopardy) {
			if (DataChunk.questions.answered.length < DataChunk.maxCount) {
				$('navigationNext').show();
			} else {
				if (!CourseTree.isPreview) {
					DataChunk.completeTest();
				}
				$('pageNumbers').hide();
				$('noPageNumber').show();
				Ext.Msg.show({
					title: 'Complete',
					msg: 'Continue to review',
					buttons: Ext.Msg.OK,
					fn: LoadReview
				});
			}			
		} else {
			if (!CourseTree.isPreview) {
				if (DataChunk.questions.answered.length == DataChunk.questions.selected.length) {
					DataChunk.completeTest();
				}				
			}
		}   	
    } else {
    	if (CourseTree.completionType == CompletionType.InlineQuestions && CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).pageType == 2 && CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).calculateGrade) {
    		DataChunk.sendInteractionsToLms(answers);
    	}
    	if (!CourseTree.isPreview) {
			$('navigationNext').show();
		    	$('navigationBack').show();
		    
			if (CourseTree.findSco(parent.currentSco).isLastPage(parent.currentPage) && CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).pageType == 2) {
				DataChunk.completeSco();
			}
    	} else {
    		if (DataChunk.currentPageIndex == 0) {
    			$('navigationBack').hide();
    		} else {
    			$('navigationBack').show();
    		}
			if (CourseTree.findSco(parent.currentSco).isLastPage(parent.currentPage)) {
				$('navigationNext').hide();
			} else {
				$('navigationNext').show();
			}    		
    	}   
    }
    DisableAnsweredElements();
    DataChunk.saveToLms();
    return feedback;
}

function ShowJeopardyScore(score){
	var tempVal = score;
	if (tempVal < 0) {
		tempVal = tempVal * -1;
	}
	if (DataChunk.jeopardyValue < 0) {
		$('jeopardyTotalMinus').style.background = 'url(../resources/images/jeopardy.total.minus.on.gif)';
	} else {
		$('jeopardyTotalMinus').style.background = 'url(../resources/images/jeopardy.total.minus.off.gif)';
	}
	if (DataChunk.jeopardyValue > -1000 && DataChunk.jeopardyValue < 1000) {
		$('jeopardyTotalThousand').style.background = 'url(../resources/images/jeopardy.total.0k.gif)';
	} else {
		$('jeopardyTotalThousand').style.background = 'url(../resources/images/jeopardy.total.' + tempVal.toString().substring(0,1) +'k.gif)';
	}
	if (tempVal.toString().length == 3) {
    	$('jeopardyTotalHundred').style.background = 'url(../resources/images/jeopardy.total.' + tempVal.toString().substring(0) +'.gif)';
    } else if (tempVal.toString().length > 3) {
        $('jeopardyTotalHundred').style.background = 'url(../resources/images/jeopardy.total.' + tempVal.toString().substring(1) +'.gif)';
    } else {
    	$('jeopardyTotalHundred').style.background = 'url(../resources/images/jeopardy.total.0.gif)';
    }
}

function PlaceTick(el, questionList, answer, gotItRight){
    if(isPreTest()) return;

    var cls = 'incorrectAnswerIndicator'; //default to incorrect
    var answered = $F(el);
    if(isPreTest()){
	//if it's a pre-test, only ever mark an answered value
	if(answered && answer.isCorrect && gotItRight){
		//they should have selected this, and they did
		cls = 'correctAnswerIndicator';
	}else if(answered && !answer.isCorrect && !gotItRight){
		//they should NOT have selected this, and they did
		cls = 'incorrectAnswerIndicator';
	}else{
		cls = '';
	}
    }else{
	if(gotItRight){
        	cls = 'correctAnswerIndicator'; //if they got it right
	}else if(answer.isCorrect){
        	cls = 'correctButGotItWrong'; //they got it wrong, and it should have been selected
    	}
    }
    if(cls){
      var parentEl = el.up().select('.' + cls)
      if(parentEl && parentEl.length == 0){
        var span = document.createElement('span');
	if(answered) cls += ' answered'; //add an 'answered' class if the element has a value
        span.className = cls;
        span.innerHTML = '&nbsp;';
	el.up().appendChild(span);
	if(!gotItRight && questionList == 'fillInTheBlankList'){
		span.innerHTML += answer.text; //put the right answer in the span if its a fill in the blank
	}
      }
    }
}

/* SetSharedContent()
	This function is called from the LoadInitialContent() function and is used to set common
	attributes within the course based on global parameters and the requested Sco.
*/
function SetSharedContent() {
	// set the display of the header "Email" link.
	if (emailAddress.length == 0) {
		$('EmailLink').style.display = 'none';
	}

	// set the display of the header "Help" link.
	if (!CourseTree.helpEnabled) {
		$('HelpLink').style.display = 'none';
	}

	// update the header with the name of the current Sco.
	if (CourseTree.findSco(parent.currentSco) == null) {
		alert('Cannot locate requested content.');
	} else {
		Ext.get('courseName').update(CourseTree.findSco(parent.currentSco).title);
	}
	
	if (isTestSco()) {
		if (!DataChunk.testStarted) {
			$('pageNumbers').hide();
		}
		$('navigationBack').hide();
		$('navigationNext').hide();

	} else {
		// set the footer current page and page count values.
		$('noPageNumber').hide();
	}
	
	CourseTree.findSco(parent.currentSco).setCurrentPage();
	CourseTree.findSco(parent.currentSco).setPageCount();
}

/* LoadSco()
	This function is called from the LoadInitialContent() function when no bookmark is 
	set within the Lms. It loads the first page in the requested Sco.
*/
function LoadSco() {
	var sco = CourseTree.findSco(parent.currentSco);
	if (sco.pages.length > 0) {
		parent.currentSco = sco.id;
		parent.currentScoUrl = sco.url;
		parent.currentPage = sco.pages[0].id;
		if (CourseTree.findSco(parent.currentSco).isLastPage(parent.currentPage)) {
			CompleteSco();
		}
	} else {
		alert('No pages exist.');
	}
}

/* ParseBookmark()
	This function is makes an API call to retrieve any saved bookmark data from the LMS.
	If no page is found for the bookmark, the first page in the requested Sco is loaded.
*/
function ParseBookmark() {
	var bookmark = API.GetBookmark();
	var pos = bookmark.lastIndexOf('/');
	var bookmarkId = bookmark.substring(pos + 1);
	parent.currentPage = bookmarkId.substring(0, bookmarkId.length - 5);
	bookmark = bookmark.substring(0, pos + 1);
	parent.currentScoUrl = bookmark;
	bookmark = bookmark.substring(0, bookmark.length - 1);
	pos = bookmark.lastIndexOf('/');
	parent.currentSco = bookmark.substring(pos + 1);
	
	if (CourseTree.findSco(parent.currentSco).findPage(parent.currentPage) == null && parent.currentPage != 'review') {
		//if no page exists based on the bookmark for the current Sco, default to the first page.
		parent.currentPage = CourseTree.findSco(parent.currentSco).pages[0].id;
		DataChunk.updatePageIndex(0);
	}
}

/************************************************************************************************/

/* NavigateBack()
	This function is wired to the footer "Back" button and is used to update the current 
	page index to the previous page in the Sco. If the Sco is already on the first page, 
	the GoToPreviousSco() function is called which sends control back to the Lms to go 
	to the previous Sco in the course.
*/
function NavigateBack() {
	//update StateManager if record does not already exist
	if (!DataChunk.isCompleted(parent.currentPage)) {
		DataChunk.setComplete(parent.currentPage, '');
	}
	if (DataChunk.currentPageIndex != 0) {
		DataChunk.updatePageIndex(DataChunk.currentPageIndex - 1);	
		Navigate();
	} else {
		//Call the functions to go to the previous Sco and complete the current Sco	
		GoToPreviousSco();
	}
}

/* NavigateNext()
	This function is wired to the footer "Next" button and is used to update the current
	page index to the next page in the Sco. If the Sco is already on the last page,
	the GoToNextSco() function is called which sends control back to the Lms to go to the
	next Sco in the course.
*/
function NavigateNext() {
	//update StateManager if record does not already exist
	if (!DataChunk.isCompleted(parent.currentPage)) {
		DataChunk.setComplete(parent.currentPage, '');
	}
	if (!isTestSco()) {	
	    if (DataChunk.currentPageIndex < CourseTree.findSco(parent.currentSco).pages.length - 1) {
			DataChunk.updatePageIndex(DataChunk.currentPageIndex + 1);
			Navigate();
		} else {
			GoToNextSco();
		}
	} else {
		if (DataChunk.questions.answered.length == DataChunk.questions.selected.length) {
			LoadReview();
		}else if(parent.currentPage == 'review'){
			GoToNextSco();
		}
		else {
			DataChunk.updatePageIndex(DataChunk.currentPageIndex + 1);
			$('navigationNext').hide();
			parent.currentPage = CourseTree.findSco(parent.currentSco).getCurrentPage().id;
			CourseTree.findSco(parent.currentSco).setCurrentPage();
			Navigate();
		}
	}
}

function FixIEStyles(el, responseText){
    //hack to force IE to render embedded style tags
    var isIE = /*@cc_on!@*/false;
    if(isIE){
        var html = responseText;
        var styleTags = /<style[^>]*>([\s\S]*?)<\/style>/gi;
        var styleMatch = null;
        //get the style tags
        var styleText = "";
        while(styleMatch = styleTags.exec(html)) {
            styleText += (styleMatch[1] + "\n");
        }
        //and create a "real" style tag so IE recognizes it
        var styleTag = document.createElement("style");
        styleTag.setAttribute("type", "text/css");
        styleTag.styleSheet.cssText = styleText;
        el.insertBefore(styleTag, el.firstChild);
    }
}

/* Navigate()
	This function is called from the NavigatePrevious() and NavigateNext() functions and
	is used to load the content of the new page and update any appropriate Lms values.
*/
function Navigate() {
	answers = '';
	// load the currrent page and update the footer's current page number with the proper value.
	parent.currentPage = CourseTree.findSco(parent.currentSco).getCurrentPage().id;
	CourseTree.findSco(parent.currentSco).setCurrentPage();
		
	if (CourseTree.isPreview) {
		if (CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).pageType == 1 || DataChunk.isCompleted(parent.currentPage)) {
			if (CourseTree.findSco(parent.currentSco).pages.length == 1) {
				$('navigationBack').hide();
				$('navigationNext').hide();			
			} else if (DataChunk.currentPageIndex == 0 && CourseTree.findSco(parent.currentSco).pages.length > 1) {
				$('navigationBack').hide();
				$('navigationNext').show();
			} else if (CourseTree.findSco(parent.currentSco).isLastPage(parent.currentPage) && CourseTree.findSco(parent.currentSco).pages.length > 1) {
				$('navigationBack').show();
				$('navigationNext').hide();
			} else {
				$('navigationBack').show();
				$('navigationNext').show();			
			}
		}
	} else {
		DataChunk.saveToLms();
		if (CourseTree.findSco(parent.currentSco).isLastPage(parent.currentPage) && CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).pageType == 1) {
			CompleteSco();
		}
	}
		
	if (isSpecialSco()) {
		DisplayTestQuestion();
	} else {
		LoadContent();		
	}
}

function LoadContent() {
	// update the center panel with the new page content via AJAX.
	var panel = Ext.get('center');	
	panel.load({
		url: parent.currentScoUrl + parent.currentPage + ".html",
		callback: function(panel, success, transport){
	        FixIEStyles(panel.dom, transport.responseText);
			//i hate putting template specific code in here, but this is the only way it would trigger at the right time,
			//and it needs to track "correct" and "incorrect" answers a little differently. crap. oh well.
			//this is the code that places a block in the image question wherever they click.
			var placeMarkerFn = QuestionLoadCallback(panel, success, transport);
			DisableAnsweredElements(placeMarkerFn);
			Cleanup();
		}
	});
}

/* GoToPreviousSco()
	This function is called from the NavigateBack() function and is used to send control
	back to the Lms in order to navigate to the previous Sco in the course.
*/
function GoToPreviousSco() {
	if (!CourseTree.isPreview) {
		// make the adl SCORM 2004 call to the Lms for navigation to the previous Sco.
		parent.SCORM2004_CallSetValue('adl.nav.request', 'previous');
		// send control back to the Lms.
		API.Unload();
	}
}

/* GoToNextSco()
	This function is called from the NavigateNext() function and is used to send control
	back to the Lms in order to navigate to the next Sco in the coure.
*/
function GoToNextSco() {
	if (!CourseTree.isPreview) {
		// make the adl SCORM 2004 call to the Lms for navigation to the next Sco.
		parent.SCORM2004_CallSetValue('adl.nav.request', 'continue');
		// send control back to the Lms.
		API.Unload();
	}
}

/* CompleteSco()
	This function is called from the Navigate() function and is used to set the completion
	method for the current Sco based on the overall course's completionType.
*/
function CompleteSco() {
	if (!CourseTree.isPreview) {
		if (API.IsLoaded()) {
			DataChunk.completeSco();
		} else {
			alert('An error occurred communicating with the Lms.');
		}
	}
}

function QuestionLoadCallback(questionData, success, transport){
    //i hate putting template specific code in here, but this is the only way it would trigger at the right time,
    //and it needs to track "correct" and "incorrect" answers a little differently. crap. oh well.
    //this is the code that places a block in the image question wherever they click.
    var placeMarker;
			    
    if($('imageAnswerContainer')){
        var getAndShowMarker = function(evt){
	        var x = Event.pointerX(evt);
	        var y = Event.pointerY(evt);
	        placeMarker(x,y);
	        Event.stop(evt);
        }
		
	   	var dim = 8; //the height/width of the square
        placeMarker = function(x,y){
			if(!$('markerDiv')){
        		var div = document.createElement('div');
		        div.id = 'markerDiv';
		        div.style.position = 'absolute';
				div.style.zIndex = 99999999999;
	        	div.style.height = dim + 'px';
	        	div.style.width =  dim + 'px';
		        div.style.backgroundColor = 'white';
		        div.style.display = 'none';
		        document.body.appendChild(div);
			}
			var div = $('markerDiv');
	        	div.style.left = (x - ( dim / 2)) + 'px';
		        div.style.top = (y - ( dim / 2)) + 'px';
		        div.style.display = 'block';

		        //DataChunk.findStatePage(id)
		        GetCurrentPage().answers[0].userAnswer = [x,y];
	    }
		
        var img = $('answerContainer').getElementsBySelector('img')[0];
        var map = $('answerContainer').getElementsBySelector('map')[0];
    	Event.observe(img,'click',function(evt){
            if(!CourseTree.isPreview && DataChunk.isCompleted(parent.currentPage).isCompleted) return;
        	getAndShowMarker(evt);
			GetCurrentPage().answers[0].isCorrect = false;
    	});
    	Event.observe(map,'click',function(evt){
            if(!CourseTree.isPreview && DataChunk.isCompleted(parent.currentPage).isCompleted) return;
	        getAndShowMarker(evt);
			GetCurrentPage().answers[0].isCorrect = true;
    	});
    }else{
		Cleanup();
    }
	return placeMarker;
}

function GetCurrentPage(){
    if (parent.currentSco == 'pretest' || parent.currentSco == 'posttest' || parent.currentSco == 'objectives') {
		return CourseTree.findSco(parent.currentSco).findPage(parent.currentPage);
	} else {
		return CourseTree.findSco(parent.currentSco).getCurrentPage();
	}
}

function isTestSco() {
	return (parent.currentSco == 'pretest' || parent.currentSco == 'posttest') ? true : false;
}

function isPreTest(){
	return (parent.currentSco == 'pretest');
}

function isSpecialSco() {
	return (parent.currentSco == 'pretest' || parent.currentSco == 'posttest' || parent.currentSco == 'objectives') ? true : false;
}

function DisplayTestQuestion() {
	var questionData = Ext.get('testQuestion');	
	questionData.load({
		url: DataChunk.getQuestion(),
		callback: function(questionData, success, transport){
		    FixIEStyles(questionData.dom, transport.responseText);
		    var placeMarkerFn = QuestionLoadCallback(questionData, success, transport);
		    DisableAnsweredElements(placeMarkerFn);
		}
	});
}

function DisplayJeopardyGame() {
	if (!DataChunk.testStarted) {
		$('introduction').hide();
		DataChunk.testStarted = true;
		DataChunk.updateStateData('TS', DataChunk.testStarted);
		$('jeopardyGame').show();
		var counter = 0;
   		for (var i = 0; i < 20; i++) { //loop through the columns
			if (counter < DataChunk.questions.selected.length) {   		
				var btnOn = 'jeopardyButton' + i;
				var btnOff = 'jeopardyButtonOff' + i;
				if(DataChunk.isCompleted(DataChunk.questions.selected[i])){
	   				$(btnOff).show();
				}else{
					$(btnOn).show();
				}
   				counter++;
   			}
   		}
		
		ShowJeopardyScore(DataChunk.jeopardyValue);
	}
}

function BeginSequentialTest() {
	if (!DataChunk.testStarted) {
		$('introduction').hide();
		DataChunk.testStarted = true;
		DataChunk.updateStateData('TS', DataChunk.testStarted);
	}
	
	$('pageNumbers').show();
	$('noPageNumber').hide();
	
	DisplayTestQuestion();
}

function DisplayJeopardyQuestion(element, position) {
	if (!questionWin) {
		questionWin = new Ext.Window({
			el: 'questionWindow',
			modal: true,
			plain: false,
			draggable: true,
			cls: templateClass,
			autoHeight:true,
			width:701,
			resizable: true,
			autoDestroy: true,
			closable: false,
			title: 'Answer The Following Question',			
			buttons: [{
				text: 'Close',
				id:'jeopardyCloseButton',
				disabled: false,
				handler: function() {
					questionWin.hide();
					if($('markerDiv')){ $('markerDiv').remove(); }
					if (DataChunk.questions.answered != null && DataChunk.questions.answered.length == DataChunk.questions.selected.length) {
				    	parent.currentPage = 'review';
					    Ext.Msg.show({
					    	title: 'Complete',
					    	msg: 'Continue to review',
					    	buttons: Ext.Msg.OK,
					    	fn: LoadReview
					    });		
					}
				}
			}]
			
		});
		questionWin.on("show",function(){
			questionWin.setPosition(160,50);
			questionWin.setSize(700,590);
		});
	}
	DataChunk.currentPageIndex = position;			
	questionWin.show();
	DisplayTestQuestion();
}

function LoadReview() {
	parent.currentPage = 'review';
	API.SetBookmark(parent.currentScoUrl + parent.currentPage + ".html");	
	LoadContent();
	CompleteSco();
}

function DisplayCertificate() {
	//params needed. StudentName, CourseName, CompletionMethod, Score
	//do the window.open stuff
	var w = (790);
	var h = (564);
	var t = ((screen.height - h) / 2);
	var l = ((screen.width - w) / 2);
	var compType = '';
	switch(CourseTree.completionType){
		case CompletionType.FreeFormNav:
		case CompletionType.SequentialNav:
			compType = 'Navigation Completed';
		case CompletionType.Test:
			compType = 'Test Completed';
		case CompletionType.InlineQuestions:
			compType = 'Inline Test Completed';
	}
	compType = escape(compType);
	window.open('../resources/printcert.html?score='+DataChunk.calculateScore()+'&name='+studentName+'&course='+CourseTree.title+'&type='+compType,'CertificateWindow','height='+h+',width='+w+',top='+t+',left='+l+',toolbar=no,menubar=no,status=no,location=no,scrollbars=yes,resizable=yes');	
}