// initialize the Sco's StateManager object.
var DataChunkFactory = new StateManagerFactory();
var DataChunk = DataChunkFactory.loadFactory();
var templateClass = 'TemplateHolder';
window.CourseTree = CourseTree;
var continueText = "You have completed the test. Click \"OK\" to continue to the review page.";

// if course is not in debug mode, disable the right click functionality.
if (!CourseTree.debugEnabled) {
	document.oncontextmenu = function()	{ return false; }
}

Ext.onReady(function(){
	// initialize the ExtJs cookie provider.
	Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

	// create the notes dialog and set its properties assuming notes is available.
	var notesWin;
	if (CourseTree.notesEnabled) {
		var link = Ext.get('NotesLink');
		link.on('click', function() {
			if (!notesWin) {
				notesWin = new Ext.Window({
					el: 'notesWindow',
					modal: true,
					plain: false,
					width: 500,
					height: 250,
					draggable: false,
					resizable: false,
					autoDestroy: true,
					closable: false,
					title: 'Enter Notes',
					
					// define the notes dialog textarea.
					items: new Ext.form.TextArea({
						id: 'Notes',
						name: 'Notes',
						width: 490,
						height: 248,
						maxLengthText: '4000 character limit exceeded.',
						value: '',
						maxLength: 4000
					}),
									
					// define the buttons available within the notes dialog and wire the appropriate functions.
					buttons: [{
						text: 'Save',
						handler: function() {
							SaveNotes();
						}
					}, {
						text: 'Delete',
						handler: function() {
							DeleteNotes();
						}
					}, {
						text: 'Close',
						handler: function() {
							notesWin.hide();
						}
					}]
				});
			}
	
			// wire the notes dialog to call the LoadNotes() function.
			notesWin.on('show', function() {
				LoadNotes();
			});
	
			notesWin.show();
			
		});
	} else {
		if (!$('NotesLink')){
			if (CourseTree.debugEnabled){
				throw new Error("$('NotesLink') has no properties (probably due to an unload/reload)");
			}
			return;
		}
		$('NotesLink').style.display = 'none';	
	}

	//LoadContent();
	var centerPanel = new Ext.Panel ({
		id:'centerInner',
		text: 'Loading...',
		cls: templateClass,
		border:false,
		discardUrl:false,
		scripts:true,
		listeners: {
			render: function(){
				LoadContent();
			}
		}		
	});
	centerPanel.render('center');
});

/************************************************************************************************/

/* LoadNotes()
	This function makes a call to the Lms to load any learner notes for the current Sco.
*/
function LoadNotes() {
	if (!CourseTree.isPreview) {
		$('Notes').value = API.GetComments();
	}
}

/* SaveNotes()
	This function is executed from the notes dialog window and saves the entire
	string back to the Lms for later use.
*/
function SaveNotes() {
	if (!CourseTree.isPreview) {
		// only 4000 characters are allowed. Inform the learner thta some data may not be saved.
		if ($F('Notes').length > 4000) {
			alert('4000 character limit exceeded. Only the first 4000 characters will be saved.');
		}
		
		if (API.IsLoaded()) {
			API.WriteComment($F('Notes'));
		}
	}
}

/* DeleteNotes()
	This function is executed from the notes dialog window and removes any Lms
	saved learniner comments and clears all text from the dialog.
*/
function DeleteNotes() {
	if (!CourseTree.isPreview) {
		if (API.IsLoaded()) {
			API.WriteComment('');
			$('Notes').value = '';
		}
	}
}

/* SendMail()
	This function is executed when the "Email" button is pressed. The recipient list 
	and subject line are prepopulated based on parameters.
*/
function SendMail() {
	document.location = 'mailto:' + emailAddress + '?subject=Question pertaining to ' + courseName;
}

/* LoadHelp()
	This function is executed when the header "Help" button is pressed. It opens a
	window and displays course help.
*/
function LoadHelp() {
	var dlg = new Ext.Window({
		modal: true,
		width: 500,
		height: 250,
		title: "Help",
		autoScroll: true,
		autoLoad: '../resources/help.html'
	});
	dlg.on("show",function(){
		dlg.setPosition(160,50);
		dlg.setSize(700,590);
	});
	dlg.show();
}

/* ExitCourse()
	This function is executed when the header "Exit" button is pressed. It sends
	control back to the Lms and tells the Lms to close the course window with the
	current status.
*/
function ExitCourse() {
	if (!CourseTree.isPreview) {
		try{
			API.parent.Control.ReturnToLms();
		}catch(e){
			API.ConcedeControl();
		}
	}
}

function Cleanup(){
	if ($('markerDiv')) {
		$('markerDiv').remove();
	}
	if ($('areaDiv')) {
		$('areaDiv').remove();
	}
}

function DisplayTestScore(){
	//set the score for the test.
	var scoreUS = parseInt(Math.round(DataChunk.calculateScore()));
	var scorePS = parseInt(Math.round(DataChunk.getMasteryScore()));
	if (CourseTree.completionType == CompletionType.FreeFormNav || CourseTree.completionType == CompletionType.SequentialNav) {
		scorePS = 100;
	}
	if(!scoreUS) scoreUS = 0;

	var len = 3;
	var scoreStringUS = scoreUS.toPaddedString(len);
	var scoreStringPS = scorePS.toPaddedString(len);
	for(var i = 0; i < len; i++){
		var revStringUS = 'url(../resources/images/review.' + i + '.' + scoreStringUS.charAt(i) + '.gif)';
		var revStringPS = 'url(../resources/images/review.' + i + '.' + scoreStringPS.charAt(i) + '.gif)';
		var revUSDiv = $('reviewus' + i);
		if (revUSDiv) {
			revUSDiv.style.background = revStringUS;
		}
		var revPSDiv = $('reviewps' + i);
		if (revPSDiv) {
			revPSDiv.style.background = revStringPS;
		}
	}
}

/*
 * This function will toggle the input elements as necessary
 */
function DisableAnsweredElements(placeMarkerFunction) {
	//we don't want to disable the page elements if:
	//1) the question is NOT FOR GRADE or
	//2) the question is being LOADED FOR THE FIRST TIME
	var currentPage = DataChunk.getCurrentPage();
	var forGrade = (isTestSco() || CourseTree.completionType == CompletionType.InlineQuestions) && (currentPage && currentPage.pageType == PageType.Question);
	if(!DataChunk.isPageComplete() || !forGrade){
		return;
	}
	
	if (DataChunk.isPageComplete() || CourseTree.isPreview) {
		//find and disable all the input elements
		var arr = $$('.answerElToDisable');
		arr.each(function(el, index) {
			//in preview mode, allow the user to click submit more than once, and to change their answers
			if(!CourseTree.isPreview){
				el.disable();
			}
			
			//for every answer in the page, re-set the status based on whats stored in the course history
			var data = DataChunk.getCurrentUserAnswers();
			if (currentPage.answers[index] && el.name.toLowerCase() != 'answersubmitbtn') {
				for (var i = 0; i < data.length; i++) {				
					if (currentPage.answers[index].id == data[i]) {
						el.value = data[i];
						el.checked = (data[i] ? true : false);
					}
				}
			}
		});
	}
	
	DisplayScoreAndCert();
	
	//HACK: this will detect if the user has 2 entries for an answer, which is only the case for an image question
	var userAnswer = (currentPage.answers && currentPage.answers[0]) ? currentPage.answers[0].userAnswer : null;
	if(currentPage.answers.length > 0 && userAnswer && userAnswer.length == 2 && placeMarkerFunction){
		placeMarkerFunction(userAnswer[0],userAnswer[1]);
	}
	if (currentPage.pageType == PageType.Question) {
		//this will always return false for non-graded questions....may need to adjust this.
		var qColl = isPreTest() ? DataChunk.questionsP : DataChunk.questions;
		var qIndex = isPreTest() ? DataChunk.questionIndexP : DataChunk.questionIndex;
		var qid = qColl.selected[qIndex];
		if (CourseTree.completionType == CompletionType.InlineQuestions) {
			qid = currentPage.id;
		}
		DisplayFeedback(DataChunk.isQuestionCorrect(qid, isPreTest()) ? "correctAnswer" : "incorrectAnswer");
	}
}

function DisplayScoreAndCert(){
	//in pre and post tests, the current page will be either 1) the page index (for sequential and random) 2) 'test' (for jeopardy) or 3) 'review'
	if(isTestSco() || isReview()){
		if(parent.currentPage == 'test' && !DataChunk.isTestStarted()){
			$('navigationNext').hide();
		}else if(isReview()){
			//only display the cert if they've passed the course
			var testCompleted = (isPreTest() && CourseTree.testOutEnabled) || isPostTest() || DataChunk.shouldShowInlineReview();
			var certDiv = $('certificate');
			if(DataChunk.hasPassed() && CourseTree.certificateEnabled && testCompleted){
				if (certDiv) {
					certDiv.show();
				}
			}else{
				if (certDiv) {
					certDiv.hide();
				}
			}
			
			//displays the test score in the big digital readout
			DisplayTestScore();
			
			//don't show the next button for the post-test
			if(isPreTest()){
				$('navigationNext').show();
			}
		}
	}
}

function DisplayFeedback( feedback ) {			
	if($("correctAnswerFeedbackDiv")){
		$("correctAnswerFeedbackDiv").hide();
	}
	if($("incorrectAnswerFeedbackDiv")){
		$("incorrectAnswerFeedbackDiv").hide();
	}
	
	var currentPage = DataChunk.getCurrentPage();
	if(!currentPage){
		return;
	}
	if ( CourseTree.feedbackEnabled && 	(currentPage.pageType == PageType.Question || isPreTest() || isPostTest())){
		var el = $( feedback + "FeedbackDiv" );
		if (el) {
			el.up().setStyle({position: 'relative'});
			if (isPreTest()) {
				el.innerHTML = (feedback == 'correctAnswer' ? 'Correct' : 'Incorrect');
			}
			el.show();
		}
	}
}

function CheckAnswer( btn , list , choices ) {
	if (!CourseTree.isPreview && $(btn).disable){
		$(btn).disable();
	}
	
	var feedback = "incorrectAnswer";
	var correctFeedback = "correctAnswer";
	var answers = '';
	
	//determine question type
	var IsCorrect;
	switch (list) {
		case "multipleChoiceList":
		case "trueFalseList":
		case "allThatApplyList":
			IsCorrect = function(pageAnswer, answer) {  
				if (pageAnswer.checked) {
					if (answers.length > 0) {
						answers += ';';
					}
					answers += answer.id;				
				}
				return answer.isCorrect == pageAnswer.checked;
			};
			break;
		case "fillInTheBlankList":
			IsCorrect = function(pageAnswer, answer) {
				if (answers.length > 0) {
					answers += ';'; 
				}
				answers += $F(pageAnswer);
				return answer.text.toLowerCase().strip() == $F(pageAnswer).toLowerCase().strip();
			};
			break;
		case "flashList":
			IsCorrect = function(pageAnswer, answer) {
				answer.userAnswer = $F(pageAnswer);
				return answer == $F(pageAnswer);
			};            
			break;
		case "hotspotList":
			var ans = DataChunk.getCurrentPage().answers[0];
			answers = (ans.userAnswer ? Object.toJSON(ans.userAnswer) : '');
			
			//update StateManager if record does not already exist
			if (!DataChunk.isPageComplete() && !isTestSco()) {
				DataChunk.setPageComplete(parent.currentPage, answers);
			}		
			IsCorrect = function(){
				return (ans.userAnswer != null) && ans.userIsCorrect;
			};
			break;
	}  
	
	//confirm if answers are correct
	var currentPage = DataChunk.getCurrentPage();
	var correctCount = 0;
	var answersArr = [];
	if($(list)) {
		answersArr = $(list).getElementsByClassName(choices);
		if(answersArr) {
			$A(answersArr).each(function(answerEl, index) {
				var answ = currentPage.answers[index];
				if (!CourseTree.isPreview){
					answerEl.disable();
				}
				//do we need this code ^
				if (IsCorrect(answerEl, answ)  ) {
					correctCount++;
					PlaceTick(answerEl,list,answ,true);
				}else{
					PlaceTick(answerEl,list,answ,false);
				}
			});
		}
	}else{ //image question code
		//put a div around the correct area here...
		var coords = currentPage.answers[0].text.evalJSON(true);
		var div = document.createElement('div');
		div.id = 'areaDiv';
		//offsets account for margin around the image container
		var xOffset = -5;
		var yOffset = -5;
		$$('.imagequestiontemplate')[0].appendChild(div);
		div = $(div.id);
		div.setStyle({
			position:'absolute',
			top: (coords.y1 + yOffset) + 'px',
			left: (coords.x1 + xOffset) + 'px',
			width: (coords.x2 - coords.x1) + 'px',
			height: (coords.y2-coords.y1) + 'px',
			border: '2px dashed ' + (IsCorrect() ? 'green' : 'red') 
		});
	}
	var responseCorrect = false;
	if ((correctCount == answersArr.length && answersArr.length == currentPage.answers.length) || (list == "hotspotList" && IsCorrect())) {
		feedback = correctFeedback;
		responseCorrect = true;		
	}

	//this makes sure we don't record ungraded questions that appear inline when we have a post-test
	if(isTestSco() || CourseTree.completionType == CompletionType.InlineQuestions){
		if(responseCorrect){
			DataChunk.addCorrectQuestion();
		}
		DataChunk.addAnsweredQuestion();
	}
	DataChunk.sendInteractionsToLms(answers, responseCorrect);
	if (isTestSco()) {
		if (DataChunk.findTestType() == TestType.Jeopardy) {
			DataChunk.calculateJeopardyTotal(responseCorrect);
			$('jeopardyCloseButton').enabled = true;
			$('jeopardyButton' + DataChunk.findQuestionIndex()).hide();
			$('jeopardyButtonOff' + DataChunk.findQuestionIndex()).show();
		} else {
			if (DataChunk.isTestComplete()) {
				DataChunk.setScoComplete();
				DataChunk.completeTest(-1);
				$('pageNumbers').hide();
				$('noPageNumber').show();
				Ext.Msg.show({
					title: 'Complete',
					msg: continueText,
					buttons: Ext.Msg.OK,
					fn: LoadReview
				});
			} else {
				$('navigationNext').show();
			}
		}
	} else {
		if (!DataChunk.isPageComplete()) {
			DataChunk.setPageComplete(parent.currentPage, answers); //NOTE changed 2nd param from tempAnswers to answers
		}
		if (CourseTree.findSco(parent.currentSco).isLastPage()) {
			DataChunk.setScoComplete(!CourseTree.findSco(parent.currentSco).hasGradedQuestions());
			if (CourseTree.completionType == CompletionType.FreeFormNav || CourseTree.completionType == CompletionType.SequentialNav) {
				DataChunk.setPageComplete(parent.currentPage, answers);
			}
		}			
	}

	DisableAnsweredElements();
	return feedback;
}

function PlaceTick(el, questionList, answer, gotItRight){
	if (isPreTest()) return;
	var cls = 'incorrectAnswerIndicator'; //default to incorrect
	var answered = $F(el);
	if (isPreTest()) {
		if (answered && answer.isCorrect && gotItRight) {
			cls = 'correctAnswerIndicator';
		} else if (answered && !answer.isCorrect && !gotItRight) {
			cls = 'incorrectAnswerIndicator';
		} else {
			cls = '';
		}
	} else {
		if (gotItRight) {
				cls = 'correctAnswerIndicator'; //if they got it right
		} else if (answer.isCorrect) {
				cls = 'correctButGotItWrong'; //they got it wrong, and it should have been selected
		}
	}
	
	if(cls) {
		var parentEl = el.up().select('.' + cls)
		if (parentEl && parentEl.length == 0) {
			var span = document.createElement('span');
			if(answered) cls += ' answered'; //add an 'answered' class if the element has a value
			span.className = cls;
			span.innerHTML = '&nbsp;';
			el.up().appendChild(span);
			if (!gotItRight && questionList == 'fillInTheBlankList') {
				span.innerHTML += answer.text; //put the right answer in the span if its a fill in the blank
			}
		}
	}
}

function FixIEStyles(el, responseText){
	//hack to force IE to render embedded style tags
	var isIE = /*@cc_on!@*/false;
	if(isIE) {
		var html = responseText;
		var styleTags = /<style[^>]*>([\s\S]*?)<\/style>/gi;
		var styleMatch = null;
		//get the style tags
		var styleText = "";
		while(styleMatch = styleTags.exec(html)) {
			styleText += (styleMatch[1] + "\n");
		}
		//and create a "real" style tag so IE recognizes it
		var styleTag = document.createElement("style");
		styleTag.setAttribute("type", "text/css");
		styleTag.styleSheet.cssText = styleText;
		el.insertBefore(styleTag, el.firstChild);
	}
}


// of course IE7 and Firefox have slightly different ways to inhibit the Backspace and Alt Arrow...
// Firfox will support an "onkeydown" at the window level, but IE7 will not
// likewise, Firefox does not seem to put a Window.document.body element into the DOM for framesets... IE7 seems to do 
// this...  Inspected with VS2008.
function observeBackspaceAltArrowEvent( windowElement ) {
		
		if( Prototype.Browser.IE ) {
			// latch IE event
			Event.observe(windowElement.document.body,'keydown',function(event){
				var key = event.which || event.keyCode;
				if( (event.altKey && key == Event.KEY_LEFT) || ( key == Event.KEY_BACKSPACE ) ) {
					var myElement = Event.element(event);
					if( (myElement.tagName.toUpperCase() == 'INPUT') && ( key == Event.KEY_BACKSPACE ) ) {
						if( myElement.type == 'checkbox' || myElement.type == 'radio' ) {
							Event.stop(event);
						}
					}
					else {
						Event.stop(event);
					}
				}
			});	
		}
		else {
			
			// latch Firefox Event
			Event.observe(windowElement,'keydown',function(event){
				var key = event.which || event.keyCode;
				if( (event.altKey && key == Event.KEY_LEFT) || ( key == Event.KEY_BACKSPACE ) ) {
					var myElement = Event.element(event);
					if( (myElement.tagName.toUpperCase() == 'INPUT') && ( key == Event.KEY_BACKSPACE ) ) {
						if( myElement.type == 'checkbox' || myElement.type == 'radio' ) {
							Event.stop(event);
						}
					}
					else {
						Event.stop(event);
					}
				}
			});		
		}

}


function assignKeyboardInhibitEvent(topWindow) {

	for (var index = 0; index < topWindow.frames.length; index++) {
		var item = topWindow[index];
	
		observeBackspaceAltArrowEvent( item );
		
		if( item.frames.length > 0 ) {
			assignKeyboardInhibitEvent( item );
		}
	}
	
}


function LoadContent() {
	var panel = Ext.get('centerInner');
	if (panel) {
		panel.load({
			url: DataChunk.loadSco(),
			callback: function(panel, success, transport){
				//set question index here if we're in an inline course...
				if (CourseTree.completionType == CompletionType.InlineQuestions && !CourseTree.isPreview) {
					var currentPage = DataChunk.getCurrentPage();
					if (currentPage && currentPage.pageType == PageType.Question) {
						var qIndex = DataChunk.questions.selected.indexOf(currentPage.id);
						DataChunk.questionIndex = qIndex;
						DataChunk.updateStateManagerItem('PostQuestionIndex', qIndex);
					}
				}
				FixIEStyles(panel.dom, transport.responseText);
				Cleanup();
				var placeMarkerFn = QuestionLoadCallback(panel, success, transport);
				if(!CourseTree.isPreview){
					DisableAnsweredElements(placeMarkerFn);
					AddUIText();	
					
					// register an Observer to deal with screwy keyboard pressing
					if( !top.isKeyboardInhibitorsRegistered ) {
						top.isKeyboardInhibitorsRegistered = true;
						//setTimeout( function() {assignKeyboardInhibitEvent( top ); }, 1000);	
						
						//alert("going to run Inhibitor code");
						assignKeyboardInhibitEvent( top );
					
						//alert("latch to top");
						// hack because recursion wasn't finding this frame
						observeBackspaceAltArrowEvent( top );
					}					
				}
				

			}
		});
	}
}

function AddUIText(){
	//todo: deal with inline questions
	if(isPostTest() || isPreTest() || isReview()){
		//update the review content with the appropriate message
		var message = (isPostTest() || CourseTree.completionType == CompletionType.FreeFormNav || CourseTree.completionType == CompletionType.SequentialNav) ? "PostTest" : "PreTest";
		var body = '';
		var title = ''
		if(isTestPage()){  //this works because the introduction will be hidden automatically
			body = 'introText';
			title = 'introHeader';
			message += "Start";
		}else if(isReview()){
			body = 'reviewText';
			var reviewDiv = $('review');
			if(DataChunk.hasPassed()){
				if (reviewDiv) {
					reviewDiv.addClassName('passed');
				}
				message += "Pass";
			}else{
				if (reviewDiv) {
					reviewDiv.addClassName('failed');
				}
				message += "Fail";
			}
		}
		
		if(CourseTree.testOutEnabled){
			message += "TestOut";
		}
		//we have to defer the creation of the test messages so the course tree can load first...
		if(!window.TestMessages){
			CreateTestMessages();
		}
		var msg = TestMessages[message];
		
        if ($(body)){
            $(body).update(msg.body);
        }
		if($(title)){
			$(title).update(msg.header);
		}
		
		DisplayScoreAndCert();
	}
}

function LoadQuestion() {
	var questionData = Ext.get('testQuestion');
	questionData.load({
		url: DataChunk.getQuestionUrl(),
		callback: function(panel, success, transport){
			FixIEStyles(panel.dom, transport.responseText);
			//i hate putting template specific code in here, but this is the only way it would trigger at the right time,
			//and it needs to track "correct" and "incorrect" answers a little differently. crap. oh well.
			//this is the code that places a block in the image question wherever they click.
			Cleanup();
			var placeMarkerFn = QuestionLoadCallback(panel, success, transport);
			DisableAnsweredElements(placeMarkerFn);
			if (DataChunk.isPageComplete()) {
				$('navigationNext').show();
			}
		}
	});	
}

function LoadReview() {
	// if you are in the PostTest, lock down the navigation tree
	if( (parent.currentSco == 'posttest') && (CourseTree.courseType != CourseFormat.SCORM2004) ) {
		DataChunk.lockDownAllTreeStatuses();
	}
	parent.currentPage = 'review';
	LoadContent();	
}

function QuestionLoadCallback(questionData, success, transport) {
	//i hate putting template specific code in here, but this is the only way it would trigger at the right time,
	//and it needs to track "correct" and "incorrect" answers a little differently. crap. oh well.
	//this is the code that places a block in the image question wherever they click.
	var placeMarker;
	if($('imageAnswerContainer')){
		var getAndShowMarker = function(evt){
			var x = Event.pointerX(evt);
			var y = Event.pointerY(evt);
			placeMarker(x,y);
			Event.stop(evt);
		}
		
		var dim = 16; //the height/width of the square
		var xOffset = -4;
		var yOffset = -1;
		placeMarker = function(x,y){
			if(!$('markerDiv')){
				var div = new Element('div', {
					'id': 'markerDiv',
					'class': 'imageAreaSelector'
				});
				div.setStyle({
					position: 'absolute',
					zIndex: 99999999999,
					height: dim+'px',
					width: dim+'px',
					display: 'none'
				});
				
				document.body.appendChild(div);
			}
			var div = $('markerDiv');
			div.style.left = x + xOffset + 'px';
			div.style.top = y + yOffset + 'px';
			div.style.display = 'block';

			//DataChunk.findStatePage(id)
			var pos = $$('.imagequestiontemplate')[0].cumulativeOffset();
			var off = {x: 5, y: 5};
			DataChunk.getCurrentPage().answers[0].userAnswer = [(x - pos.left + off.x),(y - pos.top + off.y)];
		}
		
		var img = $('answerContainer').getElementsBySelector('img')[0];
		var map = $('answerContainer').getElementsBySelector('map')[0];
		Event.observe(img,'click',function(evt){
			if(!CourseTree.isPreview && DataChunk.isPageComplete() && isTestSco() ) return;
			getAndShowMarker(evt);
			DataChunk.getCurrentPage().answers[0].userIsCorrect = false;
		});
		Event.observe(map,'click',function(evt){
			if(!CourseTree.isPreview && DataChunk.isPageComplete() && isTestSco() ) return;
			getAndShowMarker(evt);
			DataChunk.getCurrentPage().answers[0].userIsCorrect = true;
		});
	}else{
		Cleanup();
	}
	return placeMarker;
}

function LoadJeopardyQuestion(element, position) {
	if (!window.questionWin) {
		window.questionWin = new Ext.Window({
			el: 'questionWindow',
			modal: true,
			plain: false,
			draggable: true,
			cls: templateClass,
			autoHeight:true,
			width:701,
			resizable: true,
			autoDestroy: true,
			closable: false,
			title: 'Answer The Following Question',			
			buttons: [{
				text: 'Close',
				id:'jeopardyCloseButton',
				disabled: false,
				handler: function() {
					window.questionWin.hide();	
					if($('markerDiv')) {
						$('markerDiv').remove();
					}
					if (DataChunk.isTestComplete()) {
						parent.currentPage = 'review';
						DataChunk.setScoComplete();
						DataChunk.completeTest(-1);
						Ext.Msg.show({
							title: 'Complete',
							msg: continueText,
							buttons: Ext.Msg.OK,
							fn: LoadReview
						});		
					}
				}
			}]
			
		});
		window.questionWin.on("show",function(){
			window.questionWin.setPosition(160,50);
			window.questionWin.setSize(700,590);
		});
	}
	DataChunk.setQuestionIndex(position);
	window.questionWin.show();
	LoadQuestion();
}

function DisplayCertificate() {
	//params needed. StudentName, CourseName, CompletionMethod, Score
	//do the window.open stuff
	var w = (790);
	var h = (564);
	var t = ((screen.height - h) / 2);
	var l = ((screen.width - w) / 2);
	var compType = '';
	switch(CourseTree.completionType){
		case CompletionType.FreeFormNav:
		case CompletionType.SequentialNav:
			compType = 'Navigation Completed';
		case CompletionType.Test:
			compType = 'Test Completed';
		case CompletionType.InlineQuestions:
			compType = 'Inline Test Completed';
	}
	compType = escape(compType);
	var courseName = CourseTree.title;

	//allow the course name to be overridden in the url
    var result = unescape(parent.top.ExternalRegistrationId).match(/.+@Course#(.+?)($|@)/);
	
	if(result && result.length > 0){
		courseName = result[1].replace("_","'");
	}
	window.open('../resources/printcert.html?score='+DataChunk.calculateScore()+'&name='+studentName+'&course='+courseName+'&type='+compType,'CertificateWindow','height='+h+',width='+w+',top='+t+',left='+l+',toolbar=no,menubar=no,status=no,location=no,scrollbars=yes,resizable=yes');	
}

function isTestSco() {
	return (parent.currentSco == 'pretest' || parent.currentSco == 'posttest') ? true : false;
}

function isReview(){
	return (parent.currentPage == 'review');
}

function isTestPage(){
	return (parent.currentPage == 'test');
}

function isPreTest(){
	return (parent.currentSco == 'pretest');
}

function isPostTest(){
	return (parent.currentSco == 'posttest');
}

function isSpecialSco() {
	return (parent.currentSco == 'pretest' || parent.currentSco == 'posttest' || parent.currentSco == 'objectives') ? true : false;
}
