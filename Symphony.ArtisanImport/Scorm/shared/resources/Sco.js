// Sco.js

Sco = Class.create({
	//function used to initialize the Sco object.
	initialize: function(id, title, url, scoType, masteryScore) {
		this.id = id;
		switch (id) {
			case 'pretest':
				this.title = CourseTree.preTestLabel;
				this.masteryScore = CourseTree.preTestScore;			
				break;
			case 'posttest':
				this.title = CourseTree.postTestLabel;
				this.masteryScore = CourseTree.postTestScore;
				break;
			case 'objectives':
				this.title = 'Objectives';
				this.masteryScore = 0;
				break;
			default:
				this.title = title;
				this.masteryScore = masteryScore;
				break;
		}
		this.url = url;
		this.scoType = scoType;
		this.pages = new Array();
		this.parentSco = 0;
		this.scoredQuestionCount = 0;
		if (CourseTree.courseType != CourseFormat.SCORM2004) {
			var folders = this.url.split('/');
			if (folders.length > 1) {
				if (folders.length == 4) {
					this.parentSco = folders[1];
				}
			}
		}
	},
	//function used to add a new Page object to the Pages array of the Sco.
	addPage: function(page) {
		this.pages.push(page);
	},
	//function used to create a new Page object within the Sco.
	newPage: function(id, title, pageType, calculateGrade, questionType, questionText) {
		var page = new Page(id, title, pageType, calculateGrade, questionType, questionText);
		this.addPage(page);
		return page;
	},
	//function used to return a specific Page object from a Page within the Sco.
	findPage: function(id) {
		for (var i = 0; i < this.pages.length; i++)	{
			var page = this.pages[i];
			if (page.id == id)
				return page;
		}
		return null;
	},
	//function used to return the Page index of a specific page within the Sco.
	getIndexById: function(id) {
		for (var i = 0; i < this.pages.length; i++)	{
			if (this.pages[i].id == id) {
				return i;
			}
		}
		return null;
	},
	//function used to return a specific Page object from a Page index within the Sco.
	getPageByIndex: function(index) {
	    return this.pages[index];
	},
	//function used to determine if the current Page is the first Page in the Sco.
	isFirstPage: function() {
		if (this.pages[0].id == parent.currentPage) {
			return true;
		} else {
			return false;
		}
	},
	//function used to determine if the current Page is the last Page in the Sco.
	isLastPage: function() {
		if (this.pages[this.pages.length - 1].id == parent.currentPage) {
			return true;
		}
		return false;
	},
	//function used to identify if the Sco has graded Questions.
	hasGradedQuestions: function() {
		for (var i = 0; i < this.pages.length; i++) {
			if (this.pages[i].pageType == PageType.Question && this.pages[i].calculateGrade) {
				return true;
			}
		}
		return false;
	}
});