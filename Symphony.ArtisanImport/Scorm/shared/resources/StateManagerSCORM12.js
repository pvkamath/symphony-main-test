var StateManagerSCORM12 = Class.create(StateManager, {
	getFromLms: function() {
		if (!CourseTree.isPreview) {
			this.buildPageIndex();
			if(API.GetDataChunk().length == 0 || !CourseTree.bookmarkEnabled){
				this.initializeStateManagerItems();
				this.initializeTreeStatuses();
			}else{
				this.continueCourse();
			
				if(this.inPreTest() && this.questionsP.selected.length == this.questionsP.answered.length){
					this.nextSco();
				}
				if(this.inPostTest() && this.questions.selected.length == this.questions.answered.length){
					this.questions.answered = [];
					this.questions.correct = [];
					this.questionIndex = 0;
					this.updateStateManagerItem("PostAnswered", '');
					this.updateStateManagerItem("PostCorrect", '');
					this.updateStateManagerItem("PostQuestionIndex", '');
					parent.currentPage = '';
					parent.currentSco = 'objectives';				
				}
				if (CourseTree.completionType == CompletionType.InlineQuestions && this.questions.selected.length == this.questions.answered.length) {
					if (!this.reviewMode) {
						this.questions.answered = [];
						this.questions.correct = [];
						this.updateStateManagerItem("PostAnswered", '');
						this.updateStateManagerItem("PostCorrect", '');
						this.updateStateManagerItem("PostQuestionIndex", '');
						//need to take out the answered questions from the state manager
						var me = this;
						this.questions.selected.each(function(selQ, index1) {
							me.stateManagerItems.each(function(item, index2) {
								if (item && item.id == selQ.toString()) {
									me.stateManagerItems[index2] = null;
								}
							});
						});
						//now clean up the null items
						this.stateManagerItems = this.stateManagerItems.reject(function(item) {
							return item == null;
						});
					}
					this.questionIndex = 0;
					parent.currentPage = '';
					parent.currentSco = 'objectives';
				}
			}
		}
	},
	initializeStateManagerItems: function($super) {
		$super();
		switch (CourseTree.completionType) {
			case CompletionType.FreeFormNav:
			case CompletionType.SequentialNav:
				this.masteryScore = 0;
				break;
			case CompletionType.InlineQuestions:
				this.masteryScore = this.calculateMasteryScore();
				this.questions.selected = this.getAllGradedQuestions();
				this.questionIndex = 0;
				this.storePreAndPostTestValues();
				break;
			case CompletionType.Test:
				this.buildStateManagerItemValues();
				break;
		}
	},
	
	getAllGradedQuestions: function() {
		var questions = new Array();
		CourseTree.scos.each(function(sco){
			sco.pages.each(function(page){
				if(page.pageType == PageType.Question && page.calculateGrade){
					questions.push(page.id);
				}
			});
		});
		return questions;
	},
	
	//function used to reload StateManagerItems for a re-launched Course.
	continueCourse: function($super) {
		$super();
		this.readTreeStatuses();
		//this.rebuildTreeLinks();
	},

	findQuestionIndex: function() {
		if (this.inPreTest()) {
			return this.questionIndexP;
		} else if (this.inPostTest()) {
			return this.questionIndex;
		}
	},

	getBookmark: function(){ //COPIED IN FROM 2004
		return this.bookmark;
	},

	loadSco: function() {
		var sco;
		if (!parent.currentSco || parent.currentSco == "undefined") {
			parent.currentSco = CourseTree.scos[0].id;
		}
		sco = CourseTree.findSco(parent.currentSco);
		if (sco.pages.length > 0) {
			parent.currentSco = sco.id;
			parent.currentScoUrl = sco.url;
			if (isTestSco()) {
				if (this.findTestType() == TestType.Jeopardy) {
					questionWin = null;
				}
				if (parent.currentPage != 'review') { 
					parent.currentPage = 'test';
				}
				if (this.getTreeNodeStatus(parent.currentSco) == ScoStatusType.NotStarted) {
					this.setTreeNodeStatus(parent.currentSco, ScoStatusType.InProgress);
				}
			} else {				
				if (parent.currentPage != 'review'){
					parent.currentPage = sco.pages[this.getPageIndex()].id;
					if (sco.isFirstPage(parent.currentPage)) {
						this.updateStateManagerItem('PageIndex', parent.currentSco + "#0");
					}
					if (CourseTree.completionType == CompletionType.InlineQuestions && this.getPageIndex() == 0 && sco.hasGradedQuestions()) {
						this.questionIndex  = 0;
						this.updateStateManagerItem('PostQuestionIndex', this.questionIndex);
					}
				}
				if (sco.pages.length > 1) {
					this.setTreeNodeStatus(parent.currentSco, ScoStatusType.InProgress);
				}
				if (CourseTree.findSco(parent.currentSco).isLastPage(parent.currentPage)) {
					if (CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).pageType != PageType.Question && !CourseTree.findSco(parent.currentSco).hasGradedQuestions()) {
						this.setTreeNodeStatus(parent.currentSco, ScoStatusType.Completed);
						this.setScoComplete();
					}
				}
			}
			this.setPageElements();
		} else {
			alert('No pages exist.');
		}

		if (parent.currentSco != 'objectives') {
			cls = templateClass;
		}

		Cleanup();
		this.loadPageContent();
		//this.buildTreeLinks();
		this.saveToLms();
		return parent.currentScoUrl + parent.currentPage + ".html";
	},
	
	shouldShowInlineReview: function(){
		return CourseTree.isLastSco() && this.findCurrentSco().isLastPage() && CourseTree.completionType == CompletionType.InlineQuestions;
	},

		//function used to build an array of Page indexes for all Scos.
	buildPageIndex: function() {
		var pageIndex = '';
		for (var i = 0; i < CourseTree.scos.length; i++) {
			if (pageIndex.length > 0) {
				pageIndex += ';';
			}
			pageIndex += CourseTree.scos[i].id + '#' + 0;
		}
		this.addStateManagerItem('PageIndex', pageIndex);
	},

	//function used to update the current Page index.
	updatePageIndex: function(pageIncrement) {
		var pageIndex = '';
		var nameValuePair = this.findStateManagerItemValue('PageIndex').split(';');
		var found = false;
		for (var i = 0; i < nameValuePair.length; i++) {
			if (pageIndex.length > 0) {
				pageIndex += ';';
			}
			var data = nameValuePair[i].split('#');
			if (data[0] == parent.currentSco) {
				found = true;
				pageIndex += parent.currentSco + '#' + parseInt(Number(data[1]) + Number(pageIncrement));
			} else {
				pageIndex += nameValuePair[i];
			}
		}
		if(!found){
			pageIndex += parent.currentSco + '#' + "1"; //if we don't find any existing page index, initialize....since we're updating, that means initialize to the 2nd page (index = 1)
		}
		this.updateStateManagerItem('PageIndex', pageIndex);
	},

	//function used to return the current Page index.
	getPageIndex: function() {
		if (isTestSco()) {
			if (this.inPreTest()) {
				return this.findStateManagerItemValue('PreQuestionIndex');
			} else {
				return this.findStateManagerItemValue('PostQuestionIndex');
			}
		} else {
			var stateManagerItem = this.findStateManagerItemValue('PageIndex');
			if (stateManagerItem) {
				var nameValuePair = stateManagerItem.split(';');
				for (var i = 0; i < nameValuePair.length; i++) {
					var data = nameValuePair[i].split('#');
					if (data[0] == parent.currentSco) {
						return parseInt(data[1]);
					}
				}
			} else {
				return 0;
			}
		}
		return 0;
	},

	
	setScoComplete: function(setTreeNodeStatus) {
		this.setTreeNodeStatus(parent.currentSco, ScoStatusType.Completed);
		if (isTestSco()) {
			this.completeTest();
		} else {
			if (this.isCourseComplete()) {
				if (CourseTree.completionType == CompletionType.FreeFormNav || CourseTree.completionType == CompletionType.SequentialNav) {
					if (parent.SCORM_SetCompleted) {
						parent.SCORM_SetCompleted();
					} else if (parent.AICC_SetCompleted) {
						parent.AICC_SetCompleted();
						this.setScore();
					}
				} else if (CourseTree.completionType == CompletionType.InlineQuestions) {
					this.setScore();
					if (this.calculateScore() >= this.getMasteryScore()) {
						this.updateStateManagerItem('ReviewMode', 'true'); //passing score achieved, therefore explicitly put the course in review mode
					}
				}
				this.saveToLms();
			}
		}
	},
	
	setScore: function(){
		var userScore = this.calculateScore();
		var masteryScore = this.getMasteryScore();
		if (CourseTree.completionType == CompletionType.FreeFormNav || CourseTree.completionType == CompletionType.SequentialNav) {
			masteryScore = 100;
		}
		parent.SetScore(userScore, 0, 100);
		if (userScore >= masteryScore) {
			parent.SetPassed();
		}else{
			parent.SetFailed();
		}
	},
	
	setComplete: function(){
		//parent.SCORM_SetCompleted(); // this line is not necessary for SCORM 1.2, and in fact can cause problems, so commenting out here
	},

	//function used to send the results of a Test to the LMS.
	completeTest: function() {
		if (!CourseTree.isPreview) {
			if ((this.inPreTest() && CourseTree.testOutEnabled) || this.inPostTest()) {
				if (this.calculateScore() >= this.getMasteryScore()) {
					if (this.inPreTest()) {
						this.setTreeNodeStatus('pretest', ScoStatusType.LockedComplete);
						this.setTreeNodeStatus('posttest', ScoStatusType.LockedComplete);
					} else {
						this.updateStateManagerItem('ReviewMode', 'true'); //passing score achieved in postest, therefore explicitly put the course in review mode	
					}
					this.setComplete();
				    this.setScore();
				}else{
					if(this.inPreTest()){
						this.setTreeNodeStatus('pretest', ScoStatusType.LockedFailed);
					}else{
						this.setTreeNodeStatus('posttest', ScoStatusType.Failed);
						this.setScore();
					}
				}
			}else if(this.inPreTest()){
				//lock it if its a pre-test without test-out
				this.setTreeNodeStatus('pretest', ScoStatusType.LockedComplete);
			}
		}
	},

	//function used to return the Mastery Score for the current Sco.
	getMasteryScore: function() {
		if (this.inPreTest()) {
			return parseInt(this.findStateManagerItemValue('PreMasteryScore'));
		} else if (this.inPostTest()) {
			return parseInt(this.findStateManagerItemValue('PostMasteryScore'));
		} else {
			if (CourseTree.completionType == CompletionType.InlineQuestions) {
				return this.calculateMasteryScore();
			}
		}
	},
	
	previousSco: function() {
		var prev = CourseTree.findPreviousSco();
		if(prev != 0){
			parent.currentSco = prev;
			this.applyTreeStatuses();
			LoadContent();
		}else{
			ExitCourse();
		}
	},

	nextSco: function() {
		var next = CourseTree.findNextSco();
		var pIndex = '';
		if(next != 0){
			parent.currentSco = next;
			parent.currentPage = '';
			CourseTree.findSco(parent.currentSco).status = ScoStatusType.InProgress;
			
			questionsPLength =  this.questionsP.answered ? this.questionsP.answered.length : 0;
		
			if(this.inPreTest() && this.questionsP.selected.length == questionsPLength){
				this.setTreeNodeStatus('pretest', ScoStatusType.LockedComplete);
				//skip pretest, move to whatever the next sco is.
				this.nextSco();
			}	
			else if( this.inPreTest() && (this.questionsP.selected.length != questionsPLength) ) {
				// definitely in the pretest, but haven't completed it so pick up where you last left off per normal bookmarking
				this.applyTreeStatuses();
				LoadContent();
			}
			else if( !this.inPostTest() && (this.questionsP.selected.length == questionsPLength) ){
				//This would be code to set the specific page, but unsure if needed...
				pIndex += parent.currentSco + '#' + "0"; //if we don't find any existing page index, initialize....since we're updating, that means initialize to the 2nd page (index = 1)
				this.pageIndex = pIndex;
				this.updateStateManagerItem('PageIndex', pIndex);				
				this.applyTreeStatuses();
				this.navigatePage();				
			}
			else {
				this.applyTreeStatuses();
				LoadContent();
			}
		} else if ((CourseTree.completionType == CompletionType.FreeFormNav) || (CourseTree.completionType == CompletionType.SequentialNav && this.isCourseComplete())) {
			parent.SetScore(100, 0, 100);
			parent.SetPassed();
			parent.currentPage = 'review';
			this.updateStateManagerItem('PageIndex', parent.currentSco + "#0");
			LoadContent();
		} else {
			ExitCourse();
		}
	},

	//not implemented for SCORM 1.2.
	sendInteractionsToLms: function(answer) {
	},

	//not implemented for scorm1.2
	addSendQuestion: {
	},
	
	//function used to return a count of graded Questions within an entire Course.
	getGradedQuestionCount: function() {
		var counter = 0;
		for (var i = 0; i < CourseTree.scos.length; i++) {
			for (var j = 0; j < CourseTree.findSco(CourseTree.scos[i].id).pages.length; j++) {
				if (CourseTree.findSco(CourseTree.scos[i].id).pages[j].pageType == PageType.Question && CourseTree.findSco(CourseTree.scos[i].id).pages[j].calculateGrade) {
					counter++;
				}
			}
		}
		return counter;
	},

	//function used for Inline Question completion to calculate an average Mastery Score.
	calculateMasteryScore: function() {
		var counter = 0;
		var score = 0;
		for (var i = 0; i < CourseTree.scos.length; i++) {
			if (CourseTree.scos[i].hasGradedQuestions()) {
				counter++;
				score = score + parseInt(CourseTree.scos[i].masteryScore);
			}
		}

		if (counter > 0 && score > 0) {
			return parseInt(score / counter);
		} else {
			return 0;
		}
	},

	//this function sets up the status for each sco in the tree to an initial value. 
	//once the values are set, they're stored in the datachunk.
	//to parse them back, use readTreeStatuses.
	//to apply the statuses to the treeview, use applyTreeStatuses
	//to make a status change, use setTreeStatus
	initializeTreeStatuses: function() {
		//setTimeout hack to workaround odd asyn issues
		var me = this;
		setTimeout(function() {
			CourseTree.scos.each(function(sco){
				//we always initialize the objectives sco to "complete"
				if(sco.id == 'objectives'){
					sco.status = ScoStatusType.Completed;
				}else{	
					if ((CourseTree.completionType == CompletionType.SequentialNav || CourseTree.completionType == CompletionType.FreeFormNav) &&
						CourseTree.navigationType == NavigationType.Sequential) {
						//if we're doing seqential nav, lock it
						sco.status = ScoStatusType.Locked;
					} else {			
						if(CourseTree.scos.any(function(subSco){ return subSco.parentSco == sco.id; })){
							//if it has child scos, never lock
							sco.status = ScoStatusType.NotStarted;
						}else if(CourseTree.completionType != CompletionType.FreeFormNav && CourseTree.navigationType == NavigationType.Sequential){
							//if we're doing seqential nav, lock it
							sco.status = ScoStatusType.Locked;
						}else{
							//if we're doing free-form nav, open it up
							sco.status = ScoStatusType.NotStarted;
						}
					}
				}
			});
			me.applyTreeStatuses();
		}, 2500);
	},
	
	// lockDown everything except for the post test
	lockDownAllTreeStatuses: function() {
		CourseTree.scos.each(function(sco){
			//we always initialize the objectives sco to "complete"
			if(sco.id != 'posttest'){
				if(CourseTree.scos.any(function(subSco){ return subSco.parentSco == sco.id; })){
					//descend into child SCO's
					sco.status = ScoStatusType.Locked;
				}else{
					//if we're doing free-form nav, open it up
					sco.status = ScoStatusType.Locked;
				}
			}
		});
		this.applyTreeStatuses();
	},	
	
	readTreeStatuses: function(){
		var tree = this.tree;//this.findStateManagerItemValue('FlatScoStatus');
		if(!tree) return;
		tree.split(";").each(function(scoAndStatus){
			var parts = scoAndStatus.split("#");
			var id = parts[0];
			var status = parts[1];
			var sco = CourseTree.scos.find(function(sco){
				return sco.id == id;
			});
			sco.status = parseInt(status);
		});
		this.applyTreeStatuses();
	},
	
	writeTreeStatuses: function(){
		var tree = '';
		CourseTree.scos.each(function(sco){
			if(tree.length > 0){
				tree += ";";
			}
			tree += sco.id + "#" + (sco.status || 0);
		});
		this.updateStateManagerItem('FlatScoStatus',tree);
		this.saveToLms();
	},
	
	applyTreeStatuses: function(){
		CourseTree.scos.each(function(sco){
			parent.ToggleTreeNode(sco.id, sco.status);
		});
		this.writeTreeStatuses();
	},

	//function used to update the ScoStatusType for a specific Tree Node.
	setTreeNodeStatus: function(sco, scoStatusType) {
		CourseTree.findSco(sco).status = scoStatusType;
		this.applyTreeStatuses();
	},

	//function used to return a ScoStatusType for a specific Tree Node.
	getTreeNodeStatus: function(sco) {
		return CourseTree.findSco(sco).status;
	},

	isCourseComplete: function() {
		var isComplete = true;
		CourseTree.scos.each(function(sco){
			//not completing the pre-test doesn't cause the whole thing to be incomplete.
			if(sco.id != 'pretest'){
				isComplete = (isComplete && 
				(
					sco.status == ScoStatusType.Completed || 
					sco.status == ScoStatusType.LockedComplete || 
					sco.status == ScoStatusType.Failed || 
					sco.status == ScoStatusType.LockedFailed
				));
			}
		});
		return isComplete;
	},

	//function used to return the current Page object or Question object.
	getCurrentPage: function() {
		if (isTestSco()) {
			return this.getQuestion();
		} else {
			return CourseTree.findSco(parent.currentSco).findPage(parent.currentPage);
		}
	},

	//function used to return the QuestionType of the current Question.
	findQuestionType: function() {
		if (isTestSco()) {
			return this.getQuestion.questionType;
		} else {
			return CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).questionType;
		}

	}
});
