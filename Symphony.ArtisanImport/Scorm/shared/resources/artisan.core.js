var API = parent;
var studentName = '';
var TestType = {Jeopardy: 1,Sequential: 2,Random: 3};
var CompletionType = {FreeFormNav: 1,SequentialNav: 2,Test: 4,InlineQuestions: 5};
var NavigationType = {Sequential: 1,FreeForm: 2};
var QuestionType = {MultipleChoice: 1,TrueFalse: 2,FillInTheBlank: 3,AllThatApply: 4,Image: 5};
var PageType = {Content : 1,Question: 2};
var CourseFormat = {AICC: 'AICC',SCORM: 'SCORM',SCORM2004: 'SCORM2004'};
var ScoStatusType = {Locked: -1,NotStarted: 0,InProgress: 1,Failed: 2,Completed: 3,LockedComplete: 4, LockedFailed: 5};

var includeFile = function(filename){
    document.write("<script type='text/javascript' src='"+filename+"'><\/script>");
};

var path = $A(document.getElementsByTagName("script")).find( function(s) {
    return s.src && s.src.match(/artisan\.core\.js/);
}).src.replace(/artisan\.core\.js/, '');

//messages MUST load last
["Course.js", "Page.js", "Sco.js","StateManager.js", "StateManagerSCORM2004.js","StateManagerSCORM12.js", "StateManagerAICC.js", "Messages.js",].each(function(file){
    includeFile(path+file);
});
