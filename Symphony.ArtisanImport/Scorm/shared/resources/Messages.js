/*
 * These are all the messages used within the course to introduce the course or to show the review page. They are based on pre/post test, certificate enabled/disabled, and test out enabled/disabled
 */
function CreateTestMessages(){
	var TestMessages = window.TestMessages = {
		readyText : "<p>Ready? Then click the \"Begin\" button to start the test.</p>" +
					"<p>Good luck!</p>",
		readyText2 : "<p>Ready? Then click on the first topic in the menu.</p>" +
					"<p>Good luck!</p>",				
		preTestName : CourseTree.preTestLabel,
		postTestName : CourseTree.postTestLabel,
		printCert: CourseTree.certificateEnabled ? "<p>Click on the \"Print Certificate\" button below. When you are finished, or if you choose not to print the certificate at this time, you may simply exit the course by clicking the \"Exit\" button at the top of the page.</p>" : ""
	}
	Object.extend(TestMessages, {
		basicPostTestBody : "<p>The following test will allow you to demonstrate your understanding of the course topics. You will see questions drawn from the course either as a group of questions presented one question at a time, or presented like a TV game show.</p>" + 
							"<p>Your score will be recorded in your record.</p>" +
							"<p>Good luck!</p>",
		basicPreTestBody : 	"<p>You are about to begin the " + TestMessages.preTestName + ". The " + TestMessages.preTestName + " will give you the opportunity to establish your level of understanding of the course content. You will be asked questions that are typical of what you will be asked during the " + TestMessages.postTestName + ". You may see the questions presented as a group of questions at the end of the course, or even as an entertaining game board.</p>"
	});
	Object.extend(TestMessages, {
	PostTestStart: {
		header: "Time to show what you know!",
		body: TestMessages.basicPostTestBody
	},
	PostTestStartTestOut: {
		header: "You have a choice!",
		body: 	"<p>If you passed the pre-test you may either keep that score, or see if you can improve your score by taking the " + TestMessages.postTestName + ".</p>" + 
				"<p>If you choose to take the " + TestMessages.postTestName + ", you must complete that test and accept the score into your records. Once you begin the test, you are committed to finishing it.</p>" +
				(CourseTree.certificateEnabled ? "<p>If you choose to accept the " + TestMessages.preTestName + " score, you may click on the \"Print Certificate\" button below.</p>" : "") + 
				"<p>When you are finished you may simply exit the course by clicking the \"Exit\" button at the top of the page.</p>" + 
				TestMessages.basicPostTestBody
	},
	PreTestStart: {
		header: "Welcome!",
		body:	TestMessages.basicPreTestBody +
				"<p>The " + TestMessages.preTestName + " is not recorded, but you will get the opportunity to see how well you already know the material.</p>" +
				TestMessages.readyText
	},
	PreTestStartTestOut: {
		header: "Welcome!",
		body: 	TestMessages.basicPreTestBody +
				"<p>The " + TestMessages.preTestName + " allows you to choose to challenge the course. If you already know the topic and can prove it by achieving a passing score on the " + TestMessages.preTestName + ", you may accept that score into your records or you can continue with the course and take the " + TestMessages.postTestName + ". At the end of the course, you can decide if you want to keep your " + TestMessages.preTestName + " score, or try for a better score. Once you begin the " + TestMessages.postTestName + " you must finish it, and that will be the recorded score.</p>" +
				TestMessages.readyText
	},
	PreTestFailTestOut: {
		body: "<p>Now you are aware of your starting point with the course topic. When you have completed the course, you will have the opportunity to show your master of the topic in a graded " + TestMessages.postTestName + ".</p>" +
				TestMessages.readyText2
	},
	PreTestFail: {
		body: "<p>Now you are aware of your starting point with the course topic. When you have completed the course, you will have the opportunity to show your master of the topic in a graded " + TestMessages.postTestName + ". This test was for practice only, and will not affect your final score.</p>" +
				TestMessages.readyText2
	},	
	PostTestPass: {
		body: "<p>Congratulations! You have successfully completed this course. Your passing grade will be added to your records.</p>" +
				TestMessages.printCert
	},
	PostTestFail: {
		body: "<p>You have not met the minimum score to complete this course. Please re-launch the course to try again.</p>"
	},
	PostTestPassTestOut: {
		body: "<p>Congratulations! You have successfully completed this course. Your passing grade will replace your " + TestMessages.preTestName + " score in your records.</p>" +
				TestMessages.printCert
	},
	PreTestPass: {
		body: "<p>Congratulations! You are well on your way to mastery of this topic. As you progress through the course, look for new infromation or perspective on the topic. When you have completed the course, you will have the opportunity to show your master of the topic in a graded post-test. This test was for practice only, and will not affect your final score.</p>" +
				TestMessages.readyText2
	},
	PreTestFailTestOut: {
		body: "<p>Now you are aware of your starting point with the course topic. When you have completed the course, you will have the opportunity to show your mastery of the topic in a graded post-test.</p>" +
				TestMessages.readyText2
	},
	PreTestPassTestOut: {
		body: 	"<p>Congratulations! By achieving a passing score, you now have a choice. You may either keep that score, or see if you can improve your score by taking the " + TestMessages.postTestName + ".</p>" + 
				"<p>You may continue and take the course, then decide if you want to challenge the " + TestMessages.postTestName + " for a higher score, or keep the score you have just achieved and exit the course.</p>" +
				"<p>If you choose to take the " + TestMessages.postTestName + ", you must complete that test and accept the score into your records. Once you begin the test, you are committed to finishing it.</p>" + 
				(CourseTree.certificateEnabled ? "<p>If you choose to accept the " + TestMessages.preTestName + " score, you may click on the \"Print Certificate\" button below.</p>" : "") +
				"<p>When you are finished you may simply exit the course by clicking the \"Exit\" button at the top of the page.</p>"
	},
	PostTestFail: {
		body: "<p>You have not met the minimum score to complete the course. Please re-launch the course to try again.</p>"
	},
	InlinePass: {
		body: "<p>Congratulations! You have successfully completed the course. Your passing grade will be added to your records.</p>" +
				TestMessages.printCert
	},
	InlineFail: {
		body: "<p>You have not met the minimum score to complete the course. Please re-launch the course to try again.</p>"
	}
});
}