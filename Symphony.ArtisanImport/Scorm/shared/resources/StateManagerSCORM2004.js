var StateManagerSCORM2004 = Class.create(StateManager, {
	getFromLms: function() {
		if (!CourseTree.isPreview) { 
			this.questions = this.getGradedQuestions(parent.currentSco);
			if (API.GetDataChunk().length == 0 || !CourseTree.bookmarkEnabled) {
				this.initializeStateManagerItems();
			} else {
				this.continueCourse();
			}
			if(API.GetStatus() != 4){ //4 = incomplete...i'm leaving this here because i'm not sure what its purpse is
				if(this.inPreTest() && this.questionsP.selected.length == this.questionsP.answered.length){
					this.nextSco();
				}
				if(this.inPostTest() && this.questions.selected.length == this.questions.answered.length){
					this.questions.answered = [];
					this.questions.correct = [];
					this.questionIndex = 0;
					this.updateStateManagerItem("PostAnswered", '');
					this.updateStateManagerItem("PostCorrect", '');
					this.updateStateManagerItem("PostQuestionIndex", '');
				}
			}			
			/*switch(API.GetStatus()) {
				case 1://Complete
				case 2://Passed
				case 3://Failed
				case 6://Not Attempted
					if (API.GetDataChunk().length == 0 || !CourseTree.bookmarkingEnabled) {
						this.initializeStateManagerItems();
					} else {
						this.continueCourse();
					}
					if(API.GetStatus() != 4){ //incomplete
	                    if(this.inPreTest() && this.questionsP.selected.length == this.questionsP.answered.length){
	                        this.nextSco();
	                    }
	                    if(this.inPostTest() && this.questions.selected.length == this.questions.answered.length){
	                        this.questions.answered = [];
	                        this.questions.correct = [];
	                        this.questionIndex = 0;
	                        this.updateStateManagerItem("PostAnswered", '');
	                        this.updateStateManagerItem("PostCorrect", '');
	                        this.updateStateManagerItem("PostQuestionIndex", '');
	                    }
					}
					break;
				case 4://Incomplete
					if (API.GetDataChunk().length > 0 && CourseTree.bookmarkingEnabled) {
						this.continueCourse();
					} else {
						this.initializeStateManagerItems();
					}
					break;
			}*/
		}		
	},
	initializeStateManagerItems: function($super) {
		$super();
		if (this.inPreTest()) {
			this.masteryScore = CourseTree.preTestScore;
		} else if (this.inPostTest()) {
			this.masteryScore = CourseTree.postTestScore;
		} else if(parent.currentSco == 'objectives') {
			this.masteryScore = 0;
		}		
		
		switch (CourseTree.completionType) {
			case CompletionType.FreeFormNav:
			case CompletionType.SequentialNav:
				this.questions.selected = this.questions;
				break;
			case CompletionType.InlineQuestions:
				this.questions.selected = this.questions;
				this.questionIndex = 0;
				this.storePreAndPostTestValues();
				break;
			case CompletionType.Test:
				this.buildStateManagerItemValues();
				break;
		}
	},
	
	shouldShowInlineReview: function(){
		return this.findCurrentSco().isLastPage() && 
			(CourseTree.completionType == CompletionType.InlineQuestions || CourseTree.completionType == CompletionType.FreeFormNav || CourseTree.completionType == CompletionType.SequentialNav)
			&& parent.currentSco != "objectives";
	},
	
	findQuestionIndex: function() {
		if (this.inPreTest()) {
			return this.questionIndexP;
		} else {
			return this.questionIndex;
		}
	},
	
	getBookmark: function(){
		return this.bookmark;
	},
	
	loadSco: function() {
		var sco;
		if (parent.currentSco == undefined) {
			parent.currentSco = CourseTree.scos[0].id;
		}
		if (!CourseTree.isPreview) {
			if (this.getBookmark().length > 0) {
				this.parseBookmark();
			}
		}
		sco = CourseTree.findSco(parent.currentSco);
		if (sco.pages.length > 0) {
			parent.currentSco = sco.id;
			parent.currentScoUrl = sco.url;
			if (parent.currentPage != 'review'){
				parent.currentPage = sco.pages[this.getPageIndex()].id;
			}
			if (!isTestSco() && CourseTree.findSco(parent.currentSco).isLastPage(parent.currentPage) && this.getGradedQuestionCount() == 0) {
				this.setScoComplete();
			}
			this.setPageElements();
		} else {
			alert('No pages exist.');
		}
		Cleanup();
		this.loadPageContent();
		return parent.currentScoUrl + parent.currentPage + ".html";		
	},
	
	//function used to update the current Page index.
	updatePageIndex: function(pageIncrement) {
		if(!this.pageIndex){
			this.pageIndex = 0;
		}
		this.pageIndex = parseInt(this.pageIndex) + pageIncrement;
		//var pageIndex = '';
		//var stateManagerItem = this.findStateManagerItemValue('PageIndex') || 0;
		//pageIndex = parseInt(stateManagerItem) + pageIncrement;
		this.updateStateManagerItem('PageIndex', this.pageIndex);
	},
	
	getPageIndex: function() {
		if(!parseInt(this.pageIndex)){
			this.pageIndex = 0;
        }
		return this.pageIndex;
	},
	
	setScoComplete: function() {
		if(CourseTree.isPreview) return;
		switch(CourseTree.completionType) {
			case CompletionType.FreeFormNav:			
			case CompletionType.SequentialNav:
				parent.SCORM2004_CallSetValue("cmi.completion_status", "completed");
				parent.SCORM2004_CallSetValue("cmi.success_status", "passed");
				parent.SCORM2004_CallSetValue("cmi.score.scaled", 1);
				parent.SCORM2004_CallCommit();
				break;
			case CompletionType.Test:
				if (parent.currentSco != 'pretest' && parent.currentSco != 'posttest') {
					parent.SCORM2004_CallSetValue("cmi.completion_status", "completed");
					parent.SCORM2004_CallSetValue("cmi.success_status", "passed");	
					parent.SCORM2004_CallCommit();                    
				}
				break;
			case CompletionType.InlineQuestions:
				if (parent.currentSco == 'objectives'){
					parent.SCORM2004_CallSetValue("cmi.completion_status", "completed");
					parent.SCORM2004_CallSetValue("cmi.success_status", "passed");	        
					parent.SCORM2004_CallCommit();
					this.saveToLms();
				}else if (CourseTree.findSco(parent.currentSco).findPage(parent.currentPage).pageType == PageType.Content) {
					if (this.getGradedQuestionCount() == 0) {
						this.completeTest(100);
					}  else {
						this.completeTest(-1);
					}
				} else {
					this.completeTest(-1);
				}
				break;				
		}
		this.saveToLms();
	},
	
	completeTest: function(testScore) {
		if(CourseTree.isPreview) return;
		if ((this.inPreTest() && CourseTree.testOutEnabled) || this.inPostTest() || CourseTree.completionType == CompletionType.InlineQuestions) {
			if (testScore == -1) {
				testScore = this.calculateScore();
				if (this.calculateScore() >= this.getMasteryScore()) {
					parent.SCORM2004_CallSetValue("cmi.completion_status", "completed");				
				}
			} else if (this.inPostTest()) {
				parent.SCORM2004_CallSetValue("cmi.completion_status", "completed");
			}
			//HACK: found this element through trial and error... need to make sure it's actually at the same level in symphony as in test track
			if (this.inPostTest()) {
				parent.parent.frames[3].document.getElementById("MenuPlaceHolder").style.display = "none";
			}
			
			if (this.inPostTest() || 
				((this.inPreTest() && CourseTree.testOutEnabled) && this.calculateScore() >= this.getMasteryScore()) ||
				CourseTree.completionType == CompletionType.InlineQuestions) {
				parent.SCORM2004_CallSetValue("cmi.score.raw", testScore);
				parent.SCORM2004_CallSetValue("cmi.score.scaled", testScore / 100);
				parent.SCORM2004_CallCommit();
			}
		}
	},
	
	//function used to return the Mastery Score for the current Sco.
	getMasteryScore: function() {
		if (CourseTree.completionType == CompletionType.InlineQuestions) {
			return CourseTree.findSco(parent.currentSco).masteryScore;
		} else {
			if (parent.currectSco == 'pretest'){
				return parseInt(this.findStateManagerItemValue('PreMasteryScore'));
			}
			else{
				return parseInt(this.findStateManagerItemValue('PostMasteryScore'));
			}
		}
	},

	previousSco: function(){
		if(!CourseTree.isPreview){
			parent.SCORM2004_CallSetValue('adl.nav.request', 'previous');
			parent.Unload();
		}
	},
	
	nextSco: function(){
		if(!CourseTree.isPreview){
			parent.SCORM2004_CallSetValue('adl.nav.request', 'continue');
			parent.Unload();
		}
	},
	
	sendInteractionsToLms: function(answer, responseCorrect) {
		if(CourseTree.isPreview) return;

		var page = this.getCurrentPage();
		var answers = page.convertAnswers(answer);
		var correctAnswers = page.findCorrectAnswers();
		var result = false;
		switch (page.questionType) {
			case QuestionType.MultipleChoice:			
			case QuestionType.AllThatApply:
				//var allAnswers = page.findAllAnswers();
				result = parent.SCORM2004_RecordMultipleChoiceInteraction(page.id, answers, responseCorrect, correctAnswers, page.questionText, 100 / this.questions.selected.length, 0, '', Date());			
				break;
			case QuestionType.TrueFalse:
				//need to send response as answer (2nd param)
				result = parent.SCORM2004_RecordTrueFalseInteraction(page.id, responseCorrect, responseCorrect, correctAnswers, page.questionText, 100 / this.questions.selected.length, 0, '', Date());
				break;
			case QuestionType.FillInTheBlank:
				result = parent.SCORM2004_RecordFillInInteraction(page.id, answer, responseCorrect, correctAnswers, page.questionText, 100 / this.questions.selected.length, 0, '', Date());
				break;
			case QuestionType.Image:  //for now, recording image type as fill in the blank
				result = parent.SCORM2004_RecordFillInInteraction(page.id, answer, responseCorrect, correctAnswers, page.questionText, 100 / this.questions.selected.length, 0, '', Date());
				break;
		}
		if (result) {
			this.addSentQuestion(page.id);
		}
	},	

	addSentQuestion: function(id) {
		if (this.questions.sent == undefined) {
			this.questions.sent = new Array();
		}
		var value = '';
		this.questions.sent.push(id);
		for (var i = 0; i < this.questions.sent.length; i++) {
			if (value.length > 0) {
				value += ';';
			}
			value += this.questions.sent[i];
		}
		this.updateStateManagerItem('QuestionsSent', value);
	}
});