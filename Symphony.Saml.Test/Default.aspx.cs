﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Web.Security;
using ComponentPro.Saml;
using ComponentPro.Saml.Binding;
using ComponentPro.Saml2;
using ComponentPro.Saml2.Binding;
using Symphony.Web.Saml;
using System.Security.Cryptography.X509Certificates;

namespace Symphony.Saml.Test
{
    public partial class _Default : Page
    {
        private string binding = SamlBindingUri.HttpArtifact;


            /// <summary>
        /// Builds an authentication request.
        /// </summary>
        /// <returns>The authentication request.</returns>
        private AuthnRequest BuildAuthenticationRequest()
        {
            
            // Create some URLs to identify the service provider to the identity provider.
            // As we're using the same endpoint for the different bindings, add a query string parameter
            // to identify the binding.
            string issuerUrl = Symphony.Web.Saml.Util.GetAbsoluteUrl(this.Context, "~/");

            string assertionConsumerServiceUrl = string.Format("{0}?{1}={2}", Symphony.Web.Saml.Util.GetAbsoluteUrl(this.Context, "~/AssertionService.aspx"), Symphony.Web.Saml.Util.BindingQueryParameter, HttpUtility.UrlEncode(binding));

            // Create the authentication request.
            AuthnRequest authnRequest = new AuthnRequest();
            authnRequest.Destination = WebConfigurationManager.AppSettings["SingleSignonIdProviderUrl"];
            authnRequest.Issuer = new Issuer(issuerUrl);
            authnRequest.ForceAuthn = false;
            authnRequest.NameIdPolicy = new NameIdPolicy(null, null, true);
            authnRequest.ProtocolBinding = binding;
            authnRequest.AssertionConsumerServiceUrl = assertionConsumerServiceUrl;

            X509Certificate2 x509Certificate = (X509Certificate2)Application[Symphony.Web.Saml.SamlParams.SPCertKey];

            authnRequest.Sign(x509Certificate);

            return authnRequest;

        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {

            // Create a logout request.
            LogoutRequest logoutRequest = new LogoutRequest();
            logoutRequest.Issuer = new Issuer(Util.GetAbsoluteUrl(this, "~/"));
            logoutRequest.NameId = new NameId(Context.User.Identity.Name);

            // Send the logout request to the IdP over HTTP redirect.
            string logoutUrl = "http://test2.portal.betraining.com/Handlers/SamlLogoutHandler.ashx";
                
            X509Certificate2 x509Certificate = (X509Certificate2)Application[Global.SPCertKey];

            // Logout locally.
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Abandon();

            logoutRequest.Redirect(Response, logoutUrl, null, x509Certificate.PrivateKey);


        }

        /// <summary>
        /// Handles the IdpLogin button to requests login at the Identify Provider site.
        /// </summary>
        /// <param name="sender">The button object.</param>
        /// <param name="e">The event arguments.</param>
        protected void btnIdPLogin_Click(object sender, EventArgs e)
        {
            // Create the authentication request.
            AuthnRequest authnRequest = BuildAuthenticationRequest();

            // Create and cache the relay state so we remember which SP resource the user wishes 
            // to access after SSO.

            string spResourceUrl = Symphony.Web.Saml.Util.GetAbsoluteUrl(this.Context, FormsAuthentication.GetRedirectUrl("", false));
            string relayState = Guid.NewGuid().ToString();
            SamlSettings.CacheProvider.Insert(relayState, spResourceUrl, new TimeSpan(1, 0, 0));

            if (string.IsNullOrEmpty(customer.Text))
            {
                customer.Text = "be";
            }

            // Send the authentication request to the identity provider over the selected binding.
            string idpUrl = string.Format("{0}?{1}={2}&customer={3}", "http://test2.portal.betraining.com/Handlers/SamlHandler.ashx", "binding", binding, customer.Text);

            switch (binding)
            {
                case SamlBindingUri.HttpRedirect:
                    X509Certificate2 x509Certificate = (X509Certificate2)Application[Global.SPCertKey];

                    authnRequest.Redirect(Response, idpUrl, relayState, x509Certificate.PrivateKey);
                    break;

                case SamlBindingUri.HttpPost:
                    authnRequest.SendHttpPost(Response, idpUrl, relayState);

                    // Don't send this form.
                    Response.End();
                    break;

                case SamlBindingUri.HttpArtifact:
                    // Create the artifact.
                    string identificationUrl = Symphony.Web.Saml.Util.GetAbsoluteUrl(this.Context, "~/");
                    Saml2ArtifactType0004 httpArtifact = new Saml2ArtifactType0004(SamlArtifact.GetSourceId(identificationUrl), SamlArtifact.GetHandle());

                    // Cache the authentication request for subsequent sending using the artifact resolution protocol.
                    SamlSettings.CacheProvider.Insert(httpArtifact.ToString(), authnRequest.GetXml(), new TimeSpan(1, 0, 0));

                    // Send the artifact.
                    httpArtifact.Redirect(Response, idpUrl, relayState);
                    break;
            }

        }
            


    }
}