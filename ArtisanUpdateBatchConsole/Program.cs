﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Symphony.Core.Controllers;
using System.Configuration;

namespace ArtisanUpdateBatchConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            log.Info("Update started");

            string rusticiCoursePath = ConfigurationManager.AppSettings["FilePathToContentRoot"];
            bool isTestMode = bool.Parse(ConfigurationManager.AppSettings["IsTestMode"]);

            (new ArtisanController()).UpdateArtisanCourses(isTestMode, null, rusticiCoursePath, log);

            log.Info("Finished");
            Console.ReadLine();
        }
    }
}
