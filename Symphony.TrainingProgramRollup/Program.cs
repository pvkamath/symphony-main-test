﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Symphony.Core;
using Symphony.Core.Controllers;
using Symphony.Core.Models;

namespace Symphony.TrainingProgramRollup
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Log.Setup();

                UserController userController = new UserController();
                userController.LoginBatch("be", "admin");

                Log.Info("Starting...");

                PortalController portalController = new PortalController();
                portalController.BackFillTrainingProgramRollup(Log.log, null, null);

                Log.Info("Complete!");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }
    }
}
