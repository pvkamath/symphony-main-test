﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Management.Smo;
using System.Configuration;
using System.Collections.Specialized;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using System.IO;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using System.Data;

namespace Symphony.DatabaseMerger
{
    static class Extensions
    {
        public static bool Match(this string[] list, string match)
        {
            return list.Any(p => match.StartsWith(p));
        }
    }
    class Program
    {
        /// <summary>
        /// The list of databases to merge. We're excluding "contentauthor2" from this list
        /// because it's not going to be merged with everything else. 
        /// </summary>
        /// <remarks>
        /// The databases are listed in the most sensible scripting order; the customer bb first, with
        /// all the user info, then the "simple" databases, then the classroom db which should only
        /// have one or two cross-references, and finally course assignment.
        /// </remarks>
        private static string[] databases = new string[]{
            "CustomerBlackBox",
            "Messaging",
            "Collaboration",
            "Classroom",
            "CourseAssignment"
        };
        
        // note that onlinetraining and contentauthor2 are left out

        static void Main(string[] args)
        {
            try
            {
                string server = "localhost";
                string root = @"C:\temp\dbscript\";

                // Create a script for each database
                foreach (string database in databases)
                {
                    // Script the database
                    ScriptDatabase(root, server, database,
                        // Exclude items that start with these values
                        new string[] { "Scorm", "DDLChangeLog", "SubSonic", "3709", "DataDictionary", "CourseStudentCount",
                            "ClassRoster", "TrainingCostReport", "CoursesRegisteredNotRequired", "PublicCoursesNew", "UserToCourse" },
                        // Don't script content for items that start with these values
                        // Basically we just ignore file content because it's so huge.
                        // NOTE: we still need to handle these manually
                        new string[] { "Import", "CourseFile", "File" },
                        true);
                }

                //// Create a combined script for all the databases
                //string outputFile = Path.Combine(root, string.Format(@"{0}\{0}.sql", server));
                //File.Delete(outputFile);
                //foreach (string database in databases)
                //{
                //    string path = Path.Combine(root, string.Format(@"{0}\{1}\{1}.sql", server, database));
                //    string text = File.ReadAllText(path);
                //    File.AppendAllText(outputFile, text);
                //}
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                    Console.WriteLine(ex.InnerException.StackTrace);
                }

            }

            foreach (string s in conflicts)
            {
                Console.WriteLine("Conflict: " + s);
            }
            Console.Read();
        }

        public static string Cleanup(string databaseName, string itemName, string lineToFix)
        {
            // avoid conflicts with "CourseType" in classroom vs course assignment
            if (databaseName == "Classroom" && (itemName == "CourseType" || itemName == "Course"))
            {
                lineToFix = lineToFix.Replace("[CourseType]", "[PresentationType]");
            }
            if (databaseName == "Classroom" && itemName == "File")
            {
                lineToFix = lineToFix.Replace("[File]", "[CourseFile]");
            }
            if (databaseName == "CustomerBlackBox" && itemName == "GetAudienceByParent")
            {
                lineToFix = lineToFix.Replace("BusinessEntityID", "CustomerID");
            }
            return lineToFix;
        }

        /// <summary>
        /// Kills any references to [otherdatabase].[dbo].[table] (and similar). Note that this *will* kill the one
        /// reference to the "Scorm" table currently in the online transcript, but we'll fix that by hand (since the script
        /// will complain when we run it anyway).
        /// </summary>
        /// <param name="line">The line to clean</param>
        /// <returns>The cleaned line</returns>
        public static string RemoveCrossReferences(string line)
        {
            foreach (string database in databases)
            {
                line = line.Replace(database + ".", "");
                line = line.Replace(string.Format("[{0}].", database), "");
            }
            return line;
        }

        /// <summary>
        /// Removes all the various "SET" statements from the SQL line - they're not needed
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static string RemoveSetStatements(string line)
        {
            line = line.Replace("SET ANSI_NULLS ON", string.Empty);
            line = line.Replace("SET QUOTED_IDENTIFIER ON", string.Empty);
            line = line.Replace("SET ANSI_NULLS OFF", string.Empty);
            line = line.Replace("SET QUOTED_IDENTIFIER OFF", string.Empty);
            return line;
        }

        public static void ScriptDatabase(string folder, string serverName, string databaseName)
        {
            ScriptDatabase(folder, serverName, databaseName, new string[] { }, new string[] { }, false);
        }

        private static HashSet<string> tableNames = new HashSet<string>();
        private static HashSet<string> conflicts = new HashSet<string>();
        public static void ScriptDatabase(string folder, string serverName, string databaseName, string[] excludeStartsWith, string[] excludeContentStartsWith, bool allData)
        {
            // Reference the SQL Server instance
            SqlConnection sqlConnection = new SqlConnection(string.Format("Data Source={0};Initial Catalog={1};Integrated Security=SSPI;", serverName, databaseName));
            ServerConnection serverConnection = new ServerConnection(sqlConnection);
            Server server = new Server(serverConnection);

            // Reference the database
            Microsoft.SqlServer.Management.Smo.Database database = server.Databases[databaseName];

            // Delete output folder if it exist
            DirectoryInfo outputFolder = new DirectoryInfo(Path.Combine(folder, string.Format(@"{0}\{1}", serverName, databaseName)));
            bool clear = false;
            while (!clear)
            {
                try
                {
                    if (outputFolder.Exists) { outputFolder.Delete(true); }

                    System.Threading.Thread.Sleep(100);

                    // Create [Output] folder
                    outputFolder.Create();

                    System.Threading.Thread.Sleep(100);

                    clear = true;
                }
                catch
                {
                }
            }

            // Create [Tables] output folder
            DirectoryInfo tablesOutputFolder = new DirectoryInfo(Path.Combine(outputFolder.FullName, "Tables"));
            tablesOutputFolder.Create();

            StringCollection fileList = new StringCollection();

            // Loop tables
            foreach (Table table in database.Tables)
            {
                if (!table.IsSystemObject)
                {

                    if (excludeStartsWith.Match(table.Name))
                    {
                        Console.WriteLine(string.Format("Skipping {0}...", table.Name));
                        continue;
                    }

                    if (tableNames.Contains(table.Name))
                    {
                        conflicts.Add(table.Name);
                        Console.WriteLine(string.Format("Conflict hit, table: " + table.Name));
                    }
                    else
                    {
                        tableNames.Add(table.Name);
                    }

                    Console.Write(string.Format("Scripting table '{0}' structure...", table.Name));

                    // Generate script
                    // Include primairykey, foreignkey, constaints, defaults etc.
                    // Exclude collation information
                    StringBuilder resultScript = new StringBuilder(string.Empty);

                    // Only create table when it does not exist
                    string tableName = Cleanup(databaseName, table.Name, table.Name);
                    resultScript.AppendFormat("IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'U'))", tableName).Append(Environment.NewLine);
                    resultScript.AppendLine("BEGIN");

                    ScriptingOptions options = new ScriptingOptions();
                    options.DriAll = true;
                    options.NoCollation = true;
                    StringCollection scriptLines = table.Script(options);

                    // Add script to file content
                    foreach (string scriptLine in scriptLines)
                    {
                        string line = scriptLine;
                        line = Cleanup(databaseName, table.Name, scriptLine);
                        line = RemoveSetStatements(line);
                        line = RemoveCrossReferences(line);
                        resultScript.AppendLine(line.Trim());
                    }

                    resultScript.AppendLine("END");

                    // Write file content to disk
                    string fileName = string.Format("{0}.sql", table.Name);
                    string path = Path.Combine(tablesOutputFolder.FullName, fileName);
                    File.WriteAllText(path, resultScript.ToString());
                    fileList.Add(path);

                    Console.WriteLine("complete.");
                }
            }

            // Create [Content] output folder
            DirectoryInfo contentOutputFolder = new DirectoryInfo(Path.Combine(outputFolder.FullName, "Content"));
            contentOutputFolder.Create();

            // Loop tables for content
            foreach (Table table in database.Tables)
            {
                if (!table.IsSystemObject)
                {
                    if (excludeStartsWith.Match(table.Name) || excludeContentStartsWith.Match(table.Name))
                    {
                        Console.WriteLine(string.Format("Skipping content in {0}...", table.Name));
                        continue;
                    }

                    Console.Write(string.Format("Scripting table '{0}' content...", table.Name));

                    // Only include lookup tables unless overridden
                    if (table.ForeignKeys.Count == 0 || allData)
                    {

                        // Generate script
                        // Include content in script
                        // Exclude table schema (table creation etc.) and Dri (foreignkeys etc.)
                        StringBuilder resultScript = new StringBuilder(string.Empty);

                        // Only insert data when table is empty
                        resultScript.AppendFormat("IF NOT EXISTS (SELECT 1 FROM [dbo].[{0}])", table.Name).Append(Environment.NewLine);
                        resultScript.AppendLine("BEGIN");

                        Scripter scripter = new Scripter(server);
                        ScriptingOptions options = new ScriptingOptions();
                        options.DriAll = false;
                        options.ScriptSchema = false;
                        options.ScriptData = true;
                        scripter.Options = options;

                        // Add script to file content
                        bool hasContent = false;
                        foreach (string scriptLine in scripter.EnumScript(new Urn[] { table.Urn }))
                        {
                            hasContent = true;

                            string line = scriptLine;
                            line = RemoveSetStatements(line);
                            line = RemoveCrossReferences(line);
                            line = Cleanup(databaseName, table.Name, line);
                            resultScript.AppendLine(line.Trim());
                        }
                        // Skip tables without content
                        if (!hasContent)
                        {
                            Console.WriteLine("no content found.");
                            continue;
                        }

                        resultScript.AppendLine("END");

                        // Write file content to disk
                        string fileName = string.Format("{0}.sql", table.Name);
                        string path = Path.Combine(contentOutputFolder.FullName, fileName);
                        File.WriteAllText(path, resultScript.ToString());
                        fileList.Add(path);
                    }

                    Console.WriteLine("complete.");
                }
            }

            // create a single file with the content
            string contentFile = Path.Combine(outputFolder.FullName, "content.sql");
            File.Delete(contentFile);
            foreach (FileInfo fi in contentOutputFolder.GetFiles())
            {
                File.AppendAllText(contentFile, File.ReadAllText(fi.FullName));
            }


            // Create [Stored Procedures] output folder
            DirectoryInfo storedProceduresOutputFolder = new DirectoryInfo(Path.Combine(outputFolder.FullName, "Stored Procedures"));
            storedProceduresOutputFolder.Create();

            // Loop stored procedures
            foreach (StoredProcedure storedProcedure in database.StoredProcedures)
            {
                // Exclude system stored procedures
                if (!storedProcedure.IsSystemObject)
                {
                    if (excludeStartsWith.Match(storedProcedure.Name))
                    {
                        Console.WriteLine(string.Format("Skipping {0}...", storedProcedure.Name));
                        continue;
                    }

                    Console.Write(string.Format("Scripting stored procedure {0}...", storedProcedure.Name));

                    // Generate script
                    // Include drop statements
                    StringBuilder resultScript = new StringBuilder(string.Empty);

                    // Append drop statement
                    resultScript.AppendFormat("IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'P', N'PC'))", storedProcedure.Name).Append(Environment.NewLine);
                    resultScript.AppendLine("BEGIN");
                    resultScript.AppendFormat("    DROP PROCEDURE [dbo].[{0}]", storedProcedure.Name).Append(Environment.NewLine);
                    resultScript.AppendLine("END");
                    resultScript.AppendLine("GO");

                    // Add script to file content
                    foreach (string scriptLine in storedProcedure.Script())
                    {
                        string line = scriptLine;
                        line = RemoveSetStatements(line);
                        line = RemoveCrossReferences(line);
                        resultScript.AppendLine(line.Trim());
                    }

                    resultScript.AppendLine("GO");

                    // Write file content to disk
                    string fileName = string.Format("{0}.sql", storedProcedure.Name);
                    string path = Path.Combine(storedProceduresOutputFolder.FullName, fileName);
                    File.WriteAllText(path, resultScript.ToString());
                    fileList.Add(path);

                    Console.WriteLine("complete.");
                }
            }

            // Create [Functions] output folder
            DirectoryInfo functionsOutputFolder = new DirectoryInfo(Path.Combine(outputFolder.FullName, "Functions"));
            functionsOutputFolder.Create();

            // Loop stored procedures
            foreach (UserDefinedFunction function in database.UserDefinedFunctions)
            {


                // Exclude system functions
                if (!function.IsSystemObject)
                {
                    if (excludeStartsWith.Match(function.Name))
                    {
                        Console.WriteLine(string.Format("Skipping {0}...", function.Name));
                        continue;
                    }

                    Console.Write(string.Format("Scripting function {0}...", function.Name));

                    // Generate script
                    // Include drop statements
                    StringBuilder resultScript = new StringBuilder(string.Empty);

                    // Append drop statement
                    resultScript.AppendFormat("IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))", function.Name).Append(Environment.NewLine);
                    resultScript.AppendLine("BEGIN");
                    resultScript.AppendFormat("    DROP FUNCTION [dbo].[{0}]", function.Name).Append(Environment.NewLine);
                    resultScript.AppendLine("END");
                    resultScript.AppendLine("GO");

                    // Add script to file content
                    foreach (string scriptLine in function.Script())
                    {
                        string line = scriptLine;
                        line = RemoveSetStatements(line);
                        line = RemoveCrossReferences(line);
                        resultScript.AppendLine(line.Trim());
                    }

                    resultScript.AppendLine("GO");

                    // Write file content to disk
                    string fileName = string.Format("{0}.sql", function.Name);
                    string path = Path.Combine(functionsOutputFolder.FullName, fileName);
                    File.WriteAllText(path, resultScript.ToString());
                    fileList.Add(path);

                    Console.WriteLine("complete.");
                }
            }

            // Create [Views] output folder
            DirectoryInfo viewsOutputFolder = new DirectoryInfo(Path.Combine(outputFolder.FullName, "Views"));
            viewsOutputFolder.Create();

            // Loop views
            foreach (View view in database.Views)
            {
                if (!view.IsSystemObject)
                {
                    if (excludeStartsWith.Match(view.Name))
                    {
                        Console.WriteLine(string.Format("Skipping {0}...", view.Name));
                        continue;
                    }

                    Console.Write(string.Format("Scripting view '{0}'...", view.Name));

                    // Generate script
                    StringBuilder resultScript = new StringBuilder(string.Empty);

                    // Only create table when it does not exist
                    resultScript.AppendFormat("IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'V'))", view.Name).Append(Environment.NewLine);
                    resultScript.AppendLine("EXEC dbo.sp_executesql @statement = N'");

                    ScriptingOptions options = new ScriptingOptions();
                    options.DriAll = true;
                    options.NoCollation = true;
                    StringCollection scriptLines = view.Script(options);

                    // Add script to file content
                    foreach (string scriptLine in scriptLines)
                    {
                        string line = scriptLine;
                        line = RemoveSetStatements(line);
                        line = RemoveCrossReferences(line);
                        // escape quotes
                        line = line.Replace("'", "''");
                        resultScript.AppendLine(line.Trim());
                    }

                    resultScript.AppendLine("';");

                    // Write file content to disk
                    string fileName = string.Format("{0}.sql", view.Name);
                    string path = Path.Combine(viewsOutputFolder.FullName, fileName);
                    File.WriteAllText(path, resultScript.ToString());
                    fileList.Add(path);

                    Console.WriteLine("complete.");
                }
            }

            Console.Write(string.Format("Writing '{0}.sql'...", database.Name));

            string output = Path.Combine(outputFolder.FullName, "schema.sql");
            File.Delete(output);
            foreach (DirectoryInfo di in new DirectoryInfo[] { tablesOutputFolder, viewsOutputFolder, storedProceduresOutputFolder, functionsOutputFolder })
            {
                foreach (FileInfo fi in di.GetFiles())
                {
                    File.AppendAllText(output, File.ReadAllText(fi.FullName));
                }
            }

            Console.WriteLine("complete.");
        }
    }
}
