﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Symphony.Core.Controllers;
using HtmlAgilityPack;
using Symphony.Core.Models;
using System.Net;
using SubSonic;
using System.Reflection;
using Symphony.Core.Data;

namespace Symphony.ArtisanImport2
{
    class Program
    {
        static ContentAuthor2DataContext db = new ContentAuthor2DataContext();

        static void Main(string[] args)
        {
            try
            {
                //PropertyInfo[] assetProperties = typeof(Symphony.Core.Models.ArtisanAsset).GetProperties();
                foreach (string company in Directory.GetDirectories(@"C:\temp\exports"))
                {
                    int companyId = int.Parse(Path.GetFileName(company));
                    Symphony.Core.Data.Customer c = new Symphony.Core.Data.Customer(companyId);
                    foreach (string course in Directory.GetFiles(company))
                    {
                        Import(course, companyId, companyId, string.Empty); //int.Parse(companyId));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Imports a course from a json file
        /// </summary>
        /// <param name="courseFile">The course json file</param>
        /// <param name="customerId">The target customer to import the course into</param>
        /// <param name="queryAsCustomerId">For assets, the id of the customer who owns the asset</param>
        /// <param name="prefix">A prefix for the name of the course, so if you import into a "test" bank, you can identify the courses</param>
        static void Import(string courseFile, int customerId, int queryAsCustomerId, string prefix)
        {
            string courseJson = System.IO.File.ReadAllText(courseFile);
            Symphony.Core.Models.ArtisanCourse course = Symphony.Core.Utilities.Deserialize<Symphony.Core.Models.ArtisanCourse>(courseJson);
            course.Name = prefix + course.Name;
            ArtisanController controller = new ArtisanController();


            Symphony.Core.Models.SingleResult<Symphony.Core.Models.ArtisanCourse> result = controller.SaveCourse(customerId, 0, -1, course);

            ArtisanPageCollection allPages = Select.AllColumnsFrom<Symphony.Core.Data.ArtisanPage>()
                .Where("CourseID").IsEqualTo(result.Data.Id)
                .ExecuteAsCollection<ArtisanPageCollection>();

            foreach (Symphony.Core.Data.ArtisanPage page in allPages)
            {
                // TODO: Parse out assets, change links, embed within page wrapper
                // Add "http://service.betraining.com" to the beginning
                // of the asset path to retrieve it from the server
                // Use the code in PackageBuilder.BuildPage() for parsing
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(page.Html);

                foreach (string selector in new string[] { "//*[@src]", "//*[@href]", "//*[@name='movie' and @value]" })
                {
                    HtmlNodeCollection nodes = document.DocumentNode.SelectNodes(selector);
                    if (nodes != null)
                    {
                        foreach (HtmlNode node in nodes)
                        {
                            HtmlAttribute attribute = node.Attributes["href"] ?? node.Attributes["src"] ?? node.Attributes["value"];
                            string target = attribute.Value;
                            if (target.StartsWith("http://artisan.betraining.com/"))
                            {
                                target = target.Replace("http://artisan.betraining.com/", "");
                            }
                            if (target.StartsWith("Assets"))
                            {
                                string assetId = Path.GetFileNameWithoutExtension(target);

                                // look up the asset in contentauthor2
                                var assets =
                                    from asset in db.Assets
                                    where asset.ID == int.Parse(assetId)
                                    select asset;

                                var thisAsset = assets.FirstOrDefault();
                                if (thisAsset == null)
                                {
                                    continue;
                                }

                                // look up the asset by filename; ignore "from imports" to avoid possible collisions
                                Symphony.Core.Models.ArtisanAsset existing = Select.AllColumnsFrom<Symphony.Core.Data.ArtisanAsset>()
                                    .Where(Symphony.Core.Data.ArtisanAsset.Columns.Filename).IsEqualTo(thisAsset.FileName)
                                    .And(Symphony.Core.Data.ArtisanAsset.Columns.FromImport).IsEqualTo(false)
                                    .ExecuteSingle<Symphony.Core.Models.ArtisanAsset>();

                                // not found, so download and re-upload it
                                if (existing == null)
                                {
                                    /*string url = "http://service.betraining.com/" + target;
                                    WebClient wc = new WebClient();
                                    byte[] bytes = wc.DownloadData(url);

                                    existing = controller.UploadArtisanAsset(customerId, thisAsset.FileName, thisAsset.Title, thisAsset.Description, bytes).Data;*/
                                }

                                if (existing != null)
                                {
                                    Symphony.Core.Models.ArtisanAsset model = controller.GetAsset(queryAsCustomerId, 0, existing.Id).Data;
                                    //TODO: flash elements aren't always found, see John's test course and asset id 8926 (swf)
                                    //TODO: if the asset does't exist in the filesystem in the new asset location, upload it as well

                                    // just change the path
                                    if (model != null)
                                    {
                                        attribute.Value = model.Path;
                                    }
                                }
                            }
                        }
                    }
                }
                page.Html = document.DocumentNode.WriteTo();
            }
            allPages.SaveAll();
        }
    }
}
