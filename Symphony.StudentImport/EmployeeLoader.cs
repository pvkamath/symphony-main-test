﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Web.Security;
using System.Text.RegularExpressions;

using System.Collections.Generic;

using Symphony.Core;
using Symphony.Core.Models;
using Symphony.Core.Models.Salesforce;
using Symphony.Core.Controllers;
using Symphony.Core.Controllers.Salesforce;



namespace StudentImport
{


    // --------------------------------------------------------
    // Loader for Employee Records
    //  The process (order of the method calls) is controled by the Program.css module
    // --------------------------------------------------------
    public class EmployeeLoader
    {

        Options opts = null;

        // Main Inbound Employee information to be loaded
        private DataTable inEmployees = null;
        String customerID = "-1";
        ArrayList errorMessages = null;

        SqlConnection db = null;
        DataTable oldEmployees = null;
        string baseDirName = "";
        string programName = "Import";

        // Lookup tables to validate JobRole and Location assignments
        DataTable inLocations = null;
        DataTable inJobRoles = null;
        DataTable inAudiences = null;
        Hashtable empCodeList = null;

        // Silent mode ( no console output )
        Boolean silent = false;

        // Noop mode ( do not make updates. Just report and produce logs/files )
        Boolean noop = false;

        // Ignore stat checks
        Boolean ignoreWarnings = false; // Turn this on to stop reasonability checks on changes. (If there are a lot of changes )

        // Job Queue interactions
        Queue jobQueue = null;

        // Standard Stats
        StatInfo stats = null;


        // -------------------------------------------------------------
        // Constructor 
        // Parameter dt is the inbound file data loaded into a DataTable
        // -------------------------------------------------------------
        public EmployeeLoader(SqlConnection dbCon, Options optvalues, Queue q)
        {



            this.opts = optvalues; // Holds runtime options and rules
            //this.inEmployees = dt;
            this.customerID = this.opts.getCustomerID();
            this.db = dbCon;
            this.errorMessages = new ArrayList();
            this.baseDirName = this.opts.getBaseDirectory();
            this.jobQueue = q;
            this.opts = optvalues; // Holds runtime options and rules

            this.stats = new StatInfo();
            this.empCodeList = new Hashtable();


        }

        // --------------------------------------------------------
        // Silent mode means there is no console output
        // * TODO * Put in another way to output errors
        //  ? ErrorList attribute ?
        // Parameters:
        //  isSilent = true  ( no output )
        //          = false  ( output )
        // ----------------------------------------------------------
        public void setSilentOutput(Boolean isSilent)
        {
            this.silent = isSilent;
        }

        // --------------------------------------------------------
        // Silent mode means there is no console output
        // * TODO * Put in another way to output errors
        //  ? ErrorList attribute ?
        // Parameters:
        //  isSilent = true  ( no output )
        //          = false  ( output )
        // ----------------------------------------------------------
        public void setNoop(Boolean isNoop)
        {
            this.noop = isNoop;
        }

        // ----------------------------------------------
        // Return the existing Employee records.
        // ----------------------------------------------
        public DataTable getExistingRecords()
        {
            return this.oldEmployees;
        }
        // ----------------------------------------------
        // Return the inbound/modified JobRole records.
        // ----------------------------------------------
        public DataTable getRecords()
        {
            return this.inEmployees;
        }

        // ----------------------------------------------
        // Return the process statistics
        // ----------------------------------------------
        public StatInfo getStats()
        {
            return this.stats;
        }
        
        // --------------------------------------------------
        // Load the CSV File data
        // --------------------------------------------------
        public int loadInboundCSVFile(String baseDir, String employeeFileName)
        {

            Utility.ConstructSchema(baseDir, employeeFileName);
            this.inEmployees = Utility.ParseCSV(baseDir + employeeFileName);

            if (this.inEmployees == null)
            {
                return -1; // Fail
            }
            return 0;

        }


        // --------------------------------------------------
        // Load the Excel File data (optional)
        // --------------------------------------------------
        public int loadInboundExcelFile(String baseDir, String employeeFileName, String startrow,String sheetname)
        {

            Utility.ConstructExcelSchema(baseDir, employeeFileName, startrow, sheetname);
            this.inEmployees = Utility.ParseExcel(baseDir + employeeFileName, startrow, sheetname);

            if (this.inEmployees == null)
            {
                return -1; // Fail
            }
            return 0;

        }


        // -----------------------------------------------------------
        // sanitizeColumns
        // brief: Renames the column headers
        // to make sure column names are consistent.
        // 1. Change to consistent Case.
        // 2. Trims
        // 3. Adds in some processing columns
        // * This is the place to catch common misspellings if needed
        // (example:  name turns into Name, NAME turns into Name )
        // -----------------------------------------------------------
        public int sanitizeColumns()
        {

            //Copy to new column rule ( avoids the nasty read only column issue when loading Excel )
            // Instead of renaming a column, it adds one and copies the value into it from the original. ( which may be renamed later )
            for (int colNum = 1; colNum < 16; colNum++)
            {
                String ruleName = "Rule_14_CopyColumn" + colNum;
                String ruleValue = this.opts.getRuleValue(ruleName).ToLower();
                if (ruleValue.Length > 1)
                {
                    // This configuration has a column copy rule ( up to 15 of them possible )
                    Console.WriteLine(ruleName + " found. " + ruleValue);
                    string[] cols = ruleValue.Split('|');
                    if (cols.Length > 1)
                    {
                        string fromCol = cols[0];
                        string toCol = cols[1];

                        int colIndex = this.inEmployees.Columns.IndexOf(fromCol);
                        Console.WriteLine(ruleName + " " + fromCol + " to " + toCol + " index: " + colIndex);
                        if (colIndex >= 0)
                        {
                            // Add here
                            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, toCol, "System.String");
                   
                            //Not the best way or place to do this copy, but this sould be rarely needed.

                            for (int curRow = 0; curRow < this.inEmployees.Rows.Count; curRow++)
                            {
                  
                                    this.inEmployees.Rows[curRow][toCol] = this.inEmployees.Rows[curRow][fromCol].ToString().Trim();
                            }

                        }
                        else
                        {
                            Console.WriteLine("Column Copy: " + fromCol + " not found in Data.");
                        }

                    }

                } // End Rule Found

            } // End Copy to new Column  loop

            // Rename column rules get processed
            for (int colNum = 1; colNum < 16; colNum++)
            {
                String ruleName = "Rule_10_RenameColumn" + colNum;
                String ruleValue = this.opts.getRuleValue(ruleName).ToLower();
                if (ruleValue.Length > 1)
                {
                    // This configuration has a column rename rule ( up to 15 of them possible )
                    Console.WriteLine(ruleName + " found. " + ruleValue);
                    string[] cols = ruleValue.Split('|');
                    if (cols.Length > 1)
                    {
                        string fromCol = cols[0];
                        string toCol = cols[1];

                        int colIndex = this.inEmployees.Columns.IndexOf(fromCol);
                        Console.WriteLine(ruleName + " " + fromCol + " to " + toCol + " index: " + colIndex);
                        if (colIndex >= 0)
                        {
                            this.inEmployees.Columns[colIndex].ColumnName = toCol;
                        }
                        else
                        {
                            Console.WriteLine("Column Rename: " + fromCol + " not found in Data.");
                        }

                    }

                } // End Rule Found

            } // End Rule Column rename loop

            // Standard and customized columname sanitzation 
            for (int curCol = 0; curCol < inEmployees.Columns.Count; curCol++)
            {
                String colName = inEmployees.Columns[curCol].ColumnName.ToLower().Trim();

          


                if (this.opts.getRuleValue("Rule_CNBANKPA").ToLower().Equals("yes"))
                {
                    Console.WriteLine("CNBANKPA Rule." + colName);
                    // Change First Email Address column to UserName ( Not always the case. CSV loader will rename the
                    // column to Emp#csv.Email Address  and Emp#csv.Email Address1  in this case.
                    if (colName.ToLower().ToString().Contains("email address") && curCol == 0)
                    {
                        Console.WriteLine("CNBANKPA Rule: Changing Email Address to UserName ");
                        inEmployees.Columns[curCol].ColumnName = "UserName";
                    }
                    if (colName.ToLower().ToString().Contains("emailaddress") && curCol == 0)
                    {
                        Console.WriteLine("CNBANKPA Rule: Changing emailaddress to UserName ");
                        inEmployees.Columns[curCol].ColumnName = "UserName";
                    }
                    if (colName.ToLower().ToString().Contains("email address1"))
                    {
                        Console.WriteLine("CNBANKPA Rule: Changing Email Address1 to Email ");
                        inEmployees.Columns[curCol].ColumnName = "Email";
                    }
                    if (colName.ToLower().ToString().Contains("emailaddress1"))
                    {
                        Console.WriteLine("CNBANKPA Rule: Changing emailaddress1 to Email ");
                        inEmployees.Columns[curCol].ColumnName = "Email";
                    }

                    // Org Level 2   is the Location
                    if (colName.ToLower().ToString().Equals("org level 2"))
                    {
                        Console.WriteLine("CNBANKPA Rule: Changing org level 2 to LocationCode ");
                        inEmployees.Columns[curCol].ColumnName = "LocationCode";
                    }
                    if (colName.ToLower().ToString().Equals("orglevel2"))
                    {
                        Console.WriteLine("CNBANKPA Rule: Changing orglevel2 Address1 to LocationCode ");
                        inEmployees.Columns[curCol].ColumnName = "LocationCode";
                    }

                    // Job Title   is the Location
                    if (colName.ToLower().ToString().Equals("job title"))
                    {
                        inEmployees.Columns[curCol].ColumnName = "JobRoleCode";
                    }
                    if (colName.ToLower().ToString().Equals("jobtitle"))
                    {
                        inEmployees.Columns[curCol].ColumnName = "JobRoleCode";
                    }
                    // Last Hire Date is Hire Date
                    if (colName.ToLower().ToString().Equals("last hire date"))
                    {
                        inEmployees.Columns[curCol].ColumnName = "HireDate";
                    }
                    if (colName.ToLower().ToString().Equals("lasthiredate"))
                    {
                        inEmployees.Columns[curCol].ColumnName = "HireDate";
                    }
                    // Supervisor
                    if (colName.ToLower().ToString().Contains("last name (supervisor)"))
                    {
                        inEmployees.Columns[curCol].ColumnName = "SupervisorLastName";
                    }
                    if (colName.ToLower().ToString().Contains("first name (supervisor)"))
                    {
                        inEmployees.Columns[curCol].ColumnName = "SupervisorFirstName";
                    }
                    if (colName.ToLower().ToString().Contains("lastnamesupervisor"))
                    {
                        inEmployees.Columns[curCol].ColumnName = "SupervisorLastName";
                    }
                    if (colName.ToLower().ToString().Contains("firstnamesupervisor"))
                    {
                        inEmployees.Columns[curCol].ColumnName = "SupervisorFirstName";
                    }
                    
                } // End Custom Rule


                //NBOFIN Custom File
                if (this.opts.getRuleValue("Rule_CustomNBOFIN").ToLower().Equals("yes"))
                {
                    Console.WriteLine("Rule_CustomNBOFIN Rule:" + colName);
                    // Change Home Department Desc to Location
                    if (colName.ToLower().ToString().Contains("homedepartmentdesc") )
                    {
                        Console.WriteLine("Rule_CustomNBOFIN Rule: Changing Home Department Desc ");
                        inEmployees.Columns[curCol].ColumnName = "LocationCode";
                    }
             
                }
                //Bayview  Custom File
                if (this.opts.getRuleValue("Rule_CustomBayview").ToLower().Equals("yes"))
                {
                    //Console.WriteLine("Rule_CustomBayview Rule:" + colName);
                    // Change Home Department Desc to Location
                    if (colName.ToLower().ToString().Contains("work e-mail") ||
                        colName.ToLower().ToString().Contains("worke-mail"))
                    {
                        Console.WriteLine("Rule_CustomBayview Rule: Changing Work E-Mail to Email ");
                        inEmployees.Columns[curCol].ColumnName = "email";
                    }
                    if (colName.ToLower().ToString().Contains("job title") ||
                       colName.ToLower().ToString().Contains("jobtitle") )
                    {
                        Console.WriteLine("Rule_CustomBayview Rule: Changing Job Title to JobRoleCode ");
                        inEmployees.Columns[curCol].ColumnName = "jobrolecode";
                    }
                    if (colName.ToLower().ToString().Contains("home department code"))
                    {
                        Console.WriteLine("Rule_CustomBayview Rule: Changing Home Department ");
                        inEmployees.Columns[curCol].ColumnName = "homedepartmentcode";
                    }
                    if (colName.ToLower().ToString().Contains("home department name"))
                    {
                        Console.WriteLine("Rule_CustomBayview Rule: Changing Home Department Name");
                        inEmployees.Columns[curCol].ColumnName = "homedepartmentname";
                    }

                    // Change File Number to employeenumber
                    if (colName.ToLower().ToString().Contains("file number") || colName.ToLower().ToString().Contains("filenumber"))
                    {
                        Console.WriteLine("Rule_CustomBayview Rule: File Number to Employee Number ");
                        inEmployees.Columns[curCol].ColumnName = "employeenumber";
                    }

                }


                // --------------------------------------------------------------------------
                if (this.opts.getRuleValue("Rule_CustomCadence").ToLower().Equals("yes"))
                {

                    //Convert 'Employee First Name' to 'First Name' if it does not exist already
                    if ((colName.ToLower().ToString().Contains("employee first name") || 
                            (colName.ToLower().ToString().Contains("employeefirstname")) ) &&
                                !(inEmployees.Columns.Contains("FirstName")))
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: Changing Employee First Name to FirstName:" + colName);
                        inEmployees.Columns[curCol].ColumnName = "FirstName";
                        continue; // Next column. There are some common rules that mess this up.
                    }


                    // Prep for Change Home Department Code to LocationCode
                    if (colName.ToLower().ToString().Contains("location") && !(colName.ToLower().ToString().Contains("code"))
                          && !(colName.ToLower().ToString().Contains("name")) )
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: Changing Location to ADPLoc:" + colName);
                        inEmployees.Columns[curCol].ColumnName = "ADPLoc";
                        continue; // Next column. There are some common rules below that mess this up.
                    }
                    // Prep Change Home Department Code to LocationCode (replace other location name/code fields )
                    if (colName.ToLower().ToString().Contains("location") && ((colName.ToLower().ToString().Contains("code"))
                          || (colName.ToLower().ToString().Contains("name"))))
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: Changing Location to ADPLoc:" + colName);
                        inEmployees.Columns[curCol].ColumnName = "ADPLoc-"  + colName;
                        continue; // Next column. There are some other common rules below that mess this up.
                    }
                    
                    // Change Home Department Code to LocationCode
                    if (colName.ToLower().ToString().Contains("home department code") || colName.ToLower().ToString().Contains("homedepartmentcode"))
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: Changing Home Department Code ");
                        inEmployees.Columns[curCol].ColumnName = "LocationCode";
                    }
                    // Change Home Department Desc to LocationName
                    if ( (colName.ToLower().ToString().Contains("home department")
                         && !(colName.ToLower().ToString().Contains("code"))) || 
                         (colName.ToLower().ToString().Contains("homedepartment")
                         && !(colName.ToLower().ToString().Contains("code")))   )
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: Changing Home Department Desc ");
                        inEmployees.Columns[curCol].ColumnName = "LocationName";
                    }
                    // Change File Number to username
                    if (colName.ToLower().ToString().Contains("file number") || colName.ToLower().ToString().Contains("filenumber"))
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: File Number to UserName ");
                        inEmployees.Columns[curCol].ColumnName = "Username";
                    }
                    // Change Job code to JobRoleCode
                    if (colName.ToLower().ToString().Contains("job code") || colName.ToLower().ToString().Contains("jobcode"))
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule:Job code to JobRoleCode ");
                        inEmployees.Columns[curCol].ColumnName = "JobRoleCode";
                    }  
                    
                    // Change Job title to JobRoleName
                    if ( (colName.ToLower().ToString().Contains("job title") || colName.ToLower().ToString().Contains("jobtitle")) 
                        && (!(colName.ToLower().ToString().Contains("eff") ) ))
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule:Job title to JobRoleName ");
                        inEmployees.Columns[curCol].ColumnName = "JobRoleName";
                    }
                    // Change Manager ID to SupervisorCode
                    if (colName.ToLower().ToString().Contains("manager id") || colName.ToLower().ToString().Contains("managerid"))
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: Manager ID to SupervisorCode");
                        inEmployees.Columns[curCol].ColumnName = "SupervisorCode";
                    }
                    // Change Date of hire
                    if (colName.ToLower().ToString().Contains("date of hire") || colName.ToLower().ToString().Contains("dateofhire"))
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: Date of Hire to HireDate");
                        inEmployees.Columns[curCol].ColumnName = "HireDate";
                    }
                    // Change Email
                    if (colName.ToLower().ToString().Contains("work email") || colName.ToLower().ToString().Contains("workemail")
                            || colName.ToLower().ToString().Contains("e-mail") )
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: Work Email to Email");
                        inEmployees.Columns[curCol].ColumnName = "Email";
                    }
                    // Change Status
                    if (colName.ToLower().ToString().Contains("employee status type") || colName.ToLower().ToString().Contains("employeestatustype") || colName.ToLower().ToString().Contains("statustype") || colName.ToLower().ToString().Contains("status type"))
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: Employee Status Type");
                        inEmployees.Columns[curCol].ColumnName = "Status";
                    }
                    // Change Audience
                    if (colName.ToLower().ToString().Contains("line of business") || colName.ToLower().ToString().Contains("lineofbusiness") )
                    {
                        Console.WriteLine("Rule_CustomerCadence Rule: line of business to Audience");
                        inEmployees.Columns[curCol].ColumnName = "AudienceCode";
                    }
                }
                // --------------------------------------------------------------------------

          



                if (colName.ToLower().ToString().Equals("employeenumber") || colName.ToLower().ToString().Equals("employeenum") || colName.ToLower().ToString().Equals("employee number"))
                {
                    inEmployees.Columns[curCol].ColumnName = "EmployeeNumber";
                }
                if (colName.ToLower().ToString().Equals("username") || colName.ToLower().ToString().Equals("user id"))
                {
                    inEmployees.Columns[curCol].ColumnName = "Username";
                }
                if (colName.ToLower().ToString().Equals("locationcode") || colName.ToLower().ToString().Equals("location code") || colName.ToLower().ToString().Equals("loc code") || colName.ToLower().ToString().Equals("location") || colName.ToLower().ToString().Equals("loc num"))
                {
                    Console.WriteLine("Column name: " + colName.ToLower() );
                    inEmployees.Columns[curCol].ColumnName = "LocationCode";
                }
                if (colName.ToLower().ToString().Equals("firstname") || colName.ToLower().ToString().Equals("fname") || colName.ToLower().ToString().Equals("first name"))
                {
                    inEmployees.Columns[curCol].ColumnName = "FirstName";
                }
                if (colName.ToLower().ToString().Equals("lastname") || colName.ToLower().ToString().Equals("lname") || colName.ToLower().ToString().Equals("last name"))
                {
                    inEmployees.Columns[curCol].ColumnName = "LastName";
                }
                if (colName.ToLower().ToString().Equals("middlename") || colName.ToLower().ToString().Equals("mname") || colName.ToLower().ToString().Equals("middle name") || colName.ToLower().ToString().Equals("middle initial"))
                {
                    inEmployees.Columns[curCol].ColumnName = "MiddleName";
                }
                if (colName.ToLower().ToString().Equals("hiredate") || colName.ToLower().ToString().Equals("hiredt") || colName.ToLower().ToString().Equals("hire date") || colName.ToLower().ToString().Equals("date of hire") )
                {
                    inEmployees.Columns[curCol].ColumnName = "HireDate";
                }
                if (colName.ToLower().ToString().Equals("email") || colName.ToLower().ToString().Equals("emailaddr") || colName.ToLower().ToString().Equals("emailaddress") || colName.ToLower().ToString().Equals("e-mail address"))
                {
                    inEmployees.Columns[curCol].ColumnName = "Email";
                }
                if (colName.ToLower().ToString().Equals("jobrolecode") || colName.ToLower().ToString().Equals("job role code") )
                {
                    inEmployees.Columns[curCol].ColumnName = "JobRoleCode";
                }
                if (colName.Equals("status") || colName.ToLower().ToString().Equals("statuscode"))
                {
                    inEmployees.Columns[curCol].ColumnName = "Status";
                }
                if (colName.ToLower().ToString().Equals("supervisorcode") || colName.ToLower().ToString().Equals("supervisor code"))
                {
                    inEmployees.Columns[curCol].ColumnName = "SupervisorCode";
                }
                if (colName.ToLower().ToString().Equals("password") || colName.ToLower().ToString().Equals("pwd") || colName.ToLower().ToString().Equals("passcode"))
                {
                    inEmployees.Columns[curCol].ColumnName = "Password";
                }
                if (colName.Equals("New Hire Indicator") || colName.ToLower().ToString().Equals("new hire indicator"))
                {
                    inEmployees.Columns[curCol].ColumnName = "NewHireIndicator";
                }
                if (colName.ToLower().ToString().Equals("dateofhire/rehire") || colName.ToLower().ToString().Equals("date of hire/rehire"))
                {
                    inEmployees.Columns[curCol].ColumnName = "HireDate";
                }
                if (colName.ToLower().ToString().Equals("manager first name") )
                {
                    inEmployees.Columns[curCol].ColumnName = "managerfirstname";
                }
                if (colName.ToLower().ToString().Equals("manager last name"))
                {
                    inEmployees.Columns[curCol].ColumnName = "managerlastname";
                }
                if (colName.ToLower().ToString().Equals("reportingsupervisorcode") || colName.ToLower().ToString().Equals("reporting supervisor code")
                     || colName.ToLower().ToString().Equals("reportingsupervisorcode") || colName.ToLower().ToString().Equals("report supervisor code"))
                {
                    inEmployees.Columns[curCol].ColumnName = "ReportingSupervisorCode";
                }

                if (this.opts.getRuleValue("Rule_HudsonJobRoleCode").ToLower().Equals("yes"))
                {
                    // First Pass. This way may be unordered making swapping column names trickly.
                    // Do another Column name pass to complete it.

                    if (colName.ToLower().ToString().Equals("jobrolecode"))
                    {
                        Console.WriteLine("jobrolecode found. Renaming to jobrolecode_orgininal");
                        inEmployees.Columns[curCol].ColumnName = "jobrolecode_orgininal";
                    }
       
                }

              



            } // End column loop

            // Pass two for column names .... for renames
            for (int curCol = 0; curCol < inEmployees.Columns.Count; curCol++)
            {
                String colName = inEmployees.Columns[curCol].ColumnName.ToLower().Trim();
                if (this.opts.getRuleValue("Rule_HudsonJobRoleCode").ToLower().Equals("yes"))
                {
                    //Console.WriteLine("Column: " + colName + " ");
                    if (colName.ToLower().ToString().Equals("jobcode"))
                    {
                        Console.WriteLine("jobcode found. Renaming to JobRoleCode");
                        inEmployees.Columns[curCol].ColumnName = "JobRoleCode";
                    }
                }// End custom hudson job role code rename
            }


            if (!inEmployees.Columns.Contains("Status"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "Status", "System.String");
            }

            if (!inEmployees.Columns.Contains("SupervisorCode"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "SupervisorCode", "System.String");
            }
            if (!inEmployees.Columns.Contains("Notes"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "Notes", "System.String");
            }
            if (!inEmployees.Columns.Contains("Password"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "Password", "System.String");
            }
            if (!inEmployees.Columns.Contains("Email"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "Email", "System.String");
            }
            if (!inEmployees.Columns.Contains("MiddleName"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "MiddleName", "System.String");
            }
            if (!inEmployees.Columns.Contains("LastName"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "LastName", "System.String");
            }
            if (!inEmployees.Columns.Contains("FirstName"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "FirstName", "System.String");
            }
            if (!inEmployees.Columns.Contains("NewHireIndicator"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "NewHireIndicator", "System.String");
            }
            if (!inEmployees.Columns.Contains("ReportingSupervisorCode"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "ReportingSupervisorCode", "System.String");
            }
            if (!inEmployees.Columns.Contains("LocationCode"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "LocationCode", "System.String");
            }
            //Add Audience ( one to one mapping ) if not there. 6.30.2014. Cadence
            if (!inEmployees.Columns.Contains("AudienceCode"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "AudienceCode", "System.String");
            }

            // Add HireDate if not there. 10.18.2013 ( blank will default to 1/1/1900 )
            if (!inEmployees.Columns.Contains("HireDate"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "HireDate", "System.String");
            }

            // Add field for NMLSNumber if not there 8.7.2015
            if (!inEmployees.Columns.Contains("NMLS"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "NMLS", "System.String");
            }

        



            if (this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "AudienceCostCenter", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "AudienceExempt", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "AudienceEmpLevel", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "AudienceManager", "System.String");

            
            }


            if (this.opts.getRuleValue("Rule_UseSupervisorUserName").ToLower().Equals("yes"))
            {
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "SupervisorUserName", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "ReportingSupervisorUserName", "System.String");

            }
            if (this.opts.getRuleValue("Rule_UseSupervisorFullName").ToLower().Equals("yes") ||
                this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle").ToLower().Equals("yes") ||
                this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle2").ToLower().Equals("yes") ||
                this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle3").ToLower().Equals("yes") ||
                this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle4").ToLower().Equals("yes"))
            {
                // Add missing columns for when they use a 'Supervisor Name' instead of an SupervisorCode.
                // A full name lookup is required.
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "FullName", "System.String");

            }

           
   

            //NBOFIN Custom File
            if (this.opts.getRuleValue("Rule_CustomNBOFIN").ToLower().Equals("yes"))
            {
                // Add missing columns for when they use a 'Supervisor Name' instead of an SupervisorCode.
                // A first ane last name lookup is required.( Full name is slightly different than the standard.
                //  First Name contains the Middle Initial and . )
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "SupervisorFirstName", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "SupervisorLastName", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "SupervisorFullName", "System.String");

            }
            //Cadence Custom File
            if (this.opts.getRuleValue("Rule_CustomCadence").ToLower().Equals("yes"))
            {
                // Add missing columns 
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "EmployeeNumber", "System.String");
   
            }
            //Bayview, City National, or Calc the UserName
            if (this.opts.getRuleValue("Rule_UserNameFromFirstLastName").ToLower().Equals("yes"))
            {
                // Add missing columns 
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UserName", "System.String");

            }
            //Northrim
            if (this.opts.getRuleValue("Rule_UserNameFromLastName_Dot_FirstName").ToLower().Equals("yes"))
            {
                // Add missing columns 
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UserName", "System.String");

            }

         


            //EmployeeNumber is also Username
            if (this.opts.getRuleValue("Rule_UseUserNameasEmpID").ToLower().Equals("yes"))
            {
                // Add missing EmployeeNumber column ( Username will be this as well )
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "EmployeeNumber", "System.String");
   
            }
            //Email is also Username
            if (this.opts.getRuleValue("Rule_UseEmailAsUserName").ToLower().Equals("yes"))
            {
                // Add missing EmployeeNumber column ( Username will be this as well )
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UserName", "System.String");

            }
         

            // Add isNew flag to dataTable
            // This will be used to mark which ones need to be inserted
            // ( These need to be inserted before the updates )

            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "New", "System.String");
            // Add update flag to dataTable
            // This will be used to mark which ones need to be updated in the db
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "Update", "System.String");
            // Add update comment to dataTable
            // This will be used add notes or comments
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateMsg", "System.String");
            // Add existingID to dataTable
            // This will be used to mark what existing record got modified
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateID", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateAUID", "System.String");

            // Add Existing Name, Code, ParentEmployeeID to dataTable
            // This will be used to see what was updated or not updated
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldUsername", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldEmployeeNumber", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldLocationCode", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldLocationID", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldJobRoleCode", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldJobRoleID", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldSupervisorCode", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldSupervisorID", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldReportingSupervisorCode", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldReportingSupervisorID", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldFirstName", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldLastName", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldMiddleName", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldHireDate", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldNewHireIndicator", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldEmail", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldStatus", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldPassword", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldPasswordSalt", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldAudienceCode", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldNMLSNumber", "System.String");
            if (this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes"))
            {


                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldAudienceCostCenter", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldAudienceExempt", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldAudienceEmpLevel", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldAudienceManager", "System.String");
            }
            if (this.opts.getRuleValue("Rule_AudienceCustom1").ToLower().Equals("yes"))
            {
               
                    this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldAudienceCustom1", "System.String");
            }
            if (this.opts.getRuleValue("Rule_AudienceCustom2").ToLower().Equals("yes"))
            {

                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "OldAudienceCustom2", "System.String");
            }



            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateUsername", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateEmployeeNumber", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateLocationCode", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateJobRoleCode", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateSupervisorCode", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateReportingSupervisorCode", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateLastName", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateFirstName", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateMiddleName", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateHireDate", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateNewHireIndicator", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateEmail", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateStatus", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdatePassword", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateNotes", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateAudienceCode", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateNMLSNumber", "System.String");


            if (this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes"))
            {
          

                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateAudienceCostCenter", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateAudienceExempt", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateAudienceEmpLevel", "System.String");
                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateAudienceManager", "System.String");
            }
            if (this.opts.getRuleValue("Rule_AudienceCustom1").ToLower().Equals("yes"))
            {

                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateAudienceCustom1", "System.String");
            }
            if (this.opts.getRuleValue("Rule_AudienceCustom2").ToLower().Equals("yes"))
            {

                this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "UpdateAudienceCustom2", "System.String");
            }


            // Add Error to dataTable
            // This will be used to note error messages
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "ErrorFg", "System.String");
            this.inEmployees = Utility.addColumnToDataTable(this.inEmployees, "Error", "System.String");

            //Default some values
     
            for (int curRow = 0; curRow < this.inEmployees.Rows.Count; curRow++)
            {

             

                // BC 5.16.2014 to handle JobRoleCodes with length > 50
                //Console.WriteLine("50 Check:" + this.inEmployees.Rows[curRow]["JobRoleCode"].ToString().Trim() + " | " + this.inEmployees.Rows[curRow]["JobRoleCode"].ToString().Trim().Length);
                if (this.inEmployees.Rows[curRow]["JobRoleCode"].ToString().Trim().Length > 50 )
                {
                    Console.Write("OVER 50:  " + this.inEmployees.Rows[curRow]["JobRoleCode"].ToString().Trim() + " | ");
                    this.inEmployees.Rows[curRow]["JobRoleCode"] = this.inEmployees.Rows[curRow]["JobRoleCode"].ToString().Trim().Substring(0, 50);
                    Console.WriteLine(" " + this.inEmployees.Rows[curRow]["JobRoleCode"].ToString().Trim() + " ");
                }
                if (this.inEmployees.Rows[curRow]["LocationCode"].ToString().Trim().Length > 50)
                {

                    this.inEmployees.Rows[curRow]["LocationCode"] = this.inEmployees.Rows[curRow]["LocationCode"].ToString().Trim().Substring(0, 50);
                }


                if (this.inEmployees.Rows[curRow]["Status"].ToString().Length < 1)
                {
                    //Console.WriteLine("Defaulting the Status field to value 1.");
                    this.inEmployees.Rows[curRow]["Status"] = "1";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("active"))
                {
                    //Console.WriteLine("Converting Status Field Active to 1.");
                    this.inEmployees.Rows[curRow]["Status"] = "1";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("1.00"))
                {
                    //Console.WriteLine("Converting Status Field Active to 1.");
                    this.inEmployees.Rows[curRow]["Status"] = "1";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("2.00"))
                {
                    //Console.WriteLine("Converting Status Field  to 2.");
                    this.inEmployees.Rows[curRow]["Status"] = "2";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("3.00"))
                {
                    //Console.WriteLine("Converting Status Field  to 3.");
                    this.inEmployees.Rows[curRow]["Status"] = "3";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("4.00"))
                {
                    //Console.WriteLine("Converting Status Field  to 4.");
                    this.inEmployees.Rows[curRow]["Status"] = "4";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("a"))
                {
                    //Console.WriteLine("Converting Status Field Active to 1.");
                    this.inEmployees.Rows[curRow]["Status"] = "1";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("t"))
                {
                    //Console.WriteLine("Converting Status Field Active to 1.");
                    this.inEmployees.Rows[curRow]["Status"] = "2";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("d"))
                {
                    //Console.WriteLine("Converting Status Field to inactive.");
                    this.inEmployees.Rows[curRow]["Status"] = "2";
                }

                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("inactive"))
                {
                    //Console.WriteLine("Converting Status Field Inactive to 2.");
                    this.inEmployees.Rows[curRow]["Status"] = "2";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("terminated"))
                {
                    //Console.WriteLine("Converting Status Field Inactive to 2.");
                    this.inEmployees.Rows[curRow]["Status"] = "2";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("on leave"))
                {
                    //Console.WriteLine("Converting Status Field Leave to 3.");
                    this.inEmployees.Rows[curRow]["Status"] = "3";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("l"))
                {
                    //Console.WriteLine("Converting Status Field Leave to 3");
                    this.inEmployees.Rows[curRow]["Status"] = "3";
                }
                if (this.inEmployees.Rows[curRow]["Status"].ToString().ToLower().ToString().Equals("0"))
                {
                    //Console.WriteLine("Converting Status Field Inactive to 2.");
                    this.inEmployees.Rows[curRow]["Status"] = "2";
                }
                if (this.inEmployees.Rows[curRow]["NewHireIndicator"].ToString().Length < 1)
                {
                    //Console.WriteLine("Defaulting the Status field to value 1.");
                    this.inEmployees.Rows[curRow]["NewHireIndicator"] = "FALSE";
                }
                if (this.inEmployees.Rows[curRow]["NewHireIndicator"].ToString().ToLower().ToString().Equals("1"))
                {
     
                    this.inEmployees.Rows[curRow]["NewHireIndicator"] = "TRUE";
                }
                if (this.inEmployees.Rows[curRow]["NewHireIndicator"].ToString().ToLower().ToString().Equals("0"))
                {

                    this.inEmployees.Rows[curRow]["NewHireIndicator"] = "FALSE";
                }
                if (this.inEmployees.Rows[curRow]["NewHireIndicator"].ToString().ToLower().ToString().Equals("no"))
                {

                    this.inEmployees.Rows[curRow]["NewHireIndicator"] = "FALSE";
                }
                if (this.inEmployees.Rows[curRow]["NewHireIndicator"].ToString().ToLower().ToString().Equals("yes"))
                {
                    this.inEmployees.Rows[curRow]["NewHireIndicator"] = "TRUE";
                }
                if (this.inEmployees.Rows[curRow]["NewHireIndicator"].ToString().ToLower().ToString().Equals("y"))
                {
                    this.inEmployees.Rows[curRow]["NewHireIndicator"] = "TRUE";
                }
                if (this.inEmployees.Rows[curRow]["NewHireIndicator"].ToString().ToLower().ToString().Equals("n"))
                {
                    this.inEmployees.Rows[curRow]["NewHireIndicator"] = "FALSE";
                }


                //MidCountry
                // --------------------------------------------------------------------------
                if (this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes"))
                {
                    // Combine Locations
                    string unit = this.inEmployees.Rows[curRow]["Business Units"].ToString();
                    string master_unit = unit;
                    string costcenter = this.inEmployees.Rows[curRow]["Cost Center"].ToString(); ;
                    string manager = this.inEmployees.Rows[curRow]["Manager Yes or No"].ToString(); ;
                    string exempt = this.inEmployees.Rows[curRow]["Exempt/NonExempt"].ToString() ;
                    string role = this.inEmployees.Rows[curRow]["Employee Level"].ToString() ; 
                    //Console.WriteLine("locationname: " + unit);
                    if (unit.Contains("Heights Finance"))
                    {
                        // Has Business Unit Abbrv ( HFC-, PS-, MCB-, etc )
                        unit = "HFC-";
                        //Console.WriteLine("Customer MidCountry Business Unit: " + unit);
                    
                        role = "HFC-" + role;
                        if (exempt.Contains("E"))
                        {
                            exempt = "HFC-Exempt";
                        }
                        else
                        {
                            exempt = "HFC-Non-Exempt";
                        }
                        if (manager.Contains("No"))
                        {
                            manager = "HFC-Non-Manager";
                        }
                        else
                        {
                            manager = "HFC-Manager";
                        }

                    }
                    else if (unit.Contains("MidCountry Bank"))
                    {
                        unit = "MCB-";
                       
                        role = "MCB-" + role;
                        // Has Business Unit Abbrv ( HFC-, PS-, MCB-, etc )
                        //unit = unit.Substring(0, unit.IndexOf("-") + 1);
                        //Console.WriteLine("Customer MidCountry Business Unit: " + unit);

                        if (exempt.Contains("E"))
                        {
                            exempt = "MCB-Exempt";
                        }
                        else
                        {
                            exempt = "MCB-Non-Exempt";
                        }

                        if (manager.Contains("No"))
                        {
                            manager = "MCB-Non-Manager";
                        }
                        else
                        {
                            manager = "MCB-Manager";
                        }
                    }
                    else if (unit.Contains("Pioneer Services"))
                    {
                        unit = "CBD-";
                       
                        role = "CBD-" + role;
                        // Has Business Unit Abbrv ( HFC-, PS-, MCB-, etc )
                        //unit = unit.Substring(0, unit.IndexOf("-") + 1);
                        //Console.WriteLine("Customer MidCountry Business Unit: " + unit);

                        if (exempt.Contains("E"))
                        {
                            exempt = "CBD-Exempt";
                        }
                        else
                        {
                            exempt = "CBD-Non-Exempt";
                        }
                        if (manager.Contains("No"))
                        {
                            manager = "CBD-Non-Manager";
                        }
                        else
                        {
                            manager = "CBD-Manager";
                        }

                    }
                    else if (unit.Contains("MidCountry Financial"))
                    {
                        unit = "MCF-";
                        
                        role = "MCF-" + role;
                        // Has Business Unit Abbrv ( HFC-, PS-, MCB-, etc )
                        //unit = unit.Substring(0, unit.IndexOf("-") + 1);
                        //Console.WriteLine("Customer MidCountry Business Unit: " + unit);
                        if (exempt.Contains("E"))
                        {
                            exempt = "MCF-Exempt";
                        }
                        else
                        {
                            exempt = "MCF-Non-Exempt";
                        }
                        if (manager.Contains("No"))
                        {
                            manager = "MCF-Non-Manager";
                        }
                        else
                        {
                            manager = "MCF-Manager";
                        }

                    }
                    else{
                        unit = "NA-";
                        manager = "NA";
                        costcenter = "NA";
                    }

                    unit += this.inEmployees.Rows[curRow]["Location - State"].ToString();
                    unit += this.inEmployees.Rows[curRow]["locationcodeorig"].ToString();
                    this.inEmployees.Rows[curRow]["locationcode"] = unit;
                    this.inEmployees.Rows[curRow]["AudienceCostCenter"] = costcenter;
                    this.inEmployees.Rows[curRow]["AudienceExempt"] = exempt;
                    this.inEmployees.Rows[curRow]["AudienceManager"] = manager;
                    this.inEmployees.Rows[curRow]["AudienceEmpLevel"] = role;


                    // ---------------------------


                }



                // Citizens and northern custom field translations
                if (this.opts.getRuleValue("Rule_CNBANKPA").ToLower().Equals("yes"))
                {



                    // Translate email address account  to username 
                    string uname = this.inEmployees.Rows[curRow]["UserName"].ToString();
                    int pos1 = uname.IndexOf("@");
                    if (pos1 > 0)
                    {
                        uname = uname.Substring(0, pos1);
                        this.inEmployees.Rows[curRow]["UserName"] = uname.ToLower().Trim();
                    }

                    // Default the Password
                    this.inEmployees.Rows[curRow]["Password"] = "Password1";
                }

                // Citizens and northern custom field translations. 
                if (this.opts.getRuleValue("Rule_PartialEmailtoUserName").ToLower().Equals("yes"))
                {

                    // Translate email address account  to username 
                    string uname = this.inEmployees.Rows[curRow]["Email"].ToString();
                    int pos1 = uname.IndexOf("@");
                    if (pos1 > 0)
                    {
                        uname = uname.Substring(0, pos1);
                        this.inEmployees.Rows[curRow]["UserName"] = uname.ToLower().Trim();
                    }

                } 
                if (this.opts.getRuleValue("Rule_PartialEmailUserNametoUserName").ToLower().Equals("yes"))
                {

                    // convert email address account in the username field  to username 
                    string uname = this.inEmployees.Rows[curRow]["UserName"].ToString();
                    int pos1 = uname.IndexOf("@");
                    if (pos1 > 0)
                    {
                        uname = uname.Substring(0, pos1);
                        this.inEmployees.Rows[curRow]["UserName"] = uname.ToLower().Trim();
                    }

                }

              
                String ruleValue = this.opts.getRuleValue("Rule_11_PadEmployeeNumbers").ToLower();
                //Console.WriteLine("PAD: " + ruleValue);
                // Does not work on Excel if it is a formated number column. 3.23.2015
     
               

                if (ruleValue.Length > 0)
                {
                    try
                    {
                        int padlength = Convert.ToInt16(ruleValue);
                        //Console.WriteLine("PAD: " + ruleValue);
                        string oldNumber = this.inEmployees.Rows[curRow]["EmployeeNumber"].ToString().Trim();
                        string newNumber = Utility.PadColumnValue(oldNumber, padlength);
                        this.inEmployees.Rows[curRow]["EmployeeNumber"] = newNumber;
      
                        //Console.WriteLine("Pad EmployeeNumber:" + padlength + " " + this.inEmployees.Rows[curRow]["EmployeeNumber"].ToString() + " |" + newNumber);
                    }

                    catch (Exception e)
                    {
                        Console.WriteLine("Rule 11 : Bad Pad Value: " + ruleValue);
                    }
                }
                ruleValue = this.opts.getRuleValue("Rule_12_PadSupervisorCodes").ToLower();
                if (ruleValue.Length > 0)
                {
                    try
                    {
                        int padlength = Convert.ToInt16(ruleValue);
                        this.inEmployees.Rows[curRow]["SupervisorCode"] = Utility.PadColumnValue(this.inEmployees.Rows[curRow]["SupervisorCode"].ToString().Trim(), padlength);
                        this.inEmployees.Rows[curRow]["ReportingSupervisorCode"] = Utility.PadColumnValue(this.inEmployees.Rows[curRow]["ReportingSupervisorCode"].ToString().Trim(), padlength);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Rule 12 : Bad Pad Value: " + ruleValue);
                    }
                }
                if (this.opts.getRuleValue("Rule_18_UnpadEmployeeNumbers").ToLower().Equals("yes"))
                {
                    this.inEmployees.Rows[curRow]["employeenumber"] = Utility.UnPadColumnValue(this.inEmployees.Rows[curRow]["employeenumber"].ToString().Trim());
                    this.inEmployees.Rows[curRow]["supervisorcode"] = Utility.UnPadColumnValue(this.inEmployees.Rows[curRow]["supervisorcode"].ToString().Trim());
                    this.inEmployees.Rows[curRow]["reportingsupervisorcode"] = Utility.UnPadColumnValue(this.inEmployees.Rows[curRow]["reportingsupervisorcode"].ToString().Trim());
                }

                if (this.opts.getRuleValue("Rule_CustomNBOFIN").ToLower().Equals("yes"))
                {

                    // Pad EmployeeNumber and SupervisorCode
                    this.inEmployees.Rows[curRow]["EmployeeNumber"] = Utility.PadColumnValue(this.inEmployees.Rows[curRow]["EmployeeNumber"].ToString().Trim(), 6);


                    this.inEmployees.Rows[curRow]["Email"] = this.inEmployees.Rows[curRow]["UserName"];
                    this.inEmployees.Rows[curRow]["SupervisorFullName"] = this.inEmployees.Rows[curRow]["supervisorCode"];
                    this.inEmployees.Rows[curRow]["supervisorCode"] = "";
                    
                    //Parse Out Last name
                    String payrollName = this.inEmployees.Rows[curRow]["PayrollName"].ToString();
                    int index1 = payrollName.IndexOf(", ", 0);
                    String lastName = "";
                    String firstName = "";
                    if (index1 > 0 && index1 + 1 < payrollName.Length)
                    {
                        lastName = payrollName.Substring(0, index1);
                        firstName = payrollName.Substring(index1 + 2, payrollName.Length - index1 - 2);
                    }
                    this.inEmployees.Rows[curRow]["LastName"] = lastName;
                    this.inEmployees.Rows[curRow]["FirstName"] = firstName;


                    // Parse out Supervisor Code ( which is a Full Name )
                    String supName = this.inEmployees.Rows[curRow]["SupervisorFullName"].ToString();
                    int index2 = supName.IndexOf(", ", 0);
                    String lastName2 = "";
                    String firstName2 = "";
                    if (index2 > 0 && index2 + 1 < supName.Length)
                    {
                        lastName2 = supName.Substring(0, index2);
                        firstName2 = supName.Substring(index2 + 2, supName.Length - index2 -2);
                    }


                    this.inEmployees.Rows[curRow]["SupervisorLastName"] = lastName2;
                    this.inEmployees.Rows[curRow]["SupervisorFirstName"] = firstName2;

             

                }



                if (this.opts.getRuleValue("Rule_CustomCadence").ToLower().Equals("yes"))
                {

                    // UnPad EmployeeNumber and UserName
                    this.inEmployees.Rows[curRow]["UserName"] = Utility.UnPadColumnValue(this.inEmployees.Rows[curRow]["UserName"].ToString());
                    this.inEmployees.Rows[curRow]["LocationCode"] = Utility.UnPadColumnValue(this.inEmployees.Rows[curRow]["LocationCode"].ToString());
                    

                    this.inEmployees.Rows[curRow]["EmployeeNumber"] = this.inEmployees.Rows[curRow]["UserName"];
                    this.inEmployees.Rows[curRow]["JobRoleCode"] = this.inEmployees.Rows[curRow]["JobRoleName"];


                }

                // Bayview  ( Manager First and Last Name )
                if (this.opts.getRuleValue("Rule_CustomBayview").ToLower().Equals("yes"))
                {


                    this.inEmployees.Rows[curRow]["SupervisorUserName"] = this.inEmployees.Rows[curRow]["managerfirstname"].ToString() + this.inEmployees.Rows[curRow]["managerlastname"].ToString();
                    this.inEmployees.Rows[curRow]["SupervisorCode"] = this.inEmployees.Rows[curRow]["managerfirstname"].ToString() + this.inEmployees.Rows[curRow]["managerlastname"].ToString();
                    this.inEmployees.Rows[curRow]["ReportingSupervisorUserName"] = this.inEmployees.Rows[curRow]["managerfirstname"].ToString() + this.inEmployees.Rows[curRow]["managerlastname"].ToString();
                    this.inEmployees.Rows[curRow]["ReportingSupervisorCode"] = this.inEmployees.Rows[curRow]["managerfirstname"].ToString() + this.inEmployees.Rows[curRow]["managerlastname"].ToString();

                    this.inEmployees.Rows[curRow]["EmployeeNumber"] = Utility.UnPadColumnValue(this.inEmployees.Rows[curRow]["employeenumber"].ToString());
           

                }


                // UserName is built from FirstName + LastName  ( ADP? )
                if (this.opts.getRuleValue("Rule_UserNameFromFirstLastName").ToLower().Equals("yes"))
                {

                    String uname = this.inEmployees.Rows[curRow]["FirstName"].ToString() + this.inEmployees.Rows[curRow]["LastName"].ToString();
                    uname = uname.Replace(" ", string.Empty);
                    uname = uname.Replace(" jr", string.Empty);
                    uname = uname.Replace(" III", string.Empty);
                    uname = uname.Replace("'", string.Empty);

                    this.inEmployees.Rows[curRow]["UserName"] = uname;

                    //?Remove spaces in LastName


                }
                    // UserName is built from FirstName + "." + LastName  ( Northrim )
                if (this.opts.getRuleValue("Rule_UserNameFromLastName_Dot_FirstName").ToLower().Equals("yes"))
                {

                    String uname = this.inEmployees.Rows[curRow]["LastName"].ToString().ToLower() + "." + this.inEmployees.Rows[curRow]["FirstName"].ToString().ToLower();
                    uname = uname.Replace(" ", string.Empty);
                    uname = uname.Replace(" jr", string.Empty);
                    uname = uname.Replace(" III", string.Empty);
                    uname = uname.Replace("'", string.Empty);

                    this.inEmployees.Rows[curRow]["UserName"] = uname;

                    //?Remove spaces in LastName


                }
          

                if (this.opts.getRuleValue("Rule_3_CustomFieldPresetsNBT").ToLower().Equals("yes"))
                //if (this.customerID.Equals("80"))
                {
                    //Console.WriteLine("Rule_3_CustomFieldPresentsNBT");
                    this.inEmployees.Rows[curRow]["LocationCode"] = Utility.PadColumnValue(this.inEmployees.Rows[curRow]["LocationCode"].ToString().Trim(), 6);
                    this.inEmployees.Rows[curRow]["EmployeeNumber"] = Utility.PadColumnValue(this.inEmployees.Rows[curRow]["EmployeeNumber"].ToString().Trim(), 5);
                    this.inEmployees.Rows[curRow]["SupervisorCode"] = Utility.PadColumnValue(this.inEmployees.Rows[curRow]["SupervisorCode"].ToString().Trim(), 5);

                    //this.inEmployees.Rows[curRow]["Email"] = "";
                    // Add NBT to the password
                    //this.inEmployees.Rows[curRow]["Password"] = this.inEmployees.Rows[curRow]["Password"] + "NBT";

                }
                 // UserName is built from 
                if (this.opts.getRuleValue("Rule_UseUserNameasEmpID").ToLower().Equals("yes"))
                {


                    this.inEmployees.Rows[curRow]["EmployeeNumber"] = this.inEmployees.Rows[curRow]["UserName"].ToString();

                }
                //Email is also Username ( Has Email on File.. needs to add the UserName column and values )
                if (this.opts.getRuleValue("Rule_UseEmailAsUserName").ToLower().Equals("yes"))
                {
                    
                    this.inEmployees.Rows[curRow]["UserName"] = this.inEmployees.Rows[curRow]["Email"].ToString();

                }


                // Build Names from Full Name
                if (this.opts.getRuleValue("Rule_20_UserFullName").Length > 0)
                {

                    string fullname = this.inEmployees.Rows[curRow]["UserFullName"].ToString();
                    if (fullname.Trim().Length > 0)
                    {
                        // Example:  Lastname, firstname M.
                        int l_pos = fullname.IndexOf(',');
                        int m_pos = fullname.LastIndexOf('.');
                        int m_originpos = fullname.LastIndexOf('.');
                        if (m_pos < 1 || m_pos < l_pos)
                        {
                            // No Middle Initial. Or '.' is in the lastname before the ',' ( Austin Jr., Mark )
                            m_pos = fullname.Length + 2;
                        }
                        Console.WriteLine(" * " + fullname + "  LP: " + l_pos + " MP :" + m_pos + ";");
                        string lastname = fullname.Substring(0, l_pos).Trim();
                        string firstname = fullname.Substring(l_pos + 1, m_pos - 2 - (l_pos + 1)).Trim(); // Middle initial is M. at the end
                        string middlename = "";
                        if (m_originpos > 0 && m_originpos > l_pos)
                        {
                            middlename = fullname.Substring(m_pos - 1, 1).Trim(); // Middle initial is M. at the end
                        }

                        Console.WriteLine(" * " + fullname + "  LP: " + l_pos + " MP :" + m_pos + ";" + lastname + "|" + firstname + "|" + middlename);

                        this.inEmployees.Rows[curRow]["LastName"] = lastname;
                        this.inEmployees.Rows[curRow]["FirstName"] = firstname;
                        this.inEmployees.Rows[curRow]["MiddleName"] = middlename;
                    }



                }
                
                //this.inEmployees.Rows[curRow]["Password"] = this.inEmployees.Rows[curRow]["Password"] + "01";

                // File has a userName instead of an EmployeeNumber in the SupervisorCode field.
                // SupervisorUserName field has been added to the inEmployees Table.
                // Default the values with the UserNames in the SupervisorCode field.
                // Later replace the SupervisorCodes with the EmployeeNumbers
                if (this.opts.getRuleValue("Rule_UseSupervisorUserName").ToLower().Equals("yes"))
                {
                    this.inEmployees.Rows[curRow]["SupervisorUserName"] = this.inEmployees.Rows[curRow]["SupervisorCode"];
                    this.inEmployees.Rows[curRow]["ReportingSupervisorUserName"] = this.inEmployees.Rows[curRow]["ReportingSupervisorCode"];

                }
                // File has a email instead of an EmployeeNumber in the SupervisorCode field.
                // SupervisorEmail field has been added to the inEmployees Table.
                // Default the values with the Email in the SupervisorCode field.
                // Later replace the Email with the EmployeeNumbers
                if (this.opts.getRuleValue("Rule_UseSupervisorEmail").ToLower().Equals("yes"))
                {
                    //this.inEmployees.Rows[curRow]["SupervisorEmail"] = this.inEmployees.Rows[curRow]["SupervisorCode"];
                    //this.inEmployees.Rows[curRow]["ReportingSupervisorEmail"] = this.inEmployees.Rows[curRow]["ReportingSupervisorCode"];

                }

                // File has a Full Name instead of an Employee Number for the Supervisor
                // Pre-populate a FullName for the user so this lookup can be done.
                // SupervisorFullName  vs  FullName
                if (this.opts.getRuleValue("Rule_UseSupervisorFullName").ToLower().Equals("yes"))
                {
                    string fullName = this.inEmployees.Rows[curRow]["LastName"].ToString().Trim() + ", ";
                    fullName = fullName + this.inEmployees.Rows[curRow]["FirstName"].ToString().Trim();
                    string middleName = this.inEmployees.Rows[curRow]["MiddleName"].ToString().Trim();
                    if (middleName.Length > 0)
                    {
                        // First Initial followed by a . 
                        fullName = fullName + " " + middleName.Substring(0, 1) + ".";
                    }
                    this.inEmployees.Rows[curRow]["FullName"] = fullName;

                }
                // File has a Full Name instead of an Employee Number for the Supervisor
                // Pre-populate a FullName for the user so this lookup can be done.
                // SupervisorFullName  vs  FullName
                if (this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle").ToLower().Equals("yes"))
                {
                    string fullName = this.inEmployees.Rows[curRow]["LastName"].ToString().Trim() + ", ";
                    fullName = fullName + this.inEmployees.Rows[curRow]["FirstName"].ToString().Trim();
                    string middleName = this.inEmployees.Rows[curRow]["MiddleName"].ToString().Trim();
                    if (middleName.Length > 0)
                    {
                        
                        fullName = fullName + " " + middleName;
                    }
                    this.inEmployees.Rows[curRow]["FullName"] = fullName;

                }

                // File has a Full Name instead of an Employee Number for the Supervisor
                // Pre-populate a FullName for the user so this lookup can be done.
                // SupervisorFullName  vs  FullName
                //  First M. Last      or   First Last ( period has to be added in )
                if (this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle2").ToLower().Equals("yes"))
                {
                    string fullName = this.inEmployees.Rows[curRow]["FirstName"].ToString().Trim() + " ";
                    string middleName = this.inEmployees.Rows[curRow]["MiddleName"].ToString().Trim();
                    if (middleName.Length > 0)
                    {
                        // First Initial followed by a . 
                        fullName = fullName + middleName + ". ";
                    }
                    fullName = fullName + this.inEmployees.Rows[curRow]["LastName"].ToString().Trim();

                    this.inEmployees.Rows[curRow]["FullName"] = fullName;

                }

                // File has a Full Name instead of an Employee Number for the Supervisor
                // Pre-populate a FullName for the user so this lookup can be done.
                // SupervisorFullName  vs  FullName
                if (this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle3").ToLower().Equals("yes"))
                {
                    string fullName = this.inEmployees.Rows[curRow]["LastName"].ToString().Trim() + ", ";
                    fullName = fullName + this.inEmployees.Rows[curRow]["FirstName"].ToString().Trim();
                    string middleName = this.inEmployees.Rows[curRow]["MiddleName"].ToString().Trim();
                    if (middleName.Length > 0)
                    {
                        // First Middle Initial followed by a . 
                        fullName = fullName + " " + middleName + ".";
                    }
                    this.inEmployees.Rows[curRow]["FullName"] = fullName;

                }

                // File has a Full Name instead of an Employee Number for the Supervisor
                // Pre-populate a FullName for the user so this lookup can be done.
                // !! This logic forces the Middle name to be just the initial.
                // SupervisorFullName  vs  FullName
                if (this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle4").ToLower().Equals("yes"))
                {
                    string fullName = this.inEmployees.Rows[curRow]["LastName"].ToString().Trim() + ", ";
                    fullName = fullName + this.inEmployees.Rows[curRow]["FirstName"].ToString().Trim();
                    string middleName = this.inEmployees.Rows[curRow]["MiddleName"].ToString().Trim();
                    if (middleName.Length > 0)
                    {

                        fullName = fullName + " " + middleName.Substring(0, 1);
                    }
                    this.inEmployees.Rows[curRow]["FullName"] = fullName;

                }


            }

            return 0; // ok
        }

        // -----------------------------------------------------------
        // validateColumns
        // brief: Checks to make sure column information is available
        // return -1 if no Name
        //        -2 if no Code
        //        -3 if no ParentCode
        // -----------------------------------------------------------
        public int validateColumns()
        {

            if (!inEmployees.Columns.Contains("EmployeeNumber"))
            {
                if (!this.silent)
                {
                    Console.WriteLine("EmployeeLoader.validateColumns : EmployeeNumber column not found. Fail. ");
                }

                jobQueue.failJob(opts.getJobID(), "Failed: EmployeeNumber column not found.");

                return -1;
            }
            if (!inEmployees.Columns.Contains("Username"))
            {
                if (!this.silent)
                {
                    Console.WriteLine("EmployeeLoader.validateColumns : Username column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: Username column not found.");
                return -2;
            }
            if (!inEmployees.Columns.Contains("FirstName"))
            {
                if (!this.silent)
                {
                    Console.WriteLine("EmployeeLoader.validateColumns : FirstName column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: FirstName column not found.");

                return -3;
            }
            if (!inEmployees.Columns.Contains("LastName"))
            {
                if (!this.silent)
                {
                    Console.WriteLine("EmployeeLoader.validateColumns : LastName column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: LastName column not found.");

                return -4;
            }
            if (!inEmployees.Columns.Contains("LocationCode"))
            {
                if (!this.silent)
                {
                    Console.WriteLine("EmployeeLoader.validateColumns : LocationCode column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: LocationCode column not found.");

                return -4;
            }

            if (!inEmployees.Columns.Contains("JobRoleCode"))
            {
                if (!this.silent)
                {
                    Console.WriteLine("EmployeeLoader.validateColumns : JobRoleCode column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: JobRoleCode column not found.");

                return -5;
            }
            if (!inEmployees.Columns.Contains("SupervisorCode"))
            {
                if (!this.silent)
                {
                    Console.WriteLine("EmployeeLoader.validateColumns : SupervisorCode column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: SupervisorCode column not found.");

                return -6;
            }

            // HireDate is no longer a required column 10.18.2013
            //if (!inEmployees.Columns.Contains("HireDate"))
            //{
              //  if (!this.silent)
             //   {
              //      Console.WriteLine("EmployeeLoader.validateColumns : HireDate column not found. Fail. ");
              //  }
             //   jobQueue.failJob(opts.getJobID(), "Failed: HireDate column not found.");

              //  return -6;
            //}


            return 0; // ok
        }

        // ---------------------------------------------
        // load the Employee information for the customer
        // from the database.
        // ---------------------------------------------
        public int loadEmployeeFromDB()
        {
            String query = " select ID = U.ID, EmployeeNumber = U.EmployeeNumber, Username = U.UserName, \n" +
                        " U.FirstName, U.LastName, U.MiddleName, U.HireDate, U.NewHireIndicator, \n" +
                        " U.JobRoleID, JobRoleCode =  case when J.InternalCode is null then '*none*' else J.InternalCode end, \n" +
                        " U.LocationID, LocationCode =  case when L.InternalCode is null then '*none*' else L.InternalCode end, \n" +
                        " U.SupervisorID, SupervisorCode =  case when S.EmployeeNumber is null then '*none*' else S.EmployeeNumber end, \n" +
                         " ReportingSupervisorID = case when U.reportingSupervisorID is null then '*none*' else U.ReportingSupervisorID end, ReportingSupervisorCode =  case when RS.EmployeeNumber is null then '*none*' else RS.EmployeeNumber end, \n" +
                         " FullName = RTRIM(U.LastName) + ', ' + RTRIM(U.FirstName) + ' ' + RTRIM(U.MiddleName) + '.' , \n" +
                        " U.ModifiedBy, U.CreatedBy, U.ModifiedOn, U.CreatedOn, \n" +
                        "  U.Notes, U.StatusID, U.Email, \n" +
                        " WebUserID = AU.UserId, \n" +
                        " Password = case when M.Password is not null then M.Password else '*none*' end, \n" +
                        " PasswordSalt = case when M.PasswordSalt is not null then M.PasswordSalt else '*none*' end, \n" +
                        " AudienceCode = ( select MAX(A.InternalCode) from Audience A \n" +
                        "                          inner join UserAudience AL on A.ID = AL.AudienceID and AL.UserID = U.ID \n" +
                        "                       where A.CustomerID = " + this.customerID + ") \n ";
            if (this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes"))
            {
                // Add Customer Audience Lookups
                query += " ,AudienceCostCenter = ( select MAX(A.InternalCode) from Audience A \n" +
                        "                          inner join UserAudience AL on A.ID = AL.AudienceID and AL.UserID = U.ID \n" +
                        "                       where A.CustomerID = " + this.customerID + " and A.ParentAudienceID in ( " +
                        this.opts.getRuleValue("Rule_AudienceCostCenter_ParentID").ToLower() +
                        ")  ) \n ";
                query += " ,AudienceExempt = ( select MAX(A.InternalCode) from Audience A \n" +
                     "                          inner join UserAudience AL on A.ID = AL.AudienceID and AL.UserID = U.ID \n" +
                     "                       where A.CustomerID = " + this.customerID + " and A.ParentAudienceID in (" +
                     this.opts.getRuleValue("Rule_AudienceExempt_ParentID").ToLower() +
                     ") ) \n ";
                query += ", AudienceEmpLevel = ( select MAX(A.InternalCode) from Audience A \n" +
                     "                          inner join UserAudience AL on A.ID = AL.AudienceID and AL.UserID = U.ID \n" +
                     "                       where A.CustomerID = " + this.customerID + " and A.ParentAudienceID in ( " +
                     this.opts.getRuleValue("Rule_AudienceEmpLevel_ParentID").ToLower() +
                     ") ) \n ";
                query += " ,AudienceManager = ( select MAX(A.InternalCode) from Audience A \n" +
                     "                          inner join UserAudience AL on A.ID = AL.AudienceID and AL.UserID = U.ID \n" +
                     "                       where A.CustomerID = " + this.customerID + " and A.ParentAudienceID in ( " +
                     this.opts.getRuleValue("Rule_AudienceManager_ParentID").ToLower() +
                     ") ) \n ";
            }
            if (this.opts.getRuleValue("Rule_AudienceCustom1").ToLower().Equals("yes"))
            {
                // Add Custom Audience Lookups
                query += " ,AudienceCustom1 = ( select MAX(A.InternalCode) from Audience A \n" +
                        "                          inner join UserAudience AL on A.ID = AL.AudienceID and AL.UserID = U.ID \n" +
                        "                       where A.CustomerID = " + this.customerID + " and ( A.ParentAudienceID in ( " +
                        this.opts.getRuleValue("Rule_AudienceCustom1_ParentIDs").ToLower() +
                        ") or A.iD in ( " +
                            this.opts.getRuleValue("Rule_AudienceCustom1_ParentIDs").ToLower() +
                       " )  )   ) \n ";
            }
            if (this.opts.getRuleValue("Rule_AudienceCustom2").ToLower().Equals("yes") )
            {
                // Add Custom Audience Lookups
                query += " ,AudienceCustom2 = ( select MAX(A.InternalCode) from Audience A \n" +
                        "                          inner join UserAudience AL on A.ID = AL.AudienceID and AL.UserID = U.ID \n" +
                        "                       where A.CustomerID = " + this.customerID + " and ( A.ParentAudienceID in ( " +
                        this.opts.getRuleValue("Rule_AudienceCustom2_ParentIDs").ToLower() +
                        ")  or A.iD in ( " +
                            this.opts.getRuleValue("Rule_AudienceCustom2_ParentIDs").ToLower() +
                       " )  )  )\n ";
            
            }
                        query += " from [User]  U \n" +
                        " left outer join [User] as S on S.ID = U.SupervisorID  \n" +
                        " left outer join [User] as RS on RS.ID = U.ReportingSupervisorID  \n" +
                        " left outer join Location as L on L.ID = U.LocationID  \n" +
                        " left outer join JobRole as J on J.ID = U.JobRoleID  \n" +
                        " left outer join aspnet_Users as AU on U.username = AU.UserName \n" +
                        " left outer join aspnet_Membership as M on AU.UserId = M.UserId \n" +
                        " left outer join aspnet_Applications as A on M.ApplicationID = A.ApplicationID\n" +
                        " right join Customer as C on C.SubDomain = A.Applicationname and U.CustomerID = C.ID\n" +
                        " where U.CustomerID = " + this.customerID;
            this.oldEmployees = new DataTable();

            // Get the Data
            try
            {
                if (!this.silent)
                {
                    Console.WriteLine("Employee Get Data: " + query);
                }
                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!this.silent)
                {
                    Console.WriteLine("Employee DB Data Loaded.");
                }
                // Load into DataTable
                this.oldEmployees.Load(myReader);

                // Add some processing columns

                this.oldEmployees = Utility.addColumnToDataTable(this.oldEmployees, "Delete", "System.String");
                this.oldEmployees = Utility.addColumnToDataTable(this.oldEmployees, "Update", "System.String");


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }
            //Default some values
            for (int curRow = 0; curRow < this.oldEmployees.Rows.Count; curRow++)
            {
                //
            }


            return 0; // ok
        }

      


        // ---------------------------------------------
        // load the Location Lookup Validation information
        // ---------------------------------------------
        public int loadLocationLookup()
        {
            String query = " select ID = L.ID, Name = CONVERT(NVARCHAR(100),RTRIM(L.Name)),  InternalCode = CONVERT(NVARCHAR(100),RTRIM(L.InternalCode)) , New = 0\n" +
                        " from Location  L \n" +
                        " where L.CUstomerID = " + this.customerID;

            this.inLocations = new DataTable();

            // Get the Data
            try
            {
                if (!this.silent)
                {
                    Console.WriteLine("Location Lookup Get Data: " + query);
                }
                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!this.silent)
                {
                    Console.WriteLine("Location Lookup DB Data Loaded.");
                }
                // Load into DataTable
                this.inLocations.Load(myReader);



            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }


            return 0; // ok
        }

        // ---------------------------------------------
        // load the JobRole Lookup Validation information
        // ---------------------------------------------
        public int loadJobRoleLookup()
        {
            String query = " select ID = J.ID, Name = CONVERT(NVARCHAR(100),RTRIM(J.Name)), InternalCode = CONVERT(NVARCHAR(100),RTRIM(J.InternalCode)), New = 0 \n" +
                                    " from JobRole  J \n" +
                                    " where J.CUstomerID = " + this.customerID;

            this.inJobRoles = new DataTable();

            // Get the Data
            try
            {
                if (!this.silent)
                {
                    Console.WriteLine("JobRole Lookup Get Data: " + query);
                }
                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!this.silent)
                {
                    Console.WriteLine("Location Lookup DB Data Loaded.");
                }
                // Load into DataTable
                this.inJobRoles.Load(myReader);



            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }


            return 0; // ok
        }



        // ---------------------------------------------
        // load the Location Lookup Validation information
        // ---------------------------------------------
        public int loadAudienceLookup()
        {
            String query = " select ID = A.ID, Name = CONVERT(NVARCHAR(100),RTRIM(A.Name)),  InternalCode = CONVERT(NVARCHAR(100),RTRIM(A.InternalCode)) ,ParentAudienceID, New = 0\n" +
                        " from Audience A \n" +
                        " where A.CUstomerID = " + this.customerID;

            this.inAudiences= new DataTable();

            // Get the Data
            try
            {
                if (!this.silent)
                {
                    Console.WriteLine("Audience Lookup Get Data: " + query);
                }
                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!this.silent)
                {
                    Console.WriteLine("Audience Lookup DB Data Loaded.");
                }
                // Load into DataTable
                this.inAudiences.Load(myReader);



            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }


            return 0; // ok
        }


        // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int processChanges()
        {

            // Pre Process Changes to determine what to do. 
            // Execution of changes will occur next.
            // For each row
            //  1. find it in the existing db rows
            //  2.  Mark if it is new.
            //  3.  Error if it matches to more than one row.
            //  4.  Mark as update if it needs to change.

            // Load Client Information
            if (this.loadClientInfo() < 0)
            {
                opts.setExecute(false);// Cannot update if there is an issue with the client info
            }

            //if (this.customerID.Equals("80"))
            bool doPwdUpdates = false; // By default, if a passsword hash changes, do not flag it for update
            if (this.opts.getRuleValue("Rule_1_IgnorePasswordChanges").ToLower().Equals("no"))
            {

                //Console.WriteLine("Rule_1_IgnorePasswordChanges: no");
                doPwdUpdates = true;
            }





            this.stats.inbound_count = this.inEmployees.Rows.Count;
            this.stats.existing_count = this.oldEmployees.Rows.Count;
            int found_count = 0;

            // First Valid Row Check.

            for (int curRow = 0; curRow < this.inEmployees.Rows.Count; curRow++)
              {

                  if (this.opts.getRuleValue("Rule_16_SkipLastRow").ToLower().Equals("yes") && curRow >= this.inEmployees.Rows.Count - 1)
                  {
                      continue; //Skip
                  }

                    if(this.inEmployees.Rows[curRow]["UserName"].ToString().Length < 1 ){

                        this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                      this.inEmployees.Rows[curRow]["Error"] = "No Username.Skip (Code1).";
                        this.stats.error_count++;
                        continue; // Skip
                     }

                    // ---------------------------------------------------------------
                    // Custom Processing here:
                    //
                    // Skip Terminated Rule:
                    if (this.opts.getRuleValue("Rule_15_IgnoreTerms").ToLower().Equals("yes"))
                    {
                        string termstatus = this.inEmployees.Rows[curRow]["Status"].ToString().ToLower();

                        if (termstatus.Contains("2") || termstatus.Contains("terminated"))
                        {
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + ";Configured to Skip this terminated user.";
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1"; // Errors do not get updated
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";Configured to Skip this terminated user.";
                            continue;// Skip
                        }

                    }

                 

                    // Skip UserName field:
                    if (!this.opts.getRuleValue("Rule_SkipUserName").ToLower().Equals("no"))
                    {
                        string usernamelist = this.opts.getRuleValue("Rule_SkipUserName").ToLower();
                        string checkusername = this.inEmployees.Rows[curRow]["UserName"].ToString().ToLower();
                        if (usernamelist.Contains("," + checkusername + ",") || usernamelist.Contains("(" + checkusername + ",")  || usernamelist.Contains("," +checkusername + ")") || usernamelist.Contains("(" +checkusername + ")"))
                        {
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + ";Configured to Skip this user.";
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";Configured to Skip this user.";
                            continue;// Skip
                        }

                    }

                    // Skip Blank Employee Number field:
                    if (this.opts.getRuleValue("Rule_17_SkipBlankEmployeeNumber").ToLower().Equals("yes"))
                    {
                        string employeenumber = this.inEmployees.Rows[curRow]["employeenumber"].ToString().ToLower().Trim();

                        if (employeenumber.Length < 1 )
                        {
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + ";Configured to Skip blank employee numbers.";
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1"; // Errors do not get updated
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";Configured to Skip blank employee numbers.";
                            continue;// Skip
                        }

                    }

                        
                    // Skip Blank Job Roles field:
                    if (this.opts.getRuleValue("Rule_18_SkipBlankJobRoleCode").ToLower().Equals("yes"))
                    {
                        string jobrolecode = this.inEmployees.Rows[curRow]["jobrolecode"].ToString().ToLower().Trim();

                        if (jobrolecode.Length < 1 )
                        {
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + ";Configured to Skip blank jobrolecode.";
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1"; // Errors do not get updated
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";Configured to Skip blank jobrolecode.";
                            continue;// Skip
                        }

                    }


                    // Skip certan Supervisor fields :
                    if (!this.opts.getRuleValue("Rule_SkipSupevisorUpdateEmpID").ToLower().Equals("no"))
                    {
                        string skiplist = this.opts.getRuleValue("Rule_SkipSupevisorUpdateEmpID").ToLower();
                        string checkrptsup = this.inEmployees.Rows[curRow]["ReportingSupervisorCode"].ToString().ToLower();
                        
                        if (checkrptsup.Length > 0 && (skiplist.Contains("(" + checkrptsup + ",") || skiplist.Contains("," + checkrptsup + "," ) || skiplist.Contains("," + checkrptsup + ")")
                                                        || skiplist.Contains("(" + checkrptsup + ")") ))
                        {
                            this.inEmployees.Rows[curRow]["ReportingSupervisorCode"] = "0";
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + ";Skip ReportingSupervisor Update.";
                            Console.WriteLine("Skiplist Rpt: " + skiplist + " - " + checkrptsup);
                        }
                        string checksup = this.inEmployees.Rows[curRow]["SupervisorCode"].ToString().ToLower();

                        if (checksup.Length > 0 && (skiplist.Contains("(" + checksup + ",") || skiplist.Contains("," + checksup + ",") || skiplist.Contains("," + checksup + ")")
                                                        || skiplist.Contains("(" + checksup + ")")))
                        {
                            Console.WriteLine("Skiplist Sup: " + skiplist + " - " + checksup);
                            this.inEmployees.Rows[curRow]["SupervisorCode"] = "0";
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + ";Skip Supervisor Update.";

                        }

                    }

  
                    


                // Isabella
                // if (this.customerID.Equals("95"))
                if (this.opts.getRuleValue("Rule_4_PasswordPreset").Length > 0)
                {
                    //Console.WriteLine("Rule_4_PasswordPreset: " +  this.opts.getRuleValue("Rule_4_PasswordPreset"));
                    this.inEmployees.Rows[curRow]["Password"] = this.opts.getRuleValue("Rule_4_PasswordPreset");
                }

                // Hudson City ADP
                if (this.opts.getRuleValue("Rule_CombineDepartmentAndLocation").ToLower().Equals("yes"))
                {
                    //Console.WriteLine("Rule_4_PasswordPreset: " +  this.opts.getRuleValue("Rule_4_PasswordPreset"));
                    string locCode = this.inEmployees.Rows[curRow]["LocationCode"].ToString();
                    string DepartmentName = this.inEmployees.Rows[curRow]["Department"].ToString();
                    if( !locCode.Contains("-") ){
                        // Skip if The field is already combined ( like when running the response file )
                        if (DepartmentName.Length > 0)
                        {
                            if (locCode.Length > 0)
                            {
                             locCode = locCode + "-" + DepartmentName;
                            }
                            else
                            {
                             locCode = DepartmentName;
                            }
                        }
                        this.inEmployees.Rows[curRow]["LocationCode"] = locCode;
                    }//End Contains -
                    
                }
                // Bayview ADP
                if (this.opts.getRuleValue("Rule_CombineHomeDepartment").ToLower().Equals("yes"))
                {
                    //Console.WriteLine("Rule_4_PasswordPreset: " +  this.opts.getRuleValue("Rule_4_PasswordPreset"));
                    string locCode = Utility.UnPadColumnValue(this.inEmployees.Rows[curRow]["homedepartmentcode"].ToString());
                    string DepartmentName = this.inEmployees.Rows[curRow]["homedepartmentname"].ToString();
                    if (!locCode.Contains("-"))
                    {
                        // Skip if The field is already combined ( like when running the response file )
                        if (DepartmentName.Length > 0)
                        {
                            if (locCode.Length > 0)
                            {
                                locCode = locCode + "-" + DepartmentName;
                            }
                            else
                            {
                                locCode = DepartmentName;
                            }
                        }
                        this.inEmployees.Rows[curRow]["LocationCode"] = locCode;
                    }//End Contains -

                }
                // Beneficial
                if (this.opts.getRuleValue("Rule_CombineJobRoleAndCostCenter").ToLower().Equals("yes"))
                {
                    //Console.WriteLine("Rule_4_PasswordPreset: " +  this.opts.getRuleValue("Rule_4_PasswordPreset"));
                    string jobcode = this.inEmployees.Rows[curRow]["jobrolecode"].ToString();
                    string jobname = this.inEmployees.Rows[curRow]["jobrolename"].ToString();
                    string CostCenter = this.inEmployees.Rows[curRow]["costcentercode"].ToString();
                    if (!jobcode.Contains("-"))
                    {
                        // Skip if The field is already combined ( like when running the response file )
                        if (CostCenter.Length > 0)
                        {
                            if (jobcode.Length > 0)
                            {
                                jobcode = jobcode + "-" + CostCenter;
                                jobname = jobname + "-" + CostCenter;
                            }
                            else
                            {
                                jobcode = CostCenter;
                                jobname = CostCenter;
                            }
                        }
                        this.inEmployees.Rows[curRow]["JobRoleCode"] = jobcode;
                        this.inEmployees.Rows[curRow]["JobRoleName"] = jobname;
                    }//End Contains -

                }
                
                /*
                String ruleValue = this.opts.getRuleValue("Rule_11_PadEmployeeNumbers").ToLower();
                Console.WriteLine("PAD: " + ruleValue);



                if (ruleValue.Length > 0)
                {
                    try
                    {
                        int padlength = Convert.ToInt16(ruleValue);
                        //Console.WriteLine("PAD: " + ruleValue);
                        string oldNumber = this.inEmployees.Rows[curRow]["EmployeeNumber"].ToString().Trim();
                        string newNumber = Utility.PadColumnValue(oldNumber, padlength);
                        this.inEmployees.Rows[curRow]["EmployeeNumber"] = newNumber.Trim();
                        Console.WriteLine("Pad EmployeeNumber:" + padlength + " " + this.inEmployees.Rows[curRow]["EmployeeNumber"].ToString() + " |" + newNumber);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("BAD PAD NUMBER: " + ruleValue);
                    }

                }
                 */

                // File has an Email instead of an EmployeeNumber in the SupervisorCode field.
                // SupervisorEmail field has been added to the inEmployees Table. (done in the santizeColumns method)
                // Default the values with the Emails in the SupervisorCode field. (done in the sanitizeColumns method )
                // Later replace the SupervisorCodes with the EmployeeNumbers (done here)

                if (this.opts.getRuleValue("Rule_UseSupervisorEmail").ToLower().Equals("yes"))
                {


                    // Find existing Supervisor Row using Supervisor UserName
                    string Sup_Email = this.inEmployees.Rows[curRow]["SupervisorEmail"].ToString();

                    if (Sup_Email.Length > 0)
                    {
                        DataRow[] foundSupRows;
                        foundSupRows = this.inEmployees.Select("Email = '" + Sup_Email.Replace("'", "''") + "'");
                        if (foundSupRows.Length < 1)

                            if (foundSupRows.Length < 1)
                            {
                                // Find using the EmployeeNumber from the existing records.
                                Console.WriteLine("Email not found for Supervisor Lookup in the existing records: " + Sup_Email);

                            }
                        if (foundSupRows.Length > 0)
                        {
                            // The SupervisorCode lookup has found a match. 

                            this.inEmployees.Rows[curRow]["SupervisorCode"] = foundSupRows[0]["EmployeeNumber"];

                        }
                    } // End Sup_Email not blank. Skipped if blanked

                    // Find existing Supervisor Row using Supervisor UserName
                    string RSup_Email = this.inEmployees.Rows[curRow]["ReportingSupervisorEmail"].ToString();

                    if (RSup_Email.Length > 0)
                    {
                        DataRow[] foundSupRows;
                        foundSupRows = this.inEmployees.Select("Email = '" + RSup_Email.Replace("'", "''") + "'");
                        if (foundSupRows.Length < 1)

                            if (foundSupRows.Length < 1)
                            {
                                // Find using the EmployeeNumber from the existing records.
                                Console.WriteLine("Email not found for Supervisor Lookup in the existing records: " + RSup_Email);

                            }
                        if (foundSupRows.Length > 0)
                        {
                            // The SupervisorCode lookup has found a match. 

                            this.inEmployees.Rows[curRow]["ReportingSupervisorCode"] = foundSupRows[0]["EmployeeNumber"];

                        }
                    } // End RSup_Email not blank. Skipped if blanked

                }

                // File has a userName instead of an EmployeeNumber in the SupervisorCode field.
                // SupervisorUserName field has been added to the inEmployees Table. (done in the santizeColumns method)
                // Default the values with the UserNames in the SupervisorCode field. (done in the sanitizeColumns method )
                // Later replace the SupervisorCodes with the EmployeeNumbers (done here)
                if (this.opts.getRuleValue("Rule_UseSupervisorUserName").ToLower().Equals("yes"))
                {
                    //this.inEmployees.Rows[curRow]["SupervisorUserName"] = this.inEmployees.Rows[curRow]["SupervisorCode"];

                    // Find existing Supervisor Row using Supervisor UserName
                    string Sup_UserName = this.inEmployees.Rows[curRow]["SupervisorUserName"].ToString();

                    if (Sup_UserName.Length > 0)
                    {
                        DataRow[] foundSupRows;
                        foundSupRows = this.inEmployees.Select("Username = '" + Sup_UserName.Replace("'", "''") + "'");
                        if (foundSupRows.Length < 1)
                        {
                            // Find using the EmployeeNumber from the existing records.
                            Console.WriteLine("UserName not found for Supervisor Lookup: " + Sup_UserName);
                            foundSupRows = this.oldEmployees.Select("Username = '" + Sup_UserName.Replace("'", "''") + "'");

                        }
                        if (foundSupRows.Length < 1)
                        {
                            // Find using the EmployeeNumber from the existing records.
                            Console.WriteLine("UserName not found for Supervisor Lookup in the existing records: " + Sup_UserName);

                        }
                        if (foundSupRows.Length > 0)
                        {
                            // The SupervisorCode lookup has found a match. 

                            this.inEmployees.Rows[curRow]["SupervisorCode"] = foundSupRows[0]["EmployeeNumber"];

                        }
                    } // End Sup_UserName not blank. Skipped if blanked

                    // Now do this for the ReportingSupervisorUserName/Codes  

                    // Find existing Supervisor Row using Supervisor UserName
                    string RptSup_UserName = this.inEmployees.Rows[curRow]["ReportingSupervisorUserName"].ToString();
                    if (RptSup_UserName.Length > 0)
                    {
                        DataRow[] foundRptSupRows;
                        foundRptSupRows = this.inEmployees.Select("Username = '" + RptSup_UserName.Replace("'", "''") + "'");
                        if (foundRptSupRows.Length < 1)
                        {
                            // Find using the EmployeeNumber from the existing records.
                            Console.WriteLine("UserName not found for Reporting Supervisor Lookup: " + RptSup_UserName);
                            foundRptSupRows = this.oldEmployees.Select("Username = '" + RptSup_UserName.Replace("'", "''") + "'");

                        }
                        if (foundRptSupRows.Length < 1)
                        {
                            // Find using the EmployeeNumber from the existing records.
                            Console.WriteLine("UserName not found for Reporting Supervisor Lookup in the existing records: " + RptSup_UserName);

                        }
                        if (foundRptSupRows.Length > 0)
                        {
                            // The SupervisorCode lookup has found a match. ( only for custom reporting supervisor Name based )

                            this.inEmployees.Rows[curRow]["ReportingSupervisorCode"] = foundRptSupRows[0]["EmployeeNumber"];

                        }
                    }// End If RptSupervisor UserName is not blank (skip) ( only for custom reporting supervisor Name based )
                    else  if (this.opts.getRuleValue("Rule_UseSupervisorAsReportingSupervisor").ToLower().Equals("yes"))
                     {
                         this.inEmployees.Rows[curRow]["ReportingSupervisorCode"] = this.inEmployees.Rows[curRow]["SupervisorCode"];
                     } // End RptSupervisor


                } // ENd custom rule



                // File has a Full Name instead of an EmployeeNumber as the Supervisor field.
                // SupervisorFullName field has been added to the inEmployees Table. (done in the santizeColumns method)
                // FullName field has been added to the inEmployees Table. (done in the santizeColumns method)
                // Default the values with the Full Name in the FullName field. (done in the sanitizeColumns method )
                // Full Name brought back on the existing Emp load (done in the loadEmployeeFromDB method )
                // Later replace the SupervisorCodes with the EmployeeNumbers (done here)
                if (this.opts.getRuleValue("Rule_UseSupervisorFullName").ToLower().Equals("yes") ||
                    this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle").ToLower().Equals("yes") ||
                     this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle2").ToLower().Equals("yes") ||
                     this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle3").ToLower().Equals("yes") ||
                     this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle4").ToLower().Equals("yes")
                    )
                {


                    // Find existing Supervisor Row using Supervisor UserName
                    string Sup_FullName = this.inEmployees.Rows[curRow]["SupervisorFullName"].ToString();
                    if (!this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle3").ToLower().Equals("yes"))
                    {
                        // These options do not have Jr. imbedded
                        Sup_FullName = this.inEmployees.Rows[curRow]["SupervisorFullName"].ToString().Replace(" Jr.", "");
                        Sup_FullName = Sup_FullName.Replace(" jr.", "");
                    }
                    DataRow[] foundSupRows;
                    foundSupRows = this.inEmployees.Select("FullName = '" + Sup_FullName.Replace("'", "''") + "'");
                    if (foundSupRows.Length < 1)
                    {
                        // Find using the EmployeeNumber from the existing records.
                        Console.WriteLine("FullName not found for Supervisor Lookup: " + Sup_FullName);
                        foundSupRows = this.oldEmployees.Select("FullName = '" + Sup_FullName.Replace("'", "''") + "'");

                    }
                    if (foundSupRows.Length < 1)
                    {
                        // Find using the EmployeeNumber from the existing records.
                        Console.WriteLine("FullName not found for Supervisor Lookup in the existing records: " + Sup_FullName);

                    }
                    if (foundSupRows.Length > 0)
                    {
                        // The SupervisorCode lookup has found a match. ( Custom FullName logic only )

                        this.inEmployees.Rows[curRow]["SupervisorCode"] = foundSupRows[0]["EmployeeNumber"];
                        if (this.opts.getRuleValue("Rule_UseSupervisorAsReportingSupervisor").ToLower().Equals("yes"))
                        {
                            this.inEmployees.Rows[curRow]["ReportingSupervisorCode"] = foundSupRows[0]["EmployeeNumber"];
                        }
                    }



                } // ENd custom rule


                // File has a First and Last name instead of an EmployeeNumber as the Supervisor fields.
                // Find and Match on First and Last Name
                if (this.opts.getRuleValue("Rule_UseSupervisorFirstAndLastName").ToLower().Equals("yes"))
                {


                    // Find existing Supervisor Row using Supervisor First and Last
                    string Sup_FirstName = this.inEmployees.Rows[curRow]["SupervisorFirstName"].ToString().Replace(" Jr.", "");
                    string Sup_LastName = this.inEmployees.Rows[curRow]["SupervisorLastName"].ToString().Replace(" Jr.", "");
   
                    DataRow[] foundSupRows;
                    foundSupRows = this.inEmployees.Select("FirstName = '" + Sup_FirstName.Replace("'", "''") + "' and LastName = '" + Sup_LastName.Replace("'", "''") + "'");
                    if (foundSupRows.Length < 1)
                    {
                        // Find using the EmployeeNumber from the existing records.
                        Console.WriteLine("FirstAndLast Name not found for Supervisor Lookup: " + Sup_LastName + ", " + Sup_FirstName);
                        foundSupRows = this.oldEmployees.Select("FirstName = '" + Sup_FirstName.Replace("'", "''") + "' and LastName = '" + Sup_LastName.Replace("'", "''") + "'");

                    }
                    if (foundSupRows.Length < 1)
                    {
                        // Find using the EmployeeNumber from the existing records.
                        Console.WriteLine("FirstAndLast Name not found for Supervisor Lookup in the existing records: " + Sup_LastName + "," + Sup_FirstName);

                    }
                    if (foundSupRows.Length > 0)
                    {
                        // The SupervisorCode lookup has found a match. ( Custom first and Last Name only )

                        this.inEmployees.Rows[curRow]["SupervisorCode"] = foundSupRows[0]["EmployeeNumber"];
                        if (this.opts.getRuleValue("Rule_UseSupervisorAsReportingSupervisor").ToLower().Equals("yes"))
                        {
                            this.inEmployees.Rows[curRow]["ReportingSupervisorCode"] = foundSupRows[0]["EmployeeNumber"];
                        }

                    }



                } // ENd custom rule

               // If no other special processing rules that mess around with supervisor names instead of codes.
                // then make sure to process the supervisor to reporting supervisor copy.
               if (this.opts.getRuleValue("Rule_UseSupervisorAsReportingSupervisor").ToLower().Equals("yes") &&
                    !(this.opts.getRuleValue("Rule_UseSupervisorFirstAndLastName").ToLower().Equals("yes")) &&
                    !(this.opts.getRuleValue("Rule_UseSupervisorFullName").ToLower().Equals("yes") ||
                    this.opts.getRuleValue("Rule_UseSupervisorFullNameWithMiddle").ToLower().Equals("yes")) &&
                    !(this.opts.getRuleValue("Rule_UseSupervisorUserName").ToLower().Equals("yes"))
                    )
               {
                         this.inEmployees.Rows[curRow]["ReportingSupervisorCode"] = this.inEmployees.Rows[curRow]["SupervisorCode"];
               } 

                // ---------------------------- End custom processing -----------------------------------------------

                // These are the new values on the file
                string Emp_EmployeeNumber = this.inEmployees.Rows[curRow]["EmployeeNumber"].ToString().Trim();
                string Emp_Username = this.inEmployees.Rows[curRow]["Username"].ToString().Trim();
                string Emp_FirstName = this.inEmployees.Rows[curRow]["FirstName"].ToString().Trim();
                string Emp_LastName = this.inEmployees.Rows[curRow]["LastName"].ToString().Trim();
                string Emp_MiddleName = this.inEmployees.Rows[curRow]["MiddleName"].ToString().Trim();
                string Emp_HireDate = this.inEmployees.Rows[curRow]["HireDate"].ToString();
                string Emp_NewHireIndicator = this.inEmployees.Rows[curRow]["NewHireIndicator"].ToString();
                string Emp_SupervisorCode = this.inEmployees.Rows[curRow]["SupervisorCode"].ToString();
                string Emp_ReportingSupervisorCode = this.inEmployees.Rows[curRow]["ReportingSupervisorCode"].ToString();
                string Emp_LocationCode = this.inEmployees.Rows[curRow]["LocationCode"].ToString();
                string Emp_JobRoleCode = this.inEmployees.Rows[curRow]["JobRoleCode"].ToString();
                string Emp_Email = this.inEmployees.Rows[curRow]["Email"].ToString().Trim();
                string Emp_Status = this.inEmployees.Rows[curRow]["Status"].ToString();
                string Emp_Password = this.inEmployees.Rows[curRow]["Password"].ToString().Trim();
                string Emp_Notes = this.inEmployees.Rows[curRow]["Notes"].ToString().Trim();
                string Emp_AudienceCode = this.inEmployees.Rows[curRow]["AudienceCode"].ToString().Trim();

                // Additional Optional fields for automatically adding JobRoles and Locations
                string Emp_JobRoleName  = "";
                string Emp_LocationName  = "";
                string Emp_AudienceName = "";
                if( this.inEmployees.Columns.Contains("JobRoleName")){
                    Emp_JobRoleName = this.inEmployees.Rows[curRow]["JobRoleName"].ToString();
                }
                 if( this.inEmployees.Columns.Contains("LocationName")){
                    Emp_LocationName = this.inEmployees.Rows[curRow]["LocationName"].ToString();
                }
                 if (this.inEmployees.Columns.Contains("AudienceName"))
                 {
                     Emp_AudienceName = this.inEmployees.Rows[curRow]["AudienceName"].ToString();
                 }

                // Default Some of the Metadata. 
                this.inEmployees.Rows[curRow]["UpdateID"] = "";
                this.inEmployees.Rows[curRow]["OldUsername"] = "";
                this.inEmployees.Rows[curRow]["OldEmployeeNumber"] = "";
                this.inEmployees.Rows[curRow]["OldLocationCode"] = "";
                this.inEmployees.Rows[curRow]["OldLocationID"] = "";
                this.inEmployees.Rows[curRow]["OldJobRoleCode"] = "";
                this.inEmployees.Rows[curRow]["OldJobRoleID"] = "";
                this.inEmployees.Rows[curRow]["OldSupervisorCode"] = "";
                this.inEmployees.Rows[curRow]["OldSupervisorID"] = "";
                this.inEmployees.Rows[curRow]["OldReportingSupervisorCode"] = "";
                this.inEmployees.Rows[curRow]["OldReportingSupervisorID"] = "";
                this.inEmployees.Rows[curRow]["OldHireDate"] = "";
                this.inEmployees.Rows[curRow]["OldNewHireIndicator"] = "";
                this.inEmployees.Rows[curRow]["OldFirstName"] = "";
                this.inEmployees.Rows[curRow]["OldLastName"] = "";
                this.inEmployees.Rows[curRow]["OldMiddleName"] = "";
                this.inEmployees.Rows[curRow]["OldEmail"] = "";
                this.inEmployees.Rows[curRow]["OldStatus"] = "";
                this.inEmployees.Rows[curRow]["OldAudienceCode"] = "";

                // Employee Code Lookup list. To double check that the supervisors exist.
                if (!this.empCodeList.ContainsKey(Emp_EmployeeNumber))
                {

                    this.empCodeList.Add(Emp_EmployeeNumber, "1");
                }
                else
                {
                    Console.WriteLine("Duplicate Employee?" + Emp_EmployeeNumber);
                }
                // Find Duplicate Rows using username value in the inbound file... if any
                DataRow[] foundRowsDup;
                foundRowsDup = this.inEmployees.Select("Username = '" + Emp_Username.Replace("'", "''") + "' and Status = '1'");
                if (foundRowsDup.Length > 1)
                {
                    this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                    this.inEmployees.Rows[curRow]["Error"] = "Duplicate Username in the Inbound File.Skip.";
                    this.stats.error_count++;
                    continue;
                }

                // Find existing Row using Code value ... if any
                DataRow[] foundRows;
                foundRows = this.oldEmployees.Select("Username = '" + Emp_Username.Replace("'", "''") + "'");
                if (foundRows.Length < 1)
                {
                    // Find using the EMployeeNumber
                    Console.WriteLine("No UserName found for: " + Emp_Username);
                    foundRows = this.oldEmployees.Select("EmployeeNumber = '" + Emp_EmployeeNumber.Replace("'", "''") + "'");

                }

                if (foundRows.Length < 1)
                {
                    Console.WriteLine("No EmployeeNumber found for: " + Emp_EmployeeNumber);
                     // ---------------------------------------------------------------
                    /// NEW USER
                    // ---------------------------------------------------------------


                    // Not Found. Mark as New. Unless they have a Status other than '1' or '3' (LOA). Deleted. Never should have been.
                    if ((Emp_Status.Equals("1") || Emp_Status.Equals("3") ) && Emp_Username.Length > 0)
                    {
                        this.inEmployees.Rows[curRow]["New"] = "1";
                        this.inEmployees.Rows[curRow]["Update"] = "1";
                        this.inEmployees.Rows[curRow]["UpdateLocationCode"] = "1";
                        this.inEmployees.Rows[curRow]["UpdateJobRoleCode"] = "1";
                        this.inEmployees.Rows[curRow]["UpdateSupervisorCode"] = "1";
                        this.inEmployees.Rows[curRow]["UpdateReportingSupervisorCode"] = "1";
                        this.stats.insert_count++;

                    }
                    else if (Emp_Username.Length < 1)
                    {
                        this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                        this.inEmployees.Rows[curRow]["Error"] = "No Username.Skip (Code2).";
                        this.stats.error_count++;
                    }
                    else
                    {
                        this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                        this.inEmployees.Rows[curRow]["Error"] = "New Employee with an inactive status. Skip.";
                        //this.stats.error_count++;  Ignore from a warning count perspective
                    }


                    // Check Location, JobRole, and Supervisor Lookups
                    if ((Emp_LocationCode.Length > 0 ) && (this.lookupLocation(curRow, Emp_LocationCode, Emp_LocationName) < 1))
                    {
                        // Lookup is not there. Added into the location, or mark as an error
                        if (this.opts.getRuleValue("Rule_Insert_NewLocations").ToLower().Equals("yes"))
                        {
                            // These will be added automatically for this configuration
                            //Console.WriteLine("Adding Location: " + Emp_LocationCode);

                        }else{

                            // Error. Location is not found for New User.
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";New Employee with an unknown Location: " + Emp_LocationCode;
                            this.inEmployees.Rows[curRow]["UpdateLocationCode"] = "0";
                            this.stats.error_count++;
                        }

                    }

                    if ((Emp_JobRoleCode.Length > 0) && (this.lookupJobRole(curRow, Emp_JobRoleCode, Emp_JobRoleName) < 1))
                    {
                        // Lookup is not there. Added to the Locations or mark as an error
                        if (this.opts.getRuleValue("Rule_Insert_NewJobRoles").ToLower().Equals("yes"))
                        {
                            // These will be added automatically for this configuration
                            //Console.WriteLine("Adding JobRole: " + Emp_JobRoleCode);

                        }
                        else
                        {

                            // Error. JobROle is not found for New User, and this configuration does not automatically add these in.
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";New Employee with an unknown JobRole: " + Emp_JobRoleCode;
                            this.inEmployees.Rows[curRow]["UpdateJobRoleCode"] = "0";
                            this.stats.error_count++;
                        }

                    }

                    // Update/Add Audience options
                    if (this.opts.getRuleValue("Rule_UpdateAudiences").ToLower().Equals("yes"))
                    {
                        if ((Emp_AudienceCode.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceCode, Emp_AudienceName) < 1))
                        {
                            // Lookup is not there. Added to the Locations or mark as an error
                            if (this.opts.getRuleValue("Rule_Insert_NewAudiences").ToLower().Equals("yes"))
                            {
                                // These will be added automatically for this configuration ( stored as 'New' in the audience lookup table.
                                Console.WriteLine("Adding Audience: " + Emp_AudienceCode);

                            }
                            else
                            {

                                // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                                this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";New Employee with an unknown Audience: " + Emp_AudienceCode;
                                this.inEmployees.Rows[curRow]["UpdateAudienceCode"] = "0";
                                this.stats.error_count++;
                            }// end auto insert check

                        } //end lookup
                    }//End update Audience option


                    // Update/Add Custom Audience options
                    if (this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes"))
                    {

                        string Emp_AudienceCostCenter = this.inEmployees.Rows[curRow]["AudienceCostCenter"].ToString().Trim();
                        string Emp_AudienceExempt = this.inEmployees.Rows[curRow]["AudienceExempt"].ToString().Trim();
                        string Emp_AudienceEmpLevel = this.inEmployees.Rows[curRow]["AudienceEmpLevel"].ToString().Trim();
                        string Emp_AudienceManager = this.inEmployees.Rows[curRow]["AudienceManager"].ToString().Trim();
                        string Emp_Type = this.inEmployees.Rows[curRow]["AudienceManager"].ToString().Trim();

                        // Get two in list of possible parent ids. The intersection of the two lists is the parent it
                        // needs to be associated to. Type = HCF, MCF, CBD, MBF; 
                        
                        string atype = Emp_AudienceExempt.Substring(0,3);
                        string typeparentids = this.opts.getRuleValue("Rule_Audience" + atype + "_ParentID").ToLower();
                        //Console.WriteLine("Types: " + atype + ":" + typeparentids );    
                       

                        string parentids = this.opts.getRuleValue("Rule_AudienceCostCenter_ParentID").ToLower();
                        if ((Emp_AudienceCostCenter.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceCostCenter, Emp_AudienceCostCenter, parentids, typeparentids) < 1))
                        {
            
                                // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                                //this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                //this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";New Employee with an unknown Cost Center Audience: " + Emp_AudienceCostCenter;
                                //this.inEmployees.Rows[curRow]["UpdateAudienceCostCenter"] = "0";
                                //this.stats.error_count++;
                     

                        } //end lookup

                        parentids = this.opts.getRuleValue("Rule_AudienceExempt_ParentID").ToLower();
                        if ((Emp_AudienceExempt.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceExempt, Emp_AudienceExempt, parentids, typeparentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            //this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            //this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";New Employee with an unknown Excempt Status Audience: " + Emp_AudienceExempt;
                            //this.inEmployees.Rows[curRow]["UpdateAudienceExempt"] = "0";
                            //this.stats.error_count++;


                        } //end lookup

                         parentids = this.opts.getRuleValue("Rule_AudienceEmpLevel_ParentID").ToLower();
                         if ((Emp_AudienceEmpLevel.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceEmpLevel, Emp_AudienceEmpLevel, parentids, typeparentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            //this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            //this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";New Employee with an unknown Emp Level Audience: " + Emp_AudienceEmpLevel;
                            //this.inEmployees.Rows[curRow]["UpdateAudienceEmpLevel"] = "0";
                            //this.stats.error_count++;


                        } //end lookup

                         parentids = this.opts.getRuleValue("Rule_AudienceManager_ParentID").ToLower();
                         if ((Emp_AudienceManager.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceManager, Emp_AudienceManager, parentids, typeparentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            //this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            //this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";New Employee with an unknown Manager Status Audience: " + Emp_AudienceManager;
                            //this.inEmployees.Rows[curRow]["UpdateAudienceManager"] = "0";
                            //this.stats.error_count++;


                        } //end lookup
                    }//End update Audience option
                    
                    // Update/Add Custom Audience options
                    if (this.opts.getRuleValue("Rule_AudienceCustom1").ToLower().Equals("yes"))
                    {

                        string Emp_AudienceCustom1 = this.inEmployees.Rows[curRow]["AudienceCustom1"].ToString().Trim();



                        string parentids = "0";// this.opts.getRuleValue("Rule_AudienceCustom1_ParentIDs").ToLower();

                        if ((Emp_AudienceCustom1.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceCustom1, Emp_AudienceCustom1, parentids, parentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";New Employee with an unknown  Audience C1: " + Emp_AudienceCustom1;
                            this.inEmployees.Rows[curRow]["UpdateAudienceCustom1"] = "";
                            this.stats.error_count++;


                        } //end lookup

                    }
                    // Update/Add Custom Audience options
                    if (this.opts.getRuleValue("Rule_AudienceCustom2").ToLower().Equals("yes"))
                    {

                        string Emp_AudienceCustom2 = this.inEmployees.Rows[curRow]["AudienceCustom2"].ToString().Trim();



                        string parentids = "0";//this.opts.getRuleValue("Rule_AudienceCustom2_ParentIDs").ToLower();

                        if ((Emp_AudienceCustom2.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceCustom2, Emp_AudienceCustom2, parentids, parentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";New Employee with an unknown  Audience C2: " + Emp_AudienceCustom2;
                            this.inEmployees.Rows[curRow]["UpdateAudienceCustom2"] = "";
                            this.stats.error_count++;


                        } //end lookup

                    }


                }
                else if (foundRows.Length > 1)
                {
                    // ---------------------------------------------------------------
                    /// SKIP UPDATE ( Not Found )
                    // ---------------------------------------------------------------

                    
                    // Too many Matches. Error.
                    this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                    this.inEmployees.Rows[curRow]["Error"] = "Employee found too many existing record matches(" + foundRows.Length + ")";
                    this.inEmployees.Rows[curRow]["New"] = "0";
                    this.stats.error_count++;
                    continue;
                }
                else
                {
                    // ---------------------------------------------------------------
                    ///  UPDATE USER
                    // ---------------------------------------------------------------

                    // Mark existing record as found.
                    foundRows[0]["Delete"] = "No";
                    found_count++;

                    // Update Might be needed. Compare the new values to the old values to make sure:

                    this.inEmployees.Rows[curRow]["New"] = "0";
                    this.inEmployees.Rows[curRow]["UpdateID"] = foundRows[0]["ID"].ToString();
                    this.inEmployees.Rows[curRow]["UpdateAUID"] = foundRows[0]["WebUserID"].ToString();


                    // Track the existing values for undo logic and logging.
                    this.inEmployees.Rows[curRow]["OldUsername"] = foundRows[0]["Username"].ToString();
                    this.inEmployees.Rows[curRow]["OldEmployeeNumber"] = foundRows[0]["EmployeeNumber"].ToString();
                    this.inEmployees.Rows[curRow]["OldFirstName"] = foundRows[0]["FirstName"].ToString();
                    this.inEmployees.Rows[curRow]["OldLastName"] = foundRows[0]["LastName"].ToString();
                    this.inEmployees.Rows[curRow]["OldMiddleName"] = foundRows[0]["MiddleName"].ToString();
                    this.inEmployees.Rows[curRow]["OldHireDate"] = foundRows[0]["HireDate"].ToString();
                    this.inEmployees.Rows[curRow]["OldNewHireIndicator"] = foundRows[0]["NewHireIndicator"].ToString();
                    this.inEmployees.Rows[curRow]["OldLocationCode"] = foundRows[0]["LocationCode"].ToString();
                    this.inEmployees.Rows[curRow]["OldLocationID"] = foundRows[0]["LocationID"].ToString();
                    this.inEmployees.Rows[curRow]["OldJobRoleCode"] = foundRows[0]["JobRoleCode"].ToString();
                    this.inEmployees.Rows[curRow]["OldJobRoleID"] = foundRows[0]["JobRoleID"].ToString();
                    this.inEmployees.Rows[curRow]["OldSupervisorCode"] = foundRows[0]["SupervisorCode"].ToString();
                    this.inEmployees.Rows[curRow]["OldSupervisorID"] = foundRows[0]["SupervisorID"].ToString();
                    this.inEmployees.Rows[curRow]["OldReportingSupervisorCode"] = foundRows[0]["ReportingSupervisorCode"].ToString();
                    this.inEmployees.Rows[curRow]["OldReportingSupervisorID"] = foundRows[0]["ReportingSupervisorID"].ToString();
                    this.inEmployees.Rows[curRow]["OldEmail"] = foundRows[0]["Email"].ToString();
                    this.inEmployees.Rows[curRow]["OldStatus"] = foundRows[0]["StatusID"].ToString();
                    this.inEmployees.Rows[curRow]["OldPassword"] = foundRows[0]["Password"].ToString();
                    this.inEmployees.Rows[curRow]["OldPasswordSalt"] = foundRows[0]["PasswordSalt"].ToString();
                    this.inEmployees.Rows[curRow]["OldAudienceCode"] = foundRows[0]["AudienceCode"].ToString();

                    if (this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes"))
                    {

                        this.inEmployees.Rows[curRow]["OldAudienceCostCenter"] = foundRows[0]["AudienceCostCenter"].ToString();
                        this.inEmployees.Rows[curRow]["OldAudienceExempt"] = foundRows[0]["AudienceExempt"].ToString();
                        this.inEmployees.Rows[curRow]["OldAudienceEmpLevel"] = foundRows[0]["AudienceEmpLevel"].ToString();
                        this.inEmployees.Rows[curRow]["OldAudienceManager"] = foundRows[0]["AudienceManager"].ToString();
               
                  
                    }
                    if (this.opts.getRuleValue("Rule_AudienceCustom1").ToLower().Equals("yes"))
                    {
                        this.inEmployees.Rows[curRow]["OldAudienceCustom1"] = foundRows[0]["AudienceCustom1"].ToString();
                    }
                    if (this.opts.getRuleValue("Rule_AudienceCustom2").ToLower().Equals("yes"))
                    {
                        this.inEmployees.Rows[curRow]["OldAudienceCustom2"] = foundRows[0]["AudienceCustom2"].ToString();
                    }

                    string Old_UserName = foundRows[0]["Username"].ToString();
                    string Old_EmployeeNumber = foundRows[0]["EmployeeNumber"].ToString();
                    string Old_FirstName = foundRows[0]["FirstName"].ToString();
                    string Old_LastName = foundRows[0]["LastName"].ToString();
                    string Old_MiddleName = foundRows[0]["MiddleName"].ToString();
                    string Old_HireDate = foundRows[0]["HireDate"].ToString();
                    string Old_NewHireIndicator = foundRows[0]["NewHireIndicator"].ToString();
                    string Old_LocationCode = foundRows[0]["LocationCode"].ToString();
                    string Old_LocationID = foundRows[0]["LocationID"].ToString();
                    string Old_JobRoleCode = foundRows[0]["JobRoleCode"].ToString();
                    string Old_JobRoleID = foundRows[0]["JobRoleID"].ToString();
                    string Old_SupervisorCode = foundRows[0]["SupervisorCode"].ToString();
                    string Old_ReportingSupervisorID = foundRows[0]["ReportingSupervisorID"].ToString();
                    string Old_ReportingSupervisorCode = foundRows[0]["ReportingSupervisorCode"].ToString();
                    string Old_SupervisorID = foundRows[0]["SupervisorID"].ToString();
                    string Old_Email = foundRows[0]["Email"].ToString();
                    string Old_Status = foundRows[0]["StatusID"].ToString();
                    string Old_Password = foundRows[0]["Password"].ToString();
                    string Old_PasswordSalt = foundRows[0]["PasswordSalt"].ToString();
                    string Old_Notes = foundRows[0]["Notes"].ToString();
                    string Old_AudienceCode = foundRows[0]["AudienceCode"].ToString();
                    


                    //NBT
                    if (this.opts.getRuleValue("Rule_2_BlankOutEmails").ToLower().Equals("yes"))
                    //if (this.customerID.Equals("80"))
                    {
                        //Console.WriteLine("Rule_2_BlankOutEmails: Yes");
                        // Blank out email
                        Emp_Email = "";
                        this.inEmployees.Rows[curRow]["Email"] = "";

                    }
                    if (this.opts.getRuleValue("Rule_1_IgnorePasswordChanges").ToLower().Equals("no"))
                    {
                        //Console.WriteLine("Rule_1_IgnorePasswordChanges: No");
                        // Updat the passwords
                        doPwdUpdates = true;

                    }

                    Boolean hasChanged = false;
                    if (Emp_Username.Length > 0 && !Old_UserName.Trim().ToLower().Equals(Emp_Username.ToLower()))
                    {
                        this.inEmployees.Rows[curRow]["Update"] = "1";
                        this.inEmployees.Rows[curRow]["UpdateUsername"] = "1";
                        this.stats.addColumnCount("Username");
                        this.inEmployees.Rows[curRow]["UpdateMsg"] = "Username !=" + Old_UserName + ";";
                        hasChanged = true;
                    }
                    if (!Old_EmployeeNumber.Trim().Equals(Emp_EmployeeNumber))
                    {
                        this.inEmployees.Rows[curRow]["Update"] = "1";
                        this.inEmployees.Rows[curRow]["UpdateEmployeeNumber"] = "1";
                        this.stats.addColumnCount("EmployeeNumber");
                        this.inEmployees.Rows[curRow]["UpdateMsg"] = "EmployeeNumber !=" + Old_EmployeeNumber + ";";
                        hasChanged = true;
                    }
                    if (!Old_FirstName.Trim().Equals(Emp_FirstName.Trim()))
                    {
                        this.inEmployees.Rows[curRow]["Update"] = "1";
                        this.inEmployees.Rows[curRow]["UpdateFirstName"] = "1";
                        this.stats.addColumnCount("FirstName");
                        this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "FirstName !=" + Old_FirstName + ";";
                        hasChanged = true;
                    }
                    if (!Old_LastName.Trim().Equals(Emp_LastName.Trim()))
                    {
                        this.inEmployees.Rows[curRow]["Update"] = "1";
                        this.inEmployees.Rows[curRow]["UpdateLastName"] = "1";
                        this.stats.addColumnCount("LastName");
                        this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "LastName !=" + Old_LastName + ";";
                        hasChanged = true;
                    }
                    if (Emp_Status.Trim().ToLower().Equals("deleted"))
                    {
                        Emp_Status = "4";
                    }
                    // Cadence rule goes here:
                    if (!Old_Status.Trim().Equals(Emp_Status.Trim()))
                    {

                        if (this.opts.getRuleValue("Rule_DoNotReactivateStatus").ToLower().Equals("yes") &&
                                !Old_Status.Trim().Equals("1") )
                        {
                            // Do not Update the status ( customer does not want to reactive users  )
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "Do Not Reactivate. Status !=" + Old_Status + ";";
                        }
                        else  // Update the status
                        {

                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateStatus"] = "1";
                            this.stats.addColumnCount("Status");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "Status !=" + Old_Status + ";";
                            hasChanged = true;
                        }
                    }


                    // ------------ Notes Compare -------------------------------
                    // Check to see if the new note is already a part of the record.
                    // Convert the notes to lowercase, no whitespace, no quotes for the compare
                    String filteredNote = Emp_Notes.ToLower().Trim().Replace("'", "");
                    filteredNote = filteredNote.Replace(" ", "");

                    string oldnote = Old_Notes.ToLower().Trim().Replace("'", "");
                    oldnote = oldnote.Replace(" ", "");
                    if (!oldnote.Contains(filteredNote))
                    {
                   
                        this.inEmployees.Rows[curRow]["Update"] = "1";
                        this.inEmployees.Rows[curRow]["UpdateNotes"] = "1";
                        this.stats.addColumnCount("Notes");
                        this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + " Adding Note.";
                        hasChanged = true;
                    }


                    // New Hire Indicator Check. ( If New Hire Days check is used then skip, or if adds are auto new hire then skip )
                    if ( !(this.opts.getRuleValue("Rule_NewHireUpdateDays").ToLower().Length > 0 ) &&
                         !(this.opts.getRuleValue("Rule_ForceNewHireOnAdd").ToLower().Length > 0 ) &&
                         !(this.opts.getRuleValue("Rule_ForceNewHireOnAddUseHireDate").ToLower().Length > 0) &&
                        !Old_NewHireIndicator.ToLower().Trim().Equals(Emp_NewHireIndicator.Trim().ToLower())
                       ) 
                    {


                        this.inEmployees.Rows[curRow]["Update"] = "1";
                        this.inEmployees.Rows[curRow]["UpdateNewHireIndicator"] = "1";
                        this.stats.addColumnCount("NewHireIndicator");
                        this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "NewHireIndicator !=" + Old_NewHireIndicator + ";";
                        hasChanged = true;
                    }


                    // ------------------------------------------------------------------------------
                    // Hire Date Compare
                    DateTime old_hiredt = DateTime.MinValue;
                    DateTime new_hiredt = DateTime.MinValue;

                

                    try
                    {
                       old_hiredt = Convert.ToDateTime(Old_HireDate);

                    }
                    catch (FormatException fexp)
                    {
                        this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                        this.inEmployees.Rows[curRow]["Error"] = "Error: Existing  Hire Date Format Issue.";
                        this.stats.error_count++;
                    }


                    if (Emp_HireDate.Trim().Length < 1)
                    {
                        //Blank Hire Date. Default it.
                        Emp_HireDate = "01/01/1900";
                        new_hiredt = DateTime.MinValue;
                    }
                    else
                    {

                        try
                        {
                            new_hiredt = Convert.ToDateTime(Emp_HireDate);

                        }
                        catch (FormatException fexp)
                        {
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = "Error:  Hire Date Format Issue.";
                            this.stats.error_count++;
                        }
                    } // End File Hire Date Format Check

                    if (new_hiredt.CompareTo(DateTime.MinValue) != 0 && old_hiredt.CompareTo(new_hiredt) != 0)
                    {
                        if ((this.opts.getRuleValue("Rule_ForceNewHireOnAdd").ToLower() == "yes" || this.opts.getRuleValue("Rule_NewHireUpdateDays").ToLower().Length > 0  )
                             && ( Old_NewHireIndicator.ToLower().Equals("true") || Old_NewHireIndicator.Equals("1") )
                           )
                        {
                            // New Hires are forced on inserts and indicators and dates cannot be undone.
                            // Or newHIre inidicators are based on number of days from hire date.. which should not be updated once set.
                            // If HireDates can be provided and should be updated
                            //  then use this rule instead: Rule_ForceNewHireOnAddUseHireDate

                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + " Skip Hire Date update;";

                        }else{
                            //UPdate normally
                            /* BC Turn off - 3/13/2015 
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateHireDate"] = "1";
                            this.stats.addColumnCount("HireDate");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "Hire Date !=" + Old_HireDate + ";";
                            hasChanged = true;
                             */
                        }

                    }
                    // If new hire date is within x days and the current(old) new hire indicator is
                    // false. ( Set to true if within x days. Do not unset it )
                    if( this.opts.getRuleValue("Rule_NewHireUpdateDays").ToLower().Length > 0 
                        ){
                        string days = this.opts.getRuleValue("Rule_NewHireUpdateDays").ToLower();
                        
                        try
                        {
                            int ndays = Convert.ToInt16(days);
                            if (new_hiredt.AddDays(ndays).CompareTo(DateTime.Today) >= 0)
                            {
                                // Within X number of days. The indicator should be set.

                                System.Console.WriteLine("NewHireUpdateDays " + Old_UserName + " HireDate: " + new_hiredt + " + " + ndays + " is within current. Old value = " + Old_NewHireIndicator);

                                // Only update to true if it is false.
                                if (Old_NewHireIndicator.ToLower().CompareTo("false") == 0 || Old_NewHireIndicator.ToLower().CompareTo("0") == 0)
                                {
                                    this.inEmployees.Rows[curRow]["Update"] = "1";
                                    this.inEmployees.Rows[curRow]["UpdateNewHireIndicator"] = "1";
                                    this.inEmployees.Rows[curRow]["NewHireIndicator"] = "true"; // BC Added 12.13.13 for bug fix
                                    this.stats.addColumnCount("NewHireIndicator");
                                    this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "NewHireIndicator(Within NH Days) !=" + Old_NewHireIndicator + ";";
                                    hasChanged = true;
                                }

                            }
 


                        }
                        catch (Exception e)
                        {
                            System.Console.WriteLine("NewHireUpdateDays failed to calc: " + days);
                        }
                    }

                    
               
                    


                    // -------------- END HIRE DATE COMPARE LOGIC ----------------






                    // Hash the password to see if it matches
                    string new_pwd = "";
                    if (Emp_Password.Length > 0 && Old_PasswordSalt.Length > 0)
                    {
                        new_pwd = Utility.EncodePassword(Emp_Password, Old_PasswordSalt);
                    }
                    if (!new_pwd.Equals(Old_Password))
                    {
                        if (doPwdUpdates)
                        {
                            //Console.WriteLine("NEWPWD:" + new_pwd + ", OLDPWD: " + Old_Password + " , SALT: " + Old_PasswordSalt);
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdatePassword"] = "1";
                            this.stats.addColumnCount("Password");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "Existing Password !=" + new_pwd + ";";
                            hasChanged = true;
                        }
                    }
                    else if (new_pwd.Length < 1 && doPwdUpdates)
                    {
                        this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                        this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "Password check failed";
                        this.stats.error_count++;
                    }

                    // Check to see if the Password is valid ( >= 6 characters with one number )
                    // Only if it is a new record, or if we want to update the password
                    if (doPwdUpdates)
                    {

                        //Console.WriteLine(" Password Check: " + Emp_Password + ".");
                        if (Emp_Password.Length < 7 || !Regex.IsMatch(Emp_Password, @"\d"))
                        {
                            // Is not 7 characters or more, and does not have a number in it.
                            Console.WriteLine("Invalid Password.");
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "Password needs to be 7 or more characters with at least 1 number.";
                            this.stats.error_count++;

                        }
                    }


                    // Uppercase vs Lowercase ? For now it doesn't matter. 
                    bool doEmailUpdate = true;
                    if (this.opts.getRuleValue("Rule_19_IgnoreBlankEmail").ToLower().Equals("yes"))
                    {
                        if (Emp_Email.Length < 1)
                        {
                            doEmailUpdate = false;
                        }
                    }
                    if (doEmailUpdate && !Old_Email.Trim().Equals(Emp_Email.Trim()))
                    {
                      //Email changed
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateEmail"] = "1";
                            this.stats.addColumnCount("Email");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "Email !=" + Old_Email + ";";
                            hasChanged = true;
                    
                    }

                    // Check Location -----------------------------------------------

                    // Note: ANB has a LocationCode of 0. 
                    if (!Old_LocationCode.Trim().ToLower().Equals(Emp_LocationCode.Trim().ToLower()))
                    {

                   
                        if (this.lookupLocation(curRow, Emp_LocationCode.Trim(), Emp_LocationName.Trim()) < 1 )
                        {


                            
                            if (this.opts.getRuleValue("Rule_Insert_NewLocations").ToLower().Equals("yes"))
                            {
                                // These will be added automatically for this configuration
                                //Console.Write("Adding Location: " + Emp_LocationCode);
                                
                                // Location needs updating after the location is added

                                this.inEmployees.Rows[curRow]["Update"] = "1";
                                this.inEmployees.Rows[curRow]["UpdateLocationCode"] = "1";
                                this.stats.addColumnCount("LocationCode");
                                this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "LocationCode !=" + Old_LocationCode + ";";
                                hasChanged = true;

                            }
                            else
                            {
                                // New Location not found
                                this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "Location not found for LocationCode = " + Emp_LocationCode + ";";
                                this.stats.error_count++;
                            }
                        }
                        else
                        {
                            // Location needs updating

                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateLocationCode"] = "1";
                            this.stats.addColumnCount("LocationCode");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "LocationCode !=" + Old_LocationCode + ";";
                            hasChanged = true;
                        }




                    } // End Location check

                    // Check JobRole -----------------------------------------------

                    if (!Old_JobRoleCode.Trim().ToLower().Equals(Emp_JobRoleCode.Trim().ToLower()))
                    {
                        
                        if (this.lookupJobRole(curRow, Emp_JobRoleCode.Trim(), Emp_JobRoleName.Trim()) < 1 )
                        {
                            // New JobRole is not found or valid
                            if (this.opts.getRuleValue("Rule_Insert_NewJobRoles").ToLower().Equals("yes"))
                            {
                                // These will be added automatically for this configuration
                                Console.WriteLine("Adding JobRoleCode: " + Emp_JobRoleCode);

                                // JobRole needs to be updated after it is added
                                this.inEmployees.Rows[curRow]["Update"] = "1";
                                this.inEmployees.Rows[curRow]["UpdateJobRoleCode"] = "1";
                                this.stats.addColumnCount("JobRoleCode");
                                this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "JobRoleCode !=" + Old_JobRoleCode + ";";
                                hasChanged = true;

                            }
                            else
                            {
                                this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "JobRole not found for JobRoleCode = " + Emp_JobRoleCode + ";";
                                this.stats.error_count++;
                            }
                        }
                        else
                        {
                            // JobRole needs to be updated
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateJobRoleCode"] = "1";
                            this.stats.addColumnCount("JobRoleCode");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "JobRoleCode !=" + Old_JobRoleCode + ";";
                            hasChanged = true;
                        }


                    }

                    // Check Audience  -----------------------------------------------
                    //Console.WriteLine(" User Audience Change Check" + Emp_AudienceCode + " ? " + Old_AudienceCode);
                    if (Emp_AudienceCode.Trim().Length > 0 && 
                         !Old_AudienceCode.Trim().ToLower().Equals(Emp_AudienceCode.Trim().ToLower())
                         && !this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes")
                         && !this.opts.getRuleValue("Rule_AudienceCustom1").ToLower().Equals("yes") )
                    {
                        //Console.WriteLine(" User Audience Change Detected : " + Emp_AudienceCode + " != " + Old_AudienceCode );
                        if (this.lookupAudience(curRow, Emp_AudienceCode.Trim(), Emp_AudienceCode.Trim()) < 1)
                        {

                            if (this.opts.getRuleValue("Rule_Insert_NewAudiences").ToLower().Equals("yes"))
                            {
                                // These will be added automatically for this configuration
                 

                                // User Audience needs updating after the Audience is added

                                this.inEmployees.Rows[curRow]["Update"] = "1";
                                this.inEmployees.Rows[curRow]["UpdateAudienceCode"] = "1";
                                this.stats.addColumnCount("Audience");
                                this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "Audience !=" + Emp_AudienceCode + ";";
                                hasChanged = true;

                            }
                            else
                            {
                                // New Audience not found
                                this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "Audience not found for AudienceCode = " + Emp_AudienceCode + ";";
                                this.stats.error_count++;
                            }
                        }
                        else
                        {
                            // Audience needs updating

                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateAudienceCode"] = "1";
                            this.stats.addColumnCount("Audience");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "Audience !=" + Emp_AudienceCode + ";";
                            hasChanged = true;
                        }


                    } // End Audience check
                    

                        // Update/Add Custom Audience options
                    if (this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes"))
                    {

                        string Emp_AudienceCostCenter = this.inEmployees.Rows[curRow]["AudienceCostCenter"].ToString().Trim();
                        string Emp_AudienceExempt = this.inEmployees.Rows[curRow]["AudienceExempt"].ToString().Trim();
                        string Emp_AudienceEmpLevel = this.inEmployees.Rows[curRow]["AudienceEmpLevel"].ToString().Trim();
                        string Emp_AudienceManager = this.inEmployees.Rows[curRow]["AudienceManager"].ToString().Trim();



                        string Old_AudienceCostCenter = this.inEmployees.Rows[curRow]["OldAudienceCostCenter"].ToString().Trim();
                         string Old_AudienceExempt  = this.inEmployees.Rows[curRow]["OldAudienceExempt"].ToString().Trim();
                         string Old_AudienceEmpLevel = this.inEmployees.Rows[curRow]["OldAudienceEmpLevel"].ToString().Trim();
                         string Old_AudienceManager = this.inEmployees.Rows[curRow]["OldAudienceManager"].ToString().Trim();



                         // Get two in list of possible parent ids. The intersection of the two lists is the parent it
                         // needs to be associated to. Type = HCF, MCF, CBD, MBF; 

                         string atype = Emp_AudienceExempt.Substring(0, 3);
                         string typeparentids = this.opts.getRuleValue("Rule_Audience" + atype + "_ParentID").ToLower();
                         //Console.WriteLine("Types: " + atype + ":" + typeparentids);


                         string parentids = this.opts.getRuleValue("Rule_AudienceCostCenter_ParentID").ToLower();
                        if ((Emp_AudienceCostCenter.Length > 0) && (lookupAudience(curRow, Emp_AudienceCostCenter, Emp_AudienceCostCenter, parentids, typeparentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            //this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            //this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "; Employee with an unknown Cost Center Audience: " + Emp_AudienceCostCenter;
                            //this.inEmployees.Rows[curRow]["UpdateAudienceCostCenter"] = "0";
                            //this.stats.error_count++;

       
                        }else if( !Old_AudienceCostCenter.Trim().ToLower().Equals(Emp_AudienceCostCenter.Trim().ToLower() ) ){
                            // Update
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateAudienceCostCenter"] = "1";
                            this.stats.addColumnCount("AudienceCostCenter");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "AudienceCostCenter !=" + Emp_AudienceCostCenter + ";";
                            hasChanged = true;
                        }



                         parentids = this.opts.getRuleValue("Rule_AudienceExempt_ParentID").ToLower();
                        if ((Emp_AudienceExempt.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceExempt, Emp_AudienceExempt, parentids, typeparentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            //this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            //this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "; Employee with an unknown Exempt Status Audience: " + Emp_AudienceExempt;
                            //this.inEmployees.Rows[curRow]["UpdateAudienceExempt"] = "0";
                            //this.stats.error_count++;


                        }
                        else if( !Old_AudienceExempt.Trim().ToLower().Equals(Emp_AudienceExempt.Trim().ToLower() ) )
                        {
                            // Update
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateAudienceExempt"] = "1";
                            this.stats.addColumnCount("AudienceExempt");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "AudienceExempt !=" + Emp_AudienceExempt + ";";
                            hasChanged = true;
                        }

                        parentids = this.opts.getRuleValue("Rule_AudienceEmpLevel_ParentID").ToLower();
                        if ((Emp_AudienceEmpLevel.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceEmpLevel, Emp_AudienceEmpLevel, parentids, typeparentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            //this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            //this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "; Employee with an unknown Emp Level Audience: " + Emp_AudienceEmpLevel;
                            //this.inEmployees.Rows[curRow]["UpdateAudienceEmpLevel"] = "0";
                            //this.stats.error_count++;


                        }
                        else if( !Old_AudienceEmpLevel.Trim().ToLower().Equals(Emp_AudienceEmpLevel.Trim().ToLower() ))
                        {
                            // Update
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateAudienceEmpLevel"] = "1";
                            this.stats.addColumnCount("AudienceEmpLevel");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "AudienceEmpLevel !=" + Emp_AudienceEmpLevel + ";";
                            hasChanged = true;
                        }

                        parentids = this.opts.getRuleValue("Rule_AudienceManager_ParentID").ToLower();
                        if ((Emp_AudienceManager.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceManager, Emp_AudienceManager, parentids, typeparentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            //this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            //this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "; Employee with an unknown Manager Status Audience: " + Emp_AudienceManager;
                            //this.inEmployees.Rows[curRow]["UpdateAudienceManager"] = "0";
                            //this.stats.error_count++;


                        }
                        else if( !Old_AudienceManager.Trim().ToLower().Equals(Emp_AudienceManager.Trim().ToLower() ) )
                        {
                            // Update
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateAudienceManager"] = "1";
                            this.stats.addColumnCount("AudienceManager");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "AudienceManager !=" + Emp_AudienceManager + ";";
                            hasChanged = true;
                        }
                   

                    }// End Customer MidCountry Audience Processing (x4)

                    // Check Custom Audience Procesing
                    if (this.opts.getRuleValue("Rule_AudienceCustom1").ToLower().Equals("yes"))
                    {

                        string Emp_AudienceCustom1 = this.inEmployees.Rows[curRow]["AudienceCustom1"].ToString().Trim();
                        string Old_AudienceCustom1 = this.inEmployees.Rows[curRow]["OldAudienceCustom1"].ToString().Trim();


                        string parentids = "0";// this.opts.getRuleValue("Rule_AudienceCustom1_ParentIDs").ToLower();

                        if ((Emp_AudienceCustom1.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceCustom1, Emp_AudienceCustom1, parentids, parentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";Existing Employee with an unknown  Audience C1: " + Emp_AudienceCustom1;
                            this.inEmployees.Rows[curRow]["UpdateAudienceCustom1"] = "0";
                            this.stats.error_count++;


                        }else if(!Emp_AudienceCustom1.Trim().ToLower().Equals(Old_AudienceCustom1.Trim().ToLower()) )
                        {
                            // Update
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateAudienceCustom1"] = "1";
                            this.stats.addColumnCount("AudienceCustom1");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "AudienceCustom1 !=" + Emp_AudienceCustom1 + ";";
                            hasChanged = true;
                        }


                    } // EndCustomer AUdience 1
                    
                    if (this.opts.getRuleValue("Rule_AudienceCustom2").ToLower().Equals("yes"))
                    {

                        string Emp_AudienceCustom2 = this.inEmployees.Rows[curRow]["AudienceCustom2"].ToString().Trim();
                        string Old_AudienceCustom2 = this.inEmployees.Rows[curRow]["OldAudienceCustom2"].ToString().Trim();


                        string parentids = "0";//this.opts.getRuleValue("Rule_AudienceCustom2_ParentIDs").ToLower();

                        if ((Emp_AudienceCustom2.Length > 0) && (this.lookupAudience(curRow, Emp_AudienceCustom2, Emp_AudienceCustom2, parentids, parentids) < 1))
                        {

                            // Error. Audience is not found for New User, and this configuration does not automatically add these in.
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + ";Existing Employee with an unknown  Audience C2: " + Emp_AudienceCustom2;
                            this.inEmployees.Rows[curRow]["UpdateAudienceCustom2"] = "0";
                            this.stats.error_count++;


                        }
                        else if (!Emp_AudienceCustom2.Trim().ToLower().Equals(Old_AudienceCustom2.Trim().ToLower()))
                        {
                            // Update
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateAudienceCustom2"] = "1";
                            this.stats.addColumnCount("AudienceCustom2");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "AudienceCustom2 !=" + Emp_AudienceCustom2 + ";";
                            hasChanged = true;
                        }


                    } // Customer AUdience 1

                    // Check Supervisor  -----------------------------------------------
                    if (!Emp_SupervisorCode.Equals("0") && Emp_SupervisorCode.Trim().Length > 0 && !Old_SupervisorCode.Trim().Equals(Emp_SupervisorCode.Trim()))
                    {

                        // Find Supevisor Code in lookup
                        DataRow[] foundRows3;
                        string filter = "EmployeeNumber = '" + Emp_SupervisorCode.Trim().Replace("'", "''") + "' ";
                        //foundRows3 = this.inEmployees.Select(filter, "Username", DataViewRowState.CurrentRows);
                        foundRows3 = this.inEmployees.Select(filter);


                        bool sup_found = false;
                        if (foundRows3.Length < 1)
                        {

                            // Find in the existing list of users.
                            foundRows3 = this.oldEmployees.Select(filter, "Username", DataViewRowState.CurrentRows);
                            //foundRows3 = this.oldEmployees.Select(filter);
                            if (foundRows3.Length < 1)
                            {

                                // Invalid or Not found Supervisor
                                this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "Supervisor not found for SupervisorCode = " + Emp_SupervisorCode + ";[" + filter +"]";
                                this.stats.error_count++;
                            }
                            else
                            {
                                sup_found = true; //  Supervisor COde  found in the existing users.
                            }

                        }
                        else
                        {
                            sup_found = true; // Report Supervisor Code  found in the inbound users.
                            //Console.WriteLine("rptSubFind:(found) " + Emp_ReportingSupervisorCode + "\n");
                        }


                        if (sup_found)
                        {

                            // Supervisor needs to be updated
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateSupervisorCode"] = "1";
                            this.stats.addColumnCount("SupervisorCode");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "SupervisorCode !=" + Old_SupervisorCode + ";";
                            hasChanged = true;

                        }
                    }


                    // Check Report Supervisor  -----------------------------------------------
                    //Console.WriteLine("rptSubFind: " + Emp_ReportingSupervisorCode + "\n");
                    if (!Emp_ReportingSupervisorCode.Equals("0") && Emp_ReportingSupervisorCode.Trim().Length > 0 && !Old_ReportingSupervisorCode.Trim().Equals(Emp_ReportingSupervisorCode.Trim()))
                    {

                        // Find Report Supevisor Code in lookup
                        DataRow[] foundRows3;
                        string filter = "EmployeeNumber = '" + Emp_ReportingSupervisorCode.Trim().Replace("'", "''") + "'";
                        foundRows3 = this.inEmployees.Select(filter + "  and ErrorFg <> '1' ", "Username", DataViewRowState.CurrentRows);

                        //Console.WriteLine("rptSubFind: " + Emp_ReportingSupervisorCode + "\n");
                        bool rptsup_found = false;
                        if (foundRows3.Length < 1)
                        {

                            //Console.WriteLine("rptSubFind:(not found) " + Emp_ReportingSupervisorCode + "\n");
                            // Find in the existing list of users.
                            foundRows3 = this.oldEmployees.Select(filter, "Username", DataViewRowState.CurrentRows);
                            if (foundRows3.Length < 1)
                            {
                                //Console.WriteLine("rptSubFind:(old not found) " + Emp_ReportingSupervisorCode + "\n");
                                // Invalid or Not found Supervisor
                                this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"] + "Report Supervisor not found for ReportingSupervisorCode = " + Emp_ReportingSupervisorCode + ";" + filter;
                                this.stats.error_count++;
                            }
                            else
                            {
                                rptsup_found = true; // Report Supervisor COde  found in the existing users.
                                //Console.WriteLine("rptSubFind:(old  found) " + Emp_ReportingSupervisorCode + "\n");
                            }

                        }
                        else
                        {
                            rptsup_found = true; // Report Supervisor Code  found in the inbound users.
                            //Console.WriteLine("rptSubFind:(found) " + Emp_ReportingSupervisorCode + "\n");
                        }


                        if (rptsup_found)
                        {

                            // Supervisor needs to be updated
                            this.inEmployees.Rows[curRow]["Update"] = "1";
                            this.inEmployees.Rows[curRow]["UpdateReportingSupervisorCode"] = "1";
                            this.stats.addColumnCount("ReportingSupervisorCode");
                            this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + "ReportingSupervisorCode !=" + Old_ReportingSupervisorCode + ";";
                            hasChanged = true;

                        }
                    }





                    if (hasChanged)
                    {
                        this.stats.update_count++;
                    }

                } // End Insert/Delete/Update 'if' statement




            } // End loop

            this.stats.delete_count = this.stats.existing_count - found_count;

            Utility.SaveToCSV(this.inLocations, this.opts.getBaseDirectory() + "User_Locations.csv", 0, 0);
            Utility.SaveToCSV(this.inJobRoles, this.opts.getBaseDirectory() + "User_JobRoles.csv", 0, 0);
            Utility.SaveToCSV(this.inAudiences, this.opts.getBaseDirectory() + "User_Audiences.csv", 0, 0);

            return 0; //ok
        }

        // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int executeChanges()
        {



            string deleteSQL = "";
            Console.WriteLine("Executing Changes..");

            // Process adds ( loop 1 )
            executeAdds();

            // Execute the First Round of Updates ( loop 2 )
            executePrimaryUpdates();

            // Execute the Second Round of Updates ( loop 3)
            executeSecondaryUpdates();

            // Execute salesforce training program assignments (loop 3.1)
            //
            // AM: Removing the call to execute salesforce assignments
            // Initially this was developed here, but then realized it would be
            // more appropriate to place on orders. The code is still here 
            // if we need to continue this route. 
            //
            //executeSalesforceTrainingProgramAssignments();

            // Generate Delete Statements for Removals ( loop 4 )
            for (int curRow = 0; curRow < this.oldEmployees.Rows.Count; curRow++)
            {
                //Delete field will be blank '' . No means it was found.
                if (this.oldEmployees.Rows[curRow]["Delete"].ToString().Equals("No"))
                {
                    // Do not delete
                    continue;
                }
                string OldEmp_ID = this.oldEmployees.Rows[curRow]["ID"].ToString();
                string OldEmp_Username = this.oldEmployees.Rows[curRow]["Username"].ToString();
                string OldEmp_EmployeeNumber = this.oldEmployees.Rows[curRow]["EmployeeNumber"].ToString();


                string OldEmp_ModifiedBy = this.oldEmployees.Rows[curRow]["ModifiedBy"].ToString();
                string OldEmp_CreatedBy = this.oldEmployees.Rows[curRow]["CreatedBy"].ToString();
                string OldEmp_ModifiedOn = this.oldEmployees.Rows[curRow]["ModifiedOn"].ToString();
                string OldEmp_CreatedOn = this.oldEmployees.Rows[curRow]["CreatedOn"].ToString();

                deleteSQL += " update [user] set StatusID = 2 where CustomerID = " + this.customerID + " and Username = '" + OldEmp_Username.Replace("'", "''") + "'\n" +
                        " and ID = " + OldEmp_ID + " and EmployeeNumber = '" + OldEmp_EmployeeNumber + "'\n";



            }
            Utility.SaveToFile(this.baseDirName + "u3_Emp_DeleteSQL.txt", deleteSQL);


            // NEW FILE
            string fileFilter = " New = '1' ";
            string[] colNames = new string[15] { "Username", "EmployeeNumber", "FirstName", "MiddleName", "LastName", "HireDate", "NewHireIndicator", "Email", "JobRoleCode", "LocationCode", "Notes", "SupervisorCode", "ReportingSupervisorCode", "Password", "Status" };
            // Utility.SaveFilteredToCSV(this.inEmployees, this.baseDirName + "z1_Ref_EmployeeNew.csv", fileFilter, colNames);
            Utility.SplitFilteredToCSV(this.baseDirName, "z1_Ref_EmployeeNew.csv", this.inEmployees, 400, colNames, fileFilter);

            // UPDATE FILE
            fileFilter = " Update = '1' ";
            string[] colNames2 = new string[17] { "UpdateID", "Username", "EmployeeNumber", "FirstName", "MiddleName", "LastName", "HireDate", "NewHireIndicator", "Email", "JobRoleCode", "LocationCode", "Notes", "SupervisorCode", "ReportingSupervisorCode", "Password", "Status", "UpdateMsg" };
            Utility.SaveFilteredToCSV(this.inEmployees, this.baseDirName + "z2_Ref_EmployeeUpdate.csv", fileFilter, colNames2);


            // UNDO DELETE FILE: Generate an import file to reload the deleted Employees if needed.
            fileFilter = " Delete = 'Yes' ";
            string[] colNames3 = new string[20] { "ID", "Username", "EmployeeNumber", "FirstName", "MiddleName", "LastName", "HireDate", "NewHireIndicator", "Email", "JobRoleCode", "LocationCode", "Notes", "SupervisorCode", "ReportingSupervisorCode", "Password", "Status", "ModifiedBy", "ModifiedOn", "CreatedBy", "CreatedOn" };
            Utility.SaveFilteredToCSV(this.oldEmployees, this.baseDirName + "z3_Undo_EmployeeDelete.csv", fileFilter, colNames3);

            //Rule_5_CustomPasswordFileExport
            if (this.opts.getRuleValue("Rule_5_CustomPasswordFileExport").ToLower().Equals("yes"))
            //if (this.customerID.Equals("80"))
            {
                // Create Split out employee imports to load/set password
                // UPDATE FILE
                fileFilter = " UpdatePassword = '1'  ";
                string[] colNames4 = new string[15] { "Username", "EmployeeNumber", "FirstName", "MiddleName", "LastName", "HireDate", "NewHireIndicator", "Email", "JobRoleCode", "LocationCode", "Notes", "SupervisorCode", "ReportingSupervisorCode", "Password", "Status" };
                Utility.SplitFilteredToCSV(this.baseDirName, "z4_Ref_EmployeePasswordUpdate.csv", this.inEmployees, 200, colNames4, fileFilter);


            }



            return 0; //ok

        }
        // --------------------------------------------------------------------
        //  Process the inserts first
        //
        // --------------------------------------------------------------------

        private int executeAdds()
        {


            // Insert JobRoles if configured to so do
            if (this.opts.getRuleValue("Rule_Insert_NewJobRoles").ToLower().Equals("yes"))
            {

                string insert_new_jobroles = "";
                int add_count = 0;
                // Generate/Execute SQL Scripts for Inserts
                for (int cr = 0; cr < this.inJobRoles.Rows.Count; cr++)
                {
                    string newFg = this.inJobRoles.Rows[cr]["New"].ToString();
                    if (newFg == "1")
                    {
                        add_count++;
                        insert_new_jobroles += "insert into JobRole (CustomerID, Name, InternalCode, ModifiedOn, ModifiedBy, CreatedOn, CreatedBy, ParentJobRoleID ) values (" +
                                this.opts.getCustomerID() + ",'" + this.inJobRoles.Rows[cr]["Name"].ToString().Replace("'", "''") + "', '" + this.inJobRoles.Rows[cr]["InternalCode"].ToString().Replace("'", "''") + "', " +
                                " getdate(), 'Import', getdate(), 'Import',0 )\n";

   
                    }
                }//ENd JobRole Loop

                // Update if requested.
                if (opts.isExecute() && add_count > 0)
                {


                    SqlCommand myCommand_jb = new SqlCommand(insert_new_jobroles, this.db);
                    int rowcount_jb = myCommand_jb.ExecuteNonQuery();
                    Console.WriteLine("Insert JobRole =  " + rowcount_jb + " " + insert_new_jobroles);
                    if (rowcount_jb < 0)
                    {

                        Console.WriteLine("Insert JobRole Failed: " + insert_new_jobroles + insert_new_jobroles);
                    }

                }



            }// End insert JobRoles


            // Insert Locations if configured to so do
            if (this.opts.getRuleValue("Rule_Insert_NewLocations").ToLower().Equals("yes"))
            {

                string insert_new_locations = "";
                int add_count2 = 0;
                // Generate/Execute SQL Scripts for Inserts
                for (int lcr = 0; lcr < this.inLocations.Rows.Count; lcr++)
                {
                    string newFg = this.inLocations.Rows[lcr]["New"].ToString();
                    if (newFg == "1")
                    {
                        add_count2++;
                        insert_new_locations += "insert into Location ( CustomerID, Name, InternalCode, ModifiedOn, ModifiedBy, CreatedOn, CreatedBy, Address1, Address2, City, State, PostalCode, TelephoneNbr, ParentLocationID ) values (" +
                               this.opts.getCustomerID() + ",'"  + this.inLocations.Rows[lcr]["Name"].ToString().Replace("'", "''") + "', '" + this.inLocations.Rows[lcr]["InternalCode"].ToString().Replace("'", "''") + "', " +
                                " getdate(), 'Import', getdate(), 'Import', '','','','','','',0 )\n";

          
                    }
                }

                // Update if requested.
                if (opts.isExecute() && add_count2 > 0)
                {
                    SqlCommand myCommand_l = new SqlCommand(insert_new_locations, this.db);
                    int rowcount_l = myCommand_l.ExecuteNonQuery();
                    Console.WriteLine("Insert Locations =  " + rowcount_l + " " + insert_new_locations);
                    if (rowcount_l < 0)
                    {

                        Console.WriteLine("Insert Locations Failed: " + insert_new_locations);
                    }

                }


            }// End insert Locations

            // Insert Audience if configured to so do
            if (this.opts.getRuleValue("Rule_Insert_NewAudiences").ToLower().Equals("yes") ||
                  this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes"))
            {

                string insert_new_audience = "";
                int add_count = 0;
                // Generate/Execute SQL Scripts for Inserts
                for (int cr = 0; cr < this.inAudiences.Rows.Count; cr++)
                {
                    string newFg = this.inAudiences.Rows[cr]["New"].ToString();
                    string parentID = this.inAudiences.Rows[cr]["ParentAudienceID"].ToString();
                    if (parentID.Length < 1)
                    {
                        parentID = "0";
                            
                    }
                    if (newFg == "1")
                    {
                        add_count++;
                        insert_new_audience += "insert into Audience (CustomerID, Name, InternalCode, ModifiedOn, ModifiedBy, CreatedOn, CreatedBy, ParentAudienceID , sortorder) values (" +
                                this.opts.getCustomerID() + ",'" + this.inAudiences.Rows[cr]["Name"].ToString().Replace("'", "''") + "', '" + this.inAudiences.Rows[cr]["InternalCode"].ToString().Replace("'", "''") + "', " +
                                " getdate(), 'Import', getdate(), 'Import'," + parentID + " ,0)\n";


                    }
                }//ENd Audience Loop
                Console.WriteLine("Insert Audience Preview: " + insert_new_audience);
                // Update if requested.
                if (opts.isExecute() && add_count > 0)
                {


                    SqlCommand myCommand_aud = new SqlCommand(insert_new_audience, this.db);
                    int rowcount_aud = myCommand_aud.ExecuteNonQuery();
                    Console.WriteLine("Insert Audience =  " + rowcount_aud + " " + insert_new_audience);
                    if (rowcount_aud < 0)
                    {

                        Console.WriteLine("Insert Audience Failed: " + insert_new_audience );
                    }

                }



            }// End insert Audiences




            string insertSQL = "";


            // Create a file here just in case until all other relationships, like roles, are understood:


            string[] colNames_new = new string[17]{"EmployeeNumber","Username","LastName","FirstName","MiddleName","HireDate",
                                "Email","Status","Password", "NewHireIndicator", "LocationCode","SupervisorCode","ReportingSupervisorCode","JobRoleCode","Notes","ErrorFg","Error"};
            //Utility.SaveFilteredToCSV(this.inEmployees, this.baseDirName + "a1_Add_Emp.csv", fileFilter_new, colNames_new);



            // Generate/Execute SQL Scripts for Inserts
            for (int curRow = 0; curRow < this.inEmployees.Rows.Count; curRow++)
            {

                if (curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Employee Add " + curRow + " of " + this.inEmployees.Rows.Count);
                }

                string Emp_New = this.inEmployees.Rows[curRow]["New"].ToString();
                string Emp_CurrentErrorFg = this.inEmployees.Rows[curRow]["ErrorFg"].ToString();
                string Emp_Update = this.inEmployees.Rows[curRow]["Update"].ToString();
                string Emp_ID = this.inEmployees.Rows[curRow]["UpdateID"].ToString();
                string Emp_ASPNETID = this.inEmployees.Rows[curRow]["UpdateAUID"].ToString();
                string Emp_Username = this.inEmployees.Rows[curRow]["Username"].ToString();
                string Emp_EmployeeNumber = this.inEmployees.Rows[curRow]["EmployeeNumber"].ToString();
                string Emp_FirstName = this.inEmployees.Rows[curRow]["FirstName"].ToString();
                string Emp_LastName = this.inEmployees.Rows[curRow]["LastName"].ToString();
                string Emp_MiddleName = this.inEmployees.Rows[curRow]["MiddleName"].ToString();
                string Emp_HireDate = this.inEmployees.Rows[curRow]["HireDate"].ToString();
                string Emp_LocationCode = this.inEmployees.Rows[curRow]["LocationCode"].ToString();
                string Emp_JobRoleCode = this.inEmployees.Rows[curRow]["JobRoleCode"].ToString();
                string Emp_SupervisorCode = this.inEmployees.Rows[curRow]["SupervisorCode"].ToString();
                string Emp_ReportingSupervisorCode = this.inEmployees.Rows[curRow]["ReportingSupervisorCode"].ToString();
                string Emp_Email = this.inEmployees.Rows[curRow]["Email"].ToString();
                string Emp_Status = this.inEmployees.Rows[curRow]["Status"].ToString();
                string Emp_Password = this.inEmployees.Rows[curRow]["Password"].ToString();
                string Emp_NewHireIndicator = this.inEmployees.Rows[curRow]["NewHireIndicator"].ToString().ToLower();
                string Emp_Notes = this.inEmployees.Rows[curRow]["Notes"].ToString();
                string Emp_NMLS = this.inEmployees.Rows[curRow]["NMLS"].ToString();
                string Emp_PasswordChange = "'01/01/2014'";

                string insert_s = "";

                if (Emp_New.Equals("1") && !Emp_CurrentErrorFg.Equals("1"))
                {

                    // Check to see if the Password is valid ( >= 6 characters with one number )
                    // Only if it is a new record, or if we want to update the password

                    //Console.WriteLine(" Password Check: " + Emp_Password + ".");

                    DateTime new_hiredt;
                    if (Emp_HireDate.Trim().Length < 1) 
                    {
                        new_hiredt = DateTime.MinValue;
                    }
                    else
                    {
                        try
                        {
                            new_hiredt = Convert.ToDateTime(Emp_HireDate);

                        }
                        catch (FormatException fexp)
                        {
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = "Error:  Hire Date Format Issue. Failed to Add User";
                            continue; // Skip
                        }
                    }// End New Hire Date Format Check


                  

                    if (Emp_Password.Length < 7 || !Regex.IsMatch(Emp_Password, @"\d"))
                    {
                        // Is not 7 characters or more, and does not have a number in it.
                        Console.WriteLine("Invalid Password: + " + Emp_Username);
                        this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                        this.inEmployees.Rows[curRow]["Error"] = inEmployees.Rows[curRow]["Error"] + "Password needs to be 7 or more characters with at least 1 number.";
                        this.stats.error_count++;
                        //Utility.SaveFilteredToCSV(inEmployees, this.baseDirName + "a1_Add_Emp_error.csv", fileFilter_new, colNames_new);
                        continue; // Skip

                    }


                    if (Emp_NewHireIndicator.Contains("false"))
                    {
                        Emp_NewHireIndicator = "0";
                    }
                    else if (Emp_NewHireIndicator.Contains("true"))
                    {
                        Emp_NewHireIndicator = "1";
                    }

                    // Check For Custom New Hire Rule
                    // Rule_ForceNewHireOnAddUseHireDate will not set this HireDate to getdate
                    if (this.opts.getRuleValue("Rule_ForceNewHireOnAdd").Length > 0 )
                    {
                        Emp_NewHireIndicator = "1";
                        Emp_HireDate = DateTime.Today.ToString("MM/dd/yyyy");
                        this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + ";Forced New Hire Date to : " + Emp_HireDate;
                    }
                    // Check For Custom New Hire Rule ( force on add .. but use hire date)
                    if (this.opts.getRuleValue("Rule_ForceNewHireOnAddUseHireDate").Length > 0)
                    {
                        Emp_NewHireIndicator = "1";
                        this.inEmployees.Rows[curRow]["UpdateMsg"] = this.inEmployees.Rows[curRow]["UpdateMsg"] + ";New Hire on add";
                    }

       



                    insert_s = "insert into [user] (username, CustomerID, SalesChannelID, LocationID \n" +
                            ",EmployeeNumber, FirstName, MiddleName, LastName \n" +
                            ",HireDate, NewHireIndicator, Email, StatusID, JobRoleID, Notes \n" +
                            ",LoginCounter, IsAccountExec, IsCustomerCare, ModifiedBy, CreatedBy \n" +
                            ",ModifiedOn, CreatedOn, SupervisorID, IsExternal, SecondaryLocationID, TelephoneNumber, [password] , ReportingSupervisorID, LastEnforcedPasswordReset, NMLSNumber )\n " +
                            " values ( '" + Emp_Username.Replace("'", "''") + "', " + opts.getCustomerID() + ",0,0 \n " +
                            " ,'" + Emp_EmployeeNumber.Replace("'", "''") + "', '" + Emp_FirstName.Replace("'", "''") + "','" + Emp_MiddleName.Replace("'", "''") + "','" + Emp_LastName.Replace("'", "''") + "' \n" +
                            " ,'" + Emp_HireDate + "'," + Emp_NewHireIndicator + ",'" + Emp_Email.Replace("'", "''") + "'," + Emp_Status + ",0,'" + Emp_Notes.Replace("'", "''")  + "' \n" +
                            ",0,0,0,'Import','Import',getdate(), getdate(),0,0,0,'','',0,'01/01/2014','" + Emp_NMLS.Replace("'", "''") + "' )\n\n";


                    if (opts.isExecute())
                    {
                        //  adding user
                        Emp_ASPNETID = this.AddASPNETUser(Emp_Username, Emp_Password, Emp_Email);

                        if (Emp_ASPNETID.Equals("ERROR"))
                        {
                            Console.WriteLine("Failed to add ASPNET user." + Emp_Username);
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", User Insert Failed on Membership:";

                            insert_s = "FAILED:" + insert_s;
                            this.stats.error_count++;
                            continue; //Fail all and Skip this guy



                        }
                        this.inEmployees.Rows[curRow]["UpdateAUID"] = Emp_ASPNETID;
                        Console.WriteLine("Added ASPNET user." + Emp_Username + " ID:" + Emp_ASPNETID);

                        // Add Main User
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(insert_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount < 1)
                            {
                                this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", Insert Failed with rc:" + rowcount;
                                insert_s = "FAILED:" + insert_s;
                                Console.WriteLine("Insert Failed: " + insert_s);
                                this.stats.error_count++;
                                continue;
                            }
                        }
                        catch (Exception)
                        {
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", Insert Failed.";
                            insert_s = "FAILED:" + insert_s;
                            this.stats.error_count++;
                            continue;
                        }

                        insertSQL += insert_s; // Save for final SQL Script in log

                        insert_s = "insert into aspnet_UsersInRoles (UserId, RoleID) values ('" + Emp_ASPNETID +
                              "','" + opts.getUserRoleID() + "')\n\n";

                        // Add Role
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(insert_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount < 1)
                            {
                                this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", Role Insert Failed with rc:" + rowcount;
                                insert_s = "FAILED:" + insert_s;
                                insertSQL += insert_s;
                                this.stats.error_count++;
                                continue;
                            }
                        }
                        catch (Exception)
                        {
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", Role Insert Failed.";
                            insert_s = "FAILED:" + insert_s;
                            insertSQL += insert_s;
                            this.stats.error_count++;
                            continue;
                        }
                        insertSQL += insert_s + "\n----------------------------------------------\n";

                    }
                    else
                    {// End if Execute
                        insertSQL += insert_s;
                    }

                    // ! The SupervisorID needs to be updated after the 1st update, and after the inserts.
                    // This will be done in the processSecondaryUpdate method



                }



            } // End big employee loop

            this.inEmployees.AcceptChanges();
            Utility.SaveToFile(this.baseDirName + "u2_Emp_InsertSQL.txt", insertSQL);

            return 0;

        }

        // --------------------------------------------------------------------
        //  Process the Salesforce training program assignments
        //  This is only done if salesforce mode is turned on and training program copy
        //  does not yet exist for the user. 
        //  Will copy the training programs from the users assigned audieces/locations/job roles 
        //  and duplicate them using the salesforce api IF they are not already assigned.
        // --------------------------------------------------------------------
        public int executeSalesforceTrainingProgramAssignments()
        {
            bool isPreview = (!opts.isExecute() || !opts.isSessions());

                List<int> userIds = new List<int>();
                for (int curRow = 0; curRow < this.inEmployees.Rows.Count; curRow++)
                {
                    string Emp_ID = this.inEmployees.Rows[curRow]["UpdateID"].ToString();
                    string Emp_LocationCode = this.inEmployees.Rows[curRow]["LocationCode"].ToString();
                    string Emp_JobRoleCode = this.inEmployees.Rows[curRow]["JobRoleCode"].ToString();
                    string Emp_AudienceCode = this.inEmployees.Rows[curRow]["AudienceCode"].ToString().Replace("'", "''");

                    string getTrainingProgramsSql = @"
                        select 
                            hl.TrainingProgramID as ID, tp.Sku
                        from HierarchyToTrainingProgramLink hl
                        join TrainingProgram tp on tp.ID = hl.TrainingProgramID
                        where 
                            (hl.HierarchyTypeID = 1 and hl.HierarchyNodeID = @locationID) or
                            (hl.HierarchyTypeID = 2 and hl.HierarchyNodeID = @jobRoleID) or
                            (hl.HierarchyTypeID = 3 and hl.HierarchyNodeID = @audienceID) or
                    ";

                    DataTable trainingProgramIds = new DataTable();
                    //List<SessionAssignedUser> usersToAssign = new List<SessionAssignedUser>();


                    try
                    {
                        if (!this.silent)
                        {
                            Console.WriteLine("Get training programs for hierarchies: " + getTrainingProgramsSql);
                        }
                        SqlCommand myCommand = new SqlCommand(getTrainingProgramsSql, this.db);
                        myCommand.Parameters.AddWithValue("locationID", Emp_LocationCode);
                        myCommand.Parameters.AddWithValue("jobRoleID", Emp_JobRoleCode);
                        myCommand.Parameters.AddWithValue("audienceID", Emp_AudienceCode);

                        SqlDataReader myReader = myCommand.ExecuteReader();

                        if (!this.silent)
                        {
                            Console.WriteLine("Training programs loaded.");
                        }
                        // Load into DataTable
                        trainingProgramIds.Load(myReader);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        return -1; // Fail
                    }

                    EntitlementList entitlements = new EntitlementList();

                    int userId = 0;
                    // Should always parse
                    if (int.TryParse(Emp_ID, out userId))
                    {
                        for (int tpRow = 0; tpRow < trainingProgramIds.Rows.Count; tpRow++)
                        {
                            int trainingProgramID = 0;

                            string sku = trainingProgramIds.Rows[tpRow]["Sku"].ToString();

                            entitlements.Entitlements.Add(new Entitlement()
                            {
                                EntitlementDuration = 1,
                                EntitlementDurationUnits = "year",
                                EntitlementStartDate = DateTime.UtcNow.ToString(),
                                EntitlementPurchaseDate = DateTime.UtcNow.ToString(),
                                EntitlementEndDate = DateTime.UtcNow.AddYears(1).ToString(),
                                EntitlementId = 0,
                                SalesforceEntitlementId = "",
                                IsEnabled = true,
                                IsSuppressEmail = false,
                                PartnerCode = "",
                                SalesforceProductId = "",
                                ProductId = sku
                            });

                        }

                        userIds.Add(userId);
                    }
                    if (!isPreview) {
                        (new EntitlementController()).PostEntitlements(userId, entitlements);
                    }
                }

                
                (new ClassroomController()).SaveSessionRegistrations(userIds, isPreview);

                jobQueue.setSessionUserIds(opts.getJobID(), userIds);
            

            return 0;
        }

        // --------------------------------------------------------------------
        //  Process the main field updates on the Employee
        //  * Secondary fields are the FKs that need to be joined after the adds
        // and updates. These will be updated after.
        // --------------------------------------------------------------------

        public void executePrimaryUpdates()
        {
            string update1SQL = "";
            string undoUpdate1SQL = "";

            // Generate Update Statements for Changes

            for (int curRow = 0; curRow < this.inEmployees.Rows.Count; curRow++)
            {

                if (curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Employee Main Update " + curRow + " of " + this.inEmployees.Rows.Count);
                }

                string Emp_New = this.inEmployees.Rows[curRow]["New"].ToString();
                string Emp_Update = this.inEmployees.Rows[curRow]["Update"].ToString();
                string Emp_ID = this.inEmployees.Rows[curRow]["UpdateID"].ToString();
                string Emp_ASPNETID = this.inEmployees.Rows[curRow]["UpdateAUID"].ToString();
                string Emp_Username = this.inEmployees.Rows[curRow]["Username"].ToString();
                string Emp_EmployeeNumber = this.inEmployees.Rows[curRow]["EmployeeNumber"].ToString();
                string Emp_FirstName = this.inEmployees.Rows[curRow]["FirstName"].ToString();
                string Emp_LastName = this.inEmployees.Rows[curRow]["LastName"].ToString();
                string Emp_MiddleName = this.inEmployees.Rows[curRow]["MiddleName"].ToString();
                string Emp_HireDate = this.inEmployees.Rows[curRow]["HireDate"].ToString();
                string Emp_NewHireIndicator = this.inEmployees.Rows[curRow]["NewHireIndicator"].ToString();
                string Emp_LocationCode = this.inEmployees.Rows[curRow]["LocationCode"].ToString();
                string Emp_JobRoleCode = this.inEmployees.Rows[curRow]["JobRoleCode"].ToString();
                string Emp_SupervisorCode = this.inEmployees.Rows[curRow]["SupervisorCode"].ToString();
                string Emp_Email = this.inEmployees.Rows[curRow]["Email"].ToString();
                string Emp_Status = this.inEmployees.Rows[curRow]["Status"].ToString();
                string Emp_Password = this.inEmployees.Rows[curRow]["Password"].ToString();
                string Emp_Notes = this.inEmployees.Rows[curRow]["Notes"].ToString();
                string Emp_NMLS = this.inEmployees.Rows[curRow]["NMLS"].ToString();

                // Only update what has changed:
                bool updateFirstName = false;
                if (this.inEmployees.Rows[curRow]["UpdateFirstName"].ToString().Equals("1"))
                {
                    updateFirstName = true;
                }
                bool updateEmployeeNumber = false;
                if (this.inEmployees.Rows[curRow]["UpdateEmployeeNumber"].ToString().Equals("1"))
                {
                    updateEmployeeNumber = true;
                }
                bool updateLastName = false;
                if (this.inEmployees.Rows[curRow]["UpdateLastName"].ToString().Equals("1"))
                {
                    updateLastName = true;
                }
                bool updateMiddleName = false;
                if (this.inEmployees.Rows[curRow]["UpdateMiddleName"].ToString().Equals("1"))
                {
                    updateMiddleName = true;
                }
                bool updateHireDate = false;
                if (this.inEmployees.Rows[curRow]["UpdateHireDate"].ToString().Equals("1"))
                {
                    updateHireDate = true;
                }
                bool updateNewHireIndicator = false;
                if (this.inEmployees.Rows[curRow]["UpdateNewHireIndicator"].ToString().Equals("1"))
                {
                    updateNewHireIndicator = true;
                }
                bool updateStatus = false;
                if (this.inEmployees.Rows[curRow]["UpdateStatus"].ToString().Equals("1"))
                {
                    updateStatus = true;
                }
                bool updateEmail = false;
                if (this.inEmployees.Rows[curRow]["UpdateEmail"].ToString().Equals("1"))
                {
                    updateEmail = true;
                }
                bool updatePassword = false;
                if (this.inEmployees.Rows[curRow]["UpdatePassword"].ToString().Equals("1"))
                {
                    updatePassword = true;
                }
                bool updateUsername = false;
                if (this.inEmployees.Rows[curRow]["UpdateUserName"].ToString().Equals("1"))
                {
                    updateUsername = true;
                }
                bool updateNotes = false;
                if (this.inEmployees.Rows[curRow]["UpdateNotes"].ToString().Equals("1"))
                {
                    updateNotes = true;
                }
                bool updateNMLS = false;
                if (this.inEmployees.Rows[curRow]["updateNMLSNumber"].ToString().Equals("1"))
                {
                    updateNMLS = true;
                }


                // Generate the Update SQL ( Step = 1: Update Primary Fields 
                if (Emp_Update.Equals("1"))
                {

                    if (updateEmployeeNumber || updateHireDate || updateLastName || updateFirstName ||
                        updateMiddleName || updateEmail || updateStatus || updateUsername || updatePassword || updateNewHireIndicator || updateNotes || updateNMLS )
                    {

                        string update_s = "Update [user] set ModifiedBy = '" + this.programName + "'\n";
                        string update_s2 = "";//This will be the username update on aspnet_users



                    

                        if (updateHireDate)
                        {
                            update_s += " ,HireDate = '" + Emp_HireDate + "'\n";
                        }
                        if (updateNewHireIndicator)
                        {
                            if (Emp_NewHireIndicator.ToLower().Equals("true"))
                            {
                                update_s += " ,NewHireIndicator = 1 \n";
                            }
                            else
                            {
                                update_s += " ,NewHireIndicator = 0, NewHireEndDate =  CONVERT (datetime, convert(varchar(10),GETDATE(),101) ) \n";
                            }
                        }
                        if (updateLastName)
                        {
                            update_s += ", LastName = '" + Emp_LastName.Replace("'", "''") + "'\n";
                        }
                        if (updateFirstName)
                        {
                            update_s += ", FirstName = '" + Emp_FirstName.Replace("'", "''") + "'\n";
                        }
                        if (updateMiddleName)
                        {
                            update_s += ", MiddleName = '" + Emp_MiddleName.Replace("'", "''") + "'\n";
                        }
                        if (updateEmail)
                        {
                            update_s += ", Email = '" + Emp_Email.Replace("'", "''") + "'\n";
                        }
                        if (updateStatus)
                        {
                            update_s += ", StatusID = '" + Emp_Status + "'\n";
                        }
                        if (updateUsername)
                        {
                            update_s += ", Username = '" + Emp_Username.Replace("'", "''") + "'\n";
                        }
                        if (updateEmployeeNumber)
                        {
                            update_s += ", EmployeeNumber = '" + Emp_EmployeeNumber.Replace("'", "''").Trim() + "'\n";
                        }
                        if (updateNotes)
                        {
                            update_s += ", Notes = Notes + ' " + Emp_Notes.Replace("'", "''").Trim() + "'\n";
                        }
                        if (updateNotes)
                        {
                            update_s += ", NMLSNumber = ' " + Emp_NMLS.Replace("'", "''").Trim() + "'\n";
                        }

                        update_s +=
                                " , ModifiedOn = getdate() \n" +
                                " where CustomerID = " + this.customerID + " \n" +
                                " and ID = " + Emp_ID + "\n\n";

                        if (updateUsername)
                        {
                            update_s2 = "\n" +
                                " update aspnet_users set username = '" + Emp_Username.Replace("'", "''") + "'\n " +
                                " , loweredusername = lower('" + Emp_Username.Replace("'", "''") + "') \n " +
                                " where UserID = '" + Emp_ASPNETID + "' \n\n";
                        }


                        if (opts.isExecute())
                        {
                            // Chnage password?
                            if (updatePassword)
                            {
                                Console.WriteLine("Update Password.");
                                if (changeASPNETUserPassword(Emp_Username, Emp_Password) < 1)
                                {
                                    this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                    this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + "Chanage password failed;";
                                }
                            }


                            // Update Main User
                            try
                            {
                                SqlCommand myCommand = new SqlCommand(update_s, this.db);
                                int rowcount = myCommand.ExecuteNonQuery();
                                if (rowcount < 1)
                                {
                                    this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                    this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", Update Main Failed with rc:" + rowcount;
                                    update_s = "FAILED:" + update_s;
                                }
                            }
                            catch (Exception)
                            {
                                this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", Update Main Failed.";
                                update_s = "FAILED:" + update_s;

                            }

                            // Update Asp.net User if needed
                            if (updateUsername)
                            {
                                try
                                {
                                    SqlCommand myCommand = new SqlCommand(update_s2, this.db);
                                    int rowcount = myCommand.ExecuteNonQuery();
                                    if (rowcount < 1)
                                    {
                                        this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                        this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", ASPNET Update Failed with rc:" + rowcount;
                                        update_s2 = "FAILED:" + update_s2;
                                    }
                                }
                                catch (Exception)
                                {
                                    this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                    this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", ASPNET Update Failed.";
                                    update_s2 = "FAILED:" + update_s2;
                                }
                            } // emd update user name

                        }
                        update1SQL += update_s + update_s2;

                    }

                }// End update step 1

            } // End big employee loop

            Utility.SaveToFile(this.baseDirName + "u1_Emp_Update1SQL.txt", update1SQL);
            this.inEmployees.AcceptChanges();


        }

        // --------------------------------------------------------------------
        //  Secondary Updates are the Updates that need to happen after the
        //  new records are inserted, and the primary fields (like EmployeeNumber) 
        // have been updated.
        // SupervisorID, ReportingSupervisorID, LocationID, and JobRoleID are the secondary fields.
        // * New records also need this update to happen.
        // --------------------------------------------------------------------
        private void executeSecondaryUpdates()
        {

            string update2SQL = "";


            // Generate Update Statements for Secondary Updates
            for (int curRow = 0; curRow < this.inEmployees.Rows.Count; curRow++)
            {

                if (curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Employee Secondary Update " + curRow + " of " + this.inEmployees.Rows.Count);
                }

                string Emp_New = this.inEmployees.Rows[curRow]["New"].ToString();
                string Emp_ErrorFg = this.inEmployees.Rows[curRow]["ErrorFg"].ToString();
                string Emp_Update = this.inEmployees.Rows[curRow]["Update"].ToString();
                string Emp_ID = this.inEmployees.Rows[curRow]["UpdateID"].ToString();
                string Emp_ASPNETID = this.inEmployees.Rows[curRow]["UpdateAUID"].ToString();
                string Emp_Username = this.inEmployees.Rows[curRow]["Username"].ToString().Replace("'", "''");
                string Emp_EmployeNumber = this.inEmployees.Rows[curRow]["EmployeeNumber"].ToString().Replace("'", "''"); ;

                string Emp_LocationCode = this.inEmployees.Rows[curRow]["LocationCode"].ToString().Replace("'", "''");
                string Emp_JobRoleCode = this.inEmployees.Rows[curRow]["JobRoleCode"].ToString().Replace("'", "''");
                string Emp_SupervisorCode = this.inEmployees.Rows[curRow]["SupervisorCode"].ToString().Replace("'", "''");
                string Emp_ReportingSupervisorCode = this.inEmployees.Rows[curRow]["ReportingSupervisorCode"].ToString().Replace("'", "''");
                string Emp_AudienceCode = this.inEmployees.Rows[curRow]["AudienceCode"].ToString().Replace("'", "''");
                string Emp_AudienceCostCenter = ""; // Custom for MidCountry
                string Emp_AudienceEmpLevel = "";// Custom for MidCountry
                string Emp_AudienceExempt = "";// Custom for MidCountry
                string Emp_AudienceManager = "";// Custom for MidCountry
                string Emp_AudienceCustom1 = "";// Custom Rule-based option
                string Emp_AudienceCustom2 = "";// Custom Rule-based option


                bool updateJobRoleCode = false;
                if (this.inEmployees.Rows[curRow]["UpdateJobRoleCode"].ToString().Equals("1"))
                {
                    updateJobRoleCode = true;
                }
                bool updateLocationCode = false;
                if (this.inEmployees.Rows[curRow]["UpdateLocationCode"].ToString().Equals("1"))
                {
                    updateLocationCode = true;
                }
                bool updateSupervisorCode = false;
                if (this.inEmployees.Rows[curRow]["UpdateSupervisorCode"].ToString().Equals("1"))
                {
                    updateSupervisorCode = true;
                }
                bool updateReportingSupervisorCode = false;
                if (this.inEmployees.Rows[curRow]["UpdateReportingSupervisorCode"].ToString().Equals("1"))
                {
                    updateReportingSupervisorCode = true;
                }
                bool updateAudienceCode = false;
                if (this.inEmployees.Rows[curRow]["UpdateAudienceCode"].ToString().Equals("1"))
                {
                    updateAudienceCode = true;
                }

                bool updateAudienceCostCenter = false;
                bool updateAudienceExempt = false;
                bool updateAudienceEmpLevel = false;
                bool updateAudienceManager = false;
                if (this.opts.getRuleValue("Rule_CustomMidCountry").ToLower().Equals("yes"))
                {

                    if (this.inEmployees.Rows[curRow]["updateAudienceCostCenter"].ToString().Equals("1"))
                    {
                        updateAudienceCostCenter = true;
                    }
                    if (this.inEmployees.Rows[curRow]["updateAudienceExempt"].ToString().Equals("1"))
                    {
                        updateAudienceExempt = true;
                    }
                    if (this.inEmployees.Rows[curRow]["updateAudienceEmpLevel"].ToString().Equals("1"))
                    {
                        updateAudienceEmpLevel = true;
                    }
                    if (this.inEmployees.Rows[curRow]["updateAudienceManager"].ToString().Equals("1"))
                    {
                        updateAudienceManager = true;
                    }

                    Emp_AudienceCostCenter = this.inEmployees.Rows[curRow]["AudienceCostCenter"].ToString().Replace("'", "''"); ;
                    Emp_AudienceEmpLevel = this.inEmployees.Rows[curRow]["AudienceEmpLevel"].ToString().Replace("'", "''"); ;
                    Emp_AudienceExempt = this.inEmployees.Rows[curRow]["AudienceExempt"].ToString().Replace("'", "''"); ;
                    Emp_AudienceManager = this.inEmployees.Rows[curRow]["AudienceManager"].ToString().Replace("'", "''"); ;
                }

                bool updateAudienceCustom1 = false;
                bool updateAudienceCustom2 = false;
                if (this.opts.getRuleValue("Rule_AudienceCustom1").ToLower().Equals("yes"))
                {
                    if (this.inEmployees.Rows[curRow]["updateAudienceCustom1"].ToString().Equals("1"))
                    {
                        updateAudienceCustom1 = true;

                        Emp_AudienceCustom1 = this.inEmployees.Rows[curRow]["AudienceCustom1"].ToString().Replace("'", "''"); ;
                    }

                }
                if (this.opts.getRuleValue("Rule_AudienceCustom2").ToLower().Equals("yes"))
                {
                    if (this.inEmployees.Rows[curRow]["updateAudienceCustom2"].ToString().Equals("1"))
                    {
                        updateAudienceCustom2 = true;

                        Emp_AudienceCustom2 = this.inEmployees.Rows[curRow]["AudienceCustom2"].ToString().Replace("'", "''"); ;
                    }

                }

                // Check for Supervisor Rules
                if (this.opts.getRuleValue("Rule_UseSupervisorAsReportingSupervisor").ToLower().Equals("yes"))
                {
                    Emp_ReportingSupervisorCode = this.inEmployees.Rows[curRow]["SupervisorCode"].ToString();
                } // End RptSupervisor



                if (Emp_New.Equals("1"))
                {
                    updateSupervisorCode = true;
                    updateLocationCode = true;
                    updateJobRoleCode = true;
                    updateReportingSupervisorCode = true;
                    updateAudienceCode = true;
                    updateAudienceCustom1 = true;
                    updateAudienceCustom2 = true;

                }

                // Generate the Update SQL ( Step = 1: Update Primary Fields 
                // If Update or New Insert. Except if new Insert has an error (there is no record to update in that case )
                if (Emp_Update.Equals("1") || (Emp_New.Equals("1") && !Emp_ErrorFg.Equals("1")))
                {

                    string update_s = "";
                    int changeCount = 0;

                    // Update SupervisorID after the 1st update, after the inserts, after the deletes.
                    // This prevents updating to the incorrect ID if there are duplicate Codes before
                    // all other actions have taken place.

                    if (updateSupervisorCode || updateJobRoleCode || updateLocationCode || updateReportingSupervisorCode ||updateAudienceCode
                        || updateAudienceExempt || updateAudienceManager || updateAudienceEmpLevel || updateAudienceCostCenter
                        || updateAudienceCustom1 || updateAudienceCustom2 )
                    {
 

                        update_s = " Update [user] set ModifiedOn = getdate(), ModifiedBy = 'Import' ";

                        if (updateSupervisorCode)
                        {

                            changeCount++;
                            // If The new SupervisorCode is not valid, set the person to no Supervisor.
                            // This should flag an error in the process method.
                            if (Emp_SupervisorCode.Length < 1 || Emp_SupervisorCode.Equals("*none*"))
                            {
                                update_s += ", SupervisorID = 0 \n";
                            }
                            else
                            {

                                update_s += ", SupervisorID = \n" +
                                    "    ( select S.ID from [user] S where S.CustomerID = " + this.customerID + " \n" +
                                "        and S.EmployeeNumber = '" + Emp_SupervisorCode + "' )\n";
                            }
                        }
                        if (updateReportingSupervisorCode)
                        {




                            changeCount++;
                            // If The new SupervisorCode is not valid, set the person to no Supervisor.
                            // This should flag an error in the process method.
                            if (Emp_ReportingSupervisorCode.Length < 1 || Emp_ReportingSupervisorCode.Equals("*none*"))
                            {
                                update_s += ", ReportingSupervisorID = 0 \n";
                            }
                            else
                            {

                                update_s += ", ReportingSupervisorID = \n" +
                                    "    ( select S.ID from [user] S where S.CustomerID = " + this.customerID + " \n" +
                                "        and S.EmployeeNumber = '" + Emp_ReportingSupervisorCode + "' )\n";
                            }
                        }
                        if (updateLocationCode)
                        {

                            changeCount++;
                            // If The new LocationCode is not valid, set the person to no Location.
                            // This should flag an error in the process method.
                            if (Emp_LocationCode.Length < 1 || Emp_LocationCode.Equals("*none*"))
                            {
                                update_s += ", LocationID = 0 \n";
                            }
                            else
                            {
                                update_s += ", LocationID = \n" +
                                    "    ( select MAX(L.ID) from  Location L where L.CustomerID = " + this.customerID + " \n" +
                                    "        and L.InternalCode = '" + Emp_LocationCode + "' )\n";
                            }
                        }
                        if (updateJobRoleCode)
                        {

                            changeCount++;
                            // If the new JobRoleCode is not valid, set the person to no Role.
                            // This should flag an error in the process method.
                            if (Emp_JobRoleCode.Length < 1 || Emp_JobRoleCode.Equals("*none*"))
                            {
                                update_s += ", JobRoleID = 0 \n";
                            }
                            else
                            {
                                update_s += ", JobRoleID = \n" +
                                    "    ( select MAX(J.ID) from  JobRole J where J.CustomerID = " + this.customerID + " \n" +
                                    "        and J.InternalCode = '" + Emp_JobRoleCode + "' )\n";
                         
                            }
                        }

       

                        if (Emp_New.Equals("1"))
                        {
                            // Just added. Do not have an ID yet.
                            update_s += " where CustomerID = " + this.customerID + " \n" +
                          " and Username = '" + Emp_Username + "' and EMployeeNumber = '" + Emp_EmployeNumber + "' \n\n";
                        }
                        else
                        {
                            // Update with User ID
                            update_s += " where CustomerID = " + this.customerID + " \n" +
                             " and ID = " + Emp_ID + " \n\n";
                        }

                        // Update/Insert UserAUdience for Custom Audience
                
                        if (updateAudienceCustom1)
                        {
                            changeCount++;
                            // If The new updateAudienceCustomer1 is not valid, set the person to no Audience.
                            // This should flag an error in the process method.
                 
                                if (Emp_New.Equals("1") && Emp_AudienceCustom1.Length > 0)
                                {
                                    // Just added. Do not have the ID available here... for new users you just need to add the link
                                    update_s += "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                       "        select (select MAX(U.ID) from [user] U where  U.CUstomerID = " + this.customerID + " and U.Username = '" + Emp_Username + "' and U.EMployeeNumber = '" + Emp_EmployeNumber + "' ),  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCustom1 + "'  \n " +
                                        "      and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom1_ParentIDs").ToLower() +   " ) " +
                                        "  \n ";
                                }
                                else if ( Emp_AudienceCustom1.Length > 0 )
                                {

                                    update_s += " update UserAudience set AudienceID = ( select MAX(A.ID) from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCustom1 + "' and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom1_ParentIDs").ToLower() + " )  ) , ModifiedOn = getdate(), ModifiedBy = 'Import' \n " +
                                        " from UserAudience as UA \n" +
                                       "  inner join Audience as A on UA.AudienceID = A.ID and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom1_ParentIDs").ToLower() + " ) \n" + 
                                        "  where UserID = " + Emp_ID + " \n " +
                                    "   if @@Rowcount=0 \n " +
                                     "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                    "        select " + Emp_ID + ",  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCustom1 + "'  \n " +
                                               "      and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom1_ParentIDs").ToLower() +   " ) " +
                                     "  \n " +
                                    "  else if @@Rowcount > 1 \n " +
                                    "  	 delete UserAudience where ID  in ( select UA.ID from UserAudience as UA  inner join Audience as A on A.ID = UA.AudienceID  and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom1_ParentIDs").ToLower() + "  ) and A.InternalCode <> '" + Emp_AudienceCustom1 + "' where UA.UserID = " + Emp_ID + " ) \n " +
                                     "     and UserID = " + Emp_ID  + "  \n\n\n";

                                }else if ( !Emp_New.Equals("1") ){
                                    // Blank Audience and Not new user will cause the audiences (if there is no blank audience) from this set to be
                                    // removed.
                                    update_s += "  	 delete UserAudience where ID  in ( select UA.ID from UserAudience as UA  inner join Audience as A on A.ID = UA.AudienceID  and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom1_ParentIDs").ToLower() + "  ) and A.InternalCode <> '" + Emp_AudienceCustom1 + "' where UA.UserID = " + Emp_ID + " ) \n " +
                                     "     and UserID = " + Emp_ID  + "  \n\n\n";

                                }

                            

                        } // End Custom Audience 2
                        // Update/Insert UserAUdience for Custom Audience
                        if (updateAudienceCustom2)
                        {
                            changeCount++;
                            // If The new updateAudienceCustomer2 is not valid, set the person to no Audience.
                            // This should flag an error in the process method.

                            if (Emp_New.Equals("1") && Emp_AudienceCustom2.Length > 0)
                            {
                                // Just added. Do not have the ID available here... for new users you just need to add the link
                                update_s += "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                   "        select (select MAX(U.ID) from [user] U where  U.CUstomerID = " + this.customerID + " and U.Username = '" + Emp_Username + "' and U.EMployeeNumber = '" + Emp_EmployeNumber + "' ),  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCustom2 + "'  \n " +
                                    "      and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom2_ParentIDs").ToLower() + " ) " +
                                    "  \n ";
                            }
                            else if (Emp_AudienceCustom2.Length > 0)
                            {

                                update_s += " update UserAudience set AudienceID = ( select MAX(A.ID) from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCustom2 + "' and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom2_ParentIDs").ToLower() + " )  ) , ModifiedOn = getdate(), ModifiedBy = 'Import' \n " +
                                       " from UserAudience as UA \n" +
                                       "  inner join Audience as A on UA.AudienceID = A.ID and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom2_ParentIDs").ToLower() + " ) \n" +
                                        "  where UserID = " + Emp_ID + " \n " +
                                "   if @@Rowcount=0 \n " +
                                 "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                "        select " + Emp_ID + ",  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCustom2 + "'  \n " +
                                           "      and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom2_ParentIDs").ToLower() + " ) " +
                                 "  \n " +
                                "  else if @@Rowcount > 1 \n " +
                                "  	 delete UserAudience where ID  in ( select UA.ID from UserAudience as UA  inner join Audience as A on A.ID = UA.AudienceID  and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom2_ParentIDs").ToLower() + "  ) and A.InternalCode <> '" + Emp_AudienceCustom2 + "' where UA.UserID = " + Emp_ID + " ) \n " +
                                 "     and UserID = " + Emp_ID + "  \n\n\n";

                            }
                            else if (!Emp_New.Equals("1"))
                            {
                                // Blank Audience and Not new user will cause the audiences (if there is no blank audience) from this set to be
                                // removed.
                                update_s += "  	 delete UserAudience where ID  in ( select UA.ID from UserAudience as UA  inner join Audience as A on A.ID = UA.AudienceID  and A.ID in ( " + opts.getRuleValue("Rule_AudienceCustom2_ParentIDs").ToLower() + "  ) and A.InternalCode <> '" + Emp_AudienceCustom2 + "' where UA.UserID = " + Emp_ID + " ) \n " +
                                "     and UserID = " + Emp_ID + "  \n\n\n";

                            }



                        } // End Custom Audience 2

                       // System.Console.WriteLine(" Audience Update: " + update_s);

                        // Update/Insert UserAUdience Records
                        if (updateAudienceCode)
                        {

                            changeCount++;
                            // If The new updateAudienceCode is not valid, set the person to no Audience.
                            // This should flag an error in the process method.
                            if (Emp_AudienceCode.Length < 1 || Emp_AudienceCode.Equals("*none*"))
                            {
                                update_s += "";
                            }
                            else
                            {

                                if (Emp_New.Equals("1"))
                                {
                                    // Just added. Do not have the ID available here... for new users you just need to add the link
                                    update_s += "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                       "        select (select MAX(U.ID) from [user] U where  U.CUstomerID = " + this.customerID + " and U.Username = '" + Emp_Username + "' and U.EMployeeNumber = '" + Emp_EmployeNumber + "' ),  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCode + "'  \n " +
                                        "  \n ";
                                }
                                else
                                {

                                    update_s += " update UserAudience set AudienceID = ( select MAX(A.ID) from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCode + "' ) , ModifiedOn = getdate(), ModifiedBy = 'Import' \n " +
                                    "  where UserID = " + Emp_ID + " \n " +
                                    "   if @@Rowcount=0 \n " +
                                     "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                    "        select " + Emp_ID + ",  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCode + "'  \n " +
                                     "  \n " +
                                    "  else if @@Rowcount > 1 \n " +
                                    "  	 delete UserAudience where ID not in ( select MAX(UA.ID) from UserAudience UA where UA.UserID = " + Emp_ID + " ) \n " +
                                     "     and UserID = " + Emp_ID + " \n\n\n";

                                }
                                   
                            }
                        }



                        // Custom MidCountry Financial Audience fields
                        if (updateAudienceCostCenter || updateAudienceEmpLevel || updateAudienceExempt || updateAudienceManager)
                        {

                           

                            // Get two in list of possible parent ids. The intersection of the two lists is the parent it
                            // needs to be associated to. Type = HCF, MCF, CBD, MBF; 

                            string atype = Emp_AudienceExempt.Substring(0, 3);
                            string typeparentids = this.opts.getRuleValue("Rule_Audience" + atype + "_ParentID").ToLower();
                            //Console.WriteLine("Update Types: " + atype + ":" + typeparentids);
                            string parentids = ""; // this.opts.getRuleValue("Rule_AudienceCostCenter_ParentID").ToLower();
                            //Console.WriteLine("Update PIds: " + atype + ":" + parentids);

                            changeCount++;
                            // If The new updateAudienceCode is not valid, set the person to no Audience.
                            // This should flag an error in the process method.
                            if (!updateAudienceCostCenter || Emp_AudienceCostCenter.Length < 1 || Emp_AudienceCostCenter.Equals("*none*"))
                            {
                                update_s += "";
                            }
                            else
                            {
                                parentids = this.opts.getRuleValue("Rule_AudienceCostCenter_ParentID").ToLower();
                                //Console.WriteLine("Update Cost Center: " + atype + ":" + parentids);
                                if (Emp_New.Equals("1"))
                                {
                                    // Just added. Do not have the ID available here... for new users you just need to add the link
                                    update_s += "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                       "        select (select MAX(U.ID) from [user] U where  U.CUstomerID = " + this.customerID + " and U.Username = '" + Emp_Username + "' and U.EMployeeNumber = '" + Emp_EmployeNumber + "' ),  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCostCenter + "'  \n " +
                                       " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                        "  \n ";
                                }
                                else
                                {

                                    update_s += " delete UserAudience  \n" + 
	                                    "  where ID not in ( select MAX(UA.ID) \n" +
		                                " from UserAudience as UA  \n " + 
		                                " inner join Audience as A2 on A2.ID = UA.AudienceID \n" +
		                                " where UA.UserID = " + Emp_ID + " \n" + 
		                                " and A2.CustomerID =  " + this.customerID + "  and A2.ParentAudienceID in (  " + parentids + ") and A2.ParentAudienceID in (" + typeparentids + " ) \n" +
	                                    " ) \n" +
                                        " and UserID = " + Emp_ID + " \n " +
		                                " and AudienceID in ( select ID  from Audience A where A.CustomerID = " + this.customerID + " \n" +
                                        " and A.ParentAudienceID in (  " + parentids + " )   and A.ParentAudienceID in (" + typeparentids + " ) \n" +
                                        " ) \n\n " +
                                        " update UserAudience set AudienceID = ( select MAX(A.ID) from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCostCenter + "' \n " +
                                        " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                    " ) \n " +
                                    "  where UserID = " + Emp_ID + " \n " +
                                    "  and AudienceID in ( select ID from Audience A where A.CustomerID = " + this.customerID + " \n " +
                                        " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                    " ) \n " +
                                    "   if @@Rowcount=0 \n " +
                                     "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                    "        select " + Emp_ID + ",  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceCostCenter + "'  \n " +
                                    " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                     "  \n\n\n "; ;

                                }

                            }

                            // 
                            //|| updateAudienceEmpLevel || updateAudienceExempt || updateAudienceManager

                            if (!updateAudienceExempt || Emp_AudienceExempt.Length < 1 || Emp_AudienceExempt.Equals("*none*"))
                                {
                                    update_s += "";
                                }
                                else
                                {
                                    parentids = this.opts.getRuleValue("Rule_AudienceExempt_ParentID").ToLower();
                                    //Console.WriteLine("Update Cost Center: " + atype + ":" + parentids);
                                    if (Emp_New.Equals("1"))
                                    {
                                        // Just added. Do not have the ID available here... for new users you just need to add the link
                                        update_s += "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                           "        select (select MAX(U.ID) from [user] U where  U.CUstomerID = " + this.customerID + " and U.Username = '" + Emp_Username + "' and U.EMployeeNumber = '" + Emp_EmployeNumber + "' ),  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceExempt + "'  \n " +
                                           " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                            "  \n ";
                                    }
                                    else
                                    {

                                        update_s += " delete UserAudience  \n" +
                                            "  where ID not in ( select MAX(UA.ID) \n" +
                                            " from UserAudience as UA  \n " +
                                            " inner join Audience as A2 on A2.ID = UA.AudienceID \n" +
                                            " where UA.UserID = " + Emp_ID + " \n" +
                                            " and A2.CustomerID =  " + this.customerID + "  and A2.ParentAudienceID in (  " + parentids + ") and A2.ParentAudienceID in (" + typeparentids + " ) \n" +
                                            " ) \n" +
                                            " and UserID = " + Emp_ID + " \n " +
                                            " and AudienceID in ( select ID  from Audience A where A.CustomerID = " + this.customerID + " \n" +
                                            " and A.ParentAudienceID in (  " + parentids + " )   and A.ParentAudienceID in (" + typeparentids + " ) \n" +
                                            " ) \n\n " +
                                            " update UserAudience set AudienceID = ( select MAX(A.ID) from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceExempt + "' \n " +
                                            " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                        " ) \n " +
                                        "  where UserID = " + Emp_ID + " \n " +
                                        "  and AudienceID in ( select ID from Audience A where A.CustomerID = " + this.customerID + " \n " +
                                            " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                        " ) \n " +
                                        "   if @@Rowcount=0 \n " +
                                         "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                        "        select " + Emp_ID + ",  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceExempt + "'  \n " +
                                        " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                         "  \n\n\n "; ;

                                    }

                                }
                            //|| updateAudienceEmpLevel || updateAudienceExempt || updateAudienceManager

                            if (!updateAudienceManager || Emp_AudienceManager.Length < 1 || Emp_AudienceManager.Equals("*none*"))
                            {
                                update_s += "";
                            }
                            else
                            {
                                parentids = this.opts.getRuleValue("Rule_AudienceManager_ParentID").ToLower();
                                //Console.WriteLine("Update Cost Center: " + atype + ":" + parentids);
                                if (Emp_New.Equals("1"))
                                {
                                    // Just added. Do not have the ID available here... for new users you just need to add the link
                                    update_s += "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                       "        select (select MAX(U.ID) from [user] U where  U.CUstomerID = " + this.customerID + " and U.Username = '" + Emp_Username + "' and U.EMployeeNumber = '" + Emp_EmployeNumber + "' ),  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceManager + "'  \n " +
                                       " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                        "  \n ";
                                }
                                else
                                {

                                    update_s += " delete UserAudience  \n" +
                                        "  where ID not in ( select MAX(UA.ID) \n" +
                                        " from UserAudience as UA  \n " +
                                        " inner join Audience as A2 on A2.ID = UA.AudienceID \n" +
                                        " where UA.UserID = " + Emp_ID + " \n" +
                                        " and A2.CustomerID =  " + this.customerID + "  and A2.ParentAudienceID in (  " + parentids + ") and A2.ParentAudienceID in (" + typeparentids + " ) \n" +
                                        " ) \n" +
                                        " and UserID = " + Emp_ID + " \n " +
                                        " and AudienceID in ( select ID  from Audience A where A.CustomerID = " + this.customerID + " \n" +
                                        " and A.ParentAudienceID in (  " + parentids + " )   and A.ParentAudienceID in (" + typeparentids + " ) \n" +
                                        " ) \n\n " +
                                        " update UserAudience set AudienceID = ( select MAX(A.ID) from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceManager + "' \n " +
                                        " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                    " ) \n " +
                                    "  where UserID = " + Emp_ID + " \n " +
                                    "  and AudienceID in ( select ID from Audience A where A.CustomerID = " + this.customerID + " \n " +
                                        " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                    " ) \n " +
                                    "   if @@Rowcount=0 \n " +
                                     "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                    "        select " + Emp_ID + ",  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceManager + "'  \n " +
                                    " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                     "  \n\n\n "; ;

                                }

                            }
                            if (!updateAudienceEmpLevel || Emp_AudienceEmpLevel.Length < 1 || Emp_AudienceEmpLevel.Equals("*none*"))
                            {
                                update_s += "";
                            }
                            else
                            {
                                parentids = this.opts.getRuleValue("Rule_AudienceEmpLevel_ParentID").ToLower();
                                //Console.WriteLine("Update Cost Center: " + atype + ":" + parentids);
                                if (Emp_New.Equals("1"))
                                {
                                    // Just added. Do not have the ID available here... for new users you just need to add the link
                                    update_s += "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                       "        select (select MAX(U.ID) from [user] U where  U.CUstomerID = " + this.customerID + " and U.Username = '" + Emp_Username + "' and U.EMployeeNumber = '" + Emp_EmployeNumber + "' ),  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceEmpLevel + "'  \n " +
                                       " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                        "  \n ";
                                }
                                else
                                {

                                    update_s += " delete UserAudience  \n" +
                                        "  where ID not in ( select MAX(UA.ID) \n" +
                                        " from UserAudience as UA  \n " +
                                        " inner join Audience as A2 on A2.ID = UA.AudienceID \n" +
                                        " where UA.UserID = " + Emp_ID + " \n" +
                                        " and A2.CustomerID =  " + this.customerID + "  and A2.ParentAudienceID in (  " + parentids + ") and A2.ParentAudienceID in (" + typeparentids + " ) \n" +
                                        " ) \n" +
                                        " and UserID = " + Emp_ID + " \n " +
                                        " and AudienceID in ( select ID  from Audience A where A.CustomerID = " + this.customerID + " \n" +
                                        " and A.ParentAudienceID in (  " + parentids + " )   and A.ParentAudienceID in (" + typeparentids + " ) \n" +
                                        " ) \n\n " +
                                        " update UserAudience set AudienceID = ( select MAX(A.ID) from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceEmpLevel + "' \n " +
                                        " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                    " ) \n " +
                                    "  where UserID = " + Emp_ID + " \n " +
                                    "  and AudienceID in ( select ID from Audience A where A.CustomerID = " + this.customerID + " \n " +
                                        " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                    " ) \n " +
                                    "   if @@Rowcount=0 \n " +
                                     "   insert into UserAudience ( UserID, AudienceID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn )  \n " +
                                    "        select " + Emp_ID + ",  MAX(A.ID),  'Import', 'Import', getdate(), getdate()  from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + Emp_AudienceEmpLevel + "'  \n " +
                                    " and A.ParentAudienceID in ( " + parentids + " ) and A.ParentAudienceID in ( " + typeparentids + " ) \n" +
                                     "  \n\n\n "; ;

                                }

                            }

                        } // End MidCountry financial custom Audience
                        //Console.WriteLine("Customer Audience CostCenter:" + update_s);


                    } // end UPdate specific fields check

                    // Update Main User
                    if (opts.isExecute() && changeCount > 0)
                    {

                        try
                        {
                            SqlCommand myCommand = new SqlCommand(update_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount < 1)
                            {
                                this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                                this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", Update 2 Failed with rc:" + rowcount;
                                update_s = "FAILED:" + update_s;
                                this.stats.error_count++;
                            }
                        }
                        catch (Exception e)
                        {
                            this.inEmployees.Rows[curRow]["ErrorFg"] = "1";
                            this.inEmployees.Rows[curRow]["Error"] = this.inEmployees.Rows[curRow]["Error"].ToString() + ", Update 2 Failed.";
                            update_s += e.Message;
                            update_s = "FAILED:" + update_s;
                            this.stats.error_count++;

                        }
                    } // end execute
                    if (update_s.Length > 1)
                    {
                        update2SQL += update_s + "\n ** //---------------------------------------------- **";
                    }

                } // end if update is needed

            } // End big employee loop

            this.inEmployees.AcceptChanges();

            // Update anyone that is a supervisor with a supervisor role.
            // FIX. This could be a large update. It would be best to set the rowcount to be 200
            // and to loop until the rows update count gets to less then 1. 

            string update_roles = "insert into aspnet_usersinroles (UserId, RoleId) \n" +
            "select UserId = au.UserId, RoleID = '" + opts.getSupervisorRoleID() + "' from [user] u\n" +
            "inner join aspnet_users as au on au.username = u.username \n" +
            "and u.customerId = " + opts.getCustomerID() + " \n" +
            "and au.applicationId = '" + opts.getApplicationID() + "' \n" +
            "and exists ( \n" +
            "    select 1 from [user] u2 \n" +
            "       where u2.customerId = " + opts.getCustomerID() + " \n" +
            "        and u2.SupervisorId = u.ID \n" +
            ") \n" +
            "and not exists ( \n" +
            "    select 1 from aspnet_usersinroles ur \n" +
            "    where ur.UserId = au.UserId \n" +
            "        and ur.RoleId = '" + opts.getSupervisorRoleID() + "' \n" +
            ")\n";
            Console.WriteLine(update_roles);

            // Update if requested. ( Rule_13_NoSupervisorRoleUpdates "yes" turns this update off )
            if (opts.isExecute() &&  ! (this.opts.getRuleValue("Rule_13_NoSupervisorRoleUpdates").ToLower() == "yes")  )
            {
                SqlCommand myCommand2 = new SqlCommand(update_roles, this.db);
                int rowcount_2 = myCommand2.ExecuteNonQuery();
                Console.WriteLine("Supervisor Role Update Row Count =  " + rowcount_2 + " ");
                if (rowcount_2 < 0)
                {

                    Console.WriteLine("Supervisor Role Update failed!");
                }

            }


            // Update anyone that is a reporting supervisor with a reporting supervisor role.
            // FIX. This could be a large update. It would be best to set the rowcount to be 200
            // and to loop until the rows update count gets to less then 1. 

            string update_roles2 = "insert into aspnet_usersinroles (UserId, RoleId) \n" +
            "select UserId = au.UserId, RoleID = '" + opts.getReportingSupervisorRoleID() + "' from [user] u\n" +
            "inner join aspnet_users as au on au.username = u.username \n" +
            "and u.customerId = " + opts.getCustomerID() + " \n" +
            "and au.applicationId = '" + opts.getApplicationID() + "' \n" +
            "and exists ( \n" +
            "    select 1 from [user] u2 \n" +
            "       where u2.customerId = " + opts.getCustomerID() + " \n" +
            "        and u2.ReportingSupervisorId = u.ID \n" +
            ") \n" +
            "and not exists ( \n" +
            "    select 1 from aspnet_usersinroles ur \n" +
            "    where ur.UserId = au.UserId \n" +
            "        and ur.RoleId = '" + opts.getReportingSupervisorRoleID() + "' \n" +
            ")\n";
            Console.WriteLine(update_roles2);

            // Update if requested.
            if (opts.isExecute() && !(this.opts.getRuleValue("Rule_14_NoReportingSupervisorRoleUpdates").ToLower() == "yes") )
            {
                SqlCommand myCommand2 = new SqlCommand(update_roles2, this.db);
                int rowcount_2 = myCommand2.ExecuteNonQuery();
                Console.WriteLine("Reporting Supervisor Role Update Row Count =  " + rowcount_2 + " ");
                if (rowcount_2 < 0)
                {

                    Console.WriteLine("Reporting Supervisor Role Update failed!");
                }

            }


            Utility.SaveToFile(this.baseDirName + "u4_Emp_Update2SQL.txt", update2SQL);

        }

        // ---------------- LOAD CLIENT INFORMATION ------------------
        private int loadClientInfo()
        {


            string clientInfoSQL = "select A.ApplicationName ,A.ApplicationID,  \n" +
            "UserRoleID = R1.RoleID, SupRoleID = R2.RoleID, ReportSupRoleID = R3.RoleID \n" +
            "from Customer C\n" +
            "inner join aspnet_Applications as A on A.ApplicationName = C.SubDomain\n" +
            "inner join aspnet_Roles as R1 on A.ApplicationId = R1.ApplicationId and R1.RoleName = 'Customer - User'\n" +
            "inner join aspnet_Roles as R2 on A.ApplicationId = R2.ApplicationId and R2.RoleName = 'Classroom - Supervisor'\n" +
            "inner join aspnet_Roles as R3 on A.ApplicationId = R3.ApplicationId and R3.RoleName = 'Reporting - Supervisor'\n" +
            "where C.ID = " + opts.getCustomerID() + "\n";


            DataTable cltInfo = new DataTable();

            // Get the Data
            try
            {
                if (!this.silent)
                {
                    Console.WriteLine("Client Info Get Data: " + clientInfoSQL);
                }
                SqlCommand myCommand = new SqlCommand(clientInfoSQL, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!this.silent)
                {

                    Console.WriteLine("Employee DB Data Loaded.");
                }
                // Load into DataTable
                cltInfo.Load(myReader);




            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }
            if (cltInfo.Rows.Count < 1)
            {
                Console.WriteLine("No Client Information. Will not execute.");
                return -1;
            }
            //Default some values
            for (int curRow = 0; curRow < cltInfo.Rows.Count; curRow++)
            {
                opts.setCustomerShortName(cltInfo.Rows[curRow]["ApplicationName"].ToString());
                opts.setApplicationID(cltInfo.Rows[curRow]["ApplicationID"].ToString());
                opts.setUserRoleID(cltInfo.Rows[curRow]["UserRoleID"].ToString());
                opts.setSupervisorRoleID(cltInfo.Rows[curRow]["SupRoleID"].ToString());
                opts.setReportingSupervisorRoleID(cltInfo.Rows[curRow]["ReportSupRoleID"].ToString());
            }

            Console.Write(opts.toString());

            return 0; // ok
        }

        // -----------------------------------------------------------
        // Add the user using the ASP.NET Membership
        //
        // -----------------------------------------------------------
        private string AddASPNETUser(string username, string password, string email)
        {
            //string username = "bankersedge2";
            //string password = "password1";
            //string email = "bcarlson@bankersedge.com";


            string passwordQuestion = "foo";
            string passwordAnswer = "bar";
            Boolean isApproved = true;


            Membership.ApplicationName = opts.getCustomerShortName();// "example: anbone"; Acts as the App Name

            Console.WriteLine("Create User: " + username + " App = " + Membership.ApplicationName);
            MembershipCreateStatus memStatus = new MembershipCreateStatus();


            //** ADD USER
            MembershipUser mu = Membership.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, out memStatus);
            Console.WriteLine("Created User:" + memStatus.ToString());


            if (memStatus != MembershipCreateStatus.Success)
            {
                Console.WriteLine("Error adding " + username + ".");
                return "ERROR";
            }
            Console.WriteLine("Done adding " + username + ".");
            return mu.ProviderUserKey.ToString();
        }
        // -----------------------------------------------------------
        // Change the user password using the ASP.NET Membership
        //
        // -----------------------------------------------------------
        private int changeASPNETUserPassword(string username, string password)
        {
            Console.WriteLine("Change password: " + username + " : " + password);
            Membership.ApplicationName = opts.getCustomerShortName();// "example: anbone"; Acts as the App Name
            MembershipUser mu1 = Membership.GetUser(username);
            string pwd2 = mu1.ResetPassword();
            string newpwd = password;
            try
            {
                bool hasChanged = mu1.ChangePassword(pwd2, newpwd);
                if (!hasChanged)
                {
                    Console.WriteLine("PWD Not Changed: new = " + newpwd + " Temp pwd is " + pwd2);
                    return -1;
                }
            }
            catch (System.ArgumentException ae)
            {
                Console.WriteLine("PWD has not changed: " + ae.Message + ";Temp pwd is " + pwd2);
                return -2;
            }
            return 1;
        }



        //-----------------------------------------------------------
        // Look the Location
        // ---------------------------------------------------------

        private int lookupLocation(int curRow, string LocationCode, string LocationName) {

  
                // Find Location in lookup
                DataRow[] foundRows1;
                string filter = "InternalCode = '" + LocationCode.ToLower().Replace("'","''") + "'";  // Not case sensitive
           
                foundRows1 = this.inLocations.Select(filter);

                // Auto Add the 
                if (foundRows1.Length < 1)
                {
                    try
                    {
                        // Add to lookup as New
                        DataRow newRow = this.inLocations.NewRow();
                        newRow["New"] = "1";
                        newRow["InternalCode"] = LocationCode;
                        if (LocationName.Length < 1)
                        {
                            newRow["Name"] = LocationCode;
                        }   else  {
                            newRow["Name"] = LocationName;
                        }
                        this.inLocations.Rows.Add(newRow);
                    }
                    catch (Exception e1)
                    {
                        Console.WriteLine("LookupLocation Error. " + e1.Message + " ='" + LocationCode + "'");
                    }
                }
                if( foundRows1.Length > 0 ){
                    if( foundRows1[0]["New"].ToString().CompareTo("1") == 0 ){
                        // Newly inserted. Return no current/orginal match.
                        return 0;
                    }
                }
                return foundRows1.Length;

        }// End lookup location method
  
        private int lookupJobRole(int curRow, string JobRoleCode, string JobRoleName)
        {


            // Find Location in lookup
            DataRow[] foundRows1;
            string filter = "InternalCode = '" + JobRoleCode.ToLower().Replace("'", "''") + "'";  // Not case sensitive
            foundRows1 = this.inJobRoles.Select(filter);

            if (foundRows1.Length < 1)
            {
                // Add to lookup as New
                DataRow newRow = this.inJobRoles.NewRow();
                newRow["New"] = "1";
                newRow["InternalCode"] = JobRoleCode;
                if( JobRoleName.Length < 1 ){
                    newRow["Name"] = JobRoleCode;
                }else{
                    newRow["Name"] = JobRoleName; // JobRoleName was not on the file so use the code as the name.
                }
                this.inJobRoles.Rows.Add(newRow);
            }
            if (foundRows1.Length > 0)
            {
                if (foundRows1[0]["New"].ToString().CompareTo("1") == 0)
                {
                    // Newly inserted. Return no current/orginal match.
                    return 0;
                }
            }
            return foundRows1.Length;

        }// End lookup JobRole method





        private int lookupAudience(int curRow, string AudienceCode, string AudienceName)
        {

            // Returns > 1 if found, < 1 if not found or Newly inserted


            // Find Location in lookup
            DataRow[] foundRows1;
            string filter = "InternalCode = '" + AudienceCode.ToLower().Replace("'", "''") + "'";  // Not case sensitive
            foundRows1 = this.inAudiences.Select(filter);

            if (foundRows1.Length < 1)
            {
                // Add to lookup as New
                DataRow newRow = this.inAudiences.NewRow();
                newRow["New"] = "1";
                newRow["InternalCode"] = AudienceCode;
                if (AudienceName.Length < 1)
                {
                    newRow["Name"] = AudienceCode;
                }
                else
                {
                    newRow["Name"] = AudienceName; // JobRoleName was not on the file so use the code as the name.
                }
                this.inAudiences.Rows.Add(newRow);
                return 0;
            }
            if (foundRows1.Length > 0)
            {
                if (foundRows1[0]["New"].ToString().CompareTo("1") == 0)
                {
                    // Newly inserted. Return no current/orginal match.
                    return 0;
                }
            }
            return foundRows1.Length;

        }// End lookup Audience method


        private int lookupAudience(int curRow, string AudienceCode, string AudienceName, string parentids1, string parentids2)
        {

            // Returns > 1 if found, < 1 if not found or Newly inserted

            // cross selection between parentcodes1 and parentcodes2 ( MidCountry parentcodes1 is the type, and parentcodes2 is the business unit )
            // needs to be in the correct section.

            // Find Location in lookup
            DataRow[] foundRows1;
            string filter = "InternalCode = '" + AudienceCode.ToLower().Replace("'", "''") + "' and ParentAudienceID in ( " + parentids1 + ") and ParentAudienceID in ( " + parentids2 + ")";  // Not case sensitive
            foundRows1 = this.inAudiences.Select(filter);

            //Console.WriteLine("P1: " + parentids1 + " P2: " + parentids2 + " F: " + filter);
            if (foundRows1.Length < 1)
            {
                // Add to lookup as New
                DataRow newRow = this.inAudiences.NewRow();
                newRow["New"] = "1";
                newRow["InternalCode"] = AudienceCode;
                if (AudienceName.Length < 1)
                {
                    newRow["Name"] = AudienceCode;
                }
                else
                {
                    newRow["Name"] = AudienceName; // AudienceName was not on the file so use the code as the name.
                }

                //Find Correct ParentAudienceID
                string pid = lookupAudienceParentID(parentids1, parentids2);
                if (pid.Length < 1)
                {
                    pid = "0";
                }
                newRow["ParentAudienceID"] = pid;
                this.inAudiences.Rows.Add(newRow);
                return 0;
            }
            if (foundRows1.Length > 0)
            {
                if (foundRows1[0]["New"].ToString().CompareTo("1") == 0)
                {
                    // Newly inserted. Return no current/orginal match.
                    return 0;
                }
            }
            return foundRows1.Length;

        }// End lookup Audience method


        private string lookupAudienceParentID(string parentids1, string parentids2)
        {
            string[] p1 = parentids1.Split(',');
            string[] p2 = parentids2.Split(',');
            foreach (string pid in p1)
            {
                foreach (string pid2 in p2)
                {
                    if (pid.CompareTo(pid2) == 0)
                    {
                       // Console.WriteLine("AudienceParent:" + pid + " = " + pid2);
                        return pid;
                    }
                }
            }

            return "";
        }

    }
}


