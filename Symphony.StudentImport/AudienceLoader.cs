﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace StudentImport
{


    // --------------------------------------------------------
    // Loader for Audience Records
    //  1. 
    // --------------------------------------------------------
    public class AudienceLoader
    {


        // Main Inbound Audience information to be loaded
         DataTable inAudience = null;
        String customerID = "";
        ArrayList errorMessages = null;
        SqlConnection db = null;
        DataTable oldAudience = null;
        string baseDirName = "";
        string programName = "Import";
        StudentImport.Queue jobQueue = null;
        Options opts = null;

        // Ignore stat checks
        Boolean ignoreWarnings = false; // Turn this on to stop reasonability checks on changes. (If there are a lot of changes )


        // Standard Stats
        StatInfo stats = null;


        // -------------------------------------------------------------
        // Constructor 
        // Parameter dt is the inbound file data loaded into a DataTable
        // -------------------------------------------------------------
        public AudienceLoader(SqlConnection dbCon, Options optvalues, StudentImport.Queue q, DataTable dt )
        {

            this.opts = optvalues;
            this.inAudience = dt;
            this.customerID = this.opts.getCustomerID();
            this.db = dbCon;
            this.errorMessages = new ArrayList();
            this.baseDirName = this.opts.getBaseDirectory();
            this.jobQueue = q;
            this.stats = new StatInfo();
    
            
        }



        // ----------------------------------------------
        // Return the existing Audience records.
        // ----------------------------------------------
        public DataTable getExistingRecords()
        {
            return this.oldAudience;
        }
        // ----------------------------------------------
        // Return the inbound/modified Audience records.
        // ----------------------------------------------
        public DataTable getRecords()
        {
            return this.inAudience;
        }

        // ----------------------------------------------
        // Return the process statistics
        // ----------------------------------------------
        public StatInfo getStats()
        {
            return this.stats;
        }


         // -----------------------------------------------------------
        // sanitizeColumns
        // brief: Renames the column headers
        // to make sure column names are consistent.
        // 1. Change to consistent Case.
        // 2. Trims
        // 3. Adds in some processing columns
        // * This is the place to catch common misspellings if needed
        // (example:  name turns into Name, NAME turns into Name )
        // -----------------------------------------------------------
        public int sanitizeColumns()
        {
            for (int curCol = 0; curCol < inAudience.Columns.Count; curCol++)
            {
                String colName = inAudience.Columns[curCol].ColumnName.ToLower().Trim();
                if (colName.Equals("name"))
                {
                    inAudience.Columns[curCol].ColumnName = "Name";
                }
                if (colName.Equals("code"))
                {
                    inAudience.Columns[curCol].ColumnName = "Code";
                }
                if (colName.Equals("parentcode") || colName.Equals("pcode") ||
                    colName.Equals("parcode") || colName.Equals("perentcode") || colName.Equals("Parent Code"))
                {
                    inAudience.Columns[curCol].ColumnName = "ParentCode";
                }

            }
          
            // Add isNew flag to dataTable
            // This will be used to mark which ones need to be inserted
            // ( These need to be inserted before the updates )

            this.inAudience = Utility.addColumnToDataTable(this.inAudience, "New", "System.String");
            // Add update flag to dataTable
            // This will be used to mark which ones need to be updated in the db
            this.inAudience = Utility.addColumnToDataTable(this.inAudience, "Update", "System.String");
            // Add update comment to dataTable
            // This will be used add notes or comments
            this.inAudience = Utility.addColumnToDataTable(this.inAudience, "UpdateMsg", "System.String");
            // Add existingID to dataTable
            // This will be used to mark what existing record got modified
            this.inAudience = Utility.addColumnToDataTable(this.inAudience, "UpdateID", "System.String");
            // Add Existing Name, Code, ParentAudienceID to dataTable
            // This will be used to see what was updated or not updated
            this.inAudience = Utility.addColumnToDataTable(this.inAudience, "OldName", "System.String");
            this.inAudience = Utility.addColumnToDataTable(this.inAudience, "OldCode", "System.String");
            this.inAudience = Utility.addColumnToDataTable(this.inAudience, "OldParentID", "System.String");
            this.inAudience = Utility.addColumnToDataTable(this.inAudience, "OldParentCode", "System.String");
            // Add Error to dataTable
            // This will be used to note error messages
            this.inAudience = Utility.addColumnToDataTable(this.inAudience, "ErrorFg", "System.String");
            this.inAudience = Utility.addColumnToDataTable(this.inAudience, "Error", "System.String");
 


            return 0; // ok
        }

        // -----------------------------------------------------------
        // validateColumns
        // brief: Checks to make sure column information is available
        // return -1 if no Name
        //        -2 if no Code
        //        -3 if no ParentCode
        // -----------------------------------------------------------
        public int validateColumns()
        {

            if (!inAudience.Columns.Contains("Name"))
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("AudienceLoader.validateColumns : Name column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: Name column not found.");
                    return -1;
            }
            if (!inAudience.Columns.Contains("Code"))
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("AudienceLoader.validateColumns : Code column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: Code column not found.");
                return -2;
            }
            if (!inAudience.Columns.Contains("ParentCode"))
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("AudienceLoader.validateColumns : ParentCode column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: ParentCode column not found.");
                return -3;
            }



            return 0; // ok
        }

        // ---------------------------------------------
        // load the Audience information for the customer
        // from the database.
        // ---------------------------------------------
        public int loadAudiencesFromDB()
        {
            String query = " select ID = A.ID, Name = A.Name, InternalCode = A.InternalCode, \n" +
                        " ParentAudienceID = A.ParentAudienceID, ParentCode =  case when P.InternalCode is null then '*none*' else P.InternalCode end, \n" + 
                        " A.ModifiedBy, A.CreatedBy, A.ModifiedOn, A.CreatedOn \n" +
                        " from Audience  A\n" +
                        " left outer join Audience as P on P.ID = A.ParentAudienceID  \n" +
                        " where A.CUstomerID = " + this.customerID;
            this.oldAudience = new DataTable();

            // Get the Data
            try
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("Audience Get Data: " + query);
                }
                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!opts.isSilent())
                {
                    Console.WriteLine("Audience DB Data Loaded.");
                }
                // Load into DataTable
                this.oldAudience.Load(myReader);

                // Add some processing columns
                this.oldAudience = Utility.addColumnToDataTable(this.oldAudience, "Delete", "System.String");
                this.oldAudience = Utility.addColumnToDataTable(this.oldAudience, "Update", "System.String");


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }

            //Default some values
            for (int curRow = 0; curRow < this.oldAudience.Rows.Count; curRow++)
            {
                this.oldAudience.Rows[curRow]["Delete"] = "Yes";
            }


            return 0; // ok
        }



        // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int processChanges()
        {

            // Pre Process Changes to determine what to do. 
            // Execution of changes will occur next.
            // For each row
            //  1. find it in the existing db rows
            //  2.  Mark if it is new.
            //  3.  Error if it matches to more than one row.
            //  4.  Mark as update if it needs to change.

            this.stats.inbound_count = this.inAudience.Rows.Count;
            this.stats.existing_count = this.oldAudience.Rows.Count;
            int found_count = 0;

            for (int curRow = 0; curRow < this.inAudience.Rows.Count; curRow++)
            {
                // These are the new values on the file
                string A_Code = this.inAudience.Rows[curRow]["Code"].ToString();
                string A_Name = this.inAudience.Rows[curRow]["Name"].ToString();
                string A_ParentCode = this.inAudience.Rows[curRow]["ParentCode"].ToString();

                // Default Some of the Metadata. 
                this.inAudience.Rows[curRow]["UpdateID"] = "";
                this.inAudience.Rows[curRow]["OldCode"] = "";
                this.inAudience.Rows[curRow]["OldName"] = "";
                this.inAudience.Rows[curRow]["OldParentID"] = "";
                this.inAudience.Rows[curRow]["OldParentCode"] = "";

                // Find existing Row using Code value ... if any
                DataRow[] foundRows;
                foundRows = this.oldAudience.Select("InternalCode = '" + A_Code + "'");
                if (foundRows.Length < 1)
                {
                    // Not Found. Mark as New
                    this.inAudience.Rows[curRow]["New"] = "1";

                    this.stats.insert_count++;
                }
                else if (foundRows.Length > 1)
                {
                    // Too many Matches. Error.
                    this.inAudience.Rows[curRow]["ErrorFg"] = "1";
                    this.inAudience.Rows[curRow]["Error"] = "Audience found too many existing record matches on InternalID.(" + foundRows.Length + ")";
                    this.inAudience.Rows[curRow]["New"] = "0";
                    this.stats.error_count++;
                    continue;
                }
                else
                {
                    // Mark existing record as found.
                    foundRows[0]["Delete"] = "No";
                    found_count++;

                    // Check for duplicates
                    DataRow[] dupRows;
                    dupRows = this.inAudience.Select("Code = '" + A_Code + "'");
                    if (dupRows.Length > 1)
                    {
                        this.inAudience.Rows[curRow]["ErrorFg"] = "1";
                        this.inAudience.Rows[curRow]["Error"] = "Duplicate Locations on Inbound File.(" + dupRows.Length + ")";
                        this.stats.error_count++;
                        continue;
                    }

                    // Update Might be needed. Compare the new values to the old values to make sure:

                    this.inAudience.Rows[curRow]["New"] = "0";
                    this.inAudience.Rows[curRow]["UpdateID"] = foundRows[0]["ID"].ToString();

                    string oldName = foundRows[0]["Name"].ToString();
                    string oldCode = foundRows[0]["InternalCode"].ToString();
                    string oldParentCode = foundRows[0]["ParentCode"].ToString();
                    string oldParentID = foundRows[0]["ParentAudienceID"].ToString();

                    // Track the existing values for undo logic and logging.
                    this.inAudience.Rows[curRow]["OldCode"] = oldCode;
                    this.inAudience.Rows[curRow]["OldName"] = oldName;
                    this.inAudience.Rows[curRow]["OldParentID"] = oldParentID;
                    this.inAudience.Rows[curRow]["OldParentCode"] = oldParentCode;


                    Boolean hasChanged = false;
                    if (!oldName.Trim().Equals(A_Name.Trim()))
                    {
                        this.inAudience.Rows[curRow]["Update"] = "1";
                        this.inAudience.Rows[curRow]["UpdateMsg"] = "Name !=" + oldName + ";";
                        hasChanged = true;
                    }
                    if (!oldCode.Trim().Equals(A_Code.Trim()))
                    {
                        this.inAudience.Rows[curRow]["Update"] = "1";
                        this.inAudience.Rows[curRow]["UpdateMsg"] =  this.inAudience.Rows[curRow]["UpdateMsg"] + "InternalCode !=-" + oldCode + ";";
                        hasChanged = true;
                    }

                     // Zero? Zero as a Code or Zero as no Parent? 
                    // ToDO: check for a 0 Job Code

                    if (!A_ParentCode.Equals("0") && A_ParentCode.Trim().Length > 0 && !oldParentCode.Trim().Equals(A_ParentCode.Trim()) )
                    {
                       
               

                        this.inAudience.Rows[curRow]["Update"] = "1";
                        this.inAudience.Rows[curRow]["UpdateMsg"] = this.inAudience.Rows[curRow]["UpdateMsg"] + "ParentCode !=-" + oldParentCode + ";";
                        hasChanged = true;
                    }
                    if (hasChanged)
                    {
                        this.stats.update_count++;
                    }

                }



          
            }

            this.stats.delete_count = this.stats.existing_count - found_count;


       

            return 0; //ok
        }

         // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int executeChanges()
        {

            // Insert new ones
            this.executeAdds();

            // Update primary fields
            this.executeUpdates();

            // Update lookup fields
            this.executeSecondaryUpdates();

            // Generate Delete Statements for Removals
            string deleteSQL = "";
            string undoDeleteSQL = "";
            
            for (int curRow = 0; curRow < this.oldAudience.Rows.Count; curRow++)
            {
                if (this.oldAudience.Rows[curRow]["Delete"].ToString().Equals("No"))
                {
                    continue;
                }
                string OldA_ID = this.oldAudience.Rows[curRow]["ID"].ToString();
                string OldA_Name = this.oldAudience.Rows[curRow]["Name"].ToString();
                string OldA_InternalCode = this.oldAudience.Rows[curRow]["InternalCode"].ToString();
                string OldA_ParentAudienceID = this.oldAudience.Rows[curRow]["ParentAudienceID"].ToString();
                string OldA_ModifiedBy = this.oldAudience.Rows[curRow]["ModifiedBy"].ToString();
                string OldA_CreatedBy = this.oldAudience.Rows[curRow]["CreatedBy"].ToString();
                string OldA_ModifiedOn = this.oldAudience.Rows[curRow]["ModifiedOn"].ToString();
                string OldA_CreatedOn = this.oldAudience.Rows[curRow]["CreatedOn"].ToString();

                string delete_s = " delete Audience where CustomerID = " + this.customerID + " and ID = " + OldA_ID + "\n";

                if (opts.isExecute() && opts.isDelete())
                {

                    try
                    {
                        SqlCommand myCommand = new SqlCommand(delete_s, this.db);
                        int rowcount = myCommand.ExecuteNonQuery();
                        if (rowcount != 1)
                        {
                            this.inAudience.Rows[curRow]["ErrorFg"] = "1";
                            this.inAudience.Rows[curRow]["Error"] = this.inAudience.Rows[curRow]["Error"].ToString() + ", Delete Failed with rc:" + rowcount;
                            delete_s = "FAILED:" + delete_s;
                        }
                    }
                    catch (Exception )
                    {
                        this.inAudience.Rows[curRow]["ErrorFg"] = "1";
                        this.inAudience.Rows[curRow]["Error"] = this.inAudience.Rows[curRow]["Error"].ToString() + ", Delete Failed.";
                        delete_s = "FAILED:" + delete_s;
                    }
                } // end db execute deletes
                deleteSQL += delete_s;
                undoDeleteSQL +=  " insert into Audience ( Name, CustomerID, ParentAudienceID, \n" +
                                " InternalCode, SortOrder ,ModifiedBy, CreatedBy, \n" +
                                " ModifiedOn, CreatedOn )\n" +
                                " values ( '" + OldA_Name + "', " + this.customerID + ", " + OldA_ParentAudienceID + " , \n"+
                                " '" + OldA_InternalCode + "',0,'" + OldA_ModifiedBy + "','"   + OldA_CreatedBy + "', \n" +
                                " '" + OldA_ModifiedOn + "', '" +OldA_CreatedOn + "')\n";



            }
            // NEW FILE
            string fileFilter = " New = '1' ";
            string[] colNames = new string[3] {  "Name", "Code", "ParentCode" };
            Utility.SaveFilteredToCSV(this.inAudience, this.baseDirName + "z1_Ref_AudienceNew.csv", fileFilter, colNames);

            // UPDATE FILE
             fileFilter = " Update = '1' ";
            string[] colNames2 = new string[3] { "Name", "Code", "ParentCode" };
            Utility.SaveFilteredToCSV(this.inAudience, this.baseDirName + "z2_Ref_AudienceUpdate.csv", fileFilter, colNames2);


            // UNDO DELETE FILE: Generate an import file to reload the deleted Audiences if needed.
             fileFilter = " Delete = 'Yes' ";
            string[] colNames3 = new string[9] { "ID", "Name", "InternalCode", "ParentCode", "ParentAudienceID", "ModifiedBy", "ModifiedOn", "CreatedBy", "CreatedOn" };
            Utility.SaveFilteredToCSV(this.oldAudience, this.baseDirName + "z3_Undo_AudienceDelete.csv", fileFilter, colNames3);



            Utility.SaveToFile(this.baseDirName + "u3_Audience_DeleteSQL.txt", deleteSQL +
             "\n ----------------------------------------------- \n  Undo" +
            "\n ----------------------------------------------- \n " + undoDeleteSQL);

            
            return 0; //ok

        }
            

        // ---------------------------------------------------------
        // Update the primary fields
        // -----------------------------------------------------------
        private void executeUpdates()
        {

            string update1SQL = "";
            string undoUpdate1SQL = "";

            // Generate Update Statements for Changes
            for (int curRow = 0; curRow < this.inAudience.Rows.Count; curRow++)
            {

                if (!opts.isSilent() && curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Audience Update Scan " + curRow + " of " + this.inAudience.Rows.Count);
                }

                string A_New = this.inAudience.Rows[curRow]["New"].ToString();
                string A_Update = this.inAudience.Rows[curRow]["Update"].ToString();
                string A_Code = this.inAudience.Rows[curRow]["Code"].ToString();
                string A_Name = this.inAudience.Rows[curRow]["Name"].ToString().Replace("'", "''"); ;
                string A_ParentCode = this.inAudience.Rows[curRow]["ParentCode"].ToString();
                string A_ID = this.inAudience.Rows[curRow]["UpdateID"].ToString(); // Existing AudienceID  
                string OldName = this.inAudience.Rows[curRow]["OldName"].ToString().Replace("'", "''"); ; // Existing Name  
                string OldCode = this.inAudience.Rows[curRow]["OldCode"].ToString(); // Existing Code  
                string OldParentID = this.inAudience.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
                string update_s = "";

                if (A_Update.Equals("1"))
                {


                    update_s = "Update Audience set Name = '" + A_Name.Trim() + "', InternalCode = '" + A_Code + "',\n" +
                       " ModifiedBy = '" + this.programName + "', ModifiedOn = getdate() \n" +
                       " where ID = " + A_ID + " and CustomerID = " + this.customerID + " \n\n";

                    // Update the DB if requested.    
                    if (opts.isExecute())
                    {

                        try
                        {
                            SqlCommand myCommand = new SqlCommand(update_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount != 1)
                            {
                                this.inAudience.Rows[curRow]["ErrorFg"] = "1";
                                this.inAudience.Rows[curRow]["Error"] = this.inAudience.Rows[curRow]["Error"].ToString() + ", Update Failed with rc:" + rowcount;
                                update_s = "FAILED:" + update_s;
                            }
                        }
                        catch (Exception)
                        {
                            this.inAudience.Rows[curRow]["ErrorFg"] = "1";
                            this.inAudience.Rows[curRow]["Error"] = this.inAudience.Rows[curRow]["Error"].ToString() + ", Update Failed.";
                            update_s = "FAILED:" + update_s;
                        }
                    } // end db execute

                    update1SQL += update_s;
                    undoUpdate1SQL += "Update Audience set Name = '" + OldName.Trim() + "', InternalCode = '" + OldCode + "',\n" +
                                  " ModifiedBy = ModifiedBy + ' rollback ' \n" +
                                  " where ID = " + A_ID + " and CustomerID = " + this.customerID + " \n\n";

                }// end if update
                
            } //end main loop

                    // Update ParentAudienceID after the 1st update, after the inserts, after the deletes.
                    // This prevents updating to the incorrect AudienceID if there are duplicate Codes before
                    // all other actions have taken place. 


            Utility.SaveToFile(this.baseDirName + "u1_Audience_Update1SQL.txt", update1SQL +
                 "\n ----------------------------------------------- \n  Undo" +
                 "\n ----------------------------------------------- \n " + undoUpdate1SQL);

        } // End executeUpdate()


        private void executeSecondaryUpdates()
        {

            string update2SQL = "";
            string undoUpdate2SQL = "";


            // Generate Update Statements for Changes
            for (int curRow = 0; curRow < this.inAudience.Rows.Count; curRow++)
            {


                if (!opts.isSilent() && curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Audience Secondary Update Scan " + curRow +" of " + this.inAudience.Rows.Count);
                }


                string A_New = this.inAudience.Rows[curRow]["New"].ToString();
                string A_Update = this.inAudience.Rows[curRow]["Update"].ToString();
                string A_Code = this.inAudience.Rows[curRow]["Code"].ToString();
                string A_Name = this.inAudience.Rows[curRow]["Name"].ToString();
                string A_ParentCode = this.inAudience.Rows[curRow]["ParentCode"].ToString();
                string A_ID = this.inAudience.Rows[curRow]["UpdateID"].ToString(); // Existing AudienceID  
                string OldName = this.inAudience.Rows[curRow]["OldName"].ToString(); // Existing Name  
                string OldCode = this.inAudience.Rows[curRow]["OldCode"].ToString(); // Existing Code  
                string OldParentID = this.inAudience.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
                string update_s = "";

                if (A_Update.Equals("1") || A_New.Equals("1"))
                {


                    if (A_ParentCode.Length > 0 && !A_ParentCode.Equals("*none*"))
                    {

                        //FIX

                        //update_s = "Update A set A.ParentAudienceID = case when P.ID is null then 0 else P.ID end \n" +
                         //  " from Audience as A \n" +
                         //  " left outer join Audience as P on P.ID = A.ParentAudienceID \n" +
                          //  " where ID = " + A_ID + " and CustomerID = " + this.customerID + "\n\n";

                        update_s = "UPDATE A set A.ParentAudienceID = ( select P.ID from Audience P where P.CustomerID = " + this.customerID + "\n" +
                            " and P.InternalCode = '" + A_ParentCode + "' ) \n" +
                            " from Audience A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + A_Code + "'\n";

                        // Secondary Update  the DB if requested.    
                        if (opts.isExecute())
                        {

                            try
                            {
                                SqlCommand myCommand = new SqlCommand(update_s, this.db);
                                int rowcount = myCommand.ExecuteNonQuery();
                                if (rowcount != 1)
                                {
                                    this.inAudience.Rows[curRow]["ErrorFg"] = "1";
                                    this.inAudience.Rows[curRow]["Error"] = this.inAudience.Rows[curRow]["Error"].ToString() + ", Parent Update Failed with rc:" + rowcount;
                                    update_s = "FAILED:" + update_s;
                                }
                            }
                            catch (Exception)
                            {
                                this.inAudience.Rows[curRow]["ErrorFg"] = "1";
                                this.inAudience.Rows[curRow]["Error"] = this.inAudience.Rows[curRow]["Error"].ToString() + ", Parent Update Failed.";
                                update_s = "FAILED:" + update_s;
                            }
                        } // End execute

                        update2SQL += update_s;
                        undoUpdate2SQL += "Update Audience set ParentAudienceID = " + OldParentID + ",\n" +
                        " where ID = " + A_ID + " and CustomerID = " + this.customerID + " \n\n";

                    } // End secondary parent update
                }//End update check
            }// End main loop

            Utility.SaveToFile(this.baseDirName + "u4_Audience_Update2SQL.txt", update2SQL +
            "\n ----------------------------------------------- \n  Undo" +
            "\n ----------------------------------------------- \n " + undoUpdate2SQL);
 
        } // End executeSecondaryUpdates()

        // ------------------------------------------------
        //  Add new Audiences
        // ------------------------------------------------
        private void executeAdds()
        {
            string insertSQL = "";
            string undoInsertSQL = "";

            // Generate Update Statements for Changes
            for (int curRow = 0; curRow < this.inAudience.Rows.Count; curRow++)
            {

                if (!opts.isSilent() && curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Audience Add Scan " + curRow + " of " + this.inAudience.Rows.Count);
                }

                string A_New = this.inAudience.Rows[curRow]["New"].ToString();
                string A_Update = this.inAudience.Rows[curRow]["Update"].ToString();
                string A_Code = this.inAudience.Rows[curRow]["Code"].ToString();
                string A_Name = this.inAudience.Rows[curRow]["Name"].ToString();
                string A_ParentCode = this.inAudience.Rows[curRow]["ParentCode"].ToString();
                string A_ID = this.inAudience.Rows[curRow]["UpdateID"].ToString(); // Existing AudienceID  
                string OldName = this.inAudience.Rows[curRow]["OldName"].ToString(); // Existing Name  
                string OldCode = this.inAudience.Rows[curRow]["OldCode"].ToString(); // Existing Code  
                string OldParentID = this.inAudience.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
                //string update_s = "";

                if (A_New.Equals("1") ){

                    string insert_s =  " insert into Audience ( Name, CustomerID, ParentAudienceID, \n" +
                                " InternalCode, ModifiedBy, CreatedBy, \n" +
                                " ModifiedOn, CreatedOn , SortOrder )\n" +
                                " values ( '" + A_Name + "', " + this.customerID + ", 0 , \n"+
                                " '" + A_Code + "','" + this.programName + "','"   + this.programName + "', \n" +
                                " getdate(), getdate(), 0 )\n\n";


                        // Insert into the DB if requested.    
                        if (opts.isExecute())
                        {

                            try
                            {
                                SqlCommand myCommand = new SqlCommand(insert_s, this.db);
                                int rowcount = myCommand.ExecuteNonQuery();
                                if (rowcount != 1)
                                {
                                    this.inAudience.Rows[curRow]["ErrorFg"] = "1";
                                    this.inAudience.Rows[curRow]["Error"] = this.inAudience.Rows[curRow]["Error"].ToString() + ", Insert Failed with rc:" + rowcount;
                                    insert_s = "FAILED:" + insert_s;
                                }
                            }
                            catch (Exception )
                            {
                                this.inAudience.Rows[curRow]["ErrorFg"] = "1";
                                this.inAudience.Rows[curRow]["Error"] = this.inAudience.Rows[curRow]["Error"].ToString() + ", Insert Failed.";
                                insert_s = "FAILED:" + insert_s;
                            }
                        } // End db execute
                        insertSQL += insert_s;

                        undoInsertSQL += " delete Audience where InternalCode = '" + A_Code + "' \n" +
                        " and CustomerID = " + this.customerID + " and Name = '" + A_Name + "'\n\n";
                    
                    } // End if new

                // Update ParentAudienceID after the 1st update, after the inserts, after the deletes.
                // This prevents updating to the incorrect AudienceID if there are duplicate Codes before
                // all other actions have taken place.

                
                } // End New/Insert Processing loop



            Utility.SaveToFile(this.baseDirName + "u2_Audience_InsertSQL.txt", insertSQL +
             "\n ----------------------------------------------- \n  Undo" +
             "\n ----------------------------------------------- \n " + undoInsertSQL);

            } // End executeAdd()
    }
}