﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
namespace StudentImport
{


    // --------------------------------------------------------
    // Loader for Location Records
    //  1. 
    // --------------------------------------------------------
    public class LocationLoader
    {


        // Main Inbound Location information to be loaded
        public DataTable inLocations = null;
        String customerID = "94";
        ArrayList errorMessages = null;
        SqlConnection db = null;
        DataTable oldLocations = null;
        string baseDirName = "";
        string programName = "Import";
        Queue jobQueue = null; // Interface with the Job Queue if used.
        Options opts = null;

        // NOT USED YET. Silent mode ( no console output )
        Boolean silent = false;

        // NOT USED YET. Noop mode ( do not make updates. Just report and produce logs/files )
        Boolean noop = false;

        // Ignore stat checks
        Boolean ignoreWarnings = false; // NOT USED YET. Turn this on to stop reasonability checks on changes. (If there are a lot of changes )


        // Standard Stats
        StatInfo stats = null;


        // -------------------------------------------------------------
        // Constructor 
        // Parameter dt is the inbound file data loaded into a DataTable
        // -------------------------------------------------------------
        public LocationLoader(SqlConnection dbCon, Options optvalues, Queue q, DataTable dt )
        {

            this.opts = optvalues;

            this.inLocations = dt;
            this.customerID = this.opts.getCustomerID();
            this.db = dbCon;
            this.errorMessages = new ArrayList();
            this.baseDirName = this.opts.getBaseDirectory();
            this.stats = new StatInfo();
            this.jobQueue = q;
    
            
        }

        // --------------------------------------------------------
        // Silent mode means there is no console output
        // * TODO * Put in another way to output errors
        //  ? ErrorList attribute ?
        // Parameters:
        //  isSilent = true  ( no output )
        //          = false  ( output )
        // ----------------------------------------------------------
        public void setSilentOutput(Boolean isSilent)
        {
            this.silent = isSilent;
        }

                // --------------------------------------------------------
        // Silent mode means there is no console output
        // * TODO * Put in another way to output errors
        //  ? ErrorList attribute ?
        // Parameters:
        //  isSilent = true  ( no output )
        //          = false  ( output )
        // ----------------------------------------------------------
        public void setNoop(Boolean isNoop)
        {
            this.noop = isNoop;
        }

        // ----------------------------------------------
        // Return the existing Location records.
        // ----------------------------------------------
        public DataTable getExistingRecords()
        {
            return this.oldLocations;
        }

        // ----------------------------------------------
        // Return the inbound/modified JobRole records.
        // ----------------------------------------------
        public DataTable getRecords()
        {
            return this.inLocations;
        }

        // ----------------------------------------------
        // Return the process statistics
        // ----------------------------------------------
        public StatInfo getStats()
        {
            return this.stats;
        }


         // -----------------------------------------------------------
        // sanitizeColumns
        // brief: Renames the column headers
        // to make sure column names are consistent.
        // 1. Change to consistent Case.
        // 2. Trims
        // 3. Adds in some processing columns
        // * This is the place to catch common misspellings if needed
        // (example:  name turns into Name, NAME turns into Name )
        // -----------------------------------------------------------
        public int sanitizeColumns()
        {
            for (int curCol = 0; curCol < inLocations.Columns.Count; curCol++)
            {


                String colName = inLocations.Columns[curCol].ColumnName.ToLower().Trim();
                Console.WriteLine("COLNAME: " + colName);
                if (colName.Equals("name"))
                {
                    inLocations.Columns[curCol].ColumnName = "Name";
                }
                if (colName.Equals("code"))
                {
                    inLocations.Columns[curCol].ColumnName = "Code";
                }
                if (colName.Equals("parentcode") || colName.Equals("pcode") ||
                    colName.Equals("parcode") || colName.Equals("perentcode") || colName.ToLower().Equals("parent code") )
                {
                    inLocations.Columns[curCol].ColumnName = "ParentCode";
                }
                if (colName.Equals("parentcode") || colName.Equals("pcode") ||
    colName.Equals("parcode") || colName.Equals("perentcode") || colName.Equals("Parent Code"))
                {
                    inLocations.Columns[curCol].ColumnName = "ParentCode";
                }


                if (colName.Equals("Telephone Number"))
                {
                    inLocations.Columns[curCol].ColumnName = "TelephoneNbr";
                }


                if (colName.Equals("Address"))
                {
                    inLocations.Columns[curCol].ColumnName = "Address1";
                }

            }



            // Check for other columns and add if they are not present
            if (!inLocations.Columns.Contains("Address1"))
            {
                this.inLocations = Utility.addColumnToDataTable(this.inLocations, "Address1", "System.String");
            }
            if (!inLocations.Columns.Contains("Address2"))
            {
                this.inLocations = Utility.addColumnToDataTable(this.inLocations, "Address2", "System.String");
            }
            if (!inLocations.Columns.Contains("City"))
            {
                this.inLocations = Utility.addColumnToDataTable(this.inLocations, "City", "System.String");
            }
            if (!inLocations.Columns.Contains("State"))
            {
                this.inLocations = Utility.addColumnToDataTable(this.inLocations, "State", "System.String");
            }
            if (!inLocations.Columns.Contains("PostalCode"))
            {
                this.inLocations = Utility.addColumnToDataTable(this.inLocations, "PostalCode", "System.String");
            }
            if (!inLocations.Columns.Contains("TelephoneNbr"))
            {
                this.inLocations = Utility.addColumnToDataTable(this.inLocations, "TelephoneNbr", "System.String");
            }



            // Add isNew flag to dataTable
            // This will be used to mark which ones need to be inserted
            // ( These need to be inserted before the updates )

            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "New", "System.String");
            // Add update flag to dataTable
            // This will be used to mark which ones need to be updated in the db
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "Update", "System.String");
            // Add update comment to dataTable
            // This will be used add notes or comments
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "UpdateMsg", "System.String");
            // Add existingID to dataTable
            // This will be used to mark what existing record got modified
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "UpdateID", "System.String");
            // Add Existing Name, Code, ParentLocationID to dataTable
            // This will be used to see what was updated or not updated
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "OldName", "System.String");
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "OldCode", "System.String");
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "OldParentID", "System.String");
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "OldParentCode", "System.String");
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "OldAddress1", "System.String");
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "OldAddress2", "System.String");
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "OldCity", "System.String");
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "OldState", "System.String");
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "OldPostalCode", "System.String");
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "OldTelephoneNbr", "System.String");

            // Add Error to dataTable
            // This will be used to note error messages
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "ErrorFg", "System.String");
            this.inLocations = Utility.addColumnToDataTable(this.inLocations, "Error", "System.String");
 


            return 0; // ok
        }

        // -----------------------------------------------------------
        // validateColumns
        // brief: Checks to make sure column information is available
        // return -1 if no Name
        //        -2 if no Code
        //        -3 if no ParentCode
        // -----------------------------------------------------------
        public int validateColumns()
        {

            bool errorFg = false;
            string errorMsg = "";

            if (!inLocations.Columns.Contains("Name"))
            {
                if (!this.silent)
                {
                    Console.WriteLine("LocationLoader.validateColumns : Name column not found. Fail. ");
                }
                errorMsg += " 'Name' column not found.";
                errorFg = true;
                    
       
            }
            if (!inLocations.Columns.Contains("Code"))
            {
                if (!this.silent)
                {
                    Console.WriteLine("LocationLoader.validateColumns : Code column not found. Fail. ");

                }
                errorMsg += " 'Code' column not found.";
                errorFg = true;


            }
            if (!inLocations.Columns.Contains("ParentCode"))
            {
                if (!this.silent)
                {
                    Console.WriteLine("LocationLoader.validateColumns : ParentCode column not found. Fail. ");
   
                }
                errorMsg += " 'ParentCode' column not found.";
                errorFg = true;

         
            }
            if (errorFg)
            {
                this.jobQueue.failJob(opts.getJobID(), "Location column validation failure: " + errorMsg);
                return -1;
            }


            return 0; // ok
        }

        // ---------------------------------------------
        // load the Location information for the customer
        // from the database.
        // ---------------------------------------------
        public int loadLocationFromDB()
        {
            String query = " select ID = L.ID, Name = L.Name, InternalCode = L.InternalCode, \n" +
                        " ParentLocationID = L.ParentLocationID, L.CustomerID, \n" +
                        " ParentCode =  case when P.InternalCode is null then '*none*' else P.InternalCode end, \n" + 
                        " L.ModifiedBy, L.CreatedBy, L.ModifiedOn, L.CreatedOn, \n" +
                        " L.Address1, L.Address2, L.City, L.State, L.PostalCode,L.TelephoneNbr \n" +
                        " from Location  L \n" +
                        " left outer join Location as P on P.ID = L.ParentLocationID  \n" +
                        " where L.CUstomerID = " + this.customerID;
            this.oldLocations = new DataTable();

            // Get the Data
            try
            {
                if (!this.silent)
                {
                    Console.WriteLine("Location Get Data: " + query);
                }
                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!this.silent)
                {
                    Console.WriteLine("Location DB Data Loaded.");
                }
                // Load into DataTable
                this.oldLocations.Load(myReader);

                // Add some processing columns
                this.oldLocations = Utility.addColumnToDataTable(this.oldLocations, "Delete", "System.String");
                this.oldLocations = Utility.addColumnToDataTable(this.oldLocations, "Update", "System.String");


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }

            //Default some values
            for (int curRow = 0; curRow < this.oldLocations.Rows.Count; curRow++)
            {
               this.oldLocations.Rows[curRow]["Delete"] = "Yes";
            }

            return 0; // ok
        }



        // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int processChanges()
        {

            // Pre Process Changes to determine what to do. 
            // Execution of changes will occur next.
            // For each row
            //  1. find it in the existing db rows
            //  2.  Mark if it is new.
            //  3.  Error if it matches to more than one row.
            //  4.  Mark as update if it needs to change.

            this.stats.inbound_count = this.inLocations.Rows.Count;
            this.stats.existing_count = this.oldLocations.Rows.Count;
            int found_count = 0;

            for (int curRow = 0; curRow < this.inLocations.Rows.Count; curRow++)
            {


                // ---------------------------------------------------------------
                // Custom Processing here:
                // Replace with custom rule logic
                // Load Rules:  Pad Zero Rule :  6 , Column Code
                if (this.customerID.Equals("80"))
                {

                    int padlen = 6;
                    string colvalue = this.inLocations.Rows[curRow]["Code"].ToString().Trim();
                    
                    int len = colvalue.Length;

                    //Pad only if all the characters are numeric
                    double Num;
                    bool isNum = double.TryParse(colvalue, out Num);

                    while (isNum && len < padlen)
                    {
                        colvalue = "0" + colvalue;
                        len++;
                    }
                    this.inLocations.Rows[curRow]["Code"] = colvalue;
                    

                    // Set Blank ParentLocationCOde to *none*
                    if (this.inLocations.Rows[curRow]["ParentCode"].ToString().Length < 1)
                    {
                        this.inLocations.Rows[curRow]["ParentCode"] = "*none*";
                    }
                }


                // These are the new values on the file
                string Loc_Code = this.inLocations.Rows[curRow]["Code"].ToString();
                string Loc_Name = this.inLocations.Rows[curRow]["Name"].ToString();
                string Loc_ParentCode = this.inLocations.Rows[curRow]["ParentCode"].ToString();
                string Loc_Address1 = this.inLocations.Rows[curRow]["Address1"].ToString();
                string Loc_Address2 = this.inLocations.Rows[curRow]["Address2"].ToString();
                string Loc_City = this.inLocations.Rows[curRow]["City"].ToString();
                string Loc_State = this.inLocations.Rows[curRow]["State"].ToString();
                string Loc_PostalCode = this.inLocations.Rows[curRow]["PostalCode"].ToString();
                string Loc_TelephoneNbr = this.inLocations.Rows[curRow]["TelephoneNbr"].ToString();
               
                // Default Some of the Metadata. 
                this.inLocations.Rows[curRow]["UpdateID"] = "";
                this.inLocations.Rows[curRow]["OldCode"] = "";
                this.inLocations.Rows[curRow]["OldName"] = "";
                this.inLocations.Rows[curRow]["OldParentID"] = "";
                this.inLocations.Rows[curRow]["OldParentCode"] = "";

                // Parent Code cannot point to same location
                if (Loc_ParentCode.CompareTo(Loc_Code) == 0)
                {
                    Loc_ParentCode = "0";
                    this.inLocations.Rows[curRow]["ParentCode"] = "0";
                }



                // Find existing Row using Code value ... if any
                DataRow[] foundRows;
                foundRows = this.oldLocations.Select("InternalCode = '" + Loc_Code.Replace("'", "''") + "'");
                if (foundRows.Length < 1)
                {

                    if (Loc_Code.Trim().ToString().Length < 1 || Loc_Name.Trim().ToString().Length < 1)
                    { 
                        // Blank Row Error.
                        this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                        this.inLocations.Rows[curRow]["Error"] = "Name or Code cannot be blank.";
                        this.inLocations.Rows[curRow]["New"] = "0";
                        this.inLocations.Rows[curRow]["Update"] = "0";
                        this.stats.error_count++;
                        continue;
                    }
                    // Check for duplicates
                    DataRow[] dupRows;
                    dupRows = this.inLocations.Select("Code = '" + Loc_Code.Replace("'", "''") + "' and New = 1 ");
                    if (dupRows.Length > 0)
                    {
                        this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                        this.inLocations.Rows[curRow]["Error"] = "Duplicate Locations on Inbound File.(" + dupRows.Length + ")";
                        this.stats.error_count++;
                        Console.WriteLine(" found Dup:" + Loc_Code);
                        continue;
                    }
                    // Not Found. Mark as New
                    this.inLocations.Rows[curRow]["New"] = "1";
                    this.inLocations.Rows[curRow]["Update"] = "1"; // Required for parent field update
                    this.stats.insert_count++;
                }
                else if (foundRows.Length > 1)
                {
                    // Too many Matches. Error.
                    this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                    this.inLocations.Rows[curRow]["Error"] = "Location found too many existing record matches on InternalID.(" + foundRows.Length + ")";
                    this.inLocations.Rows[curRow]["New"] = "0";
                    for (int fr = 0; fr < foundRows.Length; fr++)
                    {
                        // Make sure you mark these rows as found so you do not delete them.
                        this.stats.error_count++;
                        foundRows[fr]["Delete"] = "No";
                    }
                    continue;
                }
                else
                {
                    // Mark existing record as found.
                    foundRows[0]["Delete"] = "No";
                    found_count++;

                    // Check for duplicates
                    DataRow[] dupRows;
                    dupRows = this.inLocations.Select("Code = '" + Loc_Code.Replace("'", "''") + "'");
                    if (dupRows.Length > 1)
                    {
                        this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                        this.inLocations.Rows[curRow]["Error"] = "Duplicate Locations on Inbound File.(" + dupRows.Length + ")";
                        this.stats.error_count++;
                        Console.WriteLine(" found Dup:" + Loc_Code);
                        continue;
                    }
                    


                    // Update Might be needed. Compare the new values to the old values to make sure:

                    this.inLocations.Rows[curRow]["New"] = "0";
                    this.inLocations.Rows[curRow]["UpdateID"] = foundRows[0]["ID"].ToString();

                    string oldName = foundRows[0]["Name"].ToString();
                    string oldCode = foundRows[0]["InternalCode"].ToString();
                    string oldParentCode = foundRows[0]["ParentCode"].ToString();
                    string oldParentID = foundRows[0]["ParentLocationID"].ToString();
                    string oldAddress1 = foundRows[0]["Address1"].ToString();
                    string oldAddress2 = foundRows[0]["Address2"].ToString();
                    string oldCity = foundRows[0]["City"].ToString();
                    string oldState = foundRows[0]["State"].ToString();
                    string oldPostalCode = foundRows[0]["PostalCode"].ToString();
                    string oldTelephoneNbr = foundRows[0]["TelephoneNbr"].ToString();

                    // Track the existing values for undo logic and logging.
                    this.inLocations.Rows[curRow]["OldCode"] = oldCode;
                    this.inLocations.Rows[curRow]["OldName"] = oldName;
                    this.inLocations.Rows[curRow]["OldParentID"] = oldParentID;
                    this.inLocations.Rows[curRow]["OldParentCode"] = oldParentCode;
                    this.inLocations.Rows[curRow]["OldAddress1"] = oldAddress1;
                    this.inLocations.Rows[curRow]["OldAddress2"] = oldAddress2;
                    this.inLocations.Rows[curRow]["OldCity"] = oldCity;
                    this.inLocations.Rows[curRow]["OldState"] = oldState;
                    this.inLocations.Rows[curRow]["OldPostalCode"] = oldPostalCode;
                    this.inLocations.Rows[curRow]["OldTelephoneNbr"] = oldTelephoneNbr;


                    Boolean hasChanged = false;
                    if (!oldName.Trim().Equals(Loc_Name.Trim()))
                    {
                        this.inLocations.Rows[curRow]["Update"] = "1";
                        this.inLocations.Rows[curRow]["UpdateMsg"] = "Name !=" + oldName + ";";
                        hasChanged = true;
                    }
                    if (!oldCode.Trim().Equals(Loc_Code.Trim()))
                    {
                        this.inLocations.Rows[curRow]["Update"] = "1";
                        this.inLocations.Rows[curRow]["UpdateMsg"] =  this.inLocations.Rows[curRow]["UpdateMsg"] + "InternalCode !=-" + oldCode + ";";
                        hasChanged = true;
                    }
                    if (!oldAddress1.Trim().Equals(Loc_Address1.Trim()))
                    {
                        this.inLocations.Rows[curRow]["Update"] = "1";
                        this.inLocations.Rows[curRow]["UpdateMsg"] = this.inLocations.Rows[curRow]["UpdateMsg"] + "Address1 !=-" + oldAddress1 + ";";
                        hasChanged = true;
                    }
                    if (!oldAddress2.Trim().Equals(Loc_Address2.Trim()))
                    {
                        this.inLocations.Rows[curRow]["Update"] = "1";
                        this.inLocations.Rows[curRow]["UpdateMsg"] = this.inLocations.Rows[curRow]["UpdateMsg"] + "Address2 !=-" + oldAddress2 + ";";
                        hasChanged = true;
                    }
                    if (!oldCity.Trim().Equals(Loc_City.Trim()))
                    {
                        this.inLocations.Rows[curRow]["Update"] = "1";
                        this.inLocations.Rows[curRow]["UpdateMsg"] = this.inLocations.Rows[curRow]["UpdateMsg"] + "City !=-" + oldCity + ";";
                        hasChanged = true;
                    }
                    if (!oldState.Trim().Equals(Loc_State.Trim()))
                    {
                        this.inLocations.Rows[curRow]["Update"] = "1";
                        this.inLocations.Rows[curRow]["UpdateMsg"] = this.inLocations.Rows[curRow]["UpdateMsg"] + "State !=-" + oldState + ";";
                        hasChanged = true;
                    }
                    if (!oldPostalCode.Trim().Equals(Loc_PostalCode.Trim()))
                    {
                        this.inLocations.Rows[curRow]["Update"] = "1";
                        this.inLocations.Rows[curRow]["UpdateMsg"] = this.inLocations.Rows[curRow]["UpdateMsg"] + "PostalCode !=-" + oldPostalCode + ";";
                        hasChanged = true;
                    }
                    if (!oldTelephoneNbr.Trim().Equals(Loc_TelephoneNbr.Trim()))
                    {
                        this.inLocations.Rows[curRow]["Update"] = "1";
                        this.inLocations.Rows[curRow]["UpdateMsg"] = this.inLocations.Rows[curRow]["UpdateMsg"] + "Telephone !=-" + oldTelephoneNbr + ";";
                        hasChanged = true;
                    }

                     // Zero? Zero as a Code or Zero as no Parent? 
                    // ToDO: check for a 0 Job Code

                    if (oldParentCode.Trim().Equals("*none*") && Loc_ParentCode.Length < 1)
                    {
                        // No Parents. Same. No updated.
                    }
                    else if (oldParentCode.Trim().Equals("*none*")  && Loc_ParentCode.Equals("0"))
                    {
                        // No Parents. Same. No updated.
                    }else if (!oldParentCode.Trim().Equals(Loc_ParentCode.Trim()))
                    {
                        // Different Parents. Update
                        this.inLocations.Rows[curRow]["Update"] = "1";
                        this.inLocations.Rows[curRow]["UpdateMsg"] = this.inLocations.Rows[curRow]["UpdateMsg"] + "ParentCode !=-" + oldParentCode + ";";
                        hasChanged = true;
                    }

                    if (hasChanged)
                    {
                        this.stats.update_count++;
                    }

                }



          
            }

            this.stats.delete_count = this.stats.existing_count - found_count;
            if (this.stats.delete_count < 0)
            {
                this.stats.delete_count = 0; //Duplicates would cause this to be -1
            }


       

            return 0; //ok
        }

         // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int executeChanges()
        {

            // Insert
            this.executeAdds();

            // Update Main fields
            this.executePrimaryUpdates();

            // Update lookup fields
            this.executeSecondaryUpdates();

            
            // Generate Delete Statements for Removals
            string deleteSQL = "";
            string undoDeleteSQL = "";
            for (int curRow = 0; curRow < this.oldLocations.Rows.Count; curRow++)
            {
                if (this.oldLocations.Rows[curRow]["Delete"].ToString().Equals("No"))
                {
                    continue;
                }
                string OldLoc_ID = this.oldLocations.Rows[curRow]["ID"].ToString();
                string OldLoc_Name = this.oldLocations.Rows[curRow]["Name"].ToString();
                string OldLoc_InternalCode = this.oldLocations.Rows[curRow]["InternalCode"].ToString();
                string OldLoc_ParentLocationID = this.oldLocations.Rows[curRow]["ParentLocationID"].ToString();
                string OldLoc_ModifiedBy = this.oldLocations.Rows[curRow]["ModifiedBy"].ToString();
                string OldLoc_CreatedBy = this.oldLocations.Rows[curRow]["CreatedBy"].ToString();
                string OldLoc_ModifiedOn = this.oldLocations.Rows[curRow]["ModifiedOn"].ToString();
                string OldLoc_CreatedOn = this.oldLocations.Rows[curRow]["CreatedOn"].ToString();

                string delete_s = " delete Location where CustomerID = " + this.customerID + " and ID = " + OldLoc_ID + "\n";


                //  Delete the record in the DB if requested.    
                if (opts.isExecute() && opts.isDelete() )
                {

                    try
                    {
                        SqlCommand myCommand = new SqlCommand(delete_s, this.db);
                        int rowcount = myCommand.ExecuteNonQuery();
                        if (rowcount != 1)
                        {
                            this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                            this.inLocations.Rows[curRow]["Error"] = this.inLocations.Rows[curRow]["Error"].ToString() + ", Delete Failed with rc:" + rowcount;
                            delete_s = "FAILED:" + delete_s;
                        }
                    }
                    catch (Exception )
                    {
                        this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                        this.inLocations.Rows[curRow]["Error"] = this.inLocations.Rows[curRow]["Error"].ToString() + ", Delete  Failed.";
                        delete_s = "FAILED:" + delete_s;
                        
                    }
                } // end  db secondary update after insert

                deleteSQL += delete_s; // save sql scripts

                undoDeleteSQL +=  " insert into Location ( Name, CustomerID, ParentLocationID, \n" +
                                " InternalCode, ModifiedBy, CreatedBy, \n" +
                                " ModifiedOn, CreatedOn )\n" +
                                " values ( '" + OldLoc_Name + "', " + this.customerID + ", " + OldLoc_ParentLocationID + " , \n"+
                                " '" + OldLoc_InternalCode + "','" + OldLoc_ModifiedBy + "','"   + OldLoc_CreatedBy + "', \n" +
                                " '" + OldLoc_ModifiedOn + "', '" +OldLoc_CreatedOn + "')\n";

            }

            // NEW FILE
            string fileFilter = " New = '1' ";
            string[] colNames = new string[9] { "Name", "Code", "ParentCode","Address1", "Address2", "City","State","PostalCode","TelephoneNbr" };
            Utility.SaveFilteredToCSV(this.inLocations, this.baseDirName + "z1_Ref_LocationNew.csv", fileFilter, colNames);

            // UPDATE FILE
            fileFilter = " Update = '1' ";
            string[] colNames2 = new string[9] { "Name", "Code", "ParentCode", "Address1", "Address2", "City", "State", "PostalCode", "TelephoneNbr" };
            Utility.SaveFilteredToCSV(this.inLocations, this.baseDirName + "z2_Ref_LocationUpdate.csv", fileFilter, colNames2);


            // UNDO DELETE FILE: Generate an import file to reload the deleted JobRoles if needed.
            fileFilter = " Delete = 'Yes' ";
            string[] colNames3 = new string[15] { "ID", "Name", "InternalCode", "ParentCode", "ParentLocationID","Address1", "Address2", "City", "State", "PostalCode", "TelephoneNbr", "ModifiedBy", "ModifiedOn", "CreatedBy", "CreatedOn" };
            Utility.SaveFilteredToCSV(this.oldLocations, this.baseDirName + "z3_Undo_LocationDelete.csv", fileFilter, colNames3);

            Utility.SaveToFile(this.baseDirName + "u3_Loc_DeleteSQL.txt", deleteSQL +
             "\n ----------------------------------------------- \n  Undo" +
            "\n ----------------------------------------------- \n " + undoDeleteSQL);

            
            return 0; //ok

        }

        // ------------------------------------------------------------------------
        // Update the Location's primary fields. 
        // ------------------------------------------------------------------------
        private void executePrimaryUpdates()
        {
       

            string update1SQL = "";
            string undoUpdate1SQL = "";

            
            // Generate Update Statements for Changes
            for (int curRow = 0; curRow < this.inLocations.Rows.Count; curRow++)
            {

                if (curRow > 0 &&curRow % 50 == 0)
                {
                    Console.WriteLine("Location Primary field update " + curRow + " of " + this.inLocations.Rows.Count);
                }

                string Loc_New = this.inLocations.Rows[curRow]["New"].ToString();
                string Loc_Update = this.inLocations.Rows[curRow]["Update"].ToString();
                string Loc_ErrorFg = this.inLocations.Rows[curRow]["ErrorFg"].ToString();
                string Loc_Code = this.inLocations.Rows[curRow]["Code"].ToString().Replace("'", "''");
                string Loc_Name = this.inLocations.Rows[curRow]["Name"].ToString().Replace("'", "''");
                string Loc_Address1 = this.inLocations.Rows[curRow]["Address1"].ToString().Replace("'", "''");
                string Loc_Address2 = this.inLocations.Rows[curRow]["Address2"].ToString().Replace("'", "''");
                string Loc_City = this.inLocations.Rows[curRow]["City"].ToString().Replace("'", "''");
                string Loc_State = this.inLocations.Rows[curRow]["State"].ToString();
                string Loc_PostalCode = this.inLocations.Rows[curRow]["PostalCode"].ToString();
                string Loc_TelephoneNbr = this.inLocations.Rows[curRow]["TelephoneNbr"].ToString();
                string Loc_ParentCode = this.inLocations.Rows[curRow]["ParentCode"].ToString().Replace("'", "''"); 


                string Loc_ID = this.inLocations.Rows[curRow]["UpdateID"].ToString(); // Existing LocationID  
                string OldName = this.inLocations.Rows[curRow]["OldName"].ToString().Replace("'", "''"); // Existing Name  
                string OldCode = this.inLocations.Rows[curRow]["OldCode"].ToString().Replace("'", "''"); ; // Existing Code  
                string OldParentID = this.inLocations.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
                string OldAddress1 = this.inLocations.Rows[curRow]["OldAddress1"].ToString().Replace("'", "''");
                string OldAddress2 = this.inLocations.Rows[curRow]["OldAddress1"].ToString().Replace("'", "''");
                string OldCity = this.inLocations.Rows[curRow]["OldAddress1"].ToString();
                string OldState = this.inLocations.Rows[curRow]["OldAddress1"].ToString();
                string OldPostalCode = this.inLocations.Rows[curRow]["OldAddress1"].ToString();
                string OldTelephoneNbr = this.inLocations.Rows[curRow]["OldAddress1"].ToString();
                string update_s = "";

                if (Loc_Update.Equals("1") && !Loc_ErrorFg.Equals("1") && !Loc_New.Equals("1"))
                {

                    update_s = "Update Location set Name = '" + Loc_Name.Trim() + "', InternalCode = '" + Loc_Code + "',\n" +
                            " ModifiedBy = '" + this.programName + "', ModifiedOn = getdate(), \n" +
                            " Address1 = '" + Loc_Address1 + "', Address2 = '" + Loc_Address2 + "', \n" +
                            " City = '" + Loc_City + "', State = '" + Loc_State + "', \n" +
                            " PostalCode = '" + Loc_PostalCode + "', TelephoneNbr = '" + Loc_TelephoneNbr + "' \n" +
                            " where ID = " + Loc_ID + " and CustomerID = " + this.customerID + " \n\n";

                    //  Update  the DB if requested.    
                    if (opts.isExecute())
                    {

                        try
                        {
                            SqlCommand myCommand = new SqlCommand(update_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount != 1)
                            {
                                this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                                this.inLocations.Rows[curRow]["Error"] = this.inLocations.Rows[curRow]["Error"].ToString() + ",  Update Failed with rc:" + rowcount;
                                update_s = "FAILED:" + update_s;
                            }
                        }
                        catch (Exception)
                        {
                            this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                            this.inLocations.Rows[curRow]["Error"] = this.inLocations.Rows[curRow]["Error"].ToString() + ",  Update Failed.";
                            update_s = "FAILED:" + update_s;
                        }
                    } // end db update

                    update1SQL += update_s; // Saving to the log scripts

                    undoUpdate1SQL += "Update Location set Name = '" + OldName.Trim() + "', InternalCode = '" + OldCode + "',\n" +
                                    " ModifiedBy = ModifiedBy + ' rollback ', \n" +
                                    " Address1 = '" + OldAddress1 + "', Address2 = '" + OldAddress2 + "', \n" +
                                    " City = '" + OldCity + "', State = '" + OldState + "', \n" +
                                    " PostalCode = '" + OldPostalCode + "', TelephoneNbr = '" + OldTelephoneNbr + "', \n" +
                                    " where ID = " + Loc_ID + " and CustomerID = " + this.customerID + " \n\n";

                    // Update ParentLocationID after the 1st update, after the inserts, after the deletes.
                    // This prevents updating to the incorrect LocationID if there are duplicate Codes before
                    // all other actions have taken place.

                } // End if update
            }// End loop

            Utility.SaveToFile(this.baseDirName + "u1_Loc_Update1SQL.txt", update1SQL +
                "\n ----------------------------------------------- \n  Undo" +
                "\n ----------------------------------------------- \n " + undoUpdate1SQL);

        }
        
        
        // ------------------------------------------------------------------------
        // Update the Location's lookup fields. 
        // ------------------------------------------------------------------------
        private void executeSecondaryUpdates()
        {
            
       
            string update2SQL = "";
            string undoUpdate2SQL = "";

            Console.WriteLine("Location Lookup field updates.");
            // Generate Update Statements for Changes
            for (int curRow = 0; curRow < this.inLocations.Rows.Count; curRow++)
            {

                if (curRow > 0 && curRow % 50 == 0)
                {
                    Console.WriteLine("Location Lookup field update " + curRow + " of " + this.inLocations.Rows.Count);
                }


                string Loc_New = this.inLocations.Rows[curRow]["New"].ToString();
                string Loc_Update = this.inLocations.Rows[curRow]["Update"].ToString();
                string Loc_ErrorFg = this.inLocations.Rows[curRow]["ErrorFg"].ToString();
                string Loc_Code = this.inLocations.Rows[curRow]["Code"].ToString();
                string Loc_ParentCode = this.inLocations.Rows[curRow]["ParentCode"].ToString();

                string Loc_ID = this.inLocations.Rows[curRow]["UpdateID"].ToString(); // Existing LocationID  
                string OldName = this.inLocations.Rows[curRow]["OldName"].ToString(); // Existing Name  
                string OldCode = this.inLocations.Rows[curRow]["OldCode"].ToString(); // Existing Code  
                string OldParentID = this.inLocations.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
   
                string update_s = "";

                //Console.WriteLine(Loc_New + "-" + Loc_Update + "-" + Loc_ErrorFg + "-" + Loc_Code + "-" + Loc_ParentCode);
                if ((Loc_New.Equals("1") || Loc_Update.Equals("1")) && !Loc_ErrorFg.Equals("1"))
                {

                    //Console.WriteLine("Update.");
                    if (Loc_ParentCode.Length > 0 && !Loc_ParentCode.Equals("*none*") )
                    {
                        if (Loc_New.Equals("1"))
                        {
                            update_s = "Update Location set Location.ParentLocationID = \n" +
                                    "   (select ID from Location L where L.CustomerID = " + this.customerID + "\n" +
                                    "       and L.InternalCode = '" + Loc_ParentCode + "' )\n " +
                                 " where InternalCode = '" + Loc_Code + "' and CustomerID = " + this.customerID + "\n\n";
                        }
                        else
                        { 
                            update_s = "Update Location set Location.ParentLocationID = \n" +
                                    "   (select ID from Location L where L.CustomerID = " + this.customerID + "\n" +
                                    "       and L.InternalCode = '" + Loc_ParentCode + "' )\n " +
                                 " where ID = " + Loc_ID + " and CustomerID = " + this.customerID + "\n\n";
                        }
                    }
                    else
                    {
                        update_s =  "Update Location set Location.ParentLocationID = 0 " +
                      " where InternalCOde = '" + Loc_Code + "' and CustomerID = " + this.customerID + "\n\n";
                    }


                    //  Secondary Update the DB if requested.    
                    if (opts.isExecute())
                    {
                        //Console.WriteLine("Execute.");
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(update_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount != 1)
                            {
                                this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                                this.inLocations.Rows[curRow]["Error"] = this.inLocations.Rows[curRow]["Error"].ToString() + ", Parent Update Failed with rc:" + rowcount;
                                update_s = "FAILED:" + update_s;
                            }
                        }
                        catch (Exception )
                        {
                            this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                            this.inLocations.Rows[curRow]["Error"] = this.inLocations.Rows[curRow]["Error"].ToString() + ", Parent Update Failed.";
                            update_s = "FAILED:" + update_s;
                        }
                    } // end secondary db update

                    update2SQL += update_s; // save for logging the scripts

                        undoUpdate2SQL += "Update Location set ParentLocationID = " + OldParentID + ",\n" +
                                     " where ID = " + Loc_ID + " and CustomerID = " + this.customerID + " \n\n";


                } // End update check



            }// End main loop

            Utility.SaveToFile(this.baseDirName + "u4_Loc_Update2SQL.txt", update2SQL +
            "\n ----------------------------------------------- \n  Undo" +
            "\n ----------------------------------------------- \n " + undoUpdate2SQL);

        }

        // ------------------------------------------------------------------------
        // Insert the new Location
        // ------------------------------------------------------------------------
        private void executeAdds()
        {
            string insertSQL = "";
            string undoInsertSQL = "";

            // Generate Insert Statements for Changes
            for (int curRow = 0; curRow < this.inLocations.Rows.Count; curRow++)
            {

                if (curRow > 0 && curRow % 50 == 0)
                {
                    Console.WriteLine("Location Add " + curRow + " of " + this.inLocations.Rows.Count);
                }



                string Loc_New = this.inLocations.Rows[curRow]["New"].ToString();
                string Loc_Update = this.inLocations.Rows[curRow]["Update"].ToString();
                string Loc_ErrorFg = this.inLocations.Rows[curRow]["ErrorFg"].ToString();
                string Loc_Code = this.inLocations.Rows[curRow]["Code"].ToString();
                string Loc_Name = this.inLocations.Rows[curRow]["Name"].ToString().Replace("'", "''");
                string Loc_Address1 = this.inLocations.Rows[curRow]["Address1"].ToString().Replace("'", "''");
                string Loc_Address2 = this.inLocations.Rows[curRow]["Address2"].ToString().Replace("'", "''");
                string Loc_City = this.inLocations.Rows[curRow]["City"].ToString();
                string Loc_State = this.inLocations.Rows[curRow]["State"].ToString();
                string Loc_PostalCode = this.inLocations.Rows[curRow]["PostalCode"].ToString();
                string Loc_TelephoneNbr = this.inLocations.Rows[curRow]["TelephoneNbr"].ToString();
                string Loc_ParentCode = this.inLocations.Rows[curRow]["ParentCode"].ToString();


                string Loc_ID = this.inLocations.Rows[curRow]["UpdateID"].ToString(); // Existing LocationID  
                string OldName = this.inLocations.Rows[curRow]["OldName"].ToString(); // Existing Name  
                string OldCode = this.inLocations.Rows[curRow]["OldCode"].ToString(); // Existing Code  
                string OldParentID = this.inLocations.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
                string OldAddress1 = this.inLocations.Rows[curRow]["OldAddress1"].ToString();
                string OldAddress2 = this.inLocations.Rows[curRow]["OldAddress1"].ToString();
                string OldCity = this.inLocations.Rows[curRow]["OldAddress1"].ToString();
                string OldState = this.inLocations.Rows[curRow]["OldAddress1"].ToString();
                string OldPostalCode = this.inLocations.Rows[curRow]["OldAddress1"].ToString();
                string OldTelephoneNbr = this.inLocations.Rows[curRow]["OldAddress1"].ToString();
                string update_s = "";

                if (Loc_ParentCode.CompareTo(Loc_Code) == 0)
                {
                    // Parent Code cannot point to same location
                    Loc_ParentCode = "0";
                }

                 if (Loc_New.Equals("1") && !Loc_ErrorFg.Equals("1"))
                {

                    update_s = " insert into Location ( Name, CustomerID, ParentLocationID, \n" +
                                " InternalCode, ModifiedBy, CreatedBy, \n" +
                                " ModifiedOn, CreatedOn , Address1, Address2, City, State, PostalCode, TelephoneNbr)\n" +
                                " values ( '" + Loc_Name + "', " + this.customerID + ", 0 , \n" +
                                " '" + Loc_Code + "','" +  this.programName + "','" + this.programName + "', \n" +
                                " getdate(), getdate(), '" + Loc_Address1 + "','" + Loc_Address2 + "',\n" +
                                " '" + Loc_City + "','" + Loc_State + "','" + Loc_PostalCode + "','" + Loc_TelephoneNbr + "')\n\n";

                    //  Insert into the DB if requested.    
                    if (opts.isExecute())
                    {

                        try
                        {
                            SqlCommand myCommand = new SqlCommand(update_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount != 1)
                            {
                                this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                                this.inLocations.Rows[curRow]["Error"] = this.inLocations.Rows[curRow]["Error"].ToString() + ", Insert Failed with rc:" + rowcount;
                                update_s = "FAILED:" + update_s;
                            }
                        }
                        catch (Exception)
                        {
                            this.inLocations.Rows[curRow]["ErrorFg"] = "1";
                            this.inLocations.Rows[curRow]["Error"] = this.inLocations.Rows[curRow]["Error"].ToString() + ", Insert Failed.";
                            update_s = "FAILED:" + update_s;
                        }
                    } // end  db insert

                    insertSQL += update_s; // save for logging the sql scripts.

                    // Update ParentLocationID after the 1st update, after the inserts, after the deletes.
                    // This prevents updating to the incorrect LocationID if there are duplicate Codes before
                    // all other actions have taken place.

                      undoInsertSQL += " delete Location where InternalCode = '" + Loc_Code + "' \n" +
                      " and CustomerID = " + this.customerID + " and Name = '" + Loc_Name + "'\n\n";

                 }// End if new

            
            } //End main loop

            Utility.SaveToFile(this.baseDirName + "u2_Loc_InsertSQL.txt", insertSQL +
             "\n ----------------------------------------------- \n  Undo" +
             "\n ----------------------------------------------- \n " + undoInsertSQL);
        } // End Add method
           
        

    }
}