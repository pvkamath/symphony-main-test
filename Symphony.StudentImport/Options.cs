﻿using System;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Collections;
namespace StudentImport
{
    public class Options
    {

        private string customerID = "";
        private string customerShortName = "";
        private string applicationID = "";
        private string userRoleID = "";
        private string supRoleID = "";
        private string reportSupRoleID = "";
        private string environ = "Dev";
        private string conString = "";//user id=Cdwizard;password=w1z4rd;server=192.168.4.170;Trusted_Connection=no;database=CustomerBlackBox;connection timeout=30";
        
        private string baseDir = "";
        private string empFileName = "";
        private string jobFileName = "";
        private string locFileName = "";
        private string audFileName = "";
        private string orderFileName = "";

        private bool execute = false;
        private bool delete = false;
        private bool silent = false; // Verbose?
        private bool sessions = false;

        private string username = "";

        private string jobID = "";
        private string jobType = ""; //1 = Emp, 2 = Job, 3 = Loc, 4 = Audience

         private Hashtable ruleValues = null;
        private OrderLoader orderTable;

         public Options()
        {
            ruleValues = new Hashtable();
        }

        public string EmployeeFileName
        {
            get { return empFileName; }
            set { empFileName = value; }
        }

        public string LocationFileName
        {
            get { return locFileName; }
            set { locFileName = value; }

        }

        public string JobFileName
        {
            get { return jobFileName; }
            set { jobFileName = value; }

        }

        public string OrderFileName
        {
            get { return orderFileName; }
            set { orderFileName = value; }
        }

        public string CustomerID
        {
            get { return customerID; }
            set { customerID = value; }

        }

        // Base Directory Property
        public string BaseDirectory
        {
            get { return baseDir; }
            set { baseDir = value; }
        }
        
        public bool SetExecute
        {
            get { return execute; }
            set { execute = value; }
        }

        public string JobID
        {
            get { return jobID; }
            set { jobID = value; }
        }

        public string JobType
        {
            get { return jobType; }
            set { jobType = value; }
        }
        
        public string ConnectionString
        {
            get { return conString; }
            set { conString = value; }

        }

        public OrderLoader OrderTable
        {
            get { return orderTable; }
            set { orderTable = value; }

        }
        
    
        // Environment Property

        // Setters
        public void setEmployeeFileName(string filename)
        {
            this.empFileName = filename;
        }
        public void setLocationFileName(string filename)
        {
            this.locFileName = filename;
        }
        public void setJobFileName(string filename)
        {
            this.jobFileName = filename;
        }
        public void setAudienceFileName(string filename)
        {
            this.audFileName = filename;
        }
        public void setOrderFileName(string filename)
        {
            this.orderFileName = filename;
        }
        public void setCustomerID(string id)
        {
            this.customerID = id;
        }
        public void setBaseDirectory(string path)
        {
            this.baseDir = path;
        }
        public void setEnvironment(string env)
        {
            this.environ = env;
        }
        public void setConnectionString(string con)
        {
            this.conString = con;
        }
        public void setExecute(bool on)
        {
            this.execute = on;
        }
        public void setDelete(bool on)
        {
            this.delete = on;
        }
        public void setSilent(bool on)
        {
            this.silent = on;
        }
        public void setCustomerShortName(string name)
        {
            this.customerShortName = name;
        }
        public void setUserRoleID(string id)
        {
            this.userRoleID = id;
        }
        public void setSupervisorRoleID(string id)
        {
            this.supRoleID = id;
        }
        public void setReportingSupervisorRoleID(string id)
        {
            this.reportSupRoleID = id;
        }
        public void setApplicationID(string id)
        {
            this.applicationID = id;
        }
        public void setJobID(string id)
        {
            this.jobID = id;
        }
        public void setJobType(string id)
        {
            this.jobType = id;
        }
        public void setSessions(bool on)
        {
            this.sessions = on;
        }
        public void setUsername(string u)
        {
            this.username = u;
        }
        // Getters
        public string getEmployeeFileName()
        {
            return this.empFileName ;
        }
        public string getLocationFileName()
        {
            return this.locFileName;
        }
        public string getJobFileName()
        {
            return this.jobFileName;
        }
        public string getAudienceFileName()
        {
            return this.audFileName;
        }
        public string getOrderFileName()
        {
            return this.orderFileName;
        }
        public string getCustomerID()
        {
            return this.customerID;
        }
        public string getEnvironment()
        {
            return this.environ;
        }
        public string getConnectionString()
        {
            return this.conString;
        }
        public string getBaseDirectory()
        {
            return this.baseDir;
        }
        public string getCustomerShortName()
        {
            return this.customerShortName;
        }
        public string getUserRoleID()
        {
            return this.userRoleID;
        }
        public string getSupervisorRoleID()
        {
            return this.supRoleID;
        }
        public string getReportingSupervisorRoleID()
        {
            return this.reportSupRoleID;
        }
        public string getApplicationID()
        {
            return this.applicationID;
        }
        public string getJobID()
        {
            return this.jobID;
        }
        public string getJobType()
        {
            return this.jobType;
        }

        public bool isSilent()
        {
            return this.silent;
        }
        public bool isDelete()
        {
            return this.delete;
        }
        public bool isExecute()
        {
            return this.execute;
        }
        public bool isSessions()
        {
            return this.sessions;
        }
        public string getUsername()
        {
            return this.username;
        }
        public string getRuleValue(string ruleName)
        {
            if (!this.ruleValues.ContainsKey(ruleName))
            {

                return ""; // If not there, return blank
            }
            return this.ruleValues[ruleName].ToString();
        }

        // --------- Utilities
        public void addRuleValue(string ruleName, string value)
        {

            if (this.ruleValues.ContainsKey(ruleName))
            {
                this.ruleValues[ruleName] = value;
            }
            else
            {
                this.ruleValues.Add(ruleName, value);
            }
        }



        public string toString()
        {
            string outs = "-----------------------------------------" + Environment.NewLine  +
                "| Customer ID: " + this.customerID + Environment.NewLine +
                "| Username: " + this.username + Environment.NewLine +
                "| Customer Domain: " + this.customerShortName + Environment.NewLine +
                "| User Role ID: " + this.userRoleID + Environment.NewLine +
                "| Supervisor Role ID: " + this.supRoleID + Environment.NewLine +
                "| Environment: " + this.environ + Environment.NewLine +
                "| Base Dir: " + this.baseDir + Environment.NewLine +
                "| Employee File: " + this.empFileName + Environment.NewLine +
                 "| Job File: " + this.jobFileName + Environment.NewLine +
                 "| Loc File: " + this.locFileName + Environment.NewLine +
                 "| Execute?: " + this.execute + Environment.NewLine +
                 "| Silent?: " + this.silent + Environment.NewLine +
                 "| Sessions?: " + this.sessions + Environment.NewLine +
                 "| Rules: " + Environment.NewLine;


            foreach (DictionaryEntry entry in this.ruleValues)
            {
                outs += "|      " + entry.Key + " = " + entry.Value + Environment.NewLine;
            }

            outs += "-----------------------------------------" + Environment.NewLine;

            return outs;
        }

    }
}