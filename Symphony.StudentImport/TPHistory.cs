﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

using System.Linq;


using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.Security;

using Symphony.Core;
using Data = Symphony.Core.Data;

using Symphony.Core.Controllers.Salesforce;
using Symphony.Core.Models.Salesforce;

using Symphony.Core.Controllers;

using Newtonsoft.Json;
using System.IO;
using System.Data.OleDb;
using System.Configuration;

namespace StudentImport
{


    // --------------------------------------------------------
    // Loader for TP History Records
    //  1. 
    // --------------------------------------------------------
    public class TPHistory
    {


        // Main Inbound Order information to be loaded
        DataTable inTPHistory = null;
        DataTable oldHistory = null;
        DataTable oldHistoryUser = null;
        DataTable inSkuMap = null;
        DataTable inLocations = null;
        DataTable inTrainingPrograms = null;


        String customerID = "";
        ArrayList errorMessages = null;
        SqlConnection db = null;
        SqlConnection tpdb = null;
        string baseDirName = "";
        string programName = "Import";
        Queue jobQueue = null;
        Options opts = null;

        // Ignore stat checks
        Boolean ignoreWarnings = false; // Turn this on to stop reasonability checks on changes. (If there are a lot of changes )


        // Standard Stats
        StatInfo stats = null;


        // Special Custom Format Code
        int formatCode = 0; // 0 = Normal,  1 = Regency, 2 = Home Point Financial, 3 = Primary Residential 


        String connectionStringTP;
        string companyID; // TrainingPro companyID

        // -------------------------------------------------------------
        // Constructor 
        // Parameter dt is the inbound file data loaded into a DataTable
        // -------------------------------------------------------------
        public TPHistory(SqlConnection dbCon, SqlConnection tpdbCon, Options optvalues, Queue q)
        {

            this.opts = optvalues;
            //this.inTPHistory = dt;
            this.customerID = this.opts.getCustomerID();
            this.db = dbCon;
            this.tpdb = tpdbCon;
            this.errorMessages = new ArrayList();
            this.baseDirName = this.opts.getBaseDirectory();
            this.jobQueue = q;
            this.stats = new StatInfo();

            // Secondary Connection to TrainingPro:
            Console.WriteLine("TP Connection: " + ConfigurationManager.ConnectionStrings[opts.getEnvironment() + "TPDBConnection"]);
            connectionStringTP = ConfigurationManager.ConnectionStrings[opts.getEnvironment() + "TPDBConnection"].ConnectionString.ToString();


        }



        // ----------------------------------------------
        // Return the existing Order records.
        // ----------------------------------------------
        public DataTable getExistingRecords()
        {
            return this.oldHistory;
        }
        // ----------------------------------------------
        // Return the inbound/modified Order records.
        // ----------------------------------------------
        public DataTable getRecords()
        {
            return this.inTPHistory;
        }

        // ----------------------------------------------
        // Return the process statistics
        // ----------------------------------------------
        public StatInfo getStats()
        {
            return this.stats;
        }


        // -----------------------------------------------------------
        // sanitizeColumns
        // brief: Renames the column headers
        // to make sure column names are consistent.
        // 1. Change to consistent Case.
        // 2. Trims
        // 3. Adds in some processing columns
        // * This is the place to catch common misspellings if needed
        // (example:  name turns into Name, NAME turns into Name )
        // -----------------------------------------------------------
        public int sanitizeColumns()
        {
            for (int curCol = 0; curCol < inTPHistory.Columns.Count; curCol++)
            {
                String colName = inTPHistory.Columns[curCol].ColumnName.ToLower().Trim();
                Console.WriteLine(" COLUMN: " + colName);

            }

         
            return 0; // ok
        }

        // -----------------------------------------------------------
        // validateColumns
        // brief: Checks to make sure column information is available
        // return -1 if no Name
    
        // -----------------------------------------------------------
        public int validateColumns()
        {

 


            return 0; // ok
        }


        // ---------------------------------------------
        // load the History information for the customer
        // from the database.
        // ---------------------------------------------
        public int loadHistoryFromDB()
        {

            this.loadCompanyID();
            this.loadLocationLookup();
            this.loadTrainingProgramLookup();

            String query1 = " select U.CompanyID, NewCustomerID =  " + this.customerID + " , U.UserID, U.UserName, U.FirstName, U.LastName,MiddleName = U.MidInitial, EmployeeNumber = U.UserName, LocationID = CONVERT(CHAR(20),U.BranchID) ,\n " +
                            " U.Email,  Status = U.UserStatus, SalesforceAccountID = U.SalesforceContactID,  NMLS = U.NMLSNumber , U.Password, \n " +
                            " TPID = C.CourseID, TPName  = C.Name, TPCompletionDate =  DATEADD(HOUR,6,UC.CompletionDate), TPSKU = '', TPScore = '100', TPCompletion = '1', \n" + 
                            " TPCost = C.CreditHours, TPEndDate =   case when C.ExpirationDate is null then DATEADD(HOUR,6,'12/31/2015') else DATEADD(HOUR,6,C.ExpirationDate) end    , \n " +
                            " SKU = '' , AddressLine1 = U.Address, AddressLine2 = U.Address2, U.City, Zip = U.Zipcode, State = U.StateID, WorkPhone = U.Phone\n" + 
                            " from TrainingPro.dbo.[users] as U \n " +
                            "	 left outer join TrainingPro.dbo.UsersCourses as UC on U.UserID = UC.UserID \n " +
							" left outer join TrainingPro.dbo.Courses as C on UC.CourseID = C.CourseID \n " +
                            " where U.CompanyID = " + this.companyID + " 	and UC.CompletionDate is not null \n " +
         
                            " order by U.UserName ";



            this.inTPHistory = new DataTable();

            // Get the Data
            try
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("TP Get history user Data: " + query1);
                }
                SqlCommand myCommand = new SqlCommand(query1, this.tpdb);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!opts.isSilent())
                {
                    Console.WriteLine("Order DB Data Loaded.");
                }
                // Load into DataTable
                this.inTPHistory.Load(myReader);

                // Add some processing columns
                this.inTPHistory = Utility.addColumnToDataTable(this.inTPHistory, "New", "System.String");
                this.inTPHistory = Utility.addColumnToDataTable(this.inTPHistory, "NewUser", "System.String");
                this.inTPHistory = Utility.addColumnToDataTable(this.inTPHistory, "Delete", "System.String");
                this.inTPHistory = Utility.addColumnToDataTable(this.inTPHistory, "Update", "System.String");
                this.inTPHistory = Utility.addColumnToDataTable(this.inTPHistory, "UpdateID", "System.String");
                this.inTPHistory = Utility.addColumnToDataTable(this.inTPHistory, "UpdateAUID", "System.String");
                this.inTPHistory = Utility.addColumnToDataTable(this.inTPHistory, "ErrorFg", "System.String");
                this.inTPHistory = Utility.addColumnToDataTable(this.inTPHistory, "Error", "System.String");
         

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }





            string userfilter = " and U.username in (";
            for (int curRow = 0; curRow < this.inTPHistory.Rows.Count; curRow++)
            {
                string username = this.inTPHistory.Rows[curRow]["username"].ToString();
                if (curRow > 0)
                {
                    userfilter += ",";
                }
                userfilter += "'" + username + "'";
            }
            userfilter += ")";

            String query = " select U.CustomerID, UserID = U.ID, U.UserName, U.FirstName, U.LastName,U.MiddleName, U.EmployeeNumber, LocationID = CONVERT(CHAR(20),U.LocationID ), \n " +
                            " U.Email, Status = U.StatusID, U.SalesforceAccountID,  NMLS = U.NMLSNumber \n " +
                            " from [user] as U \n " +
                            " where U.CUstomerID = " + this.customerID + "  " +
            //" + this.customerID + "\n " +
               //             " " + userfilter + " \n" +
                            " order by U.UserName ";



            this.oldHistoryUser = new DataTable();

            // Get the Data
            try
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("History User Get Data: " + query);
                }
                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!opts.isSilent())
                {
                    Console.WriteLine("Order DB Data Loaded.");
                }
                // Load into DataTable
                this.oldHistoryUser.Load(myReader);

                // Add some processing columns
                this.oldHistoryUser = Utility.addColumnToDataTable(this.oldHistoryUser, "New", "System.String");
                this.oldHistoryUser = Utility.addColumnToDataTable(this.oldHistoryUser, "Delete", "System.String");
                this.oldHistoryUser = Utility.addColumnToDataTable(this.oldHistoryUser, "Update", "System.String");


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }

            //Default some values
            for (int curRow = 0; curRow < this.oldHistoryUser.Rows.Count; curRow++)
            {
                this.oldHistoryUser.Rows[curRow]["Delete"] = "No";
            }


            // Load just the existing users ( for adding/updating )
            // Separate this from the Order information


            String Uquery = " select UserID = U.ID, U.UserName, U.FirstName, U.MiddleName, U.LastName, U.EmployeeNumber, LocationID = CONVERT(CHAR(20),U.LocationID), \n " +
                            " U.Email, Status = U.StatusID, U.SalesforceAccountID, U.JanrainUserUuid, NMLS = U.NmlsNumber, SKU = TP.SKU, TrainingProgramName = TP.Name, TPR.DateCompleted \n " +
                            " from [user] as U \n " +   
                            " inner join TrainingProgramRollup as TPR on U.ID = TPR.UserID \n" +
                            " inner join TrainingProgram as TP on TPR.TrainingProgramID = TP.ID \n" + 
                            " where U.CUstomerID = " + this.customerID + "\n " +
                            //" " + userfilter + " \n" +
                            " order by U.UserName " ;

         



            this.oldHistory = new DataTable();

            // Get the Data
            try
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("History User Get Completion Data: " + Uquery);
                }
                SqlCommand myCommand2 = new SqlCommand(Uquery, this.db);
                SqlDataReader myReader2 = myCommand2.ExecuteReader();

                if (!opts.isSilent())
                {
                    Console.WriteLine("History User DB Completion Data Loaded.");
                }
                // Load into DataTable
                this.oldHistory.Load(myReader2);

                // Add some processing columns
                this.oldHistory = Utility.addColumnToDataTable(this.oldHistory, "New", "System.String");
                this.oldHistory = Utility.addColumnToDataTable(this.oldHistory, "Delete", "System.String");
                this.oldHistory = Utility.addColumnToDataTable(this.oldHistory, "Update", "System.String");


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }

            //Default some values
            for (int curRow = 0; curRow < this.oldHistory.Rows.Count; curRow++)
            {
                this.oldHistory.Rows[curRow]["Delete"] = "No";
            }

            return 0; // ok
        }

         // ---------------------------------------------
        // load the TrainingPro Company ID
        // ---------------------------------------------
        public int loadCompanyID()
        {


            String query1 = " select COMP.CompanyID, COMP.NewCustomerID \n" +
                            " from Analyst.dbo.TPCompany as COMP where  COMP.NewCustomerID = " + this.customerID + "  \n";
                   


            DataTable CompanyDT = new DataTable();

            // Get the Data
            try
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("TP Get Company Data: " + query1);
                }
                SqlCommand myCommand = new SqlCommand(query1, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!opts.isSilent())
                {
                    Console.WriteLine("Company DB Data Loaded.");
                }
                // Load into DataTable
                CompanyDT.Load(myReader);
     


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }


            //Get
            for (int curRow = 0; curRow < CompanyDT.Rows.Count; curRow++)
            {
                this.companyID = CompanyDT.Rows[curRow]["CompanyID"].ToString();
            }


            return 0; // OK
        }


        // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int processChanges()
        {

            // Load Client Information
            if (this.loadClientInfo() < 0)
            {
                opts.setExecute(false);// Cannot update if there is an issue with the client info
            }

            // Pre Process Changes to determine what to do. 
            // Execution of changes will occur next.
            // For each row
            //  1. find it in the existing db rows
            //  2.  Mark if it is new.
            //  3.  Error if it matches to more than one row.
            //  4.  Mark as update if it needs to change.

            this.stats.inbound_count = this.inTPHistory.Rows.Count;
            this.stats.existing_count = this.oldHistory.Rows.Count;
            int found_count = 0;

            for (int curRow = 0; curRow < this.inTPHistory.Rows.Count; curRow++)
            {
                // These are the new values on the file
                string  UserName = this.inTPHistory.Rows[curRow]["username"].ToString();
                string TrainingProgramName = this.inTPHistory.Rows[curRow]["TPName"].ToString();
                string TPID = this.inTPHistory.Rows[curRow]["TPID"].ToString();
                string  FirstName = this.inTPHistory.Rows[curRow]["firstname"].ToString();
                string LastName = this.inTPHistory.Rows[curRow]["lastname"].ToString();
                string MiddleName = this.inTPHistory.Rows[curRow]["middlename"].ToString();
                string Email = this.inTPHistory.Rows[curRow]["email"].ToString();
                string EmployeeNumber = this.inTPHistory.Rows[curRow]["employeenumber"].ToString();
                string NMLS =  this.inTPHistory.Rows[curRow]["nmls"].ToString();
                string Status = this.inTPHistory.Rows[curRow]["status"].ToString();
                string SKU = "";// this.inTPHistory.Rows[curRow]["sku"].ToString();
                string License = "";// this.inTPHistory.Rows[curRow]["License"].ToString();
                string LocationID = this.inTPHistory.Rows[curRow]["LocationID"].ToString();
                string TPCompletedDate = this.inTPHistory.Rows[curRow]["TPCompletionDate"].ToString();
                string TPEndDate = this.inTPHistory.Rows[curRow]["TPEndDate"].ToString();
                string TPCost = this.inTPHistory.Rows[curRow]["TPCost"].ToString();
                string Completed = "1";

                // Default Some of the Metadata. 
                this.inTPHistory.Rows[curRow]["UpdateID"] = "";
                //this.inTPHistory.Rows[curRow]["OldUserName"] = "";
                //this.inTPHistory.Rows[curRow]["OldEmail"] = "";
                //this.inTPHistory.Rows[curRow]["OldSKU"] = "";
                //this.inTPHistory.Rows[curRow]["OldNMLS"] = "";
                //this.inTPHistory.Rows[curRow]["OldEmployeeNumber"] = "";

                //Find existing User:
        
                DataRow[] foundRowsU;
                foundRowsU = this.oldHistoryUser.Select("UserName = '" + UserName + "'");
                if (foundRowsU.Length < 1)
                {
                    // Not Found. Mark as New
                    this.inTPHistory.Rows[curRow]["NewUser"] = "1";
                    //this.inTPHistory.Rows[curRow]["CreateRegistration"] = "1";

                    this.stats.addColumnCount("New Users");
           

                    // Add to lookup as New User
                    DataRow newRow = this.oldHistoryUser.NewRow();
                    newRow["new"] = "1";
                    newRow["CustomerID"] = this.customerID;
                    newRow["username"] = UserName;
                    newRow["email"] = Email;
                    newRow["firstname"] = FirstName;
                    newRow["lastname"] = LastName;
                    newRow["middlename"] = MiddleName;
                    newRow["EmployeeNumber"] = EmployeeNumber;
                    newRow["LocationID"] = LocationID;
                    newRow["NMLS"] = NMLS;
                    newRow["Status"] = Status; // Active

                    this.oldHistoryUser.Rows.Add(newRow); // Add to user list as New
                }
                
                // Add Location/Branch if not there:
                if (LocationID.Length > 0 ) {
                    lookupLocation(curRow, LocationID, LocationID); // This will add in a new one if needed.
                }
                // Add Training Program if not there:
                if (TrainingProgramName.Length > 0)
                {
                    lookupTrainingProgram(curRow, TrainingProgramName, TPID, TPEndDate, TPCost); // This will add in a new one if needed.
                }


                // Find existing TP Row using SKU value 
                DataRow[] foundRows;
                foundRows = this.oldHistory.Select("UserName = '" + UserName.Replace("'", "''") + "'" + " and TrainingProgramName = '" + TrainingProgramName.Replace("'", "''") + "' and DateCompleted = '" + TPCompletedDate + "'");
                if (foundRows.Length < 1)
                {
                    // Not Found. Mark as New
                    this.inTPHistory.Rows[curRow]["New"] = "1";
                    //this.inTPHistory.Rows[curRow]["CreateRegistration"] = "1";

                    this.stats.insert_count++;


                    // Lookup SKU
                    //Console.WriteLine(" SKU:" + SKU + " from license: " + License);
                    if (SKU.Length < 1)
                    {
                        // SKU not provided. Lookup using License
                        //lookupSKU(curRow, License);

                    }

                }
                else if (foundRows.Length > 1)
                {
                    // Too many Matches. Error.
                    this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                    this.inTPHistory.Rows[curRow]["Error"] = "TP History found too many existing record matches on UserName:  " + UserName + " and TrainingProgramName = '" + TrainingProgramName + "' and DateCompleted = '" + TPCompletedDate + "' .(" + foundRows.Length + ")";
                    this.inTPHistory.Rows[curRow]["New"] = "0";
                    this.stats.error_count++;
                    continue;
                }
                else
                {
                    // Mark existing record as found.
                    foundRows[0]["Delete"] = "No";
                    found_count++;

                 


                    // Update Might be needed. Compare the new values to the old values to make sure:
                    //
          


                    Boolean hasChanged = false;
                  


                    if (hasChanged)
                    {
                        this.stats.update_count++;
                    }

                }




            }

            this.stats.delete_count = 0;// this.stats.existing_count - found_count;

            Utility.SaveToCSV(this.oldHistoryUser, this.opts.getBaseDirectory() +  this.customerID + "_UserData.csv", 0, 0);
            Utility.SaveToCSV(this.inLocations, this.opts.getBaseDirectory() + this.customerID + "_User_Locations.csv", 0, 0);
            Utility.SaveToCSV(this.inTrainingPrograms, this.opts.getBaseDirectory()  +this.customerID + "_TrainingPrograms.csv", 0, 0);

            return 0; //ok
        }

        // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int executeChanges()
        {

            // Insert new Users
            this.executeAdds();

            // Insert new  Training Programs
            this.executeAddsTrainingProgram();

            // Update primary fields
            //this.executeUpdates();

            // Update lookup fields
            //this.executeSecondaryUpdates();

            //this.executeSalesforceTrainingProgramAssignments();

            return 0; //ok

        }

        private void  executeSalesforceTrainingProgramAssignments() {
            
                Console.WriteLine("Updating sessions...");

                bool isPreview = !opts.isExecute() || !opts.isSessions();

                if (isPreview)
                {
                    Console.WriteLine("Previewing session assignments");
                }

                Dictionary<string, int> userIdDictionary = new Dictionary<string, int>();

                string userfilter = "U.username in (";
                for (int curRow = 0; curRow < this.inTPHistory.Rows.Count; curRow++)
                {
                    string username = this.inTPHistory.Rows[curRow]["username"].ToString();
                    if (curRow > 0)
                    {
                        userfilter += ",";
                    }
                    userfilter += "'" + username + "'";
                }
                userfilter += ")";

                string sql = "select U.ID, U.Username from [User] U where " + userfilter + " and customerID = " + this.customerID;
                
                DataTable userIds = new DataTable();

                // Get the Data
                try
                {
                    Console.WriteLine("Get new user ids: " + sql);
                    
                    SqlCommand myCommand = new SqlCommand(sql, this.db);
                    SqlDataReader myReader = myCommand.ExecuteReader();

                    Console.WriteLine("New user ids loaded.");
                    
                    // Load into DataTable
                    userIds.Load(myReader);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return;
                }

                for (int curRow = 0; curRow < userIds.Rows.Count; curRow++)
                {
                    string uname = userIds.Rows[curRow]["Username"].ToString();
                    string id = userIds.Rows[curRow]["ID"].ToString();

                    int i = int.Parse(id);

                    if (!userIdDictionary.ContainsKey(uname))
                    {
                        userIdDictionary.Add(uname, i);
                    }
                }
                if (!opts.isSilent())
                {
                    Console.WriteLine("User dictionary created.");
                }
                for (int curRow = 0; curRow < this.inTPHistory.Rows.Count; curRow++)
                {
                    DataRow row = this.inTPHistory.Rows[curRow];

                    if (row["CreateRegistration"].ToString() == "1")
                    {
                        
                        string OrderUserName = this.inTPHistory.Rows[curRow]["username"].ToString();
                        string OrderSKU = this.inTPHistory.Rows[curRow]["sku"].ToString();
                        string OrderEnrollmentDate = this.inTPHistory.Rows[curRow]["enrollmentdate"].ToString();

                        Console.WriteLine("New Registration for: " + OrderUserName + ", " + OrderSKU + ", " + OrderEnrollmentDate);
                        
                        EntitlementList entitlements = new EntitlementList()
                        {
                            Entitlements = new List<Entitlement>()
                        };

                        DateTime enrollmentDate = DateTime.UtcNow;
                        if (!DateTime.TryParse(OrderEnrollmentDate, out enrollmentDate))
                        {
                            Console.WriteLine("Invalid order date. Defaulting to today.");
                        }
                        
                        entitlements.Entitlements.Add(new Entitlement()
                        {
                            EntitlementDuration = 12,
                            EntitlementDurationUnits = "Month",
                            EntitlementStartDate = enrollmentDate.ToString(),
                            EntitlementPurchaseDate = enrollmentDate.ToString(),
                            EntitlementEndDate = enrollmentDate.AddYears(1).ToString(),
                            EntitlementId = 0,
                            SalesforceEntitlementId = "",
                            IsEnabled = true,
                            IsSuppressEmail = false,
                            PartnerCode = "",
                            SalesforceProductId = "",
                            ProductId = OrderSKU
                        });

                        if (userIdDictionary.ContainsKey(OrderUserName))
                        {
                            Console.WriteLine("Saving Enrollment: " + OrderUserName + ", " + OrderSKU + ", " + enrollmentDate.ToString() + " to " + enrollmentDate.AddYears(1).ToString());
                            
                            SalesforceController.OverrideTargetCustomerID(int.Parse(opts.getCustomerID()));
                            if (!isPreview)
                            {
                                (new EntitlementController()).PostEntitlements(userIdDictionary[OrderUserName], entitlements);
                            }
                            else
                            {
                                Console.WriteLine("In preview mode - Session assignments can not be previewed since it relies on the training program enrollment being created first.");
                            }
                        }
                    }
                }

                List<int> userIdList = userIdDictionary.Select(kv => kv.Value).ToList();

                // Create session registrations for any users that don't have them
                Console.WriteLine("Creating session registrations...");
                List<Symphony.Core.Models.SessionAssignedUser> sessionAssigned = new List<Symphony.Core.Models.SessionAssignedUser>();
                try
                {
                    if (isPreview)
                    {
                        Console.WriteLine("In Preview mode - Saving session registrations will probably fail, unless there were previous un assigned sessions for this user.");
                    }

                    sessionAssigned = (new ClassroomController()).SaveSessionRegistrations(userIdList, isPreview).Data.ToList();

                    foreach (Symphony.Core.Models.SessionAssignedUser u in sessionAssigned)
                    {
                        Console.WriteLine(u.FullName + " is " + u.RegistrationStatusName + " in the class " + u.ClassName + " on " + u.MinStartDateTime.ToString());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Could not save session registrations. " + e.Message);
                }

                string sessionAssignedUsersJson = JsonConvert.SerializeObject(sessionAssigned);
                Console.WriteLine("Session assignments: " + sessionAssignedUsersJson);

                Console.WriteLine("Saving user ids to job queue...");
                
                jobQueue.setSessionUserIds(opts.getJobID(), userIdList);
                jobQueue.setSessionUserJson(opts.getJobID(), sessionAssignedUsersJson);
                

            
        }

        // ---------------------------------------------------------
        // Update the primary fields
        // -----------------------------------------------------------
        private void executeUpdates()
        {

            string update1SQL = "";
            string undoUpdate1SQL = "";

            // Generate Update Statements for Changes
            for (int curRow = 0; curRow < this.inTPHistory.Rows.Count; curRow++)
            {

                if (!opts.isSilent() && curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Order Update Scan " + curRow + " of " + this.inTPHistory.Rows.Count);
                }

                string A_New = this.inTPHistory.Rows[curRow]["New"].ToString();
                string A_Update = this.inTPHistory.Rows[curRow]["Update"].ToString();
                string A_Code = this.inTPHistory.Rows[curRow]["Code"].ToString();
                string A_Name = this.inTPHistory.Rows[curRow]["Name"].ToString().Replace("'", "''"); ;
                string A_ParentCode = this.inTPHistory.Rows[curRow]["ParentCode"].ToString();
                string A_ID = this.inTPHistory.Rows[curRow]["UpdateID"].ToString(); // Existing OrderID  
                string OldName = this.inTPHistory.Rows[curRow]["OldName"].ToString().Replace("'", "''"); ; // Existing Name  
                string OldCode = this.inTPHistory.Rows[curRow]["OldCode"].ToString(); // Existing Code  
                string OldParentID = this.inTPHistory.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
                string update_s = "";

                if (A_Update.Equals("1"))
                {


                    update_s = "Update Order set Name = '" + A_Name.Trim() + "', InternalCode = '" + A_Code + "',\n" +
                       " ModifiedBy = '" + this.programName + "', ModifiedOn = getdate() \n" +
                       " where ID = " + A_ID + " and CustomerID = " + this.customerID + " \n\n";

                    // Update the DB if requested.    
                    if (opts.isExecute())
                    {

                        try
                        {
                            SqlCommand myCommand = new SqlCommand(update_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount != 1)
                            {
                                this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                                this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", Update Failed with rc:" + rowcount;
                                update_s = "FAILED:" + update_s;
                            }
                        }
                        catch (Exception)
                        {
                            this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                            this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", Update Failed.";
                            update_s = "FAILED:" + update_s;
                        }
                    } // end db execute

                    update1SQL += update_s;
                    undoUpdate1SQL += "Update Order set Name = '" + OldName.Trim() + "', InternalCode = '" + OldCode + "',\n" +
                                  " ModifiedBy = ModifiedBy + ' rollback ' \n" +
                                  " where ID = " + A_ID + " and CustomerID = " + this.customerID + " \n\n";

                }// end if update

            } //end main loop

            // Update ParentOrderID after the 1st update, after the inserts, after the deletes.
            // This prevents updating to the incorrect OrderID if there are duplicate Codes before
            // all other actions have taken place. 


            Utility.SaveToFile(this.baseDirName + this.customerID + "_u1_Order_Update1SQL.txt", update1SQL +
                 "\n ----------------------------------------------- \n  Undo" +
                 "\n ----------------------------------------------- \n " + undoUpdate1SQL);

        } // End executeUpdate()


        private void executeSecondaryUpdates()
        {

            string update2SQL = "";
            string undoUpdate2SQL = "";


            // Generate Update Statements for Changes
            for (int curRow = 0; curRow < this.inTPHistory.Rows.Count; curRow++)
            {


                if (!opts.isSilent() && curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Order Secondary Update Scan " + curRow + " of " + this.inTPHistory.Rows.Count);
                }


                string A_New = this.inTPHistory.Rows[curRow]["New"].ToString();
                string A_Update = this.inTPHistory.Rows[curRow]["Update"].ToString();
                string A_Code = this.inTPHistory.Rows[curRow]["Code"].ToString();
                string A_Name = this.inTPHistory.Rows[curRow]["Name"].ToString();
                string A_ParentCode = this.inTPHistory.Rows[curRow]["ParentCode"].ToString();
                string A_ID = this.inTPHistory.Rows[curRow]["UpdateID"].ToString(); // Existing OrderID  
                string OldName = this.inTPHistory.Rows[curRow]["OldName"].ToString(); // Existing Name  
                string OldCode = this.inTPHistory.Rows[curRow]["OldCode"].ToString(); // Existing Code  
                string OldParentID = this.inTPHistory.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
                string update_s = "";

                if (A_Update.Equals("1") || A_New.Equals("1"))
                {


                    if (A_ParentCode.Length > 0 && !A_ParentCode.Equals("*none*"))
                    {

                        //FIX

                        //update_s = "Update A set A.ParentOrderID = case when P.ID is null then 0 else P.ID end \n" +
                        //  " from Order as A \n" +
                        //  " left outer join Order as P on P.ID = A.ParentOrderID \n" +
                        //  " where ID = " + A_ID + " and CustomerID = " + this.customerID + "\n\n";

                        update_s = "UPDATE A set A.ParentOrderID = ( select P.ID from Order P where P.CustomerID = " + this.customerID + "\n" +
                            " and P.InternalCode = '" + A_ParentCode + "' ) \n" +
                            " from Order A where A.CustomerID = " + this.customerID + " and A.InternalCode = '" + A_Code + "'\n";

                        // Secondary Update  the DB if requested.    
                        if (opts.isExecute())
                        {

                            try
                            {
                                SqlCommand myCommand = new SqlCommand(update_s, this.db);
                                int rowcount = myCommand.ExecuteNonQuery();
                                if (rowcount != 1)
                                {
                                    this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                                    this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", Parent Update Failed with rc:" + rowcount;
                                    update_s = "FAILED:" + update_s;
                                }
                            }
                            catch (Exception)
                            {
                                this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                                this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", Parent Update Failed.";
                                update_s = "FAILED:" + update_s;
                            }
                        } // End execute

                        update2SQL += update_s;
                        undoUpdate2SQL += "Update Order set ParentOrderID = " + OldParentID + ",\n" +
                        " where ID = " + A_ID + " and CustomerID = " + this.customerID + " \n\n";

                    } // End secondary parent update
                }//End update check
            }// End main loop

            Utility.SaveToFile(this.baseDirName + this.customerID + "_u4_Order_Update2SQL.txt", update2SQL +
            "\n ----------------------------------------------- \n  Undo" +
            "\n ----------------------------------------------- \n " + undoUpdate2SQL);

        } // End executeSecondaryUpdates()

        // ------------------------------------------------
        //  Add new Orders
        // ------------------------------------------------


        private int executeAdds()
        {


            // Insert Locations ( TrainingPro Branches ) 
            if (true)
            {

                string insert_new_locations = "";
                int add_count2 = 0;
                // Generate/Execute SQL Scripts for Inserts
                for (int lcr = 0; lcr < this.inLocations.Rows.Count; lcr++)
                {
                    string newFg = this.inLocations.Rows[lcr]["New"].ToString();
                    if (newFg == "1")
                    {
                        add_count2++;
                        insert_new_locations += "insert into Location ( CustomerID, Name, InternalCode, ModifiedOn, ModifiedBy, CreatedOn, CreatedBy, Address1, Address2, City, State, PostalCode, TelephoneNbr, ParentLocationID ) values (" +
                               this.opts.getCustomerID() + ",'" + this.inLocations.Rows[lcr]["Name"].ToString().Replace("'", "''") + "', '" + this.inLocations.Rows[lcr]["InternalCode"].ToString().Replace("'", "''") + "', " +
                                " getdate(), 'Import', getdate(), 'Import', '','','','','','',0 )\n";

                          
                        if (opts.isExecute() && add_count2 > 0)
                        {
                            SqlCommand myCommand_l = new SqlCommand(insert_new_locations, this.db);
                            SqlParameter IDParameter = new SqlParameter("@ID",SqlDbType.Int);
                            IDParameter.Direction = ParameterDirection.Output;
                            myCommand_l.Parameters.Add(IDParameter);
                            int rowcount_l = myCommand_l.ExecuteNonQuery();
                    
                            string locid = IDParameter.Value.ToString();
                            this.inLocations.Rows[lcr]["ID"] = locid;

                            Console.WriteLine("Insert Locations =  " + rowcount_l + " (" + locid + ") " + insert_new_locations);
                            if (rowcount_l < 0)
                            {

                                Console.WriteLine("Insert Locations Failed: " + insert_new_locations);
                            }

                            insert_new_locations = "";

                        }

   
                    }
                }



            }// End insert Locations


            string insertSQL = "";


            // Generate/Execute SQL Scripts for Inserts
            for (int curRow = 0; curRow < this.inTPHistory.Rows.Count; curRow++)
            {

                if (curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("User Add " + curRow + " of " + this.inTPHistory.Rows.Count);
                }

                string Emp_NewUser = this.inTPHistory.Rows[curRow]["NewUser"].ToString();
                string Emp_CurrentErrorFg = this.inTPHistory.Rows[curRow]["ErrorFg"].ToString();
                string Emp_Update = this.inTPHistory.Rows[curRow]["Update"].ToString();
                string Emp_ID = this.inTPHistory.Rows[curRow]["UpdateID"].ToString();
                string Emp_ASPNETID = this.inTPHistory.Rows[curRow]["UpdateAUID"].ToString();
                string Emp_Username = this.inTPHistory.Rows[curRow]["Username"].ToString();
                string Emp_EmployeeNumber = this.inTPHistory.Rows[curRow]["EmployeeNumber"].ToString();
                string Emp_NMLS = this.inTPHistory.Rows[curRow]["NMLS"].ToString();
                string Emp_FirstName = this.inTPHistory.Rows[curRow]["FirstName"].ToString();
                string Emp_LastName = this.inTPHistory.Rows[curRow]["LastName"].ToString();
                string Emp_MiddleName = this.inTPHistory.Rows[curRow]["MiddleName"].ToString();
                string Emp_Email = this.inTPHistory.Rows[curRow]["Email"].ToString();
                string Emp_Status = this.inTPHistory.Rows[curRow]["Status"].ToString();
                string Emp_Password = this.inTPHistory.Rows[curRow]["Password"].ToString();
                string Emp_Notes = "";// this.inTPHistory.Rows[curRow]["Notes"].ToString();
                string Emp_AddressLine1 = this.inTPHistory.Rows[curRow]["AddressLine1"].ToString();
                string Emp_AddressLine2 = this.inTPHistory.Rows[curRow]["AddressLine2"].ToString();
                string Emp_City = this.inTPHistory.Rows[curRow]["City"].ToString();
                string Emp_State = this.inTPHistory.Rows[curRow]["State"].ToString();
                string Emp_Zip = this.inTPHistory.Rows[curRow]["Zip"].ToString();
                string Emp_workphone = this.inTPHistory.Rows[curRow]["WorkPhone"].ToString();
                string Emp_PasswordChange = "'01/01/2014'";

                string insert_s = "";

                if (Emp_NewUser.Equals("1") && !Emp_CurrentErrorFg.Equals("1"))
                {

                    // Check to see if the Password is valid ( >= 6 characters with one number )
                    // Only if it is a new record, or if we want to update the password

                    if (false) //Emp_Password.Length < 7 || !Regex.IsMatch(Emp_Password, @"\d"))
                    {
                        // Is not 7 characters or more, and does not have a number in it.
                        Console.WriteLine("Invalid Password: '" + Emp_Username +"'");
                        this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                        this.inTPHistory.Rows[curRow]["Error"] = inTPHistory.Rows[curRow]["Error"] + "Password needs to be 7 or more characters with at least 1 number.";
                        this.stats.error_count++;
                        //Utility.SaveFilteredToCSV(inTPHistory, this.baseDirName + "a1_Add_Emp_error.csv", fileFilter_new, colNames_new);
                        continue; // Skip

                    }


                    insert_s = "insert into [user] (username, CustomerID, SalesChannelID, LocationID \n" +
                            ",EmployeeNumber, FirstName, MiddleName, LastName \n" +
                            ",HireDate, NewHireIndicator, Email, StatusID, JobRoleID, Notes \n" +
                            ",LoginCounter, IsAccountExec, IsCustomerCare, ModifiedBy, CreatedBy \n" +
                            ",ModifiedOn, CreatedOn, SupervisorID, IsExternal, SecondaryLocationID, TelephoneNumber, [password] , ReportingSupervisorID, LastEnforcedPasswordReset, NMLSNumber \n " + 
                            " ,AddressLine1, AddressLine2, City, State, ZipCode, WorkPhone )\n " +
                            " values ( '" + Emp_Username.Replace("'", "''") + "', " + opts.getCustomerID() + ",0,0 \n " +
                            " ,'" + Emp_EmployeeNumber.Replace("'", "''") + "', '" + Emp_FirstName.Replace("'", "''") + "','" + Emp_MiddleName.Replace("'", "''") + "','" + Emp_LastName.Replace("'", "''") + "' \n" +
                            " ,getdate(),0,'" + Emp_Email.Replace("'", "''") + "',1,0,'" + Emp_Notes.Replace("'", "''") + "' \n" +
                            ",0,0,0,'Import','Import',getdate(), getdate(),0,0,0,'','',0,'01/01/2014', '" + Emp_NMLS + "' \n" +
                            ", '" + Emp_AddressLine1.Replace("'", "''") + "' , '" + Emp_AddressLine2.Replace("'", "''") + "' , '" + Emp_City.Replace("'", "''") + "' , '" + Emp_State.Replace("'", "''") + "' , '" + Emp_Zip.Replace("'", "''") + "' , '" + Emp_workphone.Replace("'", "''") + "')\n\n";

                    //Console.WriteLine("User:" + insert_s);
                    if (opts.isExecute())
                    {
                        
                        //  adding user
                        Emp_ASPNETID = this.AddASPNETUser(Emp_Username, Emp_Password, Emp_Email);

                        if (Emp_ASPNETID.Equals("ERROR"))
                        {
                            Console.WriteLine("Failed to add ASPNET user." + Emp_Username);
                            this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                            this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", User Insert Failed on Membership:";

                            insert_s = "FAILED:" + insert_s;
                            this.stats.error_count++;
                            continue; //Fail all and Skip this guy



                        }
                        this.inTPHistory.Rows[curRow]["UpdateAUID"] = Emp_ASPNETID;
                        Console.WriteLine("Added ASPNET user." + Emp_Username + " ID:" + Emp_ASPNETID);

                        // Add Main User
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(insert_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount < 1)
                            {


                                this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                                this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", Insert Failed with rc:" + rowcount;
                                insert_s = "FAILED:" + insert_s;

                                Console.WriteLine("User Insert Failed: " + insert_s);
                                
                                this.stats.error_count++;
                                continue;
                            }
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("User Insert Failed: " + insert_s);
                                
                            this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                            this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", Insert Failed.";
                            insert_s = "FAILED:" + insert_s;
                            this.stats.error_count++;
                            continue;
                        }

                        insertSQL += insert_s; // Save for final SQL Script in log

                        insert_s = "insert into aspnet_UsersInRoles (UserId, RoleID) values ('" + Emp_ASPNETID +
                              "','" + opts.getUserRoleID() + "')\n\n";

                       
                        // Add Role
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(insert_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount < 1)
                            {
                                this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                                this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", Role Insert Failed with rc:" + rowcount;
                                insert_s = "FAILED:" + insert_s;
                                insertSQL += insert_s;
                                this.stats.error_count++;
                                continue;
                            }
                        }
                        catch (Exception)
                        {
                            this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                            this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", Role Insert Failed.";
                            insert_s = "FAILED:" + insert_s;
                            insertSQL += insert_s;
                            this.stats.error_count++;
                            continue;
                        }
                        insertSQL += insert_s + "\n----------------------------------------------\n";

                    }
                    else
                    {// End if Execute
                        insertSQL += insert_s;
                    }

                    // ! The SupervisorID needs to be updated after the 1st update, and after the inserts.
                    // This will be done in the processSecondaryUpdate method



                }



            } // End big employee loop

            this.inTPHistory.AcceptChanges();
            Utility.SaveToFile(this.baseDirName + this.customerID + "_u2_TPHistoryUser_InsertSQL.txt", insertSQL);

            return 0;

        }



        // Add Training Program and Completion Data
        private void executeAddsTrainingProgram()

        {





       

            // Insert Training Programs ( TrainingPro History Courses ) 
            if (true)
            {

                string insert_new_tp = "";
     
                // Generate/Execute SQL Scripts for Inserts
                for (int lcr = 0; lcr < this.inTrainingPrograms.Rows.Count; lcr++)
                {
                    string newFg = this.inTrainingPrograms.Rows[lcr]["New"].ToString();
                    if (newFg == "1")
                    {

                        insert_new_tp += "insert into TrainingProgram ( CustomerID, OwnerID, Name, InternalCode, Cost, Description, IsNewHire, IsLive, \n" +
                        "  StartDate, EndDate, DueDate, EnforceRequiredOrder, Minimumelectives, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, \n" +
                        "  CategoryID, NewHireOffsetEnabled, DisableScheduled, CreatedByUserID, ModifiedByUserId, IsSalesForce, EnforceCanMoveForwardIndicator, \n" +
                        " SurveyID, isSurveyMandatory, IsCourseTimingDisabled, CourseUnlockMOdeID ) \n" +
                        " values ( " + this.inTrainingPrograms.Rows[lcr]["CustomerID"].ToString() + ",\n" + // --CustomerID,  
                        this.inTrainingPrograms.Rows[lcr]["OwnerID"].ToString() + ",	\n" + //	--	OwnerID,  
                        "'" + this.inTrainingPrograms.Rows[lcr]["Name"].ToString().Replace("'", "''") + "',\n" + //		--	Name,  
                        "'" + this.inTrainingPrograms.Rows[lcr]["InternalCode"].ToString().Replace("'", "''") + "',	\n" + //		--	InternalCode,  
                        this.inTrainingPrograms.Rows[lcr]["Cost"].ToString() + ",	\n" + //	--Cost,  
                        "'" + this.inTrainingPrograms.Rows[lcr]["Description"].ToString().Replace("'", "''") + "',	\n" + //		--Description, 
                        "0, \n" + // --IsNewHire,
                        "0,		 \n" + //--IsLive, 
                        "'01/01/2015',	\n" + //	--	StartDate,  
                        "'12/31/2015',	\n" + //	--EndDate, 
                        "'12/31/2015',	\n" + //	-- DueDate,  
                        "0,	\n" + //	--EnforceRequiredOrder, 
                        "0,	\n" + //	-- Minmimumelectives, 
                        "'Import',	\n" + //	--ModifiedBy, 
                        "'Import',	\n" + //	--CreatedBy, 
                        "getdate(),	\n" + //	--ModifiedOn, 
                        "getdate(),	\n" + //	--CreatedOn,
                         this.opts.getRuleValue("TrainingProCategoryID") + ",	\n" + //	--	CategoryID,
                        "0,	\n" + //	-- NewHireOffsetEnabled, 
                        "1,	\n" + //	--DisableScheduled, 
                        "0,	\n" + //	--CreatedByUserID, 
                        "0,	\n" + //	--ModifiedByUserId, 
                        "0,	\n" + //	--IsSalesForce, 
                        "0,	\n" + //	--EnforceCanMoveForwardIndicator,
                        "0,	\n" + //	--SurveyID, 
                        "0,	\n" + //	--isSurveyMandatory, 
                        "0,	\n" + //	--IsCourseTimingDisabled, 
                        "0	\n" + //	--CourseUnlockMOdeID 
                        ") \n SET @GID = SCOPE_IDENTITY(); ";
                        

                        //Console.WriteLine("Insert Training Program : " + insert_new_tp );

                        if (opts.isExecute())
                        {
                            SqlCommand myCommand_l = new SqlCommand(insert_new_tp, this.db);
                            SqlParameter IDParameter = new SqlParameter("@GID", SqlDbType.Int);
                            IDParameter.Direction = ParameterDirection.Output;
                            myCommand_l.Parameters.Add(IDParameter);
                            int rowcount_l = myCommand_l.ExecuteNonQuery();

                            string tpid = IDParameter.Value.ToString();
                            Console.WriteLine("Training Program ID: " + tpid);
                            this.inTrainingPrograms.Rows[lcr]["NewID"] = tpid;

                            Console.WriteLine("Insert Training Program =  " + rowcount_l + " (" + tpid + ") " );
                            if (rowcount_l < 0)
                            {
                                Console.WriteLine("Insert Training Program : " + insert_new_tp);
                                Console.WriteLine("Insert Locations Failed: " + insert_new_tp);
                            }

                            insert_new_tp = "";

                        }


                    }
                }



            }// End insert Training Program




            string insertSQL = "";
        

            // Generate Update/Insert Statements for Orders
            for (int curRow = 0; curRow < this.inTPHistory.Rows.Count; curRow++)
            {

                if (!opts.isSilent() && curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("History Add Scan " + curRow + " of " + this.inTPHistory.Rows.Count);
                }


                string A_New = this.inTPHistory.Rows[curRow]["New"].ToString();
                string A_UserID = this.inTPHistory.Rows[curRow]["UserID"].ToString();
                string A_Name = this.inTPHistory.Rows[curRow]["TPName"].ToString().Replace("'", "''");
                string A_UserName = this.inTPHistory.Rows[curRow]["UserName"].ToString().Replace("'", "''");
                string A_SKU = this.inTPHistory.Rows[curRow]["TPSKU"].ToString();
                string A_CustomerID = this.inTPHistory.Rows[curRow]["NewCustomerID"].ToString();
                string A_CompletionDate =  this.inTPHistory.Rows[curRow]["TPCompletionDate"].ToString();
                string A_Completion = this.inTPHistory.Rows[curRow]["TPCompletion"].ToString();
              
                string A_ID = this.inTPHistory.Rows[curRow]["UpdateID"].ToString(); // Existing HistoryID ?
        
                //string update_s = "";

                // For now always put everything in the order queue and indicate which ones are adds.
                if (true)
                {


                    string insert_s = " insert into TrainingProgramRollup ( TrainingProgramID, UserID, IsCompleteOverride, DateCompleted, \n " +
                    " IsSurveyComplete, RequiredCompletedCount, ElectiveCOmpletedCount, FinalCompletedCount,\n " +
                    " 	AssignmentsCompletedCount, FinalExamScore, IsRequiredComplete, IsElectiveComplete,\n " +
                        " IsFinalComplete, IsAssignmentsComplete, IsComplete, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy )\n " +
                    " values (  (select MAX(TP2.ID) from TrainingProgram as TP2 where TP2.CustomerID = " + this.opts.getRuleValue("TrainingProCustomerID") + " and Name = '" + A_Name + "' and IsLive = 0 ),\n " + //-- TrainingProgramID,
                            " (select MAX(U.ID) from [user] as U where U.CustomerID = " + this.customerID + " and UserName = '" + A_UserName.Replace("'","''") + "' ) , \n" + //--UserID, 
                            " 1,\n" + //--IsCompleteOverride, 
                            " '" + A_CompletionDate + "', \n" + //--DateCompleted,
                            "  1, \n" + //--IsSurveyComplete, 
                            " 1, \n" + //--RequiredCompletedCount, 
                            " 0, \n" + //--ElectiveCOmpletedCount, 
                            " 1, \n" + //--FinalCompletedCount,
                            " 1,\n" + // --AssignmentsCompletedCount, 
                            " 100, \n" + //--FinalExamScore, 
                            "  1,\n" + // --IsRequiredComplete, 
                            " 1, \n" + //-- IsElectiveComplete,
                            " 1, \n" + //--IsFinalComplete, 
                            " 0, \n" + //--IsAssignmentsComplete, 
                            " 1, \n" + //--IsComplete, 
                            " getdate(), " +
                            " getdate(), " +
                            " 'Import', 'Import' )\n\n";




                    //Console.WriteLine("History Add " + insert_s);

                    // Insert into the DB if requested.    
                    if (opts.isExecute())
                    {

                        try
                        {
                            SqlCommand myCommand = new SqlCommand(insert_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount != 1)
                            {
                                this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                                this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", Insert Failed with rc:" + rowcount;
                                insert_s = "FAILED:" + insert_s;
                            }
                        }
                        catch (Exception)
                        {
                            this.inTPHistory.Rows[curRow]["ErrorFg"] = "1";
                            this.inTPHistory.Rows[curRow]["Error"] = this.inTPHistory.Rows[curRow]["Error"].ToString() + ", Insert Failed.";
                            insert_s = "FAILED:" + insert_s;
                        }
                    } // End db execute
                    insertSQL += insert_s;



                } // End if new

           


            } // End New/Insert Processing loop



            Utility.SaveToFile(this.baseDirName + this.customerID + "_u2_Order_InsertSQL.txt", insertSQL +
                "\n ----------------------------------------------- \n ");

        } // End executeAdd()


        private int loadClientInfo()
        {


            string clientInfoSQL = "select A.ApplicationName ,A.ApplicationID,  \n" +
            "UserRoleID = R1.RoleID, SupRoleID = R2.RoleID, ReportSupRoleID = R3.RoleID \n" +
            "from Customer C\n" +
            "inner join aspnet_Applications as A on A.ApplicationName = C.SubDomain\n" +
            "inner join aspnet_Roles as R1 on A.ApplicationId = R1.ApplicationId and R1.RoleName = 'Customer - User'\n" +
            "inner join aspnet_Roles as R2 on A.ApplicationId = R2.ApplicationId and R2.RoleName = 'Classroom - Supervisor'\n" +
            "inner join aspnet_Roles as R3 on A.ApplicationId = R3.ApplicationId and R3.RoleName = 'Reporting - Supervisor'\n" +
            "where C.ID = " + opts.getCustomerID() + "\n";


            DataTable cltInfo = new DataTable();

            // Get the Data
            try
            {
                    Console.WriteLine("Client Info Get Data: " + clientInfoSQL);
                SqlCommand myCommand = new SqlCommand(clientInfoSQL, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                    Console.WriteLine("Employee DB Data Loaded.");
                // Load into DataTable
                cltInfo.Load(myReader);




            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }
            if (cltInfo.Rows.Count < 1)
            {
                Console.WriteLine("No Client Information. Will not execute.");
                return -1;
            }
            //Default some values
            for (int curRow = 0; curRow < cltInfo.Rows.Count; curRow++)
            {
                opts.setCustomerShortName(cltInfo.Rows[curRow]["ApplicationName"].ToString());
                opts.setApplicationID(cltInfo.Rows[curRow]["ApplicationID"].ToString());
                opts.setUserRoleID(cltInfo.Rows[curRow]["UserRoleID"].ToString());
                opts.setSupervisorRoleID(cltInfo.Rows[curRow]["SupRoleID"].ToString());
                opts.setReportingSupervisorRoleID(cltInfo.Rows[curRow]["ReportSupRoleID"].ToString());
            }

            Console.Write(opts.toString());

            return 0; // ok
        }

        // -----------------------------------------------------------
        private string AddASPNETUser(string username, string password, string email)
        {
            //string username = "bankersedge2";
            //string password = "password1";
            //string email = "bcarlson@bankersedge.com";


            string passwordQuestion = "foo";
            string passwordAnswer = "bar";
            Boolean isApproved = true;


            Membership.ApplicationName = opts.getCustomerShortName();// "example: anbone"; Acts as the App Name

            Console.WriteLine("Create User: " + username + " App = " + Membership.ApplicationName);
            MembershipCreateStatus memStatus = new MembershipCreateStatus();


            //** ADD USER
            MembershipUser mu = Membership.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, out memStatus);
            Console.WriteLine("Created User:" + memStatus.ToString());


            if (memStatus != MembershipCreateStatus.Success)
            {
                Console.WriteLine("Error adding " + username + ".");
                return "ERROR";
            }
            Console.WriteLine("Done adding " + username + ".");
            return mu.ProviderUserKey.ToString();
        }


        // --------------------------------------------------
        // Load the Excel or CSV File data (optional)
        // This can detect and load different formats
        // --------------------------------------------------
        public int loadFile(String baseDir, String orderFileName)
        {

            // Find the file. ( The import screens may upload a csv, or xls, or xlsx )
            // The filename configured for the import may not match. 
            // We have to be dynamic here.

            // Simple CSV File


            if (System.IO.File.Exists(baseDir + orderFileName))
            {
                Console.WriteLine("Loading CSV FIle : " + baseDir + orderFileName);
                Utility.ConstructSchema(baseDir, orderFileName);
                this.inTPHistory = Utility.ParseCSV(baseDir + orderFileName);
                return 1;
            }

            if (orderFileName.Contains(".csv"))
            {

                Console.WriteLine("No CSV FIle. Finding XLSX or XLS files : " + baseDir );
                // Try finding xls and xlsx
                if (System.IO.File.Exists(baseDir + orderFileName.Replace(".csv", ".xls")))
                {
                    
                    orderFileName = orderFileName.Replace(".csv", ".xls");
                    Console.WriteLine("Found XLS : " + baseDir + orderFileName);
                }
                else if (System.IO.File.Exists(baseDir + orderFileName.Replace(".csv", ".xlsx")))
                {
                    orderFileName = orderFileName.Replace(".csv", ".xlsx");
                    Console.WriteLine("Found XLSX : " + baseDir + orderFileName);
                }
            }

            if (System.IO.File.Exists(baseDir + orderFileName))
            {
                Console.WriteLine("Loading Excel FIle : " + baseDir + orderFileName);
                Utility.ConstructExcelSchema(baseDir, orderFileName,"","");
                this.inTPHistory = OrderLoader.ParseExcel(baseDir + orderFileName,"","students");
                this.inSkuMap = OrderLoader.ParseExcel(baseDir + orderFileName, "", "skumap");
                Utility.PrintDataTable(inTPHistory);
                Utility.PrintDataTable(inSkuMap);
                return 1;
            }


            //this.getSheetNamesFromExcel(baseDir + orderFileName);
            

            return 0;

        }

       
       

         // -------------------------------------------------------------
        // Get Excel Sheet Names
        // Custom method to load a Excel into a DataTable
        // Params: path is the full path to the file.
        // ---------------------------------------------------------------
        public   List<string> getSheetNamesFromExcel(string path)
        {
            if (!System.IO.File.Exists(path))
                return null;

            string full = Path.GetFullPath(path);

            string filenamenoextension = Path.GetFileNameWithoutExtension(path);
            string file = Path.GetFileName(full);
            string dir = Path.GetDirectoryName(full);


            

            //create the "database" connection string
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;"
              + "Data Source=\"" + dir + "\\" + filenamenoextension + "\";";

            if (path.EndsWith(".xls"))
            {
                connString += "Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"";
            }
            else
            {
                //Assume xlsx
                connString += "Extended Properties=\"Excel 12.0 XML;HDR=YES;IMEX=1\"";
            }


            string Sheet1 = "";
            List<string> sheetnames = new List<string>();

            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                conn.Open();
                DataTable dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                foreach (DataRow row in dtSchema.Rows)
                {
                    if (!row["TABLE_NAME"].ToString().Contains("FilterDatabase"))
                    {
                        //sheetNames.Add(new SheetName() { sheetName = row["TABLE_NAME"].ToString(), sheetType = row["TABLE_TYPE"].ToString(), sheetCatalog = row["TABLE_CATALOG"].ToString(), sheetSchema = row["TABLE_SCHEMA"].ToString() });
                        Sheet1 = row["TABLE_NAME"].ToString();
                        Console.WriteLine("EXCEL: " + Sheet1);
                         Sheet1 = Sheet1.Replace("'", "");
                         Sheet1 = Sheet1.Replace("$", "");
                         sheetnames.Add(Sheet1); 

                    }
                }
                
                conn.Close();
            }


            return sheetnames;

        }



        // -------------------------------------------------------------
        // ParseExcel
        // Custom method to load a Excel into a DataTable
        // Params: path is the full path to the file.
        // ---------------------------------------------------------------
        public static DataTable ParseExcel(string path, string startrow, string sheetname)
        {
            if (!System.IO.File.Exists(path))
                return null;

            string full = Path.GetFullPath(path);

            string filenamenoextension = Path.GetFileNameWithoutExtension(path);
            string file = Path.GetFileName(full);
            string dir = Path.GetDirectoryName(full);

            //create the "database" connection string
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;"
              + "Data Source=\"" + dir + "\\" + filenamenoextension + "\";";

            if (path.EndsWith(".xls"))
            {
                connString += "Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"";
            }
            else
            {
                //Assume xlsx
                connString += "Extended Properties=\"Excel 12.0 XML;HDR=YES;IMEX=1\"";
            }


            string Sheet1 = "";
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                conn.Open();
                DataTable dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                foreach (DataRow row in dtSchema.Rows)
                {
                    if (!row["TABLE_NAME"].ToString().Contains("FilterDatabase"))
                    {
                        //sheetNames.Add(new SheetName() { sheetName = row["TABLE_NAME"].ToString(), sheetType = row["TABLE_TYPE"].ToString(), sheetCatalog = row["TABLE_CATALOG"].ToString(), sheetSchema = row["TABLE_SCHEMA"].ToString() });
                        Sheet1 = row["TABLE_NAME"].ToString();
                        break;
                    }
                }
                //Sheet1 = dtSchema.Rows[0]["TABLE_NAME"].ToString();
                Sheet1 = Sheet1.Replace("'", "");
                Sheet1 = Sheet1.Replace("$", "");
                conn.Close();
            }

            Console.WriteLine("Sheetname : " + Sheet1 + " vs asked for SheetName of " + sheetname);
            if (sheetname.Length < 1)
            {
                sheetname = Sheet1;
            }


            //create the database query
            string query = "SELECT * FROM [" + sheetname + "$A" + startrow + ":Z20000]";  // A2:ZZ will start reading from 2nd row.

            //create a DataTable to hold the query results
            DataTable dTable = new DataTable();

            //create an OleDbDataAdapter to execute the query
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(query, connString);



            try
            {
                //fill the DataTable
                dAdapter.Fill(dTable);
            }
            catch (InvalidOperationException e)
            { Console.Write(e.ToString()); }
            catch (Exception e)
            { Console.Write(e.ToString()); }

            dAdapter.Dispose();

            // Convert All columns to String
            //dc.DataType = System.Type.GetType(typeString); // System.String

            return dTable;
        }

        //-----------------------------------------------------------
        // Look the SKU
        // ---------------------------------------------------------

        private int lookupSKU(int curRow, string licenseName)
        {


            // Find Location in lookup
            DataRow[] foundRows1;
            string filter = "LICENSE = '" + licenseName.ToLower().Replace("'", "''") + "'";  // Not case sensitive


            Console.WriteLine(" Lookup SKU: License - " + licenseName);
            foundRows1 = this.inSkuMap.Select(filter);


            if (foundRows1.Length > 0)
            {

                Console.WriteLine(" Found - " + foundRows1[0]["SKUS"].ToString().Trim());
                this.inTPHistory.Rows[curRow]["SKU"] = foundRows1[0]["SKUS"].ToString().Trim();
                {
                    // Newly inserted. Return no current/orginal match.
                    return 1;
                }
            }
            return foundRows1.Length;

        }// End lookup SKU method


        // ---------------------------------------------
        // load the Location Lookup Validation information
        // ---------------------------------------------
        public int loadLocationLookup()
        {
            String query = " select ID = L.ID, Name = CONVERT(NVARCHAR(100),RTRIM(L.Name)),  InternalCode = CONVERT(NVARCHAR(100),RTRIM(L.InternalCode)) , New = 0\n" +
                        " from Location  L \n" +
                        " where L.CUstomerID = " + this.opts.getRuleValue("TrainingProCustomerID");

            this.inLocations = new DataTable();

            // Get the Data
            try
            {
         
                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

         
                // Load into DataTable
                this.inLocations.Load(myReader);



            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }


            return 0; // ok
        }
        //-----------------------------------------------------------
        // Lookup the Location
        // ---------------------------------------------------------
        private int lookupLocation(int curRow, string LocationCode, string LocationName)
        {


            // Find Location in lookup
            DataRow[] foundRows1;
            string filter = "InternalCode = '" + LocationCode.ToLower().Replace("'", "''") + "'";  // Not case sensitive

            foundRows1 = this.inLocations.Select(filter);

            // Auto Add the 
            if (foundRows1.Length < 1)
            {
                try
                {
                    // Add to lookup as New
                    DataRow newRow = this.inLocations.NewRow();
                    newRow["New"] = "1";
                    newRow["InternalCode"] = LocationCode;
                    if (LocationName.Length < 1)
                    {
                        newRow["Name"] = LocationCode;
                    }
                    else
                    {
                        newRow["Name"] = LocationName;
                    }
                    this.inLocations.Rows.Add(newRow);
                }
                catch (Exception e1)
                {
                    Console.WriteLine("LookupLocation Error. " + e1.Message + " ='" + LocationCode + "'");
                }
            }
            if (foundRows1.Length > 0)
            {
                if (foundRows1[0]["New"].ToString().CompareTo("1") == 0)
                {
                    // Newly inserted. Return no current/orginal match.
                    return 0;
                }
            }
            return foundRows1.Length;

        }// End lookup location method


        /// Training Program:
        /// 
        // ---------------------------------------------
        // load the Training Program Lookup Validation information
        // ---------------------------------------------
        public int loadTrainingProgramLookup()
        {
            String query = " select ID = CONVERT(NVARCHAR(20),RTRIM(TP.ID)),   CustomerID = CONVERT(NVARCHAR(20),RTRIM(TP.CustomerID)),\n" + 
                " Name = CONVERT(NVARCHAR(1000),RTRIM(TP.Name)),  InternalCode = CONVERT(NVARCHAR(100),RTRIM(TP.InternalCode)) , \n" +
                " SKU = CONVERT(NVARCHAR(100),RTRIM(TP.SKU)), New = 0, NewID = CONVERT(NVARCHAR(100),''),\n" +
                " OwnerID = CONVERT(NVARCHAR(100),TP.OwnerID),CategoryID = CONVERT(NVARCHAR(100),RTRIM(TP.CategoryID)), EndDate = TP.EndDate, TP.Cost, TP.Description \n" + 
                        " from TrainingProgram  as TP \n" +
                        " where TP.CUstomerID = " + this.opts.getRuleValue("TrainingProCustomerID") + "\n" +
                           " and TP.CategoryID =  " + this.opts.getRuleValue("TrainingProCategoryID");

            this.inTrainingPrograms = new DataTable();
            Console.WriteLine("Training Program Load: " + query);
            // Get the Data
            try
            {

                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();


                // Load into DataTable
                this.inTrainingPrograms.Load(myReader);



            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }
            foreach (System.Data.DataColumn col in this.inTrainingPrograms.Columns) col.ReadOnly = false; 

            return 0; // ok
        }
        //-----------------------------------------------------------
        // Lookup the Training Program
        // ---------------------------------------------------------
        private int lookupTrainingProgram(int curRow, string TrainingProgramName, string TrainingProgramID, string termDate, string hours )
        {


            // Find Location in lookup
            DataRow[] foundRows1;
            string filter = "Name = '" + TrainingProgramName.ToLower().Replace("'", "''") + "'";  // Not case sensitive

            foundRows1 = this.inTrainingPrograms.Select(filter);

            // Auto Add the 
            if (foundRows1.Length < 1)
            {
                try
                {
                    // Add to lookup as New
                    DataRow newRow = this.inTrainingPrograms.NewRow();
                    newRow["New"] = "1";
                    newRow["CustomerID"] =  this.opts.getRuleValue("TrainingProCustomerID");
                    newRow["OwnerID"] = this.opts.getRuleValue("TrainingProOwnerID");
                    newRow["InternalCode"] = TrainingProgramID;
                    newRow["Cost"] = hours;
                    newRow["EndDate"] = termDate;
                    newRow["Description"] = "History:  " + TrainingProgramID;
                    if (TrainingProgramName.Length < 1)
                    {
                        newRow["Name"] = TrainingProgramID;
                    }
                    else
                    {
                        newRow["Name"] = TrainingProgramName;
                    }
                    this.inTrainingPrograms.Rows.Add(newRow);
                }
                catch (Exception e1)
                {
                    Console.WriteLine("Lookup Training Program Error. " + e1.Message + " ='" + TrainingProgramName + "'");
                }
            }
            if (foundRows1.Length > 0)
            {
                if (foundRows1[0]["New"].ToString().CompareTo("1") == 0)
                {
                    // Newly inserted. Return no current/orginal match.
                    return 0;
                }
            }
            return foundRows1.Length;

        }// End lookup Training Program method


    }
}