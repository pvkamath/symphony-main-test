﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
namespace StudentImport
{


    // --------------------------------------------------------
    // Loader for JobRole Records
    //  1. 
    // --------------------------------------------------------
    public class JobRoleLoader
    {


        // Main Inbound JobRole information to be loaded
         DataTable inJobRoles = null;
        String customerID = "94";
        ArrayList errorMessages = null;
        SqlConnection db = null;
        DataTable oldJobRoles = null;
        string baseDirName = "";
        string programName = "Import";
        Queue jobQueue = null;
        Options opts = null;

        // Ignore stat checks
        Boolean ignoreWarnings = false; // Turn this on to stop reasonability checks on changes. (If there are a lot of changes )


        // Standard Stats
        StatInfo stats = null;


        // -------------------------------------------------------------
        // Constructor 
        // Parameter dt is the inbound file data loaded into a DataTable
        // -------------------------------------------------------------
        public JobRoleLoader(SqlConnection dbCon, Options optvalues, Queue q, DataTable dt )
        {

            this.opts = optvalues;
            this.inJobRoles = dt;
            this.customerID = this.opts.getCustomerID();
            this.db = dbCon;
            this.errorMessages = new ArrayList();
            this.baseDirName = this.opts.getBaseDirectory();
            this.jobQueue = q;
            this.stats = new StatInfo();
    
            
        }



        // ----------------------------------------------
        // Return the existing JobRole records.
        // ----------------------------------------------
        public DataTable getExistingRecords()
        {
            return this.oldJobRoles;
        }
        // ----------------------------------------------
        // Return the inbound/modified JobRole records.
        // ----------------------------------------------
        public DataTable getRecords()
        {
            return this.inJobRoles;
        }

        // ----------------------------------------------
        // Return the process statistics
        // ----------------------------------------------
        public StatInfo getStats()
        {
            return this.stats;
        }


         // -----------------------------------------------------------
        // sanitizeColumns
        // brief: Renames the column headers
        // to make sure column names are consistent.
        // 1. Change to consistent Case.
        // 2. Trims
        // 3. Adds in some processing columns
        // * This is the place to catch common misspellings if needed
        // (example:  name turns into Name, NAME turns into Name )
        // -----------------------------------------------------------
        public int sanitizeColumns()
        {
            for (int curCol = 0; curCol < inJobRoles.Columns.Count; curCol++)
            {
                String colName = inJobRoles.Columns[curCol].ColumnName.ToLower().Trim();
                if (colName.Equals("name"))
                {
                    inJobRoles.Columns[curCol].ColumnName = "Name";
                }
                if (colName.Equals("code"))
                {
                    inJobRoles.Columns[curCol].ColumnName = "Code";
                }
                if (colName.Equals("parentcode") || colName.Equals("pcode") ||
                    colName.Equals("parcode") || colName.Equals("perentcode") || colName.Equals("parent code")
                    )
                {
                    inJobRoles.Columns[curCol].ColumnName = "ParentCode";
                }

            }
          
            // Add isNew flag to dataTable
            // This will be used to mark which ones need to be inserted
            // ( These need to be inserted before the updates )

            this.inJobRoles = Utility.addColumnToDataTable(this.inJobRoles, "New", "System.String");
            // Add update flag to dataTable
            // This will be used to mark which ones need to be updated in the db
            this.inJobRoles = Utility.addColumnToDataTable(this.inJobRoles, "Update", "System.String");
            // Add update comment to dataTable
            // This will be used add notes or comments
            this.inJobRoles = Utility.addColumnToDataTable(this.inJobRoles, "UpdateMsg", "System.String");
            // Add existingID to dataTable
            // This will be used to mark what existing record got modified
            this.inJobRoles = Utility.addColumnToDataTable(this.inJobRoles, "UpdateID", "System.String");
            // Add Existing Name, Code, ParentJobRoleID to dataTable
            // This will be used to see what was updated or not updated
            this.inJobRoles = Utility.addColumnToDataTable(this.inJobRoles, "OldName", "System.String");
            this.inJobRoles = Utility.addColumnToDataTable(this.inJobRoles, "OldCode", "System.String");
            this.inJobRoles = Utility.addColumnToDataTable(this.inJobRoles, "OldParentID", "System.String");
            this.inJobRoles = Utility.addColumnToDataTable(this.inJobRoles, "OldParentCode", "System.String");
            // Add Error to dataTable
            // This will be used to note error messages
            this.inJobRoles = Utility.addColumnToDataTable(this.inJobRoles, "ErrorFg", "System.String");
            this.inJobRoles = Utility.addColumnToDataTable(this.inJobRoles, "Error", "System.String");
 
            //Default some values
            // Make sure values do not exceed 50 characters
            for (int curRow = 0; curRow < this.inJobRoles.Rows.Count; curRow++)
            {
                if (this.inJobRoles.Rows[curRow]["Name"].ToString().Length > 50)
                {
                    //Console.WriteLine("Defaulting the Status field to value 1.");
                    this.inJobRoles.Rows[curRow]["Name"] = this.inJobRoles.Rows[curRow]["Name"].ToString().Trim().Substring(0,50);
                }
                if (this.inJobRoles.Rows[curRow]["Code"].ToString().Length > 50)
                {
                    //Console.WriteLine("Defaulting the Status field to value 1.");
                    this.inJobRoles.Rows[curRow]["Code"] = this.inJobRoles.Rows[curRow]["Code"].ToString().Trim().Substring(0, 50);
                }
                if (this.inJobRoles.Rows[curRow]["ParentCode"].ToString().Length > 50)
                {
                    //Console.WriteLine("Defaulting the Status field to value 1.");
                    this.inJobRoles.Rows[curRow]["ParentCode"] = this.inJobRoles.Rows[curRow]["ParentCode"].ToString().Trim().Substring(0, 50);
                }
            } // end default loop

            return 0; // ok
        }

        // -----------------------------------------------------------
        // validateColumns
        // brief: Checks to make sure column information is available
        // return -1 if no Name
        //        -2 if no Code
        //        -3 if no ParentCode
        // -----------------------------------------------------------
        public int validateColumns()
        {

            if (!inJobRoles.Columns.Contains("Name"))
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("JobRoleLoader.validateColumns : Name column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: Name column not found.");
                    return -1;
            }
            if (!inJobRoles.Columns.Contains("Code"))
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("JobRoleLoader.validateColumns : Code column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: Code column not found.");
                return -2;
            }
            if (!inJobRoles.Columns.Contains("ParentCode"))
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("JobRoleLoader.validateColumns : ParentCode column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: ParentCode column not found.");
                return -3;
            }



            return 0; // ok
        }

        // ---------------------------------------------
        // load the JobRole information for the customer
        // from the database.
        // ---------------------------------------------
        public int loadJobRolesFromDB()
        {
            String query = " select ID = J.ID, Name = J.Name, InternalCode = J.InternalCode, \n" +
                        " ParentJobRoleID = J.ParentJobRoleID, ParentCode =  case when P.InternalCode is null then '*none*' else P.InternalCode end, \n" + 
                        " J.ModifiedBy, J.CreatedBy, J.ModifiedOn, J.CreatedOn \n" +
                        " from JobRole  J \n" + 
                        " left outer join JobRole as P on P.ID = J.ParentJobRoleID  \n"+
                        " where J.CUstomerID = " + this.customerID;
            this.oldJobRoles = new DataTable();

            // Get the Data
            try
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("JobRole Get Data: " + query);
                }
                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!opts.isSilent())
                {
                    Console.WriteLine("JobRole DB Data Loaded.");
                }
                // Load into DataTable
                this.oldJobRoles.Load(myReader);

                // Add some processing columns
                this.oldJobRoles = Utility.addColumnToDataTable(this.oldJobRoles, "Delete", "System.String");
                this.oldJobRoles = Utility.addColumnToDataTable(this.oldJobRoles, "Update", "System.String");


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }

            //Default some values
            for (int curRow = 0; curRow < this.oldJobRoles.Rows.Count; curRow++)
            {
                this.oldJobRoles.Rows[curRow]["Delete"] = "Yes";
            }


            return 0; // ok
        }



        // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int processChanges()
        {

            // Pre Process Changes to determine what to do. 
            // Execution of changes will occur next.
            // For each row
            //  1. find it in the existing db rows
            //  2.  Mark if it is new.
            //  3.  Error if it matches to more than one row.
            //  4.  Mark as update if it needs to change.

            this.stats.inbound_count = this.inJobRoles.Rows.Count;
            this.stats.existing_count = this.oldJobRoles.Rows.Count;
            int found_count = 0;

            for (int curRow = 0; curRow < this.inJobRoles.Rows.Count; curRow++)
            {
                // These are the new values on the file
                string JR_Code = this.inJobRoles.Rows[curRow]["Code"].ToString();
                string JR_Name = this.inJobRoles.Rows[curRow]["Name"].ToString();
                string JR_ParentCode = this.inJobRoles.Rows[curRow]["ParentCode"].ToString();
                if (JR_Code.Length > 50)
                {

                }

                // Default Some of the Metadata. 
                this.inJobRoles.Rows[curRow]["UpdateID"] = "";
                this.inJobRoles.Rows[curRow]["OldCode"] = "";
                this.inJobRoles.Rows[curRow]["OldName"] = "";
                this.inJobRoles.Rows[curRow]["OldParentID"] = "";
                this.inJobRoles.Rows[curRow]["OldParentCode"] = "";


                // Parent Code cannot point to same JobRole
                if (JR_ParentCode.CompareTo(JR_Code) == 0)
                {
                    JR_ParentCode = "0";
                    this.inJobRoles.Rows[curRow]["ParentCode"] = "0";
                }


                // Find existing Row using Code value ... if any
                DataRow[] foundRows;
                foundRows = this.oldJobRoles.Select("InternalCode = '" + JR_Code.Replace("'", "''") + "'");
                if (foundRows.Length < 1)
                {

                    if (JR_Name.Trim().ToString().Length < 1 || JR_Code.Trim().ToString().Length < 1)
                    { 
                        // Blank Row Error.
                        this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                        this.inJobRoles.Rows[curRow]["Error"] = "Name or Code cannot be blank.";
                        this.inJobRoles.Rows[curRow]["New"] = "0";
                        this.inJobRoles.Rows[curRow]["Update"] = "0";
                        this.stats.error_count++;
                        continue;
                    }
                    // Not Found. Mark as New
                    this.inJobRoles.Rows[curRow]["New"] = "1";

                    this.stats.insert_count++;
                }
                else if (foundRows.Length > 1)
                {
                    // Too many Matches. Error.
                    this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                    this.inJobRoles.Rows[curRow]["Error"] = "JobRole found too many existing record matches on InternalID.(" + foundRows.Length + ")";
                    this.inJobRoles.Rows[curRow]["New"] = "0";
                    this.stats.error_count++;
                    continue;
                }
                else
                {
                    // Mark existing record as found.
                    foundRows[0]["Delete"] = "No";
                    found_count++;

                    // Check for duplicates
                    DataRow[] dupRows;
                    dupRows = this.inJobRoles.Select("Code = '" + JR_Code.Replace("'", "''") + "'");
                    if (dupRows.Length > 1)
                    {
                        this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                        this.inJobRoles.Rows[curRow]["Error"] = "Duplicate Locations on Inbound File.(" + dupRows.Length + ")";
                        this.stats.error_count++;
                        continue;
                    }

                    // Update Might be needed. Compare the new values to the old values to make sure:

                    this.inJobRoles.Rows[curRow]["New"] = "0";
                    this.inJobRoles.Rows[curRow]["UpdateID"] = foundRows[0]["ID"].ToString();

                    string oldName = foundRows[0]["Name"].ToString();
                    string oldCode = foundRows[0]["InternalCode"].ToString();
                    string oldParentCode = foundRows[0]["ParentCode"].ToString();
                    string oldParentID = foundRows[0]["ParentJobRoleID"].ToString();

                    // Track the existing values for undo logic and logging.
                    this.inJobRoles.Rows[curRow]["OldCode"] = oldCode;
                    this.inJobRoles.Rows[curRow]["OldName"] = oldName;
                    this.inJobRoles.Rows[curRow]["OldParentID"] = oldParentID;
                    this.inJobRoles.Rows[curRow]["OldParentCode"] = oldParentCode;


                    Boolean hasChanged = false;
                    if (!oldName.Trim().Equals(JR_Name.Trim()))
                    {
                        this.inJobRoles.Rows[curRow]["Update"] = "1";
                        this.inJobRoles.Rows[curRow]["UpdateMsg"] = "Name !=" + oldName + ";";
                        hasChanged = true;
                    }
                    if (!oldCode.Trim().Equals(JR_Code.Trim()))
                    {
                        this.inJobRoles.Rows[curRow]["Update"] = "1";
                        this.inJobRoles.Rows[curRow]["UpdateMsg"] =  this.inJobRoles.Rows[curRow]["UpdateMsg"] + "InternalCode !=-" + oldCode + ";";
                        hasChanged = true;
                    }

                     // Zero? Zero as a Code or Zero as no Parent? 
                    // ToDO: check for a 0 Job Code

                    if (!JR_ParentCode.Equals("0") && JR_ParentCode.Trim().Length > 0 && !oldParentCode.Trim().Equals(JR_ParentCode.Trim()) )
                    {
                       
               

                        this.inJobRoles.Rows[curRow]["Update"] = "1";
                        this.inJobRoles.Rows[curRow]["UpdateMsg"] = this.inJobRoles.Rows[curRow]["UpdateMsg"] + "ParentCode !=-" + oldParentCode + ";";
                        hasChanged = true;
                    }
                    if (hasChanged)
                    {
                        this.stats.update_count++;
                    }

                }



          
            }

            this.stats.delete_count = this.stats.existing_count - found_count;


       

            return 0; //ok
        }

         // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int executeChanges()
        {

            // Insert new ones
            executeAdds();

            // Update primary fields
            executeUpdates();

            // Update lookup fields
            executeSecondaryUpdates();

            // Generate Delete Statements for Removals
            string deleteSQL = "";
            string undoDeleteSQL = "";
            
            for (int curRow = 0; curRow < this.oldJobRoles.Rows.Count; curRow++)
            {
                if (this.oldJobRoles.Rows[curRow]["Delete"].ToString().Equals("No"))
                {
                    continue;
                }
                string OldJR_ID = this.oldJobRoles.Rows[curRow]["ID"].ToString();
                string OldJR_Name = this.oldJobRoles.Rows[curRow]["Name"].ToString();
                string OldJR_InternalCode = this.oldJobRoles.Rows[curRow]["InternalCode"].ToString();
                string OldJR_ParentJobRoleID = this.oldJobRoles.Rows[curRow]["ParentJobRoleID"].ToString();
                string OldJR_ModifiedBy = this.oldJobRoles.Rows[curRow]["ModifiedBy"].ToString();
                string OldJR_CreatedBy = this.oldJobRoles.Rows[curRow]["CreatedBy"].ToString();
                string OldJR_ModifiedOn = this.oldJobRoles.Rows[curRow]["ModifiedOn"].ToString();
                string OldJR_CreatedOn = this.oldJobRoles.Rows[curRow]["CreatedOn"].ToString();

                string delete_s = " delete JobRole where CustomerID = " + this.customerID + " and ID = " + OldJR_ID.Replace("'", "''") + "\n";

                if (opts.isExecute() && opts.isDelete())
                {

                    try
                    {
                        SqlCommand myCommand = new SqlCommand(delete_s, this.db);
                        int rowcount = myCommand.ExecuteNonQuery();
                        if (rowcount < 1)
                        {
                            this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                            this.inJobRoles.Rows[curRow]["Error"] = this.inJobRoles.Rows[curRow]["Error"].ToString() + ", Delete Failed with rc:" + rowcount;
                            delete_s = "FAILED:" + delete_s;
                        }
                    }
                    catch (Exception )
                    {
                        this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                        this.inJobRoles.Rows[curRow]["Error"] = this.inJobRoles.Rows[curRow]["Error"].ToString() + ", Delete Failed.";
                        delete_s = "FAILED:" + delete_s;
                    }
                } // end db execute deletes
                deleteSQL += delete_s;
                undoDeleteSQL +=  " insert into JobRole ( Name, CustomerID, ParentJobRoleID, \n" +
                                " InternalCode, ModifiedBy, CreatedBy, \n" +
                                " ModifiedOn, CreatedOn )\n" +
                                " values ( '" + OldJR_Name.Replace("'", "''") + "', " + this.customerID + ", " + OldJR_ParentJobRoleID + " , \n" +
                                " '" + OldJR_InternalCode.Replace("'", "''") + "','" + OldJR_ModifiedBy + "','" + OldJR_CreatedBy + "', \n" +
                                " '" + OldJR_ModifiedOn + "', '" +OldJR_CreatedOn + "')\n";



            }
            // NEW FILE
            string fileFilter = " New = '1' ";
            string[] colNames = new string[3] {  "Name", "Code", "ParentCode" };
            Utility.SaveFilteredToCSV(this.inJobRoles, this.baseDirName + "z1_Ref_JobRoleNew.csv", fileFilter, colNames);

            // UPDATE FILE
             fileFilter = " Update = '1' ";
            string[] colNames2 = new string[3] { "Name", "Code", "ParentCode" };
            Utility.SaveFilteredToCSV(this.inJobRoles, this.baseDirName + "z2_Ref_JobRoleUpdate.csv", fileFilter, colNames2);


            // UNDO DELETE FILE: Generate an import file to reload the deleted JobRoles if needed.
             fileFilter = " Delete = 'Yes' ";
            string[] colNames3 = new string[9] { "ID", "Name", "InternalCode", "ParentCode", "ParentJobRoleID", "ModifiedBy", "ModifiedOn", "CreatedBy", "CreatedOn" };
            Utility.SaveFilteredToCSV(this.oldJobRoles, this.baseDirName + "z3_Undo_JobRoleDelete.csv", fileFilter, colNames3);



            Utility.SaveToFile(this.baseDirName + "u3_Job_DeleteSQL.txt", deleteSQL +
             "\n ----------------------------------------------- \n  Undo" +
            "\n ----------------------------------------------- \n " + undoDeleteSQL);

            
            return 0; //ok

        }
            

        // ---------------------------------------------------------
        // Update the primary fields
        // -----------------------------------------------------------
        private void executeUpdates()
        {

            string update1SQL = "";
            string undoUpdate1SQL = "";

            // Generate Update Statements for Changes
            for (int curRow = 0; curRow < this.inJobRoles.Rows.Count; curRow++)
            {

                if (!opts.isSilent() && curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("JobRole Update Scan " + curRow + " of " + this.inJobRoles.Rows.Count);
                }

                string JR_New = this.inJobRoles.Rows[curRow]["New"].ToString();
                string JR_Update = this.inJobRoles.Rows[curRow]["Update"].ToString();
                string JR_Code = this.inJobRoles.Rows[curRow]["Code"].ToString().Replace("'", "''");
                string JR_Name = this.inJobRoles.Rows[curRow]["Name"].ToString().Replace("'", "''"); 
                string JR_ParentCode = this.inJobRoles.Rows[curRow]["ParentCode"].ToString().Replace("'", "''");
                string JR_ID = this.inJobRoles.Rows[curRow]["UpdateID"].ToString(); // Existing JobRoleID  
                string OldName = this.inJobRoles.Rows[curRow]["OldName"].ToString().Replace("'", "''");  // Existing Name  
                string OldCode = this.inJobRoles.Rows[curRow]["OldCode"].ToString().Replace("'", "''"); // Existing Code  
                string OldParentID = this.inJobRoles.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
                string update_s = "";

                if (JR_Update.Equals("1") && !JR_New.Equals("1"))
                {


                    update_s = "Update JobRole set Name = '" + JR_Name.Trim() + "', InternalCode = '" + JR_Code + "',\n" +
                       " ModifiedBy = '" + this.programName + "', ModifiedOn = getdate() \n" +
                       " where ID = " + JR_ID + " and CustomerID = " + this.customerID + " \n\n";

                    // Update the DB if requested.    
                    if (opts.isExecute())
                    {

                        try
                        {
                            SqlCommand myCommand = new SqlCommand(update_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount != 1)
                            {
                                this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                                this.inJobRoles.Rows[curRow]["Error"] = this.inJobRoles.Rows[curRow]["Error"].ToString() + ", Update Failed with rc:" + rowcount;
                                update_s = "FAILED:" + update_s;
                            }
                        }
                        catch (Exception)
                        {
                            this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                            this.inJobRoles.Rows[curRow]["Error"] = this.inJobRoles.Rows[curRow]["Error"].ToString() + ", Update Failed.";
                            update_s = "FAILED:" + update_s;
                        }
                    } // end db execute

                    update1SQL += update_s;
                    undoUpdate1SQL += "Update JobRole set Name = '" + OldName.Trim().Replace("'", "''") + "', InternalCode = '" + OldCode.Replace("'", "''") + "',\n" +
                                  " ModifiedBy = ModifiedBy + ' rollback ' \n" +
                                  " where ID = " + JR_ID + " and CustomerID = " + this.customerID + " \n\n";

                }// end if update
                
            } //end main loop

                    // Update ParentJobRoleID after the 1st update, after the inserts, after the deletes.
                    // This prevents updating to the incorrect JobRoleID if there are duplicate Codes before
                    // all other actions have taken place. 


            Utility.SaveToFile(this.baseDirName + "u1_Job_Update1SQL.txt", update1SQL +
                 "\n ----------------------------------------------- \n  Undo" +
                 "\n ----------------------------------------------- \n " + undoUpdate1SQL);

        } // End executeUpdate()


        private void executeSecondaryUpdates()
        {

            string update2SQL = "";
            string undoUpdate2SQL = "";


            // Generate Update Statements for Changes
            for (int curRow = 0; curRow < this.inJobRoles.Rows.Count; curRow++)
            {


                if (!opts.isSilent() && curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("JobRole Secondary Update Scan " + curRow +" of " + this.inJobRoles.Rows.Count);
                }


                string JR_New = this.inJobRoles.Rows[curRow]["New"].ToString();
                string JR_Update = this.inJobRoles.Rows[curRow]["Update"].ToString();
                string JR_Code = this.inJobRoles.Rows[curRow]["Code"].ToString();
                string JR_Name = this.inJobRoles.Rows[curRow]["Name"].ToString();
                string JR_ParentCode = this.inJobRoles.Rows[curRow]["ParentCode"].ToString();
                string JR_ID = this.inJobRoles.Rows[curRow]["UpdateID"].ToString(); // Existing JobRoleID  
                string OldName = this.inJobRoles.Rows[curRow]["OldName"].ToString(); // Existing Name  
                string OldCode = this.inJobRoles.Rows[curRow]["OldCode"].ToString(); // Existing Code  
                string OldParentID = this.inJobRoles.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
                string update_s = "";

                if (JR_Update.Equals("1") || JR_New.Equals("1"))
                {


                    if (JR_ParentCode.Length > 0 && !JR_ParentCode.Equals("*none*"))
                    {

                        //FIX

                        //update_s = "Update J set J.ParentJobRoleID = case when P.ID is null then 0 else P.ID end \n" +
                         //  " from JobRole as J \n" +
                         //  " left outer join JobRole as P on P.ID = J.ParentJobRoleID \n" +
                         //   " where ID = " + JR_ID + " and CustomerID = " + this.customerID + "\n\n";

                        // This will only fail if the Parent code is not in the db.

                        update_s = "UPDATE J set J.ParentJobRoleID = ( select P.ID from JobRole P where P.CustomerID = " + this.customerID + "\n" +
               " and P.InternalCode = '" + JR_ParentCode.Replace("'", "''") + "' ) \n" +
               " from JobRole J where J.CustomerID = " + this.customerID + " and J.InternalCode = '" + JR_Code + "'\n";

                        // Secondary Update  the DB if requested.    
                        if (opts.isExecute())
                        {

                            try
                            {
                                SqlCommand myCommand = new SqlCommand(update_s, this.db);
                                int rowcount = myCommand.ExecuteNonQuery();
                                if (rowcount != 1)
                                {
                                    this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                                    this.inJobRoles.Rows[curRow]["Error"] = this.inJobRoles.Rows[curRow]["Error"].ToString() + ", Parent Update Failed with rc:" + rowcount;
                                    update_s = "FAILED:" + update_s;
                                }
                            }
                            catch (Exception)
                            {
                                this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                                this.inJobRoles.Rows[curRow]["Error"] = this.inJobRoles.Rows[curRow]["Error"].ToString() + ", Parent Update Failed.";
                                update_s = "FAILED:" + update_s;
                            }
                        } // End execute

                        update2SQL += update_s;
                        undoUpdate2SQL += "Update JobRole set ParentJobRoleID = " + OldParentID + ",\n" +
                        " where ID = " + JR_ID + " and CustomerID = " + this.customerID + " \n\n";

                    } // End secondary parent update
                }//End update check
            }// End main loop

            Utility.SaveToFile(this.baseDirName + "u4_Job_Update2SQL.txt", update2SQL +
            "\n ----------------------------------------------- \n  Undo" +
            "\n ----------------------------------------------- \n " + undoUpdate2SQL);
 
        } // End executeSecondaryUpdates()

        // ------------------------------------------------
        //  Add new JobRoles
        // ------------------------------------------------
        private void executeAdds()
        {
            string insertSQL = "";
            string undoInsertSQL = "";

            // Generate Update Statements for Changes
            for (int curRow = 0; curRow < this.inJobRoles.Rows.Count; curRow++)
            {

                if (!opts.isSilent() && curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("JobRole Add Scan " + curRow + " of " + this.inJobRoles.Rows.Count);
                }

                string JR_New = this.inJobRoles.Rows[curRow]["New"].ToString();
                string JR_Update = this.inJobRoles.Rows[curRow]["Update"].ToString();
                string JR_Code = this.inJobRoles.Rows[curRow]["Code"].ToString();
                string JR_Name = this.inJobRoles.Rows[curRow]["Name"].ToString();
                string JR_ParentCode = this.inJobRoles.Rows[curRow]["ParentCode"].ToString();
                string JR_ID = this.inJobRoles.Rows[curRow]["UpdateID"].ToString(); // Existing JobRoleID  
                string OldName = this.inJobRoles.Rows[curRow]["OldName"].ToString(); // Existing Name  
                string OldCode = this.inJobRoles.Rows[curRow]["OldCode"].ToString(); // Existing Code  
                string OldParentID = this.inJobRoles.Rows[curRow]["OldParentID"].ToString(); // Existing ParentID  
                string update_s = "";

                if (JR_New.Equals("1") ){

                    string insert_s =  " insert into JobRole ( Name, CustomerID, ParentJobRoleID, \n" +
                                " InternalCode, ModifiedBy, CreatedBy, \n" +
                                " ModifiedOn, CreatedOn )\n" +
                                " values ( '" + JR_Name.Replace("'", "''") + "', " + this.customerID + ", 0 , \n" +
                                " '" + JR_Code.Replace("'", "''") + "','" + this.programName + "','" + this.programName + "', \n" +
                                " getdate(), getdate() )\n\n";


                        // Insert into the DB if requested.    
                        if (opts.isExecute())
                        {

                            try
                            {
                                SqlCommand myCommand = new SqlCommand(insert_s, this.db);
                                int rowcount = myCommand.ExecuteNonQuery();
                                if (rowcount != 1)
                                {
                                    this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                                    this.inJobRoles.Rows[curRow]["Error"] = this.inJobRoles.Rows[curRow]["Error"].ToString() + ", Insert Failed with rc:" + rowcount;
                                    insert_s = "FAILED:" + insert_s;
                                }
                            }
                            catch (Exception )
                            {
                                this.inJobRoles.Rows[curRow]["ErrorFg"] = "1";
                                this.inJobRoles.Rows[curRow]["Error"] = this.inJobRoles.Rows[curRow]["Error"].ToString() + ", Insert Failed.";
                                insert_s = "FAILED:" + insert_s;
                            }
                        } // End db execute
                        insertSQL += insert_s;

                        undoInsertSQL += " delete JobRole where InternalCode = '" + JR_Code.Replace("'", "''") + "' \n" +
                        " and CustomerID = " + this.customerID + " and Name = '" + JR_Name.Replace("'", "''") + "'\n\n";
                    
                    } // End if new

                // Update ParentJobRoleID after the 1st update, after the inserts, after the deletes.
                // This prevents updating to the incorrect JobRoleID if there are duplicate Codes before
                // all other actions have taken place.

                
                } // End New/Insert Processing loop



            Utility.SaveToFile(this.baseDirName + "u2_Job_InsertSQL.txt", insertSQL +
             "\n ----------------------------------------------- \n  Undo" +
             "\n ----------------------------------------------- \n " + undoInsertSQL);

            } // End executeAdd()
    }
}