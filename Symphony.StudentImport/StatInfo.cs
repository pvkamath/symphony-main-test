﻿using System;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Collections;
namespace StudentImport
{
    public class StatInfo
    {

         public int existing_count = 0;
         public int inbound_count = 0;
         public int insert_count = 0;
         public int update_count = 0;
         public int delete_count = 0;
         public int error_count = 0;
         private string deleteTitle = "Delete";
         private Hashtable colCounts = null;

        public StatInfo()
        {
            colCounts = new Hashtable();
        }
        public void setDeleteTitle(string title)
        {
            this.deleteTitle = title;
        }

        public int addColumnCount(string columnName)
        {

            int i = 0;
            if (this.colCounts.ContainsKey(columnName))
            {
                i = (int)this.colCounts[columnName];
                i++;
                this.colCounts[columnName] = i;
            }
            else
            {
                this.colCounts.Add(columnName, 1);
            }
            return 0;
        }

        public string toString()
        {
            string outs = "-----------------------------------------" + Environment.NewLine +
                 "| Inbound Count: " + this.inbound_count.ToString() + Environment.NewLine +
                 "| Existing Count: " + this.existing_count.ToString() + Environment.NewLine +
                 "| Add Count: " + this.insert_count.ToString() + Environment.NewLine +
                 "| Update Count: " + this.update_count.ToString() + Environment.NewLine;


            foreach (DictionaryEntry entry in this.colCounts)
            {
                outs += "|      " + entry.Key + " updates = " + entry.Value + Environment.NewLine;
            }

            outs += "| " + this.deleteTitle + " Count: " + this.delete_count.ToString() + Environment.NewLine +
                 "| Warning Count: " + this.error_count.ToString() + Environment.NewLine +
                 "-----------------------------------------" + Environment.NewLine;

            return outs;
        }
        public string toShortString()
        {
            string outs = " In:" + this.inbound_count.ToString() + " " +
                 "Add: " + this.insert_count.ToString() + " " +
                 "Update: " + this.update_count.ToString() + " " +
                 "Warn: " + this.error_count.ToString() + " ";

            return outs;
        }

    }
}