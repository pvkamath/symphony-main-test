﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Net;


namespace StudentImport
{
    class Program
    {

        enum ProcessingStatus
        {
            Pending = 0,
            Processing = 1,
            Complete = 2
        };

        // --------------------------------------------------------
        //  Standard Import
        //  1. JobRole Import CSV File Processing
        //  2. Location Import CSV File Processing
        //  3. Employee Import CSV File Processing
        //  4. Audience Import CSV File processing
        //  5. Order Import CSV File processing
        //
        //  Paramters:
        //      -D BaseDirectory   ( where the files are to be processed )
        //      -J,-L,-E ,- A , -O ( The type of imports to be processed )
        //      -X ( run with commits. Otherwise it is just outputting the updates to files )
        //      -Delete ( full file import, delete anything not on the file. Not typical )
        //      -Q ##( update a db job queue entry for this run )
        //  Special Comments:
        //  JobRole and Location files should be processed before EMployee Files since
        //  employees can have new JobRoles or Locations. 
        //  If employees have JobRoles or Locations that are not in the database, they
        //  will fall out of this process.
        //  the file can be reran once the new JobRoles and Locations are in place.
        //  ** Currently this process does not add reporting or admin Permissions
        //  * Workaround is that this process will export a nice CSV file called a1_Emp_add.csv to load new employee information.
        //  
        //  ** Process will report on employees that are in the db, but not in the file.
        //  *  employees will note be marked as deleted in the database. No deletes ( less risk of mass destruction )
        // --------------------------------------------------------

        static int Main(string[] args)
        {
            try
            {
                int errorCode = 0;
                string baseDirName = "";
                string CustomerID = "";
                string queueID = "";
                string username = "";
                string orderFile = String.Empty;
                bool runJobRole = false;
                bool runLoc = false;
                bool runEmp = false;
                bool runAudience = false;
                bool runOrder = false;
                bool runTPHistory = false;
                bool execute = false;
                bool delete = false;
                bool silent = false;
                bool sessions = false;
                
                bool runBulkOrder = false;
                DataTable myDataTable = new DataTable();

                // --------------------------------------------------
                // Simple Command Line
                // --------------------------------------------------
                for (int ai = 0; ai < args.Length; ai++)
                {
                    // Special case when the application is called via the BulkUploadService.
                    if(args[ai].CompareTo("-QOS") == 0)
                    {
                        runBulkOrder = true;
                        Program.ProcessOrderFileBulkOrder();
                        #region TestCode
                        Process pr = new Process();
                        pr.StartInfo.FileName = @"C:\BULK Upload\BulkUploadProcessing\BulkUploadProcessingTestApp\bin\Debug\BulkUploadProcessingTestApp.exe";
                        pr.Start();
                        #endregion

                        return 1;                      
                    }
                    
                    else if (args[ai].CompareTo(" - Q") == 0 && (ai + 1) < args.Length)
                    {
                        queueID = args[ai + 1];
                        silent = true;
                    }
                    else if (args[ai].CompareTo("-C") == 0 && (ai + 1) < args.Length)
                    {
                        CustomerID = args[ai + 1];
                        ai = ai + 1;
                        
                    }
                    else if (args[ai].CompareTo("-D") == 0 && (ai + 1) < args.Length)
                    {
                        baseDirName = args[ai + 1];
                        ai += 1;
                        
                    }
                    else if (args[ai].CompareTo("-J") == 0)
                    {
                        runJobRole = true;
                    }
                    else if (args[ai].CompareTo("-L") == 0)
                    {
                        runLoc = true;
                    }
                    else if (args[ai].CompareTo("-E") == 0)
                    {
                        runEmp = true;
                    }
                    else if (args[ai].CompareTo("-A") == 0)
                    {
                        runAudience = true;
                    }
                    else if (args[ai].CompareTo("-O") == 0)
                    {
                        runOrder = true;
                        orderFile = args[ai + 1];
                        ai += 1;
                    }
                    else if (args[ai].CompareTo("-TPH") == 0)
                    {
                        runTPHistory = true;
                    }
                    else if(args[ai].CompareTo("-X") == 0)
                    {
                        execute = true;
                    }
                    else if(args[ai].CompareTo("-Delete") == 0)
                    {
                        delete = true;
                    }
                    else if(args[ai].CompareTo("-Silent") == 0)
                    {
                        silent = true;
                    }
                    else if(args[ai].CompareTo("-Sessions") == 0)
                    {
                        sessions = true;
                    }
                    else if(args[ai].CompareTo("-U") == 0 && (ai + 1) < args.Length)
                    {
                        username = args[ai + 1];
                    }
                }

                if (!runBulkOrder)
                {

                    if (args.Length < 1 || (!runEmp && !runJobRole && !runLoc && !runAudience && !runOrder && !runTPHistory) || CustomerID.Length < 1 || baseDirName.Length < 1)
                    {
                        Console.WriteLine("Import Program needs a correct command line");
                        Console.WriteLine("usages:  -C <customerId> -D <baseDirName> { -J (Run JobRole) -L (Run Location) -E (Run Employee) -A (run Audience) -O (run Order ) -TPH (run TP History) -X (execute) -Delete (Process removals) -Silent (no concole output) }");
                        Console.WriteLine("example:  Import -C 80 -D 'C:\\Dev\\ThunkData\\nbt\\' -J -L -E -A -X");

                        return -1;
                    }
                }

                if (sessions)
                {
                    Symphony.Core.Controllers.UserController userController = new Symphony.Core.Controllers.UserController();
                    if (!userController.LoginBatch(CustomerID, username))
                    {
                        Console.WriteLine("In order to generate sessions for orders, you must provide a username:");
                        Console.WriteLine("-U <Username>");
                        Console.WriteLine("The username must reference a valid user in the passed in Customer ID");
                        return -1;
                    }
                }


                // --------------------------------------------------
                // Load Application then Customer Options ( app.config )
                // --------------------------------------------------

                Options opts = new Options(); // This class is used to store the options and configurations

                if (!baseDirName.EndsWith("\\"))
                {
                    baseDirName += "\\";
                }

                opts.setBaseDirectory(baseDirName);
                opts.setCustomerID(CustomerID);
                opts.setExecute(execute);
                opts.setDelete(delete);
                opts.setSilent(silent);
                opts.setSessions(sessions);
                opts.setUsername(username);

                if (queueID.Length > 0)
                {
                    opts.setJobID(queueID);
                }
                opts.setJobType("0");
                if (runEmp)
                {
                    opts.setJobType("1");
                }
                if (runJobRole)
                {
                    opts.setJobType("2");
                }
                if (runLoc)
                {
                    opts.setJobType("3");
                }
                if (runAudience)
                {
                    opts.setJobType("4");
                }
                if (runOrder)
                {
                    opts.setJobType("5"); // Training Pro Order File
                }
                // -----------------------------------------------------------
                // Load the General Application Configuration
                //
                // -----------------------------------------------------------

                string environment = ConfigurationManager.AppSettings["Environment"];
                Console.WriteLine("Environment setting = " + environment);
                opts.setEnvironment(environment);
                Console.WriteLine(opts.getEnvironment());

                Console.WriteLine(ConfigurationManager.ConnectionStrings[opts.getEnvironment() + "DBConnection"]);
                opts.setConnectionString(ConfigurationManager.ConnectionStrings[opts.getEnvironment() + "DBConnection"].ConnectionString);

                // Load other/future base config values here.
                string JobRoleFile = ConfigurationManager.AppSettings["JobRoleFile"];
                string LocationFile = ConfigurationManager.AppSettings["LocationFile"];
                string EmployeeFile = ConfigurationManager.AppSettings["EmployeeFile"];
                string AudienceFile = ConfigurationManager.AppSettings["AudienceFile"];
                string OrderFile = "";
                if (String.IsNullOrEmpty(orderFile))
                {
                    OrderFile = ConfigurationManager.AppSettings["OrderFile"];
                }
                else
                {
                    OrderFile = orderFile;
                }

                // Check to see if output show go to console or to log (LogConsole = Yes  means write it to a file )
                string LogConsole = ConfigurationManager.AppSettings["LogConsole"];
                bool logTheConsole = false;
                FileStream ostrm;
                StreamWriter writer;
                TextWriter oldOut = Console.Out;
                String filename_start = "";
                if (runEmp)
                {
                    filename_start = "user_";
                }
                else if (runJobRole)
                {
                    filename_start = "job_";
                }
                else if (runLoc)
                {
                    filename_start = "loc_";
                }
                Console.WriteLine("baseDirName : " + baseDirName);
                Console.WriteLine("filename_start : " + filename_start);
                Console.WriteLine(baseDirName + filename_start + "output.txt");
                ostrm = new FileStream(baseDirName + filename_start + "output.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);

                // If it is configured to log to file, or if the silent option is passed in.
                // Log the console output to a file.
                if (LogConsole.ToLower().Equals("yes") || opts.isSilent())
                {
                    logTheConsole = true;
                    try
                    {
                        logTheConsole = true;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Cannot open output.txt for logging");
                        Console.WriteLine(e.Message);
                        return -1;
                    }
                    Console.SetOut(writer);
                }

                // Load all the configuration as 'Rules' into the option class.
                // this allows the program to get a custom rule value:
                // Rule_1_PasswordUpdate = false     for example
                string[] appRuleKeys = ConfigurationManager.AppSettings.AllKeys;
                for (int ai = 0; ai < appRuleKeys.Length; ai++)
                {
                    opts.addRuleValue(appRuleKeys[ai], ConfigurationManager.AppSettings[appRuleKeys[ai]]);
                }
                Console.WriteLine("Base Settings:");
                Console.WriteLine(opts.toString());

                //string configFileName =  "..\\..\\config\\" + CustomerID.Trim() + ".App.config"
                string configFileName = ConfigurationManager.AppSettings["ClientConfigFileBasepath"] + CustomerID.Trim() + ".App.config";


                //Check for the custom file. Not required.
                if (!File.Exists(configFileName))
                {
                    Console.WriteLine("No Customer Configuration File.");
                }
                else
                {
                    // ---------------------------------------------------------------
                    // Load Client Specific Configuration
                    // Dynamically loaded app.config file.
                    // <customerid>.App.config
                    // ---------------------------------------------------------------



                    ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
                    configMap.ExeConfigFilename = configFileName;
                    Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);

                    Console.WriteLine("Config Path: " + config.FilePath);


                    AppSettingsSection section = (AppSettingsSection)config.GetSection("appSettings");
                    if (section == null)
                    {
                        Console.WriteLine("Configuration File: appSettings section is null.");
                        return -1;
                    }

                    JobRoleFile = section.Settings["JobRoleFile"].Value;
                    LocationFile = section.Settings["LocationFile"].Value;
                    EmployeeFile = section.Settings["EmployeeFile"].Value;
                    AudienceFile = section.Settings["AudienceFile"].Value;

                    // Load other/future client specific fields here.

                    string[] ruleKeys = section.Settings.AllKeys;
                    for (int ai = 0; ai < ruleKeys.Length; ai++)
                    {
                        opts.addRuleValue(ruleKeys[ai], section.Settings[ruleKeys[ai]].Value);
                    }

                    Console.WriteLine("Customer Settings:");
                    Console.WriteLine(opts.toString());

                } // End custom config file

                opts.setJobFileName(JobRoleFile);
                opts.setLocationFileName(LocationFile);
                opts.setEmployeeFileName(EmployeeFile);
                opts.setAudienceFileName(AudienceFile);
                opts.setOrderFileName(OrderFile);


                Console.WriteLine(" JobRoleFile: " + opts.getJobFileName());
                Console.WriteLine(" LocationFile: " + opts.getLocationFileName());
                Console.WriteLine(" EmployeeFile: " + opts.getEmployeeFileName());
                Console.WriteLine(" AudienceFile: " + opts.getAudienceFileName());
                Console.WriteLine(" OrderFile: " + opts.getOrderFileName());


                // --------------- Update Job Q --------------------

                Queue jobQueue = new Queue();

                if (opts.getJobID().Length > 0)
                {
                    // DB Job Queue is being used.

                    SqlConnection myQConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[environment + "QueueDBConnection"].ConnectionString);
                    // make the DB Connection
                    try
                    {
                        Console.WriteLine("Open Database Connection.");
                        myQConnection.Open();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        jobQueue.failJob(opts.getJobID(), "System DB Error.");
                        return -1;
                    }

                    // Provide the Queue class with the connection
                    jobQueue.setDB(myQConnection);
                    jobQueue.startJob(opts.getJobID(), opts);
                }

                /*
                string baseDirName = @"C:\Dev\ThunkData\baylake\";
                string JobRoleFile = @"1.csv";
                string LocationFile = @"2.csv";
                string EmployeeFile = @"baylake_email.csv";
                string CustmerID = @"68";
          
            
                string baseDirName = @"C:\Dev\ThunkData\isabella\2010_05_05\";
                string JobRoleFile = @"4594_jobrole.csv";
                string LocationFile = @"4594_loc.csv";
                string EmployeeFile = @"4594_emp.csv";
                string CustmerID = @"95";

                 */




                // ------------------------------------------------------------------
                // Process the Data
                // -------------------------------------------------------------------
                try
                {
                    if (runJobRole)
                    {

                        errorCode = Program.ProcessJobRoleFile(opts, jobQueue);
                        if (errorCode < 0)
                        {
                            //jobQueue.failJob(opts.getJobID(), "JobRole Process Failed. Code: " + errorCode);
                            Console.WriteLine("JobRole Processing Failed! ABORT. " + errorCode);
                            return errorCode;
                        }
                    }

                    if (runLoc)
                    {
                        errorCode = Program.ProcessLocationFile(opts, jobQueue);
                        if (errorCode < 0)
                        {
                            //jobQueue.failJob(opts.getJobID(), "Location Process Failed. Code: " + errorCode);
                            Console.WriteLine("Location Processing Failed! ABORT. " + errorCode);
                            return errorCode;
                        }
                    }

                    if (runEmp)
                    {
                        errorCode = Program.ProcessEmployeeFile(opts, jobQueue);
                        if (errorCode < 0)
                        {
                            //jobQueue.failJob(opts.getJobID(), "Employee Process Failed. Code: " + errorCode);
                            Console.WriteLine("Employee Processing Failed! ABORT. " + errorCode);
                            return errorCode;
                        }
                    }
                    if (runAudience)
                    {
                        errorCode = Program.ProcessAudienceFile(opts, jobQueue);
                        if (errorCode < 0)
                        {
                            //jobQueue.failJob(opts.getJobID(), "Employee Process Failed. Code: " + errorCode);
                            Console.WriteLine("Audience Processing Failed! ABORT. " + errorCode);
                            return errorCode;
                        }
                    }
                    if (runOrder)
                    {
                        
                        errorCode = Program.ProcessOrderFile(opts, jobQueue);
                        if (errorCode < 0)
                        {
                            //jobQueue.failJob(opts.getJobID(), "Employee Process Failed. Code: " + errorCode);
                            Console.WriteLine("Order Processing Failed! ABORT. " + errorCode);
                            return errorCode;
                        }
                    }
                    if (runTPHistory)
                    {
                        errorCode = Program.ProcessTPHistoryFile(opts, jobQueue);
                        if (errorCode < 0)
                        {
                            //jobQueue.failJob(opts.getJobID(), "Employee Process Failed. Code: " + errorCode);
                            Console.WriteLine("TP History Processing Failed! ABORT. " + errorCode);
                            return errorCode;
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unhandled internal error occurred. " + e);
                }

                writer.Close(); // Clean up console/log file

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
                Console.WriteLine(ex.StackTrace);
                return -1;
            }
        }

        // ---------------------------------------------------------------------
        // Process Job Role Files
        //
        // ---------------------------------------------------------------------

        static int ProcessJobRoleFile(Options opts, Queue jobQueue){

            string baseDir = opts.getBaseDirectory();
            string JobRoleFileName = opts.getJobFileName();
            string customerID = opts.getCustomerID();



            Console.WriteLine("==============================================");
            Console.WriteLine("JobRole File Processing (" + opts.getEnvironment() + ") :");
            Console.WriteLine("==============================================");
            Console.WriteLine("Parse File: " + baseDir + JobRoleFileName);
            Utility.ConstructSchema(baseDir, JobRoleFileName);
            DataTable inDT = Utility.ParseCSV(baseDir + JobRoleFileName);

            jobQueue.setJobStatus(opts.getJobID(), "Parsed File");
            if (inDT == null)
            {
                Console.WriteLine("File could not be Parsed.");
                return -1;
            }


            // Make Database Connection ( add to config file later )
            //SqlConnection myConnection = new SqlConnection("user id=Cdwizard;password=w1z4rd;server=192.168.4.170;Trusted_Connection=no;database=CustomerBlackBox;connection timeout=30");
            //string myConString = ConfigurationManager.ConnectionStrings["ProdDBConnection"].ConnectionString;
            //Console.WriteLine("Conn: " + myConString);
            SqlConnection myConnection = new SqlConnection(opts.getConnectionString());

            // make the DB Connection
            try
            {
                Console.WriteLine("Open Database Connection.");
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }

            // ----------------------------------------------------
            // JobRole Processing
            //  
            // ----------------------------------------------------
          
            Console.WriteLine("Initilizing JobRole Loader.");
            
            JobRoleLoader jobs = new JobRoleLoader(myConnection, opts, jobQueue, inDT);
 

            // Sanitize/Scrub the headers
            Console.WriteLine("JobRole Sanitize Input");
            try
            {
                if (jobs.sanitizeColumns() != 0)
                {
                    Console.WriteLine("Abort! Sanitized Columns Failed. No Exception Thrown.");
                    myConnection.Close(); 
                    return -1; // failed
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Abort! Sanitized Columns Failed:" + e.ToString());
                try { myConnection.Close(); }
                catch (Exception) {  }
                return -1;
            }

            // Validate Required Fields
            Console.WriteLine("JobRole Validate Input");
            if( jobs.validateColumns() != 0 ){
                Console.WriteLine("Abort! JobRole Validate Columns Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Get the Existing Records from the DB
            Console.WriteLine("JobRole Load Existing Jobs");
            jobQueue.setJobStatus(opts.getJobID(), "Loading Existing JobRoles");
            if (jobs.loadJobRolesFromDB() != 0)
            {
                Console.WriteLine("Abort! JobRole Load Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Determine Inserts,Updates, Deletes
            Console.WriteLine("Pre Processs Changes");
            jobQueue.setJobStatus(opts.getJobID(), "Pre-process the Changes");
            if( jobs.processChanges() != 0 ){
                Console.WriteLine("Abort! JobRole Process Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            //Utility.PrintDataTable(inDT);
            //Utility.PrintDataTable(jobs.getExistingRecords());
            Console.WriteLine("Save data to files at: " + baseDir);
            Utility.SaveToCSV(jobs.getRecords(), baseDir + "JobRoleInboundData.csv", 0, 0);
            Utility.SaveToCSV(jobs.getExistingRecords(), baseDir + "JobRoleExistingData.csv", 0, 0);


            // Generate Scripts or Execute Changes
            Console.WriteLine("Execute Changes. Generated Scripts at: " + baseDir );
            jobQueue.setJobStatus(opts.getJobID(), "Execute Changes");
            if (jobs.executeChanges() != 0)
            {
                Console.WriteLine("Abort! JobRole Execute Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            jobQueue.setJobStatus(opts.getJobID(), "Finalize");
            // Print out the Counts/Stats
            StatInfo stats = jobs.getStats();
            string finishStatusMsg = "Completed.";
            if (!opts.isExecute())
            {
                finishStatusMsg = "Preview.";
            }
            jobQueue.setFinishMessage(finishStatusMsg + stats.toShortString()); // Sets file stat counts on the 'Completed' status message.
            Console.WriteLine(stats.toString());

            // Set the Job Queue as finished if used.
            if (opts.getJobID().Length > 0)
            {
                jobQueue.finishJob(opts.getJobID(), opts);

            }

            // Clean up . 
            try
            {
                myConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("DB Close Error. " + e.ToString());
                return -1;
            }

            return 0; // ok

        }

        // ---------------------------------------------------------------------
        // Process Location Files
        //
        // ---------------------------------------------------------------------

        static int ProcessLocationFile(Options opts, Queue jobQueue)
        {

            string baseDir = opts.getBaseDirectory();
            string locationFileName = opts.getLocationFileName();
            string customerID = opts.getCustomerID();

            Console.WriteLine("==============================================");
            Console.WriteLine("Location File Processing:");
            Console.WriteLine("==============================================");
            Console.WriteLine("Parse File: " + baseDir + locationFileName);

            // This call creates a Schema.ini file that the loader uses to 
            // make sure all the columns are loaded as Strings.
            Utility.ConstructSchema(baseDir, locationFileName);

            // Load the data
            DataTable inDT = Utility.ParseCSV(baseDir + locationFileName);

            jobQueue.setJobStatus(opts.getJobID(), "Parsed File");
            if (inDT == null)
            {
                Console.WriteLine("FAILED! Parse Error. No File?");
                return -1;
            }


            // Make Database Connection ( add to config file later )
            //SqlConnection myConnection = new SqlConnection("user id=Cdwizard;password=w1z4rd;server=192.168.4.170;Trusted_Connection=no;database=CustomerBlackBox;connection timeout=30");
            SqlConnection myConnection = new SqlConnection(opts.getConnectionString());

            // make the DB Connection
            try
            {
                Console.WriteLine("Open Database Connection.");
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }

            // ----------------------------------------------------
            // JobRole Processing
            //  
            // ----------------------------------------------------

            Console.WriteLine("Initilizing Location Loader.");

            LocationLoader locations = new LocationLoader(myConnection,opts,jobQueue, inDT);


            // Sanitize/Scrub the headers
            Console.WriteLine("Location Sanitize Input");
            try
            {
                if (locations.sanitizeColumns() != 0)
                {
                    Console.WriteLine("Abort! Location Sanitized Columns Failed. No Exception Thrown.");
                    myConnection.Close();
                    return -1; // failed
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Abort! Location Sanitized Columns Failed:" + e.ToString());
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Validate Required Fields
            Console.WriteLine("Location Validate Input");
            if (locations.validateColumns() != 0)
            {
                Console.WriteLine("Abort! Location Validate Columns Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Get the Existing Records from the DB
            Console.WriteLine("Location Load Existing ");
            jobQueue.setJobStatus(opts.getJobID(), "Load existing Locations");
            if (locations.loadLocationFromDB() != 0)
            {
                Console.WriteLine("Abort! Location Load Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Determine Inserts,Updates, Deletes
            Console.WriteLine("Location Pre Processs Changes");
            jobQueue.setJobStatus(opts.getJobID(), "Pre-process the changes");
            if (locations.processChanges() != 0)
            {
                Console.WriteLine("Abort! Location Process Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            //Utility.PrintDataTable(inDT);
            //Utility.PrintDataTable(jobs.getExistingRecords());
            Console.WriteLine("Save data to files at: " + baseDir);
            Utility.SaveToCSV(locations.getRecords(), baseDir + "LocationInboundData.csv", 0, 0);
            Utility.SaveToCSV(locations.getExistingRecords(), baseDir + "LocationExistingData.csv", 0, 0);


            // Generate Scripts or Execute Changes
            Console.WriteLine("Execute Changes. Generated Scripts at: " + baseDir);
            jobQueue.setJobStatus(opts.getJobID(), "Execute Changes");
            if (locations.executeChanges() != 0)
            {
                Console.WriteLine("Abort! Location Execute Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }
            jobQueue.setJobStatus(opts.getJobID(), "Finalize");
            // Print out the Counts/Stats
            StatInfo stats = locations.getStats();
            Console.WriteLine(stats.toString());
            string finishStatusMsg = "Completed.";
            if (!opts.isExecute())
            {
                finishStatusMsg = "Preview.";
            }
            jobQueue.setFinishMessage(finishStatusMsg + stats.toShortString()); // Sets file stat counts on the 'Completed' status message.
            
   

            // Set the Job Queue as finished if used.
            if (opts.getJobID().Length > 0)
            {
                jobQueue.finishJob(opts.getJobID(), opts);

            }

            // Clean up . 
            try
            {
                myConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Location DB Close Error. " + e.ToString());
                return -1;
            }

            return 0; // ok

        }

        // ---------------------------------------------------------------------
        // Process Employee Files
        //
        // ---------------------------------------------------------------------

        static int ProcessEmployeeFile(Options opts, Queue jobQueue)
        {
            string baseDir = opts.getBaseDirectory();
            string employeeFileName = opts.getEmployeeFileName();
            string customerID = opts.getCustomerID();

            Console.WriteLine("==============================================");
            Console.WriteLine("Employee File Processing:");
            Console.WriteLine("==============================================");
            Console.WriteLine("Parse File: " + baseDir + employeeFileName);

            // Load the data
            /**Move load into the Loader class : BC 2/20/2012
             *  References to the DataTable are not working nicely this way.

            // This call creates a Schema.ini file that the loader uses to 
            // make sure all the columns are loaded as Strings.
            Utility.ConstructSchema(baseDir, employeeFileName);

            DataTable inDT = Utility.ParseCSV(baseDir + employeeFileName);
            jobQueue.setJobStatus(opts.getJobID(), "Parsed File");

            if (inDT == null)
            {
                Console.WriteLine("FAILED! Parse Error. No File?");
                return -1;
            }
             **/


            // Make Database Connection ( add to config file later )
            //SqlConnection myConnection = new SqlConnection("user id=Cdwizard;password=w1z4rd;server=192.168.4.170;Trusted_Connection=no;database=CustomerBlackBox;connection timeout=30");
            SqlConnection myConnection = new SqlConnection(opts.getConnectionString());

            // make the DB Connection
            try
            {
                Console.WriteLine("Open Database Connection.");
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }

            // ----------------------------------------------------
            // Employee Processing
            //  
            // ----------------------------------------------------

            Console.WriteLine("Initilizing Employee Loader.");

            EmployeeLoader employees = new EmployeeLoader(myConnection, opts,jobQueue);


           if(opts.getRuleValue("Rule_Use_Excel_Emp").ToLower().Equals("yes")){

               //Use not so standard Excel format
               System.Console.WriteLine("Use Excel as a file format. Rule_Use_Excel_Emp = yes");

               // This allows us to skip some of the first rows if they contain junk data, like titles. This will skip x rows to the header row.
               string startrow = opts.getRuleValue("Rule_Excel_Emp_StartRow");
               if (startrow.Length < 1)
               {
                   startrow = "1";
               }

               string sheetname = opts.getRuleValue("Rule_Excel_Emp_SheetName");
               if (sheetname.Length < 1)
               {
                   sheetname = "";
               }



               if (employees.loadInboundExcelFile(baseDir, employeeFileName, startrow, sheetname) < 0)
               {
                   Console.WriteLine("Abort! Employee Excel File Load Failed.");
                   myConnection.Close();
                   return -1; // failed
               }

           }else{

                // Use Standard CSV File

                if (employees.loadInboundCSVFile(baseDir, employeeFileName) < 0)
                {
                    Console.WriteLine("Abort! Employee CSV File Load Failed.");
                    myConnection.Close();
                    return -1; // failed
                }

           }
            // Sanitize/Scrub the headers
            Console.WriteLine("Employee Sanitize Input");
            try
            {
                if (employees.sanitizeColumns() != 0)
                {
                    Console.WriteLine("Abort! Employee Sanitized Columns Failed. No Exception Thrown.");
                    myConnection.Close();
                    return -1; // failed
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Abort! Employee Sanitized Columns Failed:" + e.ToString());
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Validate Required Fields
            Console.WriteLine("Employee Validate Input");
            if (employees.validateColumns() != 0)
            {
                Console.WriteLine("Abort! Employee Validate Columns Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Get the Existing Records from the DB
            Console.WriteLine("Employee Load Existing Employees");
            if (employees.loadEmployeeFromDB() != 0)
            {
                Console.WriteLine("Abort! Employee Load Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }


            // Get the Existing Records from the DB
            Console.WriteLine("Employee Load Existing ");
            jobQueue.setJobStatus(opts.getJobID(), "Loading existing employees");
            if (employees.loadLocationLookup() != 0)
            {
                Console.WriteLine("Abort! Employee Location Load Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }


            // Get the Existing Records from the DB
            Console.WriteLine("Employee Load Existing JobRoles");
            jobQueue.setJobStatus(opts.getJobID(), "Loading secondary look up values");
            if (employees.loadJobRoleLookup() != 0)
            {
                Console.WriteLine("Abort! Employee JobRole Load Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Get the Existing Audience Records from the DB
            Console.WriteLine("Employee Load Existing Audience");
            jobQueue.setJobStatus(opts.getJobID(), "Loading secondary look up values");
            if (employees.loadAudienceLookup() != 0)
            {
                Console.WriteLine("Abort! Employee Audience Load Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Determine Inserts,Updates, Deletes
            Console.WriteLine("Employee Pre Processs Changes");
            jobQueue.setJobStatus(opts.getJobID(), "Pre-processing the changes");
            if (employees.processChanges() != 0)
            {
                Console.WriteLine("Abort! Employee Process Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

   


            // Generate Scripts or Execute Changes
            Console.WriteLine("Executing Changes. Generated Scripts at: " + baseDir);
            jobQueue.setJobStatus(opts.getJobID(), "Executing the changes");
            if (employees.executeChanges() != 0)
            {
                Console.WriteLine("Abort! Employee Execute Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }


            Console.WriteLine("Save data to files at: " + baseDir);

            // Remove OldPassword Fields before save
            // TODO: Add this as a configurable option
            DataTable inDT = employees.getRecords();
            inDT.Columns.Remove("OldPassword");
            inDT.Columns.Remove("OldPasswordSalt");

            Utility.SaveToCSV(inDT, baseDir + "EmployeeInboundData.csv", 0, 0);
            Utility.SaveToCSV(employees.getExistingRecords(), baseDir + "EmployeeExistingData.csv", 0, 0);


            jobQueue.setJobStatus(opts.getJobID(), "Finalizing");
            // Print out the Counts/Stats
            StatInfo stats = employees.getStats();
            stats.setDeleteTitle("Orphan");
            Console.WriteLine(stats.toString());
            string finishStatusMsg = "Completed.";
            if (!opts.isExecute())
            {
                finishStatusMsg = "Preview.";
            }
            jobQueue.setFinishMessage(finishStatusMsg + stats.toShortString()); // Sets file stat counts on the 'Completed' status message.
            

            // Set the Job Queue as finished if used.
            if (opts.getJobID().Length > 0)
            {
                jobQueue.finishJob(opts.getJobID(), opts);

            }

            // Clean up . 
            try
            {
                myConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Employee DB Close Error. " + e.ToString());
                return -1;
            }
       
            return 0; // ok

        }


        // ---------------------------------------------------------------------
        // Process Audience Files
        //
        // ---------------------------------------------------------------------

        static int ProcessAudienceFile(Options opts, Queue jobQueue)
        {

            string baseDir = opts.getBaseDirectory();
            string AudienceFileName = opts.getAudienceFileName();
            string customerID = opts.getCustomerID();



            Console.WriteLine("==============================================");
            Console.WriteLine("Audience File Processing (" + opts.getEnvironment() + ") :");
            Console.WriteLine("==============================================");
            Console.WriteLine("Parse File: " + baseDir + AudienceFileName);
            Utility.ConstructSchema(baseDir, AudienceFileName);
            DataTable inDT = Utility.ParseCSV(baseDir + AudienceFileName);

            jobQueue.setJobStatus(opts.getJobID(), "Parsed File");
            if (inDT == null)
            {
                Console.WriteLine("File could not be Parsed.");
                return -1;
            }


            // Make Database Connection ( add to config file later )
            //SqlConnection myConnection = new SqlConnection("user id=Cdwizard;password=w1z4rd;server=192.168.4.170;Trusted_Connection=no;database=CustomerBlackBox;connection timeout=30");
            //string myConString = ConfigurationManager.ConnectionStrings["ProdDBConnection"].ConnectionString;
            //Console.WriteLine("Conn: " + myConString);
            SqlConnection myConnection = new SqlConnection(opts.getConnectionString());

            // make the DB Connection
            try
            {
                Console.WriteLine("Open Database Connection.");
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }

            // ----------------------------------------------------
            // JobRole Processing
            //  
            // ----------------------------------------------------

            Console.WriteLine("Initilizing Audience Loader.");

            AudienceLoader audience = new AudienceLoader(myConnection, opts, jobQueue, inDT);


            // Sanitize/Scrub the headers
            Console.WriteLine("Audience Sanitize Input");
            try
            {
                if (audience.sanitizeColumns() != 0)
                {
                    Console.WriteLine("Abort! Sanitized Columns Failed. No Exception Thrown.");
                    myConnection.Close();
                    return -1; // failed
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Abort! Sanitized Columns Failed:" + e.ToString());
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Validate Required Fields
            Console.WriteLine("Audience Validate Input");
            if (audience.validateColumns() != 0)
            {
                Console.WriteLine("Abort! Audience Validate Columns Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Get the Existing Records from the DB
            Console.WriteLine("Audience Load Existing Jobs");
            jobQueue.setJobStatus(opts.getJobID(), "Loading Existing Audience");
            if (audience.loadAudiencesFromDB() != 0)
            {
                Console.WriteLine("Abort! Audience Load Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Determine Inserts,Updates, Deletes
            Console.WriteLine("Pre Processs Changes");
            jobQueue.setJobStatus(opts.getJobID(), "Pre-process the Changes");
            if (audience.processChanges() != 0)
            {
                Console.WriteLine("Abort! Audience Process Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            //Utility.PrintDataTable(inDT);
            //Utility.PrintDataTable(jobs.getExistingRecords());
            Console.WriteLine("Save data to files at: " + baseDir);
            Utility.SaveToCSV(audience.getRecords(), baseDir + "AudienceInboundData.csv", 0, 0);
            Utility.SaveToCSV(audience.getExistingRecords(), baseDir + "AudienceExistingData.csv", 0, 0);


            // Generate Scripts or Execute Changes
            Console.WriteLine("Execute Changes. Generated Scripts at: " + baseDir);
            jobQueue.setJobStatus(opts.getJobID(), "Execute Changes");
            if (audience.executeChanges() != 0)
            {
                Console.WriteLine("Abort! Audience Execute Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            jobQueue.setJobStatus(opts.getJobID(), "Finalize");
            // Print out the Counts/Stats
            StatInfo stats = audience.getStats();
            string finishStatusMsg = "Completed.";
            if (!opts.isExecute())
            {
                finishStatusMsg = "Preview.";
            }
            jobQueue.setFinishMessage(finishStatusMsg + stats.toShortString()); // Sets file stat counts on the 'Completed' status message.
            
            Console.WriteLine(stats.toString());

            // Set the Job Queue as finished if used.
            if (opts.getJobID().Length > 0)
            {
                jobQueue.finishJob(opts.getJobID(), opts);

            }

            // Clean up . 
            try
            {
                myConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("DB Close Error. " + e.ToString());
                return -1;
            }

            return 0; // ok

        }

        /// <summary>
        /// Processes bulk Order files downloaded from S3 and loaded into the ImportJobQueue table.
        /// </summary>
        static void ProcessOrderFileBulkOrder()
        {
            // Get the rows from the DB which need processing. 
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DevDBConnection"].ConnectionString);
            DataTable toBeProcessed = new DataTable();
            string logFile = String.Empty;
            StreamWriter logStream ;
            string URL = ConfigurationManager.AppSettings["ReactiveBusErrorEndpoint"];

            int pendingStatus = (int)ProcessingStatus.Pending; // Pending is always '0'
            string command = "Select * from ImportJobQueue where StatusCode = '" + pendingStatus + "'";

            SqlCommand sqlCommand = new SqlCommand(command, connection);
            connection.Open();           

            while (true) // Infinite loop
            {

                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                adapter.Fill(toBeProcessed);

                if (toBeProcessed.Rows.Count == 0)
                    break;                

                WebClient client = new WebClient();
                client.Headers[HttpRequestHeader.ContentType] = "application/json";

                foreach (DataRow row in toBeProcessed.Rows)
                {
                    // Get the outout file name for logging. For Bulk processing, unlike the normal flow,
                    // we log by default
                    logFile = row["OutputFile"].ToString();
                    logStream = new StreamWriter(logFile, false);

                    // Construct an Options object to be used by the processing. 
                    Options opt = new Options();
                    opt.CustomerID = row["CustomerID"].ToString();
                    opt.BaseDirectory = Path.GetFullPath(row["WorkingDir"].ToString());
                    opt.SetExecute = false; // TODO - Currently hardcoded. Need to find how this flag can be set/unset.
                    opt.JobID = row["Id"].ToString();
                    opt.JobType = "5"; // We can get this from the DB but for orders this field is "5".
                    opt.OrderFileName = row["OrderFile"].ToString();
                    opt.ConnectionString = ConfigurationManager.ConnectionStrings["DevDBConnection"].ConnectionString;

                    Queue queue = new Queue();
                    queue.setDB(connection);

                    try
                    {
                        Console.SetOut(logStream);
                        ProcessOrderFile(opt, queue);
                        string errorJSON = Utility.CreateErrorJson(opt.OrderTable.getRecords(), Path.Combine(opt.BaseDirectory, "OrderInboundData.csv"), 0, 0);
                        Console.WriteLine("Error string : " + errorJSON);
                        if (!String.IsNullOrEmpty(errorJSON))
                        {
                            client.UploadString(URL, errorJSON);
                        }
                        else //We have no errors. Post to the success queue
                        {

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        logStream.Close();
                    }

                }
            }            
        }

        // ---------------------------------------------------------------------
        // Process Audience Files
        //
        // ---------------------------------------------------------------------
        static int ProcessOrderFile(Options opts, Queue jobQueue)
        {

            string baseDir = opts.getBaseDirectory();
            string OrderFileName = opts.getOrderFileName();
            string customerID = opts.getCustomerID();

            Console.WriteLine("==============================================");
            Console.WriteLine("Order File Processing (" + opts.getEnvironment() + ") :");
            Console.WriteLine("==============================================");
            Console.WriteLine("Parse File: " + Path.Combine(baseDir,OrderFileName));
            //Utility.ConstructSchema(baseDir, OrderFileName);
            //DataTable inDT = Utility.ParseCSV(baseDir + OrderFileName);

            //if (inDT == null)
            //{
            //   Console.WriteLine("File could not be Parsed.");
            //   return -1;
            //}
            string temp = opts.getConnectionString();
            SqlConnection myConnection = new SqlConnection(opts.getConnectionString());

            // make the DB Connection
            try
            {
                Console.WriteLine("Open Database Connection.");
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }

            // ----------------------------------------------------
            // Order Processing
            //  
            // ----------------------------------------------------

            Console.WriteLine("Initilizing Order Loader.");

            OrderLoader order = new OrderLoader(myConnection, opts, jobQueue);

            Console.WriteLine("Order Load File.");
            order.loadFile(baseDir , OrderFileName);

            //order.getSheetNamesFromExcel(baseDir + "Order.xls");

            // Sanitize/Scrub the headers
            Console.WriteLine("Order Sanitize Input");
            try
            {
                if (order.sanitizeColumns() != 0)
                {
                    Console.WriteLine("Abort! Sanitized Columns Failed. No Exception Thrown.");
                    myConnection.Close();
                    return -1; // failed
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Abort! Sanitized Columns Failed:" + e.ToString());
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Validate Required Fields
            Console.WriteLine("Order Validate Input");
            if (order.validateColumns() != 0)
            {
                Console.WriteLine("Abort! Order Validate Columns Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Get the Existing Records from the DB
            Console.WriteLine("Order Load Existing Users");
            jobQueue.setJobStatus(opts.getJobID(), "Loading Existing Order info ( Users/TPs )");
            if (order.loadOrdersFromDB() != 0)
            {
                Console.WriteLine("Abort! Order Load Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Determine Inserts,Updates, Deletes
            Console.WriteLine("Pre Processs Changes");
            jobQueue.setJobStatus(opts.getJobID(), "Pre-process the Changes");
            
            if (order.processChanges() != 0)
            {
                Console.WriteLine("Abort! Order Process Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            opts.OrderTable = order;
            //Utility.PrintDataTable(inDT);
            //Utility.PrintDataTable(jobs.getExistingRecords());
            Console.WriteLine("Save data to files at: " + baseDir);
            Utility.SaveToCSV(order.getRecords(), Path.Combine(baseDir, "OrderInboundData.csv"), 0, 0);
            Utility.SaveToCSV(order.getExistingRecords(), Path.Combine(baseDir, "OrderExistingData.csv"), 0, 0);
            


            
            // Generate Scripts or Execute Changes
            Console.WriteLine("Execute Changes. Generated Scripts at: " + baseDir);
            jobQueue.setJobStatus(opts.getJobID(), "Execute Changes");
            if (order.executeChanges() != 0)
            {
                Console.WriteLine("Abort! Order Execute Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }
             

            jobQueue.setJobStatus(opts.getJobID(), "Finalize");
            // Print out the Counts/Stats
            StatInfo stats = order.getStats();
            string finishStatusMsg = "Completed.";
            if (!opts.isExecute())
            {
                finishStatusMsg = "Preview.";
            }
            jobQueue.setFinishMessage(finishStatusMsg + stats.toShortString()); // Sets file stat counts on the 'Completed' status message.

            Console.WriteLine(stats.toString());

            // Set the Job Queue as finished if used.
            if (opts.getJobID().Length > 0)
            {
                jobQueue.finishJob(opts.getJobID(), opts);

            }

            // Clean up . 
            try
            {
                myConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("DB Close Error. " + e.ToString());
                return -1;
            }

            return 0; // ok

        }


        // ---------------------------------------------------------------------
        // Process Audience Files
        //
        // ---------------------------------------------------------------------

        static int ProcessTPHistoryFile(Options opts, Queue jobQueue)
        {

            string baseDir = opts.getBaseDirectory();
            string TPHFileName = opts.getOrderFileName();
            string customerID = opts.getCustomerID();

            Console.WriteLine("==============================================");
            Console.WriteLine("TP History Processing (" + opts.getEnvironment() + ") :");
            Console.WriteLine("==============================================");
            Console.WriteLine("Parse File: " + baseDir + TPHFileName);
            //Utility.ConstructSchema(baseDir, OrderFileName);
            //DataTable inDT = Utility.ParseCSV(baseDir + OrderFileName);

            //if (inDT == null)
            //{
            //   Console.WriteLine("File could not be Parsed.");
            //   return -1;
            //}

            SqlConnection myConnection = new SqlConnection(opts.getConnectionString());


            // make the DB Connection
            try
            {
                Console.WriteLine("Open Database Connection.");
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }

        

            Console.WriteLine(ConfigurationManager.ConnectionStrings[opts.getEnvironment() + "TPDBConnection"]);
            string tpdbconfstring = ConfigurationManager.ConnectionStrings[opts.getEnvironment() + "TPDBConnection"].ConnectionString;
            // Open TP DB Connection
            SqlConnection myTPConnection = new SqlConnection(tpdbconfstring);



            // make the DB Connection
            try
            {
                Console.WriteLine("Open  TP Database Connection.");
                myTPConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }


            // ----------------------------------------------------
            // Order Processing
            //  
            // ----------------------------------------------------

            Console.WriteLine("Initilizing TP History Loader.");

            TPHistory history = new TPHistory(myConnection, myTPConnection, opts, jobQueue);

            Console.WriteLine("TPH Load File.");
            history.loadFile(baseDir, TPHFileName);

            //order.getSheetNamesFromExcel(baseDir + "Order.xls");



            // Get the Existing Records from the DB
            Console.WriteLine("Order Load Existing Users");
            jobQueue.setJobStatus(opts.getJobID(), "Loading Existing Order info ( Users/TPs )");
            if (history.loadHistoryFromDB() != 0)
            {
                Console.WriteLine("Abort! Order Load Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }

            // Determine Inserts,Updates, Deletes
            Console.WriteLine("Pre Processs Changes");
            jobQueue.setJobStatus(opts.getJobID(), "Pre-process the Changes");

            if (history.processChanges() != 0)
            {
                Console.WriteLine("Abort! Order Process Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }


            //Utility.PrintDataTable(inDT);
            //Utility.PrintDataTable(jobs.getExistingRecords());
            Console.WriteLine("Save data to files at: " + baseDir);
            Utility.SaveToCSV(history.getRecords(), baseDir + customerID + "_HistoryInboundData.csv", 0, 0);
            Utility.SaveToCSV(history.getExistingRecords(), baseDir + customerID + "_HistoryExistingData.csv", 0, 0);




            // Generate Scripts or Execute Changes
            Console.WriteLine("Execute Changes. Generated Scripts at: " + baseDir);
            jobQueue.setJobStatus(opts.getJobID(), "Execute Changes");
            if (history.executeChanges() != 0)
            {
                Console.WriteLine("Abort! Order Execute Failed. ");
                try { myConnection.Close(); }
                catch (Exception) { }
                return -1;
            }


            jobQueue.setJobStatus(opts.getJobID(), "Finalize");
            // Print out the Counts/Stats
            StatInfo stats = history.getStats();
            string finishStatusMsg = "Completed.";
            if (!opts.isExecute())
            {
                finishStatusMsg = "Preview.";
            }
            jobQueue.setFinishMessage(finishStatusMsg + stats.toShortString()); // Sets file stat counts on the 'Completed' status message.

            Console.WriteLine(stats.toString());

            // Set the Job Queue as finished if used.
            if (opts.getJobID().Length > 0)
            {
                jobQueue.finishJob(opts.getJobID(), opts);

            }

            // Clean up . 
            try
            {
                myConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("DB Close Error. " + e.ToString());
                return -1;
            }
            try
            {
                myTPConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("TP DB Close Error. " + e.ToString());
                return -1;
            }
            return 0; // ok

        }



        // ===========================================================
        // Adhoc test methods below
        // ===========================================================

        static int TempDB()
        {

            SqlConnection myConnection = new SqlConnection("user id=thunk;password=thunk*;server=localhost;Trusted_Connection=no;database=BrianTest;connection timeout=30");

            // make the DB Connection
            try
            {
                Console.WriteLine("Open Connection.");
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;    
            }

            // Get the Data
            try
            {
                Console.WriteLine("Get Data.");
                SqlCommand myCommand = new SqlCommand("Select * from JobLog", myConnection);
                SqlDataReader myReader = myCommand.ExecuteReader();
                Console.WriteLine("Output Data.");
                while (myReader.Read())
                {
                    Console.WriteLine(myReader[0].ToString());

                }
                Console.WriteLine("Done.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -2;
            }

            // Close Connection
            try
            {
                myConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            ExcelTest2("TBD");
          
            return 0;
        }

        /// <summary>
        /// Get all the order files for processing.
        /// </summary>
        static void GetOrderFilesForProcessing()
        {
            

        }


        static int ExcelTest()
        {
            // -------------------------------------------------------------------------------
            // Get information from an Excel:
            // ---------------------------/-----------------------------------------------------
            Console.WriteLine("Reading Excel Test.");
            string excelConnString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=C:\Dev\SharedData\ToDoList.xls;Extended Properties=""Excel 8.0;HDR=YES;""";

            try
            {
                DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");

                DbDataAdapter adapter = factory.CreateDataAdapter();

                DbCommand selectCommand = factory.CreateCommand();
                selectCommand.CommandText = "SELECT Area,Task,Detail,When FROM [Goals$]";

                DbConnection connection = factory.CreateConnection();
                connection.ConnectionString = excelConnString;

                Console.WriteLine("Reading Excel Test = Connection Done.");

                selectCommand.Connection = connection;

                adapter.SelectCommand = selectCommand;

                DataSet cities = new DataSet();
                Console.WriteLine("DataSet");
                adapter.Fill(cities);


                Console.WriteLine("Filled");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1;
            }
            //cities.
            Console.WriteLine("Reading Excel Test = Done.");

            
            //using (OleDbConnection connection = new OleDbConnection(excelConnString))
            //{
            //    connection.Open();
            //    worksheets = connection.GetSchema("Tables");
            return 0;
        }

        static int ExcelTest2(String fileName)
        {

            try{
            //String fileName = @"C:\Dev\SharedData\ToDoList.xlsx";

            OleDbConnection ConExcl = new OleDbConnection();
            string sConExcl = string.Empty;

            sConExcl = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + @";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1;""";

            ConExcl.ConnectionString = sConExcl;
            ConExcl.Open();

            //This gets the all the excel sheet records in a data table
            System.Data.DataTable tblCategory = new System.Data.DataTable("Category");
            System.Data.OleDb.OleDbDataAdapter daSelect = new OleDbDataAdapter("SELECT * FROM [Goals$]", ConExcl);
            daSelect.Fill(tblCategory);

            Utility.PrintDataTable(tblCategory);
            Utility.SplitToCSV("testsplit.csv", tblCategory, 5);
           

            }catch(Exception e){
                Console.WriteLine(e.ToString());
                return -1;
            }
            
            return 0;
        }

       

    }
}
