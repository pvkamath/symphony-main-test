﻿using System;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

namespace StudentImport
{
    public class Queue
    {

        string tableName = "ImportJobQueue";
        string idColName = "Id";
        string startDateColName = "StartDate";
        string endDateColName = "EndDate";
        string statusDateColName = "StatusDate";
        string statusCodeColName = "StatusCode";
        string statusMsgColName = "StatusMsg";
        string errorMsgColName = "ErrorMsg";
        string statusCountColName = "StatusUpdateCount";
        string userIdsColName = "SessionUserIDs";
        string userJsonColName = "SessionUserJson";

        string finishMessage = "Completed";

        string statusCode_Processing = "1";
        string statusCode_Error = "-1";
        string statusCode_Done = "2";

        SqlConnection mydb = null;


        public Queue()
        {
 
        }

        public void setDB(SqlConnection dbCon )
        {
             mydb = dbCon;
        }

        public void setFinishMessage(string msg)
        {
            this.finishMessage = msg;
        }

        // --------------------------------------------
        // Start the Job
        // ---------------------------------------------
        public int startJob(string id, Options opt)
        {
            string update_s = "";
            string jobdsc = "Import";

            if(opt.getJobType().Equals("1")){
                jobdsc = "User Import";
            }
            if(opt.getJobType().Equals("2")){
                jobdsc = "JobRole Import";
            }
            if(opt.getJobType().Equals("3")){
                jobdsc = "Location Import";
            }
            if (opt.getJobType().Equals("4"))
            {
                jobdsc = "Audience Import";
            }
            if (opt.getJobType().Equals("5"))
            {
                jobdsc = "Order Import";
            }
            if (!opt.isExecute())
            {
                jobdsc += "(Preview)";
            }
            if (id.Equals("-1"))
            {
                update_s = "insert into [" + this.tableName + "] \n" +
                    " ( JobTypeCode, Description, StartDate, EndDate, StatusDate, \n" +
                    " StatusCode, ErrorMsg, StatusMsg, StatusUpdateCount, WorkingDir, CustomerID , OutputFile) \n" +
                    " values ( " + opt.getJobType() + ", '" + jobdsc + "', getutcdate(),'12/31/2078',getutcdate(), \n" +
                    " 1, '', 'Launched',0,'" + opt.getBaseDirectory() + "', " + opt.getCustomerID() + ", '" + opt.getBaseDirectory() + "output.txt' ) SET @QID = SCOPE_IDENTITY() ";
            }
            else
            {

                update_s = "update [" + this.tableName + "] \n" +
                   " set [" + this.startDateColName + "] = getutcdate() \n" +
                   " , [" + this.statusCodeColName + "] = " + statusCode_Processing + "\n " +
               " , [" + this.errorMsgColName + "] = ''";

                // If there is a status msg then:
                if (this.statusMsgColName.Length > 0)
                {
                    update_s += " , [" + this.statusMsgColName + "] = 'Processing' \n";
                }
                if (this.statusDateColName.Length > 0)
                {
                    update_s += " , [" + this.statusDateColName + "] = getutcdate() \n";
                }
                if (this.statusCountColName.Length > 0)
                {
                    update_s += " , [" + this.statusCountColName + "]  = 1 \n";
                }

                update_s += " where " + this.idColName + " = " + id + "\n";
            }// End crafting sql
            Console.WriteLine("Queue: " + update_s);

            if (mydb != null)
            {

                int newid = -1;

                try
                {
                    SqlCommand myCommand = new SqlCommand(update_s, this.mydb);

                    // If insert, then set the new JobQueueID
                    SqlParameter parm = null;
                    if (id.Equals("-1"))
                    {
                        parm = new SqlParameter("@QID", SqlDbType.Int);
                        parm.Direction = ParameterDirection.Output;
                        myCommand.Parameters.Add(parm);
                    }
                    int rowcount = myCommand.ExecuteNonQuery();
                    if (rowcount != 1)
                    {
                        Console.WriteLine("Queue Start  Failed. No rows updated/inserted. ");
                        return -1;
                    }

                    // If insert, then set the new JobQueueID
                    if (id.Equals("-1"))
                    {
                        newid = (int)parm.Value;

                        if (newid > 0)
                        {
                            Console.WriteLine("Inserted New ID: " + newid);
                            opt.setJobID(newid.ToString());
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Queue Start Update Failed." + e.Message);
                    return -1;
                }

            }
            return 0;
        }

        // --------------------------------------------
        // Finish the Job
        // ---------------------------------------------
        public int finishJob(string id, Options opts)
        {
            string outputfile = "";
            if (opts.getJobType().Equals("1"))
            {
                outputfile = opts.getBaseDirectory() + "EmployeeInboundData.csv";
            }
            if (opts.getJobType().Equals("2"))
            {
                outputfile = opts.getBaseDirectory() + "JobRoleInboundData.csv";
            }
            if (opts.getJobType().Equals("3"))
            {
                outputfile = opts.getBaseDirectory() + "LocationInboundData.csv";
            }
            if (opts.getJobType().Equals("4"))
            {
                outputfile = opts.getBaseDirectory() + "AudienceInboundData.csv";
            }
            if (opts.getJobType().Equals("5"))
            {
                outputfile = Path.Combine(opts.getBaseDirectory(), "OrderInboundData.csv");
            }


            string update_s = "update [" + this.tableName + "] \n" +
                " set [" + this.endDateColName + "] = getutcdate() \n" +
                " , [" + this.statusCodeColName + "] = " + statusCode_Done + "\n" +
                " , OutputFile = '" + outputfile + "' ";

            // If there is a status msg then:
            if (this.statusMsgColName.Length > 0)
            {
  
                update_s += " , [" + this.statusMsgColName + "] = convert(varchar(50),'" + this.finishMessage + "') \n";
            }
            if ( this.statusDateColName.Length > 0 )
            {
                update_s += " , [" + this.statusDateColName + "] = getutcdate() \n";
             }
            if (this.statusCountColName.Length > 0)
            {
                update_s += " , [" + this.statusCountColName + "]  = case when [" + this.statusCountColName + "] is null then 1 else  [" + this.statusCountColName + "] + 1 end\n";
            }

            update_s += " where " + this.idColName + " = " + id + "\n";

            if (mydb != null)
            {

                try
                {
                    SqlCommand myCommand = new SqlCommand(update_s, this.mydb);
                    int rowcount = myCommand.ExecuteNonQuery();
                    if (rowcount != 1)
                    {
                        Console.WriteLine("Queue Finished Update Failed. No rows updated. ");
                        return -1;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Queue Finished Update Failed." + e.Message);
                    return -1;
                }

            }
            return 0;
        }


        // --------------------------------------------
        // Error the Job
        // ---------------------------------------------
        public int failJob(string id, string msg)
        {
            string update_s = "update [" + this.tableName + "] \n" +
                " set [" + this.endDateColName + "] = getutcdate() \n" +
                " , [" + this.statusCodeColName + "] = " + statusCode_Error;

            // If there is a status msg then:
            if (this.errorMsgColName.Length > 0)
            {
                update_s += " , [" + this.errorMsgColName + "] = '" + msg + "' \n";
            }
            if (this.statusDateColName.Length > 0)
            {
                update_s += " , [" + this.statusDateColName + "] = getutcdate() \n";
            }
            if (this.statusCountColName.Length > 0)
            {
                update_s += " , [" + this.statusCountColName + "]  = case when [" + this.statusCountColName + "] is null then 1 else  [" + this.statusCountColName + "] + 1 end \n";
            }
            if (this.statusMsgColName.Length > 0)
            {
                update_s += " , [" + this.statusMsgColName + "] = 'Failed' \n";
            }
            update_s += " where " + this.idColName + " = " + id + "\n";

            if (mydb != null)
            {

                try
                {
                    SqlCommand myCommand = new SqlCommand(update_s, this.mydb);
                    int rowcount = myCommand.ExecuteNonQuery();
                    if (rowcount != 1)
                    {
                        Console.WriteLine("Queue Error Update Failed. No rows updated. ");
                        return -1;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Queue Error Update Failed." + e.Message);
                    return -1;
                }

            }
            return 0;
        }


        // --------------------------------------------
        // Set the status of the Job
        // ---------------------------------------------
        public int setJobStatus(string id, string msg)
        {
            string update_s = "update [" + this.tableName + "] \n" +
                " set [" + this.statusCodeColName + "] = " + statusCode_Processing;

            // If there is a status msg then:
            if (this.statusMsgColName.Length > 0)
            {
                update_s += " , [" + this.statusMsgColName + "] = '" + msg + "' \n";
            }
            if (this.statusDateColName.Length > 0)
            {
                update_s += " , [" + this.statusDateColName + "] = getutcdate() \n";
            }
            if (this.statusCountColName.Length > 0)
            {
                update_s += " , [" + this.statusCountColName + "]  = case when [" + this.statusCountColName + "] is null then 1 else [" + this.statusCountColName + "] + 1 end \n";
            }

            update_s += " where " + this.idColName + " = " + id + "\n";

            if (mydb != null)
            {

                try
                {
                    SqlCommand myCommand = new SqlCommand(update_s, this.mydb);
                    int rowcount = myCommand.ExecuteNonQuery();
                    if (rowcount != 1)
                    {
                        Console.WriteLine("Queue Status Update Failed. No rows updated. ");
                        return -1;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Queue Status Update Failed." + e.Message);
                    return -1;
                }

            }
            return 0;
        }

        // --------------------------------------------
        // Update user ids related to sessions affected by this job
        // ---------------------------------------------
        public int setSessionUserIds(string id, List<int> ids)
        {
            string update_s = "update [" + this.tableName + "] \n" +
                " set [" + this.userIdsColName + "] = '" + string.Join(",", ids) + "'";

            
            update_s += " where " + this.idColName + " = " + id + "\n";

            if (mydb != null)
            {
                try
                {
                    SqlCommand myCommand = new SqlCommand(update_s, this.mydb);
                    int rowcount = myCommand.ExecuteNonQuery();
                    if (rowcount != 1)
                    {
                        Console.WriteLine("Could not update user ids affected by import. ");
                        return -1;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Could not update user ids affected by import." + e.Message);
                    return -1;
                }

            }
            return 0;
        }

        public int setSessionUserJson(string id, string json)
        {
            string update_s = "update [" + this.tableName + "] \n" +
                " set [" + this.userJsonColName + "] = '" + json + "'";


            update_s += " where " + this.idColName + " = " + id + "\n";

            if (mydb != null)
            {
                try
                {
                    SqlCommand myCommand = new SqlCommand(update_s, this.mydb);
                    int rowcount = myCommand.ExecuteNonQuery();
                    if (rowcount != 1)
                    {
                        Console.WriteLine("Could not update user json affected by import. ");
                        return -1;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Could not update user json affected by import." + e.Message);
                    return -1;
                }

            }
            return 0;
        }


    }
}