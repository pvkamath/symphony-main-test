﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

using System.Linq;


using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.Security;

using Symphony.Core;
using Data = Symphony.Core.Data;

using Symphony.Core.Controllers.Salesforce;
using Symphony.Core.Models.Salesforce;

using Symphony.Core.Controllers;

using Newtonsoft.Json;
using System.IO;
using System.Data.OleDb;

namespace StudentImport
{


    // --------------------------------------------------------
    // Loader for Order Records
    //  1. 
    // --------------------------------------------------------
    public class OrderLoader
    {


        // Main Inbound Order information to be loaded
        DataTable inOrder = null;
        DataTable inSkuMap = null;
        String customerID = "";
        ArrayList errorMessages = null;
        SqlConnection db = null;
        DataTable oldOrder = null;
        DataTable lookupSKUs = null;
        DataTable oldOrderUser = null;
        string baseDirName = "";
        string programName = "Order Import";
        Queue jobQueue = null;
        Options opts = null;

        // Ignore stat checks
        Boolean ignoreWarnings = false; // Turn this on to stop reasonability checks on changes. (If there are a lot of changes )


        // Standard Stats
        StatInfo stats = null;


        // Special Custom Format Code
        int formatCode = 0; // 0 = Normal,  1 = Regency, 2 = Home Point Financial, 3 = Primary Residential 

        // -------------------------------------------------------------
        // Constructor 
        // Parameter dt is the inbound file data loaded into a DataTable
        // -------------------------------------------------------------
        public OrderLoader(SqlConnection dbCon, Options optvalues, Queue q)
        {

            this.opts = optvalues;
            //this.inOrder = dt;
            this.customerID = this.opts.getCustomerID();
            this.db = dbCon;
            this.errorMessages = new ArrayList();
            this.baseDirName = this.opts.getBaseDirectory();
            this.jobQueue = q;
            this.stats = new StatInfo();
        }



        // ----------------------------------------------
        // Return the existing Order records.
        // ----------------------------------------------
        public DataTable getExistingRecords()
        {
            return this.oldOrder;
        }
        // ----------------------------------------------
        // Return the inbound/modified Order records.
        // ----------------------------------------------
        public DataTable getRecords()
        {
            return this.inOrder;
        }

        // ----------------------------------------------
        // Return the process statistics
        // ----------------------------------------------
        public StatInfo getStats()
        {
            return this.stats;
        }


        // -----------------------------------------------------------
        // sanitizeColumns
        // brief: Renames the column headers
        // to make sure column names are consistent.
        // 1. Change to consistent Case.
        // 2. Trims
        // 3. Adds in some processing columns
        // * This is the place to catch common misspellings if needed
        // (example:  name turns into Name, NAME turns into Name )
        // -----------------------------------------------------------
        public int sanitizeColumns()
        {
            for (int curCol = 0; curCol < inOrder.Columns.Count; curCol++)
            {
                String colName = inOrder.Columns[curCol].ColumnName.ToLower().Trim();
                Console.WriteLine(" COLUMN: " + colName);

            }

            if (!inOrder.Columns.Contains("Status"))
            {
                this.inOrder = Utility.addColumnToDataTable(this.inOrder, "Status", "System.String");
            }
     
            if (!inOrder.Columns.Contains("UpdateAUID"))
            {
                this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateAUID", "System.String");
            }
             if (!inOrder.Columns.Contains("Notes"))
             {
                 this.inOrder = Utility.addColumnToDataTable(this.inOrder, "Notes", "System.String");
             }

             if (!inOrder.Columns.Contains("SKU"))
             {
                 this.inOrder = Utility.addColumnToDataTable(this.inOrder, "SKU", "System.String");
             }

             if (!inOrder.Columns.Contains("enrollmentDate"))
             {
                 this.inOrder = Utility.addColumnToDataTable(this.inOrder, "enrollmentDate", "System.String");
             }
             if (!inOrder.Columns.Contains("license"))
             {
                 this.inOrder = Utility.addColumnToDataTable(this.inOrder, "license", "System.String");
             }

            // Standard and customized columname sanitzation 
            for (int curCol = 0; curCol < inOrder.Columns.Count; curCol++)
            {
                String colName = inOrder.Columns[curCol].ColumnName.ToLower().Trim();

             

                // Sanitize variations of FirstName
                if (colName.ToLower().ToString().Equals("first") ||  colName.ToLower().ToString().Equals("first name"))
                {
                    inOrder.Columns[curCol].ColumnName = "firstname";
                }

               // Sanitize variations of last Name
                if (colName.ToLower().ToString().Equals("last") ||  colName.ToLower().ToString().Equals("last name"))
                {
                    inOrder.Columns[curCol].ColumnName = "lastname";
                }

                // Sanitize variations of middle Name
                if (colName.ToLower().ToString().Equals("middle") ||  colName.ToLower().ToString().Equals("middle name"))
                {
                    inOrder.Columns[curCol].ColumnName = "middlename";
                }

                // Sanitize variations of EmployeNumber
                if (colName.ToLower().ToString().Equals("employeenumber") || colName.ToLower().ToString().Equals("employeenum") || colName.ToLower().ToString().Equals("employee number"))
                {
                    inOrder.Columns[curCol].ColumnName = "EmployeeNumber";
                }

                // Sanitize variations of last Name
                if (colName.ToLower().ToString().Equals("note") )
                {
                    inOrder.Columns[curCol].ColumnName = "notes";
                }

            }



            if (!inOrder.Columns.Contains("EmployeeNumber"))
            {
                this.inOrder = Utility.addColumnToDataTable(this.inOrder, "EmployeeNumber", "System.String");
                // Copy values from Email to EmployeeNumber
                for (int curRow = 0; curRow < this.inOrder.Rows.Count; curRow++)
                {

                    this.inOrder.Rows[curRow]["EmployeeNumber"] = this.inOrder.Rows[curRow]["Email"].ToString().Trim();
                }
            }


            if (!inOrder.Columns.Contains("Username"))
            {
                this.inOrder = Utility.addColumnToDataTable(this.inOrder, "Username", "System.String");
                // Copy values from Email to Username
                for (int curRow = 0; curRow < this.inOrder.Rows.Count; curRow++)
                {
                  
                    this.inOrder.Rows[curRow]["Username"] = this.inOrder.Rows[curRow]["Email"].ToString().Trim();
                }
            }

           if (!inOrder.Columns.Contains("MiddleName"))
            {
                this.inOrder = Utility.addColumnToDataTable(this.inOrder, "MiddleName", "System.String");
            }




            // Add isNew flag to dataTable
            // This will be used to mark which ones need to be inserted
            // ( These need to be inserted before the updates )
  
           this.inOrder = Utility.addColumnToDataTable(this.inOrder, "CustomerSubDomain", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "New", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "NewUser", "System.String");
            // Add update flag to dataTable
            // This will be used to mark which ones need to be updated in the db
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "Update", "System.String");
            // Add update comment to dataTable
            // This will be used add notes or comments
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateMsg", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "OldUserName", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "OldEmployeeNumber", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "OldEmail", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "OldFirstName", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "OldLastName", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "OldMiddleName", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "OldNMLS", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "OldSKU", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "OldNotes", "System.String");

            // Add existingID to dataTable
            // This will be used to mark what existing record got modified
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateID", "System.String");
            // Add Existing Name, Code, ParentOrderID to dataTable
            // This will be used to see what was updated or not updated
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateUserName", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateEmployeeNumber", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateEmail", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateFirstName", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateLastName", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateMiddleName", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateNMLS", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateSKU", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "UpdateNotes", "System.String");

            // Add Error to dataTable
            // This will be used to note error messages
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "ErrorFg", "System.String");
            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "Error", "System.String");

            this.inOrder = Utility.addColumnToDataTable(this.inOrder, "CreateRegistration", "System.String");

            return 0; // ok
        }

        // -----------------------------------------------------------
        // validateColumns
        // brief: Checks to make sure column information is available
        // return -1 if no Name
    
        // -----------------------------------------------------------
        public int validateColumns()
        {

            if (!inOrder.Columns.Contains("Email"))
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("OrderLoader.validateColumns : Email column not found. Fail. ");
                }
                jobQueue.failJob(opts.getJobID(), "Failed: Email column not found.");
                return -1;
            }




            return 0; // ok
        }

        // ---------------------------------------------
        // load the Order information for the customer
        // from the database.
        // ---------------------------------------------
        public int loadOrdersFromDB()
        {

            string userfilter = " ( U.username in (";
            for (int curRow = 0; curRow < this.inOrder.Rows.Count; curRow++)
            {
                string username = this.inOrder.Rows[curRow]["username"].ToString();
                if (curRow > 0)
                {
                    userfilter += ",";
                }
                userfilter += "'" + username.Replace("'", "''") + "'";
            }
            userfilter += ") )";

            string empNumfilter = " ( U.EmployeeNumber in (";
            for (int curRow = 0; curRow < this.inOrder.Rows.Count; curRow++)
            {
                string empNum = this.inOrder.Rows[curRow]["EmployeeNumber"].ToString();
                if (curRow > 0)
                {
                    empNumfilter += ",";
                }
                empNumfilter += "'" + empNum.Replace("'", "''") + "'";
            }
            empNumfilter += "))";


            String query = " select UserID = U.ID, U.UserName, U.FirstName, U.LastName,U.MiddleName, U.EmployeeNumber, \n " +
                            " U.Email, UserStatus = U.StatusID, U.SalesforceAccountID, U.JanrainUserUuid, NMLS = U.NMLSNumber, \n " +
                            " TrainingProgramName = TP.Name, TrainingProgramID = TP.ID, TP.StartDate, EndDate = TP.EndDate, U.Notes,  \n " +
                            " SKU = case when PTP.SKU  is null then TP.SKU else PTP.SKU end , UpdateAUID = AU.UserId \n " +
                            " from [user] as U \n " +
                            " inner join HierarchyToTrainingProgramLink as TPL on TPL.HierarchyNodeID = U.ID and TPL.HierarchyTypeID = 4  \n " +
                            " inner join TrainingProgram as TP on TP.ID = TPL.TrainingProgramID \n " +
                            " left outer join TrainingProgram as PTP on PTP.ID = TP.ParentTrainingProgramID \n " +
                               " left outer join aspnet_Users as AU on U.username = AU.UserName \n" +
                        " left outer join aspnet_Membership as M on AU.UserId = M.UserId \n" +
                        " left outer join aspnet_Applications as A on M.ApplicationID = A.ApplicationID\n" +
                        " right join Customer as C on C.SubDomain = A.Applicationname and U.CustomerID = C.ID\n" +
                            " where U.CUstomerID = " + this.customerID + "\n " +
                            " and (" + userfilter + " \n" +
                            "   or " + empNumfilter + " \n " +
                            "   )  \n " +
                            " order by U.UserName, TP.SKU, TP.StartDate ";
           


            this.oldOrder = new DataTable();

            // Get the Data
            try
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("Order Get Data: " + query);
                }
                SqlCommand myCommand = new SqlCommand(query, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (!opts.isSilent())
                {
                    Console.WriteLine("Order DB Data Loaded.");
                }
                // Load into DataTable
                this.oldOrder.Load(myReader);

                // Add some processing columns
                this.oldOrder = Utility.addColumnToDataTable(this.oldOrder, "New", "System.String");
                this.oldOrder = Utility.addColumnToDataTable(this.oldOrder, "Delete", "System.String");
                this.oldOrder = Utility.addColumnToDataTable(this.oldOrder, "Update", "System.String");


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }

            //Default some values
            for (int curRow = 0; curRow < this.oldOrder.Rows.Count; curRow++)
            {
                this.oldOrder.Rows[curRow]["Delete"] = "No";
            }


            // Load just the existing users ( for adding/updating )
            // Separate this from the Order information


            String Uquery = " select UserID = U.ID, U.UserName, U.FirstName, U.MiddleName, U.LastName, U.EmployeeNumber, \n " +
                            " U.Email, UserStatus = U.StatusID, U.SalesforceAccountID, U.JanrainUserUuid, \n" +
                            " NMLS = U.NmlsNumber ,UpdateAUID = AU.UserId, U.Notes \n " +
                            " from [user] as U \n " +
                               " left outer join aspnet_Users as AU on U.username = AU.UserName \n" +
                        " left outer join aspnet_Membership as M on AU.UserId = M.UserId \n" +
                        " left outer join aspnet_Applications as A on M.ApplicationID = A.ApplicationID\n" +
                        " right join Customer as C on C.SubDomain = A.Applicationname and U.CustomerID = C.ID\n" +
                            " where U.CUstomerID = " + this.customerID + "\n " +
                            " and (" + userfilter + " \n" +
                            "   or " + empNumfilter + " \n " +
                            "   )  \n " +
                            " order by U.UserName, U.EmployeeNumber, U.StatusID " ;

          


            this.oldOrderUser = new DataTable();

            // Get the Data
            try
            {
                if (!opts.isSilent())
                {
                    Console.WriteLine("Order User Get Data: " + Uquery);
                }
                SqlCommand myCommand2 = new SqlCommand(Uquery, this.db);
                SqlDataReader myReader2 = myCommand2.ExecuteReader();

                if (!opts.isSilent())
                {
                    Console.WriteLine("Order User DB Data Loaded.");
                }
                // Load into DataTable
                this.oldOrderUser.Load(myReader2);

                // Add some processing columns
                this.oldOrderUser = Utility.addColumnToDataTable(this.oldOrderUser, "New", "System.String");
                this.oldOrderUser = Utility.addColumnToDataTable(this.oldOrderUser, "Delete", "System.String");
                this.oldOrderUser = Utility.addColumnToDataTable(this.oldOrderUser, "Update", "System.String");


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }

            //Default some values
            for (int curRow = 0; curRow < this.oldOrderUser.Rows.Count; curRow++)
            {
                this.oldOrderUser.Rows[curRow]["Delete"] = "No";
            }

            // Load Lookup Values for Validation
            this.loadSKUs();


            return 0; // ok
        }



        // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int processChanges()
        {

            // Load Client Information
            if (this.loadClientInfo() < 0)
            {
                opts.setExecute(false);// Cannot update if there is an issue with the client info
            }

            // Pre Process Changes to determine what to do. 
            // Execution of changes will occur next.
            // For each row
            //  1. find it in the existing db rows
            //  2.  Mark if it is new.
            //  3.  Error if it matches to more than one row.
            //  4.  Mark as update if it needs to change.

            this.stats.inbound_count = this.inOrder.Rows.Count;
            this.stats.existing_count = this.oldOrder.Rows.Count;
            int found_count = 0;

            for (int curRow = 0; curRow < this.inOrder.Rows.Count; curRow++)
            {


                if (this.inOrder.Rows[curRow]["username"].ToString().Length < 1)
                {

                    this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                    this.inOrder.Rows[curRow]["Error"] = "No Username.Skip (Code1).";
                    //this.stats.error_count++;
                    continue; // Skip
                }

                // These are the new values on the file
                string OrderUserName = this.inOrder.Rows[curRow]["username"].ToString();
                string OrderFirstName = this.inOrder.Rows[curRow]["firstname"].ToString();
                string OrderLastName = this.inOrder.Rows[curRow]["lastname"].ToString();
                string OrderMiddleName = this.inOrder.Rows[curRow]["middlename"].ToString();
                string OrderEmail = this.inOrder.Rows[curRow]["email"].ToString();
                string OrderEmployeeNumber = this.inOrder.Rows[curRow]["employeenumber"].ToString();
                string OrderNMLS = this.inOrder.Rows[curRow]["nmls"].ToString();
                string OrderNote = this.inOrder.Rows[curRow]["Notes"].ToString();
                string OrderSKU = this.inOrder.Rows[curRow]["sku"].ToString();
                string OrderLicense= this.inOrder.Rows[curRow]["License"].ToString();
                string OrderEnrollmentDate = this.inOrder.Rows[curRow]["enrollmentdate"].ToString();

                // Default Some of the Metadata. 
                this.inOrder.Rows[curRow]["CustomerSubDomain"] = opts.getCustomerShortName();
                this.inOrder.Rows[curRow]["UpdateID"] = "";
                this.inOrder.Rows[curRow]["OldUserName"] = "";
                this.inOrder.Rows[curRow]["OldEmail"] = "";
                this.inOrder.Rows[curRow]["OldSKU"] = "";
                this.inOrder.Rows[curRow]["OldNMLS"] = "";
                this.inOrder.Rows[curRow]["OldEmployeeNumber"] = "";

                //Find existing User:
        
                DataRow[] foundRowsU;
                foundRowsU = this.oldOrderUser.Select("UserName = '" + OrderUserName.Replace("'", "''") + "'");
                string foundby = "UserName";
                string foundkey = OrderUserName;
          
                if (foundRowsU.Length < 1)
                {
                    // Find using the EMployeeNumber
                    Console.WriteLine("No UserName found for: " + OrderUserName + ". Checking EmployeeNumber:" + OrderEmployeeNumber);
                    foundRowsU = this.oldOrderUser.Select("EmployeeNumber = '" + OrderEmployeeNumber.Replace("'", "''") + "'");
                    if (foundRowsU.Length > 0)
                    {
                        Console.WriteLine("Found by EmployeeNumber: " + OrderEmployeeNumber);
                        foundby = "EmployeeNumber";
                        foundkey = OrderEmployeeNumber;
                    }
                    else
                    {
                        Console.WriteLine("New User:" + OrderUserName);
                        
                    }
                }

                if (foundRowsU.Length < 1)
                {
                    // Not Found. Mark as New
                    this.inOrder.Rows[curRow]["NewUser"] = "1";
                    this.inOrder.Rows[curRow]["CreateRegistration"] = "1";

                    this.stats.addColumnCount("New Users");
           

                    // Add to lookup as New User
                    DataRow newRow = this.oldOrderUser.NewRow();
                    newRow["new"] = "1";
                    newRow["username"] = OrderUserName;
                    newRow["email"] = OrderEmail;
                    newRow["firstname"] = OrderFirstName;
                    newRow["lastname"] = OrderLastName;
                    newRow["middlename"] = OrderMiddleName;
                    newRow["EmployeeNumber"] = OrderEmployeeNumber;
                    newRow["NMLS"] = OrderNMLS;
                    newRow["UserStatus"] = "1"; // Active

                    this.oldOrderUser.Rows.Add(newRow); // Add to user list as New
                }
                else if (foundRowsU.Length > 1)
                {
                    // ---------------------------------------------------------------
                    /// SKIP UPDATE ( duplicate users )
                    // ---------------------------------------------------------------


                    // Too many Matches. Error.
                    this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                    this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"] + "* Duplicate User! Records matched by " + foundby + " (" + foundRowsU.Length + ")";
                    this.inOrder.Rows[curRow]["New"] = "0";
                    this.inOrder.Rows[curRow]["NewUser"] = "0";
                    this.stats.error_count++;
                    continue;
                }
                else
                {
                    // Found the existing user. 
                    // Updates might be needed. Compare the new values to the old values to make sure:

                    this.inOrder.Rows[curRow]["NewUser"] = "0";
                    this.inOrder.Rows[curRow]["UpdateID"] = foundRowsU[0]["UserID"].ToString();
                    this.inOrder.Rows[curRow]["UpdateAUID"] = foundRowsU[0]["UpdateAUID"].ToString();

                    string oldFirstName = foundRowsU[0]["FirstName"].ToString();
                    string oldLastName = foundRowsU[0]["LastName"].ToString();
                    string oldMiddleName = foundRowsU[0]["MiddleName"].ToString();
                    string oldEmployeeNumber = foundRowsU[0]["EmployeeNumber"].ToString();
                    string oldUserName = foundRowsU[0]["UserName"].ToString();
                  
                    string oldEmail = foundRowsU[0]["Email"].ToString();
                    string oldNMLS = foundRowsU[0]["NMLS"].ToString();
                    string oldNotes = foundRowsU[0]["Notes"].ToString();

                    // Track the existing values for undo logic and logging.
                    this.inOrder.Rows[curRow]["OldUserName"] = oldUserName;
                    this.inOrder.Rows[curRow]["OldEmployeeNumber"] = oldEmployeeNumber;
                    this.inOrder.Rows[curRow]["OldFirstName"] = oldFirstName;
                    this.inOrder.Rows[curRow]["OldLastName"] = oldLastName;
                    this.inOrder.Rows[curRow]["OldMiddleName"] = oldMiddleName;
                    this.inOrder.Rows[curRow]["OldEmail"] = oldEmail;
                    this.inOrder.Rows[curRow]["OldNMLS"] = oldNMLS;
                    this.inOrder.Rows[curRow]["OldNotes"] = oldNotes;



                    Boolean hasChanged = false;
                    if (!oldUserName.Trim().ToLower().Equals(OrderUserName.Trim().ToLower()))
                    {
                        this.inOrder.Rows[curRow]["Update"] = "1";
                        this.inOrder.Rows[curRow]["UpdateUserName"] = "1";
                        this.inOrder.Rows[curRow]["UpdateMsg"] = this.inOrder.Rows[curRow]["UpdateMsg"] + "username (" + OrderUserName + ") !=" + oldUserName + ";";
                        hasChanged = true;
                    }
                    if (!oldEmail.Trim().ToLower().Equals(OrderEmail.Trim().ToLower()))
                    {
                        this.inOrder.Rows[curRow]["Update"] = "1";
                        this.inOrder.Rows[curRow]["UpdateEmail"] = "1";
                        this.inOrder.Rows[curRow]["UpdateMsg"] = this.inOrder.Rows[curRow]["UpdateMsg"] + "email (" + OrderEmail + ") !=" + oldEmail + ";";
                        hasChanged = true;
                    }
                    if (!oldNMLS.Trim().ToLower().Equals(OrderNMLS.Trim().ToLower()))
                    {
                        this.inOrder.Rows[curRow]["Update"] = "1";
                        this.inOrder.Rows[curRow]["UpdateNMLS"] = "1";
                        this.inOrder.Rows[curRow]["UpdateMsg"] = this.inOrder.Rows[curRow]["UpdateMsg"] + "NMLS (" + OrderNMLS + ") !=" + oldNMLS + ";";
                        hasChanged = true;
                    }
                    if (!oldFirstName.Trim().ToLower().Equals(OrderFirstName.Trim().ToLower()))
                    {
                        this.inOrder.Rows[curRow]["Update"] = "1";
                        this.inOrder.Rows[curRow]["UpdateFirstName"] = "1";
                        this.inOrder.Rows[curRow]["UpdateMsg"] = this.inOrder.Rows[curRow]["UpdateMsg"] + "FirstName (" + OrderFirstName + ") !=" + oldFirstName + ";";
                        hasChanged = true;
                    }
                    if (!oldLastName.Trim().ToLower().Equals(OrderLastName.Trim().ToLower()))
                    {
                        this.inOrder.Rows[curRow]["Update"] = "1";
                        this.inOrder.Rows[curRow]["UpdateLastName"] = "1";
                        this.inOrder.Rows[curRow]["UpdateMsg"] = this.inOrder.Rows[curRow]["UpdateMsg"] + "LastName (" + OrderLastName + ") !=" + oldLastName + ";";
                        hasChanged = true;
                    }
                    if (!oldEmployeeNumber.Trim().ToLower().Equals(OrderEmployeeNumber.Trim().ToLower()))
                    {
                        this.inOrder.Rows[curRow]["Update"] = "1";
                        this.inOrder.Rows[curRow]["UpdateEmployeeNumber"] = "1";
                        this.inOrder.Rows[curRow]["UpdateMsg"] = this.inOrder.Rows[curRow]["UpdateMsg"] + "EmployeeNumber (" + OrderEmployeeNumber + ") !=" + oldEmployeeNumber + ";";
                        hasChanged = true;
                    }

                    // ------------ Notes Compare -------------------------------
                    // Check to see if the new note is already a part of the record.
                    // Convert the notes to lowercase, no whitespace, no quotes for the compare
                    String filteredNote = OrderNote.ToLower().Trim().Replace("'", "");
                    filteredNote = filteredNote.Replace(" ", "");

                    string temp_oldnote = oldNotes.ToLower().Trim().Replace("'", "");
                    temp_oldnote = temp_oldnote.Replace(" ", "");
                    if (!temp_oldnote.Contains(filteredNote))
                    {

                        this.inOrder.Rows[curRow]["Update"] = "1";
                        this.inOrder.Rows[curRow]["UpdateNotes"] = "1";
                        this.stats.addColumnCount("Notes");
                        this.inOrder.Rows[curRow]["UpdateMsg"] = this.inOrder.Rows[curRow]["UpdateMsg"] + " Adding Note.";
                        hasChanged = true;
                    }



                    if (hasChanged)
                    {
                        this.stats.update_count++;
                    }

              
                }
               
                

                // Find existing TP Row using SKU value 
                // foundby and foundkey are usually Username or EmployeeNumber depending on the previous match logic.
                DataRow[] foundRows;
                foundRows = this.oldOrder.Select(foundby + "= '" + foundkey.Replace("'", "''") + "'" + " and SKU = '" + OrderSKU + "'");
                if (foundRows.Length < 1)
                {

                    // Not Found. Mark as New
                    this.inOrder.Rows[curRow]["New"] = "1";
                    this.inOrder.Rows[curRow]["CreateRegistration"] = "1";

                    this.stats.insert_count++;


                    // Lookup SKU
                    Console.WriteLine("ORDER SKU:" + OrderSKU + " from license: " + OrderLicense);
                    if (OrderSKU.Length < 1)
                    {
                        // SKU not provided. Lookup using License
                        lookupSKU(curRow, OrderLicense);

                    }

                    // Check to make sure the SKU is in the customer or in the shared TP network
                    if (lookupNetworkSKU(OrderSKU) < 0)
                    {
                        // SKU not found. Error.
                        this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                        this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"] + "* SKU not found: " + OrderSKU +".";
                        this.inOrder.Rows[curRow]["New"] = "0";
                        this.inOrder.Rows[curRow]["NewUser"] = "0";
                        this.stats.error_count++;
                        this.stats.insert_count--;
                        continue;
                    }

                }
                else if (foundRows.Length > 1)
                {
                    // Too many Matches. Error.
                    this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                    this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"] + "* Order found duplicates on the file using UserName:  " + OrderUserName + " and SKU : " + OrderSKU + " .(" + foundRows.Length + ")";
                    this.inOrder.Rows[curRow]["New"] = "0";
                    this.inOrder.Rows[curRow]["NewUser"] = "0";
                    this.stats.error_count++;
                    this.stats.insert_count--;
                    continue;
                }
                else
                {
                    // Mark existing order record as found.
                    foundRows[0]["Delete"] = "No";
                    found_count++;

                    // Check for duplicates ( Future duplicate.. not yet iterated apon on the inbound file. Previous check was for the existing users in the db already )
                    DataRow[] dupRows;
                    dupRows = this.inOrder.Select("UserName = '" + OrderUserName + "'" + " and SKU = '" + OrderSKU + "'");
                    if (dupRows.Length > 1)
                    {
                        this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                        this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"]  + "* Duplicate Order on Inbound File." + OrderUserName + " and SKU : " + OrderSKU + " (" + dupRows.Length + ")";
                        this.stats.error_count++;
                        continue;
                    }

   


                    // Update Might be needed. Compare the new values to the old values to make sure:

                    this.inOrder.Rows[curRow]["New"] = "0";
                    this.inOrder.Rows[curRow]["UpdateID"] = foundRows[0]["UserID"].ToString();
                    this.inOrder.Rows[curRow]["UpdateAUID"] = foundRows[0]["UpdateAUID"].ToString();

                    string oldSKU = foundRows[0]["SKU"].ToString(); 
                    this.inOrder.Rows[curRow]["OldSKU"] = oldSKU;




                }




            }

            this.stats.delete_count = this.stats.existing_count - found_count;

            Utility.SaveToCSV(this.oldOrderUser, Path.Combine(this.opts.BaseDirectory, "OrderUserData.csv"), 0, 0);


            return 0; //ok
        }

        // -------------------------------------------------------------------------
        // Process changes
        // -------------------------------------------------------------------------

        public int executeChanges()
        {

            // Insert new Users
            this.executeAdds();

            // Insert new Ordersm Training Programs
            this.executeAddsOrders();

            // Update primary fields
            this.executeUpdates();

            // Update lookup fields
            //this.executeSecondaryUpdates();

            this.executeSalesforceTrainingProgramAssignments();

            return 0; //ok

        }

        private void  executeSalesforceTrainingProgramAssignments() {
            
                Console.WriteLine("Updating sessions...");

                bool isPreview = !opts.isExecute() || !opts.isSessions();

                if (isPreview)
                {
                    Console.WriteLine("Previewing session assignments");
                }

                Dictionary<string, int> userIdDictionary = new Dictionary<string, int>();

                string userfilter = "U.username in (";
                for (int curRow = 0; curRow < this.inOrder.Rows.Count; curRow++)
                {
                    string username = this.inOrder.Rows[curRow]["username"].ToString().ToLower();
                    if (curRow > 0)
                    {
                        userfilter += ",";
                    }
                    userfilter += "'" + username + "'";
                }
                userfilter += ")";

                string sql = "select U.ID, UserName = LOWER(U.Username) from [User] U where " + userfilter + " and customerID = " + this.customerID;
                
                DataTable userIds = new DataTable();

                // Get the Data
                try
                {
                    Console.WriteLine("Get new user ids: " + sql);
                    
                    SqlCommand myCommand = new SqlCommand(sql, this.db);
                    SqlDataReader myReader = myCommand.ExecuteReader();

                    Console.WriteLine("New user ids loaded.");
                    
                    // Load into DataTable
                    userIds.Load(myReader);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return;
                }

                for (int curRow = 0; curRow < userIds.Rows.Count; curRow++)
                {
                    string uname = userIds.Rows[curRow]["Username"].ToString();
                    string id = userIds.Rows[curRow]["ID"].ToString();

                    int i = int.Parse(id);

                    if (!userIdDictionary.ContainsKey(uname))
                    {
                        userIdDictionary.Add(uname, i);
                    }
                }
                if (!opts.isSilent())
                {
                    Console.WriteLine("User dictionary created.");
                }
                for (int curRow = 0; curRow < this.inOrder.Rows.Count; curRow++)
                {
                    DataRow row = this.inOrder.Rows[curRow];
                    if (row["ErrorFg"].ToString() == "1")
                    {
                        continue; // Skip Errored order records
                    }
                    if (row["CreateRegistration"].ToString() == "1")
                    {
                        
                        string OrderUserName = this.inOrder.Rows[curRow]["username"].ToString().ToLower();
                        string OrderSKU = this.inOrder.Rows[curRow]["sku"].ToString();
                        string OrderEnrollmentDate = this.inOrder.Rows[curRow]["enrollmentdate"].ToString();
 
                        if (OrderEnrollmentDate.Length < 1)
                        {
                            OrderEnrollmentDate = DateTime.Today.ToString("MM/dd/yyyy");
                        }
                        // Optional Due Date and End Dates
                        string OrderDueDate = DateTime.Today.AddMonths(4).ToString("MM/dd/yyyy");
                        string OrderEndDate = DateTime.Today.AddMonths(6).ToString("MM/dd/yyyy");
                        if( this.inOrder.Columns.Contains("enddate")){
                            if( this.inOrder.Rows[curRow]["enddate"].ToString().Length > 0 ){
                                OrderEndDate = this.inOrder.Rows[curRow]["enddate"].ToString();
                            }
                        }
                         if( this.inOrder.Columns.Contains("duedate")){
                            if( this.inOrder.Rows[curRow]["duedate"].ToString().Length > 0 ){
                                OrderDueDate = this.inOrder.Rows[curRow]["duedate"].ToString();
                            }
                        }


                        Console.WriteLine("New Registration for: " + OrderUserName + ", " + OrderSKU + ", " + OrderEnrollmentDate);
                        
                        EntitlementList entitlements = new EntitlementList()
                        {
                            Entitlements = new List<Entitlement>()
                        };

                        DateTime enrollmentDate = DateTime.UtcNow;
                        if (!DateTime.TryParse(OrderEnrollmentDate, out enrollmentDate))
                        {
                            Console.WriteLine("Invalid order date. Defaulting to today.");
                        }
                        
      

                        entitlements.Entitlements.Add(new Entitlement()
                        {
                            EntitlementDuration = 12,
                            EntitlementDurationUnits = "Month",
                            EntitlementStartDate = enrollmentDate.ToString("MM/dd/yyyy"),
                            EntitlementPurchaseDate = enrollmentDate.ToString("MM/dd/yyyy"),
                            EntitlementEndDate = OrderEndDate,
                            //EntitlementDueDate = OrderDueDate,
                            EntitlementId = 0,
                            SalesforceEntitlementId = "",
                            IsEnabled = true,
                            IsSuppressEmail = false,
                            PartnerCode = "",
                            SalesforceProductId = "",
                            ProductId = OrderSKU
                        });
                        
                        if (userIdDictionary.ContainsKey(OrderUserName))
                        {
                            Console.WriteLine("Saving Enrollment: " + OrderUserName + ", " + OrderSKU + ", " + enrollmentDate.ToString() + " to " + enrollmentDate.AddYears(1).ToString());
                            
                            SalesforceController.OverrideTargetCustomerID(int.Parse(opts.getCustomerID()));
                            if (opts.isExecute())
                            {
                                (new EntitlementController()).PostEntitlements(userIdDictionary[OrderUserName], entitlements);
                            }
                            else
                            {
                                Console.WriteLine("In preview mode - Session assignments can not be previewed since it relies on the training program enrollment being created first.");
                            }
                        }
                    }
                }

                List<int> userIdList = userIdDictionary.Select(kv => kv.Value).ToList();

                // Create session registrations for any users that don't have them
                Console.WriteLine("Creating session registrations...");
                List<Symphony.Core.Models.SessionAssignedUser> sessionAssigned = new List<Symphony.Core.Models.SessionAssignedUser>();
                try
                {
                    if (isPreview)
                    {
                        Console.WriteLine("In Preview mode - Saving session registrations will probably fail, unless there were previous un assigned sessions for this user.");
                    }

                    sessionAssigned = (new ClassroomController()).SaveSessionRegistrations(userIdList, isPreview).Data.ToList();

                    foreach (Symphony.Core.Models.SessionAssignedUser u in sessionAssigned)
                    {
                        Console.WriteLine(u.FullName + " is " + u.RegistrationStatusName + " in the class " + u.ClassName + " on " + u.MinStartDateTime.ToString());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Could not save session registrations. " + e.Message);
                }

                string sessionAssignedUsersJson = JsonConvert.SerializeObject(sessionAssigned);
                Console.WriteLine("Session assignments: " + sessionAssignedUsersJson);

                Console.WriteLine("Saving user ids to job queue...");
                
                jobQueue.setSessionUserIds(opts.getJobID(), userIdList);
                jobQueue.setSessionUserJson(opts.getJobID(), sessionAssignedUsersJson);
                

            
        }

        // ---------------------------------------------------------
        // Update the primary fields
        // -----------------------------------------------------------
        private void executeUpdates()
        {

            string update1SQL = "";
          

            // Generate Update Statements for Changes

            for (int curRow = 0; curRow < this.inOrder.Rows.Count; curRow++)
            {

                if (curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Student Main Update " + curRow + " of " + this.inOrder.Rows.Count);
                }

                string Emp_New = this.inOrder.Rows[curRow]["New"].ToString();
                string Emp_Update = this.inOrder.Rows[curRow]["Update"].ToString();
                string Emp_ID = this.inOrder.Rows[curRow]["UpdateID"].ToString();
                string Emp_ASPNETID = this.inOrder.Rows[curRow]["UpdateAUID"].ToString();
                string Emp_Username = this.inOrder.Rows[curRow]["Username"].ToString();
                string Emp_EmployeeNumber = this.inOrder.Rows[curRow]["EmployeeNumber"].ToString();
                string Emp_FirstName = this.inOrder.Rows[curRow]["FirstName"].ToString();
                string Emp_LastName = this.inOrder.Rows[curRow]["LastName"].ToString();
                string Emp_MiddleName = this.inOrder.Rows[curRow]["MiddleName"].ToString();
  
                string Emp_Email = this.inOrder.Rows[curRow]["Email"].ToString();
                string Emp_Notes = this.inOrder.Rows[curRow]["Notes"].ToString();
                string Emp_NMLS = this.inOrder.Rows[curRow]["NMLS"].ToString();


                if (Emp_ID.Length < 1)
                {
                    // Must be a new record and not an update
                    Console.WriteLine("New Student. Skipping this update. ");
                    continue;
                }

                // Only update what has changed:
                bool updateFirstName = false;
                if (this.inOrder.Rows[curRow]["UpdateFirstName"].ToString().Equals("1"))
                {
                    updateFirstName = true;
                }
                bool updateEmployeeNumber = false;
                if (this.inOrder.Rows[curRow]["UpdateEmployeeNumber"].ToString().Equals("1"))
                {
                    updateEmployeeNumber = true;
                }
                bool updateLastName = false;
                if (this.inOrder.Rows[curRow]["UpdateLastName"].ToString().Equals("1"))
                {
                    updateLastName = true;
                }
                bool updateMiddleName = false;
                if (this.inOrder.Rows[curRow]["UpdateMiddleName"].ToString().Equals("1"))
                {
                    updateMiddleName = true;
                }
          
          
                bool updateEmail = false;
                if (this.inOrder.Rows[curRow]["UpdateEmail"].ToString().Equals("1"))
                {
                    updateEmail = true;
                }
            
                bool updateUsername = false;
                if (this.inOrder.Rows[curRow]["UpdateUserName"].ToString().Equals("1"))
                {
                    updateUsername = true;
                }
                bool updateNotes = false;
                if (this.inOrder.Rows[curRow]["UpdateNotes"].ToString().Equals("1"))
                {
                    updateNotes = true;
                }
                bool updateNMLS = false;
                if (this.inOrder.Rows[curRow]["updateNMLS"].ToString().Equals("1"))
                {
                    updateNMLS = true;
                }


                // Generate the Update SQL ( Step = 1: Update Primary Fields 
                if (Emp_Update.Equals("1"))
                {

                    if (updateEmployeeNumber  || updateLastName || updateFirstName ||
                        updateMiddleName || updateEmail  || updateUsername  || updateNotes || updateNMLS )
                    {

                        string update_s = " Update [user] set ModifiedBy = '" + this.programName + "'\n";
                        string update_s2 = "";//This will be the username update on aspnet_users



                    

                 
                        if (updateLastName)
                        {
                            update_s += ", LastName = '" + Emp_LastName.Replace("'", "''") + "'\n";
                        }
                        if (updateFirstName)
                        {
                            update_s += ", FirstName = '" + Emp_FirstName.Replace("'", "''") + "'\n";
                        }
                        if (updateMiddleName)
                        {
                            update_s += ", MiddleName = '" + Emp_MiddleName.Replace("'", "''") + "'\n";
                        }
                        if (updateEmail)
                        {
                            update_s += ", Email = '" + Emp_Email.Replace("'", "''") + "'\n";
                        }
                   
                        if (updateUsername)
                        {
                            update_s += ", Username = '" + Emp_Username.Replace("'", "''") + "'\n";
                        }
                        if (updateEmployeeNumber)
                        {
                            update_s += ", EmployeeNumber = '" + Emp_EmployeeNumber.Replace("'", "''").Trim() + "'\n";
                        }
                        if (updateNotes)
                        {
                            update_s += ", Notes = Notes + ' " + Emp_Notes.Replace("'", "''").Trim() + "'\n";
                        }
                        if (updateNMLS)
                        {
                            update_s += ", NMLSNumber = ' " + Emp_NMLS.Replace("'", "''").Trim() + "'\n";
                        }

                        update_s +=
                                " , ModifiedOn = getdate() \n" +
                                " where CustomerID = " + this.customerID + " \n" +
                                " and ID = " + Emp_ID + "\n\n";

                        if (updateUsername)
                        {
                            update_s2 = "\n" +
                                " update aspnet_users set username = '" + Emp_Username.Replace("'", "''") + "'\n " +
                                " , loweredusername = lower('" + Emp_Username.Replace("'", "''") + "') \n " +
                                " where UserID = '" + Emp_ASPNETID + "' \n\n";
                        }


                        if (opts.isExecute())
                        {
      

                            // Update Main User
                            try
                            {
                                SqlCommand myCommand = new SqlCommand(update_s, this.db);
                                int rowcount = myCommand.ExecuteNonQuery();
                                if (rowcount < 1)
                                {
                                    this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                                    this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + ", Main user record update failed with Rowcount:" + rowcount;
                                    update_s = "FAILED:" + update_s;
                                }
                            }
                            catch (Exception)
                            {
                                this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                                this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + ", Update Main Failed.";
                                update_s = "FAILED:" + update_s;

                            }

                            // Update Asp.net User if needed
                            if (updateUsername && (this.inOrder.Rows[curRow]["ErrorFg"].ToString().CompareTo("1") != 0))
                            {
                                try
                                {
                                    SqlCommand myCommand = new SqlCommand(update_s2, this.db);
                                    int rowcount = myCommand.ExecuteNonQuery();
                                    if (rowcount < 1)
                                    {
                                        this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                                        this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + "; Username Update Failed with Rowcount:" + rowcount;
                                        update_s2 = "FAILED:" + update_s2;
                                    }
                                }
                                catch (Exception)
                                {
                                    this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                                    this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + ", ASPNET Update Failed.";
                                    update_s2 = "FAILED:" + update_s2;
                                }
                            } // emd update user name

                        }
                        update1SQL += update_s + update_s2;

                    }

                }// End update step 1

            } // End big employee loop

            Utility.SaveToFile(Path.Combine(this.baseDirName,"u1_Order_Update1SQL.txt"), update1SQL);
            this.inOrder.AcceptChanges();


        
        } // End executeUpdate()


        private void executeSecondaryUpdates()
        {

          

        } // End executeSecondaryUpdates()

        // ------------------------------------------------
        //  Add new Orders
        // ------------------------------------------------


        private int executeAdds()
        {


            string insertSQL = "";


            // Generate/Execute SQL Scripts for Inserts
            for (int curRow = 0; curRow < this.inOrder.Rows.Count; curRow++)
            {

                if (curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Order Add " + curRow + " of " + this.inOrder.Rows.Count);
                }

                string Emp_New = this.inOrder.Rows[curRow]["NewUser"].ToString();
                string Emp_CurrentErrorFg = this.inOrder.Rows[curRow]["ErrorFg"].ToString();
                string Emp_Update = this.inOrder.Rows[curRow]["Update"].ToString();
                string Emp_ID = this.inOrder.Rows[curRow]["UpdateID"].ToString();
                string Emp_ASPNETID = this.inOrder.Rows[curRow]["UpdateAUID"].ToString();
                string Emp_Username = this.inOrder.Rows[curRow]["Username"].ToString();
                string Emp_EmployeeNumber = this.inOrder.Rows[curRow]["EmployeeNumber"].ToString();
                string Emp_NMLS = this.inOrder.Rows[curRow]["NMLS"].ToString();
                string Emp_FirstName = this.inOrder.Rows[curRow]["FirstName"].ToString();
                string Emp_LastName = this.inOrder.Rows[curRow]["LastName"].ToString();
                string Emp_MiddleName = this.inOrder.Rows[curRow]["MiddleName"].ToString();
                string Emp_Email = this.inOrder.Rows[curRow]["Email"].ToString();
                string Emp_Status = this.inOrder.Rows[curRow]["Status"].ToString();
                string Emp_Password = "Oncourse1";  //this.inOrder.Rows[curRow]["Password"].ToString();
                string Emp_Notes = this.inOrder.Rows[curRow]["Notes"].ToString();
                string Emp_PasswordChange = "'01/01/2014'";

                string insert_s = "";

                if (Emp_New.Equals("1") && !Emp_CurrentErrorFg.Equals("1"))
                {

                    // Check to see if the Password is valid ( >= 6 characters with one number )
                    // Only if it is a new record, or if we want to update the password

                    if (Emp_Password.Length < 7 || !Regex.IsMatch(Emp_Password, @"\d"))
                    {
                        // Is not 7 characters or more, and does not have a number in it.
                        Console.WriteLine("Invalid Password: + " + Emp_Username);
                        this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                        this.inOrder.Rows[curRow]["Error"] = inOrder.Rows[curRow]["Error"] + "* Password needs to be 7 or more characters with at least 1 number.";
                        this.stats.error_count++;
                        //Utility.SaveFilteredToCSV(inOrder, this.baseDirName + "a1_Add_Emp_error.csv", fileFilter_new, colNames_new);
                        continue; // Skip

                    }


                    insert_s = "insert into [user] (username, CustomerID, SalesChannelID, LocationID \n" +
                            ",EmployeeNumber, FirstName, MiddleName, LastName \n" +
                            ",HireDate, NewHireIndicator, Email, StatusID, JobRoleID, Notes \n" +
                            ",LoginCounter, IsAccountExec, IsCustomerCare, ModifiedBy, CreatedBy \n" +
                            ",ModifiedOn, CreatedOn, SupervisorID, IsExternal, SecondaryLocationID, TelephoneNumber, [password] , ReportingSupervisorID, LastEnforcedPasswordReset, NMLSNumber )\n " +
                            " values ( '" + Emp_Username.Replace("'", "''") + "', " + opts.getCustomerID() + ",0,0 \n " +
                            " ,'" + Emp_EmployeeNumber.Replace("'", "''") + "', '" + Emp_FirstName.Replace("'", "''") + "','" + Emp_MiddleName.Replace("'", "''") + "','" + Emp_LastName.Replace("'", "''") + "' \n" +
                            " ,getdate(),0,'" + Emp_Email.Replace("'", "''") + "',1,0,'" + Emp_Notes.Replace("'", "''") + "' \n" +
                            ",0,0,0,'" + this.programName + "','" + this.programName + "',getdate(), getdate(),0,0,0,'','',0,'01/01/2014', '" + Emp_NMLS + "' )\n\n";


                    if (opts.isExecute())
                    {
                        //  adding user
                        Emp_ASPNETID = this.AddASPNETUser(Emp_Username, Emp_Password, Emp_Email);

                        if (Emp_ASPNETID.Equals("ERROR"))
                        {
                            Console.WriteLine("Failed to add ASPNET user." + Emp_Username);
                            this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                            this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + "* User Insert Failed on Membership:";

                            insert_s = "FAILED:" + insert_s;
                            this.stats.error_count++;
                            continue; //Fail all and Skip this guy



                        }
                        this.inOrder.Rows[curRow]["UpdateAUID"] = Emp_ASPNETID;
                        Console.WriteLine("Added ASPNET user." + Emp_Username + " ID:" + Emp_ASPNETID);

                        // Add Main User
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(insert_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount < 1)
                            {


                                this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                                this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + ", Insert Failed with rc:" + rowcount;
                                insert_s = "FAILED:" + insert_s;

                                Console.WriteLine("User Insert Failed: " + insert_s);
                                
                                this.stats.error_count++;
                                continue;
                            }
                        }
                        catch (Exception)
                        {
                            this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                            this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + ", Insert Failed.";
                            insert_s = "FAILED:" + insert_s;
                            this.stats.error_count++;
                            continue;
                        }

                        insertSQL += insert_s; // Save for final SQL Script in log

                        insert_s = "insert into aspnet_UsersInRoles (UserId, RoleID) values ('" + Emp_ASPNETID +
                              "','" + opts.getUserRoleID() + "')\n\n";

                        // Add Role
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(insert_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount < 1)
                            {
                                this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                                this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + ", Role Insert Failed with rc:" + rowcount;
                                insert_s = "FAILED:" + insert_s;
                                insertSQL += insert_s;
                                this.stats.error_count++;
                                continue;
                            }
                        }
                        catch (Exception)
                        {
                            this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                            this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + ", Role Insert Failed.";
                            insert_s = "FAILED:" + insert_s;
                            insertSQL += insert_s;
                            this.stats.error_count++;
                            continue;
                        }
                        insertSQL += insert_s + "\n----------------------------------------------\n";

                    }
                    else
                    {// End if Execute
                        insertSQL += insert_s;
                    }

                    // ! The SupervisorID needs to be updated after the 1st update, and after the inserts.
                    // This will be done in the processSecondaryUpdate method



                }



            } // End big employee loop

            this.inOrder.AcceptChanges();
            Utility.SaveToFile(Path.Combine(this.baseDirName, "u2_OrderUser_InsertSQL.txt"), insertSQL);

            return 0;

        }



        // Add Training Program and Order Data
        private void executeAddsOrders()
        {
            string insertSQL = "";
        

            // Generate Update/Insert Statements for Orders
            for (int curRow = 0; curRow < this.inOrder.Rows.Count; curRow++)
            {

                if (!opts.isSilent() && curRow > 0 && curRow % 100 == 0)
                {
                    Console.WriteLine("Order Add Scan " + curRow + " of " + this.inOrder.Rows.Count);
                }

                string A_NewUser = this.inOrder.Rows[curRow]["NewUser"].ToString();
                string A_NewOrder = this.inOrder.Rows[curRow]["New"].ToString();
                string A_UserID = "";// this.inOrder.Rows[curRow]["UserID"].ToString();
                string A_SKU = this.inOrder.Rows[curRow]["sku"].ToString();
                string A_EnrollmentDate = this.inOrder.Rows[curRow]["EnrollmentDate"].ToString();
                string A_UserName = this.inOrder.Rows[curRow]["UserName"].ToString();
              
                string A_ID = this.inOrder.Rows[curRow]["UpdateID"].ToString(); // Existing OrderID  
        
                //string update_s = "";

                // For now always put everything in the order queue and indicate which ones are adds.
                if (true)
                {

                    string insert_s = " insert into Analyst.dbo.ImportJobQueueOrder ( [ImportJobQueueID] ,[UserID] ,[SKU], \n" +
                                " [EnrollmentDate], [IsNewUser], [IsNewOrder], [UserName] \n" +
                                " values ( " + this.opts.getJobID() + ", " + A_UserID + ",  \n" +
                                " '" + A_SKU + "','" + A_EnrollmentDate + "'," + A_NewUser + "," + A_NewOrder + ", " +
                                "'" + A_UserName + "' )\n\n";
                  


                    // Insert into the DB if requested.    
                    if (false) //opts.isExecute())
                    {

                        try
                        {
                            SqlCommand myCommand = new SqlCommand(insert_s, this.db);
                            int rowcount = myCommand.ExecuteNonQuery();
                            if (rowcount != 1)
                            {
                                this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                                this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + ", Insert Failed with rc:" + rowcount;
                                insert_s = "FAILED:" + insert_s;
                            }
                        }
                        catch (Exception)
                        {
                            this.inOrder.Rows[curRow]["ErrorFg"] = "1";
                            this.inOrder.Rows[curRow]["Error"] = this.inOrder.Rows[curRow]["Error"].ToString() + ", Insert Failed.";
                            insert_s = "FAILED:" + insert_s;
                        }
                    } // End db execute
                    insertSQL += insert_s;



                } // End if new

           


            } // End New/Insert Processing loop



            Utility.SaveToFile(Path.Combine(this.baseDirName,"u2_Order_InsertSQL.txt"), insertSQL +
                "\n ----------------------------------------------- \n ");

        } // End executeAdd()


        private int loadClientInfo()
        {


            string clientInfoSQL = "select A.ApplicationName ,A.ApplicationID,  \n" +
            "UserRoleID = R1.RoleID, SupRoleID = R2.RoleID, ReportSupRoleID = R3.RoleID \n" +
            "from Customer C\n" +
            "inner join aspnet_Applications as A on A.ApplicationName = C.SubDomain\n" +
            "inner join aspnet_Roles as R1 on A.ApplicationId = R1.ApplicationId and R1.RoleName = 'Customer - User'\n" +
            "inner join aspnet_Roles as R2 on A.ApplicationId = R2.ApplicationId and R2.RoleName = 'Classroom - Supervisor'\n" +
            "inner join aspnet_Roles as R3 on A.ApplicationId = R3.ApplicationId and R3.RoleName = 'Reporting - Supervisor'\n" +
            "where C.ID = " + opts.getCustomerID() + "\n";


            DataTable cltInfo = new DataTable();

            // Get the Data
            try
            {
                    Console.WriteLine("Client Info Get Data: " + clientInfoSQL);
                SqlCommand myCommand = new SqlCommand(clientInfoSQL, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                    Console.WriteLine("Employee DB Data Loaded.");
                // Load into DataTable
                cltInfo.Load(myReader);




            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }
            if (cltInfo.Rows.Count < 1)
            {
                Console.WriteLine("No Client Information. Will not execute.");
                return -1;
            }
            //Default some values
            for (int curRow = 0; curRow < cltInfo.Rows.Count; curRow++)
            {
                opts.setCustomerShortName(cltInfo.Rows[curRow]["ApplicationName"].ToString());
                opts.setApplicationID(cltInfo.Rows[curRow]["ApplicationID"].ToString());
                opts.setUserRoleID(cltInfo.Rows[curRow]["UserRoleID"].ToString());
                opts.setSupervisorRoleID(cltInfo.Rows[curRow]["SupRoleID"].ToString());
                opts.setReportingSupervisorRoleID(cltInfo.Rows[curRow]["ReportSupRoleID"].ToString());
            }

            Console.Write(opts.toString());

            return 0; // ok
        }



        private int loadSKUs()
        {


            string skuSQL = "select DISTINCT T.SKU from NetworkSharedTrainingPrograms  as NT \n " +
                            "   inner join TrainingProgram as T on NT.TrainingProgramID = T.ID  \n " +
                    "   inner join CustomerNetworkDetail as CND on CND.DestinationCustomerId = " + opts.getCustomerID() + " and CND.AllowTrainingProgramSharing = 1  \n " +
                    "   where NT.CustomerNetworkDetailId = CND.DetaildId  \n " +
                    "   and T.CustomerID = CND.SourCeCustomerID  \n " +
                    "   and T.SKU is not NULL  \n " +
                    "   union select DISTINCT TP.SKU from TrainingProgram as TP where TP.CustomerID =  " + opts.getCustomerID() + " and SKU is not NULL  \n " +
                    "   order by SKU \n";


            lookupSKUs= new DataTable();

            // Get the Data
            try
            {
                Console.WriteLine("SKU Info Get Data: " + skuSQL);
                SqlCommand myCommand = new SqlCommand(skuSQL, this.db);
                SqlDataReader myReader = myCommand.ExecuteReader();

                Console.WriteLine("Employee DB Data Loaded.");
                // Load into DataTable
                lookupSKUs.Load(myReader);




            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return -1; // Fail
            }
            if (lookupSKUs.Rows.Count < 1)
            {
                Console.WriteLine("No SKU Information. Will not execute.");
                return -1;
            }

            return 0; // ok
        }



        // -----------------------------------------------------------
        private string AddASPNETUser(string username, string password, string email)
        {
            //string username = "bankersedge2";
            //string password = "password1";
            //string email = "bcarlson@bankersedge.com";


            string passwordQuestion = "foo";
            string passwordAnswer = "bar";
            Boolean isApproved = true;


            Membership.ApplicationName = opts.getCustomerShortName();// "example: anbone"; Acts as the App Name

            Console.WriteLine("Create User: " + username + " App = " + Membership.ApplicationName);
            MembershipCreateStatus memStatus = new MembershipCreateStatus();


            //** ADD USER
            MembershipUser mu = Membership.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, out memStatus);
            Console.WriteLine("Created User:" + memStatus.ToString());


            if (memStatus != MembershipCreateStatus.Success)
            {
                Console.WriteLine("Error adding " + username + ".");
                return "ERROR";
            }
            Console.WriteLine("Done adding " + username + ".");
            return mu.ProviderUserKey.ToString();
        }


        // --------------------------------------------------
        // Load the Excel or CSV File data (optional)
        // This can detect and load different formats
        // --------------------------------------------------
        public int loadFile(String baseDir, String orderFileName)
        {

            // Find the file. ( The import screens may upload a csv, or xls, or xlsx )
            // The filename configured for the import may not match. 
            // We have to be dynamic here.

            // Simple CSV File


            //if (System.IO.File.Exists(baseDir + orderFileName))
            if(File.Exists(Path.Combine(baseDir,orderFileName)))
            {
                Console.WriteLine("Loading CSV FIle : " + baseDir + orderFileName);
                Utility.ConstructSchema(baseDir, orderFileName);
                //this.inOrder = Utility.ParseCSV(baseDir + orderFileName);
                this.inOrder = Utility.ParseCSV(Path.Combine(baseDir, orderFileName));
                return 1;
            }

            if (orderFileName.Contains(".csv"))
            {

                Console.WriteLine("No CSV FIle. Finding XLSX or XLS files : " + baseDir );
                // Try finding xls and xlsx
                if (System.IO.File.Exists(baseDir + orderFileName.Replace(".csv", ".xls")))
                {
                    
                    orderFileName = orderFileName.Replace(".csv", ".xls");
                    Console.WriteLine("Found XLS : " + baseDir + orderFileName);
                }
                else if (System.IO.File.Exists(baseDir + orderFileName.Replace(".csv", ".xlsx")))
                {
                    orderFileName = orderFileName.Replace(".csv", ".xlsx");
                    Console.WriteLine("Found XLSX : " + baseDir + orderFileName);
                }
            }

            if (System.IO.File.Exists(baseDir + orderFileName))
            {
                Console.WriteLine("Loading Excel FIle : " + baseDir + orderFileName);
                Utility.ConstructExcelSchema(baseDir, orderFileName,"","");
                this.inOrder = OrderLoader.ParseExcel(baseDir + orderFileName,"","students");
                this.inSkuMap = OrderLoader.ParseExcel(baseDir + orderFileName, "", "skumap");
                Utility.PrintDataTable(inOrder);
                Utility.PrintDataTable(inSkuMap);
                return 1;
            }


            //this.getSheetNamesFromExcel(baseDir + orderFileName);
            

            return 0;

        }

        // ------------------------------
        // Discover which custom format it is
        //
        private DataTable convertFromCustom(DataTable rawDT)
        {

            if (rawDT.Columns.Contains("Employee") && rawDT.Columns.Contains("Branch") && rawDT.Columns.Contains("NMLS")
                 && rawDT.Columns.Contains("E-mail") && rawDT.Columns.Contains("State") )
            {
                // Regency ML
                Console.WriteLine("Regency ML Custom File ");
                this.formatCode = 1;
                return convertFromRegency(rawDT);

            }else if (rawDT.Columns.Contains("State") && rawDT.Columns.Contains("Date") && rawDT.Columns.Contains("F2"))
            {
                // Possible Home Point Financial Format
                Console.WriteLine("Home Point: Column 1." + rawDT.Columns[0].ColumnName );
                if (rawDT.Rows.Count > 0 && rawDT.Columns[0].ColumnName.Equals(rawDT.Rows[1][0].ToString()))
                {
                    // Defintaly Home Point Financial
                    Console.WriteLine("Home Point Financial Custom File ");
                    this.formatCode = 2;
                }


            }
            else if (rawDT.Columns.Contains("Individual Id") && rawDT.Columns.Contains("Licensed States") )
            {
                Console.WriteLine("Primary Residential Custom File ");
                this.formatCode = 3;
            }
            else
            {
                Console.WriteLine("Normal Format ");
                formatCode = 0; // Normal
            }

            return rawDT;
        }

        // -----------------------------------------------------------
        // Custom FOrmat conversions
        // for:Regency ML
        //
        // Note: SKU is manually mapped here.
        // Branch is put into the Note field
        // FullName is parsed out.
        // -----------------------------------------------------------
        private DataTable convertFromRegency(DataTable rawDT)
        {
            DataTable dt = new DataTable();
            Utility.addColumnToDataTable(dt, "UserName", "System.String");
            Utility.addColumnToDataTable(dt, "FirstName", "System.String");
            Utility.addColumnToDataTable(dt, "LastName", "System.String");
            Utility.addColumnToDataTable(dt, "MiddleName", "System.String");
            Utility.addColumnToDataTable(dt, "Notes", "System.String");
            Utility.addColumnToDataTable(dt, "Email", "System.String");
            Utility.addColumnToDataTable(dt, "NMLS", "System.String");
            Utility.addColumnToDataTable(dt, "EnrollmentDate", "System.String");
            Utility.addColumnToDataTable(dt, "SKU", "System.String");
            Utility.addColumnToDataTable(dt, "State", "System.String");

            for (int curRow = 0; curRow < rawDT.Rows.Count; curRow++)
            {

                string statetext = rawDT.Rows[curRow]["State"].ToString();
                string fullname = rawDT.Rows[curRow]["Employee"].ToString();

                // States can be comma delimited. Duplicate the row; One for each state.
                string[] values = statetext.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < values.Length; i++)
                {
                    DataRow newRow = dt.NewRow();
                    newRow["UserName"] = rawDT.Rows[curRow]["E-mail"];
                    newRow["Email"] = rawDT.Rows[curRow]["E-mail"];
                    newRow["Notes"] = rawDT.Rows[curRow]["Branch"];
                    newRow["NMLS"] = rawDT.Rows[curRow]["NMLS"];
                    newRow["State"] = rawDT.Rows[curRow]["State"];

                    string state = values[i];
                    string sku = "";
                    if (state.ToLower().Equals("ky"))
                    {
                        sku = "333KY15001";
                    }
                    else if (state.ToLower().Equals("oh"))
                    {
                        sku = "333US15001";
                    }
                    else if (state.ToLower().Equals("pa"))
                    {
                        sku = "333PA15001";
                    }
                    else if (state.ToLower().Equals("tn"))
                    {
                        sku = "333US15001";
                    }
                    else
                    {
                        sku = "unknown";
                    }
                    newRow["SKU"] = sku;

                    // Parse out Name
                    int l_pos = fullname.IndexOf(',');
                    int m_pos = fullname.LastIndexOf('.');
                    int m_originpos = fullname.LastIndexOf('.');
                    if (m_pos < 1 || m_pos < l_pos)
                    {
                        // No Middle Initial. Or '.' is in the lastname before the ',' ( Austin Jr., Mark )
                        m_pos = fullname.Length + 2;
                    }
                    Console.WriteLine(" * " + fullname + "  LP: " + l_pos + " MP :" + m_pos + ";");
                    string lastname = fullname.Substring(0, l_pos).Trim();
                    string firstname = fullname.Substring(l_pos + 1, m_pos - 2 - (l_pos + 1)).Trim(); // Middle initial is M. at the end
                    string middlename = "";
                    if (m_originpos > 0 && m_originpos > l_pos)
                    {
                        middlename = fullname.Substring(m_pos - 1, 1).Trim(); // Middle initial is M. at the end
                    }
                    newRow["LastName"] = lastname;
                    newRow["FirstName"] = firstname;
                    newRow["MiddleName"] = middlename;

                    dt.Rows.Add(newRow);
                }
            }
            return dt;
        }

         // -------------------------------------------------------------
        // Get Excel Sheet Names
        // Custom method to load a Excel into a DataTable
        // Params: path is the full path to the file.
        // ---------------------------------------------------------------
        public   List<string> getSheetNamesFromExcel(string path)
        {
            if (!System.IO.File.Exists(path))
                return null;

            string full = Path.GetFullPath(path);

            string filenamenoextension = Path.GetFileNameWithoutExtension(path);
            string file = Path.GetFileName(full);
            string dir = Path.GetDirectoryName(full);


            

            //create the "database" connection string
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;"
              + "Data Source=\"" + dir + "\\" + filenamenoextension + "\";";

            if (path.EndsWith(".xls"))
            {
                connString += "Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"";
            }
            else
            {
                //Assume xlsx
                connString += "Extended Properties=\"Excel 12.0 XML;HDR=YES;IMEX=1\"";
            }


            string Sheet1 = "";
            List<string> sheetnames = new List<string>();

            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                conn.Open();
                DataTable dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                foreach (DataRow row in dtSchema.Rows)
                {
                    if (!row["TABLE_NAME"].ToString().Contains("FilterDatabase"))
                    {
                        //sheetNames.Add(new SheetName() { sheetName = row["TABLE_NAME"].ToString(), sheetType = row["TABLE_TYPE"].ToString(), sheetCatalog = row["TABLE_CATALOG"].ToString(), sheetSchema = row["TABLE_SCHEMA"].ToString() });
                        Sheet1 = row["TABLE_NAME"].ToString();
                        Console.WriteLine("EXCEL: " + Sheet1);
                         Sheet1 = Sheet1.Replace("'", "");
                         Sheet1 = Sheet1.Replace("$", "");
                         sheetnames.Add(Sheet1); 

                    }
                }
                
                conn.Close();
            }


            return sheetnames;

        }



        // -------------------------------------------------------------
        // ParseExcel
        // Custom method to load a Excel into a DataTable
        // Params: path is the full path to the file.
        // ---------------------------------------------------------------
        public static DataTable ParseExcel(string path, string startrow, string sheetname)
        {
            if (!System.IO.File.Exists(path))
                return null;

            string full = Path.GetFullPath(path);

            string filenamenoextension = Path.GetFileNameWithoutExtension(path);
            string file = Path.GetFileName(full);
            string dir = Path.GetDirectoryName(full);

            //create the "database" connection string
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;"
              + "Data Source=\"" + dir + "\\" + filenamenoextension + "\";";

            if (path.EndsWith(".xls"))
            {
                connString += "Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"";
            }
            else
            {
                //Assume xlsx
                connString += "Extended Properties=\"Excel 12.0 XML;HDR=YES;IMEX=1\"";
            }


            string Sheet1 = "";
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                conn.Open();
                DataTable dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                foreach (DataRow row in dtSchema.Rows)
                {
                    if (!row["TABLE_NAME"].ToString().Contains("FilterDatabase"))
                    {
                        //sheetNames.Add(new SheetName() { sheetName = row["TABLE_NAME"].ToString(), sheetType = row["TABLE_TYPE"].ToString(), sheetCatalog = row["TABLE_CATALOG"].ToString(), sheetSchema = row["TABLE_SCHEMA"].ToString() });
                        Sheet1 = row["TABLE_NAME"].ToString();
                        break;
                    }
                }
                //Sheet1 = dtSchema.Rows[0]["TABLE_NAME"].ToString();
                Sheet1 = Sheet1.Replace("'", "");
                Sheet1 = Sheet1.Replace("$", "");
                conn.Close();
            }

            Console.WriteLine("Sheetname : " + Sheet1 + " vs asked for SheetName of " + sheetname);
            if (sheetname.Length < 1)
            {
                sheetname = Sheet1;
            }


            //create the database query
            string query = "SELECT * FROM [" + sheetname + "$A" + startrow + ":Z20000]";  // A2:ZZ will start reading from 2nd row.

            //create a DataTable to hold the query results
            DataTable dTable = new DataTable();

            //create an OleDbDataAdapter to execute the query
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(query, connString);



            try
            {
                //fill the DataTable
                dAdapter.Fill(dTable);
            }
            catch (InvalidOperationException e)
            { Console.Write(e.ToString()); }
            catch (Exception e)
            { Console.Write(e.ToString()); }

            dAdapter.Dispose();

            // Convert All columns to String
            //dc.DataType = System.Type.GetType(typeString); // System.String

            return dTable;
        }

        //-----------------------------------------------------------
        // Look the SKU
        // ---------------------------------------------------------
        private int lookupNetworkSKU(string sku)
        {


            // Find Location in lookup
            DataRow[] foundRows1;
            string filter = "SKU = '" + sku.ToLower().Replace("'", "''") + "'";  // Not case sensitive


           // Console.WriteLine(" Lookup SKU: " + sku);
            foundRows1 = this.lookupSKUs.Select(filter);


            if (foundRows1.Length > 0)
            {

                //Console.WriteLine(" Found - " + foundRows1[0]["SKU"].ToString().Trim());
                {
                    // Newly inserted. Return no current/orginal match.
                    return 1;
                }
            }
            else
            {
                Console.WriteLine(" Lookup SKU Failed: " + sku);
                return -1;
            }
        

        }// End lookup network sku

        private int lookupSKU(int curRow, string licenseName)
        {


            // Find Location in lookup
            DataRow[] foundRows1;
            string filter = "LICENSE = '" + licenseName.ToLower().Replace("'", "''") + "'";  // Not case sensitive


            Console.WriteLine(" Lookup SKU: License - " + licenseName);
            foundRows1 = this.inSkuMap.Select(filter);


            if (foundRows1.Length > 0)
            {

                Console.WriteLine(" Found - " + foundRows1[0]["SKUS"].ToString().Trim());
                this.inOrder.Rows[curRow]["SKU"] = foundRows1[0]["SKUS"].ToString().Trim();
                {
                    // Newly inserted. Return no current/orginal match.
                    return 1;
                }
            }
            return foundRows1.Length;

        }// End lookup SKU by license method

    }
}