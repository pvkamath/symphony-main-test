﻿using System;
using System.Linq;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Text;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace StudentImport
{
    public class Utility
    {
        
        public Utility()
        {
            


        }

        // -------------------------------------------------------------
        // ParseCSV
        // Custom method to load a CSV into a DataTable
        // Params: path is the full path to the file.
        // ---------------------------------------------------------------
        public static DataTable ParseCSV(string path)
        {
            if (!File.Exists(path))
                return null;

            string full = Path.GetFullPath(path);
            string file = Path.GetFileName(full);
            string dir = Path.GetDirectoryName(full);

            //create the "database" connection string
            string connString = "Provider=Microsoft.Jet.OLEDB.4.0;"
              + "Data Source=\"" + dir + "\\\";"
              + "Extended Properties=\"text;HDR=Yes;FMT=Delimited\"";

            //create the database query
            string query = "SELECT * FROM [" + file +"]";

            //create a DataTable to hold the query results
            DataTable dTable = new DataTable();

            //create an OleDbDataAdapter to execute the query
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(query, connString);

            try
            {
                //fill the DataTable
                dAdapter.Fill(dTable);
            }
            catch (InvalidOperationException e)
            { Console.Write(e.ToString()); }
            catch (Exception e)
            { Console.Write(e.ToString()); }

            dAdapter.Dispose();

            return dTable;
        }


        // -------------------------------------------------------------
        // ParseExcel
        // Custom method to load a Excel into a DataTable
        // Params: path is the full path to the file.
        // ---------------------------------------------------------------
        public static DataTable ParseExcel(string path, string startrow, string sheetname)
        {
            if (!File.Exists(path))
                return null;

            string full = Path.GetFullPath(path);

            string filenamenoextension = Path.GetFileNameWithoutExtension(path);
            string file = Path.GetFileName(full);
            string dir = Path.GetDirectoryName(full);

            //create the "database" connection string
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;"
              + "Data Source=\"" + dir + "\\" + filenamenoextension + "\";";

            if(path.EndsWith(".xls")) {
                connString += "Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"";
            }else{
                //Assume xlsx
                connString += "Extended Properties=\"Excel 12.0 XML;HDR=YES;IMEX=1\"";
            }


            string Sheet1 = "";
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                conn.Open();
                DataTable dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                foreach (DataRow row in dtSchema.Rows)
                {
                    if (!row["TABLE_NAME"].ToString().Contains("FilterDatabase"))
                    {
                        //sheetNames.Add(new SheetName() { sheetName = row["TABLE_NAME"].ToString(), sheetType = row["TABLE_TYPE"].ToString(), sheetCatalog = row["TABLE_CATALOG"].ToString(), sheetSchema = row["TABLE_SCHEMA"].ToString() });
                        Sheet1 = row["TABLE_NAME"].ToString();
                        break;
                    }
                }
                //Sheet1 = dtSchema.Rows[0]["TABLE_NAME"].ToString();
                Sheet1 = Sheet1.Replace("'", "");
                Sheet1 = Sheet1.Replace("$", "");
                conn.Close();
            }

            Console.WriteLine("Sheetname : " + Sheet1 + " vs asked for SheetName of " + sheetname);
            if (sheetname.Length < 1)
            {
                sheetname = Sheet1;
            }


            //create the database query
            string query = "SELECT * FROM [" + sheetname + "$A" + startrow + ":Z20000]";  // A2:ZZ will start reading from 2nd row.

            //create a DataTable to hold the query results
            DataTable dTable = new DataTable();

            //create an OleDbDataAdapter to execute the query
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(query, connString);

  

            try
            {
                //fill the DataTable
                dAdapter.Fill(dTable);
            }
            catch (InvalidOperationException e)
            { Console.Write(e.ToString()); }
            catch (Exception e)
            { Console.Write(e.ToString()); }

            dAdapter.Dispose();

            // Convert All columns to String
            //dc.DataType = System.Type.GetType(typeString); // System.String

            return dTable;
        }



        // ----------------------------------------------------
        // Utility method to add a colun to a Data Table
        // Purpose: less to look at , less to write
        // ----------------------------------------------------
        static public DataTable addColumnToDataTable(DataTable dt, string colName, string typeString)
        {

            DataColumn dc = new DataColumn();
            dc.DataType = System.Type.GetType(typeString);
            dc.ColumnName = colName;
            dc.ReadOnly = false;
            dt.Columns.Add(dc);

            return dt;
        }



        // ----------------------------------------------------
        // Standard DataTable Printout
        // Print to Console headers and row data
        // ----------------------------------------------------
        public static void PrintDataTable(DataTable dt)
        {



            Console.WriteLine("{0} Table. \n", dt.TableName);
            for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
            {
                Console.Write(dt.Columns[curCol].ColumnName.Trim() + "\t");
            }
            Console.WriteLine("");
            for (int curRow = 0; curRow < dt.Rows.Count; curRow++)
            {
                for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
                {
                    Console.Write(dt.Rows[curRow][curCol].ToString().Trim() + "\t");
                }
                Console.WriteLine();
            }

        }

        // --------------------------------------------------
        // SplitToCSV
        //
        // --------------------------------------------------
        public static void SplitToCSV(string filename, DataTable dt, int rowsPerFile)
        {
            int curStartRow = 0;
            int fileCount = 0;
            while (curStartRow < dt.Rows.Count)
            {
                fileCount++;
                SaveToCSV(dt, Convert.ToString(fileCount) + "_" + filename, curStartRow, curStartRow + rowsPerFile);
                curStartRow += rowsPerFile;
            }


        }

        // --------------------------------------------------
        // SplitFilteredToCSV
        //
        // --------------------------------------------------
        public static void SplitFilteredToCSV(string dir, string filename, DataTable dt, int rowsPerFile, string[] colNames, string filter)
        {
            int curStartRow = 0;
            int fileCount = 0;
            while (curStartRow < dt.Rows.Count)
            {
                fileCount++;
                //SaveToCSV(dt, Convert.ToString(fileCount) + "_" + filename, curStartRow, curStartRow + rowsPerFile);

                SaveFilteredToCSV(dt, dir + Convert.ToString(fileCount) + "_" + filename, filter, colNames, curStartRow, curStartRow + rowsPerFile);
                curStartRow += rowsPerFile;
            }


        }


        // --------------------------------------------------
        // SaveToCSV
        //
        // Write the requested rows out to a file with the 
        // the header row.
        // --------------------------------------------------
        public static void SaveToCSV(DataTable dt, string filename, int startrow, int endrow)
        {
            // --------------------------------------------------
            // Check Range of rows. Adjust to be safe.
            // If startrow = 0 and endrow = 0; then all rows
            // --------------------------------------------------
            if (endrow == 0 || endrow >= dt.Rows.Count)
            {
                endrow = dt.Rows.Count;
            }
            if (startrow < 0)
            {
                startrow = 0;
            }
            if (startrow >= dt.Rows.Count)
            {
                startrow = endrow;
            }

            TextWriter tw = new StreamWriter(filename);

            // --------------------------------------------------
            // Write out the column names
            // --------------------------------------------------
            for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
            {
                if (curCol > 0)
                {
                    tw.Write(",");
                }
                tw.Write("\"" + dt.Columns[curCol].ColumnName.Trim() + "\"");
            }
            tw.WriteLine();

            // --------------------------------------------------
            // Write out the rows requested
            // --------------------------------------------------
            for (int curRow = startrow; curRow < endrow; curRow++)
            {
                for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
                {
                    if (curCol > 0)
                    {
                        tw.Write(",");
                    }

                    tw.Write("\"" + dt.Rows[curRow][curCol].ToString().Trim() + "\"");

                }
                tw.WriteLine();
            }
            // --------------------------------------------------
            // CLose and Clean
            // --------------------------------------------------
            tw.Close();
        }

        // --------------------------------------------------
        // SaveFilteredToCSV
        //
        // Write the requested rows out to a file with the 
        // the header row.
        // --------------------------------------------------
        public static void SaveFilteredToCSV(DataTable dt, string filename, string filter, string[] colNames)
        {
                SaveFilteredToCSV( dt,  filename,  filter, colNames, 0, 0); //all rows


        }


        // --------------------------------------------------
        // SaveFilteredToCSV
        //
        // Write the requested rows out to a file with the 
        // the header row.
        // --------------------------------------------------
        public static void SaveFilteredToCSV(DataTable dt, string filename, string filter, string[] colNames, int startrow, int endrow)
        {


    
            // Filter out the rows

            TextWriter tw = new StreamWriter(filename);

            DataRow[] foundRows;
            foundRows = dt.Select(filter);
            if (foundRows.Length < 1)
            {
                tw.Close();
                return;
            }
            // --------------------------------------------------
            // Check Range of rows. Adjust to be safe.
            // If startrow = 0 and endrow = 0; then all rows
            // --------------------------------------------------
            if (endrow == 0 || endrow >= foundRows.Length)
            {
                endrow = foundRows.Length;
            }
            if (startrow < 0)
            {
                startrow = 0;
            }
            if (startrow >= foundRows.Length)
            {
                startrow = endrow;
            }

            //Console.WriteLine(" FilterToCVS: Start:" + startrow + " end = " + endrow + " len = " + foundRows.Length.ToString());

           
            // --------------------------------------------------
            // Write out the column names
            // --------------------------------------------------
            for (int curCol = 0; curCol < colNames.Length; curCol++)
            {
                if (curCol > 0)
                {
                    tw.Write(",");
                }
                tw.Write("\"" + colNames[curCol] + "\"");
            }
            tw.WriteLine();

            // --------------------------------------------------
            // Write out the rows requested
            // --------------------------------------------------
            for (int curRow = startrow; curRow < endrow; curRow++)
            {
            //for (int curRow = 0; curRow < foundRows.Length; curRow++)
            
                for (int curCol = 0; curCol < colNames.Length; curCol++)
                {
                    if (curCol > 0)
                    {
                        tw.Write(",");
                    }
                    string col = colNames[curCol];
                    tw.Write("\"" + foundRows[curRow][col].ToString().Trim() + "\"");

                }
                tw.WriteLine();
            }
            // --------------------------------------------------
            // CLose and Clean
            // --------------------------------------------------
            tw.Close();
        }


        // --------------------------------------------------
        // SaveToFile
        //
        // Write the string to a file. Overwrite the file! 
        // --------------------------------------------------
        public static void SaveToFile(string fileName, string dataMsg)
        {
          

            TextWriter tw = new StreamWriter(fileName);

    
            tw.WriteLine(dataMsg);

      
            // --------------------------------------------------
            // CLose and Clean
            // --------------------------------------------------
            tw.Close();
        }


        // --------------------------------------------------------
        //
        // --------------------------------------------------------
        public static void ConstructSchema(string directory, string theFileName)
        {
            StringBuilder schema = new StringBuilder();
            //DataTable data = ParseCSV(directory + theFileName);
            DataTable data = ParseCSV(Path.Combine(directory, theFileName));

            if (data == null)
            {
                Console.WriteLine("CSV Was not loaded correctly.");
                return;
            }

            schema.AppendLine("[" + theFileName + "]");
            schema.AppendLine("ColNameHeader=True");
            for (int i = 0; i < data.Columns.Count; i++)
            {

                String col = data.Columns[i].ColumnName;
                // UTF-8 BOM in ISO-8859-1 encoding puts this ï»¿ in the file as the first 3 bytes.
                // It should open fine with the Provider, but this schema construction gets broken.
                // Remove it from the schema construction.
                Console.WriteLine("Schema: " + col);
                col = Regex.Replace(col,"[^a-zA-Z0-9_.]+","",RegexOptions.Compiled);
                schema.AppendLine("col" + (i + 1).ToString() + "=\"" + col + "\" Text");
            }
            string schemaFileName = directory + @"\Schema.ini";
            TextWriter tw = new StreamWriter(schemaFileName);
            tw.WriteLine(schema.ToString());
            tw.Close();
        }


        // --------------------------------------------------------
        //
        // --------------------------------------------------------
        public static void ConstructExcelSchema(string directory, string theFileName, string startrow, string sheetname)
        {
            StringBuilder schema = new StringBuilder();
            DataTable data = ParseExcel(directory + theFileName, startrow, sheetname);

            if (data == null)
            {
                Console.WriteLine("Excel Was not loaded correctly.");
                return;
            }

            schema.AppendLine("[" + theFileName + "]");
            schema.AppendLine("ColNameHeader=True");
            for (int i = 0; i < data.Columns.Count; i++)
            {

                String col = data.Columns[i].ColumnName;
                // UTF-8 BOM in ISO-8859-1 encoding puts this ï»¿ in the file as the first 3 bytes.
                // It should open fine with the Provider, but this schema construction gets broken.
                // Remove it from the schema construction.
                Console.WriteLine("Schema: " + col);
                col = Regex.Replace(col, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
                schema.AppendLine("col" + (i + 1).ToString() + "=\"" + col + "\" Text");
            }
            string schemaFileName = directory + @"\Schema.ini";
            TextWriter tw = new StreamWriter(schemaFileName);
            tw.WriteLine(schema.ToString());
            tw.Close();
        }



       

        // -------------------------------------------------------------------
        // Pad Column Value function
        // --------------------------------------------------------------------
        public static String PadColumnValue(string colvalue, int padlen)
        {

            int len = colvalue.Length;
            String newvalue = colvalue;

            //Pad only if all the characters are numeric
            double Num;
            bool isNum = double.TryParse(newvalue, out Num);

            //Console.WriteLine("PAD: " + padlen + " LEN: " + len + " isNum:" + isNum);
            while (isNum && len < padlen)
            {
                newvalue = "0" + newvalue;
                len++;
            }
            return newvalue;

        }


        // -------------------------------------------------------------------
        // Pad Column Value function
        // --------------------------------------------------------------------
        public static string PadColumnValueLeading(string colvalue, int padlen)
        {

            int len = colvalue.Length;
            string newvalue = "";

            //Pad only if all the characters are numeric
            double Num;
            bool isNum = double.TryParse(colvalue, out Num);

            Console.WriteLine("PAD: " + padlen + " LEN: " + len + " isNum:" + isNum);
            while (isNum && len < padlen)
            {
                newvalue = "0" + newvalue;
                len++;
            }
            return newvalue;

        }


        // -------------------------------------------------------------------
        // UnPad Column Value function
        // --------------------------------------------------------------------
        public static string UnPadColumnValue(string colvalue)
        {

            int len = colvalue.Length;


            int i = 0;
            while (colvalue.StartsWith("0"))
            {
                i++;
                if (i > 100)
                {
                    return colvalue; // safty net
                }
                colvalue = colvalue.Substring(1);
                //Console.WriteLine("UnPad: " + colvalue);
            }
            return colvalue;

        }  
        
        public static string CreateErrorJson(DataTable order, string fileName, int startRow, int endrow)
        {
            bool hasErrors = false;
            Dictionary<string, string> errorDict = new Dictionary<string, string>();
            string recordID, errorString;

            endrow = order.Rows.Count;
            string fileID = order.Rows[0][4].ToString();

            for (int curRow = 0; curRow < endrow; curRow++)
            {
                // TODO - Change column numbers to names
                recordID = order.Rows[curRow][9].ToString().Trim();
                errorString = order.Rows[curRow][43].ToString().Trim();

                if (!String.IsNullOrEmpty(errorString))
                {
                    errorDict.Add(recordID, errorString);
                }
            }

            if(errorDict.Count > 0)
            {
                hasErrors = true;
            }

            //                {
            //                    "success": false, 
            //                     "meta": {
            //                        "fileID" : "8a670488-6f28-4be1-9954-fecd70c7b2c0",
            //                        "ErrorIds" : [{"Id": "53d0c154-879a-4312-a543-4cc8094dd6f1","ErrorMsg":"Error"},
            //                          ]
            //                          //key/value pairs       
            //                      }
            //                      "applicationName": "ImportApplication",
            //                      "error": {
            //                              "errorMessage": "" ,
            //                              "errorCode": "",
            //                              "errorType":"" 
            //                      }
            //              }

            JObject errorRootObj = new JObject();// root Object
            JProperty successProperty = new JProperty("success", hasErrors.ToString());

            errorRootObj.Add(successProperty);

            JArray errorArray = new JArray();
            for(int i = 0; i < errorDict.Count; i++)
            {
                JObject tempObj = new JObject();
                JProperty idProp = new JProperty("Id", errorDict.Keys.ElementAt(i));
                JProperty valueProp = new JProperty("ErrorMsg", errorDict.Values.ElementAt(i));
                tempObj.Add(idProp);
                tempObj.Add(valueProp);
                errorArray.Add(tempObj);
            }

            JObject metaObject = new JObject();

            JProperty fileIDProperty = new JProperty("fileID", fileID);
            metaObject.Add(fileIDProperty);
            metaObject.Add(new JProperty("ErrorIDs", errorArray));

            JProperty metaProperty = new JProperty("meta", metaObject);
            errorRootObj.Add(metaProperty);

            errorRootObj.Add(new JProperty("applicationName", "Symphony.StudentImportApplication"));

            JObject errorObj = new JObject();
            errorObj.Add(new JProperty("errorMessage", ""));
            errorObj.Add(new JProperty("errorCode", ""));
            errorObj.Add(new JProperty("errorType", ""));

            JProperty errorProp = new JProperty("error", errorObj);

            errorRootObj.Add(errorProp);

            string outString = errorRootObj.ToString();

            return outString;
        }
        
        // -------------------------------------------------------------------------
        // Mimics the ASP.NET security user's password Hash so we can do the 
        // change compare.
        // -------------------------------------------------------------------------

        public static string EncodePassword(string pass, string saltBase64)
        {
            try
            {
                byte[] bytes = Encoding.Unicode.GetBytes(pass);
                byte[] src = Convert.FromBase64String(saltBase64);
                byte[] dst = new byte[src.Length + bytes.Length];
                Buffer.BlockCopy(src, 0, dst, 0, src.Length);
                Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
                HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
                byte[] inArray = algorithm.ComputeHash(dst);
                return Convert.ToBase64String(inArray);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "*BADSALT*";
            }
        }

    }
}