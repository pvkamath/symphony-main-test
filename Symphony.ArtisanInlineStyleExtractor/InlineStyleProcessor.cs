﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SubSonic;
using log4net;
using Symphony.Core.Models;
using Data = Symphony.Core.Data;
using Symphony.Core.Extensions;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace Symphony.ArtisanInlineStyleExtractor
{
    public class InlineStyleProcessor
    {
        public void RevertInlineStyleForTemplate(ILog log)
        {
            List<Data.ArtisanTemplate> templates = Select.AllColumnsFrom<Data.ArtisanTemplate>().ExecuteTypedList<Data.ArtisanTemplate>();

            foreach (Data.ArtisanTemplate template in templates)
            {
                if (!string.IsNullOrWhiteSpace(template.InlineStyledHtml))
                {
                    log.Info("Reverting html for " + template.Name);
                    template.Html = template.InlineStyledHtml;
                    template.InlineStyledHtml = null;
                    template.Save();
                }
            }
        }

        public void RevertInlineStyleForCourses(ILog log, List<GenericFilter> filters = null)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ArtisanPage>()
                .InnerJoin(Data.ArtisanCourse.IdColumn, Data.ArtisanPage.CourseIDColumn);

            if (filters != null)
            {
                query.ApplyGenericFilter<Data.ArtisanPage>(filters);
            }

            List<Data.ArtisanPage> pages = query.ExecuteTypedList<Data.ArtisanPage>();

            foreach (Data.ArtisanPage page in pages)
            {
                if (!string.IsNullOrWhiteSpace(page.InlineStyledHtml))
                {
                    log.Info("Reverting html for page: " + page.Name + " ID: " + page.Id + " Course ID: " + page.CourseID);
                    page.Html = page.InlineStyledHtml;
                    page.InlineStyledHtml = null;
                    page.Save();
                }
            }
        }

        public void FixInlineStylesForCourses(ILog log, List<GenericFilter> filters = null)
        {

            SqlQuery query = Select.AllColumnsFrom<Data.ArtisanPage>()
                .InnerJoin(Data.ArtisanCourse.IdColumn, Data.ArtisanPage.CourseIDColumn)
                .Where(Data.ArtisanCourse.IsPublishedColumn).IsEqualTo(false);

            if (filters != null)
            {
                query.ApplyGenericFilter<Data.ArtisanPage>(filters);
            }

            int totalPages = query.GetRecordCount();
            int pageSize = 2000;
            int pageCount = totalPages / pageSize;

            log.Info("Loading " + totalPages);

            for (int pageIndex = 0; pageIndex <= pageCount; pageIndex++)
            {
                log.Info("Loading page index: " + pageIndex);

                PagedResult<Data.ArtisanPage> pages = new PagedResult<Data.ArtisanPage>(query, pageIndex, pageSize, Data.ArtisanPage.IdColumn.QualifiedName, Core.OrderDirection.Ascending);

                log.Info("Loaded " + pages.Data.Count() + " pages");

                foreach (Data.ArtisanPage page in pages.Data)
                {
                    log.Info("Fixing html for page: " + page.Name + " ID: " + page.Id + " Course ID: " + page.CourseID);

                    if (string.IsNullOrWhiteSpace(page.InlineStyledHtml))
                    {
                        page.InlineStyledHtml = page.Html;
                    }

                    page.Html = ProcessHtmlForInlineStyles(page.Html, log);

                    page.Save();
                }
            }
        }

        public void FixInlineStylesForTemplates(ILog log)
        {
            List<Data.ArtisanTemplate> templates = Select.AllColumnsFrom<Data.ArtisanTemplate>().ExecuteTypedList<Data.ArtisanTemplate>();

            foreach (Data.ArtisanTemplate template in templates)
            {
                log.Info("Fixing html for template: " + template.Name);
                    
                if (string.IsNullOrWhiteSpace(template.InlineStyledHtml))
                {
                    template.InlineStyledHtml = template.Html;
                }

                template.Html = ProcessHtmlForInlineStyles(template.Html, log);

                template.Save();
            }
        }

        private string ProcessHtmlForInlineStyles(string html, ILog log)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            OrderedDictionary styleClassMap = new OrderedDictionary() {
                // Contains first

                // 2 column layouts
                { new KeyValuePair<string, string>("contains","float:left;width:48%;"), "column half left" },
                { new KeyValuePair<string, string>("contains","float:right;width:48%;"), "column half right"},

                { new KeyValuePair<string, string>("contains","width:48%;float:left;"), "column half left"},
                { new KeyValuePair<string, string>("contains","width:48%;float:right;"), "column half right"},
                
                { new KeyValuePair<string, string>("contains","float:left;width:48%"), "column half left"},
                { new KeyValuePair<string, string>("contains","float:right;width:48%"), "column half right"},

                { new KeyValuePair<string, string>("contains","width:48%;float:left"), "column half left"},
                { new KeyValuePair<string, string>("contains","width:48%;float:right"), "column half right"},

                // 3 column layouts
                { new KeyValuePair<string, string>("contains","width:32%;float:left;max-height:420px;margin-right:2%;"), "column third restrict-height"},
                { new KeyValuePair<string, string>("contains","float:left;width:32%;max-height:420px;margin-right:2%;"), "column third restrict-height"},
                
                { new KeyValuePair<string, string>("contains","width:32%;float:left;max-height:420px;margin-right:2%"), "column third restrict-height"},
                { new KeyValuePair<string, string>("contains","float:left;width:32%;max-height:420px;margin-right:2%"), "column third restrict-height"},

                { new KeyValuePair<string, string>("contains","float:left;width:32%;max-height:420px;"), "column third last restrict-height"},
                { new KeyValuePair<string, string>("contains","width:32%;float:left;max-height:420px;"), "column third last restrict-height"},

                { new KeyValuePair<string, string>("contains","float:left;width:32%;max-height:420px"), "column third last restrict-height"},
                { new KeyValuePair<string, string>("contains","width:32%;float:left;max-height:420px"), "column third last restrict-height"},

                { new KeyValuePair<string, string>("contains","width:32%;float:left;margin-right:2%;"), "column third"},
                { new KeyValuePair<string, string>("contains","float:left;width:32%;margin-right:2%;"), "column third"},
                
                { new KeyValuePair<string, string>("contains","width:32%;float:left;margin-right:2%"), "column third"},
                { new KeyValuePair<string, string>("contains","float:left;width:32%;margin-right:2%"), "column third"},
                
                { new KeyValuePair<string, string>("contains","width:32%;float:left;"), "column third last"},
                { new KeyValuePair<string, string>("contains","float:left;width:32%;"), "column third last"},
                
                // full width layouts
                { new KeyValuePair<string, string>("contains","clear:both;overflow:auto;"), "column full break overflowauto"},
                { new KeyValuePair<string, string>("contains","clear:both;overflow:auto"), "column full break overflowauto"},

                // Do direct comparisons last

                // full width layouts
                { new KeyValuePair<string, string>("equals","overflow:auto;"), "column full overflowauto"},
                { new KeyValuePair<string, string>("equals","overflow:auto"), "column full overflowauto"},
                { new KeyValuePair<string, string>("equals","text-align:center;"), "column full text-center"},
                { new KeyValuePair<string, string>("equals","text-align:center"), "column full text-center"},
                { new KeyValuePair<string, string>("equals","overflow:auto;vertical-align:top;"), "column full overflowauto vtop"},

                // 2 columns
                { new KeyValuePair<string, string>("equals","width:48%;"), "column half left"},
                { new KeyValuePair<string, string>("equals","width:48%;float:left;"), "column half left"},
                { new KeyValuePair<string, string>("equals","width:48%;float:right;"), "column half right"},
            };

            List<string> selectors = new List<string>{
                // 1. Most templates have a wrapper div with 0 attributes. <div class="contentwrapper"><div> ... want these column divs ... </div></div>
                "//div[@class='ContentWrapper']/div[not(@*)]/div[contains(@class, 'p_wrap') or contains(@class, 'element_text') or contains(@class, 'element_media')]",
                // 2. Some templates do not have the wrapper div with 0 attributes. so check the direct descendents of content wrapper
                "//div[@class='ContentWrapper']/div[contains(@class, 'p_wrap') or contains(@class, 'element_text') or contains(@class, 'element_media')]",
                // 3. Some templates just have a div without a p_wrap class to select on. If one of these divs matches our rules above replace the class as well. 
                "//div[@class='ContentWrapper']/div/div[not(contains(@class, 'column')) and (contains(@style, 'width:48%') or contains(@style, 'width:32%') or contains(@style, 'width: 48%') or contains(@style, 'width: 32%'))]",
                // 4. Same as 3, but without the extra wrapping div
                "//div[@class='ContentWrapper']/div[not(contains(@class, 'column')) and (contains(@style, 'width:48%') or contains(@style, 'width:32%') or contains(@style, 'width: 48%') or contains(@style, 'width: 32%'))]",
                // 5. One template has another extra wrapper!!!!
                "//div[@class='ContentWrapper']/div[@class='ArtisanContentTemplate05']//div[not(@*)]/div[contains(@class, 'p_wrap') or contains(@class, 'element_text') or contains(@class, 'element_media')]"
            };

            foreach (string selector in selectors)
            {
                // Look at top level divs in our content structure. These need to be defined as our columns first. 
                var columnNodes = doc.DocumentNode.SelectNodes(selector);
                if (columnNodes != null)
                {
                    foreach (HtmlNode node in columnNodes)
                    {
                        HtmlAttribute style = node.Attributes["style"];

                        if (style != null)
                        {
                            bool isStyleReplaced = false;

                            style.Value = Regex.Replace(style.Value, @"\s+", "");

                            IDictionaryEnumerator enumerator = styleClassMap.GetEnumerator();

                            while (enumerator.MoveNext())
                            {
                                KeyValuePair<string, string> enumKey = (KeyValuePair<string, string>)enumerator.Key;
                                
                                var inlineStyle = (string)enumKey.Value;
                                var cls = (string)enumerator.Value;
                                var cmpare = (string)enumKey.Key;

                                if (cmpare == "contains" && style.Value.Contains(inlineStyle) ||
                                    cmpare == "equals" && style.Value == inlineStyle)
                                {
                                    if (node.Attributes["class"] != null)
                                    {
                                        if (!node.Attributes["class"].Value.Contains("column"))
                                        {
                                            node.Attributes["class"].Value += " " + cls;
                                        }
                                    }
                                    else
                                    {
                                        node.Attributes.Add("class", cls);
                                    }
                                    style.Value = style.Value.Replace(inlineStyle, "");

                                    log.Debug("Replacing '" + inlineStyle + "' with class: '" + cls + "'");

                                    isStyleReplaced = true;
                                }
                            }

                            if (!isStyleReplaced)
                            {
                                log.Debug("Could not find replacement for: '" + style.Value + "' the inline style will be removed anyway.");
                            }

                            if (string.IsNullOrWhiteSpace(style.Value) || style.Value == ";")
                            {
                                node.Attributes.Remove("style");
                            }
                        }


                        if (node.Attributes["style"] == null && node.Attributes["class"] != null && node.Attributes["class"].Value == "p_wrap" && selector == selectors[0])
                        {
                            log.Debug("Default column");
                            node.Attributes["class"].Value += " column full";
                        }
                    }
                }
            }

            var brNodes = doc.DocumentNode.SelectNodes("//br[@style='clear:both' or @style='clear: both;' or @style='clear: both']");
            var h3Nodes = doc.DocumentNode.SelectNodes("//h3[@style='overflow:hidden;' or @style='overflow: hidden;']");

            if (brNodes != null)
            {
                foreach (HtmlNode node in brNodes)
                {
                    node.Attributes.Add("class", "break");
                    node.Attributes.Remove("style");
                    log.Debug("Adding break to BR");
                }
            }

            if (h3Nodes != null)
            {
                foreach (HtmlNode node in h3Nodes)
                {
                    node.Attributes.Remove("style");
                    log.Debug("Removing H3 style");
                }
            }

            List<string> innerNodeSelectors = new List<string>() {
                "//div[contains(@class, 'column')]/div[contains(@class, 'element_text') or contains(@class, 'element_media') or contains(@class, 'p_wrap')]",
                "//div[contains(@class, 'column')]/div[contains(@class, 'p_wrap')]/div[contains(@class, 'element_text') or contains(@class, 'element_media')]"
            };
            foreach (string innerSelector in innerNodeSelectors)
            {
                var innerNodes = doc.DocumentNode.SelectNodes(innerSelector);

                if (innerNodes != null)
                {
                    OrderedDictionary innerStyleMapping = new OrderedDictionary
                    {
                        { "text-align:left;", "text-left" },
                        { "overflow:auto; vertical-align:top;", "overflowauto vtop" },
                        { "text-align:center;", "text-center" },
                        { "overflow:hidden;", "overflowhidden" },
                        { "overflow:auto;", "overflowauto"},
                        { "vertical-align:top;", "vtop" },
                        { "min-;", ""},
                        { "width:100%;", "full" },
                        { "text-align:left", "text-left" },
                        { "overflow:auto;vertical-align:top", "overflowauto vtop" },
                        { "text-align:center", "text-center" },
                        { "overflow:hidden", "overflowhidden" },
                        { "overflow:auto", "overflowauto"},
                        { "vertical-align:top", "vtop" },
                        { "min-", ""},
                        { "width:100%", "full" }
                    };

                    foreach (HtmlNode node in innerNodes)
                    {
                        if (node.Attributes["style"] == null)
                        {
                            continue;
                        }

                        var style = node.Attributes["style"];

                        style.Value = Regex.Replace(style.Value, @"\s+", "");

                        var enumerator = innerStyleMapping.GetEnumerator();

                        while (enumerator.MoveNext())
                        {
                            var inlineStyle = (string)enumerator.Key;
                            var cls = (string)enumerator.Value;

                            if (style.Value.Contains(inlineStyle))
                            {
                                if (node.Attributes["class"] != null)
                                {
                                    if (!node.Attributes["class"].Value.Contains("column"))
                                    {
                                        node.Attributes["class"].Value += " " + cls;
                                    }
                                }
                                else
                                {
                                    node.Attributes.Add("class", cls);
                                }
                                style.Value = style.Value.Replace(inlineStyle, "");

                                log.Debug("Replacing '" + inlineStyle + "' with class: '" + cls + "'");
                            }

                            if (style.Value == " " || style.Value == ";")
                            {
                                style.Value = "";
                            }
                        }

                        if (string.IsNullOrWhiteSpace(style.Value) || style.Value == ";")
                        {
                            node.Attributes.Remove("style");
                        }
                    }
                }
            }

            // Clean up one more style
            var relativeWrappers = doc.DocumentNode.SelectNodes("//div[@class='ContentWrapper']/div[@style='position:relative;' or @style='position: relative;']");
            if (relativeWrappers != null)
            {
                foreach (HtmlNode node in relativeWrappers)
                {
                    node.Attributes.Remove("style");
                    node.Attributes.Add("class", "positionrelative");
                }
            }
            return doc.DocumentNode.WriteContentTo();
        }
    }
}
