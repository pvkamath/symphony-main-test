﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Configuration;
using Symphony.Core.Models;
using Newtonsoft.Json;

namespace Symphony.ArtisanInlineStyleExtractor
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("----- Artisan Inline Style Extractor ----");
            
            Console.WriteLine("Initializing...");

            Log.Setup();


            string templates = "templates";
            string pages = "pages";
            List<string> validObjects = new List<string> { templates, pages };

            bool isRevert = false;
            string targets = templates;
            
            List<GenericFilter> filters = null;

            bool.TryParse(ConfigurationManager.AppSettings["isRevert"], out isRevert);

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["targets"]))
            {
                targets = ConfigurationManager.AppSettings["targets"];
            }

            string validTargets = "";
            foreach (string target in validObjects)
            {
                if (targets.Contains(target))
                {
                    validTargets += (!string.IsNullOrWhiteSpace(validTargets) ? ", " : "") + target;
                }
            }
            targets = validTargets;

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["filters"])) 
            {
                string filterJson = ConfigurationManager.AppSettings["filters"];

                try
                {
                    filters = JsonConvert.DeserializeObject<List<GenericFilter>>(filterJson);
                }
                catch (Exception e)
                {
                    Log.log.Error("Could not parse filter json: " + filterJson, e);
                }
            }

            Console.WriteLine("Style extractor will run with the following settings:");
            Console.WriteLine("  Revert: " + isRevert);
            Console.WriteLine("  Targets: " + targets);
            Console.WriteLine("  Filters: " + JsonConvert.SerializeObject(filters));

            Console.WriteLine("");

            Console.WriteLine("Is this what you want? (Y/N)");


            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine("Running...");
                InlineStyleProcessor processor = new InlineStyleProcessor();

                if (targets.Contains(templates))
                {
                    if (isRevert)
                    {
                        processor.RevertInlineStyleForTemplate(Log.log);
                    }
                    else
                    {
                        processor.FixInlineStylesForTemplates(Log.log);
                    }
                }

                if (targets.Contains(pages)) {
                    if (isRevert)
                    {
                        processor.RevertInlineStyleForCourses(Log.log, filters);
                    }
                    else
                    {
                        processor.FixInlineStylesForCourses(Log.log, filters);
                    }
                }
                Console.WriteLine("Finished.");
            }
            else
            {
                Console.WriteLine("Ok, update the app.settings file and try again.");
            }
            Console.ReadLine();
        }
        
       
    }
}
