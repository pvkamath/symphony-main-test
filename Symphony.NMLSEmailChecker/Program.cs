﻿using HtmlAgilityPack;
using log4net;
using OpenPop.Mime;
using OpenPop.Pop3;
using SubSonic;
using Symphony.Core.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Data = Symphony.Core.Data;

namespace Symphony.NMLSEmailChecker
{
    class Program
    {
        static ILog Log = LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            try
            {
                string configHost = ConfigurationManager.AppSettings["host"];
                string configPort = ConfigurationManager.AppSettings["port"];
                string configUseSsl = ConfigurationManager.AppSettings["useSsl"];
                string configUsername = ConfigurationManager.AppSettings["username"];
                string configPassword = ConfigurationManager.AppSettings["password"];

                var messages = FetchAllMessages(configHost, int.Parse(configPort), bool.Parse(configUseSsl), configUsername, configPassword);

                foreach (var message in messages)
                {
                    try
                    {
                        // store the message itself
                        var body = message.FindFirstHtmlVersion().GetBodyAsText();
                        var subject = message.Headers.Subject;
                        var from = message.Headers.From.Raw;

                        if (subject.ToLower().IndexOf("confirmation") == -1)
                        {
                            Log.Warn("Skipping email:" + subject);
                            continue;
                        }

                        NmlsMessage email = new NmlsMessage()
                        {
                            Body = body,
                            Subject = subject,
                            From = from
                        };
                        email.Save();

                        // process the email message and extract the individual results from the html
                        var imports = ProcessEmail(email);

                        // process the imports and match them up to students
                        foreach (var nmlsImport in imports)
                        {
                            var users =
                                Select.AllColumnsFrom<Data.FastSearchableUser>()
                                .Where(Data.FastSearchableUser.Columns.NmlsNumber).IsEqualTo(nmlsImport.Mloid)
                                .And(Data.FastSearchableUser.Columns.StatusID).IsEqualTo(Symphony.Core.UserStatusType.Active)
                                .ExecuteTypedList<Data.FastSearchableUser>();

                            if (users == null)
                            {
                                // no match on nmls id - try name
                                users =
                                   Select.AllColumnsFrom<Data.FastSearchableUser>()
                                   .Where(Data.FastSearchableUser.Columns.FullName).Like("%" + nmlsImport.StudentName + "%")
                                   .And(Data.FastSearchableUser.Columns.StatusID).IsEqualTo(Symphony.Core.UserStatusType.Active)
                                   .ExecuteTypedList<Data.FastSearchableUser>();
                            }

                            if (users != null)
                            {
                                // got a match on the user; make sure it's not ambiguous
                                if (users.Count == 1)
                                {
                                    var user = users.First();

                                    
                                    // this gets all assigned training programs
                                    var userAssignedTps = Select.AllColumnsFrom<Data.HierarchyToTrainingProgramLink>()
                                        .Where(Data.HierarchyToTrainingProgramLink.Columns.HierarchyNodeID).IsEqualTo(user.Id)
                                        .And(Data.HierarchyToTrainingProgramLink.Columns.HierarchyTypeID).IsEqualTo(Symphony.Core.HierarchyType.User)
                                        .ExecuteTypedList<Data.HierarchyToTrainingProgramLink>();

                                    if (userAssignedTps.Count != 0)
                                    {
                                        // this gets all licenses for all assigned training programs
                                        // that match the corresponding 
                                        var licenses = Select.AllColumnsFrom<Data.TrainingProgramLicense>()
                                            .Where(Data.TrainingProgramLicense.Columns.TrainingProgramID)
                                            .In(userAssignedTps.Select(uatp => uatp.TrainingProgramID))
                                            .And(Data.TrainingProgramLicense.Columns.LicensedCourseID).IsEqualTo(nmlsImport.CourseID)
                                            .ExecuteTypedList<Data.TrainingProgramLicense>();

                                        if (licenses.Count != 0)
                                        {
                                            // ok, we know what license the user is supposed to get, and we know what training program it's for...save it!
                                            foreach (var license in licenses)
                                            {
                                                LicenseAssignment la = new LicenseAssignment();
                                                la.UserID = user.Id;
                                                la.LicenseID = license.Id;
                                                la.TrainingProgramId = license.TrainingProgramID;
                                                la.ConfirmationNumber = nmlsImport.ConfirmationID;
                                                try
                                                {
                                                    la.StartDate = DateTime.Parse(nmlsImport.CompletionDate);
                                                }
                                                catch
                                                {
                                                    la.StartDate = DateTime.UtcNow;
                                                }
                                                la.Save();

                                                /*UserLicenseNumber ula = new UserLicenseNumber();
                                                ula.UserID = user.Id;
                                                ula.LicenseID = license.Id;
                                                // TODO: fix this
                                                // TODO: licenses need history...they expire, so we need to have a start/end date
                                                // where are we using the user license number, and should we be doing something else?
                                                ula.LicenseNumber = user.NmlsNumber;
                                                ula.ConfirmationNumber = nmlsImport.ConfirmationID;
                                                ula.ConfirmationDate = DateTime.UtcNow;
                                                ula.Save();*/
                                            }
                                        }
                                        else
                                        {
                                            var ids = string.Join(",", userAssignedTps.Select(x => x.TrainingProgramID).ToArray());
                                            Log.Error("No license found for user " + user.Id + " for training programs " + ids + " with NMLS course id " + nmlsImport.CourseID);
                                        }
                                    }
                                    else
                                    {
                                        Log.Error("Whoa, got a user that had no training...but somehow got NMLS approval?");
                                    }
                                }
                                else
                                {
                                    // error
                                    Log.Warn("No user record found for " + nmlsImport.Mloid + " or " + nmlsImport.StudentName);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public static List<NmlsImport> ProcessEmail(NmlsMessage message)
        {
            List<NmlsImport> imports = new List<NmlsImport>();

            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(message.Body);

            var tables = document.DocumentNode.SelectNodes("//table");
            foreach (var table in tables)
            {
                var rows = table.SelectNodes(".//tr");
                var headers = table.SelectNodes(".//th");
                var headerNames = new List<string>();

                foreach (var header in headers)
                {
                    headerNames.Add(header.InnerText);
                }

                foreach (var row in rows)
                {
                    try
                    {
                        NmlsImport import = new NmlsImport()
                        {
                            NmlsMessageID = message.Id
                        };


                        var cells = row.SelectNodes(".//td");
                        if (cells == null)
                        {
                            // for ex, header row
                            continue;
                        }
                        if (cells.Count != headerNames.Count)
                        {
                            Log.Warn("Cell count mismatch! - NMLS Message ID: " + message.Id + ", row content: " + row.InnerHtml);
                            continue;
                        }

                        for (var i = 0; i < headerNames.Count; i++)
                        {
                            var text = cells[i].InnerText.Trim();
                            var digits = Regex.Replace(text, @"[^\d\.]", string.Empty);

                            int itext = 0;
                            int.TryParse(digits, out itext);
                            decimal dtext = 0;
                            decimal.TryParse(digits, out dtext);

                            switch (headerNames[i].ToLower().Trim())
                            {
                                case "confirm id":
                                    import.ConfirmationID = text;
                                    break;
                                case "course id":
                                    import.CourseID = text;
                                    break;
                                case "course name":
                                    import.CourseName = text;
                                    break;
                                case "completion date":
                                    import.CompletionDate = text;
                                    break;
                                case "hrs":
                                    import.Hours = itext;
                                    break;
                                case "location":
                                    import.Location = text;
                                    break;
                                case "student name":
                                    import.StudentName = text;
                                    break;
                                case "mlo id":
                                    import.Mloid = text;
                                    break;
                                case "fee":
                                    import.Fee = (int)(dtext * 100);
                                    break;
                            }
                        }
                        import.Save();

                        imports.Add(import);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                }
            }

            return imports;
        }

        public static List<Message> FetchAllMessages(string hostname, int port, bool useSsl, string username, string password)
        {
            // The client disconnects from the server when being disposed
            using (Pop3Client client = new Pop3Client())
            {
                // Connect to the server
                client.Connect(hostname, port, useSsl);

                // Authenticate ourselves to the server
                client.Authenticate(username, password);

                // Get the number of messages in the inbox
                int messageCount = client.GetMessageCount();
#if DEBUG
                messageCount = 10;
#endif
                // We want to download all messages
                List<Message> allMessages = new List<Message>(messageCount);

                // Messages are numbered in the interval: [1, messageCount]
                // Ergo: message numbers are 1-based.
                // Most servers give the latest message the highest number
                for (int i = messageCount; i > 0; i--)
                {
                    allMessages.Add(client.GetMessage(i));
#if DEBUG
                    //break; //TEMPORARY!!! JUST FOR TESTING!!!
#endif
                }

                // Now return the fetched messages
                return allMessages;
            }
        }
    }
}
