﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ArtisanImportRunner
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                if (args.Length == 0)
                {
                    WriteUsage();
                }
                else if (args.Length > 4)
                {
                    WriteUsage();
                    Console.WriteLine("NOTE: it appears that you may have used a path with spaces in it; if the path has spaces in it, it *must* be enclosed in quotes!");
                }
                else
                {
                    int customerId = int.Parse(args[0]);
                    int userId = int.Parse(args[1]);
                    if (args[2].Trim() == "-single")
                    {
                        Import(customerId, userId, args[3]);
                    }
                    else if (args[2].Trim() == "-multiple")
                    {
                        foreach (string directory in Directory.GetDirectories(args[3]))
                        {
                            Import(customerId, userId, directory);
                        }
                    }
                    else if (args[2].Trim() == "-singleHealthCare")
                    {
                        /*
                        HealthcareImporter importer = new HealthcareImporter();
                        var topicId = Convert.ToInt32(args[3]);
                        int artisanCourseId = -1;
                        importer.ImportCourse(customerId, userId, topicId, out artisanCourseId);
                        Console.WriteLine("New Artisan course id: " + artisanCourseId.ToString());
                         * */
                    }
                    else
                    {
                        WriteUsage();
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.ToString());
            }
            Console.WriteLine("Import finished. Press any key to exit...");
            Console.ReadLine();
        }

        private static void Import(int customerId, int userId, string folder)
        {
            if (!Directory.Exists(folder))
            {
                throw new Exception("The specified path doesn't exist. If there are spaces in the path, be sure to wrap it in quotes.");
            }
            var importer = new ArtisanImporter.DUImporter(customerId, userId, folder);
            importer.Process();
        }

        private static void WriteUsage()
        {
            Console.WriteLine(@"
Artisan Course Importer
-----------------------

Currently, this importer only supports DU courses. Please ensure the connection string is set correctly in the app.config for the database into which you want the courses imported.

Parameters: 

  {customerId}: the integer id of the customer under which the import will occur

  {userId}: the integer id of the user under which the import will occur

  -single {path}: imports a single course from the specified path. The course contents must be directly under the specified path.

  -multiple {path}: imports all courses in the specified path. The courses must each be in individual folders under the specified path.

You can only use one parameter or the other, not both.

Example: ArtisanImportRunner 1 147 -single ""C:\temp\Course-DU-Teller Customer Service Standards - PT""
");
        }
    }
}
