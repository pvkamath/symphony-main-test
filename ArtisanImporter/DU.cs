﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using Symphony.Core.Models;
using Symphony.Core;
using Symphony.Core.Controllers;
using System.Diagnostics;
using HtmlAgilityPack;
using System.Net;
using SubSonic;

namespace ArtisanImporter
{
    public class DUImporter
    {
        private string template = @"
            <div id=""Main"" class=""Main"">
              <div class=""MainWrapper"">
                <div id=""Content"" class=""Content"">
                  <div class=""ContentWrapper"">
                    {0}
                  </div>
                </div>
              </div>
            </div>
<script type='text/javascript'>
var popupWindow = null;
function centeredPopup(url,winName,w,h,scroll){{
  var leftPosition = (screen.width) ? (screen.width-w)/2 : 0;
  var topPosition = (screen.height) ? (screen.height-h)/4 : 0;
  var settings = 'height='+h+',width='+w+',top='+topPosition+',left='+leftPosition+',scrollbars='+scroll+',resizable,location=no';
  window.open(url,winName,settings)
}}
</script>
";



        private int PageCounter = -1;
        private ArtisanController Controller;
        private string Root = string.Empty;
        private Random Random;
        private int CustomerID = 1;
        private int UserID = 147;

        public DUImporter(int customerId, int userId, string root)
        {
            CustomerID = customerId;
            UserID = userId;
            Root = root;
            Random = new Random();
        }

        public void Process()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            string[] files = Directory.GetFiles(Root, "*.xml");
            if (files.Length == 0)
            {
                throw new Exception("Couldn't find the root XML file!");
            }

            Console.WriteLine("Processing " + files[0]);

            string text = File.ReadAllText(files[0].Trim());
            text = text.Replace("&quote;", "&quot;"); // ugh, invalid XML
            XElement root = XElement.Parse(text);

            var name = from course in root.Elements("course")
                       select course.Attribute("name").Value;

            string objectives = string.Empty;

            try
            {
                string.Join("\n", root.Element("courseMetadata")
                     .Element("objectives")
                     .Descendants("objective")
                     .Select(o => o.Value));
            }
            catch { }

            string description = string.Empty;

            // don't care if it fails
            try
            {
                description = root.Element("courseMetadata")
                         .Element("abstract")
                         .Value;
            }
            catch { }

            var tutorials = root.Elements("tutorial");

            Controller = new ArtisanController();

            var id = root.Attribute("id").Value;
            var courseName = root.Attribute("name").Value + " (" + id + ")";

            ArtisanCourse ac = new ArtisanCourse();
            ac.Description = description;
            ac.Objectives = objectives;
            ac.Name = courseName;
            ac.Keywords = id;

            ac.PassingScore = 70;
            ac.Sections = new List<ArtisanSection>();
            ac.NavigationMethod = ArtisanCourseNavigationMethod.FreeForm;
            ac.CompletionMethod = ArtisanCourseCompletionMethod.Test;

            Symphony.Core.Data.ArtisanTheme theme = new Symphony.Core.Data.ArtisanTheme("Name", "BE Smooth");
            ac.ThemeId = theme.Id;

            Symphony.Core.Data.Category category = new Symphony.Core.Data.Category("Name", "Default");
            ac.CategoryId = category.Id;

            // DU courses always have 1 sco, named by the course
            var sco = new ArtisanSection()
            {
                Name = ac.Name,
                SectionType = (int)ArtisanSectionType.Sco
            };
            ac.Sections.Add(sco);
            sco.Sections = new List<ArtisanSection>();

            // add the post-test
            var postTest = new ArtisanSection()
            {
                SectionType = (int)ArtisanSectionType.Posttest,
                TestType = ArtisanTestType.Random,
                Name = "Post-Test"
            };
            ac.Sections.Add(postTest);
            postTest.Pages = new List<ArtisanPage>();

            bool hasExamOnlyQuestions = false;
            foreach (string questionFile in Directory.GetFiles(Path.Combine(Root, "Questions"), "*.xml"))
            {
                var page = GetQuestionPage(questionFile);
                if (page.Tag == "examonly")
                {
                    hasExamOnlyQuestions = true;
                    break;
                }
            }

            // "tutorials" map to LOs
            var learningObjectIndex = 0;
            foreach (var tutorial in tutorials)
            {
                var learningObject = new ArtisanSection()
                {
                    Name = tutorial.Attribute("name").Value,
                    SectionType = (int)ArtisanSectionType.LearningObject
                };
                sco.Sections.Add(learningObject);
                learningObject.Pages = new List<ArtisanPage>();

                foreach (var article in tutorial.Elements("article"))
                {
                    string contentFile = Path.Combine(Root, "Articles", article.Attribute("id").Value.Trim() + ".xml");

                    List<ArtisanAsset> ignored = new List<ArtisanAsset>();
                    bool isIntro = article.Attribute("id").Value.EndsWith("-I");
                    bool isSummary = article.Attribute("id").Value.EndsWith("-S");

                    var hasContent = HasContent(contentFile);

                    if (isIntro)
                    {
                        // get the contents, then strip the html out and put in the objectives
                        var loObjectives = ConvertToHtml(contentFile, ignored);
                        HtmlDocument document = new HtmlDocument();
                        document.LoadHtml(loObjectives ?? "");

                        learningObject.Objectives = document.DocumentNode.InnerText;
                    }

                    // regular files (non summary/intro) are always added as content
                    // summary and intro files are *only* added iif they have at least one paragraph tag
                    if ((!isIntro && !isSummary) || (isIntro && hasContent) || isSummary && hasContent)
                    {
                        AddContentPage(contentFile, learningObject);
                    }

                    List<ArtisanPage> questionPages = new List<ArtisanPage>();
                    foreach (var quiz in article.Elements("quiz"))
                    {
                        string questionFile = Path.Combine(Root, "Questions", quiz.Attribute("id").Value + ".xml");

                        var questionPage = GetQuestionPage(questionFile);

                        if (hasExamOnlyQuestions)
                        {
                            if (questionPage.Tag == "examonly")
                            {
                                // only add to the post-test
                                postTest.Pages.Add(questionPage);
                            }
                            else
                            {
                                // only add inline
                                // Jerod, 3/26 - adding to posttest too, apparently
                                postTest.Pages.Add(questionPage);
                                questionPages.Add(questionPage);
                            }
                        }
                        else
                        {
                            questionPages.Add(questionPage);
                            postTest.Pages.Add(questionPage);
                        }
                    }

                    // select a random question from the question pages and append after the page; apparently this is how DU courses work
                    // personally, i think it may make more sense to do all of them at the end of the LO...
                    if (questionPages.Count > 0)
                    {
                        int i = Random.Next(questionPages.Count);
                        var q = questionPages[i];
                        q.IsGradable = false;
                        learningObject.Pages.Add(q);
                    }
                }

                learningObjectIndex++;
            }

            Console.WriteLine("Conversion completed in " + sw.ElapsedMilliseconds + " milliseconds (" + sw.ElapsedMilliseconds / 1000 + " seconds)");

            // save the course            
            var result = Controller.SaveCourse(CustomerID, UserID, 0, ac);
            if (result.Success)
            {
                Console.WriteLine("Save round #1 completed in " + sw.ElapsedMilliseconds + " milliseconds (" + sw.ElapsedMilliseconds / 1000 + " seconds)");

                // save round one success; add additional processing to update the referenced pages
                // the pages (for now) all have the ids in the names, so we use that for matching.
                var savedCourse = result.Data;
                foreach (var sc in savedCourse.Sections)
                {
                    if (sc.Sections != null)
                    {
                        foreach (var lo in sc.Sections)
                        {
                            ProcessPages(savedCourse, lo.Pages, root, lo, PageAction.UpdateReferences);
                        }
                    }
                    else if (sc.Pages != null)
                    {
                        ProcessPages(savedCourse, sc.Pages, root, sc, PageAction.UpdateReferences);
                    }
                }

                // once the references are updated, do it again, but rename the pages to the proper names
                foreach (var sc in savedCourse.Sections)
                {
                    if (sc.Sections != null)
                    {
                        foreach (var lo in sc.Sections)
                        {
                            ProcessPages(savedCourse, lo.Pages, root, lo, PageAction.RenamePages);
                        }
                    }
                    else if (sc.Pages != null)
                    {
                        ProcessPages(savedCourse, sc.Pages, root, sc, PageAction.RenamePages);
                    }
                }

                Console.WriteLine("Updates completed in " + sw.ElapsedMilliseconds + " milliseconds (" + sw.ElapsedMilliseconds / 1000 + " seconds)");

                // save the updated course
                var result2 = Controller.SaveCourse(CustomerID, UserID, savedCourse.Id, savedCourse);
                if (result2.Success)
                {
                    Console.WriteLine("Save round #2 completed in " + sw.ElapsedMilliseconds + " milliseconds (" + sw.ElapsedMilliseconds / 1000 + " seconds)");
                    Console.WriteLine("Success!");

                }
            }
        }

        /// <summary>
        /// Returns true if there are any paragraph tags in the file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool HasContent(string file)
        {
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.Load(file);
            var root = document.DocumentNode;
            var nodes = root.SelectNodes("//p | //olist | //ulist");
            return nodes != null && nodes.Count() > 0 && !string.IsNullOrEmpty(string.Join(string.Empty, nodes.Select(n => n.InnerText).ToArray()));
        }

        enum PageAction
        {
            UpdateReferences = 1,
            RenamePages = 2
        }

        private void ProcessPages(ArtisanCourse savedCourse, List<ArtisanPage> pages, XElement root, ArtisanSection section, PageAction action)
        {
            var tutorials = root.Elements("tutorial");

            foreach (var page in pages)
            {
                if (page.PageType == (int)ArtisanPageType.Question)
                {
                    // look up this question, and find the parent article
                    var quiz = tutorials.Elements("article").Elements("quiz").Where(q => q.Attribute("id").Value == page.Name).FirstOrDefault();
                    var article = quiz.Parent;

                    string articleId = article.Attribute("id").Value;

                    // then look up the page that has that name, and set it as the reference for this page
                    var reference = savedCourse.FindPageByName(articleId);
                    if (reference != null)
                    {
                        // rename or update references
                        if (action == PageAction.RenamePages)
                        {
                            reference.Name = article.Attribute("name").Value;
                        }
                        else
                        {
                            page.ReferencePageId = reference.Id;
                        }
                    }

                    if (action == PageAction.RenamePages)
                    {
                        page.Name = "Knowledge Check";
                    }
                }
                else if (page.PageType == (int)ArtisanPageType.Content)
                {
                    if (action == PageAction.RenamePages)
                    {
                        var article = tutorials.Elements("article").Where(a => a.Attribute("id").Value == page.Name).FirstOrDefault();
                        if (article != null)
                        {
                            var contentPage = savedCourse.FindPageByName(article.Attribute("id").Value);
                            if (contentPage != null)
                            {
                                contentPage.Name = article.Attribute("name").Value;
                            }
                        }

                        //var reference = savedCourse.FindPageByName(page.Name);
                        //var article = tutorials.Elements("article").Where(q => q.Attribute("id").Value == page.Name).FirstOrDefault();
                        //reference.Name = article.Attribute("name").Value;
                    }
                }
            }
        }


        private void AddContentPage(string file, ArtisanSection learningObject)
        {
            List<ArtisanAsset> assets = new List<ArtisanAsset>();
            ArtisanPage ap = new ArtisanPage()
            {
                Name = Path.GetFileNameWithoutExtension(file),
                Html = string.Format(template, ConvertToHtml(file, assets)),
                PageType = (int)ArtisanPageType.Content,
                Id = PageCounter--
            };

            if (ap.Name.EndsWith("-S"))
            {
                ap.Name = "Summary";
            }
            else if (ap.Name.EndsWith("-I"))
            {
                ap.Name = "Introduction";
            }
            learningObject.Pages.Add(ap);

            foreach (var asset in assets)
            {
                if (asset != null)
                {
                    // in this case, there was a SWF "asset" embedded in the page that's really another page altogether;
                    // grab it and make a new page from it.
                    string extension = Path.GetExtension(asset.Filename).Trim('.');
                    Symphony.Core.Data.ArtisanAssetType at = Select.AllColumnsFrom<Symphony.Core.Data.ArtisanAssetType>()
                        .Where(Symphony.Core.Data.ArtisanAssetType.Columns.Extensions).ContainsString(extension)
                        .ExecuteSingle<Symphony.Core.Data.ArtisanAssetType>();

                    //if (asset.Filename.EndsWith("swf"))
                    //{
                    //    at = new Symphony.Core.Data.ArtisanAssetType(Symphony.Core.Data.ArtisanAssetType.Columns.Name, "Flash");
                    //}
                    //else if (asset.Filename.EndsWith("html") || asset.Filename.EndsWith("htm"))
                    //{
                    //    at = new Symphony.Core.Data.ArtisanAssetType(Symphony.Core.Data.ArtisanAssetType.Columns.Name, "HTML");
                    //}
                    //else if (asset.Filename.EndsWith("png") || asset.Filename.EndsWith("jpeg"))
                    //{
                    //    at = new Symphony.Core.Data.ArtisanAssetType(Symphony.Core.Data.ArtisanAssetType.Columns.Name, "Image");
                    //}
                    //else if (asset.Filename.EndsWith("pdf"))
                    //{
                    //    at = new Symphony.Core.Data.ArtisanAssetType(Symphony.Core.Data.ArtisanAssetType.Columns.Name, "Document");
                    //}

                    if (at == null)
                    {
                        throw new Exception("Unexpected asset type encountered: " + asset.Filename);
                    }

                    if (at.Name.ToLower() == "image")
                    {
                        // override for images, which have ext templates
                        // we'll just use a boring old img tag
                        at.Template = @"<img src=""{path}"" alt=""{description}"" title=""{name}"" />";
                    }

                    ap = new ArtisanPage()
                    {
                        Name = "Learning Exercise",
                        Html = string.Format(template,
                                at.Template
                                    .Replace("{path}", asset.Path)
                                    .Replace("{name}", asset.Name)
                                    .Replace("{description}", asset.Description)
                            ),
                        PageType = (int)ArtisanPageType.Content,
                        Id = PageCounter--
                    };
                    learningObject.Pages.Add(ap);
                }
            }
        }

        private ArtisanPage GetQuestionPage(string file)
        {
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.Load(file);
            var root = document.DocumentNode;

            var answers = new List<ArtisanAnswer>();
            var i = 0;
            foreach (var node in root.SelectNodes("//choice"))
            {
                answers.Add(new ArtisanAnswer()
                {
                    Text = node.InnerText,
                    IsCorrect = node.Attributes["answer"] != null && node.Attributes["answer"].Value == "yes",
                    Sort = i++
                });
            }



            // 2 questions = true/false, everything else is multiple choice
            ArtisanQuestionType questionType = ArtisanQuestionType.MultipleChoice;
            if (answers.Count == 2)
            {
                questionType = ArtisanQuestionType.TrueFalse;
            }

            ArtisanPage ap = new ArtisanPage()
            {
                Name = Path.GetFileNameWithoutExtension(file),
                Html = root.SelectNodes("//query")[0].InnerHtml,
                CorrectResponse = "Correct!",
                IncorrectResponse = "Incorrect. " + root.SelectNodes("//rationale")[0].InnerHtml,
                PageType = (int)ArtisanPageType.Question,

                QuestionType = questionType,
                Answers = answers,
                Id = PageCounter--
            };

            HtmlAttribute attribute = root.SelectNodes("//question")[0].Attributes["examonly"];
            if (attribute != null && attribute.Value == "yes")
            {
                ap.Tag = "examonly";
            }

            return ap;
        }

        private string ConvertToHtml(string file, List<ArtisanAsset> additionalAssetPages)
        {
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            Console.WriteLine("Reading " + file.Trim());

            string contents = File.ReadAllText(file.Trim());
            // workaround the fact that they're using an html attribute in a non-html form
            contents = contents.Replace("<link", "<xlink").Replace("</link>", "</xlink>");

            using (MemoryStream ms = new MemoryStream(System.Text.Encoding.ASCII.GetBytes(contents)))
            {
                document.Load(ms);
            }


            Dictionary<string, string> elementMaps = new Dictionary<string, string>();
            elementMaps.Add("//artname", "h3");
            elementMaps.Add("//artsub", "h4");
            elementMaps.Add("//ulist", "ul");
            elementMaps.Add("//olist", "ol");
            elementMaps.Add("//item", "li");
            elementMaps.Add("//callout", "div"); // more info
            elementMaps.Add("//xlink", "a");
            elementMaps.Add("//image", "img"); // more info
            elementMaps.Add("//highlight", "div"); // more info
            elementMaps.Add("//example", "div"); // more info
            elementMaps.Add("//address", "pre"); // more info
            elementMaps.Add("//glossary", "div"); // more info
            elementMaps.Add("//flash", "object"); // more info
            elementMaps.Add("//linebreak", "br");

            string header1 = string.Empty;
            string header2 = string.Empty;

            foreach (var pair in elementMaps)
            {
                var found = document.DocumentNode.SelectNodes(pair.Key);
                if (found != null)
                {
                    foreach (var el in found)
                    {
                        el.Name = pair.Value;

                        // TODO: abstract this out, maybe specific handlers for each type
                        if (pair.Value == "a")
                        {
                            if (el.Attributes["file"] != null)
                            {
                                el.Attributes["file"].Name = "href";
                            }
                            else if (el.Attributes["url"] != null)
                            {
                                el.Attributes["url"].Name = "href";
                            }
                            else
                            {
                                 // Anchor tag with no file and no URL? Skip it
                                continue;
                            }
                            el.SetAttributeValue("target", "_blank");
                        }
                        else if (pair.Value == "img")
                        {
                            // upload the assets
                            string assetFile = Path.Combine(Root, "Images", el.Attributes["file"].Value);
                            string name = Path.GetFileNameWithoutExtension(assetFile);
                            SingleResult<ArtisanAsset> result = Controller.UploadArtisanAsset(CustomerID, assetFile, name, name, File.ReadAllBytes(assetFile));
                            if (result.Success)
                            {
                                el.Attributes["file"].Value = result.Data.Path;
                            }

                            el.Attributes["file"].Name = "src";

                            // special case - if there is an alt-text of "learning exercise", this is a "test" (sorta)
                            // with a SWF file upload included
                            // upload the asset
                            HtmlAttribute alt = el.Attributes["alt"];
                            HtmlAttribute link = el.Attributes["link"];
                            if (alt != null && link != null)
                            {
                                name = Path.GetFileNameWithoutExtension(assetFile);

                                if (link.Value.StartsWith("http"))
                                {
                                    string js = "centeredPopup('" + link.Value + "','bigview','500','500','yes');return false;";
                                    el.SetAttributeValue("onclick", js);
                                    el.SetAttributeValue("style", "cursor:pointer;cursor:hand;");
                                }
                                else
                                {
                                    // grab the swf file, make sure we add it as an asset
                                    assetFile = Path.Combine(Root, "Images", link.Value);


                                    // upload the asset
                                    result = Controller.UploadArtisanAsset(CustomerID, assetFile, name, name, File.ReadAllBytes(assetFile));

                                    additionalAssetPages.Add(result.Data);

                                    // then change the action of the image so an onclick moves to the next page
                                    string js = "Artisan.App.moveNext()";
                                    el.SetAttributeValue("onclick", js);
                                    el.SetAttributeValue("style", "cursor:pointer;cursor:hand;");
                                }
                            }
                        }
                        else if (pair.Value == "h3")
                        {
                            header1 = "<h3 class=\"element1 element_text\" style=\"overflow: hidden;\">" + el.InnerHtml + "</h3>";
                            el.Remove();
                        }
                        else if (pair.Value == "h4")
                        {
                            el.InnerHtml = "<h4 style=\"text-align: left;\"><span style=\"font-size: large;\">" + el.InnerHtml + "</span></h4>";
                            //el.Remove();
                        }
                    }
                }
            }

            foreach (string rootNode in new string[] { "article", "question", "image" })
            {
                var nodes = document.DocumentNode.SelectNodes("//" + rootNode);
                if (nodes != null)
                {
                    return header1 +
                        "<div class=\"p element2 element_media\">" +
                            nodes[0].InnerHtml +
                        "</div>";
                }
            }

            return null;
        }
    }
}
