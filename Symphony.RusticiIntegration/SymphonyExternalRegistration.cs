﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RusticiSoftware.ScormContentPlayer.Logic;
using Symphony.RusticiIntegration.Core;

namespace Symphony.RusticiIntegration
{
    /// <summary>
    /// This represents what makes each registration unique. For Symphony, thats a username, course id, and training program id.
    /// Note that "public" courses get a training program of 0
    /// </summary>
    public class SymphonyExternalRegistration : ExternalRegistrationId
    {
        // start_template[client.external.registration.id.declaration]    -- do not delete template declaration            

        [GlobalObjectiveScope]
        public int UserId;

        /// <summary>
        /// The external package id that will be delivered
        /// </summary>
        public int CourseId;

        /// <summary>
        /// The training program to which this course belongs
        /// </summary>
        public int TrainingProgramId;

        // end_template
    }
}
