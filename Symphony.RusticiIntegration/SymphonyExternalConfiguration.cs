﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RusticiSoftware.ScormContentPlayer.Logic;
using RusticiSoftware.ScormContentPlayer.DataHelp;
using System.Collections;

namespace Symphony.RusticiIntegration
{
    /// <summary>
    /// At this time, Symphony has no need for additional configuration.
    /// </summary>
    public class SymphonyExternalConfiguration : ExternalConfiguration
    {
        // start_template[client.external.configuration.id.declaration]    -- do not delete template declaration       
        /// <summary>
        /// If an existing course ID is specified, this is an "update" to the existing course.
        /// </summary>
        public int ExistingCourseID = 0;

        /// <summary>
        /// If specified, indicates that the course is a duplicate of this course
        /// </summary>
        public int DuplicateFromID = 0;

        public string Keywords = "";

        public decimal Cost = 0;

        public decimal Credit = 0;

        public bool PublicIndicator = false;

        public bool IsSurvey = false;

        public int CustomerID = 0;
        // end_template
    }
}
