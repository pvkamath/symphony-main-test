﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RusticiSoftware.ScormContentPlayer.Logic;
using RusticiSoftware.ScormContentPlayer.DataHelp;
using System.Collections;

namespace Symphony.RusticiIntegration
{
    /// <summary>
    /// This is a representation of what makes a course in Symphony "unique". All that's necessary is a course ID.
    /// </summary>
    public class SymphonyExternalPackage : ExternalPackageId
    {
        // start_template[client.external.package.id.declaration]    -- do not delete template declaration            
        public int CourseId;
        // end_template
    }
}
