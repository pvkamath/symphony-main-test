﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RusticiSoftware.ScormContentPlayer.Logic;
using RusticiSoftware.ScormContentPlayer.DataHelp;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using log4net;
using System.Text.RegularExpressions;
using System.Reflection;

using Symphony.RusticiIntegration.Core;
using Symphony.Core;
using Symphony.Core.Controllers;
using Models = Symphony.Core.Models;



namespace Symphony.RusticiIntegration
{
    public class SymphonyIntegration : DefaultIntegration
    {
        private ILog Log = LogManager.GetLogger(typeof(SymphonyIntegration));

        private string CreateOnlineCourseSQL = "insert OnlineCourse " +
            " (name, [description], keywords, cost, credit, publicindicator, issurvey, customerid, duplicatefromid, createdon, modifiedon, [version]) " +
            " values " +
            " (@name, @description, @keywords, @cost, @credit, @publicindicator, @issurvey, @customerid, @duplicatefromid, GETUTCDATE(), GETUTCDATE(), @version);" +
            " select scope_identity();";

        private string UpdateOnlineCourseSQL = "update OnlineCourse set " +
            " modifiedon = GETUTCDATE() " +
            " where id = @courseid;";

        private string UpdateOnlineCourseRegistrationID = "update OnlineCourseRollup " +
            " set OriginalRegistrationID = @registrationId " +
            " where UserID = @userid and CourseID = @courseid and TrainingProgramID = @trainingprogramid";

        private string IncrementOnlineCourseVersionSQL = "update OnlineCourse set [version] = [version] + 1 where id = @courseid";

        private string InsertOnlineCourseRollupSQL = "insert OnlineCourseRollup" +
            " (UserID, CourseID, TrainingProgramID, Score, TotalSeconds, Success, Completion, ModifiedOn, ModifiedBy, AttemptCount, OriginalRegistrationID, NavigationPercentage, ArtisanAuditID, CoreVersion, CurrentRegistrationID, CreatedOn, LastLaunchedOn, TotalSecondsRounded, FirstCompletionDate) " +
            " values " +
            " (@userid, @courseid, @trainingprogramid, @score, @totalseconds, @success, @completion, @modifiedon, @modifiedby, 1, @registrationId, @navigationPercentage, @artisanAuditID, @coreVersion, @currentRegistrationID, @createdOn, @lastLaunchedOn, @totalSecondsRounded, @firstCompletionDate);" +
            " select scope_identity();";

        private string UpdateOnlineCourseRollupSQL = "update OnlineCourseRollup " +
            " set Score = @score, TotalSeconds = @totalseconds, Success = @success, Completion = @completion, {TestCount}" +
            " ModifiedOn = @modifiedon, ModifiedBy = @modifiedby, NavigationPercentage = @navigationPercentage, CoreVersion = @coreVersion, ArtisanAuditID = @artisanAuditID," +
            " CurrentRegistrationID = @currentRegistrationID, TotalSecondsPreviousRegistration = @totalsecondspreviousregistration, TotalSecondsRounded = @totalSecondsRounded, TotalSecondsScorm = @totalSecondsScorm, FirstCompletionDate = @firstCompletionDate" +
            " where UserID = @userid and CourseID = @courseid and TrainingProgramID = @trainingprogramid";

        private string UpdateOnlineCourseRollupWithoutSuccessAndCompletionSQL = "update OnlineCourseRollup " +
            " set Score = @score, TotalSeconds = @totalseconds, {TestCount}" +
            " ModifiedOn = @modifiedon, ModifiedBy = @modifiedby, NavigationPercentage = @navigationPercentage, CoreVersion = @coreVersion, ArtisanAuditID = @artisanAuditID," +
            " CurrentRegistrationID = @currentRegistrationID, TotalSecondsPreviousRegistration = @totalsecondspreviousregistration, TotalSecondsRounded = @totalSecondsRounded, TotalSecondsScorm = @totalSecondsScorm" +
            " where UserID = @userid and CourseID = @courseid and TrainingProgramID = @trainingprogramid";

        private string UpdateOnlineCourseRollupAttemptCount = "update OnlineCourseRollup " +
            " set AttemptCount = AttemptCount + 1 " +
            " where UserID = @userid and CourseID = @courseid and TrainingProgramID = @trainingprogramid";

        private string UpdateLastLaunchedOn = "update OnlineCourseRollup " +
            " set LastLaunchedOn = @datetime, " +
            " TotalSecondsLastAttempt = TotalSeconds" +
            " where UserID = @userid and CourseID = @courseid and TrainingProgramID = @trainingprogramid";

        private string LocateExistingRollupSQL = @"select 
                Score, 
                OriginalRegistrationID, 
                TotalSeconds, 
                NavigationPercentage, 
                Success, 
                Completion, 
                CurrentRegistrationID,
                TotalSecondsPreviousRegistration,
                LastLaunchedOn,
                TotalSecondsLastAttempt,
                TotalSecondsScorm,
                FirstCompletionDate
            from OnlineCourseRollup 
            where UserID = @userid and CourseID = @courseid and TrainingProgramID = @trainingprogramid";

        private string GetUserSQL = "select id, firstname, lastname from [user] " +
            " where id = @userid";

        private string UpdateHighScoreDateSQL = "update OnlineCourseRollup set highscoredate = GETUTCDATE()" +
            " where UserID = @userid and CourseID = @courseid and TrainingProgramID = @trainingprogramid";

        // Rollup for time tracking
        private string InsertOnlineCourseTimeTrackingSQL = "insert OnlineCourseTimeTracking" +
            " (UserID, Duration, Date, TrainingProgramID, CategoryID) " +
            " values " +
            " (@userid, @duration, @date, @trainingprogramid, @categoryid); ";

        private string UpdateOnlineCourseTimeTrackingSQL = "update OnlineCourseTimeTracking set Duration = @duration" +
            " where UserID = @userid and Date = @date and TrainingProgramID = @trainingprogramid and CategoryID = @categoryid; ";

        private string LocateExistingOnlineCourseTimeTrackingSQL = "select ID, Duration, UserID, Date from OnlineCourseTimeTracking " +
            " where UserID = @userid and Date = @date and TrainingProgramID = @trainingprogramid and CategoryID = @categoryid;";

        private string GetUserTimeZoneSQL = "select coalesce(u.TimeZone, c.TimeZone) as TimeZone from [User] u " +
            " join Customer c on c.ID = u.CustomerID " +
            " where u.ID = @userid";

        private string GetTrainingProgramCategory = "select CategoryID from TrainingProgram where TrainingProgram.ID = @trainingprogramid";

        private string GetCourseCategory = "select CategoryID from OnlineCourse where OnlineCourse.ID = @courseid";

        // Rollup for assignments
        private string GetLongAnswerPages = @"
            select 
                p.ArtisanPageID
            from AssignmentSummaryQuestions p 
            where p.ArtisanPageID in ({0});
";

        private string InsertOnlineCourseAssignment = @"
            insert into OnlineCourseAssignments (UserID, TrainingProgramID, CourseID, PageID, Attempt, Date, Response) 
            values (
                @userid,
                @trainingprogramid,
                @courseid,
                @pageid,
                @attempt,
                @date,
                @response
            )
";

        private string GetSectionIDFromPageID = @"
            select 
                s.SectionID
            from ArtisanSectionPages s
            where s.PageID = @pageId
";
        // Copies previous correct answers from the last attempt to the current attempt
        private string CopyPreviousCorrect = @"
            insert into OnlineCourseAssignments (UserID, TrainingProgramID, CourseID, PageID, Attempt, Date, ResponseStatus, IsCorrect, Response, InstructorFeedback)
            select
                a.UserID,
                a.TrainingProgramID,
                a.CourseID,
                a.PageID,
                @currentAttempt,
                a.Date,
                a.ResponseStatus,
                a.IsCorrect,
                a.Response,
                a.InstructorFeedback
            from OnlineCourseAssignments a
            join ArtisanPages p on p.ID = a.PageID
            join ArtisanSectionPages sp on sp.PageID = p.ID
            where a.UserID = @userid
            and a.CourseID = @courseid
            and a.TrainingProgramID = @trainingprogramid
            and a.Attempt = @previousAttempt
            and sp.SectionID = @sectionID
            and a.IsCorrect = 1
            and a.PageID not in (
                select
                    ia.PageID
                from OnlineCourseAssignments ia
                join ArtisanPages ip on ip.ID = ia.PageID
                join ArtisanSectionPages isp on isp.PageID = ip.ID
                where ia.UserID = @userid
                and ia.CourseID = @courseid
                and ia.TrainingProgramID = @trainingprogramid
                and ia.Attempt = @currentAttempt
                and isp.SectionID = @sectionID
            )
";



        private string GetMaxQuestionAttempts = @"
            select max(attempt) as MaxAttempt, PageID from OnlineCourseAssignments where
                UserID = @userid
            and TrainingProgramID = @trainingprogramid
            and CourseID = @courseid
            group by PageID
        ";

        private string GetArtisanAuditId = @"
            select 
                ID
            from
                ArtisanAudit
            where 
                OnlineCourseID = @courseId
            and
                PackageGuid = @packageGuid
";

        private string GetTrainingProgramSessionIdFromRollup = @"
            select
                SessionCourseID
            from
                OnlineCourseRollup r
            join
                TrainingProgram tp on r.TrainingProgramID = tp.ID
            where
                r.UserID = @userid 
                and r.CourseID = @courseid 
                and r.TrainingProgramID = @trainingprogramid
";

        private string ConnectionStringName = "Symphony";
        private string OnlineTrainingConnectionStringName = "OnlineTraining";


        public override bool DoesRegistrationExist(ExternalRegistrationId externalReg, ExternalConfiguration externalConfig)
        {
            SymphonyExternalRegistration myExternalRegistration = externalReg as SymphonyExternalRegistration;
            //RegistrationSummary summary = ScormEngineManager.GetRegistrationSummary(externalReg, externalConfig);

            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[ConnectionStringName];
            if (settings == null)
            {
                throw new Exception("The connection \"" + ConnectionStringName + "\" could not be found in the web.config.");
            }

            // this gets fired every time a course is launched, so we can use it to track attempts
            // note that the first time it runs, it won't find a record to update; for this reason,
            // our "insert" sql sets the attempt count to 1
            using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(UpdateLastLaunchedOn))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                    cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                    cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                    cmd.Parameters.AddWithValue("@datetime", DateTime.UtcNow);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                }
            }
            return base.DoesRegistrationExist(externalReg, externalConfig);
        }

        public override bool ShouldCreateNewRegistrationInstance(ExternalRegistrationId externalReg, ExternalConfiguration externalConfig)
        {
            SymphonyExternalRegistration myExternalRegistration = externalReg as SymphonyExternalRegistration;
            //RegistrationSummary summary = ScormEngineManager.GetRegistrationSummary(externalReg, externalConfig);

            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[ConnectionStringName];
            if (settings == null)
            {
                throw new Exception("The connection \"" + ConnectionStringName + "\" could not be found in the web.config.");
            }

            // this gets fired every time a course is launched, so we can use it to track attempts
            // note that the first time it runs, it won't find a record to update; for this reason,
            // our "insert" sql sets the attempt count to 1
            if (externalReg.ExternalPackageId != null)
            {
                // DS: This function is being called twice: ShouldCreateNewRegistrationInstance
                // Looks like this ShouldCreateNewRegistrationInstance is being called from launch.aspx and from deliver.aspx 
                // when it call by launch.aspx it pass proper ExternalPackageId
                // but when it calls from deliver.aspx, the ExternalPackageId is null.
                // needs to be verified!!!

                using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(UpdateOnlineCourseRollupAttemptCount))
                    {
                        cmd.Connection = conn;
                        cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                        cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                        cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Connection.Close();
                    }
                }
            }
            
            bool shouldCreate = false;

            try
            {
                shouldCreate = base.ShouldCreateNewRegistrationInstance(externalReg, externalConfig);

                if (shouldCreate && myExternalRegistration.TrainingProgramId > 0)
                {
                    try
                    {
                        // Check if the tp has a session assigned. This is the only time assignments are valid on a training program. 
                        // If there is a session (we assume assignments), and the student has launched this course, NEVER create a new registration
                        // (We can manually update the registration if there is a circumstance where the user needs the updated course)
                        using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
                        {
                            int sessionCourseId = 0;

                            // Selecting from the course rollup, joining the training program. 
                            // If the course rollup does not exist the student hasn't launched the course yet we will get an empty result
                            // Session courseid will remain 0 so should create will remain true.
                            //
                            // If the student has taken the course, we should get one result with the training program joined. 
                            // If the session id > 0 then do not create a new registration, since there might be assignments. 
                            using (SqlCommand cmd = new SqlCommand(GetTrainingProgramSessionIdFromRollup))
                            {
                                cmd.Connection = conn;
                                cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                                cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                                cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                                cmd.Connection.Open();
                                SqlDataReader reader = cmd.ExecuteReader();
                                if (reader.Read())
                                {
                                    if (!reader.IsDBNull(0))
                                    {
                                        sessionCourseId = reader.GetInt32(0);
                                    }
                                }
                                cmd.Connection.Close();
                            }

                            if (sessionCourseId > 0)
                            {
                                shouldCreate = false;
                            }
                        }
                        
                    }
                    catch (Exception e)
                    {
                        // Just in case anything goes wrong with the check for training program session. 
                        // In this case, we don't want the registration catch to fire. Just fall back 
                        // to whatever the shouldCreate value was calculated as. 
                    }
                }

            }
            catch (Exception e)
            {
                Log.Error("Registration issue - clearing existing registration ", e);
                
                shouldCreate = true;
                
                ConnectionStringSettings onlineTrainingSettings = ConfigurationManager.ConnectionStrings[OnlineTrainingConnectionStringName];
                if (onlineTrainingSettings == null)
                {
                    throw new Exception("The connection \"" + OnlineTrainingConnectionStringName + "\" could not be found in the web.config.");
                }
                using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(@"
                        update OnlineTraining.dbo.ScormRegistration
                        set course_id = -course_id
                        where user_id = @userId
                        and course_id = @courseid
                        and training_program_id = @trainingprogramid
                    "))
                    {
                        cmd.Connection = conn;
                        cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                        cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                        cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Connection.Close();
                    }
                }
            }

            return shouldCreate;
        }

        public override void CustomizeRegistration(Registration reg, ExternalRegistrationId externalReg, ExternalConfiguration externalConfig)
        {
            SymphonyExternalRegistration myExternalRegistration = externalReg as SymphonyExternalRegistration;
            // TODO: match up existing registration
            //GetScormRegistrationIdFromExternalRegistrationId(
            base.CustomizeRegistration(reg, externalReg, externalConfig);
        }

        public override ExternalPackageId GetExternalPackageIdObject()
        {
            return new SymphonyExternalPackage();
        }

        public override ExternalRegistrationId GetExternalRegistrationIdObject()
        {
            return new SymphonyExternalRegistration();
        }

        public override ExternalConfiguration GetExternalConfigurationObject()
        {
            return new SymphonyExternalConfiguration();
        }

        /*public override RusticiSoftware.ScormContentPlayer.Logic.YesNo DefaultDesiredFullScreen(RusticiSoftware.ScormContentPlayer.Logic.LearningStandard learningStandard, bool singleSco, RusticiSoftware.ScormContentPlayer.Logic.ExternalConfiguration externalConfig)
        {
            YesNo yesNo = base.DefaultDesiredFullScreen(learningStandard, singleSco, externalConfig);
            return new YesNo(YesNoValue.YES);
        }*/

        /// <summary>
        /// Responsible for tracking the final results of a course. We don't worry about the details (all that stuff is
        /// stored in Rustici), we just want to know the score and status of a given course for a user, training program,
        /// and course.
        /// </summary>
        /// <param name="externalReg">The registration object</param>
        /// <param name="externalConfig">The configuration object</param>
        public override void RollupRegistration(ExternalRegistrationId externalReg, ExternalConfiguration externalConfig)
        {
            try
            {
                Log.Info("Rollup started...");

                SymphonyExternalRegistration myExternalRegistration = externalReg as SymphonyExternalRegistration;
                RegistrationSummary summary = ScormEngineManager.GetRegistrationSummary(externalReg, externalConfig);
                RegistrationInfo info = ScormEngineManager.GetRegistrationInfo(externalReg, externalConfig); // For assignmment data rollup
                Registration reg = ScormEngineManager.GetRegistration(externalReg, externalConfig);

                Log.Info("Registration: " + externalReg.ScormRegistrationId);
                Log.Info("Summary Score Known: " + summary.ScoreIsKnown);
                Log.Info("Summary Score: " + summary.Score);

                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[ConnectionStringName];
                if (settings == null)
                {
                    throw new Exception("The connection \"" + ConnectionStringName + "\" could not be found in the web.config.");
                }

                object result = null;
                string originalRegistrationID = string.Empty;
                string currentRegistrationID = string.Empty;
                int totalSeconds = 0;
                long totalSecondsPreviousRegistration = 0;
                long totalSecondsLastAttempt = 0;
                long totalSecondsScorm = 0;
                int currentNavigationPercentage = 0;
                bool? currentSuccess = null;
                bool? currentCompletion = null;
                DateTime? firstCompletionDate = null;
                bool trainingProgramRollupRequired = false;
                long currentSeconds = 0;
                DateTime lastLaunchedOn = DateTime.UtcNow;
                bool recordFound = false;
               
                string packageGuid = ExtractGuid(reg.Package.WebPath);
                int artisanAuditId = 0;

                Assembly assembly = Assembly.GetExecutingAssembly();
                AssemblyName name = new AssemblyName(assembly.FullName);
                Version version = name.Version;

                string coreVersion = version.ToString();

                using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(GetArtisanAuditId))
                    {
                        cmd.Connection = conn;
                        cmd.Parameters.AddWithValue("@courseId", myExternalRegistration.CourseId);
                        cmd.Parameters.AddWithValue("@packageGuid", packageGuid);
                        cmd.Connection.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            artisanAuditId = reader.GetInt32(0);
                        }
                        cmd.Connection.Close();
                    }
                
                    // if a record exists for this [userid/courseid/trainingprogramid] combination, then we need to update
                    // otherwise, we'll do an insert
                    using (SqlCommand cmd = new SqlCommand(LocateExistingRollupSQL))
                    {
                        cmd.Connection = conn;
                        cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                        cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                        cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                        cmd.Connection.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            recordFound = true;
                            result = reader.GetValue(0); // score
                            originalRegistrationID = reader.GetString(1); // registration id
                            totalSeconds = int.Parse(reader.GetValue(2).ToString()); // total seconds from all registrations
                            
                            currentNavigationPercentage = reader.GetInt32(3); // position in course by percentage
                            if (!reader.IsDBNull(4)) {
                                currentSuccess = reader.GetBoolean(4); // Current success state
                            }

                            if (!reader.IsDBNull(5))
                            {
                                currentCompletion = reader.GetBoolean(5); // Current completion state
                            }

                            currentRegistrationID = reader.GetString(6); // current registration id
                            totalSecondsPreviousRegistration = int.Parse(reader.GetValue(7).ToString()); // Total seconds from previous registrations 
                            lastLaunchedOn = reader.GetDateTime(8);
                            totalSecondsLastAttempt = int.Parse(reader.GetValue(9).ToString()); // Total seconds reported by scorm for all registrations

                            if (!reader.IsDBNull(11)) {
                                firstCompletionDate = reader.GetDateTime(11);
                            }
                        }
                        cmd.Connection.Close();
                    }

                    string commandSQL = null;
                    double previousScore = 0;
                    bool isNewScore = false; // If the previous score was null. This would happen after a course reset
                    if (!recordFound)
                    {
                        // no record means just insert the value
                        commandSQL = InsertOnlineCourseRollupSQL;
                    }
                    else
                    {
                        if (result != null && result != DBNull.Value)
                        {
                            double.TryParse(result.ToString(), out previousScore);
                        } else if (result == DBNull.Value) {
                            isNewScore = true;
                        }
                        if (summary.Score > previousScore || isNewScore)
                        {
                            // there was a record found, and the incoming score is greating than previous
                            // attempts, so update everything
                            commandSQL = UpdateOnlineCourseRollupSQL;
                        }
                        else
                        {
                            // there was a record found, but the score is worse than any previous attempts
                            // so, we don't record the completion or success
                            commandSQL = UpdateOnlineCourseRollupWithoutSuccessAndCompletionSQL;
                        }
                    }

                    int scoreChanges = 0;
                    using (SqlCommand cmd = new SqlCommand("GetScoreChanges"))
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@registrationId", myExternalRegistration.ScormRegistrationId);
                        cmd.Connection.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            scoreChanges = reader.GetInt32(0);
                        }
                        cmd.Connection.Close();
                    }

                    commandSQL = commandSQL.Replace("{TestCount}", string.Format("TestAttemptCount = {0}, ", scoreChanges));


                    // note that both queries take the exact same parameters
                    using (SqlCommand cmd = new SqlCommand(commandSQL))
                    {
                        cmd.Connection = conn;

                        // only record the registration id the first time through
                        // Also store the created on and last launched
                        if (!recordFound)
                        {
                            cmd.Parameters.AddWithValue("@registrationId", externalReg.ScormRegistrationId);
                            
                            cmd.Parameters.AddWithValue("@createdOn", DateTime.UtcNow);
                            cmd.Parameters.AddWithValue("@lastLaunchedOn", DateTime.UtcNow);
                        }
                        else
                        {
                            // for existing records that don't have a registration id set
                            if (string.IsNullOrEmpty(originalRegistrationID))
                            {
                                using (SqlCommand updateRegCommand = new SqlCommand(UpdateOnlineCourseRegistrationID))
                                {
                                    // unique identifier
                                    updateRegCommand.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                                    updateRegCommand.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                                    updateRegCommand.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);

                                    // registration id
                                    updateRegCommand.Parameters.AddWithValue("@registrationId", myExternalRegistration.ScormRegistrationId);

                                    updateRegCommand.Connection = conn;
                                    updateRegCommand.Connection.Open();
                                    updateRegCommand.ExecuteNonQuery();
                                    updateRegCommand.Connection.Close();
                                }
                            }
                        }

                        // unique identifier
                        cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                        cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                        cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);

                        // if the previous score is known, use it, even if the current attempt score isn't known
                        if (summary.ScoreIsKnown || previousScore > 0)
                        {
                            if (summary.Score > previousScore)
                            {
                                // only update the score if it's higher than the previous score
                                cmd.Parameters.AddWithValue("@score", summary.Score);
                            }
                            else
                            {
                                // otherwise, leave it alone
                                cmd.Parameters.AddWithValue("@score", previousScore);
                            }
                        }
                        else
                        {
                            // no score
                            cmd.Parameters.AddWithValue("@score", DBNull.Value);
                        }

                        // if there is no record, we're inserting so we always want to know success and completion
                        // if there IS a record found, we only want to update these values if the score is an improvement
                        //   (basically, they can't re-take and fail a course after passing it)

                        if (!recordFound || summary.Score > previousScore || isNewScore)
                        {
                            if (summary.SuccessStatus == Success.UNKNOWN)
                            {
                                cmd.Parameters.AddWithValue("@success", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@success", summary.SuccessStatus == Success.FAILED ? false : true);
                                if (summary.SuccessStatus == Success.PASSED && (!currentSuccess.HasValue || currentSuccess == false))
                                {
                                    trainingProgramRollupRequired = true;
                                }
                            }
                            // passed or failed = completion
                            if (summary.SuccessStatus == Success.PASSED || summary.SuccessStatus == Success.FAILED)
                            {
                                cmd.Parameters.AddWithValue("@completion", true);
                                if (!currentCompletion.HasValue || currentCompletion == false)
                                {
                                    cmd.Parameters.AddWithValue("@firstCompletionDate", DateTime.UtcNow);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@firstCompletionDate", currentCompletion);
                                }
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@completion", DBNull.Value);
                                cmd.Parameters.AddWithValue("@firstCompletionDate", DBNull.Value);
                            }
                        }

                        #region Primary Symphony Course Time Tracking
                        // For primary time tracking we are ignoring the scorm time tracking (summary.TotalTimeTracked)
                        // this value only updates on course exit which may not work with some browsers.
                        //
                        // Our time tracking method:
                        // On course launch, set the last launched date, copy TotalSeconds to TotalSecondsLastAttempt
                        // On each update calculate the seconds since last launch and add to TotalSecondsLastAttmempt and save in TotalSeconds

                        // Get the seconds since last launched based on the last launched date
                        int secondsSinceLastLaunch = (int)DateTime.UtcNow.Subtract(lastLaunchedOn).TotalSeconds;
                        // User our own time tracking to calculate time instead of scorm so we can ensure an update on every commit
                        long newTotalSeconds = totalSecondsLastAttempt + secondsSinceLastLaunch;
                        
                        currentSeconds = newTotalSeconds - totalSeconds;


                        cmd.Parameters.AddWithValue("@totalSeconds", newTotalSeconds);
                        long totalSecondsRounded = (long)Math.Ceiling(newTotalSeconds / (double)60) * 60;
                        cmd.Parameters.AddWithValue("@totalSecondsRounded", totalSecondsRounded);
                        #endregion

                        #region Scorm Time Tracking
                        // The following is specific for tracking the total seconds in the course as calculated by scorm which
                        // we are not actually using for anything.
                        //
                        // Calculate the scorm total seconds as well just so we have the actual scorm counts available as well. 
                        // In theory, these should be almost identical in all cases (Except for when the time tracking does not
                        // rollup properly in browsers that cancel xhr requests on exit.
                        //
                        // We don't do anything with this value. We only use totalSeconds for any time tracking or rollup calculations
                        // This is only here for reference.
                        long newTotalSecondsScorm = totalSecondsPreviousRegistration + summary.TotalSecondsTracked;
                        cmd.Parameters.AddWithValue("@totalSecondsScorm", newTotalSecondsScorm);


                        // Current registration has been backfilled for all OnlineCourseRollup
                        // This was done by taking the max registration id for the user/courseid/trainingprogramid
                        // So we shouldn't have to deal with any special cases where we need to fall back to the
                        // old time tracking handling. 
                        // It is possible for the current registration id to be 0 this means the registration
                        // is actually missing from the scorm data entirely. (Course reset, and never re-attempted, or old/test
                        // online course rollup data in the db that does not match a registration in the scorm db. 
                        // If we hit any of these, just set the current registration id and carry on as normal. 
                        cmd.Parameters.AddWithValue("@currentRegistrationID", externalReg.ScormRegistrationId);
                        
                        // If the current registration has changed, then we need to copy the total seconds to
                        // total seconds previous registration. This should work in all cases now since the 
                        // old data has been backfilled with the proper time caclulation so we wont roll
                        // forward any incorrect times.
                        if (currentRegistrationID != externalReg.ScormRegistrationId)
                        {
                            cmd.Parameters.AddWithValue("@totalsecondspreviousregistration", totalSeconds);
                        }
                        else // Don't change anything if there is no registration change. 
                        {
                            cmd.Parameters.AddWithValue("@totalsecondspreviousregistration", totalSecondsPreviousRegistration);
                        }
                        #endregion

                        cmd.Parameters.AddWithValue("@modifiedon", DateTime.UtcNow);
                        cmd.Parameters.AddWithValue("@modifiedby", myExternalRegistration.UserId);

                        // The bookmark is attached to the Activiities in which there could be multiple
                        // activities on the scorm side. There is always two activities listed for our 
                        // courses. The first activity has null RunTimeData. Looping over all of them
                        // anyway to ensure we grab everything and used the greatest percentage
                        Dictionary<string, int> bookmarkedPercentages = new Dictionary<string, int>();
                        foreach (Activity activity in reg.Activities) {
                            if (activity.RunTime != null && activity.RunTime.Location != null)
                            {
                                string location = activity.RunTime.Location.InnerString;
                                if (!string.IsNullOrEmpty(location))
                                {
                                    if (!bookmarkedPercentages.ContainsKey(location))
                                    {
                                        string[] locationParts = location.Split('|');
                                        if (locationParts.Length >= 5)
                                        {
                                            string percentString = locationParts[4];
                                            int percent = 0;
                                            int.TryParse(percentString, out percent);

                                            bookmarkedPercentages.Add(location, percent);
                                        }
                                    }
                                }
                            }
                        }
                        int navigationPercentage = bookmarkedPercentages.Count > 0 ? bookmarkedPercentages.Max(b => b.Value) : 0;
                        navigationPercentage = navigationPercentage > currentNavigationPercentage ? navigationPercentage : currentNavigationPercentage;


                        if (summary.SuccessStatus == Success.PASSED)
                        {
                            navigationPercentage = 100;
                        }
                        else if (summary.SuccessStatus == Success.FAILED)
                        {
                            navigationPercentage = 0;
                        }


                        cmd.Parameters.AddWithValue("@navigationPercentage", navigationPercentage);

                        cmd.Parameters.AddWithValue("@coreVersion", coreVersion);
                        cmd.Parameters.AddWithValue("@artisanAuditId", artisanAuditId);

                        cmd.Connection.Open();
                        cmd.ExecuteScalar();
                        cmd.Connection.Close();
                    }

                    // update the high score date if necessary
                    if (summary.Score > previousScore)
                    {
                        using (SqlCommand cmd = new SqlCommand(UpdateHighScoreDateSQL))
                        {
                            cmd.Connection = conn;
                            cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                            cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                            cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                            cmd.Connection.Open();
                            cmd.ExecuteNonQuery();
                            cmd.Connection.Close();
                        }
                    }

                    // Store todays coursework done
                    long duration = 0;
                    string timezone = "utc";
                    DateTime now = DateTime.Now;
                    string nowString;
                    int categoryId = 0;
                    recordFound = false;


                    using (SqlCommand cmd = new SqlCommand(myExternalRegistration.TrainingProgramId > 0 ? GetTrainingProgramCategory : GetCourseCategory))
                    {
                        cmd.Connection = conn;
                        if (myExternalRegistration.TrainingProgramId > 0)
                        {
                            cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                        }
                        cmd.Connection.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            categoryId = reader.GetInt32(0);
                        }
                        cmd.Connection.Close();

                    }

                    using (SqlCommand cmd = new SqlCommand(GetUserTimeZoneSQL))
                    {
                        cmd.Connection = conn;
                        cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                        cmd.Connection.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            timezone = reader.GetString(0);
                        }
                        cmd.Connection.Close();
                    }

                    try {
                        now = TimeZoneInfo.ConvertTime(now, TimeZoneInfo.Local, TimeZoneInfo.FindSystemTimeZoneById(timezone));
                    } catch (Exception e) {
                        if (e.GetType() != typeof(TimeZoneNotFoundException) &&
                            e.GetType() != typeof(InvalidTimeZoneException))
                        {
                            throw e;
                        }
                        else
                        {
                            Log.Info("Could not find timezone '" + timezone + "' using system time instead.");
                        }
                    }
                    
                    nowString = now.ToString("yyyy-MM-dd");
                    
                    using (SqlCommand cmd = new SqlCommand(LocateExistingOnlineCourseTimeTrackingSQL))
                    {
                        cmd.Connection = conn;
                        cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                        cmd.Parameters.AddWithValue("@date", nowString);
                        cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                        cmd.Parameters.AddWithValue("@categoryid", categoryId);
                        cmd.Connection.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            recordFound = true;
                            duration = reader.GetInt64(1);
                        }
                        cmd.Connection.Close();
                    }

                    if (recordFound)
                    {
                        commandSQL = UpdateOnlineCourseTimeTrackingSQL;
                    }
                    else
                    {
                        commandSQL = InsertOnlineCourseTimeTrackingSQL;
                    }

                    Log.Info("The training program id is : " + myExternalRegistration.TrainingProgramId);
                    
                    using (SqlCommand cmd = new SqlCommand(commandSQL)) {
                        duration = duration + currentSeconds;

                        cmd.Connection = conn;
                        cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                        cmd.Parameters.AddWithValue("@duration", duration);
                        cmd.Parameters.AddWithValue("@date", nowString);
                        cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                        cmd.Parameters.AddWithValue("@categoryid", categoryId);
                        
                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Connection.Close();
                    }

                    Dictionary<string, List<InteractionInfo>> interactions = new Dictionary<string,List<InteractionInfo>>();
                    Dictionary<string, int> maxAttempts = new Dictionary<string,int>();
                    Dictionary<string, bool> longAnswerPages = new Dictionary<string, bool>();
                    Dictionary<int, int> sectionAttempts = new Dictionary<int, int>();

                    // Rollup assignment data
                    foreach (ActivityInfo activity in info.Activities)
                    {
                        foreach (InteractionInfo interaction in activity.Interactions)
                        {
                            
                            if (!interactions.ContainsKey(interaction.Identifier))
                            {
                                interactions.Add(interaction.Identifier, new List<InteractionInfo>());
                            }
                            interactions[interaction.Identifier].Add(interaction);
                        }
                    }

                    if (interactions.Keys.Count() > 0)
                    {
                        using (SqlCommand cmd = new SqlCommand(string.Format(GetLongAnswerPages, string.Join(",", interactions.Keys.ToArray<string>()))))
                        {
                            cmd.Connection = conn;

                            cmd.Connection.Open();
                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                var pageId = reader.GetInt32(0);
                                longAnswerPages[pageId.ToString()] = true;
                            }
                            cmd.Connection.Close();
                        }
                    }

                    using (SqlCommand cmd = new SqlCommand(GetMaxQuestionAttempts)) {
                        cmd.Connection = conn;

                        cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                        cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                        cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);

                        cmd.Connection.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var attemptIndex = reader.GetInt32(0);
                            var pageId = reader.GetInt32(1);
                            maxAttempts[pageId.ToString()] = attemptIndex;

                        }
                        cmd.Connection.Close();
                    }

                    foreach (KeyValuePair<string, List<InteractionInfo>> pageInteractionAttempts in interactions)
                    {
                        int attempt = 0;
                        
                        foreach (InteractionInfo interactionAttempt in pageInteractionAttempts.Value)
                        {
                       
                            if ((!maxAttempts.ContainsKey(pageInteractionAttempts.Key) ||
                                 maxAttempts[pageInteractionAttempts.Key] < attempt)
                                && longAnswerPages.ContainsKey(pageInteractionAttempts.Key))
                            {
                                using (SqlCommand cmd = new SqlCommand(InsertOnlineCourseAssignment))
                                {
                                    cmd.Connection = conn;
                                    cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                                    cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                                    cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                                    cmd.Parameters.AddWithValue("@pageid", int.Parse(pageInteractionAttempts.Key));
                                    cmd.Parameters.AddWithValue("@attempt", attempt);
                                    cmd.Parameters.AddWithValue("@date", DateTime.Now);
                                    
                                    if (string.IsNullOrEmpty(interactionAttempt.LearnerResponse)) {
                                         cmd.Parameters.AddWithValue("@response", "");
                                    } else {
                                        cmd.Parameters.AddWithValue("@response", interactionAttempt.LearnerResponse);
                                    }

                                    cmd.Connection.Open();
                                    cmd.ExecuteNonQuery();
                                    cmd.Connection.Close();
                                }
                                
                                using (SqlCommand cmd = new SqlCommand(GetSectionIDFromPageID))
                                {
                                    cmd.Connection = conn;
                                    cmd.Parameters.AddWithValue("@pageid", int.Parse(pageInteractionAttempts.Key));

                                    cmd.Connection.Open();
                                    SqlDataReader reader = cmd.ExecuteReader();
                                    while (reader.Read())
                                    {
                                        
                                        int sectionId = reader.GetInt32(0);
                                        if (!sectionAttempts.ContainsKey(sectionId))
                                        {
                                            sectionAttempts.Add(sectionId, attempt);
                                        }
                                        
                                        sectionAttempts[sectionId] = attempt;
                                    }
                                    cmd.Connection.Close();
                                }
                            }
                            attempt++;
                        }
                        
                        // Each section that generated a new record
                        // in the OnlineCourseAssignments table
                        // will have a pair listed in the sectionAttempt
                        // dictionary, with the current attempt. This
                        // will copy all the correct answers from the 
                        // previous attempt.
                        foreach (KeyValuePair<int, int> sectionAttempt in sectionAttempts)
                        {
                            int sectionId = sectionAttempt.Key;
                            int currentAttempt = sectionAttempt.Value;
                            
                            if (currentAttempt > 0)
                            {
                                int previousAttempt = currentAttempt - 1;
                                
                                using (SqlCommand cmd = new SqlCommand(CopyPreviousCorrect))
                                {
                                    cmd.Connection = conn;

                                    cmd.Parameters.AddWithValue("@sectionId", sectionId);
                                    cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                                    cmd.Parameters.AddWithValue("@trainingprogramid", myExternalRegistration.TrainingProgramId);
                                    cmd.Parameters.AddWithValue("@courseid", myExternalRegistration.CourseId);
                                    cmd.Parameters.AddWithValue("@previousAttempt", previousAttempt);
                                    cmd.Parameters.AddWithValue("@currentAttempt", currentAttempt);

                                    cmd.Connection.Open();
                                    cmd.ExecuteNonQuery();
                                    cmd.Connection.Close();
                                }
                            }

                            AssignmentController assignmentController = new AssignmentController();
                            assignmentController.SummarizeAssignmentAttempt(myExternalRegistration.TrainingProgramId, myExternalRegistration.CourseId, sectionId, myExternalRegistration.UserId, currentAttempt); 
                        }

                    }
                }

                RollupController tpr = new RollupController();

                if (!trainingProgramRollupRequired && currentSeconds > 0) // Current seconds should be > 0 on every commit now that we calculate the time based on lastLaunchedOn during playback. 
                {
                    tpr.EnqueueUpdateTrainingProgramTotalTime(myExternalRegistration.TrainingProgramId, myExternalRegistration.UserId);
                } 
                else if (trainingProgramRollupRequired) // Full tp rollup is done when the course changes from incomplete to complete. This will re-rollup the time tracked as well
                {
                    tpr.EnqueueTrainingProgramRollup(myExternalRegistration.TrainingProgramId, myExternalRegistration.UserId, myExternalRegistration.CourseId, CourseType.Online);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        /// <summary>
        /// This integration method gets learner information from Symphony
        /// </summary>
        /// <param name="externalReg">Registration for which we need Learner information</param>
        /// <param name="externalConfig">External configuration information.</param>
        public override Learner GetLearnerInformation(ExternalRegistrationId externalReg, ExternalConfiguration externalConfig)
        {
            SymphonyExternalRegistration myExternalRegistration = externalReg as SymphonyExternalRegistration;

            if (myExternalRegistration == null)
            {
                throw new ScormContentPlayerApplicationException("The external registration id argument was not a valid SymphonyExternalRegistration object.");
            }

            Learner learner = new Learner();

            // select the user
            using (SqlCommand cmd = new SqlCommand(GetUserSQL))
            {
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[ConnectionStringName];
                if (settings == null)
                {
                    throw new Exception("The connection \"" + ConnectionStringName + "\" could not be found in the web.config.");
                }
                using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@userid", myExternalRegistration.UserId);
                    cmd.Connection.Open();

                    IDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        learner.Id = reader.GetInt32(0).ToString();
                        learner.FirstName = reader.GetString(1);
                        learner.LastName = reader.GetString(2);
                    }
                    else
                    {
                        throw new Exception("The specified user could not be found.");
                    }

                    cmd.Connection.Close();
                }
            }

            return learner;
        }

        /// <summary>
        /// This integration method adds this Scormcourse to Symphony
        /// </summary>
        /// <param name="manifest">Manifest of course being imported</param>
        /// <param name="webPathToCourse">Path to Location of course, unused</param>
        /// <param name="externalConfig">External configuration information.</param>
        public override ExternalPackageId AddExternalPackage(Manifest manifest, String webPathToCourse, ExternalConfiguration externalConfig)
        {
            SymphonyExternalConfiguration myExternalConfig = externalConfig as SymphonyExternalConfiguration;

            if (myExternalConfig == null)
            {
                throw new ScormContentPlayerApplicationException("The external configuration argument was not a valid SymphonyExternalConfiguration object.");
            }

            // create a package, setting the identifier for it to the new course
            // now, potentially this could be an "update". in that case, if we just set
            // the CourseId property to be an existing course, rustici will automatically
            // handle the versioning for us.
            SymphonyExternalPackage package = new SymphonyExternalPackage();
            string title = "", description = "";
            if (!string.IsNullOrEmpty(manifest.MetaData.Title.Text))
            {
                title = manifest.MetaData.Title.Text;
            }
            else if (manifest.ScosWithContext.Count() > 0 && !string.IsNullOrEmpty(manifest.ScosWithContext[0].Title))
            {
                title = manifest.ScosWithContext[0].Title;
            }
            else if (manifest.Scos.Count() > 0 && !string.IsNullOrEmpty(manifest.Scos[0].Title))
            {
                title = manifest.Scos[0].Title;
            }
            else
            {
                title = manifest.DefaultOrganization.Title;
            }
            if (!string.IsNullOrEmpty(manifest.MetaData.Description.Text))
            {
                description = manifest.MetaData.Description.Text;
            }
            else
            {
                description = title;
            }

            if (myExternalConfig.ExistingCourseID == 0)
            {
                // build the insert statement
                using (SqlCommand cmd = new SqlCommand(CreateOnlineCourseSQL))
                {
                    ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[ConnectionStringName];
                    if (settings == null)
                    {
                        throw new Exception("The connection \"" + ConnectionStringName + "\" could not be found in the web.config.");
                    }
                    using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
                    {
                        cmd.Connection = conn;

                        cmd.Parameters.AddWithValue("@name", title);
                        cmd.Parameters.AddWithValue("@description", description);
                        cmd.Parameters.AddWithValue("@keywords", myExternalConfig.Keywords);
                        cmd.Parameters.AddWithValue("@cost", myExternalConfig.Cost);
                        cmd.Parameters.AddWithValue("@credit", myExternalConfig.Credit);
                        cmd.Parameters.AddWithValue("@publicindicator", myExternalConfig.PublicIndicator);
                        cmd.Parameters.AddWithValue("@issurvey", myExternalConfig.IsSurvey);
                        cmd.Parameters.AddWithValue("@customerid", myExternalConfig.CustomerID);
                        cmd.Parameters.AddWithValue("@duplicatefromid", myExternalConfig.DuplicateFromID);
                        cmd.Parameters.AddWithValue("@version", 1);
                        cmd.Connection.Open();
                        object o = cmd.ExecuteScalar();
                        // didn't like the direct casting for some reason...
                        package.CourseId = Int32.Parse(o.ToString());

                        cmd.Connection.Close();
                    }
                }
            }
            else
            {
                // updating an existing course. in this scenario, we want to update the course name and description
                using (SqlCommand cmd = new SqlCommand(UpdateOnlineCourseSQL))
                {
                    ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[ConnectionStringName];
                    if (settings == null)
                    {
                        throw new Exception("The connection \"" + ConnectionStringName + "\" could not be found in the web.config.");
                    }
                    using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
                    {
                        // update the name and description
                        cmd.Connection = conn;
                        //cmd.Parameters.AddWithValue("@name", title);
                       // cmd.Parameters.AddWithValue("@description", description);
                        cmd.Parameters.AddWithValue("@courseid", myExternalConfig.ExistingCourseID);
                        cmd.Connection.Open();
                        int i = cmd.ExecuteNonQuery();
                        if (i == 0)
                        {
                            throw new Exception("Course with ID " + myExternalConfig.ExistingCourseID + " could not be found.");
                        }

                        // update the version
                        cmd.CommandText = IncrementOnlineCourseVersionSQL;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@courseid", myExternalConfig.ExistingCourseID);
                        cmd.ExecuteNonQuery();

                        cmd.Connection.Close();
                    }
                }
                package.CourseId = myExternalConfig.ExistingCourseID;
            }
            return package;
        }

        /// <summary>
        /// This implementation will add configuration onto query string
        /// </summary>
        /// <param name="externalConfig">External configuration information.</param>
        public override String GetRedirectOnExitUrl(ExternalConfiguration externalConfig)
        {
            String redirectUrl = base.GetRedirectOnExitUrl(externalConfig);
            return redirectUrl +
                     (redirectUrl.Contains("?") ? "&" : "?") +
                     "configuration=" + externalConfig.PersistToString();
        }

        /// <summary>
        /// See <see cref="IntegrationInterface.GetRedirectOnExitUrl(ExternalRegistrationId, ExternalConfiguration)"/> for a full description.
        /// </summary>
        /// <param name="externalReg">External identifier for the registration that is currently being delivered.</param>
        /// <param name="externalConfig">External configuration information.</param>
        public override string GetRedirectOnExitUrl(ExternalRegistrationId externalReg, ExternalConfiguration externalConfig)
        {
            string url = GetRedirectOnExitUrl(externalConfig);
            // Now tag on a #<pkgid> to scroll down to the package in the noddy lms display
            ExternalPackageId pkgId = GetExternalPackageIdFromExternalRegId(externalReg, externalConfig);
            url += "#" + GetScormPackageIdFromExternalPackageId(pkgId, externalConfig);

            return url;
        }

        /// <summary>
        /// Duplicated from Symphony.Core.Utilities - we should improve this so we can share this.
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static string ExtractGuid(string g)
        {
            Regex r = new Regex("[a-z0-9]{8}[-][a-z0-9]{4}[-][a-z0-9]{4}[-][a-z0-9]{4}[-][a-z0-9]{12}");
            Match m = r.Match(g);

            if (m != null && m.Success && !string.IsNullOrEmpty(m.Value))
            {
                return m.Value;
            }

            return null;
        }
    }
}
