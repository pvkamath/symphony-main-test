﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using RusticiSoftware.ScormContentPlayer.Logic;
using System.Collections;
using ICSharpCode.SharpZipLib.Zip;

namespace Symphony.RusticiIntegration
{
    /// <summary>
    /// This is the entry point for external calls to any of the Rustici goodies. Everything Rustici-related should go through
    /// here. The only exception is the stuff Rustici calls itself, which can be found in the BankersEdgeIntegration class.
    /// </summary>
    public class SymphonyIntegrationController
    {
        /// <summary>
        /// Imports a course into Rustici
        /// </summary>
        /// <param name="zipFilePath">The path to the zip file to be imported</param>
        /// <param name="courseToUpdate">The ID of the course being updated</param>
        /// <param name="username">The username of the person applying the import</param>
        /// <returns>An import result object</returns>
        public static ImportResult ImportCourse(string zipFilePath, int courseToUpdate, string username)
        {
            return ImportSingleCourseFromZip(zipFilePath, new SymphonyExternalConfiguration() { ExistingCourseID = courseToUpdate }, username, string.Empty);
        }

        /// <summary>
        /// Imports a course into Rustici
        /// </summary>
        /// <param name="zipFilePath">The path to the zip file to be imported</param>
        /// <param name="cfg">A config object. This should contain either a full config object with ExistingCourseID set to 0, or a blank config object with ExistingCourseID set to the course to be updated</param>
        /// <param name="username">The username of the person applying the import</param>
        /// <returns>An import result object</returns>
        public static List<ImportResult> ImportCourse(string zipFilePath, SymphonyExternalConfiguration cfg, string username)
        {
            List<ImportResult> results = new List<ImportResult>();
            if (IsZipofZips(zipFilePath))
            {
                // if this is a "zip of zips" and the course to update is not 0, we have to bail, because we can't
                // update multiple courses at once.
                if (cfg.ExistingCourseID > 0)
                {
                    throw new Exception("The zip file contains more than one course, but courses can only be updated one at a time.");
                }

                // if this is a "zip of zips", then we need to loop through each zip file and process it the same way
                // just dump everything to a temp folder initially
                string folder = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
                ExtractZipFile(zipFilePath, folder);
                foreach (string file in Directory.GetFiles(folder, "*.zip"))
                {
                    results.Add(ImportSingleCourseFromZip(file, cfg, username, string.Empty));
                }
            }
            else
            {
                results.Add(ImportSingleCourseFromZip(zipFilePath, cfg, username, string.Empty));
            }
            return results;
        }

        public static ImportResult ImportSingleCourseFromZip(string zipFilePath, SymphonyExternalConfiguration cfg, string username, string subfolder)
        {
            // determine the course name and location
            string courseName = Path.GetFileNameWithoutExtension(zipFilePath);
            string courseDir = Integration.Implementation.GetFilePathToContent(Path.Combine(subfolder, courseName), cfg);

            // extract everything
            ExtractZipFile(zipFilePath, courseDir);

            return ImportSingleCourseFromFolder(courseDir, cfg, username);
        }

        public static ImportResult ImportSingleCourseFromFolder(string courseDir, SymphonyExternalConfiguration cfg, string username)
        {

            // find the manifest file in the unzipped contents
            string manifestFilePath = GetManifestFilePath(courseDir);

            if (manifestFilePath == null)
                return new ImportResult() { Message = "No manifest found", WasSuccessful = false };

            // get the manifest information
            Manifest manifest = RusticiSoftware.ScormContentPlayer.Logic.Import.ParseManifest(manifestFilePath, cfg);

            // get the path to the web content
            string webPathToContent = Integration.Implementation.GetWebPathToContent(Path.GetDirectoryName(manifestFilePath), cfg);
            webPathToContent = webPathToContent.TrimEnd('/');

            // AICC courses contain web path information within the course
            // and should not store anything for web_path.
            if (manifest.LearningStandard.IsAICC())
            {
                HandleAICCURLPath(manifestFilePath, manifest, cfg);
                webPathToContent = "";
            }

            // apply the import
            return ScormEngineManager.CreatePackage(manifest, webPathToContent, cfg, username);
        }

        /// <summary>
        /// Imports and online course into Rustici
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="courseFile"></param>
        /// <returns></returns>
        public static void SaveZipFile(string fileName, byte[] courseFileStream)
        {
            if (!Directory.Exists(Path.GetDirectoryName(fileName)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fileName));
            }
            File.WriteAllBytes(fileName, courseFileStream);
        }


        public static ArrayList ExtractZipFileAndChildZipFiles(string filePath, string fileName)
        {
            ArrayList extractDirectories = new ArrayList();
            if (IsZipofZips(fileName))
            {
                // The Zip Contains multiple zips so extract each of the child .zip files as individual courses
                string multiZipTempPath = null;
                ArrayList courseDirs = new ArrayList();
                try
                {
                    multiZipTempPath = Path.Combine(filePath, "temp");
                    multiZipTempPath = Path.Combine(multiZipTempPath, Guid.NewGuid().ToString());
                    if (Directory.Exists(multiZipTempPath))
                        Directory.Delete(multiZipTempPath, true);
                    Directory.CreateDirectory(multiZipTempPath);
                    ExtractZipFile(fileName, multiZipTempPath);

                    foreach (string childFilePath in Directory.GetFiles(multiZipTempPath, "*.zip"))
                    {
                        string courseName = RemoveFileExtension(Path.GetFileName(childFilePath));
                        string courseDirectory = filePath + courseName;
                        ExtractZipFile(childFilePath, courseDirectory);
                        extractDirectories.Add(courseDirectory);
                        courseDirs.Add(courseDirectory);
                    }

                    if (multiZipTempPath != null && Directory.Exists(multiZipTempPath))
                        Directory.Delete(multiZipTempPath, true);
                }
                catch
                {
                    // If anything happens during this extraction process, consider it a "transaction" and clean up everything that was done
                    // and rethrow the exception.

                    if (multiZipTempPath != null && Directory.Exists(multiZipTempPath))
                        Directory.Delete(multiZipTempPath, true);

                    foreach (string courseDir in courseDirs)
                    {
                        if (Directory.Exists(courseDir))
                            Directory.Delete(courseDir, true);
                    }

                    throw;
                }
            }
            else
            {
                // It's just a single-level zip, so just extract the base file
                string courseName = RemoveFileExtension(Path.GetFileName(fileName));
                string courseDirectory = filePath + courseName;
                ExtractZipFile(fileName, courseDirectory);
                extractDirectories.Add(courseDirectory);
            }

            return extractDirectories;
        }

        /// <summary>
        /// Extracts a zip file to the specified folder
        /// </summary>
        /// <param name="filePath">The file to unzip</param>
        /// <param name="extractedFilesBaseDir">The destination for the unzipped files.</param>
        private static void ExtractZipFile(String filePath, string extractedFilesBaseDir)
        {
            if (!Directory.Exists(extractedFilesBaseDir))
            {
                Directory.CreateDirectory(extractedFilesBaseDir);
            }

            ZipInputStream s = new ZipInputStream(File.OpenRead(filePath));

            ZipEntry theEntry;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                string directoryName = Path.GetDirectoryName(theEntry.Name);
                string fileName = Path.GetFileName(theEntry.Name);
                // create directory 
                if (directoryName != "")
                {
                    directoryName = extractedFilesBaseDir + "\\" + directoryName;
                    Directory.CreateDirectory(directoryName);
                }
                if (fileName != String.Empty)
                {
                    string entryPath = extractedFilesBaseDir + "\\" + theEntry.Name;
                    FileStream streamWriter = File.Create(entryPath);
                    int size = 2048;
                    byte[] data = new byte[2048];
                    while (true)
                    {
                        size = s.Read(data, 0, data.Length);
                        if (size > 0)
                        {
                            streamWriter.Write(data, 0, size);
                        }
                        else
                        {
                            break;
                        }
                    }
                    streamWriter.Close();
                }
            }
            s.Close();
        }

        /// <summary>
        /// Utility function that examines the root of the zip to see if it is in fact a "zip of zips" that 
        /// contains multiple courses.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsZipofZips(String filePath)
        {
            bool hasManifestInRoot = false;
            bool hasZipInRoot = false;

            using (ZipInputStream s = new ZipInputStream(File.OpenRead(filePath)))
            {
                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null)
                {
                    string directoryName = Path.GetDirectoryName(theEntry.Name);
                    bool isInRootDir = directoryName == null || directoryName.Length == 0;

                    string fileName = Path.GetFileName(theEntry.Name);
                    bool isZipFile = fileName.ToLower().EndsWith(".zip");
                    bool isManifest = fileName.ToLower() == "imsmanifest.xml" || fileName.ToLower().EndsWith(".au") ||
                                 fileName.ToLower().EndsWith(".crs");

                    if (!hasManifestInRoot)
                        hasManifestInRoot = isInRootDir && isManifest;

                    if (!hasZipInRoot)
                        hasZipInRoot = isZipFile && isInRootDir;
                }
                s.Close();
            }

            // If there's a manifest in the root, consider it a single course even if we see additional .zips in the root.
            return !hasManifestInRoot && hasZipInRoot;
        }

        /// <summary>
        /// Finds the manifest in the coursePath directory given.  Subdirectories are not searched. 
        /// The imsmanifest location is returned if found, else the AICC .crs is looked for.  If
        /// neither is found, null is returned;
        /// </summary>
        /// <param name="searchDir">Root directory of the course</param>
        /// <returns>Full path to course manifest if found, null if not found</returns>
        private static string GetManifestFilePath(string searchDir)
        {
            string manifestPath = null;

            // Create a list of manifests, putting SCORM manifests at the front for priority, followed by aicc files
            ArrayList allManifests = new ArrayList();
            allManifests.AddRange(Directory.GetFileSystemEntries(searchDir, "*imsmanifest.xml"));
            allManifests.AddRange(Directory.GetFileSystemEntries(searchDir, "*.crs"));
            allManifests.AddRange(Directory.GetFileSystemEntries(searchDir, "*.au"));

            if (allManifests.Count > 0)
                manifestPath = allManifests[0] as string;
            else
                foreach (string folderpath in Directory.GetDirectories(searchDir))
                    manifestPath = GetManifestFilePath(folderpath);

            return manifestPath;
        }

        /// <summary>
        /// This method will handle local server AICC URL settings for local server testing when fully qualified URLS aren't used in the AU file.
        /// </summary>
        /// <param name="manifestFilePath">The path to the manifest file or AICC descriptor file. Can be either a file path or a web path.</param>
        /// <param name="manifest">SCORM manifest or AICC descriptor file collection parsed into LearningObjects.</param>
        /// <param name="externalConfig">External configuration information to be passed to the integration layer</param>
        /// <returns>nothing.</returns>
        private static void HandleAICCURLPath(string manifestFilePath, Manifest manifest, ExternalConfiguration externalConfig)
        {
            // AICC courses contain web path information within the course
            // and should not store anything for web_path.
            if (IsAICC(manifestFilePath))
            {
                // The aicc learning object are seen as "sco's" in the manifest
                foreach (AiccLearningObject lo in manifest.Scos)
                {
                    // read the href and see if it's a complete URL or relative URL path
                    if (lo.Href != null && !lo.Href.ToLower().StartsWith("http"))
                    {
                        // If it ends in a filename, trim down to a dir
                        if (manifestFilePath.Contains("."))
                        {
                            manifestFilePath = Path.GetDirectoryName(manifestFilePath);
                        }

                        // Since it's relative, prepend the local pathing information to the files
                        string webPathToContent = Integration.Implementation.GetWebPathToContent(manifestFilePath, externalConfig);
                        if (webPathToContent.EndsWith("/"))
                            webPathToContent.TrimEnd('/');

                        // Let's edit the base path a bit if part of the path is already in the aicc href.  This will happen
                        // when the .au file is actually in a subdirectory of the course .zip file.
                        // Trim the page off the aicc url
                        string aiccUrlDir = Path.GetDirectoryName(lo.Href);
                        if (aiccUrlDir.EndsWith("/"))
                            aiccUrlDir.TrimEnd('/');

                        // webPathToContent = webPathToContent.TrimEnd(aiccUrlDir.ToCharArray());
                        // Above code was acting strangly so trying this instead...
                        if (webPathToContent.ToLower().EndsWith(aiccUrlDir.ToLower()))
                            webPathToContent = webPathToContent.Substring(0, webPathToContent.Length - aiccUrlDir.Length);

                        if (lo.Href.StartsWith("/"))// leading slash
                            lo.Href = webPathToContent + lo.Href;
                        else
                            lo.Href = webPathToContent + "/" + lo.Href;
                    }
                }
            }
        }

        /// <summary>
        /// This method searches nested folders of uploaded .zips for the manifest file
        /// </summary>
        /// <param name="folders">string[]</param>
        private string SearchNestedFolders(string[] folders)
        {
            foreach (string folderpath in folders)
            {
                string manifestFilePath = GetManifestFilePath(folderpath);
                if (manifestFilePath != null)
                {
                    return manifestFilePath;
                }
                else
                {	// check for nested dirs
                    String[] nestedFolders = Directory.GetDirectories(folderpath);
                    if (nestedFolders != null && nestedFolders.Length > 0)
                    {
                        manifestFilePath = SearchNestedFolders(nestedFolders);
                        if (manifestFilePath != null)
                        {
                            return manifestFilePath;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// This returns true if the path is to an AICC manifest
        /// </summary>
        /// <param name="path"></param>
        /// <returns>true if path is to an AICC manifest</returns>
        private static bool IsAICC(string path)
        {
            bool result = path.ToLower().EndsWith(".crs")
                            || path.ToLower().EndsWith(".au")
                            || path.ToLower().EndsWith(".cst")
                            || path.ToLower().EndsWith(".des");

            return result;
        }

        private static string RemoveFileExtension(string name)
        {
            return name.Substring(0, name.LastIndexOf('.'));
        }

        public static ImportResult GetFakePackageImportResult(int courseId)
        {
            return new ImportResult() {
                WasSuccessful = true,
                SerializedExternalPackageId = (new SymphonyExternalPackage() { CourseId = courseId }).PersistToString()
            };
        }
    }
}