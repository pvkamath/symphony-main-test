﻿using SubSonic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data = Symphony.Core.Data;

namespace Symphony.CourseAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            // step 1: 
            List<Data.OnlineCourseRollup> courses = Select.AllColumnsFrom<Data.OnlineCourseRollup>()
                .Where(Data.OnlineCourseRollup.ModifiedOnColumn).IsGreaterThan("2/1/2016")
                .ExecuteTypedList<Data.OnlineCourseRollup>();

            string connectionString = ConfigurationManager.ConnectionStrings["OnlineTraining"].ConnectionString;

            // step 1 & 2: pull all the onlinecourse entities that have activity in the last 6 months, then, for all those, pull the SCORM data for any that have a 100% navigation
            // and have suspend data and are completed
            using (var connection = new SqlConnection(connectionString))
            {
                // sql to pull page count of the artisan course tied to a given rollup
                var cmdText = @"select
	ocr.id as OnlineCourseRollupID, aa.ArtisanCourseID, ArtisanCourseContentPageCount, a.suspend_data as SuspendData, ocr.ArtisanAuditID
from
	ScormActivityRT a
join
	ScormActivity b
on
	a.scorm_activity_id = b.scorm_activity_id
join
	ScormRegistration
on
	ScormRegistration.scorm_registration_id = b.scorm_registration_id
join
	ScormActivityObjective
on
	ScormActivityObjective.scorm_activity_id = b.scorm_activity_id
join
	symphony.dbo.OnlineCourseRollup ocr
on
	ScormRegistration.course_id = ocr.CourseID and ScormRegistration.training_program_id = ocr.TrainingProgramID and ScormRegistration.[user_id] = ocr.UserID
join
	Symphony.dbo.ArtisanAudit aa
on
	aa.ID = ocr.ArtisanAuditID
join
	Symphony.dbo.ArtisanCourses ac
on
	aa.ArtisanCourseID = ac.ID
join
	(
	select COUNT(ap.ID) as ArtisanCourseContentPageCount, ac.ID as ArtisanCourseID
	from Symphony.dbo.artisancourses ac
	join Symphony.dbo.ArtisanPages ap
	on ap.CourseID = ac.ID
	where ap.PageType = 1
	group by ac.id
	) as PageCounter
on
	PageCounter.ArtisanCourseID = ac.ID
where
	ScormActivityObjective.completion_status = 1
and
	suspend_data is not null
and 
	NavigationPercentage = 100
and
	suspend_data like 'gz%'
and
    // nav type = full sequential
and
	ocr.ModifiedOn > '3/1/2013'
	";

                foreach (var course in courses)
                {
                    using (var cmd = new SqlCommand(cmdText, connection))
                    {
                        
                        
                    }
                }
            }

            // step 3: determine, from the artisanaudit, how many pages the student should have visited

            // step 4: determine page count that was visited from the gzipped data

            // step 5: if they don't match, log it


        }
    }
}
