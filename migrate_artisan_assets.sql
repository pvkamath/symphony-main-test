use contentauthor2;

set identity_insert symphony.dbo.artisanassets on;
GO
insert symphony.dbo.artisanassets
(id, customerid, name, description, assettypeid, createdon, modifiedon, createdby, modifiedby, filename, keywords, ispublic)

select
	ID, BusinessEntityID, title, description, 
		case AssetTypeID
			when 2 then 1
			when 6 then 2
			when 1 then 3
			when 7 then 3
			when 10 then 4
			when 11  then 4
			when 8 then 5
		end as AssetTypeID, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy, filename, (
		coalesce((select keyword.keywordname + ',' from keyword join keywordasset on keywordid = keyword.id where assetid = asset.id FOR XML PATH('')),'')
	), publicindicator
from asset
where isdeleted = 0 and groupid = 0

GO
set identity_insert symphony.dbo.artisanassets off
GO
