﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.RusticiIntegration.Core;
using Symphony.RusticiIntegration;

namespace FixerUpper
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 3)
            {
                Console.Write("A course id, customer id, and folder are required, in that order. The folder should have quotes if it has spaces.");
            }
            else
            {
                SymphonyExternalConfiguration cfg = new SymphonyExternalConfiguration();
                cfg.ExistingCourseID = int.Parse(args[0]);
                cfg.CustomerID = int.Parse(args[1]);

                SymphonyIntegrationController.ImportSingleCourseFromFolder(args[2], cfg, "system");
            }
        }
    }
}
