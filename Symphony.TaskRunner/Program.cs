﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.Reflection;
using SubSonic.Utilities;
using System.Diagnostics;
using SubSonic;
using System.Data;
using System.Configuration;
using Symphony.Core.Controllers;
using log4net;
using log4net.Config;
using System.IO;
using Models = Symphony.Core.Models;
using Symphony.Core.Data;
using System.Threading.Tasks;

namespace Symphony.TaskRunner
{
    class Program
    {
        static ILog Log = LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            try
            {
                XmlConfigurator.Configure();
                // Queue 7 days worth of notifications - this will schedule any notifications that were supposed to go out
                // prior to 7 days from now. 
                //DateTime nextWeek = DateTime.UtcNow.AddDays(7);            
                //Symphony.Core.Controllers.NotificationController.SendScheduledNotifications(nextWeek);

                bool isQa = false;
                if (bool.TryParse(ConfigurationManager.AppSettings["IsQAServer"], out isQa) && isQa)
                {
                    Symphony.Core.Controllers.NotificationController.IsQAServer = true;
                }

                // Always force qa mode if IsQAServer compile symbol set, or debug flag is set. 
#if IsQAServer || DEBUG
                Symphony.Core.Controllers.NotificationController.IsQAServer = true;
#endif

                if (Symphony.Core.Controllers.NotificationController.IsQAServer)
                {
                    Console.WriteLine("Running in QA mode - Notifications will not be emailed...");
                }
                else
                {
                    Console.WriteLine("Running in production mode - Notifications will be emailed to students!");
                }

                Symphony.Core.Controllers.NotificationController.SendScheduledNotifications();
                Symphony.Core.Controllers.UserController.UpdateNewHires();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
                Log.Error(ex);
                Console.Read();
            }
            sw.Stop();
            Console.WriteLine("Notifications and new hire processing complete.");
            Console.WriteLine("Total time: " + sw.Elapsed.Minutes + ":" + sw.Elapsed.Seconds);
            System.Threading.Thread.Sleep(10000);
        }

        //private static void Test()
        //{
        //    User locuser = new User(199);
        //    BankersEdge.Data.CNRService.CNRService cnrService = new BankersEdge.Data.CNRService.CNRService();
        //    cnrService.Url = ConfigurationSettings.AppSettings["CNRService.CNRService"]; 
        //    string password = WebWidgetry.Utilities.Util.GetSharedReportingPassword();
        //    string username = "1 147 David (admin) Tufariello";
        //    //string username = WebWidgetry.Utilities.Util.GetReportingUsername(locuser); 
        //    try
        //    {
        //        string token = cnrService.AuthenticateForms(username, password);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //}

        /// <summary>
        /// Generates test templates for all the notifications to ensure all the fields are valid.
        /// </summary>
        //public static void TestTemplates()
        //{
        //    // loop through the notification templates
        //    foreach (FieldInfo fi in typeof(TemplateController).GetFields())
        //    {
        //        // static type = null object
        //        string templateName = fi.GetValue(null).ToString();

        //        // get a list of all the types that can be used in the template
        //        Type[] types = TemplateController.GetValidTypesForTemplate(new Template(Template.Columns.CodeName, templateName));

        //        List<object> objects = new List<object>();

        //        Console.WriteLine("Template: " + templateName);

        //        // for each type that can be used in the template, get a template
        //        StringBuilder sb = new StringBuilder();
        //        foreach (Type t in types)
        //        {
        //            TemplateController.BuildSampleTemplate(t, sb);
        //            objects.Add(TemplateController.GetObjectInstance(t));
        //        }
        //        string parsed = TemplateController.Parse(sb.ToString(), true, objects.ToArray());

        //        if (parsed.IndexOf("unknown", StringComparison.CurrentCultureIgnoreCase) > -1 || parsed.IndexOf("{!", StringComparison.CurrentCultureIgnoreCase) > -1)
        //        {
        //            Console.WriteLine("Fail!");
        //            Console.WriteLine(parsed);
        //            Console.Read();
        //        }
        //        else
        //        {
        //            Console.WriteLine(parsed);
        //        }
        //    }
        //}
    }
}
