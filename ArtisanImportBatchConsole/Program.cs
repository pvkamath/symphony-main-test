﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Symphony.Core.Controllers;
using log4net;
using System.Configuration;

namespace ArtisanImportBatchConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            log4net.Config.XmlConfigurator.Configure();
            ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            log.Debug("Import started");

            int userId = int.Parse(ConfigurationManager.AppSettings["userId"]);
            int customerId = int.Parse(ConfigurationManager.AppSettings["customerID"]);
            int categoryId = int.Parse(ConfigurationManager.AppSettings["categoryID"]);
            string username = ConfigurationManager.AppSettings["username"];
            string downloadPath = ConfigurationManager.AppSettings["downloadPath"];
            string path = ConfigurationManager.AppSettings["path"];


            string appPath = path;
            ArtisanController artisanController = new ArtisanController();
            artisanController.ImportArtisanCourses(downloadPath, log, appPath, userId, customerId, categoryId, username);

            log.Debug("Finished");
            Console.ReadLine();
        }
    }
}
