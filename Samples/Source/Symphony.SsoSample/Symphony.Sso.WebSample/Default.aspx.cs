﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;

namespace Symphony.Sso.WebSample
{
    public partial class Default : System.Web.UI.Page
    {
        private static readonly string ServiceProviderUrl = WebConfigurationManager.AppSettings["ServiceProviderUrl"];
        protected string NavigateUrl;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Set the navigate URL to the SSO service page. "url" parameter is the target URL of the Service Provider.
            NavigateUrl = Page.ResolveUrl(string.Format("~/SsoRedirect.aspx?url={0}", HttpUtility.UrlEncode(ServiceProviderUrl)));
        }
    }
}