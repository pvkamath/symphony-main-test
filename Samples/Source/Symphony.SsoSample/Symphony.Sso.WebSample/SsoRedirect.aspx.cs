﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
using Symphony.Sso.Core;
using System.IO;

namespace Symphony.Sso.WebSample
{
    public partial class SsoRedirect : System.Web.UI.Page
    {
        private static readonly string ConsumerServiceUrl = WebConfigurationManager.AppSettings["ConsumerServiceUrl"];
        private static readonly string IdentityProviderUrl = WebConfigurationManager.AppSettings["IdentityProviderUrl"];
        private static readonly string KeyFile = WebConfigurationManager.AppSettings["KeyFile"];
        private static readonly string KeyFilePassword = WebConfigurationManager.AppSettings["KeyFilePassword"];

        // For testing, in production this would be the currently logged in user's name
        private static readonly string UserName = "admin";

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                // Extract the url of the target (service provider).
                string targetUrl = Request.QueryString["url"];

                // Load the certificate.
                string fileName = Path.Combine(HttpRuntime.AppDomainAppPath, KeyFile);
                X509Certificate2 x509Certificate = new X509Certificate2(fileName, KeyFilePassword);

                SsoManager ssoManager = new SsoManager(IdentityProviderUrl, ConsumerServiceUrl, x509Certificate);
                ssoManager.RedirectToTarget(UserName, targetUrl, Response.OutputStream);
            }

            catch (Exception exception)
            {
                Response.Write(string.Format("An error occurred: {0}", exception));
            }
        }
    }
}