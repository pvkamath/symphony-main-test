﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ComponentPro.Saml2;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace Symphony.Sso.Core
{
    public class SsoManager
    {
        public string IdentityProviderUrl { get; set; }
        public string ConsumerServiceUrl { get; set; }
        public X509Certificate2 SigningCertificate { get; set; }

        public SsoManager(string identityProviderUrl, string consumerServiceUrl, X509Certificate2 signingCertificate)
        {
            IdentityProviderUrl = identityProviderUrl;
            ConsumerServiceUrl = consumerServiceUrl;
            SigningCertificate = signingCertificate;
        }

        public void RedirectToTarget(string username, string targetUrl, Stream output)
        {
            if (string.IsNullOrEmpty(targetUrl))
            {
                throw new ArgumentException("Target URL cannot be empty.", "targetUrl");
            }

            // Create a SAML response object.
            ComponentPro.Saml2.Response samlResponse = new ComponentPro.Saml2.Response();
            // Assign the consumer service url.
            samlResponse.Destination = ConsumerServiceUrl;
            Issuer issuer = new Issuer(IdentityProviderUrl);
            samlResponse.Issuer = issuer;
            samlResponse.Status = new Status(SamlPrimaryStatusCode.Success, null);

            Assertion samlAssertion = new Assertion();
            samlAssertion.Issuer = issuer;

            // Use the local user's local identity.
            Subject subject = new Subject(new NameId(username));
            SubjectConfirmation subjectConfirmation = new SubjectConfirmation(SamlSubjectConfirmationMethod.Bearer);
            SubjectConfirmationData subjectConfirmationData = new SubjectConfirmationData();
            subjectConfirmationData.Recipient = ConsumerServiceUrl;
            subjectConfirmation.SubjectConfirmationData = subjectConfirmationData;
            subject.SubjectConfirmations.Add(subjectConfirmation);
            samlAssertion.Subject = subject;

            // Create a new authentication statement.
            AuthnStatement authnStatement = new AuthnStatement();
            authnStatement.AuthnContext = new AuthnContext();
            authnStatement.AuthnContext.AuthnContextClassRef = new AuthnContextClassRef(SamlAuthenticateContext.Password);
            samlAssertion.Statements.Add(authnStatement);

            // Add assertion to the SAML response object.
            samlResponse.Assertions.Add(samlAssertion);

            // Sign the SAML response with the certificate.
            samlResponse.Sign(SigningCertificate);

            // Send the SAML response to the service provider.
            samlResponse.SendPostBindingForm(output, ConsumerServiceUrl, targetUrl);
        }
    }
}
