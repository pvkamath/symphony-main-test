//TODO: This class is an alternative to the XML-based Log.js class.  We may be able to use open source
//      xml functionality for non-IE browsers rather than using this array/object based method.

/*
	Log Constructor
*/
function Log(version, includeTimestampsInXml) {

	this.isXmlBased = false;

	this.children = new Array();
	this.info = new Array();
	this.version = version; 
	this.includeTimestamps = includeTimestampsInXml;
}

Log.prototype.startNew = function(recordType, args, data) {

	var logEntry;
	var functionName;
	
	// If we're passed in a string first thing, "overload" this method with a signature like " startNewAtRoot(title, arg1, arg2, etc.);
	if (typeof args == "string") 
	{
		functionName = args;
		logEntry = new LogEntry(this.includeTimestamps, functionName);	
	} 
	else if (args !== null && args !== undefined)
	{
		functionName = args.callee.toString().substring(9, args.callee.toString().indexOf("{")-1);
		logEntry = new LogEntry(this.includeTimestamps, functionName, args);
	}
	else
	{
		logEntry = new LogEntry(this.includeTimestamps, "unknown entry point");
	}
	
	logEntry.type = recordType;
	
	this.children[this.children.length] = logEntry;
	return logEntry;
};

Log.prototype.display = function() {
	var win = window.open("text/plain");
	win.document.write(
		"<style type='text/css'> body, td, th { font-family: Verdana; font-size: xx-small; color: black}" +
		"h2 { font-size: medium; font-family: Arial; color: navy }" +
		"" +
		".s { color: red }" +
		".r {color: blue }" +
		".c {color: green }" +
		".l {color: purple }" +
		".ss {color: orange }" +
		".info {color: black }" +
		"" +
		"div { margin-left: 20px;}" +
		"div.details { display: none; }" +
		"" +
		"link {cursor: hand}" +
		"" +
		"a, a#visited {color: black; text-decoration: none}" +
		"a#hover {color: blue} </style>");
		
	win.document.write(
		"<table cellspacing='0' cellpadding='0' width='100%'>" +
		"<tr>" +
		"	<td>" +
		"		<h2>SCORM Engine Log</h2>" +
		"		<div>v " + this.version + "</div>" +
		"	</td>" +
		"	<td valign='top' align='center'>" +
		"		<span class='c'>Control </span>" +
		"		<span class='r'>Runtime </span>" +
		"		<span class='s'>Sequencing </span>" +
		"		<span class='l'>Look-ahead </span>" +
		"       <span class='ss'>Readable Sequencing </span>" +
		"	</td>" +
		"</tr>" +
		"</table>");
	
	for (var i = 0; i < this.children.length; i++) {
		renderLogEntry(this.children[i], win);
	}
	
	win.document.close();
};

function renderLogEntry(le, win) {
	
	win.document.write("<div class='" + le.type + "'>");
	
	if (le.includeTimestamps && le.timestamp !== undefined) {
		var formattedTime = le.timestamp.getHours() + ":" + le.timestamp.getMinutes() +
							 ":" + le.timestamp.getSeconds() + "." + le.timestamp.getMilliseconds();
		win.document.write("[" + formattedTime + "] ");
	}
	var logCompressor = new LogCompression(window.dictionary_ll);
	
	win.document.write(logCompressor.decompressString(le.func));
	
	if (le.returnValue !== undefined) {
		win.document.write(" returned '" + le.returnValue + "'");
	}
	
	if (le.elapsedTime !== undefined) {
		win.document.write(" in " + le.elapsedTime + " seconds");
	}
	
	//win.document.write("<div class='details'>");
	
	
	
	for (var i = 0; i < le.info.length; i++) {
		win.document.write("<div class='info'>");
		win.document.write (logCompressor.decompressString(le.info[i]));
		win.document.write("</div>");
	}
	
	for (var i = 0; i < le.children.length; i++) {
		renderLogEntry(le.children[i], win);
	}	
	
	//win.document.write("</div>");
	win.document.write("</div>");
}

/*
	LogEntry Constructor
*/
function LogEntry(includeTimestampsInXml, functionName, functionArgs) {
	
	this.children = new Array();
	this.info = new Array();
	this.func = functionName;
	this.args = new Array();
	this.includeTimestamps = includeTimestampsInXml;

	if (this.includeTimestamps) {
		this.timestamp = new Date();		
	}

	if (functionArgs !== null && functionArgs !== undefined) {
		for (var i=0; i < functionArgs.length; i++) {
			args[args.length] = functionArgs[i];
		}
	}
}

LogEntry.prototype.write = function(message) {

   if (this.includeTimestamps) {
		var now = new Date();
		var timestamp = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds() + "." + now.getMilliseconds();
		this.info[this.info.length] = "[" + timestamp + "] " +  message;
	} else {
		this.info[this.info.length] = message;
	}
	
};

LogEntry.prototype.startNew = function(recordType, args, data) {
	
	var logEntry;
			
	// If we've passed in a string first thing, "overload" this method with a signature like startNewAtRoot(title, arg1, arg2, etc.);
	if (typeof args == "string") 
	{
		logEntry = new LogEntry(this.includeTimestamps, args, data);	
	} 
	else if (args !== null && args !== undefined)
	{
		var functionName = args.callee.toString().substring(9, args.callee.toString().indexOf("{")-1);
		logEntry = new LogEntry(this.includeTimestamps, functionName, args);
	}
	else
	{
		logEntry = new LogEntry(this.includeTimestamps, "unknown entry point");
	}
	
	logEntry.type = recordType;
	
	this.children[this.children.length] = logEntry;
	return logEntry;
};

LogEntry.prototype.setAttribute = function(name, value) {
	eval("this." + name + " = " + value);
};

LogEntry.prototype.setReturn = function(value) {
	
	if (this.includeTimestamps) {
		var now = new Date();
		var elapsedTime = (now.getTime() - this.timestamp.getTime()) / 1000;
		this.elapsedTime = elapsedTime;
	} 
	
	this.returnValue =  value;
};