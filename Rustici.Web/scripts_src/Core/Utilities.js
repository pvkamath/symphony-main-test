String.prototype.trim = String_Trim;	//TODO - test the ability for this to work like str.trim()...or does it need to be str=str.trim();...update code accordingly
String.prototype.toBoolean = String_ToBoolean;

function String_Trim(){
	return Trim(this);
}

function Trim(str){
	str = str.replace(/^\s*/, "");
	str = str.replace(/\s*$/, "");
	return str;
}

function String_ToBoolean(){
    
    var str = this;
    
    str = new String(str).toLowerCase();
    
    if (str == "1" || str.charAt(0) == "t"){
        return true;
    }
    else if (str == "0" || str.charAt(0) == "f"){
        return false;
    }
    else {
        Debug.AssertError("Value '" + str + "' can not be converted to a boolean.");
    }
}

//accepts an arbitrary number of arguments and merges them into a single querystring
function MergeQueryStringParameters(){
	
	var paramNames = new Array();
	var paramValues = new Array();
	
	var url = null;
	var anchor = null;
	var i;

	var numOfNonEmptyArgs = 0;
	var firstArg = null;
	for (i = 0; i < arguments.length; i++)
	{
		if (arguments[i] != null && arguments[i].length > 0) {
			numOfNonEmptyArgs++;
			if (firstArg == null) {
				firstArg = arguments[i];
			}
		}
	}
	
	// Exit it by returning our one arg if that's all we have
	if (numOfNonEmptyArgs == 1) {
		return firstArg;
	}

	for (i = 0; i < arguments.length; i++)
	{
		var qs = arguments[i];
		
		qs = new String(qs);
		
		// Pick off the anchor if it exists.
		// If the query string ends in a named anchor like "#middleofpage, save this
		// off and strip it from the url.  The first anchor seen will be the one used
		// in the merged query string.
		if (qs.indexOf("#") > -1 )
		{
			if (qs.charAt(0) == "#")
			{
				if (anchor === null)
				{
					anchor = qs.substring(1);
				}
				qs = "";
			}
			else 
			{
				var splitAtAnchor = qs.split('#');
				if (anchor === null)
				{
					anchor = splitAtAnchor[1];
				}
				qs = splitAtAnchor[0];
			}
		}		
		
		// Pick off the url if there is one
		if (qs.indexOf("?") > 0) 
		{
			var urlPart = qs.substring(0, qs.indexOf("?"));
			var qsPart = qs.substring(qs.indexOf("?")+1, qs.length);
			if (url === null)
			{
				url = urlPart;
			}
			qs = qsPart;
			
		}
		// If there's no ?, #, =, or &, assume it's a pure url
		if (qs.indexOf("#") < 0 && qs.indexOf("&") < 0 && qs.indexOf("=") < 0 && qs.indexOf("?") < 0)
		{
			if (url === null)
			{
				url = qs;
			}
			qs = "";
		}

		// May or may not be recieved with a "?" or "&" prefix so trim it off if it exists
		if (qs.charAt(0) == '?')
		{
			qs = qs.substring(1);
		}
		if (qs.charAt(0) == '&')
		{
			qs = qs.substring(1);
		}			
			
		// Now get the params	
		if (qs.indexOf("&") > -1)
		{
			var allPairs = qs.split('&');
			for (var j = 0; j < allPairs.length; j++)
			{
				AddQueryStringParm(paramNames, paramValues, allPairs[j]);
			}
		} 
		else
		{
			if (qs.length > 0) {
				AddQueryStringParm(paramNames, paramValues, qs);
			}
		}
	}

	// Now contsruct the merged query string...
	var mergedQueryString = "";
	
	if (url !== null)
	{
		mergedQueryString += url;
	}
	
	var isFirstParm = true;
	for (i = 0; i < paramNames.length; i++)
	{
		if (isFirstParm)
		{
			mergedQueryString += "?";
			isFirstParm = false;
		}
		else
		{
			mergedQueryString += "&";
		}
		
		
		mergedQueryString += paramNames[i];

		var value = paramValues[i];
		// Some params may just have a name with no value, if so don't append "="
		if (value.length > 0)
		{
			mergedQueryString += "=" + value;
		}
	}

	if (anchor !== null)
	{
		mergedQueryString += "#" + anchor;
	}

	return mergedQueryString;
}

//private helper method for MergeQueryStringParameters()
function AddQueryStringParm(names, values, qsParm)
{
	if (qsParm.indexOf("=") > -1)
	{
		var parm = qsParm.split('=');
		var name = parm[0];
		var value = parm[1];
		names[names.length] = name;
		values[values.length] = value;
	}
	else
	{
		// If it's just a name, add the parm as a property with a value of empty string
		names[names.length] = qsParm;
		values[values.length] = "";
	}	
}

function CleanExternalString(str){
	//make sure the string is in a proper JS string format (who knows where it came from originially - Flash, Java, etc)
	//these transitions are enough to make absolutely sure that we have a native JavaScript string when we're done
	str = str + "";
	str = new String(str);
	str = str.toString();
	return str;
}


function ZeroPad(num, numDigits){
	
	var strTemp;
	var intLen;
	var i;
	
	strTemp = new String(num);
	intLen = strTemp.length;
	
	if (intLen > numDigits){
		strTemp = strTemp.substr(0,numDigits);
	}
	else{
		for (i=intLen; i<numDigits; i++){
			strTemp = "0" + strTemp;
		}
	}
	
	return strTemp;
}

//currently, rounds to the number of specified specified digits after the decimal point
function RoundToPrecision(number, significantDigits){
    return Math.round(number * Math.pow(10, significantDigits)) / Math.pow(10, significantDigits);
}


