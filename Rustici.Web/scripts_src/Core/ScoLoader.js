function ScoLoader(intermediatePage, popupLauncherPage, pathToCourse, scoLaunchType, wrapScoWindowWithApi, standard){
	
	this.IntermediatePage = intermediatePage;
	this.PopupLauncherPage = popupLauncherPage;
	this.PathToCourse = pathToCourse;
	this.ScoLaunchType = scoLaunchType;
	this.WrapScoWindowWithApi = wrapScoWindowWithApi;
	this.Standard = standard;
	
	this.ContentFrame = ScoLoader_FindContentFrame(window);
	
	if (this.ContentFrame === null){
		Debug.AssertError("Unable to locate the content frame-" + IntegrationImplementation.CONTENT_FRAME_NAME);
	}
}

ScoLoader.prototype.LoadSco = ScoLoader_LoadSco;
ScoLoader.prototype.UnloadSco = ScoLoader_UnloadSco;	
ScoLoader.prototype.WriteHistoryLog = ScoLoader_WriteHistoryLog;
ScoLoader.prototype.WriteHistoryReturnValue = ScoLoader_WriteHistoryReturnValue;

function ScoLoader_WriteHistoryLog(str, atts){
	HistoryLog.WriteEventDetailed(str, atts);
}

function ScoLoader_WriteHistoryReturnValue(str, atts){
	HistoryLog.WriteEventDetailedReturnValue(str, atts);
}

function ScoLoader_LoadSco(activity){
	
	var historyAtts = {ev:'LoadSco'};
	if (activity) {
	   historyAtts.ai = activity.ItemIdentifier;
	   historyAtts.at = activity.LearningObject.Title;
	}
	this.WriteHistoryLog("", historyAtts);
	

	activity.LaunchedThisSession = false;
	
	if (activity.GetActivityStartTimestampUtc() === null) {
		activity.SetActivityStartTimestampUtc(ConvertDateToIso8601String(new Date()));
	}

	if (activity.GetAttemptStartTimestampUtc() === null) {
		activity.SetAttemptStartTimestampUtc(ConvertDateToIso8601String(new Date()));
	}
	
	var aiccParams = "";
	if (this.Standard.isAICC()){
		aiccParams = "AICC_SID=" + escape(ExternalRegistrationId + "~" + escape(ExternalConfig) + "~" + activity.GetDatabaseIdentifier() + "~" + RegistrationToDeliver.Package.Id);
		// The fourth parameter of the AICC_SID indicates whether tracking is turned on
		if (RegistrationToDeliver.TrackingEnabled === false) {
			aiccParams += escape("~false");
		} else {
			aiccParams += escape("~true");
		}
		aiccParams += "&AICC_URL=" + escape(AICC_RESULTS_PAGE);
	}
	
	var pathToSco = "";
	
	// If activity url is relative then prepend pathToCourse
	if (this.PathToCourse.length > 0 && activity.GetLaunchPath().toLowerCase().indexOf("http://") != 0 && activity.GetLaunchPath().toLowerCase().indexOf("https://") != 0){
		
		pathToSco = this.PathToCourse;
		
		if (this.PathToCourse.lastIndexOf ("/") != (this.PathToCourse.length - 1)){
			pathToSco += "/";
		}
		
		pathToSco += activity.GetLaunchPath();
		
	}
	else{
		pathToSco = activity.GetLaunchPath();
	}
	
	var launchPath;
	
	if (aiccParams !== ""){
	
		pathToSco = MergeQueryStringParameters(pathToSco, aiccParams);
	}
	
	if (this.ScoLaunchType == LAUNCH_TYPE_FRAMESET){

		Control.WriteDetailedLog("Loading Sco In Frameset at: " + pathToSco);
		
		this.ContentFrame.location = pathToSco;
		
		Control.Api.InitTrackedTimeStart(activity);
		activity.SetLaunchedThisSession();
	}
	else if (this.ScoLaunchType == LAUNCH_TYPE_POPUP ||
			 this.ScoLaunchType == LAUNCH_TYPE_POPUP_WITHOUT_BROWSER_TOOLBAR ||
			 this.ScoLaunchType == LAUNCH_TYPE_POPUP_WITH_MENU ||
			 this.ScoLaunchType == LAUNCH_TYPE_POPUP_AFTER_CLICK ||
			 this.ScoLaunchType == LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR){
		
		if (this.ScoLaunchType == LAUNCH_TYPE_POPUP_WITHOUT_BROWSER_TOOLBAR || this.ScoLaunchType == LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR) {
			var showToolbar = "false";
		} else {
			var showToolbar = "true";
		}

		if (this.ScoLaunchType == LAUNCH_TYPE_POPUP_WITH_MENU) {
			var showMenubar = "yes";
		} else {
			var showMenubar = "no";
		}

		if (this.ScoLaunchType == LAUNCH_TYPE_POPUP_AFTER_CLICK || this.ScoLaunchType == LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR){
			var launchAfterClick = "true";
		} else {
			var launchAfterClick = "false";
		}

		if (this.WrapScoWindowWithApi === true){
			launchPath = MergeQueryStringParameters(this.PopupLauncherPage.PageHref, 
													this.PopupLauncherPage.Parameters, 
													"ScoUrl=" + escape(pathToSco),
													"LaunchAfterClick=" + launchAfterClick,
													"WrapApi=true",
													"ShowToolbar=" + showToolbar,
													"ShowMenubar=" + showMenubar);
		}
		else{
			launchPath = MergeQueryStringParameters(this.PopupLauncherPage.PageHref, 
													this.PopupLauncherPage.Parameters, 
													"ScoUrl=" + escape(pathToSco),
													"LaunchAfterClick=" + launchAfterClick,
													"ShowToolbar=" + showToolbar,
													"ShowMenubar=" + showMenubar);
		}
		this.ContentFrame.location = launchPath;
	}
	else{
		Debug.AssertError("Invalid Sco Launch Type");
	}
}

function ScoLoader_UnloadSco(messageToDisplay){

	var path = "";
    
    if (Control.Package.Properties.MakeStudentPrefsGlobalToCourse === true){
        Control.UpdateGlobalLearnerPrefs()
    }

    if (Control.Package.Properties.RollupRuntimeAtScoUnload === true){
        window.setTimeout("Control.EvaluatePossibleNavigationRequests(true);", 150);
    }

	if (messageToDisplay === true){
		path = MergeQueryStringParameters(this.IntermediatePage.PageHref, this.IntermediatePage.Parameters, "MessageWaiting=true");
	}
	else{
		path = MergeQueryStringParameters(this.IntermediatePage.PageHref, this.IntermediatePage.Parameters);
	}
	
	if ( ( this.ScoLaunchType == LAUNCH_TYPE_POPUP || 
		   this.ScoLaunchType == LAUNCH_TYPE_POPUP_AFTER_CLICK) && 
		 this.ContentFrame.CloseSco){
		
		Control.WriteDetailedLog("Closing Sco");
		this.ContentFrame.CloseSco();
		
		Control.WriteDetailedLog("Launching intermediate page from " + path);
		window.setTimeout("Control.ScoLoader.ContentFrame.location = '" + path + "'", 250);
	}
	else{
		Control.WriteDetailedLog("UnLoading Sco and launching intermediate page from " + path);
		this.ContentFrame.location = path;
	}
}


function ScoLoader_FindContentFrame(wnd){
	//search all child frames recursively until we find the content frame
	var contentWindow = null;
	
	for (var i=0; i < wnd.frames.length; i++){
		
		if (wnd.frames[i].name == IntegrationImplementation.CONTENT_FRAME_NAME){
			contentWindow = wnd.frames[i];
			return contentWindow;
		}
		
		contentWindow = ScoLoader_FindContentFrame(wnd.frames[i]);
		
		if (contentWindow !== null){
			return contentWindow;
		}
	}
	
	return null;
}
