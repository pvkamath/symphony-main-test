/*
	Log Constructor
*/
function Log(version, includeTimestampsInXml, rootName) {

	this.dom = null; 
	this.root = null;
	
	this.isXmlBased = true;
	this.includeTimestamps = includeTimestampsInXml;

	//this.refreshInterval = 10000;
	//this.autoRefresh = false;

	try { 
		
		this.dom = Sarissa.getDomDocument();  
		this.dom.async = false;
		this.dom.validateOnParse = false;
		this.dom.resolveExternals = false;
		//this.dom.includeTimestamps = includeTimestampsInXml;
		
		// Needed for sarissa/IE
        try {
		    this.dom.setProperty("SelectionLanguage", "XPath");  
		    this.dom.setProperty("SelectionNamespaces", "xmlns:xhtml='http://www.w3.org/1999/xhtml'");  
        } catch (ex) {
            // if we can't set it, we probably don't need it
        }
        
		// Create a processing instruction targeted for xml.
		var node = this.dom.createProcessingInstruction("xml", "version='1.0'");
		this.dom.appendChild(node);
		node = null;
		
		// Create a comment for the document.
		node = this.dom.createComment("SCORM Engine Log");
		this.dom.appendChild(node);
		node=null;

		// Create the root element.
		if (rootName == undefined)
		{
		    this.root = this.dom.createElement("scp-log");
		}
		else 
		{
		    this.root = this.dom.createElement(rootName);
		}

		// Create a version attribute to identify the SCP version running on the server
		this.root.setAttribute("version", version);
		this.root.setAttribute("browser", navigator.userAgent);

		// Add the root element to the DOM instance.
		this.dom.appendChild(this.root);
	}
	catch (e)
	{
		alert("Error creating debug log: " + e.description);
	}
}

Log.prototype.startNew = function(recordType, args, data) {

	var logEntry;
	var functionName;
	var node = this.dom.createElement(recordType);
	
	// If we're passed in a string first thing, "overload" this method with a signature like " startNewAtRoot(title, arg1, arg2, etc.);
	if (typeof args == "string") 
	{
		functionName = args;
		logEntry = new LogEntry(node, this, functionName);
		this.root.appendChild(node);		
	} 
	else if (args !== null && args !== undefined)
	{
		functionName = args.callee.toString().substring(9, args.callee.toString().indexOf("{")-1);
		logEntry = new LogEntry(node, this, functionName, args);
		this.root.appendChild(node);
	}
	else
	{
		logEntry = new LogEntry(node, this, "unknown entry point");
		this.root.appendChild(node);
	}
	
	return logEntry;
};

Log.prototype.startNewAtRoot = function(args, data) {
	return this.startNew(this.root, args, data);
};

Log.prototype.write = function(parentNode, message) {
	var node = this.dom.createElement("i");
	var text = this.dom.createTextNode(message);
	node.appendChild(text);
	parentNode.appendChild(node);
};

Log.prototype.display = function(loadAllData) {

	this.displayRefresh(null, false, loadAllData);
};

Log.prototype.displayFilter = function(callingWindow, filterObj) {
	
	if (callingWindow) callingWindow.close()
	
	var tempdom = Sarissa.getDomDocument();
	tempdom.async = false;
	tempdom.validateOnParse = false;
	tempdom.resolveExternals = false;
	var temproot = tempdom.createElement("scp-log");
	temproot.setAttribute("version", this.root.getAttribute("version"));
	tempdom.appendChild(temproot);
	
	var xslParams = new Object();
	
	var xpathStmt = ""
	
	if (filterObj['controlFilter']){
		 xpathStmt += "scp-log/c";
	} 
	else{
	    xslParams.controlFilter = "false";
	}
	
	if (filterObj['runtimeFilter']){
		if (xpathStmt.length > 0) xpathStmt += " | ";
		xpathStmt += "scp-log/rt";
	} 
	else {
	    xslParams.runtimeFilter = "false";
	}
	
	if (filterObj['sequencingFilter']){
		if (xpathStmt.length > 0) xpathStmt += " | ";
		xpathStmt += "scp-log/s";
	} 
	else  {
	    xslParams.sequencingFilter = "false";
	}
	
	if (filterObj['sequencingReadableFilter']){
		if (xpathStmt.length > 0) xpathStmt += " | ";
		xpathStmt += "scp-log/ss";
	} 
	else  {
	    xslParams.sequencingReadableFilter = "false";
	}
	
	if (filterObj['lookaheadFilter']){
		if (xpathStmt.length > 0) xpathStmt += " | ";
		xpathStmt += "scp-log/l";
	} 
	else  {
	    xslParams.lookaheadFilter = "false";
	}
		
	if (xpathStmt.length > 0){ 
	
		var nodes = this.dom.selectNodes(xpathStmt);
		for (var i = 0; i < nodes.length; i++){
			temproot.appendChild(nodes[i].cloneNode(true));
		}
	}
	
	xslParams.detailLevel = "filter";
	xslParams.scriptsURL = SCORMENGINE_SCRIPTS_URL;
	
	this.displayLog(tempdom, xslParams);
};

Log.prototype.displayRefresh = function(callingWindow, expandAll, loadAllData) {

	if (callingWindow) callingWindow.close()
	
	var xslParams = new Object();
	if (expandAll) {
		xslParams.detailLevel = "expanded";
	} else {
		xslParams.detailLevel = "collapsed";
	}
	
	if (loadAllData === true){
	    xslParams.loadAllData = "true";
	}
	
	xslParams.scriptsURL = SCORMENGINE_SCRIPTS_URL;
	
	this.displayLog(this.dom, xslParams);
};

Log.prototype.displayLog = function(sourceDom, xslParams){

	var win = window.open("", '_blank', 'toolbar=yes,location=yes,directories=yes,resizable=yes,scrollbars=yes');
    win.document.write("<h3 style='font-size: medium; font-family: Arial; color: navy' id=\"loadMessage\">Refreshing Log Information...</h3>");

    // create a DOM Document containing an XSLT stylesheet   
    // Safari doesn't support 'load' so get the dom document via an xmlhttp request
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", SCORMENGINE_SCRIPTS_URL + "/clientDebugger/DebuggerXslt.xml", false);
    xmlhttp.send('');
    var xslDoc = xmlhttp.responseXML;

    var displayProcessor = new XSLTProcessor();         
    displayProcessor.importStylesheet(xslDoc);  
    
    for(var name in xslParams) {
        displayProcessor.setParameter(null, name, xslParams[name]);
    }
    
    if (typeof LogCompression != "undefined"){
		var logCompressor = new LogCompression(window.dictionary_ll);
		var strCleanLog = logCompressor.decompressString((new XMLSerializer()).serializeToString(sourceDom));
		
		var domToDisplay = Sarissa.getDomDocument();  
        domToDisplay = (new DOMParser()).parseFromString(strCleanLog, "text/xml");  
	}
	else{
		var domToDisplay = sourceDom;
	}

	var browserReadyLog = displayProcessor.transformToDocument(domToDisplay);  
	var output = (new XMLSerializer()).serializeToString(browserReadyLog);
	
	win.document.write(output);	
	win.document.close();
}

Log.prototype.toXml = function(shouldDecompressText) {

    var thisDomXmlString = (new XMLSerializer()).serializeToString(this.dom);

    if (shouldDecompressText && typeof LogCompression != "undefined") {
	    var logCompressor = new LogCompression(window.dictionary_ll);
	    var strCleanLog = logCompressor.decompressString(thisDomXmlString);
		
	    var uncompressed = Sarissa.getDomDocument();  
        uncompressed = (new DOMParser()).parseFromString(strCleanLog, "text/xml");  
        return (new XMLSerializer()).serializeToString(uncompressed)
    } else {
        return thisDomXmlString;
    }
}

/*
	LogEntry Constructor
*/
function LogEntry(thisNode, thisLog, functionName, functionArgs) {

	this.node = thisNode;
	this.log = thisLog;
	this.dom = this.log.dom;  // so the write method can create new elements within the dom
	
	if (this.log.includeTimestamps) {
		this.timeStamp = new Date();
		this.node.setAttribute("ts", padTwoDigits(this.timeStamp.getHours()) + ":" + padTwoDigits(this.timeStamp.getMinutes()) + ":" + padTwoDigits(this.timeStamp.getSeconds()) + "." + this.timeStamp.getMilliseconds());
	}


	this.node.setAttribute("f", substituteSpecialXmlChars(functionName));
	
	if (functionArgs !== null && functionArgs !== undefined) {
		for (var i=0; i < functionArgs.length; i++) {
			this.node.setAttribute("arg" + (i+1), functionArgs[i]);
		}
	}
}

LogEntry.prototype.write = function(message) {
	var newNode = this.dom.createElement("i");
	
	if (this.log.includeTimestamps) {
		var now = new Date();
		var timestamp = padTwoDigits(now.getHours()) + ":" + padTwoDigits(now.getMinutes()) + ":" + padTwoDigits(now.getSeconds()) + "." + now.getMilliseconds();
		newNode.setAttribute("ts", timestamp);
	}
	var text = this.dom.createTextNode(substituteSpecialXmlChars(message));
	newNode.appendChild(text);
	this.node.appendChild(newNode);
};

LogEntry.prototype.error = function(message) {
	var newNode = this.dom.createElement("e");
	var text = this.dom.createTextNode(message);
	newNode.appendChild(text);
	this.node.appendChild(newNode);
};

LogEntry.prototype.startNew = function(recordType, args, data) {
	
	var logEntry;
	var node = this.dom.createElement(recordType);
			
	// If we've passed in a string first thing, "overload" this method with a signature like startNewAtRoot(title, arg1, arg2, etc.);
	if (typeof args == "string") 
	{
		logEntry = new LogEntry(node, this, args, data);
		this.node.appendChild(node);		
	} 
	else if (args !== null && args !== undefined)
	{
		var functionName = args.callee.toString().substring(9, args.callee.toString().indexOf("{")-1);
		logEntry = new LogEntry(node, this, functionName, args);
		this.node.appendChild(node);
	}
	else
	{
		logEntry = new LogEntry(node, this, "unknown entry point");
		this.node.appendChild(node);
	}
	
	return logEntry;
};

LogEntry.prototype.setAttribute = function(name, value) {
	this.node.setAttribute(name, substituteSpecialXmlChars(value.toString()));
};

LogEntry.prototype.setReturn = function(value) {

	this.node.setAttribute("r", substituteSpecialXmlChars(value.toString()));
	
	if (this.log.includeTimestamps) {
		var now = new Date();
		var elapsedTime = (now.getTime() - this.timeStamp.getTime()) / 1000;
		this.node.setAttribute("et", elapsedTime);
	}	
};

// GLOBAL Regular Expressions - "precompiled" so we don't have to recreate them
var regExLT = new RegExp("<", "g"); // g means "replace all'
var regExGT = new RegExp(">", "g"); 
var regExAMP = new RegExp("&", "g");
var regExDBLQUOTE = new RegExp("\"", "g");
var regExNONPRINT = new RegExp("([\x00-\x1f]|\x7F)", "g");

// Little test to set a boolean which tells us whether this browser supports this syntax
var doesBrowserReplaceSupportFunctionParam = 'a'.replace(/a/, function(){return '';}).length == 0;

function substituteSpecialXmlChars(str) {

	str = str.replace(regExLT, "&lt;");
    str = str.replace(regExGT, "&gt;");
    str = str.replace(regExAMP, "&amp;");
	str = str.replace(regExDBLQUOTE, "'");
	
    // Escape non-printing characters which can't be directly added to an xml DOM
    if( doesBrowserReplaceSupportFunctionParam ) {
	    str = str.replace (regExNONPRINT, XmlElement_CharacterEscape);
    } else if(regExNONPRINT.test(str)) { 
        // Directly calling EscapeCharacters is more expensive so only do it if 
        // there's a match for any of the non-printing chars we're worried about
	    str = EscapeCharacters(str);
    } 

	return str;
}  
	    
function padTwoDigits(n) {
    if (n < 10)
    {
        return "0" + n;
    }
    else
    {
        return "" + n;
    }
}