//****************************************************************************************************************


//Retry Sequencing Request Process [SB.2.10] 
	//May return a delivery request; may return an exception code
	//Reference: 
		//Activity is Active AM.1.1
		//Activity is Suspended AM.1.1
		//Current Activity AM.1.2
		//Flow Subprocess SB.2.3 

function Sequencer_RetrySequencingRequestProcess(callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Retry Sequencing Request Process [SB.2.10]", callingLog);
	
	var returnValue;
	
	var flowSubProcessSuccess;

	this.LogSeq("[SB.2.10]1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)", logParent);
	
	if ( ! this.IsCurrentActivityDefined(logParent)){
		
		this.LogSeq("[SB.2.10]1.1. Exit Retry Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.10-1) (Nothing to deliver)", logParent);
		
		returnValue = new Sequencer_RetrySequencingRequestProcessResult(null, "SB.2.10-1", IntegrationImplementation.GetString("You cannot use 'Resume All' while the current item is open."));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	var currentActivity = this.GetCurrentActivity();
	
	this.LogSeq("[SB.2.10]2. If the Activity is Active for the Current Activity is True Or the Activity is Suspended for the Current Activity is True Then (Cannot retry an activity that is still active or suspended)", logParent);
	
	if (currentActivity.IsActive() || currentActivity.IsSuspended()){
	
		this.LogSeq("[SB.2.10]2.1. Exit Retry Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.10-2) (Nothing to deliver)", logParent);
		
		returnValue = new Sequencer_RetrySequencingRequestProcessResult(null, "SB.2.10-2", IntegrationImplementation.GetString("A 'Retry' sequencing request cannot be processed while there is an active or suspended activity."));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[SB.2.10]3. If the Current Activity is not a leaf Then", logParent);
	if (! currentActivity.IsALeaf()){
		
		this.LogSeq("[SB.2.10]3.1. Apply the Flow Subprocess to the Current Activity in the Forward direction with consider children equal to True", logParent);
		flowSubProcessResult = this.FlowSubprocess(currentActivity, FLOW_DIRECTION_FORWARD, true, logParent);
		
		this.LogSeq("[SB.2.10]3.2. If the Flow Subprocess returned False Then", logParent);
		
		if (flowSubProcessResult.Deliverable === false){
			this.LogSeq("[SB.2.10]3.2.1. Exit Retry Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.10-3) (Nothing to deliver)", logParent);
			
			returnValue = new Sequencer_RetrySequencingRequestProcessResult(null, "SB.2.10-3", IntegrationImplementation.GetString("You cannot 'Retry' this item because: {1}", currentActivity.GetTitle(), flowSubProcessResult.ExceptionText));
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		else{
			this.LogSeq("[SB.2.10]3.3. Else", logParent);
			this.LogSeq("[SB.2.10]3.3.1. Exit Retry Sequencing Request Process (Delivery Request: the activity identified by the Flow Subprocess; Exception: n/a)", logParent);

			returnValue = new Sequencer_RetrySequencingRequestProcessResult(flowSubProcessResult.IdentifiedActivity, null, "");
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
	}
	else{
		this.LogSeq("[SB.2.10]4. Else", logParent);
		this.LogSeq("[SB.2.10]4.1. Exit Retry Sequencing Request Process (Delivery Request: the Current Activity; Exception: n/a)", logParent);
		
		returnValue = new Sequencer_RetrySequencingRequestProcessResult(currentActivity, null, "");
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
 
}


function Sequencer_RetrySequencingRequestProcessResult(deliveryRequest, exception, exceptionText){
	this.DeliveryRequest = deliveryRequest;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	
}

Sequencer_RetrySequencingRequestProcessResult.prototype.toString = function(){
									return "DeliveryRequest=" + this.DeliveryRequest + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText;
									};	