//****************************************************************************************************************
//Flow Activity Traversal Subprocess [SB.2.2] 
	
	//For an activity, a traversal direction, and a previous traversal direction; returns the 'next' activity in a directed traversal of the activity tree and True if the activity can be delivered; may return an exception code)
	
	//Reference: 
		//Check Activity Process UP.5
		//Flow Activity Traversal Subprocess SB.2.2
		//Flow Tree Traversal Subprocess SB.2.1
		//Sequencing Control Flow SM.1
		//Sequencing Rules Check Process UP.2 

function Sequencer_FlowActivityTraversalSubprocess(activity, traversalDirection, previousTraversalDirection, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Flow Activity Traversal Subprocess [SB.2.2](" + activity + ", " + traversalDirection + ", " + previousTraversalDirection + ")", callingLog);
	
	var sequencingRulesCheckResult;
	var flowTreeTraversalResult;
	var flowActivityTraversalResult;
	var checkActivityResult;
	
	var returnValue;
	
	var parentActivity = this.Activities.GetParentActivity(activity);
	
	this.LogSeq("[SB.2.2]1. If Sequencing Control Flow for the parent of the activity is False Then (Confirm that 'flow' is enabled)", logParent);

	if (parentActivity.GetSequencingControlFlow() === false){
		this.LogSeq("[SB.2.2]1.1. Exit Flow Activity Traversal Subprocess (Deliverable: False; Next Activity: the activity; Exception: SB.2.2-1)", logParent);
		returnValue = new Sequencer_FlowActivityTraversalSubprocessReturnObject(false, activity, "SB.2.2-1", IntegrationImplementation.GetString("Please select a menu item to continue with {0}.", parentActivity.GetTitle()), false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[SB.2.2]2. Apply the Sequencing Rules Check Process to the activity and its Skipped sequencing rules", logParent);
	sequencingRulesCheckResult = this.SequencingRulesCheckProcess(activity, RULE_SET_SKIPPED, logParent);
	
	this.LogSeq("[SB.2.2]3. If the Sequencing Rules Check Process does not return Nil Then (Activity is skipped, try to go to the 'next' activity)", logParent);
	if (sequencingRulesCheckResult !== null){
	
		this.LogSeq("[SB.2.2]3.1. Apply the Flow Tree Traversal Subprocess to the activity in the traversal direction and the previous traversal direction with consider children equal to False", logParent);
		flowTreeTraversalResult = this.FlowTreeTraversalSubprocess(activity, traversalDirection, previousTraversalDirection, false, logParent);
		
		this.LogSeq("[SB.2.2]3.2. If the Flow Tree Traversal Subprocess does not identify an activity Then", logParent);
		if (flowTreeTraversalResult.NextActivity === null){
			
			this.LogSeq("[SB.2.2]3.2.1. Exit Flow Activity Traversal Subprocess (Deliverable: False; Next Activity: the activity; Exception: exception identified by the Flow Tree Traversal Subprocess)", logParent);
			returnValue = new Sequencer_FlowActivityTraversalSubprocessReturnObject(false, activity, flowTreeTraversalResult.Exception, flowTreeTraversalResult.ExceptionText, flowTreeTraversalResult.EndSequencingSession);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		else{
			this.LogSeq("[SB.2.2]3.3. Else", logParent);
			
			this.LogSeq("[SB.2.2]3.3.1. If the previous traversal direction is Backward And the Traversal Direction returned by the Flow Tree Traversal Subprocess is Backward Then (Make sure the recursive call considers the correct direction)", logParent);
			
			if (previousTraversalDirection == FLOW_DIRECTION_BACKWARD &&
			    flowTreeTraversalResult.TraversalDirection == FLOW_DIRECTION_BACKWARD){
				
				this.LogSeq("[SB.2.2]3.3.1.1. Apply the Flow Activity Traversal Subprocess to the activity identified by the Flow Tree Traversal Subprocess in the traversal direction and a previous traversal direction of n/a (Recursive call � make sure the 'next' activity is OK)", logParent);
				
				//The pseudo code strictly says to use "traversalDirection" here, however the newsgroup posts (search on "cm-3b") say this needs to be the traversal
				//direction returned by the last call to FlowTreeTraversalSubprocess. Need to make the newsgroup change to make it work. Confirmed with Angelo.
				
				//flowActivityTraversalResult = this.FlowActivityTraversalSubprocess(flowTreeTraversalResult.NextActivity, traversalDirection, null);
				flowActivityTraversalResult = this.FlowActivityTraversalSubprocess(flowTreeTraversalResult.NextActivity, flowTreeTraversalResult.TraversalDirection, null, logParent);
			}
			else{
				this.LogSeq("[SB.2.2]3.3.2. Else", logParent);
				
				this.LogSeq("[SB.2.2]3.3.2.1. Apply the Flow Activity Traversal Subprocess to the activity identified by the Flow Tree Traversal Subprocess in the traversal direction and a previous traversal direction of previous traversal direction (Recursive call � make sure the 'next' activity is OK)", logParent);
				
				flowActivityTraversalResult = this.FlowActivityTraversalSubprocess(flowTreeTraversalResult.NextActivity, traversalDirection, previousTraversalDirection, logParent);
			}
			
			this.LogSeq("[SB.2.2]3.3.3. Exit Flow Activity Traversal Subprocess -(Return the results of the recursive Flow Activity Traversal Subprocess) Possible exit from recursion", logParent);
			returnValue = flowActivityTraversalResult;
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
	}
	
	this.LogSeq("[SB.2.2]4. Apply the Check Activity Process to the activity (Make sure the activity is allowed)", logParent);
	checkActivityResult = this.CheckActivityProcess(activity, logParent);
	
	this.LogSeq("[SB.2.2]5. If the Check Activity Process returns True Then", logParent);
	
	if (checkActivityResult === true){
		
		this.LogSeq("[SB.2.2]5.1. Exit Flow Activity Traversal Subprocess (Deliverable: False; Next Activity: the activity; Exception: SB.2.2-2)", logParent);
		
		returnValue = new Sequencer_FlowActivityTraversalSubprocessReturnObject(false, activity, "SB.2.2-2", IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.", activity.GetTitle()), false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[SB.2.2]6. If the activity is not a leaf node in the activity tree Then (Cannot deliver a non-leaf activity; enter the cluster looking for a leaf)", logParent);
	if (activity.IsALeaf() === false){
		
		this.LogSeq("[SB.2.2]6.1. Apply the Flow Tree Traversal Subprocess to the activity in the traversal direction and a previous traversal direction of n/a with consider children equal to True", logParent);
		flowTreeTraversalResult = this.FlowTreeTraversalSubprocess(activity, traversalDirection, null, true, logParent);
		
		this.LogSeq("[SB.2.2]6.2. If the Flow Tree Traversal Subprocess does not identify an activity Then", logParent);

		if (flowTreeTraversalResult.NextActivity === null){

			this.LogSeq("[SB.2.2]6.2.1. Exit Flow Activity Traversal Subprocess (Deliverable: False; Next Activity: the activity; Exception: exception identified by the Flow Tree Traversal Subprocess)", logParent);
			
			returnValue = new Sequencer_FlowActivityTraversalSubprocessReturnObject(false, activity, flowTreeTraversalResult.Exception, flowTreeTraversalResult.ExceptionText, flowTreeTraversalResult.EndSequencingSession);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		else{
			this.LogSeq("[SB.2.2]6.3. Else", logParent);
			
			this.LogSeq("[SB.2.2]6.3.1. If the traversal direction is Backward And the traversal direction returned by the Flow Tree Traversal Subprocess is Forward Then (Check if we are flowing backward through a forward only cluster - must move forward instead)", logParent);
			
			if (traversalDirection == FLOW_DIRECTION_BACKWARD &&
				flowTreeTraversalResult.TraversalDirection == FLOW_DIRECTION_FORWARD){
				
				this.LogSeq("[SB.2.2]6.3.1.1. Apply the Flow Activity Traversal Subprocess to the activity identified by the Flow Tree Traversal Subprocess in the Forward direction with the previous traversal direction of Backward (Recursive call � Make sure the identified activity is OK)", logParent);
				
				flowActivityTraversalResult = this.FlowActivityTraversalSubprocess(flowTreeTraversalResult.NextActivity, FLOW_DIRECTION_FORWARD, FLOW_DIRECTION_BACKWARD, logParent);
			}
			else{
				this.LogSeq("[SB.2.2]6.3.2. Else", logParent);
				
				this.LogSeq("[SB.2.2]6.3.2.1. Apply the Flow Activity Traversal Subprocess to the activity identified by the Flow Tree Traversal Subprocess in the traversal direction and a previous traversal direction of n/a (Recursive call � Make sure the identified activity is OK)", logParent);
				flowActivityTraversalResult = this.FlowActivityTraversalSubprocess(flowTreeTraversalResult.NextActivity, traversalDirection, null, logParent);
			}
			
			this.LogSeq("[SB.2.2]6.3.3. Exit Flow Activity Traversal Subprocess - (Return the results of the recursive Flow Activity Traversal Subprocess) Possible exit from recursion", logParent);
			returnValue = flowActivityTraversalResult;
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
	}
	
	this.LogSeq("[SB.2.2]7. Exit Flow Activity Traversal Subprocess (Deliverable: True; Next Activity: the activity; Exception: n/a ) Found a leaf", logParent);
	returnValue = new Sequencer_FlowActivityTraversalSubprocessReturnObject(true, activity, null, "", false);
	this.LogSeqReturn(returnValue, logParent);
	return returnValue;

}

function Sequencer_FlowActivityTraversalSubprocessReturnObject(deliverable, nextActivity, exception, exceptionText, endSequencingSession){
	
	Debug.AssertError("Invalid endSequencingSession (" + endSequencingSession + ") passed to FlowActivityTraversalSubprocessReturnObject.", (endSequencingSession != true && endSequencingSession != false))
	
	this.Deliverable = deliverable;
	this.NextActivity = nextActivity;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	this.EndSequencingSession = endSequencingSession;

}

Sequencer_FlowActivityTraversalSubprocessReturnObject.prototype.toString =  function(){
									return "Deliverable=" + this.Deliverable + 
										", NextActivity=" + this.NextActivity + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText +
										", EndSequencingSession=" + this.EndSequencingSession;
									};