//****************************************************************************************************************
//Termination Request Process  

//	For a termination request, ends the current attempt on the Current Activity; returns the validity of the termination request; may return a sequencing request; may return an exception code

//	Reference: 
		//Activity is Active AM.1.1
		//Activity is Suspended AM.1.1
		//Current Activity AM.1.2
		//End Attempt Process UP.4
		//Sequencing Exit Action Rules Subprocess TB.2.1
		//Sequencing Post Condition Rules Subprocess TB.2.2
		//Terminate Descendent Attempts Process UP.3 


function Sequencer_TerminationRequestProcess(terminationRequest, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Termination Request Process [TB.2.3](" + terminationRequest + ")", callingLog);
	
	var returnValue;
	
	var currentActivity = this.GetCurrentActivity();
	var parentActivity = this.Activities.GetParentActivity(currentActivity);
	var rootActivity = this.GetRootActivity(logParent);
	var activityTree;
	
	var sequencingPostConditionResult = null;
	var fallThroughToNextCase = false;

	this.LogSeq("[TB.2.3]1. If the Current Activity is Not Defined Then (If the sequencing session has not begun, there is nothing to terminate)", logParent);
	if (!this.IsCurrentActivityDefined(logParent)){
		
		this.LogSeq("[TB.2.3]1.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-1)", logParent);

		returnValue = new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID, null, "TB.2.3-1", IntegrationImplementation.GetString("You cannot use 'Terminate' because no item is currently open."));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[TB.2.3]2. If (the termination request is Exit Or Abandon) And Activity is Active for the Current Activity is False Then (If the current activity has already been terminated, there is nothing to terminate)", logParent);
	if ( (terminationRequest == TERMINATION_REQUEST_EXIT || terminationRequest == TERMINATION_REQUEST_ABANDON) && 
		 (currentActivity.IsActive() === false) ){
	
		this.LogSeq("[TB.2.3]2.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-2)", logParent);
		
		returnValue = new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID, null, "TB.2.3-2", IntegrationImplementation.GetString("The current activity has already been terminated."));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	switch (terminationRequest){
		
		case TERMINATION_REQUEST_EXIT:
			
			this.LogSeq("[TB.2.3]3. Case: termination request is Exit", logParent);
			
			this.LogSeq("[TB.2.3]3.1. Apply the End Attempt Process to the Current Activity  (Ensure the state of the current activity is up to date)", logParent);
			this.EndAttemptProcess(currentActivity, false, logParent);
			
			this.LogSeq("[TB.2.3]3.2. Apply the Sequencing Exit Action Rules Subprocess to the Current Activity (Check if any of the current activity's ancestors need to terminate)", logParent);
			this.SequencingExitActionRulesSubprocess(logParent);
			
			this.LogSeq("[TB.2.3]3.3. Repeat", logParent);
			var processedExit;
			do {
				this.LogSeq("[TB.2.3]3.3.1. Set the processed exit to False", logParent);
				processedExit = false;
				
				this.LogSeq("[TB.2.3]3.3.2. Apply the Sequencing Post Condition Rules Subprocess to the Current Activity (" + this.GetCurrentActivity() + ")", logParent);
				sequencingPostConditionResult = this.SequencingPostConditionRulesSubprocess(logParent);
				
				this.LogSeq("[TB.2.3]3.3.3. If the Sequencing Post Condition Rule Subprocess returned a termination request of Exit All Then", logParent);
				
				if (sequencingPostConditionResult.TerminationRequest == TERMINATION_REQUEST_EXIT_ALL){
					
					this.LogSeq("[TB.2.3]3.3.3.1. Change the termination request to Exit All", logParent);
					terminationRequest = TERMINATION_REQUEST_EXIT_ALL;
					
					this.LogSeq("[TB.2.3]3.3.3.2. Break to the next Case (Process an Exit All Termination Request)", logParent);
					fallThroughToNextCase = true;	//indicate that we don't want to break at the end of this case
					break;							//exit the do loop
				} 
				
				this.LogSeq("[TB.2.3]3.3.4. If the Sequencing Post Condition Rule Subprocess returned a termination request of Exit Parent Then (If we exit the parent of the current activity, move the current activity to the parent of the current activity.)", logParent);
				
				if (sequencingPostConditionResult.TerminationRequest == TERMINATION_REQUEST_EXIT_PARENT){
					
					this.LogSeq("[TB.2.3]3.3.4.1. If the Current Activity is Not the root of the activity tree Then (The root of the activity tree does not have a parent to exit)", logParent);

					if (this.GetCurrentActivity() !== null && this.GetCurrentActivity().IsTheRoot() === false){
						
						this.LogSeq("[TB.2.3]3.3.4.1.1. Set the Current Activity to the parent of the Current Activity", logParent);
						this.SetCurrentActivity(this.Activities.GetParentActivity(this.GetCurrentActivity()), logParent);
						
						this.LogSeq("[TB.2.3]3.3.4.1.2. Apply the End Attempt Process to the Current Activity", logParent);
						this.EndAttemptProcess(this.GetCurrentActivity(), false, logParent);
						
						this.LogSeq("[TB.2.3]3.3.4.1.3. Set processed exit to True (Need to evaluate post conditions on the new current activity)", logParent);
						processedExit = true;
					}
					else{
						this.LogSeq("[TB.2.3]3.3.4.2. Else", logParent);
						this.LogSeq("[TB.2.3]3.3.4.2.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-4)", logParent);
						
						returnValue = new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID, null, "TB.2.3-4", IntegrationImplementation.GetString("An 'Exit Parent' sequencing request cannot be processed on the root of the activity tree."));						
						this.LogSeqReturn(returnValue, logParent);
						return returnValue;
					}
				}
				else{
					//this code added per Addendum 2.22 (replaces interm fix below)
					this.LogSeq("[TB.2.3]3.3.5. Else", logParent);
					this.LogSeq("[TB.2.3]3.3.5.1. If the Current Activity is the Root of the Activity Tree And the sequencing request returned by the Sequencing Post Condition Rule Subprocess is Not Retry Then If the attempt on the root of the Activity Tree is ending without a Retry, the Sequencing Session also ends", logParent);
					
					if (this.GetCurrentActivity() !== null && this.GetCurrentActivity().IsTheRoot() === true &&
						sequencingPostConditionResult.SequencingRequest != SEQUENCING_REQUEST_RETRY){
						
						this.LogSeq("[TB.2.3]3.3.5.1.1. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: Exit; Exception: n/a", logParent);
						returnValue = new Sequencer_TerminationRequestProcessResult(terminationRequest, SEQUENCING_REQUEST_EXIT, null, "");
						this.LogSeqReturn(returnValue, logParent);
						return returnValue;
					}
				}
				
				/*
				//lines 3.4 & 3.4.1 are added after a conversation with Angelo on 1-28-05, these lines will be added to the normalized
				//pseudo code in an upcoming addendum (added above now that addendum is out)
				this.LogSeq("TB.2.3]3.4. If the Current Activity is the Root of the Activity Tree AND the sequencing request returned by the Sequencing Post Condition Rule Subprocess is not Retry Then", logParent);
				if (this.GetCurrentActivity() !== null && this.GetCurrentActivity().IsTheRoot() === true &&
					sequencingPostConditionResult.SequencingRequest != SEQUENCING_REQUEST_RETRY){
					
					this.LogSeq("TB.2.3]3.4.1. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: Exit; Exception n/a)", logParent);
					return new Sequencer_TerminationRequestProcessResult(terminationRequest, SEQUENCING_REQUEST_EXIT, null, "");
				}
				*/
				this.LogSeq("TB.2.3]3.5. Until processed exit is False (processed exit=" + processedExit + ")", logParent);
			}
			while (processedExit !== false);

		if (! fallThroughToNextCase){	//if Sequencing Post Condition Rule Subprocess returned a termination request of Exit All, we fall through to the Exit All section
			
			this.LogSeq("[TB.2.3]3.6. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: is the sequencing request returned by the Sequencing Post Condition Rule Subprocess, if one exists, otherwise n/a; Exception: n/a)", logParent);
			
			//TODO: restructure this function such that there isn't a break immediately after a return so JSLint will be happy
			returnValue = new Sequencer_TerminationRequestProcessResult(terminationRequest, sequencingPostConditionResult.SequencingRequest, null, "");
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
			break;
		}
		
		case TERMINATION_REQUEST_EXIT_ALL:
			
			this.LogSeq("[TB.2.3]4. Case: termination request is Exit All", logParent);
			
			this.LogSeq("[TB.2.3]4.1.If Activity is Active for the Current Activity is True Then (Has the completion subprocess and rollup been applied to the current activity yet?)", logParent);
			
			if (currentActivity.IsActive()){
				
				this.LogSeq("[TB.2.3]4.1.1. Apply the End Attempt Process to the Current Activity", logParent);
				this.EndAttemptProcess(currentActivity, false, logParent);
				
			}
			
			this.LogSeq("[TB.2.3]4.2. Apply the Terminate Descendent Attempts Process to the root of the activity tree", logParent);
			this.TerminateDescendentAttemptsProcess(rootActivity, logParent);
			
			this.LogSeq("[TB.2.3]4.3. Apply the End Attempt Process to the root of the activity tree", logParent);
			this.EndAttemptProcess(rootActivity, false, logParent);
			
			this.LogSeq("[TB.2.3]4.4. Set the Current Activity to the root of the activity tree (Move the current activity to the root of the activity tree)", logParent);
			this.SetCurrentActivity(rootActivity, logParent);
			
			this.LogSeq("[TB.2.3]4.5. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: is the sequencing request returned by the Sequencing Post Condition Rule Subprocess, if one exists, otherwise an Exit sequencing request; Exception: n/a) Inform the sequencer that the sequencing session has ended", logParent);
			
			if (sequencingPostConditionResult !== null && sequencingPostConditionResult.SequencingRequest !== null){
				
				this.LogSeq("[TB.2.3]4.5.1 Sequencing Request: is the sequencing request returned by the Sequencing Post Condition Rule Subprocess", logParent);
				returnValue = new Sequencer_TerminationRequestProcessResult(terminationRequest, sequencingPostConditionResult.SequencingRequest, null, "");
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[TB.2.3]4.5.2 Otherwise an Exit sequencing request", logParent);
				returnValue = new Sequencer_TerminationRequestProcessResult(terminationRequest, SEQUENCING_REQUEST_EXIT, null, "");
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			
		break; 
		
		case TERMINATION_REQUEST_SUSPEND_ALL:
			
			this.LogSeq("[TB.2.3]5. Case: termination request is Suspend All", logParent);
			
			this.LogSeq("[TB.2.3]5.1. If (the Activity is Active for the Current Activity is True) Or (the Activity is Suspended for the Current Activity is True) Then (If the current activity is active or already suspended, suspend it and all of its descendents)", logParent);
			
			// Modification to perform rollup even on suspend when exiting the player [Ontime feature #211].
			// When the learner clicks Return To LMS, a suspend all request is processed to ensure that progress isn't lost. Problem is
			// that suspend all doesn't trigger rollup, so if the learner clicks Return To LMS while sitting on the last SCO, it is possible
			// that the course will not complete because it is not rolled up even though all of the requirements have been met.
			if ( (Control.Package.Properties.InvokeRollupAtSuspendAll === true || this.ReturnToLmsInvoked) && 
			      currentActivity.IsActive()
			      ){
				this.LogSeq("[TB.2.3]5.1.0.1. Apply the End Attempt Process to the Current Activity upon Return To LMS (Not in p-code)", logParent);
				this.EndAttemptProcess(currentActivity, false, logParent, true);
			}
			
			if (currentActivity.IsActive() || currentActivity.IsSuspended()){
				
				this.LogSeq("[TB.2.3]5.1.1. Apply the Overall Rollup Process", logParent);
				this.OverallRollupProcess(currentActivity, logParent);
				
				this.LogSeq("[TB.2.3]5.1.2. Set the Suspended Activity to the Current Activity", logParent);
				this.SetSuspendedActivity(currentActivity);
				
			}
			else{
				this.LogSeq("[TB.2.3]5.2. Else", logParent);
				this.LogSeq("[TB.2.3]5.2.1. If the Current Activity is not the root of the activity tree Then (Make sure the current activity is not the root of the activity tree)", logParent);
				
				if (! currentActivity.IsTheRoot()){
					this.LogSeq("[TB.2.3]5.2.1.1. Set the Suspended Activity to the parent of the Current Activity", logParent);
					this.SetSuspendedActivity(parentActivity);
				}
				else{
					this.LogSeq("[TB.2.3]5.2.2. Else", logParent);
					this.LogSeq("[TB.2.3]5.2.2.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-3) (Nothing to suspend)", logParent);

					Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID, null, "TB.2.3-3", "The suspend all termination request failed because there is no activity to suspend")
				}
			}
			
			this.LogSeq("[TB.2.3]5.3. Form the activity path as the ordered series of all activities from the Suspended Activity to the root of the activity tree, inclusive", logParent);
			activityTree = this.GetActivityPath(this.GetSuspendedActivity(logParent), true);
			
			this.LogSeq("[TB.2.3]5.4. If the activity path is Empty Then", logParent);
			if (activityTree.length === 0){
				
				this.LogSeq("[TB.2.3]5.4.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-5) (Nothing to suspend)", logParent);
				
				returnValue = new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID, null, "TB.2.3-5", IntegrationImplementation.GetString("Nothing to suspend"));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			
			this.LogSeq("[TB.2.3]5.5. For each activity in the activity path", logParent);
			for (var i=0; i < activityTree.length; i++){
				
				this.LogSeq("[TB.2.3]5.5.1. Set Activity is Active for the activity to False (Activity Identifier=" + activityTree[i].GetItemIdentifier() + ")", logParent);
				activityTree[i].SetActive(false);
				
				this.LogSeq("[TB.2.3]5.5.2. Set Activity is Suspended for the activity to True", logParent);
				activityTree[i].SetSuspended(true);
			}
			
			this.LogSeq("[TB.2.3]5.6. Set the Current Activity to the root of the activity tree (Move the current activity to the root of the activity tree)", logParent);
			this.SetCurrentActivity(rootActivity, logParent);
			
			this.LogSeq("[TB.2.3]5.7. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: Exit; Exception: n/a) Inform the sequencer that the sequencing session has ended", logParent);
			
			returnValue = new Sequencer_TerminationRequestProcessResult(terminationRequest, SEQUENCING_REQUEST_EXIT, null, "");
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		break; 
	
		case TERMINATION_REQUEST_ABANDON:
			
			this.LogSeq("[TB.2.3]6. Case: termination request is Abandon", logParent);
			
			this.LogSeq("[TB.2.3]6.1. Set Activity is Active for the Current Activity to False", logParent);
			currentActivity.SetActive(false);
			
			this.LogSeq("[TB.2.3]6.2. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: n/a; Exception: n/a)", logParent);
			
			returnValue = new Sequencer_TerminationRequestProcessResult(terminationRequest, null, null, "");
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		break;
		
		case TERMINATION_REQUEST_ABANDON_ALL:
		
			this.LogSeq("[TB.2.3]7. Case: termination request is Abandon All", logParent);
			
			this.LogSeq("[TB.2.3]7.1. Form the activity path as the ordered series of all activities from the Current Activity to the root of the activity tree, inclusive", logParent);
			activityTree = this.GetActivityPath(currentActivity, true);
			
			this.LogSeq("[TB.2.3]7.2. If the activity path is Empty Then", logParent);
			if (activityTree.length === 0){
				this.LogSeq("[TB.2.3]7.2.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-6) Nothing to abandon", logParent);
				
				returnValue = new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID, null, "TB.2.3-6", IntegrationImplementation.GetString("Nothing to close"));	
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			
			this.LogSeq("[TB.2.3]7.3. For each activity in the activity path", logParent);
			
			for (var i=0; i < activityTree.length; i++){
				
				this.LogSeq("[TB.2.3]7.3.1. Set Activity is Active for the activity to False", logParent);
				activityTree[i].SetActive(false);
				
			}
			
			
			this.LogSeq("[TB.2.3]7.4. Set the Current Activity to the root of the activity tree Move the current activity to the root of the activity tree", logParent);
			this.SetCurrentActivity(rootActivity, logParent);
			
			this.LogSeq("[TB.2.3]7.5.Exit Termination Request Process (Termination Request: Valid; Sequencing Request: Exit; Exception: n/a) Inform the sequencer that the sequencing session has ended", logParent);
	
			returnValue = new Sequencer_TerminationRequestProcessResult(terminationRequest, SEQUENCING_REQUEST_EXIT, null, "");
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		break; 
	
		default:
		
			this.LogSeq("[TB.2.3]8. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-7) Undefined termination request", logParent);
					
			returnValue = new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID, null, "TB.2.3-7", IntegrationImplementation.GetString("The 'Termination' request {0} is not recognized.", terminationRequest));		
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		break;
	}

}


function Sequencer_TerminationRequestProcessResult(terminationRequest, sequencingRequest, exception, exceptionText){
	
	this.TerminationRequest = terminationRequest;
	this.SequencingRequest = sequencingRequest;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
}

Sequencer_TerminationRequestProcessResult.prototype.toString = function(){
									return "TerminationRequest=" + this.TerminationRequest + 
										", SequencingRequest=" + this.SequencingRequest +
										", Exception=" + this.Exception +
										", ExceptionText=" + this.ExceptionText;
									};