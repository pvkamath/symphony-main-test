//****************************************************************************************************************

//Choice Activity Traversal Subprocess [SB.2.4]
	//For an activity and a traversal direction; returns True if the activity can be reached; may return an exception code
	//Reference: 
		//Check Activity Process UP.5; 
		//Sequencing Control Forward Only SM.1; 
		//Sequencing Rules Check Process UP.2 

function Sequencer_ChoiceActivityTraversalSubprocess(activity, flowDirection, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Choice Activity Traversal Subprocess [SB.2.4](" + activity + ", " + flowDirection + ")", callingLog);
	
	var sequencingRulesCheckResult = null;
	var parentActivity = null;
	var returnValue;
	
	this.LogSeq("[SB.2.4]1. If the traversal direction is Forward Then", logParent);
	
	if (flowDirection == FLOW_DIRECTION_FORWARD){
		
		this.LogSeq("[SB.2.4]1.1. Apply the Sequencing Rules Check Process to the activity and the Stop Forward Traversal sequencing rules", logParent);
		
		sequencingRulesCheckResult = this.SequencingRulesCheckProcess(activity, RULE_SET_STOP_FORWARD_TRAVERSAL, logParent);
		
		this.LogSeq("[SB.2.4]1.2. If the Sequencing Rules Check Process does not return Nil Then", logParent);
		
		if (sequencingRulesCheckResult !== null){
			this.LogSeq("[SB.2.4]1.2.1. Exit Choice Activity Traversal Subprocess (Reachable: False; Exception: SB.2.4-1)", logParent);
			returnValue = new Sequencer_ChoiceActivityTraversalSubprocessResult(false, "SB.2.4-1", IntegrationImplementation.GetString("You are not allowed to move into {0} yet.", activity.GetTitle()));			
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[SB.2.4]1.3. Exit Choice Activity Traversal Subprocess (Reachable: True; Exception: n/a )", logParent);
		returnValue = new Sequencer_ChoiceActivityTraversalSubprocessResult(true, null);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[SB.2.4]2. If the traversal direction is Backward Then", logParent);
	if (flowDirection == FLOW_DIRECTION_BACKWARD){
		
		this.LogSeq("[SB.2.4]2.1. If the activity has a parent Then", logParent);
		if (! activity.IsTheRoot()){
			
			parentActivity = this.Activities.GetParentActivity(activity);
			
			this.LogSeq("[SB.2.4]2.1.1. If Sequencing Control Forward Only for the parent of the activity is True Then", logParent);
			
			if (parentActivity.GetSequencingControlForwardOnly()){
				this.LogSeq("[SB.2.4]2.1.1.1. Exit Choice Activity Traversal Subprocess (Reachable: False; Exception: SB.2.4-2)", logParent);
				returnValue = new Sequencer_ChoiceActivityTraversalSubprocessResult(false, "SB.2.4-2", IntegrationImplementation.GetString("You must start {0} at the beginning.", parentActivity.GetTitle()));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		}
		else{
			this.LogSeq("[SB.2.4]2.1.2. Else", logParent);
			this.LogSeq("[SB.2.4]2.1.2.1. Exit Choice Activity Traversal Subprocess (Reachable: False; Exception: SB.2.4-3) (Cannot walk backward from the root of the activity tree)", logParent);
			
			returnValue = new Sequencer_ChoiceActivityTraversalSubprocessResult(false, "SB.2.4-3", IntegrationImplementation.GetString("You have reached the beginning of the course."));
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[SB.2.4]2.2. Exit Choice Activity Traversal Subprocess (Reachable: True; Exception: n/a )", logParent);
		returnValue = new Sequencer_ChoiceActivityTraversalSubprocessResult(true, null);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}

}

function Sequencer_ChoiceActivityTraversalSubprocessResult(reachable, exception, exceptionText){
	this.Reachable = reachable;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	
}

Sequencer_ChoiceActivityTraversalSubprocessResult.prototype.toString = function(){
								return "Reachable=" + this.Reachable + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText;
								  };