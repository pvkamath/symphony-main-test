//****************************************************************************************************************

//Choice Flow Subprocess [SB.2.9.1] 
	//For an activity and a traversal direction; indicates at what activity the flow stopped
	//Reference: 
		//Choice Flow Tree Traversal Subprocess SB.2.9.2 


function Sequencer_ChoiceFlowSubprocess(activity, flowDirection, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Choice Flow Subprocess [SB.2.9.1](" + activity + ", " + flowDirection + ")", callingLog);
	
	this.LogSeq("[SB.2.9.1]1. Apply the Choice Flow Tree Traversal Subprocess to the activity in the traversal direction (Attempt to move away from the activity, 'one' activity in the specified direction)", logParent);
	var choiceFlowTreeTraversalResult = this.ChoiceFlowTreeTraversalSubprocess(activity, flowDirection, logParent);
	
	this.LogSeq("[SB.2.9.1]2. If the Choice Flow Tree Traversal Subprocess returned Nil Then", logParent);
	if (choiceFlowTreeTraversalResult === null){
		
		this.LogSeq("[SB.2.9.1]2.1. Exit Choice Flow Subprocess (Identified Activity the activity)", logParent);
		this.LogSeqReturn(activity, logParent);
		return activity;
	}
	else{
		this.LogSeq("[SB.2.9.1]3. Else", logParent);
		
		this.LogSeq("[SB.2.9.1]3.1. Exit Choice Flow Subprocess (Identified Activity the activity identified by the Choice Flow Tree Traversal Subprocess)", logParent);
		this.LogSeqReturn(choiceFlowTreeTraversalResult, logParent);
		return choiceFlowTreeTraversalResult;
	}

}
