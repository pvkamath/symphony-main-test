//****************************************************************************************************************

//Objective Rollup Using Rules Process [RB.1.2 b] 
//	For an activity; may change the Objective Information for the activity

//	Reference: 
		//Objective Contributes to Rollup SM.6
		//Objective Description SM.6
		//Objective Progress Status TM.1.1
		//Objective Satisfied Status TM.1.1
		//Rollup Rule Check Subprocess RB.1.4
		//Rollup Action SM.5 
		
function Sequencer_ObjectiveRollupUsingRulesProcess(activity, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Objective Rollup Using Rules Process [RB.1.2 b](" + activity + ")", callingLog);
	
	var subprocessResult;

	this.LogSeq("[RB.1.2 b]1.Set the target objective to Undefined", logParent);
	var targetObjective = null;
	
	/*
	this.LogSeq("[RB.1.2 b]2. For each objective associated with the activity", logParent);
	var objectives = activity.GetObjectives();
	
	for (var i=0; i < objectives.length; i++){
	
		this.LogSeq("[RB.1.2 b]2.1. If Objective Contributes to Rollup for the objective is True Then (Identify the objective that may be altered based on the activity's children's rolled up status)", logParent);
		if (objectives[i].GetContributesToRollup() === true){
			
			this.LogSeq("[RB.1.2 b]2.1.1. Set the target objective to the objective", logParent);
			targetObjective = objectives[i];
			
			this.LogSeq("[RB.1.2 b]2.1.2. Break For", logParent);
			break;
		}
	}
	*/
	
	this.LogSeq("[RB.1.2 b]2. Get the primary objective (For each objective associated with the activity)", logParent);
	var targetObjective = activity.GetPrimaryObjective();
	
	this.LogSeq("[RB.1.2 b]3. If target objective is Defined Then", logParent);
	if (targetObjective !== null){
	
		this.LogSeq("[RB.1.2 b]3.1. Apply the Rollup Rule Check Subprocess to the activity and the Not Satisfied rollup action Process all Not Satisfied rules first", logParent);
		subprocessResult = this.RollupRuleCheckSubprocess(activity, RULE_SET_NOT_SATISFIED, logParent);
		
		this.LogSeq("[RB.1.2 b]3.2. If the Rollup Rule Check Subprocess returned True Then", logParent);
		if (subprocessResult === true){
			
			this.LogSeq("[RB.1.2 b]3.2.1. Set the Objective Progress Status for the target objective to True", logParent);
			targetObjective.SetProgressStatus(true, false, activity);
			
			this.LogSeq("[RB.1.2 b]3.2.2. Set the Objective Satisfied Status for the target objective to False", logParent);
			targetObjective.SetSatisfiedStatus(false, false, activity);
		} 
		
		this.LogSeq("[RB.1.2 b]3.3. Apply the Rollup Rule Check Subprocess to the activity and the Satisfied rollup action Process all Satisfied rules last", logParent);
		subprocessResult = this.RollupRuleCheckSubprocess(activity, RULE_SET_SATISFIED, logParent);
		
		this.LogSeq("[RB.1.2 b]3.4. If the Rollup Rule Check Subprocess returned True Then", logParent);
		if (subprocessResult === true){
		
			this.LogSeq("[RB.1.2 b]3.4.1. Set the Objective Progress Status for the target objective to True", logParent);
			targetObjective.SetProgressStatus(true, false, activity);
			
			this.LogSeq("[RB.1.2 b]3.4.2. Set the Objective Satisfied Status for the target objective to True", logParent);
			targetObjective.SetSatisfiedStatus(true, false, activity);
		}
		
		this.LogSeq("[RB.1.2 b]3.5. Exit Objective Rollup Using Rules Process", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	else{
		this.LogSeq("[RB.1.2 b]4. Else", logParent);
		this.LogSeq("[RB.1.2 b]4.1. Exit Objective Rollup Using Rules Process No objective contributes to rollup, so we cannot set anything", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}

}

