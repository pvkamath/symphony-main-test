//****************************************************************************************************************
//Select Children Process [SR.1]

//	For an activity; may change the Available Children for the activity

//	Reference: 
	//Activity is Active AM.1.1
	//Activity is Suspended AM.1.1
	//Available Children AM.1.1
	//Activity Progress Status TM.1.2.1
	//Selection Count SM.9
	//Selection Count Status SM.9
	//Selection Timing SM.9 

function Sequencer_SelectChildrenProcess(activity, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Select Children Process [SR.1](" + activity + ")", callingLog);

	this.LogSeq("[SR.1]1. If the activity does not have children Then (Cannot apply selection to a leaf activity)", logParent);
	if (activity.IsALeaf()){

		this.LogSeq("[SR.1]1.1. Exit Select Children Process", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	
	this.LogSeq("[SR.1]2. If Activity is Suspended for the activity is True Or the Activity is Active for the activity is True Then (Cannot apply selection to a suspended or active activity)", logParent);
	if (activity.IsActive() === true || activity.IsSuspended() === true){
		
		this.LogSeq("[SR.1]2.1. Exit Select Children Process", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	
	var selectionTiming = activity.GetSelectionTiming();
	
	switch (selectionTiming){
		
		case TIMING_NEVER:
			this.LogSeq("[SR.1]3. Case: the Selection Timing for the activity is Never", logParent);
			this.LogSeq("[SR.1]3.1. Exit Select Children Process", logParent);
		break;
		
		case TIMING_ONCE:
			
			this.LogSeq("[SR.1]4. Case: the Selection Timing for the activity is Once", logParent);
			
			this.LogSeq("[SR.1]4.05. If children have not been selected for this activity yet", logParent);
			if (activity.GetSelectedChildren() === false){
			
				this.LogSeq("[SR.1]4.1. If the Activity Progress Status for the activity is False Then (If the activity has not been attempted yet)", logParent);
				if (activity.GetActivityProgressStatus() === false){
				
					this.LogSeq("[SR.1]4.1.1. If the Selection Count Status for the activity is True Then", logParent);
					if (activity.GetSelectionCountStatus() === true){
					
						this.LogSeq("[SR.1]4.1.1.1. Initialize child list as an Empty ordered list", logParent);
						var childList = new Array();
						
						var activityChildren = activity.GetChildren();
						
						var selectionCount = activity.GetSelectionCount();
						
						if (selectionCount < activityChildren.length){
							
							var randomNumbers = Sequencer_GetUniqueRandomNumbersBetweenTwoValues(selectionCount, 0, activityChildren.length - 1);
							
							this.LogSeq("[SR.1]4.1.1.2. Iterate Selection Count, for the activity, times", logParent);
							for (var i=0; i < randomNumbers.length; i++){
								
								this.LogSeq("[SR.1]4.1.1.2.1. Randomly select (without replacement) an activity from the children of the activity", logParent);
								this.LogSeq("[SR.1]4.1.1.2.2. Add the selected activity to the child list, retaining the original (from the activity) relative order of activities", logParent);
								
								childList[i] = activityChildren[randomNumbers[i]];
							}
							
						}
						else{
							childList = activityChildren;
						}
						
						this.LogSeq("[SR.1]4.1.1.3. Set Available Children for the activity to the child list", logParent);
						activity.SetAvailableChildren(childList);
						
						activity.SetSelectedChildren(true);
						
						//this process only happens once and it is only from a start command after which the display will be updated anyway
						//Control.RedrawChildren(activity);
					}
				}
			
			}
			
			this.LogSeq("[SR.1]4.2. Exit Select Children Process", logParent);
			
		break;
		
		case TIMING_ON_EACH_NEW_ATTEMPT:
			
			this.LogSeq("[SR.1]5. Case: the Selection Timing for the activity is On Each New Attempt", logParent);
			this.LogSeq("[SR.1]5.1. Exit Select Children Process (Undefined behavior)", logParent);
			
		break;
		
		default:
			this.LogSeq("[SR.1]6. Exit Select Children Process (Undefined timing attribute)", logParent);
		break;
		
	}
 
	this.LogSeqReturn("", logParent);
}

//TODO: put this someplace more logical
function Sequencer_GetUniqueRandomNumbersBetweenTwoValues(numberOfNumbersNeeded, minValue, maxValue){
	
	if (numberOfNumbersNeeded === null || numberOfNumbersNeeded === undefined || numberOfNumbersNeeded < minValue){
		numberOfNumbersNeeded = minValue;
	}
	if (numberOfNumbersNeeded > maxValue){
		numberOfNumbersNeeded = maxValue;
	}
	
	var aryReturn = new Array(numberOfNumbersNeeded);
	
	var newRandomNumber;
	var numberIsAlreadyInUse;
	
	for (var i=0; i < numberOfNumbersNeeded; i++){
		
		numberIsAlreadyInUse = true;
		
		while (numberIsAlreadyInUse){
			newRandomNumber = Sequencer_GetRandomNumberWithinRange(minValue, maxValue);
			numberIsAlreadyInUse = Sequencer_IsNumberAlreadyInArray(newRandomNumber, aryReturn);
		}
		
		aryReturn[i] = newRandomNumber;
	}
	
	aryReturn.sort();
	
	return aryReturn;
}

function Sequencer_GetRandomNumberWithinRange(minValue, maxValue){
	var diff = maxValue - minValue;
	return Math.floor(Math.random() * (diff + minValue + 1));
}


function Sequencer_IsNumberAlreadyInArray(number, aryToCheck){

	for (var i=0; i < aryToCheck.length; i++){
		if (aryToCheck[i] == number){
			return true;
		}
	}
	
	return false;
}