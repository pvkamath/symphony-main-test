//****************************************************************************************************************

//Randomize Children Process [SR.2] 
	//For an activity; may change the Available Children for the activity
	
	//Reference: 
		//Activity is Active AM.1.1
		//Activity is Suspended AM.1.1
		//Available Children AM.1.1
		//Activity Progress Status TM.1.2.1
		//Randomize Children SM.10
		//Randomization Timing SM.10 

function Sequencer_RandomizeChildrenProcess(activity, needRedraw, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Randomize Children Process [SR.2](" + activity + ")", callingLog);
	
	var randomizedChildren;
	
	this.LogSeq("[SR.2]1. If the activity does not have children Then (Cannot apply randomization to a leaf activity)", logParent);
	if (activity.IsALeaf()){
	
		this.LogSeq("[SR.2]1.1. Exit Randomize Children Process", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	
	this.LogSeq("[SR.2]2. If Activity is Suspended for the activity is True Or the Activity is Activefor the activity is True Then (Cannot apply randomization to a suspended or active activity)", logParent);
	if (activity.IsActive() === true || activity.IsSuspended() === true){

		this.LogSeq("[SR.2]2.1. Exit Randomize Children Process", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	
	var randomizationTiming = activity.GetRandomizationTiming();
	
	switch (randomizationTiming){
		
		case TIMING_NEVER:
			this.LogSeq("[SR.2]3. Case: the Randomization Timing for the activity is Never", logParent);
			this.LogSeq("[SR.2]3.1. Exit Randomize Children Process", logParent);
		break;
		
		case TIMING_ONCE:
			this.LogSeq("[SR.2]4. Case: the Randomization Timing for the activity is Once", logParent);
			
			this.LogSeq("[SR.2]4.05. If the activity's children have not already been randomized", logParent);
			if (activity.GetRandomizedChildren() === false){
			
				this.LogSeq("[SR.2]4.1. If the Activity Progress Status for the activity is False Then (If the activity has not been attempted yet)", logParent);
				if (activity.GetActivityProgressStatus() === false){
					
					this.LogSeq("[SR.2]4.1.1. If Randomize Children for the activity is True Then", logParent);
					if (activity.GetRandomizeChildren() === true){
						
						this.LogSeq("[SR.2]4.1.1.1. Randomly reorder the activities contained in Available Children for the activity", logParent);
						randomizedChildren = Sequencer_RandomizeArray(activity.GetAvailableChildren());
						activity.SetAvailableChildren(randomizedChildren);
						
						activity.SetRandomizedChildren(true);
						
						//this will happen at the start before the menu items are actually created...no need to redraw
						//Control.RedrawChildren(activity);
					}
				}
			}
			
			this.LogSeq("[SR.2]4.2. Exit Randomize Children Process", logParent);
			
		break;
		
		case TIMING_ON_EACH_NEW_ATTEMPT:
			
			this.LogSeq("[SR.2]5. Case: the Randomization Timing for the activity is On Each New Attempt", logParent);
			
			this.LogSeq("[SR.2]5.1. If Randomize Children for the activity is True Then", logParent);
			if (activity.GetRandomizeChildren() === true){
				
				this.LogSeq("[SR.2]5.1.1. Randomly reorder the activities contained in Available Children for the activity", logParent);
				randomizedChildren = Sequencer_RandomizeArray(activity.GetAvailableChildren());
				activity.SetAvailableChildren(randomizedChildren);
				
				activity.SetRandomizedChildren(true);
				
				if (needRedraw === true){
					Control.RedrawChildren(activity);
				}
			}
			this.LogSeq("[SR.2]5.2. Exit Randomize Children Process", logParent);
			
		break;
		
		default:
			this.LogSeq("[SR.2]6. Exit Randomize Children Process Undefined timing attribute", logParent);
		break;

	}
	
	this.LogSeqReturn("", logParent);
	return;
}

function Sequencer_RandomizeArray(ary){
	
	var arySize = ary.length;
	var orig;
	var swap;
	
	for (var i = 0; i < arySize; i++){
		
		var swapWith = Math.floor(Math.random() * arySize);
		
		orig = ary[i];
		swap = ary[swapWith];
		
		ary[i] = swap;
		ary[swapWith] = orig;
	}
	
	return ary;
}