//****************************************************************************************************************

//Activity Progress Rollup Process [RB.1.3] 

//	For an activity; may change the Attempt Information for the activity

//	Reference: 
		//Attempt Completion Status TM.1.2.2; 
		//Attempt Progress Status TM.1.2.2; 
		//Rollup Rule Check Subprocess RB.1.4; Rollup Action SM.5 


function Sequencer_ActivityProgressRollupProcess(activity, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Activity Progress Rollup Process [RB.1.3](" + activity + ")", callingLog);
	
	var subprocessResult;
	
	this.LogSeq("[RB.1.3]1. Apply the Rollup Rule Check Subprocess to the activity and the Incomplete rollup action Process all Incomplete rules first", logParent);
	subprocessResult = this.RollupRuleCheckSubprocess(activity, RULE_SET_INCOMPLETE, logParent);

	this.LogSeq("[RB.1.3]2. If the Rollup Rule Check Subprocess returned True Then", logParent);
	if (subprocessResult === true){
		
		this.LogSeq("[RB.1.3]2.1. Set the Attempt Progress Status for the activity to True", logParent);
		activity.SetAttemptProgressStatus(true);
		
		this.LogSeq("[RB.1.3]2.2. Set the Attempt Completion Status for the activity to False", logParent);
		activity.SetAttemptCompletionStatus(false);
	}
	
	this.LogSeq("[RB.1.3]3. Apply the Rollup Rule Check Subprocess to the activity and the Completed rollup action Process all Completed rules last.", logParent);
	subprocessResult = this.RollupRuleCheckSubprocess(activity, RULE_SET_COMPLETED, logParent);
	
	this.LogSeq("[RB.1.3]4. If the Rollup Rule Check Subprocess returned True Then", logParent);
	if (subprocessResult === true){
	
		this.LogSeq("[RB.1.3]4.1. Set the Attempt Progress Status for the activity to True", logParent);
		activity.SetAttemptProgressStatus(true);
		
		this.LogSeq("[RB.1.3]4.2. Set the Attempt Completion Status for the activity to True", logParent);
		activity.SetAttemptCompletionStatus(true);
	}
	
	
	 if (Sequencer_GetApplicableSetofRollupRules (activity, RULE_SET_INCOMPLETE).length === 0 &&
	     Sequencer_GetApplicableSetofRollupRules (activity, RULE_SET_COMPLETED).length === 0){
	
		//NOT IN PSEUDO CODE
		this.LogSeq("[RB.1.3]4.5. Rolling up using Default Rules", logParent);
		
		this.ActivityProgressRollupProcessUsingDefault(activity, logParent);
		
	}
	
	this.LogSeq("[RB.1.3]5. Exit Activity Progress Rollup Process", logParent);
	
	this.LogSeqReturn("", logParent);
	return;
	
}


function Sequencer_ActivityProgressRollupProcessUsingDefault(activity, callingLog){
	
	var logParent = this.LogSeqAudit("Activity Progress Rollup Using Default Process [RB.1.3 a](" + activity + ")", callingLog);

	var completed;
	var attempted;
	var incomplete;
	
	this.LogSeq("[RB.1.3 a]1. If the activity is a leaf", logParent);
	if (activity.IsALeaf()){
		this.LogSeq("[RB.1.3 a]1.1 Exit, nothing to rollup", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	
	//todo - should this only be available children?
	this.LogSeq("[RB.1.3 a]2. Get the set of applicable children", logParent);
	var applicableChildren = activity.GetChildren();
	
	
	this.LogSeq("[RB.1.3 a]3. initialize all incomplete to true", logParent);
	var allIncomplete = true;
	
	this.LogSeq("[RB.1.3 a]4. initialize completed to true", logParent);
	var allCompleted = true;
	
	this.LogSeq("[RB.1.3 a]5. for each applicable child", logParent);
	
	var isEmptySet = true;
	
	for (var i=0; i < applicableChildren.length; i++){
			
			this.LogSeq("[RB.1.3 a]5.1 if child activity (" + applicableChildren[i] + ") is tracked", logParent);
			if (applicableChildren[i].IsTracked()){
			
				this.LogSeq("[RB.1.3 a]5.1.1 completed = activity.getcompleted", logParent);
				completed = applicableChildren[i].IsCompleted();
				
				this.LogSeq("[RB.1.3 a]5.1.2 attempted = activity.getattempted", logParent);
				attempted = applicableChildren[i].IsAttempted();
				
				this.LogSeq("[RB.1.3 a]5.1.3 incomplete = (completed === false || attempted === true)", logParent);
				incomplete = (completed === false || attempted === true);
				
				this.LogSeq("[RB.1.3 a]5.1.4. Child should be included in completed rollup", logParent);
				if (this.CheckChildForRollupSubprocess(applicableChildren[i], ROLLUP_RULE_ACTION_COMPLETED, logParent)){
				
					this.LogSeq("[RB.1.3 a]5.1.4.1 all completed = all completed AND completed", logParent);
					allCompleted = (allCompleted && (completed === true));
					isEmptySet = false;
				}
				
				this.LogSeq("[RB.1.3 a]5.1.5. Child should be included in incomplete rollup", logParent);
				if (this.CheckChildForRollupSubprocess(applicableChildren[i], ROLLUP_RULE_ACTION_INCOMPLETE, logParent)){
				
					this.LogSeq("[RB.1.3 a]5.1.5.1. all incomplete = all incomplete AND incomplete", logParent);
					allIncomplete = (allIncomplete && incomplete);
					isEmptySet = false;
				}
			
			}
	}
	
	if (isEmptySet && Control.Package.Properties.RollupEmptySetToUnknown) {
	
	    this.LogSeq("[SCORM Engine Extension] Child Set is Empty and Control.Package.Properties.RollupEmptySetToUnknown=" + Control.Package.Properties.RollupEmptySetToUnknown + ")", logParent);
	    // Leave unknown, we don't have to explicitly set anything.
	}
	else {
	
	    this.LogSeq("[RB.1.3 a]6. If All Incomplete", logParent);
    	
	    if (allIncomplete === true){
    	
		    this.LogSeq("[RB.1.3 a]6.1. Set the Attempt Progress Status for the activity to True", logParent);
		    activity.SetAttemptProgressStatus(true);
    		
		    this.LogSeq("[RB.1.3 a]6.2. Set the Attempt Completion Status for the activity to False", logParent);
		    activity.SetAttemptCompletionStatus(false);
	    }
    	
	    this.LogSeq("[RB.1.3 a]7. If All Satisfied", logParent);
    	
	    if (allCompleted === true){
    	
		    this.LogSeq("[RB.1.3 a]7.1. Set the Attempt Progress Status for the activity to True", logParent);
		    activity.SetAttemptProgressStatus(true);
    		
		    this.LogSeq("[RB.1.3 a]7.2. Set the Attempt Completion Status for the activity to True", logParent);
		    activity.SetAttemptCompletionStatus(true);
	    }
	}

	
	this.LogSeqReturn("", logParent);
}