//****************************************************************************************************************

//Check Child for Rollup Subprocess [RB.1.4.2] 

//	For an activity and a Rollup Action; returns True if the activity is included in rollup

//	Reference: 
		//Rollup Action SM.5
		//Rollup Objective Satisfied SM.8
		//Rollup Progress Completion SM.8
		//Activity Attempt Count TM.1.2.1
		//Sequencing Rules Check Process UP.2
		//adlseq:requiredForSatisfied SCORM SN
		//adlseq:requiredForNotSatisfied SCORM SN
		//adlseq:requiredForCompleted SCORM SN
		//adlseq:requiredForIncomplete SCORM SN 

function Sequencer_CheckChildForRollupSubprocess(activity, ruleAction, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	

	var logParent = this.LogSeqAudit("Check Child for Rollup Subprocess [RB.1.4.2](" + activity + ", " + ruleAction + ")", callingLog);
	
	var sequencingRulesCheckResult;
	
	this.LogSeq("[RB.1.4.2]1. Set included to False", logParent);
	var included = false;
	
	this.LogSeq("[RB.1.4.2]2. If the Rollup Action is Satisfied Or Not Satisfied Then", logParent);
	
	if (ruleAction == ROLLUP_RULE_ACTION_SATISFIED || ruleAction == ROLLUP_RULE_ACTION_NOT_SATISFIED){
	
		this.LogSeq("[RB.1.4.2]2.1. If the Rollup Objective Satisfied value for the activity is True Then (Test the objective rollup control)", logParent);
		if (activity.GetRollupObjectiveSatisfied() === true){
			
			this.LogSeq("[RB.1.4.2]2.1.1. Set included to True (Default Behavior � adlseq:requiredFor[xxx] == always)", logParent);
			included = true;
			
			var requiredForSatified = activity.GetRequiredForSatisfied();
			var requiredForNotSatified = activity.GetRequiredForNotSatisfied();
			
			this.LogSeq("[RB.1.4.2]2.1.2. If (the Rollup Action is Satisfied And adlseq:requiredForSatisfied is ifNotSuspended) Or (the Rollup Action is Not Satisfied And adlseq:requiredForNotSatisfied is ifNotSuspended) Then", logParent);
			if (
					(ruleAction == ROLLUP_RULE_ACTION_SATISFIED && requiredForSatified == ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED) ||
					(ruleAction == ROLLUP_RULE_ACTION_NOT_SATISFIED && requiredForNotSatified == ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)){
			
				this.LogSeq("[RB.1.4.2]2.1.2.1. If Activity Attempt Count for the activity is greater than (>) Zero (0) And Activity is Suspended for the activity is True Then", logParent);
				//should this also need to check to see if ActivityProgressStatus === true, if so, use the Activity_IsAttempted function...no, attempt count will never be > 0 without activity progress status being true.
				
				if (activity.GetAttemptCount() > 0 && activity.IsSuspended() === true){
				
					this.LogSeq("[RB.1.4.2]2.1.2.1.1. Set included to False", logParent);
					included = false;
				}
			}
			else{
				this.LogSeq("[RB.1.4.2]2.1.3. Else", logParent);
				this.LogSeq("[RB.1.4.2]2.1.3.1. If (the Rollup Action is Satisfied And adlseq:requiredForSatisfied is ifAttempted) Or (the Rollup Action is Not Satisfied And adlseq:requiredForNotSatisfied is ifAttempted) Then", logParent);
				if (
						(ruleAction == ROLLUP_RULE_ACTION_SATISFIED && requiredForSatified == ROLLUP_CONSIDERATION_IF_ATTEMPTED) ||
						(ruleAction == ROLLUP_RULE_ACTION_NOT_SATISFIED && requiredForNotSatified == ROLLUP_CONSIDERATION_IF_ATTEMPTED)){
					
					this.LogSeq("[RB.1.4.2]2.1.3.1.1. If Activity Attempt Count for the activity is Zero (0) Then", logParent);
					if (activity.GetAttemptCount() === 0){
					
						this.LogSeq("[RB.1.4.2]2.1.3.1.1.1. Set included to False", logParent);
						included = false;
					}
					
				}
				else{
					this.LogSeq("[RB.1.4.2]2.1.3.2. Else", logParent);
					this.LogSeq("[RB.1.4.2]2.1.3.2.1. If (the Rollup Action is Satisfied And adlseq:requiredForSatisfied is ifNotSkipped) Or (the Rollup Action is Not Satisfied And adlseq:requiredForNotSatisfied is ifNotSkipped) Then", logParent);
					
					if (
							(ruleAction == ROLLUP_RULE_ACTION_SATISFIED && requiredForSatified == ROLLUP_CONSIDERATION_IF_NOT_SKIPPED) ||
							(ruleAction == ROLLUP_RULE_ACTION_NOT_SATISFIED && requiredForNotSatified == ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)){
					
						this.LogSeq("[RB.1.4.2]2.1.3.2.1.1. Apply the Sequencing Rules Check Process to the activity and its Skipped sequencing rules", logParent);
						sequencingRulesCheckResult = this.SequencingRulesCheckProcess(activity, RULE_SET_SKIPPED, logParent);
						
						this.LogSeq("[RB.1.4.2]2.1.3.2.1.2. If the Sequencing Rules Check Process does not return Nil Then", logParent);
						if (sequencingRulesCheckResult !== null){
						
							this.LogSeq("[RB.1.4.2]2.1.3.2.1.2.1. Set included to False", logParent);
							included  = false;
						}
					}
				}
			}
		}
	}
	
	this.LogSeq("[RB.1.4.2]3. If the Rollup Action is Completed Or Incomplete Then", logParent);
	if (ruleAction == ROLLUP_RULE_ACTION_COMPLETED || ruleAction == ROLLUP_RULE_ACTION_INCOMPLETE){
	
		this.LogSeq("[RB.1.4.2]3.1. If the Rollup Progress Completion value for the activity is True Then (Test the progress rollup control)", logParent);
		if (activity.RollupProgressCompletion() === true){
			
			this.LogSeq("[RB.1.4.2]3.1.1. Set included to True (Default Behavior � adlseq:requiredFor[xxx] == always)", logParent);
			included = true;
			
			var requiredForCompleted = activity.GetRequiredForCompleted();
			var requiredForIncomplete = activity.GetRequiredForIncomplete();
			
			this.LogSeq("[RB.1.4.2]3.1.2. If (the Rollup Action is Completed And adlseq:requiredForCompleted is ifNotSuspended) Or (the Rollup Action is Incomplete And adlseq:requiredForIncomplete is ifNotSuspended) Then", logParent);
			if (
					(ruleAction == ROLLUP_RULE_ACTION_COMPLETED && requiredForCompleted == ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED) ||
					(ruleAction == ROLLUP_RULE_ACTION_INCOMPLETE && requiredForIncomplete == ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)){
				
				this.LogSeq("[RB.1.4.2]3.1.2.1. If Activity Attempt Count for the activity is greater than (>) Zero (0) And Activity is Suspended for the activity is True Then", logParent);
				if (activity.GetAttemptCount() > 0 && activity.IsSuspended() === true){
				
					this.LogSeq("[RB.1.4.2] Set included to False", logParent);
					included = false;
				}
			}
			else{
				this.LogSeq("[RB.1.4.2]3.1.3. Else", logParent);
				this.LogSeq("[RB.1.4.2]3.1.3.1. If (the Rollup Action is Completed And adlseq:requiredForCompleted is ifAttempted) Or (the Rollup Action is Incomplete And adlseq:requiredForIncomplete is ifAttempted) Then", logParent);
				if (
						(ruleAction == ROLLUP_RULE_ACTION_COMPLETED && requiredForCompleted == ROLLUP_CONSIDERATION_IF_ATTEMPTED) ||
						(ruleAction == ROLLUP_RULE_ACTION_INCOMPLETE && requiredForIncomplete == ROLLUP_CONSIDERATION_IF_ATTEMPTED)){
					
					this.LogSeq("[RB.1.4.2]3.1.3.1.1. If Activity Attempt Count for the activity is Zero (0) Then", logParent);
					if (activity.GetAttemptCount() === 0){
						
						this.LogSeq("[RB.1.4.2]3.1.3.1.1.1. Set included to False", logParent);
						included =	false;
					}
				}
				else{
					this.LogSeq("[RB.1.4.2]3.1.3.2. Else", logParent);
					this.LogSeq("[RB.1.4.2]3.1.3.2.1. If (the Rollup Action is Completed And adlseq:requiredForCompleted is ifNotSkipped) Or (the Rollup Action is Incomplete And adlseq:requiredForIncomplete is ifNotSkipped) Then", logParent);
					if (
						(ruleAction == ROLLUP_RULE_ACTION_COMPLETED && requiredForCompleted == ROLLUP_CONSIDERATION_IF_NOT_SKIPPED) ||
						(ruleAction == ROLLUP_RULE_ACTION_INCOMPLETE && requiredForIncomplete == ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)){
					
						this.LogSeq("[RB.1.4.2]3.1.3.2.1.1. Apply the Sequencing Rules Check Process to the activity and its Skipped sequencing rules", logParent);
						sequencingRulesCheckResult = this.SequencingRulesCheckProcess(activity, RULE_SET_SKIPPED, logParent);
						
						this.LogSeq("[RB.1.4.2]3.1.3.2.1.2. If the Sequencing Rules Check Process does not return Nil Then", logParent);
						if (sequencingRulesCheckResult !== null){
						
							this.LogSeq("[RB.1.4.2]3.1.3.2.1.2.1. Set included to False", logParent);
							included = false;
						}
					}
				}
			}
		}
		
	}
	
	this.LogSeq("[RB.1.4.2]4. Exit Check Child for Rollup Subprocess (Child is Included in Rollup: included)", logParent);
	this.LogSeqReturn(included, logParent);
	return included;

}
