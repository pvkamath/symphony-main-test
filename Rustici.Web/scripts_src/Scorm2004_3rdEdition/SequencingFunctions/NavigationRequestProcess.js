//****************************************************************************************************************

//Navigation Request Process [NB.2.1]

//For a navigation request and possibly a specified activity, returns the validity of the navigation request; 
//may return a termination request, a sequencing request, and/or a target activity; may return an exception code

//Reference: 
	//Current Activity AM.1.2; 
	//Sequencing Control Choice SM.1; 
	//Sequencing Control Choice Exit SM.1; 
	//Sequencing Control Flow SM.1; 
	//Sequencing Control Forward Only SM.1; 
	//Suspended Activity AM.1.2 


function Sequencer_NavigationRequestProcess(navigationRequest, targetActivityIdentifier, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Navigation Request Process [NB.2.1](" + navigationRequest + ", " + targetActivityIdentifier + ")", callingLog);
	
	var currentActivity = this.GetCurrentActivity();
	
	var parentActivity = null;
	
	if (currentActivity !== null){
		parentActivity = currentActivity.ParentActivity;
	}
	
	var parentActivityTitle = "";
	if (parentActivity !== null){
		parentActivityTitle = parentActivity.GetTitle();
	}
	
	var returnValue;
	
	switch (navigationRequest){
		
		case NAVIGATION_REQUEST_START:
			
			this.LogSeq("[NB.2.1]1.Case: navigation request is Start", logParent);
			this.LogSeq("[NB.2.1]1.1. If the Current Activity is Not Defined Then (Make sure the sequencing session has not already begun)", logParent);
			
			if ( ! this.IsCurrentActivityDefined(logParent)){
				
				this.LogSeq("[NB.2.1]1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Start; Target Activity: n/a; Exception: n/a)", logParent);
				
				returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, null, SEQUENCING_REQUEST_START, null, null, "");
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[NB.2.1]1.2. Else", logParent);
				this.LogSeq("[NB.2.1]1.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-1)", logParent);
				
				returnValue =new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null,  "NB.2.1-1", null, IntegrationImplementation.GetString("The sequencing session has already been started."));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
				
		break;

		case NAVIGATION_REQUEST_RESUME_ALL:
			
			this.LogSeq("[NB.2.1]2. Case: navigation request is Resume All", logParent);
			this.LogSeq("[NB.2.1]2.1. If the Current Activity is Not Defined Then (Make sure the sequencing session has not already begun)", logParent);
			
			if ( ! this.IsCurrentActivityDefined(logParent)){
				
				this.LogSeq("[NB.2.1]2.1.1. If the Suspended Activity is Defined Then (Make sure the previous sequencing session ended with a suspend all request)", logParent);
				
				if ( this.IsSuspendedActivityDefined(logParent)){
					
					this.LogSeq("[NB.2.1]2.1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Resume All; Target Activity: n/a; Exception: n/a) ", logParent);
					
					returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, null, SEQUENCING_REQUEST_RESUME_ALL, null, null, "");	
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;	
				}
				else{
					this.LogSeq("[NB.2.1]2.1.2. Else ", logParent);
					this.LogSeq("[NB.2.1]2.1.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-3)", logParent);
					
					returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-3", null, IntegrationImplementation.GetString("There is no suspended activity to resume."));					
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			}
			else{
				this.LogSeq("[NB.2.1]2.2. Else", logParent);
				this.LogSeq("[NB.2.1]2.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-1)", logParent);

				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-1", null, IntegrationImplementation.GetString("The sequencing session has already been started."));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			
		break; 

		case NAVIGATION_REQUEST_CONTINUE:
			
			this.LogSeq("[NB.2.1]3. Case: navigation request is Continue", logParent);
			
			this.LogSeq("[NB.2.1]3.1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)", logParent);
			
			if ( ! this.IsCurrentActivityDefined(logParent)){
				
				this.LogSeq("[NB.2.1]3.1.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-2)", logParent);
				
				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-1", null, IntegrationImplementation.GetString("Cannot continue until the sequencing session has begun."));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			 
			this.LogSeq("[NB.2.1]3.2. If the Current Activity is not the root of the activity tree And the Sequencing Control Flow for the parent of the Current Activity is True Then (Validate that a 'flow' sequencing request can be processed from the current activity)", logParent);
			
			if ( (! currentActivity.IsTheRoot() ) && (parentActivity.LearningObject.SequencingData.ControlFlow === true ) ){
						 
				this.LogSeq("[NB.2.1]3.2.1. If the Activity is Active for the Current Activity is True Then (If the current activity has not been terminated, terminate the current the activity)", logParent);
				
				if (currentActivity.IsActive()){
					this.LogSeq("[NB.2.1]3.2.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit; Sequencing Request: Continue; Target Activity: n/a; Exception: n/a)", logParent);
					
					returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, TERMINATION_REQUEST_EXIT, SEQUENCING_REQUEST_CONTINUE, null, null, "");				
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
				else{
					this.LogSeq("[NB.2.1]3.2.2. Else", logParent);
					this.LogSeq("[NB.2.1]3.2.2.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Continue; Target Activity: n/a; Exception: n/a)", logParent);
					
					returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, null, SEQUENCING_REQUEST_CONTINUE, null, null, "");
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			}
			else{
				this.LogSeq("[NB.2.1]3.3. Else", logParent);
				this.LogSeq("[NB.2.1]3.3.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-4) (Flow is not enabled or the current activity is the root of the activity tree)", logParent);		
				
				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-4", null, IntegrationImplementation.GetString("Please select a menu item to continue with {0}.", parentActivityTitle));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		break;
		
		case NAVIGATION_REQUEST_PREVIOUS:
			
			this.LogSeq("[NB.2.1]4. Case: navigation request is Previous", logParent);
			
			this.LogSeq("[NB.2.1]4.1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)", logParent);
			
			if ( ! this.IsCurrentActivityDefined(logParent)){
			
				this.LogSeq("[NB.2.1]4.1.1.Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-2)", logParent);
				
				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-2", null, IntegrationImplementation.GetString("Cannot move backwards until the sequencing session has begun."));				
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			
			
			this.LogSeq("[NB.2.1]4.2. If the Current Activity is not the root of the activity tree Then (There is no activity logically 'previous' to the root of the activity tree)", logParent);
			if (! currentActivity.IsTheRoot()){
				
				this.LogSeq("NB.2.1]4.2.1. If the Sequencing Control Flow for the parent of the Current Activity is True And the Sequencing Control Forward Only for the parent of the Current Activity is False Then (Validate that a 'flow' sequencing request can be processed from the current activity)", logParent);
				
				if (parentActivity.LearningObject.SequencingData.ControlFlow === true && parentActivity.LearningObject.SequencingData.ControlForwardOnly === false){
				
					this.LogSeq("[NB.2.1]4.2.1.1. If the Activity is Active for the Current Activity is True Then (If the current activity has not been terminated, terminate the current the activity)", logParent);
					
					if (currentActivity.IsActive()){
						
						this.LogSeq("[NB.2.1]4.2.1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit; Sequencing Request: Previous; Target Activity: n/a; Exception: n/a)", logParent);
						
						returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, TERMINATION_REQUEST_EXIT, SEQUENCING_REQUEST_PREVIOUS, null, null, "");						
						this.LogSeqReturn(returnValue, logParent);
						return returnValue;
					}
					else{
						this.LogSeq("[NB.2.1]4.2.1.2. Else", logParent);
						this.LogSeq("[NB.2.1]4.2.1.2.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Previous; Target Activity: n/a; Exception: n/a)", logParent);
						
						returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, null, SEQUENCING_REQUEST_PREVIOUS, null, null, "");						
						this.LogSeqReturn(returnValue, logParent);
						return returnValue;
					}
				}
				else{
					this.LogSeq("[NB.2.1]4.2.2. Else", logParent);
					this.LogSeq("[NB.2.1]4.2.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-5) (Violates control mode)", logParent);
					
					returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-5", null, IntegrationImplementation.GetString("Please select a menu item to continue with {0}.", parentActivity.GetTitle()));
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			}
			else{
				this.LogSeq("[NB.2.1]4.3. Else", logParent);
				this.LogSeq("[NB.2.1]4.3.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-6) (Cannot move backward from the root of the activity tree)", logParent);
				
				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-6", null, IntegrationImplementation.GetString("You have reached the beginning of the course."));							
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		break;
		
		
		case NAVIGATION_REQUEST_FORWARD:
		
			this.LogSeq("[NB.2.1]5. Case: navigation request is Forward (Behavior not defined)", logParent);
			this.LogSeq("[NB.2.1]5.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-7)", logParent);
			
			returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-7", null, IntegrationImplementation.GetString("The 'Forward' navigation request is not supported, try using 'Continue'."));
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;			
		//break;
		
		case NAVIGATION_REQUEST_BACKWARD:
			this.LogSeq("[NB.2.1]6. Case: navigation request is Backward (Behavior not defined)", logParent);
			this.LogSeq("[NB.2.1]6.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-7)", logParent);
			
			returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-7", null, IntegrationImplementation.GetString("The 'Backward' navigation request is not supported, try using 'Previous'."));
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		//break;
		
		case NAVIGATION_REQUEST_CHOICE:
			this.LogSeq("[NB.2.1]7. Case: navigation request is Choice ", logParent);
			 
			this.LogSeq("[NB.2.1]7.1. If the activity specified by the Choice navigation request exists within the activity tree Then (Make sure the target activity exists in the activity tree)", logParent);
			
			if (this.DoesActivityExist(targetActivityIdentifier, logParent)){
				
				var targetActivity = this.GetActivityFromIdentifier(targetActivityIdentifier, logParent);
				var targetParentActivity = this.Activities.GetParentActivity(targetActivity);
				
				//not in pseudo code
				if (targetActivity.IsAvailable() === false){
					returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-7", null, IntegrationImplementation.GetString("The activity '{0}' was not selected to be delivered in this attempt.", targetActivity));															
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;				
				}
				
				this.LogSeq("[NB.2.1]7.1.1. If the activity specified by the Choice navigation request is the root of the activity tree Or the Sequencing Control Choice for the parent of the activity specified by the Choice navigation request is True Then (Validate that a 'choice' sequencing request can be processed on the target activity)", logParent);
				if (targetActivity.IsTheRoot() || targetParentActivity.LearningObject.SequencingData.ControlChoice === true){
					
					this.LogSeq("[NB.2.1]7.1.1.1. If the Current Activity is Not Defined Then (Attempt to start the sequencing session through choice)", logParent);
					
					if (! this.IsCurrentActivityDefined(logParent)){
						
						this.LogSeq("[NB.2.1]7.1.1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Choice; Target Activity: the activity specified by the Choice navigation request; Exception: n/a)", logParent);
						
						returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, null, SEQUENCING_REQUEST_CHOICE, null, targetActivity, "");					
						this.LogSeqReturn(returnValue, logParent);
						return returnValue;
					}
					
					this.LogSeq("[NB.2.1]7.1.1.2. If the activity specified by the Choice navigation request is Not a sibling of the Current Activity Then", logParent);
					if ( ! this.AreActivitiesSiblings(currentActivity, targetActivity, logParent)){
						
						this.LogSeq("[NB.2.1]7.1.1.2.1. Find the common ancestor of the Current Activity and the activity specified by the Choice navigation request", logParent);
						var commonAncestor = this.FindCommonAncestor(currentActivity, targetActivity, logParent);
						
						
						this.LogSeq("[NB.2.1]7.1.1.2.2. Form the activity path as the ordered series of activities from the Current Activity to the common ancestor (The common ancestor will not terminate as a result of processing the choice sequencing request, unless the common ancestor is the Current Activity - the current activity should always be included in the activity path)", logParent);
						//var aryParentActivities = this.GetActivityPath(currentActivity, true);	//removed for addendum 3.8 / 3.9
						var aryParentActivities = this.GetPathToAncestorExclusive(currentActivity, commonAncestor, true);
						
						 
						this.LogSeq("[NB.2.1]7.1.1.2.3. If the activity path is Not Empty Then", logParent);
						if (aryParentActivities.length > 0){
						 
							this.LogSeq("[NB.2.1]7.1.1.2.3.1. For each activity in the activity path (Make sure that 'choosing' the target will not force an active activity to terminate, if that activity does not allow choice to terminate it)", logParent);
							for (var i = 0; i < aryParentActivities.length; i++){
								
								//stop the loop at the common ancestor
								if (aryParentActivities[i].GetItemIdentifier() == commonAncestor.GetItemIdentifier()){
									break;
								}
								
								this.LogSeq("[NB.2.1]7.1.1.2.3.1.1. If Activity is Active for the activity is True And the Sequencing Control Choice Exit for the activity is False Then (Activity Identifier-" + aryParentActivities[i].LearningObject.ItemIdentifier + ")", logParent);
								
								if (aryParentActivities[i].IsActive() === true && aryParentActivities[i].LearningObject.SequencingData.ControlChoiceExit === false){
									
									this.LogSeq("[NB.2.1]7.1.1.2.3.1.1.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: " + CONTROL_CHOICE_EXIT_ERROR_NAV + ") (Violates control mode)", logParent);
									
									returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, CONTROL_CHOICE_EXIT_ERROR_NAV, null, IntegrationImplementation.GetString("You must complete '{0}' before you can select another item.", aryParentActivities[i]));															
									this.LogSeqReturn(returnValue, logParent);
									return returnValue;
								}
							}
						}
						else{
							this.LogSeq("[NB.2.1]7.1.1.2.4. Else", logParent);
							this.LogSeq("[NB.2.1]7.1.1.2.4.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-9)", logParent);
							
							returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-9", null, IntegrationImplementation.GetString("Nothing to open"));
							this.LogSeqReturn(returnValue, logParent);
							return returnValue;
						} 
					}
					
					
					this.LogSeq("[NB.2.1]7.1.1.3. If Activity is Active for the Current Activity is True And the Sequencing Control Choice Exit for the Current Activity is False Then (The Choice target is a sibling to the Current Activity, check if the Current Activity)", logParent);
					if (currentActivity.IsActive() && currentActivity.GetSequencingControlChoiceExit() === false){
						this.LogSeq("7.1.1.3.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception " + CONTROL_CHOICE_EXIT_ERROR_NAV + ") Violates control mode.", logParent);
						returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, CONTROL_CHOICE_EXIT_ERROR_NAV, null, IntegrationImplementation.GetString("You are not allowed to jump out of {0}.", currentActivity.GetTitle()));
						return returnValue;
					}
									
					this.LogSeq("[NB.2.1]7.1.1.4. If the Activity is Active for the Current Activity is True Then (If the current activity has not been terminated, terminate the current the activity)", logParent);
					if (currentActivity.IsActive()){
						this.LogSeq("[NB.2.1]7.1.1.4.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit; Sequencing Request: Choice; Target Activity: the activity specified by the Choice navigation request; Exception: n/a) ", logParent);
						
						returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, TERMINATION_REQUEST_EXIT, SEQUENCING_REQUEST_CHOICE, null, targetActivity, "");						
						this.LogSeqReturn(returnValue, logParent);
						return returnValue;
					}
					else{
					
						this.LogSeq("[NB.2.1]7.1.1.5. Else", logParent);
						this.LogSeq("[NB.2.1]7.1.1.5.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Choice; Target Activity: the activity specified by the Choice navigation request; Exception: n/a)", logParent);
						
						returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, null, SEQUENCING_REQUEST_CHOICE, null, targetActivity, "");						
						this.LogSeqReturn(returnValue, logParent);
						return returnValue;
					}
				}
				else{
					this.LogSeq("[NB.2.1]7.1.2. Else", logParent);
					this.LogSeq("[NB.2.1]7.1.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-10) (Violates control mode)", logParent);
					
					returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-10", null, IntegrationImplementation.GetString("Please select 'Next' or 'Previous' to move through {0}.", targetParentActivity.GetTitle()));
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			} 
			else{
				this.LogSeq("[NB.2.1]7.2. Else", logParent);
				this.LogSeq("[NB.2.1]7.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-11) (Target activity does not exist)", logParent);			
				
				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-11", null, IntegrationImplementation.GetString("The activity you selected ({0}) does not exist.", targetActivityIdentifier));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		break;
		
		case NAVIGATION_REQUEST_EXIT:
			
			this.LogSeq("[NB.2.1]8. Case: navigation request is Exit", logParent);
			 
			this.LogSeq("[NB.2.1]8.1. If the Current Activity is Defined Then (Make sure the sequencing session has already begun)", logParent);
			if ( this.IsCurrentActivityDefined(logParent)){
			 
				this.LogSeq("[[NB.2.1]8.1.1. If the Activity is Active for the Current Activity is True Then (Make sure the current activity has not already been terminated)", logParent);
				
				if (currentActivity.IsActive()){
				
					this.LogSeq("[NB.2.1]8.1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit; Sequencing Request: Exit; Target Activity: n/a) ; Exception: n/a)", logParent);
					
					returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, TERMINATION_REQUEST_EXIT, SEQUENCING_REQUEST_EXIT, null, null, "");
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
				else{
					this.LogSeq("[NB.2.1]8.1.2. Else", logParent);
					this.LogSeq("[NB.2.1]8.1.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-12) (Activity has already terminated )", logParent);
					
					returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null,  "NB.2.1-12", null, IntegrationImplementation.GetString("The Exit navigation request is invalid because the current activity ({0}) is no longer active.", currentActivity.GetTitle()));
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			}
			else{
				this.LogSeq("[NB.2.1]8.2. Else", logParent);
				this.LogSeq("[NB.2.1]8.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-2) ", logParent);
				
				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-2", null, IntegrationImplementation.GetString("The Exit navigation request is invalid because there is no current activity."));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			
		break;
		 
		case NAVIGATION_REQUEST_EXIT_ALL:
			this.LogSeq("[NB.2.1]9. Case: navigation request is Exit All", logParent);
			
			this.LogSeq("[NB.2.1]9.1. If the Current Activity is Defined Then (If the sequencing session has already begun, unconditionally terminate all active activities)", logParent);
			if ( this.IsCurrentActivityDefined(logParent)){
			
				this.LogSeq("[NB.2.1]9.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit All; Sequencing Request: Exit; Target Activity: n/a; Exception: n/a)", logParent);
				
				returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, TERMINATION_REQUEST_EXIT_ALL, SEQUENCING_REQUEST_EXIT, null, null, "");
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[NB.2.1]9.2. Else", logParent);
				this.LogSeq("[NB.2.1]9.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-2)", logParent);
				
				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-2", null, IntegrationImplementation.GetString("The Exit All navigation request is invalid because there is no current activity."));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			} 
		break;
		
		case NAVIGATION_REQUEST_ABANDON: 
			
			this.LogSeq("[NB.2.1]10. Case: navigation request is Abandon", logParent);
			
			this.LogSeq("[NB.2.1]10.1. If the Current Activity is Defined Then (Make sure the sequencing session has already begun)", logParent);
			
			if ( this.IsCurrentActivityDefined(logParent)){
				
				this.LogSeq("[NB.2.1]10.1.1. If the Activity is Active for the Current Activity is True Then (Make sure the current activity has not already been terminated)", logParent);
				
				if (currentActivity.IsActive()){
					
					this.LogSeq("[NB.2.1]10.1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Abandon; Sequencing Request: Exit; Target Activity: n/a; Exception: n/a)", logParent);
					
					returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, TERMINATION_REQUEST_ABANDON, SEQUENCING_REQUEST_EXIT, null, null, "");
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
				else{
					this.LogSeq("[NB.2.1]10.1.2. Else", logParent);
					this.LogSeq("[NB.2.1]10.1.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-12)", logParent);
					
					returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-12", null, IntegrationImplementation.GetString("The 'Abandon' navigation request is invalid because the current activity '{0}' is no longer active.", currentActivity.GetTitle()));
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			}
			else{
				this.LogSeq("[NB.2.1]10.2. Else ", logParent);
				this.LogSeq("[NB.2.1]10.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-2)", logParent);
				
				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-2", null, IntegrationImplementation.GetString("The 'Abandon' navigation request is invalid because there is no current activity."));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		break;
		
		case NAVIGATION_REQUEST_ABANDON_ALL: 
			this.LogSeq("[NB.2.1]11. Case: navigation request is Abandon All", logParent);
			this.LogSeq("[NB.2.1]11.1. If the Current Activity is Defined Then (If the sequencing session has already begun, unconditionally abandon all active activities)", logParent);
			
			if ( this.IsCurrentActivityDefined(logParent)){
				
				this.LogSeq("[NB.2.1]11.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Abandon All; Sequencing Request: Exit; Target Activity: n/a; Exception: n/a)", logParent);

				returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, TERMINATION_REQUEST_ABANDON_ALL, SEQUENCING_REQUEST_EXIT, null, null, "");		
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[NB.2.1]11.2. Else", logParent);
				this.LogSeq("[NB.2.1]11.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-2) ", logParent);
				
				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-2", null, IntegrationImplementation.GetString("You cannot use 'Abandon All' if no item is currently open."));
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		break;
		 
		case NAVIGATION_REQUEST_SUSPEND_ALL:
			this.LogSeq("[NB.2.1]12. Case: navigation request is Suspend All", logParent);
			
			this.LogSeq("[NB.2.1]12.1. If the Current Activity is Defined Then (If the sequencing session has already begun)", logParent);
			if ( this.IsCurrentActivityDefined(logParent)){
			
				this.LogSeq("[NB.2.1]12.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Suspend All; Sequencing Request: Exit; Target Activity: n/a; Exception: n/a)", logParent);
				
				returnValue = new Sequencer_NavigationRequestProcessResult(navigationRequest, TERMINATION_REQUEST_SUSPEND_ALL, SEQUENCING_REQUEST_EXIT, null, null, "");			
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[NB.2.1]12.2. Else", logParent);
				this.LogSeq("[NB.2.1]12.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-2)", logParent);	
				
				returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-2", null, IntegrationImplementation.GetString("The 'Suspend All' navigation request is invalid because there is no current activity."));	
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		break;
		
		default:
			this.LogSeq("[NB.2.1]13. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-13) (Undefined navigation request)", logParent);
			
			returnValue = new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID, null, null, "NB.2.1-13", null, IntegrationImplementation.GetString("Undefined Navigation Request"));		
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		//break;
	
	}

}


function Sequencer_NavigationRequestProcessResult(navigationRequest, terminationRequest, sequencingRequest, exception, targetActivity, exceptionText){
	
	this.NavigationRequest = navigationRequest;
	this.TerminationRequest = terminationRequest;
	this.SequencingRequest = sequencingRequest;
	this.Exception = exception;
	this.TargetActivity = targetActivity;
	this.ExceptionText = exceptionText;
}

Sequencer_NavigationRequestProcessResult.prototype.toString = function(){
									return "NavigationRequest=" + this.NavigationRequest + 
										", TerminationRequest=" + this.TerminationRequest + 
										", SequencingRequest=" + this.SequencingRequest + 
										", Exception=" + this.Exception + 
										", TargetActivity=" + this.TargetActivity + 
										", ExceptionText=" + this.ExceptionText;
									};