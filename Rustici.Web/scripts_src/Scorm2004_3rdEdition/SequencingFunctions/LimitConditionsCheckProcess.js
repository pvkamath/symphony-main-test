//****************************************************************************************************************
//Limit Conditions Check Process [UP.1] 
//	For an activity; returns True if any of the activity's limit conditions have been violated
//	Reference: 
		//Activity Attempt Count TM.1.2.1
		//Activity Progress Status TM.1.2.1
		//Activity Absolute Duration TM.1.2.1
		//Activity Experienced Duration TM.1.2.1
		//Attempt Progress Status TM.1.2.2
		//Attempt Absolute Duration TM.1.2.2
		//Attempt Experienced Duration TM.1.2.2
		//Limit Condition Activity Absolute Duration Control SM.3
		//Limit Condition Activity Absolute Duration Limit SM.3
		//Limit Condition Activity Experienced Duration Control SM.3
		//Limit Condition Activity Experienced Duration Limit SM.3
		//Limit Condition Attempt Absolute Duration Control SM.3
		//Limit Condition Attempt Absolute Duration Limit SM.3
		//Limit Condition Attempt Experienced Duration Control SM.3
		//Limit Condition Attempt Experienced Duration Limit SM.3
		//Limit Condition Attempt Control SM.3
		//Limit Condition Attempt Limit SM.3
		//Limit Condition Begin Time Limit SM.3
		//Limit Condition Begin Time Limit Control SM.3
		//Limit Condition End Time Limit SM.3
		//Limit Condition End Time Limit Control SM.3
		//Tracked SM.11 

function Sequencer_LimitConditionsCheckProcess(activity, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Limit Conditions Check Process [UP.1](" + activity + ")", callingLog);

	this.LogSeq("[UP.1]1. If Tracked for the activity is False Then (If the activity is not tracked, its limit conditions cannot be violated)", logParent);
	if (activity.IsTracked() === false){
		this.LogSeq("[UP.1]1.1. Exit Limit Conditions Check Process (Limit Condition Violated: False) (Activity is not tracked, no limit conditions can be violated)", logParent);
		this.LogSeqReturn("false", logParent);
		return false;
	}
	
	this.LogSeq("[UP.1]2. If the Activity is Active for the activity is True Or the Activity is Suspended for the activity is True Then (Only need to check activities that will begin a new attempt)", logParent);
	if (activity.IsActive() || activity.IsSuspended()){
		this.LogSeq("[UP.1]2.1. Exit Limit Conditions Check Process (Limit Condition Violated: False)", logParent);
		this.LogSeqReturn("false", logParent);
		return false;
	}
	
	this.LogSeq("[UP.1]3. If the Limit Condition Attempt Control for the activity is True Then", logParent);
	if (activity.GetLimitConditionAttemptControl() === true){
	
		var attemptCount = activity.GetAttemptCount();
		var attemptLimit = activity.GetLimitConditionAttemptLimit();
		var activityProgressStatus = activity.GetActivityProgressStatus();
		
		this.LogSeq("[UP.1]3.1. If the Activity Progress Status for the activity is True (currently " + activityProgressStatus + ") And the Activity Attempt Count (currently " + attemptCount + ") for the activity is greater than or equal (>=) to the Limit Condition Attempt Limit (currently " + attemptLimit + ") for the activity Then", logParent);
	
		if (activityProgressStatus === true && (attemptCount >= attemptLimit)){
			
			this.LogSeq("[UP.1]3.1.1. Exit Limit Conditions Check Process (Limit Condition Violated: True) (Limit conditions have been violated)", logParent);
			this.LogSeqReturn("true", logParent);
			return true;
			
		}
	}
	
	this.LogSeq("[UP.1] 4 - 9 Note - duration and time based limit controls are not evaluated in this implementation", logParent);
	
	//[UP.1]4. 	If the Limit Condition Activity Absolute Duration Control for the activity is True Then 
	//[UP.1]4.1. 		If the Activity Progress Status for the activity is True And the Activity Absolute Duration for the activity is greater than or equal (>=) to Limit Condition Activity Absolute Duration Limit for the activity Then 
	//[UP.1]4.1.1. 		Exit Limit Conditions Check Process (Limit Condition Violated: True) 	(Limit conditions have been violated)
	//[UP.1]			End If 
	//[UP.1]		End If 
	
	//[UP.1]5. 	If the Limit Condition Activity Experienced Duration Control for the activity is True Then 
	//[UP.1]5.1. 		If the Activity Progress Status for the activity is True And the Activity Experienced Duration for the activity is greater than or equal (>=) to the Limit Condition Activity Experienced Duration Limitfor the activity Then 
	//[UP.1]5.1.1. 		Exit Limit Conditions Check Process (Limit Condition Violated: True) 	(Limit conditions have been violated)
	//[UP.1]			End If 
	//[UP.1]		End If 
	
	//[UP.1]6. 	If the Limit Condition Attempt Absolute Duration Control for the activity is True Then 
	//[UP.1]6.1.		If the Activity Progress Status for the activity is True And the Attempt Progress Status for the activity is True And the Attempt Absolute Duration for the activity is greater than or equal (>=) to the Limit Condition Attempt Absolute Duration Limit for the activity Then 
	//[UP.1]6.1.1. 		Exit Limit Conditions Check Process (Limit Condition Violated: True) 	(Limit conditions have been violated)
	//[UP.1]			End If 
	//[UP.1]		End If 
	
	//[UP.1]7. 	If the Limit Condition Attempt Experienced Duration Control for the activity is True Then 
	//[UP.1]7.1. 		If the Activity Progress Status for the activity is True And the Attempt Progress Status for the activity is True And the Attempt Experienced Duration for the activity is greater than or equal (>=) to the Limit Condition Attempt Experienced Duration Limit for the activity Then 
	//[UP.1]7.1.1.		Exit Limit Conditions Check Process (Limit Condition Violated: True) 	(Limit conditions have been violated) 
	//[UP.1]			End If 
	//[UP.1]		End If 
	
	//[UP.1]8. 	If the Limit Condition Begin Time Limit Control for the activity is True Then 
	//[UP.1]8.1. 		If the current time point is before the Limit Condition Begin Time Limit for the activity Then 
	//[UP.1]8.1.1. 		Exit Limit Conditions Check Process (Limit Condition Violated: True) 	(Limit conditions have been violated)  
	//[UP.1]			End If 
	//[UP.1]		End If 
	
	//[UP.1]9. 	If the Limit Condition End Time Limit Control for the activity is True Then 
	//[UP.1]9.1. 		If the current time point is after the Limit Condition End Time Limit for the activity Then 
	//[UP.1]9.1.1. 		Exit Limit Conditions Check Process (Limit Condition Violated: True) 	(Limit conditions have been violated)  
	//[UP.1]			End If 
	//[UP.1]		End If 
	
	this.LogSeq("UP.1]10. Exit Limit Conditions Check Process (Limit Condition Violated: False) (No limit conditions have been violated)", logParent);
	this.LogSeqReturn("false", logParent);
	return false;
	
}

