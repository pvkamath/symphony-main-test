//Content Delivery Environment Process [DB.2] -- Subprocess that makes edits to activity data

function Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess(activityForDelivery, logParent) {
	
	if (logParent === undefined || logParent === null) {
		logParent = this.LogSeqAudit('Content Delivery Environment Activity Data SubProcess for ' + activity.StringIdentifier);
	}
	
	var isLaunchAfterClick = (Control.Package.Properties.ScoLaunchType === LAUNCH_TYPE_POPUP_AFTER_CLICK || 
                              Control.Package.Properties.ScoLaunchType === LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR);
	
	if (isLaunchAfterClick) {
		var currentActivity = this.PreviousActivity;
	} else {
		var currentActivity = this.CurrentActivity;
	}
	var suspendedActivity = this.GetSuspendedActivity(logParent);
	var rootActivity = this.GetRootActivity(logParent);

	var activityPath = null;
    var startingFirstAttempt = false;
    
	this.LogSeq("[DB.2]2. If the activity identified for delivery is not equal to the Suspended Activity Then (Content is about to be delivered, clear any existing suspend all state)", logParent);
	if (suspendedActivity != activityForDelivery){
		this.LogSeq("[DB.2]2.1. Apply the Clear Suspended Activity Subprocess to the activity identified for delivery", logParent);
		this.ClearSuspendedActivitySubprocess(activityForDelivery, logParent);
	}
	
	this.LogSeq("[DB.2]3. Apply the Terminate Descendent Attempts Process to the activity identified for delivery (Make sure that all attempts that should end are terminated)", logParent);
	this.TerminateDescendentAttemptsProcess(activityForDelivery, logParent);
	
	this.LogSeq("[DB.2]4.Form the activity path as the ordered series of activities from the root of the activity tree to the activity identified for delivery, inclusive (Begin all attempts required to deliver the identified activity)", logParent);
	activityPath = this.GetPathToAncestorInclusive(activityForDelivery, rootActivity);

	this.LogSeq("[DB.2]5. For each activity in the activity path", logParent);
	var nowDate = ConvertDateToIso8601String(new Date());
	for (var i = (activityPath.length - 1); i >= 0; i--){
		
		this.LogSeq("[DB.2]5.1. If Activity (" + activityPath[i] + ") is Active for the activity is False Then", logParent);
		if (isLaunchAfterClick) {
			var isActive = activityPath[i].WasActiveBeforeLaunchOnClick;
		} else {
			var isActive = activityPath[i].IsActive();
		}
		
		if (isActive === false){
		
			this.LogSeq("[DB.2]5.1.1. If Tracked for the activity is True Then", logParent);
			if (activityPath[i].IsTracked()){
				
				this.LogSeq("[DB.2]5.1.1.1. If Activity is Suspended for the activity is True Then (If the previous attempt on the activity ended due to a suspension, clear the suspended state; do not start a new attempt)", logParent);
				
				if (activityPath[i].IsSuspended()){
				
					this.LogSeq("[DB.2]5.1.1.1.1. Set Activity is Suspended for the activity to False", logParent);
					activityPath[i].SetSuspended(false);
				}
				else{
					this.LogSeq("[DB.2]5.1.1.2. Else", logParent);

					//TODO - check that this is working right, it looks like we might be incrementing parent attmpt counts too much				
					
					this.LogSeq("[DB.2]5.1.1.2.1. Increment the Activity Attempt Count for the activity (Begin a new attempt on the activity)", logParent);
					activityPath[i].IncrementAttemptCount();
					
					this.LogSeq("[DB.2]5.1.1.2.2. If Activity Attempt Count for the activity is equal to One (1) Then (Is this the first attempt on the activity?)", logParent);
					if (activityPath[i].GetAttemptCount() == 1){
						
						this.LogSeq("[DB.2]5.1.1.2.2.1. Set Activity Progress Status for the activity to True", logParent);
						activityPath[i].SetActivityProgressStatus(true);
						
						startingFirstAttempt = true;
					}
					
					this.LogSeq("[DB.2]5.1.1.2.3. Initialize Objective Progress Information and Attempt Progress Information required for the new attempt. Initialize tracking information for the new attempt.", logParent);
					//TODO - improvement - would probably be more efficient to just call this on the common ancestor???? (check how this affects the calling parameters - resetObjectives & resetAttemptState)
					activityPath[i].InitializeForNewAttempt(true, true);
					
					// Reset attempt start timestamp and durations
					var atts = {ev:'AttemptStart', an:activityPath[i].GetAttemptCount(), 
					            ai:activityPath[i].ItemIdentifier, at:activityPath[i].LearningObject.Title}; 
		            this.WriteHistoryLog("", atts);
	
	                activityPath[i].SetAttemptStartTimestampUtc(nowDate);
	                // "PT0H0M0S" is ConvertHundredthsToIso8601TimeSpan(0)
	                activityPath[i].SetAttemptAbsoluteDuration("PT0H0M0S");
	                activityPath[i].SetAttemptExperiencedDurationTracked("PT0H0M0S");
	                activityPath[i].SetAttemptExperiencedDurationReported("PT0H0M0S");

					if (Control.Package.Properties.ResetRunTimeDataTiming == RESET_RT_DATA_TIMING_ON_EACH_NEW_SEQUENCING_ATTEMPT){
						if (activityPath[i].IsDeliverable() === true){
        					
        					//only log the resetting of runtime data if there was previous data to reset
        					if (startingFirstAttempt === false){
        					    var atts = {ev:'ResetRuntime', 
				                            ai:activityPath[i].ItemIdentifier, at:activityPath[i].LearningObject.Title}; 
							    this.WriteHistoryLog("", atts);
							}
							
							activityPath[i].RunTime.ResetState();
						}
					}
					
					//In 3rd Edition we are required to clear all global objectives if obj global to system = false.
					//The trigger we use for this is a new attempt starting on the root of the activity tree	
					this.LogSeq("[DB.2]5.1.1.2.4. If objectives global to system is false (obj global=" + Control.Package.ObjectivesGlobalToSystem + ") and we're starting a new attempt on the root activity.", logParent);
					
					if (Control.Package.ObjectivesGlobalToSystem === false && activityPath[i].IsTheRoot() === true){		
						this.LogSeq("[DB.2]5.1.1.2.4.1. Reset any global objectives and initialize the activity tree for a new attempt.", logParent);
						this.ResetGlobalObjectives();
					}
				}
			}
			
			//indicate that this activity was delivered during the current attempt on the parent so we know which set of
			//data to use according to the Use Current Information sequencing settings
			activityPath[i].SetAttemptedDuringThisAttempt();
		}
	}
}
