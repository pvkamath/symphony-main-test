//[SB.2.12]****************************************************************************************************************

//[SB.2.12]Sequencing Request Process [SB.2.12] 
//[SB.2.12]	For a sequencing request; validates the sequencing request; may return a delivery request; may indicate control be returned to the LTS; may return an exception code
//[SB.2.12]	Reference: 
	//[SB.2.12]Choice Sequencing Request Process SB.2.9
	//[SB.2.12]Continue Sequencing Request Process SB.2.7
	//[SB.2.12]Exit Sequencing Request Process SB.2.11
	//[SB.2.12]Previous Sequencing Request Process SB.2.8
	//[SB.2.12]Resume All Sequencing Request Process SB.2.6
	//[SB.2.12]Retry Sequencing Request Process SB.2.10
	//[SB.2.12]Start Sequencing Request Process SB.2.5.
	
	

function Sequencer_SequencingRequestProcess(sequencingRequest, targetActivity, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Sequencing Request Process [SB.2.12](" + sequencingRequest + ", " + targetActivity + ")", callingLog);
	
	var returnValue;
	
	switch (sequencingRequest){
		
		case SEQUENCING_REQUEST_START:
			
			this.LogSeq("[SB.2.12]1. Case: sequencing request is Start", logParent);
			
			this.LogSeq("[SB.2.12]1.1. Apply the Start Sequencing Request Process", logParent);
			var startSequencingRequestResult = this.StartSequencingRequestProcess(logParent);
			
			this.LogSeq("[SB.2.12]1.2. If the Start Sequencing Request Process returns an exception Then", logParent);
			
			if (startSequencingRequestResult.Exception !== null){
				
				this.LogSeq("[SB.2.12]1.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Start Sequencing Request Process)", logParent);
				
				returnValue = new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID, null, null, startSequencingRequestResult.Exception, startSequencingRequestResult.ExceptionText, false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[SB.2.12]1.3. Else", logParent);
				this.LogSeq("[SB.2.12]1.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Start Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)", logParent);
				
				returnValue = new Sequencer_SequencingRequestProcessResult(sequencingRequest, startSequencingRequestResult.DeliveryRequest, startSequencingRequestResult.EndSequencingSession, null, "", false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		break;
		
		case SEQUENCING_REQUEST_RESUME_ALL:
			
			this.LogSeq("[SB.2.12]2. Case: sequencing request is Resume All", logParent);
			
			this.LogSeq("[SB.2.12]2.1. Apply the Resume All Sequencing Request Process", logParent);
			var resumeAllSequencingRequestResult = this.ResumeAllSequencingRequestProcess(logParent);
			
			this.LogSeq("[SB.2.12]2.2. If the Resume All Sequencing Request Process returns an exception Then", logParent);
			if (resumeAllSequencingRequestResult.Exception !== null){
				
				this.LogSeq("[SB.2.12]2.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Resume All Sequencing Request Process)", logParent);
				returnValue = new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID, null, null, resumeAllSequencingRequestResult.Exception, resumeAllSequencingRequestResult.ExceptionText, false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[SB.2.12]2.3. Else", logParent);
				this.LogSeq("[SB.2.12]2.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Resume All Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)", logParent);

				returnValue = new Sequencer_SequencingRequestProcessResult(sequencingRequest, resumeAllSequencingRequestResult.DeliveryRequest, null, null, "", false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			
		break;
		
		case SEQUENCING_REQUEST_EXIT:
			
			this.LogSeq("[SB.2.12]3. Case: sequencing request is Exit", logParent);
			
			this.LogSeq("[SB.2.12]3.1. Apply the Exit Sequencing Request Process", logParent);
			var exitSequencingRequestResult = this.ExitSequencingRequestProcess(logParent);
			
			this.LogSeq("[SB.2.12]3.2. If the Exit Sequencing Request Process returns an exception Then", logParent);

			if (exitSequencingRequestResult.Exception !== null){

				this.LogSeq("[SB.2.12]3.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Exit Sequencing Request Process)", logParent);
				
				returnValue = new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID, null, null, exitSequencingRequestResult.Exception, exitSequencingRequestResult.ExceptionText, false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[SB.2.12]3.3. Else", logParent);
				this.LogSeq("[SB.2.12]3.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: n/a; End Sequencing Session: the result of the Exit Sequencing Request Process; Exception: n/a)", logParent);

				returnValue = new Sequencer_SequencingRequestProcessResult(sequencingRequest, null, exitSequencingRequestResult.EndSequencingSession, null, "", false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			
		break;
		
		case SEQUENCING_REQUEST_RETRY:
			
			this.LogSeq("[SB.2.12]4. Case: sequencing request is Retry", logParent);
			
			this.LogSeq("[SB.2.12]4.1. Apply the Retry Sequencing Request Process", logParent);
			var retrySequencingRequestResult = this.RetrySequencingRequestProcess(logParent);
			
			if (retrySequencingRequestResult.Exception !== null){
				this.LogSeq("[SB.2.12]4.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Retry Sequencing Request Process)", logParent);
				
				returnValue = new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID, null, null, retrySequencingRequestResult.Exception, retrySequencingRequestResult.ExceptionText, false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
			
				this.LogSeq("[SB.2.12]4.3. Else", logParent);
				this.LogSeq("[SB.2.12]4.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Retry Sequencing Request Process; End Sequencing Session: n/a) ; Exception: n/a)", logParent);
				
				returnValue = new Sequencer_SequencingRequestProcessResult(sequencingRequest, retrySequencingRequestResult.DeliveryRequest, null, null, "", false);		
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		break;
		
		case SEQUENCING_REQUEST_CONTINUE:
			this.LogSeq("[SB.2.12]5. Case: sequencing request is Continue", logParent);
			
			this.LogSeq("[SB.2.12]5.1. Apply the Continue Sequencing Request Process", logParent);
			var continueSequencingRequestResult = this.ContinueSequencingRequestProcess(logParent);

			this.LogSeq("[SB.2.12]5.2. If the Continue Sequencing Request Process returns an exception Then", logParent);
			
			if (continueSequencingRequestResult.Exception !== null){
				this.LogSeq("[SB.2.12]5.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: Value from Continue Sequencing Request Process; Exception: the exception identified by the Continue Sequencing Request Process)", logParent);
				
				returnValue = new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID, null, continueSequencingRequestResult.EndSequencingSession, continueSequencingRequestResult.Exception, continueSequencingRequestResult.ExceptionText, false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[SB.2.12]5.3. Else", logParent);
				this.LogSeq("[SB.2.12]5.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Continue Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)", logParent);
				
				returnValue = new Sequencer_SequencingRequestProcessResult(sequencingRequest, continueSequencingRequestResult.DeliveryRequest, continueSequencingRequestResult.EndSequencingSession, null, "", false);	
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
				
			}
		break;
		
		case SEQUENCING_REQUEST_PREVIOUS:
			
			this.LogSeq("[SB.2.12]6. Case: sequencing request is Previous", logParent);
			
			this.LogSeq("[SB.2.12]6.1. Apply the Previous Sequencing Request Process", logParent);
			var previousSequencingRequestResult = this.PreviousSequencingRequestProcess(logParent);
			
			this.LogSeq("[SB.2.12]6.2. If the Previous Sequencing Request Process returns an exception Then", logParent);
			if (previousSequencingRequestResult.Exception !== null){
				this.LogSeq("[SB.2.12]6.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Previous Sequencing Request Process)", logParent);

				returnValue = new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID, null, previousSequencingRequestResult.EndSequencingSession, previousSequencingRequestResult.Exception, previousSequencingRequestResult.ExceptionText, false);			
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[SB.2.12]6.3. Else", logParent);
				this.LogSeq("[SB.2.12]6.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Previous Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)", logParent);
				
				returnValue = new Sequencer_SequencingRequestProcessResult(sequencingRequest, previousSequencingRequestResult.DeliveryRequest, previousSequencingRequestResult.EndSequencingSession, null, "", false);				
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		break;
		
		case SEQUENCING_REQUEST_CHOICE:
			
			this.LogSeq("[SB.2.12]7. Case: sequencing request is Choice", logParent);
			
			this.LogSeq("[SB.2.12]7.1. Apply the Choice Sequencing Request Process", logParent);
			var choiceSequencingRequestResult = this.ChoiceSequencingRequestProcess(targetActivity, logParent);
			
			this.LogSeq("[SB.2.12]7.2. If the Choice Sequencing Request Process returns an exception Then", logParent);
			if (choiceSequencingRequestResult.Exception !== null){
				this.LogSeq("[SB.2.12]7.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Choice Sequencing Request Process)", logParent);

				returnValue = new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID, null, null, choiceSequencingRequestResult.Exception, choiceSequencingRequestResult.ExceptionText, choiceSequencingRequestResult.Hidden);	
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[SB.2.12]7.3. Else", logParent);
				this.LogSeq("[SB.2.12]7.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Choice Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)", logParent);
				
				returnValue = new Sequencer_SequencingRequestProcessResult(sequencingRequest, choiceSequencingRequestResult.DeliveryRequest, null, null, "", choiceSequencingRequestResult.Hidden);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		break;
		
	}
	
	this.LogSeq("[SB.2.12]8.Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: SB.2.12-1) (Invalid sequencing request)", logParent);
	
	returnValue = new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID, null, null, "SB.2.12-1", "The sequencing request (" + sequencingRequest + ") is not recognized.", false);
	this.LogSeqReturn(returnValue, logParent);
	return returnValue;
}


function Sequencer_SequencingRequestProcessResult(sequencingRequest, deliveryRequest, endSequencingSession, exception, exceptionText, hidden){
	
	Debug.AssertError("undefined hidden value", (hidden === undefined));
	Debug.AssertError("Invalid endSequencingSession (" + endSequencingSession + ") passed to SequencingRequestProcessResult.", (endSequencingSession != null && endSequencingSession != true && endSequencingSession != false))
	
	this.SequencingRequest = sequencingRequest;
	this.DeliveryRequest = deliveryRequest;
	this.EndSequencingSession = endSequencingSession;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	this.Hidden = hidden;
	
}

Sequencer_SequencingRequestProcessResult.prototype.toString = function(){
									return "SequencingRequest=" + this.SequencingRequest + 
										", DeliveryRequest=" + this.DeliveryRequest +
										", EndSequencingSession=" + this.EndSequencingSession +
										", Exception=" + this.Exception +
										", ExceptionText=" + this.ExceptionText +
										", Hidden=" + this.Hidden;
									};