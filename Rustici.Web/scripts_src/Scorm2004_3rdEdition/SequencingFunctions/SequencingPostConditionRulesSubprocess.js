//****************************************************************************************************************

//Sequencing Post Condition Rules Subprocess [TB.2.2]

//	For the Current Activity; may return a termination request and a sequencing request): 

//	Reference: 
		//Activity is Suspended AM.1.1
		//Current Activity AM.1.2
		//Sequencing Rules Check Process UP.2
		//Sequencing Rule Description SM.2 

function Sequencer_SequencingPostConditionRulesSubprocess(callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Sequencing Post Condition Rules Subprocess [TB.2.2]", callingLog);
	
	var returnValue;
	
	this.LogSeq("[TB.2.2]1. If Activity is Suspended for the Current Activity is True Then (Do not apply post condition rules to a suspended activity)", logParent);
	
	var currentActivity = this.GetCurrentActivity();
	
	if (currentActivity.IsSuspended()){
		this.LogSeq("[TB.2.2]1.1. Exit Sequencing Post Condition Rules Subprocess", logParent);
		returnValue = new Sequencer_SequencingPostConditionRulesSubprocessResult(null, null);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	
	this.LogSeq("[TB.2.2]2. Apply the Sequencing Rules Check Process to the Current Activity and the set of Post Condition actions (Apply the post condition rules to the current activity)", logParent);
	var ruleAction = this.SequencingRulesCheckProcess(currentActivity, RULE_SET_POST_CONDITION, logParent);
	
	this.LogSeq("[TB.2.2]3. If the Sequencing Rules Check Process does not return Nil Then", logParent);
	
	if (ruleAction !== null){
	
		this.LogSeq("[TB.2.2]3.1. If the Sequencing Rules Check Process returned Retry, Continue, Or Previous Then", logParent);
		
		if (ruleAction == SEQUENCING_RULE_ACTION_RETRY  || 
			ruleAction == SEQUENCING_RULE_ACTION_CONTINUE || 
			ruleAction == SEQUENCING_RULE_ACTION_PREVIOUS){
			
			this.LogSeq("[TB.2.2]3.1.1. Exit Sequencing Post Condition Rules Subprocess (Sequencing Request: the value returned by the Sequencing Rules Check Process; Termination Request: n/a) (Attempt to override any pending sequencing request with this one)", logParent);
			
			returnValue = new Sequencer_SequencingPostConditionRulesSubprocessResult(this.TranslateSequencingRuleActionIntoSequencingRequest(ruleAction), null);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[TB.2.2]3.2. If the Sequencing Rules Check Process returned Exit Parent Or Exit All Then", logParent);
	
		if (ruleAction == SEQUENCING_RULE_ACTION_EXIT_PARENT  || 
			ruleAction == SEQUENCING_RULE_ACTION_EXIT_ALL){
			
			this.LogSeq("[TB.2.2]3.2.1. Exit Sequencing Post Condition Rules Subprocess (Sequencing Request: n/a; Termination Request: the value returned by the Sequencing Rules Check Process) (Terminate the appropriate activity(s))", logParent);

			returnValue = new Sequencer_SequencingPostConditionRulesSubprocessResult(null, this.TranslateSequencingRuleActionIntoTerminationRequest(ruleAction));
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[TB.2.2]3.3. If the Sequencing Rules Check Process returned Retry All Then", logParent);
		if (ruleAction == SEQUENCING_RULE_ACTION_RETRY_ALL){
			this.LogSeq("[TB.2.2]3.3.1. Exit Sequencing Post Condition Rules Subprocess (Termination Request: Exit All; Sequencing Request: Retry) (Terminate all active activities and move the current activity to the root of the activity tree; then perform an 'in-process' start)", logParent);

			returnValue = new Sequencer_SequencingPostConditionRulesSubprocessResult(SEQUENCING_REQUEST_RETRY, TERMINATION_REQUEST_EXIT_ALL);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
	}
	
	this.LogSeq("[TB.2.2]4. Exit Sequencing Post Condition Rules Subprocess (Sequencing Request: n/a; Sequencing Request; n/a)", logParent);

	returnValue = new Sequencer_SequencingPostConditionRulesSubprocessResult(null, null);
	this.LogSeqReturn(returnValue, logParent);
	return returnValue;
}


function Sequencer_SequencingPostConditionRulesSubprocessResult(sequencingRequest, terminationRequest){
	this.TerminationRequest = terminationRequest;
	this.SequencingRequest = sequencingRequest;

}

Sequencer_SequencingPostConditionRulesSubprocessResult.prototype.toString =  function(){
									return "TerminationRequest=" + this.TerminationRequest + 
										", SequencingRequest=" + this.SequencingRequest;
									};