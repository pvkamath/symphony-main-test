function RunTimeApi(learnerId, learnerName){
	this.RunTimeData = null;
	this.LearningObject = null;
	this.Activity = null;
}

//public interface exposed to controller
RunTimeApi.prototype.GetNavigationRequest = RunTimeApi_GetNavigationRequest;
RunTimeApi.prototype.ResetState = RunTimeApi_ResetState;
RunTimeApi.prototype.InitializeForDelivery = RunTimeApi_InitializeForDelivery;
RunTimeApi.prototype.SetDirtyData = RunTimeApi_SetDirtyData;
RunTimeApi.prototype.WriteHistoryLog = RunTimeApi_WriteHistoryLog;
RunTimeApi.prototype.WriteHistoryReturnValue = RunTimeApi_WriteHistoryReturnValue;
RunTimeApi.prototype.WriteAuditLog = RunTimeApi_WriteAuditLog;
RunTimeApi.prototype.WriteAuditReturnValue = RunTimeApi_WriteAuditReturnValue;
RunTimeApi.prototype.WriteDetailedLog = RunTimeApi_WriteDetailedLog;
RunTimeApi.prototype.CloseOutSession = RunTimeApi_CloseOutSession;
RunTimeApi.prototype.NeedToCloseOutSession = RunTimeApi_NeedToCloseOutSession;
RunTimeApi.prototype.AccumulateTotalTimeTracked = RunTimeApi_AccumulateTotalTrackedTime;
RunTimeApi.prototype.InitTrackedTimeStart = RunTimeApi_InitTrackedTimeStart;

function RunTimeApi_GetNavigationRequest(){
	return null;
}
function RunTimeApi_ResetState(){
}
function RunTimeApi_InitializeForDelivery(activity){
	this.RunTimeData = activity.RunTime;
	this.LearningObject = activity.LearningObject;
	this.Activity = activity;	
}
function RunTimeApi_SetDirtyData(){
}
function RunTimeApi_WriteHistoryLog(str, atts){
}
function RunTimeApi_WriteHistoryReturnValue(str, atts){
}
function RunTimeApi_WriteAuditLog(str){
}
function RunTimeApi_WriteAuditReturnValue(str){
}
function RunTimeApi_WriteDetailedLog(str){
}
function RunTimeApi_CloseOutSession(){
}
function RunTimeApi_NeedToCloseOutSession(){
	return false;
}
function RunTimeApi_InitTrackedTimeStart() {
}
function RunTimeApi_AccumulateTotalTrackedTime() {
}