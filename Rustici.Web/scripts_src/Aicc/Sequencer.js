
function Sequencer(lookAhead, activities){
	
	this.LookAhead = lookAhead;
	this.Activities = activities;
	
	this.NavigationRequest = null;
	this.SuspendedActivity = null;
	this.CurrentActivity = null;
	
	this.ExceptionText = "";
	
}

Sequencer.prototype.OverallSequencingProcess = Sequencer_OverallSequencingProcess;
Sequencer.prototype.SetSuspendedActivity = Sequencer_SetSuspendedActivity;
Sequencer.prototype.GetSuspendedActivity = Sequencer_GetSuspendedActivity;
Sequencer.prototype.Start = Sequencer_Start;
Sequencer.prototype.InitialRandomizationAndSelection = Sequencer_InitialRandomizationAndSelection;
Sequencer.prototype.GetCurrentActivity = Sequencer_GetCurrentActivity;
Sequencer.prototype.GetExceptionText = Sequencer_GetExceptionText;
Sequencer.prototype.GetExitAction = Sequencer_GetExitAction;
Sequencer.prototype.EvaluatePossibleNavigationRequests = Sequencer_EvaluatePossibleNavigationRequests;
Sequencer.prototype.InitializePossibleNavigationRequestAbsolutes = Sequencer_InitializePossibleNavigationRequestAbsolutes;
Sequencer.prototype.ContentDeliveryEnvironmentActivityDataSubProcess = Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess;

function Sequencer_OverallSequencingProcess(){
	
	if (this.NavigationRequest === null){
		
		var exitAction = this.GetExitAction(this.GetCurrentActivity());
		
		switch (exitAction){

			case (EXIT_ACTION_EXIT_NO_CONFIRMATION):
				this.NavigationRequest = new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL, null, "");
			break;
			
			case (EXIT_ACTION_EXIT_CONFIRMATION):
				Debug.AssertError("EXIT_ACTION_EXIT_CONFIRMATION not supported for AICC");
			break;
			
			case (EXIT_ACTION_GO_TO_NEXT_SCO):
				Debug.AssertError("EXIT_ACTION_GO_TO_NEXT_SCO not supported for AICC");
			break;
			
			case (EXIT_ACTION_DISPLAY_MESSAGE):
				Debug.AssertError("EXIT_ACTION_DISPLAY_MESSAGE not supported for AICC");
			break;
			
			case (EXIT_ACTION_DO_NOTHING):
				Debug.AssertError("EXIT_ACTION_DO_NOTHING not supported for AICC");
			break;
			
			case (EXIT_ACTION_REFRESH_PAGE):
				Control.RefreshPage();
			break;
		}		
	}
	
	if (this.NavigationRequest === null){
		this.ExceptionText = IntegrationImplementation.GetString("Please make a selection.");
	}
	else{
		//only choice nav is currently supported for AICC
		
		//TODO: add handlers for exit nav requests
			
		if (this.NavigationRequest.Type == NAVIGATION_REQUEST_CHOICE){
			
			var activityToDeliver = this.Activities.GetActivityFromIdentifier(this.NavigationRequest.TargetActivity);
			
			if (activityToDeliver === null || activityToDeliver.IsDeliverable() === false){
				Debug.AssertError("Selected an activity that does not exist or is not deliverable. Activity=" + activityToDeliver);
			}
			
			this.CurrentActivity = activityToDeliver;
			
			var sortedActivityList = this.Activities.SortedActivityList;
	
			for (var i=0; i < sortedActivityList.length; i++){
				sortedActivityList[i].SetActive(false);		
			}
			
			var activeParents = this.Activities.GetActivityPath(activityToDeliver, true);
			for (var i=0; i < activeParents.length; i++){
				activeParents[i].SetActive(true);		
			}
			
			if (Control.Package.Properties.ScoLaunchType !== LAUNCH_TYPE_POPUP_AFTER_CLICK &&
                    Control.Package.Properties.ScoLaunchType !== LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR) {
				this.ContentDeliveryEnvironmentActivityDataSubProcess();
			}
			
			Control.DeliverActivity(activityToDeliver);	
		}
		else if(this.NavigationRequest.Type == NAVIGATION_REQUEST_EXIT){
			//do nothing, the SCO should already be unloaded before the sequencing process is invoked
		}
		else if(this.NavigationRequest.Type == NAVIGATION_REQUEST_SUSPEND_ALL || this.NavigationRequest.Type == NAVIGATION_REQUEST_EXIT_ALL || this.NavigationRequest.Type == NAVIGATION_REQUEST_EXIT_PLAYER){
			Control.ExitScormPlayer();
		}
		else{
			Debug.AssertError("Only choice navigation requests are supported for AICC., Received type-" + this.NavigationRequest.Type);
			this.ExceptionText = IntegrationImplementation.GetString("Please make a selection.");
		}
			
	}
}



function Sequencer_SetSuspendedActivity(activity){
	this.SuspendedActivity = activity;
}

function Sequencer_GetSuspendedActivity(){
	return this.SuspendedActivity;
}

function Sequencer_Start(){
	
	//if there is a single SCO, launch it, otherwise display a message
	
	if (this.Activities.GetNumDeliverableActivities() == 1){
		for (var i=0; i < this.Activities.SortedActivityList.length; i++){
			if (this.Activities.SortedActivityList[i].IsDeliverable()){
				
				this.SuspendedActivity = null;
				this.CurrentActivity = this.Activities.SortedActivityList[i];
				Control.DeliverActivity(this.Activities.SortedActivityList[i]);	
				
				return;
			}	
		}
	}
	else{
		this.ExceptionText = IntegrationImplementation.GetString("Please make a selection.");
	}
}

function Sequencer_InitialRandomizationAndSelection(){
}

function Sequencer_GetCurrentActivity(){
	return this.CurrentActivity;
}



function Sequencer_GetExceptionText(){
	return this.ExceptionText;
}

function Sequencer_GetExitAction(activity){
	//if single sco exit no confirm
	//else, refresh status and display message page
	
	if (this.Activities.GetNumDeliverableActivities() == 1){
		return EXIT_ACTION_EXIT_NO_CONFIRMATION;
	}
	else{
		return EXIT_ACTION_REFRESH_PAGE;
	}
	
}

function Sequencer_EvaluatePossibleNavigationRequests(aryPossibleRequests, activityTree, activityList){
	
	//look at package properties to see what should and should not be enabled
	var packageProperties = Control.Package.Properties;

	aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_START].WillSucceed = true;
	aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_RESUME_ALL].WillSucceed = true;
	aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed = false;
	aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed = false;
	aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed = true;
	aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].WillSucceed = true;
	aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL].WillSucceed = true;
	aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON].WillSucceed = true;
	aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON_ALL].WillSucceed = true;
	
	var activity;
	for (var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE; i < aryPossibleRequests.length; i++){
		
		activity = this.Activities.GetActivityFromIdentifier(aryPossibleRequests[i].TargetActivityItemIdentifier);
		
		aryPossibleRequests[i].WillSucceed = (activity.IsDeliverable() && packageProperties.EnableChoiceNav === true);
	}
	
	return aryPossibleRequests;
}

function Sequencer_InitializePossibleNavigationRequestAbsolutes(aryPossibleRequests){
	
}

function Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess() {	
	this.SuspendedActivity = null;
}
