//****************************************************************************************************************

//Sequencing Exit Action Rules Subprocess [TB.2.1]

//	For the Current Activity; may change the Current Activity

//	Reference: 
		//Current Activity AM.1.2 
		//End Attempt Process UP.4
		//Sequencing Rules Check Process UP.2
		//Sequencing Rule Description SM.2
		//Terminate Descendent Attempts Process UP.3 
		
function Sequencer_SequencingExitActionRulesSubprocess(callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Sequencing Exit Action Rules Subprocess [TB.2.1]", callingLog);
	
	this.LogSeq("[TB.2.1]1. Form the activity path as the ordered series of activities from the root of the activity tree to the parent of the Current Activity, inclusive", logParent);
	
	var currentActivity = this.GetCurrentActivity();
	var parentActivity = this.Activities.GetParentActivity(currentActivity);
	
	var aryParentActivities;
	
	if (parentActivity !== null){
		aryParentActivities = this.GetActivityPath(parentActivity, true);
	}
	else{
		//if the parent activity is null, then the current activity must be the root, so use it
		aryParentActivities = this.GetActivityPath(currentActivity, true);
	}
	
	this.LogSeq("[TB.2.1]2. Initialize exit target to Null", logParent);
	var exitTarget = null;
	var ruleCheckReturnValue = null;
	
	this.LogSeq("[TB.2.1]3. For each activity in the activity path (Evaluate all exit rules along the active path, starting at the root of the activity tree)", logParent);
	for (var i = (aryParentActivities.length - 1); i >= 0 ; i--){
	
		this.LogSeq("[TB.2.1]3.1. Apply the Sequencing Rules Check Process to the activity and the set of Exit actions", logParent);
		ruleCheckReturnValue = this.SequencingRulesCheckProcess(aryParentActivities[i], RULE_SET_EXIT, logParent);
		
		this.LogSeq("[TB.2.1]3.2. If the Sequencing Rules Check Process does not return Nil Then", logParent);
		
		if (ruleCheckReturnValue !== null){
			
			this.LogSeq("[TB.2.1]3.2.1. Set the exit target to the activity (Stop at the first activity that has an exit rule evaluating to true)", logParent);
			exitTarget = aryParentActivities[i];
			
			this.LogSeq("[TB.2.1]3.2.2. Break For", logParent);
			break;
			
		}
	}
	
	this.LogSeq("[TB.2.1]4. If exit target is Not Null Then", logParent);
	if (exitTarget !== null){
		
		this.LogSeq("[TB.2.1]4.1. Apply the Terminate Descendent Attempts Process to the exit target (End the current attempt on all active descendents)", logParent);
		this.TerminateDescendentAttemptsProcess(exitTarget, logParent);
		
		this.LogSeq("[TB.2.1]4.2. Apply the End Attempt Process to the exit target (End the current attempt on the 'exiting' activity)", logParent);
		this.EndAttemptProcess(exitTarget, false, logParent);
		
		this.LogSeq("[TB.2.1]4.3. Set the Current Activity to the exit target (Move the current activity to the activity that identified for termination)", logParent);
		this.SetCurrentActivity(exitTarget, logParent);
	}
	
	this.LogSeq("[TB.2.1]5. Exit Sequencing Exit Action Rules Subprocess", logParent);
	this.LogSeqReturn("", logParent);
	return;

}
