//****************************************************************************************************************

//Objective Rollup Using Measure Process [RB.1.2 a]

//	For an activity; may change the Objective Information for the activity

//For the primary objective of an activity, this process changes the success status if appropriate based on the normalized measure

//	Reference: 
		//Objective Contributes to Rollup SM.6; 
		//Objective Description SM.6; 
		//Objective Satisfied by Measure SM.6; 
		//Objective Measure Status TM.1.1; 
		//Objective Normalized Measure TM.1.1; 
		//Objective Progress Status TM.1.1; 
		//Objective Satisfied Status TM.1.1; 
		//Activity is Active AM.1.1; 
		//adlseq:measureSatisfactionIfActive SCORM SN. 


function Sequencer_ObjectiveRollupUsingMeasureProcess(activity, callingLog){

	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Objective Rollup Using Measure Process [RB.1.2 a](" + activity + ")", callingLog);
	
	this.LogSeq("[RB.1.2 a]1. Set the target objective to Undefined", logParent);
	var targetObjective = null;
	
	/*
	this.LogSeq("[RB.1.2 a]2. For each objective associated with the activity", logParent);
	var objectives = activity.GetObjectives();
	for (var i=0; i < objectives.length; i++){
	
		this.LogSeq("[RB.1.2 a]2.1. If Objective Contributes to Rollup for the objective is True Then (Identify the objective that may be altered based on the activity's children's rolled up measure)", logParent);
		if (objectives[i].GetContributesToRollup() === true){
		
			this.LogSeq("[RB.1.2 a]2.1.1. Set the target objective to the objective", logParent);
			targetObjective = objectives[i];
			
			this.LogSeq("[RB.1.2 a]2.1.2. Break For", logParent);
			break;
		}
	}
	*/
	
	this.LogSeq("[RB.1.2 a]2. Get the primary objective (For each objective associated with the activity)", logParent);
	var targetObjective = activity.GetPrimaryObjective();
	
	this.LogSeq("[RB.1.2 a]3. If target objective is Defined Then", logParent);
	if (targetObjective !== null){
	
		this.LogSeq("[RB.1.2 a]3.1. If Objective Satisfied by Measure for the target objective is True Then (If the objective is satisfied by measure, test the rolled-up measure against the defined threshold)", logParent);
		if (targetObjective.GetSatisfiedByMeasure() === true){
		
			this.LogSeq("[RB.1.2 a]3.1.1. If the Objective Measure Status for the target objective is False Then (No Measure known, so objective status is unreliable)", logParent);
			if (targetObjective.GetMeasureStatus(activity, false) === false){
			
				this.LogSeq("[RB.1.2 a]3.1.1.1. Set the Objective Progress Status for the target objective to False", logParent);
				targetObjective.SetProgressStatus(false, false, activity);
			}
			
			else{
				this.LogSeq("[RB.1.2 a]3.1.2. Else", logParent);
				
				this.LogSeq("[RB.1.2 a]3.1.2.1. If Activity is Active for the activity is False Or (Activity is Active for the activity is True And adlseq:measureSatisfactionIfActive for the activity is True ) Then", logParent);
				
				//note, second check for "activity is active is true" is redundent b/c if we get into the OR, it must be true
				if ( 
						activity.IsActive() === false ||
						activity.GetMeasureSatisfactionIfActive() === true){
					
					this.LogSeq("[RB.1.2 a]3.1.2.1.1. If the Objective Normalized Measure for the target objective is greater than or equal (>=) to the Objective Minimum Satisfied Normalized Measure for the target objective Then", logParent);
					if (targetObjective.GetNormalizedMeasure(activity, false) >= targetObjective.GetMinimumSatisfiedNormalizedMeasure()){
					
						this.LogSeq("[RB.1.2 a]3.1.2.1.1.1. Set the Objective Progress Status for the target objective to True", logParent);
						targetObjective.SetProgressStatus(true, false, activity);
						
						this.LogSeq("[RB.1.2 a]3.1.2.1.1.2. Set the Objective Satisfied Status for the target objective to True", logParent);
						targetObjective.SetSatisfiedStatus(true, false, activity);
					}
					else{
						this.LogSeq("[RB.1.2 a]3.1.2.1.2. Else", logParent);
						
						this.LogSeq("[RB.1.2 a]3.1.2.1.2.1. Set the Objective Progress Status for the target objective to True", logParent);
						targetObjective.SetProgressStatus(true, false, activity);
						
						this.LogSeq("[RB.1.2 a]3.1.2.1.2.2. Set the Objective Satisfied Status for the target objective to False", logParent);
						targetObjective.SetSatisfiedStatus(false, false, activity);
					}
				}
				else{
					this.LogSeq("[RB.1.2 a]3.1.2.2. Else", logParent);
					this.LogSeq("[RB.1.2 a]3.1.2.2.1. Set the Objective Progress Status for the target objective to False (Incomplete information, do not evaluate objective status)", logParent);
					targetObjective.SetProgressStatus(false, false, activity);
				}
			}
		}
		
		this.LogSeq("[RB.1.2 a]3.2. Exit Objective Rollup Using Measure Process", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	else{
		this.LogSeq("[RB.1.2 a]4. Else", logParent);
		this.LogSeq("[RB.1.2 a]4.1. Exit Objective Rollup Using Measure Process (No objective contributes to rollup, so we cannot set anything)", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}

}


