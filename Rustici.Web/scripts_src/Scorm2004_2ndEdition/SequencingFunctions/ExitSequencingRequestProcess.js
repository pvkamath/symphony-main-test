//****************************************************************************************************************

//Exit Sequencing Request Process [SB.2.11]
	//Indicates if the sequencing session has ended; may return an exception code
	//Reference: 
		//Activity is Active AM.1.1
		//Current Activity AM.1.2 

function Sequencer_ExitSequencingRequestProcess(callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Exit Sequencing Request Process [SB.2.11]", callingLog);
	
	var returnValue;
	
	this.LogSeq("[SB.2.11]1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)", logParent);
	
	if ( ! this.IsCurrentActivityDefined(logParent)){
		
		this.LogSeq("[SB.2.11]1.1. Exit Exit Sequencing Request Process (End Sequencing Session: False; Exception: SB.2.11-1)", logParent);
		
		returnValue = new Sequencer_ExitSequencingRequestProcessResult(false, "SB.2.11-1", IntegrationImplementation.GetString("An 'Exit Sequencing' request cannot be processed until the sequencing session has begun."));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	var currentActivity = this.GetCurrentActivity();
	
	this.LogSeq("[SB.2.11]2. If the Activity is Active for the Current Activity is True Then (Make sure the current activity has already been terminated)", logParent);
	if (currentActivity.IsActive()){
		
		this.LogSeq("[SB.2.11]2.1. Exit Exit Sequencing Request Process (End Sequencing Session: False; Exception: SB.2.11-2)", logParent);
		
		returnValue = new Sequencer_ExitSequencingRequestProcessResult(false, "SB.2.11-2", IntegrationImplementation.GetString("An 'Exit Sequencing' request cannot be processed while an activity is still active."));	
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[SB.2.11]3. If the Current Activity is the root of the activity tree Then", logParent);
	//added an implementation specific check for a single sco and process an exit all whenever the single sco is exited
	if (currentActivity.IsTheRoot() || this.CourseIsSingleSco() === true){
		this.LogSeq("[SB.2.11]3.1. Exit Exit Sequencing Request Process (End Sequencing Session: True; Exception: n/a) (The sequencing session has ended, return control to the LTS)", logParent);
		
		returnValue = new Sequencer_ExitSequencingRequestProcessResult(true, null, "");
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[SB.2.11]4. Exit Exit Sequencing Request Process (End Sequencing Session: False; Exception: n/a)", logParent);
	
	returnValue = new Sequencer_ExitSequencingRequestProcessResult(false, null, "");
	this.LogSeqReturn(returnValue, logParent);
	return returnValue;
	
}


function Sequencer_ExitSequencingRequestProcessResult(endSequencingSession, exception, exceptionText){
	this.EndSequencingSession = endSequencingSession;
	this.Exception = exception;
	this.ExceptionText = exceptionText;

}

Sequencer_ExitSequencingRequestProcessResult.prototype.toString = function(){
									return "EndSequencingSession=" + this.EndSequencingSession + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText;
									};