//****************************************************************************************************************

//Objective RollupProcess [RB.1.2] 

//	Reference: 

function Sequencer_ObjectiveRollupProcess(activity, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Objective RollupProcess [RB.1.2](" + activity + ")", callingLog);
	
	//select the appropriate objective rollup process using the rules defined on SN-4-38 - this pseudo code is not defined in the spec, just the logic
	
	//get the primary objetive
	
	this.LogSeq("[RB.1.2]1. Set the target objective to Undefined", logParent);
	var targetObjective = null;
	
	/*
	this.LogSeq("[RB.1.2]2. For each objective associated with the activity", logParent);
	var objectives = activity.GetObjectives();
	for (var i=0; i < objectives.length; i++){
	
		this.LogSeq("[RB.1.2]2.1. If Objective Contributes to Rollup for the objective is True Then (Identify the objective that may be altered based on the activity's children's rolled up measure)", logParent);
		if (objectives[i].GetContributesToRollup() === true){
		
			this.LogSeq("[RB.1.2]2.1.1. Set the target objective to the objective", logParent);
			targetObjective = objectives[i];
			
			this.LogSeq("[RB.1.2]2.1.2. Break For", logParent);
			break;
		}
	}
	*/
	this.LogSeq("[RB.1.2]2. Get the primary objective (For each objective associated with the activity)", logParent);
	var targetObjective = activity.GetPrimaryObjective();
	
	
	this.LogSeq("[RB.1.2 a]3. If target objective is Defined Then", logParent);
	if (targetObjective !== null){

	
		this.LogSeq("[RB.1.2]3.1. If Objective Satisfied By Measure for the target objective is True", logParent);
		if (targetObjective.GetSatisfiedByMeasure() === true){
			
			this.LogSeq("[RB.1.2]3.1.1 Invoke the Objective Rollup Process Using Measure", logParent);
			this.ObjectiveRollupUsingMeasureProcess(activity, logParent);
			
			this.LogSeq("[RB.1.2]3.1.2 Exit Objective Roll up Process", logParent);
			this.LogSeqReturn("", logParent);
			return;
		}
		
		this.LogSeq("[RB.1.2]3.2 If the Activity has rollup rules that have the action Satisfied or Not Satisfied", logParent);

		
		if (Sequencer_GetApplicableSetofRollupRules(activity, RULE_SET_SATISFIED).length > 0 ||  
			Sequencer_GetApplicableSetofRollupRules(activity, RULE_SET_NOT_SATISFIED).length > 0){
			
			this.LogSeq("[RB.1.2]3.2.1 Invoke the Objective Rollup Process Using Rules", logParent);
			this.ObjectiveRollupUsingRulesProcess(activity, logParent);
			
			this.LogSeq("[RB.1.2]3.2.2 Exit Objective Roll up Process", logParent);
			this.LogSeqReturn("", logParent);
			return;
		}
		
		this.LogSeq("[RB.1.2]3.3 Invoke the Objective Rollup Process Using Default - Neither Sub Process is Applicable, so use the Default Rules", logParent);
		this.ObjectiveRollupUsingDefaultProcess(activity, logParent);
		
		this.LogSeq("[RB.1.2]3.4 Exit Objective Roll up Process", logParent);
		this.LogSeqReturn("", logParent);
		return;
	
	}
	
	else{
		this.LogSeq("[RB.1.2 a]4. Else", logParent);
		this.LogSeq("[RB.1.2 a]4.1. Exit Objective Roll up Process (No objective contributes to rollup, so we cannot set anything)", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	
}

