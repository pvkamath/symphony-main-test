//****************************************************************************************************************
//Previous Sequencing Request Process [SB.2.8] 
	//May return a delivery request; may return an exception code
	//Reference: 
		//Current Activity AM.1.2
		//Flow Subprocess SB.2.3 
		
function Sequencer_PreviousSequencingRequestProcess(callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Previous Sequencing Request Process [SB.2.8]", callingLog);
	
	var returnValue;
	
	this.LogSeq("[SB.2.8]1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)", logParent);
	if ( ! this.IsCurrentActivityDefined(logParent)){
		
		this.LogSeq("[SB.2.8]1.1. Exit Previous Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.8-1 ) (Nothing to deliver)", logParent);
		
		returnValue = new Sequencer_PreviousSequencingRequestProcessResult(null, "SB.2.8-1", IntegrationImplementation.GetString("You cannot use 'Previous' at this time."));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	var currentActivity = this.GetCurrentActivity();
	
	this.LogSeq("[SB.2.8]2. If the activity is not the root activity of the activity tree Then", logParent);
	if ( ! currentActivity.IsTheRoot()){
		
		var parentActivity = this.Activities.GetParentActivity(currentActivity);
		
		this.LogSeq("[SB.2.8]2.1. If Sequencing Control Flow for the parent of the activity is False Then (Confirm a 'flow' traversal is allowed from the activity)", logParent);
		
		if (parentActivity.GetSequencingControlFlow() === false){
			this.LogSeq("[SB.2.8]2.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: Nil; Exception: SB.2.8-2)", logParent);
			
			returnValue = new Sequencer_PreviousSequencingRequestProcessResult(null, "SB.2.8-2", IntegrationImplementation.GetString("Please select 'Next' or 'Previous' to move through {0}.", parentActivity.GetTitle()));
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		} 
	}
	
	
	this.LogSeq("[SB.2.8]3. Apply the Flow Subprocess to the Current Activity in the Backward direction with consider children equal to False (Flow in a backward direction to the next allowed activity)", logParent);
	
	var flowSubProcessResult = this.FlowSubprocess(currentActivity, FLOW_DIRECTION_BACKWARD, false, logParent);

	this.LogSeq("[SB.2.8]4. If the Flow Subprocess returns False Then", logParent);
	
	if (flowSubProcessResult.Deliverable === false){
		this.LogSeq("[SB.2.8]4.1. Exit Previous Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Flow Subprocess) (Nothing to deliver)", logParent);
		
		returnValue = new Sequencer_PreviousSequencingRequestProcessResult(null, flowSubProcessResult.Exception, flowSubProcessResult.ExceptionText);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	else{
		this.LogSeq("[SB.2.8]5. Else", logParent);
		this.LogSeq("[SB.2.8]5.1. Exit Previous Sequencing Request Process (Delivery Request: the activity identified by the Flow Subprocess; Exception: n/a)", logParent);
		
		returnValue = new Sequencer_PreviousSequencingRequestProcessResult(flowSubProcessResult.IdentifiedActivity, null, "");
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
		
	}
}

function Sequencer_PreviousSequencingRequestProcessResult(deliveryRequest, exception, exceptionText){
	this.DeliveryRequest = deliveryRequest;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
}

Sequencer_PreviousSequencingRequestProcessResult.prototype.toString = function(){
									return "DeliveryRequest=" + this.DeliveryRequest + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText;
									};