//****************************************************************************************************************

//Sequencing Rules Check Process [UP.2]
	//For an activity and a set of Rule Actions; returns the action to apply or Nil
	//Reference: 
		//Rule Action SM.2
		//Sequencing Rule Check Subprocess UP.2.1
		//Sequencing Rule Description SM.2 
	
function Sequencer_SequencingRulesCheckProcess(activity, ruleSet, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Sequencing Rules Check Process [UP.2](" + activity + ", " + ruleSet + ")", callingLog);
	
	var subprocessResult;
	
	var applicableSequencingRules = Sequencer_GetApplicableSetofSequencingRules (activity, ruleSet);
	
	this.LogSeq("[UP.2]1. If the activity includes Sequencing Rules with any of the specified Rule Actions Then (Make sure the activity has rules to evaluate)", logParent);
	
	if (applicableSequencingRules.length > 0){
		
		this.LogSeq("[UP.2]1.1. Initialize rules list by selecting the set of Sequencing Rules for the activity that have any of the specified Rule Actions (maintaining original rule ordering", logParent);
		//done above
		
		this.LogSeq("[UP.2]1.2. For each rule in the rules list", logParent);
		for (var i=0; i < applicableSequencingRules.length; i++){
		
			this.LogSeq("[UP.2]1.2.1. Apply the Sequencing Rule Check Subprocess to the activity and the rule (Evaluate each rule, one at a time)", logParent);
			
			subprocessResult = this.SequencingRulesCheckSubprocess(activity, applicableSequencingRules[i], logParent);
			
			this.LogSeq("[UP.2]1.2.2. If the Sequencing Rule Check Subprocess returns True Then", logParent);

			if (subprocessResult === true){

				this.LogSeq("[UP.2]1.2.2.1. Exit Sequencing Rules Check Process (Action: Rule Action for the rule) (Stop at the first rule that evaluates to true - perform the associated action)", logParent);
				this.LogSeqReturn(applicableSequencingRules[i].Action, logParent);
				return applicableSequencingRules[i].Action;
				
			} 
		}
	}
	
	this.LogSeq("[UP.2]2. Exit Sequencing Rules Check Process (Action: Nil) (No rules evaluated to true - do not perform any action)", logParent);
	this.LogSeqReturn("null", logParent);
	return null;

}


function Sequencer_GetApplicableSetofSequencingRules (activity, ruleSet){
	
	var applicableRules = new Array();
	
	if (ruleSet == RULE_SET_POST_CONDITION){
		applicableRules = activity.GetPostConditionRules();
	}
	
	else if(ruleSet == RULE_SET_EXIT){
		applicableRules = activity.GetExitRules();
	}
	
	else{
		//rule condition is one of the precondition rules
		
		var preConditionRules = activity.GetPreConditionRules();
		
		for (var i=0; i < preConditionRules.length; i++){
		
			switch (ruleSet){
				
				case RULE_SET_HIDE_FROM_CHOICE:
					
					if (preConditionRules[i].Action == SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE){
						applicableRules[applicableRules.length] = preConditionRules[i];
					}
					
				break;
				
				case RULE_SET_STOP_FORWARD_TRAVERSAL:
					
					if (preConditionRules[i].Action == SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL){
						applicableRules[applicableRules.length] = preConditionRules[i];
					}
									
				break;
				
				case RULE_SET_DISABLED:
					
					if (preConditionRules[i].Action == SEQUENCING_RULE_ACTION_DISABLED){
						applicableRules[applicableRules.length] = preConditionRules[i];
					}
									
				break;
				
				case RULE_SET_SKIPPED:
					
					if (preConditionRules[i].Action == SEQUENCING_RULE_ACTION_SKIP){
						applicableRules[applicableRules.length] = preConditionRules[i];
					}
									
				break;
				
				default:
					Debug.AssertError("ERROR - invalid sequencing rule set - " + ruleSet);
					return null;
				//break;
			}
			
		}
	}
	return applicableRules;
	
}
