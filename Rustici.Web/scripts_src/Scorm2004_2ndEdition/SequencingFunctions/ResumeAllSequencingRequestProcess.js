//****************************************************************************************************************

//Resume All Sequencing Request Process [SB.2.6]
	
	//May return a delivery request; may return an exception code
	
	//Reference: 
		//Current Activity AM.1.2
		//Suspended Activity AM.1.2 

function Sequencer_ResumeAllSequencingRequestProcess(callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Resume All Sequencing Request Process [SB.2.6]", callingLog);
	
	var returnValue;
	
	this.LogSeq("[SB.2.6]1. If the Current Activity is Defined Then (Make sure the sequencing session has not already begun)", logParent);
	
	if (this.IsCurrentActivityDefined(logParent)){
		this.LogSeq("[SB.2.6]1.1. Exit Resume All Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.6-1) (Nothing to deliver)", logParent);
		
		returnValue = new Sequencer_ResumeAllSequencingRequestProcessResult(null, "SB.2.6-1", IntegrationImplementation.GetString("A 'Resume All' sequencing request cannot be processed while there is a current activity defined."));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("SuspendedActivityDefined[SB.2.6]2. If the Suspended Activity is Not Defined Then (Make sure there is something to resume)", logParent);
	
	if ( ! this.IsSuspendedActivityDefined(logParent)){
		this.LogSeq("[SB.2.6]2.1. Exit Resume All Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.6-2) (Nothing to deliver)", logParent);
		
		returnValue = new Sequencer_ResumeAllSequencingRequestProcessResult(null, "SB.2.6-2", IntegrationImplementation.GetString("There is no suspended activity to resume."));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[SB.2.6]3.Exit Resume All Sequencing Request Process (Delivery Request: the activity identified by the Suspended Activity; Exception: n/a) (The Delivery Request Process validates that the Suspended Activity can be delivered)", logParent);
	
	var suspendedActivity = this.GetSuspendedActivity(logParent);
	
	returnValue = new Sequencer_ResumeAllSequencingRequestProcessResult(suspendedActivity, null, "");
	this.LogSeqReturn(returnValue, logParent);
	return returnValue;
}


function Sequencer_ResumeAllSequencingRequestProcessResult(deliveryRequest, exception, exceptionText){
	this.DeliveryRequest = deliveryRequest;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	
}

Sequencer_ResumeAllSequencingRequestProcessResult.prototype.toString =  function(){
									return "DeliveryRequest=" + this.DeliveryRequest + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText;
									};	