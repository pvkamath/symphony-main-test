//****************************************************************************************************************

//Check Activity Process [UP.5] 
//For an activity, returns True if the activity is disabled or violates any of its limit conditions
	//Reference: 
		//Disabled Rules SM.2
		//Limit Conditions Check Process UP.1
		//Sequencing Rules Check Process UP.2

function Sequencer_CheckActivityProcess(activity, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	
	var logParent = this.LogSeqAudit("Check Activity Process [UP.5](" + activity + ")", callingLog);
	
	this.LogSeq("[UP.5]1.Apply the Sequencing Rules Check Process to the activity and the Disabled sequencing rules (Make sure the activity is not disabled)", logParent);
	var sequencingRulesCheckResult = this.SequencingRulesCheckProcess(activity, RULE_SET_DISABLED, logParent);
	
	this.LogSeq("[UP.5]2. If the Sequencing Rules Check Process does not return Nil Then", logParent);
	if (sequencingRulesCheckResult !== null){
		this.LogSeq("[UP.5]2.1. Exit Check Activity Process (Result: True) (Activity is Disabled)", logParent);
		this.LogSeqReturn("true", logParent);
		return true;
	}
	
	this.LogSeq("[UP.5]3. Apply the Limit Conditions Check Process to the activity (Make the activity does not violate any limit condition)", logParent);
	var limitConditionsCheckResult = this.LimitConditionsCheckProcess(activity, logParent);
	
	this.LogSeq("[UP.5]4. If the Limit Conditions Check Process returns True Then", logParent);
	if (limitConditionsCheckResult){
		this.LogSeq("[UP.5]4.1. Exit Check Activity Process (Result: True) (Limit Condition Has Been Violated)", logParent);
		this.LogSeqReturn("true", logParent);
		return true;
	}
	
	this.LogSeq("[UP.5]5. Exit Check Activity Process (Result: False) (Activity is allowed)", logParent);
	this.LogSeqReturn("false", logParent);
	return false;
	
}

