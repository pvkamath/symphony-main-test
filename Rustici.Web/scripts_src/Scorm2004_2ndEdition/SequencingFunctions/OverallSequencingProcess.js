//****************************************************************************************************************

//Overall Sequencing Process [OP.1]

//Reference: 
	//Content Delivery Environment Process DB.2
	//Delivery Request Process DB.1.3
	//Navigation Request Process NB.2.1
	//Sequencing Request Process SB.2.12
	//Termination Request Process TB.2.3 

//Accepts - Navigation Request
//Call this function in response to a navigation request from the LMS/RTE
//There are three types of requests, Navigation, Sequencing, Delivery (also Termination)

function Sequencer_OverallSequencingProcess(callingLog){
	
	//Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));

	var terminationRequest = null;
	var sequencingRequest = null;
	var targetActivity = null;
	var deliveryRequest = null;
	var endSequencingSession = null;
	
	this.ResetException();
	
	var logParent = this.LogSeqAudit("Overall Sequencing Process [OP.1]");
	this.LogSeq("[OP.1]1. Navigation Request = " + this.NavigationRequest, logParent);

	//extensions for Rustici SCP Only*********************** 
	//NOTE - this code is not obsolete, the GetExitAction will always return DisplayMessage

	if (this.NavigationRequest === null){
		
		var exitAction = this.GetExitAction(this.GetCurrentActivity(), logParent);
		this.LogSeq("[OP.1]x. No API Runtime Nav Request, exit action=" + exitAction, logParent);
		
		//TODO: add get text to integration layer
		var messageText = "";
		
		if (exitAction == EXIT_ACTION_EXIT_CONFIRMATION ||
		    exitAction == EXIT_ACTION_DISPLAY_MESSAGE){
		
			var rootActivity = Control.Activities.GetRootActivity();
			var courseIsSatisfied = (rootActivity.IsCompleted() || rootActivity.IsSatisfied());
			
			if (courseIsSatisfied === true){
				messageText = IntegrationImplementation.GetString("The course is now complete. Please make a selection to continue.");
			}
			else{
				messageText = IntegrationImplementation.GetString("Please make a selection.");
			}
		}
		
		switch (exitAction){

			case (EXIT_ACTION_EXIT_NO_CONFIRMATION):
				this.NavigationRequest = new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL, null, "");
			break;
			
			case (EXIT_ACTION_EXIT_CONFIRMATION):
				//TODO: add get text to integration layer
				
				if (confirm("Would you like to exit the course now?")){
					this.NavigationRequest = new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL, null, "");
				}
				else{
					this.NavigationRequest = new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE, null, messageText);
				}
			break;
			
			case (EXIT_ACTION_GO_TO_NEXT_SCO):
				this.NavigationRequest = new NavigationRequest(NAVIGATION_REQUEST_CONTINUE, null, "");
			break;
			
			case (EXIT_ACTION_DISPLAY_MESSAGE):
				//TODO: add get text to integration layer
				this.NavigationRequest = new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE, null, messageText);
			break;
			
			case (EXIT_ACTION_DO_NOTHING):
				this.NavigationRequest = null;
			break;
			
			case (EXIT_ACTION_REFRESH_PAGE):
				Control.RefreshPage();
			break;
		}		
	}
	
	if (this.NavigationRequest == null){
		this.LogSeqReturn("No navigation request, exiting", logParent);
		return;
	}

	//END extensions for Rustici SCP Only***********************
	
	//This code below is important to ensure that upong exit of a single sco, we will exit the player.
	//When cleaning up the above, remember to test that scenario.
	if (this.NavigationRequest.Type == NAVIGATION_REQUEST_DISPLAY_MESSAGE){
		//this.LogSeq("[OP.1]x. Navigation request is display message, exit sequencing process.", logParent);
		//return;
		
		//translate this to a an exit request
		this.LogSeq("[OP.1]x. Navigation request is display message, translating to an exit request.", logParent);
		this.NavigationRequest = new NavigationRequest(NAVIGATION_REQUEST_EXIT, null, "")
	}
	if (this.NavigationRequest.Type == NAVIGATION_REQUEST_EXIT_PLAYER){
		this.LogSeq("[OP.1]x. Navigation request is exit player, exit sequencing process.", logParent);
		Control.ExitScormPlayer();
		return;
	}

	this.LogSeq("[OP.1]1.1. Apply the Navigation Request Process to the navigation request", logParent);
	var navigationRequestResult = this.NavigationRequestProcess(this.NavigationRequest.Type, this.NavigationRequest.TargetActivity, logParent);
	
	this.LogSeq("[OP.1]1.2. If the Navigation Request Process returned navigation request Not Valid Then", logParent);
	
	if (navigationRequestResult.NavigationRequest == NAVIGATION_REQUEST_NOT_VALID){
		
		this.LogSeq("[OP.1]1.2.1. Handle the navigation request exception Behavior not specified", logParent);
		
		this.Exception = navigationRequestResult.Exception;	
		this.ExceptionText = navigationRequestResult.ExceptionText;
		
		this.LogSeq("[OP.1]1.2.2. Continue Loop - wait for the next navigation request", logParent);
		this.LogSeqReturn("false", logParent);
		return false;
	}
	
	
	terminationRequest = navigationRequestResult.TerminationRequest;
	sequencingRequest = navigationRequestResult.SequencingRequest;
	targetActivity = navigationRequestResult.TargetActivity;
	
	
	this.LogSeq("[OP.1]1.3. If there is a termination request Then (If the current activity is active, end the attempt on the current activity)", logParent);
	if (terminationRequest !== null){
	
		this.LogSeq("[OP.1]1.3.1. Apply the Termination Request Process to the termination request", logParent);
		var terminationRequestResult = this.TerminationRequestProcess(terminationRequest, logParent);
		
		this.LogSeq("[OP.1]1.3.2. If the Termination Request Process returned termination request Not Valid Then", logParent);
		if (terminationRequestResult.TerminationRequest == TERMINATION_REQUEST_NOT_VALID){
			
			this.LogSeq("[OP.1]1.3.2.1. Handle the termination request exception Behavior not specified", logParent);
			
			this.Exception = terminationRequestResult.Exception;
			this.ExceptionText = terminationRequestResult.ExceptionText;
			
			this.LogSeq("[OP.1]1.3.2.2. Continue Loop - wait for the next navigation request", logParent);
			this.LogSeqReturn("false", logParent);
			return false;
		}
		
		this.LogSeq("[OP.1]1.3.3. If Termination Request Process returned a sequencing request Then", logParent);
		if (terminationRequestResult.SequencingRequest !== null){
			
			this.LogSeq("[OP.1]1.3.3.1. Replace any pending sequencing request by the sequencing request returned by the Termination Request Process (There can only be one pending sequencing request. Use the one returned by the termination request process, if it exists)", logParent);
			sequencingRequest = terminationRequestResult.SequencingRequest;
		}
	
	}
	
	this.LogSeq("[OP.1]1.4. If there is a sequencing request Then", logParent);
	if (sequencingRequest !== null){
		
		this.LogSeq("[OP.1]1.4.1. Apply the Sequencing Request Process to the sequencing request", logParent);
		var sequencingRequestResult = this.SequencingRequestProcess(sequencingRequest, targetActivity, logParent);
		
		this.LogSeq("[OP.1]1.4.2. If the Sequencing Request Process returned sequencing request Not Valid Then", logParent);
		if (sequencingRequestResult.SequencingRequest == SEQUENCING_REQUEST_NOT_VALID){
		
			this.LogSeq("[OP.1]1.4.2.1. Handle the sequencing request exception Behavior not specified", logParent);
			
			this.Exception = sequencingRequestResult.Exception;
			this.ExceptionText = sequencingRequestResult.ExceptionText;
			
			this.LogSeq("[OP.1]1.4.2.2. Continue Loop - wait for the next navigation request", logParent);
			this.LogSeqReturn("false", logParent);
			return false;
		}
		
		this.LogSeq("[OP.1]1.4.3. If the Sequencing Request Process returned a request to end the sequencing session Then", logParent);
		if (sequencingRequestResult.EndSequencingSession === true){
			this.LogSeq("[OP.1]1.4.3.1. Exit Overall Sequencing Process - the sequencing session has terminated; return control to LTS (Exiting from the root of the activity tree ends the sequencing session; return control to the LTS)", logParent);
			
			Control.ExitScormPlayer();
			
			this.LogSeqReturn("", logParent);
			return;
		}
		
		this.LogSeq("[OP.1]1.4.4. If the Sequencing Request Process did not identify an activity for delivery Then", logParent);
		if (sequencingRequestResult.DeliveryRequest === null){
			this.LogSeq("[OP.1]1.4.4.1. Continue Loop - wait for the next navigation request", logParent);
			
			this.Exception = "OP.1.4";
			this.ExceptionText = IntegrationImplementation.GetString("Please make a selection.");
			
			//the SCO should have already been unloaded, don't unload it again because that will trigger the overall sequencing process again
			
			this.LogSeqReturn("", logParent);
			return;
		}
		
		this.LogSeq("[OP.1]1.4.5. Delivery request is for the activity identified by the Sequencing Request Process", logParent);
		deliveryRequest = sequencingRequestResult.DeliveryRequest;
	} 
	
	this.LogSeq("[OP.1]1.5. If there is a delivery request Then", logParent);
	if (deliveryRequest !== null){
	
		this.LogSeq("[OP.1]1.5.1. Apply the Delivery Request Process to the delivery request", logParent);
		var deliveryRequestResult = this.DeliveryRequestProcess(deliveryRequest, logParent);
		
		this.LogSeq("[OP.1]1.5.2. If the Delivery Request Process returned delivery request Not Valid Then", logParent);
		if (deliveryRequestResult.Valid === false){
			
			this.LogSeq("[OP.1]1.5.2.1. Handle the delivery request exception Behavior not specified", logParent);
			
			
			//TODO - get this text from the integration layer
			//This condition can occur when a previos attempt was suspended when the current sco was not active (such
			//as right after an exit request). When this happens, the root activity is the suspended activity and nothing
			//is identified for delivery. 
			
			this.Exception = "OP.1.5";
			this.ExceptionText = IntegrationImplementation.GetString("Please make a selection.");
			
			this.LogSeq("[OP.1]1.5.2.2. Continue Loop - wait for the next navigation request", logParent);
			this.LogSeqReturn("false", logParent);
			return false;
		}

		
		this.LogSeq("[OP.1]1.5.3. Apply the Content Delivery Environment Process to the delivery request", logParent);
		this.ContentDeliveryEnvironmentProcess(deliveryRequest, logParent);
	
	}
	
	this.LogSeq("[OP.1]2. End Loop - wait for the next navigation request", logParent);
	this.LogSeqReturn("", logParent);
	return;
	
}


