//****************************************************************************************************************

//Objective Rollup Using Default Process [RB.1.2 c]

//	For an activity; may change the Objective Information for the activity

//For the primary objective of an activity, this process changes the success status if appropriate based on the default rollup rules

//	Reference: 



function Sequencer_ObjectiveRollupUsingDefaultProcess(activity, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Objective Rollup Using Default Process [RB.1.2 c](" + activity + ")", callingLog);

	var satisfied;
	var attempted;
	var notSatisfied;
	
	this.LogSeq("[RB.1.2 c]0.5. If the activity is a leaf", logParent);
	if (activity.IsALeaf()){
		this.LogSeq("[RB.1.2 c]0.5.1 Exit, nothing to rollup", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	
	//get the primary objective
	
	this.LogSeq("[RB.1.2 c]1. Set the target objective to Undefined", logParent);
	var targetObjective = null;
	
	var i;
	
	/*
	this.LogSeq("[RB.1.2 c]2. For each objective associated with the activity", logParent);
	var objectives = activity.GetObjectives();
	for (i=0; i < objectives.length; i++){
	
		this.LogSeq("[RB.1.2 c]2.1. If Objective Contributes to Rollup for the objective is True Then (Identify the objective that may be altered based on the activity's children's rolled up measure)", logParent);
		if (objectives[i].GetContributesToRollup() === true){
		
			this.LogSeq("[RB.1.2 c]2.1.1. Set the target objective to the objective", logParent);
			targetObjective = objectives[i];
			
			this.LogSeq("[RB.1.2 c]2.1.2. Break For", logParent);
			break;
		}
	}
	*/
	
	this.LogSeq("[RB.1.2 c]2. Get the primary objective (For each objective associated with the activity)", logParent);
	var targetObjective = activity.GetPrimaryObjective();
	
	//get applicable child set
	this.LogSeq("[RB.1.2 c]3. Get the set of applicable children", logParent);
	var applicableChildren = activity.GetChildren();
	
	this.LogSeq("[RB.1.2 c]4. initialize all not satisfied to true", logParent);
	var allNotSatisfied = true;
	
	this.LogSeq("[RB.1.2 c]5. initialize all satisfied to true", logParent);
	var allSatisfied = true;
	
	this.LogSeq("[RB.1.2 c]6. for each applicable child", logParent);
	
	var isEmptySet = true;
	
	for (i=0; i < applicableChildren.length; i++){

		this.LogSeq("[RB.1.2 c]Retrieving Status for child #" + (i + 1) + " - " + applicableChildren[i], logParent);
		
		this.LogSeq("[RB.1.2 c]6.1 satisfied = activity.getsatisfied", logParent);
		satisfied = applicableChildren[i].IsSatisfied();
		
		this.LogSeq("[RB.1.2 c]6.2 attempted = activity.getattempted", logParent);
		attempted = applicableChildren[i].IsAttempted();
		
		this.LogSeq("[RB.1.2 c]6.3 not satisfied = (satisfied === false || attempted === true)", logParent);
		notSatisfied = (satisfied === false || attempted === true);
		
		this.LogSeq("[RB.1.2 c] satisfied = " + satisfied + ", attempted = " + attempted + ", notSatisfied=" + notSatisfied, logParent);
		
		if (this.CheckChildForRollupSubprocess(applicableChildren[i], ROLLUP_RULE_ACTION_SATISFIED, logParent)){
			this.LogSeq("[RB.1.2 c]6.4 all statisfied = all satisfied AND satisfied", logParent);
			allSatisfied = (allSatisfied && (satisfied === true));
			isEmptySet = false;
		}
		
		if (this.CheckChildForRollupSubprocess(applicableChildren[i], ROLLUP_RULE_ACTION_NOT_SATISFIED, logParent)){
			this.LogSeq("[RB.1.2 c]6.5 all not satisfied = all not satisfied AND not satisfied", logParent);
			allNotSatisfied = (allNotSatisfied && notSatisfied);
			isEmptySet = false;
		}
	}

	if (isEmptySet && Control.Package.Properties.RollupEmptySetToUnknown) {
	
	    this.LogSeq("[SCORM Engine Extension] Child Set is Empty and Control.Package.Properties.RollupEmptySetToUnknown=" + Control.Package.Properties.RollupEmptySetToUnknown + ")", logParent);
	    // Leave unknown, we don't have to explicitly set anything.
	}
	else {
	
    	this.LogSeq("[RB.1.2 c]7. If All Not Satified. (allNotSatisfied=" + allNotSatisfied + ")", logParent);
    	
	    if (allNotSatisfied === true){
    	
		    this.LogSeq("[RB.1.2 c]7.1. Set the Objective Progress Status for the target objective to True", logParent);
		    targetObjective.SetProgressStatus(true, false, activity);
    			
		    this.LogSeq("[RB.1.2 c]7.2. Set the Objective Satisfied Status for the target objective to False", logParent);
		    targetObjective.SetSatisfiedStatus(false, false, activity);
	    }
    	
	    this.LogSeq("[RB.1.2 c]7.3. If All Satisfied. (allSatisfied=" + allSatisfied + ")", logParent);
    	
	    if (allSatisfied === true){
    	
		    this.LogSeq("[RB.1.2 c]7.3.1. Set the Objective Progress Status for the target objective to True", logParent);
		    targetObjective.SetProgressStatus(true, false, activity);
    		
		    this.LogSeq("[RB.1.2 c]7.3.2. Set the Objective Satisfied Status for the target objective to True", logParent);
		    targetObjective.SetSatisfiedStatus(true, false, activity);
	    }
    	
	}

	this.LogSeqReturn("", logParent);
}