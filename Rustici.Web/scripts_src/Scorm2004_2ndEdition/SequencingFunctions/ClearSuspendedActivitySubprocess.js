//****************************************************************************************************************

//Clear Suspended Activity Subprocess [DB.2.1] 
	//For an activity; may change the Suspended Activity
	//Reference: 
		//Activity is Suspended AM.1.1
		//Suspended Activity AM.1.2 
	

function Sequencer_ClearSuspendedActivitySubprocess(activity, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Clear Suspended Activity Subprocess [DB.2.1](" + activity + ")", callingLog);
	
	var commonAncestor = null;
	var suspendedActivity = null;
	var activityPath = null;
	
	this.LogSeq("[DB.2.1]1. If the Suspended Activity is Defined Then Make sure there is something to clear", logParent);
	if (this.IsSuspendedActivityDefined(logParent)){
	
		this.LogSeq("[DB.2.1]1.1. Find the common ancestor of the identified activity and the Suspended Activity", logParent);
		suspendedActivity = this.GetSuspendedActivity(logParent);
		commonAncestor = this.FindCommonAncestor(activity, suspendedActivity, logParent);
		
		this.LogSeq("[DB.2.1]1.2. Form an activity path as the ordered series of activities from the Suspended Activity to the common ancestor, inclusive", logParent);
		activityPath = this.GetPathToAncestorInclusive(suspendedActivity, commonAncestor);
		
		this.LogSeq("[DB.2.1]1.3. If the activity path is Not Empty Then", logParent);
		if (activityPath.length > 0){
		
			this.LogSeq("[DB.2.1]1.3.1. For each activity in the activity path (Walk down the tree setting each of the identified activities to not suspended)", logParent);
			for (var i = 0; i < activityPath.length; i++){
			
				this.LogSeq("[DB.2.1]1.3.1.1. If the activity is a leaf Then", logParent);
				
				if (activityPath[i].IsALeaf()){
					this.LogSeq("[DB.2.1]1.3.1.1.1. Set Activity is Suspended for the activity to False", logParent);
					activityPath[i].SetSuspended(false);
				}
				else{
					this.LogSeq("[DB.2.1]1.3.1.2. Else", logParent);
					
					this.LogSeq("[DB.2.1]1.3.1.2.1. If the activity does not include any child activity whose Activity is Suspended attribute is True Then", logParent);
					
					if (activityPath[i].HasSuspendedChildren() === false){
						
						this.LogSeq("[DB.2.1]1.3.1.2.1.1. Set Activity is Suspended for the activity to False", logParent);
						activityPath[i].SetSuspended(false);
					}
				}
			}
		}
		
		this.LogSeq("[DB.2.1]1.4. Set Suspended Activity to Undefined (Clear the Suspended Activity attribute)", logParent);
		this.ClearSuspendedActivity(logParent);
	}
	
	this.LogSeq("[DB.2.1]2. Exit Clear Suspended Activity Subprocess", logParent);
	this.LogSeqReturn("", logParent);
	return;
	

}
