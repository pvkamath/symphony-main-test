//****************************************************************************************************************
//Choice Sequencing Request Process [SB.2.9] 
	//For a target activity; may return a delivery request; may change the Current Activity; may return an exception code

	//Reference: 
		//Activity is Active AM.1.1
		//Activity is Suspended AM.1.1
		//Available Children AM.1.1
		//Check Activity Process UP.5
		//Choice Activity Traversal Subprocess SB.2.4
		//Current Activity AM.1.2
		//End Attempt Process UP.4
		//Flow Subprocess SB.2.3
		//Sequencing Control Mode Choice SM.1
		//Sequencing Control Choice Exit SM.1
		//Sequencing Rules Check Process UP.2
		//Terminate Descendent Attempts Process UP.3
		//adlseq:constrainedChoice SCORM SN
		//adlseq:preventActivation SCORM SN 
		//Choice Flow Subprocess SB.2.9.1
		
function Sequencer_ChoiceSequencingRequestProcess(targetActivity, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Choice Sequencing Request Process [SB.2.9](" + targetActivity + ")", callingLog);
	
	var parentActivity = null;
	var activityPath;
	var sequencingRulesCheckResult = null;
	var commonAncestor = null;
	var currentActivity = null;
	var traversalDirection = null;
	var choiceActivityTraversalResult;
	
	var i;
	var returnValue;
	
	this.LogSeq("[SB.2.9]1. If there is no target activity Then (There must be a target activity for choice) targetActivity=" + targetActivity.LearningObject.ItemIdentifier, logParent);
	if (targetActivity === null){
		this.LogSeq("[SB.2.9]1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-1) (Nothing to deliver)", logParent);
		
		returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-1", IntegrationImplementation.GetString("Your selection is not permitted.  Please select an available menu item to continue."), false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[SB.2.9]2. If the target activity is not the root of the activity tree Then", logParent);
	if (targetActivity.IsTheRoot() === false){
		
		parentActivity = this.Activities.GetParentActivity(targetActivity);
		
		this.LogSeq("[SB.2.9]2.1. If the Available Children for the parent of the target activity does not contain the target activity Then (The activity is currently not available)", logParent);
		if (parentActivity.IsActivityAnAvailableChild(targetActivity) === false){
		
			this.LogSeq("[SB.2.9]2.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-2) (Nothing to deliver)", logParent);
			
			returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-2", IntegrationImplementation.GetString("The activity '{0}' is not currently available in {1}.", targetActivity, parentActivity.GetTitle()), false);

			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
	}
	
	
	this.LogSeq("[SB.2.9]3. Form the activity path as the ordered series of activities from the root of the activity tree to the target activity, inclusive", logParent);
	activityPath = this.GetActivityPath(targetActivity, true);
	
	this.LogSeq("[SB.2.9]4. For each activity in the activity path", logParent);
	for (i = (activityPath.length - 1); i >= 0; i--){
		
		this.LogSeq("[SB.2.9]4.1. Apply the Sequencing Rules Check Process to the activity and the Hide from Choice sequencing rules (Cannot choose something that is hidden)", logParent);
		sequencingRulesCheckResult = this.SequencingRulesCheckProcess(activityPath[i], RULE_SET_HIDE_FROM_CHOICE, logParent);
		
		this.LogSeq("[SB.2.9]4.2. If the Sequencing Rules Check Process does not return Nil Then", logParent);
		if (sequencingRulesCheckResult !== null){
			
			this.LogSeq("[SB.2.9]4.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-3) (Nothing to deliver)", logParent);
			
			returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-3", "The activity " + targetActivity.GetTitle() + " should be hidden and is not a valid selection", true);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
	}
	
	this.LogSeq("[SB.2.9]5.If the target activity is not the root of the activity tree Then", logParent);
	if ( ! targetActivity.IsTheRoot()){
		this.LogSeq("[SB.2.9]5.1. If the Sequencing Control Mode Choice for the parent of the target activity is False Then (Confirm that control mode allow 'choice' of the target)", logParent);
		
		if (parentActivity.GetSequencingControlChoice() === false){
			this.LogSeq("[SB.2.9]5.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-4) (Nothing to deliver)", logParent);
			
			returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-4", IntegrationImplementation.GetString("The activity '{0}' should be hidden and is not a valid selection.", parentActivity.GetTitle()), false);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
	}
	
	this.LogSeq("[SB.2.9]6. If the Current Activity is Defined Then (Has the sequencing session already begun?)", logParent);
	if (this.IsCurrentActivityDefined(logParent)){
		this.LogSeq("[SB.2.9]6.1. Find the common ancestor of the Current Activity and the target activity", logParent);
		
		currentActivity = this.GetCurrentActivity();
		commonAncestor = this.FindCommonAncestor(currentActivity, targetActivity, logParent);
	}
	else{
		this.LogSeq("[SB.2.9]7. Else", logParent);
		this.LogSeq("[SB.2.9]7.1. Set common ancestor is the root of the activity tree (No, choosing the target will start the sequencing session)", logParent);
		
		commonAncestor = this.GetRootActivity(logParent);
	} 
	
	
	
	if (currentActivity !== null && currentActivity.LearningObject.ItemIdentifier == targetActivity.LearningObject.ItemIdentifier){		//not moving
		this.LogSeq("[SB.2.9]8. Case: Current Activity and target activity are identical (Case #1 - select the current activity)", logParent);
		this.LogSeq("[SB.2.9]8.1. Break All Cases (Nothing to do in this case)", logParent);
	}
	
	else if (this.AreActivitiesSiblings(currentActivity, targetActivity, logParent)){	//going across
		
		this.LogSeq("[SB.2.9]9. Case: Current Activity and the target activity are siblings (Case #2 - same cluster; move toward the target activity)", logParent);
		
		this.LogSeq("[SB.2.9]9.1. Form the activity list as the ordered sequence of activities from the Current Activity to the target activity, exclusive of the target activity (We are attempted to walk toward the target activity. Once we reach the target activity, we don't need to test it.)", logParent);
		
		var activityList = parentActivity.GetActivityListBetweenChildren(currentActivity, targetActivity, false);
		
		this.LogSeq("[SB.2.9]9.2. If the activity list is Empty Then (Nothing to choose)", logParent);
		if (activityList.length === 0){
			
			this.LogSeq("[SB.2.9]9.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)", logParent);
			
			returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-5", IntegrationImplementation.GetString("Nothing to open"), false);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[SB.2.9]9.3. If the target activity occurs after the Current Activity in preorder traversal of the activity tree Then", logParent);
		
		if (activityList[0].LearningObject.ItemIdentifier == currentActivity.LearningObject.ItemIdentifier){
			this.LogSeq("[SB.2.9]9.3.1. traverse is Forward", logParent);
			
			traversalDirection = FLOW_DIRECTION_FORWARD;
			
		}
		else{
			this.LogSeq("[SB.2.9]9.4. Else", logParent);
			this.LogSeq("[SB.2.9]9.4.1. traverse is Backward", logParent);
			
			traversalDirection = FLOW_DIRECTION_BACKWARD;
			
			//reverse the array for the next step
			activityList.reverse();
		}
		
		this.LogSeq("[SB.2.9]9.5. For each activity on the activity list", logParent);
		for (i=0; i < activityList.length; i++){
			
			this.LogSeq("[SB.2.9]9.5.1. Apply the Choice Activity Traversal Subprocess to the activity in the traverse direction", logParent);
			choiceActivityTraversalResult = this.ChoiceActivityTraversalSubprocess(activityList[i], traversalDirection, logParent);
			
			this.LogSeq("[SB.2.9]9.5.2. If the Choice Activity Traversal Subprocess returns False Then", logParent);
			if (choiceActivityTraversalResult.Reachable === false){
				
				this.LogSeq("[SB.2.9]9.5.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Choice Activity Traversal Subprocess) (Nothing to deliver)", logParent);
				
				returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, choiceActivityTraversalResult.Exception, "", false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		}
		
		this.LogSeq("[SB.2.9]9.6. Break All Cases", logParent);
	}
	
	else if( currentActivity === null || currentActivity.LearningObject.ItemIdentifier == commonAncestor.LearningObject.ItemIdentifier){		//going down

		this.LogSeq("[SB.2.9]10. Case: Current Activity and common ancestor are the same Or Current Activity is Not Defined (Case #3 - path to the target is forward in the activity tree)", logParent);
		
		this.LogSeq("[SB.2.9]10.1. Form the activity path as the ordered series of activities from the common ancestor to the target activity, exclusive of the target activity", logParent);
		activityPath = this.GetPathToAncestorInclusive(targetActivity, commonAncestor);
		
		this.LogSeq("[SB.2.9]10.2. If the activity path is Empty Then", logParent);
		if (activityPath.length === 0){
			this.LogSeq("[SB.2.9]10.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)", logParent);
			
			returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-5", IntegrationImplementation.GetString("Nothing to open"), false);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[SB.2.9]10.3. For each activity on the activity path", logParent);
		for(i = activityPath.length - 1; i >= 0; i--){		//traverse backwards to achieve the proper order
			
			this.LogSeq("[SB.2.9]10.3.1. Apply the Choice Activity Traversal Subprocess to the activity in the Forward direction", logParent);
			choiceActivityTraversalResult = this.ChoiceActivityTraversalSubprocess(activityPath[i], FLOW_DIRECTION_FORWARD, logParent);
			
			this.LogSeq("[SB.2.9]10.3.2. If the Choice Activity Traversal Subprocess returns False Then", logParent);
			
			if (choiceActivityTraversalResult.Reachable === false){
				
				this.LogSeq("[SB.2.9]10.3.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Choice Activity Traversal Subprocess) (Nothing to deliver)", logParent);
				returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, choiceActivityTraversalResult.Exception, choiceActivityTraversalResult.ExceptionText, false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			
			this.LogSeq("[SB.2.9]10.3.3. If Activity is Active for the activity is False And (the activity is Not the common ancestor And adlseq:preventActivation for the activity is True) Then (If the activity being considered is not already active, make sure we are allowed to activate it)", logParent);
			if (activityPath[i].IsActive() === false &&
			    activityPath[i].LearningObject.ItemIdentifier != commonAncestor.LearningObject.ItemIdentifier &&
			    activityPath[i].GetPreventActivation() === true){
			    
				this.LogSeq("[SB.2.9]10.3.3.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-6) (Nothing to deliver)", logParent);
				
				returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-6", IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'", activityPath[i].GetTitle()), false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		}
		
		this.LogSeq("[SB.2.9]10.4. Break All Cases", logParent);
	}
	
	else if( targetActivity.LearningObject.ItemIdentifier == commonAncestor.LearningObject.ItemIdentifier ){		//going up
		
		this.LogSeq("[SB.2.9]11. Case: Target activity is the common ancestor of the Current Activity (Case #4 - path to the target is backward in the activity tree)", logParent);
		
		this.LogSeq("[SB.2.9]11.1. Form the activity path as the ordered series of activities from the Current Activity to the target activity, inclusive", logParent);
		activityPath = this.GetPathToAncestorInclusive(currentActivity, commonAncestor);
		
		this.LogSeq("[SB.2.9]11.2. If the activity path is Empty Then", logParent);
		if (activityPath.length === 0){
			this.LogSeq("[SB.2.9]11.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)", logParent);
			returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-5", IntegrationImplementation.GetString("Nothing to deliver"), false);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[SB.2.9]11.3. For each activity on the activity path", logParent);
		for (i=0; i < activityPath.length; i++){
			this.LogSeq("[SB.2.9]11.3.1. If the activity is not the last activity in the activity path Then", logParent);
			if (i != (activityPath.length - 1)){
				
				this.LogSeq("[SB.2.9]11.3.1.1. If the Sequencing Control Choice Exit for the activity is False Then (Make sure an activity that should not exit will exit if the target is delivered)", logParent);
				
				if (activityPath[i].GetSequencingControlChoiceExit() === false){
					this.LogSeq("[SB.2.9]11.3.1.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-7) (Nothing to deliver)", logParent);
					returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-7", IntegrationImplementation.GetString("Your selection is not permitted.  Please select 'Next' or 'Previous' to move through '{0}'.", activityPath[i]), false);
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			}
		}
		
		
		this.LogSeq("[SB.2.9]11.4. Break All Cases", logParent);
		
	}
	
	else {		//jumping to a cousin (we don't need to evaluate a condition here - the condition is "is target a descendent activity of the common ancestor...well of course it is, otherwise the common ancestor, wouldn't be common to the current and target activities)
		
		this.LogSeq("[SB.2.9]12.Case: Target activity is forward from the common ancestor activity (Case #5 - target is a descendent activity of the common ancestor)", logParent);
		
		this.LogSeq("[SB.2.9]12.1. Form the activity path as the ordered series of activities from the Current Activity to the common ancestor, inclusive", logParent);
		activityPath = this.GetPathToAncestorInclusive(currentActivity, commonAncestor);
		
		this.LogSeq("[SB.2.9]12.2. If the activity path is Empty Then", logParent);
		if (activityPath.length === 0){
			this.LogSeq("[SB.2.9]12.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)", logParent);
			returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-5", IntegrationImplementation.GetString("Nothing to deliver"), false);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[SB.2.9]12.3. Set constrained activity to Undefined", logParent);
		var constrainedActivity = null;
		
		this.LogSeq("[SB.2.9]12.4. For each activity on the activity path (Walk up the tree to the common ancestor)", logParent);
		for (i=0; i < activityPath.length; i++){
		
			this.LogSeq("[SB.2.9]12.4.1. If the activity is not the last activity in the activity path Then", logParent);
			if (i != (activityPath.length - 1)){
				this.LogSeq("[SB.2.9]12.4.1.1. If the Sequencing Control Choice Exit for the activity is False Then - Make sure an activity that should not exit will exit if the target is delivered", logParent);
				if (activityPath[i].GetSequencingControlChoiceExit() === false){
					this.LogSeq("[SB.2.9]12.4.1.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-7) (Nothing to deliver)", logParent);
					returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-7", IntegrationImplementation.GetString("You are not allowed to jump out of {0}.", activityPath[i].GetTitle()), false);
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			}
		
			this.LogSeq("[SB.2.9]12.4.2. If constrained activity is Undefined Then (Find the closest constrained activity to the current activity)", logParent);
			
			if (constrainedActivity === null){
				this.LogSeq("[SB.2.9]12.4.2.1. If adlseq:constrainedChoice for the activity is True Then", logParent);
				if (activityPath[i].GetConstrainedChoice() === true){
					this.LogSeq("[SB.2.9]12.4.2.1.1.Set constrained activity to activity", logParent);
					constrainedActivity = activityPath[i];
				}
			}
		}
		
		this.LogSeq("[SB.2.9]12.5. If constrained activity is Defined Then", logParent);
		if (constrainedActivity !== null){
		
			this.LogSeq("[SB.2.9]12.5.1. If the target activity is Forward in the activity tree relative to the constrained activity Then", logParent);
			if (this.IsActivity1BeforeActivity2(constrainedActivity, targetActivity, logParent)){
			
				this.LogSeq("[SB.2.9]12.5.1.1. traverse is Forward ('Flow' in a forward direction to see what activity comes next)", logParent);
				traversalDirection = FLOW_DIRECTION_FORWARD;
			}
			else{
				this.LogSeq("[SB.2.9]12.5.2. Else", logParent);
				this.LogSeq("[SB.2.9]12.5.2.1. traverse is Backward ('Flow' in a backward direction to see what activity comes next)", logParent);
				traversalDirection = FLOW_DIRECTION_BACKWARD;
			}
		
			this.LogSeq("[SB.2.9]12.5.3. Apply the Choice Flow Subprocess to the constrained activity in the traverse direction", logParent);
			var choiceFlowSubProcessResult = this.ChoiceFlowSubprocess(constrainedActivity, traversalDirection, logParent);
			
			this.LogSeq("[SB.2.9]12.5.4. Set activity to consider to the activity identified by the Choice FlowSubprocess", logParent);
			var activityToConsider = choiceFlowSubProcessResult;
			
			
			this.LogSeq("[SB.2.9]12.5.5. If the target activity is Not an available descendent of the activity to consider And the target activity is Not the activity to considered And the target activity is Not the constrained activity Then (Make sure the target activity is within the set of 'flow' constrained choices)", logParent);
			if ( ( ! activityToConsider.IsActivityAnAvailableDescendent(targetActivity) ) &&
			     (targetActivity != constrainedActivity && 
			      targetActivity != activityToConsider)) {		
				this.LogSeq("[SB.2.9]12.5.5.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-8)", logParent);
				returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-8", IntegrationImplementation.GetString("You are not allowed to jump out of {0}.", constrainedActivity.GetTitle()), false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		}
		
		
		this.LogSeq("[SB.2.9]12.6. Form the activity path as the ordered series of activities from the common ancestor to the target activity, exclusive of the target activity", logParent);
		
		//MR - changed on 2/25/05 to start from the parent of the target activity instead of the targetactivity
		activityPath = this.GetPathToAncestorInclusive(this.Activities.GetParentActivity(targetActivity), commonAncestor);
		
		this.LogSeq("[SB.2.9]12.7. If the activity path is Empty Then", logParent);
		if (activityPath.length === 0){
			this.LogSeq("[SB.2.9]12.7.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)", logParent);
			
			returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-5", IntegrationImplementation.GetString("Nothing to open"), false);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[SB.2.9]12.8. If the target activity is forward in the activity tree relative to the Current Activity Then (Walk toward the target activity)", logParent);
		if (this.IsActivity1BeforeActivity2(currentActivity, targetActivity, logParent)){
		
			this.LogSeq("[SB.2.9]12.8.1. For each activity on the activity path", logParent);
			
			for (i = (activityPath.length - 1); i >= 0; i--){
				
				this.LogSeq("[SB.2.9]12.8.1.1. Apply the Choice Activity Traversal Subprocess to the activity in the Forward direction", logParent);
				choiceActivityTraversalResult = this.ChoiceActivityTraversalSubprocess(activityPath[i], FLOW_DIRECTION_FORWARD, logParent);
				
				this.LogSeq("[SB.2.9]12.8.1.2. If the Choice Activity Traversal Subprocess returns False Then", logParent);
				if (choiceActivityTraversalResult.Reachable === false){
					this.LogSeq("[SB.2.9]12.8.1.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Choice Activity Traversal Subprocess) (Nothing to deliver)", logParent);
					returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, choiceActivityTraversalResult.Exception, choiceActivityTraversalResult.ExceptionText, false);

					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
				
				//MR - changed on 2/25/05 - calls to IsActive and GetPreventActivation now work (they were missing parens before)
				
				this.LogSeq("[SB.2.9]12.8.1.3. If Activity is Active for the activity is False And(the activity is Not the common ancestor And adlseq:preventActivation for the activity is True) Then (If the activity being considered is not already active, make sure we are allowed to activate it)", logParent);
				if (
						(activityPath[i].IsActive() === false) &&
						(activityPath[i] != commonAncestor) && 
						(activityPath[i].GetPreventActivation() === true)){
					this.LogSeq("[SB.2.9]12.8.1.3.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-6) (Nothing to deliver)", logParent);
					returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-6", IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'", activityPath[i].GetTitle()), false);
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			}
		}
		else{
			this.LogSeq("[SB.2.9]12.9. Else", logParent);
			this.LogSeq("[SB.2.9]12.9.1. For each activity on the activity path", logParent);
			
			for (i = (activityPath.length - 1); i >= 0; i--){

				this.LogSeq("SB.2.9]12.9.1.1. If Activity is Active for the activity is False And (the activity is Not the common ancestor And adlseq:preventActivation for the activity is True) Then (If the activity being considered is not already active, make sure we are allowed to activate it)", logParent);

				//MR - changed on 2/25/05 - calls to IsActive and GetPreventActivation now work (they were missing parens before)
				
				if (
						(activityPath[i].IsActive() === false) &&
						(activityPath[i] != commonAncestor) && 
						(activityPath[i].GetPreventActivation() === true)){
					this.LogSeq("[SB.2.9]12.9.1.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-6) (Nothing to deliver)", logParent);
					returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-6", IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'", activityPath[i].GetTitle()), false);
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}				
			}
		}
		
		this.LogSeq("[SB.2.9]12.10. Break All Cases", logParent);
	}
	
	
	
	this.LogSeq("[SB.2.9]13. If the target activity is a leaf activity Then", logParent);
	if (targetActivity.IsALeaf() === true){
		
		this.LogSeq("[SB.2.9]13.1. Exit Choice Sequencing Request Process (Delivery Request: the target activity; Exception: n/a)", logParent);
		
		returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(targetActivity, null, "", false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}

	this.LogSeq("[SB.2.9]14. Apply the Flow Subprocess to the target activity in the Forward direction with consider children equal to True (The identified activity is a cluster. Enter the cluster and attempt to find a descendent leaf to deliver)", logParent);
	
	var flowSubprocessResult = this.FlowSubprocess(targetActivity, FLOW_DIRECTION_FORWARD, true, logParent);

	this.LogSeq("[SB.2.9]15. If the Flow Subprocess returns False Then (Nothing to deliver, but we succeeded in reaching the target activity - move the current activity)", logParent);
	//if (flowSubprocessResult === null){
	if (flowSubprocessResult.Deliverable === false){
		
		//we don't want to make these modifications while performing the look-ahead operations as they could affect the look-ahead operations that are still coming
		if (this.LookAhead === false){
		
			this.LogSeq("[SB.2.9]15.1. Apply the Terminate Descendent Attempts Process to the common ancestor", logParent);
			this.TerminateDescendentAttemptsProcess(commonAncestor, logParent);
			
			this.LogSeq("[SB.2.9]15.2. Apply the End Attempt Process to the common ancestor", logParent);
			this.EndAttemptProcess(commonAncestor, false, logParent);
			
			this.LogSeq("[SB.2.9]15.3. Set the Current Activity to the target activity", logParent);
			this.SetCurrentActivity(targetActivity, logParent);
		
		}
		
		this.LogSeq("[SB.2.9]15.4. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-9) (Nothing to deliver)", logParent);
		returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(null, "SB.2.9-9", IntegrationImplementation.GetString("Please select another item from the menu."), false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	else{
		this.LogSeq("[SB.2.9]16. Else", logParent);
		this.LogSeq("[SB.2.9]16.1. Exit Choice Sequencing Request Process (Delivery Request: for the activity identified by the Flow Subprocess; Exception: n/a)", logParent);
		
		returnValue = new Sequencer_ChoiceSequencingRequestProcessResult(flowSubprocessResult.IdentifiedActivity, null, "", false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
}


function Sequencer_ChoiceSequencingRequestProcessResult(deliveryRequest, exception, exceptionText, hidden){
	
	if (hidden === undefined){
		Debug.AssertError("no value passed for hidden");
	}

	this.DeliveryRequest = deliveryRequest;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	this.Hidden = hidden;

}

Sequencer_ChoiceSequencingRequestProcessResult.prototype.toString = function(){
									return "DeliveryRequest=" + this.DeliveryRequest + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText +
										", Hidden=" + this.Hidden;
									};