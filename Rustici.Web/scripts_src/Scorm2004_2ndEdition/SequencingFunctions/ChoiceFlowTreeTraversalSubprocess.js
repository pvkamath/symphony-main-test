//****************************************************************************************************************

//Choice Flow Tree Traversal Subprocess [SB.2.9.2]
	//For an activity, a traversal direction; returns the 'next' activity in directed traversal of the activity tree
	//Reference: 
		//Available Children AM.1.1 
		
function Sequencer_ChoiceFlowTreeTraversalSubprocess(activity, flowDirection, callingLog){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Choice Flow Tree Traversal Subprocess [SB.2.9.2](" + activity + ", " + flowDirection + ")", callingLog);
	
	var parentActivity = this.Activities.GetParentActivity(activity);
	var choiceFlowTreeTraversalResult = null;
	var nextActivity = null;
	var previousActivity = null;
	
	this.LogSeq("[SB.2.9.2]1. If the traversal direction is Forward Then", logParent);
	
	if (flowDirection == FLOW_DIRECTION_FORWARD){
		
		this.LogSeq("[SB.2.9.2]1.1. If the activity is the last activity in a forward preorder tree traversal of the activity tree Then (Cannot walk off the activity tree)", logParent);
		
		if (this.IsActivityLastOverall(activity, logParent)){
			this.LogSeq("[SB.2.9.2]1.1.1. Exit Choice Flow Tree Traversal Subprocess (Next Activity: Nil)", logParent);
			this.LogSeqReturn("null", logParent);
			return null;
		}
		
		
		this.LogSeq("[SB.2.9.2]1.2. If the activity is the last activity in the activity's parent's list of Available Children Then", logParent);
		if (parentActivity.IsActivityTheLastAvailableChild(activity)){
		
			this.LogSeq("[SB.2.9.2]1.2.1.  Apply the Choice Flow Tree Traversal Subprocess to the activity's parent in the Forward direction (Recursion - Move to the activity's parent's next forward sibling)", logParent);
			choiceFlowTreeTraversalResult = this.ChoiceFlowTreeTraversalSubprocess(parentActivity, FLOW_DIRECTION_FORWARD, logParent);
			
			this.LogSeq("[SB.2.9.2]1.2.2. Exit Choice Flow Tree Traversal Subprocess (Next Activity: the results of the recursive Choice Flow Tree Traversal Subprocess) (Return the result of the recursion)", logParent);
			this.LogSeqReturn(choiceFlowTreeTraversalResult, logParent);
			return choiceFlowTreeTraversalResult;
		}
		else{
			this.LogSeq("[SB.2.9.2]1.3. Else", logParent);
			this.LogSeq("[SB.2.9.2]1.3.1. Traverse the tree, forward preorder, one activity to the next activity, in the activity's parent's list of Available Children", logParent);
			
			nextActivity = parentActivity.GetNextActivity(activity);
			
			this.LogSeq("[SB.2.9.2]1.3.2. Exit Choice Flow Tree Traversal Subprocess (Next Activity: the activity identified by the traversal)", logParent);
			this.LogSeqReturn(nextActivity, logParent);
			return nextActivity;
		}
	}
	
	
	this.LogSeq("[SB.2.9.2]2. If the traversal direction is Backward Then", logParent);
	if (flowDirection == FLOW_DIRECTION_BACKWARD){
		
		this.LogSeq("[SB.2.9.2]2.1. If the activity is the root activity of the tree Then (Cannot walk off the root of the activity tree)", logParent);
		if (activity.IsTheRoot()){
			this.LogSeq("[SB.2.9.2]2.1.1. Exit Choice Flow Tree Traversal Subprocess (Next Activity: Nil)", logParent);
			this.LogSeqReturn("null", logParent);
			return null;
		}
		
		this.LogSeq("[SB.2.9.2]2.2. If the activity is the first activity in the activity's parent's list of Available Children Then", logParent);
		if (parentActivity.IsActivityTheFirstAvailableChild(activity)){
			
			this.LogSeq("[SB.2.9.2]2.2.1. Apply the Choice Flow Tree Traversal Subprocess to the activity's parent in the Backward direction (Recursion � Move to the activity's parent's next backward sibling)", logParent);
			choiceFlowTreeTraversalResult = this.ChoiceFlowTreeTraversalSubprocess(parentActivity, FLOW_DIRECTION_BACKWARD, logParent);
			
			this.LogSeq("[SB.2.9.2]2.2.1.1. Exit Choice Flow Tree Traversal Subprocess (Next Activity: the results of the recursive Choice Flow Tree Traversal Subprocess) (Return the result of the recursion)", logParent);
			this.LogSeqReturn(choiceFlowTreeTraversalResult, logParent);
			return choiceFlowTreeTraversalResult;
		}
		else{
			this.LogSeq("[SB.2.9.2]2.3. Else", logParent);
			
			this.LogSeq("[SB.2.9.2]2.3.1. Traverse the tree, reverse preorder, one activity to the previous activity, from the activity's parent's list of Available Children", logParent);
			previousActivity = parentActivity.GetPreviousActivity(activity);
			
			this.LogSeq("[SB.2.9.2]2.3.2. Exit Choice Flow Tree Traversal Subprocess (Next Activity: the activity identified by the traversal)", logParent);
			this.LogSeqReturn(previousActivity, logParent);
			return previousActivity;
		}
	}
	
}
