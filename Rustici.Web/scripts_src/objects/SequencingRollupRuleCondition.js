function SequencingRollupRuleCondition (
			Operator,
			Condition){
	this.Operator = Operator;
	this.Condition = Condition;
}
			
			
			
SequencingRollupRuleCondition.prototype.toString =  function(){
									/*
									return "Operator=" + this.Operator + 
										", Condition=" + this.Condition;
									*/
									
									var ret = "";
									if (this.Operator == RULE_CONDITION_OPERATOR_NOT){
										ret += "NOT ";
									}
									
									ret += this.Condition;
									
									return ret;
									};