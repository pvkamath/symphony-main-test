function SequencingObjectiveMap (
			TargetObjectiveId,
			ReadSatisfiedStatus,
			ReadNormalizedMeasure,
			ReadRawScore,
			ReadMinScore,
			ReadMaxScore,
			ReadCompletionStatus,
			ReadProgressMeasure,
			WriteSatisfiedStatus,
			WriteNormalizedMeasure,
		    WriteRawScore,
			WriteMinScore,
			WriteMaxScore,
			WriteCompletionStatus,
			WriteProgressMeasure){
	this.TargetObjectiveId = TargetObjectiveId;
	this.ReadSatisfiedStatus = ReadSatisfiedStatus;
	this.ReadNormalizedMeasure = ReadNormalizedMeasure;
	this.ReadRawScore = ReadRawScore;
	this.ReadMinScore = ReadMinScore;
	this.ReadMaxScore = ReadMaxScore;
	this.ReadCompletionStatus = ReadCompletionStatus;
	this.ReadProgressMeasure = ReadProgressMeasure;
	this.WriteSatisfiedStatus = WriteSatisfiedStatus;
	this.WriteNormalizedMeasure = WriteNormalizedMeasure;
	this.WriteRawScore = WriteRawScore;
	this.WriteMinScore = WriteMinScore;
	this.WriteMaxScore = WriteMaxScore;
	this.WriteCompletionStatus = WriteCompletionStatus;
	this.WriteProgressMeasure = WriteProgressMeasure;
}
			
SequencingObjectiveMap.prototype.toString =  function(){
									return "TargetObjectiveId=" + this.TargetObjectiveId + 
										", ReadSatisfiedStatus=" + this.ReadSatisfiedStatus + 
										", ReadNormalizedMeasure=" + this.ReadNormalizedMeasure + 
										", ReadRawScore=" + this.ReadRawScore + 
										", ReadMinScore=" + this.ReadMinScore + 
										", ReadMaxScore=" + this.ReadMaxScore + 
										", ReadCompletionStatus=" + this.ReadCompletionStatus + 
										", ReadProgressMeasure=" + this.ReadProgressMeasure + 
										", WriteSatisfiedStatus=" + this.WriteSatisfiedStatus + 
										", WriteNormalizedMeasure=" + this.WriteNormalizedMeasure +
										", WriteRawScore=" + this.WriteRawScore + 
										", WriteMinScore=" + this.WriteMinScore + 
										", WriteMaxScore=" + this.WriteMaxScore + 
										", WriteCompletionStatus=" + this.WriteCompletionStatus + 
										", WriteProgressMeasure=" + this.ReadProgressMeasure;
									};