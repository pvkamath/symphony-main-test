function SequencingData (
			Identifier,
			Identifierref,
			ControlChoice,
			ControlChoiceExit,
			ControlFlow,
			ControlForwardOnly,
			UseCurrentAttemptObjectiveInformation,
			UseCurrentAttemptProgressInformation,
			ConstrainChoice,
			PreventActivation,
			PreConditionSequencingRules,
			PostConditionSequencingRules,
			ExitSequencingRules,
			LimitConditionAttemptControl,
			LimitConditionAttemptLimit,
			LimitConditionAttemptAbsoluteDurationControl,
			LimitConditionAttemptAbsoluteDurationLimit,
			RollupRules,
			RollupObjectiveSatisfied,
			RollupObjectiveMeasureWeight,
			RollupProgressCompletion,
			MeasureSatisfactionIfActive,
			RequiredForSatisfied,
			RequiredForNotSatisfied,
			RequiredForCompleted,
			RequiredForIncomplete,
			PrimaryObjective,
			Objectives,
			SelectionTiming,
			SelectionCountStatus,
			SelectionCount,
			RandomizationTiming,
			RandomizeChildren,
			Tracked,
			CompletionSetByContent,
			ObjectiveSetByContent,
			HidePrevious,
			HideContinue,
			HideExit,
			HideAbandon,
			HideSuspendAll,
			HideAbandonAll,
			HideExitAll){
	this.Identifier = Identifier;
	this.Identifierref = Identifierref;
	this.ControlChoice = ControlChoice;
	this.ControlChoiceExit = ControlChoiceExit;
	this.ControlFlow = ControlFlow;
	this.ControlForwardOnly = ControlForwardOnly;
	this.UseCurrentAttemptObjectiveInformation = UseCurrentAttemptObjectiveInformation;
	this.UseCurrentAttemptProgressInformation = UseCurrentAttemptProgressInformation;
	this.ConstrainChoice = ConstrainChoice;
	this.PreventActivation = PreventActivation;
	this.PreConditionSequencingRules = PreConditionSequencingRules;
	this.PostConditionSequencingRules = PostConditionSequencingRules;
	this.ExitSequencingRules = ExitSequencingRules;
	this.LimitConditionAttemptControl = LimitConditionAttemptControl;
	this.LimitConditionAttemptLimit = LimitConditionAttemptLimit;
	this.LimitConditionAttemptAbsoluteDurationControl = LimitConditionAttemptAbsoluteDurationControl;
	this.LimitConditionAttemptAbsoluteDurationLimit = LimitConditionAttemptAbsoluteDurationLimit;
	this.RollupRules = RollupRules;
	this.RollupObjectiveSatisfied = RollupObjectiveSatisfied;
	this.RollupObjectiveMeasureWeight = RollupObjectiveMeasureWeight;
	this.RollupProgressCompletion = RollupProgressCompletion;
	this.MeasureSatisfactionIfActive = MeasureSatisfactionIfActive;
	this.RequiredForSatisfied = RequiredForSatisfied;
	this.RequiredForNotSatisfied = RequiredForNotSatisfied;
	this.RequiredForCompleted = RequiredForCompleted;
	this.RequiredForIncomplete = RequiredForIncomplete;
	this.PrimaryObjective = PrimaryObjective;
	this.Objectives = Objectives;
	this.SelectionTiming = SelectionTiming;
	this.SelectionCountStatus = SelectionCountStatus;
	this.SelectionCount = SelectionCount;
	this.RandomizationTiming = RandomizationTiming;
	this.RandomizeChildren = RandomizeChildren;
	this.Tracked = Tracked;
	this.CompletionSetByContent = CompletionSetByContent;
	this.ObjectiveSetByContent = ObjectiveSetByContent;
	this.HidePrevious = HidePrevious;
	this.HideContinue = HideContinue;
	this.HideExit = HideExit;
	this.HideAbandon = HideAbandon;
	this.HideSuspendAll = HideSuspendAll;
	this.HideAbandonAll = HideAbandonAll;
	this.HideExitAll = HideExitAll;

}
			
