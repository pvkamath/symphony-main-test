function SequencingRollupRule (
			ConditionCombination,
			ChildActivitySet,
			MinimumCount, 
			MinimumPercent, 
			Action,
			Conditions){
	this.ConditionCombination = ConditionCombination;
	this.ChildActivitySet = ChildActivitySet;
	this.MinimumCount = MinimumCount; 
	this.MinimumPercent = MinimumPercent; 
	this.Action = Action;
	this.Conditions = Conditions;
}

SequencingRollupRule.prototype.toString =  
		function(){
			
			/*
			var ret= "ConditionCombination=" + this.ConditionCombination + 
				", ChildActivitySet=" + this.ChildActivitySet + 
				", MinimumCount=" + this.MinimumCount + 
				", MinimumPercent=" + this.MinimumPercent + 
				", Action=" + this.Action + 
				", Conditions: ";
			*/
			
			//if activity set, have cond Comb conditions, then action
			
			var ret = "If ";
			
			if (this.ChildActivitySet == CHILD_ACTIVITY_SET_AT_LEAST_COUNT){
				ret += "At Least " + this.MinimumCount + " Child Activities Meet "
			}
			else if (this.ChildActivitySet == CHILD_ACTIVITY_SET_AT_LEAST_PERCENT){
				ret += "At Least " + this.MinimumPercent + " Percent of Child Activities Meet "
			}
			else if (this.ChildActivitySet == CHILD_ACTIVITY_SET_ALL){
				ret += "All Child Activities Meet ";
			}
			else if (this.ChildActivitySet == CHILD_ACTIVITY_SET_ANY){
				ret += "Any Child Activity Meets ";
			}
			else if (this.ChildActivitySet == CHILD_ACTIVITY_SET_NONE){
				ret += "No Child Activity Meets ";
			}
			
			if (this.ConditionCombination == RULE_CONDITION_COMBINATION_ANY){
				ret += " Any Condition ";
			}
			else{
				ret += " All Conditions ";
			}
			
			ret += " THEN " + this.Action;
			
			if (this.Conditions.length > 1){
			
			    ret += ". Conditions: "
    				
			    for (var condition in this.Conditions){
				    ret += "{" + condition + "} " + this.Conditions[condition] + "; "; 
			    }
			
			}
			else{
			    ret += ". Condition: " + this.Conditions[0]; 
			}
			
			return ret;

			};