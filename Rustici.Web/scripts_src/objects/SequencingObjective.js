function SequencingObjective (
			Id,
			SatisfiedByMeasure,
			MinNormalizedMeasure, 
			Maps){
	this.Id = Id;
	this.SatisfiedByMeasure = SatisfiedByMeasure;
	this.MinNormalizedMeasure = MinNormalizedMeasure;
	this.Maps = Maps;
}

SequencingObjective.prototype.toString =  
	function(){
		var ret = "Id=" + this.Id + 
			", SatisfiedByMeasure=" + this.SatisfiedByMeasure + 
			", MinNormalizedMeasure=" + this.MinNormalizedMeasure;
			
			ret += "Maps:";
			for (var map in this.Maps){
				ret += "{" + map + "}" + this.Maps[map] + "  "; 
			}
			return ret;
		};