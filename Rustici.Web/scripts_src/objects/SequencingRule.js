function SequencingRule (
			ConditionCombination,
			Action,
			RuleConditions){
	this.ConditionCombination = ConditionCombination;
	this.Action = Action;
	this.RuleConditions = RuleConditions;
}


SequencingRule.prototype.toString =  
		function(){
			
			/*
			var ret= "ConditionCombination=" + this.ConditionCombination + 
				", Action=" + this.Action +
				", Conditions: ";
			*/
			var ret = "If " + this.ConditionCombination + " condition(s) evaluate to true, then " + this.Action + ".  ";
			
			if (this.RuleConditions.length > 1){
			    ret += "Conditions: ";	
    										
			    for (var condition in this.RuleConditions){
				    ret += "{" + condition + "} " + this.RuleConditions[condition] + "; "; 
			    }
			}
			else{
			    ret += "Condition: " + this.RuleConditions[0];
			}
			
			return ret;

			};