
//This js uses the renderDivSection() function in the DebuggerUtilityFunction.js file

function DisplayPossibleNavigationRequestData(){
	var PossibleNavRequests = window.opener.Control.PossibleNavigationRequests;
	var tempStr = "";
	
	for (var loopIndex = 0; loopIndex < PossibleNavRequests.length; loopIndex++){
	    var tempRequest = PossibleNavRequests[loopIndex];
	    
	    var NavRequestArray = new Array();
	    var NavRequestID = "NR_" + tempRequest.NavigationRequest + loopIndex;
	    var NavRequestName = "";
	    var idx = 0;
	    if (tempRequest.NavigationRequest == "CHOICE"){
	        NavRequestName = "Navigation Request: CHOICE - " + tempRequest.TargetActivityItemIdentifier;
	        NavRequestArray[idx++] = "Navigation Request: " + tempRequest.NavigationRequest;
	    } else {
	        NavRequestName = "Navigation Request: " + tempRequest.NavigationRequest;
	    }
	    NavRequestArray[idx++] = "Target Activity Item Identifier: " + tempRequest.TargetActivityItemIdentifier;
	    NavRequestArray[idx++] = "Disabled: " + tempRequest.Disabled;
	    NavRequestArray[idx++] = "Hidden: " + tempRequest.Hidden;
	    NavRequestArray[idx++] = "Sequencing Request: " + tempRequest.SequencingRequest;
	    //NavRequestArray[idx++] = "Target Activity: " + tempRequest.TargetActivity;
	    NavRequestArray[idx++] = "Termination Sequencing Request: " + tempRequest.TerminationSequencingRequest;
	    NavRequestArray[idx++] = "Will Always Succeed: " + tempRequest.WillAlwaysSucceed;
	    NavRequestArray[idx++] = "Will Never Succeed: " + tempRequest.WillNeverSucceed;
	    NavRequestArray[idx++] = "Will Succeed: " + tempRequest.WillSucceed;
	    NavRequestArray[idx++] = "Exception: " + tempRequest.Exception;
	    NavRequestArray[idx++] = "Exception Text: " + tempRequest.ExceptionText;
	    
	    NavRequestArray[idx++] = "Pre Condition Skipped: " + tempRequest.PreConditionSkipped;
	    NavRequestArray[idx++] = "Pre Condition Stop Forward Traversal: " + tempRequest.PreConditionStopForwardTraversal;
	    NavRequestArray[idx++] = "Pre Condition Stop Forward Traversal Violation: " + tempRequest.PreConditionStopForwardTraversalViolation;
	    NavRequestArray[idx++] = "Pre Condition Disabled: " + tempRequest.PreConditionDisabled;
	    NavRequestArray[idx++] = "Pre Condition Hidden From Choice: " + tempRequest.PreConditionHiddenFromChoice;
	    NavRequestArray[idx++] = "Limit Condition Violation: " + tempRequest.LimitConditionViolation;
	    NavRequestArray[idx++] = "Is Visible Violation: " + tempRequest.IsVisibleViolation;
	    NavRequestArray[idx++] = "Prevent Activation Violation: " + tempRequest.PreventActivationViolation;
	    NavRequestArray[idx++] = "Control Choice Violation: " + tempRequest.ControlChoiceViolation;
	    NavRequestArray[idx++] = "Forward Only Violation: " + tempRequest.ForwardOnlyViolation;
	    NavRequestArray[idx++] = "Choice Exit Violation: " + tempRequest.ChoiceExitViolation;
	    NavRequestArray[idx++] = "Constrained Choice Violation: " + tempRequest.ConstrainedChoiceViolation;
	    NavRequestArray[idx++] = "No Deliverable Activity Violation: " + tempRequest.NoDeliverablieActivityViolation;
	    
	    tempStr += renderDivSection(NavRequestID,NavRequestName,NavRequestArray);
	
	
	}
	
	
	return tempStr;
}



