//This js uses the renderDivSection() function in the DebuggerUtilityFunction.js file

function DisplayLastError(){
	var errors = window.opener.Debug.GetErrors();

    if (errors.length > 0) {
        return renderDivSection("DataPostbackErrors","Data Postback Errors", errors);
    } else {
        return "";
    }   
}