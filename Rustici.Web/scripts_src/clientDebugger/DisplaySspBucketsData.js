//This js uses the renderDivSection() function in the DebuggerUtilityFunction.js file

function DisplaySspBucketsData(){
	var SspBuckets = window.opener.Control.SSPBuckets;
	var tempStr = "";
	
	for (var loopIndex = 0; loopIndex < SspBuckets.length; loopIndex++){
	    var bucket = SspBuckets[loopIndex];
	    var SspBucketArray = new Array();
	    var SspBucketID = "SSP_" + loopIndex;
	    var SspBucketName = bucket.Id;
	    var idx = 0;
	    SspBucketArray[idx++] = "Bucket Index: " + bucket.BucketIndex;
	    SspBucketArray[idx++] = "ID: " + bucket.Id;
	    SspBucketArray[idx++] = "Bucket Type: " + bucket.BucketType;
	    SspBucketArray[idx++] = "Persistence: " + bucket.Persistence;
	    SspBucketArray[idx++] = "Minimum Size: " + bucket.SizeMin;
	    SspBucketArray[idx++] = "Requested size: " + bucket.SizeRequested;
	    SspBucketArray[idx++] = "Reducible: " + bucket.Reducible;
	    SspBucketArray[idx++] = "Local Activity Id: " + bucket.LocalActivityId;
	    SspBucketArray[idx++] = "Allocation Success: " + bucket.AllocationSuccess;
	    SspBucketArray[idx++] = "Data State: " + bucket.DataState;
	    var bucketData = bucket.Data;
	    if(bucketData.length > 500){
			bucketData = bucketData.substr(0,500) + "..." +
		                    " <span style='cursor: pointer; text-decoration: underline' onclick='DisplayBucketDataWindow(\"" + bucket.Id + "\")'>open</span>";
	    }
	    SspBucketArray[idx++] = "Data: " + 
								bucketData;
	    
	    tempStr += renderDivSection(SspBucketID,SspBucketName,SspBucketArray);
	}	
	return tempStr;
}

function DisplayBucketDataWindow(bucketId){
    //don't use index becaues the GetBucketByIndex function limits the scope to only buckets
    //that the currently active SCO can see (that could be changed though)
	//var bucket = window.opener.Control.Api.SSPApi.GetBucketByIndex(bucketIdx);
	
	var bucket = window.opener.Control.Api.SSPApi.GetBucketById(bucketId, false)
	var win = window.open();
	win.document.write(bucket.Data);
	win.document.close();
}