/* SCORM 2004 4th Edition Compatible Sequencer */

var TERMINATION_REQUEST_EXIT = "EXIT";
var TERMINATION_REQUEST_EXIT_ALL = "EXIT ALL";
var TERMINATION_REQUEST_SUSPEND_ALL = "SUSPEND ALL";
var TERMINATION_REQUEST_ABANDON = "ABANDON";
var TERMINATION_REQUEST_ABANDON_ALL = "ABANDON ALL";
var TERMINATION_REQUEST_EXIT_PARENT = "EXIT PARENT";
var TERMINATION_REQUEST_NOT_VALID = "INVALID";

var SEQUENCING_REQUEST_START = "START";
var SEQUENCING_REQUEST_RESUME_ALL = "RESUME ALL";
var SEQUENCING_REQUEST_CONTINUE = "CONTINUE";
var SEQUENCING_REQUEST_PREVIOUS = "PREVIOUS";
var SEQUENCING_REQUEST_CHOICE = "CHOICE";
var SEQUENCING_REQUEST_RETRY = "RETRY";
var SEQUENCING_REQUEST_EXIT = "EXIT";
var SEQUENCING_REQUEST_NOT_VALID = "INVALID";
var SEQUENCING_REQUEST_JUMP = "JUMP";

var RULE_SET_POST_CONDITION = "POST_CONDITION";
var RULE_SET_EXIT = "EXIT";
var RULE_SET_HIDE_FROM_CHOICE = "HIDE_FROM_CHOICE";
var RULE_SET_STOP_FORWARD_TRAVERSAL = "STOP_FORWARD_TRAVERSAL";
var RULE_SET_DISABLED = "DISABLED";
var RULE_SET_SKIPPED = "SKIPPED";

var RULE_SET_SATISFIED = "SATISFIED";
var RULE_SET_NOT_SATISFIED = "NOT_SATISFIED";
var RULE_SET_COMPLETED = "COMPLETED";
var RULE_SET_INCOMPLETE = "INCOMPLETE";

var SEQUENCING_RULE_ACTION_SKIP = "Skip";
var SEQUENCING_RULE_ACTION_DISABLED = "Disabled";
var SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE = "Hidden From Choice";
var SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL = "Stop Forward Traversal";
var SEQUENCING_RULE_ACTION_EXIT = "Exit";
var SEQUENCING_RULE_ACTION_EXIT_PARENT = "Exit Parent";
var SEQUENCING_RULE_ACTION_EXIT_ALL = "Exit All";
var SEQUENCING_RULE_ACTION_RETRY = "Retry";
var SEQUENCING_RULE_ACTION_RETRY_ALL = "Retry All";
var SEQUENCING_RULE_ACTION_CONTINUE = "Continue";
var SEQUENCING_RULE_ACTION_PREVIOUS = "Previous";

var FLOW_DIRECTION_FORWARD = "FORWARD";
var FLOW_DIRECTION_BACKWARD = "BACKWARD";

var RULE_CONDITION_OPERATOR_NOT = "Not";
var RULE_CONDITION_OPERATOR_NOOP = "No Op";

var RULE_CONDITION_COMBINATION_ALL = "All";
var RULE_CONDITION_COMBINATION_ANY = "Any";

var RESULT_UNKNOWN = "unknown";

var SEQUENCING_RULE_CONDITION_SATISFIED = "Satisfied";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN = "Objective Status Known";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN = "Objective Measure Known";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_GREATER_THAN = "Objective Measure Greater Than";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_LESS_THAN = "Objective Measure Less Than";
var SEQUENCING_RULE_CONDITION_COMPLETED = "Completed";
var SEQUENCING_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN = "Activity Progress Known";
var SEQUENCING_RULE_CONDITION_ATTEMPTED = "Attempted";
var SEQUENCING_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED = "Attempt Limit Exceeded";
var SEQUENCING_RULE_CONDITION_ALWAYS = "Always";

var ROLLUP_RULE_ACTION_SATISFIED = "Satisfied";
var ROLLUP_RULE_ACTION_NOT_SATISFIED = "Not Satisfied";
var ROLLUP_RULE_ACTION_COMPLETED = "Completed";
var ROLLUP_RULE_ACTION_INCOMPLETE = "Incomplete";

var ROLLUP_RULE_MINIMUM_COUNT_DEFAULT = 0;
var ROLLUP_RULE_MINIMUM_PERCENT_DEFAULT = 0;

var CHILD_ACTIVITY_SET_ALL = "All";
var CHILD_ACTIVITY_SET_ANY = "Any";
var CHILD_ACTIVITY_SET_NONE = "None";
var CHILD_ACTIVITY_SET_AT_LEAST_COUNT = "At Least Count";
var CHILD_ACTIVITY_SET_AT_LEAST_PERCENT = "At Least Percent";

var ROLLUP_RULE_CONDITION_SATISFIED = "Satisfied";
var ROLLUP_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN = "Objective Status Known";
var ROLLUP_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN = "Objective Measure Known";
var ROLLUP_RULE_CONDITION_COMPLETED = "Completed";
var ROLLUP_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN = "Activity Progress Known";
var ROLLUP_RULE_CONDITION_ATTEMPTED = "Attempted";
var ROLLUP_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED = "Attempt Limit Exceeded";
var ROLLUP_RULE_CONDITION_NEVER = "Never";

var ROLLUP_CONSIDERATION_ALWAYS = "Always";
var ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED = "If Not Suspended";
var ROLLUP_CONSIDERATION_IF_ATTEMPTED = "If Attempted";
var ROLLUP_CONSIDERATION_IF_NOT_SKIPPED = "If Not Skipped";

var TIMING_NEVER = "Never";
var TIMING_ONCE = "Once";
var TIMING_ON_EACH_NEW_ATTEMPT = "On Each New Attempt";

//ERROR Conditions under which ADL requires invalid choices to be hidden rather than merely disabled
var CONTROL_CHOICE_EXIT_ERROR_NAV = "NB.2.1-8";
var CONTROL_CHOICE_EXIT_ERROR_CHOICE = "SB.2.9-7";
var PREVENT_ACTIVATION_ERROR = "SB.2.9-6";
var CONSTRAINED_CHOICE_ERROR = "SB.2.9-8";


function Sequencer(lookAhead, activities){
	
	this.LookAhead = lookAhead;
	this.Activities = activities;
	
	this.NavigationRequest = null;
	this.ChoiceTargetIdentifier = null;
	
	this.SuspendedActivity = null;
	this.CurrentActivity = null;
	
	this.Exception = null;
	this.ExceptionText = null;
	
	this.GlobalObjectives = new Array();
	this.SharedData = new Array();
	
	this.ReturnToLmsInvoked = false;
}

//functions required by the Controller
Sequencer.prototype.OverallSequencingProcess = Sequencer_OverallSequencingProcess;
Sequencer.prototype.SetSuspendedActivity = Sequencer_SetSuspendedActivity;
Sequencer.prototype.GetSuspendedActivity = Sequencer_GetSuspendedActivity;
Sequencer.prototype.Start = Sequencer_Start;
Sequencer.prototype.InitialRandomizationAndSelection = Sequencer_InitialRandomizationAndSelection;
Sequencer.prototype.GetCurrentActivity = Sequencer_GetCurrentActivity;
Sequencer.prototype.GetExceptionText = Sequencer_GetExceptionText;
Sequencer.prototype.GetExitAction = Sequencer_GetExitAction;
Sequencer.prototype.EvaluatePossibleNavigationRequests = Sequencer_EvaluatePossibleNavigationRequests;
Sequencer.prototype.InitializePossibleNavigationRequestAbsolutes = Sequencer_InitializePossibleNavigationRequestAbsolutes;
Sequencer.prototype.SetAllDescendentsToDisabled = Sequencer_SetAllDescendentsToDisabled;
Sequencer.prototype.SetAllDescendentsToNotSucceed = Sequencer_SetAllDescendentsToNotSucceed;
Sequencer.prototype.SetAllDescendentsToSkipped = Sequencer_SetAllDescendentsToSkipped;
Sequencer.prototype.ContentDeliveryEnvironmentActivityDataSubProcess = Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess;

function Sequencer_SetSuspendedActivity(activity, simpleLogParent){

    if (simpleLogParent !== null && simpleLogParent !== undefined){
        this.LogSeqSimple("Setting Suspended Activity to \"" + activity + "\".", simpleLogParent);
    }
	this.SuspendedActivity = activity;
}

function Sequencer_GetSuspendedActivity(){
	return this.SuspendActivity;
}

function Sequencer_GetSuspendedActivity(logParent){
	
	var suspendedActivity = this.SuspendedActivity;
	
	if (logParent !== null && logParent !== undefined){
		this.LogSeq("Suspended Activity is " + suspendedActivity, logParent);
	}
	
	return suspendedActivity;
}

function Sequencer_Start(){
	
	if (this.SuspendedActivity === null){
	
		this.NavigationRequest = new NavigationRequest(NAVIGATION_REQUEST_START, null, "");
	}
	else{
		this.NavigationRequest = new NavigationRequest(NAVIGATION_REQUEST_RESUME_ALL, null, "");
	}
	
	this.OverallSequencingProcess();
}

function Sequencer_InitialRandomizationAndSelection(){
	
	var logParent = this.LogSeqAudit("Initial Selection and Randomization");
	
	var loggedSelectionAndRandomization = false;
	var simpleLogParent;
	
	if (this.SuspendedActivity === null){
	    
		for (var activityId in this.Activities.ActivityList){
		    
		    var activity = this.Activities.ActivityList[activityId];
		    
		    if (activity.GetRandomizationTiming() != TIMING_NEVER || 
		        activity.GetSelectionTiming() != TIMING_NEVER){
		        
		        if (loggedSelectionAndRandomization === false){
		            simpleLogParent = this.LogSeqSimpleAudit("Initial selection and randomization of activities.");
		        }
		        
		        this.SelectChildrenProcess(activity, logParent, simpleLogParent);
			    this.RandomizeChildrenProcess(activity, false, logParent, simpleLogParent);
		    }
		
		}
	}
}


function Sequencer_GetCurrentActivity(){
	return this.CurrentActivity;
}

function Sequencer_GetExceptionText(){
	if (this.ExceptionText !== null && this.ExceptionText !== undefined){
		return this.ExceptionText;
	}
	else{
		return "";
	}
}

function Sequencer_GetExitAction(activity, logEntry){
	
	return EXIT_ACTION_DISPLAY_MESSAGE;
}



//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//functions private to this sequencer
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

//Core Sequencing Functions
//Sequencer.prototype.OverallSequencingProcess = Sequencer_OverallSequencingProcess;	//set above since required by controller
Sequencer.prototype.NavigationRequestProcess = Sequencer_NavigationRequestProcess;
Sequencer.prototype.SequencingExitActionRulesSubprocess = Sequencer_SequencingExitActionRulesSubprocess;
Sequencer.prototype.SequencingPostConditionRulesSubprocess = Sequencer_SequencingPostConditionRulesSubprocess;
Sequencer.prototype.TerminationRequestProcess = Sequencer_TerminationRequestProcess;
Sequencer.prototype.MeasureRollupProcess = Sequencer_MeasureRollupProcess;
Sequencer.prototype.CompletionMeasureRollupProcess = Sequencer_CompletionMeasureRollupProcess;
Sequencer.prototype.ObjectiveRollupProcess = Sequencer_ObjectiveRollupProcess;
Sequencer.prototype.ObjectiveRollupUsingMeasureProcess = Sequencer_ObjectiveRollupUsingMeasureProcess;
Sequencer.prototype.ObjectiveRollupUsingRulesProcess = Sequencer_ObjectiveRollupUsingRulesProcess;
Sequencer.prototype.ActivityProgressRollupProcess = Sequencer_ActivityProgressRollupProcess;
Sequencer.prototype.ActivityProgressRollupUsingMeasureProcess = Sequencer_ActivityProgressRollupUsingMeasureProcess;
Sequencer.prototype.ActivityProgressRollupUsingRulesProcess = Sequencer_ActivityProgressRollupUsingRulesProcess;
Sequencer.prototype.RollupRuleCheckSubprocess = Sequencer_RollupRuleCheckSubprocess;
Sequencer.prototype.EvaluateRollupConditionsSubprocess = Sequencer_EvaluateRollupConditionsSubprocess;
Sequencer.prototype.EvaluateRollupRuleCondition = Sequencer_EvaluateRollupRuleCondition;
Sequencer.prototype.CheckChildForRollupSubprocess = Sequencer_CheckChildForRollupSubprocess;
Sequencer.prototype.OverallRollupProcess = Sequencer_OverallRollupProcess;
Sequencer.prototype.SelectChildrenProcess = Sequencer_SelectChildrenProcess;
Sequencer.prototype.RandomizeChildrenProcess = Sequencer_RandomizeChildrenProcess;
Sequencer.prototype.FlowTreeTraversalSubprocess = Sequencer_FlowTreeTraversalSubprocess;
Sequencer.prototype.FlowActivityTraversalSubprocess = Sequencer_FlowActivityTraversalSubprocess;
Sequencer.prototype.FlowSubprocess = Sequencer_FlowSubprocess;
Sequencer.prototype.JumpSequencingRequestProcess = Sequencer_JumpSequencingRequestProcess;
Sequencer.prototype.ChoiceActivityTraversalSubprocess = Sequencer_ChoiceActivityTraversalSubprocess;
Sequencer.prototype.StartSequencingRequestProcess = Sequencer_StartSequencingRequestProcess;
Sequencer.prototype.ResumeAllSequencingRequestProcess = Sequencer_ResumeAllSequencingRequestProcess;
Sequencer.prototype.ContinueSequencingRequestProcess = Sequencer_ContinueSequencingRequestProcess;
Sequencer.prototype.PreviousSequencingRequestProcess = Sequencer_PreviousSequencingRequestProcess;
Sequencer.prototype.ChoiceSequencingRequestProcess = Sequencer_ChoiceSequencingRequestProcess;
Sequencer.prototype.ChoiceFlowSubprocess = Sequencer_ChoiceFlowSubprocess;
Sequencer.prototype.ChoiceFlowTreeTraversalSubprocess = Sequencer_ChoiceFlowTreeTraversalSubprocess;
Sequencer.prototype.RetrySequencingRequestProcess = Sequencer_RetrySequencingRequestProcess;
Sequencer.prototype.ExitSequencingRequestProcess = Sequencer_ExitSequencingRequestProcess;
Sequencer.prototype.SequencingRequestProcess = Sequencer_SequencingRequestProcess;
Sequencer.prototype.DeliveryRequestProcess = Sequencer_DeliveryRequestProcess;
Sequencer.prototype.ContentDeliveryEnvironmentProcess = Sequencer_ContentDeliveryEnvironmentProcess;
Sequencer.prototype.ClearSuspendedActivitySubprocess = Sequencer_ClearSuspendedActivitySubprocess;
Sequencer.prototype.LimitConditionsCheckProcess = Sequencer_LimitConditionsCheckProcess;
Sequencer.prototype.SequencingRulesCheckProcess = Sequencer_SequencingRulesCheckProcess;
Sequencer.prototype.SequencingRulesCheckSubprocess = Sequencer_SequencingRulesCheckSubprocess;
Sequencer.prototype.TerminateDescendentAttemptsProcess = Sequencer_TerminateDescendentAttemptsProcess;
Sequencer.prototype.EndAttemptProcess = Sequencer_EndAttemptProcess;
Sequencer.prototype.CheckActivityProcess = Sequencer_CheckActivityProcess;
Sequencer.prototype.EvaluateSequencingRuleCondition = Sequencer_EvaluateSequencingRuleCondition;

//internal functions
Sequencer.prototype.ResetException = Sequencer_ResetException;

Sequencer.prototype.LogSeq = Sequencer_LogSeq;
Sequencer.prototype.LogSeqAudit = Sequencer_LogSeqAudit;
Sequencer.prototype.LogSeqReturn = Sequencer_LogSeqReturn;
Sequencer.prototype.WriteHistoryLog = Sequencer_WriteHistoryLog;
Sequencer.prototype.WriteHistoryReturnValue = Sequencer_WriteHistoryReturnValue;

Sequencer.prototype.LogSeqSimple = Sequencer_LogSeqSimple
Sequencer.prototype.LogSeqSimpleAudit = Sequencer_LogSeqSimpleAudit
Sequencer.prototype.LogSeqSimpleReturn = Sequencer_LogSeqSimpleReturn

Sequencer.prototype.SetCurrentActivity = Sequencer_SetCurrentActivity;
Sequencer.prototype.IsCurrentActivityDefined = Sequencer_IsCurrentActivityDefined;


Sequencer.prototype.IsSuspendedActivityDefined = Sequencer_IsSuspendedActivityDefined;	
Sequencer.prototype.ClearSuspendedActivity = Sequencer_ClearSuspendedActivity;

Sequencer.prototype.GetRootActivity = Sequencer_GetRootActivity;

Sequencer.prototype.DoesActivityExist = Sequencer_DoesActivityExist;

Sequencer.prototype.GetActivityFromIdentifier = Sequencer_GetActivityFromIdentifier;
Sequencer.prototype.AreActivitiesSiblings = Sequencer_AreActivitiesSiblings;
Sequencer.prototype.FindCommonAncestor = Sequencer_FindCommonAncestor;

Sequencer.prototype.GetActivityPath = Sequencer_GetActivityPath;
Sequencer.prototype.GetPathToAncestorExclusive = Sequencer_GetPathToAncestorExclusive;
Sequencer.prototype.GetPathToAncestorInclusive = Sequencer_GetPathToAncestorInclusive;

Sequencer.prototype.ActivityHasSuspendedChildren = Sequencer_ActivityHasSuspendedChildren;
Sequencer.prototype.CourseIsSingleSco = Sequencer_CourseIsSingleSco;

Sequencer.prototype.TranslateSequencingRuleActionIntoSequencingRequest = Sequencer_TranslateSequencingRuleActionIntoSequencingRequest;
Sequencer.prototype.TranslateSequencingRuleActionIntoTerminationRequest = Sequencer_TranslateSequencingRuleActionIntoTerminationRequest;

Sequencer.prototype.IsActivity1BeforeActivity2 = Sequencer_IsActivity1BeforeActivity2;
Sequencer.prototype.GetOrderedListOfActivities = Sequencer_GetOrderedListOfActivities;
Sequencer.prototype.PreOrderTraversal = Sequencer_PreOrderTraversal;
Sequencer.prototype.PostOrderTraversal = Sequencer_PostOrderTraversal;
Sequencer.prototype.IsActivityLastOverall = Sequencer_IsActivityLastOverall;

Sequencer.prototype.GetGlobalObjectiveByIdentifier = Sequencer_GetGlobalObjectiveByIdentifier;
Sequencer.prototype.AddGlobalObjective = Sequencer_AddGlobalObjective;
Sequencer.prototype.ResetGlobalObjectives = Sequencer_ResetGlobalObjectives;
Sequencer.prototype.ResetSharedData = Sequencer_ResetSharedData;
Sequencer.prototype.FindActivitiesAffectedByWriteMaps = Sequencer_FindActivitiesAffectedByWriteMaps;
Sequencer.prototype.FindDistinctParentsOfActivitySet = Sequencer_FindDistinctParentsOfActivitySet;
Sequencer.prototype.FindDistinctAncestorsOfActivitySet = Sequencer_FindDistinctAncestorsOfActivitySet;
Sequencer.prototype.GetMinimalSubsetOfActivitiesToRollup = Sequencer_GetMinimalSubsetOfActivitiesToRollup;

Sequencer.prototype.CheckForRelevantSequencingRules = Sequencer_CheckForRelevantSequencingRules;
Sequencer.prototype.DoesThisActivityHaveSequencingRulesRelevantToChoice = Sequencer_DoesThisActivityHaveSequencingRulesRelevantToChoice;

Sequencer.prototype.GetArrayOfDescendents = Sequencer_GetArrayOfDescendents;
Sequencer.prototype.IsActivity1AParentOfActivity2 = Sequencer_IsActivity1AParentOfActivity2;


function Sequencer_ResetException(){
	this.Exception = null;
	this.ExceptionText = null;
}


function Sequencer_LogSeq(str, logEntry){

	if (logEntry === null || logEntry === undefined){
		Debug.AssertError("failed to pass logEntry");
	}
	
	str = str + "";
	
	if (this.LookAhead === true){
		return Debug.WriteLookAheadDetailed(str, logEntry);
	}
	else{
		return Debug.WriteSequencingDetailed(str, logEntry);
	}
}

function Sequencer_LogSeqAudit(str, logEntry){
	
	str = str + "";
	
	if (this.LookAhead === true){
		return Debug.WriteLookAheadAudit(str, logEntry);
	}
	else{
		return Debug.WriteSequencingAudit(str, logEntry);
	}
}

function Sequencer_LogSeqReturn(str, logEntry){
	
	if (logEntry === null || logEntry === undefined){
		Debug.AssertError("failed to pass logEntry");
	}
	
	str = str + "";
	
	if (this.LookAhead === true){
		return logEntry.setReturn(str);
	}
	else{
		return logEntry.setReturn(str);
	}
}

function Sequencer_LogSeqSimple(str, logEntry){

	if (logEntry === null || logEntry === undefined){
		Debug.AssertError("failed to pass logEntry");
	}
	
	str = str + "";
	
	if (this.LookAhead === true){
		return Debug.WriteSequencingSimpleDetailed(str, logEntry);
	}
	else{
		return Debug.WriteSequencingSimpleDetailed(str, logEntry);
	}
}

function Sequencer_LogSeqSimpleAudit(str, logEntry){
	str = str + "";
	
	if (this.LookAhead === true){
		return Debug.WriteSequencingSimpleAudit(str, logEntry);
	}
	else{
		return Debug.WriteSequencingSimpleAudit(str, logEntry);
	}
}

function Sequencer_LogSeqSimpleReturn(str, logEntry){
	
	if (logEntry === null || logEntry === undefined){
		Debug.AssertError("failed to pass logEntry");
	}
	
	str = str + "";
	
	if (this.LookAhead === true){
		return logEntry.setReturn(str);
	}
	else{
		return logEntry.setReturn(str);
	}
}


function Sequencer_WriteSequencingSimple(str){
    str = str + "";
    return Debug.WriteSequencingSimple(str);
}

function Sequencer_WriteHistoryLog(str, atts){
	HistoryLog.WriteEventDetailed(str, atts);
}

function Sequencer_WriteHistoryReturnValue(str, atts){
	HistoryLog.WriteEventDetailedReturnValue(str, atts);
}

function Sequencer_SetCurrentActivity(activity, logParent, simpleLogParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	this.LogSeq("Setting Current Activity to " + activity, logParent);
	this.LogSeqSimple("Setting Current Activity to \"" + activity + "\".", simpleLogParent);
	this.CurrentActivity = activity;
}

function Sequencer_IsCurrentActivityDefined(logParent){
	
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	
	var currentActivity = this.GetCurrentActivity();
	
	var currentActivityExists = (currentActivity !== null);
	
	if (currentActivityExists){
		this.LogSeq("Current Activity is defined", logParent);
	}
	else{
		this.LogSeq("Current Activity is not defined", logParent);
	}
	
	return currentActivityExists;
}





function Sequencer_IsSuspendedActivityDefined(logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	
	var suspendedActivity = this.GetSuspendedActivity(logParent);
	
	var suspendedActivityExists = (suspendedActivity !== null);

	if (suspendedActivityExists){
		this.LogSeq("Suspended Activity is defined", logParent);
	}
	else{
		this.LogSeq("Suspended Activity is not defined", logParent);
	}
	
	return suspendedActivityExists;
}

function Sequencer_ClearSuspendedActivity(logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	this.LogSeq("Clearing Suspended Activity", logParent);
	this.SuspendedActivity = null;
}


function Sequencer_GetRootActivity(logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	
	var rootActivity = this.Activities.GetRootActivity();
	
	//this.LogSeq("Root Activity is " + rootActivity, logParent);
	
	return rootActivity;
}

function Sequencer_DoesActivityExist(identifier, logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	
	var exists = this.Activities.DoesActivityExist(identifier, logParent);
	
	if (exists){
		this.LogSeq("The identifier '" + identifier + "' represents a valid activity.", logParent);
	}
	else{
		this.LogSeq("The identifier '" + identifier + "' does not represent a valid activity.", logParent);
	}
	
	return exists;
	
}

function Sequencer_GetActivityFromIdentifier(identifier, logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	
	var activity = this.Activities.GetActivityFromIdentifier(identifier, logParent);
	
	/*
	if (activity !== null){
		this.LogSeq("The identifier '" + identifier + "' corresponds to activity " + activity, logParent);
	}
	else{
		this.LogSeq("The identifier '" + identifier + "' does not correspond to valid activity.", logParent);
	}
	*/
	
	return activity;
	
}

function Sequencer_AreActivitiesSiblings(activity1, activity2, logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	
	//needs to be able to accept null arguments
	if (activity1 === null || activity1 === undefined || activity2 === null || activity2 === undefined){
		return false;
	}

	var parent1 = activity1.ParentActivity;
	var parent2 = activity2.ParentActivity;
	
	var siblings = (parent1 == parent2);
	
	if (siblings){
		this.LogSeq("The activities " + activity1 + " and " + activity2 + " are siblings.", logParent);
	}
	else{
		this.LogSeq("The activities " + activity1 + " and " + activity2 + " are not siblings.", logParent);
	}
	
	return siblings;
}

function Sequencer_FindCommonAncestor(activity1, activity2, logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	
	var aryActivity1Parents = new Array();
	var aryActivity2Parents = new Array();
	
	//MR 12/7/05 Changed to account for one of the activities being the root
	if (activity1 !== null && activity1.IsTheRoot()){
		this.LogSeq(activity1 + "is the root, therefore the common ancestor of activities " + activity1 + " and " + activity2 + " is " + activity1, logParent);
		return activity1;
	}
	
	if (activity2 !== null && activity2.IsTheRoot()){
		this.LogSeq(activity2 + "is the root, therefore the common ancestor of activities " + activity1 + " and " + activity2 + " is " + activity2, logParent);
		return activity2;
	}
	
	//MR 5-21-05 changed GetActivityPath to not include the actual activity
	
	if (activity1 !== null){
		aryActivity1Parents = this.Activities.GetActivityPath(activity1, false);
	}
	
	if (activity2 !== null){
		aryActivity2Parents = this.Activities.GetActivityPath(activity2, false);
	}
	
	for (var i=0; i < aryActivity1Parents.length; i++){
		
		for (var j=0; j < aryActivity2Parents.length; j++){
			if (aryActivity1Parents[i] == aryActivity2Parents[j]){
				
				this.LogSeq("The common ancestor of activities " + activity1 + " and " + activity2 + " is " + aryActivity1Parents[i], logParent);
				return aryActivity1Parents[i];
			}
		}
	}
	
	this.LogSeq("Activities " + activity1 + " and " + activity2 + " do not have a common ancestor", logParent);
	return null;
}

function Sequencer_GetActivityPath(activity, includeActivity){
	return this.Activities.GetActivityPath(activity, includeActivity);
}

//TODO: improvement - should we consolidate the PathToAncestorExlusive/Inclusive functions into one function?

//NOTE: inclusive and exclusive refers to the Ancestor, the includeActivity parameter refers to the current activity

function Sequencer_GetPathToAncestorExclusive(activity, ancestorActivity, includeActivity){
	
	var aryParentActivities = new Array();
	var index = 0;
	
	if (activity !== null && ancestorActivity !== null && activity !== ancestorActivity){

		if (includeActivity === true){
			aryParentActivities[index] = activity;
			index++;
		}
		
		while (activity.ParentActivity !== null && activity.ParentActivity !== ancestorActivity){
			
			activity = activity.ParentActivity;
			
			aryParentActivities[index] = activity;
			
			index++;
		}
	}
	
	return aryParentActivities;
}

function Sequencer_GetPathToAncestorInclusive(activity, ancestorActivity, includeActivity){
	
	var aryParentActivities = new Array();
	var index = 0;
	
	if (includeActivity == null || includeActivity == undefined){
		includeActivity === true;
	}
	
	
	aryParentActivities[index] = activity;
	index++;
	
	//MR - 5/21/05 - changed from activity == ancestorActivity to activity != ancestorActivity
	
	while (activity.ParentActivity !== null && activity != ancestorActivity){
		
		activity = activity.ParentActivity;
		
		aryParentActivities[index] = activity;
		
		index++;
	}
	
	if (includeActivity === false){	
		aryParentActivities.splice(0,1);
	}
	
	return aryParentActivities;
}

function Sequencer_ActivityHasSuspendedChildren(activity, logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	
	var children = activity.GetChildren();
	var hasSuspended = false;
	
	for (var i=0; i < children.length; i++){
		if (children[i].IsSuspended()){
			hasSuspended = true;
		}
	}
	
	if (hasSuspended){
		this.LogSeq("The activity " + activity + " has suspended children.", logParent);
	}
	else{
		this.LogSeq("The activity " + activity + " does not have suspended children.", logParent);
	}
	
	return hasSuspended;
}

function Sequencer_CourseIsSingleSco(){
	if (this.Activities.ActivityList.length <= 2){
		return true;
	}
	else{
		return false;
	}
}

function Sequencer_TranslateSequencingRuleActionIntoSequencingRequest(ruleAction){
	
	switch (ruleAction){
		
		case SEQUENCING_RULE_ACTION_RETRY:
			return SEQUENCING_REQUEST_RETRY;
		//break;
		
		case SEQUENCING_RULE_ACTION_CONTINUE:
			return SEQUENCING_REQUEST_CONTINUE;
		//break;
		
		case SEQUENCING_RULE_ACTION_PREVIOUS:
			return SEQUENCING_REQUEST_PREVIOUS;
		//break;
		
		default:
			Debug.AssertError("ERROR in TranslateSequencingRuleActionIntoSequencingRequest - should never have an untranslatable sequencing request. ruleAction=" + ruleAction);
			return null;
		//break;
	}
	
}

function Sequencer_TranslateSequencingRuleActionIntoTerminationRequest(ruleAction){

	switch (ruleAction){
		
		case SEQUENCING_RULE_ACTION_EXIT_PARENT:
			return TERMINATION_REQUEST_EXIT_PARENT;
		//break;
		
		case SEQUENCING_RULE_ACTION_EXIT_ALL:
			return TERMINATION_REQUEST_EXIT_ALL;
		//break;
				
		default:
			Debug.AssertError("ERROR in TranslateSequencingRuleActionIntoTerminationRequest - should never have an untranslatable sequencing request. ruleAction=" + ruleAction);
			return null;
		//break;
	}			
}

function Sequencer_IsActivity1BeforeActivity2(activity1, activity2, logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	
	var orderedListOfActivities = this.GetOrderedListOfActivities(false, logParent);
	
	for (var i=0; i < orderedListOfActivities.length; i++){
		
		if (orderedListOfActivities[i] == activity1){
			this.LogSeq("The activity " + activity1 + " is before the activity " + activity2, logParent);
			return true;
		}
		
		if (orderedListOfActivities[i] == activity2){
			this.LogSeq("The activity " + activity1 + " is after the activity " + activity2, logParent);
			return false;
		}
	}
	
	Debug.AssertError("ERROR IN Sequencer_IsActivity1BeforeActivity2");
	return null;
}

function Sequencer_GetOrderedListOfActivities(blnBackward, logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	Debug.AssertError("Traversal Direction Not Passed.", (blnBackward === undefined || blnBackward === null));

	var list;
	var root = this.GetRootActivity(logParent);
	
	if (blnBackward === false){
	    list = this.PreOrderTraversal(root);
	}
	else{
	    list = this.PostOrderTraversal(root);
	}
	return list;
	
}

function Sequencer_PreOrderTraversal(activity){
	
	//distinct from the sorted list in ActivityRepository which uses all children rather than just available children
	
	var list = new Array();
	list[0] = activity;
	
	var children = activity.GetAvailableChildren();
	var nextActivities;
	
	for (var i=0; i < children.length; i++){
		nextActivities = this.PreOrderTraversal(children[i]);
		list = list.concat(nextActivities);
	}
	
	return list;
}

function Sequencer_PostOrderTraversal(activity){
	
	//distinct from the sorted list in ActivityRepository which uses all children rather than just available children
	
	var list = new Array();

	var children = activity.GetAvailableChildren();
	var nextActivities;
	
	for (var i=0; i < children.length; i++){
		nextActivities = this.PostOrderTraversal(children[i]);
		list = list.concat(nextActivities);
	}
	
	list = list.concat(activity);
	
	return list;
}

function Sequencer_IsActivityLastOverall(activity, logParent){
	Debug.AssertError("Parent log not passed.", (logParent === undefined || logParent === null));
	
	var orderedListOfActivities = this.GetOrderedListOfActivities(false, logParent);
	var lastAvailableActivity = null;
	
	for (var i = (orderedListOfActivities.length - 1); i >= 0; i--){
		if (orderedListOfActivities[i].IsAvailable()){
			lastAvailableActivity = orderedListOfActivities[i];
			i = -1;	//break the loop
		}
	}
	
	if (activity == lastAvailableActivity){
		this.LogSeq("The activity " + activity + " is the last overall activity", logParent);
		return true;
	}
	
	this.LogSeq("The activity " + activity + " is not the last overall activity", logParent);
	return false;
}

//TODO - improvement - make this more efficient by changing the GlobalObjectives array to an associative array so we can perform the lookup quicker
function Sequencer_GetGlobalObjectiveByIdentifier(identifier){

	for(var obj in this.GlobalObjectives){
		if (this.GlobalObjectives[obj].ID == identifier){
			return this.GlobalObjectives[obj];
		}
	}
	
	return null;
}

function Sequencer_AddGlobalObjective(ID, ObjectiveProgressStatus, SatisfiedStatus, MeasureStatus, NormalizedMeasure){
	
	var newIndex = this.GlobalObjectives.length;
	
	var obj = new GlobalObjective(newIndex, ID, ObjectiveProgressStatus, SatisfiedStatus, MeasureStatus, NormalizedMeasure);
	
	this.GlobalObjectives[newIndex] = obj;
	this.GlobalObjectives[newIndex].SetDirtyData();
}


function Sequencer_ResetGlobalObjectives(){
	
	var global;
	for (var obj in this.GlobalObjectives){
		global = this.GlobalObjectives[obj];
		global.ResetState();
	}
}

function Sequencer_ResetSharedData(){
	
	var sd;
	for (var sharedDataItem in this.SharedData){
		sd = this.SharedData[sharedDataItem];
		sd.WriteData("");
	}
}

function Sequencer_FindActivitiesAffectedByWriteMaps(activity){
	
	var aryWriteMapTargets = new Array();
	var objMaps;
	var objectives = activity.GetObjectives();
	
	var affectedActivities = new Array();
	
	var i;
	var j;
	
	//find the identifiers of all the objectives this activity writes to
	for (i=0; i < objectives.length; i++){
		objMaps = objectives[i].GetMaps();
		
		for (j=0; j < objMaps.length; j++){
			if (objMaps[j].WriteSatisfiedStatus === true || objMaps[j].WriteNormalizedMeasure === true || objMaps[j].WriteCompletionStatus === true || objMaps[j].WriteProgressMeasure === true){
				aryWriteMapTargets[aryWriteMapTargets.length] = objMaps[j].TargetObjectiveId;
			}
		}
	}
	
	//if there aren't any write maps, then don't bother searching for targets
	if (aryWriteMapTargets.length === 0){
		return affectedActivities;
	}
	
	//now find all the activities that read these objectives
	var contentObject;
	var contentObjectObjectives;
	var contentObjectObjectiveMaps;
	var readTarget;
	
	//search every activity in this course (except the original activity)
	for (var activityToCheck in this.Activities.ActivityList){
		
		contentObject = this.Activities.ActivityList[activityToCheck];
		
		if (contentObject != activity){
		
			contentObjectObjectives = contentObject.GetObjectives();
			
			//search each objective in the activity
			for (var objective=0; objective < contentObjectObjectives.length; objective++){
				
				contentObjectObjectiveMaps = contentObjectObjectives[objective].GetMaps();
				
				//search all of the objective's objective maps
				for (var map=0; map < contentObjectObjectiveMaps.length; map++){
					
					//see if this map reads from a target that is in the list of targets that are written to
					
					if (contentObjectObjectiveMaps[map].ReadSatisfiedStatus === true ||
					    contentObjectObjectiveMaps[map].ReadNormalizedMeasure === true ||
					    contentObjectObjectiveMaps[map].ReadCompletionStatus === true ||
					    contentObjectObjectiveMaps[map].ReadProgressMeasure === true ){
						readTarget = contentObjectObjectiveMaps[map].TargetObjectiveId;
						
						for (var target=0; target < aryWriteMapTargets.length; target++){
							if (aryWriteMapTargets[target] == readTarget){
								affectedActivities[affectedActivities.length] = contentObject;
							}
						}
						
					}
				}
			}
		}
	}
	
	return affectedActivities;
}

function Sequencer_FindDistinctAncestorsOfActivitySet(activityArray){
      
      var distinctAncestors = new Array();
            
      for (var activityIndex=0; activityIndex<activityArray.length; activityIndex++){
            var nextActivity = activityArray[activityIndex];
            
            if (nextActivity !== null) {
				var activityPath = this.GetActivityPath(nextActivity, true)
	            
				for (var i=0; i < activityPath.length; i++) {
	                  
					var isDistinct = true;
					for (var j=0; j < distinctAncestors.length; j++) {
							if (activityPath[i] == distinctAncestors[j]) {
								isDistinct = false;
								break;
							}
					}
					if (isDistinct) {
							distinctAncestors[distinctAncestors.length] = activityPath[i];
					}
				}
            }
      }
      
      return distinctAncestors;
}


function Sequencer_FindDistinctParentsOfActivitySet(activityArray){
	
	var distinctParents = new Array();
	var parent;
	var isDistinct;
	
	for (var activityIndex in activityArray){
		
		isDistinct = true;
		
		parent = this.Activities.GetParentActivity(activityArray[activityIndex]);
		
		if (parent !== null){
			for (var i=0; i < distinctParents.length; i++){
				if (distinctParents[i] == parent){
					isDistinct = false;
					break;
				}
			}
			
			if (isDistinct){
				distinctParents[distinctParents.length] = parent;
			}
		}
	}
	
	return distinctParents;
}

function Sequencer_GetMinimalSubsetOfActivitiesToRollup(activityArray, alreadyRolledUpActivities){
	/*
	When calling rollup on an activity, all of its parents get rolled up as well.
	So, if we're going to call rollup on a number of activities, we want to make sure
	that one activity isn't a parent of another since this would be redundent.
	
	alreadyRolledUpActivities allows us to exclude activties that were just recently rolled up 
	and that can also be excluded
	*/
	

	var activitiesThatAreIncluded = new Array();
	var minimalSubset = new Array();
	
	var parentActivities;
	var currentActivity;
	var alreadyIncluded;
	
	if (alreadyRolledUpActivities !== null){
		activitiesThatAreIncluded = activitiesThatAreIncluded.concat(alreadyRolledUpActivities);
	}
	
	for (var activityIndex in activityArray){
		
		currentActivity = activityArray[activityIndex];
		
		//check to see if this activity is already in the rollup set
		alreadyIncluded = false;
		for (var includedIndex in activitiesThatAreIncluded){
			if (activitiesThatAreIncluded[includedIndex] == currentActivity){
				alreadyIncluded = true;
				break;
			}
		}
		
		if (alreadyIncluded === false){
			minimalSubset[minimalSubset.length] = currentActivity;
			
			parentActivities = this.GetActivityPath(currentActivity, true);
			activitiesThatAreIncluded = activitiesThatAreIncluded.concat(parentActivities);
		}
	}
	
	return minimalSubset;
}


//TODO: if we have a case there a global objective being satisfied hides a selection from choice, then as soon as the sco that writes that objective
//is entered, the hidden from choice evaluates to true....is this funny? do we need to deal with it?

function Sequencer_EvaluatePossibleNavigationRequests(aryPossibleRequests){

	var logParent = this.LogSeqAudit("Evaluate Possible Navigation Requests Process [EPNR]");
	
	
	//for each possible navigation request
		//run the Navigation Request Process
		//set Allowed for the request = the result of the Navigation Request Process
	
	var navigationRequestResult;
	var terminationRequestResult = null;
	var sequencingRequestResult;
	var deliveryRequestResult;
	var id;
	
	var simpleLogParent = this.LogSeqSimpleAudit("Start of simple logs in lookahead sequencer.", simpleLogParent);
	
	if (Control.Package.Properties.UseQuickLookaheadSequencer === true){
	    
	    //only evaluate stop forward traversal if we have a precondition rule that evaluates to true for it
	    var needToConsiderStopForwardTraversal = false;
	    
	    this.LogSeq("[EPNR] Using the 'Quick' Lookahead Sequencing Mode", logParent);
	    
	    this.LogSeq("[EPNR] Clearing out possible navigation request data", logParent);
	    //clear out the initial state
	    for (id in aryPossibleRequests){
	        aryPossibleRequests[id].ResetForNewEvaluation();
	    }
	    
	    var orderedActivityList = this.GetOrderedListOfActivities(false, logParent);
	    var reverseOrderedActivityList = this.GetOrderedListOfActivities(true, logParent);
	    
	    var tmpNavRequest;
	    
	    // Before we terminate the current activity, store a list of the active path (current activity up to root)
	    var activeActivities = new Array();
	    for (var i=0; i < orderedActivityList.length; i++){
	        if (orderedActivityList[i].IsActive() === true){
	            activeActivities[orderedActivityList[i].GetItemIdentifier()] = true;
	        }
	    }
	    
	    /*
	    Run termination request process to exit the current activity and 
	    invoke rollup so we have a current state on all activities
	    */
	    var currentActivity = this.GetCurrentActivity();
    	this.LogSeq("[EPNR] The current activity is " + currentActivity, logParent);
    	
    	var parentActivity = null;
    	if (currentActivity !== null){
    	    parentActivity = currentActivity.ParentActivity;
    	}
    	
  
	    if (currentActivity !== null && currentActivity.IsActive() === true){
		    this.LogSeq("[EPNR] Run the Termination Request Process To Move Current Runtime Data to Sequencer and Invoke Rollup", logParent);
		    terminationRequestResult = this.TerminationRequestProcess(TERMINATION_REQUEST_EXIT, logParent, simpleLogParent);
		}
		
		var contextIndependendSequencingRulesLogParent = this.LogSeqAudit("[EPNR] Check each activity for sequencing rules that are independent of context (i.e. independent of the current activity)", logParent);
		
		for (var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE; i < aryPossibleRequests.length; i++){
		    
		    var possibleRequest = aryPossibleRequests[i];
            
            //initialize the target activity for each request
            possibleRequest.TargetActivity = this.GetActivityFromIdentifier(possibleRequest.TargetActivityItemIdentifier, logParent);
            
		    var requestActivity = possibleRequest.TargetActivity;
		    var requestParentActivity = requestActivity.ParentActivity;
		    
		    var contextIndependendSequencingRulesActivityLogParent = this.LogSeqAudit("[EPNR] " + requestActivity, contextIndependendSequencingRulesLogParent);

		    
		    //check for parent choice=false (and not root) (affects only this activity) - disable
		    if (requestParentActivity !== null && requestParentActivity.GetSequencingControlChoice() === false){
		        
		        this.LogSeq("[EPNR] " + requestActivity + " will be disabled because its parent does not allow choice requests (" + requestParentActivity + " has Sequencing Control Choice = false).", contextIndependendSequencingRulesActivityLogParent);
		        
		        possibleRequest.WillSucceed = false;
		        possibleRequest.ControlChoiceViolation = true;
		        
		        possibleRequest.Exception = "NB.2.1-10";
		        possibleRequest.ExceptionText = IntegrationImplementation.GetString("Please select 'Next' or 'Previous' to move through {0}.", requestParentActivity.GetTitle());
		    }
		    
		    //check for an aggregation with flow=false. will never identify an activity for delivery so disable (affects only this activity)
		    if (requestActivity.IsALeaf() === false && requestActivity.GetSequencingControlFlow() === false){
		        
		        this.LogSeq("[EPNR] " + requestActivity + " will be disabled because it is a cluster that does not allow flow navigation and thus its children must be selected explicitly.", contextIndependendSequencingRulesActivityLogParent);
		        
		        possibleRequest.WillSucceed = false;
		        possibleRequest.NoDeliverablieActivityViolation = true;
		        
		        possibleRequest.Exception = "SB.2.2-1";
		        possibleRequest.ExceptionText = requestActivity + " does not allow flow navigation so it's children must be selected explicitly.";
		    
		    }
		    
		    //check for isvisible=false (affects only this activity) - hide
		    if (requestActivity.LearningObject.Visible === false){
		    
		        this.LogSeq("[EPNR] " + requestActivity + " will be hidden because its isVisible attribute is set to false. ", contextIndependendSequencingRulesActivityLogParent);
		        
		        //keep will succeed as true here, because it is still a valid target of a choice nav request
		        possibleRequest.WillSucceed = true;
		        possibleRequest.IsVisibleViolation = true;
		        
		        possibleRequest.Exception = "n/a";
		        possibleRequest.ExceptionText = requestActivity + " is set to be invisible to the user.";

		    }
		    
		    //check for limit condition violations (affects all descendents) - disable
		    if (this.LimitConditionsCheckProcess(requestActivity, contextIndependendSequencingRulesActivityLogParent, simpleLogParent) === true){
		    
		        this.LogSeq("[EPNR] " + requestActivity + " (and all of its descendents) will be disabled because its attempt limit has been reached. ", contextIndependendSequencingRulesActivityLogParent);
		        
		        possibleRequest.WillSucceed = false;
		        possibleRequest.LimitConditionViolation = true;
		        
		        possibleRequest.Exception = "DB.1.1-3";
		        possibleRequest.ExceptionText = IntegrationImplementation.GetString("{0} has been attempted the maximum allowed number of times.", requestActivity.GetTitle());

		        this.SetAllDescendentsToNotSucceed(aryPossibleRequests, requestActivity, "LimitConditionViolation", possibleRequest.Exception, possibleRequest.ExceptionText, logParent);
		    }
		    
		    
		    //check precondition rules, 
		    this.LogSeq("[EPNR] Evaluate all precondition rules that could affect the activity.", contextIndependendSequencingRulesActivityLogParent);
		    
		    //precondition - hidden from choice (affects all descendents) - hide
		    var sequencingRulesCheckResult = this.SequencingRulesCheckProcess(aryPossibleRequests[i].TargetActivity, RULE_SET_HIDE_FROM_CHOICE, contextIndependendSequencingRulesActivityLogParent, simpleLogParent);
		    
		    if (sequencingRulesCheckResult !== null){
			    
			    this.LogSeq("[EPNR] A precondition rule indicates that " + requestActivity + " (and all of its descendents) should be hidden. ", contextIndependendSequencingRulesActivityLogParent);
			    
			    possibleRequest.WillSucceed = false;
		        possibleRequest.PreConditionHiddenFromChoice = true;
			    
			    aryPossibleRequests[id].Exception = "SB.2.9-3";
			    aryPossibleRequests[id].ExceptionText = "The activity " + requestActivity.GetTitle() + " (and all of its descendents) should be hidden and is not a valid selection";
			    
			    this.SetAllDescendentsToNotSucceed(aryPossibleRequests, requestActivity, "PreConditionHiddenFromChoice", possibleRequest.Exception, possibleRequest.ExceptionText, contextIndependendSequencingRulesActivityLogParent);
		    }
		    
		    //precondition - disabled (affects all descendents) - disable 
		    sequencingRulesCheckResult = this.SequencingRulesCheckProcess(aryPossibleRequests[i].TargetActivity, RULE_SET_DISABLED, contextIndependendSequencingRulesActivityLogParent, simpleLogParent);
		    
		    if (sequencingRulesCheckResult !== null){
			    
			    this.LogSeq("[EPNR] A precondition rule indicates that " + requestActivity + " (and all of its descendents) should be disabled. ", contextIndependendSequencingRulesActivityLogParent);
			    
			    possibleRequest.WillSucceed = false;
		        possibleRequest.PreConditionDisabled = true;
			    
			    aryPossibleRequests[i].Exception = "DB.1.1-3";
			    aryPossibleRequests[i].ExceptionText = IntegrationImplementation.GetString("{0} (and all of its descendents) are not valid selections because of sequencing rules in this course.", requestActivity.GetTitle());
			    
			    this.SetAllDescendentsToNotSucceed(aryPossibleRequests, requestActivity, "PreConditionDisabled", possibleRequest.Exception, possibleRequest.ExceptionText, contextIndependendSequencingRulesActivityLogParent);
		    }
		    
		    //precondition - stop forward traversal - disable (just mark navigation request, will actually be disabled later)
		    sequencingRulesCheckResult = this.SequencingRulesCheckProcess(aryPossibleRequests[i].TargetActivity, RULE_SET_STOP_FORWARD_TRAVERSAL, contextIndependendSequencingRulesActivityLogParent, simpleLogParent);
		    
		    if (sequencingRulesCheckResult !== null){
			    
			    this.LogSeq("[EPNR] A precondition rule on " + requestActivity + " will Stop Forward Traversal.", contextIndependendSequencingRulesActivityLogParent);
		        possibleRequest.PreConditionStopForwardTraversal = true;
		        
		        needToConsiderStopForwardTraversal = true;
		    }
		    
		    //precondition - skipped - disable (just mark navigation request, will actually be disabled later)
		    sequencingRulesCheckResult = this.SequencingRulesCheckProcess(aryPossibleRequests[i].TargetActivity, RULE_SET_SKIPPED, contextIndependendSequencingRulesActivityLogParent, simpleLogParent);
		    
		    if (sequencingRulesCheckResult !== null){
			    
			    this.LogSeq("[EPNR] " + requestActivity + " (and all its descendents) will be skipped.", contextIndependendSequencingRulesActivityLogParent);			    
		        possibleRequest.PreConditionSkipped = true;
		        this.SetAllDescendentsToSkipped(aryPossibleRequests, requestActivity, activeActivities, contextIndependendSequencingRulesActivityLogParent);
		    }
		    
		}
	
	    //start context specific checking
	    
	    //check for prevent activation
	    for (var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE; i < aryPossibleRequests.length; i++){
		    
		    var possibleRequest = aryPossibleRequests[i];
		    requestActivity = possibleRequest.TargetActivity;
		    
            //check for prevent activation=true (affects all descendents, but not the actual activity) - hide
            //but, if the activity is active, then prevent activation doesn't apply             
	        if (requestActivity.GetPreventActivation() === true){
    	    
    	        if (activeActivities[requestActivity.GetItemIdentifier()] !== true){
    	    
	                this.LogSeq("[EPNR] " + requestActivity + " (and all of its descendents) will be hidden because its Prevent Activation attribute is set to true. ", logParent);

	                this.SetAllDescendentsToNotSucceed(aryPossibleRequests, requestActivity, "PreventActivationViolation", "SB.2.9-6", requestActivity + " (and all of its descendents) can not be activated with a choice request.", logParent);
	           
	            }
	        }
		    		    
		}
		
		//go through choice requests again now that skipped/disable rules have been evaluated and see if each request
		//will result in an activity to actually be delivered. if not, then disable. (only need to make this check
		//if the request is not already disabled/hidden)
		for (var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE; i < aryPossibleRequests.length; i++){
		    
		    var possibleRequest = aryPossibleRequests[i];
            
            var stillEnabled = 
                (
                possibleRequest.PreConditionDisabled === false && 
                possibleRequest.PreConditionHiddenFromChoice === false &&
                possibleRequest.LimitConditionViolation === false && 
                //possibleRequest.IsVisibleViolation === false &&       //is visible still leave the activity as a legit choice target
                possibleRequest.PreventActivationViolation === false
                );
            
            if (stillEnabled === true){
              
                var requestActivity = possibleRequest.TargetActivity;
                
                var thisActivityIndex = -1;
		        
		        //perf improvement - store this value on the possibleRequest so we don't need to loop every time
		        for (var j=0; j < orderedActivityList.length; j++){
		            if (orderedActivityList[j] == requestActivity){
		                thisActivityIndex = j;
		                break;
		            }
		        }
		    
                /*
                walk forward. 
                    
                    if skipped, move on
                    
                    if find a cluster with flow=false, error and break
                    if disabled, error and break
                    if limit condition violation, error and break
                    
                    if find a deliverable SCO, break
                */
                
                var nextCandidateActivity;
                var nextCandidatePossibleRequest;
                var deliverable = false;
                
                for (var j=thisActivityIndex; j < orderedActivityList.length; j++){
                    
                    nextCandidateActivity = orderedActivityList[j];
                    tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(nextCandidateActivity);
                    
                    //if skipped, don't do any more evaluation, just go to the next activity
                    if (tmpNavRequest.PreConditionSkipped === false){
                        
                        if (nextCandidateActivity.IsALeaf() === false && 
                            nextCandidateActivity.GetSequencingControlFlow() === false){
                            
                            this.LogSeq("[EPNR] Selecting " + requestActivity + " would flow to a cluster (" + nextCandidateActivity + ") that does not permit flow navigation, disabling.", logParent);	
                            
                            //error
                            possibleRequest.WillSucceed = false;
                            possibleRequest.NoDeliverablieActivityViolation = true;
                            possibleRequest.Exception = tmpNavRequest.Exception;
                            possibleRequest.ExceptionText = "Selecting " + requestActivity + " is not currently allowed. Please select another activity or use the previous/next button to navigate.";

                            deliverable = false;
                            
                            break;
                            
                        }
                        else if (tmpNavRequest.PreConditionDisabled === true){
                            //error

                            this.LogSeq("[EPNR] Selecting " + requestActivity + " would flow to an activity (" + nextCandidateActivity + ") that is disabled, disabling.", logParent);	

                            possibleRequest.WillSucceed = false;
                            possibleRequest.NoDeliverablieActivityViolation = true;
                            possibleRequest.Exception = tmpNavRequest.Exception;
                            possibleRequest.ExceptionText = "Selecting " + requestActivity + " is not currently allowed. Please select another activity or use the previous/next button to navigate.";
                            
                            deliverable = false;
                            break;
                            
                        }
                        else if (tmpNavRequest.LimitConditionViolation === true){
                            //error
                            
                            this.LogSeq("[EPNR] Selecting " + requestActivity + " would flow to an activity (" + nextCandidateActivity + ") that has a limit condition violation, disabling.", logParent);	
                            
                            possibleRequest.WillSucceed = false;
                            possibleRequest.NoDeliverablieActivityViolation = true;
                            possibleRequest.Exception = tmpNavRequest.Exception;
                            possibleRequest.ExceptionText = "Selecting " + requestActivity + " is not currently allowed. Please select another activity or use the previous/next button to navigate.";
                           
                            deliverable = false;
                            break;
                        }
                        else if (nextCandidateActivity.IsALeaf() === true){
                            deliverable = true;
                            break;
                        }
                    }
                    
                }

            }

		    
		}
		
		this.LogSeq("[EPNR] Check rules that rely on the context of the current activity.", logParent);
		
		if (currentActivity === null){
		    this.LogSeq("[EPNR] Current activity is undefined, not checking context dependent rules.", logParent);
		}
		else{
		    
		    var currentActivityIndex = -1;
		    var reverseOrderedCurrentactivityIndex = -1;
		    
		    for (var i=0; i < orderedActivityList.length; i++){
		        if (orderedActivityList[i] == currentActivity){
		            currentActivityIndex = i;
		            break;
		        }
		    }
		    
		    for (var i=0; i < reverseOrderedActivityList.length; i++){
		        if (reverseOrderedActivityList[i] == currentActivity){
		            reverseOrderedCurrentactivityIndex = i;
		            break;
		        }
		    }
		    
		    /*
		    -sequencing mode forward only - disable

            if activity has a parent with Forward Only = true
	            find all siblings logically previous to this activity
	                iterate over siblings until reach the active activity
	                    disable sibling and all descendents
		    */
		    
		    if (parentActivity !== null && parentActivity.GetSequencingControlForwardOnly() === true){
		        
		        this.LogSeq("[EPNR] The current activity's parent only allows Forward Traversal. Disable all of the parent's children that are before the active activity.", logParent);
		        
		        var forwardOnlyChildren = parentActivity.GetAvailableChildren();
		        
		        for (var j=0; j < forwardOnlyChildren.length; j++){
		            
	                var currentForwardOnlyChild = forwardOnlyChildren[j];
	                
	                if (activeActivities[currentForwardOnlyChild.GetItemIdentifier()] === true){
	                    this.LogSeq("[EPNR] " + currentForwardOnlyChild + " is currently active, stop disabling.", logParent);
	                    break;
	                }
	                else{
	                    this.LogSeq("[EPNR] Disable " + currentForwardOnlyChild + " and all of its descendents.", logParent);
	                    
	                    tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(currentForwardOnlyChild);
	            
		                tmpNavRequest.WillSucceed = false;
	                    tmpNavRequest.ForwardOnlyViolation = true;
        			    
		                tmpNavRequest.Exception = "SB.2.1-4";
		                tmpNavRequest.ExceptionText = IntegrationImplementation.GetString("The activity '{0}' may only be entered from the beginning.", parentActivity.GetTitle());
		                
		                this.SetAllDescendentsToNotSucceed(aryPossibleRequests, currentForwardOnlyChild, "ForwardOnlyViolation", tmpNavRequest.Exception, tmpNavRequest.ExceptionText, logParent);

	                }
	            }
		    }
		    
		       
	        /*
	        -stop forward traversal - disable
    
            iterate through all forward activities
            if activity has stop forward traversal = true
                if activity is a leaf
                    disable the activity
                else ( activity is a cluster )
                    disable the activity's descendents
                find the activity's forward siblings
                    disable them and their descendents
	        */
	        
	        //only do this if at least 1 precondition rule evaluated to stop forward traversal
	        if (needToConsiderStopForwardTraversal === true){
	            
	            var currentStopForwardActivity;
	            
	            for (var i=currentActivityIndex; i < orderedActivityList.length; i++){
	                
	                currentStopForwardActivity = orderedActivityList[i];
	                tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(currentStopForwardActivity);
	                
	                if (tmpNavRequest.PreConditionStopForwardTraversal === true){
	                    
	                    if (currentStopForwardActivity.IsALeaf() === true){
	                        
	                        //the current activity doesn't get disabled
	                        if (currentStopForwardActivity != currentActivity){
	                        
	                            this.LogSeq("[EPNR] Disable " + currentStopForwardActivity + " because it has a stop forward traversal precondition rule and is a leaf.", logParent);
    	                    
	                            tmpNavRequest.WillSucceed = false;
	                            tmpNavRequest.Exception = "SB.2.4-1";
	                            tmpNavRequest.PreConditionStopForwardTraversalViolation = true;
		                        tmpNavRequest.ExceptionText = IntegrationImplementation.GetString("You are not allowed to move into {0} yet.", currentStopForwardActivity.GetTitle());
	                        }
	                    }
	                    else{
	                        this.LogSeq("[EPNR] Disable the descendents of " + currentStopForwardActivity + " because it has a stop forward traversal precondition rule and is a cluster.", logParent);
	                        
	                        this.SetAllDescendentsToNotSucceed(aryPossibleRequests, currentStopForwardActivity, "PreConditionStopForwardTraversalViolation", tmpNavRequest.Exception, tmpNavRequest.ExceptionText, logParent);
	                    }
	                    
	                    
	                    if (currentStopForwardActivity.IsTheRoot() === false){
	                        
	                        this.LogSeq("[EPNR] Disable the forward siblings of " + currentStopForwardActivity + " because it has a stop forward traversal precondition rule.", logParent);
	                        
	                        var stopForwardParent = currentStopForwardActivity.ParentActivity;
	                        var stopForwardSiblings = stopForwardParent.GetAvailableChildren();
	                        
	                        var blnReachedStopForwardActivity = false;
	                        
	                        for (var j=0; j < stopForwardSiblings.length; j++){
	                        
	                            if (blnReachedStopForwardActivity === true){
	                                
	                                this.LogSeq("[EPNR]" + stopForwardSiblings[j] + " is a forward sibling so it and its descendents will be disabled.", logParent);
	                          
	                                tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(stopForwardSiblings[j]);
	                                tmpNavRequest.PreConditionStopForwardTraversalViolation = true;
	                                tmpNavRequest.WillSucceed = false;
	                                tmpNavRequest.Exception = "SB.2.4-1";
		                            tmpNavRequest.ExceptionText = IntegrationImplementation.GetString("You are not allowed to move into {0} yet.", stopForwardSiblings[j].GetTitle());
		                            this.SetAllDescendentsToNotSucceed(aryPossibleRequests, stopForwardSiblings[j], "PreConditionStopForwardTraversalViolation", tmpNavRequest.Exception, tmpNavRequest.ExceptionText, logParent);
	                    
	                            }
	                        
	                            if (stopForwardSiblings[j] == currentStopForwardActivity){
	                                blnReachedStopForwardActivity = true;
	                            }
	                        }
	                        
	                    }
	                }
	            }
	        }
        
	        /*
	         -choice exit = false - disable
        
            walk from current activity to root
                check for choice exit = false 
                    everything outside of choice exit=false activity is hidden
            */
            var ancestors = this.GetActivityPath(currentActivity, true);
        
            for (var i=0; i < ancestors.length; i++){
            
                if (ancestors[i].GetSequencingControlChoiceExit() === false){
                    
                    this.LogSeq("[EPNR]" + ancestors[i] + " does not allow choice exit requests. Hiding all activities that are not its descendents.", logParent);
	                          
                    var prohibtedExitActivity = ancestors[i];
                    
                    //check all activties to see if they are a child of the choice exit=false activity
                    for (var j=0; j < orderedActivityList.length; j++){
                        
                        if (this.IsActivity1AParentOfActivity2(prohibtedExitActivity, orderedActivityList[j]) === false){
                        
                        	this.LogSeq("[EPNR]" + orderedActivityList[j] + " is not a descendent and will be hidden.", logParent);

                            tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(orderedActivityList[j]);
                            tmpNavRequest.WillSucceed = false;
                            tmpNavRequest.ChoiceExitViolation = true;
                            tmpNavRequest.Exception = CONTROL_CHOICE_EXIT_ERROR_CHOICE;
                            tmpNavRequest.ExceptionText = IntegrationImplementation.GetString("You are not allowed to jump out of {0}.", prohibtedExitActivity.GetTitle());
                        }
                    }
                    
                    break;  //exit the for loop, all other outside activities will be disabled
                }
            }
            
	        /*
	         -constrained choice = true
    
            walk from current activity to root
                check for constrained choice
                    find immediate sibling in both directions
                        everything outside of those siblings descendents is hidden
           */
	        

            for (var i=0; i < ancestors.length; i++){
            
                if (ancestors[i].IsALeaf() === false && ancestors[i].GetConstrainedChoice() === true){
                    
                    var constrainedActivity = ancestors[i];
                    
                    this.LogSeq("[EPNR]" + ancestors[i] + " only allows it immediate siblings to be selected (constrained choice). Only activities that are logically next or previous and their descendents (plus the root) are all valid targets for choice, hiding all other activities.", logParent);
                    
                    //in this case, next and previous are allowed to be aggregations
                    
                    //next is next activity that is not a descendant (from constrained)
                    var activityNextToConstrained = null;
                    
                    var constrainedActivityIndex = -1;
		    
		            for (var j=0; j < orderedActivityList.length; j++){
		                if (orderedActivityList[j] == constrainedActivity){
		                    constrainedActivityIndex = j;
		                    break;
		                }
		            }
                    
                    for (var j=(constrainedActivityIndex + 1); j < orderedActivityList.length; j++){
                        
                        if (constrainedActivity.IsActivityAnAvailableDescendent(orderedActivityList[j]) === false){
                            activityNextToConstrained = orderedActivityList[j];
                            break;
                        }
                    }
                    
                    //previous is first prior sibling of a parent to exist
                    var activityPreviousToConstrained =  null
                    var reverseConstrainedActivityIndex = -1;
		    
		            for (var j=0; j < reverseOrderedActivityList.length; j++){
		                if (reverseOrderedActivityList[j] == constrainedActivity){
		                    reverseConstrainedActivityIndex = j;
		                    break;
		                }
		            }
                    
                    for (var j=(reverseConstrainedActivityIndex - 1); j > 0; j--){
                        if (constrainedActivity.IsActivityAnAvailableDescendent(reverseOrderedActivityList[j]) === false){
                            activityPreviousToConstrained = reverseOrderedActivityList[j];
                            break;
                        }
                    }
                    
                    
                    /*
                    var activityPreviousToConstrained =  null
                    var activityToConsider = constrainedActivity;
                    while (activityPreviousToConstrained === null && activityToConsider.IsTheRoot() === false){
                        
                        activityPreviousToConstrained = activityToConsider.ParentActivity.GetPreviousActivity(activityToConsider);
                        activityToConsider = activityToConsider.ParentActivity;
                    }
                    */
                    
                    //create a lookup array of all permitted activities
                    
                    //add the constrained activity and its children to the allowed activities
                    var unconstrainedActivities = this.GetArrayOfDescendents(constrainedActivity);
                   
                    //the root activity is always allowed
                    unconstrainedActivities = unconstrainedActivities.concat(ancestors[ancestors.length-1]);
                   
                    //add the logically previous sibling and all its descendents
                    if (activityPreviousToConstrained !== null){
                        this.LogSeq("[EPNR] The logically previous activity is " + activityPreviousToConstrained, logParent);
                        unconstrainedActivities = unconstrainedActivities.concat(this.GetArrayOfDescendents(activityPreviousToConstrained));
                    }
                    else{
                        this.LogSeq("[EPNR] There is no logically previous activity.", logParent);
                    }
                    
                    //add the logically next sibling and all its descendents
                    if (activityNextToConstrained !== null){
                        this.LogSeq("[EPNR] The logically next activity is " + activityNextToConstrained, logParent);
                        unconstrainedActivities = unconstrainedActivities.concat(this.GetArrayOfDescendents(activityNextToConstrained));
                    }
                    else{
                        this.LogSeq("[EPNR] There is no logically next activity.", logParent);
                    }
                    
                    //create an associative array of all descendents for easy lookup (can't use concat on associative arrays, b/c technically there is no such thing as a real associative array in JS)
                    var allUnconstrainedActivities = new Array();
                    for (var j=0; j < unconstrainedActivities.length; j++){
                        allUnconstrainedActivities[unconstrainedActivities[j].GetItemIdentifier()] = unconstrainedActivities[j]
                    }
                    
                    
                    //check each activity to see if it is a descendent of the previous/next activity                    
                    for (var j=0; j < orderedActivityList.length; j++){
                        
                        //if the item is not in the array, then it is constrained
                        var unconstrainedActivity = allUnconstrainedActivities[orderedActivityList[j].GetItemIdentifier()];
                        
                        if (unconstrainedActivity === null || unconstrainedActivity === undefined){
                            this.LogSeq("[EPNR]" + orderedActivityList[j] + " is not a descendent of the previous/next activity and will be hidden.", logParent);
                            
                            tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(orderedActivityList[j]);
                            tmpNavRequest.WillSucceed = false;
                            tmpNavRequest.ConstrainedChoiceViolation = true;
                            
                            tmpNavRequest.Exception = CONSTRAINED_CHOICE_ERROR;
                            tmpNavRequest.ExceptionText = IntegrationImplementation.GetString("You are not allowed to jump out of {0}.", ancestors[i].GetTitle());
                        }
                        
                    }
                    
                    break;  //stop at the first constrained activity
                }
            }
            
	    }//end else on if (current activity=null)
	    
	    
	    //Flow Nav Controls
	    
	    var navRequestContinue = aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE];
	    var navRequestPrevious = aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS];
	    
	    if (currentActivity === null){
	    
	        this.LogSeq("[EPNR] Current activity is undefined, flow navigation is disabled.", logParent);
	        
	        navRequestContinue.WillSucceed = false;
	        navRequestContinue.Exception = "NB.2.1-1";
	        navRequestContinue.ExceptionText = IntegrationImplementation.GetString("Cannot continue until the sequencing session has begun.");
	        navRequestContinue.Disabled = true;
	        
	        navRequestPrevious.WillSucceed = false;
	        navRequestPrevious.Exception = "NB.2.1-2";
	        navRequestPrevious.ExceptionText = IntegrationImplementation.GetString("Cannot move backwards until the sequencing session has begun.");
	        navRequestPrevious.Disabled = true;
	    }
	    else{
	        
	        if (parentActivity !== null){
	            if (parentActivity.GetSequencingControlFlow() === false){
    	            
	                this.LogSeq("[EPNR] " + parentActivity + " does not allow flow navigation. Flow navigation is disabled.", logParent);
    	            
	                navRequestContinue.WillSucceed = false;
	                navRequestContinue.Exception = "NB.2.1-4";
	                navRequestContinue.ExceptionText = IntegrationImplementation.GetString("Please select a menu item to continue with {0}.", parentActivity.GetTitle());
	                navRequestContinue.Disabled = true;
    		        
	                navRequestPrevious.WillSucceed = false;
	                navRequestPrevious.Exception = "NB.2.1-5";
	                navRequestPrevious.ExceptionText = IntegrationImplementation.GetString("Please select a menu item to continue with {0}.", parentActivity.GetTitle());
	                navRequestPrevious.Disabled = true;
	            }
    	        
	            if (parentActivity.GetSequencingControlForwardOnly() === true){
    	        
	                this.LogSeq("[EPNR] " + parentActivity + " does not allow previous navigation. Previous button is disabled.", logParent);
    	            
	                navRequestPrevious.WillSucceed = false;
	                navRequestPrevious.Exception = "NB.2.1-5";
	                navRequestPrevious.ExceptionText = IntegrationImplementation.GetString("Please select a menu item to continue with {0}.", parentActivity.GetTitle());
	                navRequestPrevious.Disabled = true;
	            }
	        }
	        
	        //traverse previous to figure out if the logically previous activity is deliverable
	        var previousLeafActivity = null;
	        var flowCandidateParent;
	        var checkForwardOnly = true;
	        
	        for (var i=(reverseOrderedCurrentactivityIndex - 1); i >= 0; i--){
                
                flowCandidateParent = reverseOrderedActivityList[i].ParentActivity;
                
                if (flowCandidateParent !== null && flowCandidateParent.GetSequencingControlFlow() === false){
	            
	                this.LogSeq("[EPNR] Cannot flow through " + flowCandidateParent + " since it does not allow flow navigation. Previous button is disabled.", logParent);
    		        
	                navRequestPrevious.WillSucceed = false;
	                navRequestPrevious.Exception = "NB.2.1-5";
	                navRequestPrevious.ExceptionText = IntegrationImplementation.GetString("Please select a menu item to continue with {0}.", flowCandidateParent.GetTitle());
	                navRequestPrevious.Disabled = true;
	                break;
	            }
                
                tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(reverseOrderedActivityList[i]);
                
                if (tmpNavRequest.PreConditionSkipped === false){
                    
                    
                    //only check forward only if not skipped
                    
                    /*
                    If we reach a cluster that is forward only, then the actual p-code will flow into it in a forward direction creating
                    a horrendus mess of tree navigation code. In the "quick" look ahead mode, when this happens, just stop checking for forward
                    only and don't check for disabled and limit conditions (these aren't required to be checked for anyway - the reason to not check is 
                    that there is a good chance that the identified activity is not the activity that will actually be identified. in essence, here we 
                    are falling back to teh SCORM minimum requirement to just check for walking off the root of the activity tree). There's a small chance
                    that this will lead to the user being shown a "please select a menu item" prompt. That should be acceptable, but if it is not, then
                    the full lookahead sequencer will rectify the problem.
                    */
                    
                    if (checkForwardOnly === true){
                    
                        if (reverseOrderedActivityList[i].IsALeaf() === false && 
                            reverseOrderedActivityList[i].GetSequencingControlForwardOnly() === true){
                            
                            checkForwardOnly = false;
                            
                            this.LogSeq("[EPNR] Encountered a cluster that must be entered forward only. This traversal is beyond the capabilities of the 'quick' look ahead sequencer. If this navigation request results in an error message, then this course requires the full look ahead sequencer.", logParent);
            	            
                        }
                        else if (flowCandidateParent !== null && flowCandidateParent.GetSequencingControlForwardOnly() === true){
        	        
	                        this.LogSeq("[EPNR] Cannot flow backwards through " + flowCandidateParent + " since it only allows forward navigation. Previous button is disabled.", logParent);
            	            
	                        navRequestPrevious.WillSucceed = false;
	                        navRequestPrevious.Exception = "NB.2.1-5";
	                        navRequestPrevious.ExceptionText = IntegrationImplementation.GetString("Please select a menu item to continue with {0}.", flowCandidateParent.GetTitle());
	                        navRequestPrevious.Disabled = true;
	                        break;
	                    }
                        
                    }
                    
                    if (reverseOrderedActivityList[i].IsALeaf() === true){
                        previousLeafActivity = reverseOrderedActivityList[i];
                        break;           
                    }
                }
                
                
            }
            
            if (previousLeafActivity === null){
                //disabled, walked off front of activity tree
                this.LogSeq("[EPNR] There is either no previous activity or all previous activities are skipped, disable previous button.", logParent);
	            
	            navRequestPrevious.WillSucceed = false;
	            navRequestPrevious.Exception = "SB.2.1-3";
	            navRequestPrevious.ExceptionText = IntegrationImplementation.GetString("You have reached the beginning of the course.");
	            navRequestPrevious.Disabled = true;
            }
            else {
                
                if (checkForwardOnly === true){
                    tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(previousLeafActivity);
                    
                    if (tmpNavRequest.PreConditionDisabled === true){
                    
                        this.LogSeq("[EPNR] The logically previous activity," + previousLeafActivity + ", is disabled and cannot be delivered. Disable previous button.", logParent);
                        
                        navRequestPrevious.WillSucceed = false;
	                    navRequestPrevious.Exception = "SB.2.2-2";
	                    navRequestPrevious.ExceptionText = IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.", activity.GetTitle());
	                    navRequestPrevious.Disabled = true;
                    }
                    else if (tmpNavRequest.LimitConditionViolation === true){
                        
                        this.LogSeq("[EPNR] The logically previous activity," + previousLeafActivity + ", had reached its attempt limit and cannot be delivered. Disable previous button.", logParent);
                        
                        navRequestPrevious.WillSucceed = false;
	                    navRequestPrevious.Exception = "SB.2.2-2";
	                    navRequestPrevious.ExceptionText = IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.", activity.GetTitle());
	                    navRequestPrevious.Disabled = true;
                    
                    }   
                }
            }    
            
            //traverse forward to figure out if the logically next activity is deliverable
	        var nextLeafActivity = null;
	        for (var i=(currentActivityIndex + 1); i < orderedActivityList.length; i++){
                
                flowCandidateParent = orderedActivityList[i].ParentActivity;
                
                if (flowCandidateParent !== null && flowCandidateParent.GetSequencingControlFlow() === false){
	            
	                this.LogSeq("[EPNR] Cannot flow through " + flowCandidateParent + " since it does not allow flow navigation. Stopping here, continue button stays enabled even though request won't succeed. Continue results in user being prompted to select a child item.", logParent);
    		        
    		        tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(flowCandidateParent);
                    
                    if (tmpNavRequest.PreConditionSkipped === false){
                        nextLeafActivity = orderedActivityList[i];
                        break;
                    }   
    		        
	            }
                
                if (orderedActivityList[i].IsALeaf() === true){
                
                    tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(orderedActivityList[i]);
                    
                    if (tmpNavRequest.PreConditionSkipped === false){
                        nextLeafActivity = orderedActivityList[i];
                        break;
                    }                       
                }
            }
            
            //it's ok to continue off the end of the activity tree
            if (nextLeafActivity !== null){
            
                tmpNavRequest = Control.FindPossibleChoiceRequestForActivity(nextLeafActivity);
                
                if (tmpNavRequest.PreConditionDisabled === true){
                
                    this.LogSeq("[EPNR] The logically next activity," + nextLeafActivity + ", is disabled and cannot be delivered. Disable next button.", logParent);
                    
                    navRequestContinue.WillSucceed = false;
	                navRequestContinue.Exception = "SB.2.2-2";
	                navRequestContinue.ExceptionText = IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.", activity.GetTitle());
	                navRequestContinue.Disabled = true;
                }
                else if (tmpNavRequest.LimitConditionViolation === true){
                    
                    this.LogSeq("[EPNR] The logically next activity," + nextLeafActivity + ", had reached its attempt limit and cannot be delivered. Disable next button.", logParent);
                    
                    navRequestContinue.WillSucceed = false;
	                navRequestContinue.Exception = "SB.2.2-2";
	                navRequestContinue.ExceptionText = IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.", activity.GetTitle());
	                navRequestContinue.Disabled = true;
                
                }                   
            }
	        
	    }
	    
	    //go through and look at violations and mark each request as disabled/hidden as appropriate.
	    for (var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE; i < aryPossibleRequests.length; i++){
	    		    
	        if (aryPossibleRequests[i].PreConditionStopForwardTraversalViolation ||
                aryPossibleRequests[i].PreConditionDisabled ||
                aryPossibleRequests[i].LimitConditionViolation ||
                aryPossibleRequests[i].ControlChoiceViolation ||
                aryPossibleRequests[i].ForwardOnlyViolation ||
                aryPossibleRequests[i].NoDeliverablieActivityViolation){
                
                aryPossibleRequests[i].Disabled = true;
             }
             
             if (aryPossibleRequests[i].PreConditionHiddenFromChoice ||
                    aryPossibleRequests[i].IsVisibleViolation||
                    aryPossibleRequests[i].PreventActivationViolation ||
                    aryPossibleRequests[i].ConstrainedChoiceViolation ||
                    aryPossibleRequests[i].ChoiceExitViolation){
                
                aryPossibleRequests[i].Hidden = true;
             }
	    }
	    
	    
	    
	}
		
	else{
	    //clear out the initial state
	    for (id in aryPossibleRequests){
    	
		    if (aryPossibleRequests[id].WillAlwaysSucceed === true){
			    aryPossibleRequests[id].WillSucceed = true;
		    }
		    else if (aryPossibleRequests[id].WillNeverSucceed === true){
			    aryPossibleRequests[id].WillSucceed = false;
			    aryPossibleRequests[id].Exception = "NB.2.1-10";
			    aryPossibleRequests[id].ExceptionText = IntegrationImplementation.GetString("Your selection is not permitted. Please select 'Next' or 'Previous' to move through '{0}.'");
		    }
		    else {
			    aryPossibleRequests[id].WillSucceed = null;
		    }
    		
		    aryPossibleRequests[id].Hidden = false;		//reset the hidden state
		    aryPossibleRequests[id].Disabled = false;		//reset the disabled state
	    }
    	
	    this.LogSeq("[EPNR] 1. Run the navigation request process for each possible request", logParent);
	    for (id in aryPossibleRequests){
    	
		    if (aryPossibleRequests[id].WillSucceed === null){
    		
			    navigationRequestResult = this.NavigationRequestProcess(aryPossibleRequests[id].NavigationRequest, aryPossibleRequests[id].TargetActivityItemIdentifier, logParent, simpleLogParent);
    			
			    if (navigationRequestResult.NavigationRequest == NAVIGATION_REQUEST_NOT_VALID){
    				
				    this.LogSeq("[EPNR] 1.1. If the navigation request fails, set its WillSucceed to false", logParent);
				    aryPossibleRequests[id].WillSucceed = false;

                    //still set the target activity because down below we will evaulate unsuccessful requests further to see if they should be hidden or disabled (MR 07/09/09)
                    aryPossibleRequests[id].TargetActivity = this.GetActivityFromIdentifier(aryPossibleRequests[id].TargetActivityItemIdentifier, logParent);

				    aryPossibleRequests[id].Exception = navigationRequestResult.Exception;
				    aryPossibleRequests[id].ExceptionText = navigationRequestResult.ExceptionText;
			    }
			    else{
				    this.LogSeq("[EPNR] 1.2. If the navigation request succeeds, set its WillSucceed to true", logParent);
    				
				    aryPossibleRequests[id].WillSucceed = true;
				    aryPossibleRequests[id].TargetActivity = navigationRequestResult.TargetActivity;
				    aryPossibleRequests[id].SequencingRequest = navigationRequestResult.SequencingRequest;
    				
				    aryPossibleRequests[id].Exception = "";
				    aryPossibleRequests[id].ExceptionText = "";
    				
			    }
		    }
	    }

	    //if there is a current activity, set the termination request to Exit and run the Termination Request Process (note this won't catch 
	    //some error conditions with suspend all / exit all request (since those require differnet termination request), but i don't think these 
	    //are a concern at the moment, besides, we should probably handle them gracefully by just exiting the player)
    	
	    this.LogSeq("[EPNR] 2. If the current activity is active (we are going to need to terminate it for internally navigating sequencing requests (continue, previous, choice, exit)", logParent);
    	
	    var currentActivity = this.GetCurrentActivity();
    	
	    if (currentActivity !== null && currentActivity.IsActive() === true){
    		
		    this.LogSeq("[EPNR] 2.1 Run the Termination Request Process for Exit", logParent);
		    terminationRequestResult = this.TerminationRequestProcess(TERMINATION_REQUEST_EXIT, logParent, simpleLogParent);
    		
		    this.LogSeq("[EPNR] 2.2 if the termination request process return false", logParent);
		    if (terminationRequestResult.TerminationRequest == TERMINATION_REQUEST_NOT_VALID){
    			
			    this.LogSeq("[EPNR] 2.2.1 Set the possible navigations that result in sequencing to false", logParent);
    			
			    if (aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed){
				    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed = false;
				    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].Exception = terminationRequestResult.Exception;
				    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].ExceptionText = terminationRequestResult.ExceptionText;
			    }
    			
			    if (aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed){
				    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed = false;
				    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].Exception = terminationRequestResult.Exception;
				    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].ExceptionText = terminationRequestResult.ExceptionText;
			    }

			    if (aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed){
				    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed = false;
				    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].Exception = terminationRequestResult.Exception;
				    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].ExceptionText = terminationRequestResult.ExceptionText;
			    }

    			
			    for (id=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE; id < aryPossibleRequests.length; id++){
				    if (aryPossibleRequests[id].WillSucceed){
					    aryPossibleRequests[id].WillSucceed = false;
					    aryPossibleRequests[id].Exception = terminationRequestResult.Exception;
					    aryPossibleRequests[id].ExceptionText = terminatonRequestResult.ExceptionText;
				    }
				    else{
					    aryPossibleRequests[id].TerminationSequencingRequest = terminationRequestResult.SequencingRequest;
				    }
			    }
		    }
    		
		    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].TerminationSequencingRequest = terminationRequestResult.SequencingRequest;
		    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].TerminationSequencingRequest = terminationRequestResult.SequencingRequest;
		    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].TerminationSequencingRequest = terminationRequestResult.SequencingRequest;
    		
		    for (id=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE; id < aryPossibleRequests.length; id++){	
			    aryPossibleRequests[id].TerminationSequencingRequest = terminationRequestResult.SequencingRequest;
		    }
	    }
    	
	    /* 
	    -These processes affect the global activity state in a way that causes the evaluations below to fail, so don't run them before
    	
	    this.LogSeq("[EPNR] 2.2 There's a rare situation where the suspend all termination request can fail, so check for it", logParent);
	    if (aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL].WillSucceed){
    		
		    this.LogSeq("[EPNR] 2.2.1 Run the Termination Request Process For Suspend All", logParent);
		    terminationRequestResult = this.TerminationRequestProcess(TERMINATION_REQUEST_SUSPEND_ALL, logParent);
    		
		    if (terminationRequestResult.TerminationRequest == TERMINATION_REQUEST_NOT_VALID){
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL].WillSucceed = false;
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL].Exception = terminationRequestResult.Exception;
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL].ExceptionText = terminationRequestResult.ExceptionText;
		    }
		    else{
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL].TerminationSequencingRequest = terminationRequestResult.SequencingRequest;
		    }
	    }
	    this.LogSeq("[EPNR] 2.3 If there's an exit all request, the termination request process performs sequencing actions that are relevant later on, so do those.", logParent);
	    if (aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].WillSucceed){
    		
		    this.LogSeq("[EPNR] 2.2.1 Run the Termination Request Process For Suspend All", logParent);
		    terminationRequestResult = this.TerminationRequestProcess(POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL, logParent);
    		
		    if (terminationRequestResult.TerminationRequest == TERMINATION_REQUEST_NOT_VALID){
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].WillSucceed = false;
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].Exception = terminationRequestResult.Exception;
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].ExceptionText = terminationRequestResult.ExceptionText;
		    }
		    else{
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].TerminationSequencingRequest = terminationRequestResult.SequencingRequest;
		    }
	    }
	    */
    	
    	
    	
	    //Explictly check for disabled and limit condition rules on all choice requests. If an activity is disabled or has its attempt limit exceeded, all of its children
	    //are not valid for choice. Normally this is done in the DeliveryRequestProcess, but we need to do it earlier to account for the situation where
	    //a termination request changes the sequencing request. For example, if after selecting activity A, the termination request returns a retry, then
	    //we still want to check activity A's disabled status so we can grey it out. i.e., even if making a request will succeed, that doesn't mean that 
	    //we should allow the request in the first place.
    	
	    /*
	    var activitiesThatWillSucceed = new Array();
	    for (id in aryPossibleRequests){
		    if (id >= POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE && 
			    aryPossibleRequests[id].WillSucceed === true && 
			    aryPossibleRequests[id].WillAlwaysSucceed === false && 
			    aryPossibleRequests[id].WillNeverSucceed === false){
    				
			    activitiesThatWillSucceed[activitiesThatWillSucceed.length] = aryPossibleRequests[id].TargetActivity;
		    }
	    }
	    var activitySet = this.FindDistinctAncestorsOfActivitySet(activitiesThatWillSucceed);
    	
	    //alert("a");
	    // Run CheckActivityProcess on each of these ancestors and set decendents to disabled if activity ancestor is disabled
	    for (var i=0; i<activitySet.length; i++) {
		    var activityIsNotAllowed = this.CheckActivityProcess(activitySet[i], logParent);
		    if (activityIsNotAllowed === true){
			    this.LogSeq("[EPNR] 2.5.1.1. Activity is disabled, mark it and its children as disabled", logParent);
    			
			    var navRequestForActivity = null;
			    for (var j=0; j<aryPossibleRequests.length; j++){
				    if (aryPossibleRequests[j].TargetActivityItemIdentifier == activitySet[i].GetItemIdentifier()) {
					    navRequestForActivity = aryPossibleRequests[j];
					    break;
				    }
			    }
    			
			    if (navRequestForActivity !== null) {
				    if (this.LookAhead == true)
					    //alert("disabling " + activitySet[i].StringIdentifier + " and children");
				    navRequestForActivity.WillSucceed = false;
				    navRequestForActivity.Disabled = true;
    				
				    this.SetAllDescendentsToDisabled(aryPossibleRequests, activitySet[i]);
			    }
		    }
	    }
	    */
	    this.LogSeq("[EPNR] 2.5. For each possible navigation request that hasn't already been excluded", logParent);
	    for (id in aryPossibleRequests){
		    if (id >= POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE && 
			    aryPossibleRequests[id].WillSucceed === true && 
			    aryPossibleRequests[id].WillAlwaysSucceed === false && 
			    aryPossibleRequests[id].WillNeverSucceed === false){
    				
				    this.LogSeq("[EPNR] 2.5.1. Check for disabled and limit condition violations", logParent);
				    var activityIsNotAllowed = this.CheckActivityProcess(aryPossibleRequests[id].TargetActivity, logParent, simpleLogParent);
    				
				    if (activityIsNotAllowed === true){
					    this.LogSeq("[EPNR] 2.5.1.1. Activity is disabled, mark it and its children as disabled", logParent);
    					
					    aryPossibleRequests[id].WillSucceed = false;
					    aryPossibleRequests[id].Disabled = true;
    					
					    this.SetAllDescendentsToDisabled(aryPossibleRequests, aryPossibleRequests[id].TargetActivity);
				    }
		    }
	    }
    	
    	
    	
	    //TODO - (parameter for this functionality) if the termination request process returns a sequencing request, 
	    //disable all possible navigation requests that aren't equivilant to the sequencing request (only allow navigations 
	    //that are consistent with what the post conditions will allow)
    	
	    this.LogSeq("[EPNR] 3. For each possible navigation request that hasn't already been excluded", logParent);
	    var terminationSequencingRequestResult = null;
	    for (id in aryPossibleRequests){
    		
		    //Removed the test 'aryPossibleRequests[id].WillSucceed === true' so that activities that are disabled can also be marked as 
		    //hidden and hidden from view 				
		    if (aryPossibleRequests[id].WillAlwaysSucceed === false && aryPossibleRequests[id].WillNeverSucceed === false){
    			
			    //if there is a sequencing request returned from the termination request process, then that is the sequencing request that needs to be
			    //evaluated to determine if the original sequencing request will succeed
    			
			    this.LogSeq("[EPNR] 3.1 If there is a sequencing request returned by the termination request process", logParent);
    			
			    if (aryPossibleRequests[id].TerminationSequencingRequest !== null){
    				
				    this.LogSeq("[EPNR] 3.1.1 Run the sequencing request process for that sequencing request returned by the termination request process", logParent);
    				
				    //in the case where the termination request returns a sequencing result, all the evaluations will be the same so we can just use the cached result
				    if (terminationSequencingRequestResult === null){
					    sequencingRequestResult = this.SequencingRequestProcess(aryPossibleRequests[id].TerminationSequencingRequest, null, logParent, simpleLogParent);
				    }
				    else{
					    sequencingRequestResult = terminationSequencingRequestResult;
				    }
    				
				    //in the case where we've altered the sequencing request, the preconditions for hidden from choice may not be evaluated 
				    //on all requests, make sure we still evaluate these
				    if (id >= POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE){
					    this.LogSeq("[EPNR] 3.1.2 Make sure the activity is not hidden", logParent);
					    var sequencingRulesCheckResult = this.SequencingRulesCheckProcess(aryPossibleRequests[id].TargetActivity, RULE_SET_HIDE_FROM_CHOICE, logParent, simpleLogParent);
					    if (sequencingRulesCheckResult !== null){
						    aryPossibleRequests[id].Exception = "SB.2.9-3";
						    aryPossibleRequests[id].ExceptionText = "The activity " + aryPossibleRequests[id].TargetActivity.GetTitle() + " should be hidden and is not a valid selection";
						    aryPossibleRequests[id].Hidden = true;
					    }
				    }
    				
			    }
			    else if (aryPossibleRequests[id].WillSucceed === true){
				    this.LogSeq("[EPNR] 3.1.2. Else", logParent);
				    this.LogSeq("[EPNR] 3.1.3. Run the sequencing request process for that navigation request", logParent);
				    sequencingRequestResult = this.SequencingRequestProcess(aryPossibleRequests[id].SequencingRequest, aryPossibleRequests[id].TargetActivity, logParent, simpleLogParent);
			    }
    			
			    if (aryPossibleRequests[id].WillSucceed === true){
    			
			        this.LogSeq("[EPNR] 3.2. If the Sequencing Request Process returns an exception", logParent);
			        if (sequencingRequestResult.Exception !== null){
        				
				        this.LogSeq("[EPNR] 3.2.1 Set WillSucceed to false", logParent);
				        aryPossibleRequests[id].WillSucceed = false;
        				
				        aryPossibleRequests[id].Exception = sequencingRequestResult.Exception;
				        aryPossibleRequests[id].ExceptionText = sequencingRequestResult.ExceptionText;
				        aryPossibleRequests[id].Hidden = sequencingRequestResult.Hidden;
			        }
			        else if (sequencingRequestResult.DeliveryRequest !== null){
				        //TODO - performance optimizatio - can we omit this call since we are evaluating it earler? probably.
				        this.LogSeq("[EPNR] 3.2.2 Run the Delivery Request Process", logParent);
				        deliveryRequestResult = this.DeliveryRequestProcess(sequencingRequestResult.DeliveryRequest, logParent, simpleLogParent);
        				
				        this.LogSeq("[EPNR] 3.2.2 Set will succed to the results of the delivery request process (" + deliveryRequestResult.Valid + ")", logParent);
				        aryPossibleRequests[id].WillSucceed = deliveryRequestResult.Valid;
        	
			        }
			     }
		    }
	    }
    	
    	
	    //note, the API call to nav request valid technically is supposed to use a differnet set of rules (the actual look ahead evaluation) than the UI button
	    //might need to change the call to IsContinueRequestValid to look at something else
    	
	    //certain sequencing exceptions require that the TOC navigation item be hidden by the new 3rd Edition GUI requirements
	    this.LogSeq("[EPNR] 4. Set any requests that are invalid due to Control Choice, Prevent Activation or Constrained Choice violations to hidden", logParent);
	    var exceptionNumber;
	    for (id in aryPossibleRequests){
		    if (id >= POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE){
			    if (aryPossibleRequests[id].WillSucceed === false){
				    exceptionNumber = aryPossibleRequests[id].Exception;
				    if (exceptionNumber == CONTROL_CHOICE_EXIT_ERROR_NAV ||
					    exceptionNumber == CONTROL_CHOICE_EXIT_ERROR_CHOICE ||
					    exceptionNumber == PREVENT_ACTIVATION_ERROR ||
					    exceptionNumber == CONSTRAINED_CHOICE_ERROR){
    					
					    this.LogSeq("[EPNR] 4.1 Hiding request " + id + ". Exception=" + exceptionNumber, logParent);
    					
					    aryPossibleRequests[id].Hidden = true;
				    }
			    }
		    }
	    }
    	
        //override the continue request to be in line with the new 3rd Edition GUI requirements that require it to always be enabled for a cluster with flow=true
	    var parentActivity = this.Activities.GetParentActivity(currentActivity);
	    if (parentActivity != null){
		    if (parentActivity.GetSequencingControlFlow() === true){
			    this.LogSeq("[EPNR] 5.1 Overriding continue status based on 3rd Edition GUI requirements, parent's flow = true, continue is enabled", logParent);
    			
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed = true;
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].Exception = "";
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].ExceptionText = "";
		    }
		    else{
			    this.LogSeq("[EPNR] 5.2 Overriding continue status based on 3rd Edition GUI requirements, parent's flow = false, continue is disabled", logParent);
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed = false;
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].Exception = "SB.2.2-1";
			    aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].ExceptionText = "Parent activity does not allow flow traversal";
		    }
	    }
	}

	this.LogSeqReturn("", logParent)
	return aryPossibleRequests;

}

function Sequencer_SetAllDescendentsToDisabled(aryPossibleRequests, parentActivity){
	
	for (var i=0; i < parentActivity.ChildActivities.length; i++){
	
		var possibleNavRequest = Control.FindPossibleChoiceRequestForActivity(parentActivity.ChildActivities[i]);
		
		possibleNavRequest.WillSucceed = false;
		possibleNavRequest.Disabled = true;
		
		this.SetAllDescendentsToDisabled(aryPossibleRequests, parentActivity.ChildActivities[i]);
	}
}

function Sequencer_SetAllDescendentsToNotSucceed(aryPossibleRequests, parentActivity, reason, exception, exceptionText, logParent){
	
	for (var i=0; i < parentActivity.ChildActivities.length; i++){
	
		var possibleNavRequest = Control.FindPossibleChoiceRequestForActivity(parentActivity.ChildActivities[i]);
		
		possibleNavRequest.WillSucceed = false;
		possibleNavRequest.Exception = exception;
		possibleNavRequest.ExceptionText = exceptionText;

		switch (reason){
		    case "PreventActivationViolation":
		        possibleNavRequest.PreventActivationViolation = true;
		        this.LogSeq("[EPNR] " + parentActivity.ChildActivities[i] + " is a descendent and will be hidden. ", logParent);
		    break;
		    
		    case "LimitConditionViolation":
		        possibleNavRequest.LimitConditionViolation = true;
		        this.LogSeq("[EPNR] " + parentActivity.ChildActivities[i] + " is a descendent and will be disabled. ", logParent);
		    break;
		    
		    case "PreConditionHiddenFromChoice":
		        possibleNavRequest.PreConditionHiddenFromChoice = true;
		        this.LogSeq("[EPNR] " + parentActivity.ChildActivities[i] + " is a descendent and will be hidden. ", logParent);
		    break;
		    
		    case "PreConditionDisabled":
		        possibleNavRequest.PreConditionDisabled = true;
		        this.LogSeq("[EPNR] " + parentActivity.ChildActivities[i] + " is a descendent and will be disabled. ", logParent);
		    break;
		    
		    case "ForwardOnlyViolation":
		        possibleNavRequest.ForwardOnlyViolation = true;
		        this.LogSeq("[EPNR] " + parentActivity.ChildActivities[i] + " is a descendent and will be disabled. ", logParent);
		    break;
		    
		    case "PreConditionStopForwardTraversalViolation":
		        possibleNavRequest.PreConditionStopForwardTraversalViolation = true;
		        this.LogSeq("[EPNR] " + parentActivity.ChildActivities[i] + " is a descendent and will be disabled. ", logParent);
		    break;
		}
		
		this.SetAllDescendentsToNotSucceed(aryPossibleRequests, parentActivity.ChildActivities[i], reason, exception, exceptionText, logParent);
	}
}

function Sequencer_SetAllDescendentsToSkipped(aryPossibleRequests, parentActivity, activeActivities, logParent){
    
    /*
    if the activity is active, then it's children shouldn't be marked as skipped because 
    traversal will never go through the ancestor. For example, if you have
    
    Activity A
    ---Activity B
    ---Activity C
    ---Activity D
    
    if A is skipped, but you are on B, then navigating to C or D is allowed and unaffected by A being skipped
    */
    
    this.LogSeq("[EPNR] Setting " + parentActivity + " and all its descendents to skipped. IsActive=" + activeActivities[parentActivity.GetItemIdentifier()], logParent);			
    if (activeActivities[parentActivity.GetItemIdentifier()] === true){
        return;
    }
    
    for (var i=0; i < parentActivity.ChildActivities.length; i++){
	
		var possibleNavRequest = Control.FindPossibleChoiceRequestForActivity(parentActivity.ChildActivities[i]);
		
		possibleNavRequest.PreConditionSkipped = true;

		this.SetAllDescendentsToSkipped(aryPossibleRequests, parentActivity.ChildActivities[i], activeActivities, logParent);
	}
}

function Sequencer_InitializePossibleNavigationRequestAbsolutes(aryPossibleRequests, activityTree, activityList){
	
	//go through the activity tree and populate the WillAlwaysSucceed and WillNeverSucceed values for all Choice requests
	//this will optimize the execution of the EvaluatePossibleNavigationRequests function which is a performance bottleneck in large courses
	//note, this optimization will only help courses that don't use complex choice sequencing rules
	
	//if sequencing control choice = false, set will never succeed to true for all children
	
	//There are a limited set of rules that can cause an activity to not be available for choice.
	//If none of these rules are available, then a choice request for an activity will always succeed.
	//Check for the presense of these rules by recursing down the tree, if an activity has no pertenent rules, 
	//then it will always succeed.
	
	//The relevant rules that can prevent choice from succeeding are:
		//Hidden From Choice
		//Stop Forward Traversal
		//Forward Only
		//Prevent Activitation
		//Constrain Choice
		//Control Choice Exit
	
	/*
	Control Choice Exit needs to be handled seperately, the other rules are top down, they prevent a child from being selected
	Control Choice Exit operates bottom up, it prevents you from leaving an activity. Thus, if any activity has Control Choice Exit, 
	Will Always Succeed can never be true, since when that activity is active, all other activities will be disabled.
	*/
	
	var logParent = this.LogSeqAudit("Initializing Possible Navigation Request Absolutes");

	this.CheckForRelevantSequencingRules(activityTree, false);
	
	var activity;
	var possibleNavRequest;
	var controlChoiceExitIsUsed = false;
	
	for (var identifier in activityList){
		
		activity = activityList[identifier];
		var logActivity = this.LogSeqAudit(activity.StringIdentifier, logParent);
		
		if (activity.GetSequencingControlChoice() === false){
			
			this.LogSeqAudit("GetSequencingControlChoice = false.", logActivity);
			var logChildren = this.LogSeqAudit("Setting WillNeverSucceed = true on all child activities. (Count = " + activity.ChildActivities.length + ")", logActivity);
			
			for (var i=0; i < activity.ChildActivities.length; i++){
				
				possibleNavRequest = Control.FindPossibleChoiceRequestForActivity(activity.ChildActivities[i]);
				possibleNavRequest.WillNeverSucceed = true;
				this.LogSeqAudit(activity.ChildActivities[i].StringIdentifier, logChildren);
			}
			
		}
		
		controlChoiceExitIsUsed = controlChoiceExitIsUsed || (activity.GetSequencingControlChoiceExit() === true);
		this.LogSeqAudit("controlChoiceExitIsUsed = " + controlChoiceExitIsUsed, logActivity);
		
		possibleNavRequest = Control.FindPossibleChoiceRequestForActivity(activity);
		
		//if flow is disabled, you can't choose a parent
		if (activity.IsDeliverable() === false && activity.GetSequencingControlFlow() === false){
			this.LogSeqAudit("activity.IsDeliverable = false and activity.GetSequencingControlFlow = false.  Setting possibleNavRequest.WillNeverSucceed = true.", logActivity);
			possibleNavRequest.WillNeverSucceed = true;
		}
		
		//if you can't flow to any children, a choice request will never succeed
		if (activity.HasChildActivitiesDeliverableViaFlow === false){
			this.LogSeqAudit("activity.HasChildActivitiesDeliverableViaFlow = false.  Setting possibleNavRequest.WillNeverSucceed = true.", logActivity);
			possibleNavRequest.WillNeverSucceed = true;
		}
		
		if (activity.HasSeqRulesRelevantToChoice === false && possibleNavRequest.WillNeverSucceed === false){
			this.LogSeqAudit("activity.HasSeqRulesRelevantToChoice = false and possibleNavRequest.WillNeverSucceed = false.  Setting possibleNavRequest.WillAlwaysSucceed = true.", logActivity);
			possibleNavRequest.WillAlwaysSucceed = true;
		}
		else{
			this.LogSeqAudit("Either activity.HasSeqRulesRelevantToChoice = false or possibleNavRequest.WillNeverSucceed = true.  Setting possibleNavRequest.WillAlwaysSucceed = false.", logActivity);
			possibleNavRequest.WillAlwaysSucceed = false;
		}
		this.LogSeqAudit("WillAlwaysSucceed = " + possibleNavRequest.WillAlwaysSucceed, logActivity);
		this.LogSeqAudit("WillNeverSucceed = " + possibleNavRequest.WillNeverSucceed, logActivity);
	}
	
	if (controlChoiceExitIsUsed === true){
		this.LogSeqAudit("controlChoiceExitIsUsed = true.  Setting WillAlwaysSucceed = false for all possible nav. requests.", logParent);
		for (var id in aryPossibleRequests){
			aryPossibleRequests[id].WillAlwaysSucceed = false;
		}
	}
}

function Sequencer_CheckForRelevantSequencingRules(activity, parentHasRelevantRules){
	
	if (parentHasRelevantRules === true){
		activity.HasSeqRulesRelevantToChoice = true;
	}
	else {
		if (this.DoesThisActivityHaveSequencingRulesRelevantToChoice(activity)){
			activity.HasSeqRulesRelevantToChoice = true;
		}
		else{
			activity.HasSeqRulesRelevantToChoice = false;
		}
	}
	
	var aChildHasChildActivitiesDeliverableViaFlow = false;
	
	for (var i=0; i < activity.ChildActivities.length; i++){
		
		this.CheckForRelevantSequencingRules(activity.ChildActivities[i], activity.HasSeqRulesRelevantToChoice);
		
		aChildHasChildActivitiesDeliverableViaFlow = (aChildHasChildActivitiesDeliverableViaFlow || activity.ChildActivities[i].HasChildActivitiesDeliverableViaFlow);
	}
	
	activity.HasChildActivitiesDeliverableViaFlow = (activity.IsDeliverable() || 
													 (activity.GetSequencingControlFlow() && aChildHasChildActivitiesDeliverableViaFlow)
													);
}

function Sequencer_DoesThisActivityHaveSequencingRulesRelevantToChoice(activity){
	
	//Forward Only
	if (activity.GetSequencingControlForwardOnly() === true){
		return true;
	}
	
	//Prevent Activitation
	if (activity.GetPreventActivation() === true){
		return true;
	}
	
	//Constrain Choice
	if (activity.GetConstrainedChoice() === true){
		return true;
	}
	
	//Control Choice Exit
	if (activity.GetSequencingControlChoiceExit() === false){
		return true;
	}

	//Hidden From Choice
	//Stop Forward Traversal
	
	var preConditionRules = activity.GetPreConditionRules();
	
	for (var i=0; i < preConditionRules.length; i++){
		
		if (
			preConditionRules[i].Action == SEQUENCING_RULE_ACTION_DISABLED ||
			preConditionRules[i].Action == SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE ||
			preConditionRules[i].Action == SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL){
			return true;
		}
		
	}

	return false;
}


function Sequencer_GetArrayOfDescendents(parentActivity){
    
    var descendents = new Array();
    
    descendents[descendents.length] = parentActivity;
    
    var children = parentActivity.GetChildren();
    
    for (var i=0; i<children.length; i++){
        var childDescendents = this.GetArrayOfDescendents(children[i]);
        descendents = descendents.concat(childDescendents);
    }
    
    return descendents;
}

function Sequencer_IsActivity1AParentOfActivity2(activity1, activity2){
    
    var blnReturn = false;
 
    var parent = null;
    
    if (activity2.ParentActivity !== null){
        parent = activity2.ParentActivity;
    }
    
    while (parent !== null){
    
        if(parent.GetItemIdentifier() == activity1.GetItemIdentifier()){
            blnReturn = true;
            break;
        }
        
        parent = parent.ParentActivity;
    }
    
    return blnReturn;
    
}
