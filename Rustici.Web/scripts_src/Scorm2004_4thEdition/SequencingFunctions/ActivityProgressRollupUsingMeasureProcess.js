//****************************************************************************************************************

// Activity Progress Rollup Using Measure Process [RB.1.3 a] 

//	For an activity; may change the Attempt Information for the activity

//	Reference: 
		//Attempt Completion Status TM.1.2.2; 
		//Attempt Progress Status TM.1.2.2; 
		//Attempt Completion Amount Status TM.1.1;
		//Attempt Completion Amount TM.1.1;
		//adlcp:completedbyMeaseure SCORM CAM;
		//adlcp:minProgressMeasure SCORM CAM 



function Sequencer_ActivityProgressRollupUsingMeasureProcess(activity, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Activity Progress Rollup Using Measure Process [RB.1.3 a](" + activity + ")", callingLog);
	
	this.LogSeq("[RB.1.3 a]1. If activity is not tracked, return", logParent);
	if (activity.IsTracked() === false){
	    this.LogSeqReturn("", logParent);
	    return;
	}
	
	this.LogSeq("[RB.1.3 a]2. Set Attempt Progress Status to False", logParent);
    activity.SetAttemptProgressStatus(false);

    this.LogSeq("[RB.1.3 a]3.  Set Attempt Completion Status to False", logParent);
    activity.SetAttemptCompletionStatus(false);
    
    this.LogSeq("[RB.1.3 a]4. If adlcp:completedbyMeasure for the target objective is True Then (If the completion is determined by measure, test the rolled-up progress against the defined threshold.)", logParent);   
    if (activity.GetCompletedByMeasure() === true){

        this.LogSeq("[RB.1.3 a]4.1. If the Attempt Completion Amount Status is False Then (No progress amount known, so the status is unreliable.)", logParent);
        if (activity.GetAttemptCompletionAmountStatus() === false){
        
            this.LogSeq("[RB.1.3 a]4.1.1. Set the Attempt Completion Status to False", logParent);
            activity.SetAttemptCompletionStatus(false);
            
            this.LogSeqSimple("\"" + activity + "\" is completed by measure, but does not have a known progress measure so its completion status will be unknown.", simpleLogParent);
        
        } else {  
            this.LogSeq("[RB.1.3 a]4.2. Else", logParent);
            
            this.LogSeq("[RB.1.3 a]4.2.1. If the Attempt Completion Amount is greater than or equal (>=) to the adlcp:minProgressMeasure  Then", logParent);
            var completionAmount = activity.GetAttemptCompletionAmount();
            var minProgressMeasure = activity.GetMinProgressMeasure();
            if (completionAmount >= minProgressMeasure){  
            
                this.LogSeq("[RB.1.3 a]4.2.1.1. Set the Attempt Progress Status True  ", logParent);
                activity.SetAttemptProgressStatus(true);

                this.LogSeq("[RB.1.3 a]4.2.1.2. Set the Attempt Completion Status to True  ", logParent);
                activity.SetAttemptCompletionStatus(true);
            
                this.LogSeqSimple("\"" + activity + "\" is completed by measure, and its progress measure (" + completionAmount + ") is greater than the minimum progress measure (" + minProgressMeasure + ") so its completion status will be completed.", simpleLogParent);
            
            } else {
                this.LogSeq("[RB.1.3 a]4.2.2. Else ", logParent);
                
                this.LogSeq("[RB.1.3 a]4.2.2.1. Set the Attempt Progress Status True", logParent);
                activity.SetAttemptProgressStatus(true);
                
                this.LogSeq("[RB.1.3 a]4.2.2.2. Set the Attempt Completion Status to False", logParent);
                activity.SetAttemptCompletionStatus(false);
                
                this.LogSeqSimple("\"" + activity + "\" is completed by measure, and its progress measure (" + completionAmount + ") is less than the minimum progress measure (" + minProgressMeasure + ") so its completion status will be incomplete.", simpleLogParent);
                
            } //End If (not in p-code)    
        }//End If  

    
    } else {  
        this.LogSeq("[RB.1.3 a]5. Else", logParent);
        
        this.LogSeq("[RB.1.3 a]5.1. Set the Attempt Progress Status False (Incomplete information, do not evaluate completion status.) ", logParent);
        activity.SetAttemptProgressStatus(false);
        
    }//End If  
    
	this.LogSeq("[RB.1.3 a]6. Exit Activity Progress Rollup Using Measure Process", logParent);
	
	this.LogSeqReturn("", logParent);
	return;
	
}

