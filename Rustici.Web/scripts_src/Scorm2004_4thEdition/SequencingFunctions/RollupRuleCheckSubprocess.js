//****************************************************************************************************************

//Rollup Rule Check Subprocess [RB.1.4] 

//	For an activity and a Rollup Action; returns True if the action applies

//	Reference: 
		//Check Child for Rollup Subprocess RB.1.4.2; 
		//Evaluate Rollup Conditions Subprocess RB.1.4.1; 
		//Rollup Action SM.5; 
		//Rollup Child Activity Set SM.5; 
		//Rollup Minimum Count SM.5; 
		//Rollup Minimum Percent SM.5; 
		//Rollup Rule Description SM.5; 
		//Tracked SM.11; 
		//Tracking Model TM 

function Sequencer_RollupRuleCheckSubprocess(activity, ruleSet, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Rollup Rule Check Subprocess [RB.1.4](" + activity + ", " + ruleSet + ")", callingLog);
	
	//var contributingChldren;
	var children;
	var checkChildResult;
	var evaluateConditionsResult;
	var statusChange;
	var numTrue = 0;
	var numFalse = 0;
	var numUnknown = 0;
			
	this.LogSeq("[RB.1.4]1. If the activity includes Rollup Rules with the specified Rollup Action Then  (Make sure the activity has rules to evaluate)", logParent);
	
	var applicableRules = Sequencer_GetApplicableSetofRollupRules (activity, ruleSet);
	if (applicableRules.length > 0){

		this.LogSeq("[RB.1.4]1.1. Initialize rules list by selecting the set of Rollup Rules for the activity that have the specified Rollup Actions, maintaining original rule ordering", logParent);
		//done above
		
		simpleLogParent = this.LogSeqSimpleAudit("Evaluating rollup rules for " + ruleSet + " on \"" + activity + "\".", simpleLogParent);
		
		children = activity.GetAvailableChildren();
		
		this.LogSeq("[RB.1.4]1.2. For each rule in the rules list", logParent);
		for (var i=0; i < applicableRules.length; i++){
			
			this.LogSeq("[RB.1.4]1.2.0. Rule Description: " + applicableRules[i].toString() , logParent);
			
			var simpleLogParentForRule = this.LogSeqSimpleAudit("Evaluating Rule #" + (i+1) + ": \"" + applicableRules[i].toString() + "\".", simpleLogParent);
			
			this.LogSeq("[RB.1.4]1.2.1. Initialize contributing children bag as an empty collection", logParent);
			contributingChldren = new Array();
			
			numTrue = 0;
			numFalse = 0;
			numUnknown = 0;
			
			this.LogSeq("[RB.1.4]1.2.2. For each child of the activity", logParent);
			for (var j=0; j < children.length; j++){
			
				this.LogSeq("[RB.1.4]1.2.2.1. If Tracked for the child is True Then", logParent);
				if (children[j].IsTracked() === true){
					
					this.LogSeq("[RB.1.4]1.2.2.2.1. Apply Check Child for Rollup Subprocess to the child and the Rollup Action (Make sure this child contributes to the status of its parent)", logParent);
					checkChildResult = this.CheckChildForRollupSubprocess(children[j], applicableRules[i].Action, logParent, simpleLogParentForRule);
					
					this.LogSeq("[RB.1.4]1.2.2.2.2. If Check Child for Rollup Subprocess returned True Then", logParent);
					if (checkChildResult === true){
						
						this.LogSeq("[RB.1.4]1.2.2.2.2.1. Apply the Evaluate Rollup Conditions Subprocess to the child and the Rollup Conditions for the rule (Evaluate the rollup conditions on the child activity)", logParent);
						evaluateConditionsResult = this.EvaluateRollupConditionsSubprocess(children[j], applicableRules[i], logParent, simpleLogParentForRule);
						
						this.LogSeq("[RB.1.4]1.2.2.2.2.2. If Evaluate Rollup Conditions Subprocess returned Unknown Then (Account for a possible 'unknown' condition evaluation)", logParent);
						if (evaluateConditionsResult == RESULT_UNKNOWN){
							
							this.LogSeq("[RB.1.4]1.2.2.2.2.2.1. Add an Unknown value to the contributing children bag", logParent);
							numUnknown++;
							
							if (applicableRules[i].ChildActivitySet == CHILD_ACTIVITY_SET_ALL ||
								applicableRules[i].ChildActivitySet == CHILD_ACTIVITY_SET_NONE){
								//break the loop, rule cannot be satisfied
								j = children.length;
							}
						}
						else{	
							this.LogSeq("[RB.1.4]1.2.2.2.2.3. Else", logParent);
							this.LogSeq("[RB.1.4]1.2.2.2.2.4. If Evaluate Rollup Conditions Subprocess returned True Then", logParent);
							if (evaluateConditionsResult === true){
								
								this.LogSeq("[RB.1.4]1.2.2.2.2.4.1. Add a True value to the contributing children bag", logParent);
								numTrue++;
								
								if (applicableRules[i].ChildActivitySet == CHILD_ACTIVITY_SET_ANY ||
									applicableRules[i].ChildActivitySet == CHILD_ACTIVITY_SET_NONE){
									//break the loop, remaining children won't matter
									j = children.length;
								}
							}
							else{
								this.LogSeq("[RB.1.4]1.2.2.2.2.5. Else", logParent);
								this.LogSeq("[RB.1.4]1.2.2.2.2.5.1. Add a False value to the contributing children bag", logParent);
								numFalse++;
								
								if (applicableRules[i].ChildActivitySet == CHILD_ACTIVITY_SET_ALL){
									//break the loop, remaining children won't matter
									j = children.length;
								}
							}
						}
					}
				}
				else{
				    this.LogSeqSimple("\"" + children[j] + "\" is not tracked so it is not included in the rollup.", simpleLogParentForRule);
				}
			}
			this.LogSeq("[RB.1.4]1.2.3. Initialize status change to False (Determine if the appropriate children contributed to rollup; if they did, the status of the activity should be changed.)", logParent);
			statusChange = false;
			
			this.LogSeq("[RB.1.4]1.2.4. Case: the contributing children bag is empty", logParent);
			if (children.length == 0){
			    this.LogSeq("[RB.1.4]1.2.4.1. Break  (No action; do not change status unless some child contributed to rollup)", logParent);

			} else {
			
			    switch (applicableRules[i].ChildActivitySet) {
    			
				    case CHILD_ACTIVITY_SET_ALL:
					    this.LogSeq("[RB.1.4]1.2.5. Case: the Rollup Child Activity Set is All", logParent);		
					    this.LogSeq("[RB.1.4]1.2.5.1. If the contributing children bag does not contain a value of False Or Unknown Then", logParent);
					    if (numFalse === 0 && numUnknown === 0){
    						
						    this.LogSeq("[RB.1.4]1.2.5.1.1. Set status change to True", logParent);
						    statusChange = true;
					    }

				    break;
    				
				    case CHILD_ACTIVITY_SET_ANY:
					    this.LogSeq("[RB.1.4]1.2.6. Case: the Rollup Child Activity Set is Any", logParent);
					    this.LogSeq("[RB.1.4]1.2.6.1. If the contributing children bag contains a value of True Then", logParent);
					    if (numTrue > 0){
						    this.LogSeq("[RB.1.4]1.2.6.1.1. Set status change to True", logParent);
						    statusChange = true;
					    }
    					
				    break;
    				
				    case CHILD_ACTIVITY_SET_NONE:
					    this.LogSeq("[RB.1.4]1.2.7. Case: the Rollup Child Activity Set is None", logParent);
					    this.LogSeq("[RB.1.4]1.2.7.1. If the contributing children bag does not contain a value of True Or Unknown Then", logParent);
    					
					    if (numTrue === 0 && numUnknown === 0){
    						
						    this.LogSeq("[RB.1.4]1.2.7.1.1. Set status change to True", logParent);
						    statusChange = true;
					    }
    					
				    break;
    				
    				
				    case CHILD_ACTIVITY_SET_AT_LEAST_COUNT:
					    this.LogSeq("[RB.1.4]1.2.8. Case: the Rollup Child Activity Set is At Least Count", logParent);
					    this.LogSeq("[RB.1.4]1.2.8.1. If the count of True values contained in the contributing children bag equals or exceeds the Rollup Minimum Count of the rule Then", logParent);
					    if (numTrue >= applicableRules[i].MinimumCount){
    					
						    this.LogSeq("[RB.1.4]1.2.8.1.1. Set status change to True", logParent);
						    statusChange = true;
					    }
				    break;
    				
    				
				    case CHILD_ACTIVITY_SET_AT_LEAST_PERCENT:
					    this.LogSeq("[RB.1.4]1.2.9. Case: the Rollup Child Activity Set is At Least Percent", logParent);
					    this.LogSeq("[RB.1.4]1.2.9.1. If the percentage (normalized between 0..1, inclusive) of True values contained in the contributing children bag equals or exceeds the Rollup Minimum Percent of the rule Then", logParent);
					    var percent = (numTrue / (numTrue + numFalse + numUnknown));
					    if (percent >= applicableRules[i].MinimumPercent){
    						
						    this.LogSeq("[RB.1.4]1.2.9.1.1. Set status change to True", logParent);
						    statusChange = true;
					    }
    					
				    break;
    				
				    default:
					    //error
				    break;
			    }
			}
			this.LogSeq("[RB.1.4]1.2.10. If status change is True Then", logParent);
			if (statusChange === true){
			
				this.LogSeq("[RB.1.4] 1.2.10.1.Exit RollupRule Check Subprocess (Evaluation: True) (Stop at the first rule that evaluates to true - perform the associated action)", logParent);
				this.LogSeqReturn("true", logParent);
				
				this.LogSeqSimpleReturn("Rule #" + (i+1) + " fires. Status will change to " + ruleSet + ".", simpleLogParent);
				
				this.LogSeqSimpleReturn("Rule fires and status will change.", simpleLogParentForRule);
				
				return true;
			}
			else{
			    this.LogSeqSimpleReturn("Rule does not fire.", simpleLogParentForRule);
			}
			
		}
	}
	
	this.LogSeq("[RB.1.4]2. Exit Rollup Rule Check Subprocess (Evaluation: False) No rules evaluated to true - do not perform any action", logParent);
	this.LogSeqSimpleReturn("No rule fired, status will not change.", simpleLogParent);
	this.LogSeqReturn("false", logParent);
	return false;


	
}


//TODO - this should really be moved to be part of an object rather than a stand-alone function (it is used from other places - Sequencer_ObjectiveRollupProcess)
function Sequencer_GetApplicableSetofRollupRules(activity, ruleSet){
	
	var applicableRules = new Array();
	
	var allRules = activity.GetRollupRules();
	
	for (var i=0; i < allRules.length; i++){
		
		switch (ruleSet) {
			
			case RULE_SET_SATISFIED:
				if (allRules[i].Action == ROLLUP_RULE_ACTION_SATISFIED){applicableRules[applicableRules.length] = allRules[i];}
			break;
			
			case RULE_SET_NOT_SATISFIED:
				if (allRules[i].Action == ROLLUP_RULE_ACTION_NOT_SATISFIED){applicableRules[applicableRules.length] = allRules[i];}
			break;
			
			case RULE_SET_COMPLETED:
				if (allRules[i].Action == ROLLUP_RULE_ACTION_COMPLETED){applicableRules[applicableRules.length] = allRules[i];}
			break;
			
			case RULE_SET_INCOMPLETE:
				if (allRules[i].Action == ROLLUP_RULE_ACTION_INCOMPLETE){applicableRules[applicableRules.length] = allRules[i];}
			break;
			
		}

	}
	
	return applicableRules;
}