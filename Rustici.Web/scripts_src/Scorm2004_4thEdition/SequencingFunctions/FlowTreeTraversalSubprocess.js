//[SB.2.1]Flow Tree Traversal Subprocess [SB.2.1]
	//[SB.2.1]For an activity, a traversal direction, a consider children flag, and a previous traversal direction; returns the 'next' activity in directed traversal of the activity tree, may return the traversal direction, and may return an exception code
	
	//[SB.2.1]Reference: 
		//[SB.2.1]Available Children AM.1.1
		//[SB.2.1]Flow Activity Traversal Subprocess SB.2.2
		//[SB.2.1]Sequencing Control Forward Only SM.1
		//[SB.2.1]Sequencing Rules Check Process UP.2 


function Sequencer_FlowTreeTraversalSubprocess(activity, traversalDirection, previousTraversalDirection, considerChildren, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Flow Tree Traversal Subprocess [SB.2.1](" + activity + ", " + traversalDirection + ", " + previousTraversalDirection + ", " + considerChildren + ")", callingLog);
	
	var reversedDirection;
	var flowTreeTraversalResult;
	var nextAvailableActivity;
	var availableChildren;
	var previousAvailableActivity;
	
	var returnValue;
	
	var parentActivity = this.Activities.GetParentActivity(activity);
	
	this.LogSeq("[SB.2.1]1. Set reversed direction to False", logParent);
	reversedDirection = false;

	this.LogSeq("[SB.2.1]2. If (previous traversal direction is Defined And is Backward) And the activity is the last activity in the activity's parent's list of Available Children Then (Test if we have skipped all of the children in a forward only cluster moving backward)", logParent);
	
	if (	previousTraversalDirection !== null &&
			previousTraversalDirection == FLOW_DIRECTION_BACKWARD && 
			parentActivity.IsActivityTheLastAvailableChild(activity)){
		
		this.LogSeq("[SB.2.1]2.1. traversal direction is Backward", logParent);
		traversalDirection = FLOW_DIRECTION_BACKWARD;
		
		this.LogSeq("[SB.2.1]2.2. activity is the first activity in the activity's parent's list of Available Children", logParent);
		activity = parentActivity.GetFirstAvailableChild();
		
		this.LogSeq("[SB.2.1]2.3. Set reversed direction to True", logParent);
		reversedDirection = true;
	}

	this.LogSeq("[SB.2.1]3. If the traversal direction is Forward Then", logParent);
	
	if (traversalDirection == FLOW_DIRECTION_FORWARD){
	
		this.LogSeq("[SB.2.1]3.1. If the activity is the last available activity in a forward preorder tree traversal of the activity tree Or (the activity is the Root of the Activity Tree And consider children is False) Then (Walking off the tree causes the sequencing session to end)", logParent);
		
		if (
			(this.IsActivityLastOverall(activity, logParent) ) ||
			(considerChildren === false && activity.IsTheRoot() === true )
			 ){
			 
			this.LogSeqSimple("A continue request from the end of the course results in exiting the entire course.", simpleLogParent);
			 
			this.LogSeq("[SB.2.1]3.1.1. Apply the Terminate Descendent Attempt Process to the root of the activity tree", logParent);
			this.TerminateDescendentAttemptsProcess(this.Activities.GetRootActivity(), logParent, simpleLogParent);
			
			this.LogSeq("[SB.2.1]3.1.2. Exit Flow Tree Traversal Subprocess (Next Activity: n/a; End Sequencing Session: True; Exception: n/a) - Continuing past the last activity, exiting the course", logParent);
			returnValue = new  Sequencer_FlowTreeTraversalSubprocessReturnObject(null, null, null, null, true);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[SB.2.1]3.2. If the activity is a leaf Or consider children is False Then", logParent);
		
		if (activity.IsALeaf() || considerChildren === false){
		
			this.LogSeq("[SB.2.1]3.2.1. If the activity is the last activity in the activity's parent's list of Available Children Then", logParent);

//in sequencing test case RU-7c, if you complete activity #5 at the end and hit continue, we get an error here because parentActivity is null....figure out
//what the correct behavior is
//same thing happens in Competency Photoshop example when you pass the pretest and hit next
//note - this behavior should be corrected in the TerminationRequestProcess, lines 3.4 & 3.4.1 which are being added to the pseudo code
			
			if (parentActivity.IsActivityTheLastAvailableChild(activity)){
			
				this.LogSeq("[SB.2.1]3.2.1.1. Apply the Flow Tree Traversal Subprocess to the activity's parent in the Forward direction and a previous traversal direction of n/a with consider children equal to False (Recursion - Move to the activity's parent's next forward sibling)", logParent);
				flowTreeTraversalResult = this.FlowTreeTraversalSubprocess(parentActivity, FLOW_DIRECTION_FORWARD, null, false, logParent, simpleLogParent);
				
				this.LogSeq("[SB.2.1]3.2.1.2. Exit Flow Tree Traversal Subprocess (Return the results of the recursive Flow Tree Traversal Subprocess) (Return the result of the recursion)", logParent);
				returnValue = flowTreeTraversalResult;
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			
			}
			else{
				this.LogSeq("[SB.2.1]3.2.2. Else", logParent);
				
				this.LogSeq("[SB.2.1]3.2.2.1. Traverse the tree, forward preorder, one activity to the next activity, in the activity's parent's list of Available Children", logParent);
				nextAvailableActivity = parentActivity.GetNextActivity(activity);
				
				this.LogSeq("[SB.2.1]3.2.2.2.Exit Flow Tree Traversal Subprocess (Next Activity: the activity identified by the traversal; Traversal Direction: traversal direction; Exception: n/a)", logParent);
				returnValue = new Sequencer_FlowTreeTraversalSubprocessReturnObject(nextAvailableActivity, traversalDirection, null, "", false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			
		}
		else{
			this.LogSeq("[SB.2.1]3.3. Else Entering a cluster Forward", logParent);
			
			this.LogSeq("[SB.2.1]3.3.1. If the activity's list of Available Children is Not Empty Then Make sure this activity has a child activity", logParent);
			
			availableChildren = activity.GetAvailableChildren();
			
			if (availableChildren.length > 0){
			
				this.LogSeq("[SB.2.1]3.3.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: the first activity in the activity's list of Available Children (" + availableChildren[0] + "); Traversal Direction: traversal direction (" + traversalDirection + "); Exception: n/a )", logParent);
				returnValue = new Sequencer_FlowTreeTraversalSubprocessReturnObject(availableChildren[0], traversalDirection, null, "", false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;				
			}
			else{
				
				this.LogSeq("[SB.2.1]3.3.2. Else", logParent);
				this.LogSeq("[SB.2.1]3.3.2.1. Exit Flow Tree Traversal Subprocess (Next Activity Nil; Traversal Direction: n/a; Exception: SB.2.1-2)", logParent);
				
				returnValue = new Sequencer_FlowTreeTraversalSubprocessReturnObject(null, null, "SB.2.1-2", IntegrationImplementation.GetString("The activity '{0}' does not have any available children to deliver.", activity.GetTitle()), false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		}
	}

	this.LogSeq("[SB.2.1]4. If the traversal direction is Backward Then", logParent);
	
	if (traversalDirection == FLOW_DIRECTION_BACKWARD){
	
		this.LogSeq("[SB.2.1]4.1.If the activity is the root activity of the tree Then (Cannot walk off the root of the activity tree)", logParent);
		
		if (activity.IsTheRoot()){
			this.LogSeq("[SB.2.1]4.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: Nil; Traversal Direction: n/a; Exception: SB.2.1-3)", logParent);
			
			returnValue = new Sequencer_FlowTreeTraversalSubprocessReturnObject(null, null, "SB.2.1-3", IntegrationImplementation.GetString("You have reached the beginning of the course."), false);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		
		this.LogSeq("[SB.2.1]4.2. If the activity is a leaf Or consider children is False Then", logParent);
		
		if (activity.IsALeaf() || considerChildren === false){
		
			this.LogSeq("[SB.2.1]4.2.1. If reversed direction is False Then (Only test 'forward only' if we are not going to leave this forward only cluster.)", logParent);
			
			if (reversedDirection === false){
				
				this.LogSeq("[SB.2.1]4.2.1.1. If Sequencing Control Forward Only for the parent of the activity is True Then (Test the control mode before traversing)", logParent);
				
				if (parentActivity.GetSequencingControlForwardOnly() === true){
				    
				    this.LogSeqSimple("\"" + parentActivity + "\" cannot be moved through backwards (sequencing control forward only). It must be entered from the beginning and proceed forward. Navigation request not allowed.", simpleLogParent);
				    
					this.LogSeq("[SB.2.1]4.2.1.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: Nil; Traversal Direction: n/a; Exception: SB.2.1-4)", logParent);
					returnValue = new Sequencer_FlowTreeTraversalSubprocessReturnObject(null, null, "SB.2.1-4", IntegrationImplementation.GetString("The activity '{0}' may only be entered from the beginning.", parentActivity.GetTitle()), false);
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			}
			
			this.LogSeq("[SB.2.1]4.2.2. If the activity is the first activity in the activity's parent's list of Available Children Then", logParent);
			
			if (parentActivity.IsActivityTheFirstAvailableChild(activity)){
				
				this.LogSeq("[SB.2.1]4.2.2.1. Apply the Flow Tree Traversal Subprocess to the activity's parent in the Backward direction and a previous traversal direction of n/a with consider children equal to False (Recursion - Move to the activity's parent's next backward sibling)", logParent);
				flowTreeTraversalResult = this.FlowTreeTraversalSubprocess(parentActivity, FLOW_DIRECTION_BACKWARD, null, false, logParent, simpleLogParent);
				
				this.LogSeq("[SB.2.1]4.2.2.2. Exit Flow Tree Traversal Subprocess (Return the results of the recursive Flow Tree Traversal Subprocess) (Return the result of the recursion)", logParent);
				returnValue = flowTreeTraversalResult;
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
			else{
				this.LogSeq("[SB.2.1]4.2.3. Else", logParent);
				
				this.LogSeq("[SB.2.1]4.2.3.1. Traverse the tree, reverse preorder, one activity to the previous activity, from the activity's parent's list of Available Children", logParent);
				
				previousAvailableActivity = parentActivity.GetPreviousActivity(activity);
				
				this.LogSeq("[SB.2.1]4.2.3.2. Exit Flow Tree Traversal Subprocess (Next Activity: " + previousAvailableActivity + "; Traversal Direction: " + traversalDirection + " ; Exception: n/a)", logParent);
				returnValue = new Sequencer_FlowTreeTraversalSubprocessReturnObject(previousAvailableActivity, traversalDirection, null, "", false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		}
		else{
			this.LogSeq("[SB.2.1]4.3. Else (Entering a cluster Backward)", logParent);
			
			this.LogSeq("[SB.2.1]4.3.1. If the activity's list of Available Children is Not Empty Then (Make sure this activity has a child activity)", logParent);
			availableChildren = activity.GetAvailableChildren();
			
			if (availableChildren.length > 0){
			
				this.LogSeq("[[SB.2.1]4.3.1.1. If Sequencing Control Forward Only for the activity is True Then", logParent);
				
				if (activity.GetSequencingControlForwardOnly() === true){
					
					this.LogSeqSimple("\"" + activity + "\" cannot be entered backwards (sequencing control forward only), it must be entered from the beginning. Attempting to deliver its first child (" + availableChildren[0] + ").", simpleLogParent);

					this.LogSeq("[SB.2.1]4.3.1.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: the first activity in the activity's list of Available Children; Traversal Direction: Forward; Exception: n/a) (Start at the beginning of a forward only cluster)", logParent);
					
					returnValue = new Sequencer_FlowTreeTraversalSubprocessReturnObject(availableChildren[0], FLOW_DIRECTION_FORWARD, null, "", false);
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
				else{
					this.LogSeq("[SB.2.1]4.3.1.2. Else", logParent);
					this.LogSeq("[SB.2.1]4.3.1.2.1. Exit Flow Tree Traversal Subprocess (Next Activity: the last activity in the activity's list of Available Children; Traversal Direction: Backward; Exception: n/a) Start at the end of the cluster if we are backing into it", logParent);
					returnValue = new Sequencer_FlowTreeTraversalSubprocessReturnObject(availableChildren[availableChildren.length-1], FLOW_DIRECTION_BACKWARD, null, "", false);
					this.LogSeqReturn(returnValue, logParent);
					return returnValue;
				}
			}
			else{
				this.LogSeq("[SB.2.1]4.3.2. Else", logParent);
				this.LogSeq("[SB.2.1]4.3.2.1. Exit Flow Tree Traversal Subprocess (Next Activity: Nil; Traversal Direction: n/a; Exception: SB.2.1-2)", logParent);
				returnValue = new Sequencer_FlowTreeTraversalSubprocessReturnObject(null, null, "SB.2.1-2", IntegrationInterface.GetString("The activity '{0}' may only be entered from the beginning.", parentActivity.GetTitle()), false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		}
	}

}

function Sequencer_FlowTreeTraversalSubprocessReturnObject(nextActivity, traversalDirection, exception, exceptionText, endSequencingSession){
	
	Debug.AssertError("Invalid endSequencingSession (" + endSequencingSession + ") passed to FlowTreeTraversalSubprocessReturnObject.", (endSequencingSession != true && endSequencingSession != false))
	
	this.NextActivity = nextActivity;
	this.TraversalDirection = traversalDirection;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	this.EndSequencingSession = endSequencingSession;
}

Sequencer_FlowTreeTraversalSubprocessReturnObject.prototype.toString = function(){
									return "NextActivity=" + this.NextActivity + 
										", TraversalDirection=" + this.TraversalDirection + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText +
										", EndSequencingSession=" + this.EndSequencingSession;
									};