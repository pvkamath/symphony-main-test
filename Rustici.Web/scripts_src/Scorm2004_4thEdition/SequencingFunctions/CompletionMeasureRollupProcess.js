//****************************************************************************************************************

//Completion Measure Rollup Process [RB.1.1 b]

//	For an activity; may change the Objective Information for the activity

//	Reference: 
		//Attempt Completion Amount Status TM.1.1;
		//Attempt Completion Amount TM.1.1;
		//Tracked SM.11;
		//adlcp:minProgressMeasure;
		//adlcp:progressWeight

function Sequencer_CompletionMeasureRollupProcess(activity, callingLog, simpleLogParent){
    
    Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Completion Measure Rollup Process [RB.1.1 b](" + activity + ")", callingLog);
	
	simpleLogParent = this.LogSeqSimpleAudit("Rolling up the progress measure for \"" + activity + "\".", simpleLogParent);
	
	this.LogSeq("[RB.1.1 b]1. Set the total weighted measure to Zero (0.0)",logParent);
	var totalWeightedMeasure = 0;
	 
    this.LogSeq("[RB.1.1 b]2. Set valid data to False", logParent);
    var validData = false;
    
    this.LogSeq("[RB.1.1 b]3. Set the counted measures to Zero (0.0)", logParent);  
    var countedMeasures = 0;
    
    this.LogSeq("[RB.1.1 b]4. For each child of the activity", logParent);
    var children = activity.GetChildren();
    for (var i=0; i < children.length; i++){
    
        this.LogSeq("[RB.1.1 b]4.1. If Tracked for the child is True Then (Only include tracked children.)", logParent);
        if (children[i].IsTracked() === true){
        
            this.LogSeq("[RB.1.1 b]4.1.1. Increment counted measures by the adlcp:progressWeight for the child (The child is included, account for it in the weighted average)", logParent);
            var childProgressWeight = children[i].GetCompletionProgressWeight();
            countedMeasures += childProgressWeight;
            
            this.LogSeq("[RB.1.1 b]4.1.2. If the Attempt Completion Amount Status is True Then", logParent);
            if (children[i].GetAttemptCompletionAmountStatus() === true){
            
                this.LogSeq("[RB.1.1 b]4.1.2.1. Add the product of Attempt Completion Amount multiplied by the adlcp:progressWeight to the total weighted measure (Only include progress that has been reported or previously rolled-up)", logParent);
                totalWeightedMeasure += (children[i].GetAttemptCompletionAmount() * childProgressWeight);
                
                this.LogSeqSimple("\"" + children[i] + "\" has a progress measure of " + children[i].GetAttemptCompletionAmount() + " and a weight of " + childProgressWeight + " so it contributes a weighted measure of " + (children[i].GetAttemptCompletionAmount() * childProgressWeight) + ".", simpleLogParent);

                this.LogSeq("[RB.1.1 b]4.1.2.2. Set valid data to True", logParent);
                validData = true;
                
            }//End If  GetAttemptCompletionAmountStatus
        }//End If Tracked
        else{
            this.LogSeqSimple("\"" + children[i] + "\" is not tracked and will not contribute to progress measure rollup.", simpleLogParent);
        }
    
    }//End For  
    
    this.LogSeq("[RB.1.1 b]5. If valid data is False Then", logParent);
    if (!validData){
    
        this.LogSeq("[RB.1.1 b]5.1. Set the Attempt Completion Amount Status to False (No progress state rolled-up, cannot determine the rolled-up progress.)", logParent);
        this.LogSeqSimple("No children had a progress measure to rollup, setting the completion amount status to unknown.", simpleLogParent);
        this.LogSeqSimpleReturn("unknown", simpleLogParent);
        
        activity.SetAttemptCompletionAmountStatus(false);
        
    
    } else {
        this.LogSeq("[RB.1.1 b]5.2. Else", logParent);
        
        this.LogSeq("[RB.1.1 b]5.2.1. If counted measures is greater than (>) Zero (0.0) Then (Set the rolled-up progress.)", logParent);
        if (countedMeasures > 0){
        
            this.LogSeq("[RB.1.1 b]5.2.1.1. Set the Attempt Completion Amount Status to True", logParent);
            activity.SetAttemptCompletionAmountStatus(true);
            
            this.LogSeq("[RB.1.1 b]5.2.1.2. Set the Attempt Completion Amount to the total weighted measure divided by counted measures", logParent);
            var newCompletionAmount = (totalWeightedMeasure / countedMeasures);
			newCompletionAmount = RoundToPrecision(newCompletionAmount, 7);
            activity.SetAttemptCompletionAmount(newCompletionAmount);
            
            this.LogSeqSimple("Setting the attempt completion amount to the new weighted value of " + newCompletionAmount + ".", simpleLogParent);
            this.LogSeqSimpleReturn(newCompletionAmount, simpleLogParent);
        
        } else {
            this.LogSeq("[RB.1.1 b]5.2.2. Else ", logParent);
            
            this.LogSeq("[RB.1.1 b]5.2.2.1. Set the Attempt Completion Amount Status for the target objective to False (No children contributed weight.)", logParent);
            activity.SetAttemptCompletionAmountStatus(false);
        
        }//End If   
        
    }
    
    this.LogSeq("[RB.1.1 b]6. Exit Completion Measure Rollup Process", logParent);
    this.LogSeqReturn("", logParent);
	return;



}
