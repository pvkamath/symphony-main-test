//****************************************************************************************************************

//Activity Progress Rollup Process [RB.1.3] 

// (Not Specified in p-code)



function Sequencer_ActivityProgressRollupProcess(activity, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Activity Progress Rollup Process [RB.1.3](" + activity + ")", callingLog);
	
	simpleLogParent = this.LogSeqSimpleAudit("Rolling up the completion status of \"" + activity + "\".", simpleLogParent);
	
	//select the appropriate progress rollup process using the rules defined on SN-4-44 - this pseudo code is not defined in the spec, just the logic
	
    //	The Activity Progress Rollup Process sets the cluster�s activity completion status to 
    //�unknown�, �complete� or �incomplete�.  It only includes children that are tracked 
    //and that have Rollup Progress Completion equal to True.  There are three methods of 
    //rolling up progress information.  The first method that applies is the only one used to 
    //evaluate the progress status of the cluster. 
	
	
	this.LogSeq("[RB.1.3]1. Using Measure:  If the activity has Completed by Measure equal to True, then the rolled-up progress measure is compared against the Minimum Progress Measure and Measure Satisfaction if Active:", logParent);
	if (activity.GetCompletedByMeasure() === true){
	    
	    this.LogSeq("[RB.1.3]1.1 Apply Activity Progress Rollup Using Measure Process ", logParent);
	    this.ActivityProgressRollupUsingMeasureProcess(activity, logParent, simpleLogParent)
	
	
	} else {
		this.LogSeq("[RB.1.3]2. Else", logParent);
		
		this.LogSeq("[RB.1.3]2.1. Apply Activity Progress Rollup Using Measure Process", logParent);
		this.ActivityProgressRollupUsingRulesProcess(activity, logParent, simpleLogParent)
		
	}

	//Default rule is handled in the ActivityProgressRollupUsingRulesProcess.
	
	this.LogSeq("[RB.1.3]3. Exit Activity Progress Rollup Process", logParent);
	
	this.LogSeqReturn("", logParent);
	return;
	
}
