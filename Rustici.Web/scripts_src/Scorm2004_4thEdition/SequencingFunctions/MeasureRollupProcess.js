//****************************************************************************************************************

//Measure Rollup Process [RB.1.1]

//	For an activity; may change the Objective Information for the activity

//This process consolidates the normalized measures of an activity's children's primary objectives into the normalized measure for the primary objective of the activity.

//	Reference: 
		//Objective Contributes to Rollup SM.6
		//Objective Description SM.6
		//Objective Measure Status TM.1.1
		//Objective Normalized Measure TM.1.1
		//Rollup Objective Measure Weight SM.8
		//Tracked SM.11 


function Sequencer_MeasureRollupProcess(activity, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Measure Rollup Process [RB.1.1](" + activity + ")", callingLog);
	
	simpleLogParent = this.LogSeqSimpleAudit("Rolling up the score (measure) for \"" + activity + "\".", simpleLogParent);
	
	this.LogSeq("[RB.1.1]1. Set the total weighted measure to Zero (0.0)", logParent);
	var totalWeightedMeasure = 0;
	
	this.LogSeq("[RB.1.1]2. Set valid date to False", logParent);
	var validData = false;
	
	this.LogSeq("[RB.1.1]3. Set the counted measures to Zero (0.0)", logParent);
	var countedMeasures = 0;
	
	this.LogSeq("[RB.1.1]4. Set the target objective to Undefined", logParent);
	var targetObjective = null;
	
	
	
	var i;
	
	/*
	this.LogSeq("[RB.1.1]5. For each objective associated with the activity", logParent);
	var objectives = activity.GetObjectives();
	for (i=0; i < objectives.length; i++){
		this.LogSeq("[RB.1.1]5.1. If Objective Contributes to Rollup for the objective is True Then (Find the target objective for the rolled-up measure)", logParent);
		
		if (objectives[i].GetContributesToRollup() === true){
			
			this.LogSeq("[RB.1.1]5.1.1. Set the target objective to the objective", logParent);
			targetObjective = objectives[i];
			
			this.LogSeq("RB.1.1]5.1.2. Break For", logParent);
			break;
		}
	}
	*/
	this.LogSeq("[RB.1.1]5. Get the primary objective (For each objective associated with the activity)", logParent);
	var targetObjective = activity.GetPrimaryObjective();
	
	
	this.LogSeq("[RB.1.1]6. If target objective is Defined Then", logParent);
	if (targetObjective !== null){
		
		this.LogSeq("[RB.1.1]6.1. For each child of the activity", logParent);	//TODO - is this just available children, or all children?
		
		var children = activity.GetChildren();
		var rolledUpObjective = null;
		var childObjectives;
		var objectiveMeasureWeight;
		var normalizedMeasure;
		
		for (i=0; i < children.length; i++){
		
			this.LogSeq("[RB.1.1]6.1.1. If Tracked for the child is True Then (Only include tracked children). Tracked = " + children[i].IsTracked(), logParent);
			
			if (children[i].IsTracked()){
			
				this.LogSeq("[RB.1.1]6.1.1.1. Set rolled-up objective to Undefined", logParent);
				rolledUpObjective = null;
				
				this.LogSeq("[RB.1.1]6.1.1.2. Get the primary objective.", logParent);
				var rolledUpObjective = children[i].GetPrimaryObjective();
				
				this.LogSeq("[RB.1.1]6.1.1.3. If rolled-up objective is Defined Then", logParent);
				if (rolledUpObjective !== null){
				
					this.LogSeq("[RB.1.1]6.1.1.3.1. Increment counted measures by the Rollup Objective Measure Weight for the child", logParent);
					objectiveMeasureWeight = children[i].GetRollupObjectiveMeasureWeight();
					countedMeasures += objectiveMeasureWeight;
					
					this.LogSeq("[RB.1.1]6.1.1.3.2. If the Objective Measure Status for the rolled-up objective is True Then", logParent);
					if (rolledUpObjective.GetMeasureStatus(children[i], false) === true){
						
						this.LogSeq("[RB.1.1]6.1.1.3.2.1. Add the product of Objective Normalized Measure for the rolled-up objective multiplied by the Rollup Objective Measure Weight for the child to the total weighted measure", logParent);
						normalizedMeasure = rolledUpObjective.GetNormalizedMeasure(children[i], false);
						
						this.LogSeq("[RB.1.1]6.1.1.3.2.2. Set valid data to True - Normalized Measure = " + normalizedMeasure + ", Objective Measure Weight=" + objectiveMeasureWeight, logParent);
						validData = true;
						totalWeightedMeasure += (normalizedMeasure * objectiveMeasureWeight);
						
						this.LogSeqSimple("\"" + children[i] + "\" has a score of " + normalizedMeasure + " and a weight of " + objectiveMeasureWeight + " so it contributes a weighted measure of " + (normalizedMeasure * objectiveMeasureWeight) + ".", simpleLogParent);
					}
				}
				else{
					this.LogSeq("[RB.1.1]6.1.1.4. Else", logParent);
					this.LogSeq("[RB.1.1]6.1.1.4.1. Exit Measure Rollup Process (One of the children does not include a rolled-up objective)", logParent);

					Debug.AssertError("Measure Rollup Process encountered an activity with no primary objective.");
					
					this.LogSeqReturn("", logParent);
					return;
				}
			}
			else{
			    this.LogSeqSimple("\"" + children[i] + "\" is not tracked and will not contribute a score to rollup.", simpleLogParent);
			}
			
		}
		
		this.LogSeq("[RB.1.1]6.2. If valid data is False Then", logParent);
		if (validData === false || countedMeasures == 0){
			
			this.LogSeq("[RB.1.1]6.2.1. Set the Objective Measure Status for the target objective to False (No tracking state rolled-up, cannot determine the rolled-up measure. Total of all objectivemeasureweight values = " + countedMeasures + ")", logParent);
			
			this.LogSeqSimple("No children had a score to rollup, setting the measure status to unknown.", simpleLogParent);
			this.LogSeqSimpleReturn("unknown.", simpleLogParent);
			
			targetObjective.SetMeasureStatus(false, activity);
		}
		else{
			this.LogSeq("[RB.1.1]6.3. Else", logParent);
			this.LogSeq("[RB.1.1]6.3.1 If counted measures is greater than (>) Zero (0.0) Then (Set the rolled-up measure for the target objective.)", logParent);
            
            if (countedMeasures > 0){
            
                this.LogSeq("[RB.1.1]6.3.1.1 Set the Objective Measure Status for the target objective to True", logParent);
                targetObjective.SetMeasureStatus(true, activity);
                
                this.LogSeq("[RB.1.1]6.3.1.2. Set the Objective Normalized Measure for the target objective to the total weighted measure (" + totalWeightedMeasure + ") divided by counted measures (" + countedMeasures + "). Equals " + (totalWeightedMeasure / countedMeasures), logParent);
                var newNormalizedMeasure = (totalWeightedMeasure / countedMeasures);
			    newNormalizedMeasure = RoundToPrecision(newNormalizedMeasure, 7);
			    targetObjective.SetNormalizedMeasure(newNormalizedMeasure, activity);
			    
			    this.LogSeqSimple("Setting the score to the new weighted measure of " + newNormalizedMeasure + ".", simpleLogParent);
			    this.LogSeqSimpleReturn(newNormalizedMeasure, simpleLogParent);
			    
            } else {
                this.LogSeq("[RB.1.1]6.3.2. Else", logParent);
                this.LogSeq("[RB.1.1]6.3.2.1 Set the Objective Measure Status for the target objective to False (No children contributed weight.)", logParent);
			    targetObjective.SetMeasureStatus(false, activity);
			}
	    }
	}
	
	this.LogSeq("[RB.1.1]7.Exit Measure Rollup Process No objective contributes to rollup, so we cannot set anything", logParent);
	this.LogSeqReturn("", logParent);
	return;
	
}


