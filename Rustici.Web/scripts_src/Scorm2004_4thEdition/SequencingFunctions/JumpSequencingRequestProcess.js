//Jump Sequencing Request Process [SB.2.13] 

	//May return a delivery request; may return an exception code
	//Reference: 
		//Current Activity AM.1.2
		
		
function Sequencer_JumpSequencingRequestProcess(targetActivity, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Jump Sequencing Request Process [SB.2.13]", callingLog);
	
	var returnValue;
	
	this.LogSeq("[SB.2.13]1. If the Current Activity is not Defined Then (Make sure the sequencing session has already begun.)", logParent);
    if (!this.IsCurrentActivityDefined(logParent)){
    
        this.LogSeq("[SB.2.13]1.1. Exit Jump Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.13-1) (Nothing to deliver.)", logParent);
        returnValue = new Sequencer_JumpSequencingRequestProcessResult(null, "SB.2.13-1", IntegrationImplementation.GetString("The sequencing session has not begun yet."), false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
        
    }// End If
    
    this.LogSeq("[SB.2.13]2. Exit Jump Sequencing Request Process (Delivery Request: the activity identified by the target activity; Exception: n/a)", logParent);
    returnValue = new Sequencer_JumpSequencingRequestProcessResult(targetActivity, null, "", false);
	this.LogSeqReturn(returnValue, logParent);
	return returnValue;
	
}

function Sequencer_JumpSequencingRequestProcessResult(deliveryRequest, exception, exceptionText, endSequencingSession){
	
	Debug.AssertError("Invalid endSequencingSession (" + endSequencingSession + ") passed to JumpSequencingRequestProcessResult.", (endSequencingSession != true && endSequencingSession != false))
		
	this.DeliveryRequest = deliveryRequest;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	this.EndSequencingSession = endSequencingSession;
}

Sequencer_JumpSequencingRequestProcessResult.prototype.toString = function(){
									return "DeliveryRequest=" + this.DeliveryRequest + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText + 
										", EndSequencingSession=" + this.EndSequencingSession;
									};