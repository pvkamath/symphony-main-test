//****************************************************************************************************************

//Objective Rollup Using Rules Process [RB.1.2 b] 
//	For an activity; may change the Objective Information for the activity

//	Reference: 
		//Objective Contributes to Rollup SM.6
		//Objective Description SM.6
		//Objective Progress Status TM.1.1
		//Objective Satisfied Status TM.1.1
		//Rollup Rule Check Subprocess RB.1.4
		//Rollup Action SM.5 
		
function Sequencer_ObjectiveRollupUsingRulesProcess(activity, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	
	var logParent = this.LogSeqAudit("Objective Rollup Using Rules Process [RB.1.2 b](" + activity + ")", callingLog);
	
	var subprocessResult;
    
    this.LogSeq("[RB.1.2 b]1. If the activity does not include Rollup Rules with the Not Satisfied  rollup action And the activity does not include Rollup Rules with the Satisfied  rollup action Then (If no objective rollup rules are defined, use the default rollup rules.)  ", logParent);
	if (Sequencer_GetApplicableSetofRollupRules(activity, RULE_SET_NOT_SATISFIED).length === 0 &&
	     Sequencer_GetApplicableSetofRollupRules(activity, RULE_SET_SATISFIED).length === 0){
        
        this.LogSeqSimple("Applying the default set of satisfaction rollup rules to \"" + activity + "\".", simpleLogParent);
        
        this.LogSeq("[RB.1.2 b]1.1. Apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Satisfied; and a Rollup Action of Satisfied (Define the default satisfied rule )", logParent);
        activity.ApplyRollupRule(new SequencingRollupRule(RULE_CONDITION_COMBINATION_ANY,
                                                          CHILD_ACTIVITY_SET_ALL,
                                                          ROLLUP_RULE_MINIMUM_COUNT_DEFAULT,
                                                          ROLLUP_RULE_MINIMUM_PERCENT_DEFAULT,
                                                          ROLLUP_RULE_ACTION_SATISFIED,
                                                          new Array(new SequencingRollupRuleCondition(RULE_CONDITION_OPERATOR_NOOP,ROLLUP_RULE_CONDITION_SATISFIED))));
    
        this.LogSeq("[RB.1.2 b]1.2. Apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Objective Status Known; and a Rollup Action of Not Satisfied (Define the default not satisfied rule )", logParent);
        activity.ApplyRollupRule(new SequencingRollupRule(RULE_CONDITION_COMBINATION_ANY,
                                                          CHILD_ACTIVITY_SET_ALL,
                                                          ROLLUP_RULE_MINIMUM_COUNT_DEFAULT,
                                                          ROLLUP_RULE_MINIMUM_PERCENT_DEFAULT,
                                                          ROLLUP_RULE_ACTION_NOT_SATISFIED,
                                                          new Array(new SequencingRollupRuleCondition(RULE_CONDITION_OPERATOR_NOOP, ROLLUP_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN))));
        
    }
    else{
        if (activity.UsesDefaultSatisfactionRollupRules() === true){
            this.LogSeqSimple("Applying the default set of satisfaction rollup rules to \"" + activity + "\".", simpleLogParent);
        }
    }//End If 

    
	this.LogSeq("[RB.1.2 b]2.Set the target objective to Undefined", logParent);
	var targetObjective = null;
	
	/*
	this.LogSeq("[RB.1.2 b]3. For each objective associated with the activity", logParent);
	var objectives = activity.GetObjectives();
	
	for (var i=0; i < objectives.length; i++){
	
		this.LogSeq("[RB.1.2 b]3.1. If Objective Contributes to Rollup for the objective is True Then (Identify the objective that may be altered based on the activity's children's rolled up status)", logParent);
		if (objectives[i].GetContributesToRollup() === true){
			
			this.LogSeq("[RB.1.2 b]3.1.1. Set the target objective to the objective", logParent);
			targetObjective = objectives[i];
			
			this.LogSeq("[RB.1.2 b]3.1.2. Break For", logParent);
			break;
		}
	}
	*/
	
	this.LogSeq("[RB.1.2 b]3. Get the primary objective (For each objective associated with the activity)", logParent);
	var targetObjective = activity.GetPrimaryObjective();
	
	this.LogSeq("[RB.1.2 b]4. If target objective is Defined Then", logParent);
	if (targetObjective !== null){
	
		this.LogSeq("[RB.1.2 b]4.1. Apply the Rollup Rule Check Subprocess to the activity and the Not Satisfied rollup action Process all Not Satisfied rules first", logParent);
		subprocessResult = this.RollupRuleCheckSubprocess(activity, RULE_SET_NOT_SATISFIED, logParent, simpleLogParent);
		
		this.LogSeq("[RB.1.2 b]4.2. If the Rollup Rule Check Subprocess returned True Then", logParent);
		if (subprocessResult === true){
			
			this.LogSeqSimple("Setting " + activity + " to not satisfied.", simpleLogParent);
			
			this.LogSeq("[RB.1.2 b]4.2.1. Set the Objective Progress Status for the target objective to True", logParent);
			targetObjective.SetProgressStatus(true, false, activity);
			
			this.LogSeq("[RB.1.2 b]4.2.2. Set the Objective Satisfied Status for the target objective to False", logParent);
			targetObjective.SetSatisfiedStatus(false, false, activity);
		} 
		
		this.LogSeq("[RB.1.2 b]4.3. Apply the Rollup Rule Check Subprocess to the activity and the Satisfied rollup action Process all Satisfied rules last", logParent);
		subprocessResult = this.RollupRuleCheckSubprocess(activity, RULE_SET_SATISFIED, logParent, simpleLogParent);
		
		this.LogSeq("[RB.1.2 b]4.4. If the Rollup Rule Check Subprocess returned True Then", logParent);
		if (subprocessResult === true){
		    
		    this.LogSeqSimple("Setting \"" + activity + "\" to satisfied.", simpleLogParent);
		    
			this.LogSeq("[RB.1.2 b]4.4.1. Set the Objective Progress Status for the target objective to True", logParent);
			targetObjective.SetProgressStatus(true, false, activity);
			
			this.LogSeq("[RB.1.2 b]4.4.2. Set the Objective Satisfied Status for the target objective to True", logParent);
			targetObjective.SetSatisfiedStatus(true, false, activity);
		}
		
		this.LogSeq("[RB.1.2 b]4.5. Exit Objective Rollup Using Rules Process", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	else{
		this.LogSeq("[RB.1.2 b]5. Else", logParent);
		this.LogSeq("[RB.1.2 b]5.1. Exit Objective Rollup Using Rules Process No objective contributes to rollup, so we cannot set anything", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}

}

