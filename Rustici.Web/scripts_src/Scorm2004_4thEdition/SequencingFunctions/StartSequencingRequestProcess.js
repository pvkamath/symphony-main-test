//****************************************************************************************************************

//Start Sequencing Request Process [SB.2.5]
	//May return a delivery request; may return an exception code
	//Reference: 
		//Current Activity AM.1.2
		//Flow Subprocess SB.2.3 


function Sequencer_StartSequencingRequestProcess(callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Start Sequencing Request Process [SB.2.5]", callingLog);
	
	var returnValue;
	
	this.LogSeq("[SB.2.5]1. If the Current Activity is Defined Then (Make sure the sequencing session has not already begun)", logParent);
	if ( this.IsCurrentActivityDefined(logParent)){
		this.LogSeq("[SB.2.5]1.1. Exit Start Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.5-1) (Nothing to deliver)", logParent);

		returnValue = new Sequencer_StartSequencingRequestProcessResult(null, "SB.2.5-1", IntegrationImplementation.GetString("You cannot 'Start' an item that is already open."), false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	var rootActivity = this.GetRootActivity(logParent);
	
	this.LogSeq("[SB.2.5]2. If the root of the activity tree is a leaf Then (Before starting, make sure the activity tree contains more than one activity)", logParent);
	
	if (rootActivity.IsALeaf()){
		this.LogSeq("[SB.2.5]2.1. Exit Start Sequencing Request Process (Delivery Request: the root of the activity tree; Exception: n/a) (Only one activity, it must be a leaf)", logParent);
		
		returnValue = new Sequencer_StartSequencingRequestProcessResult(rootActivity, null, "", false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	else if(Control.Package.Properties.AlwaysFlowToFirstSco === true){
		
		this.LogSeqSimple("Ignoring all sequencing rules and selecting the first SCO because the package property setting AlwaysFlowToFristSco is true.", simpleLogParent);
		
		this.LogSeq("[SB.2.5]2.5 Rustici Extension - This course has a property that indicates we should always flow to the first SCO, so find it and return it", logParent);
		
		this.LogSeq("[SB.2.5]2.5.1. Get the ordered list of activities", logParent);
		var orderedListOfActivities = this.GetOrderedListOfActivities(false, logParent);
		
		this.LogSeq("[SB.2.5]2.5.2. Loop to find the first deliverable activity", logParent);
		for(var orderedActivity in orderedListOfActivities){
			
			if (orderedListOfActivities[orderedActivity].IsDeliverable() === true){
				returnValue = new Sequencer_StartSequencingRequestProcessResult(orderedListOfActivities[orderedActivity], null, "", false);
				this.LogSeqReturn(returnValue, logParent);
				return returnValue;
			}
		}
		
		returnValue = new Sequencer_StartSequencingRequestProcessResult(null, "SB.2.5-2.5", "There are no deliverable activities in this course.", false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	else{
		this.LogSeq("[SB.2.5]3. Else ", logParent);
		this.LogSeq("[SB.2.5]3.1. Apply the Flow Subprocess to the root of the activity tree in the Forward direction with consider children equal to True (Attempt to flow into the activity tree)", logParent);
		
		var flowSubProcessResult = this.FlowSubprocess(rootActivity, FLOW_DIRECTION_FORWARD, true, logParent, simpleLogParent);

		this.LogSeq("[SB.2.5]3.2. If the Flow Subprocess returns False Then", logParent);
		if (flowSubProcessResult.Deliverable === false){
			
			this.LogSeq("[SB.2.5]3.2.1. Exit Start Sequencing Request Process (Delivery Request: n/a; End Sequencing Session: as identified by the Flow Subprocess; Exception: the exception identified by the Flow Subprocess) (Nothing to deliver)", logParent);
			
			
			returnValue = new Sequencer_StartSequencingRequestProcessResult(null, flowSubProcessResult.Exception, flowSubProcessResult.ExceptionText, flowSubProcessResult.EndSequencingSession);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
		else{
			this.LogSeq("[SB.2.5]3.3. Else ", logParent);
			this.LogSeq("[SB.2.5]3.3.1. Exit Start Sequencing Request Process (Delivery Request: the activity identified by the Flow Subprocess; Exception: n/a)", logParent);
			
			returnValue = new Sequencer_StartSequencingRequestProcessResult(flowSubProcessResult.IdentifiedActivity, null, "", false);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
	}
}


function Sequencer_StartSequencingRequestProcessResult(deliveryRequest, exception, exceptionText, endSequencingSession){
	
	Debug.AssertError("Invalid endSequencingSession (" + endSequencingSession + ") passed to StartSequencingRequestProcessResult.", (endSequencingSession != true && endSequencingSession != false))
	
	this.DeliveryRequest = deliveryRequest;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	this.EndSequencingSession = endSequencingSession;
}

Sequencer_StartSequencingRequestProcessResult.prototype.toString = function(){
									return "DeliveryRequest=" + this.DeliveryRequest + 
										", Exception=" + this.Exception +
										", ExceptionText=" + this.ExceptionText + 
										", EndSequencingSession=" + this.EndSequencingSession;
									};
