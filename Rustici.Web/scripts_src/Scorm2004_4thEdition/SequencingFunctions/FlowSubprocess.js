//****************************************************************************************************************

//Flow Subprocess [SB.2.3] 
	
	//For an activity, a traversal direction, and a consider children flag; indicates if the flow was successful and at what activity the flow stopped; may return an exception code
	
	//Reference: 
		//Flow Activity Traversal Subprocess SB.2.2
		//Flow Tree Traversal Subprocess SB.2.1 
	
	
function Sequencer_FlowSubprocess(activity, traversalDirection, considerChildren, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Flow Subprocess [SB.2.3](" + activity + ", " + traversalDirection + ", " + considerChildren + ")", callingLog);
	
	var returnValue;
	
	this.LogSeq("[SB.2.3]1. The candidate activity is the activity The candidate activity is where we start 'flowing' from", logParent);
	var candidateActivity = activity;
	
	this.LogSeq("[SB.2.3]2. Apply the Flow Tree Traversal Subprocess to the candidate activity in the traversal direction and a previous traversal direction of n/a with consider children equal to the consider children flag (Attempt to move away from the starting activity, one activity in the specified direction)", logParent);
	var flowTreeTraversalResult = this.FlowTreeTraversalSubprocess(candidateActivity, traversalDirection, null, considerChildren, logParent, simpleLogParent);
	
	this.LogSeq("[SB.2.3]3. If the Flow Tree Traversal Subprocess does not identify an activity Then (No activity to move to)", logParent);
	if (flowTreeTraversalResult.NextActivity === null){
		
		this.LogSeq("[SB.2.3]3.1. Exit Flow Subprocess (Identified Activity: candidate activity; Deliverable: False; End Sequencing Session: as identified by the Flow Tree Traversal Subprocess; Exception: exception identified by the Flow Tree Traversal Subprocess)", logParent);
				
		returnValue = new Sequencer_FlowSubprocessResult(candidateActivity, false, flowTreeTraversalResult.Exception, flowTreeTraversalResult.ExceptionText, flowTreeTraversalResult.EndSequencingSession);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	else{
		this.LogSeq("[SB.2.3]4. Else", logParent);
		this.LogSeq("[SB.2.3]4.1. Candidate activity is the activity identified by the Flow Tree Traversal Subprocess", logParent);
		
		candidateActivity = flowTreeTraversalResult.NextActivity;
		
		this.LogSeq("[SB.2.3]4.2. Apply the Flow Activity Traversal Subprocess to the candidate activity in the traversal direction and a previous traversal direction of n/a (Validate the candidate activity and traverse until a valid leaf is encountered)", logParent);
		
		var flowActivityTraversalResult = this.FlowActivityTraversalSubprocess(candidateActivity, traversalDirection, null, logParent, simpleLogParent);
		
		this.LogSeq("[SB.2.3]4.3.Exit Flow Subprocess (Identified Activity: the activity identified by the Flow Activity Traversal Subprocess; Deliverable: as identified by the Flow Activity Traversal Subprocess;End Sequencing Session: value identified by the Flow Activity Traversal Subprocess; Exception: exception identified by the Flow Activity Traversal Subprocess)", logParent);

		returnValue = new Sequencer_FlowSubprocessResult(flowActivityTraversalResult.NextActivity, flowActivityTraversalResult.Deliverable, flowActivityTraversalResult.Exception, flowActivityTraversalResult.ExceptionText, flowActivityTraversalResult.EndSequencingSession);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}

}


function Sequencer_FlowSubprocessResult(identifiedActivity, deliverable, exception, exceptionText, endSequencingSession){

	Debug.AssertError("Invalid endSequencingSession (" + endSequencingSession + ") passed to FlowSubprocessResult.", (endSequencingSession != true && endSequencingSession != false))

	this.IdentifiedActivity = identifiedActivity;
	this.Deliverable = deliverable;	
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	this.EndSequencingSession = endSequencingSession;
}

Sequencer_FlowSubprocessResult.prototype.toString =  function(){
									return "IdentifiedActivity=" + this.IdentifiedActivity + 
										", Deliverable=" + this.Deliverable + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText +
										", EndSequencingSession=" + this.EndSequencingSession;
									};