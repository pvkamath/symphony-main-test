//****************************************************************************************************************

//Continue Sequencing Request Process [SB.2.7] 
	//May return a delivery request; may return an exception code
	//Reference: 
		//Current Activity AM.1.2
		//Flow Subprocess SB.2.3 
		
function Sequencer_ContinueSequencingRequestProcess(callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Continue Sequencing Request Process [SB.2.7]", callingLog);
	
	var returnValue;
	
	this.LogSeq("[SB.2.7]1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)", logParent);
	
	if ( ! this.IsCurrentActivityDefined(logParent)){
		
		this.LogSeq("[SB.2.7]1.1. Exit Continue Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.7-1) (Nothing to deliver)", logParent);
		
		returnValue = new Sequencer_ContinueSequencingRequestProcessResult(null, "SB.2.7-1", IntegrationInterface.GetString("The sequencing session has not begun yet."), false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	var currentActivity = this.GetCurrentActivity();
	
	this.LogSeq("[SB.2.7]2. If the activity is not the root activity of the activity tree Then", logParent);
	if ( ! currentActivity.IsTheRoot()){
		
		var parentActivity = this.Activities.GetParentActivity(currentActivity);
	
		this.LogSeq("[SB.2.7]2.1. If Sequencing Control Flow for the parent of the activity is False Then (Confirm a 'flow' traversal is allowed from the activity)", logParent);
		
		if (parentActivity.GetSequencingControlFlow() === false){
			this.LogSeq("[SB.2.7]2.1.1. Exit Flow Tree Traversal Subprocess (Delivery Request: n/a; Exception: SB.2.7-2)", logParent);
			
			this.LogSeqSimple("Flow navigation is not allowed within \"" + parentActivity + "\". Prompting the used to make a different selection.", simpleLogParent);
			
			returnValue = new Sequencer_ContinueSequencingRequestProcessResult(null, "SB.2.7-2", IntegrationImplementation.GetString("You cannot use 'Next' to enter {0}. Please select a menu item to continue.", parentActivity.GetTitle()), false);
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
	}
	
	this.LogSeq("[SB.2.7]3. Apply the Flow Subprocess to the Current Activity in the Forward direction with consider children equal to False (Flow in a forward direction to the next allowed activity)", logParent);
	
	var flowSubProcessResult = this.FlowSubprocess(currentActivity, FLOW_DIRECTION_FORWARD, false, logParent, simpleLogParent);
	
	this.LogSeq("[SB.2.7]4. If the Flow Subprocess returns False Then", logParent);
	
	if (flowSubProcessResult.Deliverable === false){
		this.LogSeq("[SB.2.7]4.1. Exit Continue Sequencing Request Process (Delivery Request: n/a; End Sequencing Session: as identified by the Flow Subprocess; Exception: the exception identified by the Flow Subprocess) (Nothing to deliver)", logParent);
		
		returnValue = new Sequencer_ContinueSequencingRequestProcessResult(null, flowSubProcessResult.Exception, flowSubProcessResult.ExceptionText, flowSubProcessResult.EndSequencingSession);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	else{
	
		this.LogSeq("[SB.2.7]5. Else", logParent);
		this.LogSeq("[SB.2.7]5.1. Exit Continue Sequencing Request Process (Delivery Request: the activity identified by the Flow Subprocess; Exception: n/a )", logParent);
		
		returnValue = new Sequencer_ContinueSequencingRequestProcessResult(flowSubProcessResult.IdentifiedActivity, null, "", false);
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}

}


function Sequencer_ContinueSequencingRequestProcessResult(deliveryRequest, exception, exceptionText, endSequencingSession){
	
	Debug.AssertError("Invalid endSequencingSession (" + endSequencingSession + ") passed to ContinueSequencingRequestProcessResult.", (endSequencingSession != true && endSequencingSession != false))
		
	this.DeliveryRequest = deliveryRequest;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	this.EndSequencingSession = endSequencingSession;
}

Sequencer_ContinueSequencingRequestProcessResult.prototype.toString = function(){
									return "DeliveryRequest=" + this.DeliveryRequest + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText + 
										", EndSequencingSession=" + this.EndSequencingSession;
									};