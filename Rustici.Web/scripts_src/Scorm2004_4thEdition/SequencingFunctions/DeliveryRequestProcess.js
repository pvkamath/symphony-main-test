//****************************************************************************************************************

//Delivery Request Process [DB.1.1]
	//For a delivery request; returns the validity of the delivery request; may return an exception code
	//Reference: 
		//Check Activity Process UP.5 

function Sequencer_DeliveryRequestProcess(deliveryRequest, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Delivery Request Process [DB.1.1](" + deliveryRequest + ")", callingLog);
	
	var checkActivityResult;
	var returnValue;
		
	this.LogSeq("[DB.1.1]1. If the activity specified by the delivery request (" + deliveryRequest + ") is not a leaf Then - Can only deliver leaf activities", logParent);
	
	if (! deliveryRequest.IsALeaf()){
		
		this.LogSeq("[DB.1.1]1.1. Exit Delivery Request Process (Delivery Request: Not Valid; Exception: DB.1.1-1)", logParent);
		
		returnValue = new Sequencer_DeliveryRequestProcessResult(false, "DB.1.1-1", IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'", deliveryRequest.GetTitle()));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[DB.1.1]2. Form the activity path as the ordered series of activities from the root of the activity tree to the activity specified in the delivery request, inclusive", logParent);
	var aryParents = this.GetActivityPath(deliveryRequest, true);
	
	this.LogSeq("[DB.1.1]3. If the activity path is Empty Then - Nothing to deliver", logParent);
	if (aryParents.length === 0){
		
		this.LogSeq("[DB.1.1]3.1. Exit Delivery Request Process (Delivery Request: Not Valid; Exception: DB.1.1-2)", logParent);
		
		returnValue = new Sequencer_DeliveryRequestProcessResult(false, "DB.1.1-2", IntegrationImplementation.GetString("Nothing to open"));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;
	}
	
	this.LogSeq("[DB.1.1]4. For each activity in the activity path - Make sure each activity along the path is allowed", logParent);
	for (var i=0; i < aryParents.length; i++){
		
		this.LogSeq("[DB.1.1]4.1. Apply the Check Activity Process to the activity - " + aryParents[i], logParent);
		checkActivityResult = this.CheckActivityProcess(aryParents[i], logParent, simpleLogParent);
		
		this.LogSeq("[DB.1.1]4.2. If the Check Activity Process return True Then", logParent);
		
		if (checkActivityResult === true){
			this.LogSeq("[DB.1.1]4.2.1. Exit Delivery Request Process (Delivery Request: Not Valid; Exception: DB.1.1-3)", logParent);
			
			returnValue = new Sequencer_DeliveryRequestProcessResult(false, "DB.1.1-3", IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'", deliveryRequest.GetTitle()));
			this.LogSeqReturn(returnValue, logParent);
			return returnValue;
		}
	}
	
	this.LogSeq("[DB.1.1]5. Exit Delivery Request Process (Delivery Request: Valid; Exception: n/a)", logParent);
	
	returnValue = new Sequencer_DeliveryRequestProcessResult(true, null, "");
	this.LogSeqReturn(returnValue, logParent);
	return returnValue;
}

function Sequencer_DeliveryRequestProcessResult(valid, exception, exceptionText){
	this.Valid = valid;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
	
}

Sequencer_DeliveryRequestProcessResult.prototype.toString =  function(){
									return "Valid=" + this.Valid + 
										", Exception=" + this.Exception + 
										", ExceptionText=" + this.ExceptionText;
									};