//****************************************************************************************************************

//Evaluate Rollup Conditions Subprocess [RB.1.4.1] 

//	For an activity and a set of Rollup Conditions, True if the condition(s) evaluate to True, False if the condition(s) evaluate to False, and Unknown if condition(s) cannot be evaluated

//	Reference: 
		//Condition Combination SM.5; 
		//Rollup Condition SM.5; 
		//Rollup Condition Operator SM.5; 
		//Tracking Model TM 


function Sequencer_EvaluateRollupConditionsSubprocess(activity, rollupRule, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	//returns true, false, RESULT_UNKNOWN
	
	var logParent = this.LogSeqAudit("Evaluate Rollup Conditions Subprocess [RB.1.4.1](" + activity + ", " + rollupRule + ")", callingLog);
	
	var conditionResult;
	var combinedConditionResults;
	
	this.LogSeq("[RB.1.4.1]1.Initialize rollup condition bag as an Empty collection (This is used track of the evaluation rule's conditions)", logParent);
	var conditionResults = new Array();
	
	var i;
	
	this.LogSeq("[RB.1.4.1]2. For each Rollup Condition in the set of Rollup Conditions", logParent);
	for (i=0; i < rollupRule.Conditions.length; i++){
	
		this.LogSeq("[RB.1.4.1]2.1. Evaluate the rollup condition by applying the appropriate tracking information for the activity to the Rollup Condition. (Evaluate each condition against the activity's tracking information. This evaluation may result in 'unknown'.)", logParent);
		conditionResult = this.EvaluateRollupRuleCondition(activity, rollupRule.Conditions[i], simpleLogParent);
		
		this.LogSeq("[RB.1.4.1]2.2. If the Rollup Condition Operator for the Rollup Condition is Not Then (Negating 'unknown' results in 'unknown')", logParent);
		if (rollupRule.Conditions[i].Operator == RULE_CONDITION_OPERATOR_NOT && conditionResult != RESULT_UNKNOWN){
			
			this.LogSeq("[RB.1.4.1]2.2.1. Negate the rollup condition", logParent);
			conditionResult = (!conditionResult);
		}
		
		this.LogSeq("[RB.1.4.1]2.3. Add the value of the rollup condition (" + conditionResult + ") to the rollup condition bag (Add the evaluation of this condition the set of evaluated conditions)", logParent);
		conditionResults[conditionResults.length] = conditionResult;
	}
	
	
	this.LogSeq("[RB.1.4.1]3. If the rollup condition bag is Empty Then (If there are no defined conditions for the rule, cannot determine the rule applies)", logParent);
	if (conditionResults.length === 0){
		
		this.LogSeq("[RB.1.4.1]3.1. Exit Evaluate Rollup Conditions Subprocess (Evaluation: Unknown)", logParent);
		return RESULT_UNKNOWN;
	}
	
	this.LogSeq("[RB.1.4.1]4. Apply the Condition Combination (" + rollupRule.ConditionCombination + ") to the rollup condition bag to produce a single combined rule evaluation ('And' or 'Or' set of evaluated conditions, on the rollup definition)", logParent);

	if (rollupRule.ConditionCombination == RULE_CONDITION_COMBINATION_ANY){	//ANY = OR
		
		combinedConditionResults = false;
		
		for (i=0; i < conditionResults.length; i++){
			combinedConditionResults = Sequencer_LogicalOR(combinedConditionResults, conditionResults[i]);
		}
		
		if (rollupRule.Conditions.length > 1){
	        this.LogSeqSimple("Are ANY of the conditions true=" + combinedConditionResults, simpleLogParent);
	    }	
	}

	else{	//ALL = AND
	
		combinedConditionResults = true;
	
		for (i=0; i < conditionResults.length; i++){
			combinedConditionResults = Sequencer_LogicalAND(combinedConditionResults, conditionResults[i]);
		}
		
		if (rollupRule.Conditions.length > 1){
	        this.LogSeqSimple("Are ALL of the conditions true=" + combinedConditionResults, simpleLogParent);
	    }	
			
	}	
	
	
	
	this.LogSeq("[RB.1.4.1]5. Exit Evaluate Rollup Conditions Subprocess (Evaluation: the value of combined rule evaluation)", logParent);
	this.LogSeqReturn(combinedConditionResults, logParent);
	return combinedConditionResults;
}


function Sequencer_EvaluateRollupRuleCondition(activity, ruleCondition, simpleLogParent){

	//returns true, false or RESULT_UNKNOWN
	var returnValue = null;
	
	switch (ruleCondition.Condition){

		case ROLLUP_RULE_CONDITION_SATISFIED:
			returnValue = activity.IsSatisfied("");
			
			this.LogSeqSimple("\"" + activity + "\" satisfied = " + returnValue, simpleLogParent);
		break;

		case ROLLUP_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN:
			returnValue = activity.IsObjectiveStatusKnown("", false);
			
			this.LogSeqSimple("\"" + activity + "\" objective status known = " + returnValue, simpleLogParent);
		break;

		case ROLLUP_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN:
			returnValue = activity.IsObjectiveMeasureKnown("", false);
			
			this.LogSeqSimple("\"" + activity + "\" objective measure known = " + returnValue, simpleLogParent);
		break;

		case ROLLUP_RULE_CONDITION_COMPLETED:
			returnValue = activity.IsCompleted("", false);
			
			this.LogSeqSimple("\"" + activity + "\" completed = " + returnValue, simpleLogParent);
		break;

		case ROLLUP_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN:
			returnValue = activity.IsActivityProgressKnown("", false);
			
			this.LogSeqSimple("\"" + activity + "\" activity progress known = " + returnValue, simpleLogParent);
		break;

		case ROLLUP_RULE_CONDITION_ATTEMPTED:
			returnValue = activity.IsAttempted();
			
			this.LogSeqSimple("\"" + activity + "\" attempted = " + returnValue, simpleLogParent);
		break;

		case ROLLUP_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED:
			returnValue = activity.IsAttemptLimitExceeded();
			
			this.LogSeqSimple("\"" + activity + "\" attempt limit exceeded = " + returnValue, simpleLogParent);
		break;

		case ROLLUP_RULE_CONDITION_NEVER:
			returnValue = false;
			
			this.LogSeqSimple("\"" + activity + "\": 'never' condition always equals false", simpleLogParent);
		break;

		default:
			this.LogSeq("ERROR - invalid rollup condition", logParent);
		break;

	}
	return returnValue;
}