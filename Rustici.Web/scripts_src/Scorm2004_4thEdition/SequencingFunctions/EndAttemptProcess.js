//****************************************************************************************************************

//End Attempt Process [UP.4] 
	//For an activity
	//Reference: 
		//Activity is Active AM.1.1
		//Activity is Suspended AM.1.1
		//Attempt Completion Status TM.1.2.2
		//Attempt Progress Status TM.1.2.2
		//Completion Set by Content SM.11
		//Objective Contributes to Rollup SM.6
		//Objective Progress Status TM.1.1
		//Objective Satisfied Status TM.1.1
		//Objective Set by Content SM.11
		//Tracked SM.11
		//Overall Rollup Process RB.1.4 


function Sequencer_EndAttemptProcess(activity, delayRollup, callingLog, isSuspendAll, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	if (isSuspendAll === undefined || isSuspendAll === null) {
		isSuspendAll = false;
	}
	
	var logParent = this.LogSeqAudit("End Attempt Process [UP.4](" + activity + ", " + delayRollup + ")", callingLog);
	
	simpleLogParent = this.LogSeqSimpleAudit("Ending the current attempt on \"" + activity + "\".", simpleLogParent);
	
	this.LogSeq("[UP.4]1. If the activity (" + activity.GetItemIdentifier() + ") is a leaf Then", logParent);
	
	var i;
	var rollupSet = new Array();
	
	if (activity.IsALeaf()){
	
		this.LogSeq("[UP.4]1.1. If Tracked for the activity is True Then", logParent);
		
		// The inclusion of activity.WasLaunchedThisSession() is an extension to the standard pseudo-code. This setting indicates whether
		// the activity was actually presented to the user in the browser.  This code was put into place to address the specific use case
		// when a course is delivered with the package setting "launch in new window after click".  If the user never clicked to open the
		// SCO or asset, it was STILL MARKED AS COMPLETE by the code below.  With this change, we will insure that the user had actually
		// "clicked" to view the course.
	
		if (activity.IsTracked() && activity.WasLaunchedThisSession()){
		
			this.LogSeq("[UP.4]1.1.05 Transfer data from the RTE to the sequencing engine", logParent);
			this.LogSeqSimple("Transferring run-time data to the activity tree.", simpleLogParent);
			activity.TransferRteDataToActivity();
			
			this.LogSeq("[UP.4]1.1.1.  If the Activity is Suspended for the activity is False Then (The sequencer will not affect the state of suspended activities)", logParent);
			if (activity.IsSuspended() === false){
			
				this.LogSeq("[UP.4]1.1.1.1. If the Completion Set by Content for the activity is False Then (Should the sequencer set the completion status of the activity? )", logParent);
				if (activity.IsCompletionSetByContent() === false){
					
					this.LogSeq("[UP.4]1.1.1.1.1. If the Attempt Progress Status for the activity is False  And Completion Status was not changed during runtime  Then (Did the content inform the sequencer of the activity's completion status?)", logParent);
					if (activity.GetAttemptProgressStatus() === false && activity.GetCompletionStatusChangedDuringRuntime() === false){
						
						this.LogSeqSimple("The activity did not set completion status and indicates that it will not so automatically marking the activity as completed.", simpleLogParent);
						
						this.LogSeq("[UP.4]1.1.1.1.1.1. Set the Attempt Progress Status for the activity to True", logParent);
						activity.SetAttemptProgressStatus(true);
						
						this.LogSeq("[UP.4]1.1.1.1.1.2. Set the Attempt Completion Status for the activity to True", logParent);
						activity.SetAttemptCompletionStatus(true);
						
						activity.WasAutoCompleted = true;
					}
				}
				
				this.LogSeq("[UP.4]1.1.1.2.  If the Objective Set by Content for the activity is False Then (Should the sequencer set the objective status of the activity?)", logParent);
				if (activity.IsObjectiveSetByContent() === false){
					
					this.LogSeq("[UP.4]1.1.1.2.1. Get the primary objective (For all objectives associated with the activity)", logParent);
					var primaryObjective = activity.GetPrimaryObjective();
					
					this.LogSeq("[UP.4]1.1.1.2.1.1.1. If the Objective Progress Status for the objective is False And Success Status was not changed during runtime Then (Did the content inform the sequencer of the activity's rolled-up objective status?)", logParent);
					//MR 7/13/09 - in 4th Edition, changing the status of the runtime obj that maps to the primary obj counts as changing the status and we should not overwrite that value with auto-satisfy
					if (primaryObjective.GetProgressStatus(activity, false) === false && 
					    activity.GetSuccessStatusChangedDuringRuntime() === false &&
					    primaryObjective.GetSuccessStatusChangedDuringRuntime(activity) === false){
					    
					    this.LogSeqSimple("The activity did not set satisfaction status and indicates that it will not so automatically marking the activity as satisfied.", simpleLogParent);
					    
						this.LogSeq("[UP.4]1.1.1.2.1.1.1.1. Set the Objective Progress Status for the objective to True", logParent);
						primaryObjective.SetProgressStatus(true,false, activity, true, true);
						
						this.LogSeq("[UP.4]1.1.1.2.1.1.1.2. Set the Objective Satisfied Status for the objective to True", logParent);
						primaryObjective.SetSatisfiedStatus(true, false, activity);
						
						activity.WasAutoSatisfied = true;
					}
				}
				
			}
		}
	}
	else{
		this.LogSeq("[UP.4]2. Else (The activity has children)", logParent);
		
		this.LogSeq("[UP.4]2.1. If the activity includes any child activity whose Activity is Suspended attribute is True Then (The suspended status of the parent is dependent on the suspended status of its children)", logParent);
		
		if (this.ActivityHasSuspendedChildren(activity, logParent)){
			this.LogSeq("[UP.4]2.1.1. Set the Activity is Suspended for the activity to True", logParent);
			activity.SetSuspended(true);
			
			this.LogSeqSimple("\"" + activity + "\" has suspend children and will be marked as suspended.", simpleLogParent);
		}
		else{
			this.LogSeq("[UP.4]2.2. Else", logParent);
			this.LogSeq("[UP.4]2.2.1. Set the Activity is Suspended for the activity to False", logParent);
			activity.SetSuspended(false);
			
			this.LogSeqSimple("\"" + activity + "\" does not have suspend children and will not be marked as suspended.", simpleLogParent);
		}
	}
	
	if (isSuspendAll === false) {
		this.LogSeq("[UP.4]3. Set the Activity is Active for the activity to False (The current attempt on this the activity has ended)", logParent);
		activity.SetActive(false);
	}
	
	var aryRolledUpActivities;
	
	if (delayRollup === false){

		this.LogSeq("[UP.4]4. Apply the Overall Rollup Process to the activity (Ensure that any status change to this activity is propagated through the entire activity tree)", logParent);
		aryRolledUpActivities = this.OverallRollupProcess(activity, logParent, simpleLogParent);
	}
	else{
		this.LogSeq("[UP.4]4.a. Performance Optimization - Deferring rollup to parent process to ensure minimal rollup set.", logParent);
		rollupSet[0] = activity;
	}
	
	
	//only apply the overall rollup process to the parents of the affected activities
	//the overall rollup process looks to the children of the activity it is called on to aggregate their information into the activity
	//since the global objective did not affect these children, reading them is not necessary and can even be detremental
	//if the children's status's roll up to a status different than that set by the global objective, then the overall rollup process
	//would actually overwrite the status just set by the global objective	
	this.LogSeq("[UP.4]4.5 Find all the activities that read the global objectives written by this activity and invoke the overall rollup process on them (not in pseudo code)", logParent);
	var activitiesAffectedByWriteMaps = this.FindActivitiesAffectedByWriteMaps(activity);
	var affectedParentActivities = this.FindDistinctParentsOfActivitySet(activitiesAffectedByWriteMaps);
	
	if (delayRollup === false){
		this.LogSeq("[UP.4]4.b. Apply the Overall Rollup Process to the parents of activities affected by write maps (not in pseudo code)", logParent);
		var distinctAffectedParentActivities = this.GetMinimalSubsetOfActivitiesToRollup(affectedParentActivities, aryRolledUpActivities);
		
		if(distinctAffectedParentActivities.length > 0){
		    this.LogSeqSimple("Invoking rollup for activities that are affected by write maps.", simpleLogParent)
		}
		
		for (i=0; i < distinctAffectedParentActivities.length; i++){

			if (distinctAffectedParentActivities[i] !== null){
			    
				this.OverallRollupProcess(distinctAffectedParentActivities[i], logParent, simpleLogParent);
			}
		}
	}
	else{
		this.LogSeq("[UP.4]4.c. Performance Optimization - Deferring rollup or activities affected by write maps to parent process to ensure minimal rollup set. (not in pseudo code)", logParent);
		rollupSet = rollupSet.concat(affectedParentActivities);
	}
	
	//only bother to randomize on the regular sequencer for efficiency and to avoid errors with the randomization process trying to affect the displayed menu items
	if (this.LookAhead === false && isSuspendAll === false){
		//re-randomize the children if appropriate
		//do this on the way out for several reasons:
			//-if we did it on the way in, it would look wierd if I was expecting to go to SCO 3, then randomization made SCO 4 first
			//-we do an initial randomization when the course loads
			//-the most logical place for re-randomization on the start of an activity (ContentDeliveryEnvironmentProcess) 
			//	has already identified an activity for delivery before the randomization could occur
		this.RandomizeChildrenProcess(activity, true, logParent, simpleLogParent);
	}
	
	this.LogSeq("[UP.4]5. Exit End Attempt Subprocess", logParent);
	this.LogSeqReturn("", logParent);
 	return rollupSet;
}

