//****************************************************************************************************************
//Terminate Descendent Attempts Process [UP.3] 
	//For an activity
	//Reference: 
		//Current Activity AM.1.2
		//End Attempt Process UP.4 
	
function Sequencer_TerminateDescendentAttemptsProcess(activity, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Terminate Descendent Attempts Process [UP.3](" + activity + ")", callingLog);
	
	simpleLogParent = this.LogSeqSimpleAudit("Ending all attempts underneath \"" + activity + "\".", simpleLogParent);
	
	this.LogSeq("[UP.3]1. Find the activity that is the common ancestor of the Current Activity (" + this.GetCurrentActivity() + ") and the identified activity (" + activity + ")", logParent);
	var commonAncestor = this.FindCommonAncestor(activity, this.GetCurrentActivity(), logParent);
	
	this.LogSeq("[UP.3]2. Form the activity path as the ordered series of activities from the Current Activity to the common ancestor, exclusive of the Current Activity and the common ancestor (" + commonAncestor + ") (The current activity must have already been exited)", logParent);
	var aryParents = this.GetPathToAncestorExclusive(this.GetCurrentActivity(), commonAncestor, false);
	
	var rollupSet = new Array();
	
	this.LogSeq("[UP.3]3. If the activity path is Not Empty Then (There are some activities that need to be terminated)", logParent);
	if (aryParents.length > 0){
	
		this.LogSeq("[UP.3]3.1. For each activity in the activity path", logParent);
		
		for (var i=0; i < aryParents.length; i++){
			
			this.LogSeq("[UP.3]3.1.1. Apply the End Attempt Process to the activity (End the current attempt on each activity) Activity-" + aryParents[i].LearningObject.ItemIdentifier, logParent);
			rollupSet = rollupSet.concat(this.EndAttemptProcess(aryParents[i], true, logParent, false, simpleLogParent));
			
		}
	}
	
	//as a performance optimization, we'll rollup any activities that we terminated here instead of in end attempt...this prevents parent activities from being rollup up twice
	this.LogSeq("[UP.3]3.a Rollup the rollup set accumulated during the EndAttempt processes", logParent);
	var distinctRollupActivities = this.GetMinimalSubsetOfActivitiesToRollup(rollupSet, null);
	
	if (distinctRollupActivities.length > 0){
	    this.LogSeqSimple("Invoking rollup on all activities that were just ended.", simpleLogParent);
	}
	
	for (var rollupActivity in distinctRollupActivities){
		this.OverallRollupProcess(distinctRollupActivities[rollupActivity], logParent, simpleLogParent);
	}
	
	
	this.LogSeq("[UP.3]4. Exit Terminate Descendent Attempts Process", logParent);
	this.LogSeqReturn("", logParent);
	return;
}
