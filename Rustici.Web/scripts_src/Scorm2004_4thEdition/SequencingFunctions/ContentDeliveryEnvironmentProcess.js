//****************************************************************************************************************

//Content Delivery Environment Process [DB.2]
	//For a delivery request; may return an exception code
	//Reference: 
		//Activity Progress Status TM.1.2.1
		//Activity Attempt Count TM.1.2.1
		//Activity is Active AM.1.1
		//Activity is Suspended AM.1.1
		//Attempt Absolute Duration TM.1.2.2
		//Attempt Experienced Duration TM.1.2.2
		//Attempt Progress Information TM.1.2.2
		//Clear Suspended Activity Subprocess DB.2.1
		//Current Activity AM.1.2
		//Objective Progress Information TM.1.1
		//Suspended Activity AM.1.2
		//Terminate Descendent Attempts Process UP.4
		//Tracked SM.11 

function Sequencer_ContentDeliveryEnvironmentProcess(activityForDelivery, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Content Delivery Environment Process [DB.2](" + activityForDelivery + ")", callingLog);
	
	var isLaunchAfterClick = (Control.Package.Properties.ScoLaunchType === LAUNCH_TYPE_POPUP_AFTER_CLICK || 
                              Control.Package.Properties.ScoLaunchType === LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR);
	
	var currentActivity = this.GetCurrentActivity();
	var suspendedActivity = this.GetSuspendedActivity(logParent);
	var rootActivity = this.GetRootActivity(logParent);
	
	var activityPath = null;
	
	var returnValue;
	
	this.LogSeq("[DB.2]1.If the Activity is Active for the Current Activity is True Then (If the attempt on the current activity has not been terminated, we cannot deliver new content)", logParent);
	if (currentActivity !== null && currentActivity.IsActive()){
		this.LogSeq("DB.2]1.1. Exit Content Delivery Environment Process (Exception: DB.2-1) (Delivery request is invalid - The Current Activity has not been terminated)", logParent);
		
		returnValue = new Sequencer_ContentDeliveryEnvironmentProcessResult(false, "DB.2.1", IntegrationImplementation.GetString("The previous activity must be terminated before a new activity may be attempted"));
		this.LogSeqReturn(returnValue, logParent);
		return returnValue;		
	}
	
	// JMH - 01/22/07 - If launch after click, the PopupLauncher will make this call, otherwise call here as usual.
	if (!isLaunchAfterClick) {
		this.ContentDeliveryEnvironmentActivityDataSubProcess(activityForDelivery, callingLog);
	} else {
		this.LogSeq("Skipping ContentDeliveryEnvironmentActivityDataSubProcess because content is LAUNCH AFTER CLICK.  This method will get called when activity is actually viewed", logParent);
	}
	
	this.LogSeq("[DB.2]4.Form the activity path as the ordered series of activities from the root of the activity tree to the activity identified for delivery, inclusive (Begin all attempts required to deliver the identified activity)", logParent);
	activityPath = this.GetPathToAncestorInclusive(activityForDelivery, rootActivity);

	this.LogSeq("[DB.2]5. For each activity in the activity path", logParent);
	for (var i = (activityPath.length - 1); i >= 0; i--){
		
		if (isLaunchAfterClick) {
			activityPath[i].WasActiveBeforeLaunchOnClick = activityPath[i].IsActive();
		}
		
		this.LogSeq("[DB.2]5.1. If Activity (" + activityPath[i] + ") is Active for the activity is False Then", logParent);
		if (activityPath[i].IsActive() === false){
			
			this.LogSeq("[DB.2]5.1.2. Set Activity is Active for the activity to True", logParent);
			activityPath[i].SetActive(true);
		}
	}
	
	this.LogSeq("[DB.2]6. The activity identified for delivery becomes the current activity Set Current Activity to the activity identified for delivery. Identified Activity=" + activityForDelivery.GetItemIdentifier(), logParent);
	
	if (isLaunchAfterClick) {
		this.PreviousActivity = currentActivity;
	}
	this.SetCurrentActivity(activityForDelivery, logParent, simpleLogParent);
	
	this.LogSeq("[DB.2]7. Set Suspended Activity to undefined", logParent);
	this.SetSuspendedActivity(null);
	
	//nothing to do here, 8.1.1 happens in activity.TransferRteDataToActivity, we're not tracking durations
	//[DB.2]8. 	Once the delivery of the activity's content resources and auxiliary resources begins (The delivery environment is assumed to deliver the content resources associated with the identified activity. While the activity is assumed to be active, the sequencer may track learner status )	
	//[DB.2]8.1.	If Tracked for the activity identified for delivery is False Then 
	//[DB.2]8.1.1. 		The Objective and Attempt Progress information for the activity should not be recorded during delivery 
	//[DB.2]8.1.2. 		The delivery environment begins tracking the Attempt Absolute Duration and the Attempt Experienced Duration 
	//[DB.2]		End If 
	
	this.LogSeq("[DB.2]9. Exit Content Delivery Environment Process (Exception: n/a)", logParent);
	returnValue = new Sequencer_ContentDeliveryEnvironmentProcessResult(true, null, "");
	this.LogSeqReturn(returnValue, logParent);
	
	this.LogSeqSimple("Delivering activity \"" + activityForDelivery + "\".", simpleLogParent);
	
	//put this call last (after the LogSeqReturn) to keep it from being included in the time tracking (relative to code execution, it's a time hog)
	Control.DeliverActivity(activityForDelivery);
	
	return returnValue;
	
}

function Sequencer_ContentDeliveryEnvironmentProcessResult(valid, exception, exceptionText){
	this.Valid = valid;
	this.Exception = exception;
	this.ExceptionText = exceptionText;
}

Sequencer_ContentDeliveryEnvironmentProcessResult.prototype.toString = function(){
								return "Valid=" + this.Valid + 
									", Exception=" + this.Exception + 
									", ExceptionText=" + this.ExceptionText;
								};
