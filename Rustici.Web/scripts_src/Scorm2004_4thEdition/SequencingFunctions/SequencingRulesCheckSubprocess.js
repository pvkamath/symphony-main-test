//****************************************************************************************************************
//Sequencing Rule Check Subprocess [UP.2.1] 
	//For an activity and a Sequencing Rule; returns True if the rule applies, False if the rule does not apply, and Unknown if the condition(s) cannot be evaluated
	//Reference: 
		//Rule Combination SM.2
		//Rule Condition SM.2
		//Rule Condition Operator SM.2
		//Sequencing Rule Description SM.2
		//Tracking Model TM 

function Sequencer_SequencingRulesCheckSubprocess(activity, rule, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	//returns true, false, or RESULT_UNKNOWN 
	var logParent = this.LogSeqAudit("Sequencing Rules Check Subprocess [UP.2.1](" + activity + ", " + rule + ")", callingLog);

	this.LogSeq("[UP.2.1]1. Initialize rule condition bag as an Empty collection (This is used to keep track of the evaluation of the rule's conditions)", logParent);
	var ruleConditionResults = new Array();
	
	var conditionResult;
	var combinedConditionResults;
	
	var i;
	
	this.LogSeq("[UP.2.1]2. For each Rule Condition for the Sequencing Rule for the activity", logParent);
	for (i=0; i < rule.RuleConditions.length; i++){
		
		this.LogSeq("[UP.2.1]2.1. Evaluate the rule condition by applying the appropriate tracking information for the activity to the Rule Condition (Evaluate each condition against the activity's tracking information)", logParent);
		
		conditionResult = this.EvaluateSequencingRuleCondition(activity, rule.RuleConditions[i], logParent, simpleLogParent);		//store true, false or RESULT_UNKNOWN
		
		this.LogSeq("[UP.2.1]2.2. If the Rule Condition Operator for the Rule Condition is Not Then", logParent);
		
		if (rule.RuleConditions[i].Operator == RULE_CONDITION_OPERATOR_NOT){
			
			this.LogSeq("[UP.2.1]2.2.1. Negate the rule condition (Negating 'unknown' results in 'unknown')", logParent);
			
			if (conditionResult != "unknown"){
				conditionResult = (! conditionResult);
			}
		}
		
		this.LogSeq("[UP.2.1]2.3. Add the value of rule condition to the rule condition bag (Add the evaluation of this condition to the set of evaluated conditions)", logParent);
		ruleConditionResults[ruleConditionResults.length] = conditionResult;
	}
	
	this.LogSeq("[UP.2.1]3. If the rule condition bag is Empty Then (If there are no defined conditions for the rule, the rule does not apply)", logParent);
	
	if (ruleConditionResults.length === 0){
		
		this.LogSeq("[UP.2.1]3.1. Exit Sequencing Rule Check Subprocess (Result: Unknown) (No rule conditions)", logParent);
		this.LogSeqReturn(RESULT_UNKNOWN, logParent);
		return RESULT_UNKNOWN;
	}
	
	this.LogSeq("[UP.2.1]4. Apply the Rule Combination for the Sequencing Rule to the rule condition bag to produce a single combined rule evaluation ('And' or 'Or' the set of evaluated conditions, based on the sequencing rule definition)", logParent);

	if (rule.ConditionCombination == RULE_CONDITION_COMBINATION_ANY){	//ANY = OR
		
		combinedConditionResults = false;
		
		for (i=0; i < ruleConditionResults.length; i++){
			combinedConditionResults = Sequencer_LogicalOR(combinedConditionResults, ruleConditionResults[i]);
		}
		
		if (rule.RuleConditions.length > 1){
	        this.LogSeqSimple("Are ANY of the conditions true=" + combinedConditionResults, simpleLogParent);
	    }	
	}

	else{	//ALL = AND
	
		combinedConditionResults = true;
	
		for (i=0; i < ruleConditionResults.length; i++){
			combinedConditionResults = Sequencer_LogicalAND(combinedConditionResults, ruleConditionResults[i]);
		}
		
		if (rule.RuleConditions.length > 1){
	        this.LogSeqSimple("Are ALL of the conditions true=" + combinedConditionResults, simpleLogParent);
	    }
	}	

	this.LogSeq("[UP.2.1]5. Exit Sequencing Rule Check Subprocess (Result: the value of rule evaluation) ", logParent);
	this.LogSeqReturn(combinedConditionResults, logParent);
	
	if (combinedConditionResults === true){
	    this.LogSeqSimpleReturn("True, rule will fire", simpleLogParent);
	}
	else if (combinedConditionResults === false){
	    this.LogSeqSimpleReturn("False, rule will not fire", simpleLogParent);
    }
    else{
        this.LogSeqSimpleReturn("Unknown, rule will not fire", simpleLogParent);
    }
    
	return combinedConditionResults;
	
}


function Sequencer_EvaluateSequencingRuleCondition(activity, ruleCondition, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Evaluate Sequencing Rule Condition(" + activity + ", " + ruleCondition + ")", callingLog);
	
	//return true, false or RESULT_UNKNOWN
	var returnValue = null;
	switch (ruleCondition.Condition){
		
		case SEQUENCING_RULE_CONDITION_SATISFIED:
			returnValue = activity.IsSatisfied(ruleCondition.ReferencedObjective, true);
			
			this.LogSeqSimple(activity + "\" satisfied = " + returnValue, simpleLogParent);
		break;
		
		case SEQUENCING_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN:
			returnValue = activity.IsObjectiveStatusKnown(ruleCondition.ReferencedObjective, true);
			
			this.LogSeqSimple("\"" + activity + "\" objective status known = " + returnValue, simpleLogParent);
		break;
		
		case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN:
			returnValue = activity.IsObjectiveMeasureKnown(ruleCondition.ReferencedObjective, true);
			
			this.LogSeqSimple("\"" + activity + "\" objective measure known = " + returnValue, simpleLogParent);
		break;
		
		case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_GREATER_THAN:
			returnValue = activity.IsObjectiveMeasureGreaterThan(ruleCondition.ReferencedObjective, ruleCondition.MeasureThreshold, true);
			
			this.LogSeqSimple("\"" + activity + "\" objective measure greater than " + ruleCondition.MeasureThreshold + " = " + returnValue, simpleLogParent);
		break;
		
		case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_LESS_THAN:
			returnValue = activity.IsObjectiveMeasureLessThan(ruleCondition.ReferencedObjective, ruleCondition.MeasureThreshold, true);
			
			this.LogSeqSimple("\"" + activity + "\" objective measure less than " + ruleCondition.MeasureThreshold + " = " + returnValue, simpleLogParent);
		break;
		
		case SEQUENCING_RULE_CONDITION_COMPLETED:
			returnValue = activity.IsCompleted(ruleCondition.ReferencedObjective, true);
			
			this.LogSeqSimple("\"" + activity + "\" completed = " + returnValue, simpleLogParent);
		break;
		
		case SEQUENCING_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN:
			returnValue = activity.IsActivityProgressKnown(ruleCondition.ReferencedObjective, true);
			
			this.LogSeqSimple("\"" + activity + "\" activity progress known = " + returnValue, simpleLogParent);
		break;
		
		case SEQUENCING_RULE_CONDITION_ATTEMPTED:
			returnValue = activity.IsAttempted();
			
			this.LogSeqSimple("\"" + activity + "\" attempted = " + returnValue, simpleLogParent);
		break;
		
		case SEQUENCING_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED:
			returnValue = activity.IsAttemptLimitExceeded();
			
			this.LogSeqSimple("\"" + activity + "\" attempt limit exceeded = " + returnValue, simpleLogParent);
		break;
		
		case SEQUENCING_RULE_CONDITION_ALWAYS:
			returnValue = true;
			
			this.LogSeqSimple("\"" + activity + "\" 'always' will always return true.", simpleLogParent);
		break;
		
		default:
			Debug.AssertError("ERROR - Encountered unsupported rule condition - " + ruleCondition);
			returnValue = RESULT_UNKNOWN;
		break;
		
	}
	
	logParent.setReturn(returnValue + "", logParent);
	
	return returnValue;
}


//TODO - move these some place more logical since they are reused from EvaluateRollupConditionsSubprocess
function Sequencer_LogicalOR(value1, value2){
	
	if (value1 == RESULT_UNKNOWN){
		if (value2 === true){
			return true;
		}
		else {	//value 2 === false or unknown
			return RESULT_UNKNOWN;
		}
	}
	else{
		if (value2 == RESULT_UNKNOWN){
			if (value1 === true){
				return true;
			}
			else{
				return RESULT_UNKNOWN;
			}
		}
		else{
			return (value1 || value2);
		}
	}
	
}

function Sequencer_LogicalAND(value1, value2){
	
	if (value1 == RESULT_UNKNOWN){
		if (value2 === false){
			return false;
		}
		else{
			return RESULT_UNKNOWN;
		}
	}
	else{
		if (value2 == RESULT_UNKNOWN){
			if (value1 === false){
				return false;
			}
			else{
				return RESULT_UNKNOWN;
			}
		}
		else{
			return (value1 && value2);
		}
	}
}
