//****************************************************************************************************************

//Objective RollupProcess [RB.1.2] 

//	Reference: 

function Sequencer_ObjectiveRollupProcess(activity, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Objective RollupProcess [RB.1.2](" + activity + ")", callingLog);
	
	simpleLogParent = this.LogSeqSimpleAudit("Rolling up the satisfaction status of \"" + activity + "\".", simpleLogParent);
	
	//select the appropriate objective rollup process using the rules defined on SN-4-40 - this pseudo code is not defined in the spec, just the logic
	
	this.LogSeq("[RB.1.2]1. Set the target objective to Undefined", logParent);
	var targetObjective = null;

	this.LogSeq("[RB.1.2]2. Get the primary objective (For each objective associated with the activity)", logParent);
	var targetObjective = activity.GetPrimaryObjective();
	
	this.LogSeq("[RB.1.2 a]3. If target objective is Defined Then", logParent);
	if (targetObjective !== null){

	
		this.LogSeq("[RB.1.2]3.1. If Objective Satisfied By Measure for the target objective is True", logParent);
		if (targetObjective.GetSatisfiedByMeasure() === true){
			
			this.LogSeq("[RB.1.2]3.1.1 Invoke the Objective Rollup Process Using Measure", logParent);
			this.ObjectiveRollupUsingMeasureProcess(activity, logParent, simpleLogParent);
			
		} else {
    	    this.LogSeq("[RB.1.2]3.2 Else ", logParent);
            
			this.LogSeq("[RB.1.2]3.2.1 Apply Objective Rollup Using Rules Process (The default is included in the Rules process now)", logParent);
			this.ObjectiveRollupUsingRulesProcess(activity, logParent, simpleLogParent);
			
		}
		
		
		this.LogSeq("[RB.1.2]3.3 Exit Objective Roll up Process", logParent);
		this.LogSeqReturn("", logParent);
		return;
	
	}
	
	else{
		this.LogSeq("[RB.1.2 a]4. Else", logParent);
		this.LogSeq("[RB.1.2 a]4.1. Exit Objective Roll up Process (No objective contributes to rollup, so we cannot set anything)", logParent);
		this.LogSeqReturn("", logParent);
		return;
	}
	
}

