//****************************************************************************************************************

//Activity Progress Rollup Using Rules Process [RB.1.3 b] 

//	For an activity; may change the Attempt Information for the activity

//	Reference: 
		//Attempt Completion Status TM.1.2.2; 
		//Attempt Progress Status TM.1.2.2; 
		//Rollup Rule Check Subprocess RB.1.4;
		//Rollup Action SM.5 
		//Rollup Condition SM.5;
		//Rollup Child Activity Set SM.5 



function Sequencer_ActivityProgressRollupUsingRulesProcess(activity, callingLog, simpleLogParent){
	
	Debug.AssertError("Calling log not passed.", (callingLog === undefined || callingLog === null));
	Debug.AssertError("Simple calling log not passed.", (simpleLogParent === undefined || simpleLogParent === null));
	
	var logParent = this.LogSeqAudit("Activity Progress Rollup Using Rules Process [RB.1.3 b](" + activity + ")", callingLog);
	
	this.LogSeq("[RB.1.3 b]1. If the activity does not include Rollup Rules with the Incomplete rollup action And the activity does not include Rollup Rules with the Completed  rollup action Then (If no progress rollup rules are defined, use the default rollup rules.) ", logParent);
	if (Sequencer_GetApplicableSetofRollupRules (activity, RULE_SET_INCOMPLETE).length === 0 &&
	     Sequencer_GetApplicableSetofRollupRules (activity, RULE_SET_COMPLETED).length === 0){

        this.LogSeqSimple("Applying the default set of completion rollup rules to \"" + activity + "\".", simpleLogParent);

        this.LogSeq("[RB.1.3 b]1.1. Apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Completed; and a Rollup Action of Completed (Define the default completed rule)", logParent);
        activity.ApplyRollupRule(new SequencingRollupRule(RULE_CONDITION_COMBINATION_ANY,
                                                          CHILD_ACTIVITY_SET_ALL,
                                                          ROLLUP_RULE_MINIMUM_COUNT_DEFAULT,
                                                          ROLLUP_RULE_MINIMUM_PERCENT_DEFAULT,
                                                          ROLLUP_RULE_ACTION_COMPLETED,
                                                          new Array(new SequencingRollupRuleCondition(RULE_CONDITION_OPERATOR_NOOP,SEQUENCING_RULE_CONDITION_COMPLETED))));
    
        this.LogSeq("[RB.1.3 b]1.2. Apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Activity Progress Known; and a Rollup Action of Incomplete (Define the default not incomplete rule)", logParent);
        activity.ApplyRollupRule(new SequencingRollupRule(RULE_CONDITION_COMBINATION_ANY,
                                                          CHILD_ACTIVITY_SET_ALL,
                                                          ROLLUP_RULE_MINIMUM_COUNT_DEFAULT,
                                                          ROLLUP_RULE_MINIMUM_PERCENT_DEFAULT,
                                                          ROLLUP_RULE_ACTION_INCOMPLETE,
                                                          new Array(new SequencingRollupRuleCondition(RULE_CONDITION_OPERATOR_NOOP,SEQUENCING_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN))));
        
    }
    else{
        if (activity.UsesDefaultCompletionRollupRules() === true){
            this.LogSeqSimple("Applying the default set of completion rollup rules to \"" + activity + "\".", simpleLogParent);
        }
    }//End If
    
    var subprocessResult;
    
    this.LogSeq("[RB.1.3 b]2. Apply the Rollup Rule Check Subprocess to the activity and the Incomplete rollup action (Process all Incomplete rules first.)", logParent);
    subprocessResult = this.RollupRuleCheckSubprocess(activity, RULE_SET_INCOMPLETE, logParent, simpleLogParent);

    this.LogSeq("[RB.1.3 b]3. If the Rollup Rule Check Subprocess returned True Then  ", logParent);
    if (subprocessResult === true){
		
		this.LogSeqSimple("Setting \"" + activity + "\" to incomplete.", simpleLogParent);
		
		this.LogSeq("[RB.1.3 b]3.1. Set the Attempt Progress Status for the activity to True  ", logParent);
		activity.SetAttemptProgressStatus(true);
		
		this.LogSeq("[RB.1.3 b]3.2. Set the Attempt Completion Status for the activity to False  ", logParent);
		activity.SetAttemptCompletionStatus(false);
    
    }//End If  

    this.LogSeq("[RB.1.3 b]4. Apply the Rollup Rule Check Subprocess to the activity and the Completed rollup action (Process all Completed rules last.) ", logParent);
    subprocessResult = this.RollupRuleCheckSubprocess(activity, RULE_SET_COMPLETED, logParent, simpleLogParent);
	
	this.LogSeq("[RB.1.3 b]5. If the Rollup Rule Check Subprocess returned True Then", logParent);
	if (subprocessResult === true){
	    
	    this.LogSeqSimple("Setting \"" + activity + "\" to completed.", simpleLogParent);
	    
		this.LogSeq("[RB.1.3 b]5.1. Set the Attempt Progress Status for the activity to True  ", logParent);
		activity.SetAttemptProgressStatus(true);
		
		this.LogSeq("[RB.1.3 b]5.2. Set the Attempt Completion Status for the activity to True  ", logParent);
		activity.SetAttemptCompletionStatus(true);
	
	}//End If  
    
    
    this.LogSeq("[RB.1.3 b]6. Exit Activity Progress Rollup Process", logParent);
    
	this.LogSeqReturn("", logParent);
	return;
	
}
