Debug.WriteControlAudit("Trying to create XMLHttpRequest in JavaScript1.5");
try{
	Debug.WriteControlAudit("Creating object");
	//objXmlHttp = new XMLHttpRequest();
	objXmlHttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
}
catch (e){
	//XMLHTTP object could not be created
	Debug.WriteControlAudit("Could not create object, exception=" + e);
	objXmlHttp = null;
}