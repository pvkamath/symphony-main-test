<%@ Page language="c#" %>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>
<script language="cs" runat="server">
protected void Page_Load(object sender, System.EventArgs e)
{
	ScormRuntimeUpdateRequest updateRequest = new ScormRuntimeUpdateRequest();
	updateRequest.Process(Request, Response);
}
</script>