<%@ Page Language="C#"%>

<%@ Import Namespace="System.Xml" %>

<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.DataHelp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<script language="cs" runat="server">

	IntegrationInterface imp = Integration.Implementation;
	IDataHelper dh = DataHelperFactory.GetHelper(
						Integration.Implementation.GetConfigurationSetting(Constants.CONFIG_DATA_PERSISTANCE_ENGINE),
						Integration.Implementation.GetConfigurationSetting(Constants.CONFIG_DB_CONNECTION_INFO));

	protected void Page_Load(object sender, EventArgs e)
	{
		String configuration = Request["configuration"];
		ExternalConfiguration externalConfig = imp.GetExternalConfigurationObject();
		if (configuration != null)
		{
			externalConfig.ParseFromString(configuration);
		}
		String serPackage = Request["package"];
		ExternalPackageId externalPkg = imp.GetExternalPackageIdObject();
		if (serPackage != null)
		{
			externalPkg.ParseFromString(serPackage);
		}
		String serRegistration = Request["registration"];
		ExternalRegistrationId externalRegId = imp.GetExternalRegistrationIdObject();
		if (serRegistration != null)
		{
			externalRegId.ParseFromString(serRegistration);
		}

		String action = Request["action"];
		if (String.IsNullOrEmpty(action))
		{
			Response.Write("error: Action parameter is required.");
			Response.End();
		}

		switch (action.ToLower())
		{
			case "getlaunchinfoxml":
				String launchId = Request["launchId"];
				XmlDocument launchXml = GetLaunchInfoXml(launchId, externalConfig);
				Response.ContentType = "text/xml";
				Response.Write(launchXml.OuterXml);
				Response.End();
				break;
			default:
				Response.Write("error: Sorry, " + action + " is not supported.");
				Response.End();
				break;
		}
	}

	private XmlDocument GetLaunchInfoXml(string launch_id, ExternalConfiguration externalConfig)
	{
		XmlDocument doc = new XmlDocument();

		XmlElement rootEl = doc.CreateElement("LaunchInfo");
		rootEl.SetAttribute("launch_history_id", launch_id);

		ArrayList spParams = new ArrayList();
		spParams.Add(new SPParam("@launch_history_id", launch_id));

		String launchSql = @"SELECT completion, satisfaction, measure_status, normalized_measure, experienced_duration_tracked, history_log, exit_time 
                                FROM ScormLaunchHistory 
                                WHERE launch_history_id = @launch_history_id";

		DbRow[] launchRows = dh.ExecuteSQLReturnDbRows(launchSql, spParams);

		if (launchRows.Length == 0)
		{
			throw new ScormContentPlayerApplicationException("No launch information found for launch id " + launch_id);
		}

		// If we don't have an exit_time then the launch terminated without sending a final
		// post (or it is still in progress ...)
		rootEl.SetAttribute("clean_termination",
			(launchRows[0].GetNullableDateTime("exit_time") != Constants.NullDateTime ? "true" : "false"));

		XmlDocument launchDoc = new XmlDocument();
        bool encodeUnicode = Integration.Implementation.IsPersistenceEncodedInLatinCharSet(externalConfig);

        string log = (encodeUnicode ? CommonFunctions.DecodeFromLatinCharSet(launchRows[0].GetNullableString("history_log")) : launchRows[0].GetNullableString("history_log"));
		if (!String.IsNullOrEmpty(log))
		{
			launchDoc.LoadXml(launchRows[0].GetNullableString("history_log"));
			rootEl.AppendChild(doc.ImportNode(launchDoc.DocumentElement, true));

			XmlElement statusEl = doc.CreateElement("RegistrationStatusOnExit");
			statusEl.SetAttribute("completion_status", launchRows[0].GetString("completion"));
			statusEl.SetAttribute("success_status", launchRows[0].GetString("satisfaction"));
			statusEl.SetAttribute("score", launchRows[0].GetBoolean("measure_status") ? (launchRows[0].GetDouble("normalized_measure") * 100).ToString() + "%" : "unknown");
			statusEl.SetAttribute("total_time_tracked", launchRows[0].GetLong("experienced_duration_tracked").ToString());
			rootEl.AppendChild(statusEl);
		}

		// Find previous launch (if any) so that we know the status at launch start, 
		// cl is this current launch, pl are all previous launches ordered.  Note,
		// we are using start_time and that might not be accurate if there were overlapping launches 
		// previous to this, but we can't rely on having an exit_time.
		String launchPrevSql = @"SELECT pl.completion, pl.satisfaction, pl.measure_status, pl.normalized_measure, pl.experienced_duration_tracked
                                    FROM ScormLaunchHistory pl  
                                    INNER JOIN ScormLaunchHistory cl on pl.scorm_registration_id = cl.scorm_registration_id and pl.launch_time < cl.launch_time
                                    WHERE cl.launch_history_id = @launch_history_id
										AND pl.completion is not null
									ORDER BY pl.launch_time desc";

		DbRow[] launchPrevRows = dh.ExecuteSQLReturnDbRows(launchPrevSql, spParams);

		XmlElement prevStatusEl = doc.CreateElement("RegistrationStatusOnEntry");
		if (launchPrevRows.Length == 0)
		{
			// This was the first launch - assume initial state 
			prevStatusEl.SetAttribute("completion_status", "unknown");
			prevStatusEl.SetAttribute("success_status", "unknown");
			prevStatusEl.SetAttribute("score", "unknown");
			prevStatusEl.SetAttribute("total_time_tracked", "0");
		}
		else
		{
			prevStatusEl.SetAttribute("completion_status", launchRows[0].GetString("completion"));
			prevStatusEl.SetAttribute("success_status", launchRows[0].GetString("satisfaction"));
			prevStatusEl.SetAttribute("score", launchRows[0].GetBoolean("measure_status") ? (launchRows[0].GetDouble("normalized_measure") * 100).ToString() + "%" : "unknown");
			prevStatusEl.SetAttribute("total_time_tracked", launchRows[0].GetLong("experienced_duration_tracked").ToString());
		}

		rootEl.AppendChild(prevStatusEl);

		doc.AppendChild(rootEl);
		return doc;
	}

</script>
</head>
<body>
</body>
</html>
