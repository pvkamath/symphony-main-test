<!-- Do not rename this file. The Scorm 2004 LMS Conformance Auto-Test relies on the fact that this file is named intermediate.aspx -->

<%@ Page language="c#" AutoEventWireup="false"%>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<script language=CS runat=server>
			protected IntermediateResponse Data;

			private void Page_Load(object sender, System.EventArgs e)
			{
				IntermediateRequest r = new IntermediateRequest();
				Data = r.Process(Request, Response);
			}

			override protected void OnInit(EventArgs e)
			{
				InitializeComponent();
				base.OnInit(e);
			}
			
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{    
				this.Load += new System.EventHandler(this.Page_Load);
			}
		</script>		
	
		<title>intermediage</title>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" href="<%=Integration.Implementation.GetUrlToStyleSheet(Request) %>" type="text/css">
		<script>
		<%=Data.ScpRequiredCode%>
		</script>
	</head>
	<body onload="Load();">
		<form id="Form1" method="post">
			<br>
			<br>
			<br>
			<br>
			<br>
			<div id="PleaseWait" align="center" style="visibility:hidden">
				<h3><%=Data.PleaseWaitMessage%></h3>
			</div>
			<br>
			<div id="SequencingError" align="center"></div>
			<div id="Message" align="center"></div>
		</form>
	</body>
</html>
