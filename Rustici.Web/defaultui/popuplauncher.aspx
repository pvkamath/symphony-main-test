<%@ Page language="c#" AutoEventWireup="false"%>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
		<script language=CS runat=server>
			protected PopupLauncherResponse Data;

			private void Page_Load(object sender, System.EventArgs e)
			{
				PopupLauncherRequest r = new PopupLauncherRequest();
				Data = r.Process(Request, Response);
			}

			override protected void OnInit(EventArgs e)
			{
				InitializeComponent();
				base.OnInit(e);
			}
			
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{    
				this.Load += new System.EventHandler(this.Page_Load);
			}
		</script>	  
  
  <script>
  
	// START code to invoke debug window - put cursor in window and press the "?" key five times
    function CheckKeyPress(e){
		
		var intKeyCode = 0;			
		if (window.event) {
			e = window.event;
			intKeyCode = e.keyCode;
		}
		else {
			intKeyCode = e.which;
		}
    
		Control.CheckForDebugCommand(intKeyCode);
    }
    
    window.document.onkeypress = CheckKeyPress;
    // END code to invoke debug window
  
	<%=Data.ScpRequiredCode%>
  
  </script>
  </head>
  
  <body onload="Load();" onbeforeunload="CloseSco();" onload="CloseSco()">
	
    <form id="frmPopupLauncher">
    <!-- //TODO: put better text here and maybe a button to return.-->
	<div id="PleaseClick" align="center" style="visibility:hidden">
		<h3><a href="" onclick="LaunchSco();return false;"><%=Data.ClickToLaunchMessage%></a></h3>
		
	</div>
	<div id="Message" align="center" style="visibility:hidden">
		<h3><%=Data.CourseLaunchedMessage%></h3>
	</div>
     </form>
	
  </body>
</html>
