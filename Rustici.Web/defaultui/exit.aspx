<%@ Page language="c#" ValidateRequest="false" %>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>exit dialog</title>
    <link rel="stylesheet" href="<%=Integration.Implementation.GetUrlToStyleSheet(Request) %>" type="text/css" />
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5"/>
    <script language="cs" runat="server">
        private string strDialogMessage;
        private string strSuspendAll;
        private string strExitAll;
        private string strCancel;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            //get parameters (initially external reg id and external config..later on, add ability to launch maniest or package directly)
			string config = Request.QueryString["configuration"];
			ExternalConfiguration externalConfig = Integration.Implementation.GetExternalConfigurationObject();
			externalConfig.ParseFromString(config);

            strDialogMessage = Integration.Implementation.GetString("Click 'Suspend All' To save state and pick up where you left off or choose 'Exit All' to end your attempt.",externalConfig);
            strSuspendAll = Integration.Implementation.GetString("Suspend All", externalConfig).ToUpper();
            strExitAll = Integration.Implementation.GetString("Exit All", externalConfig).ToUpper();
            strCancel = Integration.Implementation.GetString("Cancel", externalConfig).ToUpper();
            
            
        }

        /// <summary>
        /// The text displayed for the Dialog Message.
        /// </summary>
        public string DialogMessage
        {
            get { return strDialogMessage; }
        }

        /// <summary>
        /// The text displayed for the Suspend All option.
        /// </summary>
        public string SuspendAll
        {
            get { return strSuspendAll; }
        }

        /// <summary>
        /// The text displayed for the Exit All option.
        /// </summary>
        public string ExitAll
        {
            get { return strExitAll; }
        }

        /// <summary>
        /// The text displayed for the Cancel option.
        /// </summary>
        public string Cancel
        {
            get { return strCancel; }
        } 
        
    </script>
    
  </head>
  <body id="exit_dialog_body" >
	
   <div id="exit_dialog_div">
    
    <div id="exit_dialog_cancel" onclick="window.parent.Control.HideExitDialog(); return true;"><%=Cancel %></div>
    <%=DialogMessage %>
    <div id="exit_dialog_suspend" class="enabledTopMenuItem exit_dialog_btn" onclick="window.parent.Control.HideExitDialog(); window.parent.Control.ReturnToLms('suspend_all');" onmouseover="window.status='Suspend All - Save your state to resume later.';return true;" onmouseout="window.status='';return true;"><%=SuspendAll %></div>
    <div id="exit_dialog_exit" class="enabledTopMenuItem exit_dialog_btn" onclick="window.parent.Control.HideExitDialog(); window.parent.Control.ReturnToLms('exit_all');" onmouseover="window.status='Exit All - Finish the taking the course.';return true;" onmouseout="window.status='';return true;"><%=ExitAll %></div>
    
    </div> 
    
	
  </body>
</html>
