<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import namespace="RusticiSoftware.ScormContentPlayer.Logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
    <title>Save Debug Log</title>
    
    <link id="stylesheet" rel="stylesheet" runat="server" type="text/css" />
    
    <script language="cs" runat="server">
        
        string message = "";    
    
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string log = Request.Params["log"];
                log = log = HttpUtility.HtmlDecode(log);
                string reg = Request.Params["registrationId"];
                string config = Request.Params["configuration"]; // not doing anything with this currently
                
                stylesheet.Href = Integration.Implementation.GetUrlToStyleSheet(Request);

                // Post to central s3-backed service for viewing by Rustici Software support
                System.Net.WebClient webClient = new System.Net.WebClient();
                webClient.Headers.Add("Content-Type", "application/xml");
                webClient.Headers.Add("Referer", Request.UrlReferrer.Host);
                webClient.Encoding = Encoding.UTF8;
                
                try
                {
                    
                    string url = webClient.UploadString("http://cloud.scorm.com/EngineWebServices/logs/?registration=" + Server.UrlEncode(reg), "POST", log);
                    message = "Log succesfully saved and available for viewing at <br /><a target='_blank' href='" + url + "'>" + url + "</a>";
                }
                catch (Exception ex)
                {
                    message = "Sorry, an error occurred while saving your log. ERROR: " + ex.Message;
                }
            }
   
        }
	</script>   
</head>

<body>
    <div style="height: 100%; width: 100%; text-align: center; vertical-align: middle;">
        <%=message %>
        
        <br />
        <br />
        
        <a href="javascript: void window.close()">close window</a>
    </div>
</body>
</html>
