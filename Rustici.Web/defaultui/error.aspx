<%@ Page language="c#" %>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Scorm Content Player Error</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div width="100%" align="center" style="MARGIN-TOP: 100px">
				<asp:Label id="Label1" runat="server" ForeColor="#C00000"><%=Integration.Implementation.GetString("We apologize, an error has occurred.")%></asp:Label>
			</div>
		</form>
	</body>
</HTML>
