/*
Contains functions and constants that control the appearence of the SCP
once integrated in the target LMS
*/



function Integration(){
	this.CONTENT_FRAME_NAME = "ScormContent";
	
	//constants local to this specific integration
	this.MENU_INDENT = 20;
}

Integration.prototype.PopulateMenuItemDivTag = Integration_PopulateMenuItemDivTag;
Integration.prototype.UpdateIndentLevel = Integration_UpdateIndentLevel;
Integration.prototype.GetDocumentObjectForMenu = Integration_GetDocumentObjectForMenu;
Integration.prototype.GetHtmlElementToInsertMenuWithin = Integration_GetHtmlElementToInsertMenuWithin;
Integration.prototype.UpdateMenuStateDisplay = Integration_UpdateMenuStateDisplay;

Integration.prototype.SetMenuToggleVisibility = Integration_SetMenuToggleVisibility;
Integration.prototype.ShowMenu = Integration_ShowMenu;
Integration.prototype.HideMenu = Integration_HideMenu;
Integration.prototype.ShowExitDialog = Integration_ShowExitDialog;
Integration.prototype.HideExitDialog = Integration_HideExitDialog;

Integration.prototype.UpdateControlState = Integration_UpdateControlState;

Integration.prototype.GetString = Integration_GetString;

//private to this specific integration
Integration.prototype.GetDocumentObjectForControls = Integration_GetDocumentObjectForControls;


//Return an object of this type from each of the functions that interacts with the menu items.
//This will store the current state of the display.
//The next time the menu item is updated, this display will be passed back.
//You should compare the current state with the desired state before making any changes to the DOM.
//This will result in much more efficient code since manipulating the DOM is an expensive operation.
//The properties that you include here are up to you and your display. The SCP simply stores and forwards this object.

function DisplayState(){
	this.SuccessImageDisplayed = null;
	this.CompletionImageDisplayed = null;
	this.ActiveDisplayed = null;
	this.EnabledDisplayed = null;
	this.StatusTitleDisplayed = null;
	this.MenuTitleDisplayed = null;
}

function Integration_GetString(key){
	var value = AppStrings[key];

	if (value !== undefined && value !== null && value.length > 0) {
		var str = value;
	} else {
		// If the key doesn't have a value defined for it, use the key itself as the value.  We don't
		// want to create an error situation here so the worse case is an english string is shown instead
		// of a localized string.
		var str = key;
		//alert("Resource String does not exist: " + key);
	}
		
	// Substitute variables if this is a formatted string with {0}, {1}, etc
	for (var i = 1; i < arguments.length; i++) {
		var regExToReplace = new RegExp("\\{" + (i-1) + "\\}", "g");
		//str = str.replace("{" + (i-1) + "}", arguments[i].toString());
		if (arguments[i] == null){arguments[i]="";}
		
		str = str.replace(regExToReplace, arguments[i].toString());
	}
	
	return str;
}

//activityIdentifier is a combination of item id and database id that can be used for identifiying HTML elements 
//(just activity id alone is not enough because html elements names aren't case sensitive)
//activityItemId is the item identifier that needs to be passed to the choice request
function Integration_PopulateMenuItemDivTag(divTag, 
											documentObject, 
											activityIdentifier, 
											activityTitle, 
											indentLevel, 
											deliverable, 
											learningStandard, 
											statusDisplay,
											activityItemId){
	// AMP 7-31-08 Don't change the structure of this Menu. The Scorm 2004 LMS Conformance AutoTest assumes
	// that the structure of the tags (span/table/tbody/tr/etc.) will not change here.
	
	var totalIndent = indentLevel * this.MENU_INDENT;
	
	var completionImage = documentObject.createElement("img");
	completionImage.id = "CompletionStatusImg" + activityIdentifier;
	completionImage.src = "images/completionUnknown.gif";
	
	var successImage = documentObject.createElement("img");
	successImage.id = "SuccessStatusImg" + activityIdentifier;
	successImage.src = "images/successUnknown.gif";
	
	var link = documentObject.createElement("span");
	link.id = "MenuItemText" + activityIdentifier; // AMP 7-31-08 "MenuItemText" must come before the activityID in the id attribute of these span tags due to a dependency in the Scorm 2004 LMS Conformance AutoTest
	link.className = "menulink";
	link.innerHTML = activityTitle;

	var tbl = documentObject.createElement("table");
	tbl.id = "Table" + activityIdentifier;

	var tbody = documentObject.createElement("tbody");
	tbody.id = "TBody" + activityIdentifier;

	var row = documentObject.createElement("tr");
	row.id = "Tr" + activityIdentifier;

	var tdCompletion = documentObject.createElement("td");
	tdCompletion.id = "TdCompletion" + activityIdentifier;

	var tdSuccess = documentObject.createElement("td");
	tdSuccess.id = "TdSucess" + activityIdentifier;	

	var tdIndent = documentObject.createElement("td");
	tdIndent.id = "TdIndent" + activityIdentifier;
	tdIndent.style.width = totalIndent + "px";
	
	var tdLink = documentObject.createElement("td");
	tdLink.id = "TdLink" + activityIdentifier;

	tdCompletion.appendChild(completionImage);
	tdSuccess.appendChild(successImage);
	tdLink.appendChild(link);	
	

	if (statusDisplay == STATUS_DISPLAY_COMPELTION_ONLY || statusDisplay == STATUS_DISPLAY_SEPERATE || statusDisplay == STATUS_DISPLAY_COMBINED){
		row.appendChild(tdCompletion);
	}

	if (statusDisplay == STATUS_DISPLAY_SUCCESS_ONLY || statusDisplay == STATUS_DISPLAY_SEPERATE){
		row.appendChild(tdSuccess);
	}
	
	row.appendChild(tdIndent);
	row.appendChild(tdLink);

	tbody.appendChild(row);
	tbl.appendChild(tbody);
	
	divTag.appendChild(tbl);
	
	// cursor: pointer works for IE 6 and Mozilla browsers.  cursor: hand only works for older IE
	var browserName = navigator.appName; 
	var browserVer = parseInt(navigator.appVersion); 
	if (browserName == "Microsoft Internet Explorer" && browserVer < 6) {
		divTag.onmouseover = function () {this.style.cursor='hand';window.status=activityTitle;return true;};
	} else {
		divTag.onmouseover = function () {this.style.cursor='pointer';window.status=activityTitle;return true;};
	}
	divTag.onmouseout = function () {this.style.cursor='default';window.status='';return true;};
	divTag.onclick = function () { 
		    var contr = GetController();
            contr.ChoiceRequest(activityItemId);
            return true;};
	
	var currentDisplayState = new DisplayState();
	
	currentDisplayState.SuccessImageDisplayed = "images/successUnknown.gif";
	currentDisplayState.CompletionImageDisplayed = "images/completionUnknown.gif";
	currentDisplayState.ActiveDisplayed = null;
	currentDisplayState.EnabledDisplayed = null;
	currentDisplayState.StatusTitleDisplayed = "";
	currentDisplayState.MenuTitleDisplayed = "";
	
	return currentDisplayState;
}

function Integration_UpdateIndentLevel(divTag, activityIdentifier, indentLevel){

	var totalIndent = indentLevel * this.MENU_INDENT;
	
	//this may get called on a child element which might not have been added to the actual document yet, so use the divTag.document instead of just document
	var tdIndent = null;
	var tdIdentifier = "TdIndent" + activityIdentifier;
	
	if (divTag.document){
		//works well in IE
		tdIndent = divTag.document.getElementById(tdIdentifier);
	}
	else{
		//needed for FireFox
		var tdNodes = divTag.getElementsByTagName("td");
		
		for (var i=0; i < tdNodes.length; i++){
			if (tdNodes[i].id == tdIdentifier){
				tdIndent = tdNodes[i];
				break;
			}
		}
	}
	
	//this can get called before the DOM element is actually added to the document
	if (tdIndent !== null){
		
		//With the new rules in 3rd Edition about forcing some requests to be hidden, we can now be hiding the root
		if (totalIndent < 0){totalIndent=0;}
		
		tdIndent.style.width = totalIndent + "px";	
	}
	else{
		Debug.AssertError("td indent not found");
	}
}

//gets called from the window containing the controller
function Integration_GetDocumentObjectForMenu(){
	return window.ScormLeft.document;
}

//private to this specific integration
function Integration_GetDocumentObjectForControls(){
	if (window.ScormTop){
		return window.ScormTop.document;
	}
	else if (window.parent.ScormTop){
		return window.parent.ScormTop.document;
	}
}

//gets called from the window containing the controller
function Integration_GetHtmlElementToInsertMenuWithin(){
	var docObject = this.GetDocumentObjectForMenu();
	return docObject.getElementById("MenuPlaceHolder");
}


//function called when a menu item's status has changed and needs to be graphically updated
function Integration_UpdateMenuStateDisplay(divTag, 
											documentObject, 
											activity, 
											activityIdentifier, 
											deliverable, 
											currentActivity, 
											navigationRequestInfo, 
											learningStandard, 
											statusDisplay,
											currentDisplayState,
											useLookAheadActivityStatus){
	
	var newDisplayState = new DisplayState();
	
	newDisplayState.SuccessImageDisplayed = "";
	newDisplayState.CompletionImageDisplayed = "";
	newDisplayState.ActiveDisplayed = null;
	newDisplayState.EnabledDisplayed = null;
	newDisplayState.TitleDisplayed = "";
	
	if (activity.WasAutoCompleted === true && activity.LookAheadActivity === true && useLookAheadActivityStatus === true) {
		var activityIsCompleted = RESULT_UNKNOWN;
	} else {
		var activityIsCompleted = activity.IsCompleted();
	}
	
	if (activity.WasAutoSatisfied === true && activity.LookAheadActivity === true && useLookAheadActivityStatus === true) {
		var activityIsSatisfied = RESULT_UNKNOWN;
	} else {
		var activityIsSatisfied = activity.IsSatisfied();
	}
	
	var activityIsActive = (currentActivity !== null && currentActivity !== undefined && activity.GetItemIdentifier() === currentActivity.GetItemIdentifier());
	
	var statusTitle = "";
	var completionTitle = "";
	var successTitle = "";
	var menuTextTitle = "";
	
	if (statusDisplay == STATUS_DISPLAY_SEPERATE){
		//dual status display
		if (activityIsCompleted === true){
			newDisplayState.CompletionImageDisplayed = "images/completed.gif";
			completionTitle = IntegrationImplementation.GetString("Completed");
		}
		else if(activityIsCompleted === false){
			newDisplayState.CompletionImageDisplayed = "images/incomplete.gif";
			completionTitle = IntegrationImplementation.GetString("Requirement Not Fulfilled");
		}
		else{
			newDisplayState.CompletionImageDisplayed = "images/completionUnknown.gif";
			completionTitle = IntegrationImplementation.GetString("Not Started");
		}
		
		statusTitle = completionTitle + " / ";
		
		if (activityIsSatisfied === true){
			newDisplayState.SuccessImageDisplayed = "images/passed.gif";
			successTitle += IntegrationImplementation.GetString("Passed");
		}
		else if(activityIsSatisfied === false){
			newDisplayState.SuccessImageDisplayed = "images/failed.gif";
			successTitle += IntegrationImplementation.GetString("Requirement Not Fulfilled");
		}
		else{
			newDisplayState.SuccessImageDisplayed = "images/successUnknown.gif";
			successTitle += IntegrationImplementation.GetString("Unknown");
		}
		
		statusTitle += successTitle;
	}
	else if (statusDisplay == STATUS_DISPLAY_COMPELTION_ONLY || statusDisplay == STATUS_DISPLAY_COMBINED){
		//single status display
		if (activityIsCompleted === true){
			newDisplayState.CompletionImageDisplayed = "images/completed.gif";
			statusTitle = IntegrationImplementation.GetString("Completed");
		}
		else if(activityIsCompleted === false){
			newDisplayState.CompletionImageDisplayed = "images/incomplete.gif";
			statusTitle = IntegrationImplementation.GetString("Requirement Not Fulfilled");
		}
		else{
			newDisplayState.CompletionImageDisplayed = "images/completionUnknown.gif";
			statusTitle = IntegrationImplementation.GetString("Not Started");
		}
		
		//success status overrides complete status and we don't have a success image so only use complete image
		if (activityIsSatisfied === true){
			newDisplayState.CompletionImageDisplayed = "images/passed.gif";
			statusTitle = IntegrationImplementation.GetString("Passed");
		}
		else if(activityIsSatisfied === false){
			newDisplayState.CompletionImageDisplayed = "images/failed.gif";
			statusTitle = IntegrationImplementation.GetString("Requirement Not Fulfilled");
		}
		
	}
	else if (statusDisplay == STATUS_DISPLAY_SUCCESS_ONLY ){
		
		if (activityIsSatisfied === true){
			newDisplayState.SuccessImageDisplayed = "images/passed.gif";
			statusTitle += IntegrationImplementation.GetString("Passed");
		}
		else if(activityIsSatisfied === false){
			newDisplayState.SuccessImageDisplayed = "images/failed.gif";
			statusTitle += IntegrationImplementation.GetString("Requirement Not Fulfilled");
		}
		else{
			newDisplayState.SuccessImageDisplayed = "images/successUnknown.gif";
			statusTitle += IntegrationImplementation.GetString("Unknown");
		}
		
	}
	
	if (navigationRequestInfo.WillSucceed === true){
		newDisplayState.EnabledDisplayed = true;
	}
	else{
		newDisplayState.EnabledDisplayed = false;
	}
	
	if (activityIsActive){
		newDisplayState.ActiveDisplayed = true;
	}
	else{
		newDisplayState.ActiveDisplayed = false;
	}
	

	if (learningStandard.is2004()){
	    
		if (activity.GetPrimaryObjective().GetMeasureStatus(null, false)){
			statusTitle += ", ";
			statusTitle += IntegrationImplementation.GetString("Score: {0}", activity.GetPrimaryObjective().GetNormalizedMeasure(null, false)); 
			successTitle += ", ";
			successTitle += IntegrationImplementation.GetString("Score: {0}", activity.GetPrimaryObjective().GetNormalizedMeasure(null, false)); 
		}
	}
	else{
		if (activity.RunTime !== null){
			if (activity.RunTime.ScoreRaw !== null){
				statusTitle += ", ";
				successTitle += ", ";
				
				if (activity.RunTime.ScoreMax !== null){
					statusTitle += IntegrationImplementation.GetString("Score: {0} of {1}", activity.RunTime.ScoreRaw, activity.RunTime.ScoreMax);
					successTitle += IntegrationImplementation.GetString("Score: {0} of {1}", activity.RunTime.ScoreRaw, activity.RunTime.ScoreMax);
				} else {
					statusTitle += IntegrationImplementation.GetString("Score: {0}", activity.RunTime.ScoreRaw);
					successTitle += IntegrationImplementation.GetString("Score: {0}", activity.RunTime.ScoreRaw);
				}
			}
		}	
	}
	
	
	newDisplayState.StatusTitleDisplayed = statusTitle;
	newDisplayState.MenuTitleDisplayed = statusTitle;
	
	var completionImage = null;
	var successImage = null;
	
	if (statusDisplay == STATUS_DISPLAY_SEPERATE || statusDisplay == STATUS_DISPLAY_COMBINED || statusDisplay == STATUS_DISPLAY_COMPELTION_ONLY){
		if (newDisplayState.CompletionImageDisplayed != currentDisplayState.CompletionImageDisplayed){
			completionImage = documentObject.getElementById("CompletionStatusImg" + activityIdentifier);
			completionImage.src = newDisplayState.CompletionImageDisplayed;
		}
	}
	
	if (statusDisplay == STATUS_DISPLAY_SEPERATE || statusDisplay == STATUS_DISPLAY_SUCCESS_ONLY ){
		if (newDisplayState.SuccessImageDisplayed != currentDisplayState.SuccessImageDisplayed){
			successImage = documentObject.getElementById("SuccessStatusImg" + activityIdentifier);
			successImage.src = newDisplayState.SuccessImageDisplayed;
		}
	}
	
	if ((newDisplayState.EnabledDisplayed != currentDisplayState.EnabledDisplayed) ||
		(newDisplayState.ActiveDisplayed != currentDisplayState.ActiveDisplayed) ||
		(newDisplayState.MenuTitleDisplayed != currentDisplayState.MenuTitleDisplayed)){
	
		var menuText = documentObject.getElementById("MenuItemText" + activityIdentifier);
		
		if (newDisplayState.ActiveDisplayed === true){
			if (	
				(newDisplayState.ActiveDisplayed != currentDisplayState.ActiveDisplayed) ||
				(newDisplayState.EnabledDisplayed != currentDisplayState.EnabledDisplayed)
				){
				
				if (newDisplayState.EnabledDisplayed === true ){
					menuText.className = "activeMenulink"; // AMP 7-31-08 Do not change this string. The Scorm 2004 LMS Conformance AutoTest depends on this string.
				}
				else{
					menuText.className = "activeMenulinkDisabled"; // AMP 7-31-08 Do not change this string. The Scorm 2004 LMS Conformance AutoTest depends on this string.
				}
			}
		}
		else{
			if (
				(newDisplayState.EnabledDisplayed != currentDisplayState.EnabledDisplayed) ||
				(newDisplayState.ActiveDisplayed === false && (newDisplayState.ActiveDisplayed != currentDisplayState.ActiveDisplayed))
			    ){
			
				if(newDisplayState.EnabledDisplayed === true)
				{
					menuText.className = "enabledMenulink"; // AMP 7-31-08 Do not change this string. The Scorm 2004 LMS Conformance AutoTest depends on this string.
					menuTextTitle = "";
				}
				else
				{
					menuText.className = "disabledMenulink"; // AMP 7-31-08 Do not change this string. The Scorm 2004 LMS Conformance AutoTest depends on this string.
					menuTextTitle = navigationRequestInfo.Exception + " " + navigationRequestInfo.ExceptionText;
				}
			}
		}
		
		if (newDisplayState.MenuTitleDisplayed != currentDisplayState.MenuTitleDisplayed){
			menuText.title = newDisplayState.MenuTitleDisplayed;
		}
	}
	
	if (newDisplayState.StatusTitleDisplayed != currentDisplayState.StatusTitleDisplayed){
		
		if (statusDisplay == STATUS_DISPLAY_SEPERATE) {
			if (completionImage === null){
				completionImage = documentObject.getElementById("CompletionStatusImg" + activityIdentifier);
			}
			if (successImage === null){
				successImage = documentObject.getElementById("SuccessStatusImg" + activityIdentifier);
			}
			completionImage.title = completionTitle;
			successImage.title = successTitle;
		}
		
		if (statusDisplay == STATUS_DISPLAY_COMBINED || statusDisplay == STATUS_DISPLAY_COMPELTION_ONLY){
			if (completionImage === null){
				completionImage = documentObject.getElementById("CompletionStatusImg" + activityIdentifier);
			}
			completionImage.title = statusTitle;
		}
		
		if (statusDisplay == STATUS_DISPLAY_SUCCESS_ONLY){
			if (successImage === null){
				successImage = documentObject.getElementById("SuccessStatusImg" + activityIdentifier);
			}
			successImage.title = statusTitle;
		}
	}
	
	
	
	
	return newDisplayState;
}

//determines whether the "show/hide menu" button should be displayed (according to whether or not the course structure is shown)
//called from the window containing the controller
function Integration_SetMenuToggleVisibility(isVisible){
	
	var doc = this.GetDocumentObjectForControls();
	var control = doc.getElementById("menuToggle");
	
	if (control !== null){
		if (isVisible === true){
			control.style.visibility = "visible";
		}
		else{
			control.style.visibility = "hidden";
		}
	}
}

//gets called from both the controller frame and from the frame containing the show/hide button
function Integration_ShowMenu(width) {
	
	var frameset = null;
	var toggleElement = null;
	
	frameset = document.getElementById("ScormMainFrameset");
	toggleElement = window.frames["ScormTop"].document.getElementById("menuToggle");
	
	if (frameset === null){
		frameset = window.parent.document.getElementById("ScormMainFrameset");
		toggleElement = window.parent.frames["ScormTop"].document.getElementById("menuToggle");
	}
	
	frameset.cols = width + ",*";
	
	if (toggleElement !== null){
		toggleElement.innerHTML = this.GetString("Hide Menu").toUpperCase();
	}
}

//gets called from both the controller frame and from the frame containing the show/hide button
//needs to be able to handle being called when the frameset is already hidden (happens onload)
function Integration_HideMenu(){
	
	var frameset = null;
	var toggleElement = null;
	
	frameset = document.getElementById("ScormMainFrameset");
	toggleElement = window.frames["ScormTop"].document.getElementById("menuToggle");
	
	if (frameset === null){
		frameset = window.parent.document.getElementById("ScormMainFrameset");
		toggleElement = window.parent.frames["ScormTop"].document.getElementById("menuToggle");
	}
	
	frameset.cols = "0,*";
	if (toggleElement !== null){
		toggleElement.innerHTML = this.GetString("Show Menu").toUpperCase();
	}
}

function Integration_ShowExitDialog(){
    var frameset = null;
	
	frameset = document.getElementById("ScormPageFrameset");
	
	if (frameset === null){
		frameset = window.parent.document.getElementById("ScormPageFrameset");
	}
	var rows = frameset.rows;
	rows = rows.replace(/0,\*/,"75,*");
	
	frameset.rows = rows;
	
}

function Integration_HideExitDialog(){
    var frameset = null;
	
	frameset = document.getElementById("ScormPageFrameset");
	if (frameset === null){
		frameset = window.parent.document.getElementById("ScormPageFrameset");
	}
	
	var rows = frameset.rows;
	rows = rows.replace(/75,\*/,"0,*");
	
	frameset.rows = rows;
	
}


//gets called from the window containing the controller

function Integration_UpdateControlState(doc, aryPossibleRequests, currentActivity){

	//TODO: don't access the DOM unless there is a change
	
	var previousElement = doc.getElementById("previous");
	var nextElement = doc.getElementById("next");
	//var quitScoElement = doc.getElementById("quitSco");
	//var quitCourseElement = doc.getElementById("quitCourse");
	//var pauseElement = doc.getElementById("pause");
	var exitScoElement = doc.getElementById("closeSco");
	var exitCourseElement = doc.getElementById("returnToLms");
	
	var hidePrevious = false;
	var hideContinue = false;
	var hideExit = false;
	var hideAbandon = false;
	var hideSuspendAll = false;
	var hideAbandonAll = false;
	var hideExitAll = false;
	
	var invalidMenuItemAction = Control.Package.Properties.InvalidMenuItemAction;
	
	if (currentActivity !== null){
		var hidePrevious = currentActivity.LearningObject.SequencingData.HidePrevious;
		var hideContinue = currentActivity.LearningObject.SequencingData.HideContinue;
		var hideExit = currentActivity.LearningObject.SequencingData.HideExit;
		var hideAbandon = currentActivity.LearningObject.SequencingData.HideAbandon;
		var hideSuspendAll = currentActivity.LearningObject.SequencingData.HideSuspendAll;
		var hideAbandonAll = currentActivity.LearningObject.SequencingData.HideAbandonAll;
		var hideExitAll = currentActivity.LearningObject.SequencingData.HideExitAll;		
	}
	
	if (nextElement !== null){
		if (
			(aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed === true && hideContinue === false) ||
			(invalidMenuItemAction == INVALID_MENU_ITEM_ACTION_SHOW_ENABLE)
			){
			nextElement.className = "enabledTopMenuItem";
			nextElement.onclick = function () {
			    var contr = GetController();
                contr.Next();
			    return true;};
			nextElement.title = "";
			nextElement.style.display = "inline"; // AMP 7-31-08 Do not change this string. The Scorm 2004 LMS Conformance AutoTest assumes that this style will make the div visible
		}
		else{
			if (invalidMenuItemAction == INVALID_MENU_ITEM_ACTION_DISABLE){
				nextElement.className = "disabledTopMenuItem";
				nextElement.onclick = "";
				if (hideContinue){
					nextElement.title = IntegrationImplementation.GetString("You cannot use 'Next' with this item.");
				}
				else{
					nextElement.title = aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].GetErrorString();
				}
			}
			else if (invalidMenuItemAction == INVALID_MENU_ITEM_ACTION_HIDE) {
				nextElement.style.display = "none"; // AMP 7-31-08 Do not change this string. The Scorm 2004 LMS Conformance AutoTest assumes that this style will make the div invisible
			}
		}
	}
	
	if (previousElement !== null){
		if (
			(aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed === true && hidePrevious === false) ||
			(invalidMenuItemAction == INVALID_MENU_ITEM_ACTION_SHOW_ENABLE)
			){
			previousElement.className = "enabledTopMenuItem";
			previousElement.onclick = function () {
			    var contr = GetController();
                contr.Previous();
			    return true;};
			previousElement.title = "";
			previousElement.style.display = "inline"; // AMP 7-31-08 Do not change this string. The Scorm 2004 LMS Conformance AutoTest assumes that this style will make the div visible
		}
		else{
			if (invalidMenuItemAction == INVALID_MENU_ITEM_ACTION_DISABLE){
				previousElement.className = "disabledTopMenuItem";
				previousElement.onclick = "";
				if (hidePrevious){
					previousElement.title = IntegrationImplementation.GetString("This lesson does not allow 'Previous' requests.");
				}
				else{
					previousElement.title = aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].GetErrorString();
				}
			}
			else if (invalidMenuItemAction == INVALID_MENU_ITEM_ACTION_HIDE) {
				previousElement.style.display = "none"; // AMP 7-31-08 Do not change this string. The Scorm 2004 LMS Conformance AutoTest assumes that this style will make the div invisible
			}
		}
	}
	
	if (exitScoElement !== null){
		if (
			(aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed === true && hideExit === false) ||
			(invalidMenuItemAction == INVALID_MENU_ITEM_ACTION_SHOW_ENABLE)
			){
			exitScoElement.className = "enabledTopMenuItem";
			exitScoElement.onclick = function () {
			    var contr = GetController();
                contr.CloseSco();
			    return true;};
			exitScoElement.title = "";
			exitScoElement.style.display = "inline"; // AMP 7-31-08 Do not change this string. The Scorm 2004 LMS Conformance AutoTest assumes that this style will make the div visible
		}
		else{
			if (invalidMenuItemAction == INVALID_MENU_ITEM_ACTION_DISABLE){
				exitScoElement.className = "disabledTopMenuItem";
				exitScoElement.onclick = "";
				if (hideExit){
					exitScoElement.title = IntegrationImplementation.GetString("You cannot use 'Exit' with this item.");
				}
				else{
					exitScoElement.title = aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].GetErrorString();
				}
			}
			else if (invalidMenuItemAction == INVALID_MENU_ITEM_ACTION_HIDE) {
				exitScoElement.style.display = "none"; // AMP 7-31-08 Do not change this string. The Scorm 2004 LMS Conformance AutoTest assumes that this style will make the div invisible
			}
		}
	}
	
	//We only want to disable the Exit button if it is is explicitly hidden by sequencing rules,
	//otherwise, always enable it (regardless of what the look ahead sequencer says)
	if (exitCourseElement !== null){
		if (hideExitAll === true && hideSuspendAll === true && hideAbandonAll === true){
			exitCourseElement.className = "disabledTopMenuItem";
			exitCourseElement.title = aryPossibleRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].GetErrorString();
		}
		else{
			exitCourseElement.className = "enabledTopMenuItem";
			exitCourseElement.title = "";
		}
	}
	
	// Progress bar
    var progressText = doc.getElementById("progressText");
    var totalProgressImage = doc.getElementById("totalProgressImage");

    //if using measure for progress display, measureProgressImage will be present, otherwise progressImage will be present
    var progressImage = doc.getElementById("progressImage");
    var measureProgressImage = doc.getElementById("measureProgressImage");

    if (progressText !== null){
	    var intProgressPercent;
	    var intRemainingPercent;
    	
	    if (progressImage !== null){
    	
    	
	        if (this.NumDeliverableScos == null){
		        this.NumDeliverableScos = Control.Activities.GetNumDeliverableActivities();
	        }
    		
	        //find the number of complete and not failed activities
	        var activityList = Control.Activities.ActivityList;
	        var numComplete = 0;
	        for (var i=0; i < activityList.length; i++){
		        if (activityList[i].IsDeliverable()){
    				
			        if (activityList[i].IsCompleted() == true && activityList[i].IsSatisfied() != false){
				        numComplete++;
			        }
		        }
	        }
    		
	        progressText.innerHTML = numComplete + " of " + this.NumDeliverableScos;
    		
	        intProgressPercent = parseInt((numComplete / this.NumDeliverableScos) * 100);
	        intRemainingPercent = 100 - intProgressPercent;		
    			
	        totalProgressImage.width = intRemainingPercent;
	        progressImage.width = intProgressPercent;
	        progressImage.src = "images/progressX.gif";
	    }
	    else {
    	    
	        var rootActivity = currentActivity;
	        if (rootActivity !== null){
	            while (rootActivity.IsTheRoot() !== true){
	                rootActivity = rootActivity.ParentActivity;
	            }
    		    
	            var progressAmount = rootActivity.GetAttemptCompletionAmount();
	            intProgressPercent =  parseInt(progressAmount * 100);
	        }
	        else{
	            intProgressPercent =  0;
	        }
    	    
	        intRemainingPercent = 100 - intProgressPercent;		
    	    
	        progressText.innerHTML = intProgressPercent + "% Complete";
    		
		    totalProgressImage.width = intRemainingPercent;
	        measureProgressImage.width = intProgressPercent;
	        measureProgressImage.src = "images/progressX.gif";
    	
	    }
    }

}