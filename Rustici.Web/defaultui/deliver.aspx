<%@ Page language="c#" AutoEventWireup="false" %>
<%@ Import namespace="RusticiSoftware.ScormContentPlayer.Logic"%>
<%@ Import Namespace="Symphony.Core" %>
<%@ Import Namespace="Symphony.Core.Controllers" %>
<%@ Import Namespace="Symphony.Web" %>
<%@ Import Namespace="Symphony.Core.Models" %>
<%@ Import Namespace="Symphony.RusticiIntegration" %>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.DataHelp" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<%=Data.VersionInformation%>

<html>
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=8">
	<script language="CS" runat="server">
 
		protected DeliverResponse Data;

        IntegrationInterface imp = Integration.Implementation;
        IDataHelper dh = DataHelperFactory.GetHelper(
                            Integration.Implementation.GetConfigurationSetting(Constants.CONFIG_DATA_PERSISTANCE_ENGINE),
                            Integration.Implementation.GetConfigurationSetting(Constants.CONFIG_DB_CONNECTION_INFO));

        protected int RegistrationID = 0;
        protected int UserID = 0;
        protected int ActualUserID = 0;
        protected string SymphonyDomain = "";
        
		private void Page_Load(object sender, System.EventArgs e)
		{
            DeliverRequest r = new DeliverRequest();
                       
            UserController userController = new UserController();
            ActualUserID = userController.ActualUserID;
            UserID = 0;

            string registration = Request.QueryString["registration"];
            if (!string.IsNullOrEmpty(registration))
            {

                string[] registrationParts = registration.Split('!');
                
                foreach (string p in registrationParts)
                {
                    string[] keyValuePair = p.Split('|');

                    if (keyValuePair.Count() > 1 && keyValuePair[0] == "UserId")
                    {
                        int.TryParse(keyValuePair[1], out UserID);
                    }
                }

                if (UserID != ActualUserID)
                {
                    if (!userController.ActualUserIsSalesChannelAdmin)
                    {
                        //     throw new Exception("You do not have permission to take this course as a different user.");
                    }
                }
            }
            
            Data = r.Process(Request, Response);

            
            String serRegistration = Request["registration"];
            ExternalRegistrationId externalRegId = imp.GetExternalRegistrationIdObject();
            if (serRegistration != null)
            {
                externalRegId.ParseFromString(serRegistration);
            }
            
            imp.ShouldCreateNewRegistrationInstance(externalRegId, imp.GetExternalConfigurationObject());
            
            SymphonyExternalRegistration reg = externalRegId as SymphonyExternalRegistration;
            int.TryParse(reg.ScormRegistrationId, out RegistrationID);

            if (ConfigurationManager.AppSettings["symphonyDomain"] != null)
            {
                SymphonyDomain = ConfigurationManager.AppSettings["symphonyDomain"];
            }
		}

		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
	</script>		
	
	
	<!--//TODO: eventually, we probably want this entire HEAD tag (or at least the important) written out from the Deliver class-->
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" href="<%=Integration.Implementation.GetUrlToStyleSheet(Request)%>" type="text/css" />
		<title>
			<%=Data.Title%>
		</title>

        <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.js"></script>

        <script type="text/javascript" src="../websync/fm.min.js" ></script>
        <script type="text/javascript" src="../websync/fm.websync.min.js" ></script>
        <script type="text/javascript" src="../websync/fm.websync.subscribers.min.js" ></script>

		<%=Data.ScpRequiredCode%>
        <script type ="text/javascript">
            var Symphony = { 
                registrationId: <%=RegistrationID%>,
                actualUserId: <%=ActualUserID%>,
                userId: <%=UserID%>,
                symphonyDomain: "<%=SymphonyDomain%>"
            };
        </script>
        <!--<script type="text/javascript" src="../scripts/CourseMutex.js"></script>-->
        <script type="text/javascript">
            // Fix for video-js adding indexOf to array prototype
            function Controller_MarkPostedDataDirty() {
                this.WriteAuditLog("Control MarkPostedDataDirty");

                for (var activity in this.Activities.ActivityList) {
                    if (this.Activities.ActivityList.hasOwnProperty(activity)) {
                        if (this.Activities.ActivityList[activity].DataState == DATA_STATE_POSTED) {
                            this.Activities.ActivityList[activity].DataState = DATA_STATE_DIRTY;
                        }

                        this.Activities.ActivityList[activity].MarkPostedObjectiveDataDirty();
                    }
                }

                if (this.Sequencer.GlobalObjectives !== null && this.Sequencer.GlobalObjectives !== undefined) {
                    for (var globalObjective in this.Sequencer.GlobalObjectives) {
                        for (var globalObjective in this.Sequencer.GlobalObjectives) {
                            dataState = this.Sequencer.GlobalObjectives[globalObjective].DataState;

                            if (dataState == DATA_STATE_POSTED) {
                                this.Sequencer.GlobalObjectives[globalObjective].DataState = DATA_STATE_DIRTY;
                            }
                        }
                    }
                }

                for (var bucket in this.SSPBuckets) {
                    if (this.SSPBuckets.hasOwnProperty(bucket)) {
                        if (this.SSPBuckets[bucket].DataState == DATA_STATE_POSTED) {
                            this.SSPBuckets[bucket].DataState = DATA_STATE_DIRTY;
                        }
                    }
                }
            }

            function Controller_MarkDirtyDataPosted() {
                this.WriteAuditLog("Control MarkDirtyDataPosted");

                for (var activity in this.Activities.ActivityList) {
                    if (this.Activities.ActivityList.hasOwnProperty(activity)) {
                        if (this.Activities.ActivityList[activity].IsAnythingDirty()) {
                            this.Activities.ActivityList[activity].DataState = DATA_STATE_POSTED;
                        }

                        this.Activities.ActivityList[activity].MarkDirtyObjectiveDataPosted();
                    }
                }

                if (this.Sequencer.GlobalObjectives !== null && this.Sequencer.GlobalObjectives !== undefined) {
                    for (var globalObjective in this.Sequencer.GlobalObjectives) {
                        if (this.Sequencer.GlobalObjectives.hasOwnProperty(globalObjective)) {
                            dataState = this.Sequencer.GlobalObjectives[globalObjective].DataState;

                            if (dataState == DATA_STATE_DIRTY) {
                                this.Sequencer.GlobalObjectives[globalObjective].DataState = DATA_STATE_POSTED;
                            }
                        }
                    }
                }

                for (var bucket in this.SSPBuckets) {
                    if (this.SSPBuckets.hasOwnProperty(bucket)) {
                        if (this.SSPBuckets[bucket].DataState == DATA_STATE_DIRTY) {
                            this.SSPBuckets[bucket].DataState = DATA_STATE_POSTED;
                        }
                    }
                }
            }

            function Controller_GetXmlForDirtyData() {

                this.WriteAuditLog("Control GetXmlForDirtyData");

                var ServerFormat = new ServerFormater();
                var xml = new XmlElement("RTD");

                xml.AddAttribute("RI", RegistrationToDeliver.Id);

                if (this.Sequencer.GetSuspendedActivity() !== null) {
                    xml.AddAttribute("SAI", this.Sequencer.GetSuspendedActivity().GetDatabaseIdentifier());
                }

                if (this.GetLaunchHistoryId() !== null) {
                    xml.AddAttribute("LH", this.GetLaunchHistoryId());
                }

                for (var activity in this.Activities.ActivityList) {
                    if (this.Activities.ActivityList.hasOwnProperty(activity)) {
                        if (this.Activities.ActivityList[activity].IsAnythingDirty() ||
                            this.Activities.ActivityList[activity].DataState == DATA_STATE_POSTED) {

                            xml.AddElement(this.Activities.ActivityList[activity].GetXml());
                        }
                    }
                }


                if (this.Sequencer.GlobalObjectives !== null && this.Sequencer.GlobalObjectives !== undefined) {
                    for (var globalObjective in this.Sequencer.GlobalObjectives) {
                        if (this.Sequencer.GlobalObjectives.hasOwnProperty(globalObjective)) {
                            dataState = this.Sequencer.GlobalObjectives[globalObjective].DataState;

                            //for now we're posting all global objectives
                            if (dataState == DATA_STATE_DIRTY || dataState == DATA_STATE_POSTED) {
                                xml.AddElement(this.Sequencer.GlobalObjectives[globalObjective].GetXml(RegistrationToDeliver.Id, globalObjective));
                            }
                        }
                    }
                }

                for (var bucket in this.SSPBuckets) {
                    if (this.SSPBuckets.hasOwnProperty(bucket)) {
                        if (this.SSPBuckets[bucket].DataState == DATA_STATE_DIRTY ||
                            this.SSPBuckets[bucket].DataState == DATA_STATE_POSTED) {

                            xml.AddElement(this.SSPBuckets[bucket].GetXml());

                        }
                    }
                }

                for (var sharedDataItem in this.SharedData) {
                    if (this.SharedData.hasOwnProperty(sharedDataItem)) {
                        if (this.SharedData[sharedDataItem].DataState == DATA_STATE_DIRTY ||
                            this.SharedData[sharedDataItem].DataState == DATA_STATE_POSTED) {

                            xml.AddElement(this.SharedData[sharedDataItem].GetXml());

                        }
                    }
                }

                // Append the current event log for this launch
                var historyLogXml = HistoryLog.log.dom.getElementsByTagName('RTL')[0];
                //xml.AddElement(historyLogXml);
                xml.AddElement((new XMLSerializer()).serializeToString(historyLogXml));

                // Append overall registration status 
                var rootActivity = this.Activities.GetRootActivity();
                var xmlStatus = new XmlElement("RS");

                // Satisfaction - pass/fail/unknown
                if (rootActivity.GetPrimaryObjective().ProgressStatus) {
                    var satisfactionStatus = rootActivity.GetPrimaryObjective().SatisfiedStatus ? "passed" : "failed";
                } else {
                    var satisfactionStatus = "unknown";
                }
                xmlStatus.AddAttribute("SS", satisfactionStatus);

                // Completion - complete/incomplete/unknown
                if (rootActivity.AttemptProgressStatus) {
                    var completionStatus = rootActivity.AttemptCompletionStatus ? "complete" : "incomplete";
                } else {
                    var completionStatus = "unknown";
                }
                xmlStatus.AddAttribute("CS", completionStatus);

                xmlStatus.AddAttribute("MS", ServerFormat.ConvertBoolean(rootActivity.GetPrimaryObjective().MeasureStatus));
                xmlStatus.AddAttribute("NM", rootActivity.GetPrimaryObjective().NormalizedMeasure);

                xmlStatus.AddAttribute("ED", ServerFormat.ConvertTimeSpan(rootActivity.ActivityExperiencedDurationTracked));

                xml.AddElement(xmlStatus);

                return "<?xml version=\"1.0\"?>" + xml.toString();
            }

            function Controller_IsThereDirtyData() {
                this.WriteAuditLog("Control IsThereDirtyData");

                for (var activity in this.Activities.ActivityList) {
                    if (this.Activities.ActivityList.hasOwnProperty(activity)) {
                        if (this.Activities.ActivityList[activity].IsAnythingDirty() ||
                            this.Activities.ActivityList[activity].DataState == DATA_STATE_POSTED) {
                            return true;
                        }
                    }
                }

                if (this.Sequencer.GlobalObjectives !== null && this.Sequencer.GlobalObjectives !== undefined) {
                    for (var globalObjective in this.Sequencer.GlobalObjectives) {
                        if (this.Sequencer.GlobalObjectives.hasOwnProperty(globalObjective)) {
                            dataState = this.Sequencer.GlobalObjectives[globalObjective].DataState;

                            if (dataState == DATA_STATE_DIRTY || dataState == DATA_STATE_POSTED) {
                                return true;
                            }
                        }
                    }
                }

                for (var bucket in this.SSPBuckets) {
                    if (this.SSPBuckets.hasOwnProperty(bucket)) {
                        if (this.SSPBuckets[bucket].DataState == DATA_STATE_DIRTY ||
                            this.SSPBuckets[bucket].DataState == DATA_STATE_POSTED) {

                            return true;

                        }
                    }
                }

                return false;
            }

            function Controller_MarkPostedDataClean() {
                this.WriteAuditLog("Control MarkPostedDataClean");

                for (var activity in this.Activities.ActivityList) {
                    if (this.Activities.ActivityList.hasOwnProperty(activity)) {
                        if (this.Activities.ActivityList[activity].DataState == DATA_STATE_POSTED) {
                            this.Activities.ActivityList[activity].DataState = DATA_STATE_CLEAN;
                        }

                        this.Activities.ActivityList[activity].MarkPostedObjectiveDataClean();
                    }
                }

                if (this.Sequencer.GlobalObjectives !== null && this.Sequencer.GlobalObjectives !== undefined) {
                    for (var globalObjective in this.Sequencer.GlobalObjectives) {
                        if (this.Sequencer.GlobalObjectives.hasOwnProperty(globalObjective)) {
                            dataState = this.Sequencer.GlobalObjectives[globalObjective].DataState;

                            if (dataState == DATA_STATE_POSTED) {
                                this.Sequencer.GlobalObjectives[globalObjective].DataState = DATA_STATE_CLEAN;
                            }
                        }
                    }
                }

                for (var bucket in this.SSPBuckets) {
                    if (this.SSPBuckets.hasOwnProperty(bucket)) {
                        if (this.SSPBuckets[bucket].DataState == DATA_STATE_POSTED) {
                            this.SSPBuckets[bucket].DataState = DATA_STATE_CLEAN;
                        }
                    }
                }
            }

            Control.MarkPostedDataDirty = Controller_MarkPostedDataDirty;
            Control.MarkPostedDataClean = Controller_MarkPostedDataClean;
            Control.MarkDirtyDataPosted = Controller_MarkDirtyDataPosted;
            Control.GetXmlForDirtyData = Controller_GetXmlForDirtyData;
            Control.IsThereDirtyData = Controller_IsThereDirtyData;
        </script>
		
        <script type="text/javascript">
            $(document).ready(function() {
                $('#ScormContent').load(function(e) {
                    var iframe = $('#ScormContent')[0];
                    var doc = iframe.contentDocument ?
                                iframe.contentDocument :
                                (iframe.contentWindow ? iframe.contentWindow.document : iframe.document);

                    var content = $(doc).find('frame[name=content]');

                    if (content.length) {
                        content.load(function(e) {
                            var frame = this;
                            var waitForArtisanLoad = function() {
                                if (!frame.contentWindow.Artisan ||
                                    !frame.contentWindow.Artisan.App ||
                                    !frame.contentWindow.Artisan.App.loaded) {

                                    setTimeout(waitForArtisanLoad, 100);
                                    return;
                                }
                                
                                var Artisan = frame.contentWindow.Artisan;
                                var state = Artisan.App.state;
                                var models = Artisan.Utilities.ns('Artisan.Models');
                                var ArtisanCourseCompletionMethod = frame.contentWindow.ArtisanCourseCompletionMethod;

                                Artisan.App.course.randomizePages = function (sectionConfig, max, answeredQuestions) {
                                    max = max ? max : sectionConfig.maxQuestions ? sectionConfig.maxQuestions : 20;

                                    // If its a single page quiz, the original page configs are all in combined pages. 
                                    // We will want to rearrange those, then update the combined page with the result, if it's
                                    // not a single page, we operate directly on the regular page configuration
                                    var pagesToUse = sectionConfig.isSinglePage ? sectionConfig.combinedPages : sectionConfig.pages;

                                    var hasIntro = pagesToUse[0].isIntro;
                                    var hasReview = pagesToUse[sectionConfig.pages.length - 1].isReview;

                                    var currentPages = pagesToUse.slice(hasIntro ? 1 : 0, hasReview ? pagesToUse.length - 1 : pagesToUse.length);
                                    var originalPages, usedPages;

                                    if (sectionConfig.originalPages) {
                                        originalPages = sectionConfig.originalPages;
                                    } else {
                                        originalPages = currentPages;
                                        sectionConfig.originalPages = originalPages;
                                    }

                                    if (sectionConfig.usedPages) {
                                        usedPages = sectionConfig.usedPages;
                                    } else {
                                        usedPages = {};
                                    }

                                    originalPages.sort(Artisan.Utilities.randomSort);
                                    var usedPagesArray = [];
                                    var newPages = [];
                                    var answeredPages = {};
                                    var originalPageMap = {};

                                    // Add in any previously answered questions first so we can make sure they are always available
                                    for (var i = 0; i < originalPages.length; i++) {
                                        originalPageMap[originalPages[i].id] = originalPages[i];
                                    }

                                    if (answeredQuestions && answeredQuestions.length > 0) {
                                        for (var i = 0; i < answeredQuestions.length; i++) {
                                            newPages.push(originalPageMap[answeredQuestions[i].id]);
                                            answeredPages[answeredQuestions[i].id] = true;
                                        }
                                    }

                                    for (var i = 0; i < originalPages.length; i++) {
                                        var p = originalPages[i];
                                        if (!usedPages.hasOwnProperty(p.id)) {
                                            // Exclude answered pages as well. Don't allow them in the 
                                            // used pages array as otherwise we could end up with duplicate pages.
                                            if (!answeredPages.hasOwnProperty(p.id)) {
                                                newPages.push(p);
                                            }
                                        } else {
                                            usedPagesArray.push(p);
                                        }
                                    }

                                    if (newPages.length > max) {
                                        newPages = newPages.slice(0, max);
                                    } else {
                                        var usedPagesRequired = max - newPages.length;
                                        for (var i = 0; i < usedPagesRequired && i < usedPagesArray.length; i++) {
                                            newPages.push(usedPagesArray[i]);
                                        }
                                        newPages.sort(Artisan.Utilities.randomSort);
                                    }

                                    $.each(newPages, function (i, p) {
                                        if (!usedPages.hasOwnProperty(p.id)) {
                                            usedPages[p.id] = true;
                                        }
                                    });

                                    if (hasIntro) {
                                        newPages.unshift(sectionConfig.pages[0]);
                                    }

                                    if (hasReview) {
                                        newPages.push(new models.ReviewPage(sectionConfig, sectionConfig.id, this.record));
                                    }

                                    if (this.config && (this.config.completionMethod == ArtisanCourseCompletionMethod.InlineQuestions || this.config.completionMethod == ArtisanCourseCompletionMethod.Navigation || this.config.completionMethod == ArtisanCourseCompletionMethod.Survey)) {
                                        var section = this.getLastSectionWithPages();
                                        if (section != null && section.getId() == sectionConfig.id) {
                                            newPages.push(new models.ReviewPage(section, section.getId(), this.record));
                                        }
                                    }

                                    sectionConfig.usedPages = usedPages;
            
                                    // Stick the pages back in the proper location depending on the learning object type
                                    if (sectionConfig.isSinglePage) {
                                        sectionConfig.combinedPages = newPages;
                
                                        // Note that this will have no effect on the page. 
                                        // We will need to reorder after the page renders
                                        // based on these combined pages. 

                                    } else {
                                        sectionConfig.pages = newPages;
                                    }

                                }
                            
                                Artisan.App.course.fixQuiz = function() {
                                    // Look for quizes
                                    var courseRecord = this.record;
                                    var me = this;
                                    var config = this.config;

                                    var sectionSearch = function (section) {
                                        for (var j = 0; j < section.sections.length; j++) {
                                            var innerSection = section.sections[j];

                                            if (innerSection.sections && innerSection.sections.length) {
                                                sectionSearch(innerSection);
                                            }

                                            if (innerSection.pages && innerSection.pages.length > 0 && innerSection.isQuiz) {
                                                if (innerSection.isSinglePage) { // If it's a single page quiz, update the stored page config instead
                                                    // we will randomize, and truncate those pages as needed,
                                                    // then copy the result to the displayed single page
                                                    innerSection.combinedPages[0].isIntro = true;
                                                } else {
                                                    innerSection.pages[0].isIntro = true;
                                                }
												
                                                if (innerSection.isRandomizeQuizQuestions) {
                                                    var requiredPages = [];
                                    
                                                    if (courseRecord.answeredQuestions.length > 0) {
                                                        requiredPages = $.grep(courseRecord.answeredQuestions, function (q) { return q.learningObjectId == innerSection.id });
                                                    }
                                                    me.randomizePages(innerSection, null, requiredPages);
                                                }
                                            }
                                        }
                                    }

                                    for (var i = 0; i < config.sections.length; i++) {
                                        // look for quizzes in scos
                                        if (config.sections[i].sections && config.sections[i].sections.length > 0) {
                                            if (config.sections[i].sections && config.sections[i].sections.length) {
                                                sectionSearch(config.sections[i]);
                                            }
                                        }
                                    }
                                };
                                
                                Artisan.App.course.fixQuiz();

                                Artisan.App.retryLearningObject = function () {
                                    var learningObject = state.learningObject;

                                    // Clear out the randomized question order as we want to 
                                    // rerandomize the order when we retry the quiz. 
                                    for (var i = 0; i < learningObject.pages.length; i++) {
                                        if (learningObject.pages[i].config.randomizedOrder) {
                                            delete learningObject.pages[i].config.randomizedOrder;
                                        }
                                    }

                                    if (learningObject.config.isReRandomizeOrder) {
                                        Artisan.App.course.randomizePages(learningObject.config);
                                    }

                                    // In the case of a quiz, and single page learning object where the quiz is 
                                    // the last learning object in the course, we might end up missing the final
                                    // review page for the course. If the pages in the LO has 1 more page than 
                                    // the config and it's a review page, make sure we copy it to the new 
                                    // learning object for retry.
                                    var isAddFinalReviewPage = false;
                                    if (learningObject.config.isSinglePage && learningObject.pages.length - 1 == learningObject.config.pages.length && learningObject.pages[learningObject.pages.length - 1].isReview) {
                                        isAddFinalReviewPage = true;
                                    }

                                    // Create a new learning object from the updated config which will generate the new page array
                                    // copy the new page array to the current learning object. 
                                    var newLearningObject = new Artisan.Models.Container(learningObject.config, learningObject.parent);
                                    learningObject.pages = newLearningObject.pages;
                                   
                                    if (isAddFinalReviewPage) {
                                        learningObject.pages.push(new Artisan.Models.ReviewPage(learningObject, learningObject.getId(), Artisan.App.course.record));
                                    }

                                    // Check for last learning object in course. 
                                    var lastLearningObject = Artisan.App.course.getLastSectionWithPages();
                                    if (lastLearningObject.getId() == learningObject.getId()) {
                                        // if this is a quiz, it must have 2 final review pages if this quiz is the last learning object of the course
                                        if (learningObject.config.isQuiz) {
                                            var lastPage = learningObject.getLastPage();
                                            var secondLastPage = learningObject.pages[learningObject.pages.length - 2];
                                            var isThereTwoReviewPagesForThisQuiz = lastPage.isReview && secondLastPage.isReview;

                                            if (!isThereTwoReviewPagesForThisQuiz) {
                                                learningObject.pages.push(new Artisan.Models.ReviewPage(learningObject, learningObject.getId(), Artisan.App.course.record));
                                            }
                                        }
                                    }


                                    Artisan.App.updateState({
                                        learningObject: learningObject,
                                        page: learningObject.getFirstPage()
                                    });

                                    $('.artisan-learningobject-title .artisan-content').html(learningObject.getName());
                                    if (learningObject.getName() == learningObject.getParent().getName()) {
                                        $('.artisan-learningobject-title').hide();
                                    } else {
                                        $('.artisan-learningobject-title').show();
                                    }
                                }

                                frame.contentWindow.$('.artisan-learningobject').die('click');
                                
                                frame.contentWindow.$('.artisan-learningobject').live('click', function (e) {
                                    e.stopPropagation();
                                    e.preventDefault();

                                    var menuItem = $(this);
                                    var firstParent = menuItem.parent();
                                    var topParent = firstParent ? firstParent.parent() : null;

                                    var learningObjectId = parseInt(menuItem.attr('learningObjectId'));

                                    if (topParent && topParent.hasClass("disabled")) {
                                        return;
                                    }

                                    // don't allow locked learning objects
                                    if (menuItem.hasClass('disabled')) {
                                        return;
                                    }

                                    Artisan.App.selectLearningObject(Artisan.App.course.getLearningObject(learningObjectId));
                                });
                            }

                            waitForArtisanLoad();

                        });
                    }

                });

                
            });
        </script>

	</head>
	
	<%
	int topFrameHeight = 0;
	if ((bool)Data.PackageProps.ShowNavBar == false){
		topFrameHeight = topFrameHeight - 20;
	}	
	%>
	<!-- AMP 07-31-08 Keep Control.Initialize() in this frameset. The Scorm 2004 LMS AutoTest depends on being able to call Control from here -->
	<frameset cols="8,*,8" frameborder=0 framespacing=0 onload="Control.Initialize();" onunload="Control.Unload();">
		<frame name="blackleft" id="blackleft" src="black.aspx" scrolling="no">
		<frameset id="ScormPageFrameset" rows="8,<%=topFrameHeight%>,0,*,0,8" frameborder="0" framespacing="0" border="0">
			<frame id="blacktop" name="blacktop" src="black.aspx" scrolling="no" noresize="true">
			<frame id="<%=Data.CosmeticInfo.OtherPages[0].FrameName%>" name="<%=Data.CosmeticInfo.OtherPages[0].FrameName%>" src="<%=Data.CosmeticInfo.OtherPages[0].PageHref + Data.CosmeticInfo.OtherPages[0].Parameters%>" scrolling="no" noresize="true" >
			<frame id="<%=Data.CosmeticInfo.OtherPages[2].FrameName%>" name="<%=Data.CosmeticInfo.OtherPages[2].FrameName%>" src="<%=Data.CosmeticInfo.OtherPages[2].PageHref + Data.CosmeticInfo.OtherPages[2].Parameters%>" scrolling=no border="0" frameborder=0 framespacing=0>
			<frameset id="ScormMainFrameset" cols="<%=Data.CourseStructureInitializationWidth%>,*" border="0" frameborder=0 framespacing=0 >
				<frame id="<%=Data.CosmeticInfo.OtherPages[1].FrameName%>" name="<%=Data.CosmeticInfo.OtherPages[1].FrameName%>" src="<%=Data.CosmeticInfo.OtherPages[1].PageHref + Data.CosmeticInfo.OtherPages[1].Parameters%>" marginheight=0 marginwidth=0 scrolling=auto>
				<!--
				This frame must have a name identical to the CONTENT_FRAME_NAME constant in the client-side integration file. Must also be a sub-window to the window where Control is.
				-->
				<frame id="<%=Data.CosmeticInfo.ContentFrame.FrameName%>" name="<%=Data.CosmeticInfo.ContentFrame.FrameName%>" src="<%=Data.CosmeticInfo.IntermediatePage.PageHref + Data.CosmeticInfo.IntermediatePage.Parameters%>&SignalLoaded=false" marginheight=0 marginwidth=0>
			</frameset>
			<frame id="bottomNav" name="bottomNav" src="bottom.aspx" scrolling=no>
			<frame id=blackbottom name=blackbottom src="black.aspx" scrolling=no>
		</frameset>
		<frame name="blackright" id="blackright" src="black.aspx" scrolling=no>
		<noframes>Your browser must support frames to continue.</noframes>		
	</frameset>
</html>	