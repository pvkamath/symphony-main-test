<%@ Page language="c#" ValidateRequest="false" AutoEventWireup="true" %>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>top</title>
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" href="<%=Integration.Implementation.GetUrlToStyleSheet(Request) %>" type="text/css" />
    <script language="cs" runat="server">
        private string courseTitle = "";
		private bool showFinish = true;
		private bool showClose = true;
		private bool showProgress = true;
        private bool useMeasureProgress = true;
		private bool showHelp = true;
		private bool enableFlow = true;
		private bool showNavBar = true;
		private bool showTitleBar = true;
		private bool showCourseStructure = true;
		private string strCourseStructureWidth = "";
		private string strMenu_ToggleMenu = "";
		private string strMenu_Previous = "";
		private string strMenu_Next = "";
		private string strMenu_CloseSco = "";
		private string strMenu_ReturnToLMS = "";
		private string strMenu_Help = "";

		protected void Page_Load(object sender, System.EventArgs e)
		{
			Integration.Implementation.SetCulture(Request);
			
            if(!String.IsNullOrEmpty(Request.QueryString["courseTitle"]))
            {
			courseTitle = Request.QueryString["courseTitle"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["showFinish"]))
            {
                showFinish = CommonFunctions.ConvertStringToBoolean(Request.QueryString["showFinish"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["showClose"]))
            {
                showClose = CommonFunctions.ConvertStringToBoolean(Request.QueryString["showClose"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["showProgress"]))
            {
                showProgress = CommonFunctions.ConvertStringToBoolean(Request.QueryString["showProgress"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["useMeasureProgress"]))
            {
                useMeasureProgress = CommonFunctions.ConvertStringToBoolean(Request.QueryString["useMeasureProgress"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["showHelp"]))
            {
                showHelp = CommonFunctions.ConvertStringToBoolean(Request.QueryString["showHelp"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["enableFlow"]))
            {
                enableFlow = CommonFunctions.ConvertStringToBoolean(Request.QueryString["enableFlow"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["showNavBar"]))
            {
                showNavBar = CommonFunctions.ConvertStringToBoolean(Request.QueryString["showNavBar"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["showTitleBar"]))
            {
                showTitleBar = CommonFunctions.ConvertStringToBoolean(Request.QueryString["showTitleBar"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["showCourseStructure"]))
            {
                showCourseStructure = CommonFunctions.ConvertStringToBoolean(Request.QueryString["showCourseStructure"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["courseStructureWidth"]))
            {
                strCourseStructureWidth = Request.QueryString["courseStructureWidth"];
            }
			//get parameters (initially external reg id and external config..later on, add ability to launch maniest or package directly)
			string config = Request.QueryString["configuration"];
			ExternalConfiguration externalConfig = Integration.Implementation.GetExternalConfigurationObject();
			externalConfig.ParseFromString(config);

			// TopMenu options
			strMenu_ToggleMenu = Integration.Implementation.GetString("Hide Menu", externalConfig).ToUpper();
			strMenu_Previous = Integration.Implementation.GetString("Previous", externalConfig).ToUpper();
			strMenu_Next = Integration.Implementation.GetString("Next", externalConfig).ToUpper();
			strMenu_CloseSco = Integration.Implementation.GetString("Close Item", externalConfig).ToUpper();
			strMenu_ReturnToLMS = Integration.Implementation.GetString("Return to LMS", externalConfig).ToUpper();
			strMenu_Help = Integration.Implementation.GetString("Help", externalConfig).ToUpper();

		}

		/// <summary>
		/// The title of the package being delivered
		/// </summary>
		public string CourseTitle
		{
			get { return Server.HtmlEncode(courseTitle); }
		}
		
		/// <summary>
		/// The text displayed for the show/hide menu option
		/// </summary>
		public string menu_ToggleMenu
		{
			get{ return strMenu_ToggleMenu;	}
		}
		
		/// <summary>
		/// The text displayed for the Previous menu option.
		/// </summary>
		public string menu_Previous
		{
			get{ return strMenu_Previous; }
		} 
		
		/// <summary>
		/// The text displayed for the Next menu option.
		/// </summary>
		public string menu_Next
		{
			get{ return strMenu_Next; }
		} 
		
		/// <summary>
		/// The text displayed for the CloseSco menu option.
		/// </summary>
		public string menu_CloseSco
		{
			get{ return strMenu_CloseSco; }
		} 
		
		/// <summary>
		/// The text displayed for the Return To LMS menu option.
		/// </summary>
		public string menu_ReturnToLMS
		{
			get{ return strMenu_ReturnToLMS; }
		} 

		/// <summary>
		/// The text displayed for the Help menu option.
		/// </summary>
		public string menu_Help
		{
			get{ return strMenu_Help; }
		} 

		/// <summary>
		/// Specifies whether or not the Return to LMS button should be shown
		/// </summary>
		public bool ShowFinish
		{
			get { return showFinish; }
		}

		/// <summary>
		/// Specifies whether or not the Close SCO button should be shown.
		/// </summary>
		public bool ShowClose
		{
			get { return showClose; }
		}

		/// <summary>
		/// Specifies whether the progress bar should be shown
		/// </summary>
		public bool ShowProgress
		{
			get { return showProgress; }
		}

        /// <summary>
        /// Specifies whether the progress bar should reflect progress measure or SCO completion
        /// </summary>
        public bool UseMeasureProgress
        {
            get { return useMeasureProgress; }
        }

        /// <summary>
        /// Returns the image id to use for the progress bar.
        /// </summary>
        public string ProgressImageId
        {
            get
            {
                if (UseMeasureProgress)
                    return "measureProgressImage";
                else
                    return "progressImage";
                        
            }
        }

		/// <summary>
		/// Specifies whether the help button should be shown
		/// </summary>
		public bool ShowHelp
		{
			get { return showHelp; }
		}
		
		/// <summary>
		/// Returns the course strucure width defined for this course
		/// </summary>
		public string CourseStructureWidth
		{
			get { return strCourseStructureWidth; }
		}

		/// <summary>
		/// Specifies whether the previoud and next buttons should be shown
		/// </summary>
		public bool EnableFlow
		{
			get { return enableFlow; }
		}

		/// <summary>
		/// Specifies whether the navigation bar should be shown
		/// </summary>
		public bool ShowNavBar
		{
			get { return showNavBar; }
		}

		/// <summary>
		/// Specifies whether the title bar should be shown
		/// </summary>
		public bool ShowTitleBar
		{
			get { return showTitleBar; }
		}

		/// <summary>
		/// Specifies whether the course outline should be shown
		/// </summary>
		public bool ShowCourseStructure
		{
			get { return showCourseStructure; }
		}

		
    </script>
    <script language="javascript">
    window.document.onkeypress = CheckKeyPress;
    function CheckKeyPress(e){
		
		var intKeyCode = 0;			
		if (window.event) {
			e = window.event;
			intKeyCode = e.keyCode;
		}
		else {
			intKeyCode = e.which;
		}
    
		window.parent.Control.CheckForDebugCommand(intKeyCode);
    }
    
    if (window.location.toString().toLowerCase().indexOf("preventrightclick=true") > 0){
		window.document.oncontextmenu = function(){return false;};
	}
	
	function initializeTopControls()
	{
		// If Netscape pre 7.1, disable hide/show due to bug in Netscape rendering
		if ( UserBrowserIsNetscapeVerOlderThan7_1() )
		{
			document.getElementById("menuToggle").style.display = "none";
		}
	}
	
	function UserBrowserIsNetscapeVerOlderThan7_1()
	{
		var result = false;
		var netscapeLocation = navigator.userAgent.indexOf("Netscape/");

		if (netscapeLocation > -1)
		{
			// The version number follows the slash.
			var versionString = navigator.userAgent.substr(netscapeLocation + 9);
			var versionParts = versionString.split('.');
			try
			{
				var majorVersion = versionParts[0]; 
				var minorVersion = "." + versionParts[1];

				// Look for Netscape versions earlier than 7.1
				if (majorVersion < 7 || (majorVersion == 7 && minorVersion < 1) )
				{
					result = true;
				}
			}
			catch(e)
			{
				result = false;
			}
		}

		return result;
	}	

    </script>
  </head>
  <body id="top_body" class="playerBody" onload="initializeTopControls()">
    <form id="Form1" method="post" action="#">
    
<table id="top_navMenu">
<%
if (this.ShowNavBar){
%>
<tr>
	<td width="<%=this.CourseStructureWidth%>">
	<%if (this.ShowCourseStructure){%>
		<div id="menuToggle" class="enabledTopMenuItem" onclick="window.parent.Control.ToggleMenuVisibility();" 
			title="<%=menu_ToggleMenu%>" onmouseover="window.status='<%=menu_ToggleMenu%>';return true;" 
			onmouseout="window.status='';return true;"> <%=menu_ToggleMenu%>
		</div>
	<%}%>
	</td>

	<%if (this.ShowProgress){%>
	<td>
		<table height="15" border="0" cellpadding=0 cellspacing=0>
			<tr height=15>
			    <td height=15 width="105" class="progress"><IMG id="<%=ProgressImageId %>" class="progressXImage" height="6" src="images/progressY.gif" width="0"><IMG id="totalProgressImage" class="progressYImage" height="6" src="images/progressY.gif" width="100"></td>
				<td id="progressText" align="left" class="progressText"></td>
			</tr>
		</table>
		
	</td>
	<%}%>
	
	<!-- AMP 07-30-08 Do not change the ids of the previous or next button, and do not change the class attribute from enabledTopMenuItem or disabledTopMenuItem, because the Scorm 2004 LMS Conformance Test depends on these -->
	<td><%if (this.EnableFlow){%><div id="previous" class="enabledTopMenuItem" onclick="window.parent.Control.Previous();" title="<%=menu_Previous%>" onmouseover="window.status='<%=menu_Previous%>';return true;" onmouseout="window.status='';return true;"><- <%=menu_Previous%></div><%}%></td>
	<td><%if (this.EnableFlow){%><div id="next" class="enabledTopMenuItem" onclick="window.parent.Control.Next();" title="<%=menu_Next%>" onmouseover="window.status='<%=menu_Next%>';return true;" onmouseout="window.status='';return true;"><%=menu_Next%> -></div><%}%></td>
	<td><%if (this.ShowClose){%><div id="closeSco" class="enabledTopMenuItem" onclick="window.parent.Control.CloseSco();" title="<%=menu_CloseSco%>" onmouseover="window.status='<%=menu_CloseSco%>';return true;" onmouseout="window.status='';return true;"><%=menu_CloseSco%></div><%}%></td>
	<td><%if (this.ShowFinish){%><div id="returnToLms" class="enabledTopMenuItem" onclick="window.parent.Control.TriggerReturnToLMS();" title="<%=menu_ReturnToLMS%>" onmouseover="window.status='<%=menu_ReturnToLMS%>';return true;" onmouseout="window.status='';return true;"><%=menu_ReturnToLMS%></div><%}%></td>
	<td><%if (this.ShowHelp){%><div id="help" class="enabledTopMenuItem" onclick="window.open('help.aspx')" title="<%=menu_Help%>" onmouseover="window.status='<%=menu_Help%>';return true;" onmouseout="window.status='';return true;"><%=menu_Help%></div><%}%></td>
</tr>
<%}%>
</table>

<%if (this.ShowTitleBar) { %>
    <div id="top_title"><%=CourseTitle%></div>
    <div id="top_credit" ><a target="_blank" href="http://www.scorm.com/scorm-solved/scorm-engine/">POWERED BY THE SCORM ENGINE</a></div>
<% } %>

     </form>


  </body>
</html>
