<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import namespace="RusticiSoftware.ScormContentPlayer.Logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
    <title>Send Log to Support</title>
    
    <link id="stylesheet" rel="stylesheet" runat="server" type="text/css" />
    
    <script language="cs" runat="server">
        
        string url = "";    
    
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string log = Request.Params["log"];
                log = log = HttpUtility.HtmlDecode(log);
                string reg = Request.Params["registrationId"];
                string configString = Request.Params["configuration"]; // not doing anything with this currently

                stylesheet.Href = Integration.Implementation.GetUrlToStyleSheet(Request);

                // Post to central s3-backed service for viewing by Rustici Software support
                System.Net.WebClient webClient = new System.Net.WebClient();
                webClient.Headers.Add("Content-Type", "application/xml");
                webClient.Headers.Add("Referer", Request.UrlReferrer.Host);
                webClient.Encoding = Encoding.UTF8;

                try
                {
                    url = webClient.UploadString("http://cloud.scorm.com/EngineWebServices/logs/?registration=" + Server.UrlEncode(reg), "POST", log);
                }
                catch (Exception ex)
                {
                    url = "Sorry, an error occurred while sending your log to support. ERROR: " + ex.Message;
                }

                logUrl.Value = url;

                // Repopulate form
                ExternalRegistrationId externalReg = Integration.Implementation.GetExternalRegistrationIdObject();
                externalReg.ParseFromString(reg);
                ExternalConfiguration externalConfig = Integration.Implementation.GetExternalConfigurationObject();
                externalConfig.ParseFromString(configString);
                Learner learner = Integration.Implementation.GetLearnerInformation(externalReg, externalConfig);

                name.Value = learner.FirstName + " " + learner.LastName + " [" + learner.Id + "]";
                company.Value = Request.UrlReferrer.Host;
                configuration.Value = configString;

            }
            else
            {
                string configString = Request.Params["configuration"];
                ExternalConfiguration externalConfig = Integration.Implementation.GetExternalConfigurationObject();
                externalConfig.ParseFromString(configString);
                
                // Gather up all values and send them to support
                Hashtable data = new Hashtable();

                string name = Request.Params["name"];
                string email = Request.Params["email"];
                string company = Request.Params["company"];
                string description = Request.Params["description"];
                string logUrl = Request.Params["logUrl"];

                data.Add("name", name);
                data.Add("email", email);
                data.Add("company", company);
                data.Add("description", description);
                data.Add("logUrl", logUrl);


                Integration.Implementation.SendToSupport(data, externalConfig);

                Response.Write("<script>alert('Error report submitted successfully. Thank you.');window.close();</" + "script>");
            }
        }
	</script>   
</head>

<body>
    <form id="form1" runat="server" action >
    <div>
    <H3 >
    Support Request
    </H3>
    <table>
        <tr>
            <td>Name</td>
            <td><input runat="server" id="name" type="text" size="60" /></td>
            </tr>
        <tr>
            <td>Email (required)</td>
            <td><input runat="server" id="email" type="text" size="60" /></td>
        </tr>
        <tr>
            <td>Company / Domain Name</td>
            <td><input runat="server" id="company" type="text" size="60" /></td>
        </tr>
        <tr>
            <td>Log URL</td>
            <td><input readonly runat="server" id="logUrl" type="text" size="60" />
            <input runat="server" id="configuration" type="hidden" size="60" /></td>
        </tr>
         <tr>
            <td>Problem Description</td>
            <td><textarea id="description" cols="45" rows="18"></textarea></td>
        </tr>
        <tr>
            <td colspan=2 align="right" ><input type="submit" value="submit" /></td>
        </tr>
        
    
    </table>
    </form>
    
</body>
</html>
