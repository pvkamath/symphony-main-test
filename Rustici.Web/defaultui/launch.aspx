<%@ Page language="c#" AutoEventWireup="false"%>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<script language=CS runat=server>
			protected LaunchResponse Data;

			private void Page_Load(object sender, System.EventArgs e)
			{
				LaunchRequest lr = new LaunchRequest();
				Data = lr.Process(Request, Response);
			}

			override protected void OnInit(EventArgs e)
			{
				InitializeComponent();
				base.OnInit(e);
			}
			
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{    
				this.Load += new System.EventHandler(this.Page_Load);
			}
		</script>	
	
		<title>SCORM Launch Page</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script>
<%=Data.ScpRequiredCode%>
		</script>
	</head>
	<body onload="Load();" onbeforeunload="Unload();" onunload="Unload();">
		<form id="frmPopupLauncher">
			<div id="PleaseClick" align="center" style="visibility:hidden">
				<h3><a href="" onclick="LaunchCourse();return false;"><%=Data.ClickToLaunch%></a></h3>
			</div>
			<div id="Message" align="center" style="visibility:hidden">
				<h3><%=Data.CourseLaunchedMessage%></h3>
			</div>
			<br>
			<h3 align="center"><a href="<%=Data.ExitUrl%>"><%=Data.ReturnToLmsMessage%></a></h3>
		</form>
	</body>
</html>