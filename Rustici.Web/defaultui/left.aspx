<%@ Page language="c#" ValidateRequest="false" AutoEventWireup="true" %>
<%@ Import namespace="RusticiSoftware.ScormContentPlayer.Logic"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title></title>
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" href="<%=Integration.Implementation.GetUrlToStyleSheet(Request) %>" type="text/css" >
	<script>
    
    window.document.onkeypress = CheckKeyPress;
    
    function CheckKeyPress(e){
		
		var intKeyCode = 0;			
		if (window.event) {
			e = window.event;
			intKeyCode = e.keyCode;
		}
		else {
			intKeyCode = e.which;
		}
    
		window.parent.Control.CheckForDebugCommand(intKeyCode);
    }
    
    if (window.location.toString().toLowerCase().indexOf("preventrightclick=true") > 0){
		window.document.oncontextmenu = function(){return false;};
	}
    </script>    
  </head>
  <body id="left_body">
	
    <form id="Form1" method="post" style="">
	<!-- AMP 07-31-08 Do not change the id of this div element, and do not remove it. The Scorm 2004 AutoTest assumes that there is a div element here with id="MenuPlaceHolder" -->
	<div id="MenuPlaceHolder" style="LEFT: 0px; POSITION: relative; TOP: 10px;"></div>
    </form>
	
  </body>
</html>
