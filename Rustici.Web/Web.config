﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" />
    <section name="rewriter" requirePermission="false" type="Intelligencia.UrlRewriter.Configuration.RewriterConfigurationSectionHandler, Intelligencia.UrlRewriter" />
    <section name="SubSonicService" type="SubSonic.SubSonicSection, SubSonic" requirePermission="false" />
  </configSections> 
  <appSettings file="..\appSettings.config">
    <add key="releasehounds.ServerSideUiLogic" value="true" />
    <add key="releasehounds.ServerSideUiLogic.Portal" value="true" />
  </appSettings> 
  <connectionStrings configSource=".\config\connections.development.config" />
  <!-- FILE-BASED LOGGING SECTION (VIA LOG4NET)
       Set level to "DEBUG" for the most messages, "INFO" for high-level messages and "ERROR" for errors only.
       This level can be changed at runtime so it's possible to briefly change the logging level without 
       restarting the web application or IIS.
       Set the appender File param to the log file path.
       * Note that the levels are cumulative so DEBUG will actually log INFO and ERROR level message as well.
       The log4net section can be commented out to essentially disable file-based logging.
  --> 
  <log4net>  
    <root> 
      <level value="DEBUG" /> 
      <appender-ref ref="LogFileAppender" />
    </root>
    <appender name="LogFileAppender" type="log4net.Appender.RollingFileAppender">
      <param name="File" value="C:\\logs\\scormengine-log.txt" />
      <param name="AppendToFile" value="true" /> 
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="240" />
      <maximumFileSize value="10MB" />
      <staticLogFileName value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <param name="ConversionPattern" value="%-5p %d{yyyy-MM-dd hh:mm:ss} – %m%n" />
      </layout>
    </appender>
  </log4net>
  <SubSonicService defaultProvider="Symphony">
    <providers>
      <clear />
      <add name="Symphony" type="SubSonic.SqlDataProvider, SubSonic" connectionStringName="Symphony" generatedNamespace="Symphony.Data" generateNullableProperties="false" useExtendedProperties="true" excludeTableList="DDLChangeLog, aspnet_*" regexDictionaryReplace="^UserName,UserNameX" />
    </providers>
  </SubSonicService>
  <system.webServer>
		<handlers>
			<remove name="WebSync" />
		</handlers>
		<modules>
			<remove name="WebSyncNewRelicIgnoreHttpModule" />
		</modules>
	</system.webServer>
	<system.web>
		<httpHandlers>
			<remove path="websync.ashx" verb="*" />
			<remove path="Telerik.Web.UI.WebResource.axd" verb="*" />
		</httpHandlers>
		<httpModules>
			<remove name="WebSyncNewRelicIgnoreHttpModule" />
		</httpModules>
		
		<!--  DYNAMIC DEBUG COMPILATION
          Set compilation debug="true" to enable ASPX debugging.  Otherwise, setting this value to
          false will improve runtime performance of this application. 
          Set compilation debug="true" to insert debugging symbols (.pdb information)
          into the compiled page. Because this creates a larger file that executes
          more slowly, you should set this value to true only when debugging and to
          false at all other times. For more information, refer to the documentation about
          debugging ASP.NET files.
    -->
		<compilation defaultLanguage="c#" debug="true" targetFramework="4.0">
			<assemblies>
				<add assembly="System.Data.OracleClient, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
				<add assembly="System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" />
			</assemblies>
		</compilation>
		<!--  CUSTOM ERROR MESSAGES
          Set customErrors mode="On" or "RemoteOnly" to enable custom error messages, "Off" to disable. 
          Add <error> tags for each of the errors you want to handle.

          "On" Always display custom (friendly) messages.
          "Off" Always display detailed ASP.NET error information.
          "RemoteOnly" Display custom (friendly) messages only to users not running 
           on the local Web server. This setting is recommended for security purposes, so 
           that you do not display application detail information to remote clients.
    -->
		<customErrors mode="Off" defaultRedirect="/ScormEngine/defaultui/error.aspx" />
		<!--  AUTHENTICATION 
          This section sets the authentication policies of the application. Possible modes are "Windows", 
          "Forms", "Passport" and "None"

          "None" No authentication is performed. 
          "Windows" IIS performs authentication (Basic, Digest, or Integrated Windows) according to 
           its settings for the application. Anonymous access must be disabled in IIS. 
          "Forms" You provide a custom form (Web page) for users to enter their credentials, and then 
           you authenticate them in your application. A user credential token is stored in a cookie.
          "Passport" Authentication is performed via a centralized authentication service provided
           by Microsoft that offers a single logon and core profile services for member sites.
    -->
		<authentication mode="None" />
		<!--  AUTHORIZATION 
          This section sets the authorization policies of the application. You can allow or deny access
          to application resources by user or role. Wildcards: "*" mean everyone, "?" means anonymous 
          (unauthenticated) users.
    -->
		<authorization>
			<allow users="*" />
			<!-- Allow all users -->
			<!--  <allow     users="[comma separated list of users]"
                             roles="[comma separated list of roles]"/>
                  <deny      users="[comma separated list of users]"
                             roles="[comma separated list of roles]"/>
            -->
		</authorization>
		<!--  APPLICATION-LEVEL TRACE LOGGING
          Application-level tracing enables trace log output for every page within an application. 
          Set trace enabled="true" to enable application trace logging.  If pageOutput="true", the
          trace information will be displayed at the bottom of each page.  Otherwise, you can view the 
          application trace log by browsing the "trace.axd" page from your web application
          root. 
    -->
		<trace enabled="true" requestLimit="500" pageOutput="false" traceMode="SortByTime" localOnly="true" />
		<!--  SESSION STATE SETTINGS
          By default ASP.NET uses cookies to identify which requests belong to a particular session. 
          If cookies are not available, a session can be tracked by adding a session identifier to the URL. 
          To disable cookies, set sessionState cookieless="true".
    -->
		<sessionState mode="Off" />
		<!--  GLOBALIZATION
          This section sets the globalization settings of the application. 
    -->
		<globalization requestEncoding="utf-8" responseEncoding="utf-8" />
		<!-- Web Service posts of 20MB to allow for large manifests -->
		<httpRuntime maxRequestLength="20480" executionTimeout="300" />
		<!-- SCORM/AICC Courses can easily send odd looking data.  We don't want ASP.NET stopping them -->
		<pages validateRequest="false" controlRenderingCompatibilityVersion="3.5" clientIDMode="AutoID" />
		<xhtmlConformance mode="Legacy" />
	</system.web>
	<!-- For hosted debug log persistance -->
	<system.net>
		<settings>
			<servicePointManager expect100Continue="false" />
		</settings>
		
	</system.net>
  <rewriter>
    <redirect url="artisan_course_hook.js(\?(.+))" to="http://localhost:63997/Handlers/ArtisanCourseHookHandler.ashx$1" />
  </rewriter>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="MySql.Data" publicKeyToken="c5687fc88969c44d" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="ICSharpCode.SharpZipLib" publicKeyToken="1b03e6acf1164f73" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-0.85.5.452" newVersion="0.85.5.452" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="GoogleAnalyticsTracker.Core" publicKeyToken="04ab204e84b117c0" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.1.27.0" newVersion="4.1.27.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.0.1.0" newVersion="3.0.1.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
</configuration>