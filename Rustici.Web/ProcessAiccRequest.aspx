<%@ Page language="c#" %>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>

<html>
<script language="cs" runat="server">
protected void Page_Load(object sender, EventArgs e)
{
    if (!Integration.Implementation.IsAllowed(Request, SecureMethod.RECORD_AICC_RESULTS_WEB_SERVICE))
    {
        throw new ScormContentPlayerApplicationException("Access denied to ProcessAiccRequest (Central)");
    }

    string responseString;
    bool errorPresent = false;

	bool useWebServices;
	try
	{
		useWebServices = CommonFunctions.ConvertStringToBoolean(Integration.Implementation.GetConfigurationSetting(Constants.CONFIG_USE_CROSS_DOMAIN_WEB_SERVICES));
	}
	catch (ScormContentPlayerApplicationException)
	{
        useWebServices = CommonFunctions.ConvertStringToBoolean(Integration.Implementation.GetConfigurationSetting(Constants.CONFIG_USE_WEB_SERVICES));
	}

    if (useWebServices)
    {
        string centralAiccRequestPage = Integration.Implementation.GetUrlToCentralAiccRequestProcessor(Request);
        responseString = RusticiSoftware.ScormContentPlayer.Util.Communications.MakeHttpPostRequest(centralAiccRequestPage, Request.Form);
    }
    else
    {
        AiccResponse aiccResponse;

        try
        {
            AiccRequest aiccRequest = new AiccRequest(Request);

            Integration.Implementation.LogAudit("ProcessAiccRequest page called with command: " + aiccRequest.Command, aiccRequest.ExternalConfig);
            Integration.Implementation.LogDetail(aiccRequest.AiccData, aiccRequest.ExternalConfig);
            
            aiccResponse = AiccRequestProcessor.Process(aiccRequest);
            responseString = aiccResponse.ToString();

            if (aiccResponse.ErrorCode > 0)
            {
                Integration.Implementation.LogAudit("Error Processing " + aiccRequest.Command + ": " + aiccResponse.ErrorText, aiccRequest.ExternalConfig);
                errorPresent = true;
            }
            else
            {
                Integration.Implementation.LogAudit("Sending response: " + responseString, aiccRequest.ExternalConfig);
            }
        }
        catch (Exception ex)
        {
            aiccResponse = new AiccResponse(AiccResponse.AICC_ERROR_INVALID_COMMAND, AiccResponse.AICC_ERROR_INVALID_COMMAND_TEXT, "", ex.Message);
            responseString = aiccResponse.ToString();

            //use response.end to trigger the transaction to rollback
            Integration.Implementation.LogError("Error processing AICC Request, sending response-" + responseString, ex);
            errorPresent = true;
        }
    }

    Response.Write(responseString);
    Response.Flush();

    // use response.end to trigger the automatic transaction to abort
    if (errorPresent)
    {
        Response.End();
    }
}
</script>
</html>