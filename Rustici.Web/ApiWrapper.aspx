<%@ Page language="c#" %>
<%@ Import Namespace="RusticiSoftware.ScormContentPlayer.Logic" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Course</title>
		<script language="cs" runat="server">
		private string scoUrl;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			scoUrl = Request.QueryString["scoUrl"];
		}

		/// <summary>
		/// Encoded url to the SCO
		/// </summary>
		public string ScoUrl
		{
			get { return JavaScript.Encode(scoUrl); }
		}
		</script>
	<script>
	var API = null;
	var API_1484_11 = null;
	
	scoUrl = "<%=ScoUrl%>";
	
	function LoadContent(){
	
		API = ScanParentsForApi(window.opener);
		API_1484_11 = ScanParentsFor2004Api(window.opener);
		
		window.contentWindow.location = scoUrl;
	}

	function ScanParentsForApi(win) 
	{ 
		var MAX_PARENTS_TO_SEARCH = 500; 
		var nParentsSearched = 0;
	
		while ( (win.API == null) && 
				(win.parent != null) && (win.parent != win) && 
				(nParentsSearched <= MAX_PARENTS_TO_SEARCH) 
			)
		{ 
					
			nParentsSearched++; 
			win = win.parent;
		} 
		
		return win.API; 
	} 
	function ScanParentsFor2004Api(win) 
	{ 
		var MAX_PARENTS_TO_SEARCH = 500; 
		var nParentsSearched = 0;
	
		while ( (win.API_1484_11 == null) && 
				(win.parent != null) && (win.parent != win) && 
				(nParentsSearched <= MAX_PARENTS_TO_SEARCH) 
			)
		{ 
					
			nParentsSearched++; 
			win = win.parent;
		} 
		
		return win.API_1484_11; 
	} 	
	</script>
	</HEAD>
	<frameset rows="100%" onload="LoadContent();">
	<frame name="contentWindow" src="">
	</frameset>
</HTML>
