<%@ language="C#" %>
<%@ Import Namespace="Symphony.Core" %>
<%@ Import Namespace="Symphony.Core.Controllers" %>
<%@ Import Namespace="Symphony.Web" %>
<%@ Import Namespace="Symphony.Core.Models" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="Hangfire" %>
<script runat="server">
        private BackgroundJobServer _backgroundJobServer;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_Start(Object sender, EventArgs e)
		{
            
            GlobalConfiguration.Configuration.UseSqlServerStorage(ConfigurationManager.ConnectionStrings["Symphony"].ConnectionString);
           
		}
 /// <summary>
 /// 
 /// </summary>
 /// <param name="sender"></param>
 /// <param name="e"></param>
		protected void Session_Start(Object sender, EventArgs e)
		{

		}
/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
            try
            {
                HttpApplication app = (HttpApplication)sender;
                HttpCookie cookie = HttpContext.Current.Request.Cookies.Get("Symphony.Auth");
                AuthUserData userData = ApplicationProcessor.GetUserData(cookie);
                
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_CUSTOMER] = userData.ApplicationName;
                HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER] = new object();
                
                app.Context.Items[ApplicationProcessor.CONTEXT_USERDATA] = userData;
            }
            catch (Exception ex)
            {
                LogManager.GetLogger("Global_Application_BeginRequest").Error("Could not obtain symphony authentication data - " + ex.Message);
            }
		}
/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}
/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}
/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
		protected void Application_Error(Object sender, EventArgs e)
		{
            RusticiSoftware.ScormContentPlayer.Logic.Integration.Implementation.LogError("Uncaught Exception occurred in the SCORM Content Player", Context.Server.GetLastError());
		}
/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
		protected void Session_End(Object sender, EventArgs e)
		{

		}
/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
		protected void Application_End(Object sender, EventArgs e)
		{
           
		}
</script>