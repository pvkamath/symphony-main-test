function Log(_1,_2){
this.isXmlBased=false;
this.children=new Array();
this.info=new Array();
this.version=_1;
this.includeTimestamps=_2;
}
Log.prototype.startNew=function(_3,_4,_5){
var _6;
var _7;
if(typeof _4=="string"){
_7=_4;
_6=new LogEntry(this.includeTimestamps,_7);
}else{
if(_4!==null&&_4!==undefined){
_7=_4.callee.toString().substring(9,_4.callee.toString().indexOf("{")-1);
_6=new LogEntry(this.includeTimestamps,_7,_4);
}else{
_6=new LogEntry(this.includeTimestamps,"unknown entry point");
}
}
_6.type=_3;
this.children[this.children.length]=_6;
return _6;
};
Log.prototype.display=function(){
var _8=window.open("text/plain");
_8.document.write("<style type='text/css'> body, td, th { font-family: Verdana; font-size: xx-small; color: black}"+"h2 { font-size: medium; font-family: Arial; color: navy }"+""+".s { color: red }"+".r {color: blue }"+".c {color: green }"+".l {color: purple }"+".ss {color: orange }"+".info {color: black }"+""+"div { margin-left: 20px;}"+"div.details { display: none; }"+""+"link {cursor: hand}"+""+"a, a#visited {color: black; text-decoration: none}"+"a#hover {color: blue} </style>");
_8.document.write("<table cellspacing='0' cellpadding='0' width='100%'>"+"<tr>"+"\t<td>"+"\t\t<h2>SCORM Engine Log</h2>"+"\t\t<div>v "+this.version+"</div>"+"\t</td>"+"\t<td valign='top' align='center'>"+"\t\t<span class='c'>Control </span>"+"\t\t<span class='r'>Runtime </span>"+"\t\t<span class='s'>Sequencing </span>"+"\t\t<span class='l'>Look-ahead </span>"+"       <span class='ss'>Readable Sequencing </span>"+"\t</td>"+"</tr>"+"</table>");
for(var i=0;i<this.children.length;i++){
renderLogEntry(this.children[i],_8);
}
_8.document.close();
};
function renderLogEntry(le,_b){
_b.document.write("<div class='"+le.type+"'>");
if(le.includeTimestamps&&le.timestamp!==undefined){
var _c=le.timestamp.getHours()+":"+le.timestamp.getMinutes()+":"+le.timestamp.getSeconds()+"."+le.timestamp.getMilliseconds();
_b.document.write("["+_c+"] ");
}
var _d=new LogCompression(window.dictionary_ll);
_b.document.write(_d.decompressString(le.func));
if(le.returnValue!==undefined){
_b.document.write(" returned '"+le.returnValue+"'");
}
if(le.elapsedTime!==undefined){
_b.document.write(" in "+le.elapsedTime+" seconds");
}
for(var i=0;i<le.info.length;i++){
_b.document.write("<div class='info'>");
_b.document.write(_d.decompressString(le.info[i]));
_b.document.write("</div>");
}
for(var i=0;i<le.children.length;i++){
renderLogEntry(le.children[i],_b);
}
_b.document.write("</div>");
}
function LogEntry(_f,_10,_11){
this.children=new Array();
this.info=new Array();
this.func=_10;
this.args=new Array();
this.includeTimestamps=_f;
if(this.includeTimestamps){
this.timestamp=new Date();
}
if(_11!==null&&_11!==undefined){
for(var i=0;i<_11.length;i++){
args[args.length]=_11[i];
}
}
}
LogEntry.prototype.write=function(_13){
if(this.includeTimestamps){
var now=new Date();
var _15=now.getHours()+":"+now.getMinutes()+":"+now.getSeconds()+"."+now.getMilliseconds();
this.info[this.info.length]="["+_15+"] "+_13;
}else{
this.info[this.info.length]=_13;
}
};
LogEntry.prototype.startNew=function(_16,_17,_18){
var _19;
if(typeof _17=="string"){
_19=new LogEntry(this.includeTimestamps,_17,_18);
}else{
if(_17!==null&&_17!==undefined){
var _1a=_17.callee.toString().substring(9,_17.callee.toString().indexOf("{")-1);
_19=new LogEntry(this.includeTimestamps,_1a,_17);
}else{
_19=new LogEntry(this.includeTimestamps,"unknown entry point");
}
}
_19.type=_16;
this.children[this.children.length]=_19;
return _19;
};
LogEntry.prototype.setAttribute=function(_1b,_1c){
eval("this."+_1b+" = "+_1c);
};
LogEntry.prototype.setReturn=function(_1d){
if(this.includeTimestamps){
var now=new Date();
var _1f=(now.getTime()-this.timestamp.getTime())/1000;
this.elapsedTime=_1f;
}
this.returnValue=_1d;
};

