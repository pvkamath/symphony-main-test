var SCORM_TRUE="true";
var SCORM_FALSE="false";
var SCORM_UNKNOWN="unknown";
var SCORM2004_NO_ERROR="0";
var SCORM2004_GENERAL_EXCEPTION_ERROR="101";
var SCORM2004_GENERAL_INITIALIZATION_FAILURE_ERROR="102";
var SCORM2004_ALREADY_INTIAILIZED_ERROR="103";
var SCORM2004_CONTENT_INSTANCE_TERMINATED_ERROR="104";
var SCORM2004_GENERAL_TERMINATION_FAILURE_ERROR="111";
var SCORM2004_TERMINATION_BEFORE_INITIALIZATION_ERROR="112";
var SCORM2004_TERMINATION_AFTER_TERMINATION_ERROR="113";
var SCORM2004_RETRIEVE_DATA_BEFORE_INITIALIZATION_ERROR="122";
var SCORM2004_RETRIEVE_DATA_AFTER_TERMINATION_ERROR="123";
var SCORM2004_STORE_DATA_BEFORE_INITIALIZATION_ERROR="132";
var SCORM2004_STORE_DATA_AFTER_TERMINATION_ERROR="133";
var SCORM2004_COMMIT_BEFORE_INITIALIZATION_ERROR="142";
var SCORM2004_COMMIT_AFTER_TERMINATION_ERROR="143";
var SCORM2004_GENERAL_ARGUMENT_ERROR="201";
var SCORM2004_GENERAL_GET_FAILURE_ERROR="301";
var SCORM2004_GENERAL_SET_FAILURE_ERROR="351";
var SCORM2004_GENERAL_COMMIT_FAILURE_ERROR="391";
var SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR="401";
var SCORM2004_UNIMPLEMENTED_DATA_MODEL_ELEMENT_ERROR="402";
var SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR="403";
var SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR="404";
var SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR="405";
var SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR="406";
var SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR="407";
var SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR="408";
var SCORM2004_ErrorStrings=new Array();
SCORM2004_ErrorStrings[SCORM2004_NO_ERROR]="No Error";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_EXCEPTION_ERROR]="General Exception";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_INITIALIZATION_FAILURE_ERROR]="General Initialization Failure";
SCORM2004_ErrorStrings[SCORM2004_ALREADY_INTIAILIZED_ERROR]="Already Initialized";
SCORM2004_ErrorStrings[SCORM2004_CONTENT_INSTANCE_TERMINATED_ERROR]="Content Instance Terminated";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_TERMINATION_FAILURE_ERROR]="General Termination Failure";
SCORM2004_ErrorStrings[SCORM2004_TERMINATION_BEFORE_INITIALIZATION_ERROR]="Termination Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_TERMINATION_AFTER_TERMINATION_ERROR]="Termination AFter Termination";
SCORM2004_ErrorStrings[SCORM2004_RETRIEVE_DATA_BEFORE_INITIALIZATION_ERROR]="Retrieve Data Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_RETRIEVE_DATA_AFTER_TERMINATION_ERROR]="Retrieve Data After Termination";
SCORM2004_ErrorStrings[SCORM2004_STORE_DATA_BEFORE_INITIALIZATION_ERROR]="Store Data Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_STORE_DATA_AFTER_TERMINATION_ERROR]="Store Data After Termination";
SCORM2004_ErrorStrings[SCORM2004_COMMIT_BEFORE_INITIALIZATION_ERROR]="Commit Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_COMMIT_AFTER_TERMINATION_ERROR]="Commit After Termination";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_ARGUMENT_ERROR]="General Argument Error";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_GET_FAILURE_ERROR]="General Get Failure";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_SET_FAILURE_ERROR]="General Set Failure";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_COMMIT_FAILURE_ERROR]="General Commit Failure";
SCORM2004_ErrorStrings[SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR]="Undefined Data Model Element";
SCORM2004_ErrorStrings[SCORM2004_UNIMPLEMENTED_DATA_MODEL_ELEMENT_ERROR]="Unimplemented Data Model Element";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR]="Data Model Element Value Not Initialized";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR]="Data Model Element Is Read Only";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR]="Data Model Element Is Write Only";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR]="Data Model Element Type Mismatch";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR]="Data Model Element Value Out Of Range";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR]="Data Model Dependency Not Established";
var SCORM2004_VERSION="1.0";
var SCORM2004_COMMENTS_FROM_LEARNER_CHILDREN="comment,location,timestamp";
var SCORM2004_COMMENTS_FROM_LMS_CHILDREN="comment,location,timestamp";
var SCORM2004_INTERACTIONS_CHILDREN="id,type,objectives,timestamp,correct_responses,weighting,learner_response,result,latency,description";
var SCORM2004_LEARNER_PREFERENCE_CHILDREN="audio_level,language,delivery_speed,audio_captioning";
var SCORM2004_OBJECTIVES_CHILDREN="progress_measure,description,score,id,success_status,completion_status";
var SCORM2004_OBJECTIVES_SCORE_CHILDREN="scaled,raw,min,max";
var SCORM2004_SCORE_CHILDREN="scaled,min,max,raw";
function RunTimeApi(_1,_2){
this.LearnerId=_1;
this.LearnerName=_2;
this.ErrorNumber=SCORM2004_NO_ERROR;
this.ErrorString="";
this.ErrorDiagnostic="";
this.TrackedStartDate=null;
this.TrackedEndDate=null;
this.Initialized=false;
this.Terminated=false;
this.ScoCalledFinish=false;
this.RunTimeData=null;
this.LearningObject=null;
this.Activity=null;
this.IsLookAheadSequencerDataDirty=false;
this.LearnerPrefsArray=new Object();
if(SSP_ENABLED){
this.SSPApi=new SSPApi(MAX_SSP_STORAGE,this);
}
}
RunTimeApi.prototype.GetNavigationRequest=RunTimeApi_GetNavigationRequest;
RunTimeApi.prototype.ResetState=RunTimeApi_ResetState;
RunTimeApi.prototype.InitializeForDelivery=RunTimeApi_InitializeForDelivery;
RunTimeApi.prototype.SetDirtyData=RunTimeApi_SetDirtyData;
RunTimeApi.prototype.WriteHistoryLog=RunTimeApi_WriteHistoryLog;
RunTimeApi.prototype.WriteHistoryReturnValue=RunTimeApi_WriteHistoryReturnValue;
RunTimeApi.prototype.WriteAuditLog=RunTimeApi_WriteAuditLog;
RunTimeApi.prototype.WriteAuditReturnValue=RunTimeApi_WriteAuditReturnValue;
RunTimeApi.prototype.WriteDetailedLog=RunTimeApi_WriteDetailedLog;
RunTimeApi.prototype.CloseOutSession=RunTimeApi_CloseOutSession;
RunTimeApi.prototype.NeedToCloseOutSession=RunTimeApi_NeedToCloseOutSession;
RunTimeApi.prototype.AccumulateTotalTimeTracked=RunTimeApi_AccumulateTotalTrackedTime;
RunTimeApi.prototype.InitTrackedTimeStart=RunTimeApi_InitTrackedTimeStart;
function RunTimeApi_GetNavigationRequest(){
return null;
}
function RunTimeApi_ResetState(_3){
this.TrackedStartDate=null;
this.TrackedEndDate=null;
}
function RunTimeApi_InitializeForDelivery(_4){
this.RunTimeData=_4.RunTime;
this.LearningObject=_4.LearningObject;
this.Activity=_4;
if(Control.Package.Properties.ResetRunTimeDataTiming==RESET_RT_DATA_TIMING_WHEN_EXIT_IS_NOT_SUSPEND){
if(this.RunTimeData.Exit!=SCORM_EXIT_SUSPEND&&this.RunTimeData.Exit!=SCORM_EXIT_LOGOUT){
var _5={ev:"ResetRuntime",ai:_4.ItemIdentifier,at:_4.LearningObject.Title};
this.WriteHistoryLog("",_5);
this.RunTimeData.ResetState();
}
}
this.RunTimeData.Exit=SCORM_EXIT_UNKNOWN;
this.RunTimeData.NavRequest=SCORM_RUNTIME_NAV_REQUEST_NONE;
this.Initialized=false;
this.Terminated=false;
this.ScoCalledFinish=false;
this.TrackedStartDate=null;
this.TrackedEndDate=null;
this.ErrorNumber=SCORM2004_NO_ERROR;
this.ErrorString="";
this.ErrorDiagnostic="";
var _6;
var _7;
var _8;
for(var _9 in this.Activity.ActivityObjectives){
_8=this.Activity.ActivityObjectives[_9];
_6=_8.GetIdentifier();
if(_6!==null&&_6!==undefined&&_6.length>0){
_7=this.RunTimeData.FindObjectiveWithId(_6);
if(_7===null){
this.RunTimeData.AddObjective();
_7=this.RunTimeData.Objectives[this.RunTimeData.Objectives.length-1];
_7.Identifier=_6;
}
if(_8.GetProgressStatus(this.Activity,false)===true){
if(_8.GetSatisfiedStatus(this.Activity,false)===true){
_7.SuccessStatus=SCORM_STATUS_PASSED;
}else{
_7.SuccessStatus=SCORM_STATUS_FAILED;
}
}
if(_8.GetMeasureStatus(this.Activity,false)===true){
_7.ScoreScaled=_8.GetNormalizedMeasure(this.Activity,false);
}
_7.SuccessStatusChangedDuringRuntime=false;
_7.MeasureChangedDuringRuntime=false;
}
}
}
function RunTimeApi_SetDirtyData(){
this.Activity.DataState=DATA_STATE_DIRTY;
}
function RunTimeApi_WriteHistoryLog(_a,_b){
HistoryLog.WriteEventDetailed(_a,_b);
}
function RunTimeApi_WriteHistoryReturnValue(_c,_d){
HistoryLog.WriteEventDetailedReturnValue(_c,_d);
}
function RunTimeApi_WriteAuditLog(_e){
Debug.WriteRteAudit(_e);
}
function RunTimeApi_WriteAuditReturnValue(_f){
Debug.WriteRteAuditReturnValue(_f);
}
function RunTimeApi_WriteDetailedLog(str){
Debug.WriteRteDetailed(str);
}
function RunTimeApi_NeedToCloseOutSession(){
return ((this.Initialized===false)||this.ScoCalledFinish);
}
RunTimeApi.prototype.version=SCORM2004_VERSION;
RunTimeApi.prototype.Initialize=RunTimeApi_Initialize;
RunTimeApi.prototype.Terminate=RunTimeApi_Terminate;
RunTimeApi.prototype.GetValue=RunTimeApi_GetValue;
RunTimeApi.prototype.SetValue=RunTimeApi_SetValue;
RunTimeApi.prototype.Commit=RunTimeApi_Commit;
RunTimeApi.prototype.GetLastError=RunTimeApi_GetLastError;
RunTimeApi.prototype.GetErrorString=RunTimeApi_GetErrorString;
RunTimeApi.prototype.GetDiagnostic=RunTimeApi_GetDiagnostic;
RunTimeApi.prototype.RetrieveGetValueData=RunTimeApi_RetrieveGetValueData;
RunTimeApi.prototype.StoreValue=RunTimeApi_StoreValue;
RunTimeApi.prototype.SetErrorState=RunTimeApi_SetErrorState;
RunTimeApi.prototype.ClearErrorState=RunTimeApi_ClearErrorState;
RunTimeApi.prototype.CheckMaxLength=RunTimeApi_CheckMaxLength;
RunTimeApi.prototype.CheckLengthAndWarn=RunTimeApi_CheckLengthAndWarn;
RunTimeApi.prototype.CheckForInitializeError=RunTimeApi_CheckForInitializeError;
RunTimeApi.prototype.CheckForTerminateError=RunTimeApi_CheckForTerminateError;
RunTimeApi.prototype.CheckForGetValueError=RunTimeApi_CheckForGetValueError;
RunTimeApi.prototype.CheckForSetValueError=RunTimeApi_CheckForSetValueError;
RunTimeApi.prototype.CheckForCommitError=RunTimeApi_CheckForCommitError;
RunTimeApi.prototype.CheckCommentsCollectionLength=RunTimeApi_CheckCommentsCollectionLength;
RunTimeApi.prototype.CheckInteractionsCollectionLength=RunTimeApi_CheckInteractionsCollectionLength;
RunTimeApi.prototype.CheckInteractionObjectivesCollectionLength=RunTimeApi_CheckInteractionObjectivesCollectionLength;
RunTimeApi.prototype.CheckInteractionsCorrectResponsesCollectionLength=RunTimeApi_CheckInteractionsCorrectResponsesCollectionLength;
RunTimeApi.prototype.CheckObjectivesCollectionLength=RunTimeApi_CheckObjectivesCollectionLength;
RunTimeApi.prototype.LookAheadSessionClose=RunTimeApi_LookAheadSessionClose;
RunTimeApi.prototype.ValidOtheresponse=RunTimeApi_ValidOtheresponse;
RunTimeApi.prototype.ValidNumericResponse=RunTimeApi_ValidNumericResponse;
RunTimeApi.prototype.ValidSequencingResponse=RunTimeApi_ValidSequencingResponse;
RunTimeApi.prototype.ValidPerformanceResponse=RunTimeApi_ValidPerformanceResponse;
RunTimeApi.prototype.ValidMatchingResponse=RunTimeApi_ValidMatchingResponse;
RunTimeApi.prototype.ValidLikeRTResponse=RunTimeApi_ValidLikeRTResponse;
RunTimeApi.prototype.ValidLongFillInResponse=RunTimeApi_ValidLongFillInResponse;
RunTimeApi.prototype.ValidFillInResponse=RunTimeApi_ValidFillInResponse;
RunTimeApi.prototype.IsValidArrayOfLocalizedStrings=RunTimeApi_IsValidArrayOfLocalizedStrings;
RunTimeApi.prototype.IsValidArrayOfShortIdentifiers=RunTimeApi_IsValidArrayOfShortIdentifiers;
RunTimeApi.prototype.IsValidCommaDelimitedArrayOfShortIdentifiers=RunTimeApi_IsValidCommaDelimitedArrayOfShortIdentifiers;
RunTimeApi.prototype.ValidMultipleChoiceResponse=RunTimeApi_ValidMultipleChoiceResponse;
RunTimeApi.prototype.ValidTrueFalseResponse=RunTimeApi_ValidTrueFalseResponse;
RunTimeApi.prototype.ValidTimeInterval=RunTimeApi_ValidTimeInterval;
RunTimeApi.prototype.ValidTime=RunTimeApi_ValidTime;
RunTimeApi.prototype.ValidReal=RunTimeApi_ValidReal;
RunTimeApi.prototype.IsValidUrn=RunTimeApi_IsValidUrn;
RunTimeApi.prototype.ValidIdentifier=RunTimeApi_ValidIdentifier;
RunTimeApi.prototype.ValidShortIdentifier=RunTimeApi_ValidShortIdentifier;
RunTimeApi.prototype.ValidLongIdentifier=RunTimeApi_ValidLongIdentifier;
RunTimeApi.prototype.ValidLanguage=RunTimeApi_ValidLanguage;
RunTimeApi.prototype.ExtractLanguageDelimiterFromLocalizedString=RunTimeApi_ExtractLanguageDelimiterFromLocalizedString;
RunTimeApi.prototype.ValidLocalizedString=RunTimeApi_ValidLocalizedString;
RunTimeApi.prototype.ValidCharString=RunTimeApi_ValidCharString;
RunTimeApi.prototype.TranslateBooleanIntoCMI=RunTimeApi_TranslateBooleanIntoCMI;
RunTimeApi.prototype.SetLookAheadDirtyDataFlagIfNeeded=RunTimeApi_SetLookAheadDirtyDataFlagIfNeeded;
RunTimeApi.prototype.GetCompletionStatus=RunTimeApi_GetCompletionStatus;
RunTimeApi.prototype.GetSuccessStatus=RunTimeApi_GetSuccessStatus;
function RunTimeApi_Initialize(arg){
this.WriteAuditLog("`1702`"+arg+"')");
var _12={ev:"ApiInitialize"};
if(this.Activity){
_12.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_12);
var _13;
var _14;
this.ClearErrorState();
_13=this.CheckForInitializeError(arg);
if(!_13){
_14=SCORM_FALSE;
}else{
this.Initialized=true;
_14=SCORM_TRUE;
}
this.WriteAuditReturnValue(_14);
return _14;
}
function RunTimeApi_Terminate(arg){
this.WriteAuditLog("`1714`"+arg+"')");
var _16;
var _17;
var _18;
this.ClearErrorState();
_16=this.CheckForTerminateError(arg);
var _19=(_16&&(this.ScoCalledFinish===false));
if(!_16){
_18=SCORM_FALSE;
}else{
var _1a={ev:"ApiTerminate"};
if(this.Activity){
_1a.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_1a);
this.LookAheadSessionClose();
this.CloseOutSession();
if(this.IsLookAheadSequencerDataDirty===true){
this.IsLookAheadSequencerDataDirty=false;
window.setTimeout("Control.EvaluatePossibleNavigationRequests(true);",150);
}
this.Terminated=true;
this.ScoCalledFinish=true;
this.SetDirtyData();
_18=SCORM_TRUE;
}
if(_19===true&&this.RunTimeData.NavRequest!=SCORM_RUNTIME_NAV_REQUEST_NONE&&Control.IsThereAPendingNavigationRequest()===false){
this.WriteDetailedLog("`449`"+Control.IsThereAPendingNavigationRequest());
window.setTimeout("Control.ScoHasTerminatedSoUnload();",150);
}
Control.SignalTerminated();
this.WriteAuditReturnValue(_18);
return _18;
}
function RunTimeApi_GetValue(_1b){
this.WriteAuditLog("`1719`"+_1b+"')");
var _1c;
var _1d;
this.ClearErrorState();
_1b=CleanExternalString(_1b);
var _1e=RemoveIndiciesFromCmiElement(_1b);
var _1f=ExtractIndex(_1b);
var _20=ExtractSecondaryIndex(_1b);
_1d=this.CheckForGetValueError(_1b,_1e,_1f,_20);
if(!_1d){
_1c="";
}else{
_1c=this.RetrieveGetValueData(_1b,_1e,_1f,_20);
if(_1c===null){
_1c="";
}
}
this.WriteAuditReturnValue(_1c);
return _1c;
}
function RunTimeApi_SetValue(_21,_22){
this.WriteAuditLog("`1717`"+_21+"`1729`"+_22+"')");
var _23;
var _24;
this.ClearErrorState();
_21=CleanExternalString(_21);
_22=CleanExternalString(_22);
var _25=RemoveIndiciesFromCmiElement(_21);
var _26=ExtractIndex(_21);
var _27=ExtractSecondaryIndex(_21);
this.CheckMaxLength(_25,_22);
_23=this.CheckForSetValueError(_21,_22,_25,_26,_27);
if(!_23){
_24=SCORM_FALSE;
}else{
this.StoreValue(_21,_22,_25,_26,_27);
this.SetDirtyData();
_24=SCORM_TRUE;
}
this.WriteAuditReturnValue(_24);
return _24;
}
function RunTimeApi_Commit(arg){
this.WriteAuditLog("`1722`"+arg+"')");
var _29;
var _2a;
this.ClearErrorState();
_29=this.CheckForCommitError(arg);
if(!_29){
_2a=SCORM_FALSE;
}else{
_2a=SCORM_TRUE;
}
this.LookAheadSessionClose();
if(this.IsLookAheadSequencerDataDirty===true){
this.IsLookAheadSequencerDataDirty=false;
window.setTimeout("Control.EvaluatePossibleNavigationRequests(true);",150);
}
this.WriteAuditReturnValue(_2a);
return _2a;
}
function RunTimeApi_GetLastError(){
this.WriteAuditLog("`1696`");
var _2b=this.ErrorNumber;
this.WriteAuditReturnValue(_2b);
return _2b;
}
function RunTimeApi_GetErrorString(arg){
this.WriteAuditLog("`1672`"+arg+"')");
var _2d="";
if(arg===""){
_2d="";
}else{
if(SCORM2004_ErrorStrings[arg]!==null&&SCORM2004_ErrorStrings[arg]!==undefined){
_2d=SCORM2004_ErrorStrings[arg];
}
}
this.WriteAuditReturnValue(_2d);
return _2d;
}
function RunTimeApi_GetDiagnostic(arg){
this.WriteAuditLog("`1687`"+arg+"')");
var _2f;
if(this.ErrorDiagnostic===""){
_2f="No diagnostic information available";
}else{
_2f=this.ErrorDiagnostic;
}
this.WriteAuditReturnValue(_2f);
return _2f;
}
function RunTimeApi_CloseOutSession(){
this.WriteDetailedLog("`1653`");
this.WriteDetailedLog("`1724`"+this.RunTimeData.Mode);
this.WriteDetailedLog("`1721`"+this.RunTimeData.Credit);
this.WriteDetailedLog("`1431`"+this.RunTimeData.CompletionStatus);
this.WriteDetailedLog("`1486`"+this.RunTimeData.SuccessStatus);
this.WriteDetailedLog("`1557`"+this.LearningObject.GetScaledPassingScore());
this.WriteDetailedLog("`1723`"+this.RunTimeData.ScoreScaled);
this.WriteDetailedLog("`1527`"+this.LearningObject.CompletionThreshold);
this.WriteDetailedLog("`1593`"+this.RunTimeData.ProgressMeasure);
var _30=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.SessionTime);
var _31=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTime);
var _32=_30+_31;
var _33=ConvertHundredthsToIso8601TimeSpan(_32);
this.WriteDetailedLog("`1695`"+this.RunTimeData.SessionTime+" ("+_30+"`1705`");
this.WriteDetailedLog("`1677`"+this.RunTimeData.TotalTime+" ("+_31+"`1705`");
this.WriteDetailedLog("`1673`"+_33+" ("+_32+"`1705`");
this.RunTimeData.TotalTime=_33;
this.RunTimeData.SessionTime="";
this.AccumulateTotalTimeTracked();
this.WriteDetailedLog("`1498`"+this.RunTimeData.TotalTimeTracked);
if(Control.IsThereAPendingNavigationRequest()){
if(Control.PendingNavigationRequest.Type==NAVIGATION_REQUEST_SUSPEND_ALL){
this.WriteDetailedLog("`915`");
this.RunTimeData.Exit=SCORM_EXIT_SUSPEND;
}
}else{
if(this.RunTimeData.NavRequest==SCORM_RUNTIME_NAV_REQUEST_SUSPENDALL){
this.WriteDetailedLog("`936`");
this.RunTimeData.Exit=SCORM_EXIT_SUSPEND;
}
}
if(this.RunTimeData.Exit==SCORM_EXIT_SUSPEND||this.RunTimeData.Exit==SCORM_EXIT_LOGOUT){
this.WriteDetailedLog("`1583`");
this.RunTimeData.Entry=SCORM_ENTRY_RESUME;
}
this.RunTimeData.CompletionStatus=this.GetCompletionStatus();
this.RunTimeData.SuccessStatus=this.GetSuccessStatus();
if(this.RunTimeData.Exit==SCORM_EXIT_TIME_OUT){
this.WriteDetailedLog("`1449`");
this.RunTimeData.NavRequest=SCORM_RUNTIME_NAV_REQUEST_EXITALL;
}else{
if(this.RunTimeData.Exit==SCORM_EXIT_LOGOUT){
if(Control.Package.Properties.LogoutCausesPlayerExit===true){
this.WriteDetailedLog("`1332`");
this.RunTimeData.NavRequest=SCORM_RUNTIME_NAV_REQUEST_SUSPENDALL;
}else{
this.WriteDetailedLog("`340`");
this.Activity.SetSuspended(true);
}
}else{
if(this.RunTimeData.Exit==SCORM_EXIT_SUSPEND){
this.WriteDetailedLog("`1601`");
this.Activity.SetSuspended(true);
}
}
}
return true;
}
function RunTimeApi_LookAheadSessionClose(){
this.RunTimeData.LookAheadCompletionStatus=this.GetCompletionStatus();
this.RunTimeData.LookAheadSuccessStatus=this.GetSuccessStatus();
}
function RunTimeApi_RetrieveGetValueData(_34,_35,_36,_37){
this.WriteDetailedLog("`1541`"+_34+", "+_35+", "+_36+", "+_37+") ");
var _38;
var _39="";
if(_34.indexOf("adl.nav.request_valid.choice.{")===0){
this.WriteDetailedLog("`1283`");
_38=_34.substring(_34.indexOf("{"));
if(!Control.IsTargetValid(_38)){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The target of the choice request ("+_38+") is invalid.");
return SCORM_FALSE;
}
_39=Control.IsChoiceRequestValid(_38);
if(_39==false){
var _3a=Control.ParseTargetStringIntoActivity(_38);
var _3b=Control.FindPossibleChoiceRequestForActivity(_3a);
this.WriteDetailedLog(_3b.GetExceptionReason());
}
_39=this.TranslateBooleanIntoCMI(_39);
return _39;
}
switch(_35){
case "cmi._version":
this.WriteDetailedLog("`1497`");
_39=SCORM2004_VERSION;
break;
case "cmi.comments_from_learner._children":
this.WriteDetailedLog("`1159`");
_39=SCORM2004_COMMENTS_FROM_LEARNER_CHILDREN;
break;
case "cmi.comments_from_learner._count":
this.WriteDetailedLog("`1226`");
_39=this.RunTimeData.Comments.length;
break;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1206`");
_39=this.RunTimeData.Comments[_36].GetCommentValue();
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1195`");
_39=this.RunTimeData.Comments[_36].Location;
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1153`");
_39=this.RunTimeData.Comments[_36].Timestamp;
break;
case "cmi.comments_from_lms._children":
this.WriteDetailedLog("`1244`");
_39=SCORM2004_COMMENTS_FROM_LMS_CHILDREN;
break;
case "cmi.comments_from_lms._count":
this.WriteDetailedLog("`1289`");
_39=this.RunTimeData.CommentsFromLMS.length;
break;
case "cmi.comments_from_lms.n.comment":
this.WriteDetailedLog("`1279`");
_39=this.RunTimeData.CommentsFromLMS[_36].GetCommentValue();
break;
case "cmi.comments_from_lms.n.location":
this.WriteDetailedLog("`1266`");
_39=this.RunTimeData.CommentsFromLMS[_36].Location;
break;
case "cmi.comments_from_lms.n.timestamp":
this.WriteDetailedLog("`1238`");
_39=this.RunTimeData.CommentsFromLMS[_36].Timestamp;
break;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
_39=this.GetCompletionStatus();
break;
case "cmi.completion_threshold":
this.WriteDetailedLog("`1367`");
_39=this.LearningObject.CompletionThreshold;
break;
case "cmi.credit":
this.WriteDetailedLog("`1617`");
_39=this.RunTimeData.Credit;
break;
case "cmi.entry":
this.WriteDetailedLog("`1643`");
_39=this.RunTimeData.Entry;
break;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
Debug.AssertError("Exit element is write only");
_39="";
break;
case "cmi.interactions._children":
this.WriteDetailedLog("`1321`");
_39=SCORM2004_INTERACTIONS_CHILDREN;
break;
case "cmi.interactions._count":
this.WriteDetailedLog("`1380`");
_39=this.RunTimeData.Interactions.length;
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1444`");
_39=this.RunTimeData.Interactions[_36].Id;
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1402`");
_39=this.RunTimeData.Interactions[_36].Type;
break;
case "cmi.interactions.n.objectives._count":
this.WriteDetailedLog("`1200`");
_39=this.RunTimeData.Interactions[_36].Objectives.length;
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1272`");
_39=this.RunTimeData.Interactions[_36].Objectives[_37];
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1337`");
_39=this.RunTimeData.Interactions[_36].Timestamp;
break;
case "cmi.interactions.n.correct_responses._count":
this.WriteDetailedLog("`1066`");
_39=this.RunTimeData.Interactions[_36].CorrectResponses.length;
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`1028`");
_39=this.RunTimeData.Interactions[_36].CorrectResponses[_37];
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1329`");
_39=this.RunTimeData.Interactions[_36].Weighting;
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1210`");
_39=this.RunTimeData.Interactions[_36].LearnerResponse;
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1378`");
_39=this.RunTimeData.Interactions[_36].Result;
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1362`");
_39=this.RunTimeData.Interactions[_36].Latency;
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1297`");
_39=this.RunTimeData.Interactions[_36].Description;
break;
case "cmi.launch_data":
this.WriteDetailedLog("`1532`");
_39=this.LearningObject.DataFromLms;
break;
case "cmi.learner_id":
this.WriteDetailedLog("`1549`");
_39=LearnerId;
break;
case "cmi.learner_name":
this.WriteDetailedLog("`1516`");
_39=LearnerName;
break;
case "cmi.learner_preference._children":
this.WriteDetailedLog("`1230`");
_39=SCORM2004_LEARNER_PREFERENCE_CHILDREN;
break;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1528`");
_39=this.RunTimeData.AudioLevel;
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1574`");
_39=this.RunTimeData.LanguagePreference;
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1467`");
_39=this.RunTimeData.DeliverySpeed;
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1436`");
_39=this.RunTimeData.AudioCaptioning;
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
_39=this.RunTimeData.Location;
var _3c={ev:"Get",k:"location",v:(_39==null?"<null>":_39)};
if(this.Activity){
_3c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3c);
break;
case "cmi.max_time_allowed":
this.WriteDetailedLog("`1430`");
_39="";
if(this.LearningObject.SequencingData!==null&&this.LearningObject.SequencingData.LimitConditionAttemptAbsoluteDurationControl===true){
_39=this.LearningObject.SequencingData.LimitConditionAttemptAbsoluteDurationLimit;
}
break;
case "cmi.mode":
this.WriteDetailedLog("`1676`");
_39=this.RunTimeData.Mode;
break;
case "cmi.objectives._children":
this.WriteDetailedLog("`1365`");
_39=SCORM2004_OBJECTIVES_CHILDREN;
break;
case "cmi.objectives._count":
this.WriteDetailedLog("`1407`");
_39=this.RunTimeData.Objectives.length;
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1493`");
_39=this.RunTimeData.Objectives[_36].Identifier;
break;
case "cmi.objectives.n.score._children":
this.WriteDetailedLog("`1264`");
_39=SCORM2004_OBJECTIVES_SCORE_CHILDREN;
break;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1303`");
_39=this.RunTimeData.Objectives[_36].ScoreScaled;
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1359`");
_39=this.RunTimeData.Objectives[_36].ScoreRaw;
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1357`");
_39=this.RunTimeData.Objectives[_36].ScoreMin;
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1358`");
_39=this.RunTimeData.Objectives[_36].ScoreMax;
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1275`");
_39=this.RunTimeData.Objectives[_36].SuccessStatus;
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1221`");
_39=this.RunTimeData.Objectives[_36].CompletionStatus;
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1242`");
_39=this.RunTimeData.Objectives[_36].ProgressMeasure;
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1324`");
_39=this.RunTimeData.Objectives[_36].Description;
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1427`");
_39=this.RunTimeData.ProgressMeasure;
break;
case "cmi.scaled_passing_score":
this.WriteDetailedLog("`1368`");
_39=this.LearningObject.GetScaledPassingScore();
if(_39===null){
_39="";
}
break;
case "cmi.score._children":
this.WriteDetailedLog("`1446`");
_39=SCORM2004_SCORE_CHILDREN;
break;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
_39=this.RunTimeData.ScoreScaled;
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
_39=this.RunTimeData.ScoreRaw;
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
_39=this.RunTimeData.ScoreMax;
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
_39=this.RunTimeData.ScoreMin;
break;
case "cmi.session_time":
this.WriteDetailedLog("`1505`");
_39="";
break;
case "cmi.success_status":
this.WriteDetailedLog("`1470`");
_39=this.GetSuccessStatus();
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1518`");
_39=this.RunTimeData.SuspendData;
break;
case "cmi.time_limit_action":
this.WriteDetailedLog("`1419`");
_39=this.LearningObject.TimeLimitAction;
break;
case "cmi.total_time":
this.WriteDetailedLog("`1547`");
_39=this.RunTimeData.TotalTime;
break;
case "adl.nav.request":
this.WriteDetailedLog("`1441`");
_39=this.RunTimeData.NavRequest;
break;
case "adl.nav.request_valid.continue":
this.WriteDetailedLog("`1168`");
_39=Control.IsContinueRequestValid();
if(_39==false){
var _3d=Control.GetPossibleContinueRequest();
this.WriteDetailedLog(_3d.GetExceptionReason());
}
_39=this.TranslateBooleanIntoCMI(_39);
break;
case "adl.nav.request_valid.previous":
this.WriteDetailedLog("`1193`");
_39=Control.IsPreviousRequestValid();
if(_39==false){
var _3d=Control.GetPossiblePreviousRequest();
this.WriteDetailedLog(_3d.GetExceptionReason());
}
_39=this.TranslateBooleanIntoCMI(_39);
break;
case "adl.nav.request_valid.choice":
this.WriteDetailedLog("`1335`");
Debug.AssertError("Entered invalid case in RunTimeApi_RetrieveGetValueData");
break;
default:
if(_35.indexOf("ssp")===0&&SSP_ENABLED){
_39=this.SSPApi.RetrieveGetValueData(_34,_35,_36,_37);
}else{
Debug.AssertError("Entered default case in RunTimeApi_RetrieveGetValueData");
_39="";
}
break;
}
return _39;
}
function RunTimeApi_StoreValue(_3e,_3f,_40,_41,_42){
this.WriteDetailedLog("`1707`"+_3e+", "+_3f+", "+_40+", "+_41+", "+_42+") ");
var _43=true;
switch(_40){
case "cmi._version":
this.WriteDetailedLog("`1572`");
break;
case "cmi.comments_from_learner._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_learner._children");
_43=false;
break;
case "cmi.comments_from_learner._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_learner._count");
_43=false;
break;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1228`");
this.CheckCommentsCollectionLength(_41);
this.RunTimeData.Comments[_41].SetCommentValue(_3f);
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1179`");
this.CheckCommentsCollectionLength(_41);
this.RunTimeData.Comments[_41].Location=_3f;
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1165`");
this.CheckCommentsCollectionLength(_41);
this.RunTimeData.Comments[_41].Timestamp=_3f;
break;
case "cmi.comments_from_lms._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms._children");
_43=false;
break;
case "cmi.comments_from_lms._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms._count");
_43=false;
break;
case "cmi.comments_from_lms.n.comment":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms.comment");
_43=false;
break;
case "cmi.comments_from_lms.n.location":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms.location");
_43=false;
break;
case "cmi.comments_from_lms.n.timestamp":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms.timestamp");
_43=false;
break;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
var _44={ev:"Set",k:"completion",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.CompletionStatus,_3f);
this.RunTimeData.CompletionStatus=_3f;
this.RunTimeData.CompletionStatusChangedDuringRuntime=true;
break;
case "cmi.completion_threshold":
Debug.AssertError("ERROR - Element is Read Only, cmi.completion_threshold");
_43=false;
break;
case "cmi.credit":
Debug.AssertError("ERROR - Element is Read Only, cmi.credit");
_43=false;
break;
case "cmi.entry":
Debug.AssertError("ERROR - Element is Read Only, cmi.entry");
_43=false;
break;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
var _44={ev:"Set",k:"cmi.exit",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.Exit=_3f;
break;
case "cmi.interactions._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions._children");
_43=false;
break;
case "cmi.interactions._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions._count");
_43=false;
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1444`");
var _44={ev:"Set",k:"interactions id",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Id=_3f;
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1402`");
var _44={ev:"Set",k:"interactions type",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Type=_3f;
break;
case "cmi.interactions.n.objectives._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions.n.objectives._count");
_43=false;
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1272`");
var _44={ev:"Set",k:"interactions objectives id",i:_41,si:_42,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionObjectivesCollectionLength(_41,_42);
this.RunTimeData.Interactions[_41].Objectives[_42]=_3f;
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1337`");
var _44={ev:"Set",k:"interactions timestamp",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Timestamp=_3f;
break;
case "cmi.interactions.n.correct_responses._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions.n.correct_responses._count");
_43=false;
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`1028`");
var _44={ev:"Set",k:"interactions correct_responses pattern",i:_41,si:_42,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCorrectResponsesCollectionLength(_41,_42);
this.RunTimeData.Interactions[_41].CorrectResponses[_42]=_3f;
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1329`");
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Weighting=_3f;
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1210`");
var _44={ev:"Set",k:"interactions learner_response",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].LearnerResponse=_3f;
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1378`");
var _44={ev:"Set",k:"interactions result",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Result=_3f;
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1362`");
var _44={ev:"Set",k:"interactions latency",i:_41,vh:ConvertIso8601TimeSpanToHundredths(_3f)};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Latency=_3f;
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1297`");
var _44={ev:"Set",k:"interactions description",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Description=_3f;
break;
case "cmi.launch_data":
Debug.AssertError("ERROR - Element is Read Only, cmi.launch_data");
_43=false;
break;
case "cmi.learner_id":
Debug.AssertError("ERROR - Element is Read Only, cmi.learner_id");
_43=false;
break;
case "cmi.learner_name":
Debug.AssertError("ERROR - Element is Read Only, cmi.learner_name");
_43=false;
break;
case "cmi.learner_preference._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.learner_preference._children");
_43=false;
break;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1509`");
this.RunTimeData.AudioLevel=_3f;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.AudioLevel=_3f;
}
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1574`");
this.RunTimeData.LanguagePreference=_3f;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.LanguagePreference=_3f;
}
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1483`");
this.RunTimeData.DeliverySpeed=_3f;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.DeliverySpeed=_3f;
}
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1436`");
this.RunTimeData.AudioCaptioning=_3f;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.AudioCaptioning=_3f;
}
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
this.RunTimeData.Location=_3f;
break;
case "cmi.max_time_allowed":
Debug.AssertError("ERROR - Element is Read Only, cmi.max_time_allowed");
_43=false;
break;
case "cmi.mode":
Debug.AssertError("ERROR - Element is Read Only, cmi.mode");
_43=false;
break;
case "cmi.objectives._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.objectives._children");
_43=false;
break;
case "cmi.objectives._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.objectives._count");
_43=false;
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1493`");
var _44={ev:"Set",k:"objectives id",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].Identifier,_3f);
this.RunTimeData.Objectives[_41].Identifier=_3f;
break;
case "cmi.objectives.n.score._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.objectives.n.score._children");
_43=false;
break;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1311`");
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].ScoreScaled,_3f);
this.RunTimeData.Objectives[_41].ScoreScaled=_3f;
this.RunTimeData.Objectives[_41].MeasureChangedDuringRuntime=true;
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1359`");
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].ScoreRaw,_3f);
this.RunTimeData.Objectives[_41].ScoreRaw=_3f;
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1357`");
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].ScoreMin,_3f);
this.RunTimeData.Objectives[_41].ScoreMin=_3f;
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1358`");
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].ScoreMax,_3f);
this.RunTimeData.Objectives[_41].ScoreMax=_3f;
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1275`");
var _44={ev:"Set",k:"objectives success",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Objectives[_41].Identifier){
_44.intid=this.RunTimeData.Objectives[_41].Identifier;
}
this.WriteHistoryLog("",_44);
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].SuccessStatus,_3f);
this.RunTimeData.Objectives[_41].SuccessStatus=_3f;
this.RunTimeData.Objectives[_41].SuccessStatusChangedDuringRuntime=true;
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1221`");
var _44={ev:"Set",k:"objectives completion",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Objectives[_41].Identifier){
_44.intid=this.RunTimeData.Objectives[_41].Identifier;
}
this.WriteHistoryLog("",_44);
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].CompletionStatus,_3f);
this.RunTimeData.Objectives[_41].CompletionStatus=_3f;
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1242`");
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].ProgressMeasure,_3f);
this.RunTimeData.Objectives[_41].ProgressMeasure=_3f;
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1324`");
this.CheckObjectivesCollectionLength(_41);
this.RunTimeData.Objectives[_41].Description=_3f;
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1426`");
this.RunTimeData.ProgressMeasure=_3f;
break;
case "cmi.scaled_passing_score":
Debug.AssertError("ERROR - Element is Read Only, cmi.scaled_passing_score");
_43=false;
break;
case "cmi.score._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.score._children");
_43=false;
break;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
var _44={ev:"Set",k:"score.scaled",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.ScoreScaled,_3f);
this.RunTimeData.ScoreScaled=_3f;
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
var _44={ev:"Set",k:"score.raw",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.ScoreRaw=_3f;
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
var _44={ev:"Set",k:"score.max",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.ScoreMax=_3f;
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
var _44={ev:"Set",k:"score.min",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.ScoreMin=_3f;
break;
case "cmi.session_time":
this.WriteDetailedLog("`1515`");
var _44={ev:"Set",k:"session time",vh:ConvertIso8601TimeSpanToHundredths(_3f)};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.SessionTime=_3f;
break;
case "cmi.success_status":
this.WriteDetailedLog("`1480`");
var _44={ev:"Set",k:"success",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.SuccessStatus,_3f);
this.RunTimeData.SuccessStatus=_3f;
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1513`");
this.RunTimeData.SuspendData=_3f;
break;
case "cmi.time_limit_action":
Debug.AssertError("ERROR - Element is Read Only, cmi.time_limit_action");
_43=false;
break;
case "cmi.total_time":
Debug.AssertError("ERROR - Element is Read Only, cmi.total_time");
_43=false;
break;
case "adl.nav.request":
this.WriteDetailedLog("`1455`");
var _44={ev:"Set",k:"nav.request"};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
var _45=_3f.match(/target\s*=\s*(\S+)\s*}\s*choice/);
if(_45){
_44.tai=_45[1];
_44.tat=Control.Activities.GetActivityFromIdentifier(_45[1]).LearningObject.Title;
_44.v="choice";
}else{
_44.v=_3f;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.NavRequest=_3f;
break;
case "adl.nav.request_valid.continue":
Debug.AssertError("ERROR - Element is Read Only, adl.nav.request_valid.continue");
break;
case "adl.nav.request_valid.previous":
Debug.AssertError("ERROR - Element is Read Only, adl.nav.request_valid.previous");
break;
case "adl.nav.request_valid.choice":
Debug.AssertError("ERROR - Should never get here...handled above - Element is Read Only, adl.nav.request_valid.choice");
break;
default:
if(_40.indexOf("ssp")===0){
if(SSP_ENABLED){
return this.SSPApi.StoreValue(_3e,_3f,_40,_41,_42);
}
}
Debug.AssertError("ERROR reached default case in RunTimeApi_StoreValue");
returnData="";
break;
}
}
function RunTimeApi_CheckCommentsCollectionLength(_46){
if(this.RunTimeData.Comments.length<=_46){
this.WriteDetailedLog("`1130`"+_46);
this.RunTimeData.Comments[_46]=new ActivityRunTimeComment(null,null,null,null,null);
}
}
function RunTimeApi_CheckInteractionsCollectionLength(_47){
if(this.RunTimeData.Interactions.length<=_47){
this.WriteDetailedLog("`1299`"+_47);
this.RunTimeData.Interactions[_47]=new ActivityRunTimeInteraction(null,null,null,null,null,null,null,null,null,new Array(),new Array());
}
}
function RunTimeApi_CheckInteractionObjectivesCollectionLength(_48,_49){
if(this.RunTimeData.Interactions[_48].Objectives.length<=_49){
this.WriteDetailedLog("`1110`"+_49);
this.RunTimeData.Interactions[_48].Objectives[_49]=null;
}
}
function RunTimeApi_CheckInteractionsCorrectResponsesCollectionLength(_4a,_4b){
if(this.RunTimeData.Interactions[_4a].CorrectResponses.length<=_4b){
this.WriteDetailedLog("`990`"+_4b);
this.RunTimeData.Interactions[_4a].CorrectResponses[_4b]=null;
}
}
function RunTimeApi_CheckObjectivesCollectionLength(_4c){
if(this.RunTimeData.Objectives.length<=_4c){
this.WriteDetailedLog("`1342`"+_4c);
this.RunTimeData.Objectives[_4c]=new ActivityRunTimeObjective(null,"unknown","unknown",null,null,null,null,null,null);
}
}
function RunTimeApi_SetErrorState(_4d,_4e){
if(_4d!=SCORM2004_NO_ERROR){
this.WriteDetailedLog("`1281`"+_4d+" - "+_4e);
}
this.ErrorNumber=_4d;
this.ErrorString=SCORM2004_ErrorStrings[_4d];
this.ErrorDiagnostic=_4e;
}
function RunTimeApi_ClearErrorState(){
this.SetErrorState(SCORM2004_NO_ERROR,"");
}
function RunTimeApi_CheckForInitializeError(arg){
this.WriteDetailedLog("`1420`");
if(this.Initialized){
this.SetErrorState(SCORM2004_ALREADY_INTIAILIZED_ERROR,"Initialize has already been called and may only be called once per session.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_CONTENT_INSTANCE_TERMINATED_ERROR,"Initialize cannot be called after Terminate has already beeen called.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM2004_GENERAL_ARGUMENT_ERROR,"The argument to Initialize must be an empty string (\"\"). The argument '"+arg+"' is invalid.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForTerminateError(arg){
this.WriteDetailedLog("`1437`");
if(!this.Initialized){
this.SetErrorState(SCORM2004_TERMINATION_BEFORE_INITIALIZATION_ERROR,"Terminate cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_TERMINATION_AFTER_TERMINATION_ERROR,"Terminate cannot be called after Terminate has already beeen called.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM2004_GENERAL_ARGUMENT_ERROR,"The argument to Terminate must be an empty string (\"\"). The argument '"+arg+"' is invalid.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForCommitError(arg){
this.WriteDetailedLog("`1487`");
if(!this.Initialized){
this.SetErrorState(SCORM2004_COMMIT_BEFORE_INITIALIZATION_ERROR,"Commit cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_COMMIT_AFTER_TERMINATION_ERROR,"Commit cannot be called after Terminate has already beeen called.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM2004_GENERAL_ARGUMENT_ERROR,"The argument to Commit must be an empty string (\"\"). The argument '"+arg+"' is invalid.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckMaxLength(_52,_53){
switch(_52){
case "`1304`":
this.CheckLengthAndWarn(_53,4250);
break;
case "cmi.comments_from_learner.n.location":
this.CheckLengthAndWarn(_53,250);
break;
case "cmi.interactions.n.id":
this.CheckLengthAndWarn(_53,4000);
break;
case "cmi.interactions.n.objectives.n.id":
this.CheckLengthAndWarn(_53,4000);
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.CheckLengthAndWarn(_53,7800);
break;
case "cmi.interactions.n.learner_response":
this.CheckLengthAndWarn(_53,7800);
break;
case "cmi.interactions.n.description":
this.CheckLengthAndWarn(_53,500);
break;
case "cmi.learner_preference.language":
this.CheckLengthAndWarn(_53,250);
break;
case "cmi.location":
this.CheckLengthAndWarn(_53,1000);
break;
case "cmi.objectives.n.id":
this.CheckLengthAndWarn(_53,4000);
break;
case "cmi.objectives.n.description":
this.CheckLengthAndWarn(_53,500);
break;
case "cmi.suspend_data":
this.CheckLengthAndWarn(_53,64000);
break;
default:
break;
}
return;
}
function RunTimeApi_CheckLengthAndWarn(str,len){
if(str.length>len){
this.SetErrorState(SCORM2004_NO_ERROR,"The string was trimmed to fit withing the SPM of "+len+" characters.");
}
return;
}
function RunTimeApi_CheckForGetValueError(_56,_57,_58,_59){
this.WriteDetailedLog("`1454`");
if(!this.Initialized){
this.SetErrorState(SCORM2004_RETRIEVE_DATA_BEFORE_INITIALIZATION_ERROR,"GetValue cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_RETRIEVE_DATA_AFTER_TERMINATION_ERROR,"GetValue cannot be called after Terminate has already beeen called.");
return false;
}
if(_56.length===0){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The data model element for GetValue was not specified.");
return false;
}
if(_58!==""){
if(_57.indexOf("cmi.comments_from_learner")>=0){
if(_58>=this.RunTimeData.Comments.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Comments From Learner collection does not have an element at index "+_58+", the current element count is "+this.RunTimeData.Comments.length+".");
return false;
}
}else{
if(_57.indexOf("cmi.comments_from_lms")>=0){
if(_58>=this.RunTimeData.CommentsFromLMS.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Comments From LMS collection does not have an element at index "+_58+", the current element count is "+this.RunTimeData.CommentsFromLMS.length+".");
return false;
}
}else{
if(_57.indexOf("cmi.objectives")>=0){
if(_58>=this.RunTimeData.Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Objectives collection does not have an element at index "+_58+", the current element count is "+this.RunTimeData.Objectives.length+".");
return false;
}
}else{
if(_57.indexOf("cmi.interactions")>=0){
if(_58>=this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Interactions collection does not have an element at index "+_58+", the current element count is "+this.RunTimeData.Interactions.length+".");
return false;
}
if(_57.indexOf("cmi.interactions.n.correct_responses")>=0){
if(_59!==""){
if(_59>=this.RunTimeData.Interactions[_58].CorrectResponses.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Correct Responses collection for Interaction #"+_58+" does not have "+_59+" elements in it, the current element count is "+this.RunTimeData.Interactions[_58].CorrectResponses.length+".");
return false;
}
}
}else{
if(_57.indexOf("cmi.interactions.n.objectives")>=0){
if(_59!==""){
if(_59>=this.RunTimeData.Interactions[_58].Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Objectives collection for Interaction #"+_58+" does not have "+_59+" elements in it, the current element count is "+this.RunTimeData.Interactions[_58].Objectives.length+".");
return false;
}
}
}
}
}
}
}
}
}
switch(_57){
case "cmi._version":
this.WriteDetailedLog("`1600`");
break;
case "cmi.comments_from_learner._children":
this.WriteDetailedLog("`1196`");
break;
case "cmi.comments_from_learner._count":
this.WriteDetailedLog("`1226`");
break;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1154`");
if(this.RunTimeData.Comments[_58].Comment===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Comment field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1139`");
if(this.RunTimeData.Comments[_58].Location===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Location field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1113`");
if(this.RunTimeData.Comments[_58].Timestamp===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The TimeStamp field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.comments_from_lms._children":
this.WriteDetailedLog("`1244`");
break;
case "cmi.comments_from_lms._count":
this.WriteDetailedLog("`1289`");
break;
case "cmi.comments_from_lms.n.comment":
this.WriteDetailedLog("`1252`");
if(this.RunTimeData.CommentsFromLMS[_58].Comment===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Comment field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.comments_from_lms.n.location":
this.WriteDetailedLog("`1235`");
if(this.RunTimeData.CommentsFromLMS[_58].Location===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Location field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.comments_from_lms.n.timestamp":
this.WriteDetailedLog("`1218`");
if(this.RunTimeData.CommentsFromLMS[_58].Timestamp===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Timestamp field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
break;
case "cmi.completion_threshold":
this.WriteDetailedLog("`1363`");
if(this.LearningObject.CompletionThreshold===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The completion threshold for this SCO was not specificed.");
return false;
}
break;
case "cmi.credit":
this.WriteDetailedLog("`1617`");
break;
case "cmi.entry":
this.WriteDetailedLog("`1643`");
break;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR,"The Exit data model element is write-only.");
return false;
case "cmi.interactions._children":
this.WriteDetailedLog("`1321`");
break;
case "cmi.interactions._count":
this.WriteDetailedLog("`1380`");
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1414`");
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1371`");
if(this.RunTimeData.Interactions[_58].Type===null||this.RunTimeData.Interactions[_58].Type===""){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Type field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.objectives._count":
this.WriteDetailedLog("`1146`");
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1199`");
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1294`");
if(this.RunTimeData.Interactions[_58].Timestamp===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Time Stamp field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.correct_responses._count":
this.WriteDetailedLog("`999`");
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`973`");
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1295`");
if(this.RunTimeData.Interactions[_58].Weighting===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Weighting field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1180`");
if(this.RunTimeData.Interactions[_58].LearnerResponse===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Learner Response field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1348`");
if(this.RunTimeData.Interactions[_58].Result===null||this.RunTimeData.Interactions[_58].Result===""){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Result field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1323`");
if(this.RunTimeData.Interactions[_58].Latency===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Latency field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1270`");
if(this.RunTimeData.Interactions[_58].Description===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Description field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.launch_data":
this.WriteDetailedLog("`1532`");
if(this.LearningObject.DataFromLms===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Launch Data field was not specified for this SCO.");
return false;
}
break;
case "cmi.learner_id":
this.WriteDetailedLog("`1542`");
break;
case "cmi.learner_name":
this.WriteDetailedLog("`1519`");
break;
case "cmi.learner_preference._children":
this.WriteDetailedLog("`1230`");
break;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1181`");
if(this.RunTimeData.AudioLevel===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Audio Level field has not been set for this SCO.");
return false;
}
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1241`");
if(this.RunTimeData.LanguagePreference===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Language Preference field has not been set for this SCO.");
return false;
}
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1109`");
if(this.RunTimeData.DeliverySpeed===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Delivery Speed field has not been set for this SCO.");
return false;
}
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1075`");
if(this.RunTimeData.AudioCaptioning===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Audio Captioning field has not been set for this SCO.");
return false;
}
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
if(this.RunTimeData.Location===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Location field has not been set for this SCO.");
return false;
}
break;
case "cmi.max_time_allowed":
this.WriteDetailedLog("`1439`");
if(this.LearningObject.SequencingData===null||this.LearningObject.SequencingData.LimitConditionAttemptAbsoluteDurationControl===false){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Max Time Allowed field was not specified in the manifest for this SCO.");
return false;
}
break;
case "cmi.mode":
this.WriteDetailedLog("`1676`");
break;
case "cmi.objectives._children":
this.WriteDetailedLog("`1365`");
break;
case "cmi.objectives._count":
this.WriteDetailedLog("`1407`");
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1445`");
break;
case "cmi.objectives.n.score._children":
this.WriteDetailedLog("`1229`");
break;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1282`");
if(this.RunTimeData.Objectives[_58].ScoreScaled===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Scaled Score field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1334`");
if(this.RunTimeData.Objectives[_58].ScoreRaw===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Raw Score field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1318`");
if(this.RunTimeData.Objectives[_58].ScoreMin===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Min Score field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1316`");
if(this.RunTimeData.Objectives[_58].ScoreMax===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Max Score field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1251`");
if(this.RunTimeData.Objectives[_58].SuccessStatus===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The SuccessStatus field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1197`");
if(this.RunTimeData.Objectives[_58].CompletionStatus===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The CompletionStatus field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1204`");
if(this.RunTimeData.Objectives[_58].ProgressMeasure===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The ProgressMeasure field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1290`");
if(this.RunTimeData.Objectives[_58].Description===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Description field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1426`");
if(this.RunTimeData.ProgressMeasure===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Progress Measure field has not been set for this SCO.");
return false;
}
break;
case "cmi.scaled_passing_score":
this.WriteDetailedLog("`1366`");
if(this.LearningObject.GetScaledPassingScore()===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Scaled Passing Score field was not specificed for this SCO.");
return false;
}
break;
case "cmi.score._children":
this.WriteDetailedLog("`1446`");
break;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
if(this.RunTimeData.ScoreScaled===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Scaled Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
if(this.RunTimeData.ScoreRaw===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Raw Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
if(this.RunTimeData.ScoreMax===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Max Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
if(this.RunTimeData.ScoreMin===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Min Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.session_time":
this.WriteDetailedLog("`1515`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR,"The Exit data model element is write-only.");
return false;
break;
case "cmi.success_status":
this.WriteDetailedLog("`1480`");
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1513`");
if(this.RunTimeData.SuspendData===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Suspend Data field has not been set for this SCO.");
return false;
}
break;
case "cmi.time_limit_action":
this.WriteDetailedLog("`1411`");
if(this.LearningObject.TimeLimitAction===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Time Limit Action field has not been set for this SCO.");
return false;
}
break;
case "cmi.total_time":
this.WriteDetailedLog("`1545`");
break;
case "adl.nav.request":
this.WriteDetailedLog("`1455`");
break;
case "adl.nav.request_valid.continue":
this.WriteDetailedLog("`1176`");
break;
case "adl.nav.request_valid.previous":
this.WriteDetailedLog("`1183`");
break;
case "adl.nav.request_valid.choice":
Debug.AssertError("entered invalid case in scorm2004 check for getvalue error");
break;
default:
if(_57.indexOf("adl.nav.request_valid.choice.{")===0){
this.WriteDetailedLog("`1587`");
return true;
}
if(_57.indexOf("ssp")===0){
if(SSP_ENABLED){
return this.SSPApi.CheckForGetValueError(_56,_57,_58,_59);
}
}
this.SetErrorState(SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR,"The data model element '"+_56+"' does not exist.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForSetValueError(_5a,_5b,_5c,_5d,_5e){
this.WriteDetailedLog("`1526`"+_5a+", "+_5b+", "+_5c+", "+_5d+", "+_5e+") ");
if(!this.Initialized){
this.SetErrorState(SCORM2004_STORE_DATA_BEFORE_INITIALIZATION_ERROR,"SetValue cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_STORE_DATA_AFTER_TERMINATION_ERROR,"SetValue cannot be called after Terminate has already beeen called.");
return false;
}
if(_5a.length===0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The data model element for SetValue was not specified.");
return false;
}
if(_5a.indexOf("adl.nav.request_valid.choice.{")===0){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.nav.request_valid.choice element is read only");
return false;
}
if(_5d!==""){
if(_5c.indexOf("cmi.comments_from_learner")>=0){
if(_5d>this.RunTimeData.Comments.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Comments From Learner collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.Comments.length+".");
return false;
}
}else{
if(_5c.indexOf("cmi.comments_from_lms")>=0){
if(_5d>this.RunTimeData.CommentsFromLMS.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Comments From LMS collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.CommentsFromLMS.length+".");
return false;
}
}else{
if(_5c.indexOf("cmi.objectives")>=0){
if(_5d>this.RunTimeData.Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Objectives collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.Objectives.length+".");
return false;
}
}else{
if(_5c.indexOf("cmi.interactions")>=0){
if(_5d>this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Interactions collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.Interactions.length+".");
return false;
}else{
if(_5c.indexOf("cmi.interactions.n.correct_responses")>=0){
if(_5d>=this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The Interactions collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.Interactions.length+".");
return false;
}
if(_5e!==""){
if(_5e>this.RunTimeData.Interactions[_5d].CorrectResponses.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Correct Responses collection elements for Interaction #"+_5d+" must be set sequentially the index "+_5e+" is greater than the next available index of "+this.RunTimeData.Interactions[_5d].CorrectResponses.length+".");
return false;
}
}
}else{
if(_5c.indexOf("cmi.interactions.n.objectives")>=0){
if(_5d>=this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The Interactions collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.Interactions.length+".");
return false;
}
if(_5e!==""){
if(_5e>this.RunTimeData.Interactions[_5d].Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Objectives collection elements for Interaction #"+_5d+" must be set sequentially the index "+_5e+" is greater than the next available index of "+this.RunTimeData.Interactions[_5d].Objectives.length+".");
return false;
}
}
}
}
}
}
}
}
}
}
var _5f;
var i;
switch(_5c){
case "cmi._version":
this.WriteDetailedLog("`1572`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi._version data model element is read-only");
return false;
case "cmi.comments_from_learner._children":
this.WriteDetailedLog("`1159`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_learner._children data model element is read-only");
return false;
case "cmi.comments_from_learner._count":
this.WriteDetailedLog("`1226`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_learner._count data model element is read-only");
return false;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1154`");
if(!this.ValidLocalizedString(_5b,4000)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.comments_from_learner.n.comment data model element is not a valid localized string type (SPM 4000)");
return false;
}
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1139`");
if(!this.ValidCharString(_5b,250)){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The cmi.comments_from_learner.n.comment data model element is not a valid char string type (SPM 250)");
return false;
}
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1113`");
if(!this.ValidTime(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.comments_from_learner.n.timestamp data model element is not a valid time");
return false;
}
break;
case "cmi.comments_from_lms._children":
this.WriteDetailedLog("`1244`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms._children data model element is read-only");
return false;
case "cmi.comments_from_lms._count":
this.WriteDetailedLog("`1289`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms._count data model element is read-only");
return false;
case "cmi.comments_from_lms.n.comment":
this.WriteDetailedLog("`1252`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms.comment data model element is read-only");
return false;
case "cmi.comments_from_lms.n.location":
this.WriteDetailedLog("`1235`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms.location data model element is read-only");
return false;
case "cmi.comments_from_lms.n.timestamp":
this.WriteDetailedLog("`1218`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms.timestamp data model element is read-only");
return false;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
if(_5b!=SCORM_STATUS_COMPLETED&&_5b!=SCORM_STATUS_INCOMPLETE&&_5b!=SCORM_STATUS_NOT_ATTEMPTED&&_5b!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The completion_status data model element must be a proper vocabulary element.");
return false;
}
break;
case "cmi.completion_threshold":
this.WriteDetailedLog("`1363`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The completion_threshold data model element is read-only");
return false;
case "cmi.credit":
this.WriteDetailedLog("`1617`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The credit data model element is read-only");
return false;
case "cmi.entry":
this.WriteDetailedLog("`1643`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The entry data model element is read-only");
return false;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
if(_5b!=SCORM_EXIT_TIME_OUT&&_5b!=SCORM_EXIT_SUSPEND&&_5b!=SCORM_EXIT_LOGOUT&&_5b!=SCORM_EXIT_NORMAL&&_5b!=SCORM_EXIT_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The exit data model element must be a proper vocabulary element.");
return false;
}
if(_5b==SCORM_EXIT_LOGOUT){
this.WriteDetailedLog("`349`");
}
break;
case "cmi.interactions._children":
this.WriteDetailedLog("`1321`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions._children element is read-only");
return false;
case "cmi.interactions._count":
this.WriteDetailedLog("`1380`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions._count element is read-only");
return false;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1414`");
if(!this.ValidLongIdentifier(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".id value of '"+_5b+"' is not a valid long identifier.");
return false;
}
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1371`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(_5b!=SCORM_TRUE_FALSE&&_5b!=SCORM_CHOICE&&_5b!=SCORM_FILL_IN&&_5b!=SCORM_LONG_FILL_IN&&_5b!=SCORM_LIKERT&&_5b!=SCORM_MATCHING&&_5b!=SCORM_PERFORMANCE&&_5b!=SCORM_SEQUENCING&&_5b!=SCORM_NUMERIC&&_5b!=SCORM_OTHER){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".type value of '"+_5b+"' is not a valid interaction type.");
return false;
}
break;
case "cmi.interactions.n.objectives._count":
this.WriteDetailedLog("`1146`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions.objectives._count element is read-only");
return false;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1199`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidLongIdentifier(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".objectives."+_5e+".id value of '"+_5b+"' is not a valid long identifier type.");
return false;
}
for(i=0;i<this.RunTimeData.Interactions[_5d].Objectives.length;i++){
if((this.RunTimeData.Interactions[_5d].Objectives[i]==_5b)&&(i!=_5e)){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Every interaction objective identifier must be unique. The value '"+_5b+"' has already been set in objective #"+i);
return false;
}
}
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1294`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidTime(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".timestamp value of '"+_5b+"' is not a valid time type.");
return false;
}
break;
case "cmi.interactions.n.correct_responses._count":
this.WriteDetailedLog("`999`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions.correct_responses._count element is read-only");
return false;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`973`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Type===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.type element must be set before a correct response can be set.");
return false;
}
_5f=true;
if(RegistrationToDeliver.Package.Properties.ValidateInteractionResponses){
switch(this.RunTimeData.Interactions[_5d].Type){
case SCORM_TRUE_FALSE:
if(this.RunTimeData.Interactions[_5d].CorrectResponses.length>0&&_5e>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"A true-false interaction can only have one correct response.");
return false;
}
_5f=this.ValidTrueFalseResponse(_5b);
break;
case SCORM_CHOICE:
_5f=this.ValidMultipleChoiceResponse(_5b);
for(i=0;i<this.RunTimeData.Interactions[_5d].CorrectResponses.length;i++){
if(this.RunTimeData.Interactions[_5d].CorrectResponses[i]==_5b){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Every correct response to a choice interaction must be unique. The value '"+_5b+"' has already been set in correct response #"+i);
return false;
}
}
break;
case SCORM_FILL_IN:
_5f=this.ValidFillInResponse(_5b,true);
break;
case SCORM_LONG_FILL_IN:
_5f=this.ValidLongFillInResponse(_5b,true);
break;
case SCORM_LIKERT:
if(this.RunTimeData.Interactions[_5d].CorrectResponses.length>0&&_5e>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"A likert interaction can only have one correct response.");
return false;
}
_5f=this.ValidLikeRTResponse(_5b);
break;
case SCORM_MATCHING:
_5f=this.ValidMatchingResponse(_5b);
break;
case SCORM_PERFORMANCE:
_5f=this.ValidPerformanceResponse(_5b,true);
break;
case SCORM_SEQUENCING:
_5f=this.ValidSequencingResponse(_5b);
break;
case SCORM_NUMERIC:
if(this.RunTimeData.Interactions[_5d].CorrectResponses.length>0&&_5e>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"A numeric interaction can only have one correct response.");
return false;
}
_5f=this.ValidNumericResponse(_5b,true);
break;
case SCORM_OTHER:
if(this.RunTimeData.Interactions[_5d].CorrectResponses.length>0&&_5e>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"An 'other' interaction can only have one correct response.");
return false;
}
_5f=this.ValidOtheresponse(_5b);
break;
}
}
if(!_5f){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".correct_responses."+_5e+".pattern value of '"+_5b+"' is not a valid correct response to an interaction of type "+this.RunTimeData.Interactions[_5d].Type+".");
return false;
}
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1295`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".weighting value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1161`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(this.RunTimeData.Interactions[_5d].Type===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.type element must be set before a learner response can be set.");
return false;
}
_5f=true;
if(RegistrationToDeliver.Package.Properties.ValidateInteractionResponses){
switch(this.RunTimeData.Interactions[_5d].Type){
case "true-false":
_5f=this.ValidTrueFalseResponse(_5b);
break;
case "choice":
_5f=this.ValidMultipleChoiceResponse(_5b);
break;
case "fill-in":
_5f=this.ValidFillInResponse(_5b,false);
break;
case "long-fill-in":
_5f=this.ValidLongFillInResponse(_5b,false);
break;
case "likert":
_5f=this.ValidLikeRTResponse(_5b);
break;
case "matching":
_5f=this.ValidMatchingResponse(_5b);
break;
case "performance":
_5f=this.ValidPerformanceResponse(_5b,false);
break;
case "sequencing":
_5f=this.ValidSequencingResponse(_5b);
break;
case "numeric":
_5f=this.ValidNumericResponse(_5b,false);
break;
case "other":
_5f=this.ValidOtheresponse(_5b);
break;
}
}
if(!_5f){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".learner_response value of '"+_5b+"' is not a valid response to an interaction of type "+this.RunTimeData.Interactions[_5d].Type+".");
return false;
}
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1348`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(_5b!=SCORM_CORRECT&&_5b!=SCORM_INCORRECT&&_5b!=SCORM_UNANTICIPATED&&_5b!=SCORM_NEUTRAL){
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".result value of '"+_5b+"' is not a valid interaction result.");
return false;
}
}
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1323`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidTimeInterval(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".latency value of '"+_5b+"' is not a valid timespan.");
return false;
}
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1270`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidLocalizedString(_5b,250)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".description value of '"+_5b+"' is not a valid localized string SPM 250.");
return false;
}
break;
case "cmi.launch_data":
this.WriteDetailedLog("`1532`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.launch_data element is read-only");
return false;
case "cmi.learner_id":
this.WriteDetailedLog("`1542`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.learner_id element is read-only");
return false;
case "cmi.learner_name":
this.WriteDetailedLog("`1519`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.learner_name element is read-only");
return false;
case "cmi.learner_preference._children":
this.WriteDetailedLog("`1230`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.learner_preference._children element is read-only");
return false;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1181`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.audio_level value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<0){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.learner_preference.audio_level value of '"+_5b+"' must be greater than zero.");
return false;
}
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1241`");
if(!this.ValidLanguage(_5b,true)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.language value of '"+_5b+"' is not a valid language.");
return false;
}
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1109`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.delivery_speed value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<0){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.learner_preference.delivery_speed value of '"+_5b+"' must be greater than zero.");
return false;
}
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1075`");
if(_5b!="-1"&&_5b!="0"&&_5b!="1"){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.audio_captioning value of '"+_5b+"' must be -1, 0 or 1.");
return false;
}
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
if(!this.ValidCharString(_5b,1000)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.location value of '"+_5b+"' is not a valid char string SPM 1000.");
return false;
}
break;
case "cmi.max_time_allowed":
this.WriteDetailedLog("`1439`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.max_time_allowed element is read only");
return false;
case "cmi.mode":
this.WriteDetailedLog("`1676`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.mode element is read only");
return false;
case "cmi.objectives._children":
this.WriteDetailedLog("`1365`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.objectives._children element is read only");
return false;
case "cmi.objectives._count":
this.WriteDetailedLog("`1407`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.objectives._count element is read only");
return false;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1445`");
if(!this.ValidLongIdentifier(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives.n.id value of '"+_5b+"' is not a valid long identifier.");
return false;
}
if(this.RunTimeData.Objectives[_5d]!=undefined&&this.RunTimeData.Objectives[_5d].Identifier!=null){
if(this.RunTimeData.Objectives[_5d].Identifier!==null&&this.RunTimeData.Objectives[_5d].Identifier!=_5b){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Objective identifiers may only be set once and may not be overwritten. The objective at index "+_5d+" already has the identifier "+this.RunTimeData.Objectives[_5d].Identifier);
return false;
}
}
for(i=0;i<this.RunTimeData.Objectives.length;i++){
if((this.RunTimeData.Objectives[i].Identifier==_5b)&&(i!=_5d)){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Every objective identifier must be unique. The value '"+_5b+"' has already been set in objective #"+i);
return false;
}
}
break;
case "cmi.objectives.n.score._children":
this.WriteDetailedLog("`1229`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.objectives.n.score._children element is read only");
return false;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1282`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".score.scaled value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<-1||parseFloat(_5b)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.objectives."+_5d+".score.scaled value of '"+_5b+"' must be between -1 and 1.");
return false;
}
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1334`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".score.raw value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1318`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".score.min value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1316`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".score.max value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1251`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(_5b!=SCORM_STATUS_PASSED&&_5b!=SCORM_STATUS_FAILED&&_5b!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".success_status value of '"+_5b+"' is not a valid success status.");
return false;
}
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1197`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(_5b!=SCORM_STATUS_COMPLETED&&_5b!=SCORM_STATUS_INCOMPLETE&&_5b!=SCORM_STATUS_NOT_ATTEMPTED&&_5b!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".completion_status value of '"+_5b+"' is not a valid completion status.");
return false;
}
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1204`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".progress_measure value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<0||parseFloat(_5b)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.objectives."+_5d+".progress_measure value of '"+_5b+"' must be between 0 and 1.");
return false;
}
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1290`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidLocalizedString(_5b,250)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".description value of '"+_5b+"' is not a valid localized string SPM 250.");
return false;
}
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1426`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.progress_measure value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<0||parseFloat(_5b)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.pogress_measure value of '"+_5b+"' must be between 0 and 1.");
return false;
}
break;
case "cmi.scaled_passing_score":
this.WriteDetailedLog("`1366`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.scaled_passing_score element is read only");
return false;
case "cmi.score._children":
this.WriteDetailedLog("`1446`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.score._children element is read only");
return false;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.scaled value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<-1||parseFloat(_5b)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi..score.scaled value of '"+_5b+"' must be between -1 and 1.");
return false;
}
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.raw value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.raw value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.raw value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.session_time":
this.WriteDetailedLog("`1515`");
if(!this.ValidTimeInterval(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.session_time value of '"+_5b+"' is not a valid time intervals.");
return false;
}
break;
case "cmi.success_status":
this.WriteDetailedLog("`1480`");
if(_5b!=SCORM_STATUS_PASSED&&_5b!=SCORM_STATUS_FAILED&&_5b!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.success_status value of '"+_5b+"' is not a valid success status.");
return false;
}
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1513`");
if(!this.ValidCharString(_5b,64000)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.suspend_data value of '"+_5b+"' is not a valid char string SPM 64000.");
return false;
}
break;
case "cmi.time_limit_action":
this.WriteDetailedLog("`1411`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.time_limit_action element is read only");
return false;
case "cmi.total_time":
this.WriteDetailedLog("`1545`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.total_time element is read only");
return false;
case "adl.nav.request":
this.WriteDetailedLog("`1455`");
if(_5b.substring(0,1)=="{"){
var _61=_5b.substring(0,_5b.indexOf("}")+1);
if(Control.IsTargetValid(_61)===false){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The value '"+_61+"' is not a valid target of a choice request.");
return false;
}
if(_5b.indexOf("choice")!=_61.length){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"A target may only be provided for a choice request.");
return false;
}
}else{
if(_5b!=SCORM_RUNTIME_NAV_REQUEST_CONTINUE&&_5b!=SCORM_RUNTIME_NAV_REQUEST_PREVIOUS&&_5b!=SCORM_RUNTIME_NAV_REQUEST_CHOICE&&_5b!=SCORM_RUNTIME_NAV_REQUEST_EXIT&&_5b!=SCORM_RUNTIME_NAV_REQUEST_EXITALL&&_5b!=SCORM_RUNTIME_NAV_REQUEST_ABANDON&&_5b!=SCORM_RUNTIME_NAV_REQUEST_ABANDONALL&&_5b!=SCORM_RUNTIME_NAV_REQUEST_SUSPENDALL&&_5b!=SCORM_RUNTIME_NAV_REQUEST_NONE){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The adl.nav.request value of '"+_5b+"' is not a valid nav request.");
return false;
}
}
break;
case "adl.nav.request_valid.continue":
this.WriteDetailedLog("`1176`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.nav.request_valid.continue element is read only");
return false;
case "adl.nav.request_valid.previous":
this.WriteDetailedLog("`1183`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.nav.request_valid.previous element is read only");
return false;
case "adl.nav.request_valid.choice":
this.WriteDetailedLog("`1223`");
break;
default:
if(_5c.indexOf("ssp")===0){
if(SSP_ENABLED){
return this.SSPApi.CheckForSetValueError(_5a,_5b,_5c,_5d,_5e);
}
}
this.SetErrorState(SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR,"The data model element '"+_5a+"' is not defined in SCORM 2004");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_ValidCharString(str,_63){
this.WriteDetailedLog("`1466`");
return true;
}
function RunTimeApi_ValidLocalizedString(str,_65){
this.WriteDetailedLog("`1373`");
var _66;
var _67=new String();
var _68;
_66=str;
if(str.indexOf("{lang=")===0){
_68=str.indexOf("}");
if(_68>0){
_67=str.substr(0,_68);
_67=_67.replace(/\{lang=/,"");
_67=_67.replace(/\}/,"");
if(!this.ValidLanguage(_67,false)){
return false;
}
if(str.length>=(_68+2)){
_66=str.substring(_68+1);
}else{
_66="";
}
}
}
return true;
}
function RunTimeApi_ExtractLanguageDelimiterFromLocalizedString(str){
var _6a;
var _6b="";
if(str.indexOf("{lang=")===0){
_6a=str.indexOf("}");
if(_6a>0){
_6b=str.substr(0,_6a+1);
}
}
return _6b;
}
function RunTimeApi_ValidLanguage(str,_6d){
this.WriteDetailedLog("`1514`");
var _6e;
if(str.length===0){
if(_6d){
return true;
}else{
return false;
}
}
_6e=str.split("-");
for(var i=0;i<_6e.length;i++){
if(_6e[i].length>8){
return false;
}
if(_6e[i].length<2){
if(_6e[i]!="i"&&_6e[i]!="x"){
return false;
}
}
}
var _70=new Iso639LangCodes_LangCodes();
if(!_70.IsValid(_6e[0].toLowerCase())){
return false;
}
if(str.length>250){
return false;
}
return true;
}
function RunTimeApi_ValidLongIdentifier(str){
this.WriteDetailedLog("`1396`");
str=str.trim();
if(!this.ValidIdentifier(str)){
return false;
}
return true;
}
function RunTimeApi_ValidShortIdentifier(str){
this.WriteDetailedLog("`1382`");
if(!this.ValidIdentifier(str)){
return false;
}
return true;
}
function RunTimeApi_ValidIdentifier(str){
this.WriteDetailedLog("`1463`");
str=str.trim();
if(str.length===0){
return false;
}
if(str.toLowerCase().indexOf("urn:")===0){
return this.IsValidUrn(str);
}
if(str.search(/\w/)<0){
return false;
}
if(str.search(/[^\w\-\(\)\+\.\:\=\@\;\$\_\!\*\'\%]/)>=0){
return false;
}
return true;
}
function RunTimeApi_IsValidUrn(str){
this.WriteDetailedLog("`1568`");
var _75=str.split(":");
var nid=new String("");
var nss="";
if(_75.length>1){
nid=_75[1];
}else{
return false;
}
if(_75.length>2){
nss=_75[2];
}
if(nid.length===0){
return false;
}
if(nid.indexOf(" ")>0||nss.indexOf(" ")>0){
return false;
}
return true;
}
function RunTimeApi_ValidReal(str){
this.WriteDetailedLog("`1578`");
if(str.search(/[^.\d-]/)>-1){
return false;
}
if(str.search("-")>-1){
if(str.indexOf("-",1)>-1){
return false;
}
}
if(str.indexOf(".")!=str.lastIndexOf(".")){
return false;
}
if(str.search(/\d/)<0){
return false;
}
return true;
}
function RunTimeApi_ValidTime(str){
this.WriteDetailedLog("`1571`");
var _7a="";
var _7b="";
var day="";
var _7d="";
var _7e="";
var _7f="";
var _80="";
var _81="";
var _82="";
var _83="";
var _84;
str=new String(str);
var _85=/^(\d\d\d\d)(-(\d\d)(-(\d\d)(T(\d\d)(:(\d\d)(:(\d\d))?)?)?)?)?/;
if(str.search(_85)!==0){
return false;
}
if(str.substr(str.length-1,1).search(/[\-T\:]/)>=0){
return false;
}
var len=str.length;
if(len!=4&&len!=7&&len!=10&&len!=13&&len!=16&&len<19){
return false;
}
if(len>=5){
if(str.substr(4,1)!="-"){
return false;
}
}
if(len>=8){
if(str.substr(7,1)!="-"){
return false;
}
}
if(len>=11){
if(str.substr(10,1)!="T"){
return false;
}
}
if(len>=14){
if(str.substr(13,1)!=":"){
return false;
}
}
if(len>=17){
if(str.substr(16,1)!=":"){
return false;
}
}
var _87=str.match(_85);
_7a=_87[1];
_7b=_87[3];
day=_87[5];
_7d=_87[7];
_7e=_87[9];
_7f=_87[11];
if(str.length>19){
if(str.length<21){
return false;
}
if(str.substr(19,1)!="."){
return false;
}
_84=str.substr(20,1);
if(_84.search(/\d/)<0){
return false;
}else{
_80+=_84;
}
for(var i=21;i<str.length;i++){
_84=str.substr(i,1);
if((i==21)&&(_84.search(/\d/)===0)){
_80+=_84;
}else{
_81+=_84;
}
}
}
if(_81.length===0){
}else{
if(_81.length==1){
if(_81!="Z"){
return false;
}
}else{
if(_81.length==3){
if(_81.search(/[\+\-]\d\d/)!==0){
return false;
}else{
_82=_81.substr(1,2);
}
}else{
if(_81.length==6){
if(_81.search(/[\+\-]\d\d:\d\d/)!==0){
return false;
}else{
_82=_81.substr(1,2);
_83=_81.substr(4,2);
}
}else{
return false;
}
}
}
}
if(_7a<1970||_7a>2038){
return false;
}
if(_7b!==undefined&&_7b!==""){
_7b=parseInt(_7b,10);
if(_7b<1||_7b>12){
return false;
}
}
if(day!==undefined&&day!==""){
var dtm=new Date(_7a,(_7b-1),day);
if(dtm.getDate()!=day){
return false;
}
}
if(_7d!==undefined&&_7d!==""){
_7d=parseInt(_7d,10);
if(_7d<0||_7d>23){
return false;
}
}
if(_7e!==undefined&&_7e!==""){
_7e=parseInt(_7e,10);
if(_7e<0||_7e>59){
return false;
}
}
if(_7f!==undefined&&_7f!==""){
_7f=parseInt(_7f,10);
if(_7f<0||_7f>59){
return false;
}
}
if(_82!==undefined&&_82!==""){
_82=parseInt(_82,10);
if(_82<0||_82>23){
return false;
}
}
if(_83!==undefined&&_83!==""){
_83=parseInt(_83,10);
if(_83<0||_83>59){
return false;
}
}
return true;
}
function RunTimeApi_ValidTimeInterval(str){
this.WriteDetailedLog("`1425`");
var _8b=/^P(\d+Y)?(\d+M)?(\d+D)?(T(\d+H)?(\d+M)?(\d+(.\d\d?)?S)?)?$/;
if(str=="P"){
return false;
}
if(str.lastIndexOf("T")==(str.length-1)){
return false;
}
if(str.search(_8b)<0){
return false;
}
return true;
}
function RunTimeApi_ValidTrueFalseResponse(str){
this.WriteDetailedLog("`1346`");
var _8d=/^(true|false)$/;
if(str.search(_8d)<0){
return false;
}
return true;
}
function RunTimeApi_ValidMultipleChoiceResponse(str){
this.WriteDetailedLog("`1254`");
if(str.length===0){
return true;
}
return (this.IsValidArrayOfShortIdentifiers(str,36,true)||this.IsValidCommaDelimitedArrayOfShortIdentifiers(str,36,true));
}
function RunTimeApi_IsValidCommaDelimitedArrayOfShortIdentifiers(str,_90,_91){
this.WriteDetailedLog("`1205`");
var _92=",";
var _93=str.split(_92);
for(var i=0;i<_93.length;i++){
if(!this.ValidShortIdentifier(_93[i])){
return false;
}
if(_91){
for(var j=0;j<_93.length;j++){
if(j!=i){
if(_93[j]==_93[i]){
return false;
}
}
}
}
}
return true;
}
function RunTimeApi_IsValidArrayOfShortIdentifiers(str,_97,_98){
this.WriteDetailedLog("`1205`");
var _99="[,]";
var _9a=str.split(_99);
for(var i=0;i<_9a.length;i++){
if(!this.ValidShortIdentifier(_9a[i])){
return false;
}
if(_98){
for(var j=0;j<_9a.length;j++){
if(j!=i){
if(_9a[j]==_9a[i]){
return false;
}
}
}
}
}
return true;
}
function RunTimeApi_IsValidArrayOfLocalizedStrings(str,_9e,_9f){
this.WriteDetailedLog("`1212`");
var _a0="[,]";
var _a1=str.split(_a0);
for(var i=0;i<_a1.length;i++){
if(!this.ValidLocalizedString(_a1[i],0)){
return false;
}
if(_9f){
for(var j=0;j<_a1.length;j++){
if(j!=i){
if(_a1[j]==_a1[i]){
return false;
}
}
}
}
}
return true;
}
function RunTimeApi_ValidFillInResponse(str,_a5){
this.WriteDetailedLog("`1391`");
if(str.length===0){
return true;
}
var _a6=/^\{case_matters=/;
var _a7=/^\{order_matters=/;
var _a8=/^\{lang=[\w\-]+\}\{/;
var _a9=/^\{case_matters=(true|false)\}/;
var _aa=/^\{order_matters=(true|false)\}/;
var _ab=new String(str);
if(_a5){
if(_ab.search(_a6)>=0){
if(_ab.search(_a9)<0){
return false;
}
_ab=_ab.replace(_a9,"");
}
if(_ab.search(_a7)>=0){
if(_ab.search(_aa)<0){
return false;
}
_ab=_ab.replace(_aa,"");
}
if(_ab.search(_a6)>=0){
if(_ab.search(_a9)<0){
return false;
}
_ab=_ab.replace(_a9,"");
}
}
return this.IsValidArrayOfLocalizedStrings(_ab,10,false);
}
function RunTimeApi_ValidLongFillInResponse(str,_ad){
this.WriteDetailedLog("`1328`");
var _ae=/^\{case_matters=/;
var _af=/^\{case_matters=(true|false)\}/;
var _b0=new String(str);
if(_ad){
if(_b0.search(_ae)>=0){
if(_b0.search(_af)<0){
return false;
}
_b0=_b0.replace(_af,"");
}
}
return this.ValidLocalizedString(_b0,4000);
}
function RunTimeApi_ValidLikeRTResponse(str){
this.WriteDetailedLog("`1401`");
return this.ValidShortIdentifier(str);
}
function RunTimeApi_ValidMatchingResponse(str){
this.WriteDetailedLog("`1364`");
var _b3="[,]";
var _b4="[.]";
var _b5;
var _b6;
_b5=str.split(_b3);
for(var i=0;i<_b5.length;i++){
_b6=_b5[i].split(_b4);
if(_b6.length!=2){
return false;
}
if(!this.ValidShortIdentifier(_b6[0])){
return false;
}
if(!this.ValidShortIdentifier(_b6[1])){
return false;
}
}
return true;
}
function RunTimeApi_ValidPerformanceResponse(str,_b9){
this.WriteDetailedLog("`1300`");
var _ba=/^\{order_matters=/;
var _bb=/^\{order_matters=(true|false)\}/;
var _bc;
var _bd;
var _be;
var _bf;
var _c0;
var _c1=new String(str);
if(str.length===0){
return false;
}
if(_b9){
if(_c1.search(_ba)>=0){
if(_c1.search(_bb)<0){
return false;
}
_c1=_c1.replace(_bb,"");
}
}
_bc=_c1.split("[,]");
if(_bc.length===0){
return false;
}
for(var i=0;i<_bc.length;i++){
_bd=_bc[i];
if(_bd.length===0){
return false;
}
_be=_bd.split("[.]");
if(_be.length==2){
_bf=_be[0];
_c0=_be[1];
if(_bf.length===0&&_c0===0){
return false;
}
if(_bf.length>0){
if(!this.ValidShortIdentifier(_bf)){
return false;
}
}
}else{
return false;
}
}
return true;
}
function RunTimeApi_ValidSequencingResponse(str){
this.WriteDetailedLog("`1339`");
return this.IsValidArrayOfShortIdentifiers(str,36,false);
}
function RunTimeApi_ValidNumericResponse(str,_c5){
this.WriteDetailedLog("`1372`");
var _c6="[:]";
var _c7=str.split(_c6);
if(_c5){
if(_c7.length>2){
return false;
}
}else{
if(_c7.length>1){
return false;
}
}
for(var i=0;i<_c7.length;i++){
if(!this.ValidReal(_c7[i])){
return false;
}
}
if(_c7.length>=2){
if(_c7[0].length>0&&_c7[1].length>0){
if(parseFloat(_c7[0])>parseFloat(_c7[1])){
return false;
}
}
}
return true;
}
function RunTimeApi_ValidOtheresponse(str){
this.WriteDetailedLog("`1429`");
return true;
}
function RunTimeApi_TranslateBooleanIntoCMI(_ca){
if(_ca===true){
return SCORM_TRUE;
}else{
if(_ca===false){
return SCORM_FALSE;
}else{
return SCORM_UNKNOWN;
}
}
}
function RunTimeApi_GetCompletionStatus(){
if(this.LearningObject.CompletionThreshold!==null){
if(this.RunTimeData.ProgressMeasure!==null){
if(parseFloat(this.RunTimeData.ProgressMeasure)>=parseFloat(this.LearningObject.CompletionThreshold)){
this.WriteDetailedLog("`619`");
return SCORM_STATUS_COMPLETED;
}else{
this.WriteDetailedLog("`576`");
return SCORM_STATUS_INCOMPLETE;
}
}else{
this.WriteDetailedLog("`996`"+this.LearningObject.CompletionThreshold+"`676`");
return SCORM_STATUS_UNKNOWN;
}
}
this.WriteDetailedLog("`623`"+this.RunTimeData.CompletionStatus);
return this.RunTimeData.CompletionStatus;
}
function RunTimeApi_GetSuccessStatus(){
var _cb=this.LearningObject.GetScaledPassingScore();
if(_cb!==null){
if(this.RunTimeData.ScoreScaled!==null){
if(parseFloat(this.RunTimeData.ScoreScaled)>=parseFloat(_cb)){
this.WriteDetailedLog("`698`");
return SCORM_STATUS_PASSED;
}else{
this.WriteDetailedLog("`650`");
return SCORM_STATUS_FAILED;
}
}else{
this.WriteDetailedLog("`577`");
return SCORM_STATUS_UNKNOWN;
}
}
this.WriteDetailedLog("`640`"+this.RunTimeData.SuccessStatus);
return this.RunTimeData.SuccessStatus;
}
function RunTimeApi_InitTrackedTimeStart(_cc){
this.TrackedStartDate=new Date();
this.StartSessionTotalTime=_cc.RunTime.TotalTime;
}
function RunTimeApi_AccumulateTotalTrackedTime(){
this.TrackedEndDate=new Date();
var _cd=Math.round((this.TrackedEndDate-this.TrackedStartDate)/10);
var _ce=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTimeTracked);
var _cf=_cd+_ce;
this.RunTimeData.TotalTimeTracked=ConvertHundredthsToIso8601TimeSpan(_cf);
this.Activity.ActivityEndedDate=this.TrackedEndDate;
var _d0=GetDateFromUtcIso8601Time(this.Activity.GetActivityStartTimestampUtc());
var _d1=GetDateFromUtcIso8601Time(this.Activity.GetAttemptStartTimestampUtc());
this.Activity.SetActivityAbsoluteDuration(ConvertHundredthsToIso8601TimeSpan((this.TrackedEndDate-_d0)/10));
this.Activity.SetAttemptAbsoluteDuration(ConvertHundredthsToIso8601TimeSpan((this.TrackedEndDate-_d1)/10));
var _d2=ConvertIso8601TimeSpanToHundredths(this.Activity.GetActivityExperiencedDurationTracked());
var _d3=ConvertHundredthsToIso8601TimeSpan(_d2+_cd);
this.Activity.SetActivityExperiencedDurationTracked(_d3);
var _d4=ConvertIso8601TimeSpanToHundredths(this.Activity.GetActivityExperiencedDurationReported());
var _d5=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTime)-ConvertIso8601TimeSpanToHundredths(this.StartSessionTotalTime);
var _d6=ConvertHundredthsToIso8601TimeSpan(_d4+_d5);
this.Activity.SetActivityExperiencedDurationReported(_d6);
var _d7=ConvertIso8601TimeSpanToHundredths(this.Activity.GetAttemptExperiencedDurationTracked());
var _d8=ConvertHundredthsToIso8601TimeSpan(_d7+_cd);
this.Activity.SetAttemptExperiencedDurationTracked(_d8);
var _d9=ConvertIso8601TimeSpanToHundredths(this.Activity.GetAttemptExperiencedDurationReported());
var _da=ConvertHundredthsToIso8601TimeSpan(_d9+_d5);
this.Activity.SetAttemptExperiencedDurationReported(_da);
}
function RunTimeApi_SetLookAheadDirtyDataFlagIfNeeded(_db,_dc){
if(this.IsLookAheadSequencerDataDirty==false&&_db!=_dc){
this.IsLookAheadSequencerDataDirty=true;
}
}
var TERMINATION_REQUEST_EXIT="EXIT";
var TERMINATION_REQUEST_EXIT_ALL="EXIT ALL";
var TERMINATION_REQUEST_SUSPEND_ALL="SUSPEND ALL";
var TERMINATION_REQUEST_ABANDON="ABANDON";
var TERMINATION_REQUEST_ABANDON_ALL="ABANDON ALL";
var TERMINATION_REQUEST_EXIT_PARENT="EXIT PARENT";
var TERMINATION_REQUEST_NOT_VALID="INVALID";
var SEQUENCING_REQUEST_START="START";
var SEQUENCING_REQUEST_RESUME_ALL="RESUME ALL";
var SEQUENCING_REQUEST_CONTINUE="CONTINUE";
var SEQUENCING_REQUEST_PREVIOUS="PREVIOUS";
var SEQUENCING_REQUEST_CHOICE="CHOICE";
var SEQUENCING_REQUEST_RETRY="RETRY";
var SEQUENCING_REQUEST_EXIT="EXIT";
var SEQUENCING_REQUEST_NOT_VALID="INVALID";
var RULE_SET_POST_CONDITION="POST_CONDITION";
var RULE_SET_EXIT="EXIT";
var RULE_SET_HIDE_FROM_CHOICE="HIDE_FROM_CHOICE";
var RULE_SET_STOP_FORWARD_TRAVERSAL="STOP_FORWARD_TRAVERSAL";
var RULE_SET_DISABLED="DISABLED";
var RULE_SET_SKIPPED="SKIPPED";
var RULE_SET_SATISFIED="SATISFIED";
var RULE_SET_NOT_SATISFIED="NOT_SATISFIED";
var RULE_SET_COMPLETED="COMPLETED";
var RULE_SET_INCOMPLETE="INCOMPLETE";
var SEQUENCING_RULE_ACTION_SKIP="Skip";
var SEQUENCING_RULE_ACTION_DISABLED="Disabled";
var SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE="Hidden From Choice";
var SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL="Stop Forward Traversal";
var SEQUENCING_RULE_ACTION_EXIT="Exit";
var SEQUENCING_RULE_ACTION_EXIT_PARENT="Exit Parent";
var SEQUENCING_RULE_ACTION_EXIT_ALL="Exit All";
var SEQUENCING_RULE_ACTION_RETRY="Retry";
var SEQUENCING_RULE_ACTION_RETRY_ALL="Retry All";
var SEQUENCING_RULE_ACTION_CONTINUE="Continue";
var SEQUENCING_RULE_ACTION_PREVIOUS="Previous";
var FLOW_DIRECTION_FORWARD="FORWARD";
var FLOW_DIRECTION_BACKWARD="BACKWARD";
var RULE_CONDITION_OPERATOR_NOT="Not";
var RULE_CONDITION_COMBINATION_ALL="All";
var RULE_CONDITION_COMBINATION_ANY="Any";
var RESULT_UNKNOWN="unknown";
var SEQUENCING_RULE_CONDITION_SATISFIED="Satisfied";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN="Objective Status Known";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN="Objective Measure Known";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_GREATER_THAN="Objective Measure Greater Than";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_LESS_THAN="Objective Measure Less Than";
var SEQUENCING_RULE_CONDITION_COMPLETED="Completed";
var SEQUENCING_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN="Activity Progress Known";
var SEQUENCING_RULE_CONDITION_ATTEMPTED="Attempted";
var SEQUENCING_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED="Attempt Limit Exceeded";
var SEQUENCING_RULE_CONDITION_ALWAYS="Always";
var ROLLUP_RULE_ACTION_SATISFIED="Satisfied";
var ROLLUP_RULE_ACTION_NOT_SATISFIED="Not Satisfied";
var ROLLUP_RULE_ACTION_COMPLETED="Completed";
var ROLLUP_RULE_ACTION_INCOMPLETE="Incomplete";
var CHILD_ACTIVITY_SET_ALL="All";
var CHILD_ACTIVITY_SET_ANY="Any";
var CHILD_ACTIVITY_SET_NONE="None";
var CHILD_ACTIVITY_SET_AT_LEAST_COUNT="At Least Count";
var CHILD_ACTIVITY_SET_AT_LEAST_PERCENT="At Least Percent";
var ROLLUP_RULE_CONDITION_SATISFIED="Satisfied";
var ROLLUP_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN="Objective Status Known";
var ROLLUP_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN="Objective Measure Known";
var ROLLUP_RULE_CONDITION_COMPLETED="Completed";
var ROLLUP_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN="Activity Progress Known";
var ROLLUP_RULE_CONDITION_ATTEMPTED="Attempted";
var ROLLUP_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED="Attempt Limit Exceeded";
var ROLLUP_RULE_CONDITION_NEVER="Never";
var ROLLUP_CONSIDERATION_ALWAYS="Always";
var ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED="If Not Suspended";
var ROLLUP_CONSIDERATION_IF_ATTEMPTED="If Attempted";
var ROLLUP_CONSIDERATION_IF_NOT_SKIPPED="If Not Skipped";
var TIMING_NEVER="Never";
var TIMING_ONCE="Once";
var TIMING_ON_EACH_NEW_ATTEMPT="On Each New Attempt";
var CONTROL_CHOICE_EXIT_ERROR_NAV="NB.2.1-8";
var CONTROL_CHOICE_EXIT_ERROR_CHOICE="SB.2.9-7";
var PREVENT_ACTIVATION_ERROR="SB.2.9-6";
var CONSTRAINED_CHOICE_ERROR="SB.2.9-8";
function Sequencer(_dd,_de){
this.LookAhead=_dd;
this.Activities=_de;
this.NavigationRequest=null;
this.ChoiceTargetIdentifier=null;
this.SuspendedActivity=null;
this.CurrentActivity=null;
this.Exception=null;
this.ExceptionText=null;
this.GlobalObjectives=new Array();
this.ReturnToLmsInvoked=false;
}
Sequencer.prototype.OverallSequencingProcess=Sequencer_OverallSequencingProcess;
Sequencer.prototype.SetSuspendedActivity=Sequencer_SetSuspendedActivity;
Sequencer.prototype.GetSuspendedActivity=Sequencer_GetSuspendedActivity;
Sequencer.prototype.Start=Sequencer_Start;
Sequencer.prototype.InitialRandomizationAndSelection=Sequencer_InitialRandomizationAndSelection;
Sequencer.prototype.GetCurrentActivity=Sequencer_GetCurrentActivity;
Sequencer.prototype.GetExceptionText=Sequencer_GetExceptionText;
Sequencer.prototype.GetExitAction=Sequencer_GetExitAction;
Sequencer.prototype.EvaluatePossibleNavigationRequests=Sequencer_EvaluatePossibleNavigationRequests;
Sequencer.prototype.InitializePossibleNavigationRequestAbsolutes=Sequencer_InitializePossibleNavigationRequestAbsolutes;
Sequencer.prototype.SetAllDescendentsToDisabled=Sequencer_SetAllDescendentsToDisabled;
Sequencer.prototype.ContentDeliveryEnvironmentActivityDataSubProcess=Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess;
function Sequencer_SetSuspendedActivity(_df){
this.SuspendedActivity=_df;
}
function Sequencer_GetSuspendedActivity(){
return this.SuspendActivity;
}
function Sequencer_GetSuspendedActivity(_e0){
var _e1=this.SuspendedActivity;
if(_e0!==null&&_e0!==undefined){
this.LogSeq("`1552`"+_e1,_e0);
}
return _e1;
}
function Sequencer_Start(){
if(this.SuspendedActivity===null){
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_START,null,"");
}else{
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_RESUME_ALL,null,"");
}
this.OverallSequencingProcess();
}
function Sequencer_InitialRandomizationAndSelection(){
var _e2=this.LogSeqAudit("`1306`");
if(this.SuspendedActivity===null){
for(var _e3 in this.Activities.ActivityList){
this.SelectChildrenProcess(this.Activities.ActivityList[_e3],_e2);
this.RandomizeChildrenProcess(this.Activities.ActivityList[_e3],false,_e2);
}
}
}
function Sequencer_GetCurrentActivity(){
return this.CurrentActivity;
}
function Sequencer_GetExceptionText(){
if(this.ExceptionText!==null&&this.ExceptionText!==undefined){
return this.ExceptionText;
}else{
return "";
}
}
function Sequencer_GetExitAction(_e4,_e5){
return EXIT_ACTION_DISPLAY_MESSAGE;
}
Sequencer.prototype.NavigationRequestProcess=Sequencer_NavigationRequestProcess;
Sequencer.prototype.SequencingExitActionRulesSubprocess=Sequencer_SequencingExitActionRulesSubprocess;
Sequencer.prototype.SequencingPostConditionRulesSubprocess=Sequencer_SequencingPostConditionRulesSubprocess;
Sequencer.prototype.TerminationRequestProcess=Sequencer_TerminationRequestProcess;
Sequencer.prototype.MeasureRollupProcess=Sequencer_MeasureRollupProcess;
Sequencer.prototype.ObjectiveRollupProcess=Sequencer_ObjectiveRollupProcess;
Sequencer.prototype.ObjectiveRollupUsingDefaultProcess=Sequencer_ObjectiveRollupUsingDefaultProcess;
Sequencer.prototype.ObjectiveRollupUsingMeasureProcess=Sequencer_ObjectiveRollupUsingMeasureProcess;
Sequencer.prototype.ObjectiveRollupUsingRulesProcess=Sequencer_ObjectiveRollupUsingRulesProcess;
Sequencer.prototype.ActivityProgressRollupProcess=Sequencer_ActivityProgressRollupProcess;
Sequencer.prototype.ActivityProgressRollupProcessUsingDefault=Sequencer_ActivityProgressRollupProcessUsingDefault;
Sequencer.prototype.RollupRuleCheckSubprocess=Sequencer_RollupRuleCheckSubprocess;
Sequencer.prototype.EvaluateRollupConditionsSubprocess=Sequencer_EvaluateRollupConditionsSubprocess;
Sequencer.prototype.CheckChildForRollupSubprocess=Sequencer_CheckChildForRollupSubprocess;
Sequencer.prototype.OverallRollupProcess=Sequencer_OverallRollupProcess;
Sequencer.prototype.SelectChildrenProcess=Sequencer_SelectChildrenProcess;
Sequencer.prototype.RandomizeChildrenProcess=Sequencer_RandomizeChildrenProcess;
Sequencer.prototype.FlowTreeTraversalSubprocess=Sequencer_FlowTreeTraversalSubprocess;
Sequencer.prototype.FlowActivityTraversalSubprocess=Sequencer_FlowActivityTraversalSubprocess;
Sequencer.prototype.FlowSubprocess=Sequencer_FlowSubprocess;
Sequencer.prototype.ChoiceActivityTraversalSubprocess=Sequencer_ChoiceActivityTraversalSubprocess;
Sequencer.prototype.StartSequencingRequestProcess=Sequencer_StartSequencingRequestProcess;
Sequencer.prototype.ResumeAllSequencingRequestProcess=Sequencer_ResumeAllSequencingRequestProcess;
Sequencer.prototype.ContinueSequencingRequestProcess=Sequencer_ContinueSequencingRequestProcess;
Sequencer.prototype.PreviousSequencingRequestProcess=Sequencer_PreviousSequencingRequestProcess;
Sequencer.prototype.ChoiceSequencingRequestProcess=Sequencer_ChoiceSequencingRequestProcess;
Sequencer.prototype.ChoiceFlowSubprocess=Sequencer_ChoiceFlowSubprocess;
Sequencer.prototype.ChoiceFlowTreeTraversalSubprocess=Sequencer_ChoiceFlowTreeTraversalSubprocess;
Sequencer.prototype.RetrySequencingRequestProcess=Sequencer_RetrySequencingRequestProcess;
Sequencer.prototype.ExitSequencingRequestProcess=Sequencer_ExitSequencingRequestProcess;
Sequencer.prototype.SequencingRequestProcess=Sequencer_SequencingRequestProcess;
Sequencer.prototype.DeliveryRequestProcess=Sequencer_DeliveryRequestProcess;
Sequencer.prototype.ContentDeliveryEnvironmentProcess=Sequencer_ContentDeliveryEnvironmentProcess;
Sequencer.prototype.ClearSuspendedActivitySubprocess=Sequencer_ClearSuspendedActivitySubprocess;
Sequencer.prototype.LimitConditionsCheckProcess=Sequencer_LimitConditionsCheckProcess;
Sequencer.prototype.SequencingRulesCheckProcess=Sequencer_SequencingRulesCheckProcess;
Sequencer.prototype.SequencingRulesCheckSubprocess=Sequencer_SequencingRulesCheckSubprocess;
Sequencer.prototype.TerminateDescendentAttemptsProcess=Sequencer_TerminateDescendentAttemptsProcess;
Sequencer.prototype.EndAttemptProcess=Sequencer_EndAttemptProcess;
Sequencer.prototype.CheckActivityProcess=Sequencer_CheckActivityProcess;
Sequencer.prototype.EvaluateSequencingRuleCondition=Sequencer_EvaluateSequencingRuleCondition;
Sequencer.prototype.ResetException=Sequencer_ResetException;
Sequencer.prototype.LogSeq=Sequencer_LogSeq;
Sequencer.prototype.LogSeqAudit=Sequencer_LogSeqAudit;
Sequencer.prototype.LogSeqReturn=Sequencer_LogSeqReturn;
Sequencer.prototype.WriteHistoryLog=Sequencer_WriteHistoryLog;
Sequencer.prototype.WriteHistoryReturnValue=Sequencer_WriteHistoryReturnValue;
Sequencer.prototype.SetCurrentActivity=Sequencer_SetCurrentActivity;
Sequencer.prototype.IsCurrentActivityDefined=Sequencer_IsCurrentActivityDefined;
Sequencer.prototype.IsSuspendedActivityDefined=Sequencer_IsSuspendedActivityDefined;
Sequencer.prototype.ClearSuspendedActivity=Sequencer_ClearSuspendedActivity;
Sequencer.prototype.GetRootActivity=Sequencer_GetRootActivity;
Sequencer.prototype.DoesActivityExist=Sequencer_DoesActivityExist;
Sequencer.prototype.GetActivityFromIdentifier=Sequencer_GetActivityFromIdentifier;
Sequencer.prototype.AreActivitiesSiblings=Sequencer_AreActivitiesSiblings;
Sequencer.prototype.FindCommonAncestor=Sequencer_FindCommonAncestor;
Sequencer.prototype.GetActivityPath=Sequencer_GetActivityPath;
Sequencer.prototype.GetPathToAncestorExclusive=Sequencer_GetPathToAncestorExclusive;
Sequencer.prototype.GetPathToAncestorInclusive=Sequencer_GetPathToAncestorInclusive;
Sequencer.prototype.ActivityHasSuspendedChildren=Sequencer_ActivityHasSuspendedChildren;
Sequencer.prototype.CourseIsSingleSco=Sequencer_CourseIsSingleSco;
Sequencer.prototype.TranslateSequencingRuleActionIntoSequencingRequest=Sequencer_TranslateSequencingRuleActionIntoSequencingRequest;
Sequencer.prototype.TranslateSequencingRuleActionIntoTerminationRequest=Sequencer_TranslateSequencingRuleActionIntoTerminationRequest;
Sequencer.prototype.IsActivity1BeforeActivity2=Sequencer_IsActivity1BeforeActivity2;
Sequencer.prototype.GetOrderedListOfActivities=Sequencer_GetOrderedListOfActivities;
Sequencer.prototype.PreOrderTraversal=Sequencer_PreOrderTraversal;
Sequencer.prototype.IsActivityLastOverall=Sequencer_IsActivityLastOverall;
Sequencer.prototype.GetGlobalObjectiveByIdentifier=Sequencer_GetGlobalObjectiveByIdentifier;
Sequencer.prototype.AddGlobalObjective=Sequencer_AddGlobalObjective;
Sequencer.prototype.ResetGlobalObjectives=Sequencer_ResetGlobalObjectives;
Sequencer.prototype.FindActivitiesAffectedByWriteMaps=Sequencer_FindActivitiesAffectedByWriteMaps;
Sequencer.prototype.FindDistinctParentsOfActivitySet=Sequencer_FindDistinctParentsOfActivitySet;
Sequencer.prototype.FindDistinctAncestorsOfActivitySet=Sequencer_FindDistinctAncestorsOfActivitySet;
Sequencer.prototype.GetMinimalSubsetOfActivitiesToRollup=Sequencer_GetMinimalSubsetOfActivitiesToRollup;
Sequencer.prototype.CheckForRelevantSequencingRules=Sequencer_CheckForRelevantSequencingRules;
Sequencer.prototype.DoesThisActivityHaveSequencingRulesRelevantToChoice=Sequencer_DoesThisActivityHaveSequencingRulesRelevantToChoice;
function Sequencer_ResetException(){
this.Exception=null;
this.ExceptionText=null;
}
function Sequencer_LogSeq(str,_e7){
if(_e7===null||_e7===undefined){
Debug.AssertError("`1537`");
}
str=str+"";
if(this.LookAhead===true){
return Debug.WriteLookAheadDetailed(str,_e7);
}else{
return Debug.WriteSequencingDetailed(str,_e7);
}
}
function Sequencer_LogSeqAudit(str,_e9){
str=str+"";
if(this.LookAhead===true){
return Debug.WriteLookAheadAudit(str,_e9);
return "";
}else{
return Debug.WriteSequencingAudit(str,_e9);
}
}
function Sequencer_LogSeqReturn(str,_eb){
if(_eb===null||_eb===undefined){
Debug.AssertError("`1537`");
}
str=str+"";
if(this.LookAhead===true){
return _eb.setReturn(str);
}else{
return _eb.setReturn(str);
}
}
function Sequencer_WriteHistoryLog(str,_ed){
HistoryLog.WriteEventDetailed(str,_ed);
}
function Sequencer_WriteHistoryReturnValue(str,_ef){
HistoryLog.WriteEventDetailedReturnValue(str,_ef);
}
function Sequencer_SetCurrentActivity(_f0,_f1){
Debug.AssertError("Parent log not passed.",(_f1===undefined||_f1===null));
this.LogSeq("`1421`"+_f0,_f1);
this.CurrentActivity=_f0;
}
function Sequencer_IsCurrentActivityDefined(_f2){
Debug.AssertError("Parent log not passed.",(_f2===undefined||_f2===null));
var _f3=this.GetCurrentActivity();
var _f4=(_f3!==null);
if(_f4){
this.LogSeq("`1448`",_f2);
}else{
this.LogSeq("`1379`",_f2);
}
return _f4;
}
function Sequencer_IsSuspendedActivityDefined(_f5){
Debug.AssertError("Parent log not passed.",(_f5===undefined||_f5===null));
var _f6=this.GetSuspendedActivity(_f5);
var _f7=(_f6!==null);
if(_f7){
this.LogSeq("`1406`",_f5);
}else{
this.LogSeq("`1341`",_f5);
}
return _f7;
}
function Sequencer_ClearSuspendedActivity(_f8){
Debug.AssertError("Parent log not passed.",(_f8===undefined||_f8===null));
this.LogSeq("`1459`",_f8);
this.SuspendedActivity=null;
}
function Sequencer_GetRootActivity(_f9){
Debug.AssertError("Parent log not passed.",(_f9===undefined||_f9===null));
var _fa=this.Activities.GetRootActivity();
this.LogSeq("`1638`"+_fa,_f9);
return _fa;
}
function Sequencer_DoesActivityExist(_fb,_fc){
Debug.AssertError("Parent log not passed.",(_fc===undefined||_fc===null));
var _fd=this.Activities.DoesActivityExist(_fb,_fc);
if(_fd){
this.LogSeq("`1670`"+_fb+"`1397`",_fc);
}else{
this.LogSeq("`1670`"+_fb+"`1256`",_fc);
}
return _fd;
}
function Sequencer_GetActivityFromIdentifier(_fe,_ff){
Debug.AssertError("Parent log not passed.",(_ff===undefined||_ff===null));
var _100=this.Activities.GetActivityFromIdentifier(_fe,_ff);
if(_100!==null){
this.LogSeq("`1670`"+_fe+"`1461`"+_100,_ff);
}else{
this.LogSeq("`1670`"+_fe+"`1225`",_ff);
}
return _100;
}
function Sequencer_AreActivitiesSiblings(_101,_102,_103){
Debug.AssertError("Parent log not passed.",(_103===undefined||_103===null));
if(_101===null||_101===undefined||_102===null||_102===undefined){
return false;
}
var _104=_101.ParentActivity;
var _105=_102.ParentActivity;
var _106=(_104==_105);
if(_106){
this.LogSeq("`1680`"+_101+"`1727`"+_102+"`1697`",_103);
}else{
this.LogSeq("`1680`"+_101+"`1727`"+_102+"`1627`",_103);
}
return _106;
}
function Sequencer_FindCommonAncestor(_107,_108,_109){
Debug.AssertError("Parent log not passed.",(_109===undefined||_109===null));
var _10a=new Array();
var _10b=new Array();
if(_107!==null&&_107.IsTheRoot()){
this.LogSeq(_107+"`939`"+_107+"`1727`"+_108+"`1730`"+_107,_109);
return _107;
}
if(_108!==null&&_108.IsTheRoot()){
this.LogSeq(_108+"`939`"+_107+"`1727`"+_108+"`1730`"+_108,_109);
return _108;
}
if(_107!==null){
_10a=this.Activities.GetActivityPath(_107,false);
}
if(_108!==null){
_10b=this.Activities.GetActivityPath(_108,false);
}
for(var i=0;i<_10a.length;i++){
for(var j=0;j<_10b.length;j++){
if(_10a[i]==_10b[j]){
this.LogSeq("`1326`"+_107+"`1727`"+_108+"`1730`"+_10a[i],_109);
return _10a[i];
}
}
}
this.LogSeq("`1713`"+_107+"`1727`"+_108+"`1388`",_109);
return null;
}
function Sequencer_GetActivityPath(_10e,_10f){
return this.Activities.GetActivityPath(_10e,_10f);
}
function Sequencer_GetPathToAncestorExclusive(_110,_111,_112){
var _113=new Array();
var _114=0;
if(_110!==null&&_111!==null&&_110!==_111){
if(_112===true){
_113[_114]=_110;
_114++;
}
while(_110.ParentActivity!==null&&_110.ParentActivity!==_111){
_110=_110.ParentActivity;
_113[_114]=_110;
_114++;
}
}
return _113;
}
function Sequencer_GetPathToAncestorInclusive(_115,_116,_117){
var _118=new Array();
var _119=0;
if(_117==null||_117==undefined){
_117===true;
}
_118[_119]=_115;
_119++;
while(_115.ParentActivity!==null&&_115!=_116){
_115=_115.ParentActivity;
_118[_119]=_115;
_119++;
}
if(_117===false){
_118.splice(0,1);
}
return _118;
}
function Sequencer_ActivityHasSuspendedChildren(_11a,_11b){
Debug.AssertError("Parent log not passed.",(_11b===undefined||_11b===null));
var _11c=_11a.GetChildren();
var _11d=false;
for(var i=0;i<_11c.length;i++){
if(_11c[i].IsSuspended()){
_11d=true;
}
}
if(_11d){
this.LogSeq("`1698`"+_11a+"`1512`",_11b);
}else{
this.LogSeq("`1698`"+_11a+"`1338`",_11b);
}
return _11d;
}
function Sequencer_CourseIsSingleSco(){
if(this.Activities.ActivityList.length<=2){
return true;
}else{
return false;
}
}
function Sequencer_TranslateSequencingRuleActionIntoSequencingRequest(_11f){
switch(_11f){
case SEQUENCING_RULE_ACTION_RETRY:
return SEQUENCING_REQUEST_RETRY;
case SEQUENCING_RULE_ACTION_CONTINUE:
return SEQUENCING_REQUEST_CONTINUE;
case SEQUENCING_RULE_ACTION_PREVIOUS:
return SEQUENCING_REQUEST_PREVIOUS;
default:
Debug.AssertError("ERROR in TranslateSequencingRuleActionIntoSequencingRequest - should never have an untranslatable sequencing request. ruleAction="+_11f);
return null;
}
}
function Sequencer_TranslateSequencingRuleActionIntoTerminationRequest(_120){
switch(_120){
case SEQUENCING_RULE_ACTION_EXIT_PARENT:
return TERMINATION_REQUEST_EXIT_PARENT;
case SEQUENCING_RULE_ACTION_EXIT_ALL:
return TERMINATION_REQUEST_EXIT_ALL;
default:
Debug.AssertError("ERROR in TranslateSequencingRuleActionIntoTerminationRequest - should never have an untranslatable sequencing request. ruleAction="+_120);
return null;
}
}
function Sequencer_IsActivity1BeforeActivity2(_121,_122,_123){
Debug.AssertError("Parent log not passed.",(_123===undefined||_123===null));
var _124=this.GetOrderedListOfActivities(_123);
for(var i=0;i<_124.length;i++){
if(_124[i]==_121){
this.LogSeq("`1698`"+_121+"`1510`"+_122,_123);
return true;
}
if(_124[i]==_122){
this.LogSeq("`1698`"+_121+"`1539`"+_122,_123);
return false;
}
}
Debug.AssertError("ERROR IN Sequencer_IsActivity1BeforeActivity2");
return null;
}
function Sequencer_GetOrderedListOfActivities(_126){
Debug.AssertError("Parent log not passed.",(_126===undefined||_126===null));
var list;
var root=this.GetRootActivity(_126);
list=this.PreOrderTraversal(root);
return list;
}
function Sequencer_PreOrderTraversal(_129){
var list=new Array();
list[0]=_129;
var _12b=_129.GetAvailableChildren();
var _12c;
for(var i=0;i<_12b.length;i++){
_12c=this.PreOrderTraversal(_12b[i]);
list=list.concat(_12c);
}
return list;
}
function Sequencer_IsActivityLastOverall(_12e,_12f){
Debug.AssertError("Parent log not passed.",(_12f===undefined||_12f===null));
var _130=this.GetOrderedListOfActivities(_12f);
var _131=null;
for(var i=(_130.length-1);i>=0;i--){
if(_130[i].IsAvailable()){
_131=_130[i];
i=-1;
}
}
if(_12e==_131){
this.LogSeq("`1698`"+_12e+"`1405`",_12f);
return true;
}
this.LogSeq("`1698`"+_12e+"`1344`",_12f);
return false;
}
function Sequencer_GetGlobalObjectiveByIdentifier(_133){
for(var obj in this.GlobalObjectives){
if(this.GlobalObjectives[obj].ID==_133){
return this.GlobalObjectives[obj];
}
}
return null;
}
function Sequencer_AddGlobalObjective(ID,_136,_137,_138,_139){
var _13a=this.GlobalObjectives.length;
var obj=new GlobalObjective(_13a,ID,_136,_137,_138,_139);
this.GlobalObjectives[_13a]=obj;
this.GlobalObjectives[_13a].SetDirtyData();
}
function Sequencer_ResetGlobalObjectives(){
var _13c;
for(var obj in this.GlobalObjectives){
_13c=this.GlobalObjectives[obj];
_13c.ResetState();
}
}
function Sequencer_FindActivitiesAffectedByWriteMaps(_13e){
var _13f=new Array();
var _140;
var _141=_13e.GetObjectives();
var _142=new Array();
var i;
var j;
for(i=0;i<_141.length;i++){
_140=_141[i].GetMaps();
for(j=0;j<_140.length;j++){
if(_140[j].WriteSatisfiedStatus===true||_140[j].WriteNormalizedMeasure===true){
_13f[_13f.length]=_140[j].TargetObjectiveId;
}
}
}
if(_13f.length===0){
return _142;
}
var _145;
var _146;
var _147;
var _148;
for(var _149 in this.Activities.ActivityList){
_145=this.Activities.ActivityList[_149];
if(_145!=_13e){
_146=_145.GetObjectives();
for(var _14a=0;_14a<_146.length;_14a++){
_147=_146[_14a].GetMaps();
for(var map=0;map<_147.length;map++){
if(_147[map].ReadSatisfiedStatus===true||_147[map].ReadNormalizedMeasure===true){
_148=_147[map].TargetObjectiveId;
for(var _14c=0;_14c<_13f.length;_14c++){
if(_13f[_14c]==_148){
_142[_142.length]=_145;
}
}
}
}
}
}
}
return _142;
}
function Sequencer_FindDistinctAncestorsOfActivitySet(_14d){
var _14e=new Array();
for(var _14f=0;_14f<_14d.length;_14f++){
var _150=_14d[_14f];
if(_150!==null){
var _151=this.GetActivityPath(_150,true);
for(var i=0;i<_151.length;i++){
var _153=true;
for(var j=0;j<_14e.length;j++){
if(_151[i]==_14e[j]){
_153=false;
break;
}
}
if(_153){
_14e[_14e.length]=_151[i];
}
}
}
}
return _14e;
}
function Sequencer_FindDistinctParentsOfActivitySet(_155){
var _156=new Array();
var _157;
var _158;
for(var _159 in _155){
_158=true;
_157=this.Activities.GetParentActivity(_155[_159]);
if(_157!==null){
for(var i=0;i<_156.length;i++){
if(_156[i]==_157){
_158=false;
break;
}
}
if(_158){
_156[_156.length]=_157;
}
}
}
return _156;
}
function Sequencer_GetMinimalSubsetOfActivitiesToRollup(_15b,_15c){
var _15d=new Array();
var _15e=new Array();
var _15f;
var _160;
var _161;
if(_15c!==null){
_15d=_15d.concat(_15c);
}
for(var _162 in _15b){
_160=_15b[_162];
_161=false;
for(var _163 in _15d){
if(_15d[_163]==_160){
_161=true;
break;
}
}
if(_161===false){
_15e[_15e.length]=_160;
_15f=this.GetActivityPath(_160,true);
_15d=_15d.concat(_15f);
}
}
return _15e;
}
function Sequencer_EvaluatePossibleNavigationRequests(_164){
var _165=this.LogSeqAudit("`989`");
var _166;
var _167=null;
var _168;
var _169;
var id;
for(id in _164){
if(_164[id].WillAlwaysSucceed===true){
_164[id].WillSucceed=true;
}else{
if(_164[id].WillNeverSucceed===true){
_164[id].WillSucceed=false;
_164[id].Exception="NB.2.1-10";
_164[id].ExceptionText=IntegrationImplementation.GetString("Your selection is not permitted. Please select 'Next' or 'Previous' to move through '{0}.'");
}else{
_164[id].WillSucceed=null;
}
}
_164[id].Hidden=false;
_164[id].Disabled=false;
}
this.LogSeq("`795`",_165);
for(id in _164){
if(_164[id].WillSucceed===null){
_166=this.NavigationRequestProcess(_164[id].NavigationRequest,_164[id].TargetActivityItemIdentifier,_165);
if(_166.NavigationRequest==NAVIGATION_REQUEST_NOT_VALID){
this.LogSeq("`760`",_165);
_164[id].WillSucceed=false;
_164[id].TargetActivity=this.GetActivityFromIdentifier(_164[id].TargetActivityItemIdentifier,_165);
_164[id].Exception=_166.Exception;
_164[id].ExceptionText=_166.ExceptionText;
}else{
this.LogSeq("`743`",_165);
_164[id].WillSucceed=true;
_164[id].TargetActivity=_166.TargetActivity;
_164[id].SequencingRequest=_166.SequencingRequest;
_164[id].Exception="";
_164[id].ExceptionText="";
}
}
}
this.LogSeq("`215`",_165);
var _16b=this.GetCurrentActivity();
if(_16b!==null&&_16b.IsActive()===true){
this.LogSeq("`953`",_165);
_167=this.TerminationRequestProcess(TERMINATION_REQUEST_EXIT,_165);
this.LogSeq("`906`",_165);
if(_167.TerminationRequest==TERMINATION_REQUEST_NOT_VALID){
this.LogSeq("`720`",_165);
if(_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed){
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed=false;
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].Exception=_167.Exception;
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].ExceptionText=_167.ExceptionText;
}
if(_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed){
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed=false;
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].Exception=_167.Exception;
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].ExceptionText=_167.ExceptionText;
}
if(_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed){
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed=false;
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].Exception=_167.Exception;
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].ExceptionText=_167.ExceptionText;
}
for(id=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;id<_164.length;id++){
if(_164[id].WillSucceed){
_164[id].WillSucceed=false;
_164[id].Exception=_167.Exception;
_164[id].ExceptionText=terminatonRequestResult.ExceptionText;
}else{
_164[id].TerminationSequencingRequest=_167.SequencingRequest;
}
}
}
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].TerminationSequencingRequest=_167.SequencingRequest;
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].TerminationSequencingRequest=_167.SequencingRequest;
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].TerminationSequencingRequest=_167.SequencingRequest;
for(id=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;id<_164.length;id++){
_164[id].TerminationSequencingRequest=_167.SequencingRequest;
}
}
this.LogSeq("`660`",_165);
for(id in _164){
if(id>=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE&&_164[id].WillSucceed===true&&_164[id].WillAlwaysSucceed===false&&_164[id].WillNeverSucceed===false){
this.LogSeq("`869`",_165);
var _16c=this.CheckActivityProcess(_164[id].TargetActivity,_165);
if(_16c===true){
this.LogSeq("`751`",_165);
_164[id].WillSucceed=false;
_164[id].Disabled=true;
this.SetAllDescendentsToDisabled(_164,_164[id].TargetActivity);
}
}
}
this.LogSeq("`678`",_165);
var _16d=null;
for(id in _164){
if(_164[id].WillSucceed===true&&_164[id].WillAlwaysSucceed===false&&_164[id].WillNeverSucceed===false){
this.LogSeq("`625`",_165);
if(_164[id].TerminationSequencingRequest!==null){
this.LogSeq("`420`",_165);
if(_16d===null){
_168=this.SequencingRequestProcess(_164[id].TerminationSequencingRequest,null,_165);
}else{
_168=_16d;
}
if(id>=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE){
this.LogSeq("`1036`",_165);
var _16e=this.SequencingRulesCheckProcess(_164[id].TargetActivity,RULE_SET_HIDE_FROM_CHOICE,_165);
if(_16e!==null){
_164[id].Exception="SB.2.9-3";
_164[id].ExceptionText="The activity "+_164[id].TargetActivity.GetTitle()+" should be hidden and is not a valid selection";
_164[id].Hidden=true;
}
}
}else{
this.LogSeq("`1635`",_165);
this.LogSeq("`721`",_165);
_168=this.SequencingRequestProcess(_164[id].SequencingRequest,_164[id].TargetActivity,_165);
}
this.LogSeq("`841`",_165);
if(_168.Exception!==null){
this.LogSeq("`1277`",_165);
_164[id].WillSucceed=false;
_164[id].Exception=_168.Exception;
_164[id].ExceptionText=_168.ExceptionText;
_164[id].Hidden=_168.Hidden;
}else{
if(_168.DeliveryRequest!==null){
this.LogSeq("`1117`",_165);
_169=this.DeliveryRequestProcess(_168.DeliveryRequest,_165);
this.LogSeq("`714`"+_169.Valid+")",_165);
_164[id].WillSucceed=_169.Valid;
}
}
}
}
this.LogSeq("`360`",_165);
var _16f;
for(id in _164){
if(id>=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE){
if(_164[id].WillSucceed===false){
_16f=_164[id].Exception;
if(_16f==CONTROL_CHOICE_EXIT_ERROR_NAV||_16f==CONTROL_CHOICE_EXIT_ERROR_CHOICE||_16f==PREVENT_ACTIVATION_ERROR||_16f==CONSTRAINED_CHOICE_ERROR){
this.LogSeq("`1462`"+id+"`1703`"+_16f,_165);
_164[id].Hidden=true;
}
}
}
}
var _170=this.Activities.GetParentActivity(_16b);
if(_170!=null){
if(_170.GetSequencingControlFlow()===true){
this.LogSeq("`430`",_165);
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed=true;
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].Exception="";
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].ExceptionText="";
}else{
this.LogSeq("`409`",_165);
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed=false;
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].Exception="SB.2.2-1";
_164[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].ExceptionText="Parent activity does not allow flow traversal";
}
}
this.LogSeqReturn("",_165);
return _164;
}
function Sequencer_SetAllDescendentsToDisabled(_171,_172){
for(var i=0;i<_172.ChildActivities.length;i++){
var _174=Control.FindPossibleChoiceRequestForActivity(_172.ChildActivities[i]);
_174.WillSucceed=false;
_174.Disabled=true;
this.SetAllDescendentsToDisabled(_171,_172.ChildActivities[i]);
}
}
function Sequencer_SetAllDescendentsToDisabled(_175,_176){
for(var i=0;i<_176.ChildActivities.length;i++){
var _178=Control.FindPossibleChoiceRequestForActivity(_176.ChildActivities[i]);
_178.WillSucceed=false;
_178.Disabled=true;
this.SetAllDescendentsToDisabled(_175,_176.ChildActivities[i]);
}
}
function Sequencer_InitializePossibleNavigationRequestAbsolutes(_179,_17a,_17b){
var _17c=this.LogSeqAudit("`1017`");
this.CheckForRelevantSequencingRules(_17a,false);
var _17d;
var _17e;
var _17f=false;
for(var _180 in _17b){
_17d=_17b[_180];
var _181=this.LogSeqAudit(_17d.StringIdentifier,_17c);
if(_17d.GetSequencingControlChoice()===false){
this.LogSeqAudit("`1314`",_181);
var _182=this.LogSeqAudit("`840`"+_17d.ChildActivities.length+")",_181);
for(var i=0;i<_17d.ChildActivities.length;i++){
_17e=Control.FindPossibleChoiceRequestForActivity(_17d.ChildActivities[i]);
_17e.WillNeverSucceed=true;
this.LogSeqAudit(_17d.ChildActivities[i].StringIdentifier,_182);
}
}
_17f=_17f||(_17d.GetSequencingControlChoiceExit()===true);
this.LogSeqAudit("`1473`"+_17f,_181);
_17e=Control.FindPossibleChoiceRequestForActivity(_17d);
if(_17d.IsDeliverable()===false&&_17d.GetSequencingControlFlow()===false){
this.LogSeqAudit("`354`",_181);
_17e.WillNeverSucceed=true;
}
if(_17d.HasChildActivitiesDeliverableViaFlow===false){
this.LogSeqAudit("`503`",_181);
_17e.WillNeverSucceed=true;
}
if(_17d.HasSeqRulesRelevantToChoice===false&&_17e.WillNeverSucceed===false){
this.LogSeqAudit("`272`",_181);
_17e.WillAlwaysSucceed=true;
}else{
this.LogSeqAudit("`246`",_181);
_17e.WillAlwaysSucceed=false;
}
this.LogSeqAudit("`1582`"+_17e.WillAlwaysSucceed,_181);
this.LogSeqAudit("`1584`"+_17e.WillNeverSucceed,_181);
}
if(_17f===true){
this.LogSeqAudit("`544`",_17c);
for(var id in _179){
_179[id].WillAlwaysSucceed=false;
}
}
}
function Sequencer_CheckForRelevantSequencingRules(_185,_186){
if(_186===true){
_185.HasSeqRulesRelevantToChoice=true;
}else{
if(this.DoesThisActivityHaveSequencingRulesRelevantToChoice(_185)){
_185.HasSeqRulesRelevantToChoice=true;
}else{
_185.HasSeqRulesRelevantToChoice=false;
}
}
var _187=false;
for(var i=0;i<_185.ChildActivities.length;i++){
this.CheckForRelevantSequencingRules(_185.ChildActivities[i],_185.HasSeqRulesRelevantToChoice);
_187=(_187||_185.ChildActivities[i].HasChildActivitiesDeliverableViaFlow);
}
_185.HasChildActivitiesDeliverableViaFlow=(_185.IsDeliverable()||(_185.GetSequencingControlFlow()&&_187));
}
function Sequencer_DoesThisActivityHaveSequencingRulesRelevantToChoice(_189){
if(_189.GetSequencingControlForwardOnly()===true){
return true;
}
if(_189.GetPreventActivation()===true){
return true;
}
if(_189.GetConstrainedChoice()===true){
return true;
}
if(_189.GetSequencingControlChoiceExit()===false){
return true;
}
var _18a=_189.GetPreConditionRules();
for(var i=0;i<_18a.length;i++){
if(_18a[i].Action==SEQUENCING_RULE_ACTION_DISABLED||_18a[i].Action==SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE||_18a[i].Action==SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL){
return true;
}
}
return false;
}
function Sequencer_ActivityProgressRollupProcess(_18c,_18d){
Debug.AssertError("Calling log not passed.",(_18d===undefined||_18d===null));
var _18e=this.LogSeqAudit("`1191`"+_18c+")",_18d);
var _18f;
this.LogSeq("`335`",_18e);
_18f=this.RollupRuleCheckSubprocess(_18c,RULE_SET_INCOMPLETE,_18e);
this.LogSeq("`848`",_18e);
if(_18f===true){
this.LogSeq("`797`",_18e);
_18c.SetAttemptProgressStatus(true);
this.LogSeq("`767`",_18e);
_18c.SetAttemptCompletionStatus(false);
}
this.LogSeq("`351`",_18e);
_18f=this.RollupRuleCheckSubprocess(_18c,RULE_SET_COMPLETED,_18e);
this.LogSeq("`851`",_18e);
if(_18f===true){
this.LogSeq("`809`",_18e);
_18c.SetAttemptProgressStatus(true);
this.LogSeq("`779`",_18e);
_18c.SetAttemptCompletionStatus(true);
}
if(Sequencer_GetApplicableSetofRollupRules(_18c,RULE_SET_INCOMPLETE).length===0&&Sequencer_GetApplicableSetofRollupRules(_18c,RULE_SET_COMPLETED).length===0){
this.LogSeq("`1167`",_18e);
this.ActivityProgressRollupProcessUsingDefault(_18c,_18e);
}
this.LogSeq("`1062`",_18e);
this.LogSeqReturn("",_18e);
return;
}
function Sequencer_ActivityProgressRollupProcessUsingDefault(_190,_191){
var _192=this.LogSeqAudit("`923`"+_190+")",_191);
var _193;
var _194;
var _195;
this.LogSeq("`1263`",_192);
if(_190.IsALeaf()){
this.LogSeq("`1285`",_192);
this.LogSeqReturn("",_192);
return;
}
this.LogSeq("`1084`",_192);
var _196=_190.GetChildren();
this.LogSeq("`1103`",_192);
var _197=true;
this.LogSeq("`1211`",_192);
var _198=true;
this.LogSeq("`1273`",_192);
var _199=true;
for(var i=0;i<_196.length;i++){
this.LogSeq("`1353`"+_196[i]+"`1704`",_192);
if(_196[i].IsTracked()){
this.LogSeq("`1030`",_192);
_193=_196[i].IsCompleted();
this.LogSeq("`1049`",_192);
_194=_196[i].IsAttempted();
this.LogSeq("`771`",_192);
_195=(_193===false||_194===true);
this.LogSeq("`883`",_192);
if(this.CheckChildForRollupSubprocess(_196[i],ROLLUP_RULE_ACTION_COMPLETED,_192)){
this.LogSeq("`881`",_192);
_198=(_198&&(_193===true));
_199=false;
}
this.LogSeq("`870`",_192);
if(this.CheckChildForRollupSubprocess(_196[i],ROLLUP_RULE_ACTION_INCOMPLETE,_192)){
this.LogSeq("`847`",_192);
_197=(_197&&_195);
_199=false;
}
}
}
if(_199&&Control.Package.Properties.RollupEmptySetToUnknown){
this.LogSeq("`537`"+Control.Package.Properties.RollupEmptySetToUnknown+")",_192);
}else{
this.LogSeq("`1387`",_192);
if(_197===true){
this.LogSeq("`775`",_192);
_190.SetAttemptProgressStatus(true);
this.LogSeq("`746`",_192);
_190.SetAttemptCompletionStatus(false);
}
this.LogSeq("`1416`",_192);
if(_198===true){
this.LogSeq("`780`",_192);
_190.SetAttemptProgressStatus(true);
this.LogSeq("`759`",_192);
_190.SetAttemptCompletionStatus(true);
}
}
this.LogSeqReturn("",_192);
}
function Sequencer_CheckActivityProcess(_19b,_19c){
Debug.AssertError("Calling log not passed.",(_19c===undefined||_19c===null));
var _19d=this.LogSeqAudit("`1395`"+_19b+")",_19c);
this.LogSeq("`308`",_19d);
var _19e=this.SequencingRulesCheckProcess(_19b,RULE_SET_DISABLED,_19d);
this.LogSeq("`781`",_19d);
if(_19e!==null){
this.LogSeq("`717`",_19d);
this.LogSeqReturn("`1732`",_19d);
return true;
}
this.LogSeq("`393`",_19d);
var _19f=this.LimitConditionsCheckProcess(_19b,_19d);
this.LogSeq("`860`",_19d);
if(_19f){
this.LogSeq("`609`",_19d);
this.LogSeqReturn("`1732`",_19d);
return true;
}
this.LogSeq("`747`",_19d);
this.LogSeqReturn("`1728`",_19d);
return false;
}
function Sequencer_CheckChildForRollupSubprocess(_1a0,_1a1,_1a2){
Debug.AssertError("Calling log not passed.",(_1a2===undefined||_1a2===null));
var _1a3=this.LogSeqAudit("`1119`"+_1a0+", "+_1a1+")",_1a2);
var _1a4;
this.LogSeq("`1317`",_1a3);
var _1a5=false;
this.LogSeq("`815`",_1a3);
if(_1a1==ROLLUP_RULE_ACTION_SATISFIED||_1a1==ROLLUP_RULE_ACTION_NOT_SATISFIED){
this.LogSeq("`412`",_1a3);
if(_1a0.GetRollupObjectiveSatisfied()===true){
this.LogSeq("`594`",_1a3);
_1a5=true;
var _1a6=_1a0.GetRequiredForSatisfied();
var _1a7=_1a0.GetRequiredForNotSatisfied();
this.LogSeq("`98`",_1a3);
if((_1a1==ROLLUP_RULE_ACTION_SATISFIED&&_1a6==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)||(_1a1==ROLLUP_RULE_ACTION_NOT_SATISFIED&&_1a7==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)){
this.LogSeq("`278`",_1a3);
if(_1a0.GetAttemptCount()>0&&_1a0.IsSuspended()===true){
this.LogSeq("`1201`",_1a3);
_1a5=false;
}
}else{
this.LogSeq("`1558`",_1a3);
this.LogSeq("`104`",_1a3);
if((_1a1==ROLLUP_RULE_ACTION_SATISFIED&&_1a6==ROLLUP_CONSIDERATION_IF_ATTEMPTED)||(_1a1==ROLLUP_RULE_ACTION_NOT_SATISFIED&&_1a7==ROLLUP_CONSIDERATION_IF_ATTEMPTED)){
this.LogSeq("`684`",_1a3);
if(_1a0.GetAttemptCount()===0){
this.LogSeq("`1147`",_1a3);
_1a5=false;
}
}else{
this.LogSeq("`1531`",_1a3);
this.LogSeq("`97`",_1a3);
if((_1a1==ROLLUP_RULE_ACTION_SATISFIED&&_1a6==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)||(_1a1==ROLLUP_RULE_ACTION_NOT_SATISFIED&&_1a7==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)){
this.LogSeq("`468`",_1a3);
_1a4=this.SequencingRulesCheckProcess(_1a0,RULE_SET_SKIPPED,_1a3);
this.LogSeq("`635`",_1a3);
if(_1a4!==null){
this.LogSeq("`1095`",_1a3);
_1a5=false;
}
}
}
}
}
}
this.LogSeq("`844`",_1a3);
if(_1a1==ROLLUP_RULE_ACTION_COMPLETED||_1a1==ROLLUP_RULE_ACTION_INCOMPLETE){
this.LogSeq("`423`",_1a3);
if(_1a0.RollupProgressCompletion()===true){
this.LogSeq("`591`",_1a3);
_1a5=true;
var _1a8=_1a0.GetRequiredForCompleted();
var _1a9=_1a0.GetRequiredForIncomplete();
this.LogSeq("`106`",_1a3);
if((_1a1==ROLLUP_RULE_ACTION_COMPLETED&&_1a8==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)||(_1a1==ROLLUP_RULE_ACTION_INCOMPLETE&&_1a9==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)){
this.LogSeq("`279`",_1a3);
if(_1a0.GetAttemptCount()>0&&_1a0.IsSuspended()===true){
this.LogSeq("`1361`",_1a3);
_1a5=false;
}
}else{
this.LogSeq("`1555`",_1a3);
this.LogSeq("`118`",_1a3);
if((_1a1==ROLLUP_RULE_ACTION_COMPLETED&&_1a8==ROLLUP_CONSIDERATION_IF_ATTEMPTED)||(_1a1==ROLLUP_RULE_ACTION_INCOMPLETE&&_1a9==ROLLUP_CONSIDERATION_IF_ATTEMPTED)){
this.LogSeq("`687`",_1a3);
if(_1a0.GetAttemptCount()===0){
this.LogSeq("`1128`",_1a3);
_1a5=false;
}
}else{
this.LogSeq("`1523`",_1a3);
this.LogSeq("`108`",_1a3);
if((_1a1==ROLLUP_RULE_ACTION_COMPLETED&&_1a8==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)||(_1a1==ROLLUP_RULE_ACTION_INCOMPLETE&&_1a9==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)){
this.LogSeq("`469`",_1a3);
_1a4=this.SequencingRulesCheckProcess(_1a0,RULE_SET_SKIPPED,_1a3);
this.LogSeq("`630`",_1a3);
if(_1a4!==null){
this.LogSeq("`1101`",_1a3);
_1a5=false;
}
}
}
}
}
}
this.LogSeq("`599`",_1a3);
this.LogSeqReturn(_1a5,_1a3);
return _1a5;
}
function Sequencer_ChoiceActivityTraversalSubprocess(_1aa,_1ab,_1ac){
Debug.AssertError("Calling log not passed.",(_1ac===undefined||_1ac===null));
var _1ad=this.LogSeqAudit("`1097`"+_1aa+", "+_1ab+")",_1ac);
var _1ae=null;
var _1af=null;
var _1b0;
this.LogSeq("`981`",_1ad);
if(_1ab==FLOW_DIRECTION_FORWARD){
this.LogSeq("`443`",_1ad);
_1ae=this.SequencingRulesCheckProcess(_1aa,RULE_SET_STOP_FORWARD_TRAVERSAL,_1ad);
this.LogSeq("`727`",_1ad);
if(_1ae!==null){
this.LogSeq("`559`",_1ad);
_1b0=new Sequencer_ChoiceActivityTraversalSubprocessResult(false,"SB.2.4-1",IntegrationImplementation.GetString("You are not allowed to move into {0} yet.",_1aa.GetTitle()));
this.LogSeqReturn(_1b0,_1ad);
return _1b0;
}
this.LogSeq("`614`",_1ad);
_1b0=new Sequencer_ChoiceActivityTraversalSubprocessResult(true,null);
this.LogSeqReturn(_1b0,_1ad);
return _1b0;
}
this.LogSeq("`970`",_1ad);
if(_1ab==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`1096`",_1ad);
if(!_1aa.IsTheRoot()){
_1af=this.Activities.GetParentActivity(_1aa);
this.LogSeq("`585`",_1ad);
if(_1af.GetSequencingControlForwardOnly()){
this.LogSeq("`547`",_1ad);
_1b0=new Sequencer_ChoiceActivityTraversalSubprocessResult(false,"SB.2.4-2",IntegrationImplementation.GetString("You must start {0} at the beginning.",_1af.GetTitle()));
this.LogSeqReturn(_1b0,_1ad);
return _1b0;
}
}else{
this.LogSeq("`1607`",_1ad);
this.LogSeq("`238`",_1ad);
_1b0=new Sequencer_ChoiceActivityTraversalSubprocessResult(false,"SB.2.4-3",IntegrationImplementation.GetString("You have reached the beginning of the course."));
this.LogSeqReturn(_1b0,_1ad);
return _1b0;
}
this.LogSeq("`607`",_1ad);
_1b0=new Sequencer_ChoiceActivityTraversalSubprocessResult(true,null);
this.LogSeqReturn(_1b0,_1ad);
return _1b0;
}
}
function Sequencer_ChoiceActivityTraversalSubprocessResult(_1b1,_1b2,_1b3){
this.Reachable=_1b1;
this.Exception=_1b2;
this.ExceptionText=_1b3;
}
Sequencer_ChoiceActivityTraversalSubprocessResult.prototype.toString=function(){
return "Reachable="+this.Reachable+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_ChoiceFlowSubprocess(_1b4,_1b5,_1b6){
Debug.AssertError("Calling log not passed.",(_1b6===undefined||_1b6===null));
var _1b7=this.LogSeqAudit("`1333`"+_1b4+", "+_1b5+")",_1b6);
this.LogSeq("`127`",_1b7);
var _1b8=this.ChoiceFlowTreeTraversalSubprocess(_1b4,_1b5,_1b7);
this.LogSeq("`724`",_1b7);
if(_1b8===null){
this.LogSeq("`713`",_1b7);
this.LogSeqReturn(_1b4,_1b7);
return _1b4;
}else{
this.LogSeq("`1660`",_1b7);
this.LogSeq("`338`",_1b7);
this.LogSeqReturn(_1b8,_1b7);
return _1b8;
}
}
function Sequencer_ChoiceFlowTreeTraversalSubprocess(_1b9,_1ba,_1bb){
Debug.AssertError("Calling log not passed.",(_1bb===undefined||_1bb===null));
var _1bc=this.LogSeqAudit("`1046`"+_1b9+", "+_1ba+")",_1bb);
var _1bd=this.Activities.GetParentActivity(_1b9);
var _1be=null;
var _1bf=null;
var _1c0=null;
this.LogSeq("`956`",_1bc);
if(_1ba==FLOW_DIRECTION_FORWARD){
this.LogSeq("`72`",_1bc);
if(this.IsActivityLastOverall(_1b9,_1bc)||_1b9.IsTheRoot()===true){
this.LogSeq("`685`",_1bc);
this.LogSeqReturn("`1731`",_1bc);
return null;
}
this.LogSeq("`477`",_1bc);
if(_1bd.IsActivityTheLastAvailableChild(_1b9)){
this.LogSeq("`139`",_1bc);
_1be=this.ChoiceFlowTreeTraversalSubprocess(_1bd,FLOW_DIRECTION_FORWARD,_1bc);
this.LogSeq("`144`",_1bc);
this.LogSeqReturn(_1be,_1bc);
return _1be;
}else{
this.LogSeq("`1598`",_1bc);
this.LogSeq("`299`",_1bc);
_1bf=_1bd.GetNextActivity(_1b9);
this.LogSeq("`448`",_1bc);
this.LogSeqReturn(_1bf,_1bc);
return _1bf;
}
}
this.LogSeq("`941`",_1bc);
if(_1ba==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`451`",_1bc);
if(_1b9.IsTheRoot()){
this.LogSeq("`680`",_1bc);
this.LogSeqReturn("`1731`",_1bc);
return null;
}
this.LogSeq("`472`",_1bc);
if(_1bd.IsActivityTheFirstAvailableChild(_1b9)){
this.LogSeq("`135`",_1bc);
_1be=this.ChoiceFlowTreeTraversalSubprocess(_1bd,FLOW_DIRECTION_BACKWARD,_1bc);
this.LogSeq("`137`",_1bc);
this.LogSeqReturn(_1be,_1bc);
return _1be;
}else{
this.LogSeq("`1606`",_1bc);
this.LogSeq("`265`",_1bc);
_1c0=_1bd.GetPreviousActivity(_1b9);
this.LogSeq("`441`",_1bc);
this.LogSeqReturn(_1c0,_1bc);
return _1c0;
}
}
}
function Sequencer_ChoiceSequencingRequestProcess(_1c1,_1c2){
Debug.AssertError("Calling log not passed.",(_1c2===undefined||_1c2===null));
var _1c3=this.LogSeqAudit("`1156`"+_1c1+")",_1c2);
var _1c4=null;
var _1c5;
var _1c6=null;
var _1c7=null;
var _1c8=null;
var _1c9=null;
var _1ca;
var i;
var _1cc;
this.LogSeq("`498`"+_1c1.LearningObject.ItemIdentifier,_1c3);
if(_1c1===null){
this.LogSeq("`442`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-1",IntegrationImplementation.GetString("Your selection is not permitted.  Please select an available menu item to continue."),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
this.LogSeq("`331`",_1c3);
_1c5=this.GetActivityPath(_1c1,true);
this.LogSeq("`1022`",_1c3);
for(i=(_1c5.length-1);i>=0;i--){
this.LogSeq("`787`",_1c3);
if(_1c5[i].IsTheRoot()==false){
this.LogSeq("`263`",_1c3);
if(_1c5[i].IsAvailable===false){
this.LogSeq("`394`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-2","The activity "+_1c1.GetTitle()+" should not be available and is not a valid selection",true);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
this.LogSeq("`250`",_1c3);
_1c6=this.SequencingRulesCheckProcess(_1c5[i],RULE_SET_HIDE_FROM_CHOICE,_1c3);
this.LogSeq("`741`",_1c3);
if(_1c6!==null){
this.LogSeq("`222`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-3","The activity "+_1c1.GetTitle()+" should be hidden and is not a valid selection",true);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
this.LogSeq("`752`",_1c3);
if(!_1c1.IsTheRoot()){
this.LogSeq("`219`",_1c3);
_1c4=this.Activities.GetParentActivity(_1c1);
if(_1c4.GetSequencingControlChoice()===false){
this.LogSeq("`419`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-4",IntegrationImplementation.GetString("The activity '{0}' should be hidden and is not a valid selection.",_1c4.GetTitle()),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
this.LogSeq("`578`",_1c3);
if(this.IsCurrentActivityDefined(_1c3)){
this.LogSeq("`631`",_1c3);
_1c8=this.GetCurrentActivity();
_1c7=this.FindCommonAncestor(_1c8,_1c1,_1c3);
}else{
this.LogSeq("`1678`",_1c3);
this.LogSeq("`371`",_1c3);
_1c7=this.GetRootActivity(_1c3);
}
if(_1c8!==null&&_1c8.LearningObject.ItemIdentifier==_1c1.LearningObject.ItemIdentifier){
this.LogSeq("`497`",_1c3);
this.LogSeq("`933`",_1c3);
}else{
if(this.AreActivitiesSiblings(_1c8,_1c1,_1c3)){
this.LogSeq("`361`",_1c3);
this.LogSeq("`10`",_1c3);
var _1cd=_1c4.GetActivityListBetweenChildren(_1c8,_1c1,false);
this.LogSeq("`831`",_1c3);
if(_1cd.length===0){
this.LogSeq("`425`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to open"),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
this.LogSeq("`446`",_1c3);
if(_1c1.Ordinal>_1c8.Ordinal){
this.LogSeq("`1331`",_1c3);
_1c9=FLOW_DIRECTION_FORWARD;
}else{
this.LogSeq("`1652`",_1c3);
this.LogSeq("`1307`",_1c3);
_1c9=FLOW_DIRECTION_BACKWARD;
_1cd.reverse();
}
this.LogSeq("`998`",_1c3);
for(i=0;i<_1cd.length;i++){
this.LogSeq("`513`",_1c3);
_1ca=this.ChoiceActivityTraversalSubprocess(_1cd[i],_1c9,_1c3);
this.LogSeq("`709`",_1c3);
if(_1ca.Reachable===false){
this.LogSeq("`140`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,_1ca.Exception,"",false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
this.LogSeq("`1432`",_1c3);
}else{
if(_1c8===null||_1c8.LearningObject.ItemIdentifier==_1c7.LearningObject.ItemIdentifier){
this.LogSeq("`212`",_1c3);
this.LogSeq("`249`",_1c3);
_1c5=this.GetPathToAncestorInclusive(_1c1,_1c7);
this.LogSeq("`1073`",_1c3);
if(_1c5.length===0){
this.LogSeq("`418`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to open"),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
this.LogSeq("`1009`",_1c3);
for(i=_1c5.length-1;i>=0;i--){
this.LogSeq("`526`",_1c3);
_1ca=this.ChoiceActivityTraversalSubprocess(_1c5[i],FLOW_DIRECTION_FORWARD,_1c3);
this.LogSeq("`707`",_1c3);
if(_1ca.Reachable===false){
this.LogSeq("`141`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,_1ca.Exception,_1ca.ExceptionText,false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
this.LogSeq("`19`",_1c3);
if(_1c5[i].IsActive()===false&&_1c5[i].LearningObject.ItemIdentifier!=_1c7.LearningObject.ItemIdentifier&&_1c5[i].GetPreventActivation()===true){
this.LogSeq("`601`"+PREVENT_ACTIVATION_ERROR+"`1540`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,PREVENT_ACTIVATION_ERROR,IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_1c5[i].GetTitle()),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
this.LogSeq("`1422`",_1c3);
}else{
if(_1c1.LearningObject.ItemIdentifier==_1c7.LearningObject.ItemIdentifier){
this.LogSeq("`286`",_1c3);
this.LogSeq("`346`",_1c3);
_1c5=this.GetPathToAncestorInclusive(_1c8,_1c7);
this.LogSeq("`1058`",_1c3);
if(_1c5.length===0){
this.LogSeq("`411`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to deliver"),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
this.LogSeq("`984`",_1c3);
for(i=0;i<_1c5.length;i++){
this.LogSeq("`668`",_1c3);
if(i!=(_1c5.length-1)){
this.LogSeq("`195`",_1c3);
if(_1c5[i].GetSequencingControlChoiceExit()===false){
this.LogSeq("`571`"+CONTROL_CHOICE_EXIT_ERROR_CHOICE+"`1540`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,CONTROL_CHOICE_EXIT_ERROR_CHOICE,IntegrationImplementation.GetString("Your selection is not permitted.  Please select 'Next' or 'Previous' to move through '{0}'.",_1c5[i]),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
}
this.LogSeq("`1408`",_1c3);
}else{
this.LogSeq("`287`",_1c3);
this.LogSeq("`343`",_1c3);
_1c5=this.GetPathToAncestorExclusive(_1c8,_1c7,false);
this.LogSeq("`1004`",_1c3);
var _1ce=null;
this.LogSeq("`572`",_1c3);
for(i=0;i<_1c5.length;i++){
this.LogSeq("`664`",_1c3);
if(i!=(_1c5.length-1)){
this.LogSeq("`197`",_1c3);
if(_1c5[i].GetSequencingControlChoiceExit()===false){
this.LogSeq("`569`"+CONTROL_CHOICE_EXIT_ERROR_CHOICE+"`1540`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,CONTROL_CHOICE_EXIT_ERROR_CHOICE,IntegrationImplementation.GetString("You are not allowed to jump out of {0}.",_1c5[i].GetTitle()),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
this.LogSeq("`399`",_1c3);
if(_1ce===null){
this.LogSeq("`742`",_1c3);
if(_1c5[i].GetConstrainedChoice()===true){
this.LogSeq("`918`"+_1c5[i]+"'",_1c3);
_1ce=_1c5[i];
}
}
}
this.LogSeq("`977`",_1c3);
if(_1ce!==null){
this.LogSeq("`467`",_1c3);
if(this.IsActivity1BeforeActivity2(_1ce,_1c1,_1c3)){
this.LogSeq("`528`",_1c3);
_1c9=FLOW_DIRECTION_FORWARD;
}else{
this.LogSeq("`1579`",_1c3);
this.LogSeq("`515`",_1c3);
_1c9=FLOW_DIRECTION_BACKWARD;
}
this.LogSeq("`523`",_1c3);
var _1cf=this.ChoiceFlowSubprocess(_1ce,_1c9,_1c3);
this.LogSeq("`558`",_1c3);
var _1d0=_1cf;
this.LogSeq("`5`",_1c3);
if((!_1d0.IsActivityAnAvailableDescendent(_1c1))&&(_1c1!=_1ce&&_1c1!=_1d0)){
this.LogSeq("`593`"+CONSTRAINED_CHOICE_ERROR+")",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,CONSTRAINED_CHOICE_ERROR,IntegrationImplementation.GetString("You are not allowed to jump out of {0}.",_1ce.GetTitle()),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
this.LogSeq("`242`",_1c3);
_1c5=this.GetPathToAncestorInclusive(_1c1,_1c7);
this.LogSeq("`1064`",_1c3);
if(_1c5.length===0){
this.LogSeq("`406`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to open"),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
this.LogSeq("`305`",_1c3);
if(this.IsActivity1BeforeActivity2(_1c8,_1c1,_1c3)){
this.LogSeq("`967`",_1c3);
for(i=(_1c5.length-1);i>=0;i--){
if(i>0){
this.LogSeq("`235`"+i,_1c3);
_1ca=this.ChoiceActivityTraversalSubprocess(_1c5[i],FLOW_DIRECTION_FORWARD,_1c3);
this.LogSeq("`683`",_1c3);
if(_1ca.Reachable===false){
this.LogSeq("`133`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,_1ca.Exception,_1ca.ExceptionText,false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
this.LogSeq("`13`",_1c3);
if((_1c5[i].IsActive()===false)&&(_1c5[i]!=_1c7)&&(_1c5[i].GetPreventActivation()===true)){
this.LogSeq("`580`"+PREVENT_ACTIVATION_ERROR+"`1540`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,PREVENT_ACTIVATION_ERROR,IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_1c5[i].GetTitle()),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
}else{
this.LogSeq("`1621`",_1c3);
this.LogSeq("`965`",_1c3);
for(i=(_1c5.length-1);i>=0;i--){
this.LogSeq("`15`",_1c3);
if((_1c5[i].IsActive()===false)&&(_1c5[i]!=_1c7)&&(_1c5[i].GetPreventActivation()===true)){
this.LogSeq("`573`"+PREVENT_ACTIVATION_ERROR+"`1540`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,PREVENT_ACTIVATION_ERROR,IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_1c5[i].GetTitle()),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
}
this.LogSeq("`1384`",_1c3);
}
}
}
}
this.LogSeq("`907`",_1c3);
if(_1c1.IsALeaf()===true){
this.LogSeq("`490`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(_1c1,null,"",false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
this.LogSeq("`51`",_1c3);
var _1d1=this.FlowSubprocess(_1c1,FLOW_DIRECTION_FORWARD,true,_1c3);
this.LogSeq("`252`",_1c3);
if(_1d1.Deliverable===false){
if(this.LookAhead===false){
this.LogSeq("`647`",_1c3);
this.TerminateDescendentAttemptsProcess(_1c7,_1c3);
this.LogSeq("`835`",_1c3);
this.EndAttemptProcess(_1c7,false,_1c3);
this.LogSeq("`880`",_1c3);
this.SetCurrentActivity(_1c1,_1c3);
}
this.LogSeq("`440`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-9",IntegrationImplementation.GetString("Please select another item from the menu."),false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}else{
this.LogSeq("`1674`",_1c3);
this.LogSeq("`312`",_1c3);
_1cc=new Sequencer_ChoiceSequencingRequestProcessResult(_1d1.IdentifiedActivity,null,"",false);
this.LogSeqReturn(_1cc,_1c3);
return _1cc;
}
}
function Sequencer_ChoiceSequencingRequestProcessResult(_1d2,_1d3,_1d4,_1d5){
if(_1d5===undefined){
Debug.AssertError("no value passed for hidden");
}
this.DeliveryRequest=_1d2;
this.Exception=_1d3;
this.ExceptionText=_1d4;
this.Hidden=_1d5;
}
Sequencer_ChoiceSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", Hidden="+this.Hidden;
};
function Sequencer_ClearSuspendedActivitySubprocess(_1d6,_1d7){
Debug.AssertError("Calling log not passed.",(_1d7===undefined||_1d7===null));
var _1d8=this.LogSeqAudit("`1107`"+_1d6+")",_1d7);
var _1d9=null;
var _1da=null;
var _1db=null;
this.LogSeq("`605`",_1d8);
if(this.IsSuspendedActivityDefined(_1d8)){
this.LogSeq("`602`",_1d8);
_1da=this.GetSuspendedActivity(_1d8);
_1d9=this.FindCommonAncestor(_1d6,_1da,_1d8);
this.LogSeq("`342`",_1d8);
_1db=this.GetPathToAncestorInclusive(_1da,_1d9);
this.LogSeq("`1007`",_1d8);
if(_1db.length>0){
this.LogSeq("`333`",_1d8);
for(var i=0;i<_1db.length;i++){
this.LogSeq("`1091`",_1d8);
if(_1db[i].IsALeaf()){
this.LogSeq("`792`",_1d8);
_1db[i].SetSuspended(false);
}else{
this.LogSeq("`1556`",_1d8);
this.LogSeq("`403`",_1d8);
if(_1db[i].HasSuspendedChildren()===false){
this.LogSeq("`770`",_1d8);
_1db[i].SetSuspended(false);
}
}
}
}
this.LogSeq("`606`",_1d8);
this.ClearSuspendedActivity(_1d8);
}
this.LogSeq("`1011`",_1d8);
this.LogSeqReturn("",_1d8);
return;
}
function Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess(_1dd,_1de){
if(_1de===undefined||_1de===null){
_1de=this.LogSeqAudit("Content Delivery Environment Activity Data SubProcess for "+activity.StringIdentifier);
}
var _1df=(Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK||Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR);
if(_1df){
var _1e0=this.PreviousActivity;
}else{
var _1e0=this.CurrentActivity;
}
var _1e1=this.GetSuspendedActivity(_1de);
var _1e2=this.GetRootActivity(_1de);
var _1e3=null;
var _1e4=false;
this.LogSeq("`210`",_1de);
if(_1e1!=_1dd){
this.LogSeq("`555`",_1de);
this.ClearSuspendedActivitySubprocess(_1dd,_1de);
}
this.LogSeq("`231`",_1de);
this.TerminateDescendentAttemptsProcess(_1dd,_1de);
this.LogSeq("`67`",_1de);
_1e3=this.GetPathToAncestorInclusive(_1dd,_1e2);
this.LogSeq("`1080`",_1de);
var _1e5=ConvertDateToIso8601String(new Date());
for(var i=(_1e3.length-1);i>=0;i--){
this.LogSeq("`1502`"+_1e3[i]+"`1194`",_1de);
if(_1df){
var _1e7=_1e3[i].WasActiveBeforeLaunchOnClick;
}else{
var _1e7=_1e3[i].IsActive();
}
if(_1e7===false){
this.LogSeq("`979`",_1de);
if(_1e3[i].IsTracked()){
this.LogSeq("`116`",_1de);
if(_1e3[i].IsSuspended()){
this.LogSeq("`810`",_1de);
_1e3[i].SetSuspended(false);
}else{
this.LogSeq("`1611`",_1de);
this.LogSeq("`487`",_1de);
_1e3[i].IncrementAttemptCount();
this.LogSeq("`358`",_1de);
if(_1e3[i].GetAttemptCount()==1){
this.LogSeq("`773`",_1de);
_1e3[i].SetActivityProgressStatus(true);
_1e4=true;
}
this.LogSeq("`164`",_1de);
_1e3[i].InitializeForNewAttempt(true,true);
var atts={ev:"AttemptStart",an:_1e3[i].GetAttemptCount(),ai:_1e3[i].ItemIdentifier,at:_1e3[i].LearningObject.Title};
this.WriteHistoryLog("",atts);
_1e3[i].SetAttemptStartTimestampUtc(_1e5);
_1e3[i].SetAttemptAbsoluteDuration("PT0H0M0S");
_1e3[i].SetAttemptExperiencedDurationTracked("PT0H0M0S");
_1e3[i].SetAttemptExperiencedDurationReported("PT0H0M0S");
if(Control.Package.Properties.ResetRunTimeDataTiming==RESET_RT_DATA_TIMING_ON_EACH_NEW_SEQUENCING_ATTEMPT){
if(_1e3[i].IsDeliverable()===true){
if(_1e4===false){
var atts={ev:"ResetRuntime",ai:_1e3[i].ItemIdentifier,at:_1e3[i].LearningObject.Title};
this.WriteHistoryLog("",atts);
}
_1e3[i].RunTime.ResetState();
}
}
this.LogSeq("`802`"+Control.Package.ObjectivesGlobalToSystem+"`940`",_1de);
if(Control.Package.ObjectivesGlobalToSystem===false&&_1e3[i].IsTheRoot()===true){
this.LogSeq("`543`",_1de);
this.ResetGlobalObjectives();
}
}
}
_1e3[i].SetAttemptedDuringThisAttempt();
}
}
}
function Sequencer_ContentDeliveryEnvironmentProcess(_1e9,_1ea){
Debug.AssertError("Calling log not passed.",(_1ea===undefined||_1ea===null));
var _1eb=this.LogSeqAudit("`1145`"+_1e9+")",_1ea);
var _1ec=(Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK||Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR);
var _1ed=this.GetCurrentActivity();
var _1ee=this.GetSuspendedActivity(_1eb);
var _1ef=this.GetRootActivity(_1eb);
var _1f0=null;
var _1f1;
this.LogSeq("`200`",_1eb);
if(_1ed!==null&&_1ed.IsActive()){
this.LogSeq("`266`",_1eb);
_1f1=new Sequencer_ContentDeliveryEnvironmentProcessResult(false,"DB.2.1",IntegrationImplementation.GetString("The previous activity must be terminated before a new activity may be attempted"));
this.LogSeqReturn(_1f1,_1eb);
return _1f1;
}
if(!_1ec){
this.ContentDeliveryEnvironmentActivityDataSubProcess(_1e9,_1ea);
}else{
this.LogSeq("`228`",_1eb);
}
this.LogSeq("`67`",_1eb);
_1f0=this.GetPathToAncestorInclusive(_1e9,_1ef);
this.LogSeq("`1080`",_1eb);
for(var i=(_1f0.length-1);i>=0;i--){
if(_1ec){
_1f0[i].WasActiveBeforeLaunchOnClick=_1f0[i].IsActive();
}
this.LogSeq("`1502`"+_1f0[i]+"`1194`",_1eb);
if(_1f0[i].IsActive()===false){
this.LogSeq("`892`",_1eb);
_1f0[i].SetActive(true);
}
}
this.LogSeq("`234`"+_1e9.GetItemIdentifier(),_1eb);
if(_1ec){
this.PreviousActivity=_1ed;
}
this.SetCurrentActivity(_1e9,_1eb);
this.LogSeq("`823`",_1eb);
_1f1=new Sequencer_ContentDeliveryEnvironmentProcessResult(true,null,"");
this.LogSeqReturn(_1f1,_1eb);
Control.DeliverActivity(_1e9);
return _1f1;
}
function Sequencer_ContentDeliveryEnvironmentProcessResult(_1f3,_1f4,_1f5){
this.Valid=_1f3;
this.Exception=_1f4;
this.ExceptionText=_1f5;
}
Sequencer_ContentDeliveryEnvironmentProcessResult.prototype.toString=function(){
return "Valid="+this.Valid+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_ContinueSequencingRequestProcess(_1f6){
Debug.AssertError("Calling log not passed.",(_1f6===undefined||_1f6===null));
var _1f7=this.LogSeqAudit("`1133`",_1f6);
var _1f8;
this.LogSeq("`504`",_1f7);
if(!this.IsCurrentActivityDefined(_1f7)){
this.LogSeq("`427`",_1f7);
_1f8=new Sequencer_ContinueSequencingRequestProcessResult(null,"SB.2.7-1",IntegrationInterface.GetString("The sequencing session has not begun yet."),false);
this.LogSeqReturn(_1f8,_1f7);
return _1f8;
}
var _1f9=this.GetCurrentActivity();
this.LogSeq("`712`",_1f7);
if(!_1f9.IsTheRoot()){
var _1fa=this.Activities.GetParentActivity(_1f9);
this.LogSeq("`297`",_1f7);
if(_1fa.GetSequencingControlFlow()===false){
this.LogSeq("`583`",_1f7);
_1f8=new Sequencer_ContinueSequencingRequestProcessResult(null,"SB.2.7-2",IntegrationImplementation.GetString("You cannot use 'Next' to enter {0}. Please select a menu item to continue.",_1fa.GetTitle()),false);
this.LogSeqReturn(_1f8,_1f7);
return _1f8;
}
}
this.LogSeq("`136`",_1f7);
var _1fb=this.FlowSubprocess(_1f9,FLOW_DIRECTION_FORWARD,false,_1f7);
this.LogSeq("`986`",_1f7);
if(_1fb.Deliverable===false){
this.LogSeq("`227`",_1f7);
_1f8=new Sequencer_ContinueSequencingRequestProcessResult(null,_1fb.Exception,_1fb.ExceptionText,_1fb.EndSequencingSession);
this.LogSeqReturn(_1f8,_1f7);
return _1f8;
}else{
this.LogSeq("`1679`",_1f7);
this.LogSeq("`318`",_1f7);
_1f8=new Sequencer_ContinueSequencingRequestProcessResult(_1fb.IdentifiedActivity,null,"",false);
this.LogSeqReturn(_1f8,_1f7);
return _1f8;
}
}
function Sequencer_ContinueSequencingRequestProcessResult(_1fc,_1fd,_1fe,_1ff){
Debug.AssertError("Invalid endSequencingSession ("+_1ff+") passed to ContinueSequencingRequestProcessResult.",(_1ff!=true&&_1ff!=false));
this.DeliveryRequest=_1fc;
this.Exception=_1fd;
this.ExceptionText=_1fe;
this.EndSequencingSession=_1ff;
}
Sequencer_ContinueSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_DeliveryRequestProcess(_200,_201){
Debug.AssertError("Calling log not passed.",(_201===undefined||_201===null));
var _202=this.LogSeqAudit("`1327`"+_200+")",_201);
var _203;
var _204;
this.LogSeq("`874`"+_200+"`954`",_202);
if(!_200.IsALeaf()){
this.LogSeq("`589`",_202);
_204=new Sequencer_DeliveryRequestProcessResult(false,"DB.1.1-1",IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_200.GetTitle()));
this.LogSeqReturn(_204,_202);
return _204;
}
this.LogSeq("`208`",_202);
var _205=this.GetActivityPath(_200,true);
this.LogSeq("`839`",_202);
if(_205.length===0){
this.LogSeq("`584`",_202);
_204=new Sequencer_DeliveryRequestProcessResult(false,"DB.1.1-2",IntegrationImplementation.GetString("Nothing to open"));
this.LogSeqReturn(_204,_202);
return _204;
}
this.LogSeq("`527`",_202);
for(var i=0;i<_205.length;i++){
this.LogSeq("`862`"+_205[i],_202);
_203=this.CheckActivityProcess(_205[i],_202);
this.LogSeq("`904`",_202);
if(_203===true){
this.LogSeq("`568`",_202);
_204=new Sequencer_DeliveryRequestProcessResult(false,"DB.1.1-3",IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_200.GetTitle()));
this.LogSeqReturn(_204,_202);
return _204;
}
}
this.LogSeq("`667`",_202);
_204=new Sequencer_DeliveryRequestProcessResult(true,null,"");
this.LogSeqReturn(_204,_202);
return _204;
}
function Sequencer_DeliveryRequestProcessResult(_207,_208,_209){
this.Valid=_207;
this.Exception=_208;
this.ExceptionText=_209;
}
Sequencer_DeliveryRequestProcessResult.prototype.toString=function(){
return "Valid="+this.Valid+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_EndAttemptProcess(_20a,_20b,_20c,_20d){
Debug.AssertError("Calling log not passed.",(_20c===undefined||_20c===null));
if(_20d===undefined||_20d===null){
_20d=false;
}
var _20e=this.LogSeqAudit("`1440`"+_20a+", "+_20b+")",_20c);
this.LogSeq("`1475`"+_20a.GetItemIdentifier()+"`1671`",_20e);
var i;
var _210=new Array();
if(_20a.IsALeaf()){
this.LogSeq("`1000`",_20e);
if(_20a.IsTracked()&&_20a.WasLaunchedThisSession()){
this.LogSeq("`861`",_20e);
_20a.TransferRteDataToActivity();
this.LogSeq("`309`",_20e);
if(_20a.IsSuspended()===false){
this.LogSeq("`282`",_20e);
if(_20a.IsCompletionSetByContent()===false){
this.LogSeq("`243`",_20e);
if(_20a.GetAttemptProgressStatus()===false){
this.LogSeq("`737`",_20e);
_20a.SetAttemptProgressStatus(true);
this.LogSeq("`711`",_20e);
_20a.SetAttemptCompletionStatus(true);
_20a.WasAutoCompleted=true;
}
}
this.LogSeq("`295`",_20e);
if(_20a.IsObjectiveSetByContent()===false){
this.LogSeq("`592`",_20e);
var _211=_20a.GetPrimaryObjective();
this.LogSeq("`187`",_20e);
if(_211.GetProgressStatus(_20a,false)===false){
this.LogSeq("`662`",_20e);
_211.SetProgressStatus(true,false,_20a);
this.LogSeq("`654`",_20e);
_211.SetSatisfiedStatus(true,false,_20a);
_20a.WasAutoSatisfied=true;
}
}
}
}
}else{
this.LogSeq("`1203`",_20e);
this.LogSeq("`107`",_20e);
if(this.ActivityHasSuspendedChildren(_20a,_20e)){
this.LogSeq("`822`",_20e);
_20a.SetSuspended(true);
}else{
this.LogSeq("`1690`",_20e);
this.LogSeq("`812`",_20e);
_20a.SetSuspended(false);
}
}
if(_20d===false){
this.LogSeq("`460`",_20e);
_20a.SetActive(false);
}
var _212;
if(_20b===false){
this.LogSeq("`247`",_20e);
_212=this.OverallRollupProcess(_20a,_20e);
}else{
this.LogSeq("`519`",_20e);
_210[0]=_20a;
}
this.LogSeq("`230`",_20e);
var _213=this.FindActivitiesAffectedByWriteMaps(_20a);
var _214=this.FindDistinctParentsOfActivitySet(_213);
if(_20b===false){
this.LogSeq("`453`",_20e);
var _215=this.GetMinimalSubsetOfActivitiesToRollup(_214,_212);
for(i=0;i<_215.length;i++){
if(_215[i]!==null){
this.OverallRollupProcess(_215[i],_20e);
}
}
}else{
this.LogSeq("`218`",_20e);
_210=_210.concat(_214);
}
if(this.LookAhead===false&&_20d===false){
this.RandomizeChildrenProcess(_20a,true,_20e);
}
this.LogSeq("`1291`",_20e);
this.LogSeqReturn("",_20e);
return _210;
}
function Sequencer_EvaluateRollupConditionsSubprocess(_216,_217,_218){
Debug.AssertError("Calling log not passed.",(_218===undefined||_218===null));
var _219=this.LogSeqAudit("`1045`"+_216+", "+_217+")",_218);
var _21a;
var _21b;
this.LogSeq("`388`",_219);
var _21c=new Array();
var i;
this.LogSeq("`790`",_219);
for(i=0;i<_217.Conditions.length;i++){
this.LogSeq("`37`",_219);
_21a=Sequencer_EvaluateRollupRuleCondition(_216,_217.Conditions[i]);
this.LogSeq("`367`",_219);
if(_217.Conditions[i].Operator==RULE_CONDITION_OPERATOR_NOT&&_21a!=RESULT_UNKNOWN){
this.LogSeq("`1150`",_219);
_21a=(!_21a);
}
this.LogSeq("`966`"+_21a+"`536`",_219);
_21c[_21c.length]=_21a;
}
this.LogSeq("`311`",_219);
if(_21c.length===0){
this.LogSeq("`690`",_219);
return RESULT_UNKNOWN;
}
this.LogSeq("`1099`"+_217.ConditionCombination+"`291`",_219);
if(_217.ConditionCombination==RULE_CONDITION_COMBINATION_ANY){
_21b=false;
for(i=0;i<_21c.length;i++){
_21b=Sequencer_LogicalOR(_21b,_21c[i]);
}
}else{
_21b=true;
for(i=0;i<_21c.length;i++){
_21b=Sequencer_LogicalAND(_21b,_21c[i]);
}
}
this.LogSeq("`495`",_219);
this.LogSeqReturn(_21b,_219);
return _21b;
}
function Sequencer_EvaluateRollupRuleCondition(_21e,_21f){
var _220=null;
switch(_21f.Condition){
case ROLLUP_RULE_CONDITION_SATISFIED:
_220=_21e.IsSatisfied("");
break;
case ROLLUP_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN:
_220=_21e.IsObjectiveStatusKnown("",false);
break;
case ROLLUP_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN:
_220=_21e.IsObjectiveMeasureKnown("",false);
break;
case ROLLUP_RULE_CONDITION_COMPLETED:
_220=_21e.IsCompleted("",false);
break;
case ROLLUP_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN:
_220=_21e.IsActivityProgressKnown("",false);
break;
case ROLLUP_RULE_CONDITION_ATTEMPTED:
_220=_21e.IsAttempted();
break;
case ROLLUP_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED:
_220=_21e.IsAttemptLimitExceeded();
break;
case ROLLUP_RULE_CONDITION_NEVER:
_220=false;
break;
default:
this.LogSeq("`1370`",logParent);
break;
}
return _220;
}
function Sequencer_ExitSequencingRequestProcess(_221){
Debug.AssertError("Calling log not passed.",(_221===undefined||_221===null));
var _222=this.LogSeqAudit("`1214`",_221);
var _223;
this.LogSeq("`486`",_222);
if(!this.IsCurrentActivityDefined(_222)){
this.LogSeq("`511`",_222);
_223=new Sequencer_ExitSequencingRequestProcessResult(false,"SB.2.11-1",IntegrationImplementation.GetString("An 'Exit Sequencing' request cannot be processed until the sequencing session has begun."));
this.LogSeqReturn(_223,_222);
return _223;
}
var _224=this.GetCurrentActivity();
this.LogSeq("`320`",_222);
if(_224.IsActive()){
this.LogSeq("`510`",_222);
_223=new Sequencer_ExitSequencingRequestProcessResult(false,"SB.2.11-2",IntegrationImplementation.GetString("An 'Exit Sequencing' request cannot be processed while an activity is still active."));
this.LogSeqReturn(_223,_222);
return _223;
}
this.LogSeq("`753`",_222);
if(_224.IsTheRoot()||this.CourseIsSingleSco()===true){
this.LogSeq("`220`",_222);
_223=new Sequencer_ExitSequencingRequestProcessResult(true,null,"");
this.LogSeqReturn(_223,_222);
return _223;
}
this.LogSeq("`560`",_222);
_223=new Sequencer_ExitSequencingRequestProcessResult(false,null,"");
this.LogSeqReturn(_223,_222);
return _223;
}
function Sequencer_ExitSequencingRequestProcessResult(_225,_226,_227){
Debug.AssertError("Invalid endSequencingSession ("+_225+") passed to ExitSequencingRequestProcessResult.",(_225!=true&&_225!=false));
this.EndSequencingSession=_225;
this.Exception=_226;
this.ExceptionText=_227;
}
Sequencer_ExitSequencingRequestProcessResult.prototype.toString=function(){
return "EndSequencingSession="+this.EndSequencingSession+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_FlowActivityTraversalSubprocess(_228,_229,_22a,_22b){
Debug.AssertError("Calling log not passed.",(_22b===undefined||_22b===null));
var _22c=this.LogSeqAudit("`1141`"+_228+", "+_229+", "+_22a+")",_22b);
var _22d;
var _22e;
var _22f;
var _230;
var _231;
var _232=this.Activities.GetParentActivity(_228);
this.LogSeq("`458`",_22c);
if(_232.GetSequencingControlFlow()===false){
this.LogSeq("`384`",_22c);
_231=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_228,"SB.2.2-1",IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_232.GetTitle()),false);
this.LogSeqReturn(_231,_22c);
return _231;
}
this.LogSeq("`531`",_22c);
_22d=this.SequencingRulesCheckProcess(_228,RULE_SET_SKIPPED,_22c);
this.LogSeq("`359`",_22c);
if(_22d!==null){
this.LogSeq("`181`",_22c);
_22e=this.FlowTreeTraversalSubprocess(_228,_229,_22a,false,_22c);
this.LogSeq("`638`",_22c);
if(_22e.NextActivity===null){
this.LogSeq("`158`",_22c);
_231=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_228,_22e.Exception,_22e.ExceptionText,_22e.EndSequencingSession);
this.LogSeqReturn(_231,_22c);
return _231;
}else{
this.LogSeq("`1654`",_22c);
this.LogSeq("`66`",_22c);
if(_22a==FLOW_DIRECTION_BACKWARD&&_22e.TraversalDirection==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`35`",_22c);
_22f=this.FlowActivityTraversalSubprocess(_22e.NextActivity,_22e.TraversalDirection,null,_22c);
}else{
this.LogSeq("`1586`",_22c);
this.LogSeq("`9`",_22c);
_22f=this.FlowActivityTraversalSubprocess(_22e.NextActivity,_229,_22a,_22c);
}
this.LogSeq("`233`",_22c);
_231=_22f;
this.LogSeqReturn(_231,_22c);
return _231;
}
}
this.LogSeq("`564`",_22c);
_230=this.CheckActivityProcess(_228,_22c);
this.LogSeq("`920`",_22c);
if(_230===true){
this.LogSeq("`385`",_22c);
_231=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_228,"SB.2.2-2",IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.",_228.GetTitle()),false);
this.LogSeqReturn(_231,_22c);
return _231;
}
this.LogSeq("`276`",_22c);
if(_228.IsALeaf()===false){
this.LogSeq("`160`",_22c);
_22e=this.FlowTreeTraversalSubprocess(_228,_229,null,true,_22c);
this.LogSeq("`637`",_22c);
if(_22e.NextActivity===null){
this.LogSeq("`157`",_22c);
_231=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_228,_22e.Exception,_22e.ExceptionText,_22e.EndSequencingSession);
this.LogSeqReturn(_231,_22c);
return _231;
}else{
this.LogSeq("`1647`",_22c);
this.LogSeq("`45`",_22c);
if(_229==FLOW_DIRECTION_BACKWARD&&_22e.TraversalDirection==FLOW_DIRECTION_FORWARD){
this.LogSeq("`22`",_22c);
_22f=this.FlowActivityTraversalSubprocess(_22e.NextActivity,FLOW_DIRECTION_FORWARD,FLOW_DIRECTION_BACKWARD,_22c);
}else{
this.LogSeq("`1588`",_22c);
this.LogSeq("`30`",_22c);
_22f=this.FlowActivityTraversalSubprocess(_22e.NextActivity,_229,null,_22c);
}
this.LogSeq("`225`",_22c);
_231=_22f;
this.LogSeqReturn(_231,_22c);
return _231;
}
}
this.LogSeq("`357`",_22c);
_231=new Sequencer_FlowActivityTraversalSubprocessReturnObject(true,_228,null,"",false);
this.LogSeqReturn(_231,_22c);
return _231;
}
function Sequencer_FlowActivityTraversalSubprocessReturnObject(_233,_234,_235,_236,_237){
Debug.AssertError("Invalid endSequencingSession ("+_237+") passed to FlowActivityTraversalSubprocessReturnObject.",(_237!=true&&_237!=false));
this.Deliverable=_233;
this.NextActivity=_234;
this.Exception=_235;
this.ExceptionText=_236;
this.EndSequencingSession=_237;
}
Sequencer_FlowActivityTraversalSubprocessReturnObject.prototype.toString=function(){
return "Deliverable="+this.Deliverable+", NextActivity="+this.NextActivity+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_FlowSubprocess(_238,_239,_23a,_23b){
Debug.AssertError("Calling log not passed.",(_23b===undefined||_23b===null));
var _23c=this.LogSeqAudit("`1489`"+_238+", "+_239+", "+_23a+")",_23b);
var _23d;
this.LogSeq("`509`",_23c);
var _23e=_238;
this.LogSeq("`6`",_23c);
var _23f=this.FlowTreeTraversalSubprocess(_23e,_239,null,_23a,_23c);
this.LogSeq("`488`",_23c);
if(_23f.NextActivity===null){
this.LogSeq("`205`",_23c);
_23d=new Sequencer_FlowSubprocessResult(_23e,false,_23f.Exception,_23f.ExceptionText,_23f.EndSequencingSession);
this.LogSeqReturn(_23d,_23c);
return _23d;
}else{
this.LogSeq("`1691`",_23c);
this.LogSeq("`557`",_23c);
_23e=_23f.NextActivity;
this.LogSeq("`48`",_23c);
var _240=this.FlowActivityTraversalSubprocess(_23e,_239,null,_23c);
this.LogSeq("`18`",_23c);
_23d=new Sequencer_FlowSubprocessResult(_240.NextActivity,_240.Deliverable,_240.Exception,_240.ExceptionText,_240.EndSequencingSession);
this.LogSeqReturn(_23d,_23c);
return _23d;
}
}
function Sequencer_FlowSubprocessResult(_241,_242,_243,_244,_245){
Debug.AssertError("Invalid endSequencingSession ("+_245+") passed to FlowSubprocessResult.",(_245!=true&&_245!=false));
this.IdentifiedActivity=_241;
this.Deliverable=_242;
this.Exception=_243;
this.ExceptionText=_244;
this.EndSequencingSession=_245;
}
Sequencer_FlowSubprocessResult.prototype.toString=function(){
return "IdentifiedActivity="+this.IdentifiedActivity+", Deliverable="+this.Deliverable+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_FlowTreeTraversalSubprocess(_246,_247,_248,_249,_24a){
Debug.AssertError("Calling log not passed.",(_24a===undefined||_24a===null));
var _24b=this.LogSeqAudit("`1237`"+_246+", "+_247+", "+_248+", "+_249+")",_24a);
var _24c;
var _24d;
var _24e;
var _24f;
var _250;
var _251;
var _252=this.Activities.GetParentActivity(_246);
this.LogSeq("`1174`",_24b);
_24c=false;
this.LogSeq("`24`",_24b);
if(_248!==null&&_248==FLOW_DIRECTION_BACKWARD&&_252.IsActivityTheLastAvailableChild(_246)){
this.LogSeq("`1126`",_24b);
_247=FLOW_DIRECTION_BACKWARD;
this.LogSeq("`552`",_24b);
_246=_252.GetFirstAvailableChild();
this.LogSeq("`1151`",_24b);
_24c=true;
}
this.LogSeq("`980`",_24b);
if(_247==FLOW_DIRECTION_FORWARD){
this.LogSeq("`264`",_24b);
if((this.IsActivityLastOverall(_246,_24b))||(_249===false&&_246.IsTheRoot()===true)){
this.LogSeq("`575`",_24b);
this.TerminateDescendentAttemptsProcess(this.Activities.GetRootActivity(),_24b);
this.LogSeq("`167`",_24b);
_251=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,null,null,true);
this.LogSeqReturn(_251,_24b);
return _251;
}
this.LogSeq("`755`",_24b);
if(_246.IsALeaf()||_249===false){
this.LogSeq("`474`",_24b);
if(_252.IsActivityTheLastAvailableChild(_246)){
this.LogSeq("`29`",_24b);
_24d=this.FlowTreeTraversalSubprocess(_252,FLOW_DIRECTION_FORWARD,null,false,_24b);
this.LogSeq("`224`",_24b);
_251=_24d;
this.LogSeqReturn(_251,_24b);
return _251;
}else{
this.LogSeq("`1592`",_24b);
this.LogSeq("`300`",_24b);
_24e=_252.GetNextActivity(_246);
this.LogSeq("`201`",_24b);
_251=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_24e,_247,null,"",false);
this.LogSeqReturn(_251,_24b);
return _251;
}
}else{
this.LogSeq("`1131`",_24b);
this.LogSeq("`395`",_24b);
_24f=_246.GetAvailableChildren();
if(_24f.length>0){
this.LogSeq("`327`"+_24f[0]+"); Traversal Direction: traversal direction ("+_247+"); Exception: n/a )",_24b);
_251=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_24f[0],_247,null,"",false);
this.LogSeqReturn(_251,_24b);
return _251;
}else{
this.LogSeq("`1599`",_24b);
this.LogSeq("`426`",_24b);
_251=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-2",IntegrationImplementation.GetString("The activity '{0}' does not have any available children to deliver.",_246.GetTitle()),false);
this.LogSeqReturn(_251,_24b);
return _251;
}
}
}
this.LogSeq("`971`",_24b);
if(_247==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`462`",_24b);
if(_246.IsTheRoot()){
this.LogSeq("`434`",_24b);
_251=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-3",IntegrationImplementation.GetString("You have reached the beginning of the course."),false);
this.LogSeqReturn(_251,_24b);
return _251;
}
this.LogSeq("`765`",_24b);
if(_246.IsALeaf()||_249===false){
this.LogSeq("`337`",_24b);
if(_24c===false){
this.LogSeq("`316`",_24b);
if(_252.GetSequencingControlForwardOnly()===true){
this.LogSeq("`389`",_24b);
_251=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-4",IntegrationImplementation.GetString("The activity '{0}' may only be entered from the beginning.",_252.GetTitle()),false);
this.LogSeqReturn(_251,_24b);
return _251;
}
}
this.LogSeq("`473`",_24b);
if(_252.IsActivityTheFirstAvailableChild(_246)){
this.LogSeq("`25`",_24b);
_24d=this.FlowTreeTraversalSubprocess(_252,FLOW_DIRECTION_BACKWARD,null,false,_24b);
this.LogSeq("`229`",_24b);
_251=_24d;
this.LogSeqReturn(_251,_24b);
return _251;
}else{
this.LogSeq("`1597`",_24b);
this.LogSeq("`268`",_24b);
_250=_252.GetPreviousActivity(_246);
this.LogSeq("`798`"+_250+"`1535`"+_247+"`1633`",_24b);
_251=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_250,_247,null,"",false);
this.LogSeqReturn(_251,_24b);
return _251;
}
}else{
this.LogSeq("`1079`",_24b);
this.LogSeq("`379`",_24b);
_24f=_246.GetAvailableChildren();
if(_24f.length>0){
this.LogSeq("`665`",_24b);
if(_246.GetSequencingControlForwardOnly()===true){
this.LogSeq("`49`",_24b);
_251=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_24f[0],FLOW_DIRECTION_FORWARD,null,"",false);
this.LogSeqReturn(_251,_24b);
return _251;
}else{
this.LogSeq("`1566`",_24b);
this.LogSeq("`43`",_24b);
_251=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_24f[_24f.length-1],FLOW_DIRECTION_BACKWARD,null,"",false);
this.LogSeqReturn(_251,_24b);
return _251;
}
}else{
this.LogSeq("`1595`",_24b);
this.LogSeq("`404`",_24b);
_251=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-2",IntegrationInterface.GetString("The activity '{0}' may only be entered from the beginning.",_252.GetTitle()),false);
this.LogSeqReturn(_251,_24b);
return _251;
}
}
}
}
function Sequencer_FlowTreeTraversalSubprocessReturnObject(_253,_254,_255,_256,_257){
Debug.AssertError("Invalid endSequencingSession ("+_257+") passed to FlowTreeTraversalSubprocessReturnObject.",(_257!=true&&_257!=false));
this.NextActivity=_253;
this.TraversalDirection=_254;
this.Exception=_255;
this.ExceptionText=_256;
this.EndSequencingSession=_257;
}
Sequencer_FlowTreeTraversalSubprocessReturnObject.prototype.toString=function(){
return "NextActivity="+this.NextActivity+", TraversalDirection="+this.TraversalDirection+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_LimitConditionsCheckProcess(_258,_259){
Debug.AssertError("Calling log not passed.",(_259===undefined||_259===null));
var _25a=this.LogSeqAudit("`1271`"+_258+")",_259);
this.LogSeq("`377`",_25a);
if(_258.IsTracked()===false){
this.LogSeq("`292`",_25a);
this.LogSeqReturn("`1728`",_25a);
return false;
}
this.LogSeq("`143`",_25a);
if(_258.IsActive()||_258.IsSuspended()){
this.LogSeq("`677`",_25a);
this.LogSeqReturn("`1728`",_25a);
return false;
}
this.LogSeq("`705`",_25a);
if(_258.GetLimitConditionAttemptControl()===true){
var _25b=_258.GetAttemptCount();
var _25c=_258.GetLimitConditionAttemptLimit();
var _25d=_258.GetActivityProgressStatus();
this.LogSeq("`691`"+_25d+"`1132`"+_25b+"`551`"+_25c+"`1522`",_25a);
if(_25d===true&&(_25b>=_25c)){
this.LogSeq("`416`",_25a);
this.LogSeqReturn("`1732`",_25a);
return true;
}
}
this.LogSeq("`539`",_25a);
this.LogSeq("`428`",_25a);
this.LogSeqReturn("`1728`",_25a);
return false;
}
function Sequencer_MeasureRollupProcess(_25e,_25f){
Debug.AssertError("Calling log not passed.",(_25f===undefined||_25f===null));
var _260=this.LogSeqAudit("`1360`"+_25e+")",_25f);
this.LogSeq("`961`",_260);
var _261=0;
this.LogSeq("`1322`",_260);
var _262=false;
this.LogSeq("`1039`",_260);
var _263=0;
this.LogSeq("`1052`",_260);
var _264=null;
var i;
this.LogSeq("`626`",_260);
var _264=_25e.GetPrimaryObjective();
this.LogSeq("`1102`",_260);
if(_264!==null){
this.LogSeq("`1157`",_260);
var _266=_25e.GetChildren();
var _267=null;
var _268;
var _269;
var _26a;
for(i=0;i<_266.length;i++){
this.LogSeq("`556`"+_266[i].IsTracked(),_260);
if(_266[i].IsTracked()){
this.LogSeq("`978`",_260);
_267=null;
this.LogSeq("`1158`",_260);
var _267=_266[i].GetPrimaryObjective();
this.LogSeq("`962`",_260);
if(_267!==null){
this.LogSeq("`545`",_260);
_269=_266[i].GetRollupObjectiveMeasureWeight();
_263+=_269;
this.LogSeq("`600`",_260);
if(_267.GetMeasureStatus(_266[i],false)===true){
this.LogSeq("`122`",_260);
_26a=_267.GetNormalizedMeasure(_266[i],false);
this.LogSeq("`829`"+_26a+"`1458`"+_269,_260);
_262=true;
_261+=(_26a*_269);
}
}else{
this.LogSeq("`1567`",_260);
this.LogSeq("`501`",_260);
Debug.AssertError("Measure Rollup Process encountered an activity with no primary objective.");
this.LogSeqReturn("",_260);
return;
}
}
}
this.LogSeq("`1233`",_260);
if(_262===false||_263==0){
this.LogSeq("`103`"+_263+")",_260);
_264.SetMeasureStatus(false,_25e);
}else{
this.LogSeq("`1644`",_260);
this.LogSeq("`688`",_260);
_264.SetMeasureStatus(true,_25e);
this.LogSeq("`491`"+_261+"`1374`"+_263+"`1718`"+(_261/_263),_260);
var _26b=(_261/_263);
_26b=RoundToPrecision(_26b,7);
_264.SetNormalizedMeasure(_26b,_25e);
}
this.LogSeq("`1190`",_260);
this.LogSeqReturn("",_260);
return;
}
this.LogSeq("`542`",_260);
this.LogSeqReturn("",_260);
return;
}
function Sequencer_NavigationRequestProcess(_26c,_26d,_26e){
Debug.AssertError("Calling log not passed.",(_26e===undefined||_26e===null));
var _26f=this.LogSeqAudit("`1293`"+_26c+", "+_26d+")",_26e);
var _270=this.GetCurrentActivity();
var _271=null;
if(_270!==null){
_271=_270.ParentActivity;
}
var _272="";
if(_271!==null){
_272=_271.GetTitle();
}
var _273;
switch(_26c){
case NAVIGATION_REQUEST_START:
this.LogSeq("`1162`",_26f);
this.LogSeq("`463`",_26f);
if(!this.IsCurrentActivityDefined(_26f)){
this.LogSeq("`207`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,null,SEQUENCING_REQUEST_START,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}else{
this.LogSeq("`1640`",_26f);
this.LogSeq("`176`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-1",null,IntegrationImplementation.GetString("The sequencing session has already been started."));
this.LogSeqReturn(_273,_26f);
return _273;
}
break;
case NAVIGATION_REQUEST_RESUME_ALL:
this.LogSeq("`1037`",_26f);
this.LogSeq("`465`",_26f);
if(!this.IsCurrentActivityDefined(_26f)){
this.LogSeq("`334`",_26f);
if(this.IsSuspendedActivityDefined(_26f)){
this.LogSeq("`168`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,null,SEQUENCING_REQUEST_RESUME_ALL,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}else{
this.LogSeq("`1580`",_26f);
this.LogSeq("`161`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-3",null,IntegrationImplementation.GetString("There is no suspended activity to resume."));
this.LogSeqReturn(_273,_26f);
return _273;
}
}else{
this.LogSeq("`1649`",_26f);
this.LogSeq("`177`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-1",null,IntegrationImplementation.GetString("The sequencing session has already been started."));
this.LogSeqReturn(_273,_26f);
return _273;
}
break;
case NAVIGATION_REQUEST_CONTINUE:
this.LogSeq("`1081`",_26f);
this.LogSeq("`482`",_26f);
if(!this.IsCurrentActivityDefined(_26f)){
this.LogSeq("`179`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-1",null,IntegrationImplementation.GetString("Cannot continue until the sequencing session has begun."));
this.LogSeqReturn(_273,_26f);
return _273;
}
this.LogSeq("`40`",_26f);
if((!_270.IsTheRoot())&&(_271.LearningObject.SequencingData.ControlFlow===true)){
this.LogSeq("`213`",_26f);
if(_270.IsActive()){
this.LogSeq("`185`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_CONTINUE,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}else{
this.LogSeq("`1591`",_26f);
this.LogSeq("`192`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,null,SEQUENCING_REQUEST_CONTINUE,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}
}else{
this.LogSeq("`1659`",_26f);
this.LogSeq("`34`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-4",null,IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_272));
this.LogSeqReturn(_273,_26f);
return _273;
}
break;
case NAVIGATION_REQUEST_PREVIOUS:
this.LogSeq("`1090`",_26f);
this.LogSeq("`483`",_26f);
if(!this.IsCurrentActivityDefined(_26f)){
this.LogSeq("`184`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("Cannot move backwards until the sequencing session has begun."));
this.LogSeqReturn(_273,_26f);
return _273;
}
this.LogSeq("`241`",_26f);
if(!_270.IsTheRoot()){
this.LogSeq("`12`",_26f);
if(_271.LearningObject.SequencingData.ControlFlow===true&&_271.LearningObject.SequencingData.ControlForwardOnly===false){
this.LogSeq("`204`",_26f);
if(_270.IsActive()){
this.LogSeq("`173`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_PREVIOUS,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}else{
this.LogSeq("`1569`",_26f);
this.LogSeq("`180`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,null,SEQUENCING_REQUEST_PREVIOUS,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}
}else{
this.LogSeq("`1609`",_26f);
this.LogSeq("`102`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-5",null,IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_271.GetTitle()));
this.LogSeqReturn(_273,_26f);
return _273;
}
}else{
this.LogSeq("`1656`",_26f);
this.LogSeq("`50`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-6",null,IntegrationImplementation.GetString("You have reached the beginning of the course."));
this.LogSeqReturn(_273,_26f);
return _273;
}
break;
case NAVIGATION_REQUEST_FORWARD:
this.LogSeq("`799`",_26f);
this.LogSeq("`188`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-7",null,IntegrationImplementation.GetString("The 'Forward' navigation request is not supported, try using 'Continue'."));
this.LogSeqReturn(_273,_26f);
return _273;
case NAVIGATION_REQUEST_BACKWARD:
this.LogSeq("`788`",_26f);
this.LogSeq("`189`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-7",null,IntegrationImplementation.GetString("The 'Backward' navigation request is not supported, try using 'Previous'."));
this.LogSeqReturn(_273,_26f);
return _273;
case NAVIGATION_REQUEST_CHOICE:
this.LogSeq("`1100`",_26f);
this.LogSeq("`196`",_26f);
if(this.DoesActivityExist(_26d,_26f)){
var _274=this.GetActivityFromIdentifier(_26d,_26f);
var _275=this.Activities.GetParentActivity(_274);
if(_274.IsAvailable()===false){
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-7",null,IntegrationImplementation.GetString("The activity '{0}' was not selected to be delivered in this attempt.",_274));
this.LogSeqReturn(_273,_26f);
return _273;
}
this.LogSeq("`2`",_26f);
if(_274.IsTheRoot()||_275.LearningObject.SequencingData.ControlChoice===true){
this.LogSeq("`450`",_26f);
if(!this.IsCurrentActivityDefined(_26f)){
this.LogSeq("`59`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,null,SEQUENCING_REQUEST_CHOICE,null,_274,"");
this.LogSeqReturn(_273,_26f);
return _273;
}
this.LogSeq("`402`",_26f);
if(!this.AreActivitiesSiblings(_270,_274,_26f)){
this.LogSeq("`365`",_26f);
var _276=this.FindCommonAncestor(_270,_274,_26f);
this.LogSeq("`1`",_26f);
var _277=this.GetPathToAncestorExclusive(_270,_276,true);
this.LogSeq("`938`",_26f);
if(_277.length>0){
this.LogSeq("`95`",_26f);
for(var i=0;i<_277.length;i++){
if(_277[i].GetItemIdentifier()==_276.GetItemIdentifier()){
break;
}
this.LogSeq("`216`"+_277[i].LearningObject.ItemIdentifier+")",_26f);
if(_277[i].IsActive()===true&&_277[i].LearningObject.SequencingData.ControlChoiceExit===false){
this.LogSeq("`175`"+CONTROL_CHOICE_EXIT_ERROR_NAV+"`1482`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,CONTROL_CHOICE_EXIT_ERROR_NAV,null,IntegrationImplementation.GetString("You must complete '{0}' before you can select another item.",_277[i]));
this.LogSeqReturn(_273,_26f);
return _273;
}
}
}else{
this.LogSeq("`1525`",_26f);
this.LogSeq("`147`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-9",null,IntegrationImplementation.GetString("Nothing to open"));
this.LogSeqReturn(_273,_26f);
return _273;
}
}
this.LogSeq("`46`",_26f);
if(_270.IsActive()&&_270.GetSequencingControlChoiceExit()===false){
this.LogSeq("`223`"+CONTROL_CHOICE_EXIT_ERROR_NAV+"`1508`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,CONTROL_CHOICE_EXIT_ERROR_NAV,null,IntegrationImplementation.GetString("You are not allowed to jump out of {0}.",_270.GetTitle()));
return _273;
}
this.LogSeq("`203`",_26f);
if(_270.IsActive()){
this.LogSeq("`56`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_CHOICE,null,_274,"");
this.LogSeqReturn(_273,_26f);
return _273;
}else{
this.LogSeq("`1560`",_26f);
this.LogSeq("`60`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,null,SEQUENCING_REQUEST_CHOICE,null,_274,"");
this.LogSeqReturn(_273,_26f);
return _273;
}
}else{
this.LogSeq("`1604`",_26f);
this.LogSeq("`99`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-10",null,IntegrationImplementation.GetString("Please select 'Next' or 'Previous' to move through {0}.",_275.GetTitle()));
this.LogSeqReturn(_273,_26f);
return _273;
}
}else{
this.LogSeq("`1648`",_26f);
this.LogSeq("`86`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-11",null,IntegrationImplementation.GetString("The activity you selected ({0}) does not exist.",_26d));
this.LogSeqReturn(_273,_26f);
return _273;
}
break;
case NAVIGATION_REQUEST_EXIT:
this.LogSeq("`1164`",_26f);
this.LogSeq("`506`",_26f);
if(this.IsCurrentActivityDefined(_26f)){
this.LogSeq("`285`",_26f);
if(_270.IsActive()){
this.LogSeq("`194`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}else{
this.LogSeq("`1603`",_26f);
this.LogSeq("`75`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-12",null,IntegrationImplementation.GetString("The Exit navigation request is invalid because the current activity ({0}) is no longer active.",_270.GetTitle()));
this.LogSeqReturn(_273,_26f);
return _273;
}
}else{
this.LogSeq("`1645`",_26f);
this.LogSeq("`172`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The Exit navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_273,_26f);
return _273;
}
break;
case NAVIGATION_REQUEST_EXIT_ALL:
this.LogSeq("`1087`",_26f);
this.LogSeq("`275`",_26f);
if(this.IsCurrentActivityDefined(_26f)){
this.LogSeq("`193`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,TERMINATION_REQUEST_EXIT_ALL,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}else{
this.LogSeq("`1646`",_26f);
this.LogSeq("`178`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The Exit All navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_273,_26f);
return _273;
}
break;
case NAVIGATION_REQUEST_ABANDON:
this.LogSeq("`1085`",_26f);
this.LogSeq("`505`",_26f);
if(this.IsCurrentActivityDefined(_26f)){
this.LogSeq("`289`",_26f);
if(_270.IsActive()){
this.LogSeq("`182`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,TERMINATION_REQUEST_ABANDON,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}else{
this.LogSeq("`1575`",_26f);
this.LogSeq("`154`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-12",null,IntegrationImplementation.GetString("The 'Abandon' navigation request is invalid because the current activity '{0}' is no longer active.",_270.GetTitle()));
this.LogSeqReturn(_273,_26f);
return _273;
}
}else{
this.LogSeq("`1596`",_26f);
this.LogSeq("`174`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The 'Abandon' navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_273,_26f);
return _273;
}
break;
case NAVIGATION_REQUEST_ABANDON_ALL:
this.LogSeq("`1005`",_26f);
this.LogSeq("`277`",_26f);
if(this.IsCurrentActivityDefined(_26f)){
this.LogSeq("`170`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,TERMINATION_REQUEST_ABANDON_ALL,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}else{
this.LogSeq("`1616`",_26f);
this.LogSeq("`165`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("You cannot use 'Abandon All' if no item is currently open."));
this.LogSeqReturn(_273,_26f);
return _273;
}
break;
case NAVIGATION_REQUEST_SUSPEND_ALL:
this.LogSeq("`995`",_26f);
this.LogSeq("`540`",_26f);
if(this.IsCurrentActivityDefined(_26f)){
this.LogSeq("`166`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(_26c,TERMINATION_REQUEST_SUSPEND_ALL,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_273,_26f);
return _273;
}else{
this.LogSeq("`1628`",_26f);
this.LogSeq("`169`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The 'Suspend All' navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_273,_26f);
return _273;
}
break;
default:
this.LogSeq("`96`",_26f);
_273=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-13",null,IntegrationImplementation.GetString("Undefined Navigation Request"));
this.LogSeqReturn(_273,_26f);
return _273;
}
}
function Sequencer_NavigationRequestProcessResult(_279,_27a,_27b,_27c,_27d,_27e){
this.NavigationRequest=_279;
this.TerminationRequest=_27a;
this.SequencingRequest=_27b;
this.Exception=_27c;
this.TargetActivity=_27d;
this.ExceptionText=_27e;
}
Sequencer_NavigationRequestProcessResult.prototype.toString=function(){
return "NavigationRequest="+this.NavigationRequest+", TerminationRequest="+this.TerminationRequest+", SequencingRequest="+this.SequencingRequest+", Exception="+this.Exception+", TargetActivity="+this.TargetActivity+", ExceptionText="+this.ExceptionText;
};
function Sequencer_ObjectiveRollupProcess(_27f,_280){
Debug.AssertError("Calling log not passed.",(_280===undefined||_280===null));
var _281=this.LogSeqAudit("`1347`"+_27f+")",_280);
this.LogSeq("`1067`",_281);
var _282=null;
this.LogSeq("`627`",_281);
var _282=_27f.GetPrimaryObjective();
this.LogSeq("`1054`",_281);
if(_282!==null){
this.LogSeq("`693`",_281);
if(_282.GetSatisfiedByMeasure()===true){
this.LogSeq("`864`",_281);
this.ObjectiveRollupUsingMeasureProcess(_27f,_281);
this.LogSeq("`1134`",_281);
this.LogSeqReturn("",_281);
return;
}
this.LogSeq("`590`",_281);
if(Sequencer_GetApplicableSetofRollupRules(_27f,RULE_SET_SATISFIED).length>0||Sequencer_GetApplicableSetofRollupRules(_27f,RULE_SET_NOT_SATISFIED).length>0){
this.LogSeq("`879`",_281);
this.ObjectiveRollupUsingRulesProcess(_27f,_281);
this.LogSeq("`1137`",_281);
this.LogSeqReturn("",_281);
return;
}
this.LogSeq("`386`",_281);
this.ObjectiveRollupUsingDefaultProcess(_27f,_281);
this.LogSeq("`1186`",_281);
this.LogSeqReturn("",_281);
return;
}else{
this.LogSeq("`1655`",_281);
this.LogSeq("`479`",_281);
this.LogSeqReturn("",_281);
return;
}
}
function Sequencer_ObjectiveRollupUsingDefaultProcess(_283,_284){
Debug.AssertError("Calling log not passed.",(_284===undefined||_284===null));
var _285=this.LogSeqAudit("`1018`"+_283+")",_284);
var _286;
var _287;
var _288;
this.LogSeq("`1224`",_285);
if(_283.IsALeaf()){
this.LogSeq("`1250`",_285);
this.LogSeqReturn("",_285);
return;
}
this.LogSeq("`1019`",_285);
var _289=null;
var i;
this.LogSeq("`620`",_285);
var _289=_283.GetPrimaryObjective();
this.LogSeq("`1078`",_285);
var _28b=_283.GetChildren();
this.LogSeq("`1041`",_285);
var _28c=true;
this.LogSeq("`1114`",_285);
var _28d=true;
this.LogSeq("`1274`",_285);
var _28e=true;
for(i=0;i<_28b.length;i++){
this.LogSeq("`1343`"+_28b[i]+"`1546`"+_28b[i].IsTracked(),_285);
if(_28b[i].IsTracked()){
this.LogSeq("`1247`"+(i+1)+" - "+_28b[i],_285);
this.LogSeq("`1031`",_285);
_286=_28b[i].IsSatisfied();
this.LogSeq("`1050`",_285);
_287=_28b[i].IsAttempted();
this.LogSeq("`736`",_285);
_288=(_286===false||_287===true);
this.LogSeq("`1533`"+_286+"`1694`"+_287+"`1683`"+_288,_285);
if(this.CheckChildForRollupSubprocess(_28b[i],ROLLUP_RULE_ACTION_SATISFIED,_285)){
this.LogSeq("`887`",_285);
_28d=(_28d&&(_286===true));
_28e=false;
}
if(this.CheckChildForRollupSubprocess(_28b[i],ROLLUP_RULE_ACTION_NOT_SATISFIED,_285)){
this.LogSeq("`777`",_285);
_28c=(_28c&&_288);
_28e=false;
}
}
}
if(_28e&&Control.Package.Properties.RollupEmptySetToUnknown){
this.LogSeq("`537`"+Control.Package.Properties.RollupEmptySetToUnknown+")",_285);
}else{
this.LogSeq("`1001`"+_28c+")",_285);
if(_28c===true){
this.LogSeq("`671`",_285);
_289.SetProgressStatus(true,false,_283);
this.LogSeq("`652`",_285);
_289.SetSatisfiedStatus(false,false,_283);
}
this.LogSeq("`1077`"+_28d+")",_285);
if(_28d===true){
this.LogSeq("`648`",_285);
_289.SetProgressStatus(true,false,_283);
this.LogSeq("`645`",_285);
_289.SetSatisfiedStatus(true,false,_283);
}
}
this.LogSeqReturn("",_285);
}
function Sequencer_ObjectiveRollupUsingMeasureProcess(_28f,_290){
Debug.AssertError("Calling log not passed.",(_290===undefined||_290===null));
var _291=this.LogSeqAudit("`1025`"+_28f+")",_290);
this.LogSeq("`1016`",_291);
var _292=null;
this.LogSeq("`617`",_291);
var _292=_28f.GetPrimaryObjective();
this.LogSeq("`1054`",_291);
if(_292!==null){
this.LogSeq("`128`",_291);
if(_292.GetSatisfiedByMeasure()===true){
this.LogSeq("`302`",_291);
if(_292.GetMeasureStatus(_28f,false)===false){
this.LogSeq("`628`",_291);
_292.SetProgressStatus(false,false,_28f);
}else{
this.LogSeq("`1559`",_291);
this.LogSeq("`130`",_291);
if(_28f.IsActive()===false||_28f.GetMeasureSatisfactionIfActive()===true){
this.LogSeq("`105`",_291);
if(_292.GetNormalizedMeasure(_28f,false)>=_292.GetMinimumSatisfiedNormalizedMeasure()){
this.LogSeq("`613`",_291);
_292.SetProgressStatus(true,false,_28f);
this.LogSeq("`604`",_291);
_292.SetSatisfiedStatus(true,false,_28f);
}else{
this.LogSeq("`1484`",_291);
this.LogSeq("`612`",_291);
_292.SetProgressStatus(true,false,_28f);
this.LogSeq("`598`",_291);
_292.SetSatisfiedStatus(false,false,_28f);
}
}else{
this.LogSeq("`1534`",_291);
this.LogSeq("`270`",_291);
_292.SetProgressStatus(false,false,_28f);
}
}
}
this.LogSeq("`911`",_291);
this.LogSeqReturn("",_291);
return;
}else{
this.LogSeq("`1655`",_291);
this.LogSeq("`391`",_291);
this.LogSeqReturn("",_291);
return;
}
}
function Sequencer_ObjectiveRollupUsingRulesProcess(_293,_294){
Debug.AssertError("Calling log not passed.",(_294===undefined||_294===null));
var _295=this.LogSeqAudit("`1053`"+_293+")",_294);
var _296;
this.LogSeq("`1048`",_295);
var _297=null;
this.LogSeq("`616`",_295);
var _297=_293.GetPrimaryObjective();
this.LogSeq("`1059`",_295);
if(_297!==null){
this.LogSeq("`296`",_295);
_296=this.RollupRuleCheckSubprocess(_293,RULE_SET_NOT_SATISFIED,_295);
this.LogSeq("`805`",_295);
if(_296===true){
this.LogSeq("`656`",_295);
_297.SetProgressStatus(true,false,_293);
this.LogSeq("`636`",_295);
_297.SetSatisfiedStatus(false,false,_293);
}
this.LogSeq("`328`",_295);
_296=this.RollupRuleCheckSubprocess(_293,RULE_SET_SATISFIED,_295);
this.LogSeq("`801`",_295);
if(_296===true){
this.LogSeq("`653`",_295);
_297.SetProgressStatus(true,false,_293);
this.LogSeq("`644`",_295);
_297.SetSatisfiedStatus(true,false,_293);
}
this.LogSeq("`947`",_295);
this.LogSeqReturn("",_295);
return;
}else{
this.LogSeq("`1641`",_295);
this.LogSeq("`432`",_295);
this.LogSeqReturn("",_295);
return;
}
}
function Sequencer_OverallRollupProcess(_298,_299){
Debug.AssertError("Calling log not passed.",(_299===undefined||_299===null));
var _29a=this.LogSeqAudit("`1369`"+_298+")",_299);
this.LogSeq("`256`",_29a);
var _29b=this.GetActivityPath(_298,true);
this.LogSeq("`1115`",_29a);
if(_29b.length===0){
this.LogSeq("`895`",_29a);
this.LogSeqReturn("",_29a);
return;
}
this.LogSeq("`1043`",_29a);
var _29c;
var _29d;
var _29e;
var _29f;
var _2a0;
var _2a1;
var _2a2;
var _2a3;
var _2a4;
var _2a5;
var _2a6;
var _2a7;
var _2a8;
var _2a9=new Array();
var _2aa=false;
for(var i=0;i<_29b.length;i++){
if(!_29b[i].IsALeaf()){
_29b[i].RollupDurations();
}
if(_2aa){
continue;
}
_29c=_29b[i].GetPrimaryObjective();
_29d=_29c.GetMeasureStatus(_29b[i],false);
_29e=_29c.GetNormalizedMeasure(_29b[i],false);
_29f=_29c.GetProgressStatus(_29b[i],false);
_2a0=_29c.GetSatisfiedStatus(_29b[i],false);
_2a1=_29b[i].GetAttemptProgressStatus();
_2a2=_29b[i].GetAttemptCompletionStatus();
this.LogSeq("`553`",_29a);
if(!_29b[i].IsALeaf()){
this.LogSeq("`563`",_29a);
this.MeasureRollupProcess(_29b[i],_29a);
}
this.LogSeq("`728`",_29a);
this.ObjectiveRollupProcess(_29b[i],_29a);
this.LogSeq("`783`",_29a);
this.ActivityProgressRollupProcess(_29b[i],_29a);
_2a3=_29c.GetMeasureStatus(_29b[i],false);
_2a4=_29c.GetNormalizedMeasure(_29b[i],false);
_2a5=_29c.GetProgressStatus(_29b[i],false);
_2a6=_29c.GetSatisfiedStatus(_29b[i],false);
_2a7=_29b[i].GetAttemptProgressStatus();
_2a8=_29b[i].GetAttemptCompletionStatus();
if(!this.LookAhead&&_29b[i].IsTheRoot()){
if(_2a7!=_2a1||_2a8!=_2a2){
var _2ac=(_2a7?(_2a8?SCORM_STATUS_COMPLETED:SCORM_STATUS_INCOMPLETE):SCORM_STATUS_NOT_ATTEMPTED);
this.WriteHistoryLog("",{ev:"Rollup Completion",v:_2ac,ai:_29b[i].ItemIdentifier});
}
if(_2a5!=_29f||_2a6!=_2a0){
var _2ad=(_2a5?(_2a6?SCORM_STATUS_PASSED:SCORM_STATUS_FAILED):SCORM_STATUS_UNKNOWN);
this.WriteHistoryLog("",{ev:"Rollup Satisfaction",v:_2ad,ai:_29b[i].ItemIdentifier});
}
}
if(_2a6!=_2a0){
_29b[i].WasAutoSatisfied=_298.WasAutoSatisfied;
}
if(_2a8!=_2a2){
_29b[i].WasAutoCompleted=_298.WasAutoCompleted;
}
if(i>0&&_2a3==_29d&&_2a4==_29e&&_2a5==_29f&&_2a6==_2a0&&_2a7==_2a1&&_2a8==_2a2){
this.LogSeq("`901`",_29a);
_2aa=true;
}else{
this.LogSeq("`865`",_29a);
_2a9[_2a9.length]=_29b[i];
}
}
this.LogSeq("`1261`",_29a);
this.LogSeqReturn("",_29a);
return _2a9;
}
function Sequencer_OverallSequencingProcess(_2ae){
var _2af=null;
var _2b0=null;
var _2b1=null;
var _2b2=null;
var _2b3=null;
this.ResetException();
var _2b4=this.LogSeqAudit("`1349`");
this.LogSeq("`1399`"+this.NavigationRequest,_2b4);
if(this.NavigationRequest===null){
var _2b5=this.GetExitAction(this.GetCurrentActivity(),_2b4);
this.LogSeq("`1035`"+_2b5,_2b4);
var _2b6="";
if(_2b5==EXIT_ACTION_EXIT_CONFIRMATION||_2b5==EXIT_ACTION_DISPLAY_MESSAGE){
var _2b7=Control.Activities.GetRootActivity();
var _2b8=(_2b7.IsCompleted()||_2b7.IsSatisfied());
if(_2b8===true){
_2b6=IntegrationImplementation.GetString("The course is now complete. Please make a selection to continue.");
}else{
_2b6=IntegrationImplementation.GetString("Please make a selection.");
}
}
switch(_2b5){
case (EXIT_ACTION_EXIT_NO_CONFIRMATION):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
break;
case (EXIT_ACTION_EXIT_CONFIRMATION):
if(confirm("Would you like to exit the course now?")){
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
}else{
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE,null,_2b6);
}
break;
case (EXIT_ACTION_GO_TO_NEXT_SCO):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_CONTINUE,null,"");
break;
case (EXIT_ACTION_DISPLAY_MESSAGE):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE,null,_2b6);
break;
case (EXIT_ACTION_DO_NOTHING):
this.NavigationRequest=null;
break;
case (EXIT_ACTION_REFRESH_PAGE):
Control.RefreshPage();
break;
}
}
if(this.NavigationRequest==null){
this.LogSeqReturn("`1393`",_2b4);
return;
}
if(this.NavigationRequest.Type==NAVIGATION_REQUEST_DISPLAY_MESSAGE){
this.LogSeq("`696`",_2b4);
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT,null,"");
}
if(this.NavigationRequest.Type==NAVIGATION_REQUEST_EXIT_PLAYER){
this.LogSeq("`813`",_2b4);
Control.ExitScormPlayer();
return;
}
this.LogSeq("`763`",_2b4);
var _2b9=this.NavigationRequestProcess(this.NavigationRequest.Type,this.NavigationRequest.TargetActivity,_2b4);
this.LogSeq("`624`",_2b4);
if(_2b9.NavigationRequest==NAVIGATION_REQUEST_NOT_VALID){
this.LogSeq("`731`",_2b4);
this.Exception=_2b9.Exception;
this.ExceptionText=_2b9.ExceptionText;
this.LogSeq("`853`",_2b4);
this.LogSeqReturn("`1728`",_2b4);
return false;
}
_2af=_2b9.TerminationRequest;
_2b0=_2b9.SequencingRequest;
_2b1=_2b9.TargetActivity;
this.LogSeq("`368`",_2b4);
if(_2af!==null){
this.LogSeq("`706`",_2b4);
var _2ba=this.TerminationRequestProcess(_2af,_2b4);
this.LogSeq("`603`",_2b4);
if(_2ba.TerminationRequest==TERMINATION_REQUEST_NOT_VALID){
this.LogSeq("`702`",_2b4);
this.Exception=_2ba.Exception;
this.ExceptionText=_2ba.ExceptionText;
this.LogSeq("`833`",_2b4);
this.LogSeqReturn("`1728`",_2b4);
return false;
}
this.LogSeq("`701`",_2b4);
if(_2ba.SequencingRequest!==null){
this.LogSeq("`39`",_2b4);
_2b0=_2ba.SequencingRequest;
}
}
this.LogSeq("`1060`",_2b4);
if(_2b0!==null){
this.LogSeq("`732`",_2b4);
var _2bb=this.SequencingRequestProcess(_2b0,_2b1,_2b4);
this.LogSeq("`608`",_2b4);
if(_2bb.SequencingRequest==SEQUENCING_REQUEST_NOT_VALID){
this.LogSeq("`716`",_2b4);
this.Exception=_2bb.Exception;
this.ExceptionText=_2bb.ExceptionText;
this.LogSeq("`824`",_2b4);
this.LogSeqReturn("`1728`",_2b4);
return false;
}
this.LogSeq("`535`",_2b4);
if(_2bb.EndSequencingSession===true){
this.LogSeq("`76`",_2b4);
Control.ExitScormPlayer();
this.LogSeqReturn("",_2b4);
return;
}
this.LogSeq("`582`",_2b4);
if(_2bb.DeliveryRequest===null){
this.LogSeq("`826`",_2b4);
this.Exception="OP.1.4";
this.ExceptionText=IntegrationImplementation.GetString("Please make a selection.");
this.LogSeqReturn("",_2b4);
return;
}
this.LogSeq("`574`",_2b4);
_2b2=_2bb.DeliveryRequest;
}
this.LogSeq("`1098`",_2b4);
if(_2b2!==null){
this.LogSeq("`782`",_2b4);
var _2bc=this.DeliveryRequestProcess(_2b2,_2b4);
this.LogSeq("`633`",_2b4);
if(_2bc.Valid===false){
this.LogSeq("`738`",_2b4);
this.Exception="OP.1.5";
this.ExceptionText=IntegrationImplementation.GetString("Please make a selection.");
this.LogSeq("`820`",_2b4);
this.LogSeqReturn("`1728`",_2b4);
return false;
}
this.LogSeq("`657`",_2b4);
this.ContentDeliveryEnvironmentProcess(_2b2,_2b4);
}
this.LogSeq("`944`",_2b4);
this.LogSeqReturn("",_2b4);
return;
}
function Sequencer_PreviousSequencingRequestProcess(_2bd){
Debug.AssertError("Calling log not passed.",(_2bd===undefined||_2bd===null));
var _2be=this.LogSeqAudit("`1129`",_2bd);
var _2bf;
this.LogSeq("`502`",_2be);
if(!this.IsCurrentActivityDefined(_2be)){
this.LogSeq("`407`",_2be);
_2bf=new Sequencer_PreviousSequencingRequestProcessResult(null,"SB.2.8-1",IntegrationImplementation.GetString("You cannot use 'Previous' at this time."),false);
this.LogSeqReturn(_2bf,_2be);
return _2bf;
}
var _2c0=this.GetCurrentActivity();
this.LogSeq("`708`",_2be);
if(!_2c0.IsTheRoot()){
var _2c1=this.Activities.GetParentActivity(_2c0);
this.LogSeq("`298`",_2be);
if(_2c1.GetSequencingControlFlow()===false){
this.LogSeq("`596`",_2be);
_2bf=new Sequencer_PreviousSequencingRequestProcessResult(null,"SB.2.8-2",IntegrationImplementation.GetString("Please select 'Next' or 'Previous' to move through {0}.",_2c1.GetTitle()),false);
this.LogSeqReturn(_2bf,_2be);
return _2bf;
}
}
this.LogSeq("`131`",_2be);
var _2c2=this.FlowSubprocess(_2c0,FLOW_DIRECTION_BACKWARD,false,_2be);
this.LogSeq("`985`",_2be);
if(_2c2.Deliverable===false){
this.LogSeq("`226`",_2be);
_2bf=new Sequencer_PreviousSequencingRequestProcessResult(null,_2c2.Exception,_2c2.ExceptionText,_2c2.EndSequencingSession);
this.LogSeqReturn(_2bf,_2be);
return _2bf;
}else{
this.LogSeq("`1682`",_2be);
this.LogSeq("`323`",_2be);
_2bf=new Sequencer_PreviousSequencingRequestProcessResult(_2c2.IdentifiedActivity,null,"",false);
this.LogSeqReturn(_2bf,_2be);
return _2bf;
}
}
function Sequencer_PreviousSequencingRequestProcessResult(_2c3,_2c4,_2c5,_2c6){
Debug.AssertError("Invalid endSequencingSession ("+_2c6+") passed to PreviousSequencingRequestProcessResult.",(_2c6!=true&&_2c6!=false));
this.DeliveryRequest=_2c3;
this.Exception=_2c4;
this.ExceptionText=_2c5;
this.EndSequencingSession=_2c6;
}
Sequencer_PreviousSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_RandomizeChildrenProcess(_2c7,_2c8,_2c9){
Debug.AssertError("Calling log not passed.",(_2c9===undefined||_2c9===null));
var _2ca=this.LogSeqAudit("`1325`"+_2c7+")",_2c9);
var _2cb;
this.LogSeq("`532`",_2ca);
if(_2c7.IsALeaf()){
this.LogSeq("`1184`",_2ca);
this.LogSeqReturn("",_2ca);
return;
}
this.LogSeq("`153`",_2ca);
if(_2c7.IsActive()===true||_2c7.IsSuspended()===true){
this.LogSeq("`1173`",_2ca);
this.LogSeqReturn("",_2ca);
return;
}
var _2cc=_2c7.GetRandomizationTiming();
switch(_2cc){
case TIMING_NEVER:
this.LogSeq("`845`",_2ca);
this.LogSeq("`1185`",_2ca);
break;
case TIMING_ONCE:
this.LogSeq("`856`",_2ca);
this.LogSeq("`778`",_2ca);
if(_2c7.GetRandomizedChildren()===false){
this.LogSeq("`429`",_2ca);
if(_2c7.GetActivityProgressStatus()===false){
this.LogSeq("`863`",_2ca);
if(_2c7.GetRandomizeChildren()===true){
this.LogSeq("`567`",_2ca);
_2cb=Sequencer_RandomizeArray(_2c7.GetAvailableChildren());
_2c7.SetAvailableChildren(_2cb);
_2c7.SetRandomizedChildren(true);
}
}
}
this.LogSeq("`1188`",_2ca);
break;
case TIMING_ON_EACH_NEW_ATTEMPT:
this.LogSeq("`694`",_2ca);
this.LogSeq("`872`",_2ca);
if(_2c7.GetRandomizeChildren()===true){
this.LogSeq("`581`",_2ca);
_2cb=Sequencer_RandomizeArray(_2c7.GetAvailableChildren());
_2c7.SetAvailableChildren(_2cb);
_2c7.SetRandomizedChildren(true);
if(_2c8===true){
Control.RedrawChildren(_2c7);
}
}
this.LogSeq("`1171`",_2ca);
break;
default:
this.LogSeq("`828`",_2ca);
break;
}
this.LogSeqReturn("",_2ca);
return;
}
function Sequencer_RandomizeArray(ary){
var _2ce=ary.length;
var orig;
var swap;
for(var i=0;i<_2ce;i++){
var _2d2=Math.floor(Math.random()*_2ce);
orig=ary[i];
swap=ary[_2d2];
ary[i]=swap;
ary[_2d2]=orig;
}
return ary;
}
function Sequencer_ResumeAllSequencingRequestProcess(_2d3){
Debug.AssertError("Calling log not passed.",(_2d3===undefined||_2d3===null));
var _2d4=this.LogSeqAudit("`1093`",_2d3);
var _2d5;
this.LogSeq("`494`",_2d4);
if(this.IsCurrentActivityDefined(_2d4)){
this.LogSeq("`401`",_2d4);
_2d5=new Sequencer_ResumeAllSequencingRequestProcessResult(null,"SB.2.6-1",IntegrationImplementation.GetString("A 'Resume All' sequencing request cannot be processed while there is a current activity defined."));
this.LogSeqReturn(_2d5,_2d4);
return _2d5;
}
this.LogSeq("`400`",_2d4);
if(!this.IsSuspendedActivityDefined(_2d4)){
this.LogSeq("`398`",_2d4);
_2d5=new Sequencer_ResumeAllSequencingRequestProcessResult(null,"SB.2.6-2",IntegrationImplementation.GetString("There is no suspended activity to resume."));
this.LogSeqReturn(_2d5,_2d4);
return _2d5;
}
this.LogSeq("`57`",_2d4);
var _2d6=this.GetSuspendedActivity(_2d4);
_2d5=new Sequencer_ResumeAllSequencingRequestProcessResult(_2d6,null,"");
this.LogSeqReturn(_2d5,_2d4);
return _2d5;
}
function Sequencer_ResumeAllSequencingRequestProcessResult(_2d7,_2d8,_2d9){
this.DeliveryRequest=_2d7;
this.Exception=_2d8;
this.ExceptionText=_2d9;
}
Sequencer_ResumeAllSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_RetrySequencingRequestProcess(_2da){
Debug.AssertError("Calling log not passed.",(_2da===undefined||_2da===null));
var _2db=this.LogSeqAudit("`1192`",_2da);
var _2dc;
var _2dd;
this.LogSeq("`489`",_2db);
if(!this.IsCurrentActivityDefined(_2db)){
this.LogSeq("`433`",_2db);
_2dc=new Sequencer_RetrySequencingRequestProcessResult(null,"SB.2.10-1",IntegrationImplementation.GetString("You cannot use 'Resume All' while the current item is open."));
this.LogSeqReturn(_2dc,_2db);
return _2dc;
}
var _2de=this.GetCurrentActivity();
this.LogSeq("`101`",_2db);
if(_2de.IsActive()||_2de.IsSuspended()){
this.LogSeq("`435`",_2db);
_2dc=new Sequencer_RetrySequencingRequestProcessResult(null,"SB.2.10-2",IntegrationImplementation.GetString("A 'Retry' sequencing request cannot be processed while there is an active or suspended activity."));
this.LogSeqReturn(_2dc,_2db);
return _2dc;
}
this.LogSeq("`968`",_2db);
if(!_2de.IsALeaf()){
this.LogSeq("`370`",_2db);
flowSubProcessResult=this.FlowSubprocess(_2de,FLOW_DIRECTION_FORWARD,true,_2db);
this.LogSeq("`942`",_2db);
if(flowSubProcessResult.Deliverable===false){
this.LogSeq("`414`",_2db);
_2dc=new Sequencer_RetrySequencingRequestProcessResult(null,"SB.2.10-3",IntegrationImplementation.GetString("You cannot 'Retry' this item because: {1}",_2de.GetTitle(),flowSubProcessResult.ExceptionText));
this.LogSeqReturn(_2dc,_2db);
return _2dc;
}else{
this.LogSeq("`1636`",_2db);
this.LogSeq("`319`",_2db);
_2dc=new Sequencer_RetrySequencingRequestProcessResult(flowSubProcessResult.IdentifiedActivity,null,"");
this.LogSeqReturn(_2dc,_2db);
return _2dc;
}
}else{
this.LogSeq("`1667`",_2db);
this.LogSeq("`484`",_2db);
_2dc=new Sequencer_RetrySequencingRequestProcessResult(_2de,null,"");
this.LogSeqReturn(_2dc,_2db);
return _2dc;
}
}
function Sequencer_RetrySequencingRequestProcessResult(_2df,_2e0,_2e1){
this.DeliveryRequest=_2df;
this.Exception=_2e0;
this.ExceptionText=_2e1;
}
Sequencer_RetrySequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_RollupRuleCheckSubprocess(_2e2,_2e3,_2e4){
Debug.AssertError("Calling log not passed.",(_2e4===undefined||_2e4===null));
var _2e5=this.LogSeqAudit("`1255`"+_2e2+", "+_2e3+")",_2e4);
var _2e6;
var _2e7;
var _2e8;
var _2e9;
var _2ea=0;
var _2eb=0;
var _2ec=0;
this.LogSeq("`329`",_2e5);
var _2ed=Sequencer_GetApplicableSetofRollupRules(_2e2,_2e3);
if(_2ed.length>0){
this.LogSeq("`214`",_2e5);
_2e6=_2e2.GetChildren();
this.LogSeq("`1124`",_2e5);
for(var i=0;i<_2ed.length;i++){
this.LogSeq("`745`",_2e5);
contributingChldren=new Array();
_2ea=0;
_2eb=0;
_2ec=0;
this.LogSeq("`1111`",_2e5);
for(var j=0;j<_2e6.length;j++){
this.LogSeq("`974`",_2e5);
if(_2e6[j].IsTracked()===true){
this.LogSeq("`237`",_2e5);
_2e7=this.CheckChildForRollupSubprocess(_2e6[j],_2ed[i].Action,_2e5);
this.LogSeq("`744`",_2e5);
if(_2e7===true){
this.LogSeq("`156`",_2e5);
_2e8=this.EvaluateRollupConditionsSubprocess(_2e6[j],_2ed[i],_2e5);
this.LogSeq("`310`",_2e5);
if(_2e8==RESULT_UNKNOWN){
this.LogSeq("`719`",_2e5);
_2ec++;
if(_2ed[i].ChildActivitySet==CHILD_ACTIVITY_SET_ALL||_2ed[i].ChildActivitySet==CHILD_ACTIVITY_SET_NONE){
j=_2e6.length;
}
}else{
this.LogSeq("`1495`",_2e5);
this.LogSeq("`686`",_2e5);
if(_2e8===true){
this.LogSeq("`766`",_2e5);
_2ea++;
if(_2ed[i].ChildActivitySet==CHILD_ACTIVITY_SET_ANY||_2ed[i].ChildActivitySet==CHILD_ACTIVITY_SET_NONE){
j=_2e6.length;
}
}else{
this.LogSeq("`1494`",_2e5);
this.LogSeq("`754`",_2e5);
_2eb++;
if(_2ed[i].ChildActivitySet==CHILD_ACTIVITY_SET_ALL){
j=_2e6.length;
}
}
}
}
}
}
switch(_2ed[i].ChildActivitySet){
case CHILD_ACTIVITY_SET_ALL:
this.LogSeq("`927`",_2e5);
this.LogSeq("`541`",_2e5);
if(_2eb===0&&_2ec===0){
this.LogSeq("`1149`",_2e5);
_2e9=true;
}
break;
case CHILD_ACTIVITY_SET_ANY:
this.LogSeq("`931`",_2e5);
this.LogSeq("`689`",_2e5);
if(_2ea>0){
this.LogSeq("`1142`",_2e5);
_2e9=true;
}
break;
case CHILD_ACTIVITY_SET_NONE:
this.LogSeq("`914`",_2e5);
this.LogSeq("`546`",_2e5);
if(_2ea===0&&_2ec===0){
this.LogSeq("`1123`",_2e5);
_2e9=true;
}
break;
case CHILD_ACTIVITY_SET_AT_LEAST_COUNT:
this.LogSeq("`814`",_2e5);
this.LogSeq("`271`",_2e5);
if(_2ea>=_2ed[i].MinimumCount){
this.LogSeq("`1138`",_2e5);
_2e9=true;
}
break;
case CHILD_ACTIVITY_SET_AT_LEAST_PERCENT:
this.LogSeq("`786`",_2e5);
this.LogSeq("`117`",_2e5);
var _2f0=(_2ea/(_2ea+_2eb+_2ec));
if(_2f0>=_2ed[i].MinimumPercent){
this.LogSeq("`1122`",_2e5);
_2e9=true;
}
break;
default:
break;
}
this.LogSeq("`1144`",_2e5);
if(_2e9===true){
this.LogSeq("`267`",_2e5);
this.LogSeqReturn("`1732`",_2e5);
return true;
}
}
}
this.LogSeq("`424`",_2e5);
this.LogSeqReturn("`1728`",_2e5);
return false;
}
function Sequencer_GetApplicableSetofRollupRules(_2f1,_2f2){
var _2f3=new Array();
var _2f4=_2f1.GetRollupRules();
for(var i=0;i<_2f4.length;i++){
switch(_2f2){
case RULE_SET_SATISFIED:
if(_2f4[i].Action==ROLLUP_RULE_ACTION_SATISFIED){
_2f3[_2f3.length]=_2f4[i];
}
break;
case RULE_SET_NOT_SATISFIED:
if(_2f4[i].Action==ROLLUP_RULE_ACTION_NOT_SATISFIED){
_2f3[_2f3.length]=_2f4[i];
}
break;
case RULE_SET_COMPLETED:
if(_2f4[i].Action==ROLLUP_RULE_ACTION_COMPLETED){
_2f3[_2f3.length]=_2f4[i];
}
break;
case RULE_SET_INCOMPLETE:
if(_2f4[i].Action==ROLLUP_RULE_ACTION_INCOMPLETE){
_2f3[_2f3.length]=_2f4[i];
}
break;
}
}
return _2f3;
}
function Sequencer_SelectChildrenProcess(_2f6,_2f7){
Debug.AssertError("Calling log not passed.",(_2f7===undefined||_2f7===null));
var _2f8=this.LogSeqAudit("`1377`"+_2f6+")",_2f7);
this.LogSeq("`561`",_2f8);
if(_2f6.IsALeaf()){
this.LogSeq("`1239`",_2f8);
this.LogSeqReturn("",_2f8);
return;
}
this.LogSeq("`171`",_2f8);
if(_2f6.IsActive()===true||_2f6.IsSuspended()===true){
this.LogSeq("`1253`",_2f8);
this.LogSeqReturn("",_2f8);
return;
}
var _2f9=_2f6.GetSelectionTiming();
switch(_2f9){
case TIMING_NEVER:
this.LogSeq("`886`",_2f8);
this.LogSeq("`1246`",_2f8);
break;
case TIMING_ONCE:
this.LogSeq("`893`",_2f8);
this.LogSeq("`811`",_2f8);
if(_2f6.GetSelectedChildren()===false){
this.LogSeq("`438`",_2f8);
if(_2f6.GetActivityProgressStatus()===false){
this.LogSeq("`768`",_2f8);
if(_2f6.GetSelectionCountStatus()===true){
this.LogSeq("`875`",_2f8);
var _2fa=new Array();
var _2fb=_2f6.GetChildren();
var _2fc=_2f6.GetSelectionCount();
if(_2fc<_2fb.length){
var _2fd=Sequencer_GetUniqueRandomNumbersBetweenTwoValues(_2fc,0,_2fb.length-1);
this.LogSeq("`867`",_2f8);
for(var i=0;i<_2fd.length;i++){
this.LogSeq("`534`",_2f8);
this.LogSeq("`339`",_2f8);
_2fa[i]=_2fb[_2fd[i]];
}
}else{
_2fa=_2fb;
}
this.LogSeq("`769`",_2f8);
_2f6.SetAvailableChildren(_2fa);
_2f6.SetSelectedChildren(true);
}
}
}
this.LogSeq("`1240`",_2f8);
break;
case TIMING_ON_EACH_NEW_ATTEMPT:
this.LogSeq("`735`",_2f8);
this.LogSeq("`894`",_2f8);
break;
default:
this.LogSeq("`836`",_2f8);
break;
}
this.LogSeqReturn("",_2f8);
}
function Sequencer_GetUniqueRandomNumbersBetweenTwoValues(_2ff,_300,_301){
if(_2ff===null||_2ff===undefined||_2ff<_300){
_2ff=_300;
}
if(_2ff>_301){
_2ff=_301;
}
var _302=new Array(_2ff);
var _303;
var _304;
for(var i=0;i<_2ff;i++){
_304=true;
while(_304){
_303=Sequencer_GetRandomNumberWithinRange(_300,_301);
_304=Sequencer_IsNumberAlreadyInArray(_303,_302);
}
_302[i]=_303;
}
_302.sort();
return _302;
}
function Sequencer_GetRandomNumberWithinRange(_306,_307){
var diff=_307-_306;
return Math.floor(Math.random()*(diff+_306+1));
}
function Sequencer_IsNumberAlreadyInArray(_309,_30a){
for(var i=0;i<_30a.length;i++){
if(_30a[i]==_309){
return true;
}
}
return false;
}
function Sequencer_SequencingExitActionRulesSubprocess(_30c){
Debug.AssertError("Calling log not passed.",(_30c===undefined||_30c===null));
var _30d=this.LogSeqAudit("`1061`",_30c);
this.LogSeq("`248`",_30d);
var _30e=this.GetCurrentActivity();
var _30f=this.Activities.GetParentActivity(_30e);
var _310;
if(_30f!==null){
_310=this.GetActivityPath(_30f,true);
}else{
_310=this.GetActivityPath(_30e,true);
}
this.LogSeq("`1217`",_30d);
var _311=null;
var _312=null;
this.LogSeq("`306`",_30d);
for(var i=(_310.length-1);i>=0;i--){
this.LogSeq("`549`",_30d);
_312=this.SequencingRulesCheckProcess(_310[i],RULE_SET_EXIT,_30d);
this.LogSeq("`734`",_30d);
if(_312!==null){
this.LogSeq("`413`",_30d);
_311=_310[i];
this.LogSeq("`1501`",_30d);
break;
}
}
this.LogSeq("`1187`",_30d);
if(_311!==null){
this.LogSeq("`353`",_30d);
this.TerminateDescendentAttemptsProcess(_311,_30d);
this.LogSeq("`464`",_30d);
this.EndAttemptProcess(_311,false,_30d);
this.LogSeq("`344`",_30d);
this.SetCurrentActivity(_311,_30d);
}
this.LogSeq("`957`",_30d);
this.LogSeqReturn("",_30d);
return;
}
function Sequencer_SequencingPostConditionRulesSubprocess(_314){
Debug.AssertError("Calling log not passed.",(_314===undefined||_314===null));
var _315=this.LogSeqAudit("`1006`",_314);
var _316;
this.LogSeq("`336`",_315);
var _317=this.GetCurrentActivity();
if(_317.IsSuspended()){
this.LogSeq("`888`",_315);
_316=new Sequencer_SequencingPostConditionRulesSubprocessResult(null,null);
this.LogSeqReturn(_316,_315);
return _316;
}
this.LogSeq("`191`",_315);
var _318=this.SequencingRulesCheckProcess(_317,RULE_SET_POST_CONDITION,_315);
this.LogSeq("`764`",_315);
if(_318!==null){
this.LogSeq("`588`",_315);
if(_318==SEQUENCING_RULE_ACTION_RETRY||_318==SEQUENCING_RULE_ACTION_CONTINUE||_318==SEQUENCING_RULE_ACTION_PREVIOUS){
this.LogSeq("`47`",_315);
_316=new Sequencer_SequencingPostConditionRulesSubprocessResult(this.TranslateSequencingRuleActionIntoSequencingRequest(_318),null);
this.LogSeqReturn(_316,_315);
return _316;
}
this.LogSeq("`621`",_315);
if(_318==SEQUENCING_RULE_ACTION_EXIT_PARENT||_318==SEQUENCING_RULE_ACTION_EXIT_ALL){
this.LogSeq("`83`",_315);
_316=new Sequencer_SequencingPostConditionRulesSubprocessResult(null,this.TranslateSequencingRuleActionIntoTerminationRequest(_318));
this.LogSeqReturn(_316,_315);
return _316;
}
this.LogSeq("`750`",_315);
if(_318==SEQUENCING_RULE_ACTION_RETRY_ALL){
this.LogSeq("`27`",_315);
_316=new Sequencer_SequencingPostConditionRulesSubprocessResult(SEQUENCING_REQUEST_RETRY,TERMINATION_REQUEST_EXIT_ALL);
this.LogSeqReturn(_316,_315);
return _316;
}
}
this.LogSeq("`481`",_315);
_316=new Sequencer_SequencingPostConditionRulesSubprocessResult(null,null);
this.LogSeqReturn(_316,_315);
return _316;
}
function Sequencer_SequencingPostConditionRulesSubprocessResult(_319,_31a){
this.TerminationRequest=_31a;
this.SequencingRequest=_319;
}
Sequencer_SequencingPostConditionRulesSubprocessResult.prototype.toString=function(){
return "TerminationRequest="+this.TerminationRequest+", SequencingRequest="+this.SequencingRequest;
};
function Sequencer_SequencingRequestProcess(_31b,_31c,_31d){
Debug.AssertError("Calling log not passed.",(_31d===undefined||_31d===null));
var _31e=this.LogSeqAudit("`1287`"+_31b+", "+_31c+")",_31d);
var _31f;
switch(_31b){
case SEQUENCING_REQUEST_START:
this.LogSeq("`1112`",_31e);
this.LogSeq("`946`",_31e);
var _320=this.StartSequencingRequestProcess(_31e);
this.LogSeq("`692`",_31e);
if(_320.Exception!==null){
this.LogSeq("`81`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_320.Exception,_320.ExceptionText,false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}else{
this.LogSeq("`1631`",_31e);
this.LogSeq("`121`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(_31b,_320.DeliveryRequest,_320.EndSequencingSession,null,"",false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}
break;
case SEQUENCING_REQUEST_RESUME_ALL:
this.LogSeq("`1020`",_31e);
this.LogSeq("`878`",_31e);
var _321=this.ResumeAllSequencingRequestProcess(_31e);
this.LogSeq("`643`",_31e);
if(_321.Exception!==null){
this.LogSeq("`71`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_321.Exception,_321.ExceptionText,false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}else{
this.LogSeq("`1625`",_31e);
this.LogSeq("`109`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(_31b,_321.DeliveryRequest,null,null,"",false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}
break;
case SEQUENCING_REQUEST_EXIT:
this.LogSeq("`1148`",_31e);
this.LogSeq("`955`",_31e);
var _322=this.ExitSequencingRequestProcess(_31e);
this.LogSeq("`704`",_31e);
if(_322.Exception!==null){
this.LogSeq("`85`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_322.Exception,_322.ExceptionText,false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}else{
this.LogSeq("`1626`",_31e);
this.LogSeq("`124`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(_31b,null,_322.EndSequencingSession,null,"",false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}
break;
case SEQUENCING_REQUEST_RETRY:
this.LogSeq("`1118`",_31e);
this.LogSeq("`949`",_31e);
var _323=this.RetrySequencingRequestProcess(_31e);
if(_323.Exception!==null){
this.LogSeq("`80`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_323.Exception,_323.ExceptionText,false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}else{
this.LogSeq("`1619`",_31e);
this.LogSeq("`114`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(_31b,_323.DeliveryRequest,null,null,"",false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}
break;
case SEQUENCING_REQUEST_CONTINUE:
this.LogSeq("`1068`",_31e);
this.LogSeq("`898`",_31e);
var _324=this.ContinueSequencingRequestProcess(_31e);
this.LogSeq("`661`",_31e);
if(_324.Exception!==null){
this.LogSeq("`28`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,_324.EndSequencingSession,_324.Exception,_324.ExceptionText,false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}else{
this.LogSeq("`1615`",_31e);
this.LogSeq("`112`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(_31b,_324.DeliveryRequest,_324.EndSequencingSession,null,"",false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}
break;
case SEQUENCING_REQUEST_PREVIOUS:
this.LogSeq("`1063`",_31e);
this.LogSeq("`900`",_31e);
var _325=this.PreviousSequencingRequestProcess(_31e);
this.LogSeq("`666`",_31e);
if(_325.Exception!==null){
this.LogSeq("`74`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,_325.EndSequencingSession,_325.Exception,_325.ExceptionText,false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}else{
this.LogSeq("`1637`",_31e);
this.LogSeq("`111`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(_31b,_325.DeliveryRequest,_325.EndSequencingSession,null,"",false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}
break;
case SEQUENCING_REQUEST_CHOICE:
this.LogSeq("`1105`",_31e);
this.LogSeq("`934`",_31e);
var _326=this.ChoiceSequencingRequestProcess(_31c,_31e);
this.LogSeq("`679`",_31e);
if(_326.Exception!==null){
this.LogSeq("`78`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_326.Exception,_326.ExceptionText,_326.Hidden);
this.LogSeqReturn(_31f,_31e);
return _31f;
}else{
this.LogSeq("`1630`",_31e);
this.LogSeq("`119`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(_31b,_326.DeliveryRequest,null,null,"",_326.Hidden);
this.LogSeqReturn(_31f,_31e);
return _31f;
}
break;
}
this.LogSeq("`150`",_31e);
_31f=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,"SB.2.12-1","The sequencing request ("+_31b+") is not recognized.",false);
this.LogSeqReturn(_31f,_31e);
return _31f;
}
function Sequencer_SequencingRequestProcessResult(_327,_328,_329,_32a,_32b,_32c){
Debug.AssertError("undefined hidden value",(_32c===undefined));
Debug.AssertError("Invalid endSequencingSession ("+_329+") passed to SequencingRequestProcessResult.",(_329!=null&&_329!=true&&_329!=false));
this.SequencingRequest=_327;
this.DeliveryRequest=_328;
this.EndSequencingSession=_329;
this.Exception=_32a;
this.ExceptionText=_32b;
this.Hidden=_32c;
}
Sequencer_SequencingRequestProcessResult.prototype.toString=function(){
return "SequencingRequest="+this.SequencingRequest+", DeliveryRequest="+this.DeliveryRequest+", EndSequencingSession="+this.EndSequencingSession+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", Hidden="+this.Hidden;
};
function Sequencer_SequencingRulesCheckProcess(_32d,_32e,_32f){
Debug.AssertError("Calling log not passed.",(_32f===undefined||_32f===null));
var _330=this.LogSeqAudit("`1268`"+_32d+", "+_32e+")",_32f);
var _331;
var _332=Sequencer_GetApplicableSetofSequencingRules(_32d,_32e);
this.LogSeq("`301`",_330);
if(_332.length>0){
this.LogSeq("`190`",_330);
this.LogSeq("`1189`",_330);
for(var i=0;i<_332.length;i++){
this.LogSeq("`415`",_330);
_331=this.SequencingRulesCheckSubprocess(_32d,_332[i],_330);
this.LogSeq("`791`",_330);
if(_331===true){
this.LogSeq("`211`",_330);
this.LogSeqReturn(_332[i].Action,_330);
return _332[i].Action;
}
}
}
this.LogSeq("`457`",_330);
this.LogSeqReturn("`1731`",_330);
return null;
}
function Sequencer_GetApplicableSetofSequencingRules(_334,_335){
var _336=new Array();
if(_335==RULE_SET_POST_CONDITION){
_336=_334.GetPostConditionRules();
}else{
if(_335==RULE_SET_EXIT){
_336=_334.GetExitRules();
}else{
var _337=_334.GetPreConditionRules();
for(var i=0;i<_337.length;i++){
switch(_335){
case RULE_SET_HIDE_FROM_CHOICE:
if(_337[i].Action==SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE){
_336[_336.length]=_337[i];
}
break;
case RULE_SET_STOP_FORWARD_TRAVERSAL:
if(_337[i].Action==SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL){
_336[_336.length]=_337[i];
}
break;
case RULE_SET_DISABLED:
if(_337[i].Action==SEQUENCING_RULE_ACTION_DISABLED){
_336[_336.length]=_337[i];
}
break;
case RULE_SET_SKIPPED:
if(_337[i].Action==SEQUENCING_RULE_ACTION_SKIP){
_336[_336.length]=_337[i];
}
break;
default:
Debug.AssertError("ERROR - invalid sequencing rule set - "+_335);
return null;
}
}
}
}
return _336;
}
function Sequencer_SequencingRulesCheckSubprocess(_339,rule,_33b){
Debug.AssertError("Calling log not passed.",(_33b===undefined||_33b===null));
var _33c=this.LogSeqAudit("`1166`"+_339+", "+rule+")",_33b);
this.LogSeq("`324`",_33c);
var _33d=new Array();
var _33e;
var _33f;
var i;
this.LogSeq("`733`",_33c);
for(i=0;i<rule.RuleConditions.length;i++){
this.LogSeq("`100`",_33c);
_33e=this.EvaluateSequencingRuleCondition(_339,rule.RuleConditions[i],_33c);
this.LogSeq("`697`",_33c);
if(rule.RuleConditions[i].Operator==RULE_CONDITION_OPERATOR_NOT){
this.LogSeq("`658`",_33c);
if(_33e!="unknown"){
_33e=(!_33e);
}
}
this.LogSeq("`284`",_33c);
_33d[_33d.length]=_33e;
}
this.LogSeq("`372`",_33c);
if(_33d.length===0){
this.LogSeq("`611`",_33c);
this.LogSeqReturn(RESULT_UNKNOWN,_33c);
return RESULT_UNKNOWN;
}
this.LogSeq("`62`",_33c);
if(rule.ConditionCombination==RULE_CONDITION_COMBINATION_ANY){
_33f=false;
for(i=0;i<_33d.length;i++){
_33f=Sequencer_LogicalOR(_33f,_33d[i]);
}
}else{
_33f=true;
for(i=0;i<_33d.length;i++){
_33f=Sequencer_LogicalAND(_33f,_33d[i]);
}
}
this.LogSeq("`618`",_33c);
this.LogSeqReturn(_33f,_33c);
return _33f;
}
function Sequencer_EvaluateSequencingRuleCondition(_341,_342,_343){
Debug.AssertError("Calling log not passed.",(_343===undefined||_343===null));
var _344=this.LogSeqAudit("`1305`"+_341+", "+_342+")",_343);
var _345=null;
switch(_342.Condition){
case SEQUENCING_RULE_CONDITION_SATISFIED:
_345=_341.IsSatisfied(_342.ReferencedObjective,true);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN:
_345=_341.IsObjectiveStatusKnown(_342.ReferencedObjective,true);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN:
_345=_341.IsObjectiveMeasureKnown(_342.ReferencedObjective,true);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_GREATER_THAN:
_345=_341.IsObjectiveMeasureGreaterThan(_342.ReferencedObjective,_342.MeasureThreshold,true);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_LESS_THAN:
_345=_341.IsObjectiveMeasureLessThan(_342.ReferencedObjective,_342.MeasureThreshold,true);
break;
case SEQUENCING_RULE_CONDITION_COMPLETED:
_345=_341.IsCompleted(_342.ReferencedObjective,true);
break;
case SEQUENCING_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN:
_345=_341.IsActivityProgressKnown(_342.ReferencedObjective,true);
break;
case SEQUENCING_RULE_CONDITION_ATTEMPTED:
_345=_341.IsAttempted();
break;
case SEQUENCING_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED:
_345=_341.IsAttemptLimitExceeded();
break;
case SEQUENCING_RULE_CONDITION_ALWAYS:
_345=true;
break;
default:
Debug.AssertError("ERROR - Encountered unsupported rule condition - "+_342);
_345=RESULT_UNKNOWN;
break;
}
_344.setReturn(_345+"",_344);
return _345;
}
function Sequencer_LogicalOR(_346,_347){
if(_346==RESULT_UNKNOWN){
if(_347===true){
return true;
}else{
return RESULT_UNKNOWN;
}
}else{
if(_347==RESULT_UNKNOWN){
if(_346===true){
return true;
}else{
return RESULT_UNKNOWN;
}
}else{
return (_346||_347);
}
}
}
function Sequencer_LogicalAND(_348,_349){
if(_348==RESULT_UNKNOWN){
if(_349===false){
return false;
}else{
return RESULT_UNKNOWN;
}
}else{
if(_349==RESULT_UNKNOWN){
if(_348===false){
return false;
}else{
return RESULT_UNKNOWN;
}
}else{
return (_348&&_349);
}
}
}
function Sequencer_StartSequencingRequestProcess(_34a){
Debug.AssertError("Calling log not passed.",(_34a===undefined||_34a===null));
var _34b=this.LogSeqAudit("`1208`",_34a);
var _34c;
this.LogSeq("`500`",_34b);
if(this.IsCurrentActivityDefined(_34b)){
this.LogSeq("`456`",_34b);
_34c=new Sequencer_StartSequencingRequestProcessResult(null,"SB.2.5-1",IntegrationImplementation.GetString("You cannot 'Start' an item that is already open."),false);
this.LogSeqReturn(_34c,_34b);
return _34c;
}
var _34d=this.GetRootActivity(_34b);
this.LogSeq("`317`",_34b);
if(_34d.IsALeaf()){
this.LogSeq("`239`",_34b);
_34c=new Sequencer_StartSequencingRequestProcessResult(_34d,null,"",false);
this.LogSeqReturn(_34c,_34b);
return _34c;
}else{
if(Control.Package.Properties.AlwaysFlowToFirstSco===true){
this.LogSeq("`315`",_34b);
this.LogSeq("`1033`",_34b);
var _34e=this.GetOrderedListOfActivities(_34b);
this.LogSeq("`922`",_34b);
for(var _34f in _34e){
if(_34e[_34f].IsDeliverable()===true){
_34c=new Sequencer_StartSequencingRequestProcessResult(_34e[_34f],null,"",false);
this.LogSeqReturn(_34c,_34b);
return _34c;
}
}
_34c=new Sequencer_StartSequencingRequestProcessResult(null,"SB.2.5-2.5","There are no deliverable activities in this course.",false);
this.LogSeqReturn(_34c,_34b);
return _34c;
}else{
this.LogSeq("`1668`",_34b);
this.LogSeq("`163`",_34b);
var _350=this.FlowSubprocess(_34d,FLOW_DIRECTION_FORWARD,true,_34b);
this.LogSeq("`963`",_34b);
if(_350.Deliverable===false){
this.LogSeq("`232`",_34b);
_34c=new Sequencer_StartSequencingRequestProcessResult(null,_350.Exception,_350.ExceptionText,_350.EndSequencingSession);
this.LogSeqReturn(_34c,_34b);
return _34c;
}else{
this.LogSeq("`1623`",_34b);
this.LogSeq("`325`",_34b);
_34c=new Sequencer_StartSequencingRequestProcessResult(_350.IdentifiedActivity,null,"",false);
this.LogSeqReturn(_34c,_34b);
return _34c;
}
}
}
}
function Sequencer_StartSequencingRequestProcessResult(_351,_352,_353,_354){
Debug.AssertError("Invalid endSequencingSession ("+_354+") passed to StartSequencingRequestProcessResult.",(_354!=true&&_354!=false));
this.DeliveryRequest=_351;
this.Exception=_352;
this.ExceptionText=_353;
this.EndSequencingSession=_354;
}
Sequencer_StartSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_TerminateDescendentAttemptsProcess(_355,_356){
Debug.AssertError("Calling log not passed.",(_356===undefined||_356===null));
var _357=this.LogSeqAudit("`1106`"+_355+")",_356);
this.LogSeq("`682`"+this.GetCurrentActivity()+"`1376`"+_355+")",_357);
var _358=this.FindCommonAncestor(_355,this.GetCurrentActivity(),_357);
this.LogSeq("`149`"+_358+"`972`",_357);
var _359=this.GetPathToAncestorExclusive(this.GetCurrentActivity(),_358,false);
var _35a=new Array();
this.LogSeq("`520`",_357);
if(_359.length>0){
this.LogSeq("`1047`",_357);
for(var i=0;i<_359.length;i++){
this.LogSeq("`471`"+_359[i].LearningObject.ItemIdentifier,_357);
_35a=_35a.concat(this.EndAttemptProcess(_359[i],true,_357));
}
}
this.LogSeq("`729`",_357);
var _35c=this.GetMinimalSubsetOfActivitiesToRollup(_35a,null);
for(var _35d in _35c){
this.OverallRollupProcess(_35c[_35d],_357);
}
this.LogSeq("`1012`",_357);
this.LogSeqReturn("",_357);
return;
}
function Sequencer_TerminationRequestProcess(_35e,_35f){
Debug.AssertError("Calling log not passed.",(_35f===undefined||_35f===null));
var _360=this.LogSeqAudit("`1284`"+_35e+")",_35f);
var _361;
var _362=this.GetCurrentActivity();
var _363=this.Activities.GetParentActivity(_362);
var _364=this.GetRootActivity(_360);
var _365;
var _366=null;
var _367=false;
this.LogSeq("`366`",_360);
if(!this.IsCurrentActivityDefined(_360)){
this.LogSeq("`380`",_360);
_361=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-1",IntegrationImplementation.GetString("You cannot use 'Terminate' because no item is currently open."));
this.LogSeqReturn(_361,_360);
return _361;
}
this.LogSeq("`88`",_360);
if((_35e==TERMINATION_REQUEST_EXIT||_35e==TERMINATION_REQUEST_ABANDON)&&(_362.IsActive()===false)){
this.LogSeq("`381`",_360);
_361=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-2",IntegrationImplementation.GetString("The current activity has already been terminated."));
this.LogSeqReturn(_361,_360);
return _361;
}
switch(_35e){
case TERMINATION_REQUEST_EXIT:
this.LogSeq("`1125`",_360);
this.LogSeq("`383`",_360);
this.EndAttemptProcess(_362,false,_360);
this.LogSeq("`240`",_360);
this.SequencingExitActionRulesSubprocess(_360);
this.LogSeq("`1612`",_360);
var _368;
do{
this.LogSeq("`1104`",_360);
_368=false;
this.LogSeq("`586`"+this.GetCurrentActivity()+")",_360);
_366=this.SequencingPostConditionRulesSubprocess(_360);
this.LogSeq("`470`",_360);
if(_366.TerminationRequest==TERMINATION_REQUEST_EXIT_ALL){
this.LogSeq("`905`",_360);
_35e=TERMINATION_REQUEST_EXIT_ALL;
this.LogSeq("`673`",_360);
_367=true;
break;
}
this.LogSeq("`53`",_360);
if(_366.TerminationRequest==TERMINATION_REQUEST_EXIT_PARENT){
this.LogSeq("`283`",_360);
if(this.GetCurrentActivity()!==null&&this.GetCurrentActivity().IsTheRoot()===false){
this.LogSeq("`674`",_360);
this.SetCurrentActivity(this.Activities.GetParentActivity(this.GetCurrentActivity()),_360);
this.LogSeq("`774`",_360);
this.EndAttemptProcess(this.GetCurrentActivity(),false,_360);
this.LogSeq("`492`",_360);
_368=true;
}else{
this.LogSeq("`1565`",_360);
this.LogSeq("`355`",_360);
_361=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-4",IntegrationImplementation.GetString("An 'Exit Parent' sequencing request cannot be processed on the root of the activity tree."));
this.LogSeqReturn(_361,_360);
return _361;
}
}else{
this.LogSeq("`1608`",_360);
this.LogSeq("`8`",_360);
if(this.GetCurrentActivity()!==null&&this.GetCurrentActivity().IsTheRoot()===true&&_366.SequencingRequest!=SEQUENCING_REQUEST_RETRY){
this.LogSeq("`396`",_360);
_361=new Sequencer_TerminationRequestProcessResult(_35e,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_361,_360);
return _361;
}
}
this.LogSeq("`909`"+_368+")",_360);
}while(_368!==false);
if(!_367){
this.LogSeq("`54`",_360);
_361=new Sequencer_TerminationRequestProcessResult(_35e,_366.SequencingRequest,null,"");
this.LogSeqReturn(_361,_360);
return _361;
break;
}
case TERMINATION_REQUEST_EXIT_ALL:
this.LogSeq("`1065`",_360);
this.LogSeq("`236`",_360);
if(_362.IsActive()){
this.LogSeq("`818`",_360);
this.EndAttemptProcess(_362,false,_360);
}
this.LogSeq("`587`",_360);
this.TerminateDescendentAttemptsProcess(_364,_360);
this.LogSeq("`725`",_360);
this.EndAttemptProcess(_364,false,_360);
this.LogSeq("`350`",_360);
this.SetCurrentActivity(_364,_360);
this.LogSeq("`3`",_360);
if(_366!==null&&_366.SequencingRequest!==null){
this.LogSeq("`445`",_360);
_361=new Sequencer_TerminationRequestProcessResult(_35e,_366.SequencingRequest,null,"");
this.LogSeqReturn(_361,_360);
return _361;
}else{
this.LogSeq("`1027`",_360);
_361=new Sequencer_TerminationRequestProcessResult(_35e,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_361,_360);
return _361;
}
break;
case TERMINATION_REQUEST_SUSPEND_ALL:
this.LogSeq("`991`",_360);
this.LogSeq("`44`",_360);
if((Control.Package.Properties.InvokeRollupAtSuspendAll===true||this.ReturnToLmsInvoked)&&_362.IsActive()){
this.LogSeq("`507`",_360);
this.EndAttemptProcess(_362,false,_360,true);
}
if(_362.IsActive()||_362.IsSuspended()){
this.LogSeq("`1086`",_360);
this.OverallRollupProcess(_362,_360);
this.LogSeq("`843`",_360);
this.SetSuspendedActivity(_362);
}else{
this.LogSeq("`1651`",_360);
this.LogSeq("`258`",_360);
if(!_362.IsTheRoot()){
this.LogSeq("`670`",_360);
this.SetSuspendedActivity(_363);
}else{
this.LogSeq("`1614`",_360);
this.LogSeq("`260`",_360);
Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-3","The suspend all termination request failed because there is no activity to suspend");
}
}
this.LogSeq("`274`",_360);
_365=this.GetActivityPath(this.GetSuspendedActivity(_360),true);
this.LogSeq("`1074`",_360);
if(_365.length===0){
this.LogSeq("`273`",_360);
_361=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-5",IntegrationImplementation.GetString("Nothing to suspend"));
this.LogSeqReturn(_361,_360);
return _361;
}
this.LogSeq("`1003`",_360);
for(var i=0;i<_365.length;i++){
this.LogSeq("`632`"+_365[i].GetItemIdentifier()+")",_360);
_365[i].SetActive(false);
this.LogSeq("`846`",_360);
_365[i].SetSuspended(true);
}
this.LogSeq("`348`",_360);
this.SetCurrentActivity(_364,_360);
this.LogSeq("`159`",_360);
_361=new Sequencer_TerminationRequestProcessResult(_35e,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_361,_360);
return _361;
break;
case TERMINATION_REQUEST_ABANDON:
this.LogSeq("`1083`",_360);
this.LogSeq("`796`",_360);
_362.SetActive(false);
this.LogSeq("`459`",_360);
_361=new Sequencer_TerminationRequestProcessResult(_35e,null,null,"");
this.LogSeqReturn(_361,_360);
return _361;
break;
case TERMINATION_REQUEST_ABANDON_ALL:
this.LogSeq("`994`",_360);
this.LogSeq("`281`",_360);
_365=this.GetActivityPath(_362,true);
this.LogSeq("`1082`",_360);
if(_365.length===0){
this.LogSeq("`280`",_360);
_361=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-6",IntegrationImplementation.GetString("Nothing to close"));
this.LogSeqReturn(_361,_360);
return _361;
}
this.LogSeq("`993`",_360);
for(var i=0;i<_365.length;i++){
this.LogSeq("`866`",_360);
_365[i].SetActive(false);
}
this.LogSeq("`356`",_360);
this.SetCurrentActivity(_364,_360);
this.LogSeq("`162`",_360);
_361=new Sequencer_TerminationRequestProcessResult(_35e,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_361,_360);
return _361;
break;
default:
this.LogSeq("`254`",_360);
_361=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-7",IntegrationImplementation.GetString("The 'Termination' request {0} is not recognized.",_35e));
this.LogSeqReturn(_361,_360);
return _361;
break;
}
}
function Sequencer_TerminationRequestProcessResult(_36a,_36b,_36c,_36d){
this.TerminationRequest=_36a;
this.SequencingRequest=_36b;
this.Exception=_36c;
this.ExceptionText=_36d;
}
Sequencer_TerminationRequestProcessResult.prototype.toString=function(){
return "TerminationRequest="+this.TerminationRequest+", SequencingRequest="+this.SequencingRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};

