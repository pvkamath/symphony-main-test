﻿(function(window) {
    if (!Symphony) {
        Symphony = {}
    }

    var isConsole = window.console && typeof(console.log) == "function";

    Symphony.WebSync = {
        url: Symphony.symphonyDomain + '/websync.ashx',
        baseChannel: '/registration/',
        client: null,
        init: function (onSuccess) {
            
            if (Symphony.WebSync.client) {
                if (typeof onSuccess == 'function') {
                    onSuccess();
                }
                return;
            }
return;
            var client = new fm.websync.client(Symphony.WebSync.url);

            Symphony.WebSync.client = client;

            client.setAutoDisconnect({
                synchronous: true
            });

            client.connect({
                stayConnected: false,
                onSuccess: function (e) {
                    client.subscribe({
                        channel: Symphony.WebSync.getRegistrationChannel(),
                        onSuccess: function (e) {
                            Symphony.WebSync.client.publish({
                                channel: Symphony.WebSync.getRegistrationChannel(),
                                data: {
                                    userId: Symphony.userId,
                                    actualUserId: Symphony.actualUserId
                                }
                            });
                        },
                        onFailure: function (e) {
                            if (isConsole) {
                                console.log("Could not subscribe to channel" + Symphony.WebSync.getRegistrationChannel());
                            }
                        },
                        onReceive: function (e) {
                            if (!e.getWasSentByMe()) {
                                var userData = e.getData();

                                Control.Unload();
                                
                                $('frame').remove();

                                client.unsubscribe({
                                    channel: Symphony.WebSync.getRegistrationChannel(),
                                    onSuccess: function (e) {
                                        client.disconnect();
                                    },
                                    onFailure: function (e) {
                                        if (isConsole) {
                                            console.log("Could not unsubscribe from channel: " + Symphony.WebSync.getRegistrationChannel());
                                        }
                                        client.disconnect();
                                    }
                                });

                                var message = "This course has been interrupted due to being launched in another browser window.";

                                if (userData.actualUserId > 0 && userData.actualUserId != Symphony.userId) {
                                    message = "This course has been interrupted due an administrator launching the course on your behalf. If you are unaware of this action, please contact your administrator or your instructor for further details.";
                                    $.ajax({
                                        type: 'GET',
                                        url: Symphony.symphonyDomain + '/services/customer.svc/currentusercustomercare?jsonp=?',
                                        success: function (r) {
                                            var result = r.data,
                                            mobile = result.cellPhone || result.mobile,
                                            phone = result.workPhone || result.telephone || result.telephoneNumber;

                                            message = "This course has been interrupted due to an administrator launching the course on your behalf. If you are unaware of this action, please contact your customer care representative:" +
                                                        "\n\n" + result.firstName + " " + result.lastName +
                                                        "\nEmail: " + result.email +
                                                        (phone ? "\nPhone: " + phone : "") +
                                                        (mobile ? "\nMobile: " + mobile : "") +
                                                        "\n";
                                        },
                                        dataType: 'json',
                                        complete: function () {
                                            Symphony.WebSync.exit(message);
                                        }
                                    });
                                } else {
                                    Symphony.WebSync.exit(message);
                                }
                            }
                        }
                    });
                },
                onFailure: function (e) {
                    if (isConsole) {
                        console.log("Could not connect to websync " + e.getException().message);
                    }
                },
                onStreamFailure: function (e) {
                    if (isConsole) {
                        console.log("Stream failure");
                    }
                }
            });
        },
        getRegistrationChannel: function () {
            return Symphony.WebSync.baseChannel + Symphony.registrationId;
        },
        exit: function (message) {
            alert(message);
            window.top.close();
        }
    }

    Symphony.WebSync.init();
})(window)