var LogDataObject = new Object();
LogDataObject.ActivityDebug = false;
LogDataObject.ActivityDebugData = false;

LogDataObject.PossibleNavRequests = false;
LogDataObject.PossibleNavRequestsData = false;

LogDataObject.GlobalObjectives = false;
LogDataObject.GlobalObjectivesData = false;

LogDataObject.SspBuckets = false;
LogDataObject.SspBucketsData = false;

function toggleLogData(sectionDivID){
    //these var function assignments are inside this function to ensure that the files are loaded
    // and the functions declared
    LogDataObject.ActivityDebugCode = DisplayActivityDebugData;
    LogDataObject.PossibleNavRequestsCode = DisplayPossibleNavigationRequestData;
    LogDataObject.GlobalObjectivesCode = DisplayGlobalObjectivesData;
    LogDataObject.SspBucketsCode = DisplaySspBucketsData;
    LogDataObject.ErrorsCode = DisplayLastError;
        
	var div = document.getElementById(sectionDivID);
	var a = document.getElementById(sectionDivID + "Link");
	
	if (!eval("LogDataObject." + sectionDivID)){
		if (!eval("LogDataObject." + sectionDivID + "Data")){ 
			div.innerHTML = eval("LogDataObject." + sectionDivID + "Code()");
			eval("LogDataObject." + sectionDivID + "Data = true");
		}
		div.style.display = "block";	
		changeInnerText(a, "Hide");
		eval("LogDataObject." + sectionDivID + " = true");
	} else {
		div.style.display = "none";	
		changeInnerText(a, "Show");
		eval("LogDataObject." + sectionDivID + " = false");
	}
	
}


function changeInnerText(element, newTxt){
	if (element.textContent){
		element.textContent = newTxt;
	} else{
		element.innerText = newTxt;
	}
}

function toggle(id) {
	var div = document.getElementById(id);
	id = "t" + id;
	var a = document.getElementById(id);
			
	if (div.style.display === "none" || div.style.display === "") {
		div.style.display = "block";
		changeInnerText(a,"- ");
	} else {
		div.style.display = "none";
		changeInnerText(a,"+ ");
	}
}

function expandAll() {						
	var divs = document.getElementsByTagName("div");

	for (var i = 0; i < divs.length; i++) {
		if (divs[i].className === "details") {
			divs[i].style.display = "block";
			var id = "t" + divs[i].id;
			var a = document.getElementById(id);
			changeInnerText(a,"- ");
		}
	}
	detailLevel = "expanded";
}

function collapseAll() {						
	var divs = document.getElementsByTagName("div");
	
	for (var i = 0; i < divs.length; i++) {
		if (divs[i].className === "details") {
			divs[i].style.display = "none";
			var id = "t" + divs[i].id;
			var a = document.getElementById(id);
			changeInnerText(a,"+ ");							
		}
	}
	detailLevel = "collapsed";
}



function init() {
    if (detailLevel != "child") toggleLogData('ActivityDebug');
	document.getElementById('reportBody').style.display = 'block'; 
	document.getElementById('loadMessage').style.display = 'none';
	if (detailLevel == "child" || detailLevel == "expanded") expandAll();
	
	var supportUrl = window.opener.ERROR_REPORT_FORM_URL;
	
	if (supportUrl == null || supportUrl.length == 0) {
	    document.getElementById("sendToSupportLink").style.display = 'none';
	} else {
	    document.getElementById("saveDebugLogLink").style.display = 'none';
	
	    // Load up the hidden 'send log to support' form
        var sendLogToSupportForm = document.getElementById("sendLogToSupportForm");
        sendLogToSupportForm.action = supportUrl;
        sendLogToSupportForm.log.value = window.opener.Debug.log.toXml(true);
        sendLogToSupportForm.registrationId.value = window.opener.ExternalRegistrationId;
        sendLogToSupportForm.configuration.value = window.opener.ExternalConfig;
	}
	
    var saveDebugLogForm = document.getElementById("saveDebugLogForm");
    saveDebugLogForm.action = window.opener.DEBUG_LOG_PERSIST_PAGE;
    saveDebugLogForm.log.value = window.opener.Debug.log.toXml(true);
    saveDebugLogForm.registrationId.value = window.opener.ExternalRegistrationId;
    saveDebugLogForm.configuration.value = window.opener.ExternalConfig;
    
    if (loadAllData === true){
        toggleLogData('PossibleNavRequests');
        toggleLogData('GlobalObjectives');
        toggleLogData('SspBuckets');
        toggleLogData('Errors');
        
    
    }

	window.focus();
}		
							
function renderNode(elementName, nodeId) {
	window.opener.Debug.log.displayChild(elementName, nodeId);
}


function keyPressHandler(p_event) {
	var key = p_event.keyCode ? p_event.keyCode : p_event.charCode;
	var esc = 27;
	var r = 114;
	
	if (key === esc) {
		window.close();
	} else if (key === r) {
		refresh();
	}
}
					
function refresh() {
	var expandAll = (detailLevel == "expanded")? true : false;
	window.opener.Debug.log.displayRefresh(window, expandAll);												
}

function filteredRefresh(){
	var filterObj = new Object();
	filterObj['controlFilter'] = controlFilter;
	filterObj['runtimeFilter'] = runtimeFilter;
	filterObj['sequencingFilter'] = sequencingFilter;
	filterObj['sequencingReadableFilter'] = sequencingReadableFilter;
	filterObj['lookaheadFilter'] = lookaheadFilter;

	window.opener.Debug.log.displayFilter(window,filterObj);
}

function toggleCheckbox(checkboxID){
	var cbox = document.getElementById(checkboxID);
	cbox.checked = !cbox.checked;
}	

function renderDivSection(divID, sectionName, sectionItems){
	var tempStr = "";
	
	if (sectionItems.length > 0){
	    tempStr += "<span style=\"cursor: pointer\" onclick=\"toggle('" + divID +"')\">";
		tempStr += "<span id='t" + divID + "'>+ </span><span class='control'>";
		tempStr += sectionName;
		tempStr += "</span><br>";
		tempStr += "</span>";
		tempStr += "<div class='details' id='" + divID + "'>";
		tempStr += "<ul style='margin-top:0;margin-bottom:0'>";
		
		for (var i=0; i < sectionItems.length; i++){
			tempStr += "<li>" + sectionItems[i] + "</li>";	
		}
		tempStr += "</ul></div>";
	} else {
		tempStr += sectionName;
	}
	return tempStr;	
}		

function sendLogToSupport() {
    var form = document.getElementById("sendLogToSupportForm");
    window.open("","_sendLogToSupportForm","toolbars=0,location=0,width=600,height=550");
    form.submit();
}	

function saveDebugLog() {
    var form = document.getElementById("saveDebugLogForm");
    window.open("","_saveDebugLogForm","toolbars=0,location=0,width=600,height=50");
    form.submit();
}		
					