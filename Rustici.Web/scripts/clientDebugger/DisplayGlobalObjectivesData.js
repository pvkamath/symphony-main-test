//This js uses the renderDivSection() function in the DebuggerUtilityFunction.js file

function DisplayGlobalObjectivesData(){
	var GlobalObjectives = window.opener.Control.Sequencer.GlobalObjectives;
	var tempStr = "";
	//tempStr += GlobalObjectives;
	
	for (var loopIndex = 0; loopIndex < GlobalObjectives.length; loopIndex++){
	    var globalObjective = GlobalObjectives[loopIndex];
	    
	    var GlobalObjectiveArray = new Array();
	    var GlobalObjectiveID = "GlobalObj_" + loopIndex;
	    var GlobalObjectiveName = globalObjective.ID;
	    var idx = 0;
	    GlobalObjectiveArray[idx++] = "Measure Status: " + globalObjective.MeasureStatus;
	    GlobalObjectiveArray[idx++] = "Normalized Measure: " + globalObjective.NormalizedMeasure;
	    GlobalObjectiveArray[idx++] = "Progress Status: " + globalObjective.ProgressStatus;
	    GlobalObjectiveArray[idx++] = "Satisfied Status: " + globalObjective.SatisfiedStatus;

	    GlobalObjectiveArray[idx++] = "Completion Status: " + globalObjective.CompletionStatus;
	    GlobalObjectiveArray[idx++] = "Completion Status Value: " + globalObjective.CompletionStatusValue;
	    GlobalObjectiveArray[idx++] = "Progress Measure Status: " + globalObjective.ProgressMeasureStatus;
	    GlobalObjectiveArray[idx++] = "Progress Measure: " + globalObjective.ProgressMeasure;
	    
	    GlobalObjectiveArray[idx++] = "Score Raw: " + globalObjective.ScoreRaw;
	    GlobalObjectiveArray[idx++] = "Score Min: " + globalObjective.ScoreMin;
	    GlobalObjectiveArray[idx++] = "Score Max: " + globalObjective.ScoreMax;
	
	    
	    tempStr += renderDivSection(GlobalObjectiveID,GlobalObjectiveName,GlobalObjectiveArray);
	
	
	}
	
	
	return tempStr;
}
