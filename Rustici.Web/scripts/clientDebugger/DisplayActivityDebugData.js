//This js uses the renderDivSection() function in the DebuggerUtilityFunction.js file

function DisplayActivityDebugData(){

	// Only render if activity info is still available, which may not be the
	// case when closing/exiting
	if (window.opener.Control) {
		var rootActivity = window.opener.Control.Activities.ActivityTree;
		return renderActivity(rootActivity,0);
	}
}

function renderDivSectionWithMargin(divID, sectionName, sectionItems, treeLevel){
	var tempStr = "";
	tempStr += "<span style=\"margin-left:" + (treeLevel * 10) + "px; \">";
	tempStr += renderDivSection(divID, sectionName, sectionItems);
	tempStr += "</span>";
	return tempStr;
}

// activity is the activity tree object
// treeLevel is the level within the tree of the activity (root is zero, root's children are 1, etc.)
function renderActivity(activity,treeLevel) {

	var idx = 0;
	var tempStr = "";
	
	//Course Information
	if (treeLevel == 0){
		var CourseInfo = new Array();
		var CourseInfoID = activity.DatabaseId + "cinfo";
		var CourseInfoName = "Course Information";
		CourseInfo[idx++] = "Course Name = " + activity.GetTitle();
		CourseInfo[idx++] = "Learning Standard = " + window.opener.Control.Package.LearningStandard;
		CourseInfo[idx++] = "Root Path = " + window.opener.PathToCourse;
		//CourseInfo[idx++] = ;
	}
	
	//Tracking Data
	var TrackingData = new Array();
	var TrackingDataID = activity.DatabaseId + "trac";
	var TrackingDataName = "Tracking Data";
	idx = 0;
	TrackingData[idx++] = "Activity Progress Status = " + activity.GetActivityProgressStatus();
	TrackingData[idx++] = "Attempt Count = " + activity.GetAttemptCount();
	TrackingData[idx++] = "Attempt Progress Status = " + activity.GetAttemptProgressStatus();
	if(activity.GetAttemptProgressStatus() === true) {
		TrackingData[idx++] = "Attempt Completion Amount Status = " + activity.GetAttemptCompletionAmountStatus();
	} else {
		TrackingData[idx++] = "Attempt Completion Amount Status = unknown";
	}
	TrackingData[idx++] = "Attempt Completion Amount = " + activity.GetAttemptCompletionAmount();
	TrackingData[idx++] = "Attempt Completion Status = " + activity.GetAttemptCompletionStatus();
	TrackingData[idx++] = "Previous Attempt Progress Status = " + activity.PrevAttemptProgressStatus;
	TrackingData[idx++] = "Previous Attempt Completion Status = " + activity.PrevAttemptCompletionStatus;
	TrackingData[idx++] = "Active = " + activity.IsActive();
	TrackingData[idx++] = "Suspended = " + activity.IsSuspended();
	TrackingData[idx++] = "Is an Available Child of Parent = " + activity.Included;
	TrackingData[idx++] = "Ordinal = " + activity.Ordinal;
	
	
	//Rule Condition Evaluation
	var RuleCondEval = new Array();
	var RuleCondEvalID = activity.DatabaseId + "rcond";
	var RuleCondEvalName = "Rule Condition Evaluation";
	idx = 0;
	RuleCondEval[idx++] = "Completed = " + activity.IsCompleted();
	RuleCondEval[idx++] = "Activity Progress Known = " + activity.IsActivityProgressKnown("",false);
	RuleCondEval[idx++] = "Attempted = " + activity.IsAttempted();
	RuleCondEval[idx++] = "Attempt Limit Exceeded = " + activity.IsAttemptLimitExceeded();
	
	
	if (activity.IsDeliverable()){
		
		var ScoAssetData = new Array();
		var ScoAssetDataID = activity.DatabaseId + "lodef";
		var ScoAssetDataName = "SCO / Asset Data";
		idx = 0;
		ScoAssetData[idx++] = "Href = " + activity.LearningObject.Href;
		ScoAssetData[idx++] = "Parameters = " + activity.LearningObject.Parameters;
		ScoAssetData[idx++] = "DataFromLms = " + activity.LearningObject.DataFromLms;
		ScoAssetData[idx++] = "MasteryScore = " + activity.LearningObject.MasteryScore;
		ScoAssetData[idx++] = "Visible = " + activity.LearningObject.Visible;
		ScoAssetData[idx++] = "CompletionThreshold = " + activity.LearningObject.CompletionThreshold;
		ScoAssetData[idx++] = "ResourceIdentifier = " + activity.LearningObject.ResourceIdentifier;
	}
	
//Sequencing Definitions
	//Sequencing Control Modes
	var SeqControlModes = new Array();
	var SeqControlModesID = activity.DatabaseId +"scm";
	var SeqControlModesName = "Sequencing Control Modes: ";
	idx = 0;
	SeqControlModes[idx++] = "Sequencing Control Choice: " + activity.GetSequencingControlChoice();
	SeqControlModes[idx++] = "Sequencing Control Choice Exit: " + activity.GetSequencingControlChoiceExit();
	SeqControlModes[idx++] = "Sequencing Control Choice Flow: " + activity.GetSequencingControlFlow();
	SeqControlModes[idx++] = "Sequencing Control Choice Forward Only: " + activity.GetSequencingControlForwardOnly();
	SeqControlModes[idx++] = "Use Current Attempt Objective Information: " + activity.LearningObject.SequencingData.UseCurrentAttemptObjectiveInformation;
	SeqControlModes[idx++] = "Use Current Attempt Progress Information: " + activity.LearningObject.SequencingData.UseCurrentAttemptProgressInformation;
	
	//Constrain Choice Controls
	var ConstrainChoiceConts = new Array();
	var ConstrainChoiceContsID = activity.DatabaseId +"ccc";
	var ConstrainChoiceContsName = "Constrain Choice Controls: ";
	idx = 0;
	ConstrainChoiceConts[idx++] = "Constrained Choice: " + activity.GetConstrainedChoice();
	ConstrainChoiceConts[idx++] = "Limit Condition Attempt Limit: " + activity.GetLimitConditionAttemptLimit();
	
	//Limit Conditions
	var LimitConditions = new Array();
	var LimitConditionsID = activity.DatabaseId +"lc";
	var LimitConditionsName = "Limit Conditions: ";
	idx = 0;
	LimitConditions[idx++] = "Limit Condition Attempt Control: " + activity.GetLimitConditionAttemptControl();
	LimitConditions[idx++] = "Prevent Activation: " + activity.GetPreventActivation();
	
	//Rollup Controls
	var RollupControls = new Array();
	var RollupControlsID = activity.DatabaseId +"rc";
	var RollupControlsName = "Rollup Controls: ";
	idx = 0;
	RollupControls[idx++] = "Rollup Objective Satisfied: " + activity.GetRollupObjectiveSatisfied();
	RollupControls[idx++] = "Rollup Objective Measure Weight: " + activity.GetRollupObjectiveMeasureWeight();
	RollupControls[idx++] = "Rollup Progress Completion: " + activity.RollupProgressCompletion();
	
	//Rollup Consideration Controls
	var RollupConsidControls = new Array();
	var RollupConsidControlsID = activity.DatabaseId +"rcc";
	var RollupConsidControlsName = "Rollup Consideration Controls: ";
	idx = 0;
	RollupConsidControls[idx++] = "Measure Satisfaction If Active: " + activity.GetMeasureSatisfactionIfActive();
	RollupConsidControls[idx++] = "Required For Satisfied: " + activity.GetRequiredForSatisfied();
	RollupConsidControls[idx++] = "Required For Not Satisfied: " + activity.GetRequiredForNotSatisfied();
	RollupConsidControls[idx++] = "Required For Completed: " + activity.GetRequiredForCompleted();
	RollupConsidControls[idx++] = "Required For Incomplete: " + activity.GetRequiredForIncomplete();
	
	//Selection Controls
	var SelectionControls = new Array();
	var SelectionControlsID = activity.DatabaseId +"sc";
	var SelectionControlsName = "Selection Controls: ";
	idx = 0;
	SelectionControls[idx++] = "Selection Timing: " + activity.GetSelectionTiming();
	SelectionControls[idx++] = "Selection Count Status: " + activity.GetSelectionCountStatus();
	SelectionControls[idx++] = "Selection Count: " + activity.GetSelectionCount();
	
	//Randomization Controls
	var RandomizationControls = new Array();
	var RandomizationControlsID = activity.DatabaseId +"ranc";
	var RandomizationControlsName = "Randomization Controls: ";
	idx = 0;
	RandomizationControls[idx++] = "Randomization Timing: " + activity.GetRandomizationTiming();
	RandomizationControls[idx++] = "Randomize Children: " + activity.GetRandomizeChildren();
	
	//Delivery Controls
	var DeliveryControls = new Array();
	var DeliveryControlsID = activity.DatabaseId +"delc";
	var DeliveryControlsName = "Delivery Controls: ";
	idx = 0;
	DeliveryControls[idx++] = "Tracked: " + activity.IsTracked();
	DeliveryControls[idx++] = "Completion Set By Content: " + activity.IsCompletionSetByContent();
	DeliveryControls[idx++] = "Objective Set By Content: " + activity.IsObjectiveSetByContent();
	
	//Hide LMS UI Controls
	var HideLmsUiControls = new Array();
	var HideLmsUiControlsID = activity.DatabaseId+"hidelmsuic";
	var HideLmsUiControlsName = "Hide LMS UI Controls: ";
	idx = 0;
    HideLmsUiControls[idx++] = "Hide Previous: " + activity.LearningObject.SequencingData.HidePrevious;
    HideLmsUiControls[idx++] = "Hide Continue: " + activity.LearningObject.SequencingData.HideContinue;
    HideLmsUiControls[idx++] = "Hide Exit: " + activity.LearningObject.SequencingData.HideExit;
    HideLmsUiControls[idx++] = "Hide Abandon: " + activity.LearningObject.SequencingData.HideAbandon;
    HideLmsUiControls[idx++] = "Hide Suspend All: " + activity.LearningObject.SequencingData.HideSuspendAll;
    HideLmsUiControls[idx++] = "Hide Abandon All: " + activity.LearningObject.SequencingData.HideAbandonAll;
    HideLmsUiControls[idx++] = "Hide Exit All: " + activity.LearningObject.SequencingData.HideExitAll;		


	//Roll Up Rules
	var RollupRules = activity.GetRollupRules();
	var RollupRulesID = activity.DatabaseId +"rr";
	var RollupRulesName = "Rollup Rules:";
	if (RollupRules.length == 0) {
		RollupRulesName += " None<br>";
	}
	
	//Sequencing Rules
	var PreCondRules = activity.GetPreConditionRules();
	var PreCondRulesID = activity.DatabaseId +"pcr";
	var PreCondRulesName = "Pre Condition Sequencing Rules:";
	if (PreCondRules.length == 0) {
		PreCondRulesName += " None<br>";
	}
	
	var PostCondRules = activity.GetPostConditionRules();
	var PostCondRulesID = activity.DatabaseId +"pcr";
	var PostCondRulesName = "Post Condition Sequencing Rules:";
	if (PostCondRules.length == 0) {
		PostCondRulesName += " None<br>";
	}
	
	var ExitSeqRules = activity.GetExitRules();
	var ExitSeqRulesID = activity.DatabaseId +"esr";
	var ExitSeqRulesName = "Exit Sequencing Rules:";
	if (ExitSeqRules.length == 0) {
		ExitSeqRulesName += " None<br>";
	}
	
	//Sequencing Definitions Head
	var SeqDefinition = new Array();
	var SeqDefinitionID = activity.DatabaseId +"def";
	var SeqDefinitionName = "Sequencing Definition";
	idx = 0;
	SeqDefinition[idx++] = renderDivSection(SeqControlModesID,SeqControlModesName,SeqControlModes);
	SeqDefinition[idx++] = renderDivSection(ConstrainChoiceContsID,ConstrainChoiceContsName,ConstrainChoiceConts);
	SeqDefinition[idx++] = renderDivSection(LimitConditionsID,LimitConditionsName,LimitConditions);
	SeqDefinition[idx++] = renderDivSection(RollupControlsID,RollupControlsName,RollupControls);
	SeqDefinition[idx++] = renderDivSection(RollupConsidControlsID,RollupConsidControlsName,RollupConsidControls);
	SeqDefinition[idx++] = renderDivSection(SelectionControlsID,SelectionControlsName,SelectionControls);
	SeqDefinition[idx++] = renderDivSection(RandomizationControlsID,RandomizationControlsName,RandomizationControls);
	SeqDefinition[idx++] = renderDivSection(DeliveryControlsID,DeliveryControlsName,DeliveryControls);
	SeqDefinition[idx++] = renderDivSection(HideLmsUiControlsID,HideLmsUiControlsName,HideLmsUiControls);
	SeqDefinition[idx++] = renderDivSection(RollupRulesID,RollupRulesName,RollupRules);
	SeqDefinition[idx++] = renderDivSection(PreCondRulesID,PreCondRulesName,PreCondRules);
	SeqDefinition[idx++] = renderDivSection(PostCondRulesID,PostCondRulesName,PostCondRules);
	SeqDefinition[idx++] = renderDivSection(ExitSeqRulesID,ExitSeqRulesName,ExitSeqRules);
//End Seq Defs	
	
	//Activity Objective Runtime Data
	var ActObjectives = new Array();
	var ActObjectivesID = activity.DatabaseId +"objs";
	var ActObjectivesName = "Activity Objectives";
	var objectives = activity.GetObjectives();
	for (var i = 0; i < objectives.length; i++) {
		ActObjectives[i] = renderObjective(objectives[i], activity, i+1);
	}
	

//Activity Head
	var ActivityData = new Array();	
	var ActivityDataID = activity.DatabaseId;
	var ActivityDataName = activity.toString();
	if (activity.IsActive()) {
		 ActivityDataName = "<strong>" + ActivityDataName + "</strong>";
	}
	idx = 0;
	if (treeLevel == 0){
		ActivityData[idx++] = renderDivSection(CourseInfoID,CourseInfoName,CourseInfo);
	}
	ActivityData[idx++] = renderDivSection(TrackingDataID,TrackingDataName,TrackingData);
	ActivityData[idx++] = renderDivSection(RuleCondEvalID,RuleCondEvalName,RuleCondEval);
	if (activity.IsDeliverable()){
		ActivityData[idx++] = renderDivSection(ScoAssetDataID,ScoAssetDataName,ScoAssetData);
	}
	ActivityData[idx++] = renderDivSection(SeqDefinitionID,SeqDefinitionName,SeqDefinition);
	ActivityData[idx++] = renderDivSection(ActObjectivesID,ActObjectivesName,ActObjectives);
	if (activity.IsDeliverable() && activity.RunTime != null) {
		ActivityData[idx++] = renderRuntime(activity);
	}
	
	tempStr = renderDivSectionWithMargin(ActivityDataID,ActivityDataName,ActivityData,treeLevel);
	
	//child activities
	var children = activity.GetChildren();
	for (var i = 0; i < children.length; i++) {
		tempStr += renderActivity(children[i],treeLevel+1);
	}
	
	return tempStr;
	
}

function renderObjective(o, activity, index) {
	
	var idx = 0;
	
	var ObjSeqDef = new Array();	
	var ObjSeqDefID = activity.DatabaseId + "obj" + index + "sd";
	var ObjSeqDefName = "Sequencing Definition";
	ObjSeqDef[idx++] = "Satisfied By Measure = " + o.SatisfiedByMeasure;
	ObjSeqDef[idx++] = "Min Normalized Measure = " + o.MinNormalizedMeasure;
	
	var ObjMaps = new Array();
	var ObjMapsID = activity.DatabaseId + "obj" + index + "map";
	var ObjMapsName = "Maps:";
	var maps = o.Maps;
	if (maps.length == 0) {
		ObjMapsName += " None<br>";
	}
	else{
		for (var i=0; i < maps.length; i++){
			ObjMaps[i] = renderMap(maps[i].toString(),ObjMapsID + (i+1));
		}
	}
		
	var Objective = new Array();
	var ObjectiveID = activity.DatabaseId + "obj" + index;
	var ObjectiveName = "";
	if (o.GetContributesToRollup() === true) {
		ObjectiveName = "Primary Objective:";
	}
	else{
		ObjectiveName = "Objective: " + index;
	}
	idx = 0;
	Objective[idx++] = "Identifier = " + o.GetIdentifier();
	Objective[idx++] = "Progress Status = " + o.GetProgressStatus(activity, false);
	if(o.GetProgressStatus(activity, false)) {
		Objective[idx++] = "Satisfied Status = " + o.GetSatisfiedStatus(activity, false);
	} else {
		Objective[idx++] = "Satisfied Status = unknown";
	}	
	Objective[idx++] = "Measure Status = " + o.GetMeasureStatus(activity, false);
	if(o.GetMeasureStatus(activity, false)) {
		Objective[idx++] = "Normalized Measure = " + o.GetNormalizedMeasure(activity, false);
	} else {
		Objective[idx++] = "Normalized Measure = unknown";
	}
	Objective[idx++] = "Previous Progress Status = " + o.PrevProgressStatus;
	Objective[idx++] = "Previous Satisfied Status = " + o.PrevSatisfiedStatus;
	Objective[idx++] = "Previous Measure Status = " + o.PrevMeasureStatus;
	Objective[idx++] = "Previous Normalized Measure = " + o.PrevNormalizedMeasure;
	Objective[idx++] = "Satisfied = " + activity.IsSatisfied(o.Identifier);
	Objective[idx++] = "Objective Status Known = " + activity.IsObjectiveStatusKnown(o.Identifier, false);
	Objective[idx++] = "Objective Measure Known = " + activity.IsObjectiveMeasureKnown(o.Identifier, false);
	Objective[idx++] = "Completion Status = " + o.CompletionStatus;
	Objective[idx++] = "Completion Status Value = " + o.CompletionStatusValue;
	Objective[idx++] = "Progress Measure Status = " + o.ProgressMeasureStatus;
	Objective[idx++] = "Progress Measure = " + o.ProgressMeasure;
	Objective[idx++] = renderDivSection(ObjSeqDefID,ObjSeqDefName,ObjSeqDef);
	Objective[idx++] = renderDivSection(ObjMapsID,ObjMapsName,ObjMaps);
	
	return renderDivSection(ObjectiveID,ObjectiveName,Objective);

}

function renderMap(map,mapid){
	
	var mapArr = map.split(",");
	var ObjMap = new Array();
	var ObjMapID = mapid;
	var ObjMapName = mapArr[0];
	for (var i=1; i < mapArr.length; i++){
		ObjMap[i-1] = mapArr[i];
	}
	return renderDivSection(ObjMapID,ObjMapName,ObjMap);
}

function renderRuntime(activity) {
	
	var rt = activity.RunTime;
	
	var RuntimeData = new Array();
	var RuntimeDataID = activity.DatabaseId + "rt";
	var RuntimeDataName = "Runtime Data";
	var idx = 0;	
	if (window.opener.Control.Package.LearningStandard.is12()) {
		RuntimeData[idx++] = "LessonStatus = " + window.opener.TranslateDualStausToSingleStatus(rt.CompletionStatus, rt.SuccessStatus);
	} else {
		RuntimeData[idx++] = "Completion Status = " + rt.CompletionStatus;
		RuntimeData[idx++] = "Success Status = " + rt.SuccessStatus;
	}
	RuntimeData[idx++] = "Credit = " + rt.Credit;
	RuntimeData[idx++] = "Entry = " + rt.Entry;
	RuntimeData[idx++] = "Exit = " + rt.Exit;
	RuntimeData[idx++] = "Location = " + rt.Location;
	RuntimeData[idx++] = "Mode = " + rt.Mode;
	RuntimeData[idx++] = "ProgressMeasure = " + rt.ProgressMeasure;
	RuntimeData[idx++] = "ScoreRaw = " + rt.ScoreRaw;
	RuntimeData[idx++] = "ScoreMax = " + rt.ScoreMax;
	RuntimeData[idx++] = "ScoreMin = " + rt.ScoreMin;
	RuntimeData[idx++] = "ScoreScaled = " + rt.ScoreScaled;
	RuntimeData[idx++] = "SuspendData = " + rt.SuspendData;
	RuntimeData[idx++] = "TotalTime = " + rt.TotalTime;
	RuntimeData[idx++] = "TotalTimeTracked = " + rt.TotalTimeTracked;
	RuntimeData[idx++] = "AudioLevel = " + rt.AudioLevel;
	RuntimeData[idx++] = "LanguagePreference = " + rt.LanguagePreference;
	RuntimeData[idx++] = "DeliverySpeed = " + rt.DeliverySpeed;
	RuntimeData[idx++] = "AudioCaptioning = " + rt.AudioCaptioning;
	var comments = rt.Comments;
	if (comments.length > 0) {
		var CommentsArr = new Array();
		var CommentsID = activity.DatabaseId + "rt_com";
		var CommentsName = "Comments";
		for (var i = 0; i < comments.length; i++) {
			CommentsArr[i] = renderRtComment(comments[i], CommentsID + (i+1));
		}
		RuntimeData[idx++] = renderDivSection(CommentsID,CommentsName,CommentsArr);
	}
	var commentsFromLms = rt.CommentsFromLMS;
	if (commentsFromLms.length > 0) {
		var LMSComments = new Array();
		var LMSCommentsID = activity.DatabaseId + "rt_LMScom";
		var LMSCommentsName = "Comments From LMS";
		for (var i = 0; i < commentsFromLms.length; i++) {
			LMSComments[i] = renderRtComment(commentsFromLms[i], LMSCommentsID + (i+1));
		}
		RuntimeData[idx++] = renderDivSection(LMSCommentsID,LMSCommentsName,LMSComments);
	}	
	var interactions = rt.Interactions;
	if (interactions.length > 0) {
		var RTInteractions = new Array();
		var RTInteractionsID = activity.DatabaseId + "rt_int";
		var RTInteractionsName = "Interactions";
		for (var i = 0; i < interactions.length; i++) {
			RTInteractions[i] = renderRtInteraction(interactions[i], RTInteractionsID + (i+1));
		}
		RuntimeData[idx++] = renderDivSection(RTInteractionsID,RTInteractionsName,RTInteractions);
	}
	var objectives = rt.Objectives;
	if (objectives.length > 0) {
		var RTObjectives = new Array();
		var RTObjectivesID = activity.DatabaseId + "rt_obj";
		var RTObjectivesName = "Runtime Objectives";
		for (var i = 0; i < objectives.length; i++) {
			RTObjectives[i] = renderRtObjective(objectives[i], i+1, RTObjectivesID + (i+1));
		}
		RuntimeData[idx++] = renderDivSection(RTObjectivesID,RTObjectivesName,RTObjectives);
	}
	
	return renderDivSection(RuntimeDataID,RuntimeDataName,RuntimeData);
	 
}


function renderRtComment(c, cID) {
	var cmtArr = new Array();
	var idx = 0;
	cmtArr[idx++] = "Location = " + c.Location;
	cmtArr[idx++] = "Timestamp = " + c.Timestamp;
	
	return renderDivSection(cID,"<u>\"" + c.Comment + "\"</u>",cmtArr);
}

function renderRtInteraction(i, iID) {
	var Interaction = new Array();
	var idx = 0;
	Interaction[idx++] = "Interaction ID = " + i.Id;
	Interaction[idx++] = "Type = " + i.Type;
	Interaction[idx++] = "Result = " + i.Result;
	Interaction[idx++] = "Latency = " + i.Latency;
	Interaction[idx++] = "Description = " + i.Description;
	Interaction[idx++] = "Weighting = " + i.Weighting;
	Interaction[idx++] = "Learner Response = " + i.LearnerResponse;
	Interaction[idx++] = "Correct Responses = " + i.CorrectResponses;
	Interaction[idx++] = "Timestamp = " + i.Timestamp;
	var objectives = i.Objectives;
	if (objectives.length > 0) {
		var IntObj = new Array();
		var IntObjID = iID + "obj";
		var IntObjName = "Runtime Interaction Objectives";
		for (var i = 0; i < objectives.length; i++) {
			IntObj[i] = "Interaction Objective " + (i+1) + " = " + objectives[i];
		}
		Interaction[idx++] = renderDivSection(IntObjID, IntObjName, IntObj);
	}
	
	return renderDivSection(iID, "Interaction ID = " + i.Id,Interaction);
}

function renderRtObjective(o,index,RTObjID) {
	var RTObj = new Array();
	var idx = 0;
	RTObj[idx++] = "Success Status = " + o.SuccessStatus;
	RTObj[idx++] = "Score Scaled = " + o.ScoreScaled;
	RTObj[idx++] = "Score Raw = " + o.ScoreRaw;
	RTObj[idx++] = "Progress Measure = " + o.ProgressMeasure; 
	RTObj[idx++] = "Description = " + o.Description;
	
	return renderDivSection(RTObjID,"Runtime Objective " + index + ": Identifier = " + o.Identifier,RTObj);
}
