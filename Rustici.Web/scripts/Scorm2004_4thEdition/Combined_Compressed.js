var SCORM_TRUE="true";
var SCORM_FALSE="false";
var SCORM_UNKNOWN="unknown";
var SCORM2004_NO_ERROR="0";
var SCORM2004_GENERAL_EXCEPTION_ERROR="101";
var SCORM2004_GENERAL_INITIALIZATION_FAILURE_ERROR="102";
var SCORM2004_ALREADY_INTIAILIZED_ERROR="103";
var SCORM2004_CONTENT_INSTANCE_TERMINATED_ERROR="104";
var SCORM2004_GENERAL_TERMINATION_FAILURE_ERROR="111";
var SCORM2004_TERMINATION_BEFORE_INITIALIZATION_ERROR="112";
var SCORM2004_TERMINATION_AFTER_TERMINATION_ERROR="113";
var SCORM2004_RETRIEVE_DATA_BEFORE_INITIALIZATION_ERROR="122";
var SCORM2004_RETRIEVE_DATA_AFTER_TERMINATION_ERROR="123";
var SCORM2004_STORE_DATA_BEFORE_INITIALIZATION_ERROR="132";
var SCORM2004_STORE_DATA_AFTER_TERMINATION_ERROR="133";
var SCORM2004_COMMIT_BEFORE_INITIALIZATION_ERROR="142";
var SCORM2004_COMMIT_AFTER_TERMINATION_ERROR="143";
var SCORM2004_GENERAL_ARGUMENT_ERROR="201";
var SCORM2004_GENERAL_GET_FAILURE_ERROR="301";
var SCORM2004_GENERAL_SET_FAILURE_ERROR="351";
var SCORM2004_GENERAL_COMMIT_FAILURE_ERROR="391";
var SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR="401";
var SCORM2004_UNIMPLEMENTED_DATA_MODEL_ELEMENT_ERROR="402";
var SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR="403";
var SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR="404";
var SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR="405";
var SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR="406";
var SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR="407";
var SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR="408";
var SCORM2004_ErrorStrings=new Array();
SCORM2004_ErrorStrings[SCORM2004_NO_ERROR]="No Error";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_EXCEPTION_ERROR]="General Exception";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_INITIALIZATION_FAILURE_ERROR]="General Initialization Failure";
SCORM2004_ErrorStrings[SCORM2004_ALREADY_INTIAILIZED_ERROR]="Already Initialized";
SCORM2004_ErrorStrings[SCORM2004_CONTENT_INSTANCE_TERMINATED_ERROR]="Content Instance Terminated";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_TERMINATION_FAILURE_ERROR]="General Termination Failure";
SCORM2004_ErrorStrings[SCORM2004_TERMINATION_BEFORE_INITIALIZATION_ERROR]="Termination Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_TERMINATION_AFTER_TERMINATION_ERROR]="Termination AFter Termination";
SCORM2004_ErrorStrings[SCORM2004_RETRIEVE_DATA_BEFORE_INITIALIZATION_ERROR]="Retrieve Data Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_RETRIEVE_DATA_AFTER_TERMINATION_ERROR]="Retrieve Data After Termination";
SCORM2004_ErrorStrings[SCORM2004_STORE_DATA_BEFORE_INITIALIZATION_ERROR]="Store Data Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_STORE_DATA_AFTER_TERMINATION_ERROR]="Store Data After Termination";
SCORM2004_ErrorStrings[SCORM2004_COMMIT_BEFORE_INITIALIZATION_ERROR]="Commit Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_COMMIT_AFTER_TERMINATION_ERROR]="Commit After Termination";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_ARGUMENT_ERROR]="General Argument Error";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_GET_FAILURE_ERROR]="General Get Failure";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_SET_FAILURE_ERROR]="General Set Failure";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_COMMIT_FAILURE_ERROR]="General Commit Failure";
SCORM2004_ErrorStrings[SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR]="Undefined Data Model Element";
SCORM2004_ErrorStrings[SCORM2004_UNIMPLEMENTED_DATA_MODEL_ELEMENT_ERROR]="Unimplemented Data Model Element";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR]="Data Model Element Value Not Initialized";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR]="Data Model Element Is Read Only";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR]="Data Model Element Is Write Only";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR]="Data Model Element Type Mismatch";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR]="Data Model Element Value Out Of Range";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR]="Data Model Dependency Not Established";
var SCORM2004_VERSION="1.0";
var SCORM2004_COMMENTS_FROM_LEARNER_CHILDREN="comment,location,timestamp";
var SCORM2004_COMMENTS_FROM_LMS_CHILDREN="comment,location,timestamp";
var SCORM2004_INTERACTIONS_CHILDREN="id,type,objectives,timestamp,correct_responses,weighting,learner_response,result,latency,description";
var SCORM2004_LEARNER_PREFERENCE_CHILDREN="audio_level,language,delivery_speed,audio_captioning";
var SCORM2004_OBJECTIVES_CHILDREN="progress_measure,description,score,id,success_status,completion_status";
var SCORM2004_OBJECTIVES_SCORE_CHILDREN="scaled,raw,min,max";
var SCORM2004_SCORE_CHILDREN="scaled,min,max,raw";
var SCORM2004_SHARED_DATA_CHILDREN="id,store";
function RunTimeApi(_1,_2){
this.LearnerId=_1;
this.LearnerName=_2;
this.ErrorNumber=SCORM2004_NO_ERROR;
this.ErrorString="";
this.ErrorDiagnostic="";
this.TrackedStartDate=null;
this.TrackedEndDate=null;
this.Initialized=false;
this.Terminated=false;
this.ScoCalledFinish=false;
this.RunTimeData=null;
this.LearningObject=null;
this.Activity=null;
this.IsLookAheadSequencerDataDirty=false;
this.LearnerPrefsArray=new Object();
if(SSP_ENABLED){
this.SSPApi=new SSPApi(MAX_SSP_STORAGE,this);
}
}
RunTimeApi.prototype.GetNavigationRequest=RunTimeApi_GetNavigationRequest;
RunTimeApi.prototype.ResetState=RunTimeApi_ResetState;
RunTimeApi.prototype.InitializeForDelivery=RunTimeApi_InitializeForDelivery;
RunTimeApi.prototype.SetDirtyData=RunTimeApi_SetDirtyData;
RunTimeApi.prototype.WriteHistoryLog=RunTimeApi_WriteHistoryLog;
RunTimeApi.prototype.WriteHistoryReturnValue=RunTimeApi_WriteHistoryReturnValue;
RunTimeApi.prototype.WriteAuditLog=RunTimeApi_WriteAuditLog;
RunTimeApi.prototype.WriteAuditReturnValue=RunTimeApi_WriteAuditReturnValue;
RunTimeApi.prototype.WriteDetailedLog=RunTimeApi_WriteDetailedLog;
RunTimeApi.prototype.CloseOutSession=RunTimeApi_CloseOutSession;
RunTimeApi.prototype.NeedToCloseOutSession=RunTimeApi_NeedToCloseOutSession;
RunTimeApi.prototype.AccumulateTotalTimeTracked=RunTimeApi_AccumulateTotalTrackedTime;
RunTimeApi.prototype.InitTrackedTimeStart=RunTimeApi_InitTrackedTimeStart;
function RunTimeApi_GetNavigationRequest(){
return null;
}
function RunTimeApi_ResetState(_3){
this.TrackedStartDate=null;
this.TrackedEndDate=null;
}
function RunTimeApi_InitializeForDelivery(_4){
this.RunTimeData=_4.RunTime;
this.LearningObject=_4.LearningObject;
this.Activity=_4;
if(Control.Package.Properties.ResetRunTimeDataTiming==RESET_RT_DATA_TIMING_WHEN_EXIT_IS_NOT_SUSPEND){
if(this.RunTimeData.Exit!=SCORM_EXIT_SUSPEND&&this.RunTimeData.Exit!=SCORM_EXIT_LOGOUT){
var _5={ev:"ResetRuntime",ai:_4.ItemIdentifier,at:_4.LearningObject.Title};
this.WriteHistoryLog("",_5);
this.RunTimeData.ResetState();
}
}
this.RunTimeData.Exit=SCORM_EXIT_UNKNOWN;
this.RunTimeData.NavRequest=SCORM_RUNTIME_NAV_REQUEST_NONE;
this.Initialized=false;
this.Terminated=false;
this.ScoCalledFinish=false;
this.TrackedStartDate=null;
this.TrackedEndDate=null;
this.ErrorNumber=SCORM2004_NO_ERROR;
this.ErrorString="";
this.ErrorDiagnostic="";
var _6;
var _7;
var _8;
for(var _9 in this.Activity.ActivityObjectives){
_8=this.Activity.ActivityObjectives[_9];
_6=_8.GetIdentifier();
if(_6!==null&&_6!==undefined&&_6.length>0){
_7=this.RunTimeData.FindObjectiveWithId(_6);
if(_7===null){
this.RunTimeData.AddObjective();
_7=this.RunTimeData.Objectives[this.RunTimeData.Objectives.length-1];
_7.Identifier=_6;
}
if(_8.GetProgressStatus(this.Activity,false)===true){
if(_8.GetSatisfiedStatus(this.Activity,false)===true){
_7.SuccessStatus=SCORM_STATUS_PASSED;
}else{
_7.SuccessStatus=SCORM_STATUS_FAILED;
}
}
if(_8.GetMeasureStatus(this.Activity,false)===true){
_7.ScoreScaled=_8.GetNormalizedMeasure(this.Activity,false);
}
if(_8.GetScoreRaw()!==null){
_7.ScoreRaw=_8.GetScoreRaw();
}
if(_8.GetScoreMin()!==null){
_7.ScoreMin=_8.GetScoreMin();
}
if(_8.GetScoreMax()!==null){
_7.ScoreMax=_8.GetScoreMax();
}
var _a=_8.GetCompletionStatus(this.Activity,false);
if(_a===true){
var _b=_8.GetCompletionStatusValue(this.Activity,false);
if(_b===true){
_7.CompletionStatus=SCORM_STATUS_COMPLETED;
}else{
_7.CompletionStatus=SCORM_STATUS_INCOMPLETE;
}
}
if(_8.GetProgressMeasure()!==null){
_7.ProgressMeasure=_8.GetProgressMeasure();
}
_7.SuccessStatusChangedDuringRuntime=false;
_7.MeasureChangedDuringRuntime=false;
_7.ProgressMeasureChangedDuringRuntime=false;
_7.CompletionStatusChangedDuringRuntime=false;
}
}
}
function RunTimeApi_SetDirtyData(){
this.Activity.DataState=DATA_STATE_DIRTY;
}
function RunTimeApi_WriteHistoryLog(_c,_d){
HistoryLog.WriteEventDetailed(_c,_d);
}
function RunTimeApi_WriteHistoryReturnValue(_e,_f){
HistoryLog.WriteEventDetailedReturnValue(_e,_f);
}
function RunTimeApi_WriteAuditLog(str){
Debug.WriteRteAudit(str);
}
function RunTimeApi_WriteAuditReturnValue(str){
Debug.WriteRteAuditReturnValue(str);
}
function RunTimeApi_WriteDetailedLog(str){
Debug.WriteRteDetailed(str);
}
function RunTimeApi_NeedToCloseOutSession(){
return ((this.Initialized===false)||this.ScoCalledFinish);
}
RunTimeApi.prototype.version=SCORM2004_VERSION;
RunTimeApi.prototype.Initialize=RunTimeApi_Initialize;
RunTimeApi.prototype.Terminate=RunTimeApi_Terminate;
RunTimeApi.prototype.GetValue=RunTimeApi_GetValue;
RunTimeApi.prototype.SetValue=RunTimeApi_SetValue;
RunTimeApi.prototype.Commit=RunTimeApi_Commit;
RunTimeApi.prototype.GetLastError=RunTimeApi_GetLastError;
RunTimeApi.prototype.GetErrorString=RunTimeApi_GetErrorString;
RunTimeApi.prototype.GetDiagnostic=RunTimeApi_GetDiagnostic;
RunTimeApi.prototype.RetrieveGetValueData=RunTimeApi_RetrieveGetValueData;
RunTimeApi.prototype.StoreValue=RunTimeApi_StoreValue;
RunTimeApi.prototype.SetErrorState=RunTimeApi_SetErrorState;
RunTimeApi.prototype.ClearErrorState=RunTimeApi_ClearErrorState;
RunTimeApi.prototype.CheckMaxLength=RunTimeApi_CheckMaxLength;
RunTimeApi.prototype.CheckLengthAndWarn=RunTimeApi_CheckLengthAndWarn;
RunTimeApi.prototype.CheckForInitializeError=RunTimeApi_CheckForInitializeError;
RunTimeApi.prototype.CheckForTerminateError=RunTimeApi_CheckForTerminateError;
RunTimeApi.prototype.CheckForGetValueError=RunTimeApi_CheckForGetValueError;
RunTimeApi.prototype.CheckForSetValueError=RunTimeApi_CheckForSetValueError;
RunTimeApi.prototype.CheckForCommitError=RunTimeApi_CheckForCommitError;
RunTimeApi.prototype.CheckCommentsCollectionLength=RunTimeApi_CheckCommentsCollectionLength;
RunTimeApi.prototype.CheckInteractionsCollectionLength=RunTimeApi_CheckInteractionsCollectionLength;
RunTimeApi.prototype.CheckInteractionObjectivesCollectionLength=RunTimeApi_CheckInteractionObjectivesCollectionLength;
RunTimeApi.prototype.CheckInteractionsCorrectResponsesCollectionLength=RunTimeApi_CheckInteractionsCorrectResponsesCollectionLength;
RunTimeApi.prototype.CheckObjectivesCollectionLength=RunTimeApi_CheckObjectivesCollectionLength;
RunTimeApi.prototype.LookAheadSessionClose=RunTimeApi_LookAheadSessionClose;
RunTimeApi.prototype.ValidOtheresponse=RunTimeApi_ValidOtheresponse;
RunTimeApi.prototype.ValidNumericResponse=RunTimeApi_ValidNumericResponse;
RunTimeApi.prototype.ValidSequencingResponse=RunTimeApi_ValidSequencingResponse;
RunTimeApi.prototype.ValidPerformanceResponse=RunTimeApi_ValidPerformanceResponse;
RunTimeApi.prototype.ValidMatchingResponse=RunTimeApi_ValidMatchingResponse;
RunTimeApi.prototype.ValidLikeRTResponse=RunTimeApi_ValidLikeRTResponse;
RunTimeApi.prototype.ValidLongFillInResponse=RunTimeApi_ValidLongFillInResponse;
RunTimeApi.prototype.ValidFillInResponse=RunTimeApi_ValidFillInResponse;
RunTimeApi.prototype.IsValidArrayOfLocalizedStrings=RunTimeApi_IsValidArrayOfLocalizedStrings;
RunTimeApi.prototype.IsValidArrayOfShortIdentifiers=RunTimeApi_IsValidArrayOfShortIdentifiers;
RunTimeApi.prototype.IsValidCommaDelimitedArrayOfShortIdentifiers=RunTimeApi_IsValidCommaDelimitedArrayOfShortIdentifiers;
RunTimeApi.prototype.ValidMultipleChoiceResponse=RunTimeApi_ValidMultipleChoiceResponse;
RunTimeApi.prototype.ValidTrueFalseResponse=RunTimeApi_ValidTrueFalseResponse;
RunTimeApi.prototype.ValidTimeInterval=RunTimeApi_ValidTimeInterval;
RunTimeApi.prototype.ValidTime=RunTimeApi_ValidTime;
RunTimeApi.prototype.ValidReal=RunTimeApi_ValidReal;
RunTimeApi.prototype.IsValidUrn=RunTimeApi_IsValidUrn;
RunTimeApi.prototype.ValidIdentifier=RunTimeApi_ValidIdentifier;
RunTimeApi.prototype.ValidShortIdentifier=RunTimeApi_ValidShortIdentifier;
RunTimeApi.prototype.ValidLongIdentifier=RunTimeApi_ValidLongIdentifier;
RunTimeApi.prototype.ValidLanguage=RunTimeApi_ValidLanguage;
RunTimeApi.prototype.ExtractLanguageDelimiterFromLocalizedString=RunTimeApi_ExtractLanguageDelimiterFromLocalizedString;
RunTimeApi.prototype.ValidLocalizedString=RunTimeApi_ValidLocalizedString;
RunTimeApi.prototype.ValidCharString=RunTimeApi_ValidCharString;
RunTimeApi.prototype.TranslateBooleanIntoCMI=RunTimeApi_TranslateBooleanIntoCMI;
RunTimeApi.prototype.SetLookAheadDirtyDataFlagIfNeeded=RunTimeApi_SetLookAheadDirtyDataFlagIfNeeded;
RunTimeApi.prototype.GetCompletionStatus=RunTimeApi_GetCompletionStatus;
RunTimeApi.prototype.GetSuccessStatus=RunTimeApi_GetSuccessStatus;
function RunTimeApi_Initialize(arg){
this.WriteAuditLog("`1702`"+arg+"')");
var _14={ev:"ApiInitialize"};
if(this.Activity){
_14.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_14);
var _15;
var _16;
this.ClearErrorState();
_15=this.CheckForInitializeError(arg);
if(!_15){
_16=SCORM_FALSE;
}else{
this.Initialized=true;
_16=SCORM_TRUE;
}
this.WriteAuditReturnValue(_16);
return _16;
}
function RunTimeApi_Terminate(arg){
this.WriteAuditLog("`1714`"+arg+"')");
var _18;
var _19;
var _1a;
this.ClearErrorState();
_18=this.CheckForTerminateError(arg);
var _1b=(_18&&(this.ScoCalledFinish===false));
if(!_18){
_1a=SCORM_FALSE;
}else{
var _1c={ev:"ApiTerminate"};
if(this.Activity){
_1c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_1c);
this.LookAheadSessionClose();
this.CloseOutSession();
this.Terminated=true;
this.ScoCalledFinish=true;
this.SetDirtyData();
_1a=SCORM_TRUE;
}
var _1d=Control.FindPossibleNavRequestForRuntimeNavRequest(this.RunTimeData.NavRequest);
var _1e=false;
if(_1b===true&&this.RunTimeData.NavRequest!=SCORM_RUNTIME_NAV_REQUEST_NONE&&Control.IsThereAPendingNavigationRequest()===false&&_1d.WillSucceed===true){
this.WriteDetailedLog("`449`"+Control.IsThereAPendingNavigationRequest());
window.setTimeout("Control.ScoHasTerminatedSoUnload();",150);
}else{
if(this.RunTimeData.NavRequest!=SCORM_RUNTIME_NAV_REQUEST_NONE&&_1d.WillSucceed!==true){
_1e=true;
this.WriteDetailedLog("`1320`"+this.RunTimeData.NavRequest+"`38`");
}
}
if(this.IsLookAheadSequencerDataDirty===true){
this.IsLookAheadSequencerDataDirty=false;
if(_1e===true){
window.setTimeout("Control.EvaluatePossibleNavigationRequests(true, true);",150);
}else{
window.setTimeout("Control.EvaluatePossibleNavigationRequests(true, false);",150);
}
}
Control.SignalTerminated();
this.WriteAuditReturnValue(_1a);
return _1a;
}
function RunTimeApi_GetValue(_1f){
this.WriteAuditLog("`1719`"+_1f+"')");
var _20;
var _21;
this.ClearErrorState();
_1f=CleanExternalString(_1f);
var _22=RemoveIndiciesFromCmiElement(_1f);
var _23=ExtractIndex(_1f);
var _24=ExtractSecondaryIndex(_1f);
_21=this.CheckForGetValueError(_1f,_22,_23,_24);
if(!_21){
_20="";
}else{
_20=this.RetrieveGetValueData(_1f,_22,_23,_24);
if(_20===null){
_20="";
}
}
this.WriteAuditReturnValue(_20);
return _20;
}
function RunTimeApi_SetValue(_25,_26){
this.WriteAuditLog("`1717`"+_25+"`1729`"+_26+"')");
var _27;
var _28;
this.ClearErrorState();
_25=CleanExternalString(_25);
_26=CleanExternalString(_26);
var _29=RemoveIndiciesFromCmiElement(_25);
var _2a=ExtractIndex(_25);
var _2b=ExtractSecondaryIndex(_25);
this.CheckMaxLength(_29,_26);
_27=this.CheckForSetValueError(_25,_26,_29,_2a,_2b);
if(!_27){
_28=SCORM_FALSE;
}else{
this.StoreValue(_25,_26,_29,_2a,_2b);
this.SetDirtyData();
_28=SCORM_TRUE;
}
this.WriteAuditReturnValue(_28);
return _28;
}
function RunTimeApi_Commit(arg){
this.WriteAuditLog("`1722`"+arg+"')");
var _2d;
var _2e;
this.ClearErrorState();
_2d=this.CheckForCommitError(arg);
if(!_2d){
_2e=SCORM_FALSE;
}else{
_2e=SCORM_TRUE;
}
this.LookAheadSessionClose();
if(this.IsLookAheadSequencerDataDirty===true){
this.IsLookAheadSequencerDataDirty=false;
window.setTimeout("Control.EvaluatePossibleNavigationRequests(true);",150);
}
this.WriteAuditReturnValue(_2e);
return _2e;
}
function RunTimeApi_GetLastError(){
this.WriteAuditLog("`1696`");
var _2f=this.ErrorNumber;
this.WriteAuditReturnValue(_2f);
return _2f;
}
function RunTimeApi_GetErrorString(arg){
this.WriteAuditLog("`1672`"+arg+"')");
var _31="";
if(arg===""){
_31="";
}else{
if(SCORM2004_ErrorStrings[arg]!==null&&SCORM2004_ErrorStrings[arg]!==undefined){
_31=SCORM2004_ErrorStrings[arg];
}
}
this.WriteAuditReturnValue(_31);
return _31;
}
function RunTimeApi_GetDiagnostic(arg){
this.WriteAuditLog("`1687`"+arg+"')");
var _33;
if(this.ErrorDiagnostic===""){
_33="No diagnostic information available";
}else{
_33=this.ErrorDiagnostic;
}
this.WriteAuditReturnValue(_33);
return _33;
}
function RunTimeApi_CloseOutSession(){
this.WriteDetailedLog("`1653`");
this.WriteDetailedLog("`1724`"+this.RunTimeData.Mode);
this.WriteDetailedLog("`1721`"+this.RunTimeData.Credit);
this.WriteDetailedLog("`1431`"+this.RunTimeData.CompletionStatus);
this.WriteDetailedLog("`1486`"+this.RunTimeData.SuccessStatus);
this.WriteDetailedLog("`1557`"+this.LearningObject.GetScaledPassingScore());
this.WriteDetailedLog("`1723`"+this.RunTimeData.ScoreScaled);
this.WriteDetailedLog("`1562`"+this.LearningObject.CompletedByMeasure);
if(this.LearningObject.CompletedByMeasure===true){
this.WriteDetailedLog("`1527`"+this.LearningObject.CompletionThreshold);
}
this.WriteDetailedLog("`1593`"+this.RunTimeData.ProgressMeasure);
var _34=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.SessionTime);
var _35=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTime);
var _36=_34+_35;
var _37=ConvertHundredthsToIso8601TimeSpan(_36);
this.WriteDetailedLog("`1695`"+this.RunTimeData.SessionTime+" ("+_34+"`1705`");
this.WriteDetailedLog("`1677`"+this.RunTimeData.TotalTime+" ("+_35+"`1705`");
this.WriteDetailedLog("`1673`"+_37+" ("+_36+"`1705`");
this.RunTimeData.TotalTime=_37;
this.RunTimeData.SessionTime="";
this.AccumulateTotalTimeTracked();
this.WriteDetailedLog("`1498`"+this.RunTimeData.TotalTimeTracked);
if(Control.IsThereAPendingNavigationRequest()){
if(Control.PendingNavigationRequest.Type==NAVIGATION_REQUEST_SUSPEND_ALL){
this.WriteDetailedLog("`915`");
this.RunTimeData.Exit=SCORM_EXIT_SUSPEND;
}
}else{
if(this.RunTimeData.NavRequest==SCORM_RUNTIME_NAV_REQUEST_SUSPENDALL){
this.WriteDetailedLog("`936`");
this.RunTimeData.Exit=SCORM_EXIT_SUSPEND;
}
}
if(this.RunTimeData.Exit==SCORM_EXIT_SUSPEND||this.RunTimeData.Exit==SCORM_EXIT_LOGOUT){
this.WriteDetailedLog("`1583`");
this.RunTimeData.Entry=SCORM_ENTRY_RESUME;
}
this.RunTimeData.CompletionStatus=this.GetCompletionStatus();
this.RunTimeData.SuccessStatus=this.GetSuccessStatus();
if(this.RunTimeData.Exit==SCORM_EXIT_TIME_OUT){
this.WriteDetailedLog("`1449`");
this.RunTimeData.NavRequest=SCORM_RUNTIME_NAV_REQUEST_EXITALL;
}else{
if(this.RunTimeData.Exit==SCORM_EXIT_LOGOUT){
if(Control.Package.Properties.LogoutCausesPlayerExit===true){
this.WriteDetailedLog("`1332`");
this.RunTimeData.NavRequest=SCORM_RUNTIME_NAV_REQUEST_SUSPENDALL;
}else{
this.WriteDetailedLog("`340`");
this.Activity.SetSuspended(true);
}
}else{
if(this.RunTimeData.Exit==SCORM_EXIT_SUSPEND){
this.WriteDetailedLog("`1601`");
this.Activity.SetSuspended(true);
}
}
}
return true;
}
function RunTimeApi_LookAheadSessionClose(){
this.RunTimeData.LookAheadCompletionStatus=this.GetCompletionStatus();
this.RunTimeData.LookAheadSuccessStatus=this.GetSuccessStatus();
}
function RunTimeApi_RetrieveGetValueData(_38,_39,_3a,_3b){
this.WriteDetailedLog("`1541`"+_38+", "+_39+", "+_3a+", "+_3b+") ");
var _3c;
var _3d="";
if(_38.indexOf("adl.nav.request_valid.choice.{")===0||_38.indexOf("adl.nav.request_valid.jump.{")===0){
this.WriteDetailedLog("`1202`");
var _3e=(_38.indexOf("jump")>0);
_3c=_38.substring(_38.indexOf("{"));
if(!Control.IsTargetValid(_3c)){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The target of the choice/jump request ("+_3c+") is invalid.");
return SCORM_FALSE;
}
if(_3e===true){
_3d=Control.IsJumpRequestValid(_3c);
}else{
_3d=Control.IsChoiceRequestValid(_3c);
}
if(_3d==false){
var _3f=Control.ParseTargetStringIntoActivity(_3c);
var _40="";
var _41;
if(_3e===true){
if(_3f!=null){
_40="The target activity for the jump navigation is not available.";
}else{
_40="The target activity for the jump navigation does not exist.";
}
}else{
_41=Control.FindPossibleChoiceRequestForActivity(_3f);
_40=_41.GetExceptionReason();
}
this.WriteDetailedLog(_40);
}
_3d=this.TranslateBooleanIntoCMI(_3d);
return _3d;
}
switch(_39){
case "cmi._version":
this.WriteDetailedLog("`1497`");
_3d=SCORM2004_VERSION;
break;
case "cmi.comments_from_learner._children":
this.WriteDetailedLog("`1159`");
_3d=SCORM2004_COMMENTS_FROM_LEARNER_CHILDREN;
break;
case "cmi.comments_from_learner._count":
this.WriteDetailedLog("`1226`");
_3d=this.RunTimeData.Comments.length;
break;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1206`");
_3d=this.RunTimeData.Comments[_3a].GetCommentValue();
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1195`");
_3d=this.RunTimeData.Comments[_3a].Location;
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1153`");
_3d=this.RunTimeData.Comments[_3a].Timestamp;
break;
case "cmi.comments_from_lms._children":
this.WriteDetailedLog("`1244`");
_3d=SCORM2004_COMMENTS_FROM_LMS_CHILDREN;
break;
case "cmi.comments_from_lms._count":
this.WriteDetailedLog("`1289`");
_3d=this.RunTimeData.CommentsFromLMS.length;
break;
case "cmi.comments_from_lms.n.comment":
this.WriteDetailedLog("`1279`");
_3d=this.RunTimeData.CommentsFromLMS[_3a].GetCommentValue();
break;
case "cmi.comments_from_lms.n.location":
this.WriteDetailedLog("`1266`");
_3d=this.RunTimeData.CommentsFromLMS[_3a].Location;
break;
case "cmi.comments_from_lms.n.timestamp":
this.WriteDetailedLog("`1238`");
_3d=this.RunTimeData.CommentsFromLMS[_3a].Timestamp;
break;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
_3d=this.GetCompletionStatus();
break;
case "cmi.completion_threshold":
this.WriteDetailedLog("`1367`");
_3d=this.LearningObject.CompletionThreshold;
break;
case "cmi.credit":
this.WriteDetailedLog("`1617`");
_3d=this.RunTimeData.Credit;
break;
case "cmi.entry":
this.WriteDetailedLog("`1643`");
_3d=this.RunTimeData.Entry;
break;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
Debug.AssertError("Exit element is write only");
_3d="";
break;
case "cmi.interactions._children":
this.WriteDetailedLog("`1321`");
_3d=SCORM2004_INTERACTIONS_CHILDREN;
break;
case "cmi.interactions._count":
this.WriteDetailedLog("`1380`");
_3d=this.RunTimeData.Interactions.length;
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1444`");
_3d=this.RunTimeData.Interactions[_3a].Id;
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1402`");
_3d=this.RunTimeData.Interactions[_3a].Type;
break;
case "cmi.interactions.n.objectives._count":
this.WriteDetailedLog("`1200`");
_3d=this.RunTimeData.Interactions[_3a].Objectives.length;
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1272`");
_3d=this.RunTimeData.Interactions[_3a].Objectives[_3b];
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1337`");
_3d=this.RunTimeData.Interactions[_3a].Timestamp;
break;
case "cmi.interactions.n.correct_responses._count":
this.WriteDetailedLog("`1066`");
_3d=this.RunTimeData.Interactions[_3a].CorrectResponses.length;
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`1028`");
_3d=this.RunTimeData.Interactions[_3a].CorrectResponses[_3b];
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1329`");
_3d=this.RunTimeData.Interactions[_3a].Weighting;
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1210`");
_3d=this.RunTimeData.Interactions[_3a].LearnerResponse;
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1378`");
_3d=this.RunTimeData.Interactions[_3a].Result;
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1362`");
_3d=this.RunTimeData.Interactions[_3a].Latency;
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1297`");
_3d=this.RunTimeData.Interactions[_3a].Description;
break;
case "cmi.launch_data":
this.WriteDetailedLog("`1532`");
_3d=this.LearningObject.DataFromLms;
break;
case "cmi.learner_id":
this.WriteDetailedLog("`1549`");
_3d=LearnerId;
break;
case "cmi.learner_name":
this.WriteDetailedLog("`1516`");
_3d=LearnerName;
break;
case "cmi.learner_preference._children":
this.WriteDetailedLog("`1230`");
_3d=SCORM2004_LEARNER_PREFERENCE_CHILDREN;
break;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1528`");
_3d=this.RunTimeData.AudioLevel;
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1574`");
_3d=this.RunTimeData.LanguagePreference;
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1467`");
_3d=this.RunTimeData.DeliverySpeed;
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1436`");
_3d=this.RunTimeData.AudioCaptioning;
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
_3d=this.RunTimeData.Location;
var _42={ev:"Get",k:"location",v:(_3d==null?"<null>":_3d)};
if(this.Activity){
_42.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_42);
break;
case "cmi.max_time_allowed":
this.WriteDetailedLog("`1430`");
_3d="";
if(this.LearningObject.SequencingData!==null&&this.LearningObject.SequencingData.LimitConditionAttemptAbsoluteDurationControl===true){
_3d=this.LearningObject.SequencingData.LimitConditionAttemptAbsoluteDurationLimit;
}
break;
case "cmi.mode":
this.WriteDetailedLog("`1676`");
_3d=this.RunTimeData.Mode;
break;
case "cmi.objectives._children":
this.WriteDetailedLog("`1365`");
_3d=SCORM2004_OBJECTIVES_CHILDREN;
break;
case "cmi.objectives._count":
this.WriteDetailedLog("`1407`");
_3d=this.RunTimeData.Objectives.length;
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1493`");
_3d=this.RunTimeData.Objectives[_3a].Identifier;
break;
case "cmi.objectives.n.score._children":
this.WriteDetailedLog("`1264`");
_3d=SCORM2004_OBJECTIVES_SCORE_CHILDREN;
break;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1303`");
_3d=this.RunTimeData.Objectives[_3a].ScoreScaled;
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1359`");
_3d=this.RunTimeData.Objectives[_3a].ScoreRaw;
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1357`");
_3d=this.RunTimeData.Objectives[_3a].ScoreMin;
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1358`");
_3d=this.RunTimeData.Objectives[_3a].ScoreMax;
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1275`");
_3d=this.RunTimeData.Objectives[_3a].SuccessStatus;
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1221`");
_3d=this.RunTimeData.Objectives[_3a].CompletionStatus;
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1242`");
_3d=this.RunTimeData.Objectives[_3a].ProgressMeasure;
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1324`");
_3d=this.RunTimeData.Objectives[_3a].Description;
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1427`");
_3d=this.RunTimeData.ProgressMeasure;
break;
case "cmi.scaled_passing_score":
this.WriteDetailedLog("`1368`");
_3d=this.LearningObject.GetScaledPassingScore();
if(_3d===null){
_3d="";
}
break;
case "cmi.score._children":
this.WriteDetailedLog("`1446`");
_3d=SCORM2004_SCORE_CHILDREN;
break;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
_3d=this.RunTimeData.ScoreScaled;
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
_3d=this.RunTimeData.ScoreRaw;
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
_3d=this.RunTimeData.ScoreMax;
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
_3d=this.RunTimeData.ScoreMin;
break;
case "cmi.session_time":
this.WriteDetailedLog("`1505`");
_3d="";
break;
case "cmi.success_status":
this.WriteDetailedLog("`1470`");
_3d=this.GetSuccessStatus();
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1518`");
_3d=this.RunTimeData.SuspendData;
break;
case "cmi.time_limit_action":
this.WriteDetailedLog("`1419`");
_3d=this.LearningObject.TimeLimitAction;
break;
case "cmi.total_time":
this.WriteDetailedLog("`1547`");
_3d=this.RunTimeData.TotalTime;
break;
case "adl.nav.request":
this.WriteDetailedLog("`1441`");
_3d=this.RunTimeData.NavRequest;
break;
case "adl.nav.request_valid.continue":
this.WriteDetailedLog("`1168`");
_3d=Control.IsContinueRequestValid();
if(_3d==false){
var _43=Control.GetPossibleContinueRequest();
this.WriteDetailedLog(_43.GetExceptionReason());
}
_3d=this.TranslateBooleanIntoCMI(_3d);
break;
case "adl.nav.request_valid.previous":
this.WriteDetailedLog("`1193`");
_3d=Control.IsPreviousRequestValid();
if(_3d==false){
var _43=Control.GetPossiblePreviousRequest();
this.WriteDetailedLog(_43.GetExceptionReason());
}
_3d=this.TranslateBooleanIntoCMI(_3d);
break;
case "adl.nav.request_valid.choice":
this.WriteDetailedLog("`1335`");
Debug.AssertError("Entered invalid case in RunTimeApi_RetrieveGetValueData (choice)");
break;
case "adl.nav.request_valid.jump":
this.WriteDetailedLog("`1355`");
Debug.AssertError("Entered invalid case in RunTimeApi_RetrieveGetValueData (jump)");
break;
case "adl.data._children":
this.WriteDetailedLog("`1390`");
_3d=SCORM2004_SHARED_DATA_CHILDREN;
break;
case "adl.data._count":
this.WriteDetailedLog("`1451`");
_3d=this.LearningObject.SharedDataMaps.length;
break;
case "adl.data.n.id":
this.WriteDetailedLog("`1529`");
_3d=this.LearningObject.SharedDataMaps[_3a].Id;
break;
case "adl.data.n.store":
this.WriteDetailedLog("`1481`");
var id=this.LearningObject.SharedDataMaps[_3a].Id;
for(var idx=0;idx<RegistrationToDeliver.SharedData.length;idx++){
if(RegistrationToDeliver.SharedData[idx].SharedDataId==id){
_3d=RegistrationToDeliver.SharedData[idx].GetData();
break;
}
}
break;
default:
if(_39.indexOf("ssp")===0&&SSP_ENABLED){
_3d=this.SSPApi.RetrieveGetValueData(_38,_39,_3a,_3b);
}else{
Debug.AssertError("Entered default case in RunTimeApi_RetrieveGetValueData");
_3d="";
}
break;
}
return _3d;
}
function RunTimeApi_StoreValue(_46,_47,_48,_49,_4a){
this.WriteDetailedLog("`1707`"+_46+", "+_47+", "+_48+", "+_49+", "+_4a+") ");
var _4b=true;
switch(_48){
case "cmi._version":
this.WriteDetailedLog("`1572`");
break;
case "cmi.comments_from_learner._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_learner._children");
_4b=false;
break;
case "cmi.comments_from_learner._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_learner._count");
_4b=false;
break;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1228`");
this.CheckCommentsCollectionLength(_49);
this.RunTimeData.Comments[_49].SetCommentValue(_47);
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1179`");
this.CheckCommentsCollectionLength(_49);
this.RunTimeData.Comments[_49].Location=_47;
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1165`");
this.CheckCommentsCollectionLength(_49);
this.RunTimeData.Comments[_49].Timestamp=_47;
break;
case "cmi.comments_from_lms._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms._children");
_4b=false;
break;
case "cmi.comments_from_lms._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms._count");
_4b=false;
break;
case "cmi.comments_from_lms.n.comment":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms.comment");
_4b=false;
break;
case "cmi.comments_from_lms.n.location":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms.location");
_4b=false;
break;
case "cmi.comments_from_lms.n.timestamp":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms.timestamp");
_4b=false;
break;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
var _4c={ev:"Set",k:"completion",v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.CompletionStatus,_47);
this.RunTimeData.CompletionStatus=_47;
this.RunTimeData.CompletionStatusChangedDuringRuntime=true;
break;
case "cmi.completion_threshold":
Debug.AssertError("ERROR - Element is Read Only, cmi.completion_threshold");
_4b=false;
break;
case "cmi.credit":
Debug.AssertError("ERROR - Element is Read Only, cmi.credit");
_4b=false;
break;
case "cmi.entry":
Debug.AssertError("ERROR - Element is Read Only, cmi.entry");
_4b=false;
break;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
var _4c={ev:"Set",k:"cmi.exit",v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.RunTimeData.Exit=_47;
break;
case "cmi.interactions._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions._children");
_4b=false;
break;
case "cmi.interactions._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions._count");
_4b=false;
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1444`");
var _4c={ev:"Set",k:"interactions id",i:_49,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.CheckInteractionsCollectionLength(_49);
this.RunTimeData.Interactions[_49].Id=_47;
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1402`");
var _4c={ev:"Set",k:"interactions type",i:_49,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_49].Id){
_4c.intid=this.RunTimeData.Interactions[_49].Id;
}
this.WriteHistoryLog("",_4c);
this.CheckInteractionsCollectionLength(_49);
this.RunTimeData.Interactions[_49].Type=_47;
break;
case "cmi.interactions.n.objectives._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions.n.objectives._count");
_4b=false;
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1272`");
var _4c={ev:"Set",k:"interactions objectives id",i:_49,si:_4a,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.CheckInteractionObjectivesCollectionLength(_49,_4a);
this.RunTimeData.Interactions[_49].Objectives[_4a]=_47;
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1337`");
var _4c={ev:"Set",k:"interactions timestamp",i:_49,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_49].Id){
_4c.intid=this.RunTimeData.Interactions[_49].Id;
}
this.WriteHistoryLog("",_4c);
this.CheckInteractionsCollectionLength(_49);
this.RunTimeData.Interactions[_49].Timestamp=_47;
break;
case "cmi.interactions.n.correct_responses._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions.n.correct_responses._count");
_4b=false;
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`1028`");
var _4c={ev:"Set",k:"interactions correct_responses pattern",i:_49,si:_4a,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.CheckInteractionsCorrectResponsesCollectionLength(_49,_4a);
this.RunTimeData.Interactions[_49].CorrectResponses[_4a]=_47;
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1329`");
this.CheckInteractionsCollectionLength(_49);
this.RunTimeData.Interactions[_49].Weighting=_47;
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1210`");
var _4c={ev:"Set",k:"interactions learner_response",i:_49,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_49].Id){
_4c.intid=this.RunTimeData.Interactions[_49].Id;
}
this.WriteHistoryLog("",_4c);
this.CheckInteractionsCollectionLength(_49);
this.RunTimeData.Interactions[_49].LearnerResponse=_47;
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1378`");
var _4c={ev:"Set",k:"interactions result",i:_49,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_49].Id){
_4c.intid=this.RunTimeData.Interactions[_49].Id;
}
this.WriteHistoryLog("",_4c);
this.CheckInteractionsCollectionLength(_49);
this.RunTimeData.Interactions[_49].Result=_47;
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1362`");
var _4c={ev:"Set",k:"interactions latency",i:_49,vh:ConvertIso8601TimeSpanToHundredths(_47)};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_49].Id){
_4c.intid=this.RunTimeData.Interactions[_49].Id;
}
this.WriteHistoryLog("",_4c);
this.CheckInteractionsCollectionLength(_49);
this.RunTimeData.Interactions[_49].Latency=_47;
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1297`");
var _4c={ev:"Set",k:"interactions description",i:_49,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_49].Id){
_4c.intid=this.RunTimeData.Interactions[_49].Id;
}
this.WriteHistoryLog("",_4c);
this.CheckInteractionsCollectionLength(_49);
this.RunTimeData.Interactions[_49].Description=_47;
break;
case "cmi.launch_data":
Debug.AssertError("ERROR - Element is Read Only, cmi.launch_data");
_4b=false;
break;
case "cmi.learner_id":
Debug.AssertError("ERROR - Element is Read Only, cmi.learner_id");
_4b=false;
break;
case "cmi.learner_name":
Debug.AssertError("ERROR - Element is Read Only, cmi.learner_name");
_4b=false;
break;
case "cmi.learner_preference._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.learner_preference._children");
_4b=false;
break;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1509`");
this.RunTimeData.AudioLevel=_47;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.AudioLevel=_47;
}
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1574`");
this.RunTimeData.LanguagePreference=_47;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.LanguagePreference=_47;
}
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1483`");
this.RunTimeData.DeliverySpeed=_47;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.DeliverySpeed=_47;
}
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1436`");
this.RunTimeData.AudioCaptioning=_47;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.AudioCaptioning=_47;
}
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
this.RunTimeData.Location=_47;
break;
case "cmi.max_time_allowed":
Debug.AssertError("ERROR - Element is Read Only, cmi.max_time_allowed");
_4b=false;
break;
case "cmi.mode":
Debug.AssertError("ERROR - Element is Read Only, cmi.mode");
_4b=false;
break;
case "cmi.objectives._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.objectives._children");
_4b=false;
break;
case "cmi.objectives._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.objectives._count");
_4b=false;
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1493`");
var _4c={ev:"Set",k:"objectives id",i:_49,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.CheckObjectivesCollectionLength(_49);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_49].Identifier,_47);
this.RunTimeData.Objectives[_49].Identifier=_47;
break;
case "cmi.objectives.n.score._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.objectives.n.score._children");
_4b=false;
break;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1311`");
this.CheckObjectivesCollectionLength(_49);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_49].ScoreScaled,_47);
this.RunTimeData.Objectives[_49].ScoreScaled=_47;
this.RunTimeData.Objectives[_49].MeasureChangedDuringRuntime=true;
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1359`");
this.CheckObjectivesCollectionLength(_49);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_49].ScoreRaw,_47);
this.RunTimeData.Objectives[_49].ScoreRaw=_47;
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1357`");
this.CheckObjectivesCollectionLength(_49);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_49].ScoreMin,_47);
this.RunTimeData.Objectives[_49].ScoreMin=_47;
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1358`");
this.CheckObjectivesCollectionLength(_49);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_49].ScoreMax,_47);
this.RunTimeData.Objectives[_49].ScoreMax=_47;
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1275`");
var _4c={ev:"Set",k:"objectives success",i:_49,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Objectives[_49].Identifier){
_4c.intid=this.RunTimeData.Objectives[_49].Identifier;
}
this.WriteHistoryLog("",_4c);
this.CheckObjectivesCollectionLength(_49);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_49].SuccessStatus,_47);
this.RunTimeData.Objectives[_49].SuccessStatus=_47;
this.RunTimeData.Objectives[_49].SuccessStatusChangedDuringRuntime=true;
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1221`");
var _4c={ev:"Set",k:"objectives completion",i:_49,v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Objectives[_49].Identifier){
_4c.intid=this.RunTimeData.Objectives[_49].Identifier;
}
this.WriteHistoryLog("",_4c);
this.CheckObjectivesCollectionLength(_49);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_49].CompletionStatus,_47);
this.RunTimeData.Objectives[_49].CompletionStatus=_47;
this.RunTimeData.Objectives[_49].CompletionStatusChangedDuringRuntime=true;
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1242`");
this.CheckObjectivesCollectionLength(_49);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_49].ProgressMeasure,_47);
this.RunTimeData.Objectives[_49].ProgressMeasure=_47;
this.RunTimeData.Objectives[_49].ProgressMeasureChangedDuringRuntime=true;
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1324`");
this.CheckObjectivesCollectionLength(_49);
this.RunTimeData.Objectives[_49].Description=_47;
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1426`");
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.ProgressMeasure,_47);
this.RunTimeData.ProgressMeasure=_47;
break;
case "cmi.scaled_passing_score":
Debug.AssertError("ERROR - Element is Read Only, cmi.scaled_passing_score");
_4b=false;
break;
case "cmi.score._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.score._children");
_4b=false;
break;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
var _4c={ev:"Set",k:"score.scaled",v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.ScoreScaled,_47);
this.RunTimeData.ScoreScaled=_47;
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
var _4c={ev:"Set",k:"score.raw",v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.RunTimeData.ScoreRaw=_47;
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
var _4c={ev:"Set",k:"score.max",v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.RunTimeData.ScoreMax=_47;
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
var _4c={ev:"Set",k:"score.min",v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.RunTimeData.ScoreMin=_47;
break;
case "cmi.session_time":
this.WriteDetailedLog("`1515`");
var _4c={ev:"Set",k:"session time",vh:ConvertIso8601TimeSpanToHundredths(_47)};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.RunTimeData.SessionTime=_47;
break;
case "cmi.success_status":
this.WriteDetailedLog("`1480`");
var _4c={ev:"Set",k:"success",v:_47};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.SuccessStatus,_47);
this.RunTimeData.SuccessStatus=_47;
this.RunTimeData.SuccessStatusChangedDuringRuntime=true;
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1513`");
this.RunTimeData.SuspendData=_47;
break;
case "cmi.time_limit_action":
Debug.AssertError("ERROR - Element is Read Only, cmi.time_limit_action");
_4b=false;
break;
case "cmi.total_time":
Debug.AssertError("ERROR - Element is Read Only, cmi.total_time");
_4b=false;
break;
case "adl.nav.request":
this.WriteDetailedLog("`1455`");
var _4c={ev:"Set",k:"nav.request"};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
var _4d=_47.match(/target\s*=\s*(\S+)\s*}\s*(choice|jump)/);
if(_4d){
_4c.tai=_4d[1];
_4c.tat=Control.Activities.GetActivityFromIdentifier(_4d[1]).LearningObject.Title;
_4c.v=_4d[2];
}else{
_4c.v=_47;
}
this.WriteHistoryLog("",_4c);
this.RunTimeData.NavRequest=_47;
break;
case "adl.nav.request_valid.continue":
Debug.AssertError("ERROR - Element is Read Only, adl.nav.request_valid.continue");
break;
case "adl.nav.request_valid.previous":
Debug.AssertError("ERROR - Element is Read Only, adl.nav.request_valid.previous");
break;
case "adl.nav.request_valid.choice":
Debug.AssertError("ERROR - Should never get here...handled above - Element is Read Only, adl.nav.request_valid.choice");
break;
case "adl.nav.request_valid.jump":
Debug.AssertError("ERROR - Should never get here...handled above - Element is Read Only, adl.nav.request_valid.jump");
break;
case "adl.data._children":
Debug.AssertError("ERROR - Element is Read Only, adl.data._children");
_4b=false;
break;
case "adl.data._count":
Debug.AssertError("ERROR - Element is Read Only, adl.data._count");
_4b=false;
break;
case "adl.data.n.id":
Debug.AssertError("ERROR - Element is Read Only, adl.data.n.id");
_4b=false;
break;
case "adl.data.n.store":
this.WriteDetailedLog("`1464`");
var id=this.LearningObject.SharedDataMaps[_49].Id;
for(var idx=0;idx<RegistrationToDeliver.SharedData.length;idx++){
if(RegistrationToDeliver.SharedData[idx].SharedDataId==id){
RegistrationToDeliver.SharedData[idx].WriteData(_47);
break;
}
}
break;
default:
if(_48.indexOf("ssp")===0){
if(SSP_ENABLED){
return this.SSPApi.StoreValue(_46,_47,_48,_49,_4a);
}
}
Debug.AssertError("ERROR reached default case in RunTimeApi_StoreValue");
returnData="";
break;
}
}
function RunTimeApi_CheckCommentsCollectionLength(_50){
if(this.RunTimeData.Comments.length<=_50){
this.WriteDetailedLog("`1130`"+_50);
this.RunTimeData.Comments[_50]=new ActivityRunTimeComment(null,null,null,null,null);
}
}
function RunTimeApi_CheckInteractionsCollectionLength(_51){
if(this.RunTimeData.Interactions.length<=_51){
this.WriteDetailedLog("`1299`"+_51);
this.RunTimeData.Interactions[_51]=new ActivityRunTimeInteraction(null,null,null,null,null,null,null,null,null,new Array(),new Array());
}
}
function RunTimeApi_CheckInteractionObjectivesCollectionLength(_52,_53){
if(this.RunTimeData.Interactions[_52].Objectives.length<=_53){
this.WriteDetailedLog("`1110`"+_53);
this.RunTimeData.Interactions[_52].Objectives[_53]=null;
}
}
function RunTimeApi_CheckInteractionsCorrectResponsesCollectionLength(_54,_55){
if(this.RunTimeData.Interactions[_54].CorrectResponses.length<=_55){
this.WriteDetailedLog("`990`"+_55);
this.RunTimeData.Interactions[_54].CorrectResponses[_55]=null;
}
}
function RunTimeApi_CheckObjectivesCollectionLength(_56){
if(this.RunTimeData.Objectives.length<=_56){
this.WriteDetailedLog("`1342`"+_56);
this.RunTimeData.Objectives[_56]=new ActivityRunTimeObjective(null,"unknown","unknown",null,null,null,null,null,null);
}
}
function RunTimeApi_SetErrorState(_57,_58){
if(_57!=SCORM2004_NO_ERROR){
this.WriteDetailedLog("`1281`"+_57+" - "+_58);
}
this.ErrorNumber=_57;
this.ErrorString=SCORM2004_ErrorStrings[_57];
this.ErrorDiagnostic=_58;
}
function RunTimeApi_ClearErrorState(){
this.SetErrorState(SCORM2004_NO_ERROR,"");
}
function RunTimeApi_CheckForInitializeError(arg){
this.WriteDetailedLog("`1420`");
if(this.Initialized){
this.SetErrorState(SCORM2004_ALREADY_INTIAILIZED_ERROR,"Initialize has already been called and may only be called once per session.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_CONTENT_INSTANCE_TERMINATED_ERROR,"Initialize cannot be called after Terminate has already beeen called.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM2004_GENERAL_ARGUMENT_ERROR,"The argument to Initialize must be an empty string (\"\"). The argument '"+arg+"' is invalid.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForTerminateError(arg){
this.WriteDetailedLog("`1437`");
if(!this.Initialized){
this.SetErrorState(SCORM2004_TERMINATION_BEFORE_INITIALIZATION_ERROR,"Terminate cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_TERMINATION_AFTER_TERMINATION_ERROR,"Terminate cannot be called after Terminate has already beeen called.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM2004_GENERAL_ARGUMENT_ERROR,"The argument to Terminate must be an empty string (\"\"). The argument '"+arg+"' is invalid.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForCommitError(arg){
this.WriteDetailedLog("`1487`");
if(!this.Initialized){
this.SetErrorState(SCORM2004_COMMIT_BEFORE_INITIALIZATION_ERROR,"Commit cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_COMMIT_AFTER_TERMINATION_ERROR,"Commit cannot be called after Terminate has already beeen called.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM2004_GENERAL_ARGUMENT_ERROR,"The argument to Commit must be an empty string (\"\"). The argument '"+arg+"' is invalid.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckMaxLength(_5c,_5d){
switch(_5c){
case "`1304`":
this.CheckLengthAndWarn(_5d,4250);
break;
case "cmi.comments_from_learner.n.location":
this.CheckLengthAndWarn(_5d,250);
break;
case "cmi.interactions.n.id":
this.CheckLengthAndWarn(_5d,4000);
break;
case "cmi.interactions.n.objectives.n.id":
this.CheckLengthAndWarn(_5d,4000);
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.CheckLengthAndWarn(_5d,7800);
break;
case "cmi.interactions.n.learner_response":
this.CheckLengthAndWarn(_5d,7800);
break;
case "cmi.interactions.n.description":
this.CheckLengthAndWarn(_5d,500);
break;
case "cmi.learner_preference.language":
this.CheckLengthAndWarn(_5d,250);
break;
case "cmi.location":
this.CheckLengthAndWarn(_5d,1000);
break;
case "cmi.objectives.n.id":
this.CheckLengthAndWarn(_5d,4000);
break;
case "cmi.objectives.n.description":
this.CheckLengthAndWarn(_5d,500);
break;
case "cmi.suspend_data":
this.CheckLengthAndWarn(_5d,64000);
break;
default:
break;
}
return;
}
function RunTimeApi_CheckLengthAndWarn(str,len){
if(str.length>len){
this.SetErrorState(SCORM2004_NO_ERROR,"The string was trimmed to fit withing the SPM of "+len+" characters.");
}
return;
}
function RunTimeApi_CheckForGetValueError(_60,_61,_62,_63){
this.WriteDetailedLog("`1454`");
if(!this.Initialized){
this.SetErrorState(SCORM2004_RETRIEVE_DATA_BEFORE_INITIALIZATION_ERROR,"GetValue cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_RETRIEVE_DATA_AFTER_TERMINATION_ERROR,"GetValue cannot be called after Terminate has already beeen called.");
return false;
}
if(_60.length===0){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The data model element for GetValue was not specified.");
return false;
}
if(_62!==""){
if(_61.indexOf("cmi.comments_from_learner")>=0){
if(_62>=this.RunTimeData.Comments.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Comments From Learner collection does not have an element at index "+_62+", the current element count is "+this.RunTimeData.Comments.length+".");
return false;
}
}else{
if(_61.indexOf("cmi.comments_from_lms")>=0){
if(_62>=this.RunTimeData.CommentsFromLMS.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Comments From LMS collection does not have an element at index "+_62+", the current element count is "+this.RunTimeData.CommentsFromLMS.length+".");
return false;
}
}else{
if(_61.indexOf("cmi.objectives")>=0){
if(_62>=this.RunTimeData.Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Objectives collection does not have an element at index "+_62+", the current element count is "+this.RunTimeData.Objectives.length+".");
return false;
}
}else{
if(_61.indexOf("cmi.interactions")>=0){
if(_62>=this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Interactions collection does not have an element at index "+_62+", the current element count is "+this.RunTimeData.Interactions.length+".");
return false;
}
if(_61.indexOf("cmi.interactions.n.correct_responses")>=0){
if(_63!==""){
if(_63>=this.RunTimeData.Interactions[_62].CorrectResponses.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Correct Responses collection for Interaction #"+_62+" does not have "+_63+" elements in it, the current element count is "+this.RunTimeData.Interactions[_62].CorrectResponses.length+".");
return false;
}
}
}else{
if(_61.indexOf("cmi.interactions.n.objectives")>=0){
if(_63!==""){
if(_63>=this.RunTimeData.Interactions[_62].Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Objectives collection for Interaction #"+_62+" does not have "+_63+" elements in it, the current element count is "+this.RunTimeData.Interactions[_62].Objectives.length+".");
return false;
}
}
}
}
}else{
if(_61.indexOf("adl.data")>=0){
if(_62>=this.LearningObject.SharedDataMaps.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Shared Data collection does not have an element at index "+_62+", the current element count is "+this.LearningObject.SharedDataMaps.length+".");
return false;
}
}
}
}
}
}
}
switch(_61){
case "cmi._version":
this.WriteDetailedLog("`1600`");
break;
case "cmi.comments_from_learner._children":
this.WriteDetailedLog("`1196`");
break;
case "cmi.comments_from_learner._count":
this.WriteDetailedLog("`1226`");
break;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1154`");
if(this.RunTimeData.Comments[_62].Comment===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Comment field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1139`");
if(this.RunTimeData.Comments[_62].Location===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Location field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1113`");
if(this.RunTimeData.Comments[_62].Timestamp===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The TimeStamp field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.comments_from_lms._children":
this.WriteDetailedLog("`1244`");
break;
case "cmi.comments_from_lms._count":
this.WriteDetailedLog("`1289`");
break;
case "cmi.comments_from_lms.n.comment":
this.WriteDetailedLog("`1252`");
if(this.RunTimeData.CommentsFromLMS[_62].Comment===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Comment field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.comments_from_lms.n.location":
this.WriteDetailedLog("`1235`");
if(this.RunTimeData.CommentsFromLMS[_62].Location===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Location field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.comments_from_lms.n.timestamp":
this.WriteDetailedLog("`1218`");
if(this.RunTimeData.CommentsFromLMS[_62].Timestamp===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Timestamp field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
break;
case "cmi.completion_threshold":
this.WriteDetailedLog("`1363`");
if(this.LearningObject.CompletedByMeasure===false||this.LearningObject.CompletionThreshold===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The completion threshold for this SCO was not specificed.");
return false;
}
break;
case "cmi.credit":
this.WriteDetailedLog("`1617`");
break;
case "cmi.entry":
this.WriteDetailedLog("`1643`");
break;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR,"The Exit data model element is write-only.");
return false;
case "cmi.interactions._children":
this.WriteDetailedLog("`1321`");
break;
case "cmi.interactions._count":
this.WriteDetailedLog("`1380`");
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1414`");
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1371`");
if(this.RunTimeData.Interactions[_62].Type===null||this.RunTimeData.Interactions[_62].Type===""){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Type field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.interactions.n.objectives._count":
this.WriteDetailedLog("`1146`");
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1199`");
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1294`");
if(this.RunTimeData.Interactions[_62].Timestamp===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Time Stamp field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.interactions.n.correct_responses._count":
this.WriteDetailedLog("`999`");
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`973`");
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1295`");
if(this.RunTimeData.Interactions[_62].Weighting===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Weighting field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1180`");
if(this.RunTimeData.Interactions[_62].LearnerResponse===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Learner Response field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1348`");
if(this.RunTimeData.Interactions[_62].Result===null||this.RunTimeData.Interactions[_62].Result===""){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Result field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1323`");
if(this.RunTimeData.Interactions[_62].Latency===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Latency field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1270`");
if(this.RunTimeData.Interactions[_62].Description===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Description field has not been initialized for the element at index "+_62);
return false;
}
break;
case "cmi.launch_data":
this.WriteDetailedLog("`1532`");
if(this.LearningObject.DataFromLms===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Launch Data field was not specified for this SCO.");
return false;
}
break;
case "cmi.learner_id":
this.WriteDetailedLog("`1542`");
break;
case "cmi.learner_name":
this.WriteDetailedLog("`1519`");
break;
case "cmi.learner_preference._children":
this.WriteDetailedLog("`1230`");
break;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1181`");
if(this.RunTimeData.AudioLevel===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Audio Level field has not been set for this SCO.");
return false;
}
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1241`");
if(this.RunTimeData.LanguagePreference===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Language Preference field has not been set for this SCO.");
return false;
}
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1109`");
if(this.RunTimeData.DeliverySpeed===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Delivery Speed field has not been set for this SCO.");
return false;
}
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1075`");
if(this.RunTimeData.AudioCaptioning===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Audio Captioning field has not been set for this SCO.");
return false;
}
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
if(this.RunTimeData.Location===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Location field has not been set for this SCO.");
return false;
}
break;
case "cmi.max_time_allowed":
this.WriteDetailedLog("`1439`");
if(this.LearningObject.SequencingData===null||this.LearningObject.SequencingData.LimitConditionAttemptAbsoluteDurationControl===false){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Max Time Allowed field was not specified in the manifest for this SCO.");
return false;
}
break;
case "cmi.mode":
this.WriteDetailedLog("`1676`");
break;
case "cmi.objectives._children":
this.WriteDetailedLog("`1365`");
break;
case "cmi.objectives._count":
this.WriteDetailedLog("`1407`");
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1445`");
break;
case "cmi.objectives.n.score._children":
this.WriteDetailedLog("`1229`");
break;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1282`");
if(this.RunTimeData.Objectives[_62].ScoreScaled===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Scaled Score field has not been initialized for the objective at index "+_62);
return false;
}
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1334`");
if(this.RunTimeData.Objectives[_62].ScoreRaw===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Raw Score field has not been initialized for the objective at index "+_62);
return false;
}
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1318`");
if(this.RunTimeData.Objectives[_62].ScoreMin===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Min Score field has not been initialized for the objective at index "+_62);
return false;
}
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1316`");
if(this.RunTimeData.Objectives[_62].ScoreMax===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Max Score field has not been initialized for the objective at index "+_62);
return false;
}
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1251`");
if(this.RunTimeData.Objectives[_62].SuccessStatus===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The SuccessStatus field has not been initialized for the objective at index "+_62);
return false;
}
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1197`");
if(this.RunTimeData.Objectives[_62].CompletionStatus===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The CompletionStatus field has not been initialized for the objective at index "+_62);
return false;
}
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1204`");
if(this.RunTimeData.Objectives[_62].ProgressMeasure===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The ProgressMeasure field has not been initialized for the objective at index "+_62);
return false;
}
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1290`");
if(this.RunTimeData.Objectives[_62].Description===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Description field has not been initialized for the objective at index "+_62);
return false;
}
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1426`");
if(this.RunTimeData.ProgressMeasure===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Progress Measure field has not been set for this SCO.");
return false;
}
break;
case "cmi.scaled_passing_score":
this.WriteDetailedLog("`1366`");
if(this.LearningObject.GetScaledPassingScore()===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Scaled Passing Score field was not specificed for this SCO.");
return false;
}
break;
case "cmi.score._children":
this.WriteDetailedLog("`1446`");
break;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
if(this.RunTimeData.ScoreScaled===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Scaled Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
if(this.RunTimeData.ScoreRaw===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Raw Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
if(this.RunTimeData.ScoreMax===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Max Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
if(this.RunTimeData.ScoreMin===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Min Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.session_time":
this.WriteDetailedLog("`1515`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR,"The Exit data model element is write-only.");
return false;
break;
case "cmi.success_status":
this.WriteDetailedLog("`1480`");
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1513`");
if(this.RunTimeData.SuspendData===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Suspend Data field has not been set for this SCO.");
return false;
}
break;
case "cmi.time_limit_action":
this.WriteDetailedLog("`1411`");
if(this.LearningObject.TimeLimitAction===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Time Limit Action field has not been set for this SCO.");
return false;
}
break;
case "cmi.total_time":
this.WriteDetailedLog("`1545`");
break;
case "adl.nav.request":
this.WriteDetailedLog("`1455`");
break;
case "adl.nav.request_valid.continue":
this.WriteDetailedLog("`1176`");
break;
case "adl.nav.request_valid.previous":
this.WriteDetailedLog("`1183`");
break;
case "adl.nav.request_valid.choice":
Debug.AssertError("entered invalid case in scorm2004 check for getvalue error (choice)");
break;
case "adl.nav.request_valid.jump":
Debug.AssertError("entered invalid case in scorm2004 check for getvalue error (jump)");
break;
case "adl.data._children":
this.WriteDetailedLog("`1390`");
break;
case "adl.data._count":
this.WriteDetailedLog("`1451`");
break;
case "adl.data.n.id":
this.WriteDetailedLog("`1485`");
break;
case "adl.data.n.store":
this.WriteDetailedLog("`1435`");
if(this.LearningObject.SharedDataMaps[_62].ReadSharedData==false){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR,"The specified shared data is write-only.");
return false;
}
var id=this.LearningObject.SharedDataMaps[_62].Id;
for(var idx=0;idx<RegistrationToDeliver.SharedData.length;idx++){
if(RegistrationToDeliver.SharedData[idx].SharedDataId==id){
var _66=RegistrationToDeliver.SharedData[idx].GetData();
break;
}
}
if(_66===null||_66===""){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The store field has not been initialized for the element at index "+_62);
return false;
}
break;
default:
if(_61.indexOf("adl.nav.request_valid.choice.{")===0){
this.WriteDetailedLog("`1587`");
return true;
}
if(_61.indexOf("adl.nav.request_valid.jump.{")===0){
this.WriteDetailedLog("`1587`");
return true;
}
if(_61.indexOf("ssp")===0){
if(SSP_ENABLED){
return this.SSPApi.CheckForGetValueError(_60,_61,_62,_63);
}
}
this.SetErrorState(SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR,"The data model element '"+_60+"' does not exist.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForSetValueError(_67,_68,_69,_6a,_6b){
this.WriteDetailedLog("`1526`"+_67+", "+_68+", "+_69+", "+_6a+", "+_6b+") ");
if(!this.Initialized){
this.SetErrorState(SCORM2004_STORE_DATA_BEFORE_INITIALIZATION_ERROR,"SetValue cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_STORE_DATA_AFTER_TERMINATION_ERROR,"SetValue cannot be called after Terminate has already beeen called.");
return false;
}
if(_67.length===0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The data model element for SetValue was not specified.");
return false;
}
if(_67.indexOf("adl.nav.request_valid.choice.{")===0){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.nav.request_valid.choice element is read only");
return false;
}
if(_67.indexOf("adl.nav.request_valid.jump.{")===0){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.nav.request_valid.jump element is read only");
return false;
}
if(_6a!==""){
if(_69.indexOf("cmi.comments_from_learner")>=0){
if(_6a>this.RunTimeData.Comments.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Comments From Learner collection elements must be set sequentially, the index "+_6a+", is greater than the next available index of "+this.RunTimeData.Comments.length+".");
return false;
}
}else{
if(_69.indexOf("cmi.comments_from_lms")>=0){
if(_6a>this.RunTimeData.CommentsFromLMS.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Comments From LMS collection elements must be set sequentially, the index "+_6a+", is greater than the next available index of "+this.RunTimeData.CommentsFromLMS.length+".");
return false;
}
}else{
if(_69.indexOf("cmi.objectives")>=0){
if(_6a>this.RunTimeData.Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Objectives collection elements must be set sequentially, the index "+_6a+", is greater than the next available index of "+this.RunTimeData.Objectives.length+".");
return false;
}
}else{
if(_69.indexOf("cmi.interactions")>=0){
if(_6a>this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Interactions collection elements must be set sequentially, the index "+_6a+", is greater than the next available index of "+this.RunTimeData.Interactions.length+".");
return false;
}else{
if(_69.indexOf("cmi.interactions.n.correct_responses")>=0){
if(_6a>=this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The Interactions collection elements must be set sequentially, the index "+_6a+", is greater than the next available index of "+this.RunTimeData.Interactions.length+".");
return false;
}
if(_6b!==""){
if(_6b>this.RunTimeData.Interactions[_6a].CorrectResponses.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Correct Responses collection elements for Interaction #"+_6a+" must be set sequentially the index "+_6b+" is greater than the next available index of "+this.RunTimeData.Interactions[_6a].CorrectResponses.length+".");
return false;
}
}
}else{
if(_69.indexOf("cmi.interactions.n.objectives")>=0){
if(_6a>=this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The Interactions collection elements must be set sequentially, the index "+_6a+", is greater than the next available index of "+this.RunTimeData.Interactions.length+".");
return false;
}
if(_6b!==""){
if(_6b>this.RunTimeData.Interactions[_6a].Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Objectives collection elements for Interaction #"+_6a+" must be set sequentially the index "+_6b+" is greater than the next available index of "+this.RunTimeData.Interactions[_6a].Objectives.length+".");
return false;
}
}
}
}
}
}else{
if(_69.indexOf("adl.data")>=0){
if(_6a>=this.LearningObject.SharedDataMaps.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Shared Data index "+_6a+", is greater than the maximum available index of "+this.LearningObject.SharedDataMaps.length+".");
return false;
}
}
}
}
}
}
}
var _6c;
var i;
switch(_69){
case "cmi._version":
this.WriteDetailedLog("`1572`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi._version data model element is read-only");
return false;
case "cmi.comments_from_learner._children":
this.WriteDetailedLog("`1159`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_learner._children data model element is read-only");
return false;
case "cmi.comments_from_learner._count":
this.WriteDetailedLog("`1226`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_learner._count data model element is read-only");
return false;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1154`");
if(!this.ValidLocalizedString(_68,4000)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.comments_from_learner.n.comment data model element is not a valid localized string type (SPM 4000)");
return false;
}
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1139`");
if(!this.ValidCharString(_68,250)){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The cmi.comments_from_learner.n.comment data model element is not a valid char string type (SPM 250)");
return false;
}
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1113`");
if(!this.ValidTime(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.comments_from_learner.n.timestamp data model element is not a valid time");
return false;
}
break;
case "cmi.comments_from_lms._children":
this.WriteDetailedLog("`1244`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms._children data model element is read-only");
return false;
case "cmi.comments_from_lms._count":
this.WriteDetailedLog("`1289`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms._count data model element is read-only");
return false;
case "cmi.comments_from_lms.n.comment":
this.WriteDetailedLog("`1252`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms.comment data model element is read-only");
return false;
case "cmi.comments_from_lms.n.location":
this.WriteDetailedLog("`1235`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms.location data model element is read-only");
return false;
case "cmi.comments_from_lms.n.timestamp":
this.WriteDetailedLog("`1218`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms.timestamp data model element is read-only");
return false;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
if(_68!=SCORM_STATUS_COMPLETED&&_68!=SCORM_STATUS_INCOMPLETE&&_68!=SCORM_STATUS_NOT_ATTEMPTED&&_68!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The completion_status data model element must be a proper vocabulary element.");
return false;
}
break;
case "cmi.completion_threshold":
this.WriteDetailedLog("`1363`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The completion_threshold data model element is read-only");
return false;
case "cmi.credit":
this.WriteDetailedLog("`1617`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The credit data model element is read-only");
return false;
case "cmi.entry":
this.WriteDetailedLog("`1643`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The entry data model element is read-only");
return false;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
if(_68!=SCORM_EXIT_TIME_OUT&&_68!=SCORM_EXIT_SUSPEND&&_68!=SCORM_EXIT_LOGOUT&&_68!=SCORM_EXIT_NORMAL&&_68!=SCORM_EXIT_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The exit data model element must be a proper vocabulary element.");
return false;
}
if(_68==SCORM_EXIT_LOGOUT){
this.WriteDetailedLog("`349`");
}
break;
case "cmi.interactions._children":
this.WriteDetailedLog("`1321`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions._children element is read-only");
return false;
case "cmi.interactions._count":
this.WriteDetailedLog("`1380`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions._count element is read-only");
return false;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1414`");
if(!this.ValidLongIdentifier(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_6a+".id value of '"+_68+"' is not a valid long identifier.");
return false;
}
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1371`");
if(this.RunTimeData.Interactions[_6a]===undefined||this.RunTimeData.Interactions[_6a].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(_68!=SCORM_TRUE_FALSE&&_68!=SCORM_CHOICE&&_68!=SCORM_FILL_IN&&_68!=SCORM_LONG_FILL_IN&&_68!=SCORM_LIKERT&&_68!=SCORM_MATCHING&&_68!=SCORM_PERFORMANCE&&_68!=SCORM_SEQUENCING&&_68!=SCORM_NUMERIC&&_68!=SCORM_OTHER){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_6a+".type value of '"+_68+"' is not a valid interaction type.");
return false;
}
break;
case "cmi.interactions.n.objectives._count":
this.WriteDetailedLog("`1146`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions.objectives._count element is read-only");
return false;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1199`");
if(this.RunTimeData.Interactions[_6a]===undefined||this.RunTimeData.Interactions[_6a].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidLongIdentifier(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_6a+".objectives."+_6b+".id value of '"+_68+"' is not a valid long identifier type.");
return false;
}
for(i=0;i<this.RunTimeData.Interactions[_6a].Objectives.length;i++){
if((this.RunTimeData.Interactions[_6a].Objectives[i]==_68)&&(i!=_6b)){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Every interaction objective identifier must be unique. The value '"+_68+"' has already been set in objective #"+i);
return false;
}
}
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1294`");
if(this.RunTimeData.Interactions[_6a]===undefined||this.RunTimeData.Interactions[_6a].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidTime(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_6a+".timestamp value of '"+_68+"' is not a valid time type.");
return false;
}
break;
case "cmi.interactions.n.correct_responses._count":
this.WriteDetailedLog("`999`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions.correct_responses._count element is read-only");
return false;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`973`");
if(this.RunTimeData.Interactions[_6a]===undefined||this.RunTimeData.Interactions[_6a].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(this.RunTimeData.Interactions[_6a]===undefined||this.RunTimeData.Interactions[_6a].Type===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.type element must be set before a correct response can be set.");
return false;
}
_6c=true;
if(RegistrationToDeliver.Package.Properties.ValidateInteractionResponses){
switch(this.RunTimeData.Interactions[_6a].Type){
case SCORM_TRUE_FALSE:
if(this.RunTimeData.Interactions[_6a].CorrectResponses.length>0&&_6b>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"A true-false interaction can only have one correct response.");
return false;
}
_6c=this.ValidTrueFalseResponse(_68);
break;
case SCORM_CHOICE:
_6c=this.ValidMultipleChoiceResponse(_68);
for(i=0;i<this.RunTimeData.Interactions[_6a].CorrectResponses.length;i++){
if(this.RunTimeData.Interactions[_6a].CorrectResponses[i]==_68){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Every correct response to a choice interaction must be unique. The value '"+_68+"' has already been set in correct response #"+i);
return false;
}
}
break;
case SCORM_FILL_IN:
_6c=this.ValidFillInResponse(_68,true);
break;
case SCORM_LONG_FILL_IN:
_6c=this.ValidLongFillInResponse(_68,true);
break;
case SCORM_LIKERT:
if(this.RunTimeData.Interactions[_6a].CorrectResponses.length>0&&_6b>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"A likert interaction can only have one correct response.");
return false;
}
_6c=this.ValidLikeRTResponse(_68);
break;
case SCORM_MATCHING:
_6c=this.ValidMatchingResponse(_68);
break;
case SCORM_PERFORMANCE:
_6c=this.ValidPerformanceResponse(_68,true);
break;
case SCORM_SEQUENCING:
_6c=this.ValidSequencingResponse(_68);
break;
case SCORM_NUMERIC:
if(this.RunTimeData.Interactions[_6a].CorrectResponses.length>0&&_6b>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"A numeric interaction can only have one correct response.");
return false;
}
_6c=this.ValidNumericResponse(_68,true);
break;
case SCORM_OTHER:
if(this.RunTimeData.Interactions[_6a].CorrectResponses.length>0&&_6b>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"An 'other' interaction can only have one correct response.");
return false;
}
_6c=this.ValidOtheresponse(_68);
break;
}
}
if(!_6c){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_6a+".correct_responses."+_6b+".pattern value of '"+_68+"' is not a valid correct response to an interaction of type "+this.RunTimeData.Interactions[_6a].Type+".");
return false;
}
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1295`");
if(this.RunTimeData.Interactions[_6a]===undefined||this.RunTimeData.Interactions[_6a].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_6a+".weighting value of '"+_68+"' is not a valid real number.");
return false;
}
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1161`");
if(this.RunTimeData.Interactions[_6a]===undefined||this.RunTimeData.Interactions[_6a].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(this.RunTimeData.Interactions[_6a].Type===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.type element must be set before a learner response can be set.");
return false;
}
_6c=true;
if(RegistrationToDeliver.Package.Properties.ValidateInteractionResponses){
switch(this.RunTimeData.Interactions[_6a].Type){
case "true-false":
_6c=this.ValidTrueFalseResponse(_68);
break;
case "choice":
_6c=this.ValidMultipleChoiceResponse(_68);
break;
case "fill-in":
_6c=this.ValidFillInResponse(_68,false);
break;
case "long-fill-in":
_6c=this.ValidLongFillInResponse(_68,false);
break;
case "likert":
_6c=this.ValidLikeRTResponse(_68);
break;
case "matching":
_6c=this.ValidMatchingResponse(_68);
break;
case "performance":
_6c=this.ValidPerformanceResponse(_68,false);
break;
case "sequencing":
_6c=this.ValidSequencingResponse(_68);
break;
case "numeric":
_6c=this.ValidNumericResponse(_68,false);
break;
case "other":
_6c=this.ValidOtheresponse(_68);
break;
}
}
if(!_6c){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_6a+".learner_response value of '"+_68+"' is not a valid response to an interaction of type "+this.RunTimeData.Interactions[_6a].Type+".");
return false;
}
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1348`");
if(this.RunTimeData.Interactions[_6a]===undefined||this.RunTimeData.Interactions[_6a].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(_68!=SCORM_CORRECT&&_68!=SCORM_INCORRECT&&_68!=SCORM_UNANTICIPATED&&_68!=SCORM_NEUTRAL){
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_6a+".result value of '"+_68+"' is not a valid interaction result.");
return false;
}
}
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1323`");
if(this.RunTimeData.Interactions[_6a]===undefined||this.RunTimeData.Interactions[_6a].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidTimeInterval(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_6a+".latency value of '"+_68+"' is not a valid timespan.");
return false;
}
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1270`");
if(this.RunTimeData.Interactions[_6a]===undefined||this.RunTimeData.Interactions[_6a].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidLocalizedString(_68,250)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_6a+".description value of '"+_68+"' is not a valid localized string SPM 250.");
return false;
}
break;
case "cmi.launch_data":
this.WriteDetailedLog("`1532`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.launch_data element is read-only");
return false;
case "cmi.learner_id":
this.WriteDetailedLog("`1542`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.learner_id element is read-only");
return false;
case "cmi.learner_name":
this.WriteDetailedLog("`1519`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.learner_name element is read-only");
return false;
case "cmi.learner_preference._children":
this.WriteDetailedLog("`1230`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.learner_preference._children element is read-only");
return false;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1181`");
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.audio_level value of '"+_68+"' is not a valid real number.");
return false;
}
if(parseFloat(_68)<0){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.learner_preference.audio_level value of '"+_68+"' must be greater than zero.");
return false;
}
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1241`");
if(!this.ValidLanguage(_68,true)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.language value of '"+_68+"' is not a valid language.");
return false;
}
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1109`");
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.delivery_speed value of '"+_68+"' is not a valid real number.");
return false;
}
if(parseFloat(_68)<0){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.learner_preference.delivery_speed value of '"+_68+"' must be greater than zero.");
return false;
}
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1075`");
if(_68!="-1"&&_68!="0"&&_68!="1"){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.audio_captioning value of '"+_68+"' must be -1, 0 or 1.");
return false;
}
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
if(!this.ValidCharString(_68,1000)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.location value of '"+_68+"' is not a valid char string SPM 1000.");
return false;
}
break;
case "cmi.max_time_allowed":
this.WriteDetailedLog("`1439`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.max_time_allowed element is read only");
return false;
case "cmi.mode":
this.WriteDetailedLog("`1676`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.mode element is read only");
return false;
case "cmi.objectives._children":
this.WriteDetailedLog("`1365`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.objectives._children element is read only");
return false;
case "cmi.objectives._count":
this.WriteDetailedLog("`1407`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.objectives._count element is read only");
return false;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1445`");
if(!this.ValidLongIdentifier(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives.n.id value of '"+_68+"' is not a valid long identifier.");
return false;
}
if(this.RunTimeData.Objectives[_6a]!=undefined&&this.RunTimeData.Objectives[_6a].Identifier!=null){
if(this.RunTimeData.Objectives[_6a].Identifier!==null&&this.RunTimeData.Objectives[_6a].Identifier!=_68){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Objective identifiers may only be set once and may not be overwritten. The objective at index "+_6a+" already has the identifier "+this.RunTimeData.Objectives[_6a].Identifier);
return false;
}
}
for(i=0;i<this.RunTimeData.Objectives.length;i++){
if((this.RunTimeData.Objectives[i].Identifier==_68)&&(i!=_6a)){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Every objective identifier must be unique. The value '"+_68+"' has already been set in objective #"+i);
return false;
}
}
break;
case "cmi.objectives.n.score._children":
this.WriteDetailedLog("`1229`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.objectives.n.score._children element is read only");
return false;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1282`");
if(this.RunTimeData.Objectives[_6a]===undefined||this.RunTimeData.Objectives[_6a].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_6a+".score.scaled value of '"+_68+"' is not a valid real number.");
return false;
}
if(parseFloat(_68)<-1||parseFloat(_68)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.objectives."+_6a+".score.scaled value of '"+_68+"' must be between -1 and 1.");
return false;
}
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1334`");
if(this.RunTimeData.Objectives[_6a]===undefined||this.RunTimeData.Objectives[_6a].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_6a+".score.raw value of '"+_68+"' is not a valid real number.");
return false;
}
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1318`");
if(this.RunTimeData.Objectives[_6a]===undefined||this.RunTimeData.Objectives[_6a].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_6a+".score.min value of '"+_68+"' is not a valid real number.");
return false;
}
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1316`");
if(this.RunTimeData.Objectives[_6a]===undefined||this.RunTimeData.Objectives[_6a].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_6a+".score.max value of '"+_68+"' is not a valid real number.");
return false;
}
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1251`");
if(this.RunTimeData.Objectives[_6a]===undefined||this.RunTimeData.Objectives[_6a].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(_68!=SCORM_STATUS_PASSED&&_68!=SCORM_STATUS_FAILED&&_68!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_6a+".success_status value of '"+_68+"' is not a valid success status.");
return false;
}
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1197`");
if(this.RunTimeData.Objectives[_6a]===undefined||this.RunTimeData.Objectives[_6a].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(_68!=SCORM_STATUS_COMPLETED&&_68!=SCORM_STATUS_INCOMPLETE&&_68!=SCORM_STATUS_NOT_ATTEMPTED&&_68!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_6a+".completion_status value of '"+_68+"' is not a valid completion status.");
return false;
}
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1204`");
if(this.RunTimeData.Objectives[_6a]===undefined||this.RunTimeData.Objectives[_6a].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_6a+".progress_measure value of '"+_68+"' is not a valid real number.");
return false;
}
if(parseFloat(_68)<0||parseFloat(_68)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.objectives."+_6a+".progress_measure value of '"+_68+"' must be between 0 and 1.");
return false;
}
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1290`");
if(this.RunTimeData.Objectives[_6a]===undefined||this.RunTimeData.Objectives[_6a].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidLocalizedString(_68,250)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_6a+".description value of '"+_68+"' is not a valid localized string SPM 250.");
return false;
}
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1426`");
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.progress_measure value of '"+_68+"' is not a valid real number.");
return false;
}
if(parseFloat(_68)<0||parseFloat(_68)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.pogress_measure value of '"+_68+"' must be between 0 and 1.");
return false;
}
break;
case "cmi.scaled_passing_score":
this.WriteDetailedLog("`1366`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.scaled_passing_score element is read only");
return false;
case "cmi.score._children":
this.WriteDetailedLog("`1446`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.score._children element is read only");
return false;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.scaled value of '"+_68+"' is not a valid real number.");
return false;
}
if(parseFloat(_68)<-1||parseFloat(_68)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi..score.scaled value of '"+_68+"' must be between -1 and 1.");
return false;
}
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.raw value of '"+_68+"' is not a valid real number.");
return false;
}
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.raw value of '"+_68+"' is not a valid real number.");
return false;
}
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
if(!this.ValidReal(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.raw value of '"+_68+"' is not a valid real number.");
return false;
}
break;
case "cmi.session_time":
this.WriteDetailedLog("`1515`");
if(!this.ValidTimeInterval(_68)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.session_time value of '"+_68+"' is not a valid time intervals.");
return false;
}
break;
case "cmi.success_status":
this.WriteDetailedLog("`1480`");
if(_68!=SCORM_STATUS_PASSED&&_68!=SCORM_STATUS_FAILED&&_68!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.success_status value of '"+_68+"' is not a valid success status.");
return false;
}
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1513`");
if(!this.ValidCharString(_68,64000)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.suspend_data value of '"+_68+"' is not a valid char string SPM 64000.");
return false;
}
break;
case "cmi.time_limit_action":
this.WriteDetailedLog("`1411`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.time_limit_action element is read only");
return false;
case "cmi.total_time":
this.WriteDetailedLog("`1545`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.total_time element is read only");
return false;
case "adl.nav.request":
this.WriteDetailedLog("`1455`");
if(_68.substring(0,1)=="{"){
var _6e=_68.substring(0,_68.indexOf("}")+1);
if(Control.IsTargetValid(_6e)===false){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The value '"+_6e+"' is not a valid target of a choice/jump request.");
return false;
}
if(_68.indexOf("choice")!=_6e.length&&_68.indexOf("jump")!=_6e.length){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"A target may only be provided for a choice or jump request.");
return false;
}
}else{
if(_68!=SCORM_RUNTIME_NAV_REQUEST_CONTINUE&&_68!=SCORM_RUNTIME_NAV_REQUEST_PREVIOUS&&_68!=SCORM_RUNTIME_NAV_REQUEST_CHOICE&&_68!=SCORM_RUNTIME_NAV_REQUEST_JUMP&&_68!=SCORM_RUNTIME_NAV_REQUEST_EXIT&&_68!=SCORM_RUNTIME_NAV_REQUEST_EXITALL&&_68!=SCORM_RUNTIME_NAV_REQUEST_ABANDON&&_68!=SCORM_RUNTIME_NAV_REQUEST_ABANDONALL&&_68!=SCORM_RUNTIME_NAV_REQUEST_SUSPENDALL&&_68!=SCORM_RUNTIME_NAV_REQUEST_NONE){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The adl.nav.request value of '"+_68+"' is not a valid nav request.");
return false;
}
}
break;
case "adl.nav.request_valid.continue":
this.WriteDetailedLog("`1176`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.nav.request_valid.continue element is read only");
return false;
case "adl.nav.request_valid.previous":
this.WriteDetailedLog("`1183`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.nav.request_valid.previous element is read only");
return false;
case "adl.nav.request_valid.choice":
this.WriteDetailedLog("`1223`");
break;
case "adl.nav.request_valid.jump":
this.WriteDetailedLog("`1259`");
break;
case "adl.data._children":
this.WriteDetailedLog("`1390`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.data._children element is read-only");
return false;
case "adl.data._count":
this.WriteDetailedLog("`1451`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.data._count element is read-only");
return false;
case "adl.data.n.id":
this.WriteDetailedLog("`1485`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.data.n.id element is read-only");
return false;
break;
case "adl.data.n.store":
this.WriteDetailedLog("`1435`");
if(this.LearningObject.SharedDataMaps[_6a]===undefined||this.LearningObject.SharedDataMaps[_6a].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The adl.data.id element must be set before other elements can be set.");
return false;
}
if(this.LearningObject.SharedDataMaps[_6a].WriteSharedData==false){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The specified shared data is read-only.");
return false;
}
if(!this.ValidCharString(_68,64000)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The adl.data."+_6a+".store value is not a valid char string SPM 64000.");
return false;
}
break;
default:
if(_69.indexOf("ssp")===0){
if(SSP_ENABLED){
return this.SSPApi.CheckForSetValueError(_67,_68,_69,_6a,_6b);
}
}
this.SetErrorState(SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR,"The data model element '"+_67+"' is not defined in SCORM 2004");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_ValidCharString(str,_70){
this.WriteDetailedLog("`1466`");
return true;
}
function RunTimeApi_ValidLocalizedString(str,_72){
this.WriteDetailedLog("`1373`");
var _73;
var _74=new String();
var _75;
_73=str;
if(str.indexOf("{lang=")===0){
_75=str.indexOf("}");
if(_75>0){
_74=str.substr(0,_75);
_74=_74.replace(/\{lang=/,"");
_74=_74.replace(/\}/,"");
if(!this.ValidLanguage(_74,false)){
return false;
}
if(str.length>=(_75+2)){
_73=str.substring(_75+1);
}else{
_73="";
}
}
}
return true;
}
function RunTimeApi_ExtractLanguageDelimiterFromLocalizedString(str){
var _77;
var _78="";
if(str.indexOf("{lang=")===0){
_77=str.indexOf("}");
if(_77>0){
_78=str.substr(0,_77+1);
}
}
return _78;
}
function RunTimeApi_ValidLanguage(str,_7a){
this.WriteDetailedLog("`1514`");
var _7b;
if(str.length===0){
if(_7a){
return true;
}else{
return false;
}
}
_7b=str.split("-");
for(var i=0;i<_7b.length;i++){
if(_7b[i].length>8){
return false;
}
if(_7b[i].length<2){
if(_7b[i]!="i"&&_7b[i]!="x"){
return false;
}
}
}
var _7d=new Iso639LangCodes_LangCodes();
if(!_7d.IsValid(_7b[0].toLowerCase())){
return false;
}
if(str.length>250){
return false;
}
return true;
}
function RunTimeApi_ValidLongIdentifier(str){
this.WriteDetailedLog("`1396`");
str=str.trim();
if(!this.ValidIdentifier(str)){
return false;
}
return true;
}
function RunTimeApi_ValidShortIdentifier(str){
this.WriteDetailedLog("`1382`");
if(!this.ValidIdentifier(str)){
return false;
}
return true;
}
function RunTimeApi_ValidIdentifier(str){
this.WriteDetailedLog("`1463`");
str=str.trim();
if(str.length===0){
return false;
}
if(str.toLowerCase().indexOf("urn:")===0){
return this.IsValidUrn(str);
}
if(str.search(/\w/)<0){
return false;
}
if(str.search(/[^\w\-\(\)\+\.\:\=\@\;\$\_\!\*\'\%]/)>=0){
return false;
}
return true;
}
function RunTimeApi_IsValidUrn(str){
this.WriteDetailedLog("`1568`");
var _82=str.split(":");
var nid=new String("");
var nss="";
if(_82.length>1){
nid=_82[1];
}else{
return false;
}
if(_82.length>2){
nss=_82[2];
}
if(nid.length===0){
return false;
}
if(nid.indexOf(" ")>0||nss.indexOf(" ")>0){
return false;
}
return true;
}
function RunTimeApi_ValidReal(str){
this.WriteDetailedLog("`1578`");
if(str.search(/[^.\d-]/)>-1){
return false;
}
if(str.search("-")>-1){
if(str.indexOf("-",1)>-1){
return false;
}
}
if(str.indexOf(".")!=str.lastIndexOf(".")){
return false;
}
if(str.search(/\d/)<0){
return false;
}
return true;
}
function RunTimeApi_ValidTime(str){
this.WriteDetailedLog("`1571`");
var _87="";
var _88="";
var day="";
var _8a="";
var _8b="";
var _8c="";
var _8d="";
var _8e="";
var _8f="";
var _90="";
var _91;
str=new String(str);
var _92=/^(\d\d\d\d)(-(\d\d)(-(\d\d)(T(\d\d)(:(\d\d)(:(\d\d))?)?)?)?)?/;
if(str.search(_92)!==0){
return false;
}
if(str.substr(str.length-1,1).search(/[\-T\:]/)>=0){
return false;
}
var len=str.length;
if(len!=4&&len!=7&&len!=10&&len!=13&&len!=16&&len<19){
return false;
}
if(len>=5){
if(str.substr(4,1)!="-"){
return false;
}
}
if(len>=8){
if(str.substr(7,1)!="-"){
return false;
}
}
if(len>=11){
if(str.substr(10,1)!="T"){
return false;
}
}
if(len>=14){
if(str.substr(13,1)!=":"){
return false;
}
}
if(len>=17){
if(str.substr(16,1)!=":"){
return false;
}
}
var _94=str.match(_92);
_87=_94[1];
_88=_94[3];
day=_94[5];
_8a=_94[7];
_8b=_94[9];
_8c=_94[11];
if(str.length>19){
if(str.length<21){
return false;
}
if(str.substr(19,1)!="."){
return false;
}
_91=str.substr(20,1);
if(_91.search(/\d/)<0){
return false;
}else{
_8d+=_91;
}
for(var i=21;i<str.length;i++){
_91=str.substr(i,1);
if((i==21)&&(_91.search(/\d/)===0)){
_8d+=_91;
}else{
_8e+=_91;
}
}
}
if(_8e.length===0){
}else{
if(_8e.length==1){
if(_8e!="Z"){
return false;
}
}else{
if(_8e.length==3){
if(_8e.search(/[\+\-]\d\d/)!==0){
return false;
}else{
_8f=_8e.substr(1,2);
}
}else{
if(_8e.length==6){
if(_8e.search(/[\+\-]\d\d:\d\d/)!==0){
return false;
}else{
_8f=_8e.substr(1,2);
_90=_8e.substr(4,2);
}
}else{
return false;
}
}
}
}
if(_87<1970||_87>2038){
return false;
}
if(_88!==undefined&&_88!==""){
_88=parseInt(_88,10);
if(_88<1||_88>12){
return false;
}
}
if(day!==undefined&&day!==""){
var dtm=new Date(_87,(_88-1),day);
if(dtm.getDate()!=day){
return false;
}
}
if(_8a!==undefined&&_8a!==""){
_8a=parseInt(_8a,10);
if(_8a<0||_8a>23){
return false;
}
}
if(_8b!==undefined&&_8b!==""){
_8b=parseInt(_8b,10);
if(_8b<0||_8b>59){
return false;
}
}
if(_8c!==undefined&&_8c!==""){
_8c=parseInt(_8c,10);
if(_8c<0||_8c>59){
return false;
}
}
if(_8f!==undefined&&_8f!==""){
_8f=parseInt(_8f,10);
if(_8f<0||_8f>23){
return false;
}
}
if(_90!==undefined&&_90!==""){
_90=parseInt(_90,10);
if(_90<0||_90>59){
return false;
}
}
return true;
}
function RunTimeApi_ValidTimeInterval(str){
this.WriteDetailedLog("`1425`");
var _98=/^P(\d+Y)?(\d+M)?(\d+D)?(T(\d+H)?(\d+M)?(\d+(.\d\d?)?S)?)?$/;
if(str=="P"){
return false;
}
if(str.lastIndexOf("T")==(str.length-1)){
return false;
}
if(str.search(_98)<0){
return false;
}
return true;
}
function RunTimeApi_ValidTrueFalseResponse(str){
this.WriteDetailedLog("`1346`");
var _9a=/^(true|false)$/;
if(str.search(_9a)<0){
return false;
}
return true;
}
function RunTimeApi_ValidMultipleChoiceResponse(str){
this.WriteDetailedLog("`1254`");
if(str.length===0){
return true;
}
return (this.IsValidArrayOfShortIdentifiers(str,36,true)||this.IsValidCommaDelimitedArrayOfShortIdentifiers(str,36,true));
}
function RunTimeApi_IsValidCommaDelimitedArrayOfShortIdentifiers(str,_9d,_9e){
this.WriteDetailedLog("`1205`");
var _9f=",";
var _a0=str.split(_9f);
for(var i=0;i<_a0.length;i++){
if(!this.ValidShortIdentifier(_a0[i])){
return false;
}
if(_9e){
for(var j=0;j<_a0.length;j++){
if(j!=i){
if(_a0[j]==_a0[i]){
return false;
}
}
}
}
}
return true;
}
function RunTimeApi_IsValidArrayOfShortIdentifiers(str,_a4,_a5){
this.WriteDetailedLog("`1205`");
var _a6="[,]";
var _a7=str.split(_a6);
for(var i=0;i<_a7.length;i++){
if(!this.ValidShortIdentifier(_a7[i])){
return false;
}
if(_a5){
for(var j=0;j<_a7.length;j++){
if(j!=i){
if(_a7[j]==_a7[i]){
return false;
}
}
}
}
}
return true;
}
function RunTimeApi_IsValidArrayOfLocalizedStrings(str,_ab,_ac){
this.WriteDetailedLog("`1212`");
var _ad="[,]";
var _ae=str.split(_ad);
for(var i=0;i<_ae.length;i++){
if(!this.ValidLocalizedString(_ae[i],0)){
return false;
}
if(_ac){
for(var j=0;j<_ae.length;j++){
if(j!=i){
if(_ae[j]==_ae[i]){
return false;
}
}
}
}
}
return true;
}
function RunTimeApi_ValidFillInResponse(str,_b2){
this.WriteDetailedLog("`1391`");
if(str.length===0){
return true;
}
var _b3=/^\{case_matters=/;
var _b4=/^\{order_matters=/;
var _b5=/^\{lang=[\w\-]+\}\{/;
var _b6=/^\{case_matters=(true|false)\}/;
var _b7=/^\{order_matters=(true|false)\}/;
var _b8=new String(str);
if(_b2){
if(_b8.search(_b3)>=0){
if(_b8.search(_b6)<0){
return false;
}
_b8=_b8.replace(_b6,"");
}
if(_b8.search(_b4)>=0){
if(_b8.search(_b7)<0){
return false;
}
_b8=_b8.replace(_b7,"");
}
if(_b8.search(_b3)>=0){
if(_b8.search(_b6)<0){
return false;
}
_b8=_b8.replace(_b6,"");
}
}
return this.IsValidArrayOfLocalizedStrings(_b8,10,false);
}
function RunTimeApi_ValidLongFillInResponse(str,_ba){
this.WriteDetailedLog("`1328`");
var _bb=/^\{case_matters=/;
var _bc=/^\{case_matters=(true|false)\}/;
var _bd=new String(str);
if(_ba){
if(_bd.search(_bb)>=0){
if(_bd.search(_bc)<0){
return false;
}
_bd=_bd.replace(_bc,"");
}
}
return this.ValidLocalizedString(_bd,4000);
}
function RunTimeApi_ValidLikeRTResponse(str){
this.WriteDetailedLog("`1401`");
return this.ValidShortIdentifier(str);
}
function RunTimeApi_ValidMatchingResponse(str){
this.WriteDetailedLog("`1364`");
var _c0="[,]";
var _c1="[.]";
var _c2;
var _c3;
_c2=str.split(_c0);
for(var i=0;i<_c2.length;i++){
_c3=_c2[i].split(_c1);
if(_c3.length!=2){
return false;
}
if(!this.ValidShortIdentifier(_c3[0])){
return false;
}
if(!this.ValidShortIdentifier(_c3[1])){
return false;
}
}
return true;
}
function RunTimeApi_ValidPerformanceResponse(str,_c6){
this.WriteDetailedLog("`1300`");
var _c7=/^\{order_matters=/;
var _c8=/^\{order_matters=(true|false)\}/;
var _c9;
var _ca;
var _cb;
var _cc;
var _cd;
var _ce=new String(str);
if(str.length===0){
return false;
}
if(_c6){
if(_ce.search(_c7)>=0){
if(_ce.search(_c8)<0){
return false;
}
_ce=_ce.replace(_c8,"");
}
}
_c9=_ce.split("[,]");
if(_c9.length===0){
return false;
}
for(var i=0;i<_c9.length;i++){
_ca=_c9[i];
if(_ca.length===0){
return false;
}
_cb=_ca.split("[.]");
if(_cb.length==2){
_cc=_cb[0];
_cd=_cb[1];
if(_cc.length===0&&_cd===0){
return false;
}
if(_cc.length>0){
if(!this.ValidShortIdentifier(_cc)){
return false;
}
}
}else{
return false;
}
}
return true;
}
function RunTimeApi_ValidSequencingResponse(str){
this.WriteDetailedLog("`1339`");
return this.IsValidArrayOfShortIdentifiers(str,36,false);
}
function RunTimeApi_ValidNumericResponse(str,_d2){
this.WriteDetailedLog("`1372`");
var _d3="[:]";
var _d4=str.split(_d3);
if(_d2){
if(_d4.length>2){
return false;
}
}else{
if(_d4.length>1){
return false;
}
}
for(var i=0;i<_d4.length;i++){
if(!this.ValidReal(_d4[i])){
return false;
}
}
if(_d4.length>=2){
if(_d4[0].length>0&&_d4[1].length>0){
if(parseFloat(_d4[0])>parseFloat(_d4[1])){
return false;
}
}
}
return true;
}
function RunTimeApi_ValidOtheresponse(str){
this.WriteDetailedLog("`1429`");
return true;
}
function RunTimeApi_TranslateBooleanIntoCMI(_d7){
if(_d7===true){
return SCORM_TRUE;
}else{
if(_d7===false){
return SCORM_FALSE;
}else{
return SCORM_UNKNOWN;
}
}
}
function RunTimeApi_GetCompletionStatus(){
if(this.LearningObject.CompletedByMeasure===true){
if(this.LearningObject.CompletionThreshold!==null){
if(this.RunTimeData.ProgressMeasure!==null){
if(parseFloat(this.RunTimeData.ProgressMeasure)>=parseFloat(this.LearningObject.CompletionThreshold)){
this.WriteDetailedLog("`619`");
return SCORM_STATUS_COMPLETED;
}else{
this.WriteDetailedLog("`576`");
return SCORM_STATUS_INCOMPLETE;
}
}else{
this.WriteDetailedLog("`996`"+this.LearningObject.CompletionThreshold+"`676`");
return SCORM_STATUS_UNKNOWN;
}
}else{
this.WriteDetailedLog("`623`"+this.RunTimeData.CompletionStatus);
return this.RunTimeData.CompletionStatus;
}
}else{
this.WriteDetailedLog("`651`"+this.RunTimeData.CompletionStatus);
return this.RunTimeData.CompletionStatus;
}
}
function RunTimeApi_GetSuccessStatus(){
var _d8=this.LearningObject.GetScaledPassingScore();
if(_d8!==null){
if(this.RunTimeData.ScoreScaled!==null){
if(parseFloat(this.RunTimeData.ScoreScaled)>=parseFloat(_d8)){
this.WriteDetailedLog("`698`");
return SCORM_STATUS_PASSED;
}else{
this.WriteDetailedLog("`650`");
return SCORM_STATUS_FAILED;
}
}else{
this.WriteDetailedLog("`577`");
return SCORM_STATUS_UNKNOWN;
}
}
this.WriteDetailedLog("`640`"+this.RunTimeData.SuccessStatus);
return this.RunTimeData.SuccessStatus;
}
function RunTimeApi_InitTrackedTimeStart(_d9){
this.TrackedStartDate=new Date();
this.StartSessionTotalTime=_d9.RunTime.TotalTime;
}
function RunTimeApi_AccumulateTotalTrackedTime(){
this.TrackedEndDate=new Date();
var _da=Math.round((this.TrackedEndDate-this.TrackedStartDate)/10);
var _db=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTimeTracked);
var _dc=_da+_db;
this.RunTimeData.TotalTimeTracked=ConvertHundredthsToIso8601TimeSpan(_dc);
this.Activity.ActivityEndedDate=this.TrackedEndDate;
var _dd=GetDateFromUtcIso8601Time(this.Activity.GetActivityStartTimestampUtc());
var _de=GetDateFromUtcIso8601Time(this.Activity.GetAttemptStartTimestampUtc());
this.Activity.SetActivityAbsoluteDuration(ConvertHundredthsToIso8601TimeSpan((this.TrackedEndDate-_dd)/10));
this.Activity.SetAttemptAbsoluteDuration(ConvertHundredthsToIso8601TimeSpan((this.TrackedEndDate-_de)/10));
var _df=ConvertIso8601TimeSpanToHundredths(this.Activity.GetActivityExperiencedDurationTracked());
var _e0=ConvertHundredthsToIso8601TimeSpan(_df+_da);
this.Activity.SetActivityExperiencedDurationTracked(_e0);
var _e1=ConvertIso8601TimeSpanToHundredths(this.Activity.GetActivityExperiencedDurationReported());
var _e2=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTime)-ConvertIso8601TimeSpanToHundredths(this.StartSessionTotalTime);
var _e3=ConvertHundredthsToIso8601TimeSpan(_e1+_e2);
this.Activity.SetActivityExperiencedDurationReported(_e3);
var _e4=ConvertIso8601TimeSpanToHundredths(this.Activity.GetAttemptExperiencedDurationTracked());
var _e5=ConvertHundredthsToIso8601TimeSpan(_e4+_da);
this.Activity.SetAttemptExperiencedDurationTracked(_e5);
var _e6=ConvertIso8601TimeSpanToHundredths(this.Activity.GetAttemptExperiencedDurationReported());
var _e7=ConvertHundredthsToIso8601TimeSpan(_e6+_e2);
this.Activity.SetAttemptExperiencedDurationReported(_e7);
}
function RunTimeApi_SetLookAheadDirtyDataFlagIfNeeded(_e8,_e9){
if(this.IsLookAheadSequencerDataDirty==false&&_e8!=_e9){
this.IsLookAheadSequencerDataDirty=true;
}
}
var TERMINATION_REQUEST_EXIT="EXIT";
var TERMINATION_REQUEST_EXIT_ALL="EXIT ALL";
var TERMINATION_REQUEST_SUSPEND_ALL="SUSPEND ALL";
var TERMINATION_REQUEST_ABANDON="ABANDON";
var TERMINATION_REQUEST_ABANDON_ALL="ABANDON ALL";
var TERMINATION_REQUEST_EXIT_PARENT="EXIT PARENT";
var TERMINATION_REQUEST_NOT_VALID="INVALID";
var SEQUENCING_REQUEST_START="START";
var SEQUENCING_REQUEST_RESUME_ALL="RESUME ALL";
var SEQUENCING_REQUEST_CONTINUE="CONTINUE";
var SEQUENCING_REQUEST_PREVIOUS="PREVIOUS";
var SEQUENCING_REQUEST_CHOICE="CHOICE";
var SEQUENCING_REQUEST_RETRY="RETRY";
var SEQUENCING_REQUEST_EXIT="EXIT";
var SEQUENCING_REQUEST_NOT_VALID="INVALID";
var SEQUENCING_REQUEST_JUMP="JUMP";
var RULE_SET_POST_CONDITION="POST_CONDITION";
var RULE_SET_EXIT="EXIT";
var RULE_SET_HIDE_FROM_CHOICE="HIDE_FROM_CHOICE";
var RULE_SET_STOP_FORWARD_TRAVERSAL="STOP_FORWARD_TRAVERSAL";
var RULE_SET_DISABLED="DISABLED";
var RULE_SET_SKIPPED="SKIPPED";
var RULE_SET_SATISFIED="SATISFIED";
var RULE_SET_NOT_SATISFIED="NOT_SATISFIED";
var RULE_SET_COMPLETED="COMPLETED";
var RULE_SET_INCOMPLETE="INCOMPLETE";
var SEQUENCING_RULE_ACTION_SKIP="Skip";
var SEQUENCING_RULE_ACTION_DISABLED="Disabled";
var SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE="Hidden From Choice";
var SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL="Stop Forward Traversal";
var SEQUENCING_RULE_ACTION_EXIT="Exit";
var SEQUENCING_RULE_ACTION_EXIT_PARENT="Exit Parent";
var SEQUENCING_RULE_ACTION_EXIT_ALL="Exit All";
var SEQUENCING_RULE_ACTION_RETRY="Retry";
var SEQUENCING_RULE_ACTION_RETRY_ALL="Retry All";
var SEQUENCING_RULE_ACTION_CONTINUE="Continue";
var SEQUENCING_RULE_ACTION_PREVIOUS="Previous";
var FLOW_DIRECTION_FORWARD="FORWARD";
var FLOW_DIRECTION_BACKWARD="BACKWARD";
var RULE_CONDITION_OPERATOR_NOT="Not";
var RULE_CONDITION_OPERATOR_NOOP="No Op";
var RULE_CONDITION_COMBINATION_ALL="All";
var RULE_CONDITION_COMBINATION_ANY="Any";
var RESULT_UNKNOWN="unknown";
var SEQUENCING_RULE_CONDITION_SATISFIED="Satisfied";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN="Objective Status Known";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN="Objective Measure Known";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_GREATER_THAN="Objective Measure Greater Than";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_LESS_THAN="Objective Measure Less Than";
var SEQUENCING_RULE_CONDITION_COMPLETED="Completed";
var SEQUENCING_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN="Activity Progress Known";
var SEQUENCING_RULE_CONDITION_ATTEMPTED="Attempted";
var SEQUENCING_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED="Attempt Limit Exceeded";
var SEQUENCING_RULE_CONDITION_ALWAYS="Always";
var ROLLUP_RULE_ACTION_SATISFIED="Satisfied";
var ROLLUP_RULE_ACTION_NOT_SATISFIED="Not Satisfied";
var ROLLUP_RULE_ACTION_COMPLETED="Completed";
var ROLLUP_RULE_ACTION_INCOMPLETE="Incomplete";
var ROLLUP_RULE_MINIMUM_COUNT_DEFAULT=0;
var ROLLUP_RULE_MINIMUM_PERCENT_DEFAULT=0;
var CHILD_ACTIVITY_SET_ALL="All";
var CHILD_ACTIVITY_SET_ANY="Any";
var CHILD_ACTIVITY_SET_NONE="None";
var CHILD_ACTIVITY_SET_AT_LEAST_COUNT="At Least Count";
var CHILD_ACTIVITY_SET_AT_LEAST_PERCENT="At Least Percent";
var ROLLUP_RULE_CONDITION_SATISFIED="Satisfied";
var ROLLUP_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN="Objective Status Known";
var ROLLUP_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN="Objective Measure Known";
var ROLLUP_RULE_CONDITION_COMPLETED="Completed";
var ROLLUP_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN="Activity Progress Known";
var ROLLUP_RULE_CONDITION_ATTEMPTED="Attempted";
var ROLLUP_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED="Attempt Limit Exceeded";
var ROLLUP_RULE_CONDITION_NEVER="Never";
var ROLLUP_CONSIDERATION_ALWAYS="Always";
var ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED="If Not Suspended";
var ROLLUP_CONSIDERATION_IF_ATTEMPTED="If Attempted";
var ROLLUP_CONSIDERATION_IF_NOT_SKIPPED="If Not Skipped";
var TIMING_NEVER="Never";
var TIMING_ONCE="Once";
var TIMING_ON_EACH_NEW_ATTEMPT="On Each New Attempt";
var CONTROL_CHOICE_EXIT_ERROR_NAV="NB.2.1-8";
var CONTROL_CHOICE_EXIT_ERROR_CHOICE="SB.2.9-7";
var PREVENT_ACTIVATION_ERROR="SB.2.9-6";
var CONSTRAINED_CHOICE_ERROR="SB.2.9-8";
function Sequencer(_ea,_eb){
this.LookAhead=_ea;
this.Activities=_eb;
this.NavigationRequest=null;
this.ChoiceTargetIdentifier=null;
this.SuspendedActivity=null;
this.CurrentActivity=null;
this.Exception=null;
this.ExceptionText=null;
this.GlobalObjectives=new Array();
this.SharedData=new Array();
this.ReturnToLmsInvoked=false;
}
Sequencer.prototype.OverallSequencingProcess=Sequencer_OverallSequencingProcess;
Sequencer.prototype.SetSuspendedActivity=Sequencer_SetSuspendedActivity;
Sequencer.prototype.GetSuspendedActivity=Sequencer_GetSuspendedActivity;
Sequencer.prototype.Start=Sequencer_Start;
Sequencer.prototype.InitialRandomizationAndSelection=Sequencer_InitialRandomizationAndSelection;
Sequencer.prototype.GetCurrentActivity=Sequencer_GetCurrentActivity;
Sequencer.prototype.GetExceptionText=Sequencer_GetExceptionText;
Sequencer.prototype.GetExitAction=Sequencer_GetExitAction;
Sequencer.prototype.EvaluatePossibleNavigationRequests=Sequencer_EvaluatePossibleNavigationRequests;
Sequencer.prototype.InitializePossibleNavigationRequestAbsolutes=Sequencer_InitializePossibleNavigationRequestAbsolutes;
Sequencer.prototype.SetAllDescendentsToDisabled=Sequencer_SetAllDescendentsToDisabled;
Sequencer.prototype.SetAllDescendentsToNotSucceed=Sequencer_SetAllDescendentsToNotSucceed;
Sequencer.prototype.SetAllDescendentsToSkipped=Sequencer_SetAllDescendentsToSkipped;
Sequencer.prototype.ContentDeliveryEnvironmentActivityDataSubProcess=Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess;
function Sequencer_SetSuspendedActivity(_ec,_ed){
if(_ed!==null&&_ed!==undefined){
this.LogSeqSimple("Setting Suspended Activity to \""+_ec+"\".",_ed);
}
this.SuspendedActivity=_ec;
}
function Sequencer_GetSuspendedActivity(){
return this.SuspendActivity;
}
function Sequencer_GetSuspendedActivity(_ee){
var _ef=this.SuspendedActivity;
if(_ee!==null&&_ee!==undefined){
this.LogSeq("`1552`"+_ef,_ee);
}
return _ef;
}
function Sequencer_Start(){
if(this.SuspendedActivity===null){
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_START,null,"");
}else{
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_RESUME_ALL,null,"");
}
this.OverallSequencingProcess();
}
function Sequencer_InitialRandomizationAndSelection(){
var _f0=this.LogSeqAudit("`1306`");
var _f1=false;
var _f2;
if(this.SuspendedActivity===null){
for(var _f3 in this.Activities.ActivityList){
var _f4=this.Activities.ActivityList[_f3];
if(_f4.GetRandomizationTiming()!=TIMING_NEVER||_f4.GetSelectionTiming()!=TIMING_NEVER){
if(_f1===false){
_f2=this.LogSeqSimpleAudit("Initial selection and randomization of activities.");
}
this.SelectChildrenProcess(_f4,_f0,_f2);
this.RandomizeChildrenProcess(_f4,false,_f0,_f2);
}
}
}
}
function Sequencer_GetCurrentActivity(){
return this.CurrentActivity;
}
function Sequencer_GetExceptionText(){
if(this.ExceptionText!==null&&this.ExceptionText!==undefined){
return this.ExceptionText;
}else{
return "";
}
}
function Sequencer_GetExitAction(_f5,_f6){
return EXIT_ACTION_DISPLAY_MESSAGE;
}
Sequencer.prototype.NavigationRequestProcess=Sequencer_NavigationRequestProcess;
Sequencer.prototype.SequencingExitActionRulesSubprocess=Sequencer_SequencingExitActionRulesSubprocess;
Sequencer.prototype.SequencingPostConditionRulesSubprocess=Sequencer_SequencingPostConditionRulesSubprocess;
Sequencer.prototype.TerminationRequestProcess=Sequencer_TerminationRequestProcess;
Sequencer.prototype.MeasureRollupProcess=Sequencer_MeasureRollupProcess;
Sequencer.prototype.CompletionMeasureRollupProcess=Sequencer_CompletionMeasureRollupProcess;
Sequencer.prototype.ObjectiveRollupProcess=Sequencer_ObjectiveRollupProcess;
Sequencer.prototype.ObjectiveRollupUsingMeasureProcess=Sequencer_ObjectiveRollupUsingMeasureProcess;
Sequencer.prototype.ObjectiveRollupUsingRulesProcess=Sequencer_ObjectiveRollupUsingRulesProcess;
Sequencer.prototype.ActivityProgressRollupProcess=Sequencer_ActivityProgressRollupProcess;
Sequencer.prototype.ActivityProgressRollupUsingMeasureProcess=Sequencer_ActivityProgressRollupUsingMeasureProcess;
Sequencer.prototype.ActivityProgressRollupUsingRulesProcess=Sequencer_ActivityProgressRollupUsingRulesProcess;
Sequencer.prototype.RollupRuleCheckSubprocess=Sequencer_RollupRuleCheckSubprocess;
Sequencer.prototype.EvaluateRollupConditionsSubprocess=Sequencer_EvaluateRollupConditionsSubprocess;
Sequencer.prototype.EvaluateRollupRuleCondition=Sequencer_EvaluateRollupRuleCondition;
Sequencer.prototype.CheckChildForRollupSubprocess=Sequencer_CheckChildForRollupSubprocess;
Sequencer.prototype.OverallRollupProcess=Sequencer_OverallRollupProcess;
Sequencer.prototype.SelectChildrenProcess=Sequencer_SelectChildrenProcess;
Sequencer.prototype.RandomizeChildrenProcess=Sequencer_RandomizeChildrenProcess;
Sequencer.prototype.FlowTreeTraversalSubprocess=Sequencer_FlowTreeTraversalSubprocess;
Sequencer.prototype.FlowActivityTraversalSubprocess=Sequencer_FlowActivityTraversalSubprocess;
Sequencer.prototype.FlowSubprocess=Sequencer_FlowSubprocess;
Sequencer.prototype.JumpSequencingRequestProcess=Sequencer_JumpSequencingRequestProcess;
Sequencer.prototype.ChoiceActivityTraversalSubprocess=Sequencer_ChoiceActivityTraversalSubprocess;
Sequencer.prototype.StartSequencingRequestProcess=Sequencer_StartSequencingRequestProcess;
Sequencer.prototype.ResumeAllSequencingRequestProcess=Sequencer_ResumeAllSequencingRequestProcess;
Sequencer.prototype.ContinueSequencingRequestProcess=Sequencer_ContinueSequencingRequestProcess;
Sequencer.prototype.PreviousSequencingRequestProcess=Sequencer_PreviousSequencingRequestProcess;
Sequencer.prototype.ChoiceSequencingRequestProcess=Sequencer_ChoiceSequencingRequestProcess;
Sequencer.prototype.ChoiceFlowSubprocess=Sequencer_ChoiceFlowSubprocess;
Sequencer.prototype.ChoiceFlowTreeTraversalSubprocess=Sequencer_ChoiceFlowTreeTraversalSubprocess;
Sequencer.prototype.RetrySequencingRequestProcess=Sequencer_RetrySequencingRequestProcess;
Sequencer.prototype.ExitSequencingRequestProcess=Sequencer_ExitSequencingRequestProcess;
Sequencer.prototype.SequencingRequestProcess=Sequencer_SequencingRequestProcess;
Sequencer.prototype.DeliveryRequestProcess=Sequencer_DeliveryRequestProcess;
Sequencer.prototype.ContentDeliveryEnvironmentProcess=Sequencer_ContentDeliveryEnvironmentProcess;
Sequencer.prototype.ClearSuspendedActivitySubprocess=Sequencer_ClearSuspendedActivitySubprocess;
Sequencer.prototype.LimitConditionsCheckProcess=Sequencer_LimitConditionsCheckProcess;
Sequencer.prototype.SequencingRulesCheckProcess=Sequencer_SequencingRulesCheckProcess;
Sequencer.prototype.SequencingRulesCheckSubprocess=Sequencer_SequencingRulesCheckSubprocess;
Sequencer.prototype.TerminateDescendentAttemptsProcess=Sequencer_TerminateDescendentAttemptsProcess;
Sequencer.prototype.EndAttemptProcess=Sequencer_EndAttemptProcess;
Sequencer.prototype.CheckActivityProcess=Sequencer_CheckActivityProcess;
Sequencer.prototype.EvaluateSequencingRuleCondition=Sequencer_EvaluateSequencingRuleCondition;
Sequencer.prototype.ResetException=Sequencer_ResetException;
Sequencer.prototype.LogSeq=Sequencer_LogSeq;
Sequencer.prototype.LogSeqAudit=Sequencer_LogSeqAudit;
Sequencer.prototype.LogSeqReturn=Sequencer_LogSeqReturn;
Sequencer.prototype.WriteHistoryLog=Sequencer_WriteHistoryLog;
Sequencer.prototype.WriteHistoryReturnValue=Sequencer_WriteHistoryReturnValue;
Sequencer.prototype.LogSeqSimple=Sequencer_LogSeqSimple;
Sequencer.prototype.LogSeqSimpleAudit=Sequencer_LogSeqSimpleAudit;
Sequencer.prototype.LogSeqSimpleReturn=Sequencer_LogSeqSimpleReturn;
Sequencer.prototype.SetCurrentActivity=Sequencer_SetCurrentActivity;
Sequencer.prototype.IsCurrentActivityDefined=Sequencer_IsCurrentActivityDefined;
Sequencer.prototype.IsSuspendedActivityDefined=Sequencer_IsSuspendedActivityDefined;
Sequencer.prototype.ClearSuspendedActivity=Sequencer_ClearSuspendedActivity;
Sequencer.prototype.GetRootActivity=Sequencer_GetRootActivity;
Sequencer.prototype.DoesActivityExist=Sequencer_DoesActivityExist;
Sequencer.prototype.GetActivityFromIdentifier=Sequencer_GetActivityFromIdentifier;
Sequencer.prototype.AreActivitiesSiblings=Sequencer_AreActivitiesSiblings;
Sequencer.prototype.FindCommonAncestor=Sequencer_FindCommonAncestor;
Sequencer.prototype.GetActivityPath=Sequencer_GetActivityPath;
Sequencer.prototype.GetPathToAncestorExclusive=Sequencer_GetPathToAncestorExclusive;
Sequencer.prototype.GetPathToAncestorInclusive=Sequencer_GetPathToAncestorInclusive;
Sequencer.prototype.ActivityHasSuspendedChildren=Sequencer_ActivityHasSuspendedChildren;
Sequencer.prototype.CourseIsSingleSco=Sequencer_CourseIsSingleSco;
Sequencer.prototype.TranslateSequencingRuleActionIntoSequencingRequest=Sequencer_TranslateSequencingRuleActionIntoSequencingRequest;
Sequencer.prototype.TranslateSequencingRuleActionIntoTerminationRequest=Sequencer_TranslateSequencingRuleActionIntoTerminationRequest;
Sequencer.prototype.IsActivity1BeforeActivity2=Sequencer_IsActivity1BeforeActivity2;
Sequencer.prototype.GetOrderedListOfActivities=Sequencer_GetOrderedListOfActivities;
Sequencer.prototype.PreOrderTraversal=Sequencer_PreOrderTraversal;
Sequencer.prototype.PostOrderTraversal=Sequencer_PostOrderTraversal;
Sequencer.prototype.IsActivityLastOverall=Sequencer_IsActivityLastOverall;
Sequencer.prototype.GetGlobalObjectiveByIdentifier=Sequencer_GetGlobalObjectiveByIdentifier;
Sequencer.prototype.AddGlobalObjective=Sequencer_AddGlobalObjective;
Sequencer.prototype.ResetGlobalObjectives=Sequencer_ResetGlobalObjectives;
Sequencer.prototype.ResetSharedData=Sequencer_ResetSharedData;
Sequencer.prototype.FindActivitiesAffectedByWriteMaps=Sequencer_FindActivitiesAffectedByWriteMaps;
Sequencer.prototype.FindDistinctParentsOfActivitySet=Sequencer_FindDistinctParentsOfActivitySet;
Sequencer.prototype.FindDistinctAncestorsOfActivitySet=Sequencer_FindDistinctAncestorsOfActivitySet;
Sequencer.prototype.GetMinimalSubsetOfActivitiesToRollup=Sequencer_GetMinimalSubsetOfActivitiesToRollup;
Sequencer.prototype.CheckForRelevantSequencingRules=Sequencer_CheckForRelevantSequencingRules;
Sequencer.prototype.DoesThisActivityHaveSequencingRulesRelevantToChoice=Sequencer_DoesThisActivityHaveSequencingRulesRelevantToChoice;
Sequencer.prototype.GetArrayOfDescendents=Sequencer_GetArrayOfDescendents;
Sequencer.prototype.IsActivity1AParentOfActivity2=Sequencer_IsActivity1AParentOfActivity2;
function Sequencer_ResetException(){
this.Exception=null;
this.ExceptionText=null;
}
function Sequencer_LogSeq(str,_f8){
if(_f8===null||_f8===undefined){
Debug.AssertError("`1537`");
}
str=str+"";
if(this.LookAhead===true){
return Debug.WriteLookAheadDetailed(str,_f8);
}else{
return Debug.WriteSequencingDetailed(str,_f8);
}
}
function Sequencer_LogSeqAudit(str,_fa){
str=str+"";
if(this.LookAhead===true){
return Debug.WriteLookAheadAudit(str,_fa);
}else{
return Debug.WriteSequencingAudit(str,_fa);
}
}
function Sequencer_LogSeqReturn(str,_fc){
if(_fc===null||_fc===undefined){
Debug.AssertError("`1537`");
}
str=str+"";
if(this.LookAhead===true){
return _fc.setReturn(str);
}else{
return _fc.setReturn(str);
}
}
function Sequencer_LogSeqSimple(str,_fe){
if(_fe===null||_fe===undefined){
Debug.AssertError("failed to pass logEntry");
}
str=str+"";
if(this.LookAhead===true){
return Debug.WriteSequencingSimpleDetailed(str,_fe);
}else{
return Debug.WriteSequencingSimpleDetailed(str,_fe);
}
}
function Sequencer_LogSeqSimpleAudit(str,_100){
str=str+"";
if(this.LookAhead===true){
return Debug.WriteSequencingSimpleAudit(str,_100);
}else{
return Debug.WriteSequencingSimpleAudit(str,_100);
}
}
function Sequencer_LogSeqSimpleReturn(str,_102){
if(_102===null||_102===undefined){
Debug.AssertError("failed to pass logEntry");
}
str=str+"";
if(this.LookAhead===true){
return _102.setReturn(str);
}else{
return _102.setReturn(str);
}
}
function Sequencer_WriteSequencingSimple(str){
str=str+"";
return Debug.WriteSequencingSimple(str);
}
function Sequencer_WriteHistoryLog(str,atts){
HistoryLog.WriteEventDetailed(str,atts);
}
function Sequencer_WriteHistoryReturnValue(str,atts){
HistoryLog.WriteEventDetailedReturnValue(str,atts);
}
function Sequencer_SetCurrentActivity(_108,_109,_10a){
Debug.AssertError("Parent log not passed.",(_109===undefined||_109===null));
this.LogSeq("`1421`"+_108,_109);
this.LogSeqSimple("Setting Current Activity to \""+_108+"\".",_10a);
this.CurrentActivity=_108;
}
function Sequencer_IsCurrentActivityDefined(_10b){
Debug.AssertError("Parent log not passed.",(_10b===undefined||_10b===null));
var _10c=this.GetCurrentActivity();
var _10d=(_10c!==null);
if(_10d){
this.LogSeq("`1448`",_10b);
}else{
this.LogSeq("`1379`",_10b);
}
return _10d;
}
function Sequencer_IsSuspendedActivityDefined(_10e){
Debug.AssertError("Parent log not passed.",(_10e===undefined||_10e===null));
var _10f=this.GetSuspendedActivity(_10e);
var _110=(_10f!==null);
if(_110){
this.LogSeq("`1406`",_10e);
}else{
this.LogSeq("`1341`",_10e);
}
return _110;
}
function Sequencer_ClearSuspendedActivity(_111){
Debug.AssertError("Parent log not passed.",(_111===undefined||_111===null));
this.LogSeq("`1459`",_111);
this.SuspendedActivity=null;
}
function Sequencer_GetRootActivity(_112){
Debug.AssertError("Parent log not passed.",(_112===undefined||_112===null));
var _113=this.Activities.GetRootActivity();
return _113;
}
function Sequencer_DoesActivityExist(_114,_115){
Debug.AssertError("Parent log not passed.",(_115===undefined||_115===null));
var _116=this.Activities.DoesActivityExist(_114,_115);
if(_116){
this.LogSeq("`1670`"+_114+"`1397`",_115);
}else{
this.LogSeq("`1670`"+_114+"`1256`",_115);
}
return _116;
}
function Sequencer_GetActivityFromIdentifier(_117,_118){
Debug.AssertError("Parent log not passed.",(_118===undefined||_118===null));
var _119=this.Activities.GetActivityFromIdentifier(_117,_118);
return _119;
}
function Sequencer_AreActivitiesSiblings(_11a,_11b,_11c){
Debug.AssertError("Parent log not passed.",(_11c===undefined||_11c===null));
if(_11a===null||_11a===undefined||_11b===null||_11b===undefined){
return false;
}
var _11d=_11a.ParentActivity;
var _11e=_11b.ParentActivity;
var _11f=(_11d==_11e);
if(_11f){
this.LogSeq("`1680`"+_11a+"`1727`"+_11b+"`1697`",_11c);
}else{
this.LogSeq("`1680`"+_11a+"`1727`"+_11b+"`1627`",_11c);
}
return _11f;
}
function Sequencer_FindCommonAncestor(_120,_121,_122){
Debug.AssertError("Parent log not passed.",(_122===undefined||_122===null));
var _123=new Array();
var _124=new Array();
if(_120!==null&&_120.IsTheRoot()){
this.LogSeq(_120+"`939`"+_120+"`1727`"+_121+"`1730`"+_120,_122);
return _120;
}
if(_121!==null&&_121.IsTheRoot()){
this.LogSeq(_121+"`939`"+_120+"`1727`"+_121+"`1730`"+_121,_122);
return _121;
}
if(_120!==null){
_123=this.Activities.GetActivityPath(_120,false);
}
if(_121!==null){
_124=this.Activities.GetActivityPath(_121,false);
}
for(var i=0;i<_123.length;i++){
for(var j=0;j<_124.length;j++){
if(_123[i]==_124[j]){
this.LogSeq("`1326`"+_120+"`1727`"+_121+"`1730`"+_123[i],_122);
return _123[i];
}
}
}
this.LogSeq("`1713`"+_120+"`1727`"+_121+"`1388`",_122);
return null;
}
function Sequencer_GetActivityPath(_127,_128){
return this.Activities.GetActivityPath(_127,_128);
}
function Sequencer_GetPathToAncestorExclusive(_129,_12a,_12b){
var _12c=new Array();
var _12d=0;
if(_129!==null&&_12a!==null&&_129!==_12a){
if(_12b===true){
_12c[_12d]=_129;
_12d++;
}
while(_129.ParentActivity!==null&&_129.ParentActivity!==_12a){
_129=_129.ParentActivity;
_12c[_12d]=_129;
_12d++;
}
}
return _12c;
}
function Sequencer_GetPathToAncestorInclusive(_12e,_12f,_130){
var _131=new Array();
var _132=0;
if(_130==null||_130==undefined){
_130===true;
}
_131[_132]=_12e;
_132++;
while(_12e.ParentActivity!==null&&_12e!=_12f){
_12e=_12e.ParentActivity;
_131[_132]=_12e;
_132++;
}
if(_130===false){
_131.splice(0,1);
}
return _131;
}
function Sequencer_ActivityHasSuspendedChildren(_133,_134){
Debug.AssertError("Parent log not passed.",(_134===undefined||_134===null));
var _135=_133.GetChildren();
var _136=false;
for(var i=0;i<_135.length;i++){
if(_135[i].IsSuspended()){
_136=true;
}
}
if(_136){
this.LogSeq("`1698`"+_133+"`1512`",_134);
}else{
this.LogSeq("`1698`"+_133+"`1338`",_134);
}
return _136;
}
function Sequencer_CourseIsSingleSco(){
if(this.Activities.ActivityList.length<=2){
return true;
}else{
return false;
}
}
function Sequencer_TranslateSequencingRuleActionIntoSequencingRequest(_138){
switch(_138){
case SEQUENCING_RULE_ACTION_RETRY:
return SEQUENCING_REQUEST_RETRY;
case SEQUENCING_RULE_ACTION_CONTINUE:
return SEQUENCING_REQUEST_CONTINUE;
case SEQUENCING_RULE_ACTION_PREVIOUS:
return SEQUENCING_REQUEST_PREVIOUS;
default:
Debug.AssertError("ERROR in TranslateSequencingRuleActionIntoSequencingRequest - should never have an untranslatable sequencing request. ruleAction="+_138);
return null;
}
}
function Sequencer_TranslateSequencingRuleActionIntoTerminationRequest(_139){
switch(_139){
case SEQUENCING_RULE_ACTION_EXIT_PARENT:
return TERMINATION_REQUEST_EXIT_PARENT;
case SEQUENCING_RULE_ACTION_EXIT_ALL:
return TERMINATION_REQUEST_EXIT_ALL;
default:
Debug.AssertError("ERROR in TranslateSequencingRuleActionIntoTerminationRequest - should never have an untranslatable sequencing request. ruleAction="+_139);
return null;
}
}
function Sequencer_IsActivity1BeforeActivity2(_13a,_13b,_13c){
Debug.AssertError("Parent log not passed.",(_13c===undefined||_13c===null));
var _13d=this.GetOrderedListOfActivities(false,_13c);
for(var i=0;i<_13d.length;i++){
if(_13d[i]==_13a){
this.LogSeq("`1698`"+_13a+"`1510`"+_13b,_13c);
return true;
}
if(_13d[i]==_13b){
this.LogSeq("`1698`"+_13a+"`1539`"+_13b,_13c);
return false;
}
}
Debug.AssertError("ERROR IN Sequencer_IsActivity1BeforeActivity2");
return null;
}
function Sequencer_GetOrderedListOfActivities(_13f,_140){
Debug.AssertError("Parent log not passed.",(_140===undefined||_140===null));
Debug.AssertError("Traversal Direction Not Passed.",(_13f===undefined||_13f===null));
var list;
var root=this.GetRootActivity(_140);
if(_13f===false){
list=this.PreOrderTraversal(root);
}else{
list=this.PostOrderTraversal(root);
}
return list;
}
function Sequencer_PreOrderTraversal(_143){
var list=new Array();
list[0]=_143;
var _145=_143.GetAvailableChildren();
var _146;
for(var i=0;i<_145.length;i++){
_146=this.PreOrderTraversal(_145[i]);
list=list.concat(_146);
}
return list;
}
function Sequencer_PostOrderTraversal(_148){
var list=new Array();
var _14a=_148.GetAvailableChildren();
var _14b;
for(var i=0;i<_14a.length;i++){
_14b=this.PostOrderTraversal(_14a[i]);
list=list.concat(_14b);
}
list=list.concat(_148);
return list;
}
function Sequencer_IsActivityLastOverall(_14d,_14e){
Debug.AssertError("Parent log not passed.",(_14e===undefined||_14e===null));
var _14f=this.GetOrderedListOfActivities(false,_14e);
var _150=null;
for(var i=(_14f.length-1);i>=0;i--){
if(_14f[i].IsAvailable()){
_150=_14f[i];
i=-1;
}
}
if(_14d==_150){
this.LogSeq("`1698`"+_14d+"`1405`",_14e);
return true;
}
this.LogSeq("`1698`"+_14d+"`1344`",_14e);
return false;
}
function Sequencer_GetGlobalObjectiveByIdentifier(_152){
for(var obj in this.GlobalObjectives){
if(this.GlobalObjectives[obj].ID==_152){
return this.GlobalObjectives[obj];
}
}
return null;
}
function Sequencer_AddGlobalObjective(ID,_155,_156,_157,_158){
var _159=this.GlobalObjectives.length;
var obj=new GlobalObjective(_159,ID,_155,_156,_157,_158);
this.GlobalObjectives[_159]=obj;
this.GlobalObjectives[_159].SetDirtyData();
}
function Sequencer_ResetGlobalObjectives(){
var _15b;
for(var obj in this.GlobalObjectives){
_15b=this.GlobalObjectives[obj];
_15b.ResetState();
}
}
function Sequencer_ResetSharedData(){
var sd;
for(var _15e in this.SharedData){
sd=this.SharedData[_15e];
sd.WriteData("");
}
}
function Sequencer_FindActivitiesAffectedByWriteMaps(_15f){
var _160=new Array();
var _161;
var _162=_15f.GetObjectives();
var _163=new Array();
var i;
var j;
for(i=0;i<_162.length;i++){
_161=_162[i].GetMaps();
for(j=0;j<_161.length;j++){
if(_161[j].WriteSatisfiedStatus===true||_161[j].WriteNormalizedMeasure===true||_161[j].WriteCompletionStatus===true||_161[j].WriteProgressMeasure===true){
_160[_160.length]=_161[j].TargetObjectiveId;
}
}
}
if(_160.length===0){
return _163;
}
var _166;
var _167;
var _168;
var _169;
for(var _16a in this.Activities.ActivityList){
_166=this.Activities.ActivityList[_16a];
if(_166!=_15f){
_167=_166.GetObjectives();
for(var _16b=0;_16b<_167.length;_16b++){
_168=_167[_16b].GetMaps();
for(var map=0;map<_168.length;map++){
if(_168[map].ReadSatisfiedStatus===true||_168[map].ReadNormalizedMeasure===true||_168[map].ReadCompletionStatus===true||_168[map].ReadProgressMeasure===true){
_169=_168[map].TargetObjectiveId;
for(var _16d=0;_16d<_160.length;_16d++){
if(_160[_16d]==_169){
_163[_163.length]=_166;
}
}
}
}
}
}
}
return _163;
}
function Sequencer_FindDistinctAncestorsOfActivitySet(_16e){
var _16f=new Array();
for(var _170=0;_170<_16e.length;_170++){
var _171=_16e[_170];
if(_171!==null){
var _172=this.GetActivityPath(_171,true);
for(var i=0;i<_172.length;i++){
var _174=true;
for(var j=0;j<_16f.length;j++){
if(_172[i]==_16f[j]){
_174=false;
break;
}
}
if(_174){
_16f[_16f.length]=_172[i];
}
}
}
}
return _16f;
}
function Sequencer_FindDistinctParentsOfActivitySet(_176){
var _177=new Array();
var _178;
var _179;
for(var _17a in _176){
_179=true;
_178=this.Activities.GetParentActivity(_176[_17a]);
if(_178!==null){
for(var i=0;i<_177.length;i++){
if(_177[i]==_178){
_179=false;
break;
}
}
if(_179){
_177[_177.length]=_178;
}
}
}
return _177;
}
function Sequencer_GetMinimalSubsetOfActivitiesToRollup(_17c,_17d){
var _17e=new Array();
var _17f=new Array();
var _180;
var _181;
var _182;
if(_17d!==null){
_17e=_17e.concat(_17d);
}
for(var _183 in _17c){
_181=_17c[_183];
_182=false;
for(var _184 in _17e){
if(_17e[_184]==_181){
_182=true;
break;
}
}
if(_182===false){
_17f[_17f.length]=_181;
_180=this.GetActivityPath(_181,true);
_17e=_17e.concat(_180);
}
}
return _17f;
}
function Sequencer_EvaluatePossibleNavigationRequests(_185){
var _186=this.LogSeqAudit("`989`");
var _187;
var _188=null;
var _189;
var _18a;
var id;
var _18c=this.LogSeqSimpleAudit("Start of simple logs in lookahead sequencer.",_18c);
if(Control.Package.Properties.UseQuickLookaheadSequencer===true){
var _18d=false;
this.LogSeq("`1024`",_186);
this.LogSeq("`983`",_186);
for(id in _185){
_185[id].ResetForNewEvaluation();
}
var _18e=this.GetOrderedListOfActivities(false,_186);
var _18f=this.GetOrderedListOfActivities(true,_186);
var _190;
var _191=new Array();
for(var i=0;i<_18e.length;i++){
if(_18e[i].IsActive()===true){
_191[_18e[i].GetItemIdentifier()]=true;
}
}
var _193=this.GetCurrentActivity();
this.LogSeq("`1381`"+_193,_186);
var _194=null;
if(_193!==null){
_194=_193.ParentActivity;
}
if(_193!==null&&_193.IsActive()===true){
this.LogSeq("`525`",_186);
_188=this.TerminationRequestProcess(TERMINATION_REQUEST_EXIT,_186,_18c);
}
var _195=this.LogSeqAudit("`392`",_186);
for(var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;i<_185.length;i++){
var _196=_185[i];
_196.TargetActivity=this.GetActivityFromIdentifier(_196.TargetActivityItemIdentifier,_186);
var _197=_196.TargetActivity;
var _198=_197.ParentActivity;
var _199=this.LogSeqAudit("`1725`"+_197,_195);
if(_198!==null&&_198.GetSequencingControlChoice()===false){
this.LogSeq("`1725`"+_197+"`800`"+_198+"`1234`",_199);
_196.WillSucceed=false;
_196.ControlChoiceViolation=true;
_196.Exception="NB.2.1-10";
_196.ExceptionText=IntegrationImplementation.GetString("Please select 'Next' or 'Previous' to move through {0}.",_198.GetTitle());
}
if(_197.IsALeaf()===false&&_197.GetSequencingControlFlow()===false){
this.LogSeq("`1725`"+_197+"`363`",_199);
_196.WillSucceed=false;
_196.NoDeliverablieActivityViolation=true;
_196.Exception="SB.2.2-1";
_196.ExceptionText=_197+" does not allow flow navigation so it's children must be selected explicitly.";
}
if(_197.LearningObject.Visible===false){
this.LogSeq("`1725`"+_197+"`850`",_199);
_196.WillSucceed=true;
_196.IsVisibleViolation=true;
_196.Exception="n/a";
_196.ExceptionText=_197+" is set to be invisible to the user.";
}
if(this.LimitConditionsCheckProcess(_197,_199,_18c)===true){
this.LogSeq("`1725`"+_197+"`597`",_199);
_196.WillSucceed=false;
_196.LimitConditionViolation=true;
_196.Exception="DB.1.1-3";
_196.ExceptionText=IntegrationImplementation.GetString("{0} has been attempted the maximum allowed number of times.",_197.GetTitle());
this.SetAllDescendentsToNotSucceed(_185,_197,"LimitConditionViolation",_196.Exception,_196.ExceptionText,_186);
}
this.LogSeq("`794`",_199);
var _19a=this.SequencingRulesCheckProcess(_185[i].TargetActivity,RULE_SET_HIDE_FROM_CHOICE,_199,_18c);
if(_19a!==null){
this.LogSeq("`1169`"+_197+"`1051`",_199);
_196.WillSucceed=false;
_196.PreConditionHiddenFromChoice=true;
_185[id].Exception="SB.2.9-3";
_185[id].ExceptionText="The activity "+_197.GetTitle()+" (and all of its descendents) should be hidden and is not a valid selection";
this.SetAllDescendentsToNotSucceed(_185,_197,"PreConditionHiddenFromChoice",_196.Exception,_196.ExceptionText,_199);
}
_19a=this.SequencingRulesCheckProcess(_185[i].TargetActivity,RULE_SET_DISABLED,_199,_18c);
if(_19a!==null){
this.LogSeq("`1169`"+_197+"`1026`",_199);
_196.WillSucceed=false;
_196.PreConditionDisabled=true;
_185[i].Exception="DB.1.1-3";
_185[i].ExceptionText=IntegrationImplementation.GetString("{0} (and all of its descendents) are not valid selections because of sequencing rules in this course.",_197.GetTitle());
this.SetAllDescendentsToNotSucceed(_185,_197,"PreConditionDisabled",_196.Exception,_196.ExceptionText,_199);
}
_19a=this.SequencingRulesCheckProcess(_185[i].TargetActivity,RULE_SET_STOP_FORWARD_TRAVERSAL,_199,_18c);
if(_19a!==null){
this.LogSeq("`1394`"+_197+"`1418`",_199);
_196.PreConditionStopForwardTraversal=true;
_18d=true;
}
_19a=this.SequencingRulesCheckProcess(_185[i].TargetActivity,RULE_SET_SKIPPED,_199,_18c);
if(_19a!==null){
this.LogSeq("`1725`"+_197+"`1163`",_199);
_196.PreConditionSkipped=true;
this.SetAllDescendentsToSkipped(_185,_197,_191,_199);
}
}
for(var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;i<_185.length;i++){
var _196=_185[i];
_197=_196.TargetActivity;
if(_197.GetPreventActivation()===true){
if(_191[_197.GetItemIdentifier()]!==true){
this.LogSeq("`1725`"+_197+"`522`",_186);
this.SetAllDescendentsToNotSucceed(_185,_197,"PreventActivationViolation","SB.2.9-6",_197+" (and all of its descendents) can not be activated with a choice request.",_186);
}
}
}
for(var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;i<_185.length;i++){
var _196=_185[i];
var _19b=(_196.PreConditionDisabled===false&&_196.PreConditionHiddenFromChoice===false&&_196.LimitConditionViolation===false&&_196.PreventActivationViolation===false);
if(_19b===true){
var _197=_196.TargetActivity;
var _19c=-1;
for(var j=0;j<_18e.length;j++){
if(_18e[j]==_197){
_19c=j;
break;
}
}
var _19e;
var _19f;
var _1a0=false;
for(var j=_19c;j<_18e.length;j++){
_19e=_18e[j];
_190=Control.FindPossibleChoiceRequestForActivity(_19e);
if(_190.PreConditionSkipped===false){
if(_19e.IsALeaf()===false&&_19e.GetSequencingControlFlow()===false){
this.LogSeq("`1650`"+_197+"`1472`"+_19e+"`1029`",_186);
_196.WillSucceed=false;
_196.NoDeliverablieActivityViolation=true;
_196.Exception=_190.Exception;
_196.ExceptionText="Selecting "+_197+" is not currently allowed. Please select another activity or use the previous/next button to navigate.";
_1a0=false;
break;
}else{
if(_190.PreConditionDisabled===true){
this.LogSeq("`1650`"+_197+"`1438`"+_19e+"`1383`",_186);
_196.WillSucceed=false;
_196.NoDeliverablieActivityViolation=true;
_196.Exception=_190.Exception;
_196.ExceptionText="Selecting "+_197+" is not currently allowed. Please select another activity or use the previous/next button to navigate.";
_1a0=false;
break;
}else{
if(_190.LimitConditionViolation===true){
this.LogSeq("`1650`"+_197+"`1438`"+_19e+"`1014`",_186);
_196.WillSucceed=false;
_196.NoDeliverablieActivityViolation=true;
_196.Exception=_190.Exception;
_196.ExceptionText="Selecting "+_197+" is not currently allowed. Please select another activity or use the previous/next button to navigate.";
_1a0=false;
break;
}else{
if(_19e.IsALeaf()===true){
_1a0=true;
break;
}
}
}
}
}
}
}
}
this.LogSeq("`816`",_186);
if(_193===null){
this.LogSeq("`730`",_186);
}else{
var _1a1=-1;
var _1a2=-1;
for(var i=0;i<_18e.length;i++){
if(_18e[i]==_193){
_1a1=i;
break;
}
}
for(var i=0;i<_18f.length;i++){
if(_18f[i]==_193){
_1a2=i;
break;
}
}
if(_194!==null&&_194.GetSequencingControlForwardOnly()===true){
this.LogSeq("`303`",_186);
var _1a3=_194.GetAvailableChildren();
for(var j=0;j<_1a3.length;j++){
var _1a4=_1a3[j];
if(_191[_1a4.GetItemIdentifier()]===true){
this.LogSeq("`1725`"+_1a4+"`1288`",_186);
break;
}else{
this.LogSeq("`1688`"+_1a4+"`1424`",_186);
_190=Control.FindPossibleChoiceRequestForActivity(_1a4);
_190.WillSucceed=false;
_190.ForwardOnlyViolation=true;
_190.Exception="SB.2.1-4";
_190.ExceptionText=IntegrationImplementation.GetString("The activity '{0}' may only be entered from the beginning.",_194.GetTitle());
this.SetAllDescendentsToNotSucceed(_185,_1a4,"ForwardOnlyViolation",_190.Exception,_190.ExceptionText,_186);
}
}
}
if(_18d===true){
var _1a5;
for(var i=_1a1;i<_18e.length;i++){
_1a5=_18e[i];
_190=Control.FindPossibleChoiceRequestForActivity(_1a5);
if(_190.PreConditionStopForwardTraversal===true){
if(_1a5.IsALeaf()===true){
if(_1a5!=_193){
this.LogSeq("`1688`"+_1a5+"`758`",_186);
_190.WillSucceed=false;
_190.Exception="SB.2.4-1";
_190.PreConditionStopForwardTraversalViolation=true;
_190.ExceptionText=IntegrationImplementation.GetString("You are not allowed to move into {0} yet.",_1a5.GetTitle());
}
}else{
this.LogSeq("`1330`"+_1a5+"`722`",_186);
this.SetAllDescendentsToNotSucceed(_185,_1a5,"PreConditionStopForwardTraversalViolation",_190.Exception,_190.ExceptionText,_186);
}
if(_1a5.IsTheRoot()===false){
this.LogSeq("`1243`"+_1a5+"`897`",_186);
var _1a6=_1a5.ParentActivity;
var _1a7=_1a6.GetAvailableChildren();
var _1a8=false;
for(var j=0;j<_1a7.length;j++){
if(_1a8===true){
this.LogSeq("`1726`"+_1a7[j]+"`854`",_186);
_190=Control.FindPossibleChoiceRequestForActivity(_1a7[j]);
_190.PreConditionStopForwardTraversalViolation=true;
_190.WillSucceed=false;
_190.Exception="SB.2.4-1";
_190.ExceptionText=IntegrationImplementation.GetString("You are not allowed to move into {0} yet.",_1a7[j].GetTitle());
this.SetAllDescendentsToNotSucceed(_185,_1a7[j],"PreConditionStopForwardTraversalViolation",_190.Exception,_190.ExceptionText,_186);
}
if(_1a7[j]==_1a5){
_1a8=true;
}
}
}
}
}
}
var _1a9=this.GetActivityPath(_193,true);
for(var i=0;i<_1a9.length;i++){
if(_1a9[i].GetSequencingControlChoiceExit()===false){
this.LogSeq("`1726`"+_1a9[i]+"`610`",_186);
var _1aa=_1a9[i];
for(var j=0;j<_18e.length;j++){
if(this.IsActivity1AParentOfActivity2(_1aa,_18e[j])===false){
this.LogSeq("`1726`"+_18e[j]+"`1220`",_186);
_190=Control.FindPossibleChoiceRequestForActivity(_18e[j]);
_190.WillSucceed=false;
_190.ChoiceExitViolation=true;
_190.Exception=CONTROL_CHOICE_EXIT_ERROR_CHOICE;
_190.ExceptionText=IntegrationImplementation.GetString("You are not allowed to jump out of {0}.",_1aa.GetTitle());
}
}
break;
}
}
for(var i=0;i<_1a9.length;i++){
if(_1a9[i].IsALeaf()===false&&_1a9[i].GetConstrainedChoice()===true){
var _1ab=_1a9[i];
this.LogSeq("`1726`"+_1a9[i]+"`58`",_186);
var _1ac=null;
var _1ad=-1;
for(var j=0;j<_18e.length;j++){
if(_18e[j]==_1ab){
_1ad=j;
break;
}
}
for(var j=(_1ad+1);j<_18e.length;j++){
if(_1ab.IsActivityAnAvailableDescendent(_18e[j])===false){
_1ac=_18e[j];
break;
}
}
var _1ae=null;
var _1af=-1;
for(var j=0;j<_18f.length;j++){
if(_18f[j]==_1ab){
_1af=j;
break;
}
}
for(var j=(_1af-1);j>0;j--){
if(_1ab.IsActivityAnAvailableDescendent(_18f[j])===false){
_1ae=_18f[j];
break;
}
}
var _1b0=this.GetArrayOfDescendents(_1ab);
_1b0=_1b0.concat(_1a9[_1a9.length-1]);
if(_1ae!==null){
this.LogSeq("`1177`"+_1ae,_186);
_1b0=_1b0.concat(this.GetArrayOfDescendents(_1ae));
}else{
this.LogSeq("`1072`",_186);
}
if(_1ac!==null){
this.LogSeq("`1260`"+_1ac,_186);
_1b0=_1b0.concat(this.GetArrayOfDescendents(_1ac));
}else{
this.LogSeq("`1155`",_186);
}
var _1b1=new Array();
for(var j=0;j<_1b0.length;j++){
_1b1[_1b0[j].GetItemIdentifier()]=_1b0[j];
}
for(var j=0;j<_18e.length;j++){
var _1b2=_1b1[_18e[j].GetItemIdentifier()];
if(_1b2===null||_1b2===undefined){
this.LogSeq("`1726`"+_18e[j]+"`793`",_186);
_190=Control.FindPossibleChoiceRequestForActivity(_18e[j]);
_190.WillSucceed=false;
_190.ConstrainedChoiceViolation=true;
_190.Exception=CONSTRAINED_CHOICE_ERROR;
_190.ExceptionText=IntegrationImplementation.GetString("You are not allowed to jump out of {0}.",_1a9[i].GetTitle());
}
}
break;
}
}
}
var _1b3=_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE];
var _1b4=_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS];
if(_193===null){
this.LogSeq("`838`",_186);
_1b3.WillSucceed=false;
_1b3.Exception="NB.2.1-1";
_1b3.ExceptionText=IntegrationImplementation.GetString("Cannot continue until the sequencing session has begun.");
_1b3.Disabled=true;
_1b4.WillSucceed=false;
_1b4.Exception="NB.2.1-2";
_1b4.ExceptionText=IntegrationImplementation.GetString("Cannot move backwards until the sequencing session has begun.");
_1b4.Disabled=true;
}else{
if(_194!==null){
if(_194.GetSequencingControlFlow()===false){
this.LogSeq("`1725`"+_194+"`882`",_186);
_1b3.WillSucceed=false;
_1b3.Exception="NB.2.1-4";
_1b3.ExceptionText=IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_194.GetTitle());
_1b3.Disabled=true;
_1b4.WillSucceed=false;
_1b4.Exception="NB.2.1-5";
_1b4.ExceptionText=IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_194.GetTitle());
_1b4.Disabled=true;
}
if(_194.GetSequencingControlForwardOnly()===true){
this.LogSeq("`1725`"+_194+"`842`",_186);
_1b4.WillSucceed=false;
_1b4.Exception="NB.2.1-5";
_1b4.ExceptionText=IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_194.GetTitle());
_1b4.Disabled=true;
}
}
var _1b5=null;
var _1b6;
var _1b7=true;
for(var i=(_1a2-1);i>=0;i--){
_1b6=_18f[i].ParentActivity;
if(_1b6!==null&&_1b6.GetSequencingControlFlow()===false){
this.LogSeq("`1447`"+_1b6+"`789`",_186);
_1b4.WillSucceed=false;
_1b4.Exception="NB.2.1-5";
_1b4.ExceptionText=IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_1b6.GetTitle());
_1b4.Disabled=true;
break;
}
_190=Control.FindPossibleChoiceRequestForActivity(_18f[i]);
if(_190.PreConditionSkipped===false){
if(_1b7===true){
if(_18f[i].IsALeaf()===false&&_18f[i].GetSequencingControlForwardOnly()===true){
_1b7=false;
this.LogSeq("`26`",_186);
}else{
if(_1b6!==null&&_1b6.GetSequencingControlForwardOnly()===true){
this.LogSeq("`1276`"+_1b6+"`785`",_186);
_1b4.WillSucceed=false;
_1b4.Exception="NB.2.1-5";
_1b4.ExceptionText=IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_1b6.GetTitle());
_1b4.Disabled=true;
break;
}
}
}
if(_18f[i].IsALeaf()===true){
_1b5=_18f[i];
break;
}
}
}
if(_1b5===null){
this.LogSeq("`493`",_186);
_1b4.WillSucceed=false;
_1b4.Exception="SB.2.1-3";
_1b4.ExceptionText=IntegrationImplementation.GetString("You have reached the beginning of the course.");
_1b4.Disabled=true;
}else{
if(_1b7===true){
_190=Control.FindPossibleChoiceRequestForActivity(_1b5);
if(_190.PreConditionDisabled===true){
this.LogSeq("`1248`"+_1b5+"`868`",_186);
_1b4.WillSucceed=false;
_1b4.Exception="SB.2.2-2";
_1b4.ExceptionText=IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.",activity.GetTitle());
_1b4.Disabled=true;
}else{
if(_190.LimitConditionViolation===true){
this.LogSeq("`1248`"+_1b5+"`669`",_186);
_1b4.WillSucceed=false;
_1b4.Exception="SB.2.2-2";
_1b4.ExceptionText=IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.",activity.GetTitle());
_1b4.Disabled=true;
}
}
}
}
var _1b8=null;
for(var i=(_1a1+1);i<_18e.length;i++){
_1b6=_18e[i].ParentActivity;
if(_1b6!==null&&_1b6.GetSequencingControlFlow()===false){
this.LogSeq("`1447`"+_1b6+"`129`",_186);
_190=Control.FindPossibleChoiceRequestForActivity(_1b6);
if(_190.PreConditionSkipped===false){
_1b8=_18e[i];
break;
}
}
if(_18e[i].IsALeaf()===true){
_190=Control.FindPossibleChoiceRequestForActivity(_18e[i]);
if(_190.PreConditionSkipped===false){
_1b8=_18e[i];
break;
}
}
}
if(_1b8!==null){
_190=Control.FindPossibleChoiceRequestForActivity(_1b8);
if(_190.PreConditionDisabled===true){
this.LogSeq("`1302`"+_1b8+"`899`",_186);
_1b3.WillSucceed=false;
_1b3.Exception="SB.2.2-2";
_1b3.ExceptionText=IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.",activity.GetTitle());
_1b3.Disabled=true;
}else{
if(_190.LimitConditionViolation===true){
this.LogSeq("`1302`"+_1b8+"`715`",_186);
_1b3.WillSucceed=false;
_1b3.Exception="SB.2.2-2";
_1b3.ExceptionText=IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.",activity.GetTitle());
_1b3.Disabled=true;
}
}
}
}
for(var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;i<_185.length;i++){
if(_185[i].PreConditionStopForwardTraversalViolation||_185[i].PreConditionDisabled||_185[i].LimitConditionViolation||_185[i].ControlChoiceViolation||_185[i].ForwardOnlyViolation||_185[i].NoDeliverablieActivityViolation){
_185[i].Disabled=true;
}
if(_185[i].PreConditionHiddenFromChoice||_185[i].IsVisibleViolation||_185[i].PreventActivationViolation||_185[i].ConstrainedChoiceViolation||_185[i].ChoiceExitViolation){
_185[i].Hidden=true;
}
}
}else{
for(id in _185){
if(_185[id].WillAlwaysSucceed===true){
_185[id].WillSucceed=true;
}else{
if(_185[id].WillNeverSucceed===true){
_185[id].WillSucceed=false;
_185[id].Exception="NB.2.1-10";
_185[id].ExceptionText=IntegrationImplementation.GetString("Your selection is not permitted. Please select 'Next' or 'Previous' to move through '{0}.'");
}else{
_185[id].WillSucceed=null;
}
}
_185[id].Hidden=false;
_185[id].Disabled=false;
}
this.LogSeq("`795`",_186);
for(id in _185){
if(_185[id].WillSucceed===null){
_187=this.NavigationRequestProcess(_185[id].NavigationRequest,_185[id].TargetActivityItemIdentifier,_186,_18c);
if(_187.NavigationRequest==NAVIGATION_REQUEST_NOT_VALID){
this.LogSeq("`760`",_186);
_185[id].WillSucceed=false;
_185[id].TargetActivity=this.GetActivityFromIdentifier(_185[id].TargetActivityItemIdentifier,_186);
_185[id].Exception=_187.Exception;
_185[id].ExceptionText=_187.ExceptionText;
}else{
this.LogSeq("`743`",_186);
_185[id].WillSucceed=true;
_185[id].TargetActivity=_187.TargetActivity;
_185[id].SequencingRequest=_187.SequencingRequest;
_185[id].Exception="";
_185[id].ExceptionText="";
}
}
}
this.LogSeq("`215`",_186);
var _193=this.GetCurrentActivity();
if(_193!==null&&_193.IsActive()===true){
this.LogSeq("`953`",_186);
_188=this.TerminationRequestProcess(TERMINATION_REQUEST_EXIT,_186,_18c);
this.LogSeq("`906`",_186);
if(_188.TerminationRequest==TERMINATION_REQUEST_NOT_VALID){
this.LogSeq("`720`",_186);
if(_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed){
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed=false;
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].Exception=_188.Exception;
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].ExceptionText=_188.ExceptionText;
}
if(_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed){
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed=false;
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].Exception=_188.Exception;
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].ExceptionText=_188.ExceptionText;
}
if(_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed){
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed=false;
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].Exception=_188.Exception;
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].ExceptionText=_188.ExceptionText;
}
for(id=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;id<_185.length;id++){
if(_185[id].WillSucceed){
_185[id].WillSucceed=false;
_185[id].Exception=_188.Exception;
_185[id].ExceptionText=terminatonRequestResult.ExceptionText;
}else{
_185[id].TerminationSequencingRequest=_188.SequencingRequest;
}
}
}
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].TerminationSequencingRequest=_188.SequencingRequest;
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].TerminationSequencingRequest=_188.SequencingRequest;
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].TerminationSequencingRequest=_188.SequencingRequest;
for(id=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;id<_185.length;id++){
_185[id].TerminationSequencingRequest=_188.SequencingRequest;
}
}
this.LogSeq("`660`",_186);
for(id in _185){
if(id>=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE&&_185[id].WillSucceed===true&&_185[id].WillAlwaysSucceed===false&&_185[id].WillNeverSucceed===false){
this.LogSeq("`869`",_186);
var _1b9=this.CheckActivityProcess(_185[id].TargetActivity,_186,_18c);
if(_1b9===true){
this.LogSeq("`751`",_186);
_185[id].WillSucceed=false;
_185[id].Disabled=true;
this.SetAllDescendentsToDisabled(_185,_185[id].TargetActivity);
}
}
}
this.LogSeq("`678`",_186);
var _1ba=null;
for(id in _185){
if(_185[id].WillAlwaysSucceed===false&&_185[id].WillNeverSucceed===false){
this.LogSeq("`625`",_186);
if(_185[id].TerminationSequencingRequest!==null){
this.LogSeq("`420`",_186);
if(_1ba===null){
_189=this.SequencingRequestProcess(_185[id].TerminationSequencingRequest,null,_186,_18c);
}else{
_189=_1ba;
}
if(id>=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE){
this.LogSeq("`1036`",_186);
var _19a=this.SequencingRulesCheckProcess(_185[id].TargetActivity,RULE_SET_HIDE_FROM_CHOICE,_186,_18c);
if(_19a!==null){
_185[id].Exception="SB.2.9-3";
_185[id].ExceptionText="The activity "+_185[id].TargetActivity.GetTitle()+" should be hidden and is not a valid selection";
_185[id].Hidden=true;
}
}
}else{
if(_185[id].WillSucceed===true){
this.LogSeq("`1635`",_186);
this.LogSeq("`721`",_186);
_189=this.SequencingRequestProcess(_185[id].SequencingRequest,_185[id].TargetActivity,_186,_18c);
}
}
if(_185[id].WillSucceed===true){
this.LogSeq("`841`",_186);
if(_189.Exception!==null){
this.LogSeq("`1277`",_186);
_185[id].WillSucceed=false;
_185[id].Exception=_189.Exception;
_185[id].ExceptionText=_189.ExceptionText;
_185[id].Hidden=_189.Hidden;
}else{
if(_189.DeliveryRequest!==null){
this.LogSeq("`1117`",_186);
_18a=this.DeliveryRequestProcess(_189.DeliveryRequest,_186,_18c);
this.LogSeq("`714`"+_18a.Valid+")",_186);
_185[id].WillSucceed=_18a.Valid;
}
}
}
}
}
this.LogSeq("`360`",_186);
var _1bb;
for(id in _185){
if(id>=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE){
if(_185[id].WillSucceed===false){
_1bb=_185[id].Exception;
if(_1bb==CONTROL_CHOICE_EXIT_ERROR_NAV||_1bb==CONTROL_CHOICE_EXIT_ERROR_CHOICE||_1bb==PREVENT_ACTIVATION_ERROR||_1bb==CONSTRAINED_CHOICE_ERROR){
this.LogSeq("`1462`"+id+"`1703`"+_1bb,_186);
_185[id].Hidden=true;
}
}
}
}
var _194=this.Activities.GetParentActivity(_193);
if(_194!=null){
if(_194.GetSequencingControlFlow()===true){
this.LogSeq("`430`",_186);
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed=true;
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].Exception="";
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].ExceptionText="";
}else{
this.LogSeq("`409`",_186);
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed=false;
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].Exception="SB.2.2-1";
_185[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].ExceptionText="Parent activity does not allow flow traversal";
}
}
}
this.LogSeqReturn("",_186);
return _185;
}
function Sequencer_SetAllDescendentsToDisabled(_1bc,_1bd){
for(var i=0;i<_1bd.ChildActivities.length;i++){
var _1bf=Control.FindPossibleChoiceRequestForActivity(_1bd.ChildActivities[i]);
_1bf.WillSucceed=false;
_1bf.Disabled=true;
this.SetAllDescendentsToDisabled(_1bc,_1bd.ChildActivities[i]);
}
}
function Sequencer_SetAllDescendentsToNotSucceed(_1c0,_1c1,_1c2,_1c3,_1c4,_1c5){
for(var i=0;i<_1c1.ChildActivities.length;i++){
var _1c7=Control.FindPossibleChoiceRequestForActivity(_1c1.ChildActivities[i]);
_1c7.WillSucceed=false;
_1c7.Exception=_1c3;
_1c7.ExceptionText=_1c4;
switch(_1c2){
case "PreventActivationViolation":
_1c7.PreventActivationViolation=true;
this.LogSeq("`1725`"+_1c1.ChildActivities[i]+"`1278`",_1c5);
break;
case "LimitConditionViolation":
_1c7.LimitConditionViolation=true;
this.LogSeq("`1725`"+_1c1.ChildActivities[i]+"`1245`",_1c5);
break;
case "PreConditionHiddenFromChoice":
_1c7.PreConditionHiddenFromChoice=true;
this.LogSeq("`1725`"+_1c1.ChildActivities[i]+"`1278`",_1c5);
break;
case "PreConditionDisabled":
_1c7.PreConditionDisabled=true;
this.LogSeq("`1725`"+_1c1.ChildActivities[i]+"`1245`",_1c5);
break;
case "ForwardOnlyViolation":
_1c7.ForwardOnlyViolation=true;
this.LogSeq("`1725`"+_1c1.ChildActivities[i]+"`1245`",_1c5);
break;
case "PreConditionStopForwardTraversalViolation":
_1c7.PreConditionStopForwardTraversalViolation=true;
this.LogSeq("`1725`"+_1c1.ChildActivities[i]+"`1245`",_1c5);
break;
}
this.SetAllDescendentsToNotSucceed(_1c0,_1c1.ChildActivities[i],_1c2,_1c3,_1c4,_1c5);
}
}
function Sequencer_SetAllDescendentsToSkipped(_1c8,_1c9,_1ca,_1cb){
this.LogSeq("`1681`"+_1c9+"`1094`"+_1ca[_1c9.GetItemIdentifier()],_1cb);
if(_1ca[_1c9.GetItemIdentifier()]===true){
return;
}
for(var i=0;i<_1c9.ChildActivities.length;i++){
var _1cd=Control.FindPossibleChoiceRequestForActivity(_1c9.ChildActivities[i]);
_1cd.PreConditionSkipped=true;
this.SetAllDescendentsToSkipped(_1c8,_1c9.ChildActivities[i],_1ca,_1cb);
}
}
function Sequencer_InitializePossibleNavigationRequestAbsolutes(_1ce,_1cf,_1d0){
var _1d1=this.LogSeqAudit("`1017`");
this.CheckForRelevantSequencingRules(_1cf,false);
var _1d2;
var _1d3;
var _1d4=false;
for(var _1d5 in _1d0){
_1d2=_1d0[_1d5];
var _1d6=this.LogSeqAudit(_1d2.StringIdentifier,_1d1);
if(_1d2.GetSequencingControlChoice()===false){
this.LogSeqAudit("`1314`",_1d6);
var _1d7=this.LogSeqAudit("`840`"+_1d2.ChildActivities.length+")",_1d6);
for(var i=0;i<_1d2.ChildActivities.length;i++){
_1d3=Control.FindPossibleChoiceRequestForActivity(_1d2.ChildActivities[i]);
_1d3.WillNeverSucceed=true;
this.LogSeqAudit(_1d2.ChildActivities[i].StringIdentifier,_1d7);
}
}
_1d4=_1d4||(_1d2.GetSequencingControlChoiceExit()===true);
this.LogSeqAudit("`1473`"+_1d4,_1d6);
_1d3=Control.FindPossibleChoiceRequestForActivity(_1d2);
if(_1d2.IsDeliverable()===false&&_1d2.GetSequencingControlFlow()===false){
this.LogSeqAudit("`354`",_1d6);
_1d3.WillNeverSucceed=true;
}
if(_1d2.HasChildActivitiesDeliverableViaFlow===false){
this.LogSeqAudit("`503`",_1d6);
_1d3.WillNeverSucceed=true;
}
if(_1d2.HasSeqRulesRelevantToChoice===false&&_1d3.WillNeverSucceed===false){
this.LogSeqAudit("`272`",_1d6);
_1d3.WillAlwaysSucceed=true;
}else{
this.LogSeqAudit("`246`",_1d6);
_1d3.WillAlwaysSucceed=false;
}
this.LogSeqAudit("`1582`"+_1d3.WillAlwaysSucceed,_1d6);
this.LogSeqAudit("`1584`"+_1d3.WillNeverSucceed,_1d6);
}
if(_1d4===true){
this.LogSeqAudit("`544`",_1d1);
for(var id in _1ce){
_1ce[id].WillAlwaysSucceed=false;
}
}
}
function Sequencer_CheckForRelevantSequencingRules(_1da,_1db){
if(_1db===true){
_1da.HasSeqRulesRelevantToChoice=true;
}else{
if(this.DoesThisActivityHaveSequencingRulesRelevantToChoice(_1da)){
_1da.HasSeqRulesRelevantToChoice=true;
}else{
_1da.HasSeqRulesRelevantToChoice=false;
}
}
var _1dc=false;
for(var i=0;i<_1da.ChildActivities.length;i++){
this.CheckForRelevantSequencingRules(_1da.ChildActivities[i],_1da.HasSeqRulesRelevantToChoice);
_1dc=(_1dc||_1da.ChildActivities[i].HasChildActivitiesDeliverableViaFlow);
}
_1da.HasChildActivitiesDeliverableViaFlow=(_1da.IsDeliverable()||(_1da.GetSequencingControlFlow()&&_1dc));
}
function Sequencer_DoesThisActivityHaveSequencingRulesRelevantToChoice(_1de){
if(_1de.GetSequencingControlForwardOnly()===true){
return true;
}
if(_1de.GetPreventActivation()===true){
return true;
}
if(_1de.GetConstrainedChoice()===true){
return true;
}
if(_1de.GetSequencingControlChoiceExit()===false){
return true;
}
var _1df=_1de.GetPreConditionRules();
for(var i=0;i<_1df.length;i++){
if(_1df[i].Action==SEQUENCING_RULE_ACTION_DISABLED||_1df[i].Action==SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE||_1df[i].Action==SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL){
return true;
}
}
return false;
}
function Sequencer_GetArrayOfDescendents(_1e1){
var _1e2=new Array();
_1e2[_1e2.length]=_1e1;
var _1e3=_1e1.GetChildren();
for(var i=0;i<_1e3.length;i++){
var _1e5=this.GetArrayOfDescendents(_1e3[i]);
_1e2=_1e2.concat(_1e5);
}
return _1e2;
}
function Sequencer_IsActivity1AParentOfActivity2(_1e6,_1e7){
var _1e8=false;
var _1e9=null;
if(_1e7.ParentActivity!==null){
_1e9=_1e7.ParentActivity;
}
while(_1e9!==null){
if(_1e9.GetItemIdentifier()==_1e6.GetItemIdentifier()){
_1e8=true;
break;
}
_1e9=_1e9.ParentActivity;
}
return _1e8;
}
function Sequencer_ActivityProgressRollupProcess(_1ea,_1eb,_1ec){
Debug.AssertError("Calling log not passed.",(_1eb===undefined||_1eb===null));
Debug.AssertError("Simple calling log not passed.",(_1ec===undefined||_1ec===null));
var _1ed=this.LogSeqAudit("`1191`"+_1ea+")",_1eb);
_1ec=this.LogSeqSimpleAudit("Rolling up the completion status of \""+_1ea+"\".",_1ec);
this.LogSeq("`91`",_1ed);
if(_1ea.GetCompletedByMeasure()===true){
this.LogSeq("`852`",_1ed);
this.ActivityProgressRollupUsingMeasureProcess(_1ea,_1ed,_1ec);
}else{
this.LogSeq("`1686`",_1ed);
this.LogSeq("`855`",_1ed);
this.ActivityProgressRollupUsingRulesProcess(_1ea,_1ed,_1ec);
}
this.LogSeq("`1056`",_1ed);
this.LogSeqReturn("",_1ed);
return;
}
function Sequencer_ActivityProgressRollupUsingMeasureProcess(_1ee,_1ef,_1f0){
Debug.AssertError("Calling log not passed.",(_1ef===undefined||_1ef===null));
Debug.AssertError("Simple calling log not passed.",(_1f0===undefined||_1f0===null));
var _1f1=this.LogSeqAudit("`913`"+_1ee+")",_1ef);
this.LogSeq("`1092`",_1f1);
if(_1ee.IsTracked()===false){
this.LogSeqReturn("",_1f1);
return;
}
this.LogSeq("`1040`",_1f1);
_1ee.SetAttemptProgressStatus(false);
this.LogSeq("`988`",_1f1);
_1ee.SetAttemptCompletionStatus(false);
this.LogSeq("`134`",_1f1);
if(_1ee.GetCompletedByMeasure()===true){
this.LogSeq("`373`",_1f1);
if(_1ee.GetAttemptCompletionAmountStatus()===false){
this.LogSeq("`903`",_1f1);
_1ee.SetAttemptCompletionStatus(false);
this.LogSeqSimple("\""+_1ee+"\" is completed by measure, but does not have a known progress measure so its completion status will be unknown.",_1f0);
}else{
this.LogSeq("`1610`",_1f1);
this.LogSeq("`444`",_1f1);
var _1f2=_1ee.GetAttemptCompletionAmount();
var _1f3=_1ee.GetMinProgressMeasure();
if(_1f2>=_1f3){
this.LogSeq("`935`",_1f1);
_1ee.SetAttemptProgressStatus(true);
this.LogSeq("`871`",_1f1);
_1ee.SetAttemptCompletionStatus(true);
this.LogSeqSimple("\""+_1ee+"\" is completed by measure, and its progress measure ("+_1f2+") is greater than the minimum progress measure ("+_1f3+") so its completion status will be completed.",_1f0);
}else{
this.LogSeq("`1544`",_1f1);
this.LogSeq("`959`",_1f1);
_1ee.SetAttemptProgressStatus(true);
this.LogSeq("`884`",_1f1);
_1ee.SetAttemptCompletionStatus(false);
this.LogSeqSimple("\""+_1ee+"\" is completed by measure, and its progress measure ("+_1f2+") is less than the minimum progress measure ("+_1f3+") so its completion status will be incomplete.",_1f0);
}
}
}else{
this.LogSeq("`1662`",_1f1);
this.LogSeq("`461`",_1f1);
_1ee.SetAttemptProgressStatus(false);
}
this.LogSeq("`859`",_1f1);
this.LogSeqReturn("",_1f1);
return;
}
function Sequencer_ActivityProgressRollupUsingRulesProcess(_1f4,_1f5,_1f6){
Debug.AssertError("Calling log not passed.",(_1f5===undefined||_1f5===null));
Debug.AssertError("Simple calling log not passed.",(_1f6===undefined||_1f6===null));
var _1f7=this.LogSeqAudit("`948`"+_1f4+")",_1f5);
this.LogSeq("`33`",_1f7);
if(Sequencer_GetApplicableSetofRollupRules(_1f4,RULE_SET_INCOMPLETE).length===0&&Sequencer_GetApplicableSetofRollupRules(_1f4,RULE_SET_COMPLETED).length===0){
this.LogSeqSimple("Applying the default set of completion rollup rules to \""+_1f4+"\".",_1f6);
this.LogSeq("`113`",_1f7);
_1f4.ApplyRollupRule(new SequencingRollupRule(RULE_CONDITION_COMBINATION_ANY,CHILD_ACTIVITY_SET_ALL,ROLLUP_RULE_MINIMUM_COUNT_DEFAULT,ROLLUP_RULE_MINIMUM_PERCENT_DEFAULT,ROLLUP_RULE_ACTION_COMPLETED,new Array(new SequencingRollupRuleCondition(RULE_CONDITION_OPERATOR_NOOP,SEQUENCING_RULE_CONDITION_COMPLETED))));
this.LogSeq("`70`",_1f7);
_1f4.ApplyRollupRule(new SequencingRollupRule(RULE_CONDITION_COMBINATION_ANY,CHILD_ACTIVITY_SET_ALL,ROLLUP_RULE_MINIMUM_COUNT_DEFAULT,ROLLUP_RULE_MINIMUM_PERCENT_DEFAULT,ROLLUP_RULE_ACTION_INCOMPLETE,new Array(new SequencingRollupRuleCondition(RULE_CONDITION_OPERATOR_NOOP,SEQUENCING_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN))));
}else{
if(_1f4.UsesDefaultCompletionRollupRules()===true){
this.LogSeqSimple("Applying the default set of completion rollup rules to \""+_1f4+"\".",_1f6);
}
}
var _1f8;
this.LogSeq("`313`",_1f7);
_1f8=this.RollupRuleCheckSubprocess(_1f4,RULE_SET_INCOMPLETE,_1f7,_1f6);
this.LogSeq("`806`",_1f7);
if(_1f8===true){
this.LogSeqSimple("Setting \""+_1f4+"\" to incomplete.",_1f6);
this.LogSeq("`757`",_1f7);
_1f4.SetAttemptProgressStatus(true);
this.LogSeq("`718`",_1f7);
_1f4.SetAttemptCompletionStatus(false);
}
this.LogSeq("`322`",_1f7);
_1f8=this.RollupRuleCheckSubprocess(_1f4,RULE_SET_COMPLETED,_1f7,_1f6);
this.LogSeq("`827`",_1f7);
if(_1f8===true){
this.LogSeqSimple("Setting \""+_1f4+"\" to completed.",_1f6);
this.LogSeq("`761`",_1f7);
_1f4.SetAttemptProgressStatus(true);
this.LogSeq("`726`",_1f7);
_1f4.SetAttemptCompletionStatus(true);
}
this.LogSeq("`1021`",_1f7);
this.LogSeqReturn("",_1f7);
return;
}
function Sequencer_CheckActivityProcess(_1f9,_1fa,_1fb){
Debug.AssertError("Calling log not passed.",(_1fa===undefined||_1fa===null));
Debug.AssertError("Simple calling log not passed.",(_1fb===undefined||_1fb===null));
var _1fc=this.LogSeqAudit("`1395`"+_1f9+")",_1fa);
this.LogSeq("`308`",_1fc);
var _1fd=this.SequencingRulesCheckProcess(_1f9,RULE_SET_DISABLED,_1fc,_1fb);
this.LogSeq("`781`",_1fc);
if(_1fd!==null){
this.LogSeq("`717`",_1fc);
this.LogSeqReturn("`1732`",_1fc);
this.LogSeqSimple("\""+_1f9+"\" (and its descendents) can not be delivered because sequencing rules make it currently disabled.",_1fb);
return true;
}
this.LogSeq("`393`",_1fc);
var _1fe=this.LimitConditionsCheckProcess(_1f9,_1fc,_1fb);
this.LogSeq("`860`",_1fc);
if(_1fe){
this.LogSeq("`609`",_1fc);
this.LogSeqReturn("`1732`",_1fc);
this.LogSeqSimple("\""+_1f9+"\" (and its descendents) can not be delivered because has exceeded its attempt limit.",_1fb);
return true;
}
this.LogSeq("`747`",_1fc);
this.LogSeqReturn("`1728`",_1fc);
return false;
}
function Sequencer_CheckChildForRollupSubprocess(_1ff,_200,_201,_202){
Debug.AssertError("Calling log not passed.",(_201===undefined||_201===null));
Debug.AssertError("Simple calling log not passed.",(_202===undefined||_202===null));
var _203=this.LogSeqAudit("`1119`"+_1ff+", "+_200+")",_201);
var _204;
this.LogSeq("`1317`",_203);
var _205=false;
this.LogSeq("`815`",_203);
if(_200==ROLLUP_RULE_ACTION_SATISFIED||_200==ROLLUP_RULE_ACTION_NOT_SATISFIED){
this.LogSeq("`412`",_203);
if(_1ff.GetRollupObjectiveSatisfied()===true){
this.LogSeq("`594`",_203);
_205=true;
var _206=_1ff.GetRequiredForSatisfied();
var _207=_1ff.GetRequiredForNotSatisfied();
this.LogSeq("`98`",_203);
if((_200==ROLLUP_RULE_ACTION_SATISFIED&&_206==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)||(_200==ROLLUP_RULE_ACTION_NOT_SATISFIED&&_207==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)){
this.LogSeq("`90`",_203);
if(_1ff.GetActivityProgressStatus()===false||(_1ff.GetAttemptCount()>0&&_1ff.IsSuspended()===true)){
this.LogSeq("`1201`",_203);
_205=false;
this.LogSeqSimple("\""+_1ff+"\" is not included in rollup because it is currently suspended and the sequencing rules indicate that it should only be included if it is not suspended.",_202);
}
}else{
this.LogSeq("`1558`",_203);
this.LogSeq("`104`",_203);
if((_200==ROLLUP_RULE_ACTION_SATISFIED&&_206==ROLLUP_CONSIDERATION_IF_ATTEMPTED)||(_200==ROLLUP_RULE_ACTION_NOT_SATISFIED&&_207==ROLLUP_CONSIDERATION_IF_ATTEMPTED)){
this.LogSeq("`330`",_203);
if(_1ff.GetActivityProgressStatus()===false||_1ff.GetAttemptCount()===0){
this.LogSeq("`1147`",_203);
_205=false;
this.LogSeqSimple("\""+_1ff+"\" is not included in rollup because it has not been attempted yet and the sequencing rules indicate that it should only be included if it has been attempted.",_202);
}
}else{
this.LogSeq("`1531`",_203);
this.LogSeq("`97`",_203);
if((_200==ROLLUP_RULE_ACTION_SATISFIED&&_206==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)||(_200==ROLLUP_RULE_ACTION_NOT_SATISFIED&&_207==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)){
this.LogSeq("`468`",_203);
_204=this.SequencingRulesCheckProcess(_1ff,RULE_SET_SKIPPED,_203,_202);
this.LogSeq("`635`",_203);
if(_204!==null){
this.LogSeq("`1095`",_203);
_205=false;
this.LogSeqSimple("\""+_1ff+"\" is not included in rollup because it is currently skipped and the sequencing rules indicate that it should only be included if it is not skipped.",_202);
}
}
}
}
}
}
this.LogSeq("`844`",_203);
if(_200==ROLLUP_RULE_ACTION_COMPLETED||_200==ROLLUP_RULE_ACTION_INCOMPLETE){
this.LogSeq("`423`",_203);
if(_1ff.RollupProgressCompletion()===true){
this.LogSeq("`591`",_203);
_205=true;
var _208=_1ff.GetRequiredForCompleted();
var _209=_1ff.GetRequiredForIncomplete();
this.LogSeq("`106`",_203);
if((_200==ROLLUP_RULE_ACTION_COMPLETED&&_208==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)||(_200==ROLLUP_RULE_ACTION_INCOMPLETE&&_209==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)){
this.LogSeq("`93`",_203);
if(_1ff.GetActivityProgressStatus()===false||(_1ff.GetAttemptCount()>0&&_1ff.IsSuspended()===true)){
this.LogSeq("`1361`",_203);
_205=false;
this.LogSeqSimple("\""+_1ff+"\" is not included in rollup because it is currently suspended and the sequencing rules indicate that it should only be included if it is not suspended.",_202);
}
}else{
this.LogSeq("`1555`",_203);
this.LogSeq("`118`",_203);
if((_200==ROLLUP_RULE_ACTION_COMPLETED&&_208==ROLLUP_CONSIDERATION_IF_ATTEMPTED)||(_200==ROLLUP_RULE_ACTION_INCOMPLETE&&_209==ROLLUP_CONSIDERATION_IF_ATTEMPTED)){
this.LogSeq("`332`",_203);
if(_1ff.GetActivityProgressStatus()===false||_1ff.GetAttemptCount()===0){
this.LogSeq("`1128`",_203);
_205=false;
this.LogSeqSimple("\""+_1ff+"\" is not included in rollup because it is has not been attempted yet and the sequencing rules indicate that it should only be included if it has been attempted.",_202);
}
}else{
this.LogSeq("`1523`",_203);
this.LogSeq("`108`",_203);
if((_200==ROLLUP_RULE_ACTION_COMPLETED&&_208==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)||(_200==ROLLUP_RULE_ACTION_INCOMPLETE&&_209==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)){
this.LogSeq("`469`",_203);
_204=this.SequencingRulesCheckProcess(_1ff,RULE_SET_SKIPPED,_203,_202);
this.LogSeq("`630`",_203);
if(_204!==null){
this.LogSeq("`1101`",_203);
_205=false;
this.LogSeqSimple("\""+_1ff+"\" is not included in rollup because it is currently skipped and the sequencing rules indicate that it should only be included if it is not skipped.",_202);
}
}
}
}
}
}
this.LogSeq("`599`",_203);
this.LogSeqReturn(_205,_203);
return _205;
}
function Sequencer_ChoiceActivityTraversalSubprocess(_20a,_20b,_20c,_20d){
Debug.AssertError("Calling log not passed.",(_20c===undefined||_20c===null));
Debug.AssertError("Simple calling log not passed.",(_20d===undefined||_20d===null));
var _20e=this.LogSeqAudit("`1097`"+_20a+", "+_20b+")",_20c);
var _20f=null;
var _210=null;
var _211;
this.LogSeq("`981`",_20e);
if(_20b==FLOW_DIRECTION_FORWARD){
this.LogSeq("`443`",_20e);
_20f=this.SequencingRulesCheckProcess(_20a,RULE_SET_STOP_FORWARD_TRAVERSAL,_20e,_20d);
this.LogSeq("`727`",_20e);
if(_20f!==null){
this.LogSeqSimple("Moving beyond \""+_20a+"\" is curently disallowed because of a stop forward traversal sequencing rule. This choice request is not allowed.",_20d);
this.LogSeq("`559`",_20e);
_211=new Sequencer_ChoiceActivityTraversalSubprocessResult(false,"SB.2.4-1",IntegrationImplementation.GetString("You are not allowed to move into {0} yet.",_20a.GetTitle()));
this.LogSeqReturn(_211,_20e);
return _211;
}
this.LogSeq("`614`",_20e);
_211=new Sequencer_ChoiceActivityTraversalSubprocessResult(true,null);
this.LogSeqReturn(_211,_20e);
return _211;
}
this.LogSeq("`970`",_20e);
if(_20b==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`1096`",_20e);
if(!_20a.IsTheRoot()){
_210=this.Activities.GetParentActivity(_20a);
this.LogSeq("`585`",_20e);
if(_210.GetSequencingControlForwardOnly()){
this.LogSeqSimple("Moving to \""+_20a+"\" from a subesequent activity is not allowed because "+_210+" has sequencing control forward only. This choice request is not allowed.",_20d);
this.LogSeq("`547`",_20e);
_211=new Sequencer_ChoiceActivityTraversalSubprocessResult(false,"SB.2.4-2",IntegrationImplementation.GetString("You must start {0} at the beginning.",_210.GetTitle()));
this.LogSeqReturn(_211,_20e);
return _211;
}
}else{
this.LogSeq("`1607`",_20e);
this.LogSeq("`238`",_20e);
_211=new Sequencer_ChoiceActivityTraversalSubprocessResult(false,"SB.2.4-3",IntegrationImplementation.GetString("You have reached the beginning of the course."));
this.LogSeqReturn(_211,_20e);
return _211;
}
this.LogSeq("`607`",_20e);
_211=new Sequencer_ChoiceActivityTraversalSubprocessResult(true,null);
this.LogSeqReturn(_211,_20e);
return _211;
}
}
function Sequencer_ChoiceActivityTraversalSubprocessResult(_212,_213,_214){
this.Reachable=_212;
this.Exception=_213;
this.ExceptionText=_214;
}
Sequencer_ChoiceActivityTraversalSubprocessResult.prototype.toString=function(){
return "Reachable="+this.Reachable+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_ChoiceFlowSubprocess(_215,_216,_217,_218){
Debug.AssertError("Calling log not passed.",(_217===undefined||_217===null));
Debug.AssertError("Simple calling log not passed.",(_218===undefined||_218===null));
var _219=this.LogSeqAudit("`1333`"+_215+", "+_216+")",_217);
this.LogSeq("`127`",_219);
var _21a=this.ChoiceFlowTreeTraversalSubprocess(_215,_216,_219,_218);
this.LogSeq("`724`",_219);
if(_21a===null){
this.LogSeq("`713`",_219);
this.LogSeqReturn(_215,_219);
return _215;
}else{
this.LogSeq("`1660`",_219);
this.LogSeq("`338`",_219);
this.LogSeqReturn(_21a,_219);
return _21a;
}
}
function Sequencer_ChoiceFlowTreeTraversalSubprocess(_21b,_21c,_21d,_21e){
Debug.AssertError("Calling log not passed.",(_21d===undefined||_21d===null));
Debug.AssertError("Simple calling log not passed.",(_21e===undefined||_21e===null));
var _21f=this.LogSeqAudit("`1046`"+_21b+", "+_21c+")",_21d);
var _220=this.Activities.GetParentActivity(_21b);
var _221=null;
var _222=null;
var _223=null;
this.LogSeq("`956`",_21f);
if(_21c==FLOW_DIRECTION_FORWARD){
this.LogSeq("`72`",_21f);
if(this.IsActivityLastOverall(_21b,_21f)||_21b.IsTheRoot()===true){
this.LogSeq("`685`",_21f);
this.LogSeqReturn("`1731`",_21f);
return null;
}
this.LogSeq("`477`",_21f);
if(_220.IsActivityTheLastAvailableChild(_21b)){
this.LogSeq("`139`",_21f);
_221=this.ChoiceFlowTreeTraversalSubprocess(_220,FLOW_DIRECTION_FORWARD,_21f,_21e);
this.LogSeq("`144`",_21f);
this.LogSeqReturn(_221,_21f);
return _221;
}else{
this.LogSeq("`1598`",_21f);
this.LogSeq("`299`",_21f);
_222=_220.GetNextActivity(_21b);
this.LogSeq("`448`",_21f);
this.LogSeqReturn(_222,_21f);
return _222;
}
}
this.LogSeq("`941`",_21f);
if(_21c==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`451`",_21f);
if(_21b.IsTheRoot()){
this.LogSeq("`680`",_21f);
this.LogSeqReturn("`1731`",_21f);
return null;
}
this.LogSeq("`472`",_21f);
if(_220.IsActivityTheFirstAvailableChild(_21b)){
this.LogSeq("`135`",_21f);
_221=this.ChoiceFlowTreeTraversalSubprocess(_220,FLOW_DIRECTION_BACKWARD,_21f,_21e);
this.LogSeq("`137`",_21f);
this.LogSeqReturn(_221,_21f);
return _221;
}else{
this.LogSeq("`1606`",_21f);
this.LogSeq("`265`",_21f);
_223=_220.GetPreviousActivity(_21b);
this.LogSeq("`441`",_21f);
this.LogSeqReturn(_223,_21f);
return _223;
}
}
}
function Sequencer_ChoiceSequencingRequestProcess(_224,_225,_226){
Debug.AssertError("Calling log not passed.",(_225===undefined||_225===null));
Debug.AssertError("Simple calling log not passed.",(_226===undefined||_226===null));
var _227=this.LogSeqAudit("`1156`"+_224+")",_225);
var _228=null;
var _229;
var _22a=null;
var _22b=null;
var _22c=null;
var _22d=null;
var _22e;
var i;
var _230;
this.LogSeq("`498`"+_224.LearningObject.ItemIdentifier,_227);
if(_224===null){
this.LogSeq("`442`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-1",IntegrationImplementation.GetString("Your selection is not permitted.  Please select an available menu item to continue."),false);
this.LogSeqReturn(_230,_227);
return _230;
}
this.LogSeq("`331`",_227);
_229=this.GetActivityPath(_224,true);
this.LogSeq("`1022`",_227);
for(i=(_229.length-1);i>=0;i--){
this.LogSeq("`787`",_227);
if(_229[i].IsTheRoot()==false){
this.LogSeq("`263`",_227);
if(_229[i].IsAvailable===false){
this.LogSeq("`394`",_227);
this.LogSeqSimple("\""+_229[i]+"\" is not currently available (it was not selected during the selection and randomization process). It and all of its descendents are not available targets for choice navigation.",_226);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-2","The activity "+_224.GetTitle()+" should not be available and is not a valid selection",true);
this.LogSeqReturn(_230,_227);
return _230;
}
}
this.LogSeq("`250`",_227);
_22a=this.SequencingRulesCheckProcess(_229[i],RULE_SET_HIDE_FROM_CHOICE,_227,_226);
this.LogSeq("`741`",_227);
if(_22a!==null){
this.LogSeq("`222`",_227);
this.LogSeqSimple("Sequencing rules indicate that \""+_229[i]+"\" should currently be hidden. It and all of its descendents are not valid targets for choice navigation.",_226);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-3","The activity "+_224.GetTitle()+" should be hidden and is not a valid selection",true);
this.LogSeqReturn(_230,_227);
return _230;
}
}
this.LogSeq("`752`",_227);
if(!_224.IsTheRoot()){
this.LogSeq("`219`",_227);
_228=this.Activities.GetParentActivity(_224);
if(_228.GetSequencingControlChoice()===false){
this.LogSeq("`419`",_227);
this.LogSeqSimple("\""+_228+"\" does not allow its children to be selected using choice navigation.",_226);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-4",IntegrationImplementation.GetString("The activity '{0}' should be hidden and is not a valid selection.",_228.GetTitle()),false);
this.LogSeqReturn(_230,_227);
return _230;
}
}
this.LogSeq("`578`",_227);
if(this.IsCurrentActivityDefined(_227)){
this.LogSeq("`631`",_227);
_22c=this.GetCurrentActivity();
_22b=this.FindCommonAncestor(_22c,_224,_227);
}else{
this.LogSeq("`1678`",_227);
this.LogSeq("`371`",_227);
_22b=this.GetRootActivity(_227);
}
if(_22c!==null&&_22c.LearningObject.ItemIdentifier==_224.LearningObject.ItemIdentifier){
this.LogSeq("`497`",_227);
this.LogSeq("`933`",_227);
}else{
if(this.AreActivitiesSiblings(_22c,_224,_227)){
this.LogSeq("`361`",_227);
this.LogSeq("`10`",_227);
var _231=_228.GetActivityListBetweenChildren(_22c,_224,false);
this.LogSeq("`831`",_227);
if(_231.length===0){
this.LogSeq("`425`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to open"),false);
this.LogSeqReturn(_230,_227);
return _230;
}
this.LogSeq("`446`",_227);
if(_224.Ordinal>_22c.Ordinal){
this.LogSeq("`1331`",_227);
_22d=FLOW_DIRECTION_FORWARD;
}else{
this.LogSeq("`1652`",_227);
this.LogSeq("`1307`",_227);
_22d=FLOW_DIRECTION_BACKWARD;
_231.reverse();
}
this.LogSeq("`998`",_227);
for(i=0;i<_231.length;i++){
this.LogSeq("`513`",_227);
_22e=this.ChoiceActivityTraversalSubprocess(_231[i],_22d,_227,_226);
this.LogSeq("`709`",_227);
if(_22e.Reachable===false){
this.LogSeq("`140`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,_22e.Exception,"",false);
this.LogSeqReturn(_230,_227);
return _230;
}
}
this.LogSeq("`1432`",_227);
}else{
if(_22c===null||_22c.LearningObject.ItemIdentifier==_22b.LearningObject.ItemIdentifier){
this.LogSeq("`212`",_227);
this.LogSeq("`249`",_227);
_229=this.GetPathToAncestorInclusive(_224,_22b,false);
this.LogSeq("`1073`",_227);
if(_229.length===0){
this.LogSeq("`418`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to open"),false);
this.LogSeqReturn(_230,_227);
return _230;
}
this.LogSeq("`1009`",_227);
for(i=_229.length-1;i>=0;i--){
this.LogSeq("`526`",_227);
_22e=this.ChoiceActivityTraversalSubprocess(_229[i],FLOW_DIRECTION_FORWARD,_227,_226);
this.LogSeq("`707`",_227);
if(_22e.Reachable===false){
this.LogSeq("`141`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,_22e.Exception,_22e.ExceptionText,false);
this.LogSeqReturn(_230,_227);
return _230;
}
this.LogSeq("`19`",_227);
if(_229[i].IsActive()===false&&_229[i].LearningObject.ItemIdentifier!=_22b.LearningObject.ItemIdentifier&&_229[i].GetPreventActivation()===true){
this.LogSeqSimple("\""+_229[i]+"\" cannot be entered as a result of a choice request because of its prevent activation attribute. This choice request is not allowed.",_226);
this.LogSeq("`601`"+PREVENT_ACTIVATION_ERROR+"`1540`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,PREVENT_ACTIVATION_ERROR,IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_229[i].GetTitle()),false);
this.LogSeqReturn(_230,_227);
return _230;
}
}
this.LogSeq("`1422`",_227);
}else{
if(_224.LearningObject.ItemIdentifier==_22b.LearningObject.ItemIdentifier){
this.LogSeq("`286`",_227);
this.LogSeq("`346`",_227);
_229=this.GetPathToAncestorInclusive(_22c,_22b);
this.LogSeq("`1058`",_227);
if(_229.length===0){
this.LogSeq("`411`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to deliver"),false);
this.LogSeqReturn(_230,_227);
return _230;
}
this.LogSeq("`984`",_227);
for(i=0;i<_229.length;i++){
this.LogSeq("`668`",_227);
if(i!=(_229.length-1)){
this.LogSeq("`195`",_227);
if(_229[i].GetSequencingControlChoiceExit()===false){
this.LogSeqSimple("\""+_229[i]+"\" cannot be exited as a result of a choice request because of its control choice exit attribute. This choice request is not allowed.",_226);
this.LogSeq("`571`"+CONTROL_CHOICE_EXIT_ERROR_CHOICE+"`1540`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,CONTROL_CHOICE_EXIT_ERROR_CHOICE,IntegrationImplementation.GetString("Your selection is not permitted.  Please select 'Next' or 'Previous' to move through '{0}'.",_229[i]),false);
this.LogSeqReturn(_230,_227);
return _230;
}
}
}
this.LogSeq("`1408`",_227);
}else{
this.LogSeq("`287`",_227);
this.LogSeq("`253`",_227);
_229=this.GetPathToAncestorExclusive(_22c,_22b,false);
this.LogSeq("`1004`",_227);
var _232=null;
this.LogSeq("`572`",_227);
for(i=0;i<_229.length;i++){
this.LogSeq("`202`",_227);
if(_229[i].GetSequencingControlChoiceExit()===false){
this.LogSeqSimple("\""+_229[i]+"\" cannot be exited as a result of a choice request because of its control choice exit attribute. This choice request is not allowed.",_226);
this.LogSeq("`595`"+CONTROL_CHOICE_EXIT_ERROR_CHOICE+"`1540`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,CONTROL_CHOICE_EXIT_ERROR_CHOICE,IntegrationImplementation.GetString("You are not allowed to jump out of {0}.",_229[i].GetTitle()),false);
this.LogSeqReturn(_230,_227);
return _230;
}
this.LogSeq("`399`",_227);
if(_232===null){
this.LogSeq("`742`",_227);
if(_229[i].GetConstrainedChoice()===true){
this.LogSeq("`918`"+_229[i]+"'",_227);
_232=_229[i];
}
}
}
this.LogSeq("`977`",_227);
if(_232!==null){
this.LogSeq("`467`",_227);
if(this.IsActivity1BeforeActivity2(_232,_224,_227)){
this.LogSeq("`528`",_227);
_22d=FLOW_DIRECTION_FORWARD;
}else{
this.LogSeq("`1579`",_227);
this.LogSeq("`515`",_227);
_22d=FLOW_DIRECTION_BACKWARD;
}
this.LogSeq("`523`",_227);
var _233=this.ChoiceFlowSubprocess(_232,_22d,_227,_226);
this.LogSeq("`558`",_227);
var _234=_233;
this.LogSeq("`7`",_227);
if((!_234.IsActivityAnAvailableDescendent(_224))&&(_224!=_232&&_224!=_234)){
this.LogSeqSimple("\""+_232+"\" has a constrained choice sequencing control so only activities which are logically next or previous are allowed to be targets of choice navigation. This choice request is not allowed.",_226);
if(_22d==FLOW_DIRECTION_FORWARD){
this.LogSeqSimple("The logically next activity is \""+_234+"\".",_226);
}else{
this.LogSeqSimple("The logically previous activity is \""+_234+"\".",_226);
}
this.LogSeq("`593`"+CONSTRAINED_CHOICE_ERROR+")",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,CONSTRAINED_CHOICE_ERROR,IntegrationImplementation.GetString("You are not allowed to jump out of {0}.",_232.GetTitle()),false);
this.LogSeqReturn(_230,_227);
return _230;
}
}
this.LogSeq("`242`",_227);
_229=this.GetPathToAncestorInclusive(_224,_22b);
this.LogSeq("`1064`",_227);
if(_229.length===0){
this.LogSeq("`406`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to open"),false);
this.LogSeqReturn(_230,_227);
return _230;
}
this.LogSeq("`305`",_227);
if(this.IsActivity1BeforeActivity2(_22c,_224,_227)){
this.LogSeq("`967`",_227);
for(i=(_229.length-1);i>=0;i--){
if(i>0){
this.LogSeq("`235`"+i,_227);
_22e=this.ChoiceActivityTraversalSubprocess(_229[i],FLOW_DIRECTION_FORWARD,_227,_226);
this.LogSeq("`683`",_227);
if(_22e.Reachable===false){
this.LogSeq("`133`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,_22e.Exception,_22e.ExceptionText,false);
this.LogSeqReturn(_230,_227);
return _230;
}
}
this.LogSeq("`13`",_227);
if((_229[i].IsActive()===false)&&(_229[i]!=_22b)&&(_229[i].GetPreventActivation()===true)){
this.LogSeqSimple("\""+_229[i]+"\" cannot be entered as a result of a choice request because of its prevent activation attribute. This choice request is not allowed.",_226);
this.LogSeq("`580`"+PREVENT_ACTIVATION_ERROR+"`1540`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,PREVENT_ACTIVATION_ERROR,IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_229[i].GetTitle()),false);
this.LogSeqReturn(_230,_227);
return _230;
}
}
}else{
this.LogSeq("`1621`",_227);
this.LogSeq("`965`",_227);
for(i=(_229.length-1);i>=0;i--){
this.LogSeq("`15`",_227);
if((_229[i].IsActive()===false)&&(_229[i]!=_22b)&&(_229[i].GetPreventActivation()===true)){
this.LogSeqSimple("\""+_229[i]+"\" cannot be entered as a result of a choice request because of its prevent activation attribute. This choice request is not allowed.",_226);
this.LogSeq("`573`"+PREVENT_ACTIVATION_ERROR+"`1540`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,PREVENT_ACTIVATION_ERROR,IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_229[i].GetTitle()),false);
this.LogSeqReturn(_230,_227);
return _230;
}
}
}
this.LogSeq("`1384`",_227);
}
}
}
}
this.LogSeq("`907`",_227);
if(_224.IsALeaf()===true){
this.LogSeq("`490`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(_224,null,"",false);
this.LogSeqReturn(_230,_227);
return _230;
}
this.LogSeq("`51`",_227);
var _235=this.FlowSubprocess(_224,FLOW_DIRECTION_FORWARD,true,_227,_226);
this.LogSeq("`252`",_227);
if(_235.Deliverable===false){
this.LogSeqSimple("A choice request for the \""+_224+"\" cluster did not flow to a deliverable SCO. Setting it to be the curent activity and prompting the user to make another selection.",_226);
if(this.LookAhead===false){
this.LogSeq("`647`",_227);
this.TerminateDescendentAttemptsProcess(_22b,_227,_226);
this.LogSeq("`835`",_227);
this.EndAttemptProcess(_22b,false,_227,false,_226);
this.LogSeq("`880`",_227);
this.SetCurrentActivity(_224,_227,_226);
}
this.LogSeq("`440`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-9",IntegrationImplementation.GetString("Please select another item from the menu."),false);
this.LogSeqReturn(_230,_227);
return _230;
}else{
this.LogSeq("`1674`",_227);
this.LogSeq("`312`",_227);
_230=new Sequencer_ChoiceSequencingRequestProcessResult(_235.IdentifiedActivity,null,"",false);
this.LogSeqReturn(_230,_227);
return _230;
}
}
function Sequencer_ChoiceSequencingRequestProcessResult(_236,_237,_238,_239){
if(_239===undefined){
Debug.AssertError("no value passed for hidden");
}
this.DeliveryRequest=_236;
this.Exception=_237;
this.ExceptionText=_238;
this.Hidden=_239;
}
Sequencer_ChoiceSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", Hidden="+this.Hidden;
};
function Sequencer_ClearSuspendedActivitySubprocess(_23a,_23b){
Debug.AssertError("Calling log not passed.",(_23b===undefined||_23b===null));
var _23c=this.LogSeqAudit("`1107`"+_23a+")",_23b);
var _23d=null;
var _23e=null;
var _23f=null;
this.LogSeq("`605`",_23c);
if(this.IsSuspendedActivityDefined(_23c)){
this.LogSeq("`602`",_23c);
_23e=this.GetSuspendedActivity(_23c);
_23d=this.FindCommonAncestor(_23a,_23e,_23c);
this.LogSeq("`342`",_23c);
_23f=this.GetPathToAncestorInclusive(_23e,_23d);
this.LogSeq("`1007`",_23c);
if(_23f.length>0){
this.LogSeq("`333`",_23c);
for(var i=0;i<_23f.length;i++){
this.LogSeq("`1091`",_23c);
if(_23f[i].IsALeaf()){
this.LogSeq("`792`",_23c);
_23f[i].SetSuspended(false);
}else{
this.LogSeq("`1556`",_23c);
this.LogSeq("`403`",_23c);
if(_23f[i].HasSuspendedChildren()===false){
this.LogSeq("`770`",_23c);
_23f[i].SetSuspended(false);
}
}
}
}
this.LogSeq("`606`",_23c);
this.ClearSuspendedActivity(_23c);
}
this.LogSeq("`1011`",_23c);
this.LogSeqReturn("",_23c);
return;
}
function Sequencer_CompletionMeasureRollupProcess(_241,_242,_243){
Debug.AssertError("Calling log not passed.",(_242===undefined||_242===null));
Debug.AssertError("Simple calling log not passed.",(_243===undefined||_243===null));
var _244=this.LogSeqAudit("`1121`"+_241+")",_242);
_243=this.LogSeqSimpleAudit("Rolling up the progress measure for \""+_241+"\".",_243);
this.LogSeq("`929`",_244);
var _245=0;
this.LogSeq("`1296`",_244);
var _246=false;
this.LogSeq("`997`",_244);
var _247=0;
this.LogSeq("`1160`",_244);
var _248=_241.GetChildren();
for(var i=0;i<_248.length;i++){
this.LogSeq("`634`",_244);
if(_248[i].IsTracked()===true){
this.LogSeq("`259`",_244);
var _24a=_248[i].GetCompletionProgressWeight();
_247+=_24a;
this.LogSeq("`808`",_244);
if(_248[i].GetAttemptCompletionAmountStatus()===true){
this.LogSeq("`92`",_244);
_245+=(_248[i].GetAttemptCompletionAmount()*_24a);
this.LogSeqSimple("\""+_248[i]+"\" has a progress measure of "+_248[i].GetAttemptCompletionAmount()+" and a weight of "+_24a+" so it contributes a weighted measure of "+(_248[i].GetAttemptCompletionAmount()*_24a)+".",_243);
this.LogSeq("`1209`",_244);
_246=true;
}
}else{
this.LogSeqSimple("\""+_248[i]+"\" is not tracked and will not contribute to progress measure rollup.",_243);
}
}
this.LogSeq("`1236`",_244);
if(!_246){
this.LogSeq("`321`",_244);
this.LogSeqSimple("No children had a progress measure to rollup, setting the completion amount status to unknown.",_243);
this.LogSeqSimpleReturn("unknown",_243);
_241.SetAttemptCompletionAmountStatus(false);
}else{
this.LogSeq("`1613`",_244);
this.LogSeq("`524`",_244);
if(_247>0){
this.LogSeq("`825`",_244);
_241.SetAttemptCompletionAmountStatus(true);
this.LogSeq("`475`",_244);
var _24b=(_245/_247);
_24b=RoundToPrecision(_24b,7);
_241.SetAttemptCompletionAmount(_24b);
this.LogSeqSimple("Setting the attempt completion amount to the new weighted value of "+_24b+".",_243);
this.LogSeqSimpleReturn(_24b,_243);
}else{
this.LogSeq("`1554`",_244);
this.LogSeq("`364`",_244);
_241.SetAttemptCompletionAmountStatus(false);
}
}
this.LogSeq("`1002`",_244);
this.LogSeqReturn("",_244);
return;
}
function Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess(_24c,_24d,_24e){
if(_24d===undefined||_24d===null){
_24d=this.LogSeqAudit("Content Delivery Environment Activity Data SubProcess for "+activity.StringIdentifier);
}
if(_24e===undefined||_24e===null){
_24e=this.LogSeqSimpleAudit("Preparing data for delivery of \""+_24c+"\".");
}
var _24f=(Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK||Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR);
if(_24f){
var _250=this.PreviousActivity;
}else{
var _250=this.CurrentActivity;
}
var _251=this.GetSuspendedActivity(_24d);
var _252=this.GetRootActivity(_24d);
var _253=null;
var _254=false;
this.LogSeq("`210`",_24d);
if(_251!=_24c){
this.LogSeqSimple("Clearing out suspended activities.",_24e);
this.LogSeq("`555`",_24d);
this.ClearSuspendedActivitySubprocess(_24c,_24d);
}
this.LogSeq("`231`",_24d);
this.TerminateDescendentAttemptsProcess(_24c,_24d,_24e);
this.LogSeq("`67`",_24d);
_253=this.GetPathToAncestorInclusive(_24c,_252);
this.LogSeqSimple("Setting activities along the current activity path to active and incrementing their attempt count if the activity was not previously active.",_24e);
this.LogSeq("`1080`",_24d);
var _255=ConvertDateToIso8601String(new Date());
for(var i=(_253.length-1);i>=0;i--){
this.LogSeq("`1502`"+_253[i]+"`1194`",_24d);
if(_24f){
var _257=_253[i].WasActiveBeforeLaunchOnClick;
}else{
var _257=_253[i].IsActive();
}
if(_257===false){
this.LogSeq("`979`",_24d);
if(_253[i].IsTracked()){
this.LogSeq("`116`",_24d);
if(_253[i].IsSuspended()){
this.LogSeq("`810`",_24d);
_253[i].SetSuspended(false);
}else{
this.LogSeq("`1611`",_24d);
this.LogSeq("`487`",_24d);
_253[i].IncrementAttemptCount();
this.LogSeq("`358`",_24d);
if(_253[i].GetAttemptCount()==1){
this.LogSeq("`773`",_24d);
_253[i].SetActivityProgressStatus(true);
_254=true;
}
this.LogSeqSimple("Started a new attempt with fresh sequencing data for \""+_253[i]+"\" (attempt number "+_253[i].GetAttemptCount()+") .",_24e);
this.LogSeq("`164`",_24d);
_253[i].InitializeForNewAttempt(true,true);
var atts={ev:"AttemptStart",an:_253[i].GetAttemptCount(),ai:_253[i].ItemIdentifier,at:_253[i].LearningObject.Title};
this.WriteHistoryLog("",atts);
_253[i].SetAttemptStartTimestampUtc(_255);
_253[i].SetAttemptAbsoluteDuration("PT0H0M0S");
_253[i].SetAttemptExperiencedDurationTracked("PT0H0M0S");
_253[i].SetAttemptExperiencedDurationReported("PT0H0M0S");
if(Control.Package.Properties.ResetRunTimeDataTiming==RESET_RT_DATA_TIMING_ON_EACH_NEW_SEQUENCING_ATTEMPT){
if(_253[i].IsDeliverable()===true){
if(_254===false){
this.LogSeqSimple("Resetting the CMI runtime data for \""+_253[i]+"\".",_24e);
var atts={ev:"ResetRuntime",ai:_253[i].ItemIdentifier,at:_253[i].LearningObject.Title};
this.WriteHistoryLog("",atts);
}
_253[i].RunTime.ResetState();
}
}
this.LogSeq("`802`"+Control.Package.ObjectivesGlobalToSystem+"`940`",_24d);
if(Control.Package.ObjectivesGlobalToSystem===false&&_253[i].IsTheRoot()===true){
this.LogSeq("`543`",_24d);
this.LogSeqSimple("A new attempt on the root activity is starting, so reset all global objectives (since objectives global to system is false).",_24e);
this.ResetGlobalObjectives();
}
this.LogSeq("`700`"+Control.Package.SharedDataGlobalToSystem+"`940`",_24d);
if(Control.Package.SharedDataGlobalToSystem===false&&_253[i].IsTheRoot()===true){
this.LogSeq("`675`",_24d);
this.LogSeqSimple("A new attempt on the root activity is starting, so reset all shared data (since shared data global to system is false).",_24e);
this.ResetSharedData();
}
}
}
_253[i].SetAttemptedDuringThisAttempt();
}
}
}
function Sequencer_ContentDeliveryEnvironmentProcess(_259,_25a,_25b){
Debug.AssertError("Calling log not passed.",(_25a===undefined||_25a===null));
Debug.AssertError("Simple calling log not passed.",(_25b===undefined||_25b===null));
var _25c=this.LogSeqAudit("`1145`"+_259+")",_25a);
var _25d=(Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK||Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR);
var _25e=this.GetCurrentActivity();
var _25f=this.GetSuspendedActivity(_25c);
var _260=this.GetRootActivity(_25c);
var _261=null;
var _262;
this.LogSeq("`200`",_25c);
if(_25e!==null&&_25e.IsActive()){
this.LogSeq("`266`",_25c);
_262=new Sequencer_ContentDeliveryEnvironmentProcessResult(false,"DB.2.1",IntegrationImplementation.GetString("The previous activity must be terminated before a new activity may be attempted"));
this.LogSeqReturn(_262,_25c);
return _262;
}
if(!_25d){
this.ContentDeliveryEnvironmentActivityDataSubProcess(_259,_25a);
}else{
this.LogSeq("`228`",_25c);
}
this.LogSeq("`67`",_25c);
_261=this.GetPathToAncestorInclusive(_259,_260);
this.LogSeq("`1080`",_25c);
for(var i=(_261.length-1);i>=0;i--){
if(_25d){
_261[i].WasActiveBeforeLaunchOnClick=_261[i].IsActive();
}
this.LogSeq("`1502`"+_261[i]+"`1194`",_25c);
if(_261[i].IsActive()===false){
this.LogSeq("`892`",_25c);
_261[i].SetActive(true);
}
}
this.LogSeq("`234`"+_259.GetItemIdentifier(),_25c);
if(_25d){
this.PreviousActivity=_25e;
}
this.SetCurrentActivity(_259,_25c,_25b);
this.LogSeq("`1136`",_25c);
this.SetSuspendedActivity(null);
this.LogSeq("`830`",_25c);
_262=new Sequencer_ContentDeliveryEnvironmentProcessResult(true,null,"");
this.LogSeqReturn(_262,_25c);
this.LogSeqSimple("Delivering activity \""+_259+"\".",_25b);
Control.DeliverActivity(_259);
return _262;
}
function Sequencer_ContentDeliveryEnvironmentProcessResult(_264,_265,_266){
this.Valid=_264;
this.Exception=_265;
this.ExceptionText=_266;
}
Sequencer_ContentDeliveryEnvironmentProcessResult.prototype.toString=function(){
return "Valid="+this.Valid+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_ContinueSequencingRequestProcess(_267,_268){
Debug.AssertError("Calling log not passed.",(_267===undefined||_267===null));
Debug.AssertError("Simple calling log not passed.",(_268===undefined||_268===null));
var _269=this.LogSeqAudit("`1133`",_267);
var _26a;
this.LogSeq("`504`",_269);
if(!this.IsCurrentActivityDefined(_269)){
this.LogSeq("`427`",_269);
_26a=new Sequencer_ContinueSequencingRequestProcessResult(null,"SB.2.7-1",IntegrationInterface.GetString("The sequencing session has not begun yet."),false);
this.LogSeqReturn(_26a,_269);
return _26a;
}
var _26b=this.GetCurrentActivity();
this.LogSeq("`712`",_269);
if(!_26b.IsTheRoot()){
var _26c=this.Activities.GetParentActivity(_26b);
this.LogSeq("`297`",_269);
if(_26c.GetSequencingControlFlow()===false){
this.LogSeq("`566`",_269);
this.LogSeqSimple("Flow navigation is not allowed within \""+_26c+"\". Prompting the used to make a different selection.",_268);
_26a=new Sequencer_ContinueSequencingRequestProcessResult(null,"SB.2.7-2",IntegrationImplementation.GetString("You cannot use 'Next' to enter {0}. Please select a menu item to continue.",_26c.GetTitle()),false);
this.LogSeqReturn(_26a,_269);
return _26a;
}
}
this.LogSeq("`136`",_269);
var _26d=this.FlowSubprocess(_26b,FLOW_DIRECTION_FORWARD,false,_269,_268);
this.LogSeq("`986`",_269);
if(_26d.Deliverable===false){
this.LogSeq("`64`",_269);
_26a=new Sequencer_ContinueSequencingRequestProcessResult(null,_26d.Exception,_26d.ExceptionText,_26d.EndSequencingSession);
this.LogSeqReturn(_26a,_269);
return _26a;
}else{
this.LogSeq("`1679`",_269);
this.LogSeq("`318`",_269);
_26a=new Sequencer_ContinueSequencingRequestProcessResult(_26d.IdentifiedActivity,null,"",false);
this.LogSeqReturn(_26a,_269);
return _26a;
}
}
function Sequencer_ContinueSequencingRequestProcessResult(_26e,_26f,_270,_271){
Debug.AssertError("Invalid endSequencingSession ("+_271+") passed to ContinueSequencingRequestProcessResult.",(_271!=true&&_271!=false));
this.DeliveryRequest=_26e;
this.Exception=_26f;
this.ExceptionText=_270;
this.EndSequencingSession=_271;
}
Sequencer_ContinueSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_DeliveryRequestProcess(_272,_273,_274){
Debug.AssertError("Calling log not passed.",(_273===undefined||_273===null));
Debug.AssertError("Simple calling log not passed.",(_274===undefined||_274===null));
var _275=this.LogSeqAudit("`1327`"+_272+")",_273);
var _276;
var _277;
this.LogSeq("`874`"+_272+"`954`",_275);
if(!_272.IsALeaf()){
this.LogSeq("`589`",_275);
_277=new Sequencer_DeliveryRequestProcessResult(false,"DB.1.1-1",IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_272.GetTitle()));
this.LogSeqReturn(_277,_275);
return _277;
}
this.LogSeq("`208`",_275);
var _278=this.GetActivityPath(_272,true);
this.LogSeq("`839`",_275);
if(_278.length===0){
this.LogSeq("`584`",_275);
_277=new Sequencer_DeliveryRequestProcessResult(false,"DB.1.1-2",IntegrationImplementation.GetString("Nothing to open"));
this.LogSeqReturn(_277,_275);
return _277;
}
this.LogSeq("`527`",_275);
for(var i=0;i<_278.length;i++){
this.LogSeq("`862`"+_278[i],_275);
_276=this.CheckActivityProcess(_278[i],_275,_274);
this.LogSeq("`904`",_275);
if(_276===true){
this.LogSeq("`568`",_275);
_277=new Sequencer_DeliveryRequestProcessResult(false,"DB.1.1-3",IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_272.GetTitle()));
this.LogSeqReturn(_277,_275);
return _277;
}
}
this.LogSeq("`667`",_275);
_277=new Sequencer_DeliveryRequestProcessResult(true,null,"");
this.LogSeqReturn(_277,_275);
return _277;
}
function Sequencer_DeliveryRequestProcessResult(_27a,_27b,_27c){
this.Valid=_27a;
this.Exception=_27b;
this.ExceptionText=_27c;
}
Sequencer_DeliveryRequestProcessResult.prototype.toString=function(){
return "Valid="+this.Valid+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_EndAttemptProcess(_27d,_27e,_27f,_280,_281){
Debug.AssertError("Calling log not passed.",(_27f===undefined||_27f===null));
Debug.AssertError("Simple calling log not passed.",(_281===undefined||_281===null));
if(_280===undefined||_280===null){
_280=false;
}
var _282=this.LogSeqAudit("`1440`"+_27d+", "+_27e+")",_27f);
_281=this.LogSeqSimpleAudit("Ending the current attempt on \""+_27d+"\".",_281);
this.LogSeq("`1475`"+_27d.GetItemIdentifier()+"`1671`",_282);
var i;
var _284=new Array();
if(_27d.IsALeaf()){
this.LogSeq("`1000`",_282);
if(_27d.IsTracked()&&_27d.WasLaunchedThisSession()){
this.LogSeq("`861`",_282);
this.LogSeqSimple("Transferring run-time data to the activity tree.",_281);
_27d.TransferRteDataToActivity();
this.LogSeq("`309`",_282);
if(_27d.IsSuspended()===false){
this.LogSeq("`282`",_282);
if(_27d.IsCompletionSetByContent()===false){
this.LogSeq("`77`",_282);
if(_27d.GetAttemptProgressStatus()===false&&_27d.GetCompletionStatusChangedDuringRuntime()===false){
this.LogSeqSimple("The activity did not set completion status and indicates that it will not so automatically marking the activity as completed.",_281);
this.LogSeq("`737`",_282);
_27d.SetAttemptProgressStatus(true);
this.LogSeq("`711`",_282);
_27d.SetAttemptCompletionStatus(true);
_27d.WasAutoCompleted=true;
}
}
this.LogSeq("`295`",_282);
if(_27d.IsObjectiveSetByContent()===false){
this.LogSeq("`592`",_282);
var _285=_27d.GetPrimaryObjective();
this.LogSeq("`63`",_282);
if(_285.GetProgressStatus(_27d,false)===false&&_27d.GetSuccessStatusChangedDuringRuntime()===false&&_285.GetSuccessStatusChangedDuringRuntime(_27d)===false){
this.LogSeqSimple("The activity did not set satisfaction status and indicates that it will not so automatically marking the activity as satisfied.",_281);
this.LogSeq("`662`",_282);
_285.SetProgressStatus(true,false,_27d,true,true);
this.LogSeq("`654`",_282);
_285.SetSatisfiedStatus(true,false,_27d);
_27d.WasAutoSatisfied=true;
}
}
}
}
}else{
this.LogSeq("`1203`",_282);
this.LogSeq("`107`",_282);
if(this.ActivityHasSuspendedChildren(_27d,_282)){
this.LogSeq("`822`",_282);
_27d.SetSuspended(true);
this.LogSeqSimple("\""+_27d+"\" has suspend children and will be marked as suspended.",_281);
}else{
this.LogSeq("`1690`",_282);
this.LogSeq("`812`",_282);
_27d.SetSuspended(false);
this.LogSeqSimple("\""+_27d+"\" does not have suspend children and will not be marked as suspended.",_281);
}
}
if(_280===false){
this.LogSeq("`460`",_282);
_27d.SetActive(false);
}
var _286;
if(_27e===false){
this.LogSeq("`247`",_282);
_286=this.OverallRollupProcess(_27d,_282,_281);
}else{
this.LogSeq("`519`",_282);
_284[0]=_27d;
}
this.LogSeq("`230`",_282);
var _287=this.FindActivitiesAffectedByWriteMaps(_27d);
var _288=this.FindDistinctParentsOfActivitySet(_287);
if(_27e===false){
this.LogSeq("`453`",_282);
var _289=this.GetMinimalSubsetOfActivitiesToRollup(_288,_286);
if(_289.length>0){
this.LogSeqSimple("Invoking rollup for activities that are affected by write maps.",_281);
}
for(i=0;i<_289.length;i++){
if(_289[i]!==null){
this.OverallRollupProcess(_289[i],_282,_281);
}
}
}else{
this.LogSeq("`218`",_282);
_284=_284.concat(_288);
}
if(this.LookAhead===false&&_280===false){
this.RandomizeChildrenProcess(_27d,true,_282,_281);
}
this.LogSeq("`1291`",_282);
this.LogSeqReturn("",_282);
return _284;
}
function Sequencer_EvaluateRollupConditionsSubprocess(_28a,_28b,_28c,_28d){
Debug.AssertError("Calling log not passed.",(_28c===undefined||_28c===null));
Debug.AssertError("Simple calling log not passed.",(_28d===undefined||_28d===null));
var _28e=this.LogSeqAudit("`1045`"+_28a+", "+_28b+")",_28c);
var _28f;
var _290;
this.LogSeq("`388`",_28e);
var _291=new Array();
var i;
this.LogSeq("`790`",_28e);
for(i=0;i<_28b.Conditions.length;i++){
this.LogSeq("`37`",_28e);
_28f=this.EvaluateRollupRuleCondition(_28a,_28b.Conditions[i],_28d);
this.LogSeq("`367`",_28e);
if(_28b.Conditions[i].Operator==RULE_CONDITION_OPERATOR_NOT&&_28f!=RESULT_UNKNOWN){
this.LogSeq("`1150`",_28e);
_28f=(!_28f);
}
this.LogSeq("`966`"+_28f+"`536`",_28e);
_291[_291.length]=_28f;
}
this.LogSeq("`311`",_28e);
if(_291.length===0){
this.LogSeq("`690`",_28e);
return RESULT_UNKNOWN;
}
this.LogSeq("`1099`"+_28b.ConditionCombination+"`291`",_28e);
if(_28b.ConditionCombination==RULE_CONDITION_COMBINATION_ANY){
_290=false;
for(i=0;i<_291.length;i++){
_290=Sequencer_LogicalOR(_290,_291[i]);
}
if(_28b.Conditions.length>1){
this.LogSeqSimple("Are ANY of the conditions true="+_290,_28d);
}
}else{
_290=true;
for(i=0;i<_291.length;i++){
_290=Sequencer_LogicalAND(_290,_291[i]);
}
if(_28b.Conditions.length>1){
this.LogSeqSimple("Are ALL of the conditions true="+_290,_28d);
}
}
this.LogSeq("`495`",_28e);
this.LogSeqReturn(_290,_28e);
return _290;
}
function Sequencer_EvaluateRollupRuleCondition(_293,_294,_295){
var _296=null;
switch(_294.Condition){
case ROLLUP_RULE_CONDITION_SATISFIED:
_296=_293.IsSatisfied("");
this.LogSeqSimple("\""+_293+"\" satisfied = "+_296,_295);
break;
case ROLLUP_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN:
_296=_293.IsObjectiveStatusKnown("",false);
this.LogSeqSimple("\""+_293+"\" objective status known = "+_296,_295);
break;
case ROLLUP_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN:
_296=_293.IsObjectiveMeasureKnown("",false);
this.LogSeqSimple("\""+_293+"\" objective measure known = "+_296,_295);
break;
case ROLLUP_RULE_CONDITION_COMPLETED:
_296=_293.IsCompleted("",false);
this.LogSeqSimple("\""+_293+"\" completed = "+_296,_295);
break;
case ROLLUP_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN:
_296=_293.IsActivityProgressKnown("",false);
this.LogSeqSimple("\""+_293+"\" activity progress known = "+_296,_295);
break;
case ROLLUP_RULE_CONDITION_ATTEMPTED:
_296=_293.IsAttempted();
this.LogSeqSimple("\""+_293+"\" attempted = "+_296,_295);
break;
case ROLLUP_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED:
_296=_293.IsAttemptLimitExceeded();
this.LogSeqSimple("\""+_293+"\" attempt limit exceeded = "+_296,_295);
break;
case ROLLUP_RULE_CONDITION_NEVER:
_296=false;
this.LogSeqSimple("\""+_293+"\": 'never' condition always equals false",_295);
break;
default:
this.LogSeq("`1370`",logParent);
break;
}
return _296;
}
function Sequencer_ExitSequencingRequestProcess(_297,_298){
Debug.AssertError("Calling log not passed.",(_297===undefined||_297===null));
Debug.AssertError("Simple calling log not passed.",(_298===undefined||_298===null));
var _299=this.LogSeqAudit("`1214`",_297);
var _29a;
this.LogSeq("`486`",_299);
if(!this.IsCurrentActivityDefined(_299)){
this.LogSeq("`511`",_299);
_29a=new Sequencer_ExitSequencingRequestProcessResult(false,"SB.2.11-1",IntegrationImplementation.GetString("An 'Exit Sequencing' request cannot be processed until the sequencing session has begun."));
this.LogSeqReturn(_29a,_299);
return _29a;
}
var _29b=this.GetCurrentActivity();
this.LogSeq("`320`",_299);
if(_29b.IsActive()){
this.LogSeq("`510`",_299);
_29a=new Sequencer_ExitSequencingRequestProcessResult(false,"SB.2.11-2",IntegrationImplementation.GetString("An 'Exit Sequencing' request cannot be processed while an activity is still active."));
this.LogSeqReturn(_29a,_299);
return _29a;
}
this.LogSeq("`753`",_299);
if(_29b.IsTheRoot()||this.CourseIsSingleSco()===true){
this.LogSeq("`220`",_299);
if(_29b.IsTheRoot()){
this.LogSeqSimple("Exiting the root activity causes the entire course to exit.",_298);
}else{
this.LogSeqSimple("Exiting a SCO that is the only SCO in the course causes the entire course to exit.",_298);
}
_29a=new Sequencer_ExitSequencingRequestProcessResult(true,null,"");
this.LogSeqReturn(_29a,_299);
return _29a;
}
this.LogSeq("`560`",_299);
_29a=new Sequencer_ExitSequencingRequestProcessResult(false,null,"");
this.LogSeqReturn(_29a,_299);
return _29a;
}
function Sequencer_ExitSequencingRequestProcessResult(_29c,_29d,_29e){
Debug.AssertError("Invalid endSequencingSession ("+_29c+") passed to ExitSequencingRequestProcessResult.",(_29c!=true&&_29c!=false));
this.EndSequencingSession=_29c;
this.Exception=_29d;
this.ExceptionText=_29e;
}
Sequencer_ExitSequencingRequestProcessResult.prototype.toString=function(){
return "EndSequencingSession="+this.EndSequencingSession+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_FlowActivityTraversalSubprocess(_29f,_2a0,_2a1,_2a2,_2a3){
Debug.AssertError("Calling log not passed.",(_2a2===undefined||_2a2===null));
Debug.AssertError("Simple calling log not passed.",(_2a3===undefined||_2a3===null));
var _2a4=this.LogSeqAudit("`1141`"+_29f+", "+_2a0+", "+_2a1+")",_2a2);
var _2a5;
var _2a6;
var _2a7;
var _2a8;
var _2a9;
var _2aa=this.Activities.GetParentActivity(_29f);
this.LogSeq("`458`",_2a4);
if(_2aa.GetSequencingControlFlow()===false){
this.LogSeqSimple("\""+_2aa+"\" does not allow flow navigation, stopping and prompting the user to make another selection.",_2a3);
this.LogSeq("`384`",_2a4);
_2a9=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_29f,"SB.2.2-1",IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_2aa.GetTitle()),false);
this.LogSeqReturn(_2a9,_2a4);
return _2a9;
}
this.LogSeq("`531`",_2a4);
_2a5=this.SequencingRulesCheckProcess(_29f,RULE_SET_SKIPPED,_2a4,_2a3);
this.LogSeq("`359`",_2a4);
if(_2a5!==null){
this.LogSeqSimple("\""+_29f+"\" is skipped, considering the next activity for delivery.",_2a3);
this.LogSeq("`181`",_2a4);
_2a6=this.FlowTreeTraversalSubprocess(_29f,_2a0,_2a1,false,_2a4,_2a3);
this.LogSeq("`638`",_2a4);
if(_2a6.NextActivity===null){
this.LogSeq("`31`",_2a4);
_2a9=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_29f,_2a6.Exception,_2a6.ExceptionText,_2a6.EndSequencingSession);
this.LogSeqReturn(_2a9,_2a4);
return _2a9;
}else{
this.LogSeq("`1654`",_2a4);
this.LogSeq("`66`",_2a4);
if(_2a1==FLOW_DIRECTION_BACKWARD&&_2a6.TraversalDirection==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`35`",_2a4);
_2a7=this.FlowActivityTraversalSubprocess(_2a6.NextActivity,_2a6.TraversalDirection,null,_2a4,_2a3);
}else{
this.LogSeq("`1586`",_2a4);
this.LogSeq("`9`",_2a4);
_2a7=this.FlowActivityTraversalSubprocess(_2a6.NextActivity,_2a0,_2a1,_2a4,_2a3);
}
this.LogSeq("`233`",_2a4);
_2a9=_2a7;
this.LogSeqReturn(_2a9,_2a4);
return _2a9;
}
}
this.LogSeq("`564`",_2a4);
_2a8=this.CheckActivityProcess(_29f,_2a4,_2a3);
this.LogSeq("`920`",_2a4);
if(_2a8===true){
this.LogSeq("`385`",_2a4);
_2a9=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_29f,"SB.2.2-2",IntegrationImplementation.GetString("'{0}' is not available at this time.  Please select another menu item to continue.",_29f.GetTitle()),false);
this.LogSeqReturn(_2a9,_2a4);
return _2a9;
}
this.LogSeq("`276`",_2a4);
if(_29f.IsALeaf()===false){
this.LogSeq("`160`",_2a4);
_2a6=this.FlowTreeTraversalSubprocess(_29f,_2a0,null,true,_2a4,_2a3);
this.LogSeq("`637`",_2a4);
if(_2a6.NextActivity===null){
this.LogSeq("`32`",_2a4);
_2a9=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_29f,_2a6.Exception,_2a6.ExceptionText,_2a6.EndSequencingSession);
this.LogSeqReturn(_2a9,_2a4);
return _2a9;
}else{
this.LogSeq("`1647`",_2a4);
this.LogSeq("`45`",_2a4);
if(_2a0==FLOW_DIRECTION_BACKWARD&&_2a6.TraversalDirection==FLOW_DIRECTION_FORWARD){
this.LogSeq("`22`",_2a4);
_2a7=this.FlowActivityTraversalSubprocess(_2a6.NextActivity,FLOW_DIRECTION_FORWARD,FLOW_DIRECTION_BACKWARD,_2a4,_2a3);
}else{
this.LogSeq("`1588`",_2a4);
this.LogSeq("`30`",_2a4);
_2a7=this.FlowActivityTraversalSubprocess(_2a6.NextActivity,_2a0,null,_2a4,_2a3);
}
this.LogSeq("`225`",_2a4);
_2a9=_2a7;
this.LogSeqReturn(_2a9,_2a4);
return _2a9;
}
}
this.LogSeq("`357`",_2a4);
_2a9=new Sequencer_FlowActivityTraversalSubprocessReturnObject(true,_29f,null,"",false);
this.LogSeqReturn(_2a9,_2a4);
return _2a9;
}
function Sequencer_FlowActivityTraversalSubprocessReturnObject(_2ab,_2ac,_2ad,_2ae,_2af){
Debug.AssertError("Invalid endSequencingSession ("+_2af+") passed to FlowActivityTraversalSubprocessReturnObject.",(_2af!=true&&_2af!=false));
this.Deliverable=_2ab;
this.NextActivity=_2ac;
this.Exception=_2ad;
this.ExceptionText=_2ae;
this.EndSequencingSession=_2af;
}
Sequencer_FlowActivityTraversalSubprocessReturnObject.prototype.toString=function(){
return "Deliverable="+this.Deliverable+", NextActivity="+this.NextActivity+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_FlowSubprocess(_2b0,_2b1,_2b2,_2b3,_2b4){
Debug.AssertError("Calling log not passed.",(_2b3===undefined||_2b3===null));
Debug.AssertError("Simple calling log not passed.",(_2b4===undefined||_2b4===null));
var _2b5=this.LogSeqAudit("`1489`"+_2b0+", "+_2b1+", "+_2b2+")",_2b3);
var _2b6;
this.LogSeq("`509`",_2b5);
var _2b7=_2b0;
this.LogSeq("`6`",_2b5);
var _2b8=this.FlowTreeTraversalSubprocess(_2b7,_2b1,null,_2b2,_2b5,_2b4);
this.LogSeq("`488`",_2b5);
if(_2b8.NextActivity===null){
this.LogSeq("`41`",_2b5);
_2b6=new Sequencer_FlowSubprocessResult(_2b7,false,_2b8.Exception,_2b8.ExceptionText,_2b8.EndSequencingSession);
this.LogSeqReturn(_2b6,_2b5);
return _2b6;
}else{
this.LogSeq("`1691`",_2b5);
this.LogSeq("`557`",_2b5);
_2b7=_2b8.NextActivity;
this.LogSeq("`48`",_2b5);
var _2b9=this.FlowActivityTraversalSubprocess(_2b7,_2b1,null,_2b5,_2b4);
this.LogSeq("`0`",_2b5);
_2b6=new Sequencer_FlowSubprocessResult(_2b9.NextActivity,_2b9.Deliverable,_2b9.Exception,_2b9.ExceptionText,_2b9.EndSequencingSession);
this.LogSeqReturn(_2b6,_2b5);
return _2b6;
}
}
function Sequencer_FlowSubprocessResult(_2ba,_2bb,_2bc,_2bd,_2be){
Debug.AssertError("Invalid endSequencingSession ("+_2be+") passed to FlowSubprocessResult.",(_2be!=true&&_2be!=false));
this.IdentifiedActivity=_2ba;
this.Deliverable=_2bb;
this.Exception=_2bc;
this.ExceptionText=_2bd;
this.EndSequencingSession=_2be;
}
Sequencer_FlowSubprocessResult.prototype.toString=function(){
return "IdentifiedActivity="+this.IdentifiedActivity+", Deliverable="+this.Deliverable+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_FlowTreeTraversalSubprocess(_2bf,_2c0,_2c1,_2c2,_2c3,_2c4){
Debug.AssertError("Calling log not passed.",(_2c3===undefined||_2c3===null));
Debug.AssertError("Simple calling log not passed.",(_2c4===undefined||_2c4===null));
var _2c5=this.LogSeqAudit("`1237`"+_2bf+", "+_2c0+", "+_2c1+", "+_2c2+")",_2c3);
var _2c6;
var _2c7;
var _2c8;
var _2c9;
var _2ca;
var _2cb;
var _2cc=this.Activities.GetParentActivity(_2bf);
this.LogSeq("`1174`",_2c5);
_2c6=false;
this.LogSeq("`24`",_2c5);
if(_2c1!==null&&_2c1==FLOW_DIRECTION_BACKWARD&&_2cc.IsActivityTheLastAvailableChild(_2bf)){
this.LogSeq("`1126`",_2c5);
_2c0=FLOW_DIRECTION_BACKWARD;
this.LogSeq("`552`",_2c5);
_2bf=_2cc.GetFirstAvailableChild();
this.LogSeq("`1151`",_2c5);
_2c6=true;
}
this.LogSeq("`980`",_2c5);
if(_2c0==FLOW_DIRECTION_FORWARD){
this.LogSeq("`20`",_2c5);
if((this.IsActivityLastOverall(_2bf,_2c5))||(_2c2===false&&_2bf.IsTheRoot()===true)){
this.LogSeqSimple("A continue request from the end of the course results in exiting the entire course.",_2c4);
this.LogSeq("`575`",_2c5);
this.TerminateDescendentAttemptsProcess(this.Activities.GetRootActivity(),_2c5,_2c4);
this.LogSeq("`167`",_2c5);
_2cb=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,null,null,true);
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}
this.LogSeq("`755`",_2c5);
if(_2bf.IsALeaf()||_2c2===false){
this.LogSeq("`474`",_2c5);
if(_2cc.IsActivityTheLastAvailableChild(_2bf)){
this.LogSeq("`29`",_2c5);
_2c7=this.FlowTreeTraversalSubprocess(_2cc,FLOW_DIRECTION_FORWARD,null,false,_2c5,_2c4);
this.LogSeq("`224`",_2c5);
_2cb=_2c7;
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}else{
this.LogSeq("`1592`",_2c5);
this.LogSeq("`300`",_2c5);
_2c8=_2cc.GetNextActivity(_2bf);
this.LogSeq("`201`",_2c5);
_2cb=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_2c8,_2c0,null,"",false);
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}
}else{
this.LogSeq("`1131`",_2c5);
this.LogSeq("`395`",_2c5);
_2c9=_2bf.GetAvailableChildren();
if(_2c9.length>0){
this.LogSeq("`327`"+_2c9[0]+"); Traversal Direction: traversal direction ("+_2c0+"); Exception: n/a )",_2c5);
_2cb=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_2c9[0],_2c0,null,"",false);
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}else{
this.LogSeq("`1599`",_2c5);
this.LogSeq("`426`",_2c5);
_2cb=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-2",IntegrationImplementation.GetString("The activity '{0}' does not have any available children to deliver.",_2bf.GetTitle()),false);
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}
}
}
this.LogSeq("`971`",_2c5);
if(_2c0==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`462`",_2c5);
if(_2bf.IsTheRoot()){
this.LogSeq("`434`",_2c5);
_2cb=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-3",IntegrationImplementation.GetString("You have reached the beginning of the course."),false);
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}
this.LogSeq("`765`",_2c5);
if(_2bf.IsALeaf()||_2c2===false){
this.LogSeq("`337`",_2c5);
if(_2c6===false){
this.LogSeq("`316`",_2c5);
if(_2cc.GetSequencingControlForwardOnly()===true){
this.LogSeqSimple("\""+_2cc+"\" cannot be moved through backwards (sequencing control forward only). It must be entered from the beginning and proceed forward. Navigation request not allowed.",_2c4);
this.LogSeq("`389`",_2c5);
_2cb=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-4",IntegrationImplementation.GetString("The activity '{0}' may only be entered from the beginning.",_2cc.GetTitle()),false);
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}
}
this.LogSeq("`473`",_2c5);
if(_2cc.IsActivityTheFirstAvailableChild(_2bf)){
this.LogSeq("`25`",_2c5);
_2c7=this.FlowTreeTraversalSubprocess(_2cc,FLOW_DIRECTION_BACKWARD,null,false,_2c5,_2c4);
this.LogSeq("`229`",_2c5);
_2cb=_2c7;
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}else{
this.LogSeq("`1597`",_2c5);
this.LogSeq("`268`",_2c5);
_2ca=_2cc.GetPreviousActivity(_2bf);
this.LogSeq("`798`"+_2ca+"`1535`"+_2c0+"`1633`",_2c5);
_2cb=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_2ca,_2c0,null,"",false);
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}
}else{
this.LogSeq("`1079`",_2c5);
this.LogSeq("`379`",_2c5);
_2c9=_2bf.GetAvailableChildren();
if(_2c9.length>0){
this.LogSeq("`665`",_2c5);
if(_2bf.GetSequencingControlForwardOnly()===true){
this.LogSeqSimple("\""+_2bf+"\" cannot be entered backwards (sequencing control forward only), it must be entered from the beginning. Attempting to deliver its first child ("+_2c9[0]+").",_2c4);
this.LogSeq("`49`",_2c5);
_2cb=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_2c9[0],FLOW_DIRECTION_FORWARD,null,"",false);
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}else{
this.LogSeq("`1566`",_2c5);
this.LogSeq("`43`",_2c5);
_2cb=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_2c9[_2c9.length-1],FLOW_DIRECTION_BACKWARD,null,"",false);
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}
}else{
this.LogSeq("`1595`",_2c5);
this.LogSeq("`404`",_2c5);
_2cb=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-2",IntegrationInterface.GetString("The activity '{0}' may only be entered from the beginning.",_2cc.GetTitle()),false);
this.LogSeqReturn(_2cb,_2c5);
return _2cb;
}
}
}
}
function Sequencer_FlowTreeTraversalSubprocessReturnObject(_2cd,_2ce,_2cf,_2d0,_2d1){
Debug.AssertError("Invalid endSequencingSession ("+_2d1+") passed to FlowTreeTraversalSubprocessReturnObject.",(_2d1!=true&&_2d1!=false));
this.NextActivity=_2cd;
this.TraversalDirection=_2ce;
this.Exception=_2cf;
this.ExceptionText=_2d0;
this.EndSequencingSession=_2d1;
}
Sequencer_FlowTreeTraversalSubprocessReturnObject.prototype.toString=function(){
return "NextActivity="+this.NextActivity+", TraversalDirection="+this.TraversalDirection+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_JumpSequencingRequestProcess(_2d2,_2d3,_2d4){
Debug.AssertError("Calling log not passed.",(_2d3===undefined||_2d3===null));
Debug.AssertError("Simple calling log not passed.",(_2d4===undefined||_2d4===null));
var _2d5=this.LogSeqAudit("`1213`",_2d3);
var _2d6;
this.LogSeq("`480`",_2d5);
if(!this.IsCurrentActivityDefined(_2d5)){
this.LogSeq("`437`",_2d5);
_2d6=new Sequencer_JumpSequencingRequestProcessResult(null,"SB.2.13-1",IntegrationImplementation.GetString("The sequencing session has not begun yet."),false);
this.LogSeqReturn(_2d6,_2d5);
return _2d6;
}
this.LogSeq("`352`",_2d5);
_2d6=new Sequencer_JumpSequencingRequestProcessResult(_2d2,null,"",false);
this.LogSeqReturn(_2d6,_2d5);
return _2d6;
}
function Sequencer_JumpSequencingRequestProcessResult(_2d7,_2d8,_2d9,_2da){
Debug.AssertError("Invalid endSequencingSession ("+_2da+") passed to JumpSequencingRequestProcessResult.",(_2da!=true&&_2da!=false));
this.DeliveryRequest=_2d7;
this.Exception=_2d8;
this.ExceptionText=_2d9;
this.EndSequencingSession=_2da;
}
Sequencer_JumpSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_LimitConditionsCheckProcess(_2db,_2dc,_2dd){
Debug.AssertError("Calling log not passed.",(_2dc===undefined||_2dc===null));
Debug.AssertError("Simple calling log not passed.",(_2dd===undefined||_2dd===null));
var _2de=this.LogSeqAudit("`1271`"+_2db+")",_2dc);
this.LogSeq("`377`",_2de);
if(_2db.IsTracked()===false){
this.LogSeq("`292`",_2de);
this.LogSeqReturn("`1728`",_2de);
return false;
}
this.LogSeq("`143`",_2de);
if(_2db.IsActive()||_2db.IsSuspended()){
this.LogSeq("`677`",_2de);
this.LogSeqReturn("`1728`",_2de);
return false;
}
this.LogSeq("`705`",_2de);
if(_2db.GetLimitConditionAttemptControl()===true){
_2dd=this.LogSeqSimpleAudit("Evaluating the limit condition rules on \""+_2db+"\".",_2dd);
var _2df=_2db.GetAttemptCount();
var _2e0=_2db.GetLimitConditionAttemptLimit();
var _2e1=_2db.GetActivityProgressStatus();
this.LogSeq("`691`"+_2e1+"`1132`"+_2df+"`551`"+_2e0+"`1522`",_2de);
if(_2e1===true&&(_2df>=_2e0)){
this.LogSeq("`416`",_2de);
this.LogSeqReturn("`1732`",_2de);
this.LogSeqSimpleReturn("Attempt limit ("+_2e0+") exceeded. Current attempt count="+_2df,_2dd);
return true;
}
}
this.LogSeq("`539`",_2de);
this.LogSeq("`428`",_2de);
this.LogSeqReturn("`1728`",_2de);
return false;
}
function Sequencer_MeasureRollupProcess(_2e2,_2e3,_2e4){
Debug.AssertError("Calling log not passed.",(_2e3===undefined||_2e3===null));
Debug.AssertError("Simple calling log not passed.",(_2e4===undefined||_2e4===null));
var _2e5=this.LogSeqAudit("`1360`"+_2e2+")",_2e3);
_2e4=this.LogSeqSimpleAudit("Rolling up the score (measure) for \""+_2e2+"\".",_2e4);
this.LogSeq("`961`",_2e5);
var _2e6=0;
this.LogSeq("`1322`",_2e5);
var _2e7=false;
this.LogSeq("`1039`",_2e5);
var _2e8=0;
this.LogSeq("`1052`",_2e5);
var _2e9=null;
var i;
this.LogSeq("`626`",_2e5);
var _2e9=_2e2.GetPrimaryObjective();
this.LogSeq("`1102`",_2e5);
if(_2e9!==null){
this.LogSeq("`1157`",_2e5);
var _2eb=_2e2.GetChildren();
var _2ec=null;
var _2ed;
var _2ee;
var _2ef;
for(i=0;i<_2eb.length;i++){
this.LogSeq("`556`"+_2eb[i].IsTracked(),_2e5);
if(_2eb[i].IsTracked()){
this.LogSeq("`978`",_2e5);
_2ec=null;
this.LogSeq("`1158`",_2e5);
var _2ec=_2eb[i].GetPrimaryObjective();
this.LogSeq("`962`",_2e5);
if(_2ec!==null){
this.LogSeq("`545`",_2e5);
_2ee=_2eb[i].GetRollupObjectiveMeasureWeight();
_2e8+=_2ee;
this.LogSeq("`600`",_2e5);
if(_2ec.GetMeasureStatus(_2eb[i],false)===true){
this.LogSeq("`122`",_2e5);
_2ef=_2ec.GetNormalizedMeasure(_2eb[i],false);
this.LogSeq("`829`"+_2ef+"`1458`"+_2ee,_2e5);
_2e7=true;
_2e6+=(_2ef*_2ee);
this.LogSeqSimple("\""+_2eb[i]+"\" has a score of "+_2ef+" and a weight of "+_2ee+" so it contributes a weighted measure of "+(_2ef*_2ee)+".",_2e4);
}
}else{
this.LogSeq("`1567`",_2e5);
this.LogSeq("`501`",_2e5);
Debug.AssertError("Measure Rollup Process encountered an activity with no primary objective.");
this.LogSeqReturn("",_2e5);
return;
}
}else{
this.LogSeqSimple("\""+_2eb[i]+"\" is not tracked and will not contribute a score to rollup.",_2e4);
}
}
this.LogSeq("`1233`",_2e5);
if(_2e7===false||_2e8==0){
this.LogSeq("`103`"+_2e8+")",_2e5);
this.LogSeqSimple("No children had a score to rollup, setting the measure status to unknown.",_2e4);
this.LogSeqSimpleReturn("unknown.",_2e4);
_2e9.SetMeasureStatus(false,_2e2);
}else{
this.LogSeq("`1644`",_2e5);
this.LogSeq("`387`",_2e5);
if(_2e8>0){
this.LogSeq("`672`",_2e5);
_2e9.SetMeasureStatus(true,_2e2);
this.LogSeq("`478`"+_2e6+"`1374`"+_2e8+"`1718`"+(_2e6/_2e8),_2e5);
var _2f0=(_2e6/_2e8);
_2f0=RoundToPrecision(_2f0,7);
_2e9.SetNormalizedMeasure(_2f0,_2e2);
this.LogSeqSimple("Setting the score to the new weighted measure of "+_2f0+".",_2e4);
this.LogSeqSimpleReturn(_2f0,_2e4);
}else{
this.LogSeq("`1585`",_2e5);
this.LogSeq("`452`",_2e5);
_2e9.SetMeasureStatus(false,_2e2);
}
}
}
this.LogSeq("`542`",_2e5);
this.LogSeqReturn("",_2e5);
return;
}
function Sequencer_NavigationRequestProcess(_2f1,_2f2,_2f3,_2f4){
Debug.AssertError("Calling log not passed.",(_2f3===undefined||_2f3===null));
Debug.AssertError("Simple calling log not passed.",(_2f4===undefined||_2f4===null));
var _2f5=this.LogSeqAudit("`1293`"+_2f1+", "+_2f2+")",_2f3);
var _2f6=this.GetCurrentActivity();
if(_2f6!==null){
this.LogSeqSimple("The current activity is \""+_2f6+"\".",_2f4,_2f4);
}else{
this.LogSeqSimple("There is no current activity.",_2f4,_2f4);
}
var _2f7=null;
if(_2f6!==null){
_2f7=_2f6.ParentActivity;
}
var _2f8="";
if(_2f7!==null){
_2f8=_2f7.GetTitle();
}
var _2f9;
switch(_2f1){
case NAVIGATION_REQUEST_START:
this.LogSeq("`1162`",_2f5);
this.LogSeq("`463`",_2f5);
if(!this.IsCurrentActivityDefined(_2f5)){
this.LogSeq("`207`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,null,SEQUENCING_REQUEST_START,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`1640`",_2f5);
this.LogSeq("`176`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-1",null,IntegrationImplementation.GetString("The sequencing session has already been started."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
case NAVIGATION_REQUEST_RESUME_ALL:
this.LogSeq("`1037`",_2f5);
this.LogSeq("`465`",_2f5);
if(!this.IsCurrentActivityDefined(_2f5)){
this.LogSeq("`334`",_2f5);
if(this.IsSuspendedActivityDefined(_2f5)){
this.LogSeq("`168`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,null,SEQUENCING_REQUEST_RESUME_ALL,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`1580`",_2f5);
this.LogSeq("`161`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-3",null,IntegrationImplementation.GetString("There is no suspended activity to resume."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}else{
this.LogSeq("`1649`",_2f5);
this.LogSeq("`177`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-1",null,IntegrationImplementation.GetString("The sequencing session has already been started."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
case NAVIGATION_REQUEST_CONTINUE:
this.LogSeq("`1081`",_2f5);
this.LogSeq("`482`",_2f5);
if(!this.IsCurrentActivityDefined(_2f5)){
this.LogSeq("`179`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-1",null,IntegrationImplementation.GetString("Cannot continue until the sequencing session has begun."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
this.LogSeq("`40`",_2f5);
if((!_2f6.IsTheRoot())&&(_2f7.LearningObject.SequencingData.ControlFlow===true)){
this.LogSeq("`213`",_2f5);
if(_2f6.IsActive()){
this.LogSeq("`185`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_CONTINUE,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`1591`",_2f5);
this.LogSeq("`192`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,null,SEQUENCING_REQUEST_CONTINUE,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}else{
this.LogSeq("`1659`",_2f5);
this.LogSeq("`34`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-4",null,IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_2f8));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
case NAVIGATION_REQUEST_PREVIOUS:
this.LogSeq("`1090`",_2f5);
this.LogSeq("`483`",_2f5);
if(!this.IsCurrentActivityDefined(_2f5)){
this.LogSeq("`184`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("Cannot move backwards until the sequencing session has begun."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
this.LogSeq("`241`",_2f5);
if(!_2f6.IsTheRoot()){
this.LogSeq("`12`",_2f5);
if(_2f7.LearningObject.SequencingData.ControlFlow===true&&_2f7.LearningObject.SequencingData.ControlForwardOnly===false){
this.LogSeq("`204`",_2f5);
if(_2f6.IsActive()){
this.LogSeq("`173`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_PREVIOUS,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`1569`",_2f5);
this.LogSeq("`180`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,null,SEQUENCING_REQUEST_PREVIOUS,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}else{
this.LogSeq("`1609`",_2f5);
this.LogSeq("`102`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-5",null,IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_2f7.GetTitle()));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}else{
this.LogSeq("`1656`",_2f5);
this.LogSeq("`50`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-6",null,IntegrationImplementation.GetString("You have reached the beginning of the course."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
case NAVIGATION_REQUEST_FORWARD:
this.LogSeq("`799`",_2f5);
this.LogSeq("`188`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-7",null,IntegrationImplementation.GetString("The 'Forward' navigation request is not supported, try using 'Continue'."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
case NAVIGATION_REQUEST_BACKWARD:
this.LogSeq("`788`",_2f5);
this.LogSeq("`189`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-7",null,IntegrationImplementation.GetString("The 'Backward' navigation request is not supported, try using 'Previous'."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
case NAVIGATION_REQUEST_CHOICE:
this.LogSeq("`1100`",_2f5);
this.LogSeq("`196`",_2f5);
if(this.DoesActivityExist(_2f2,_2f5)){
var _2fa=this.GetActivityFromIdentifier(_2f2,_2f5);
var _2fb=this.Activities.GetParentActivity(_2fa);
if(_2fa.IsAvailable()===false){
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-7",null,IntegrationImplementation.GetString("The activity '{0}' was not selected to be delivered in this attempt.",_2fa));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
this.LogSeq("`2`",_2f5);
if(_2fa.IsTheRoot()||_2fb.LearningObject.SequencingData.ControlChoice===true){
this.LogSeq("`450`",_2f5);
if(!this.IsCurrentActivityDefined(_2f5)){
this.LogSeq("`59`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,null,SEQUENCING_REQUEST_CHOICE,null,_2fa,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
this.LogSeq("`402`",_2f5);
if(!this.AreActivitiesSiblings(_2f6,_2fa,_2f5)){
this.LogSeq("`365`",_2f5);
var _2fc=this.FindCommonAncestor(_2f6,_2fa,_2f5);
this.LogSeq("`1`",_2f5);
var _2fd=this.GetPathToAncestorExclusive(_2f6,_2fc,true);
this.LogSeq("`938`",_2f5);
if(_2fd.length>0){
this.LogSeq("`95`",_2f5);
for(var i=0;i<_2fd.length;i++){
if(_2fd[i].GetItemIdentifier()==_2fc.GetItemIdentifier()){
break;
}
this.LogSeq("`216`"+_2fd[i].LearningObject.ItemIdentifier+")",_2f5);
if(_2fd[i].IsActive()===true&&_2fd[i].LearningObject.SequencingData.ControlChoiceExit===false){
this.LogSeq("`175`"+CONTROL_CHOICE_EXIT_ERROR_NAV+"`1482`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,CONTROL_CHOICE_EXIT_ERROR_NAV,null,IntegrationImplementation.GetString("You must complete '{0}' before you can select another item.",_2fd[i]));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}
}else{
this.LogSeq("`1525`",_2f5);
this.LogSeq("`147`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-9",null,IntegrationImplementation.GetString("Nothing to open"));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}
this.LogSeq("`46`",_2f5);
if(_2f6.IsActive()&&_2f6.GetSequencingControlChoiceExit()===false){
this.LogSeq("`223`"+CONTROL_CHOICE_EXIT_ERROR_NAV+"`1508`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,CONTROL_CHOICE_EXIT_ERROR_NAV,null,IntegrationImplementation.GetString("You are not allowed to jump out of {0}.",_2f6.GetTitle()));
return _2f9;
}
this.LogSeq("`203`",_2f5);
if(_2f6.IsActive()){
this.LogSeq("`56`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_CHOICE,null,_2fa,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`1560`",_2f5);
this.LogSeq("`60`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,null,SEQUENCING_REQUEST_CHOICE,null,_2fa,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}else{
this.LogSeq("`1604`",_2f5);
this.LogSeq("`99`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-10",null,IntegrationImplementation.GetString("Please select 'Next' or 'Previous' to move through {0}.",_2fb.GetTitle()));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}else{
this.LogSeq("`1648`",_2f5);
this.LogSeq("`86`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-11",null,IntegrationImplementation.GetString("The activity you selected ({0}) does not exist.",_2f2));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
case NAVIGATION_REQUEST_EXIT:
this.LogSeq("`1164`",_2f5);
this.LogSeq("`506`",_2f5);
if(this.IsCurrentActivityDefined(_2f5)){
this.LogSeq("`285`",_2f5);
if(_2f6.IsActive()){
this.LogSeq("`194`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`1603`",_2f5);
this.LogSeq("`75`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-12",null,IntegrationImplementation.GetString("The Exit navigation request is invalid because the current activity ({0}) is no longer active.",_2f6.GetTitle()));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}else{
this.LogSeq("`1645`",_2f5);
this.LogSeq("`172`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The Exit navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
case NAVIGATION_REQUEST_EXIT_ALL:
this.LogSeq("`1087`",_2f5);
this.LogSeq("`275`",_2f5);
if(this.IsCurrentActivityDefined(_2f5)){
this.LogSeq("`193`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,TERMINATION_REQUEST_EXIT_ALL,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`1646`",_2f5);
this.LogSeq("`178`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The Exit All navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
case NAVIGATION_REQUEST_ABANDON:
this.LogSeq("`1085`",_2f5);
this.LogSeq("`505`",_2f5);
if(this.IsCurrentActivityDefined(_2f5)){
this.LogSeq("`289`",_2f5);
if(_2f6.IsActive()){
this.LogSeq("`182`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,TERMINATION_REQUEST_ABANDON,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`1575`",_2f5);
this.LogSeq("`154`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-12",null,IntegrationImplementation.GetString("The 'Abandon' navigation request is invalid because the current activity '{0}' is no longer active.",_2f6.GetTitle()));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}else{
this.LogSeq("`1596`",_2f5);
this.LogSeq("`174`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The 'Abandon' navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
case NAVIGATION_REQUEST_ABANDON_ALL:
this.LogSeq("`1005`",_2f5);
this.LogSeq("`277`",_2f5);
if(this.IsCurrentActivityDefined(_2f5)){
this.LogSeq("`170`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,TERMINATION_REQUEST_ABANDON_ALL,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`1616`",_2f5);
this.LogSeq("`165`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("You cannot use 'Abandon All' if no item is currently open."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
case NAVIGATION_REQUEST_SUSPEND_ALL:
this.LogSeq("`995`",_2f5);
this.LogSeq("`540`",_2f5);
if(this.IsCurrentActivityDefined(_2f5)){
this.LogSeq("`166`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,TERMINATION_REQUEST_SUSPEND_ALL,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`1628`",_2f5);
this.LogSeq("`169`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The 'Suspend All' navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
case NAVIGATION_REQUEST_JUMP:
this.LogSeq("`1140`",_2f5);
this.LogSeq("`21`",_2f5);
if(this.DoesActivityExist(_2f2,_2f5)){
var _2fa=this.GetActivityFromIdentifier(_2f2,_2f5);
if(_2fa.IsAvailable()===true){
this.LogSeq("`68`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(_2f1,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_JUMP,null,_2fa,"");
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}else{
this.LogSeq("`79`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-11",null,IntegrationImplementation.GetString("The Jump target activity requested '{0}' is not available.",_2fb.GetTitle()));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}else{
this.LogSeq("`1624`",_2f5);
this.LogSeq("`82`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-11",null,IntegrationImplementation.GetString("The Jump target activity requested '{0}' does not exist.",_2fb.GetTitle()));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
break;
default:
this.LogSeq("`94`",_2f5);
_2f9=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-13",null,IntegrationImplementation.GetString("Undefined Navigation Request"));
this.LogSeqReturn(_2f9,_2f5);
return _2f9;
}
}
function Sequencer_NavigationRequestProcessResult(_2ff,_300,_301,_302,_303,_304){
this.NavigationRequest=_2ff;
this.TerminationRequest=_300;
this.SequencingRequest=_301;
this.Exception=_302;
this.TargetActivity=_303;
this.ExceptionText=_304;
}
Sequencer_NavigationRequestProcessResult.prototype.toString=function(){
return "NavigationRequest="+this.NavigationRequest+", TerminationRequest="+this.TerminationRequest+", SequencingRequest="+this.SequencingRequest+", Exception="+this.Exception+", TargetActivity="+this.TargetActivity+", ExceptionText="+this.ExceptionText;
};
function Sequencer_ObjectiveRollupProcess(_305,_306,_307){
Debug.AssertError("Calling log not passed.",(_306===undefined||_306===null));
Debug.AssertError("Simple calling log not passed.",(_307===undefined||_307===null));
var _308=this.LogSeqAudit("`1347`"+_305+")",_306);
_307=this.LogSeqSimpleAudit("Rolling up the satisfaction status of \""+_305+"\".",_307);
this.LogSeq("`1067`",_308);
var _309=null;
this.LogSeq("`627`",_308);
var _309=_305.GetPrimaryObjective();
this.LogSeq("`1054`",_308);
if(_309!==null){
this.LogSeq("`693`",_308);
if(_309.GetSatisfiedByMeasure()===true){
this.LogSeq("`864`",_308);
this.ObjectiveRollupUsingMeasureProcess(_305,_308,_307);
}else{
this.LogSeq("`1642`",_308);
this.LogSeq("`499`",_308);
this.ObjectiveRollupUsingRulesProcess(_305,_308,_307);
}
this.LogSeq("`1182`",_308);
this.LogSeqReturn("",_308);
return;
}else{
this.LogSeq("`1655`",_308);
this.LogSeq("`479`",_308);
this.LogSeqReturn("",_308);
return;
}
}
function Sequencer_ObjectiveRollupUsingMeasureProcess(_30a,_30b,_30c){
Debug.AssertError("Calling log not passed.",(_30b===undefined||_30b===null));
Debug.AssertError("Simple calling log not passed.",(_30c===undefined||_30c===null));
var _30d=this.LogSeqAudit("`1025`"+_30a+")",_30b);
this.LogSeq("`1016`",_30d);
var _30e=null;
this.LogSeq("`617`",_30d);
var _30e=_30a.GetPrimaryObjective();
this.LogSeq("`1054`",_30d);
if(_30e!==null){
this.LogSeq("`128`",_30d);
if(_30e.GetSatisfiedByMeasure()===true){
this.LogSeq("`302`",_30d);
if(_30e.GetMeasureStatus(_30a,false)===false){
this.LogSeq("`628`",_30d);
this.LogSeqSimple("\""+_30a+"\" is sastisfied by measure but does not have a known measure so its satisfaction is unknown.",_30c);
_30e.SetProgressStatus(false,false,_30a);
}else{
this.LogSeq("`1559`",_30d);
this.LogSeq("`130`",_30d);
if(_30a.IsActive()===false||_30a.GetMeasureSatisfactionIfActive()===true){
this.LogSeq("`105`",_30d);
var _30f=_30e.GetNormalizedMeasure(_30a,false);
var _310=_30e.GetMinimumSatisfiedNormalizedMeasure();
if(_30f>=_310){
this.LogSeqSimple("\""+_30a+"\" is sastisfied by measure and its measure of "+_30f+" exceeds the threshold of "+_310+" so setting the status to satisfied.",_30c);
this.LogSeq("`613`",_30d);
_30e.SetProgressStatus(true,false,_30a);
this.LogSeq("`604`",_30d);
_30e.SetSatisfiedStatus(true,false,_30a);
}else{
this.LogSeqSimple("\""+_30a+"\" is sastisfied by measure and its measure of "+_30f+" is below the threshold of "+_310+" so setting the status to not satisfied.",_30c);
this.LogSeq("`1484`",_30d);
this.LogSeq("`612`",_30d);
_30e.SetProgressStatus(true,false,_30a);
this.LogSeq("`598`",_30d);
_30e.SetSatisfiedStatus(false,false,_30a);
}
}else{
this.LogSeq("`1534`",_30d);
this.LogSeq("`270`",_30d);
_30e.SetProgressStatus(false,false,_30a);
}
}
}
this.LogSeq("`911`",_30d);
this.LogSeqReturn("",_30d);
return;
}else{
this.LogSeq("`1655`",_30d);
this.LogSeq("`391`",_30d);
this.LogSeqReturn("",_30d);
return;
}
}
function Sequencer_ObjectiveRollupUsingRulesProcess(_311,_312,_313){
Debug.AssertError("Calling log not passed.",(_312===undefined||_312===null));
var _314=this.LogSeqAudit("`1053`"+_311+")",_312);
var _315;
this.LogSeq("`23`",_314);
if(Sequencer_GetApplicableSetofRollupRules(_311,RULE_SET_NOT_SATISFIED).length===0&&Sequencer_GetApplicableSetofRollupRules(_311,RULE_SET_SATISFIED).length===0){
this.LogSeqSimple("Applying the default set of satisfaction rollup rules to \""+_311+"\".",_313);
this.LogSeq("`110`",_314);
_311.ApplyRollupRule(new SequencingRollupRule(RULE_CONDITION_COMBINATION_ANY,CHILD_ACTIVITY_SET_ALL,ROLLUP_RULE_MINIMUM_COUNT_DEFAULT,ROLLUP_RULE_MINIMUM_PERCENT_DEFAULT,ROLLUP_RULE_ACTION_SATISFIED,new Array(new SequencingRollupRuleCondition(RULE_CONDITION_OPERATOR_NOOP,ROLLUP_RULE_CONDITION_SATISFIED))));
this.LogSeq("`69`",_314);
_311.ApplyRollupRule(new SequencingRollupRule(RULE_CONDITION_COMBINATION_ANY,CHILD_ACTIVITY_SET_ALL,ROLLUP_RULE_MINIMUM_COUNT_DEFAULT,ROLLUP_RULE_MINIMUM_PERCENT_DEFAULT,ROLLUP_RULE_ACTION_NOT_SATISFIED,new Array(new SequencingRollupRuleCondition(RULE_CONDITION_OPERATOR_NOOP,ROLLUP_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN))));
}else{
if(_311.UsesDefaultSatisfactionRollupRules()===true){
this.LogSeqSimple("Applying the default set of satisfaction rollup rules to \""+_311+"\".",_313);
}
}
this.LogSeq("`1038`",_314);
var _316=null;
this.LogSeq("`622`",_314);
var _316=_311.GetPrimaryObjective();
this.LogSeq("`1057`",_314);
if(_316!==null){
this.LogSeq("`293`",_314);
_315=this.RollupRuleCheckSubprocess(_311,RULE_SET_NOT_SATISFIED,_314,_313);
this.LogSeq("`803`",_314);
if(_315===true){
this.LogSeqSimple("Setting "+_311+" to not satisfied.",_313);
this.LogSeq("`655`",_314);
_316.SetProgressStatus(true,false,_311);
this.LogSeq("`639`",_314);
_316.SetSatisfiedStatus(false,false,_311);
}
this.LogSeq("`326`",_314);
_315=this.RollupRuleCheckSubprocess(_311,RULE_SET_SATISFIED,_314,_313);
this.LogSeq("`804`",_314);
if(_315===true){
this.LogSeqSimple("Setting \""+_311+"\" to satisfied.",_313);
this.LogSeq("`649`",_314);
_316.SetProgressStatus(true,false,_311);
this.LogSeq("`641`",_314);
_316.SetSatisfiedStatus(true,false,_311);
}
this.LogSeq("`950`",_314);
this.LogSeqReturn("",_314);
return;
}else{
this.LogSeq("`1664`",_314);
this.LogSeq("`431`",_314);
this.LogSeqReturn("",_314);
return;
}
}
function Sequencer_OverallRollupProcess(_317,_318,_319){
Debug.AssertError("Calling log not passed.",(_318===undefined||_318===null));
Debug.AssertError("Simple calling log not passed.",(_319===undefined||_319===null));
var _31a=this.LogSeqAudit("`1369`"+_317+")",_318);
_319=this.LogSeqSimpleAudit("Rolling up data for \""+_317+"\".",_319);
this.LogSeq("`256`",_31a);
var _31b=this.GetActivityPath(_317,true);
this.LogSeq("`1115`",_31a);
if(_31b.length===0){
this.LogSeq("`895`",_31a);
this.LogSeqReturn("",_31a);
return;
}
this.LogSeq("`1043`",_31a);
var _31c;
var _31d;
var _31e;
var _31f;
var _320;
var _321;
var _322;
var _323;
var _324;
var _325;
var _326;
var _327;
var _328;
var _329=new Array();
var _32a=false;
for(var i=0;i<_31b.length;i++){
if(!_31b[i].IsALeaf()){
_31b[i].RollupDurations();
}
if(_32a){
continue;
}
_31c=_31b[i].GetPrimaryObjective();
_31d=_31c.GetMeasureStatus(_31b[i],false);
_31e=_31c.GetNormalizedMeasure(_31b[i],false);
_31f=_31c.GetProgressStatus(_31b[i],false);
_320=_31c.GetSatisfiedStatus(_31b[i],false);
_321=_31b[i].GetAttemptProgressStatus();
_322=_31b[i].GetAttemptCompletionStatus();
this.LogSeq("`553`",_31a);
if(!_31b[i].IsALeaf()){
this.LogSeq("`563`",_31a);
this.MeasureRollupProcess(_31b[i],_31a,_319);
this.LogSeq("`454`",_31a);
this.CompletionMeasureRollupProcess(_31b[i],_31a,_319);
}
this.LogSeq("`728`",_31a);
this.ObjectiveRollupProcess(_31b[i],_31a,_319);
this.LogSeq("`261`",_31a);
this.ActivityProgressRollupProcess(_31b[i],_31a,_319);
_323=_31c.GetMeasureStatus(_31b[i],false);
_324=_31c.GetNormalizedMeasure(_31b[i],false);
_325=_31c.GetProgressStatus(_31b[i],false);
_326=_31c.GetSatisfiedStatus(_31b[i],false);
_327=_31b[i].GetAttemptProgressStatus();
_328=_31b[i].GetAttemptCompletionStatus();
if(!this.LookAhead&&_31b[i].IsTheRoot()){
if(_327!=_321||_328!=_322){
var _32c=(_327?(_328?SCORM_STATUS_COMPLETED:SCORM_STATUS_INCOMPLETE):SCORM_STATUS_NOT_ATTEMPTED);
this.WriteHistoryLog("",{ev:"Rollup Completion",v:_32c,ai:_31b[i].ItemIdentifier});
}
if(_325!=_31f||_326!=_320){
var _32d=(_325?(_326?SCORM_STATUS_PASSED:SCORM_STATUS_FAILED):SCORM_STATUS_UNKNOWN);
this.WriteHistoryLog("",{ev:"Rollup Satisfaction",v:_32d,ai:_31b[i].ItemIdentifier});
}
}
if(_326!=_320){
_31b[i].WasAutoSatisfied=_317.WasAutoSatisfied;
}
if(_328!=_322){
_31b[i].WasAutoCompleted=_317.WasAutoCompleted;
}
if(i>0&&_323==_31d&&_324==_31e&&_325==_31f&&_326==_320&&_327==_321&&_328==_322){
this.LogSeq("`901`",_31a);
_32a=true;
}else{
this.LogSeq("`865`",_31a);
_329[_329.length]=_31b[i];
}
}
this.LogSeq("`1261`",_31a);
this.LogSeqReturn("",_31a);
return _329;
}
function Sequencer_OverallSequencingProcess(_32e){
var _32f=null;
var _330=null;
var _331=null;
var _332=null;
var _333=null;
this.ResetException();
if(this.NavigationRequest!==null){
var _334=this.LogSeqSimpleAudit("Starting the sequencing process for navigation request "+this.NavigationRequest.toDescriptiveString()+".",_334);
}else{
var _334=this.LogSeqSimpleAudit("Starting the sequencing process with no navigation request.",_334);
}
var _335=this.LogSeqAudit("`1349`");
this.LogSeq("`1399`"+this.NavigationRequest,_335);
if(this.NavigationRequest===null){
var _336=this.GetExitAction(this.GetCurrentActivity(),_335);
this.LogSeq("`1035`"+_336,_335);
var _337="";
if(_336==EXIT_ACTION_EXIT_CONFIRMATION||_336==EXIT_ACTION_DISPLAY_MESSAGE){
var _338=Control.Activities.GetRootActivity();
var _339=(_338.IsCompleted()||_338.IsSatisfied());
if(_339===true){
_337=IntegrationImplementation.GetString("The course is now complete. Please make a selection to continue.");
}else{
_337=IntegrationImplementation.GetString("Please make a selection.");
}
}
switch(_336){
case (EXIT_ACTION_EXIT_NO_CONFIRMATION):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
break;
case (EXIT_ACTION_EXIT_CONFIRMATION):
if(confirm("Would you like to exit the course now?")){
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
}else{
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE,null,_337);
}
break;
case (EXIT_ACTION_GO_TO_NEXT_SCO):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_CONTINUE,null,"");
break;
case (EXIT_ACTION_DISPLAY_MESSAGE):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE,null,_337);
break;
case (EXIT_ACTION_DO_NOTHING):
this.NavigationRequest=null;
break;
case (EXIT_ACTION_REFRESH_PAGE):
Control.RefreshPage();
break;
}
}
if(this.NavigationRequest==null){
this.LogSeqSimpleReturn("Perform no action, no navigation request identified",_334);
this.LogSeqReturn("`1393`",_335);
return;
}
if(this.NavigationRequest.Type==NAVIGATION_REQUEST_DISPLAY_MESSAGE){
this.LogSeq("`696`",_335);
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT,null,"");
}
if(this.NavigationRequest.Type==NAVIGATION_REQUEST_EXIT_PLAYER){
this.LogSeq("`813`",_335);
Control.ExitScormPlayer();
return;
}
this.LogSeq("`763`",_335);
var _33a=this.NavigationRequestProcess(this.NavigationRequest.Type,this.NavigationRequest.TargetActivity,_335,_334);
this.LogSeq("`624`",_335);
if(_33a.NavigationRequest==NAVIGATION_REQUEST_NOT_VALID){
this.LogSeqSimple("The navigation request is invalid and should not have been triggered. The specific error was: "+_33a.ExceptionText,_334);
this.LogSeq("`731`",_335);
this.Exception=_33a.Exception;
this.ExceptionText=_33a.ExceptionText;
this.LogSeq("`853`",_335);
this.LogSeqSimpleReturn("Perform no action, navigation request is invalid.",_334);
this.LogSeqReturn("`1728`",_335);
return false;
}
_32f=_33a.TerminationRequest;
_330=_33a.SequencingRequest;
_331=_33a.TargetActivity;
this.LogSeq("`368`",_335);
if(_32f!==null){
this.LogSeq("`706`",_335);
var _33b=this.TerminationRequestProcess(_32f,_335,_334);
this.LogSeq("`603`",_335);
if(_33b.TerminationRequest==TERMINATION_REQUEST_NOT_VALID){
this.LogSeq("`702`",_335);
this.Exception=_33b.Exception;
this.ExceptionText=_33b.ExceptionText;
this.LogSeq("`833`",_335);
this.LogSeqSimpleReturn("Perform no action, can't terminate current activity.",_334);
this.LogSeqReturn("`1728`",_335);
return false;
}
this.LogSeq("`701`",_335);
if(_33b.SequencingRequest!==null){
if(_330!=_33b.SequencingRequest){
this.LogSeqSimple("After exiting the current activity and evaluating all exit/post condition sequencing rules, the original sequencing request (from the user or from the SCO) of '"+_330+"' is being replaced with a sequencing request of '"+_33b.SequencingRequest+"'.",_334);
}
this.LogSeq("`39`",_335);
_330=_33b.SequencingRequest;
}
}
this.LogSeq("`1060`",_335);
if(_330!==null){
this.LogSeq("`732`",_335);
var _33c=this.SequencingRequestProcess(_330,_331,_335,_334);
this.LogSeq("`608`",_335);
if(_33c.SequencingRequest==SEQUENCING_REQUEST_NOT_VALID){
this.LogSeqSimple("No activity identified for delivery, invalid request. Display an error message or user prompt.",_334);
this.LogSeq("`716`",_335);
this.Exception=_33c.Exception;
this.ExceptionText=_33c.ExceptionText;
this.LogSeq("`824`",_335);
this.LogSeqSimpleReturn("Perform no action, couldn't identify an activity for delivery.",_334);
this.LogSeqReturn("`1728`",_335);
return false;
}
this.LogSeq("`535`",_335);
if(_33c.EndSequencingSession===true){
this.LogSeqSimple("The sequencing request resulting in the end of the course. Exit the SCORM player.",_334);
this.LogSeq("`76`",_335);
Control.ExitScormPlayer();
this.LogSeqSimpleReturn("Exit SCORM Player.",_334);
this.LogSeqReturn("",_335);
return;
}
this.LogSeq("`582`",_335);
if(_33c.DeliveryRequest===null){
this.LogSeqSimple("The sequencing session did not identify an activity for delivery, but not because of an error condition. Display a message to the user.",_334);
this.LogSeq("`826`",_335);
this.Exception="OP.1.4";
this.ExceptionText=IntegrationImplementation.GetString("Please make a selection.");
this.LogSeqSimpleReturn("Prompt user for selection.",_334);
this.LogSeqReturn("",_335);
return;
}
this.LogSeqSimple("The sequencing session identified \""+_33c.DeliveryRequest+"\" for delivery.",_334);
this.LogSeq("`574`",_335);
_332=_33c.DeliveryRequest;
}
this.LogSeq("`1098`",_335);
var _33d=false;
if(_332!==null){
this.LogSeq("`782`",_335);
var _33e=this.DeliveryRequestProcess(_332,_335,_334);
this.LogSeq("`633`",_335);
if(_33e.Valid===false){
this.LogSeq("`738`",_335);
this.Exception="OP.1.5";
this.ExceptionText=IntegrationImplementation.GetString("Please make a selection.");
this.LogSeq("`820`",_335);
this.LogSeqSimpleReturn("Prompt user for selection.",_334);
this.LogSeqReturn("`1728`",_335);
return false;
}
this.LogSeq("`657`",_335);
_33d=true;
this.ContentDeliveryEnvironmentProcess(_332,_335,_334);
}
this.LogSeq("`944`",_335);
if(_33d===true){
this.LogSeqSimpleReturn("Deliver activity: "+_332,_334);
this.LogSeqReturn("`1629`"+_332,_335);
}else{
this.LogSeqReturn("",_335);
}
return;
}
function Sequencer_PreviousSequencingRequestProcess(_33f,_340){
Debug.AssertError("Calling log not passed.",(_33f===undefined||_33f===null));
Debug.AssertError("Simple calling log not passed.",(_340===undefined||_340===null));
var _341=this.LogSeqAudit("`1129`",_33f);
var _342;
this.LogSeq("`502`",_341);
if(!this.IsCurrentActivityDefined(_341)){
this.LogSeq("`407`",_341);
_342=new Sequencer_PreviousSequencingRequestProcessResult(null,"SB.2.8-1",IntegrationImplementation.GetString("You cannot use 'Previous' at this time."),false);
this.LogSeqReturn(_342,_341);
return _342;
}
var _343=this.GetCurrentActivity();
this.LogSeq("`708`",_341);
if(!_343.IsTheRoot()){
var _344=this.Activities.GetParentActivity(_343);
this.LogSeq("`298`",_341);
if(_344.GetSequencingControlFlow()===false){
this.LogSeq("`562`",_341);
this.LogSeqSimple("Flow navigation is not allowed within \""+_344+"\". Prompting the used to make a different selection.",_340);
_342=new Sequencer_PreviousSequencingRequestProcessResult(null,"SB.2.8-2",IntegrationImplementation.GetString("Please select 'Next' or 'Previous' to move through {0}.",_344.GetTitle()),false);
this.LogSeqReturn(_342,_341);
return _342;
}
}
this.LogSeq("`131`",_341);
var _345=this.FlowSubprocess(_343,FLOW_DIRECTION_BACKWARD,false,_341,_340);
this.LogSeq("`985`",_341);
if(_345.Deliverable===false){
this.LogSeq("`226`",_341);
_342=new Sequencer_PreviousSequencingRequestProcessResult(null,_345.Exception,_345.ExceptionText,_345.EndSequencingSession);
this.LogSeqReturn(_342,_341);
return _342;
}else{
this.LogSeq("`1682`",_341);
this.LogSeq("`323`",_341);
_342=new Sequencer_PreviousSequencingRequestProcessResult(_345.IdentifiedActivity,null,"",false);
this.LogSeqReturn(_342,_341);
return _342;
}
}
function Sequencer_PreviousSequencingRequestProcessResult(_346,_347,_348,_349){
Debug.AssertError("Invalid endSequencingSession ("+_349+") passed to PreviousSequencingRequestProcessResult.",(_349!=true&&_349!=false));
this.DeliveryRequest=_346;
this.Exception=_347;
this.ExceptionText=_348;
this.EndSequencingSession=_349;
}
Sequencer_PreviousSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_RandomizeChildrenProcess(_34a,_34b,_34c,_34d){
Debug.AssertError("Calling log not passed.",(_34c===undefined||_34c===null));
Debug.AssertError("Simple calling log not passed.",(_34d===undefined||_34d===null));
var _34e=this.LogSeqAudit("`1325`"+_34a+")",_34c);
var _34f;
this.LogSeq("`532`",_34e);
if(_34a.IsALeaf()){
this.LogSeq("`1184`",_34e);
this.LogSeqReturn("",_34e);
return;
}
this.LogSeq("`153`",_34e);
if(_34a.IsActive()===true||_34a.IsSuspended()===true){
this.LogSeq("`1173`",_34e);
this.LogSeqReturn("",_34e);
return;
}
var _350=_34a.GetRandomizationTiming();
switch(_350){
case TIMING_NEVER:
this.LogSeq("`845`",_34e);
this.LogSeq("`1185`",_34e);
break;
case TIMING_ONCE:
this.LogSeq("`856`",_34e);
this.LogSeq("`778`",_34e);
if(_34a.GetRandomizedChildren()===false){
this.LogSeq("`429`",_34e);
if(_34a.GetActivityProgressStatus()===false){
this.LogSeq("`863`",_34e);
if(_34a.GetRandomizeChildren()===true){
this.LogSeqSimple("Randomizing the order of the children of \""+_34a+"\".",_34d);
this.LogSeq("`567`",_34e);
_34f=Sequencer_RandomizeArray(_34a.GetAvailableChildren());
_34a.SetAvailableChildren(_34f);
_34a.SetRandomizedChildren(true);
}
}
}
this.LogSeq("`1188`",_34e);
break;
case TIMING_ON_EACH_NEW_ATTEMPT:
this.LogSeq("`694`",_34e);
this.LogSeq("`872`",_34e);
if(_34a.GetRandomizeChildren()===true){
this.LogSeqSimple("Randomizing the order of the children of \""+_34a+"\".",_34d);
this.LogSeq("`581`",_34e);
_34f=Sequencer_RandomizeArray(_34a.GetAvailableChildren());
_34a.SetAvailableChildren(_34f);
_34a.SetRandomizedChildren(true);
if(_34b===true){
Control.RedrawChildren(_34a);
}
}
this.LogSeq("`1171`",_34e);
break;
default:
this.LogSeq("`828`",_34e);
break;
}
this.LogSeqReturn("",_34e);
return;
}
function Sequencer_RandomizeArray(ary){
var _352=ary.length;
var orig;
var swap;
for(var i=0;i<_352;i++){
var _356=Math.floor(Math.random()*_352);
orig=ary[i];
swap=ary[_356];
ary[i]=swap;
ary[_356]=orig;
}
return ary;
}
function Sequencer_ResumeAllSequencingRequestProcess(_357,_358){
Debug.AssertError("Calling log not passed.",(_357===undefined||_357===null));
Debug.AssertError("Simple calling log not passed.",(_358===undefined||_358===null));
var _359=this.LogSeqAudit("`1093`",_357);
var _35a;
this.LogSeq("`494`",_359);
if(this.IsCurrentActivityDefined(_359)){
this.LogSeq("`401`",_359);
_35a=new Sequencer_ResumeAllSequencingRequestProcessResult(null,"SB.2.6-1",IntegrationImplementation.GetString("A 'Resume All' sequencing request cannot be processed while there is a current activity defined."));
this.LogSeqReturn(_35a,_359);
return _35a;
}
this.LogSeq("`400`",_359);
if(!this.IsSuspendedActivityDefined(_359)){
this.LogSeq("`398`",_359);
_35a=new Sequencer_ResumeAllSequencingRequestProcessResult(null,"SB.2.6-2",IntegrationImplementation.GetString("There is no suspended activity to resume."));
this.LogSeqReturn(_35a,_359);
return _35a;
}
this.LogSeq("`57`",_359);
var _35b=this.GetSuspendedActivity(_359);
this.LogSeqSimple("The previously suspended activity that should be resumed is \""+_35b+"\".",_358);
_35a=new Sequencer_ResumeAllSequencingRequestProcessResult(_35b,null,"");
this.LogSeqReturn(_35a,_359);
return _35a;
}
function Sequencer_ResumeAllSequencingRequestProcessResult(_35c,_35d,_35e){
this.DeliveryRequest=_35c;
this.Exception=_35d;
this.ExceptionText=_35e;
}
Sequencer_ResumeAllSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_RetrySequencingRequestProcess(_35f,_360){
Debug.AssertError("Calling log not passed.",(_35f===undefined||_35f===null));
Debug.AssertError("Simple calling log not passed.",(_360===undefined||_360===null));
var _361=this.LogSeqAudit("`1192`",_35f);
var _362;
var _363;
this.LogSeq("`489`",_361);
if(!this.IsCurrentActivityDefined(_361)){
this.LogSeq("`433`",_361);
_362=new Sequencer_RetrySequencingRequestProcessResult(null,"SB.2.10-1",IntegrationImplementation.GetString("You cannot use 'Resume All' while the current item is open."));
this.LogSeqReturn(_362,_361);
return _362;
}
var _364=this.GetCurrentActivity();
this.LogSeq("`101`",_361);
if(_364.IsActive()||_364.IsSuspended()){
this.LogSeq("`435`",_361);
_362=new Sequencer_RetrySequencingRequestProcessResult(null,"SB.2.10-2",IntegrationImplementation.GetString("A 'Retry' sequencing request cannot be processed while there is an active or suspended activity."));
this.LogSeqReturn(_362,_361);
return _362;
}
this.LogSeq("`968`",_361);
if(!_364.IsALeaf()){
_360=this.LogSeqSimpleAudit("Processing a Retry of \""+_364+"\".",_360);
this.LogSeq("`512`"+_364+").",_361);
this.LogSeqSimple("Resetting sequencing activity tree data (but not CMI runtime data) for \""+_364+"\" and all its descendents.",_360);
_364.InitializeForNewAttempt(true,true);
this.LogSeq("`821`"+Control.Package.ObjectivesGlobalToSystem+"`928`"+_364.IsTheRoot()+").",_361);
if(Control.Package.ObjectivesGlobalToSystem===false&&_364.IsTheRoot()===true){
this.LogSeq("`1175`",_361);
this.LogSeqSimple("A retry on the root activity when objectives global to system is false will also reset all global objectives.",_360);
this.ResetGlobalObjectives();
}
this.LogSeq("`370`",_361);
flowSubProcessResult=this.FlowSubprocess(_364,FLOW_DIRECTION_FORWARD,true,_361,_360);
this.LogSeq("`942`",_361);
if(flowSubProcessResult.Deliverable===false){
this.LogSeq("`414`",_361);
_362=new Sequencer_RetrySequencingRequestProcessResult(null,"SB.2.10-3",IntegrationImplementation.GetString("You cannot 'Retry' this item because: {1}",_364.GetTitle(),flowSubProcessResult.ExceptionText));
this.LogSeqReturn(_362,_361);
return _362;
}else{
this.LogSeq("`1636`",_361);
this.LogSeq("`319`",_361);
_362=new Sequencer_RetrySequencingRequestProcessResult(flowSubProcessResult.IdentifiedActivity,null,"");
this.LogSeqReturn(_362,_361);
return _362;
}
}else{
this.LogSeq("`1667`",_361);
this.LogSeq("`484`",_361);
_362=new Sequencer_RetrySequencingRequestProcessResult(_364,null,"");
this.LogSeqReturn(_362,_361);
return _362;
}
}
function Sequencer_RetrySequencingRequestProcessResult(_365,_366,_367){
this.DeliveryRequest=_365;
this.Exception=_366;
this.ExceptionText=_367;
}
Sequencer_RetrySequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_RollupRuleCheckSubprocess(_368,_369,_36a,_36b){
Debug.AssertError("Calling log not passed.",(_36a===undefined||_36a===null));
Debug.AssertError("Simple calling log not passed.",(_36b===undefined||_36b===null));
var _36c=this.LogSeqAudit("`1255`"+_368+", "+_369+")",_36a);
var _36d;
var _36e;
var _36f;
var _370;
var _371=0;
var _372=0;
var _373=0;
this.LogSeq("`329`",_36c);
var _374=Sequencer_GetApplicableSetofRollupRules(_368,_369);
if(_374.length>0){
this.LogSeq("`214`",_36c);
_36b=this.LogSeqSimpleAudit("Evaluating rollup rules for "+_369+" on \""+_368+"\".",_36b);
_36d=_368.GetAvailableChildren();
this.LogSeq("`1124`",_36c);
for(var i=0;i<_374.length;i++){
this.LogSeq("`1345`"+_374[i].toString(),_36c);
var _376=this.LogSeqSimpleAudit("Evaluating Rule #"+(i+1)+": \""+_374[i].toString()+"\".",_36b);
this.LogSeq("`745`",_36c);
contributingChldren=new Array();
_371=0;
_372=0;
_373=0;
this.LogSeq("`1111`",_36c);
for(var j=0;j<_36d.length;j++){
this.LogSeq("`974`",_36c);
if(_36d[j].IsTracked()===true){
this.LogSeq("`237`",_36c);
_36e=this.CheckChildForRollupSubprocess(_36d[j],_374[i].Action,_36c,_376);
this.LogSeq("`744`",_36c);
if(_36e===true){
this.LogSeq("`156`",_36c);
_36f=this.EvaluateRollupConditionsSubprocess(_36d[j],_374[i],_36c,_376);
this.LogSeq("`310`",_36c);
if(_36f==RESULT_UNKNOWN){
this.LogSeq("`719`",_36c);
_373++;
if(_374[i].ChildActivitySet==CHILD_ACTIVITY_SET_ALL||_374[i].ChildActivitySet==CHILD_ACTIVITY_SET_NONE){
j=_36d.length;
}
}else{
this.LogSeq("`1495`",_36c);
this.LogSeq("`686`",_36c);
if(_36f===true){
this.LogSeq("`766`",_36c);
_371++;
if(_374[i].ChildActivitySet==CHILD_ACTIVITY_SET_ANY||_374[i].ChildActivitySet==CHILD_ACTIVITY_SET_NONE){
j=_36d.length;
}
}else{
this.LogSeq("`1494`",_36c);
this.LogSeq("`754`",_36c);
_372++;
if(_374[i].ChildActivitySet==CHILD_ACTIVITY_SET_ALL){
j=_36d.length;
}
}
}
}
}else{
this.LogSeqSimple("\""+_36d[j]+"\" is not tracked so it is not included in the rollup.",_376);
}
}
this.LogSeq("`186`",_36c);
_370=false;
this.LogSeq("`902`",_36c);
if(_36d.length==0){
this.LogSeq("`550`",_36c);
}else{
switch(_374[i].ChildActivitySet){
case CHILD_ACTIVITY_SET_ALL:
this.LogSeq("`930`",_36c);
this.LogSeq("`538`",_36c);
if(_372===0&&_373===0){
this.LogSeq("`1142`",_36c);
_370=true;
}
break;
case CHILD_ACTIVITY_SET_ANY:
this.LogSeq("`937`",_36c);
this.LogSeq("`695`",_36c);
if(_371>0){
this.LogSeq("`1123`",_36c);
_370=true;
}
break;
case CHILD_ACTIVITY_SET_NONE:
this.LogSeq("`925`",_36c);
this.LogSeq("`548`",_36c);
if(_371===0&&_373===0){
this.LogSeq("`1138`",_36c);
_370=true;
}
break;
case CHILD_ACTIVITY_SET_AT_LEAST_COUNT:
this.LogSeq("`817`",_36c);
this.LogSeq("`269`",_36c);
if(_371>=_374[i].MinimumCount){
this.LogSeq("`1122`",_36c);
_370=true;
}
break;
case CHILD_ACTIVITY_SET_AT_LEAST_PERCENT:
this.LogSeq("`784`",_36c);
this.LogSeq("`120`",_36c);
var _378=(_371/(_371+_372+_373));
if(_378>=_374[i].MinimumPercent){
this.LogSeq("`1135`",_36c);
_370=true;
}
break;
default:
break;
}
}
this.LogSeq("`1108`",_36c);
if(_370===true){
this.LogSeq("`262`",_36c);
this.LogSeqReturn("`1732`",_36c);
this.LogSeqSimpleReturn("Rule #"+(i+1)+" fires. Status will change to "+_369+".",_36b);
this.LogSeqSimpleReturn("Rule fires and status will change.",_376);
return true;
}else{
this.LogSeqSimpleReturn("Rule does not fire.",_376);
}
}
}
this.LogSeq("`424`",_36c);
this.LogSeqSimpleReturn("No rule fired, status will not change.",_36b);
this.LogSeqReturn("`1728`",_36c);
return false;
}
function Sequencer_GetApplicableSetofRollupRules(_379,_37a){
var _37b=new Array();
var _37c=_379.GetRollupRules();
for(var i=0;i<_37c.length;i++){
switch(_37a){
case RULE_SET_SATISFIED:
if(_37c[i].Action==ROLLUP_RULE_ACTION_SATISFIED){
_37b[_37b.length]=_37c[i];
}
break;
case RULE_SET_NOT_SATISFIED:
if(_37c[i].Action==ROLLUP_RULE_ACTION_NOT_SATISFIED){
_37b[_37b.length]=_37c[i];
}
break;
case RULE_SET_COMPLETED:
if(_37c[i].Action==ROLLUP_RULE_ACTION_COMPLETED){
_37b[_37b.length]=_37c[i];
}
break;
case RULE_SET_INCOMPLETE:
if(_37c[i].Action==ROLLUP_RULE_ACTION_INCOMPLETE){
_37b[_37b.length]=_37c[i];
}
break;
}
}
return _37b;
}
function Sequencer_SelectChildrenProcess(_37e,_37f,_380){
Debug.AssertError("Calling log not passed.",(_37f===undefined||_37f===null));
Debug.AssertError("Simple calling log not passed.",(_380===undefined||_380===null));
var _381=this.LogSeqAudit("`1377`"+_37e+")",_37f);
this.LogSeq("`561`",_381);
if(_37e.IsALeaf()){
this.LogSeq("`1239`",_381);
this.LogSeqReturn("",_381);
return;
}
this.LogSeq("`171`",_381);
if(_37e.IsActive()===true||_37e.IsSuspended()===true){
this.LogSeq("`1253`",_381);
this.LogSeqReturn("",_381);
return;
}
var _382=_37e.GetSelectionTiming();
switch(_382){
case TIMING_NEVER:
this.LogSeq("`886`",_381);
this.LogSeq("`1246`",_381);
break;
case TIMING_ONCE:
this.LogSeq("`893`",_381);
this.LogSeq("`811`",_381);
if(_37e.GetSelectedChildren()===false){
this.LogSeq("`438`",_381);
if(_37e.GetActivityProgressStatus()===false){
this.LogSeq("`768`",_381);
if(_37e.GetSelectionCountStatus()===true){
this.LogSeq("`875`",_381);
var _383=new Array();
var _384=_37e.GetChildren();
var _385=_37e.GetSelectionCount();
_380=this.LogSeqSimpleAudit("Randomly selecting "+_37e.GetSelectionCount()+" children of \""+_37e+"\" to be included.",_380);
if(_385<_384.length){
var _386=Sequencer_GetUniqueRandomNumbersBetweenTwoValues(_385,0,_384.length-1);
this.LogSeq("`867`",_381);
for(var i=0;i<_386.length;i++){
this.LogSeq("`534`",_381);
this.LogSeq("`339`",_381);
this.LogSeqSimple("Selected \""+_384[_386[i]]+"\".",_380);
_383[i]=_384[_386[i]];
}
}else{
_383=_384;
}
this.LogSeq("`769`",_381);
_37e.SetAvailableChildren(_383);
_37e.SetSelectedChildren(true);
}
}
}
this.LogSeq("`1240`",_381);
break;
case TIMING_ON_EACH_NEW_ATTEMPT:
this.LogSeq("`735`",_381);
this.LogSeq("`894`",_381);
this.LogSeqSimple("Selection timing of on each new attempt is currently not supported in SCORM.",_380);
break;
default:
this.LogSeq("`836`",_381);
break;
}
this.LogSeqReturn("",_381);
}
function Sequencer_GetUniqueRandomNumbersBetweenTwoValues(_388,_389,_38a){
if(_388===null||_388===undefined||_388<_389){
_388=_389;
}
if(_388>_38a){
_388=_38a;
}
var _38b=new Array(_388);
var _38c;
var _38d;
for(var i=0;i<_388;i++){
_38d=true;
while(_38d){
_38c=Sequencer_GetRandomNumberWithinRange(_389,_38a);
_38d=Sequencer_IsNumberAlreadyInArray(_38c,_38b);
}
_38b[i]=_38c;
}
_38b.sort();
return _38b;
}
function Sequencer_GetRandomNumberWithinRange(_38f,_390){
var diff=_390-_38f;
return Math.floor(Math.random()*(diff+_38f+1));
}
function Sequencer_IsNumberAlreadyInArray(_392,_393){
for(var i=0;i<_393.length;i++){
if(_393[i]==_392){
return true;
}
}
return false;
}
function Sequencer_SequencingExitActionRulesSubprocess(_395,_396){
Debug.AssertError("Calling log not passed.",(_395===undefined||_395===null));
Debug.AssertError("Simple calling log not passed.",(_396===undefined||_396===null));
var _397=this.LogSeqAudit("`1061`",_395);
this.LogSeq("`248`",_397);
var _398=this.GetCurrentActivity();
var _399=this.Activities.GetParentActivity(_398);
var _39a;
if(_399!==null){
_39a=this.GetActivityPath(_399,true);
}else{
_39a=this.GetActivityPath(_398,true);
}
this.LogSeq("`1217`",_397);
var _39b=null;
var _39c=null;
this.LogSeq("`306`",_397);
for(var i=(_39a.length-1);i>=0;i--){
this.LogSeq("`549`",_397);
_39c=this.SequencingRulesCheckProcess(_39a[i],RULE_SET_EXIT,_397,_396);
this.LogSeq("`734`",_397);
if(_39c!==null){
this.LogSeq("`413`",_397);
_39b=_39a[i];
this.LogSeq("`1501`",_397);
break;
}
}
this.LogSeq("`1187`",_397);
if(_39b!==null){
this.LogSeqSimple("Exiting "+_39b+" because of its EXIT sequencing rule.",_396);
this.LogSeq("`353`",_397);
this.TerminateDescendentAttemptsProcess(_39b,_397,_396);
this.LogSeq("`464`",_397);
this.EndAttemptProcess(_39b,false,_397,false,_396);
this.LogSeq("`344`",_397);
this.SetCurrentActivity(_39b,_397,_396);
}
this.LogSeq("`957`",_397);
this.LogSeqReturn("",_397);
return;
}
function Sequencer_SequencingPostConditionRulesSubprocess(_39e,_39f){
Debug.AssertError("Calling log not passed.",(_39e===undefined||_39e===null));
Debug.AssertError("Simple calling log not passed.",(_39f===undefined||_39f===null));
var _3a0=this.LogSeqAudit("`1006`",_39e);
var _3a1;
this.LogSeq("`336`",_3a0);
var _3a2=this.GetCurrentActivity();
if(_3a2.IsSuspended()){
this.LogSeq("`888`",_3a0);
_3a1=new Sequencer_SequencingPostConditionRulesSubprocessResult(null,null);
this.LogSeqReturn(_3a1,_3a0);
return _3a1;
}
this.LogSeq("`191`",_3a0);
var _3a3=this.SequencingRulesCheckProcess(_3a2,RULE_SET_POST_CONDITION,_3a0,_39f);
this.LogSeq("`764`",_3a0);
if(_3a3!==null){
this.LogSeq("`588`",_3a0);
if(_3a3==SEQUENCING_RULE_ACTION_RETRY||_3a3==SEQUENCING_RULE_ACTION_CONTINUE||_3a3==SEQUENCING_RULE_ACTION_PREVIOUS){
this.LogSeq("`47`",_3a0);
_3a1=new Sequencer_SequencingPostConditionRulesSubprocessResult(this.TranslateSequencingRuleActionIntoSequencingRequest(_3a3),null);
this.LogSeqReturn(_3a1,_3a0);
return _3a1;
}
this.LogSeq("`621`",_3a0);
if(_3a3==SEQUENCING_RULE_ACTION_EXIT_PARENT||_3a3==SEQUENCING_RULE_ACTION_EXIT_ALL){
this.LogSeq("`83`",_3a0);
_3a1=new Sequencer_SequencingPostConditionRulesSubprocessResult(null,this.TranslateSequencingRuleActionIntoTerminationRequest(_3a3));
this.LogSeqReturn(_3a1,_3a0);
return _3a1;
}
this.LogSeq("`750`",_3a0);
if(_3a3==SEQUENCING_RULE_ACTION_RETRY_ALL){
this.LogSeq("`27`",_3a0);
_3a1=new Sequencer_SequencingPostConditionRulesSubprocessResult(SEQUENCING_REQUEST_RETRY,TERMINATION_REQUEST_EXIT_ALL);
this.LogSeqReturn(_3a1,_3a0);
return _3a1;
}
}
this.LogSeq("`481`",_3a0);
_3a1=new Sequencer_SequencingPostConditionRulesSubprocessResult(null,null);
this.LogSeqReturn(_3a1,_3a0);
return _3a1;
}
function Sequencer_SequencingPostConditionRulesSubprocessResult(_3a4,_3a5){
this.TerminationRequest=_3a5;
this.SequencingRequest=_3a4;
}
Sequencer_SequencingPostConditionRulesSubprocessResult.prototype.toString=function(){
return "TerminationRequest="+this.TerminationRequest+", SequencingRequest="+this.SequencingRequest;
};
function Sequencer_SequencingRequestProcess(_3a6,_3a7,_3a8,_3a9){
Debug.AssertError("Calling log not passed.",(_3a8===undefined||_3a8===null));
Debug.AssertError("Simple calling log not passed.",(_3a9===undefined||_3a9===null));
var _3aa=this.LogSeqAudit("`1287`"+_3a6+", "+_3a7+")",_3a8);
var _3ab;
_3a9=this.LogSeqSimpleAudit("Identifying the activity to be delivered in response to a sequencing request of '"+_3a6+"'.",_3a9);
switch(_3a6){
case SEQUENCING_REQUEST_START:
this.LogSeq("`1112`",_3aa);
this.LogSeq("`946`",_3aa);
var _3ac=this.StartSequencingRequestProcess(_3aa,_3a9);
this.LogSeq("`692`",_3aa);
if(_3ac.Exception!==null){
this.LogSeq("`81`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_3ac.Exception,_3ac.ExceptionText,false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}else{
this.LogSeq("`1631`",_3aa);
this.LogSeq("`42`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(_3a6,_3ac.DeliveryRequest,_3ac.EndSequencingSession,null,"",false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}
break;
case SEQUENCING_REQUEST_RESUME_ALL:
this.LogSeq("`1020`",_3aa);
this.LogSeq("`878`",_3aa);
var _3ad=this.ResumeAllSequencingRequestProcess(_3aa,_3a9);
this.LogSeq("`643`",_3aa);
if(_3ad.Exception!==null){
this.LogSeq("`71`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_3ad.Exception,_3ad.ExceptionText,false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}else{
this.LogSeq("`1625`",_3aa);
this.LogSeq("`109`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(_3a6,_3ad.DeliveryRequest,null,null,"",false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}
break;
case SEQUENCING_REQUEST_EXIT:
this.LogSeq("`1148`",_3aa);
this.LogSeq("`955`",_3aa);
var _3ae=this.ExitSequencingRequestProcess(_3aa,_3a9);
this.LogSeq("`704`",_3aa);
if(_3ae.Exception!==null){
this.LogSeq("`85`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_3ae.Exception,_3ae.ExceptionText,false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}else{
this.LogSeq("`1626`",_3aa);
this.LogSeq("`124`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(_3a6,null,_3ae.EndSequencingSession,null,"",false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}
break;
case SEQUENCING_REQUEST_RETRY:
this.LogSeq("`1118`",_3aa);
this.LogSeq("`949`",_3aa);
var _3af=this.RetrySequencingRequestProcess(_3aa,_3a9);
if(_3af.Exception!==null){
this.LogSeq("`80`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_3af.Exception,_3af.ExceptionText,false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}else{
this.LogSeq("`1619`",_3aa);
this.LogSeq("`114`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(_3a6,_3af.DeliveryRequest,null,null,"",false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}
break;
case SEQUENCING_REQUEST_CONTINUE:
this.LogSeq("`1068`",_3aa);
this.LogSeq("`898`",_3aa);
var _3b0=this.ContinueSequencingRequestProcess(_3aa,_3a9);
this.LogSeq("`661`",_3aa);
if(_3b0.Exception!==null){
this.LogSeq("`28`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,_3b0.EndSequencingSession,_3b0.Exception,_3b0.ExceptionText,false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}else{
this.LogSeq("`1615`",_3aa);
this.LogSeq("`36`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(_3a6,_3b0.DeliveryRequest,_3b0.EndSequencingSession,null,"",false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}
break;
case SEQUENCING_REQUEST_PREVIOUS:
this.LogSeq("`1063`",_3aa);
this.LogSeq("`900`",_3aa);
var _3b1=this.PreviousSequencingRequestProcess(_3aa,_3a9);
this.LogSeq("`666`",_3aa);
if(_3b1.Exception!==null){
this.LogSeq("`74`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,_3b1.EndSequencingSession,_3b1.Exception,_3b1.ExceptionText,false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}else{
this.LogSeq("`1637`",_3aa);
this.LogSeq("`111`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(_3a6,_3b1.DeliveryRequest,_3b1.EndSequencingSession,null,"",false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}
break;
case SEQUENCING_REQUEST_CHOICE:
this.LogSeq("`1105`",_3aa);
this.LogSeq("`934`",_3aa);
var _3b2=this.ChoiceSequencingRequestProcess(_3a7,_3aa,_3a9);
this.LogSeq("`679`",_3aa);
if(_3b2.Exception!==null){
this.LogSeq("`78`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_3b2.Exception,_3b2.ExceptionText,_3b2.Hidden);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}else{
this.LogSeq("`1630`",_3aa);
this.LogSeq("`119`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(_3a6,_3b2.DeliveryRequest,null,null,"",_3b2.Hidden);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}
break;
case SEQUENCING_REQUEST_JUMP:
this.LogSeq("`1143`",_3aa);
this.LogSeq("`952`",_3aa);
var _3b3=this.JumpSequencingRequestProcess(_3a7,_3aa,_3a9);
this.LogSeq("`703`",_3aa);
if(_3b3.Exception!==null){
this.LogSeq("`84`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_3b3.Exception,_3b3.ExceptionText,false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}else{
this.LogSeq("`1618`",_3aa);
this.LogSeq("`123`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(_3a6,_3b3.DeliveryRequest,null,null,"",false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}
break;
}
this.LogSeq("`148`",_3aa);
_3ab=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,"SB.2.12-1","The sequencing request ("+_3a6+") is not recognized.",false);
this.LogSeqReturn(_3ab,_3aa);
return _3ab;
}
function Sequencer_SequencingRequestProcessResult(_3b4,_3b5,_3b6,_3b7,_3b8,_3b9){
Debug.AssertError("undefined hidden value",(_3b9===undefined));
Debug.AssertError("Invalid endSequencingSession ("+_3b6+") passed to SequencingRequestProcessResult.",(_3b6!=null&&_3b6!=true&&_3b6!=false));
this.SequencingRequest=_3b4;
this.DeliveryRequest=_3b5;
this.EndSequencingSession=_3b6;
this.Exception=_3b7;
this.ExceptionText=_3b8;
this.Hidden=_3b9;
}
Sequencer_SequencingRequestProcessResult.prototype.toString=function(){
return "SequencingRequest="+this.SequencingRequest+", DeliveryRequest="+this.DeliveryRequest+", EndSequencingSession="+this.EndSequencingSession+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", Hidden="+this.Hidden;
};
function Sequencer_SequencingRulesCheckProcess(_3ba,_3bb,_3bc,_3bd){
Debug.AssertError("Calling log not passed.",(_3bc===undefined||_3bc===null));
Debug.AssertError("Simple calling log not passed.",(_3bd===undefined||_3bd===null));
var _3be=this.LogSeqAudit("`1268`"+_3ba+", "+_3bb+")",_3bc);
var _3bf;
var _3c0=Sequencer_GetApplicableSetofSequencingRules(_3ba,_3bb);
this.LogSeq("`301`",_3be);
if(_3c0.length>0){
var _3c1=this.LogSeqSimpleAudit("\""+_3ba+"\" has "+_3bb+" sequencing rules to evaulate. Checking to see if any apply.",_3bd);
this.LogSeq("`190`",_3be);
this.LogSeq("`1189`",_3be);
for(var i=0;i<_3c0.length;i++){
this.LogSeq("`415`",_3be);
var _3c3=this.LogSeqSimpleAudit("Evaluating Rule #"+(i+1)+": \""+_3c0[i].toString()+"\".",_3c1);
_3bf=this.SequencingRulesCheckSubprocess(_3ba,_3c0[i],_3be,_3c3);
this.LogSeq("`791`",_3be);
if(_3bf===true){
this.LogSeq("`211`",_3be);
this.LogSeqReturn(_3c0[i].Action,_3be);
this.LogSeqSimpleReturn("Rule fired, performing action: "+_3c0[i].Action+".",_3c1);
return _3c0[i].Action;
}
}
}
this.LogSeq("`457`",_3be);
this.LogSeqReturn("`1731`",_3be);
return null;
}
function Sequencer_GetApplicableSetofSequencingRules(_3c4,_3c5){
var _3c6=new Array();
if(_3c5==RULE_SET_POST_CONDITION){
_3c6=_3c4.GetPostConditionRules();
}else{
if(_3c5==RULE_SET_EXIT){
_3c6=_3c4.GetExitRules();
}else{
var _3c7=_3c4.GetPreConditionRules();
for(var i=0;i<_3c7.length;i++){
switch(_3c5){
case RULE_SET_HIDE_FROM_CHOICE:
if(_3c7[i].Action==SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE){
_3c6[_3c6.length]=_3c7[i];
}
break;
case RULE_SET_STOP_FORWARD_TRAVERSAL:
if(_3c7[i].Action==SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL){
_3c6[_3c6.length]=_3c7[i];
}
break;
case RULE_SET_DISABLED:
if(_3c7[i].Action==SEQUENCING_RULE_ACTION_DISABLED){
_3c6[_3c6.length]=_3c7[i];
}
break;
case RULE_SET_SKIPPED:
if(_3c7[i].Action==SEQUENCING_RULE_ACTION_SKIP){
_3c6[_3c6.length]=_3c7[i];
}
break;
default:
Debug.AssertError("ERROR - invalid sequencing rule set - "+_3c5);
return null;
}
}
}
}
return _3c6;
}
function Sequencer_SequencingRulesCheckSubprocess(_3c9,rule,_3cb,_3cc){
Debug.AssertError("Calling log not passed.",(_3cb===undefined||_3cb===null));
Debug.AssertError("Simple calling log not passed.",(_3cc===undefined||_3cc===null));
var _3cd=this.LogSeqAudit("`1166`"+_3c9+", "+rule+")",_3cb);
this.LogSeq("`324`",_3cd);
var _3ce=new Array();
var _3cf;
var _3d0;
var i;
this.LogSeq("`733`",_3cd);
for(i=0;i<rule.RuleConditions.length;i++){
this.LogSeq("`100`",_3cd);
_3cf=this.EvaluateSequencingRuleCondition(_3c9,rule.RuleConditions[i],_3cd,_3cc);
this.LogSeq("`697`",_3cd);
if(rule.RuleConditions[i].Operator==RULE_CONDITION_OPERATOR_NOT){
this.LogSeq("`658`",_3cd);
if(_3cf!="unknown"){
_3cf=(!_3cf);
}
}
this.LogSeq("`284`",_3cd);
_3ce[_3ce.length]=_3cf;
}
this.LogSeq("`372`",_3cd);
if(_3ce.length===0){
this.LogSeq("`611`",_3cd);
this.LogSeqReturn(RESULT_UNKNOWN,_3cd);
return RESULT_UNKNOWN;
}
this.LogSeq("`62`",_3cd);
if(rule.ConditionCombination==RULE_CONDITION_COMBINATION_ANY){
_3d0=false;
for(i=0;i<_3ce.length;i++){
_3d0=Sequencer_LogicalOR(_3d0,_3ce[i]);
}
if(rule.RuleConditions.length>1){
this.LogSeqSimple("Are ANY of the conditions true="+_3d0,_3cc);
}
}else{
_3d0=true;
for(i=0;i<_3ce.length;i++){
_3d0=Sequencer_LogicalAND(_3d0,_3ce[i]);
}
if(rule.RuleConditions.length>1){
this.LogSeqSimple("Are ALL of the conditions true="+_3d0,_3cc);
}
}
this.LogSeq("`618`",_3cd);
this.LogSeqReturn(_3d0,_3cd);
if(_3d0===true){
this.LogSeqSimpleReturn("True, rule will fire",_3cc);
}else{
if(_3d0===false){
this.LogSeqSimpleReturn("False, rule will not fire",_3cc);
}else{
this.LogSeqSimpleReturn("Unknown, rule will not fire",_3cc);
}
}
return _3d0;
}
function Sequencer_EvaluateSequencingRuleCondition(_3d2,_3d3,_3d4,_3d5){
Debug.AssertError("Calling log not passed.",(_3d4===undefined||_3d4===null));
Debug.AssertError("Simple calling log not passed.",(_3d5===undefined||_3d5===null));
var _3d6=this.LogSeqAudit("`1305`"+_3d2+", "+_3d3+")",_3d4);
var _3d7=null;
switch(_3d3.Condition){
case SEQUENCING_RULE_CONDITION_SATISFIED:
_3d7=_3d2.IsSatisfied(_3d3.ReferencedObjective,true);
this.LogSeqSimple(_3d2+"\" satisfied = "+_3d7,_3d5);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN:
_3d7=_3d2.IsObjectiveStatusKnown(_3d3.ReferencedObjective,true);
this.LogSeqSimple("\""+_3d2+"\" objective status known = "+_3d7,_3d5);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN:
_3d7=_3d2.IsObjectiveMeasureKnown(_3d3.ReferencedObjective,true);
this.LogSeqSimple("\""+_3d2+"\" objective measure known = "+_3d7,_3d5);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_GREATER_THAN:
_3d7=_3d2.IsObjectiveMeasureGreaterThan(_3d3.ReferencedObjective,_3d3.MeasureThreshold,true);
this.LogSeqSimple("\""+_3d2+"\" objective measure greater than "+_3d3.MeasureThreshold+" = "+_3d7,_3d5);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_LESS_THAN:
_3d7=_3d2.IsObjectiveMeasureLessThan(_3d3.ReferencedObjective,_3d3.MeasureThreshold,true);
this.LogSeqSimple("\""+_3d2+"\" objective measure less than "+_3d3.MeasureThreshold+" = "+_3d7,_3d5);
break;
case SEQUENCING_RULE_CONDITION_COMPLETED:
_3d7=_3d2.IsCompleted(_3d3.ReferencedObjective,true);
this.LogSeqSimple("\""+_3d2+"\" completed = "+_3d7,_3d5);
break;
case SEQUENCING_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN:
_3d7=_3d2.IsActivityProgressKnown(_3d3.ReferencedObjective,true);
this.LogSeqSimple("\""+_3d2+"\" activity progress known = "+_3d7,_3d5);
break;
case SEQUENCING_RULE_CONDITION_ATTEMPTED:
_3d7=_3d2.IsAttempted();
this.LogSeqSimple("\""+_3d2+"\" attempted = "+_3d7,_3d5);
break;
case SEQUENCING_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED:
_3d7=_3d2.IsAttemptLimitExceeded();
this.LogSeqSimple("\""+_3d2+"\" attempt limit exceeded = "+_3d7,_3d5);
break;
case SEQUENCING_RULE_CONDITION_ALWAYS:
_3d7=true;
this.LogSeqSimple("\""+_3d2+"\" 'always' will always return true.",_3d5);
break;
default:
Debug.AssertError("ERROR - Encountered unsupported rule condition - "+_3d3);
_3d7=RESULT_UNKNOWN;
break;
}
_3d6.setReturn(_3d7+"",_3d6);
return _3d7;
}
function Sequencer_LogicalOR(_3d8,_3d9){
if(_3d8==RESULT_UNKNOWN){
if(_3d9===true){
return true;
}else{
return RESULT_UNKNOWN;
}
}else{
if(_3d9==RESULT_UNKNOWN){
if(_3d8===true){
return true;
}else{
return RESULT_UNKNOWN;
}
}else{
return (_3d8||_3d9);
}
}
}
function Sequencer_LogicalAND(_3da,_3db){
if(_3da==RESULT_UNKNOWN){
if(_3db===false){
return false;
}else{
return RESULT_UNKNOWN;
}
}else{
if(_3db==RESULT_UNKNOWN){
if(_3da===false){
return false;
}else{
return RESULT_UNKNOWN;
}
}else{
return (_3da&&_3db);
}
}
}
function Sequencer_StartSequencingRequestProcess(_3dc,_3dd){
Debug.AssertError("Calling log not passed.",(_3dc===undefined||_3dc===null));
Debug.AssertError("Simple calling log not passed.",(_3dd===undefined||_3dd===null));
var _3de=this.LogSeqAudit("`1208`",_3dc);
var _3df;
this.LogSeq("`500`",_3de);
if(this.IsCurrentActivityDefined(_3de)){
this.LogSeq("`456`",_3de);
_3df=new Sequencer_StartSequencingRequestProcessResult(null,"SB.2.5-1",IntegrationImplementation.GetString("You cannot 'Start' an item that is already open."),false);
this.LogSeqReturn(_3df,_3de);
return _3df;
}
var _3e0=this.GetRootActivity(_3de);
this.LogSeq("`317`",_3de);
if(_3e0.IsALeaf()){
this.LogSeq("`239`",_3de);
_3df=new Sequencer_StartSequencingRequestProcessResult(_3e0,null,"",false);
this.LogSeqReturn(_3df,_3de);
return _3df;
}else{
if(Control.Package.Properties.AlwaysFlowToFirstSco===true){
this.LogSeqSimple("Ignoring all sequencing rules and selecting the first SCO because the package property setting AlwaysFlowToFristSco is true.",_3dd);
this.LogSeq("`315`",_3de);
this.LogSeq("`1033`",_3de);
var _3e1=this.GetOrderedListOfActivities(false,_3de);
this.LogSeq("`922`",_3de);
for(var _3e2 in _3e1){
if(_3e1[_3e2].IsDeliverable()===true){
_3df=new Sequencer_StartSequencingRequestProcessResult(_3e1[_3e2],null,"",false);
this.LogSeqReturn(_3df,_3de);
return _3df;
}
}
_3df=new Sequencer_StartSequencingRequestProcessResult(null,"SB.2.5-2.5","There are no deliverable activities in this course.",false);
this.LogSeqReturn(_3df,_3de);
return _3df;
}else{
this.LogSeq("`1668`",_3de);
this.LogSeq("`163`",_3de);
var _3e3=this.FlowSubprocess(_3e0,FLOW_DIRECTION_FORWARD,true,_3de,_3dd);
this.LogSeq("`963`",_3de);
if(_3e3.Deliverable===false){
this.LogSeq("`65`",_3de);
_3df=new Sequencer_StartSequencingRequestProcessResult(null,_3e3.Exception,_3e3.ExceptionText,_3e3.EndSequencingSession);
this.LogSeqReturn(_3df,_3de);
return _3df;
}else{
this.LogSeq("`1623`",_3de);
this.LogSeq("`325`",_3de);
_3df=new Sequencer_StartSequencingRequestProcessResult(_3e3.IdentifiedActivity,null,"",false);
this.LogSeqReturn(_3df,_3de);
return _3df;
}
}
}
}
function Sequencer_StartSequencingRequestProcessResult(_3e4,_3e5,_3e6,_3e7){
Debug.AssertError("Invalid endSequencingSession ("+_3e7+") passed to StartSequencingRequestProcessResult.",(_3e7!=true&&_3e7!=false));
this.DeliveryRequest=_3e4;
this.Exception=_3e5;
this.ExceptionText=_3e6;
this.EndSequencingSession=_3e7;
}
Sequencer_StartSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", EndSequencingSession="+this.EndSequencingSession;
};
function Sequencer_TerminateDescendentAttemptsProcess(_3e8,_3e9,_3ea){
Debug.AssertError("Calling log not passed.",(_3e9===undefined||_3e9===null));
Debug.AssertError("Simple calling log not passed.",(_3ea===undefined||_3ea===null));
var _3eb=this.LogSeqAudit("`1106`"+_3e8+")",_3e9);
_3ea=this.LogSeqSimpleAudit("Ending all attempts underneath \""+_3e8+"\".",_3ea);
this.LogSeq("`682`"+this.GetCurrentActivity()+"`1376`"+_3e8+")",_3eb);
var _3ec=this.FindCommonAncestor(_3e8,this.GetCurrentActivity(),_3eb);
this.LogSeq("`149`"+_3ec+"`972`",_3eb);
var _3ed=this.GetPathToAncestorExclusive(this.GetCurrentActivity(),_3ec,false);
var _3ee=new Array();
this.LogSeq("`520`",_3eb);
if(_3ed.length>0){
this.LogSeq("`1047`",_3eb);
for(var i=0;i<_3ed.length;i++){
this.LogSeq("`471`"+_3ed[i].LearningObject.ItemIdentifier,_3eb);
_3ee=_3ee.concat(this.EndAttemptProcess(_3ed[i],true,_3eb,false,_3ea));
}
}
this.LogSeq("`729`",_3eb);
var _3f0=this.GetMinimalSubsetOfActivitiesToRollup(_3ee,null);
if(_3f0.length>0){
this.LogSeqSimple("Invoking rollup on all activities that were just ended.",_3ea);
}
for(var _3f1 in _3f0){
this.OverallRollupProcess(_3f0[_3f1],_3eb,_3ea);
}
this.LogSeq("`1012`",_3eb);
this.LogSeqReturn("",_3eb);
return;
}
function Sequencer_TerminationRequestProcess(_3f2,_3f3,_3f4){
Debug.AssertError("Calling log not passed.",(_3f3===undefined||_3f3===null));
Debug.AssertError("Simple calling log not passed.",(_3f4===undefined||_3f4===null));
var _3f5=this.LogSeqAudit("`1284`"+_3f2+")",_3f3);
var _3f6;
var _3f7=this.GetCurrentActivity();
var _3f8=this.Activities.GetParentActivity(_3f7);
var _3f9=this.GetRootActivity(_3f5);
var _3fa;
var _3fb=null;
var _3fc=false;
this.LogSeq("`366`",_3f5);
if(!this.IsCurrentActivityDefined(_3f5)){
this.LogSeq("`380`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-1",IntegrationImplementation.GetString("You cannot use 'Terminate' because no item is currently open."));
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
}
this.LogSeq("`88`",_3f5);
if((_3f2==TERMINATION_REQUEST_EXIT||_3f2==TERMINATION_REQUEST_ABANDON)&&(_3f7.IsActive()===false)){
this.LogSeq("`381`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-2",IntegrationImplementation.GetString("The current activity has already been terminated."));
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
}
switch(_3f2){
case TERMINATION_REQUEST_EXIT:
this.LogSeq("`1125`",_3f5);
this.LogSeq("`383`",_3f5);
this.EndAttemptProcess(_3f7,false,_3f5,false,_3f4);
this.LogSeq("`240`",_3f5);
this.SequencingExitActionRulesSubprocess(_3f5,_3f4);
this.LogSeq("`1612`",_3f5);
var _3fd;
do{
this.LogSeq("`1104`",_3f5);
_3fd=false;
this.LogSeq("`586`"+this.GetCurrentActivity()+")",_3f5);
_3fb=this.SequencingPostConditionRulesSubprocess(_3f5,_3f4);
this.LogSeq("`470`",_3f5);
if(_3fb.TerminationRequest==TERMINATION_REQUEST_EXIT_ALL){
this.LogSeq("`905`",_3f5);
_3f2=TERMINATION_REQUEST_EXIT_ALL;
this.LogSeq("`673`",_3f5);
_3fc=true;
break;
}
this.LogSeq("`53`",_3f5);
if(_3fb.TerminationRequest==TERMINATION_REQUEST_EXIT_PARENT){
this.LogSeq("`283`",_3f5);
if(this.GetCurrentActivity()!==null&&this.GetCurrentActivity().IsTheRoot()===false){
this.LogSeq("`674`",_3f5);
this.SetCurrentActivity(this.Activities.GetParentActivity(this.GetCurrentActivity()),_3f5,_3f4);
this.LogSeq("`774`",_3f5);
this.EndAttemptProcess(this.GetCurrentActivity(),false,_3f5,false,_3f4);
this.LogSeq("`492`",_3f5);
_3fd=true;
}else{
this.LogSeq("`1565`",_3f5);
this.LogSeq("`355`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-4",IntegrationImplementation.GetString("An 'Exit Parent' sequencing request cannot be processed on the root of the activity tree."));
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
}
}else{
this.LogSeq("`1608`",_3f5);
this.LogSeq("`8`",_3f5);
if(this.GetCurrentActivity()!==null&&this.GetCurrentActivity().IsTheRoot()===true&&_3fb.SequencingRequest!=SEQUENCING_REQUEST_RETRY){
this.LogSeq("`396`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(_3f2,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
}
}
this.LogSeq("`909`"+_3fd+")",_3f5);
}while(_3fd!==false);
if(!_3fc){
this.LogSeq("`54`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(_3f2,_3fb.SequencingRequest,null,"");
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
break;
}
case TERMINATION_REQUEST_EXIT_ALL:
this.LogSeq("`1065`",_3f5);
this.LogSeq("`236`",_3f5);
if(_3f7.IsActive()){
this.LogSeq("`818`",_3f5);
this.EndAttemptProcess(_3f7,false,_3f5,false,_3f4);
}
this.LogSeq("`587`",_3f5);
this.TerminateDescendentAttemptsProcess(_3f9,_3f5,_3f4);
this.LogSeq("`725`",_3f5);
this.EndAttemptProcess(_3f9,false,_3f5,false,_3f4);
this.LogSeq("`350`",_3f5);
this.SetCurrentActivity(_3f9,_3f5,_3f4);
this.LogSeq("`3`",_3f5);
if(_3fb!==null&&_3fb.SequencingRequest!==null){
this.LogSeq("`445`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(_3f2,_3fb.SequencingRequest,null,"");
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
}else{
this.LogSeq("`1027`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(_3f2,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
}
break;
case TERMINATION_REQUEST_SUSPEND_ALL:
this.LogSeq("`991`",_3f5);
this.LogSeq("`44`",_3f5);
this.LogSeqSimple("Exiting the course because of a Suspend All request.",_3f4);
if((Control.Package.Properties.InvokeRollupAtSuspendAll===true||this.ReturnToLmsInvoked)&&_3f7.IsActive()){
this.LogSeq("`507`",_3f5);
this.EndAttemptProcess(_3f7,false,_3f5,true,_3f4);
}
if(_3f7.IsActive()||_3f7.IsSuspended()){
this.LogSeq("`1086`",_3f5);
this.OverallRollupProcess(_3f7,_3f5,_3f4);
this.LogSeq("`843`",_3f5);
this.SetSuspendedActivity(_3f7,_3f4);
}else{
this.LogSeq("`1651`",_3f5);
this.LogSeq("`258`",_3f5);
if(!_3f7.IsTheRoot()){
this.LogSeq("`670`",_3f5);
this.SetSuspendedActivity(_3f8,_3f4);
}else{
this.LogSeq("`1614`",_3f5);
this.LogSeq("`260`",_3f5);
Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-3","The suspend all termination request failed because there is no activity to suspend");
}
}
this.LogSeq("`274`",_3f5);
var _3fe=this.GetSuspendedActivity(_3f5);
_3fa=this.GetActivityPath(_3fe,true);
this.LogSeq("`1074`",_3f5);
if(_3fa.length===0){
this.LogSeq("`273`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-5",IntegrationImplementation.GetString("Nothing to suspend"));
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
}
this.LogSeqSimple("Marking all the parents of \""+_3fe+"\" as suspended and not active, then setting the root to be the current activity.",_3f4);
this.LogSeq("`1003`",_3f5);
for(var i=0;i<_3fa.length;i++){
this.LogSeq("`632`"+_3fa[i].GetItemIdentifier()+")",_3f5);
_3fa[i].SetActive(false);
this.LogSeq("`846`",_3f5);
_3fa[i].SetSuspended(true);
}
this.LogSeq("`348`",_3f5);
this.SetCurrentActivity(_3f9,_3f5,_3f4);
this.LogSeq("`159`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(_3f2,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
break;
case TERMINATION_REQUEST_ABANDON:
this.LogSeqSimple("Setting \""+_3f7+"\" to be not active.",_3f4);
this.LogSeq("`1083`",_3f5);
this.LogSeq("`796`",_3f5);
_3f7.SetActive(false);
this.LogSeq("`459`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(_3f2,null,null,"");
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
break;
case TERMINATION_REQUEST_ABANDON_ALL:
this.LogSeqSimple("Exiting the course with an abandon all request.",_3f4);
this.LogSeqSimple("Setting \""+_3f7+"\" and all its parents to be not active. Also setting the current activity to the root.",_3f4);
this.LogSeq("`994`",_3f5);
this.LogSeq("`281`",_3f5);
_3fa=this.GetActivityPath(_3f7,true);
this.LogSeq("`1082`",_3f5);
if(_3fa.length===0){
this.LogSeq("`280`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-6",IntegrationImplementation.GetString("Nothing to close"));
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
}
this.LogSeq("`993`",_3f5);
for(var i=0;i<_3fa.length;i++){
this.LogSeq("`866`",_3f5);
_3fa[i].SetActive(false);
}
this.LogSeq("`356`",_3f5);
this.SetCurrentActivity(_3f9,_3f5,_3f4);
this.LogSeq("`162`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(_3f2,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
break;
default:
this.LogSeq("`254`",_3f5);
_3f6=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-7",IntegrationImplementation.GetString("The 'Termination' request {0} is not recognized.",_3f2));
this.LogSeqReturn(_3f6,_3f5);
return _3f6;
break;
}
}
function Sequencer_TerminationRequestProcessResult(_400,_401,_402,_403){
this.TerminationRequest=_400;
this.SequencingRequest=_401;
this.Exception=_402;
this.ExceptionText=_403;
}
Sequencer_TerminationRequestProcessResult.prototype.toString=function(){
return "TerminationRequest="+this.TerminationRequest+", SequencingRequest="+this.SequencingRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};

