function RunTimeApi(_1,_2){
this.RunTimeData=null;
this.LearningObject=null;
this.Activity=null;
}
RunTimeApi.prototype.GetNavigationRequest=RunTimeApi_GetNavigationRequest;
RunTimeApi.prototype.ResetState=RunTimeApi_ResetState;
RunTimeApi.prototype.InitializeForDelivery=RunTimeApi_InitializeForDelivery;
RunTimeApi.prototype.SetDirtyData=RunTimeApi_SetDirtyData;
RunTimeApi.prototype.WriteHistoryLog=RunTimeApi_WriteHistoryLog;
RunTimeApi.prototype.WriteHistoryReturnValue=RunTimeApi_WriteHistoryReturnValue;
RunTimeApi.prototype.WriteAuditLog=RunTimeApi_WriteAuditLog;
RunTimeApi.prototype.WriteAuditReturnValue=RunTimeApi_WriteAuditReturnValue;
RunTimeApi.prototype.WriteDetailedLog=RunTimeApi_WriteDetailedLog;
RunTimeApi.prototype.CloseOutSession=RunTimeApi_CloseOutSession;
RunTimeApi.prototype.NeedToCloseOutSession=RunTimeApi_NeedToCloseOutSession;
RunTimeApi.prototype.AccumulateTotalTimeTracked=RunTimeApi_AccumulateTotalTrackedTime;
RunTimeApi.prototype.InitTrackedTimeStart=RunTimeApi_InitTrackedTimeStart;
function RunTimeApi_GetNavigationRequest(){
return null;
}
function RunTimeApi_ResetState(){
}
function RunTimeApi_InitializeForDelivery(_3){
this.RunTimeData=_3.RunTime;
this.LearningObject=_3.LearningObject;
this.Activity=_3;
}
function RunTimeApi_SetDirtyData(){
}
function RunTimeApi_WriteHistoryLog(_4,_5){
}
function RunTimeApi_WriteHistoryReturnValue(_6,_7){
}
function RunTimeApi_WriteAuditLog(_8){
}
function RunTimeApi_WriteAuditReturnValue(_9){
}
function RunTimeApi_WriteDetailedLog(_a){
}
function RunTimeApi_CloseOutSession(){
}
function RunTimeApi_NeedToCloseOutSession(){
return false;
}
function RunTimeApi_InitTrackedTimeStart(){
}
function RunTimeApi_AccumulateTotalTrackedTime(){
}
function Sequencer(_b,_c){
this.LookAhead=_b;
this.Activities=_c;
this.NavigationRequest=null;
this.SuspendedActivity=null;
this.CurrentActivity=null;
this.ExceptionText="";
}
Sequencer.prototype.OverallSequencingProcess=Sequencer_OverallSequencingProcess;
Sequencer.prototype.SetSuspendedActivity=Sequencer_SetSuspendedActivity;
Sequencer.prototype.GetSuspendedActivity=Sequencer_GetSuspendedActivity;
Sequencer.prototype.Start=Sequencer_Start;
Sequencer.prototype.InitialRandomizationAndSelection=Sequencer_InitialRandomizationAndSelection;
Sequencer.prototype.GetCurrentActivity=Sequencer_GetCurrentActivity;
Sequencer.prototype.GetExceptionText=Sequencer_GetExceptionText;
Sequencer.prototype.GetExitAction=Sequencer_GetExitAction;
Sequencer.prototype.EvaluatePossibleNavigationRequests=Sequencer_EvaluatePossibleNavigationRequests;
Sequencer.prototype.InitializePossibleNavigationRequestAbsolutes=Sequencer_InitializePossibleNavigationRequestAbsolutes;
Sequencer.prototype.ContentDeliveryEnvironmentActivityDataSubProcess=Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess;
function Sequencer_OverallSequencingProcess(){
if(this.NavigationRequest===null){
var _d=this.GetExitAction(this.GetCurrentActivity());
switch(_d){
case (EXIT_ACTION_EXIT_NO_CONFIRMATION):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
break;
case (EXIT_ACTION_EXIT_CONFIRMATION):
Debug.AssertError("EXIT_ACTION_EXIT_CONFIRMATION not supported for AICC");
break;
case (EXIT_ACTION_GO_TO_NEXT_SCO):
Debug.AssertError("EXIT_ACTION_GO_TO_NEXT_SCO not supported for AICC");
break;
case (EXIT_ACTION_DISPLAY_MESSAGE):
Debug.AssertError("EXIT_ACTION_DISPLAY_MESSAGE not supported for AICC");
break;
case (EXIT_ACTION_DO_NOTHING):
Debug.AssertError("EXIT_ACTION_DO_NOTHING not supported for AICC");
break;
case (EXIT_ACTION_REFRESH_PAGE):
Control.RefreshPage();
break;
}
}
if(this.NavigationRequest===null){
this.ExceptionText=IntegrationImplementation.GetString("Please make a selection.");
}else{
if(this.NavigationRequest.Type==NAVIGATION_REQUEST_CHOICE){
var _e=this.Activities.GetActivityFromIdentifier(this.NavigationRequest.TargetActivity);
if(_e===null||_e.IsDeliverable()===false){
Debug.AssertError("Selected an activity that does not exist or is not deliverable. Activity="+_e);
}
this.CurrentActivity=_e;
var _f=this.Activities.SortedActivityList;
for(var i=0;i<_f.length;i++){
_f[i].SetActive(false);
}
var _11=this.Activities.GetActivityPath(_e,true);
for(var i=0;i<_11.length;i++){
_11[i].SetActive(true);
}
if(Control.Package.Properties.ScoLaunchType!==LAUNCH_TYPE_POPUP_AFTER_CLICK&&Control.Package.Properties.ScoLaunchType!==LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR){
this.ContentDeliveryEnvironmentActivityDataSubProcess();
}
Control.DeliverActivity(_e);
}else{
if(this.NavigationRequest.Type==NAVIGATION_REQUEST_EXIT){
}else{
if(this.NavigationRequest.Type==NAVIGATION_REQUEST_SUSPEND_ALL||this.NavigationRequest.Type==NAVIGATION_REQUEST_EXIT_ALL||this.NavigationRequest.Type==NAVIGATION_REQUEST_EXIT_PLAYER){
Control.ExitScormPlayer();
}else{
Debug.AssertError("Only choice navigation requests are supported for AICC., Received type-"+this.NavigationRequest.Type);
this.ExceptionText=IntegrationImplementation.GetString("Please make a selection.");
}
}
}
}
}
function Sequencer_SetSuspendedActivity(_12){
this.SuspendedActivity=_12;
}
function Sequencer_GetSuspendedActivity(){
return this.SuspendedActivity;
}
function Sequencer_Start(){
if(this.Activities.GetNumDeliverableActivities()==1){
for(var i=0;i<this.Activities.SortedActivityList.length;i++){
if(this.Activities.SortedActivityList[i].IsDeliverable()){
this.SuspendedActivity=null;
this.CurrentActivity=this.Activities.SortedActivityList[i];
Control.DeliverActivity(this.Activities.SortedActivityList[i]);
return;
}
}
}else{
this.ExceptionText=IntegrationImplementation.GetString("Please make a selection.");
}
}
function Sequencer_InitialRandomizationAndSelection(){
}
function Sequencer_GetCurrentActivity(){
return this.CurrentActivity;
}
function Sequencer_GetExceptionText(){
return this.ExceptionText;
}
function Sequencer_GetExitAction(_14){
if(this.Activities.GetNumDeliverableActivities()==1){
return EXIT_ACTION_EXIT_NO_CONFIRMATION;
}else{
return EXIT_ACTION_REFRESH_PAGE;
}
}
function Sequencer_EvaluatePossibleNavigationRequests(_15,_16,_17){
var _18=Control.Package.Properties;
_15[POSSIBLE_NAVIGATION_REQUEST_INDEX_START].WillSucceed=true;
_15[POSSIBLE_NAVIGATION_REQUEST_INDEX_RESUME_ALL].WillSucceed=true;
_15[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed=false;
_15[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed=false;
_15[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed=true;
_15[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].WillSucceed=true;
_15[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL].WillSucceed=true;
_15[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON].WillSucceed=true;
_15[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON_ALL].WillSucceed=true;
var _19;
for(var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;i<_15.length;i++){
_19=this.Activities.GetActivityFromIdentifier(_15[i].TargetActivityItemIdentifier);
_15[i].WillSucceed=(_19.IsDeliverable()&&_18.EnableChoiceNav===true);
}
return _15;
}
function Sequencer_InitializePossibleNavigationRequestAbsolutes(_1b){
}
function Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess(){
this.SuspendedActivity=null;
}

