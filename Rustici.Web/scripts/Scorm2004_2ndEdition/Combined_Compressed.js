var SCORM_TRUE="true";
var SCORM_FALSE="false";
var SCORM_UNKNOWN="unknown";
var SCORM2004_NO_ERROR="0";
var SCORM2004_GENERAL_EXCEPTION_ERROR="101";
var SCORM2004_GENERAL_INITIALIZATION_FAILURE_ERROR="102";
var SCORM2004_ALREADY_INTIAILIZED_ERROR="103";
var SCORM2004_CONTENT_INSTANCE_TERMINATED_ERROR="104";
var SCORM2004_GENERAL_TERMINATION_FAILURE_ERROR="111";
var SCORM2004_TERMINATION_BEFORE_INITIALIZATION_ERROR="112";
var SCORM2004_TERMINATION_AFTER_TERMINATION_ERROR="113";
var SCORM2004_RETRIEVE_DATA_BEFORE_INITIALIZATION_ERROR="122";
var SCORM2004_RETRIEVE_DATA_AFTER_TERMINATION_ERROR="123";
var SCORM2004_STORE_DATA_BEFORE_INITIALIZATION_ERROR="132";
var SCORM2004_STORE_DATA_AFTER_TERMINATION_ERROR="133";
var SCORM2004_COMMIT_BEFORE_INITIALIZATION_ERROR="142";
var SCORM2004_COMMIT_AFTER_TERMINATION_ERROR="143";
var SCORM2004_GENERAL_ARGUMENT_ERROR="201";
var SCORM2004_GENERAL_GET_FAILURE_ERROR="301";
var SCORM2004_GENERAL_SET_FAILURE_ERROR="351";
var SCORM2004_GENERAL_COMMIT_FAILURE_ERROR="391";
var SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR="401";
var SCORM2004_UNIMPLEMENTED_DATA_MODEL_ELEMENT_ERROR="402";
var SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR="403";
var SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR="404";
var SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR="405";
var SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR="406";
var SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR="407";
var SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR="408";
var SCORM2004_ErrorStrings=new Array();
SCORM2004_ErrorStrings[SCORM2004_NO_ERROR]="No Error";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_EXCEPTION_ERROR]="General Exception";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_INITIALIZATION_FAILURE_ERROR]="General Initialization Failure";
SCORM2004_ErrorStrings[SCORM2004_ALREADY_INTIAILIZED_ERROR]="Already Initialized";
SCORM2004_ErrorStrings[SCORM2004_CONTENT_INSTANCE_TERMINATED_ERROR]="Content Instance Terminated";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_TERMINATION_FAILURE_ERROR]="General Termination Failure";
SCORM2004_ErrorStrings[SCORM2004_TERMINATION_BEFORE_INITIALIZATION_ERROR]="Termination Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_TERMINATION_AFTER_TERMINATION_ERROR]="Termination AFter Termination";
SCORM2004_ErrorStrings[SCORM2004_RETRIEVE_DATA_BEFORE_INITIALIZATION_ERROR]="Retrieve Data Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_RETRIEVE_DATA_AFTER_TERMINATION_ERROR]="Retrieve Data After Termination";
SCORM2004_ErrorStrings[SCORM2004_STORE_DATA_BEFORE_INITIALIZATION_ERROR]="Store Data Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_STORE_DATA_AFTER_TERMINATION_ERROR]="Store Data After Termination";
SCORM2004_ErrorStrings[SCORM2004_COMMIT_BEFORE_INITIALIZATION_ERROR]="Commit Before Initialization";
SCORM2004_ErrorStrings[SCORM2004_COMMIT_AFTER_TERMINATION_ERROR]="Commit After Termination";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_ARGUMENT_ERROR]="General Argument Error";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_GET_FAILURE_ERROR]="General Get Failure";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_SET_FAILURE_ERROR]="General Set Failure";
SCORM2004_ErrorStrings[SCORM2004_GENERAL_COMMIT_FAILURE_ERROR]="General Commit Failure";
SCORM2004_ErrorStrings[SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR]="Undefined Data Model Element";
SCORM2004_ErrorStrings[SCORM2004_UNIMPLEMENTED_DATA_MODEL_ELEMENT_ERROR]="Unimplemented Data Model Element";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR]="Data Model Element Value Not Initialized";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR]="Data Model Element Is Read Only";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR]="Data Model Element Is Write Only";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR]="Data Model Element Type Mismatch";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR]="Data Model Element Value Out Of Range";
SCORM2004_ErrorStrings[SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR]="Data Model Dependency Not Established";
var SCORM2004_VERSION="1.0";
var SCORM2004_COMMENTS_FROM_LEARNER_CHILDREN="comment,location,timestamp";
var SCORM2004_COMMENTS_FROM_LMS_CHILDREN="comment,location,timestamp";
var SCORM2004_INTERACTIONS_CHILDREN="id,type,objectives,timestamp,correct_responses,weighting,learner_response,result,latency,description";
var SCORM2004_LEARNER_PREFERENCE_CHILDREN="audio_level,language,delivery_speed,audio_captioning";
var SCORM2004_OBJECTIVES_CHILDREN="progress_measure,description,score,id,success_status,completion_status";
var SCORM2004_OBJECTIVES_SCORE_CHILDREN="scaled,raw,min,max";
var SCORM2004_SCORE_CHILDREN="scaled,min,max,raw";
function RunTimeApi(_1,_2){
this.LearnerId=_1;
this.LearnerName=_2;
this.ErrorNumber=SCORM2004_NO_ERROR;
this.ErrorString="";
this.ErrorDiagnostic="";
this.TrackedStartDate=null;
this.TrackedEndDate=null;
this.Initialized=false;
this.Terminated=false;
this.ScoCalledFinish=false;
this.RunTimeData=null;
this.LearningObject=null;
this.Activity=null;
this.IsLookAheadSequencerDataDirty=false;
this.LearnerPrefsArray=new Object();
if(SSP_ENABLED){
this.SSPApi=new SSPApi(MAX_SSP_STORAGE,this);
}
}
RunTimeApi.prototype.GetNavigationRequest=RunTimeApi_GetNavigationRequest;
RunTimeApi.prototype.ResetState=RunTimeApi_ResetState;
RunTimeApi.prototype.InitializeForDelivery=RunTimeApi_InitializeForDelivery;
RunTimeApi.prototype.SetDirtyData=RunTimeApi_SetDirtyData;
RunTimeApi.prototype.WriteHistoryLog=RunTimeApi_WriteHistoryLog;
RunTimeApi.prototype.WriteHistoryReturnValue=RunTimeApi_WriteHistoryReturnValue;
RunTimeApi.prototype.WriteAuditLog=RunTimeApi_WriteAuditLog;
RunTimeApi.prototype.WriteAuditReturnValue=RunTimeApi_WriteAuditReturnValue;
RunTimeApi.prototype.WriteDetailedLog=RunTimeApi_WriteDetailedLog;
RunTimeApi.prototype.CloseOutSession=RunTimeApi_CloseOutSession;
RunTimeApi.prototype.NeedToCloseOutSession=RunTimeApi_NeedToCloseOutSession;
RunTimeApi.prototype.AccumulateTotalTimeTracked=RunTimeApi_AccumulateTotalTrackedTime;
RunTimeApi.prototype.InitTrackedTimeStart=RunTimeApi_InitTrackedTimeStart;
function RunTimeApi_GetNavigationRequest(){
return null;
}
function RunTimeApi_ResetState(_3){
this.TrackedStartDate=null;
this.TrackedEndDate=null;
}
function RunTimeApi_InitializeForDelivery(_4){
this.RunTimeData=_4.RunTime;
this.LearningObject=_4.LearningObject;
this.Activity=_4;
if(Control.Package.Properties.ResetRunTimeDataTiming==RESET_RT_DATA_TIMING_WHEN_EXIT_IS_NOT_SUSPEND){
if(this.RunTimeData.Exit!=SCORM_EXIT_SUSPEND&&this.RunTimeData.Exit!=SCORM_EXIT_LOGOUT){
var _5={ev:"ResetRuntime",ai:_4.ItemIdentifier,at:_4.LearningObject.Title};
this.WriteHistoryLog("",_5);
this.RunTimeData.ResetState();
}
}
this.RunTimeData.Exit=SCORM_EXIT_UNKNOWN;
this.RunTimeData.NavRequest=SCORM_RUNTIME_NAV_REQUEST_NONE;
this.Initialized=false;
this.Terminated=false;
this.ScoCalledFinish=false;
this.TrackedStartDate=null;
this.TrackedEndDate=null;
this.ErrorNumber=SCORM2004_NO_ERROR;
this.ErrorString="";
this.ErrorDiagnostic="";
var _6;
var _7;
var _8;
for(var _9 in this.Activity.ActivityObjectives){
_8=this.Activity.ActivityObjectives[_9];
_6=_8.GetIdentifier();
if(_6!==null&&_6!==undefined&&_6.length>0){
_7=this.RunTimeData.FindObjectiveWithId(_6);
if(_7===null){
this.RunTimeData.AddObjective();
_7=this.RunTimeData.Objectives[this.RunTimeData.Objectives.length-1];
_7.Identifier=_6;
}
if(_8.GetProgressStatus(this.Activity,false)===true){
if(_8.GetSatisfiedStatus(this.Activity,false)===true){
_7.SuccessStatus=SCORM_STATUS_PASSED;
}else{
_7.SuccessStatus=SCORM_STATUS_FAILED;
}
}
if(_8.GetMeasureStatus(this.Activity,false)===true){
_7.ScoreScaled=_8.GetNormalizedMeasure(this.Activity,false);
}
_7.SuccessStatusChangedDuringRuntime=false;
_7.MeasureChangedDuringRuntime=false;
}
}
}
function RunTimeApi_SetDirtyData(){
this.Activity.DataState=DATA_STATE_DIRTY;
}
function RunTimeApi_WriteHistoryLog(_a,_b){
return HistoryLog.WriteEventDetailed(_a,_b);
}
function RunTimeApi_WriteHistoryReturnValue(_c,_d){
HistoryLog.WriteEventDetailedReturnValue(_c,_d);
}
function RunTimeApi_WriteAuditLog(_e){
return Debug.WriteRteAudit(_e);
}
function RunTimeApi_WriteAuditReturnValue(_f){
Debug.WriteRteAuditReturnValue(_f);
}
function RunTimeApi_WriteDetailedLog(str){
Debug.WriteRteDetailed(str);
}
function RunTimeApi_NeedToCloseOutSession(){
return ((this.Initialized===false)||this.ScoCalledFinish);
}
RunTimeApi.prototype.version=SCORM2004_VERSION;
RunTimeApi.prototype.Initialize=RunTimeApi_Initialize;
RunTimeApi.prototype.Terminate=RunTimeApi_Terminate;
RunTimeApi.prototype.GetValue=RunTimeApi_GetValue;
RunTimeApi.prototype.SetValue=RunTimeApi_SetValue;
RunTimeApi.prototype.Commit=RunTimeApi_Commit;
RunTimeApi.prototype.GetLastError=RunTimeApi_GetLastError;
RunTimeApi.prototype.GetErrorString=RunTimeApi_GetErrorString;
RunTimeApi.prototype.GetDiagnostic=RunTimeApi_GetDiagnostic;
RunTimeApi.prototype.RetrieveGetValueData=RunTimeApi_RetrieveGetValueData;
RunTimeApi.prototype.StoreValue=RunTimeApi_StoreValue;
RunTimeApi.prototype.SetErrorState=RunTimeApi_SetErrorState;
RunTimeApi.prototype.ClearErrorState=RunTimeApi_ClearErrorState;
RunTimeApi.prototype.CheckMaxLength=RunTimeApi_CheckMaxLength;
RunTimeApi.prototype.CheckLengthAndWarn=RunTimeApi_CheckLengthAndWarn;
RunTimeApi.prototype.CheckForInitializeError=RunTimeApi_CheckForInitializeError;
RunTimeApi.prototype.CheckForTerminateError=RunTimeApi_CheckForTerminateError;
RunTimeApi.prototype.CheckForGetValueError=RunTimeApi_CheckForGetValueError;
RunTimeApi.prototype.CheckForSetValueError=RunTimeApi_CheckForSetValueError;
RunTimeApi.prototype.CheckForCommitError=RunTimeApi_CheckForCommitError;
RunTimeApi.prototype.CheckCommentsCollectionLength=RunTimeApi_CheckCommentsCollectionLength;
RunTimeApi.prototype.CheckInteractionsCollectionLength=RunTimeApi_CheckInteractionsCollectionLength;
RunTimeApi.prototype.CheckInteractionObjectivesCollectionLength=RunTimeApi_CheckInteractionObjectivesCollectionLength;
RunTimeApi.prototype.CheckInteractionsCorrectResponsesCollectionLength=RunTimeApi_CheckInteractionsCorrectResponsesCollectionLength;
RunTimeApi.prototype.CheckObjectivesCollectionLength=RunTimeApi_CheckObjectivesCollectionLength;
RunTimeApi.prototype.LookAheadSessionClose=RunTimeApi_LookAheadSessionClose;
RunTimeApi.prototype.ValidOtheresponse=RunTimeApi_ValidOtheresponse;
RunTimeApi.prototype.ValidNumericResponse=RunTimeApi_ValidNumericResponse;
RunTimeApi.prototype.ValidSequencingResponse=RunTimeApi_ValidSequencingResponse;
RunTimeApi.prototype.ValidPerformanceResponse=RunTimeApi_ValidPerformanceResponse;
RunTimeApi.prototype.ValidMatchingResponse=RunTimeApi_ValidMatchingResponse;
RunTimeApi.prototype.ValidLikeRTResponse=RunTimeApi_ValidLikeRTResponse;
RunTimeApi.prototype.ValidLongFillInResponse=RunTimeApi_ValidLongFillInResponse;
RunTimeApi.prototype.ValidFillInResponse=RunTimeApi_ValidFillInResponse;
RunTimeApi.prototype.IsValidArrayOfLocalizedStrings=RunTimeApi_IsValidArrayOfLocalizedStrings;
RunTimeApi.prototype.IsValidArrayOfShortIdentifiers=RunTimeApi_IsValidArrayOfShortIdentifiers;
RunTimeApi.prototype.IsValidCommaDelimitedArrayOfShortIdentifiers=RunTimeApi_IsValidCommaDelimitedArrayOfShortIdentifiers;
RunTimeApi.prototype.ValidMultipleChoiceResponse=RunTimeApi_ValidMultipleChoiceResponse;
RunTimeApi.prototype.ValidTrueFalseResponse=RunTimeApi_ValidTrueFalseResponse;
RunTimeApi.prototype.ValidTimeInterval=RunTimeApi_ValidTimeInterval;
RunTimeApi.prototype.ValidTime=RunTimeApi_ValidTime;
RunTimeApi.prototype.ValidReal=RunTimeApi_ValidReal;
RunTimeApi.prototype.IsValidUrn=RunTimeApi_IsValidUrn;
RunTimeApi.prototype.ValidIdentifier=RunTimeApi_ValidIdentifier;
RunTimeApi.prototype.ValidShortIdentifier=RunTimeApi_ValidShortIdentifier;
RunTimeApi.prototype.ValidLongIdentifier=RunTimeApi_ValidLongIdentifier;
RunTimeApi.prototype.ValidLanguage=RunTimeApi_ValidLanguage;
RunTimeApi.prototype.ExtractLanguageDelimiterFromLocalizedString=RunTimeApi_ExtractLanguageDelimiterFromLocalizedString;
RunTimeApi.prototype.ValidLocalizedString=RunTimeApi_ValidLocalizedString;
RunTimeApi.prototype.ValidCharString=RunTimeApi_ValidCharString;
RunTimeApi.prototype.TranslateBooleanIntoCMI=RunTimeApi_TranslateBooleanIntoCMI;
RunTimeApi.prototype.SetLookAheadDirtyDataFlagIfNeeded=RunTimeApi_SetLookAheadDirtyDataFlagIfNeeded;
function RunTimeApi_Initialize(arg){
this.WriteAuditLog("`1702`"+arg+"')");
var _12={ev:"ApiInitialize"};
if(this.Activity){
_12.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_12);
var _13;
var _14;
this.ClearErrorState();
_13=this.CheckForInitializeError(arg);
if(!_13){
_14=SCORM_FALSE;
}else{
this.Initialized=true;
_14=SCORM_TRUE;
}
this.WriteAuditReturnValue(_14);
return _14;
}
function RunTimeApi_Terminate(arg){
var _16=this.WriteAuditLog("`1714`"+arg+"')");
var _17;
var _18;
var _19;
this.ClearErrorState();
_17=this.CheckForTerminateError(arg);
var _1a=(_17&&(this.ScoCalledFinish===false));
if(!_17){
_19=SCORM_FALSE;
}else{
var _1b={ev:"ApiTerminate"};
if(this.Activity){
_1b.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_1b);
this.LookAheadSessionClose();
this.CloseOutSession();
if(this.IsLookAheadSequencerDataDirty===true){
this.IsLookAheadSequencerDataDirty=false;
window.setTimeout("Control.EvaluatePossibleNavigationRequests(true);",150);
}
this.Terminated=true;
this.ScoCalledFinish=true;
this.SetDirtyData();
_19=SCORM_TRUE;
}
if(_1a===true&&this.RunTimeData.NavRequest!=SCORM_RUNTIME_NAV_REQUEST_NONE&&Control.IsThereAPendingNavigationRequest()===false){
this.WriteDetailedLog("`449`"+Control.IsThereAPendingNavigationRequest());
window.setTimeout("Control.ScoHasTerminatedSoUnload();",150);
}
Control.SignalTerminated();
this.WriteAuditReturnValue(_19);
return _19;
}
function RunTimeApi_GetValue(_1c){
this.WriteAuditLog("`1719`"+_1c+"')");
var _1d;
var _1e;
this.ClearErrorState();
_1c=CleanExternalString(_1c);
var _1f=RemoveIndiciesFromCmiElement(_1c);
var _20=ExtractIndex(_1c);
var _21=ExtractSecondaryIndex(_1c);
_1e=this.CheckForGetValueError(_1c,_1f,_20,_21);
if(!_1e){
_1d="";
}else{
_1d=this.RetrieveGetValueData(_1c,_1f,_20,_21);
if(_1d===null){
_1d="";
}
}
this.WriteAuditReturnValue(_1d);
return _1d;
}
function RunTimeApi_SetValue(_22,_23){
this.WriteAuditLog("`1717`"+_22+"`1729`"+_23+"')");
var _24;
var _25;
this.ClearErrorState();
_22=CleanExternalString(_22);
_23=CleanExternalString(_23);
var _26=RemoveIndiciesFromCmiElement(_22);
var _27=ExtractIndex(_22);
var _28=ExtractSecondaryIndex(_22);
this.CheckMaxLength(_26,_23);
_24=this.CheckForSetValueError(_22,_23,_26,_27,_28);
if(!_24){
_25=SCORM_FALSE;
}else{
this.StoreValue(_22,_23,_26,_27,_28);
this.SetDirtyData();
_25=SCORM_TRUE;
}
this.WriteAuditReturnValue(_25);
return _25;
}
function RunTimeApi_Commit(arg){
this.WriteAuditLog("`1722`"+arg+"')");
var _2a;
var _2b;
this.ClearErrorState();
_2a=this.CheckForCommitError(arg);
if(!_2a){
_2b=SCORM_FALSE;
}else{
_2b=SCORM_TRUE;
}
this.LookAheadSessionClose();
if(this.IsLookAheadSequencerDataDirty===true){
this.IsLookAheadSequencerDataDirty=false;
window.setTimeout("Control.EvaluatePossibleNavigationRequests(true);",150);
}
this.WriteAuditReturnValue(_2b);
return _2b;
}
function RunTimeApi_GetLastError(){
this.WriteAuditLog("`1696`");
var _2c=this.ErrorNumber;
this.WriteAuditReturnValue(_2c);
return _2c;
}
function RunTimeApi_GetErrorString(arg){
this.WriteAuditLog("`1672`"+arg+"')");
var _2e="";
if(arg===""){
_2e=this.ErrorString;
}else{
if(SCORM2004_ErrorStrings[arg]!==null&&SCORM2004_ErrorStrings[arg]!==undefined){
_2e=SCORM2004_ErrorStrings[arg];
}
}
this.WriteAuditReturnValue(_2e);
return _2e;
}
function RunTimeApi_GetDiagnostic(arg){
this.WriteAuditLog("`1687`"+arg+"')");
var _30;
if(this.ErrorDiagnostic===""){
_30="No diagnostic information available";
}else{
_30=this.ErrorDiagnostic;
}
this.WriteAuditReturnValue(_30);
return _30;
}
function RunTimeApi_CloseOutSession(){
this.WriteDetailedLog("`1653`");
var _31=this.LearningObject.GetScaledPassingScore();
this.WriteDetailedLog("`1724`"+this.RunTimeData.Mode);
this.WriteDetailedLog("`1721`"+this.RunTimeData.Credit);
this.WriteDetailedLog("`1589`"+this.RunTimeData.CompletionStatus);
this.WriteDetailedLog("`1665`"+this.RunTimeData.SuccessStatus);
this.WriteDetailedLog("`1557`"+_31);
this.WriteDetailedLog("`1723`"+this.RunTimeData.ScoreScaled);
this.WriteDetailedLog("`1527`"+this.LearningObject.CompletionThreshold);
this.WriteDetailedLog("`1593`"+this.RunTimeData.ProgressMeasure);
var _32=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.SessionTime);
var _33=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTime);
var _34=_32+_33;
var _35=ConvertHundredthsToIso8601TimeSpan(_34);
this.WriteDetailedLog("`1695`"+this.RunTimeData.SessionTime+" ("+_32+"`1705`");
this.WriteDetailedLog("`1677`"+this.RunTimeData.TotalTime+" ("+_33+"`1705`");
this.WriteDetailedLog("`1673`"+_35+" ("+_34+"`1705`");
this.RunTimeData.TotalTime=_35;
this.RunTimeData.SessionTime="";
this.AccumulateTotalTimeTracked();
this.WriteDetailedLog("`1498`"+this.RunTimeData.TotalTimeTracked);
if(this.RunTimeData.Exit==SCORM_EXIT_SUSPEND||this.RunTimeData.Exit==SCORM_EXIT_LOGOUT){
this.WriteDetailedLog("`1583`");
this.RunTimeData.Entry=SCORM_ENTRY_RESUME;
}
if(this.RunTimeData.ProgressMeasure!==null&&this.LearningObject.CompletionThreshold!==null){
if(parseFloat(this.RunTimeData.ProgressMeasure)>=parseFloat(this.LearningObject.CompletionThreshold)){
this.WriteDetailedLog("`619`");
this.RunTimeData.CompletionStatus=SCORM_STATUS_COMPLETED;
}else{
this.WriteDetailedLog("`576`");
this.RunTimeData.CompletionStatus=SCORM_STATUS_INCOMPLETE;
}
}
if(_31!==null){
if(this.RunTimeData.ScoreScaled!==null){
if(parseFloat(this.RunTimeData.ScoreScaled)>=parseFloat(_31)){
this.WriteDetailedLog("`698`");
this.RunTimeData.SuccessStatus=SCORM_STATUS_PASSED;
}else{
this.WriteDetailedLog("`650`");
this.RunTimeData.SuccessStatus=SCORM_STATUS_FAILED;
}
}else{
this.WriteDetailedLog("`577`");
this.RunTimeData.SuccessStatus=SCORM_STATUS_UNKNOWN;
}
}
if(this.RunTimeData.Exit==SCORM_EXIT_TIME_OUT){
this.WriteDetailedLog("`1449`");
this.RunTimeData.NavRequest=SCORM_RUNTIME_NAV_REQUEST_EXIT;
}else{
if(this.RunTimeData.Exit==SCORM_EXIT_LOGOUT){
if(Control.Package.Properties.LogoutCausesPlayerExit===true){
this.WriteDetailedLog("`1332`");
this.RunTimeData.NavRequest=SCORM_RUNTIME_NAV_REQUEST_SUSPENDALL;
}else{
this.WriteDetailedLog("`340`");
this.Activity.SetSuspended(true);
}
}else{
if(this.RunTimeData.Exit==SCORM_EXIT_SUSPEND){
this.WriteDetailedLog("`1601`");
this.Activity.SetSuspended(true);
}
}
}
return true;
}
function RunTimeApi_LookAheadSessionClose(){
this.RunTimeData.LookAheadCompletionStatus=this.RunTimeData.CompletionStatus;
if(this.RunTimeData.ProgressMeasure!==null&&this.LearningObject.CompletionThreshold!==null){
if(parseFloat(this.RunTimeData.ProgressMeasure)>=parseFloat(this.LearningObject.CompletionThreshold)){
this.WriteDetailedLog("`619`");
this.RunTimeData.LookAheadCompletionStatus=SCORM_STATUS_COMPLETED;
}else{
this.WriteDetailedLog("`576`");
this.RunTimeData.LookAheadCompletionStatus=SCORM_STATUS_INCOMPLETE;
}
}
this.RunTimeData.LookAheadSuccessStatus=this.RunTimeData.SuccessStatus;
var _36=this.LearningObject.GetScaledPassingScore();
if(_36!==null){
if(this.RunTimeData.ScoreScaled!==null){
if(parseFloat(this.RunTimeData.ScoreScaled)>=parseFloat(_36)){
this.WriteDetailedLog("`698`");
this.RunTimeData.LookAheadSuccessStatus=SCORM_STATUS_PASSED;
}else{
this.WriteDetailedLog("`650`");
this.RunTimeData.LookAheadSuccessStatus=SCORM_STATUS_FAILED;
}
}else{
this.WriteDetailedLog("`577`");
this.RunTimeData.LookAheadSuccessStatus=SCORM_STATUS_UNKNOWN;
}
}
}
function RunTimeApi_RetrieveGetValueData(_37,_38,_39,_3a){
this.WriteDetailedLog("`1541`"+_37+", "+_38+", "+_39+", "+_3a+") ");
var _3b;
var _3c="";
if(_37.indexOf("adl.nav.request_valid.choice.{")===0){
this.WriteDetailedLog("`1283`");
_3b=_37.substring(_37.indexOf("{"));
if(!Control.IsTargetValid(_3b)){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The target of the choice request ("+_3b+") is invalid.");
return SCORM_FALSE;
}
_3c=this.TranslateBooleanIntoCMI(Control.IsChoiceRequestValid(_3b));
return _3c;
}
switch(_38){
case "cmi._version":
this.WriteDetailedLog("`1497`");
_3c=SCORM2004_VERSION;
break;
case "cmi.comments_from_learner._children":
this.WriteDetailedLog("`1159`");
_3c=SCORM2004_COMMENTS_FROM_LEARNER_CHILDREN;
break;
case "cmi.comments_from_learner._count":
this.WriteDetailedLog("`1226`");
_3c=this.RunTimeData.Comments.length;
break;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1206`");
_3c=this.RunTimeData.Comments[_39].GetCommentValue();
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1195`");
_3c=this.RunTimeData.Comments[_39].Location;
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1153`");
_3c=this.RunTimeData.Comments[_39].Timestamp;
break;
case "cmi.comments_from_lms._children":
this.WriteDetailedLog("`1244`");
_3c=SCORM2004_COMMENTS_FROM_LMS_CHILDREN;
break;
case "cmi.comments_from_lms._count":
this.WriteDetailedLog("`1289`");
_3c=this.RunTimeData.CommentsFromLMS.length;
break;
case "cmi.comments_from_lms.n.comment":
this.WriteDetailedLog("`1279`");
_3c=this.RunTimeData.CommentsFromLMS[_39].GetCommentValue();
break;
case "cmi.comments_from_lms.n.location":
this.WriteDetailedLog("`1266`");
_3c=this.RunTimeData.CommentsFromLMS[_39].Location;
break;
case "cmi.comments_from_lms.n.timestamp":
this.WriteDetailedLog("`1238`");
_3c=this.RunTimeData.CommentsFromLMS[_39].Timestamp;
break;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
_3c=this.RunTimeData.CompletionStatus;
break;
case "cmi.completion_threshold":
this.WriteDetailedLog("`1367`");
_3c=this.LearningObject.CompletionThreshold;
break;
case "cmi.credit":
this.WriteDetailedLog("`1617`");
_3c=this.RunTimeData.Credit;
break;
case "cmi.entry":
this.WriteDetailedLog("`1643`");
_3c=this.RunTimeData.Entry;
break;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
Debug.AssertError("Exit element is write only");
_3c="";
break;
case "cmi.interactions._children":
this.WriteDetailedLog("`1321`");
_3c=SCORM2004_INTERACTIONS_CHILDREN;
break;
case "cmi.interactions._count":
this.WriteDetailedLog("`1380`");
_3c=this.RunTimeData.Interactions.length;
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1444`");
_3c=this.RunTimeData.Interactions[_39].Id;
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1402`");
_3c=this.RunTimeData.Interactions[_39].Type;
break;
case "cmi.interactions.n.objectives._count":
this.WriteDetailedLog("`1200`");
_3c=this.RunTimeData.Interactions[_39].Objectives.length;
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1272`");
_3c=this.RunTimeData.Interactions[_39].Objectives[_3a];
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1337`");
_3c=this.RunTimeData.Interactions[_39].Timestamp;
break;
case "cmi.interactions.n.correct_responses._count":
this.WriteDetailedLog("`1066`");
_3c=this.RunTimeData.Interactions[_39].CorrectResponses.length;
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`1028`");
_3c=this.RunTimeData.Interactions[_39].CorrectResponses[_3a];
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1329`");
_3c=this.RunTimeData.Interactions[_39].Weighting;
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1210`");
_3c=this.RunTimeData.Interactions[_39].LearnerResponse;
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1378`");
_3c=this.RunTimeData.Interactions[_39].Result;
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1362`");
_3c=this.RunTimeData.Interactions[_39].Latency;
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1297`");
_3c=this.RunTimeData.Interactions[_39].Description;
break;
case "cmi.launch_data":
this.WriteDetailedLog("`1532`");
_3c=this.LearningObject.DataFromLms;
break;
case "cmi.learner_id":
this.WriteDetailedLog("`1549`");
_3c=LearnerId;
break;
case "cmi.learner_name":
this.WriteDetailedLog("`1516`");
_3c=LearnerName;
break;
case "cmi.learner_preference._children":
this.WriteDetailedLog("`1230`");
_3c=SCORM2004_LEARNER_PREFERENCE_CHILDREN;
break;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1528`");
_3c=this.RunTimeData.AudioLevel;
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1574`");
_3c=this.RunTimeData.LanguagePreference;
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1467`");
_3c=this.RunTimeData.DeliverySpeed;
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1436`");
_3c=this.RunTimeData.AudioCaptioning;
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
_3c=this.RunTimeData.Location;
var _3d={ev:"Get",k:"location",v:(_3c==null?"<null>":_3c)};
if(this.Activity){
_3d.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3d);
break;
case "cmi.max_time_allowed":
this.WriteDetailedLog("`1430`");
_3c="";
if(this.LearningObject.SequencingData!==null&&this.LearningObject.SequencingData.LimitConditionAttemptAbsoluteDurationControl===true){
_3c=this.LearningObject.SequencingData.LimitConditionAttemptAbsoluteDurationLimit;
}
break;
case "cmi.mode":
this.WriteDetailedLog("`1676`");
_3c=this.RunTimeData.Mode;
break;
case "cmi.objectives._children":
this.WriteDetailedLog("`1365`");
_3c=SCORM2004_OBJECTIVES_CHILDREN;
break;
case "cmi.objectives._count":
this.WriteDetailedLog("`1407`");
_3c=this.RunTimeData.Objectives.length;
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1493`");
_3c=this.RunTimeData.Objectives[_39].Identifier;
break;
case "cmi.objectives.n.score._children":
this.WriteDetailedLog("`1264`");
_3c=SCORM2004_OBJECTIVES_SCORE_CHILDREN;
break;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1303`");
_3c=this.RunTimeData.Objectives[_39].ScoreScaled;
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1359`");
_3c=this.RunTimeData.Objectives[_39].ScoreRaw;
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1357`");
_3c=this.RunTimeData.Objectives[_39].ScoreMin;
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1358`");
_3c=this.RunTimeData.Objectives[_39].ScoreMax;
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1275`");
_3c=this.RunTimeData.Objectives[_39].SuccessStatus;
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1221`");
_3c=this.RunTimeData.Objectives[_39].CompletionStatus;
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1242`");
_3c=this.RunTimeData.Objectives[_39].ProgressMeasure;
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1324`");
_3c=this.RunTimeData.Objectives[_39].Description;
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1427`");
_3c=this.RunTimeData.ProgressMeasure;
break;
case "cmi.scaled_passing_score":
this.WriteDetailedLog("`1368`");
_3c=this.LearningObject.GetScaledPassingScore();
if(_3c===null){
_3c="";
}
break;
case "cmi.score._children":
this.WriteDetailedLog("`1446`");
_3c=SCORM2004_SCORE_CHILDREN;
break;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
_3c=this.RunTimeData.ScoreScaled;
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
_3c=this.RunTimeData.ScoreRaw;
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
_3c=this.RunTimeData.ScoreMax;
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
_3c=this.RunTimeData.ScoreMin;
break;
case "cmi.session_time":
this.WriteDetailedLog("`1505`");
_3c=this.RunTimeData.SessionTime;
break;
case "cmi.success_status":
this.WriteDetailedLog("`1470`");
_3c=this.RunTimeData.SuccessStatus;
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1518`");
_3c=this.RunTimeData.SuspendData;
break;
case "cmi.time_limit_action":
this.WriteDetailedLog("`1419`");
_3c=this.LearningObject.TimeLimitAction;
break;
case "cmi.total_time":
this.WriteDetailedLog("`1547`");
_3c=this.RunTimeData.TotalTime;
break;
case "adl.nav.request":
this.WriteDetailedLog("`1441`");
_3c=this.RunTimeData.NavRequest;
break;
case "adl.nav.request_valid.continue":
this.WriteDetailedLog("`1168`");
_3c=this.TranslateBooleanIntoCMI(Control.IsContinueRequestValid());
break;
case "adl.nav.request_valid.previous":
this.WriteDetailedLog("`1193`");
_3c=this.TranslateBooleanIntoCMI(Control.IsPreviousRequestValid());
break;
case "adl.nav.request_valid.choice":
this.WriteDetailedLog("`1335`");
Debug.AssertError("Entered invalid case in RunTimeApi_RetrieveGetValueData");
break;
default:
if(_38.indexOf("ssp")===0&&SSP_ENABLED){
_3c=this.SSPApi.RetrieveGetValueData(_37,_38,_39,_3a);
}else{
Debug.AssertError("Entered default case in RunTimeApi_RetrieveGetValueData");
_3c="";
}
break;
}
return _3c;
}
function RunTimeApi_StoreValue(_3e,_3f,_40,_41,_42){
this.WriteDetailedLog("`1707`"+_3e+", "+_3f+", "+_40+", "+_41+", "+_42+") ");
var _43=true;
switch(_40){
case "cmi._version":
this.WriteDetailedLog("`1572`");
break;
case "cmi.comments_from_learner._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_learner._children");
_43=false;
break;
case "cmi.comments_from_learner._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_learner._count");
_43=false;
break;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1228`");
this.CheckCommentsCollectionLength(_41);
this.RunTimeData.Comments[_41].SetCommentValue(_3f);
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1179`");
this.CheckCommentsCollectionLength(_41);
this.RunTimeData.Comments[_41].Location=_3f;
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1165`");
this.CheckCommentsCollectionLength(_41);
this.RunTimeData.Comments[_41].Timestamp=_3f;
break;
case "cmi.comments_from_lms._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms._children");
_43=false;
break;
case "cmi.comments_from_lms._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms._count");
_43=false;
break;
case "cmi.comments_from_lms.n.comment":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms.comment");
_43=false;
break;
case "cmi.comments_from_lms.n.location":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms.location");
_43=false;
break;
case "cmi.comments_from_lms.n.timestamp":
Debug.AssertError("ERROR - Element is Read Only, cmi.comments_from_lms.timestamp");
_43=false;
break;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
var _44={ev:"Set",k:"completion",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.CompletionStatus,_3f);
this.RunTimeData.CompletionStatus=_3f;
this.RunTimeData.CompletionStatusChangedDuringRuntime=true;
break;
case "cmi.completion_threshold":
Debug.AssertError("ERROR - Element is Read Only, cmi.completion_threshold");
_43=false;
break;
case "cmi.credit":
Debug.AssertError("ERROR - Element is Read Only, cmi.credit");
_43=false;
break;
case "cmi.entry":
Debug.AssertError("ERROR - Element is Read Only, cmi.entry");
_43=false;
break;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
var _44={ev:"Set",k:"cmi.exit",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.Exit=_3f;
break;
case "cmi.interactions._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions._children");
_43=false;
break;
case "cmi.interactions._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions._count");
_43=false;
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1444`");
var _44={ev:"Set",k:"interactions id",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Id=_3f;
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1402`");
var _44={ev:"Set",k:"interactions type",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Type=_3f;
break;
case "cmi.interactions.n.objectives._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions.n.objectives._count");
_43=false;
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1272`");
var _44={ev:"Set",k:"interactions objectives id",i:_41,si:_42,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionObjectivesCollectionLength(_41,_42);
this.RunTimeData.Interactions[_41].Objectives[_42]=_3f;
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1337`");
var _44={ev:"Set",k:"interactions timestamp",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Timestamp=_3f;
break;
case "cmi.interactions.n.correct_responses._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.interactions.n.correct_responses._count");
_43=false;
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`1028`");
var _44={ev:"Set",k:"interactions correct_responses pattern",i:_41,si:_42,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCorrectResponsesCollectionLength(_41,_42);
this.RunTimeData.Interactions[_41].CorrectResponses[_42]=_3f;
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1329`");
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Weighting=_3f;
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1210`");
var _44={ev:"Set",k:"interactions learner_response",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].LearnerResponse=_3f;
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1378`");
var _44={ev:"Set",k:"interactions result",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Result=_3f;
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1362`");
var _44={ev:"Set",k:"interactions latency",i:_41,vh:ConvertIso8601TimeSpanToHundredths(_3f)};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Latency=_3f;
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1297`");
var _44={ev:"Set",k:"interactions description",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_41].Id){
_44.intid=this.RunTimeData.Interactions[_41].Id;
}
this.WriteHistoryLog("",_44);
this.CheckInteractionsCollectionLength(_41);
this.RunTimeData.Interactions[_41].Description=_3f;
break;
case "cmi.launch_data":
Debug.AssertError("ERROR - Element is Read Only, cmi.launch_data");
_43=false;
break;
case "cmi.learner_id":
Debug.AssertError("ERROR - Element is Read Only, cmi.learner_id");
_43=false;
break;
case "cmi.learner_name":
Debug.AssertError("ERROR - Element is Read Only, cmi.learner_name");
_43=false;
break;
case "cmi.learner_preference._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.learner_preference._children");
_43=false;
break;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1509`");
this.RunTimeData.AudioLevel=_3f;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.AudioLevel=_3f;
}
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1574`");
this.RunTimeData.LanguagePreference=_3f;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.LanguagePreference=_3f;
}
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1483`");
this.RunTimeData.DeliverySpeed=_3f;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.DeliverySpeed=_3f;
}
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1436`");
this.RunTimeData.AudioCaptioning=_3f;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.AudioCaptioning=_3f;
}
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
this.RunTimeData.Location=_3f;
break;
case "cmi.max_time_allowed":
Debug.AssertError("ERROR - Element is Read Only, cmi.max_time_allowed");
_43=false;
break;
case "cmi.mode":
Debug.AssertError("ERROR - Element is Read Only, cmi.mode");
_43=false;
break;
case "cmi.objectives._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.objectives._children");
_43=false;
break;
case "cmi.objectives._count":
Debug.AssertError("ERROR - Element is Read Only, cmi.objectives._count");
_43=false;
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1493`");
var _44={ev:"Set",k:"objectives id",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].Identifier,_3f);
this.RunTimeData.Objectives[_41].Identifier=_3f;
break;
case "cmi.objectives.n.score._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.objectives.n.score._children");
_43=false;
break;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1311`");
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].ScoreScaled,_3f);
this.RunTimeData.Objectives[_41].ScoreScaled=_3f;
this.RunTimeData.Objectives[_41].MeasureChangedDuringRuntime=true;
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1359`");
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].ScoreRaw,_3f);
this.RunTimeData.Objectives[_41].ScoreRaw=_3f;
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1357`");
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].ScoreMin,_3f);
this.RunTimeData.Objectives[_41].ScoreMin=_3f;
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1358`");
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].ScoreMax,_3f);
this.RunTimeData.Objectives[_41].ScoreMax=_3f;
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1275`");
var _44={ev:"Set",k:"objectives success",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Objectives[_41].Identifier){
_44.intid=this.RunTimeData.Objectives[_41].Identifier;
}
this.WriteHistoryLog("",_44);
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].SuccessStatus,_3f);
this.RunTimeData.Objectives[_41].SuccessStatus=_3f;
this.RunTimeData.Objectives[_41].SuccessStatusChangedDuringRuntime=true;
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1221`");
var _44={ev:"Set",k:"objectives completion",i:_41,v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Objectives[_41].Identifier){
_44.intid=this.RunTimeData.Objectives[_41].Identifier;
}
this.WriteHistoryLog("",_44);
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].CompletionStatus,_3f);
this.RunTimeData.Objectives[_41].CompletionStatus=_3f;
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1242`");
this.CheckObjectivesCollectionLength(_41);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.Objectives[_41].ProgressMeasure,_3f);
this.RunTimeData.Objectives[_41].ProgressMeasure=_3f;
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1324`");
this.CheckObjectivesCollectionLength(_41);
this.RunTimeData.Objectives[_41].Description=_3f;
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1426`");
this.RunTimeData.ProgressMeasure=_3f;
break;
case "cmi.scaled_passing_score":
Debug.AssertError("ERROR - Element is Read Only, cmi.scaled_passing_score");
_43=false;
break;
case "cmi.score._children":
Debug.AssertError("ERROR - Element is Read Only, cmi.score._children");
_43=false;
break;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
var _44={ev:"Set",k:"score.scaled",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.ScoreScaled,_3f);
this.RunTimeData.ScoreScaled=_3f;
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
var _44={ev:"Set",k:"score.raw",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.ScoreRaw=_3f;
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
var _44={ev:"Set",k:"score.max",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.ScoreMax=_3f;
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
var _44={ev:"Set",k:"score.min",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.ScoreMin=_3f;
break;
case "cmi.session_time":
this.WriteDetailedLog("`1515`");
var _44={ev:"Set",k:"session time",vh:ConvertIso8601TimeSpanToHundredths(_3f)};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.SessionTime=_3f;
break;
case "cmi.success_status":
this.WriteDetailedLog("`1480`");
var _44={ev:"Set",k:"success",v:_3f};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_44);
this.SetLookAheadDirtyDataFlagIfNeeded(this.RunTimeData.SuccessStatus,_3f);
this.RunTimeData.SuccessStatus=_3f;
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1513`");
this.RunTimeData.SuspendData=_3f;
break;
case "cmi.time_limit_action":
Debug.AssertError("ERROR - Element is Read Only, cmi.time_limit_action");
_43=false;
break;
case "cmi.total_time":
Debug.AssertError("ERROR - Element is Read Only, cmi.total_time");
_43=false;
break;
case "adl.nav.request":
this.WriteDetailedLog("`1455`");
var _44={ev:"Set",k:"nav.request"};
if(this.Activity){
_44.ai=this.Activity.ItemIdentifier;
}
var _45=_3f.match(/target\s*=\s*(\S+)\s*}\s*choice/);
if(_45){
_44.tai=_45[1];
_44.tat=Control.Activities.GetActivityFromIdentifier(_45[1]).LearningObject.Title;
_44.v="choice";
}else{
_44.v=_3f;
}
this.WriteHistoryLog("",_44);
this.RunTimeData.NavRequest=_3f;
break;
case "adl.nav.request_valid.continue":
Debug.AssertError("ERROR - Element is Read Only, adl.nav.request_valid.continue");
break;
case "adl.nav.request_valid.previous":
Debug.AssertError("ERROR - Element is Read Only, adl.nav.request_valid.previous");
break;
case "adl.nav.request_valid.choice":
Debug.AssertError("ERROR - Should never get here...handled above - Element is Read Only, adl.nav.request_valid.choice");
break;
default:
if(_40.indexOf("ssp")===0){
if(SSP_ENABLED){
return this.SSPApi.StoreValue(_3e,_3f,_40,_41,_42);
}
}
Debug.AssertError("ERROR reached default case in RunTimeApi_StoreValue");
returnData="";
break;
}
}
function RunTimeApi_CheckCommentsCollectionLength(_46){
if(this.RunTimeData.Comments.length<=_46){
this.WriteDetailedLog("`1130`"+_46);
this.RunTimeData.Comments[_46]=new ActivityRunTimeComment(null,null,null,null,null);
}
}
function RunTimeApi_CheckInteractionsCollectionLength(_47){
if(this.RunTimeData.Interactions.length<=_47){
this.WriteDetailedLog("`1299`"+_47);
this.RunTimeData.Interactions[_47]=new ActivityRunTimeInteraction(null,null,null,null,null,null,null,null,null,new Array(),new Array());
}
}
function RunTimeApi_CheckInteractionObjectivesCollectionLength(_48,_49){
if(this.RunTimeData.Interactions[_48].Objectives.length<=_49){
this.WriteDetailedLog("`1110`"+_49);
this.RunTimeData.Interactions[_48].Objectives[_49]=null;
}
}
function RunTimeApi_CheckInteractionsCorrectResponsesCollectionLength(_4a,_4b){
if(this.RunTimeData.Interactions[_4a].CorrectResponses.length<=_4b){
this.WriteDetailedLog("`990`"+_4b);
this.RunTimeData.Interactions[_4a].CorrectResponses[_4b]=null;
}
}
function RunTimeApi_CheckObjectivesCollectionLength(_4c){
if(this.RunTimeData.Objectives.length<=_4c){
this.WriteDetailedLog("`1342`"+_4c);
this.RunTimeData.Objectives[_4c]=new ActivityRunTimeObjective(null,"unknown","unknown",null,null,null,null,null,null);
}
}
function RunTimeApi_SetErrorState(_4d,_4e){
if(_4d!=SCORM2004_NO_ERROR){
this.WriteDetailedLog("`1281`"+_4d+" - "+_4e);
}
this.ErrorNumber=_4d;
this.ErrorString=SCORM2004_ErrorStrings[_4d];
this.ErrorDiagnostic=_4e;
}
function RunTimeApi_ClearErrorState(){
this.SetErrorState(SCORM2004_NO_ERROR,"");
}
function RunTimeApi_CheckForInitializeError(arg){
this.WriteDetailedLog("`1420`");
if(this.Initialized){
this.SetErrorState(SCORM2004_ALREADY_INTIAILIZED_ERROR,"Initialize has already been called and may only be called once per session.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_CONTENT_INSTANCE_TERMINATED_ERROR,"Initialize cannot be called after Terminate has already beeen called.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM2004_GENERAL_ARGUMENT_ERROR,"The argument to Initialize must be an empty string (\"\"). The argument '"+arg+"' is invalid.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForTerminateError(arg){
this.WriteDetailedLog("`1437`");
if(!this.Initialized){
this.SetErrorState(SCORM2004_TERMINATION_BEFORE_INITIALIZATION_ERROR,"Terminate cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_TERMINATION_AFTER_TERMINATION_ERROR,"Terminate cannot be called after Terminate has already beeen called.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM2004_GENERAL_ARGUMENT_ERROR,"The argument to Terminate must be an empty string (\"\"). The argument '"+arg+"' is invalid.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForCommitError(arg){
this.WriteDetailedLog("`1487`");
if(!this.Initialized){
this.SetErrorState(SCORM2004_COMMIT_BEFORE_INITIALIZATION_ERROR,"Commit cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_COMMIT_AFTER_TERMINATION_ERROR,"Commit cannot be called after Terminate has already beeen called.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM2004_GENERAL_ARGUMENT_ERROR,"The argument to Commit must be an empty string (\"\"). The argument '"+arg+"' is invalid.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckMaxLength(_52,_53){
switch(_52){
case "`1304`":
this.CheckLengthAndWarn(_53,4250);
break;
case "cmi.comments_from_learner.n.location":
this.CheckLengthAndWarn(_53,250);
break;
case "cmi.interactions.n.id":
this.CheckLengthAndWarn(_53,4000);
break;
case "cmi.interactions.n.objectives.n.id":
this.CheckLengthAndWarn(_53,4000);
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.CheckLengthAndWarn(_53,7800);
break;
case "cmi.interactions.n.learner_response":
this.CheckLengthAndWarn(_53,7800);
break;
case "cmi.interactions.n.description":
this.CheckLengthAndWarn(_53,500);
break;
case "cmi.learner_preference.language":
this.CheckLengthAndWarn(_53,250);
break;
case "cmi.location":
this.CheckLengthAndWarn(_53,1000);
break;
case "cmi.objectives.n.id":
this.CheckLengthAndWarn(_53,4000);
break;
case "cmi.objectives.n.description":
this.CheckLengthAndWarn(_53,500);
break;
case "cmi.suspend_data":
this.CheckLengthAndWarn(_53,4000);
break;
default:
break;
}
return;
}
function RunTimeApi_CheckLengthAndWarn(str,len){
if(str.length>len){
this.SetErrorState(SCORM2004_NO_ERROR,"The string was trimmed to fit withing the SPM of "+len+" characters.");
}
return;
}
function RunTimeApi_CheckForGetValueError(_56,_57,_58,_59){
this.WriteDetailedLog("`1454`");
if(!this.Initialized){
this.SetErrorState(SCORM2004_RETRIEVE_DATA_BEFORE_INITIALIZATION_ERROR,"GetValue cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_RETRIEVE_DATA_AFTER_TERMINATION_ERROR,"GetValue cannot be called after Terminate has already beeen called.");
return false;
}
if(_56.length===0){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The data model element for GetValue was not specified.");
return false;
}
if(_58!==""){
if(_57.indexOf("cmi.comments_from_learner")>=0){
if(_58>=this.RunTimeData.Comments.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Comments From Learner collection does not have an element at index "+_58+", the current element count is "+this.RunTimeData.Comments.length+".");
return false;
}
}else{
if(_57.indexOf("cmi.comments_from_lms")>=0){
if(_58>=this.RunTimeData.CommentsFromLMS.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Comments From LMS collection does not have an element at index "+_58+", the current element count is "+this.RunTimeData.CommentsFromLMS.length+".");
return false;
}
}else{
if(_57.indexOf("cmi.objectives")>=0){
if(_58>=this.RunTimeData.Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Objectives collection does not have an element at index "+_58+", the current element count is "+this.RunTimeData.Objectives.length+".");
return false;
}
}else{
if(_57.indexOf("cmi.interactions")>=0){
if(_58>=this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Interactions collection does not have an element at index "+_58+", the current element count is "+this.RunTimeData.Interactions.length+".");
return false;
}
if(_57.indexOf("cmi.interactions.n.correct_responses")>=0){
if(_59!==""){
if(_59>=this.RunTimeData.Interactions[_58].CorrectResponses.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Correct Responses collection for Interaction #"+_58+" does not have "+_59+" elements in it, the current element count is "+this.RunTimeData.Interactions[_58].CorrectResponses.length+".");
return false;
}
}
}else{
if(_57.indexOf("cmi.interactions.n.objectives")>=0){
if(_59!==""){
if(_59>=this.RunTimeData.Interactions[_58].Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_GET_FAILURE_ERROR,"The Objectives collection for Interaction #"+_58+" does not have "+_59+" elements in it, the current element count is "+this.RunTimeData.Interactions[_58].Objectives.length+".");
return false;
}
}
}
}
}
}
}
}
}
switch(_57){
case "cmi._version":
this.WriteDetailedLog("`1600`");
break;
case "cmi.comments_from_learner._children":
this.WriteDetailedLog("`1196`");
break;
case "cmi.comments_from_learner._count":
this.WriteDetailedLog("`1226`");
break;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1154`");
if(this.RunTimeData.Comments[_58].Comment===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Comment field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1139`");
if(this.RunTimeData.Comments[_58].Location===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Location field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1113`");
if(this.RunTimeData.Comments[_58].Timestamp===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The TimeStamp field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.comments_from_lms._children":
this.WriteDetailedLog("`1244`");
break;
case "cmi.comments_from_lms._count":
this.WriteDetailedLog("`1289`");
break;
case "cmi.comments_from_lms.n.comment":
this.WriteDetailedLog("`1252`");
if(this.RunTimeData.CommentsFromLMS[_58].Comment===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Comment field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.comments_from_lms.n.location":
this.WriteDetailedLog("`1235`");
if(this.RunTimeData.CommentsFromLMS[_58].Location===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Location field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.comments_from_lms.n.timestamp":
this.WriteDetailedLog("`1218`");
if(this.RunTimeData.CommentsFromLMS[_58].Timestamp===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Timestamp field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
break;
case "cmi.completion_threshold":
this.WriteDetailedLog("`1363`");
if(this.LearningObject.CompletionThreshold===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The completion threshold for this SCO was not specificed.");
return false;
}
break;
case "cmi.credit":
this.WriteDetailedLog("`1617`");
break;
case "cmi.entry":
this.WriteDetailedLog("`1643`");
break;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR,"The Exit data model element is write-only.");
return false;
case "cmi.interactions._children":
this.WriteDetailedLog("`1321`");
break;
case "cmi.interactions._count":
this.WriteDetailedLog("`1380`");
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1414`");
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1371`");
if(this.RunTimeData.Interactions[_58].Type===null||this.RunTimeData.Interactions[_58].Type===""){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Type field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.objectives._count":
this.WriteDetailedLog("`1146`");
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1199`");
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1294`");
if(this.RunTimeData.Interactions[_58].Timestamp===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Time Stamp field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.correct_responses._count":
this.WriteDetailedLog("`999`");
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`973`");
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1295`");
if(this.RunTimeData.Interactions[_58].Weighting===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Weighting field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1180`");
if(this.RunTimeData.Interactions[_58].LearnerResponse===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Learner Response field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1348`");
if(this.RunTimeData.Interactions[_58].Result===null||this.RunTimeData.Interactions[_58].Result===""){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Result field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1323`");
if(this.RunTimeData.Interactions[_58].Latency===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Latency field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1270`");
if(this.RunTimeData.Interactions[_58].Description===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Description field has not been initialized for the element at index "+_58);
return false;
}
break;
case "cmi.launch_data":
this.WriteDetailedLog("`1532`");
if(this.LearningObject.DataFromLms===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Launch Data field was not specified for this SCO.");
return false;
}
break;
case "cmi.learner_id":
this.WriteDetailedLog("`1542`");
break;
case "cmi.learner_name":
this.WriteDetailedLog("`1519`");
break;
case "cmi.learner_preference._children":
this.WriteDetailedLog("`1230`");
break;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1181`");
if(this.RunTimeData.AudioLevel===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Audio Level field has not been set for this SCO.");
return false;
}
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1241`");
if(this.RunTimeData.LanguagePreference===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Language Preference field has not been set for this SCO.");
return false;
}
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1109`");
if(this.RunTimeData.DeliverySpeed===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Delivery Speed field has not been set for this SCO.");
return false;
}
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1075`");
if(this.RunTimeData.AudioCaptioning===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Audio Captioning field has not been set for this SCO.");
return false;
}
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
if(this.RunTimeData.Location===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Location field has not been set for this SCO.");
return false;
}
break;
case "cmi.max_time_allowed":
this.WriteDetailedLog("`1439`");
if(this.LearningObject.SequencingData===null||this.LearningObject.SequencingData.LimitConditionAttemptAbsoluteDurationControl===false){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Max Time Allowed field was not specified in the manifest for this SCO.");
return false;
}
break;
case "cmi.mode":
this.WriteDetailedLog("`1676`");
break;
case "cmi.objectives._children":
this.WriteDetailedLog("`1365`");
break;
case "cmi.objectives._count":
this.WriteDetailedLog("`1407`");
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1445`");
break;
case "cmi.objectives.n.score._children":
this.WriteDetailedLog("`1229`");
break;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1282`");
if(this.RunTimeData.Objectives[_58].ScoreScaled===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Scaled Score field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1334`");
if(this.RunTimeData.Objectives[_58].ScoreRaw===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Raw Score field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1318`");
if(this.RunTimeData.Objectives[_58].ScoreMin===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Min Score field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1316`");
if(this.RunTimeData.Objectives[_58].ScoreMax===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Max Score field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1251`");
if(this.RunTimeData.Objectives[_58].SuccessStatus===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The SuccessStatus field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1197`");
if(this.RunTimeData.Objectives[_58].CompletionStatus===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The CompletionStatus field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1204`");
if(this.RunTimeData.Objectives[_58].ProgressMeasure===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The ProgressMeasure field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1290`");
if(this.RunTimeData.Objectives[_58].Description===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Description field has not been initialized for the objective at index "+_58);
return false;
}
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1426`");
if(this.RunTimeData.ProgressMeasure===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Progress Measure field has not been set for this SCO.");
return false;
}
break;
case "cmi.scaled_passing_score":
this.WriteDetailedLog("`1366`");
if(this.LearningObject.GetScaledPassingScore()===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Scaled Passing Score field was not specificed for this SCO.");
return false;
}
break;
case "cmi.score._children":
this.WriteDetailedLog("`1446`");
break;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
if(this.RunTimeData.ScoreScaled===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Scaled Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
if(this.RunTimeData.ScoreRaw===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Raw Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
if(this.RunTimeData.ScoreMax===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Max Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
if(this.RunTimeData.ScoreMin===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Min Score field has not been set for this SCO.");
return false;
}
break;
case "cmi.session_time":
this.WriteDetailedLog("`1515`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_WRITE_ONLY_ERROR,"The Exit data model element is write-only.");
break;
case "cmi.success_status":
this.WriteDetailedLog("`1480`");
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1513`");
if(this.RunTimeData.SuspendData===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Suspend Data field has not been set for this SCO.");
return false;
}
break;
case "cmi.time_limit_action":
this.WriteDetailedLog("`1411`");
if(this.LearningObject.TimeLimitAction===null){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_NOT_INITIALIZED_ERROR,"The Time Limit Action field has not been set for this SCO.");
return false;
}
break;
case "cmi.total_time":
this.WriteDetailedLog("`1545`");
break;
case "adl.nav.request":
this.WriteDetailedLog("`1455`");
break;
case "adl.nav.request_valid.continue":
this.WriteDetailedLog("`1176`");
break;
case "adl.nav.request_valid.previous":
this.WriteDetailedLog("`1183`");
break;
case "adl.nav.request_valid.choice":
Debug.AssertError("entered invalid case in scorm2004 check for getvalue error");
break;
default:
if(_57.indexOf("adl.nav.request_valid.choice.{")===0){
this.WriteDetailedLog("`1587`");
return true;
}
if(_57.indexOf("ssp")===0){
if(SSP_ENABLED){
return this.SSPApi.CheckForGetValueError(_56,_57,_58,_59);
}
}
this.SetErrorState(SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR,"The data model element '"+_56+"' does not exist.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForSetValueError(_5a,_5b,_5c,_5d,_5e){
this.WriteDetailedLog("`1526`"+_5a+", "+_5b+", "+_5c+", "+_5d+", "+_5e+") ");
if(!this.Initialized){
this.SetErrorState(SCORM2004_STORE_DATA_BEFORE_INITIALIZATION_ERROR,"SetValue cannot be called before Initialize has been called.");
return false;
}
if(this.Terminated){
this.SetErrorState(SCORM2004_STORE_DATA_AFTER_TERMINATION_ERROR,"SetValue cannot be called after Terminate has already beeen called.");
return false;
}
if(_5a.length===0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The data model element for SetValue was not specified.");
return false;
}
if(_5a.indexOf("adl.nav.request_valid.choice.{")===0){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.nav.request_valid.choice element is read only");
return false;
}
if(_5d!==""){
if(_5c.indexOf("cmi.comments_from_learner")>=0){
if(_5d>this.RunTimeData.Comments.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Comments From Learner collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.Comments.length+".");
return false;
}
}else{
if(_5c.indexOf("cmi.comments_from_lms")>=0){
if(_5d>this.RunTimeData.CommentsFromLMS.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Comments From LMS collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.CommentsFromLMS.length+".");
return false;
}
}else{
if(_5c.indexOf("cmi.objectives")>=0){
if(_5d>this.RunTimeData.Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Objectives collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.Objectives.length+".");
return false;
}
}else{
if(_5c.indexOf("cmi.interactions")>=0){
if(_5d>this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Interactions collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.Interactions.length+".");
return false;
}else{
if(_5c.indexOf("cmi.interactions.n.correct_responses")>=0){
if(_5d>=this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The Interactions collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.Interactions.length+".");
return false;
}
if(_5e!==""){
if(_5e>this.RunTimeData.Interactions[_5d].CorrectResponses.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Correct Responses collection elements for Interaction #"+_5d+" must be set sequentially the index "+_5e+" is greater than the next available index of "+this.RunTimeData.Interactions[_5d].CorrectResponses.length+".");
return false;
}
}
}else{
if(_5c.indexOf("cmi.interactions.n.objectives")>=0){
if(_5d>=this.RunTimeData.Interactions.length){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The Interactions collection elements must be set sequentially, the index "+_5d+", is greater than the next available index of "+this.RunTimeData.Interactions.length+".");
return false;
}
if(_5e!==""){
if(_5e>this.RunTimeData.Interactions[_5d].Objectives.length){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The Objectives collection elements for Interaction #"+_5d+" must be set sequentially the index "+_5e+" is greater than the next available index of "+this.RunTimeData.Interactions[_5d].Objectives.length+".");
return false;
}
}
}
}
}
}
}
}
}
}
var _5f;
var i;
switch(_5c){
case "cmi._version":
this.WriteDetailedLog("`1572`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi._version data model element is read-only");
return false;
case "cmi.comments_from_learner._children":
this.WriteDetailedLog("`1159`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_learner._children data model element is read-only");
return false;
case "cmi.comments_from_learner._count":
this.WriteDetailedLog("`1226`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_learner._count data model element is read-only");
return false;
case "cmi.comments_from_learner.n.comment":
this.WriteDetailedLog("`1154`");
if(!this.ValidLocalizedString(_5b,4000)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.comments_from_learner.n.comment data model element is not a valid localized string type (SPM 4000)");
return false;
}
break;
case "cmi.comments_from_learner.n.location":
this.WriteDetailedLog("`1139`");
if(!this.ValidCharString(_5b,250)){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"The cmi.comments_from_learner.n.comment data model element is not a valid char string type (SPM 250)");
return false;
}
break;
case "cmi.comments_from_learner.n.timestamp":
this.WriteDetailedLog("`1113`");
if(!this.ValidTime(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.comments_from_learner.n.timestamp data model element is not a valid time");
return false;
}
break;
case "cmi.comments_from_lms._children":
this.WriteDetailedLog("`1244`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms._children data model element is read-only");
return false;
case "cmi.comments_from_lms._count":
this.WriteDetailedLog("`1289`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms._count data model element is read-only");
return false;
case "cmi.comments_from_lms.n.comment":
this.WriteDetailedLog("`1252`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms.comment data model element is read-only");
return false;
case "cmi.comments_from_lms.n.location":
this.WriteDetailedLog("`1235`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms.location data model element is read-only");
return false;
case "cmi.comments_from_lms.n.timestamp":
this.WriteDetailedLog("`1218`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.comments_from_lms.timestamp data model element is read-only");
return false;
case "cmi.completion_status":
this.WriteDetailedLog("`1413`");
if(_5b!=SCORM_STATUS_COMPLETED&&_5b!=SCORM_STATUS_INCOMPLETE&&_5b!=SCORM_STATUS_NOT_ATTEMPTED&&_5b!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The completion_status data model element must be a proper vocabulary element.");
return false;
}
break;
case "cmi.completion_threshold":
this.WriteDetailedLog("`1363`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The completion_threshold data model element is read-only");
return false;
case "cmi.credit":
this.WriteDetailedLog("`1617`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The credit data model element is read-only");
return false;
case "cmi.entry":
this.WriteDetailedLog("`1643`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The entry data model element is read-only");
return false;
case "cmi.exit":
this.WriteDetailedLog("`1669`");
if(_5b!=SCORM_EXIT_TIME_OUT&&_5b!=SCORM_EXIT_SUSPEND&&_5b!=SCORM_EXIT_LOGOUT&&_5b!=SCORM_EXIT_NORMAL&&_5b!=SCORM_EXIT_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The exit data model element must be a proper vocabulary element.");
return false;
}
break;
case "cmi.interactions._children":
this.WriteDetailedLog("`1321`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions._children element is read-only");
return false;
case "cmi.interactions._count":
this.WriteDetailedLog("`1380`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions._count element is read-only");
return false;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1414`");
if(!this.ValidLongIdentifier(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".id value of '"+_5b+"' is not a valid long identifier.");
return false;
}
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1371`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(_5b!=SCORM_TRUE_FALSE&&_5b!=SCORM_CHOICE&&_5b!=SCORM_FILL_IN&&_5b!=SCORM_LONG_FILL_IN&&_5b!=SCORM_LIKERT&&_5b!=SCORM_MATCHING&&_5b!=SCORM_PERFORMANCE&&_5b!=SCORM_SEQUENCING&&_5b!=SCORM_NUMERIC&&_5b!=SCORM_OTHER){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".type value of '"+_5b+"' is not a valid interaction type.");
return false;
}
break;
case "cmi.interactions.n.objectives._count":
this.WriteDetailedLog("`1146`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions.objectives._count element is read-only");
return false;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1199`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidLongIdentifier(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".objectives."+_5e+".id value of '"+_5b+"' is not a valid long identifier type.");
return false;
}
for(i=0;i<this.RunTimeData.Interactions[_5d].Objectives.length;i++){
if((this.RunTimeData.Interactions[_5d].Objectives[i]==_5b)&&(i!=_5e)){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Every interaction objective identifier must be unique. The value '"+_5b+"' has already been set in objective #"+i);
return false;
}
}
break;
case "cmi.interactions.n.timestamp":
this.WriteDetailedLog("`1294`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidTime(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".timestamp value of '"+_5b+"' is not a valid time type.");
return false;
}
break;
case "cmi.interactions.n.correct_responses._count":
this.WriteDetailedLog("`999`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The interactions.correct_responses._count element is read-only");
return false;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`973`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Type===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.type element must be set before a correct response can be set.");
return false;
}
_5f=true;
if(RegistrationToDeliver.Package.Properties.ValidateInteractionResponses){
switch(this.RunTimeData.Interactions[_5d].Type){
case SCORM_TRUE_FALSE:
if(this.RunTimeData.Interactions[_5d].CorrectResponses.length>0&&_5e>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"A true-false interaction can only have one correct response.");
return false;
}
_5f=this.ValidTrueFalseResponse(_5b);
break;
case SCORM_CHOICE:
_5f=this.ValidMultipleChoiceResponse(_5b);
for(i=0;i<this.RunTimeData.Interactions[_5d].CorrectResponses.length;i++){
if(this.RunTimeData.Interactions[_5d].CorrectResponses[i]==_5b){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Every correct response to a choice interaction must be unique. The value '"+_5b+"' has already been set in correct response #"+i);
return false;
}
}
break;
case SCORM_FILL_IN:
_5f=this.ValidFillInResponse(_5b,true);
break;
case SCORM_LONG_FILL_IN:
_5f=this.ValidLongFillInResponse(_5b,true);
break;
case SCORM_LIKERT:
if(this.RunTimeData.Interactions[_5d].CorrectResponses.length>0&&_5e>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"A likert interaction can only have one correct response.");
return false;
}
_5f=this.ValidLikeRTResponse(_5b);
break;
case SCORM_MATCHING:
_5f=this.ValidMatchingResponse(_5b);
break;
case SCORM_PERFORMANCE:
_5f=this.ValidPerformanceResponse(_5b,true);
break;
case SCORM_SEQUENCING:
_5f=this.ValidSequencingResponse(_5b);
break;
case SCORM_NUMERIC:
if(this.RunTimeData.Interactions[_5d].CorrectResponses.length>0&&_5e>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"A numeric interaction can only have one correct response.");
return false;
}
_5f=this.ValidNumericResponse(_5b,true);
break;
case SCORM_OTHER:
if(this.RunTimeData.Interactions[_5d].CorrectResponses.length>0&&_5e>0){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"An 'other' interaction can only have one correct response.");
return false;
}
_5f=this.ValidOtheresponse(_5b);
break;
}
}
if(!_5f){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".correct_responses."+_5e+".pattern value of '"+_5b+"' is not a valid correct response to an interaction of type "+this.RunTimeData.Interactions[_5d].Type+".");
return false;
}
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1295`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".weighting value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.interactions.n.learner_response":
this.WriteDetailedLog("`1161`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(this.RunTimeData.Interactions[_5d].Type===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.type element must be set before a learner response can be set.");
return false;
}
_5f=true;
if(RegistrationToDeliver.Package.Properties.ValidateInteractionResponses){
switch(this.RunTimeData.Interactions[_5d].Type){
case "true-false":
_5f=this.ValidTrueFalseResponse(_5b);
break;
case "choice":
_5f=this.ValidMultipleChoiceResponse(_5b);
break;
case "fill-in":
_5f=this.ValidFillInResponse(_5b,false);
break;
case "long-fill-in":
_5f=this.ValidLongFillInResponse(_5b,false);
break;
case "likert":
_5f=this.ValidLikeRTResponse(_5b);
break;
case "matching":
_5f=this.ValidMatchingResponse(_5b);
break;
case "performance":
_5f=this.ValidPerformanceResponse(_5b,false);
break;
case "sequencing":
_5f=this.ValidSequencingResponse(_5b);
break;
case "numeric":
_5f=this.ValidNumericResponse(_5b,false);
break;
case "other":
_5f=this.ValidOtheresponse(_5b);
break;
}
}
if(!_5f){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".learner_response value of '"+_5b+"' is not a valid response to an interaction of type "+this.RunTimeData.Interactions[_5d].Type+".");
return false;
}
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1348`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(_5b!=SCORM_CORRECT&&_5b!=SCORM_INCORRECT&&_5b!=SCORM_UNANTICIPATED&&_5b!=SCORM_NEUTRAL){
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".result value of '"+_5b+"' is not a valid interaction result.");
return false;
}
}
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1323`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidTimeInterval(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".latency value of '"+_5b+"' is not a valid timespan.");
return false;
}
break;
case "cmi.interactions.n.description":
this.WriteDetailedLog("`1270`");
if(this.RunTimeData.Interactions[_5d]===undefined||this.RunTimeData.Interactions[_5d].Id===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The interactions.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidLocalizedString(_5b,250)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.interactions."+_5d+".description value of '"+_5b+"' is not a valid localized string SPM 250.");
return false;
}
break;
case "cmi.launch_data":
this.WriteDetailedLog("`1532`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.launch_data element is read-only");
return false;
case "cmi.learner_id":
this.WriteDetailedLog("`1542`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.learner_id element is read-only");
return false;
case "cmi.learner_name":
this.WriteDetailedLog("`1519`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.learner_name element is read-only");
return false;
case "cmi.learner_preference._children":
this.WriteDetailedLog("`1230`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.learner_preference._children element is read-only");
return false;
case "cmi.learner_preference.audio_level":
this.WriteDetailedLog("`1181`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.audio_level value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<0){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.learner_preference.audio_level value of '"+_5b+"' must be greater than zero.");
return false;
}
break;
case "cmi.learner_preference.language":
this.WriteDetailedLog("`1241`");
if(!this.ValidLanguage(_5b,true)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.language value of '"+_5b+"' is not a valid language.");
return false;
}
break;
case "cmi.learner_preference.delivery_speed":
this.WriteDetailedLog("`1109`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.delivery_speed value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<0){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.learner_preference.delivery_speed value of '"+_5b+"' must be greater than zero.");
return false;
}
break;
case "cmi.learner_preference.audio_captioning":
this.WriteDetailedLog("`1075`");
if(_5b!="-1"&&_5b!="0"&&_5b!="1"){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.learner_preference.audio_captioning value of '"+_5b+"' must be -1, 0 or 1.");
return false;
}
break;
case "cmi.location":
this.WriteDetailedLog("`1576`");
if(!this.ValidCharString(_5b,1000)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.location value of '"+_5b+"' is not a valid char string SPM 1000.");
return false;
}
break;
case "cmi.max_time_allowed":
this.WriteDetailedLog("`1439`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.max_time_allowed element is read only");
return false;
case "cmi.mode":
this.WriteDetailedLog("`1676`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.mode element is read only");
return false;
case "cmi.objectives._children":
this.WriteDetailedLog("`1365`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.objectives._children element is read only");
return false;
case "cmi.objectives._count":
this.WriteDetailedLog("`1407`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.objectives._count element is read only");
return false;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1445`");
if(!this.ValidLongIdentifier(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives.n.id value of '"+_5b+"' is not a valid long identifier.");
return false;
}
for(i=0;i<this.RunTimeData.Objectives.length;i++){
if((this.RunTimeData.Objectives[i].Identifier==_5b)&&(i!=_5d)){
this.SetErrorState(SCORM2004_GENERAL_SET_FAILURE_ERROR,"Every objective identifier must be unique. The value '"+_5b+"' has already been set in objective #"+i);
return false;
}
}
break;
case "cmi.objectives.n.score._children":
this.WriteDetailedLog("`1229`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.objectives.n.score._children element is read only");
return false;
case "cmi.objectives.n.score.scaled":
this.WriteDetailedLog("`1282`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".score.scaled value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<-1||parseFloat(_5b)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.objectives."+_5d+".score.scaled value of '"+_5b+"' must be between -1 and 1.");
return false;
}
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1334`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".score.raw value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1318`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".score.min value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1316`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".score.max value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.objectives.n.success_status":
this.WriteDetailedLog("`1251`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(_5b!=SCORM_STATUS_PASSED&&_5b!=SCORM_STATUS_FAILED&&_5b!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".success_status value of '"+_5b+"' is not a valid success status.");
return false;
}
break;
case "cmi.objectives.n.completion_status":
this.WriteDetailedLog("`1197`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(_5b!=SCORM_STATUS_COMPLETED&&_5b!=SCORM_STATUS_INCOMPLETE&&_5b!=SCORM_STATUS_NOT_ATTEMPTED&&_5b!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".completion_status value of '"+_5b+"' is not a valid completion status.");
return false;
}
break;
case "cmi.objectives.n.progress_measure":
this.WriteDetailedLog("`1204`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".progress_measure value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<0||parseFloat(_5b)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.objectives."+_5d+".progress_measure value of '"+_5b+"' must be between 0 and 1.");
return false;
}
break;
case "cmi.objectives.n.description":
this.WriteDetailedLog("`1290`");
if(this.RunTimeData.Objectives[_5d]===undefined||this.RunTimeData.Objectives[_5d].Identifier===null){
this.SetErrorState(SCORM2004_DATA_MODEL_DEPENDENCY_NOT_ESTABLISHED_ERROR,"The objectives.id element must be set before other elements can be set.");
return false;
}
if(!this.ValidLocalizedString(_5b,250)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.objectives."+_5d+".description value of '"+_5b+"' is not a valid localized string SPM 250.");
return false;
}
break;
case "cmi.progress_measure":
this.WriteDetailedLog("`1426`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.progress_measure value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<0||parseFloat(_5b)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi.pogress_measure value of '"+_5b+"' must be between 0 and 1.");
return false;
}
break;
case "cmi.scaled_passing_score":
this.WriteDetailedLog("`1366`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.scaled_passing_score element is read only");
return false;
case "cmi.score._children":
this.WriteDetailedLog("`1446`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.score._children element is read only");
return false;
case "cmi.score.scaled":
this.WriteDetailedLog("`1499`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.scaled value of '"+_5b+"' is not a valid real number.");
return false;
}
if(parseFloat(_5b)<-1||parseFloat(_5b)>1){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_VALUE_OUT_OF_RANGE_ERROR,"The cmi..score.scaled value of '"+_5b+"' must be between -1 and 1.");
return false;
}
break;
case "cmi.score.raw":
this.WriteDetailedLog("`1564`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.raw value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.score.max":
this.WriteDetailedLog("`1561`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.raw value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.score.min":
this.WriteDetailedLog("`1563`");
if(!this.ValidReal(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.score.raw value of '"+_5b+"' is not a valid real number.");
return false;
}
break;
case "cmi.session_time":
this.WriteDetailedLog("`1515`");
if(!this.ValidTimeInterval(_5b)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.session_time value of '"+_5b+"' is not a valid time intervals.");
return false;
}
break;
case "cmi.success_status":
this.WriteDetailedLog("`1480`");
if(_5b!=SCORM_STATUS_PASSED&&_5b!=SCORM_STATUS_FAILED&&_5b!=SCORM_STATUS_UNKNOWN){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.success_status value of '"+_5b+"' is not a valid success status.");
return false;
}
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1513`");
if(!this.ValidCharString(_5b,4000)){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The cmi.suspend_data value of '"+_5b+"' is not a valid char string SPM 4000.");
return false;
}
break;
case "cmi.time_limit_action":
this.WriteDetailedLog("`1411`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.time_limit_action element is read only");
return false;
case "cmi.total_time":
this.WriteDetailedLog("`1545`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The cmi.total_time element is read only");
return false;
case "adl.nav.request":
this.WriteDetailedLog("`1455`");
if(_5b.substring(0,1)=="{"){
var _61=_5b.substring(0,_5b.indexOf("}")+1);
if(Control.IsTargetValid(_61)===false){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The value '"+_61+"' is not a valid target of a choice request.");
return false;
}
if(_5b.indexOf("choice")!=_61.length){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"A target may only be provided for a choice request.");
return false;
}
}else{
if(_5b!=SCORM_RUNTIME_NAV_REQUEST_CONTINUE&&_5b!=SCORM_RUNTIME_NAV_REQUEST_PREVIOUS&&_5b!=SCORM_RUNTIME_NAV_REQUEST_CHOICE&&_5b!=SCORM_RUNTIME_NAV_REQUEST_EXIT&&_5b!=SCORM_RUNTIME_NAV_REQUEST_EXITALL&&_5b!=SCORM_RUNTIME_NAV_REQUEST_ABANDON&&_5b!=SCORM_RUNTIME_NAV_REQUEST_ABANDONALL&&_5b!=SCORM_RUNTIME_NAV_REQUEST_NONE){
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_TYPE_MISMATCH_ERROR,"The adl.nav.request value of '"+_5b+"' is not a valid nav request.");
return false;
}
}
break;
case "adl.nav.request_valid.continue":
this.WriteDetailedLog("`1176`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.nav.request_valid.continue element is read only");
return false;
case "adl.nav.request_valid.previous":
this.WriteDetailedLog("`1183`");
this.SetErrorState(SCORM2004_DATA_MODEL_ELEMENT_IS_READ_ONLY_ERROR,"The adl.nav.request_valid.previous element is read only");
return false;
case "adl.nav.request_valid.choice":
this.WriteDetailedLog("`1223`");
break;
default:
if(_5c.indexOf("ssp")===0){
if(SSP_ENABLED){
return this.SSPApi.CheckForSetValueError(_5a,_5b,_5c,_5d,_5e);
}
}
this.SetErrorState(SCORM2004_UNDEFINED_DATA_MODEL_ELEMENT_ERROR,"The data model element '"+_5a+"' is not defined in SCORM 2004");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_ValidCharString(str,_63){
this.WriteDetailedLog("`1466`");
return true;
}
function RunTimeApi_ValidLocalizedString(str,_65){
this.WriteDetailedLog("`1373`");
var _66;
var _67=new String();
var _68;
_66=str;
if(str.indexOf("{lang=")===0){
_68=str.indexOf("}");
if(_68>0){
_67=str.substr(0,_68);
_67=_67.replace(/\{lang=/,"");
_67=_67.replace(/\}/,"");
if(!this.ValidLanguage(_67,false)){
return false;
}
if(str.length>=(_68+2)){
_66=str.substring(_68+1);
}else{
_66="";
}
}
}
return true;
}
function RunTimeApi_ExtractLanguageDelimiterFromLocalizedString(str){
var _6a;
var _6b="";
if(str.indexOf("{lang=")===0){
_6a=str.indexOf("}");
if(_6a>0){
_6b=str.substr(0,_6a+1);
}
}
return _6b;
}
function RunTimeApi_ValidLanguage(str,_6d){
this.WriteDetailedLog("`1514`");
var _6e;
if(str.length===0){
if(_6d){
return true;
}else{
return false;
}
}
_6e=str.split("-");
for(var i=0;i<_6e.length;i++){
if(_6e[i].length>8){
return false;
}
if(_6e[i].length<2){
if(_6e[i]!="i"&&_6e[i]!="x"){
return false;
}
}
}
var _70=new Iso639LangCodes_LangCodes();
if(!_70.IsValid(_6e[0].toLowerCase())){
return false;
}
if(str.length>250){
return false;
}
return true;
}
function RunTimeApi_ValidLongIdentifier(str){
this.WriteDetailedLog("`1396`");
str=str.trim();
if(!this.ValidIdentifier(str)){
return false;
}
return true;
}
function RunTimeApi_ValidShortIdentifier(str){
this.WriteDetailedLog("`1382`");
if(!this.ValidIdentifier(str)){
return false;
}
return true;
}
function RunTimeApi_ValidIdentifier(str){
this.WriteDetailedLog("`1463`");
str=str.trim();
if(str.length===0){
return false;
}
if(str.toLowerCase().indexOf("urn:")===0){
return this.IsValidUrn(str);
}
if(str.search(/\w/)<0){
return false;
}
if(str.search(/[^\w\-\(\)\+\.\:\=\@\;\$\_\!\*\'\%]/)>=0){
return false;
}
return true;
}
function RunTimeApi_IsValidUrn(str){
this.WriteDetailedLog("`1568`");
var _75=str.split(":");
var nid=new String("");
var nss="";
if(_75.length>1){
nid=_75[1];
}else{
return false;
}
if(_75.length>2){
nss=_75[2];
}
if(nid.length===0){
return false;
}
if(nid.indexOf(" ")>0||nss.indexOf(" ")>0){
return false;
}
return true;
}
function RunTimeApi_ValidReal(str){
this.WriteDetailedLog("`1578`");
if(str.search(/[^.\d-]/)>-1){
return false;
}
if(str.search("-")>-1){
if(str.indexOf("-",1)>-1){
return false;
}
}
if(str.indexOf(".")!=str.lastIndexOf(".")){
return false;
}
if(str.search(/\d/)<0){
return false;
}
return true;
}
function RunTimeApi_ValidTime(str){
this.WriteDetailedLog("`1571`");
var _7a="";
var _7b="";
var day="";
var _7d="";
var _7e="";
var _7f="";
var _80="";
var _81="";
var _82="";
var _83="";
var _84;
str=new String(str);
var _85=/^(\d\d\d\d)(-(\d\d)(-(\d\d)(T(\d\d)(:(\d\d)(:(\d\d))?)?)?)?)?/;
if(str.search(_85)!==0){
return false;
}
if(str.substr(str.length-1,1).search(/[\-T\:]/)>=0){
return false;
}
var len=str.length;
if(len!=4&&len!=7&&len!=10&&len!=13&&len!=16&&len<19){
return false;
}
if(len>=5){
if(str.substr(4,1)!="-"){
return false;
}
}
if(len>=8){
if(str.substr(7,1)!="-"){
return false;
}
}
if(len>=11){
if(str.substr(10,1)!="T"){
return false;
}
}
if(len>=14){
if(str.substr(13,1)!=":"){
return false;
}
}
if(len>=17){
if(str.substr(16,1)!=":"){
return false;
}
}
var _87=str.match(_85);
_7a=_87[1];
_7b=_87[3];
day=_87[5];
_7d=_87[7];
_7e=_87[9];
_7f=_87[11];
if(str.length>19){
if(str.length<21){
return false;
}
if(str.substr(19,1)!="."){
return false;
}
_84=str.substr(20,1);
if(_84.search(/\d/)<0){
return false;
}else{
_80+=_84;
}
for(var i=21;i<str.length;i++){
_84=str.substr(i,1);
if((i==21)&&(_84.search(/\d/)===0)){
_80+=_84;
}else{
_81+=_84;
}
}
}
if(_81.length===0){
}else{
if(_81.length==1){
if(_81!="Z"){
return false;
}
}else{
if(_81.length==3){
if(_81.search(/[\+\-]\d\d/)!==0){
return false;
}else{
_82=_81.substr(1,2);
}
}else{
if(_81.length==6){
if(_81.search(/[\+\-]\d\d:\d\d/)!==0){
return false;
}else{
_82=_81.substr(1,2);
_83=_81.substr(4,2);
}
}else{
return false;
}
}
}
}
if(_7a<1970||_7a>2038){
return false;
}
if(_7b!==undefined&&_7b!==""){
_7b=parseInt(_7b,10);
if(_7b<1||_7b>12){
return false;
}
}
if(day!==undefined&&day!==""){
var dtm=new Date(_7a,(_7b-1),day);
if(dtm.getDate()!=day){
return false;
}
}
if(_7d!==undefined&&_7d!==""){
_7d=parseInt(_7d,10);
if(_7d<0||_7d>23){
return false;
}
}
if(_7e!==undefined&&_7e!==""){
_7e=parseInt(_7e,10);
if(_7e<0||_7e>59){
return false;
}
}
if(_7f!==undefined&&_7f!==""){
_7f=parseInt(_7f,10);
if(_7f<0||_7f>59){
return false;
}
}
if(_82!==undefined&&_82!==""){
_82=parseInt(_82,10);
if(_82<0||_82>23){
return false;
}
}
if(_83!==undefined&&_83!==""){
_83=parseInt(_83,10);
if(_83<0||_83>59){
return false;
}
}
return true;
}
function RunTimeApi_ValidTimeInterval(str){
this.WriteDetailedLog("`1425`");
var _8b=/^P(\d+Y)?(\d+M)?(\d+D)?(T(\d+H)?(\d+M)?(\d+(.\d\d?)?S)?)?$/;
if(str=="P"){
return false;
}
if(str.lastIndexOf("T")==(str.length-1)){
return false;
}
if(str.search(_8b)<0){
return false;
}
return true;
}
function RunTimeApi_ValidTrueFalseResponse(str){
this.WriteDetailedLog("`1346`");
var _8d=/^(true|false)$/;
if(str.search(_8d)<0){
return false;
}
return true;
}
function RunTimeApi_ValidMultipleChoiceResponse(str){
this.WriteDetailedLog("`1254`");
if(str.length===0){
return true;
}
return (this.IsValidArrayOfShortIdentifiers(str,36,true)||this.IsValidCommaDelimitedArrayOfShortIdentifiers(str,36,true));
}
function RunTimeApi_IsValidCommaDelimitedArrayOfShortIdentifiers(str,_90,_91){
this.WriteDetailedLog("`1205`");
var _92=",";
var _93=str.split(_92);
for(var i=0;i<_93.length;i++){
if(!this.ValidShortIdentifier(_93[i])){
return false;
}
if(_91){
for(var j=0;j<_93.length;j++){
if(j!=i){
if(_93[j]==_93[i]){
return false;
}
}
}
}
}
return true;
}
function RunTimeApi_IsValidArrayOfShortIdentifiers(str,_97,_98){
this.WriteDetailedLog("`1205`");
var _99="[,]";
var _9a=str.split(_99);
for(var i=0;i<_9a.length;i++){
if(!this.ValidShortIdentifier(_9a[i])){
return false;
}
if(_98){
for(var j=0;j<_9a.length;j++){
if(j!=i){
if(_9a[j]==_9a[i]){
return false;
}
}
}
}
}
return true;
}
function RunTimeApi_IsValidArrayOfLocalizedStrings(str,_9e,_9f){
this.WriteDetailedLog("`1212`");
var _a0="[,]";
var _a1=str.split(_a0);
for(var i=0;i<_a1.length;i++){
if(!this.ValidLocalizedString(_a1[i],0)){
return false;
}
if(_9f){
for(var j=0;j<_a1.length;j++){
if(j!=i){
if(_a1[j]==_a1[i]){
return false;
}
}
}
}
}
return true;
}
function RunTimeApi_ValidFillInResponse(str,_a5){
this.WriteDetailedLog("`1391`");
if(str.length===0){
return true;
}
var _a6=/^\{case_matters=/;
var _a7=/^\{order_matters=/;
var _a8=/^\{lang=[\w\-]+\}\{/;
var _a9=/^\{case_matters=(true|false)\}/;
var _aa=/^\{order_matters=(true|false)\}/;
var _ab=new String(str);
if(_a5){
if(_ab.search(_a6)>=0){
if(_ab.search(_a9)<0){
return false;
}
_ab=_ab.replace(_a9,"");
}
if(_ab.search(_a7)>=0){
if(_ab.search(_aa)<0){
return false;
}
_ab=_ab.replace(_aa,"");
}
if(_ab.search(_a6)>=0){
if(_ab.search(_a9)<0){
return false;
}
_ab=_ab.replace(_a9,"");
}
}
return this.IsValidArrayOfLocalizedStrings(_ab,10,false);
}
function RunTimeApi_ValidLongFillInResponse(str,_ad){
this.WriteDetailedLog("`1328`");
var _ae=/^\{case_matters=/;
var _af=/^\{case_matters=(true|false)\}/;
var _b0=new String(str);
if(_ad){
if(_b0.search(_ae)>=0){
if(_b0.search(_af)<0){
return false;
}
_b0=_b0.replace(_af,"");
}
}
return this.ValidLocalizedString(_b0,4000);
}
function RunTimeApi_ValidLikeRTResponse(str){
this.WriteDetailedLog("`1401`");
return this.ValidShortIdentifier(str);
}
function RunTimeApi_ValidMatchingResponse(str){
this.WriteDetailedLog("`1364`");
var _b3="[,]";
var _b4="[.]";
var _b5;
var _b6;
_b5=str.split(_b3);
for(var i=0;i<_b5.length;i++){
_b6=_b5[i].split(_b4);
if(_b6.length!=2){
return false;
}
if(!this.ValidShortIdentifier(_b6[0])){
return false;
}
if(!this.ValidShortIdentifier(_b6[1])){
return false;
}
}
return true;
}
function RunTimeApi_ValidPerformanceResponse(str,_b9){
this.WriteDetailedLog("`1300`");
var _ba=/^\{order_matters=/;
var _bb=/^\{order_matters=(true|false)\}/;
var _bc;
var _bd;
var _be;
var _bf;
var _c0;
var _c1=new String(str);
if(str.length===0){
return false;
}
if(_b9){
if(_c1.search(_ba)>=0){
if(_c1.search(_bb)<0){
return false;
}
_c1=_c1.replace(_bb,"");
}
}
_bc=_c1.split("[,]");
if(_bc.length===0){
return false;
}
for(var i=0;i<_bc.length;i++){
_bd=_bc[i];
if(_bd.length===0){
return false;
}
_be=_bd.split("[.]");
if(_be.length==2){
_bf=_be[0];
_c0=_be[1];
if(_bf.length===0&&_c0===0){
return false;
}
if(_bf.length>0){
if(!this.ValidShortIdentifier(_bf)){
return false;
}
}
}else{
return false;
}
}
return true;
}
function RunTimeApi_ValidSequencingResponse(str){
this.WriteDetailedLog("`1339`");
return this.IsValidArrayOfShortIdentifiers(str,36,false);
}
function RunTimeApi_ValidNumericResponse(str,_c5){
this.WriteDetailedLog("`1372`");
var _c6="[:]";
var _c7=str.split(_c6);
if(_c5){
if(_c7.length>2){
return false;
}
}else{
if(_c7.length>1){
return false;
}
}
for(var i=0;i<_c7.length;i++){
if(!this.ValidReal(_c7[i])){
return false;
}
}
if(_c7.length>=2){
if(_c7[0].length>0&&_c7[1].length>0){
if(parseFloat(_c7[0])>parseFloat(_c7[1])){
return false;
}
}
}
return true;
}
function RunTimeApi_ValidOtheresponse(str){
this.WriteDetailedLog("`1429`");
return true;
}
function RunTimeApi_TranslateBooleanIntoCMI(_ca){
if(_ca===true){
return SCORM_TRUE;
}else{
if(_ca===false){
return SCORM_FALSE;
}else{
return SCORM_UNKNOWN;
}
}
}
function RunTimeApi_InitTrackedTimeStart(_cb){
this.TrackedStartDate=new Date();
this.StartSessionTotalTime=_cb.RunTime.TotalTime;
}
function RunTimeApi_AccumulateTotalTrackedTime(){
this.TrackedEndDate=new Date();
var _cc=Math.round((this.TrackedEndDate-this.TrackedStartDate)/10);
var _cd=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTimeTracked);
var _ce=_cc+_cd;
this.RunTimeData.TotalTimeTracked=ConvertHundredthsToIso8601TimeSpan(_ce);
this.Activity.ActivityEndedDate=this.TrackedEndDate;
var _cf=GetDateFromUtcIso8601Time(this.Activity.GetActivityStartTimestampUtc());
var _d0=GetDateFromUtcIso8601Time(this.Activity.GetAttemptStartTimestampUtc());
this.Activity.SetActivityAbsoluteDuration(ConvertHundredthsToIso8601TimeSpan((this.TrackedEndDate-_cf)/10));
this.Activity.SetAttemptAbsoluteDuration(ConvertHundredthsToIso8601TimeSpan((this.TrackedEndDate-_d0)/10));
var _d1=ConvertIso8601TimeSpanToHundredths(this.Activity.GetActivityExperiencedDurationTracked());
var _d2=ConvertHundredthsToIso8601TimeSpan(_d1+_cc);
this.Activity.SetActivityExperiencedDurationTracked(_d2);
var _d3=ConvertIso8601TimeSpanToHundredths(this.Activity.GetActivityExperiencedDurationReported());
var _d4=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTime)-ConvertIso8601TimeSpanToHundredths(this.StartSessionTotalTime);
var _d5=ConvertHundredthsToIso8601TimeSpan(_d3+_d4);
this.Activity.SetActivityExperiencedDurationReported(_d5);
var _d6=ConvertIso8601TimeSpanToHundredths(this.Activity.GetAttemptExperiencedDurationTracked());
var _d7=ConvertHundredthsToIso8601TimeSpan(_d6+_cc);
this.Activity.SetAttemptExperiencedDurationTracked(_d7);
var _d8=ConvertIso8601TimeSpanToHundredths(this.Activity.GetAttemptExperiencedDurationReported());
var _d9=ConvertHundredthsToIso8601TimeSpan(_d8+_d4);
this.Activity.SetAttemptExperiencedDurationReported(_d9);
}
function RunTimeApi_SetLookAheadDirtyDataFlagIfNeeded(_da,_db){
if(this.IsLookAheadSequencerDataDirty==false&&_da!=_db){
this.IsLookAheadSequencerDataDirty=true;
}
}
var TERMINATION_REQUEST_EXIT="EXIT";
var TERMINATION_REQUEST_EXIT_ALL="EXIT ALL";
var TERMINATION_REQUEST_SUSPEND_ALL="SUSPEND ALL";
var TERMINATION_REQUEST_ABANDON="ABANDON";
var TERMINATION_REQUEST_ABANDON_ALL="ABANDON ALL";
var TERMINATION_REQUEST_EXIT_PARENT="EXIT PARENT";
var TERMINATION_REQUEST_NOT_VALID="INVALID";
var SEQUENCING_REQUEST_START="START";
var SEQUENCING_REQUEST_RESUME_ALL="RESUME ALL";
var SEQUENCING_REQUEST_CONTINUE="CONTINUE";
var SEQUENCING_REQUEST_PREVIOUS="PREVIOUS";
var SEQUENCING_REQUEST_CHOICE="CHOICE";
var SEQUENCING_REQUEST_RETRY="RETRY";
var SEQUENCING_REQUEST_EXIT="EXIT";
var SEQUENCING_REQUEST_NOT_VALID="INVALID";
var RULE_SET_POST_CONDITION="POST_CONDITION";
var RULE_SET_EXIT="EXIT";
var RULE_SET_HIDE_FROM_CHOICE="HIDE_FROM_CHOICE";
var RULE_SET_STOP_FORWARD_TRAVERSAL="STOP_FORWARD_TRAVERSAL";
var RULE_SET_DISABLED="DISABLED";
var RULE_SET_SKIPPED="SKIPPED";
var RULE_SET_SATISFIED="SATISFIED";
var RULE_SET_NOT_SATISFIED="NOT_SATISFIED";
var RULE_SET_COMPLETED="COMPLETED";
var RULE_SET_INCOMPLETE="INCOMPLETE";
var SEQUENCING_RULE_ACTION_SKIP="Skip";
var SEQUENCING_RULE_ACTION_DISABLED="Disabled";
var SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE="Hidden From Choice";
var SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL="Stop Forward Traversal";
var SEQUENCING_RULE_ACTION_EXIT="Exit";
var SEQUENCING_RULE_ACTION_EXIT_PARENT="Exit Parent";
var SEQUENCING_RULE_ACTION_EXIT_ALL="Exit All";
var SEQUENCING_RULE_ACTION_RETRY="Retry";
var SEQUENCING_RULE_ACTION_RETRY_ALL="Retry All";
var SEQUENCING_RULE_ACTION_CONTINUE="Continue";
var SEQUENCING_RULE_ACTION_PREVIOUS="Previous";
var FLOW_DIRECTION_FORWARD="FORWARD";
var FLOW_DIRECTION_BACKWARD="BACKWARD";
var RULE_CONDITION_OPERATOR_NOT="Not";
var RULE_CONDITION_COMBINATION_ALL="All";
var RULE_CONDITION_COMBINATION_ANY="Any";
var RESULT_UNKNOWN="unknown";
var SEQUENCING_RULE_CONDITION_SATISFIED="Satisfied";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN="Objective Status Known";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN="Objective Measure Known";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_GREATER_THAN="Objective Measure Greater Than";
var SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_LESS_THAN="Objective Measure Less Than";
var SEQUENCING_RULE_CONDITION_COMPLETED="Completed";
var SEQUENCING_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN="Activity Progress Known";
var SEQUENCING_RULE_CONDITION_ATTEMPTED="Attempted";
var SEQUENCING_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED="Attempt Limit Exceeded";
var SEQUENCING_RULE_CONDITION_ALWAYS="Always";
var ROLLUP_RULE_ACTION_SATISFIED="Satisfied";
var ROLLUP_RULE_ACTION_NOT_SATISFIED="Not Satisfied";
var ROLLUP_RULE_ACTION_COMPLETED="Completed";
var ROLLUP_RULE_ACTION_INCOMPLETE="Incomplete";
var CHILD_ACTIVITY_SET_ALL="All";
var CHILD_ACTIVITY_SET_ANY="Any";
var CHILD_ACTIVITY_SET_NONE="None";
var CHILD_ACTIVITY_SET_AT_LEAST_COUNT="At Least Count";
var CHILD_ACTIVITY_SET_AT_LEAST_PERCENT="At Least Percent";
var ROLLUP_RULE_CONDITION_SATISFIED="Satisfied";
var ROLLUP_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN="Objective Status Known";
var ROLLUP_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN="Objective Measure Known";
var ROLLUP_RULE_CONDITION_COMPLETED="Completed";
var ROLLUP_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN="Activity Progress Known";
var ROLLUP_RULE_CONDITION_ATTEMPTED="Attempted";
var ROLLUP_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED="Attempt Limit Exceeded";
var ROLLUP_RULE_CONDITION_NEVER="Never";
var ROLLUP_CONSIDERATION_ALWAYS="Always";
var ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED="If Not Suspended";
var ROLLUP_CONSIDERATION_IF_ATTEMPTED="If Attempted";
var ROLLUP_CONSIDERATION_IF_NOT_SKIPPED="If Not Skipped";
var TIMING_NEVER="Never";
var TIMING_ONCE="Once";
var TIMING_ON_EACH_NEW_ATTEMPT="On Each New Attempt";
function Sequencer(_dc,_dd){
this.LookAhead=_dc;
this.Activities=_dd;
this.NavigationRequest=null;
this.ChoiceTargetIdentifier=null;
this.SuspendedActivity=null;
this.CurrentActivity=null;
this.Exception=null;
this.ExceptionText=null;
this.GlobalObjectives=new Array();
this.ReturnToLmsInvoked=false;
}
Sequencer.prototype.OverallSequencingProcess=Sequencer_OverallSequencingProcess;
Sequencer.prototype.SetSuspendedActivity=Sequencer_SetSuspendedActivity;
Sequencer.prototype.GetSuspendedActivity=Sequencer_GetSuspendedActivity;
Sequencer.prototype.Start=Sequencer_Start;
Sequencer.prototype.InitialRandomizationAndSelection=Sequencer_InitialRandomizationAndSelection;
Sequencer.prototype.GetCurrentActivity=Sequencer_GetCurrentActivity;
Sequencer.prototype.GetExceptionText=Sequencer_GetExceptionText;
Sequencer.prototype.GetExitAction=Sequencer_GetExitAction;
Sequencer.prototype.EvaluatePossibleNavigationRequests=Sequencer_EvaluatePossibleNavigationRequests;
Sequencer.prototype.InitializePossibleNavigationRequestAbsolutes=Sequencer_InitializePossibleNavigationRequestAbsolutes;
Sequencer.prototype.SetAllDescendentsToDisabled=Sequencer_SetAllDescendentsToDisabled;
Sequencer.prototype.ContentDeliveryEnvironmentActivityDataSubProcess=Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess;
function Sequencer_SetSuspendedActivity(_de){
this.SuspendedActivity=_de;
}
function Sequencer_GetSuspendedActivity(){
return this.SuspendActivity;
}
function Sequencer_GetSuspendedActivity(_df){
var _e0=this.SuspendedActivity;
if(_df!==null&&_df!==undefined){
this.LogSeq("`1552`"+_e0,_df);
}
return _e0;
}
function Sequencer_Start(){
if(this.SuspendedActivity===null){
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_START,null,"");
}else{
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_RESUME_ALL,null,"");
}
this.OverallSequencingProcess();
}
function Sequencer_InitialRandomizationAndSelection(){
var _e1=this.LogSeqAudit("`1306`");
if(this.SuspendedActivity===null){
for(var _e2 in this.Activities.ActivityList){
this.SelectChildrenProcess(this.Activities.ActivityList[_e2],_e1);
this.RandomizeChildrenProcess(this.Activities.ActivityList[_e2],false,_e1);
}
}
}
function Sequencer_GetCurrentActivity(){
return this.CurrentActivity;
}
function Sequencer_GetExceptionText(){
if(this.ExceptionText!==null&&this.ExceptionText!==undefined){
return this.ExceptionText;
}else{
return "";
}
}
function Sequencer_GetExitAction(_e3,_e4){
return EXIT_ACTION_DISPLAY_MESSAGE;
}
Sequencer.prototype.NavigationRequestProcess=Sequencer_NavigationRequestProcess;
Sequencer.prototype.SequencingExitActionRulesSubprocess=Sequencer_SequencingExitActionRulesSubprocess;
Sequencer.prototype.SequencingPostConditionRulesSubprocess=Sequencer_SequencingPostConditionRulesSubprocess;
Sequencer.prototype.TerminationRequestProcess=Sequencer_TerminationRequestProcess;
Sequencer.prototype.MeasureRollupProcess=Sequencer_MeasureRollupProcess;
Sequencer.prototype.ObjectiveRollupProcess=Sequencer_ObjectiveRollupProcess;
Sequencer.prototype.ObjectiveRollupUsingDefaultProcess=Sequencer_ObjectiveRollupUsingDefaultProcess;
Sequencer.prototype.ObjectiveRollupUsingMeasureProcess=Sequencer_ObjectiveRollupUsingMeasureProcess;
Sequencer.prototype.ObjectiveRollupUsingRulesProcess=Sequencer_ObjectiveRollupUsingRulesProcess;
Sequencer.prototype.ActivityProgressRollupProcess=Sequencer_ActivityProgressRollupProcess;
Sequencer.prototype.ActivityProgressRollupProcessUsingDefault=Sequencer_ActivityProgressRollupProcessUsingDefault;
Sequencer.prototype.RollupRuleCheckSubprocess=Sequencer_RollupRuleCheckSubprocess;
Sequencer.prototype.EvaluateRollupConditionsSubprocess=Sequencer_EvaluateRollupConditionsSubprocess;
Sequencer.prototype.CheckChildForRollupSubprocess=Sequencer_CheckChildForRollupSubprocess;
Sequencer.prototype.OverallRollupProcess=Sequencer_OverallRollupProcess;
Sequencer.prototype.SelectChildrenProcess=Sequencer_SelectChildrenProcess;
Sequencer.prototype.RandomizeChildrenProcess=Sequencer_RandomizeChildrenProcess;
Sequencer.prototype.FlowTreeTraversalSubprocess=Sequencer_FlowTreeTraversalSubprocess;
Sequencer.prototype.FlowActivityTraversalSubprocess=Sequencer_FlowActivityTraversalSubprocess;
Sequencer.prototype.FlowSubprocess=Sequencer_FlowSubprocess;
Sequencer.prototype.ChoiceActivityTraversalSubprocess=Sequencer_ChoiceActivityTraversalSubprocess;
Sequencer.prototype.StartSequencingRequestProcess=Sequencer_StartSequencingRequestProcess;
Sequencer.prototype.ResumeAllSequencingRequestProcess=Sequencer_ResumeAllSequencingRequestProcess;
Sequencer.prototype.ContinueSequencingRequestProcess=Sequencer_ContinueSequencingRequestProcess;
Sequencer.prototype.PreviousSequencingRequestProcess=Sequencer_PreviousSequencingRequestProcess;
Sequencer.prototype.ChoiceSequencingRequestProcess=Sequencer_ChoiceSequencingRequestProcess;
Sequencer.prototype.ChoiceFlowSubprocess=Sequencer_ChoiceFlowSubprocess;
Sequencer.prototype.ChoiceFlowTreeTraversalSubprocess=Sequencer_ChoiceFlowTreeTraversalSubprocess;
Sequencer.prototype.RetrySequencingRequestProcess=Sequencer_RetrySequencingRequestProcess;
Sequencer.prototype.ExitSequencingRequestProcess=Sequencer_ExitSequencingRequestProcess;
Sequencer.prototype.SequencingRequestProcess=Sequencer_SequencingRequestProcess;
Sequencer.prototype.DeliveryRequestProcess=Sequencer_DeliveryRequestProcess;
Sequencer.prototype.ContentDeliveryEnvironmentProcess=Sequencer_ContentDeliveryEnvironmentProcess;
Sequencer.prototype.ClearSuspendedActivitySubprocess=Sequencer_ClearSuspendedActivitySubprocess;
Sequencer.prototype.LimitConditionsCheckProcess=Sequencer_LimitConditionsCheckProcess;
Sequencer.prototype.SequencingRulesCheckProcess=Sequencer_SequencingRulesCheckProcess;
Sequencer.prototype.SequencingRulesCheckSubprocess=Sequencer_SequencingRulesCheckSubprocess;
Sequencer.prototype.TerminateDescendentAttemptsProcess=Sequencer_TerminateDescendentAttemptsProcess;
Sequencer.prototype.EndAttemptProcess=Sequencer_EndAttemptProcess;
Sequencer.prototype.CheckActivityProcess=Sequencer_CheckActivityProcess;
Sequencer.prototype.EvaluateSequencingRuleCondition=Sequencer_EvaluateSequencingRuleCondition;
Sequencer.prototype.ResetException=Sequencer_ResetException;
Sequencer.prototype.LogSeq=Sequencer_LogSeq;
Sequencer.prototype.LogSeqAudit=Sequencer_LogSeqAudit;
Sequencer.prototype.LogSeqReturn=Sequencer_LogSeqReturn;
Sequencer.prototype.WriteHistoryLog=Sequencer_WriteHistoryLog;
Sequencer.prototype.WriteHistoryReturnValue=Sequencer_WriteHistoryReturnValue;
Sequencer.prototype.SetCurrentActivity=Sequencer_SetCurrentActivity;
Sequencer.prototype.IsCurrentActivityDefined=Sequencer_IsCurrentActivityDefined;
Sequencer.prototype.IsSuspendedActivityDefined=Sequencer_IsSuspendedActivityDefined;
Sequencer.prototype.ClearSuspendedActivity=Sequencer_ClearSuspendedActivity;
Sequencer.prototype.GetRootActivity=Sequencer_GetRootActivity;
Sequencer.prototype.DoesActivityExist=Sequencer_DoesActivityExist;
Sequencer.prototype.GetActivityFromIdentifier=Sequencer_GetActivityFromIdentifier;
Sequencer.prototype.AreActivitiesSiblings=Sequencer_AreActivitiesSiblings;
Sequencer.prototype.FindCommonAncestor=Sequencer_FindCommonAncestor;
Sequencer.prototype.GetActivityPath=Sequencer_GetActivityPath;
Sequencer.prototype.GetPathToAncestorExclusive=Sequencer_GetPathToAncestorExclusive;
Sequencer.prototype.GetPathToAncestorInclusive=Sequencer_GetPathToAncestorInclusive;
Sequencer.prototype.ActivityHasSuspendedChildren=Sequencer_ActivityHasSuspendedChildren;
Sequencer.prototype.CourseIsSingleSco=Sequencer_CourseIsSingleSco;
Sequencer.prototype.TranslateSequencingRuleActionIntoSequencingRequest=Sequencer_TranslateSequencingRuleActionIntoSequencingRequest;
Sequencer.prototype.TranslateSequencingRuleActionIntoTerminationRequest=Sequencer_TranslateSequencingRuleActionIntoTerminationRequest;
Sequencer.prototype.IsActivity1BeforeActivity2=Sequencer_IsActivity1BeforeActivity2;
Sequencer.prototype.GetOrderedListOfActivities=Sequencer_GetOrderedListOfActivities;
Sequencer.prototype.PreOrderTraversal=Sequencer_PreOrderTraversal;
Sequencer.prototype.IsActivityLastOverall=Sequencer_IsActivityLastOverall;
Sequencer.prototype.GetGlobalObjectiveByIdentifier=Sequencer_GetGlobalObjectiveByIdentifier;
Sequencer.prototype.AddGlobalObjective=Sequencer_AddGlobalObjective;
Sequencer.prototype.FindActivitiesAffectedByWriteMaps=Sequencer_FindActivitiesAffectedByWriteMaps;
Sequencer.prototype.FindDistinctParentsOfActivitySet=Sequencer_FindDistinctParentsOfActivitySet;
Sequencer.prototype.GetMinimalSubsetOfActivitiesToRollup=Sequencer_GetMinimalSubsetOfActivitiesToRollup;
Sequencer.prototype.CheckForRelevantSequencingRules=Sequencer_CheckForRelevantSequencingRules;
Sequencer.prototype.DoesThisActivityHaveSequencingRulesRelevantToChoice=Sequencer_DoesThisActivityHaveSequencingRulesRelevantToChoice;
function Sequencer_ResetException(){
this.Exception=null;
this.ExceptionText=null;
}
function Sequencer_LogSeq(str,_e6){
if(_e6===null||_e6===undefined){
Debug.AssertError("`1537`");
}
str=str+"";
if(this.LookAhead===true){
return Debug.WriteLookAheadDetailed(str,_e6);
}else{
return Debug.WriteSequencingDetailed(str,_e6);
}
}
function Sequencer_LogSeqAudit(str,_e8){
str=str+"";
if(this.LookAhead===true){
return Debug.WriteLookAheadAudit(str,_e8);
return "";
}else{
return Debug.WriteSequencingAudit(str,_e8);
}
}
function Sequencer_LogSeqReturn(str,_ea){
if(_ea===null||_ea===undefined){
Debug.AssertError("`1537`");
}
str=str+"";
if(this.LookAhead===true){
return _ea.setReturn(str);
}else{
return _ea.setReturn(str);
}
}
function Sequencer_WriteHistoryLog(str,_ec){
HistoryLog.WriteEventDetailed(str,_ec);
}
function Sequencer_WriteHistoryReturnValue(str,_ee){
HistoryLog.WriteEventDetailedReturnValue(str,_ee);
}
function Sequencer_SetCurrentActivity(_ef,_f0){
Debug.AssertError("Parent log not passed.",(_f0===undefined||_f0===null));
this.LogSeq("`1421`"+_ef,_f0);
this.CurrentActivity=_ef;
}
function Sequencer_IsCurrentActivityDefined(_f1){
Debug.AssertError("Parent log not passed.",(_f1===undefined||_f1===null));
var _f2=this.GetCurrentActivity();
var _f3=(_f2!==null);
if(_f3){
this.LogSeq("`1448`",_f1);
}else{
this.LogSeq("`1379`",_f1);
}
return _f3;
}
function Sequencer_IsSuspendedActivityDefined(_f4){
Debug.AssertError("Parent log not passed.",(_f4===undefined||_f4===null));
var _f5=this.GetSuspendedActivity(_f4);
var _f6=(_f5!==null);
if(_f6){
this.LogSeq("`1406`",_f4);
}else{
this.LogSeq("`1341`",_f4);
}
return _f6;
}
function Sequencer_ClearSuspendedActivity(_f7){
Debug.AssertError("Parent log not passed.",(_f7===undefined||_f7===null));
this.LogSeq("`1459`",_f7);
this.SuspendedActivity=null;
}
function Sequencer_GetRootActivity(_f8){
Debug.AssertError("Parent log not passed.",(_f8===undefined||_f8===null));
var _f9=this.Activities.GetRootActivity();
this.LogSeq("`1638`"+_f9,_f8);
return _f9;
}
function Sequencer_DoesActivityExist(_fa,_fb){
Debug.AssertError("Parent log not passed.",(_fb===undefined||_fb===null));
var _fc=this.Activities.DoesActivityExist(_fa,_fb);
if(_fc){
this.LogSeq("`1670`"+_fa+"`1397`",_fb);
}else{
this.LogSeq("`1670`"+_fa+"`1256`",_fb);
}
return _fc;
}
function Sequencer_GetActivityFromIdentifier(_fd,_fe){
Debug.AssertError("Parent log not passed.",(_fe===undefined||_fe===null));
var _ff=this.Activities.GetActivityFromIdentifier(_fd,_fe);
if(_ff!==null){
this.LogSeq("`1670`"+_fd+"`1461`"+_ff,_fe);
}else{
this.LogSeq("`1670`"+_fd+"`1225`",_fe);
}
return _ff;
}
function Sequencer_AreActivitiesSiblings(_100,_101,_102){
Debug.AssertError("Parent log not passed.",(_102===undefined||_102===null));
if(_100===null||_100===undefined||_101===null||_101===undefined){
return false;
}
var _103=_100.ParentActivity;
var _104=_101.ParentActivity;
var _105=(_103==_104);
if(_105){
this.LogSeq("`1680`"+_100+"`1727`"+_101+"`1697`",_102);
}else{
this.LogSeq("`1680`"+_100+"`1727`"+_101+"`1627`",_102);
}
return _105;
}
function Sequencer_FindCommonAncestor(_106,_107,_108){
Debug.AssertError("Parent log not passed.",(_108===undefined||_108===null));
var _109=new Array();
var _10a=new Array();
if(_106!==null&&_106.IsTheRoot()){
this.LogSeq(_106+"`939`"+_106+"`1727`"+_107+"`1730`"+_106,_108);
return _106;
}
if(_107!==null&&_107.IsTheRoot()){
this.LogSeq(_107+"`939`"+_106+"`1727`"+_107+"`1730`"+_107,_108);
return _107;
}
if(_106!==null){
_109=this.Activities.GetActivityPath(_106,false);
}
if(_107!==null){
_10a=this.Activities.GetActivityPath(_107,false);
}
for(var i=0;i<_109.length;i++){
for(var j=0;j<_10a.length;j++){
if(_109[i]==_10a[j]){
this.LogSeq("`1326`"+_106+"`1727`"+_107+"`1730`"+_109[i],_108);
return _109[i];
}
}
}
this.LogSeq("`1713`"+_106+"`1727`"+_107+"`1388`",_108);
return null;
}
function Sequencer_GetActivityPath(_10d,_10e){
return this.Activities.GetActivityPath(_10d,_10e);
}
function Sequencer_GetPathToAncestorExclusive(_10f,_110,_111){
var _112=new Array();
var _113=0;
if(_10f!==null&&_110!==null&&_10f!==_110){
if(_111===true){
_112[_113]=_10f;
_113++;
}
while(_10f.ParentActivity!==null&&_10f.ParentActivity!==_110){
_10f=_10f.ParentActivity;
_112[_113]=_10f;
_113++;
}
}
return _112;
}
function Sequencer_GetPathToAncestorInclusive(_114,_115){
var _116=new Array();
var _117=0;
_116[_117]=_114;
_117++;
while(_114.ParentActivity!==null&&_114!=_115){
_114=_114.ParentActivity;
_116[_117]=_114;
_117++;
}
return _116;
}
function Sequencer_ActivityHasSuspendedChildren(_118,_119){
Debug.AssertError("Parent log not passed.",(_119===undefined||_119===null));
var _11a=_118.GetChildren();
var _11b=false;
for(var i=0;i<_11a.length;i++){
if(_11a[i].IsSuspended()){
_11b=true;
}
}
if(_11b){
this.LogSeq("`1698`"+_118+"`1512`",_119);
}else{
this.LogSeq("`1698`"+_118+"`1338`",_119);
}
return _11b;
}
function Sequencer_CourseIsSingleSco(){
if(this.Activities.ActivityList.length<=2){
return true;
}else{
return false;
}
}
function Sequencer_TranslateSequencingRuleActionIntoSequencingRequest(_11d){
switch(_11d){
case SEQUENCING_RULE_ACTION_RETRY:
return SEQUENCING_REQUEST_RETRY;
case SEQUENCING_RULE_ACTION_CONTINUE:
return SEQUENCING_REQUEST_CONTINUE;
case SEQUENCING_RULE_ACTION_PREVIOUS:
return SEQUENCING_REQUEST_PREVIOUS;
default:
Debug.AssertError("ERROR in TranslateSequencingRuleActionIntoSequencingRequest - should never have an untranslatable sequencing request. ruleAction="+_11d);
return null;
}
}
function Sequencer_TranslateSequencingRuleActionIntoTerminationRequest(_11e){
switch(_11e){
case SEQUENCING_RULE_ACTION_EXIT_PARENT:
return TERMINATION_REQUEST_EXIT_PARENT;
case SEQUENCING_RULE_ACTION_EXIT_ALL:
return TERMINATION_REQUEST_EXIT_ALL;
default:
Debug.AssertError("ERROR in TranslateSequencingRuleActionIntoTerminationRequest - should never have an untranslatable sequencing request. ruleAction="+_11e);
return null;
}
}
function Sequencer_IsActivity1BeforeActivity2(_11f,_120,_121){
Debug.AssertError("Parent log not passed.",(_121===undefined||_121===null));
var _122=this.GetOrderedListOfActivities(_121);
for(var i=0;i<_122.length;i++){
if(_122[i]==_11f){
this.LogSeq("`1698`"+_11f+"`1510`"+_120,_121);
return true;
}
if(_122[i]==_120){
this.LogSeq("`1698`"+_11f+"`1539`"+_120,_121);
return false;
}
}
Debug.AssertError("ERROR IN Sequencer_IsActivity1BeforeActivity2");
return null;
}
function Sequencer_GetOrderedListOfActivities(_124){
Debug.AssertError("Parent log not passed.",(_124===undefined||_124===null));
var list;
var root=this.GetRootActivity(_124);
list=this.PreOrderTraversal(root);
return list;
}
function Sequencer_PreOrderTraversal(_127){
var list=new Array();
list[0]=_127;
var _129=_127.GetAvailableChildren();
var _12a;
for(var i=0;i<_129.length;i++){
_12a=this.PreOrderTraversal(_129[i]);
list=list.concat(_12a);
}
return list;
}
function Sequencer_IsActivityLastOverall(_12c,_12d){
Debug.AssertError("Parent log not passed.",(_12d===undefined||_12d===null));
var _12e=this.GetOrderedListOfActivities(_12d);
if(_12c==_12e[_12e.length-1]){
this.LogSeq("`1698`"+_12c+"`1405`",_12d);
return true;
}
this.LogSeq("`1698`"+_12c+"`1344`",_12d);
return false;
}
function Sequencer_GetGlobalObjectiveByIdentifier(_12f){
for(var obj in this.GlobalObjectives){
if(this.GlobalObjectives[obj].ID==_12f){
return this.GlobalObjectives[obj];
}
}
return null;
}
function Sequencer_AddGlobalObjective(ID,_132,_133,_134,_135){
var _136=this.GlobalObjectives.length;
var obj=new GlobalObjective(_136,ID,_132,_133,_134,_135);
this.GlobalObjectives[_136]=obj;
this.GlobalObjectives[_136].SetDirtyData();
}
function Sequencer_FindActivitiesAffectedByWriteMaps(_138){
var _139=new Array();
var _13a;
var _13b=_138.GetObjectives();
var _13c=new Array();
var i;
var j;
for(i=0;i<_13b.length;i++){
_13a=_13b[i].GetMaps();
for(j=0;j<_13a.length;j++){
if(_13a[j].WriteSatisfiedStatus===true||_13a[j].WriteNormalizedMeasure===true){
_139[_139.length]=_13a[j].TargetObjectiveId;
}
}
}
if(_139.length===0){
return _13c;
}
var _13f;
var _140;
var _141;
var _142;
for(var _143 in this.Activities.ActivityList){
_13f=this.Activities.ActivityList[_143];
if(_13f!=_138){
_140=_13f.GetObjectives();
for(var _144=0;_144<_140.length;_144++){
_141=_140[_144].GetMaps();
for(var map=0;map<_141.length;map++){
if(_141[map].ReadSatisfiedStatus===true||_141[map].ReadNormalizedMeasure===true){
_142=_141[map].TargetObjectiveId;
for(var _146=0;_146<_139.length;_146++){
if(_139[_146]==_142){
_13c[_13c.length]=_13f;
}
}
}
}
}
}
}
return _13c;
}
function Sequencer_FindDistinctParentsOfActivitySet(_147){
var _148=new Array();
var _149;
var _14a;
for(var _14b in _147){
_14a=true;
_149=this.Activities.GetParentActivity(_147[_14b]);
if(_149!==null){
for(var i=0;i<_148.length;i++){
if(_148[i]==_149){
_14a=false;
break;
}
}
if(_14a){
_148[_148.length]=_149;
}
}
}
return _148;
}
function Sequencer_GetMinimalSubsetOfActivitiesToRollup(_14d,_14e){
var _14f=new Array();
var _150=new Array();
var _151;
var _152;
var _153;
if(_14e!==null){
_14f=_14f.concat(_14e);
}
for(var _154 in _14d){
_152=_14d[_154];
_153=false;
for(var _155 in _14f){
if(_14f[_155]==_152){
_153=true;
break;
}
}
if(_153===false){
_150[_150.length]=_152;
_151=this.GetActivityPath(_152,true);
_14f=_14f.concat(_151);
}
}
return _150;
}
function Sequencer_EvaluatePossibleNavigationRequests(_156){
var _157=this.LogSeqAudit("`989`");
var _158;
var _159=null;
var _15a;
var _15b;
var id;
for(id in _156){
if(_156[id].WillAlwaysSucceed===true){
_156[id].WillSucceed=true;
}else{
if(_156[id].WillNeverSucceed===true){
_156[id].WillSucceed=false;
_156[id].Exception="NB.2.1-10";
_156[id].ExceptionText=IntegrationImplementation.GetString("Your selection is not permitted. Please select 'Next' or 'Previous' to move through '{0}.'");
}else{
_156[id].WillSucceed=null;
}
}
_156[id].Hidden=false;
_156[id].Disabled=false;
}
this.LogSeq("`795`",_157);
for(id in _156){
if(_156[id].WillSucceed===null){
_158=this.NavigationRequestProcess(_156[id].NavigationRequest,_156[id].TargetActivityItemIdentifier,_157);
if(_158.NavigationRequest==NAVIGATION_REQUEST_NOT_VALID){
this.LogSeq("`760`",_157);
_156[id].WillSucceed=false;
_156[id].TargetActivity=this.GetActivityFromIdentifier(_156[id].TargetActivityItemIdentifier,_157);
_156[id].Exception=_158.Exception;
_156[id].ExceptionText=_158.ExceptionText;
}else{
this.LogSeq("`743`",_157);
_156[id].WillSucceed=true;
_156[id].TargetActivity=_158.TargetActivity;
_156[id].SequencingRequest=_158.SequencingRequest;
_156[id].Exception="";
_156[id].ExceptionText="";
}
}
}
this.LogSeq("`215`",_157);
var _15d=this.GetCurrentActivity();
if(_15d!==null&&_15d.IsActive()===true){
this.LogSeq("`953`",_157);
_159=this.TerminationRequestProcess(TERMINATION_REQUEST_EXIT,_157);
this.LogSeq("`906`",_157);
if(_159.TerminationRequest==TERMINATION_REQUEST_NOT_VALID){
this.LogSeq("`720`",_157);
if(_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed){
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed=false;
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].Exception=_159.Exception;
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].ExceptionText=_159.ExceptionText;
}
if(_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed){
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed=false;
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].Exception=_159.Exception;
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].ExceptionText=_159.ExceptionText;
}
if(_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed){
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed=false;
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].Exception=_159.Exception;
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].ExceptionText=_159.ExceptionText;
}
for(id=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;id<_156.length;id++){
if(_156[id].WillSucceed){
_156[id].WillSucceed=false;
_156[id].Exception=_159.Exception;
_156[id].ExceptionText=terminatonRequestResult.ExceptionText;
}else{
_156[id].TerminationSequencingRequest=_159.SequencingRequest;
}
}
}
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].TerminationSequencingRequest=_159.SequencingRequest;
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].TerminationSequencingRequest=_159.SequencingRequest;
_156[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].TerminationSequencingRequest=_159.SequencingRequest;
for(id=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;id<_156.length;id++){
_156[id].TerminationSequencingRequest=_159.SequencingRequest;
}
}
this.LogSeq("`660`",_157);
for(id in _156){
if(id>=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE&&_156[id].WillSucceed===true&&_156[id].WillAlwaysSucceed===false&&_156[id].WillNeverSucceed===false){
this.LogSeq("`869`",_157);
var _15e=this.CheckActivityProcess(_156[id].TargetActivity,_157);
if(_15e===true){
this.LogSeq("`751`",_157);
_156[id].WillSucceed=false;
_156[id].Disabled=true;
this.SetAllDescendentsToDisabled(_156,_156[id].TargetActivity);
}
}
}
this.LogSeq("`678`",_157);
var _15f=null;
for(id in _156){
if(_156[id].WillAlwaysSucceed===false&&_156[id].WillNeverSucceed===false){
this.LogSeq("`625`",_157);
if(_156[id].TerminationSequencingRequest!==null){
this.LogSeq("`420`",_157);
if(_15f===null){
_15a=this.SequencingRequestProcess(_156[id].TerminationSequencingRequest,null,_157);
}else{
_15a=_15f;
}
if(id>=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE){
this.LogSeq("`1036`",_157);
var _160=this.SequencingRulesCheckProcess(_156[id].TargetActivity,RULE_SET_HIDE_FROM_CHOICE,_157);
if(_160!==null){
_156[id].Exception="SB.2.9-3";
_156[id].ExceptionText="The activity "+_156[id].TargetActivity.GetTitle()+" should be hidden and is not a valid selection";
_156[id].Hidden=true;
}
}
}else{
this.LogSeq("`1635`",_157);
this.LogSeq("`721`",_157);
_15a=this.SequencingRequestProcess(_156[id].SequencingRequest,_156[id].TargetActivity,_157);
}
this.LogSeq("`841`",_157);
if(_15a.Exception!==null){
this.LogSeq("`1277`",_157);
_156[id].WillSucceed=false;
_156[id].Exception=_15a.Exception;
_156[id].ExceptionText=_15a.ExceptionText;
_156[id].Hidden=_15a.Hidden;
}else{
if(_156[id].WillSucceed===true&&_15a.DeliveryRequest!==null){
this.LogSeq("`1117`",_157);
_15b=this.DeliveryRequestProcess(_15a.DeliveryRequest,_157);
this.LogSeq("`714`"+_15b.Valid+")",_157);
_156[id].WillSucceed=_15b.Valid;
}
}
}
}
this.LogSeqReturn("",_157);
return _156;
}
function Sequencer_SetAllDescendentsToDisabled(_161,_162){
for(var i=0;i<_162.ChildActivities.length;i++){
var _164=Control.FindPossibleChoiceRequestForActivity(_162.ChildActivities[i]);
_164.WillSucceed=false;
_164.Disabled=true;
this.SetAllDescendentsToDisabled(_161,_162.ChildActivities[i]);
}
}
function Sequencer_InitializePossibleNavigationRequestAbsolutes(_165,_166,_167){
var _168=(Control.Package.Properties.LookaheadSequencerMode===LOOKAHEAD_SEQUENCER_MODE_DISABLE);
this.CheckForRelevantSequencingRules(_166,false);
var _169;
var _16a;
for(var _16b in _167){
_169=_167[_16b];
if(_169.GetSequencingControlChoice()===false){
for(var i=0;i<_169.ChildActivities.length;i++){
_16a=Control.FindPossibleChoiceRequestForActivity(_169.ChildActivities[i]);
_16a.WillNeverSucceed=true;
}
}
_16a=Control.FindPossibleChoiceRequestForActivity(_169);
if(_169.IsDeliverable()===false&&_169.GetSequencingControlFlow()===false){
_16a.WillNeverSucceed=true;
}
if(_169.HasChildActivitiesDeliverableViaFlow===false){
_16a.WillNeverSucceed=true;
}
if(_169.HasSeqRulesRelevantToChoice===false&&_16a.WillNeverSucceed===false){
_16a.WillAlwaysSucceed=true;
}else{
_16a.WillAlwaysSucceed=false;
}
}
}
function Sequencer_CheckForRelevantSequencingRules(_16d,_16e){
if(_16e===true){
_16d.HasSeqRulesRelevantToChoice=true;
}else{
if(this.DoesThisActivityHaveSequencingRulesRelevantToChoice(_16d)){
_16d.HasSeqRulesRelevantToChoice=true;
}else{
_16d.HasSeqRulesRelevantToChoice=false;
}
}
var _16f=false;
for(var i=0;i<_16d.ChildActivities.length;i++){
this.CheckForRelevantSequencingRules(_16d.ChildActivities[i],_16d.HasSeqRulesRelevantToChoice);
_16f=(_16f||_16d.ChildActivities[i].HasChildActivitiesDeliverableViaFlow);
}
_16d.HasChildActivitiesDeliverableViaFlow=(_16d.IsDeliverable()||(_16d.GetSequencingControlFlow()&&_16f));
}
function Sequencer_DoesThisActivityHaveSequencingRulesRelevantToChoice(_171){
if(_171.GetSequencingControlForwardOnly()===true){
return true;
}
if(_171.GetPreventActivation()===true){
return true;
}
if(_171.GetConstrainedChoice()===true){
return true;
}
if(_171.GetSequencingControlChoiceExit()===false){
return true;
}
var _172=_171.GetPreConditionRules();
for(var i=0;i<_172.length;i++){
if(_172[i].Action==SEQUENCING_RULE_ACTION_DISABLED||_172[i].Action==SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE||_172[i].Action==SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL){
return true;
}
}
return false;
}
function Sequencer_ActivityProgressRollupProcess(_174,_175){
Debug.AssertError("Calling log not passed.",(_175===undefined||_175===null));
var _176=this.LogSeqAudit("`1191`"+_174+")",_175);
var _177;
this.LogSeq("`335`",_176);
_177=this.RollupRuleCheckSubprocess(_174,RULE_SET_INCOMPLETE,_176);
this.LogSeq("`848`",_176);
if(_177===true){
this.LogSeq("`797`",_176);
_174.SetAttemptProgressStatus(true);
this.LogSeq("`767`",_176);
_174.SetAttemptCompletionStatus(false);
}
this.LogSeq("`351`",_176);
_177=this.RollupRuleCheckSubprocess(_174,RULE_SET_COMPLETED,_176);
this.LogSeq("`851`",_176);
if(_177===true){
this.LogSeq("`809`",_176);
_174.SetAttemptProgressStatus(true);
this.LogSeq("`779`",_176);
_174.SetAttemptCompletionStatus(true);
}
if(Sequencer_GetApplicableSetofRollupRules(_174,RULE_SET_INCOMPLETE).length===0&&Sequencer_GetApplicableSetofRollupRules(_174,RULE_SET_COMPLETED).length===0){
this.LogSeq("`1167`",_176);
this.ActivityProgressRollupProcessUsingDefault(_174,_176);
}
this.LogSeq("`1062`",_176);
this.LogSeqReturn("",_176);
return;
}
function Sequencer_ActivityProgressRollupProcessUsingDefault(_178,_179){
var _17a=this.LogSeqAudit("`923`"+_178+")",_179);
var _17b;
var _17c;
var _17d;
this.LogSeq("`1263`",_17a);
if(_178.IsALeaf()){
this.LogSeq("`1285`",_17a);
this.LogSeqReturn("",_17a);
return;
}
this.LogSeq("`1084`",_17a);
var _17e=_178.GetChildren();
this.LogSeq("`1103`",_17a);
var _17f=true;
this.LogSeq("`1211`",_17a);
var _180=true;
this.LogSeq("`1273`",_17a);
var _181=true;
for(var i=0;i<_17e.length;i++){
this.LogSeq("`1353`"+_17e[i]+"`1704`",_17a);
if(_17e[i].IsTracked()){
this.LogSeq("`1030`",_17a);
_17b=_17e[i].IsCompleted();
this.LogSeq("`1049`",_17a);
_17c=_17e[i].IsAttempted();
this.LogSeq("`771`",_17a);
_17d=(_17b===false||_17c===true);
this.LogSeq("`883`",_17a);
if(this.CheckChildForRollupSubprocess(_17e[i],ROLLUP_RULE_ACTION_COMPLETED,_17a)){
this.LogSeq("`881`",_17a);
_180=(_180&&(_17b===true));
_181=false;
}
this.LogSeq("`870`",_17a);
if(this.CheckChildForRollupSubprocess(_17e[i],ROLLUP_RULE_ACTION_INCOMPLETE,_17a)){
this.LogSeq("`847`",_17a);
_17f=(_17f&&_17d);
_181=false;
}
}
}
if(_181&&Control.Package.Properties.RollupEmptySetToUnknown){
this.LogSeq("`537`"+Control.Package.Properties.RollupEmptySetToUnknown+")",_17a);
}else{
this.LogSeq("`1387`",_17a);
if(_17f===true){
this.LogSeq("`775`",_17a);
_178.SetAttemptProgressStatus(true);
this.LogSeq("`746`",_17a);
_178.SetAttemptCompletionStatus(false);
}
this.LogSeq("`1416`",_17a);
if(_180===true){
this.LogSeq("`780`",_17a);
_178.SetAttemptProgressStatus(true);
this.LogSeq("`759`",_17a);
_178.SetAttemptCompletionStatus(true);
}
}
this.LogSeqReturn("",_17a);
}
function Sequencer_CheckActivityProcess(_183,_184){
Debug.AssertError("Calling log not passed.",(_184===undefined||_184===null));
var _185=this.LogSeqAudit("`1395`"+_183+")",_184);
this.LogSeq("`308`",_185);
var _186=this.SequencingRulesCheckProcess(_183,RULE_SET_DISABLED,_185);
this.LogSeq("`781`",_185);
if(_186!==null){
this.LogSeq("`717`",_185);
this.LogSeqReturn("`1732`",_185);
return true;
}
this.LogSeq("`393`",_185);
var _187=this.LimitConditionsCheckProcess(_183,_185);
this.LogSeq("`860`",_185);
if(_187){
this.LogSeq("`609`",_185);
this.LogSeqReturn("`1732`",_185);
return true;
}
this.LogSeq("`747`",_185);
this.LogSeqReturn("`1728`",_185);
return false;
}
function Sequencer_CheckChildForRollupSubprocess(_188,_189,_18a){
Debug.AssertError("Calling log not passed.",(_18a===undefined||_18a===null));
var _18b=this.LogSeqAudit("`1119`"+_188+", "+_189+")",_18a);
var _18c;
this.LogSeq("`1317`",_18b);
var _18d=false;
this.LogSeq("`815`",_18b);
if(_189==ROLLUP_RULE_ACTION_SATISFIED||_189==ROLLUP_RULE_ACTION_NOT_SATISFIED){
this.LogSeq("`412`",_18b);
if(_188.GetRollupObjectiveSatisfied()===true){
this.LogSeq("`594`",_18b);
_18d=true;
var _18e=_188.GetRequiredForSatisfied();
var _18f=_188.GetRequiredForNotSatisfied();
this.LogSeq("`98`",_18b);
if((_189==ROLLUP_RULE_ACTION_SATISFIED&&_18e==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)||(_189==ROLLUP_RULE_ACTION_NOT_SATISFIED&&_18f==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)){
this.LogSeq("`278`",_18b);
if(_188.GetAttemptCount()>0&&_188.IsSuspended()===true){
this.LogSeq("`1201`",_18b);
_18d=false;
}
}else{
this.LogSeq("`1558`",_18b);
this.LogSeq("`104`",_18b);
if((_189==ROLLUP_RULE_ACTION_SATISFIED&&_18e==ROLLUP_CONSIDERATION_IF_ATTEMPTED)||(_189==ROLLUP_RULE_ACTION_NOT_SATISFIED&&_18f==ROLLUP_CONSIDERATION_IF_ATTEMPTED)){
this.LogSeq("`684`",_18b);
if(_188.GetAttemptCount()===0){
this.LogSeq("`1147`",_18b);
_18d=false;
}
}else{
this.LogSeq("`1531`",_18b);
this.LogSeq("`97`",_18b);
if((_189==ROLLUP_RULE_ACTION_SATISFIED&&_18e==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)||(_189==ROLLUP_RULE_ACTION_NOT_SATISFIED&&_18f==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)){
this.LogSeq("`468`",_18b);
_18c=this.SequencingRulesCheckProcess(_188,RULE_SET_SKIPPED,_18b);
this.LogSeq("`635`",_18b);
if(_18c!==null){
this.LogSeq("`1095`",_18b);
_18d=false;
}
}
}
}
}
}
this.LogSeq("`844`",_18b);
if(_189==ROLLUP_RULE_ACTION_COMPLETED||_189==ROLLUP_RULE_ACTION_INCOMPLETE){
this.LogSeq("`423`",_18b);
if(_188.RollupProgressCompletion()===true){
this.LogSeq("`591`",_18b);
_18d=true;
var _190=_188.GetRequiredForCompleted();
var _191=_188.GetRequiredForIncomplete();
this.LogSeq("`106`",_18b);
if((_189==ROLLUP_RULE_ACTION_COMPLETED&&_190==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)||(_189==ROLLUP_RULE_ACTION_INCOMPLETE&&_191==ROLLUP_CONSIDERATION_IF_NOT_SUSPENDED)){
this.LogSeq("`279`",_18b);
if(_188.GetAttemptCount()>0&&_188.IsSuspended()===true){
this.LogSeq("`1361`",_18b);
_18d=false;
}
}else{
this.LogSeq("`1555`",_18b);
this.LogSeq("`118`",_18b);
if((_189==ROLLUP_RULE_ACTION_COMPLETED&&_190==ROLLUP_CONSIDERATION_IF_ATTEMPTED)||(_189==ROLLUP_RULE_ACTION_INCOMPLETE&&_191==ROLLUP_CONSIDERATION_IF_ATTEMPTED)){
this.LogSeq("`687`",_18b);
if(_188.GetAttemptCount()===0){
this.LogSeq("`1128`",_18b);
_18d=false;
}
}else{
this.LogSeq("`1523`",_18b);
this.LogSeq("`108`",_18b);
if((_189==ROLLUP_RULE_ACTION_COMPLETED&&_190==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)||(_189==ROLLUP_RULE_ACTION_INCOMPLETE&&_191==ROLLUP_CONSIDERATION_IF_NOT_SKIPPED)){
this.LogSeq("`469`",_18b);
_18c=this.SequencingRulesCheckProcess(_188,RULE_SET_SKIPPED,_18b);
this.LogSeq("`630`",_18b);
if(_18c!==null){
this.LogSeq("`1101`",_18b);
_18d=false;
}
}
}
}
}
}
this.LogSeq("`599`",_18b);
this.LogSeqReturn(_18d,_18b);
return _18d;
}
function Sequencer_ChoiceActivityTraversalSubprocess(_192,_193,_194){
Debug.AssertError("Calling log not passed.",(_194===undefined||_194===null));
var _195=this.LogSeqAudit("`1097`"+_192+", "+_193+")",_194);
var _196=null;
var _197=null;
var _198;
this.LogSeq("`981`",_195);
if(_193==FLOW_DIRECTION_FORWARD){
this.LogSeq("`443`",_195);
_196=this.SequencingRulesCheckProcess(_192,RULE_SET_STOP_FORWARD_TRAVERSAL,_195);
this.LogSeq("`727`",_195);
if(_196!==null){
this.LogSeq("`559`",_195);
_198=new Sequencer_ChoiceActivityTraversalSubprocessResult(false,"SB.2.4-1",IntegrationImplementation.GetString("You are not allowed to move into {0} yet.",_192.GetTitle()));
this.LogSeqReturn(_198,_195);
return _198;
}
this.LogSeq("`614`",_195);
_198=new Sequencer_ChoiceActivityTraversalSubprocessResult(true,null);
this.LogSeqReturn(_198,_195);
return _198;
}
this.LogSeq("`970`",_195);
if(_193==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`1096`",_195);
if(!_192.IsTheRoot()){
_197=this.Activities.GetParentActivity(_192);
this.LogSeq("`585`",_195);
if(_197.GetSequencingControlForwardOnly()){
this.LogSeq("`547`",_195);
_198=new Sequencer_ChoiceActivityTraversalSubprocessResult(false,"SB.2.4-2",IntegrationImplementation.GetString("You must start {0} at the beginning.",_197.GetTitle()));
this.LogSeqReturn(_198,_195);
return _198;
}
}else{
this.LogSeq("`1607`",_195);
this.LogSeq("`238`",_195);
_198=new Sequencer_ChoiceActivityTraversalSubprocessResult(false,"SB.2.4-3",IntegrationImplementation.GetString("You have reached the beginning of the course."));
this.LogSeqReturn(_198,_195);
return _198;
}
this.LogSeq("`607`",_195);
_198=new Sequencer_ChoiceActivityTraversalSubprocessResult(true,null);
this.LogSeqReturn(_198,_195);
return _198;
}
}
function Sequencer_ChoiceActivityTraversalSubprocessResult(_199,_19a,_19b){
this.Reachable=_199;
this.Exception=_19a;
this.ExceptionText=_19b;
}
Sequencer_ChoiceActivityTraversalSubprocessResult.prototype.toString=function(){
return "Reachable="+this.Reachable+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_ChoiceFlowSubprocess(_19c,_19d,_19e){
Debug.AssertError("Calling log not passed.",(_19e===undefined||_19e===null));
var _19f=this.LogSeqAudit("`1333`"+_19c+", "+_19d+")",_19e);
this.LogSeq("`127`",_19f);
var _1a0=this.ChoiceFlowTreeTraversalSubprocess(_19c,_19d,_19f);
this.LogSeq("`724`",_19f);
if(_1a0===null){
this.LogSeq("`713`",_19f);
this.LogSeqReturn(_19c,_19f);
return _19c;
}else{
this.LogSeq("`1660`",_19f);
this.LogSeq("`338`",_19f);
this.LogSeqReturn(_1a0,_19f);
return _1a0;
}
}
function Sequencer_ChoiceFlowTreeTraversalSubprocess(_1a1,_1a2,_1a3){
Debug.AssertError("Calling log not passed.",(_1a3===undefined||_1a3===null));
var _1a4=this.LogSeqAudit("`1046`"+_1a1+", "+_1a2+")",_1a3);
var _1a5=this.Activities.GetParentActivity(_1a1);
var _1a6=null;
var _1a7=null;
var _1a8=null;
this.LogSeq("`956`",_1a4);
if(_1a2==FLOW_DIRECTION_FORWARD){
this.LogSeq("`257`",_1a4);
if(this.IsActivityLastOverall(_1a1,_1a4)){
this.LogSeq("`685`",_1a4);
this.LogSeqReturn("`1731`",_1a4);
return null;
}
this.LogSeq("`477`",_1a4);
if(_1a5.IsActivityTheLastAvailableChild(_1a1)){
this.LogSeq("`139`",_1a4);
_1a6=this.ChoiceFlowTreeTraversalSubprocess(_1a5,FLOW_DIRECTION_FORWARD,_1a4);
this.LogSeq("`144`",_1a4);
this.LogSeqReturn(_1a6,_1a4);
return _1a6;
}else{
this.LogSeq("`1598`",_1a4);
this.LogSeq("`299`",_1a4);
_1a7=_1a5.GetNextActivity(_1a1);
this.LogSeq("`448`",_1a4);
this.LogSeqReturn(_1a7,_1a4);
return _1a7;
}
}
this.LogSeq("`941`",_1a4);
if(_1a2==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`451`",_1a4);
if(_1a1.IsTheRoot()){
this.LogSeq("`680`",_1a4);
this.LogSeqReturn("`1731`",_1a4);
return null;
}
this.LogSeq("`472`",_1a4);
if(_1a5.IsActivityTheFirstAvailableChild(_1a1)){
this.LogSeq("`135`",_1a4);
_1a6=this.ChoiceFlowTreeTraversalSubprocess(_1a5,FLOW_DIRECTION_BACKWARD,_1a4);
this.LogSeq("`137`",_1a4);
this.LogSeqReturn(_1a6,_1a4);
return _1a6;
}else{
this.LogSeq("`1606`",_1a4);
this.LogSeq("`265`",_1a4);
_1a8=_1a5.GetPreviousActivity(_1a1);
this.LogSeq("`441`",_1a4);
this.LogSeqReturn(_1a8,_1a4);
return _1a8;
}
}
}
function Sequencer_ChoiceSequencingRequestProcess(_1a9,_1aa){
Debug.AssertError("Calling log not passed.",(_1aa===undefined||_1aa===null));
var _1ab=this.LogSeqAudit("`1156`"+_1a9+")",_1aa);
var _1ac=null;
var _1ad;
var _1ae=null;
var _1af=null;
var _1b0=null;
var _1b1=null;
var _1b2;
var i;
var _1b4;
this.LogSeq("`498`"+_1a9.LearningObject.ItemIdentifier,_1ab);
if(_1a9===null){
this.LogSeq("`442`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-1",IntegrationImplementation.GetString("Your selection is not permitted.  Please select an available menu item to continue."),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
this.LogSeq("`723`",_1ab);
if(_1a9.IsTheRoot()===false){
_1ac=this.Activities.GetParentActivity(_1a9);
this.LogSeq("`217`",_1ab);
if(_1ac.IsActivityAnAvailableChild(_1a9)===false){
this.LogSeq("`422`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-2",IntegrationImplementation.GetString("The activity '{0}' is not currently available in {1}.",_1a9,_1ac.GetTitle()),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
this.LogSeq("`314`",_1ab);
_1ad=this.GetActivityPath(_1a9,true);
this.LogSeq("`1034`",_1ab);
for(i=(_1ad.length-1);i>=0;i--){
this.LogSeq("`251`",_1ab);
_1ae=this.SequencingRulesCheckProcess(_1ad[i],RULE_SET_HIDE_FROM_CHOICE,_1ab);
this.LogSeq("`740`",_1ab);
if(_1ae!==null){
this.LogSeq("`417`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-3","The activity "+_1a9.GetTitle()+" should be hidden and is not a valid selection",true);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
this.LogSeq("`748`",_1ab);
if(!_1a9.IsTheRoot()){
this.LogSeq("`221`",_1ab);
if(_1ac.GetSequencingControlChoice()===false){
this.LogSeq("`421`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-4",IntegrationImplementation.GetString("The activity '{0}' should be hidden and is not a valid selection.",_1ac.GetTitle()),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
this.LogSeq("`570`",_1ab);
if(this.IsCurrentActivityDefined(_1ab)){
this.LogSeq("`629`",_1ab);
_1b0=this.GetCurrentActivity();
_1af=this.FindCommonAncestor(_1b0,_1a9,_1ab);
}else{
this.LogSeq("`1689`",_1ab);
this.LogSeq("`369`",_1ab);
_1af=this.GetRootActivity(_1ab);
}
if(_1b0!==null&&_1b0.LearningObject.ItemIdentifier==_1a9.LearningObject.ItemIdentifier){
this.LogSeq("`496`",_1ab);
this.LogSeq("`932`",_1ab);
}else{
if(this.AreActivitiesSiblings(_1b0,_1a9,_1ab)){
this.LogSeq("`362`",_1ab);
this.LogSeq("`11`",_1ab);
var _1b5=_1ac.GetActivityListBetweenChildren(_1b0,_1a9,false);
this.LogSeq("`832`",_1ab);
if(_1b5.length===0){
this.LogSeq("`418`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to open"),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
this.LogSeq("`447`",_1ab);
if(_1b5[0].LearningObject.ItemIdentifier==_1b0.LearningObject.ItemIdentifier){
this.LogSeq("`1340`",_1ab);
_1b1=FLOW_DIRECTION_FORWARD;
}else{
this.LogSeq("`1657`",_1ab);
this.LogSeq("`1301`",_1ab);
_1b1=FLOW_DIRECTION_BACKWARD;
_1b5.reverse();
}
this.LogSeq("`1010`",_1ab);
for(i=0;i<_1b5.length;i++){
this.LogSeq("`518`",_1ab);
_1b2=this.ChoiceActivityTraversalSubprocess(_1b5[i],_1b1,_1ab);
this.LogSeq("`710`",_1ab);
if(_1b2.Reachable===false){
this.LogSeq("`142`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,_1b2.Exception,"",false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
this.LogSeq("`1434`",_1ab);
}else{
if(_1b0===null||_1b0.LearningObject.ItemIdentifier==_1af.LearningObject.ItemIdentifier){
this.LogSeq("`209`",_1ab);
this.LogSeq("`245`",_1ab);
_1ad=this.GetPathToAncestorInclusive(_1a9,_1af);
this.LogSeq("`1058`",_1ab);
if(_1ad.length===0){
this.LogSeq("`411`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to open"),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
this.LogSeq("`984`",_1ab);
for(i=_1ad.length-1;i>=0;i--){
this.LogSeq("`516`",_1ab);
_1b2=this.ChoiceActivityTraversalSubprocess(_1ad[i],FLOW_DIRECTION_FORWARD,_1ab);
this.LogSeq("`699`",_1ab);
if(_1b2.Reachable===false){
this.LogSeq("`138`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,_1b2.Exception,_1b2.ExceptionText,false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
this.LogSeq("`17`",_1ab);
if(_1ad[i].IsActive()===false&&_1ad[i].LearningObject.ItemIdentifier!=_1af.LearningObject.ItemIdentifier&&_1ad[i].GetPreventActivation()===true){
this.LogSeq("`390`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-6",IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_1ad[i].GetTitle()),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
this.LogSeq("`1408`",_1ab);
}else{
if(_1a9.LearningObject.ItemIdentifier==_1af.LearningObject.ItemIdentifier){
this.LogSeq("`290`",_1ab);
this.LogSeq("`341`",_1ab);
_1ad=this.GetPathToAncestorInclusive(_1b0,_1af);
this.LogSeq("`1070`",_1ab);
if(_1ad.length===0){
this.LogSeq("`405`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to deliver"),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
this.LogSeq("`987`",_1ab);
for(i=0;i<_1ad.length;i++){
this.LogSeq("`663`",_1ab);
if(i!=(_1ad.length-1)){
this.LogSeq("`198`",_1ab);
if(_1ad[i].GetSequencingControlChoiceExit()===false){
this.LogSeq("`376`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-7",IntegrationImplementation.GetString("Your selection is not permitted.  Please select 'Next' or 'Previous' to move through '{0}'.",_1ad[i]),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
}
this.LogSeq("`1412`",_1ab);
}else{
this.LogSeq("`288`",_1ab);
this.LogSeq("`345`",_1ab);
_1ad=this.GetPathToAncestorInclusive(_1b0,_1af);
this.LogSeq("`1055`",_1ab);
if(_1ad.length===0){
this.LogSeq("`410`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to deliver"),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
this.LogSeq("`1008`",_1ab);
var _1b6=null;
this.LogSeq("`579`",_1ab);
for(i=0;i<_1ad.length;i++){
this.LogSeq("`659`",_1ab);
if(i!=(_1ad.length-1)){
this.LogSeq("`199`",_1ab);
if(_1ad[i].GetSequencingControlChoiceExit()===false){
this.LogSeq("`378`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-7",IntegrationImplementation.GetString("You are not allowed to jump out of {0}.",_1ad[i].GetTitle()),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
this.LogSeq("`397`",_1ab);
if(_1b6===null){
this.LogSeq("`739`",_1ab);
if(_1ad[i].GetConstrainedChoice()===true){
this.LogSeq("`958`",_1ab);
_1b6=_1ad[i];
}
}
}
this.LogSeq("`982`",_1ab);
if(_1b6!==null){
this.LogSeq("`466`",_1ab);
if(this.IsActivity1BeforeActivity2(_1b6,_1a9,_1ab)){
this.LogSeq("`529`",_1ab);
_1b1=FLOW_DIRECTION_FORWARD;
}else{
this.LogSeq("`1581`",_1ab);
this.LogSeq("`517`",_1ab);
_1b1=FLOW_DIRECTION_BACKWARD;
}
this.LogSeq("`521`",_1ab);
var _1b7=this.ChoiceFlowSubprocess(_1b6,_1b1,_1ab);
this.LogSeq("`554`",_1ab);
var _1b8=_1b7;
this.LogSeq("`4`",_1ab);
if((!_1b8.IsActivityAnAvailableDescendent(_1a9))&&(_1a9!=_1b6&&_1a9!=_1b8)){
this.LogSeq("`530`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-8",IntegrationImplementation.GetString("You are not allowed to jump out of {0}.",_1b6.GetTitle()),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
this.LogSeq("`244`",_1ab);
_1ad=this.GetPathToAncestorInclusive(this.Activities.GetParentActivity(_1a9),_1af);
this.LogSeq("`1069`",_1ab);
if(_1ad.length===0){
this.LogSeq("`408`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-5",IntegrationImplementation.GetString("Nothing to open"),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
this.LogSeq("`304`",_1ab);
if(this.IsActivity1BeforeActivity2(_1b0,_1a9,_1ab)){
this.LogSeq("`975`",_1ab);
for(i=(_1ad.length-1);i>=0;i--){
this.LogSeq("`508`",_1ab);
_1b2=this.ChoiceActivityTraversalSubprocess(_1ad[i],FLOW_DIRECTION_FORWARD,_1ab);
this.LogSeq("`681`",_1ab);
if(_1b2.Reachable===false){
this.LogSeq("`132`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,_1b2.Exception,_1b2.ExceptionText,false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
this.LogSeq("`14`",_1ab);
if((_1ad[i].IsActive()===false)&&(_1ad[i]!=_1af)&&(_1ad[i].GetPreventActivation()===true)){
this.LogSeq("`382`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-6",IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_1ad[i].GetTitle()),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
}else{
this.LogSeq("`1622`",_1ab);
this.LogSeq("`976`",_1ab);
for(i=(_1ad.length-1);i>=0;i--){
this.LogSeq("`16`",_1ab);
if((_1ad[i].IsActive()===false)&&(_1ad[i]!=_1af)&&(_1ad[i].GetPreventActivation()===true)){
this.LogSeq("`375`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-6",IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_1ad[i].GetTitle()),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
}
this.LogSeq("`1392`",_1ab);
}
}
}
}
this.LogSeq("`926`",_1ab);
if(_1a9.IsALeaf()===true){
this.LogSeq("`485`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(_1a9,null,"",false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
this.LogSeq("`52`",_1ab);
var _1b9=this.FlowSubprocess(_1a9,FLOW_DIRECTION_FORWARD,true,_1ab);
this.LogSeq("`255`",_1ab);
if(_1b9.Deliverable===false){
if(this.LookAhead===false){
this.LogSeq("`642`",_1ab);
this.TerminateDescendentAttemptsProcess(_1af,_1ab);
this.LogSeq("`834`",_1ab);
this.EndAttemptProcess(_1af,false,_1ab);
this.LogSeq("`876`",_1ab);
this.SetCurrentActivity(_1a9,_1ab);
}
this.LogSeq("`436`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(null,"SB.2.9-9",IntegrationImplementation.GetString("Please select another item from the menu."),false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}else{
this.LogSeq("`1666`",_1ab);
this.LogSeq("`307`",_1ab);
_1b4=new Sequencer_ChoiceSequencingRequestProcessResult(_1b9.IdentifiedActivity,null,"",false);
this.LogSeqReturn(_1b4,_1ab);
return _1b4;
}
}
function Sequencer_ChoiceSequencingRequestProcessResult(_1ba,_1bb,_1bc,_1bd){
if(_1bd===undefined){
Debug.AssertError("no value passed for hidden");
}
this.DeliveryRequest=_1ba;
this.Exception=_1bb;
this.ExceptionText=_1bc;
this.Hidden=_1bd;
}
Sequencer_ChoiceSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", Hidden="+this.Hidden;
};
function Sequencer_ClearSuspendedActivitySubprocess(_1be,_1bf){
Debug.AssertError("Calling log not passed.",(_1bf===undefined||_1bf===null));
var _1c0=this.LogSeqAudit("`1107`"+_1be+")",_1bf);
var _1c1=null;
var _1c2=null;
var _1c3=null;
this.LogSeq("`605`",_1c0);
if(this.IsSuspendedActivityDefined(_1c0)){
this.LogSeq("`602`",_1c0);
_1c2=this.GetSuspendedActivity(_1c0);
_1c1=this.FindCommonAncestor(_1be,_1c2,_1c0);
this.LogSeq("`342`",_1c0);
_1c3=this.GetPathToAncestorInclusive(_1c2,_1c1);
this.LogSeq("`1007`",_1c0);
if(_1c3.length>0){
this.LogSeq("`333`",_1c0);
for(var i=0;i<_1c3.length;i++){
this.LogSeq("`1091`",_1c0);
if(_1c3[i].IsALeaf()){
this.LogSeq("`792`",_1c0);
_1c3[i].SetSuspended(false);
}else{
this.LogSeq("`1556`",_1c0);
this.LogSeq("`403`",_1c0);
if(_1c3[i].HasSuspendedChildren()===false){
this.LogSeq("`770`",_1c0);
_1c3[i].SetSuspended(false);
}
}
}
}
this.LogSeq("`606`",_1c0);
this.ClearSuspendedActivity(_1c0);
}
this.LogSeq("`1011`",_1c0);
this.LogSeqReturn("",_1c0);
return;
}
function Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess(_1c5,_1c6){
if(_1c6===undefined||_1c6===null){
_1c6=this.LogSeqAudit("Content Delivery Environment Activity Data SubProcess for "+activity.StringIdentifier);
}
var _1c7=(Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK||Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR);
if(_1c7){
var _1c8=this.PreviousActivity;
}else{
var _1c8=this.CurrentActivity;
}
var _1c9=this.GetSuspendedActivity(_1c6);
var _1ca=this.GetRootActivity(_1c6);
var _1cb=null;
var _1cc=false;
this.LogSeq("`210`",_1c6);
if(_1c9!=_1c5){
this.LogSeq("`555`",_1c6);
this.ClearSuspendedActivitySubprocess(_1c5,_1c6);
}
this.LogSeq("`231`",_1c6);
this.TerminateDescendentAttemptsProcess(_1c5,_1c6);
this.LogSeq("`67`",_1c6);
_1cb=this.GetPathToAncestorInclusive(_1c5,_1ca);
this.LogSeq("`1080`",_1c6);
var _1cd=ConvertDateToIso8601String(new Date());
for(var i=(_1cb.length-1);i>=0;i--){
this.LogSeq("`1502`"+_1cb[i]+"`1194`",_1c6);
if(_1c7){
var _1cf=_1cb[i].WasActiveBeforeLaunchOnClick;
}else{
var _1cf=_1cb[i].IsActive();
}
if(_1cf===false){
this.LogSeq("`979`",_1c6);
if(_1cb[i].IsTracked()){
this.LogSeq("`116`",_1c6);
if(_1cb[i].IsSuspended()){
this.LogSeq("`810`",_1c6);
_1cb[i].SetSuspended(false);
}else{
this.LogSeq("`1611`",_1c6);
this.LogSeq("`487`",_1c6);
_1cb[i].IncrementAttemptCount();
this.LogSeq("`358`",_1c6);
if(_1cb[i].GetAttemptCount()==1){
this.LogSeq("`773`",_1c6);
_1cb[i].SetActivityProgressStatus(true);
_1cc=true;
}
this.LogSeq("`164`",_1c6);
_1cb[i].InitializeForNewAttempt(true,true);
var atts={ev:"AttemptStart",an:_1cb[i].GetAttemptCount(),ai:_1cb[i].ItemIdentifier,at:_1cb[i].LearningObject.Title};
this.WriteHistoryLog("",atts);
_1cb[i].SetAttemptStartTimestampUtc(_1cd);
_1cb[i].SetAttemptAbsoluteDuration("PT0H0M0S");
_1cb[i].SetAttemptExperiencedDurationTracked("PT0H0M0S");
_1cb[i].SetAttemptExperiencedDurationReported("PT0H0M0S");
if(Control.Package.Properties.ResetRunTimeDataTiming==RESET_RT_DATA_TIMING_ON_EACH_NEW_SEQUENCING_ATTEMPT){
if(_1cb[i].IsDeliverable()===true){
if(_1cc===false){
var atts={ev:"ResetRuntime",ai:_1cb[i].ItemIdentifier,at:_1cb[i].LearningObject.Title};
this.WriteHistoryLog("",atts);
}
_1cb[i].RunTime.ResetState();
}
}
}
}
}
}
}
function Sequencer_ContentDeliveryEnvironmentProcess(_1d1,_1d2){
Debug.AssertError("Calling log not passed.",(_1d2===undefined||_1d2===null));
var _1d3=this.LogSeqAudit("`1145`"+_1d1+")",_1d2);
var _1d4=(Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK||Control.Package.Properties.ScoLaunchType===LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR);
var _1d5=this.GetCurrentActivity();
var _1d6=this.GetSuspendedActivity(_1d3);
var _1d7=this.GetRootActivity(_1d3);
var _1d8=null;
var _1d9;
this.LogSeq("`200`",_1d3);
if(_1d5!==null&&_1d5.IsActive()){
this.LogSeq("`266`",_1d3);
_1d9=new Sequencer_ContentDeliveryEnvironmentProcessResult(false,"DB.2.1",IntegrationImplementation.GetString("The previous activity must be terminated before a new activity may be attempted"));
this.LogSeqReturn(_1d9,_1d3);
return _1d9;
}
if(!_1d4){
this.ContentDeliveryEnvironmentActivityDataSubProcess(_1d1,_1d2);
}else{
this.LogSeq("`228`",_1d3);
}
this.LogSeq("`67`",_1d3);
_1d8=this.GetPathToAncestorInclusive(_1d1,_1d7);
this.LogSeq("`1080`",_1d3);
for(var i=(_1d8.length-1);i>=0;i--){
if(_1d4){
_1d8[i].WasActiveBeforeLaunchOnClick=_1d8[i].IsActive();
}
this.LogSeq("`1502`"+_1d8[i]+"`1194`",_1d3);
if(_1d8[i].IsActive()===false){
this.LogSeq("`892`",_1d3);
_1d8[i].SetActive(true);
}
}
this.LogSeq("`234`"+_1d1.GetItemIdentifier(),_1d3);
if(_1d4){
this.PreviousActivity=_1d5;
}
this.SetCurrentActivity(_1d1,_1d3);
this.LogSeq("`823`",_1d3);
_1d9=new Sequencer_ContentDeliveryEnvironmentProcessResult(true,null,"");
this.LogSeqReturn(_1d9,_1d3);
Control.DeliverActivity(_1d1);
return _1d9;
}
function Sequencer_ContentDeliveryEnvironmentProcessResult(_1db,_1dc,_1dd){
this.Valid=_1db;
this.Exception=_1dc;
this.ExceptionText=_1dd;
}
Sequencer_ContentDeliveryEnvironmentProcessResult.prototype.toString=function(){
return "Valid="+this.Valid+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_ContinueSequencingRequestProcess(_1de){
Debug.AssertError("Calling log not passed.",(_1de===undefined||_1de===null));
var _1df=this.LogSeqAudit("`1133`",_1de);
var _1e0;
this.LogSeq("`504`",_1df);
if(!this.IsCurrentActivityDefined(_1df)){
this.LogSeq("`427`",_1df);
_1e0=new Sequencer_ContinueSequencingRequestProcessResult(null,"SB.2.7-1",IntegrationInterface.GetString("The sequencing session has not begun yet."));
this.LogSeqReturn(_1e0,_1df);
return _1e0;
}
var _1e1=this.GetCurrentActivity();
this.LogSeq("`712`",_1df);
if(!_1e1.IsTheRoot()){
var _1e2=this.Activities.GetParentActivity(_1e1);
this.LogSeq("`297`",_1df);
if(_1e2.GetSequencingControlFlow()===false){
this.LogSeq("`583`",_1df);
_1e0=new Sequencer_ContinueSequencingRequestProcessResult(null,"SB.2.7-2",IntegrationImplementation.GetString("You cannot use 'Next' to enter {0}. Please select a menu item to continue.",_1e2.GetTitle()));
this.LogSeqReturn(_1e0,_1df);
return _1e0;
}
}
this.LogSeq("`136`",_1df);
var _1e3=this.FlowSubprocess(_1e1,FLOW_DIRECTION_FORWARD,false,_1df);
this.LogSeq("`986`",_1df);
if(_1e3.Deliverable===false){
this.LogSeq("`227`",_1df);
_1e0=new Sequencer_ContinueSequencingRequestProcessResult(null,_1e3.Exception,_1e3.ExceptionText);
this.LogSeqReturn(_1e0,_1df);
return _1e0;
}else{
this.LogSeq("`1679`",_1df);
this.LogSeq("`318`",_1df);
_1e0=new Sequencer_ContinueSequencingRequestProcessResult(_1e3.IdentifiedActivity,null,"");
this.LogSeqReturn(_1e0,_1df);
return _1e0;
}
}
function Sequencer_ContinueSequencingRequestProcessResult(_1e4,_1e5,_1e6){
this.DeliveryRequest=_1e4;
this.Exception=_1e5;
this.ExceptionText=_1e6;
}
Sequencer_ContinueSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_DeliveryRequestProcess(_1e7,_1e8){
Debug.AssertError("Calling log not passed.",(_1e8===undefined||_1e8===null));
var _1e9=this.LogSeqAudit("`1327`"+_1e7+")",_1e8);
var _1ea;
var _1eb;
this.LogSeq("`874`"+_1e7+"`954`",_1e9);
if(!_1e7.IsALeaf()){
this.LogSeq("`589`",_1e9);
_1eb=new Sequencer_DeliveryRequestProcessResult(false,"DB.1.1-1",IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_1e7.GetTitle()));
this.LogSeqReturn(_1eb,_1e9);
return _1eb;
}
this.LogSeq("`208`",_1e9);
var _1ec=this.GetActivityPath(_1e7,true);
this.LogSeq("`839`",_1e9);
if(_1ec.length===0){
this.LogSeq("`584`",_1e9);
_1eb=new Sequencer_DeliveryRequestProcessResult(false,"DB.1.1-2",IntegrationImplementation.GetString("Nothing to open"));
this.LogSeqReturn(_1eb,_1e9);
return _1eb;
}
this.LogSeq("`527`",_1e9);
for(var i=0;i<_1ec.length;i++){
this.LogSeq("`862`"+_1ec[i],_1e9);
_1ea=this.CheckActivityProcess(_1ec[i],_1e9);
this.LogSeq("`904`",_1e9);
if(_1ea===true){
this.LogSeq("`568`",_1e9);
_1eb=new Sequencer_DeliveryRequestProcessResult(false,"DB.1.1-3",IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_1e7.GetTitle()));
this.LogSeqReturn(_1eb,_1e9);
return _1eb;
}
}
this.LogSeq("`667`",_1e9);
_1eb=new Sequencer_DeliveryRequestProcessResult(true,null,"");
this.LogSeqReturn(_1eb,_1e9);
return _1eb;
}
function Sequencer_DeliveryRequestProcessResult(_1ee,_1ef,_1f0){
this.Valid=_1ee;
this.Exception=_1ef;
this.ExceptionText=_1f0;
}
Sequencer_DeliveryRequestProcessResult.prototype.toString=function(){
return "Valid="+this.Valid+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_EndAttemptProcess(_1f1,_1f2,_1f3,_1f4){
Debug.AssertError("Calling log not passed.",(_1f3===undefined||_1f3===null));
if(_1f4===undefined||_1f4===null){
_1f4=false;
}
var _1f5=this.LogSeqAudit("`1440`"+_1f1+", "+_1f2+")",_1f3);
this.LogSeq("`1475`"+_1f1.GetItemIdentifier()+"`1671`",_1f5);
var i;
var _1f7=new Array();
if(_1f1.IsALeaf()){
this.LogSeq("`1000`",_1f5);
if(_1f1.IsTracked()&&_1f1.WasLaunchedThisSession()){
this.LogSeq("`861`",_1f5);
_1f1.TransferRteDataToActivity();
this.LogSeq("`309`",_1f5);
if(_1f1.IsSuspended()===false){
this.LogSeq("`282`",_1f5);
if(_1f1.IsCompletionSetByContent()===false){
this.LogSeq("`243`",_1f5);
if(_1f1.GetAttemptProgressStatus()===false){
this.LogSeq("`737`",_1f5);
_1f1.SetAttemptProgressStatus(true);
this.LogSeq("`711`",_1f5);
_1f1.SetAttemptCompletionStatus(true);
_1f1.WasAutoCompleted=true;
}
}
this.LogSeq("`295`",_1f5);
if(_1f1.IsObjectiveSetByContent()===false){
this.LogSeq("`592`",_1f5);
var _1f8=_1f1.GetPrimaryObjective();
this.LogSeq("`187`",_1f5);
if(_1f8.GetProgressStatus(_1f1,false)===false){
this.LogSeq("`662`",_1f5);
_1f8.SetProgressStatus(true,false,_1f1);
this.LogSeq("`654`",_1f5);
_1f8.SetSatisfiedStatus(true,false,_1f1);
_1f1.WasAutoSatisfied=true;
}
}
}
}
}else{
this.LogSeq("`1203`",_1f5);
this.LogSeq("`107`",_1f5);
if(this.ActivityHasSuspendedChildren(_1f1,_1f5)){
this.LogSeq("`822`",_1f5);
_1f1.SetSuspended(true);
}else{
this.LogSeq("`1690`",_1f5);
this.LogSeq("`812`",_1f5);
_1f1.SetSuspended(false);
}
}
if(_1f4===false){
this.LogSeq("`460`",_1f5);
_1f1.SetActive(false);
}
var _1f9;
if(_1f2===false){
this.LogSeq("`247`",_1f5);
_1f9=this.OverallRollupProcess(_1f1,_1f5);
}else{
this.LogSeq("`519`",_1f5);
_1f7[0]=_1f1;
}
this.LogSeq("`230`",_1f5);
var _1fa=this.FindActivitiesAffectedByWriteMaps(_1f1);
var _1fb=this.FindDistinctParentsOfActivitySet(_1fa);
if(_1f2===false){
this.LogSeq("`453`",_1f5);
var _1fc=this.GetMinimalSubsetOfActivitiesToRollup(_1fb,_1f9);
for(i=0;i<_1fc.length;i++){
if(_1fc[i]!==null){
this.OverallRollupProcess(_1fc[i],_1f5);
}
}
}else{
this.LogSeq("`218`",_1f5);
_1f7=_1f7.concat(_1fb);
}
if(this.LookAhead===false&&_1f4===false){
this.RandomizeChildrenProcess(_1f1,true,_1f5);
}
this.LogSeq("`1291`",_1f5);
this.LogSeqReturn("",_1f5);
return _1f7;
}
function Sequencer_EvaluateRollupConditionsSubprocess(_1fd,_1fe,_1ff){
Debug.AssertError("Calling log not passed.",(_1ff===undefined||_1ff===null));
var _200=this.LogSeqAudit("`1045`"+_1fd+", "+_1fe+")",_1ff);
var _201;
var _202;
this.LogSeq("`388`",_200);
var _203=new Array();
var i;
this.LogSeq("`790`",_200);
for(i=0;i<_1fe.Conditions.length;i++){
this.LogSeq("`37`",_200);
_201=Sequencer_EvaluateRollupRuleCondition(_1fd,_1fe.Conditions[i]);
this.LogSeq("`367`",_200);
if(_1fe.Conditions[i].Operator==RULE_CONDITION_OPERATOR_NOT&&_201!=RESULT_UNKNOWN){
this.LogSeq("`1150`",_200);
_201=(!_201);
}
this.LogSeq("`966`"+_201+"`536`",_200);
_203[_203.length]=_201;
}
this.LogSeq("`311`",_200);
if(_203.length===0){
this.LogSeq("`690`",_200);
return RESULT_UNKNOWN;
}
this.LogSeq("`1099`"+_1fe.ConditionCombination+"`291`",_200);
if(_1fe.ConditionCombination==RULE_CONDITION_COMBINATION_ANY){
_202=false;
for(i=0;i<_203.length;i++){
_202=Sequencer_LogicalOR(_202,_203[i]);
}
}else{
_202=true;
for(i=0;i<_203.length;i++){
_202=Sequencer_LogicalAND(_202,_203[i]);
}
}
this.LogSeq("`495`",_200);
this.LogSeqReturn(_202,_200);
return _202;
}
function Sequencer_EvaluateRollupRuleCondition(_205,_206){
var _207=null;
switch(_206.Condition){
case ROLLUP_RULE_CONDITION_SATISFIED:
_207=_205.IsSatisfied("");
break;
case ROLLUP_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN:
_207=_205.IsObjectiveStatusKnown("",false);
break;
case ROLLUP_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN:
_207=_205.IsObjectiveMeasureKnown("",false);
break;
case ROLLUP_RULE_CONDITION_COMPLETED:
_207=_205.IsCompleted("",false);
break;
case ROLLUP_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN:
_207=_205.IsActivityProgressKnown("",false);
break;
case ROLLUP_RULE_CONDITION_ATTEMPTED:
_207=_205.IsAttempted();
break;
case ROLLUP_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED:
_207=_205.IsAttemptLimitExceeded();
break;
case ROLLUP_RULE_CONDITION_NEVER:
_207=false;
break;
default:
this.LogSeq("`1370`",logParent);
break;
}
return _207;
}
function Sequencer_ExitSequencingRequestProcess(_208){
Debug.AssertError("Calling log not passed.",(_208===undefined||_208===null));
var _209=this.LogSeqAudit("`1214`",_208);
var _20a;
this.LogSeq("`486`",_209);
if(!this.IsCurrentActivityDefined(_209)){
this.LogSeq("`511`",_209);
_20a=new Sequencer_ExitSequencingRequestProcessResult(false,"SB.2.11-1",IntegrationImplementation.GetString("An 'Exit Sequencing' request cannot be processed until the sequencing session has begun."));
this.LogSeqReturn(_20a,_209);
return _20a;
}
var _20b=this.GetCurrentActivity();
this.LogSeq("`320`",_209);
if(_20b.IsActive()){
this.LogSeq("`510`",_209);
_20a=new Sequencer_ExitSequencingRequestProcessResult(false,"SB.2.11-2",IntegrationImplementation.GetString("An 'Exit Sequencing' request cannot be processed while an activity is still active."));
this.LogSeqReturn(_20a,_209);
return _20a;
}
this.LogSeq("`753`",_209);
if(_20b.IsTheRoot()||this.CourseIsSingleSco()===true){
this.LogSeq("`220`",_209);
_20a=new Sequencer_ExitSequencingRequestProcessResult(true,null,"");
this.LogSeqReturn(_20a,_209);
return _20a;
}
this.LogSeq("`560`",_209);
_20a=new Sequencer_ExitSequencingRequestProcessResult(false,null,"");
this.LogSeqReturn(_20a,_209);
return _20a;
}
function Sequencer_ExitSequencingRequestProcessResult(_20c,_20d,_20e){
this.EndSequencingSession=_20c;
this.Exception=_20d;
this.ExceptionText=_20e;
}
Sequencer_ExitSequencingRequestProcessResult.prototype.toString=function(){
return "EndSequencingSession="+this.EndSequencingSession+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_FlowActivityTraversalSubprocess(_20f,_210,_211,_212){
Debug.AssertError("Calling log not passed.",(_212===undefined||_212===null));
var _213=this.LogSeqAudit("`1141`"+_20f+", "+_210+", "+_211+")",_212);
var _214;
var _215;
var _216;
var _217;
var _218;
var _219=this.Activities.GetParentActivity(_20f);
this.LogSeq("`458`",_213);
if(_219.GetSequencingControlFlow()===false){
this.LogSeq("`384`",_213);
_218=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_20f,"SB.2.2-1",IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_219.GetTitle()));
this.LogSeqReturn(_218,_213);
return _218;
}
this.LogSeq("`531`",_213);
_214=this.SequencingRulesCheckProcess(_20f,RULE_SET_SKIPPED,_213);
this.LogSeq("`359`",_213);
if(_214!==null){
this.LogSeq("`181`",_213);
_215=this.FlowTreeTraversalSubprocess(_20f,_210,_211,false,_213);
this.LogSeq("`638`",_213);
if(_215.NextActivity===null){
this.LogSeq("`158`",_213);
_218=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_20f,_215.Exception,_215.ExceptionText);
this.LogSeqReturn(_218,_213);
return _218;
}else{
this.LogSeq("`1654`",_213);
this.LogSeq("`66`",_213);
if(_211==FLOW_DIRECTION_BACKWARD&&_215.TraversalDirection==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`35`",_213);
_216=this.FlowActivityTraversalSubprocess(_215.NextActivity,_215.TraversalDirection,null,_213);
}else{
this.LogSeq("`1586`",_213);
this.LogSeq("`9`",_213);
_216=this.FlowActivityTraversalSubprocess(_215.NextActivity,_210,_211,_213);
}
this.LogSeq("`233`",_213);
_218=_216;
this.LogSeqReturn(_218,_213);
return _218;
}
}
this.LogSeq("`564`",_213);
_217=this.CheckActivityProcess(_20f,_213);
this.LogSeq("`920`",_213);
if(_217===true){
this.LogSeq("`385`",_213);
_218=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_20f,"SB.2.2-2",IntegrationImplementation.GetString("You cannot select '{0}' at this time.  Please select another menu item to continue with '{0}.'",_20f.GetTitle()));
this.LogSeqReturn(_218,_213);
return _218;
}
this.LogSeq("`276`",_213);
if(_20f.IsALeaf()===false){
this.LogSeq("`160`",_213);
_215=this.FlowTreeTraversalSubprocess(_20f,_210,null,true,_213);
this.LogSeq("`637`",_213);
if(_215.NextActivity===null){
this.LogSeq("`157`",_213);
_218=new Sequencer_FlowActivityTraversalSubprocessReturnObject(false,_20f,_215.Exception,_215.ExceptionText);
this.LogSeqReturn(_218,_213);
return _218;
}else{
this.LogSeq("`1647`",_213);
this.LogSeq("`45`",_213);
if(_210==FLOW_DIRECTION_BACKWARD&&_215.TraversalDirection==FLOW_DIRECTION_FORWARD){
this.LogSeq("`22`",_213);
_216=this.FlowActivityTraversalSubprocess(_215.NextActivity,FLOW_DIRECTION_FORWARD,FLOW_DIRECTION_BACKWARD,_213);
}else{
this.LogSeq("`1588`",_213);
this.LogSeq("`30`",_213);
_216=this.FlowActivityTraversalSubprocess(_215.NextActivity,_210,null,_213);
}
this.LogSeq("`225`",_213);
_218=_216;
this.LogSeqReturn(_218,_213);
return _218;
}
}
this.LogSeq("`357`",_213);
_218=new Sequencer_FlowActivityTraversalSubprocessReturnObject(true,_20f,null,"");
this.LogSeqReturn(_218,_213);
return _218;
}
function Sequencer_FlowActivityTraversalSubprocessReturnObject(_21a,_21b,_21c,_21d){
this.Deliverable=_21a;
this.NextActivity=_21b;
this.Exception=_21c;
this.ExceptionText=_21d;
}
Sequencer_FlowActivityTraversalSubprocessReturnObject.prototype.toString=function(){
return "Deliverable="+this.Deliverable+", NextActivity="+this.NextActivity+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_FlowSubprocess(_21e,_21f,_220,_221){
Debug.AssertError("Calling log not passed.",(_221===undefined||_221===null));
var _222=this.LogSeqAudit("`1489`"+_21e+", "+_21f+", "+_220+")",_221);
var _223;
this.LogSeq("`509`",_222);
var _224=_21e;
this.LogSeq("`6`",_222);
var _225=this.FlowTreeTraversalSubprocess(_224,_21f,null,_220,_222);
this.LogSeq("`488`",_222);
if(_225.NextActivity===null){
this.LogSeq("`205`",_222);
_223=new Sequencer_FlowSubprocessResult(_224,false,_225.Exception,_225.ExceptionText);
this.LogSeqReturn(_223,_222);
return _223;
}else{
this.LogSeq("`1691`",_222);
this.LogSeq("`557`",_222);
_224=_225.NextActivity;
this.LogSeq("`48`",_222);
var _226=this.FlowActivityTraversalSubprocess(_224,_21f,null,_222);
this.LogSeq("`18`",_222);
_223=new Sequencer_FlowSubprocessResult(_226.NextActivity,_226.Deliverable,_226.Exception,_226.ExceptionText);
this.LogSeqReturn(_223,_222);
return _223;
}
}
function Sequencer_FlowSubprocessResult(_227,_228,_229,_22a){
this.IdentifiedActivity=_227;
this.Deliverable=_228;
this.Exception=_229;
this.ExceptionText=_22a;
}
Sequencer_FlowSubprocessResult.prototype.toString=function(){
return "IdentifiedActivity="+this.IdentifiedActivity+", Deliverable="+this.Deliverable+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_FlowTreeTraversalSubprocess(_22b,_22c,_22d,_22e,_22f){
Debug.AssertError("Calling log not passed.",(_22f===undefined||_22f===null));
var _230=this.LogSeqAudit("`1237`"+_22b+", "+_22c+", "+_22d+", "+_22e+")",_22f);
var _231;
var _232;
var _233;
var _234;
var _235;
var _236;
var _237=this.Activities.GetParentActivity(_22b);
this.LogSeq("`1174`",_230);
_231=false;
this.LogSeq("`24`",_230);
if(_22d!==null&&_22d==FLOW_DIRECTION_BACKWARD&&_237.IsActivityTheLastAvailableChild(_22b)){
this.LogSeq("`1126`",_230);
_22c=FLOW_DIRECTION_BACKWARD;
this.LogSeq("`552`",_230);
_22b=_237.GetFirstAvailableChild();
this.LogSeq("`1151`",_230);
_231=true;
}
this.LogSeq("`980`",_230);
if(_22c==FLOW_DIRECTION_FORWARD){
this.LogSeq("`264`",_230);
if((this.IsActivityLastOverall(_22b,_230))||(_22e===false&&_22b.IsTheRoot()===true)){
this.LogSeq("`439`",_230);
_236=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-1",IntegrationImplementation.GetString("You have reached the end of the course."));
this.LogSeqReturn(_236,_230);
return _236;
}
this.LogSeq("`755`",_230);
if(_22b.IsALeaf()||_22e===false){
this.LogSeq("`474`",_230);
if(_237.IsActivityTheLastAvailableChild(_22b)){
this.LogSeq("`29`",_230);
_232=this.FlowTreeTraversalSubprocess(_237,FLOW_DIRECTION_FORWARD,null,false,_230);
this.LogSeq("`224`",_230);
_236=_232;
this.LogSeqReturn(_236,_230);
return _236;
}else{
this.LogSeq("`1592`",_230);
this.LogSeq("`300`",_230);
_233=_237.GetNextActivity(_22b);
this.LogSeq("`201`",_230);
_236=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_233,_22c,null,"");
this.LogSeqReturn(_236,_230);
return _236;
}
}else{
this.LogSeq("`1131`",_230);
this.LogSeq("`395`",_230);
_234=_22b.GetAvailableChildren();
if(_234.length>0){
this.LogSeq("`327`"+_234[0]+"); Traversal Direction: traversal direction ("+_22c+"); Exception: n/a )",_230);
_236=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_234[0],_22c,null,"");
this.LogSeqReturn(_236,_230);
return _236;
}else{
this.LogSeq("`1599`",_230);
this.LogSeq("`426`",_230);
_236=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-2",IntegrationImplementation.GetString("The activity '{0}' does not have any available children to deliver.",_22b.GetTitle()));
this.LogSeqReturn(_236,_230);
return _236;
}
}
}
this.LogSeq("`971`",_230);
if(_22c==FLOW_DIRECTION_BACKWARD){
this.LogSeq("`462`",_230);
if(_22b.IsTheRoot()){
this.LogSeq("`434`",_230);
_236=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-3",IntegrationImplementation.GetString("You have reached the beginning of the course."));
this.LogSeqReturn(_236,_230);
return _236;
}
this.LogSeq("`765`",_230);
if(_22b.IsALeaf()||_22e===false){
this.LogSeq("`337`",_230);
if(_231===false){
this.LogSeq("`316`",_230);
if(_237.GetSequencingControlForwardOnly()===true){
this.LogSeq("`389`",_230);
_236=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-4",IntegrationImplementation.GetString("The activity '{0}' may only be entered from the beginning.",_237.GetTitle()));
this.LogSeqReturn(_236,_230);
return _236;
}
}
this.LogSeq("`473`",_230);
if(_237.IsActivityTheFirstAvailableChild(_22b)){
this.LogSeq("`25`",_230);
_232=this.FlowTreeTraversalSubprocess(_237,FLOW_DIRECTION_BACKWARD,null,false,_230);
this.LogSeq("`229`",_230);
_236=_232;
this.LogSeqReturn(_236,_230);
return _236;
}else{
this.LogSeq("`1597`",_230);
this.LogSeq("`268`",_230);
_235=_237.GetPreviousActivity(_22b);
this.LogSeq("`798`"+_235+"`1535`"+_22c+"`1633`",_230);
_236=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_235,_22c,null,"");
this.LogSeqReturn(_236,_230);
return _236;
}
}else{
this.LogSeq("`1079`",_230);
this.LogSeq("`379`",_230);
_234=_22b.GetAvailableChildren();
if(_234.length>0){
this.LogSeq("`665`",_230);
if(_22b.GetSequencingControlForwardOnly()===true){
this.LogSeq("`49`",_230);
_236=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_234[0],FLOW_DIRECTION_FORWARD,null,"");
this.LogSeqReturn(_236,_230);
return _236;
}else{
this.LogSeq("`1566`",_230);
this.LogSeq("`43`",_230);
_236=new Sequencer_FlowTreeTraversalSubprocessReturnObject(_234[_234.length-1],FLOW_DIRECTION_BACKWARD,null,"");
this.LogSeqReturn(_236,_230);
return _236;
}
}else{
this.LogSeq("`1595`",_230);
this.LogSeq("`404`",_230);
_236=new Sequencer_FlowTreeTraversalSubprocessReturnObject(null,null,"SB.2.1-2",IntegrationInterface.GetString("The activity '{0}' may only be entered from the beginning.",_237.GetTitle()));
this.LogSeqReturn(_236,_230);
return _236;
}
}
}
}
function Sequencer_FlowTreeTraversalSubprocessReturnObject(_238,_239,_23a,_23b){
this.NextActivity=_238;
this.TraversalDirection=_239;
this.Exception=_23a;
this.ExceptionText=_23b;
}
Sequencer_FlowTreeTraversalSubprocessReturnObject.prototype.toString=function(){
return "NextActivity="+this.NextActivity+", TraversalDirection="+this.TraversalDirection+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_LimitConditionsCheckProcess(_23c,_23d){
Debug.AssertError("Calling log not passed.",(_23d===undefined||_23d===null));
var _23e=this.LogSeqAudit("`1271`"+_23c+")",_23d);
this.LogSeq("`377`",_23e);
if(_23c.IsTracked()===false){
this.LogSeq("`292`",_23e);
this.LogSeqReturn("`1728`",_23e);
return false;
}
this.LogSeq("`143`",_23e);
if(_23c.IsActive()||_23c.IsSuspended()){
this.LogSeq("`677`",_23e);
this.LogSeqReturn("`1728`",_23e);
return false;
}
this.LogSeq("`705`",_23e);
if(_23c.GetLimitConditionAttemptControl()===true){
this.LogSeq("`89`",_23e);
var _23f=_23c.GetAttemptCount();
var _240=_23c.GetLimitConditionAttemptLimit();
if(_23c.GetActivityProgressStatus()===true&&(_23f>=_240)){
this.LogSeq("`416`",_23e);
this.LogSeqReturn("`1732`",_23e);
return true;
}
}
this.LogSeq("`539`",_23e);
this.LogSeq("`428`",_23e);
this.LogSeqReturn("`1728`",_23e);
return false;
}
function Sequencer_MeasureRollupProcess(_241,_242){
Debug.AssertError("Calling log not passed.",(_242===undefined||_242===null));
var _243=this.LogSeqAudit("`1360`"+_241+")",_242);
this.LogSeq("`961`",_243);
var _244=0;
this.LogSeq("`1322`",_243);
var _245=false;
this.LogSeq("`1039`",_243);
var _246=0;
this.LogSeq("`1052`",_243);
var _247=null;
var i;
this.LogSeq("`626`",_243);
var _247=_241.GetPrimaryObjective();
this.LogSeq("`1102`",_243);
if(_247!==null){
this.LogSeq("`1157`",_243);
var _249=_241.GetChildren();
var _24a=null;
var _24b;
var _24c;
var _24d;
for(i=0;i<_249.length;i++){
this.LogSeq("`556`"+_249[i].IsTracked(),_243);
if(_249[i].IsTracked()){
this.LogSeq("`978`",_243);
_24a=null;
this.LogSeq("`1158`",_243);
var _24a=_249[i].GetPrimaryObjective();
this.LogSeq("`962`",_243);
if(_24a!==null){
this.LogSeq("`545`",_243);
_24c=_249[i].GetRollupObjectiveMeasureWeight();
_246+=_24c;
this.LogSeq("`600`",_243);
if(_24a.GetMeasureStatus(_249[i],false)===true){
this.LogSeq("`122`",_243);
_24d=_24a.GetNormalizedMeasure(_249[i],false);
this.LogSeq("`829`"+_24d+"`1458`"+_24c,_243);
_245=true;
_244+=(_24d*_24c);
}
}else{
this.LogSeq("`1567`",_243);
this.LogSeq("`501`",_243);
Debug.AssertError("Measure Rollup Process encountered an activity with no primary objective.");
this.LogSeqReturn("",_243);
return;
}
}
}
this.LogSeq("`1233`",_243);
if(_245===false||_246==0){
this.LogSeq("`103`"+_246+")",_243);
_247.SetMeasureStatus(false,_241);
}else{
this.LogSeq("`1644`",_243);
this.LogSeq("`688`",_243);
_247.SetMeasureStatus(true,_241);
this.LogSeq("`491`"+_244+"`1374`"+_246+"`1718`"+(_244/_246),_243);
var _24e=(_244/_246);
_24e=RoundToPrecision(_24e,7);
_247.SetNormalizedMeasure(_24e,_241);
}
this.LogSeq("`1190`",_243);
this.LogSeqReturn("",_243);
return;
}
this.LogSeq("`542`",_243);
this.LogSeqReturn("",_243);
return;
}
function Sequencer_NavigationRequestProcess(_24f,_250,_251){
Debug.AssertError("Calling log not passed.",(_251===undefined||_251===null));
var _252=this.LogSeqAudit("`1293`"+_24f+", "+_250+")",_251);
var _253=this.GetCurrentActivity();
var _254=null;
if(_253!==null){
_254=_253.ParentActivity;
}
var _255="";
if(_254!==null){
_255=_254.GetTitle();
}
var _256;
switch(_24f){
case NAVIGATION_REQUEST_START:
this.LogSeq("`1162`",_252);
this.LogSeq("`463`",_252);
if(!this.IsCurrentActivityDefined(_252)){
this.LogSeq("`207`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,null,SEQUENCING_REQUEST_START,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}else{
this.LogSeq("`1640`",_252);
this.LogSeq("`176`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-1",null,IntegrationImplementation.GetString("The sequencing session has already been started."));
this.LogSeqReturn(_256,_252);
return _256;
}
break;
case NAVIGATION_REQUEST_RESUME_ALL:
this.LogSeq("`1037`",_252);
this.LogSeq("`465`",_252);
if(!this.IsCurrentActivityDefined(_252)){
this.LogSeq("`334`",_252);
if(this.IsSuspendedActivityDefined(_252)){
this.LogSeq("`168`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,null,SEQUENCING_REQUEST_RESUME_ALL,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}else{
this.LogSeq("`1580`",_252);
this.LogSeq("`161`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-3",null,IntegrationImplementation.GetString("There is no suspended activity to resume."));
this.LogSeqReturn(_256,_252);
return _256;
}
}else{
this.LogSeq("`1649`",_252);
this.LogSeq("`177`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-1",null,IntegrationImplementation.GetString("The sequencing session has already been started."));
this.LogSeqReturn(_256,_252);
return _256;
}
break;
case NAVIGATION_REQUEST_CONTINUE:
this.LogSeq("`1081`",_252);
this.LogSeq("`482`",_252);
if(!this.IsCurrentActivityDefined(_252)){
this.LogSeq("`179`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-1",null,IntegrationImplementation.GetString("Cannot continue until the sequencing session has begun."));
this.LogSeqReturn(_256,_252);
return _256;
}
this.LogSeq("`40`",_252);
if((!_253.IsTheRoot())&&(_254.LearningObject.SequencingData.ControlFlow===true)){
this.LogSeq("`213`",_252);
if(_253.IsActive()){
this.LogSeq("`185`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_CONTINUE,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}else{
this.LogSeq("`1591`",_252);
this.LogSeq("`192`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,null,SEQUENCING_REQUEST_CONTINUE,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}
}else{
this.LogSeq("`1659`",_252);
this.LogSeq("`34`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-4",null,IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_255));
this.LogSeqReturn(_256,_252);
return _256;
}
break;
case NAVIGATION_REQUEST_PREVIOUS:
this.LogSeq("`1090`",_252);
this.LogSeq("`483`",_252);
if(!this.IsCurrentActivityDefined(_252)){
this.LogSeq("`184`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("Cannot move backwards until the sequencing session has begun."));
this.LogSeqReturn(_256,_252);
return _256;
}
this.LogSeq("`241`",_252);
if(!_253.IsTheRoot()){
this.LogSeq("`12`",_252);
if(_254.LearningObject.SequencingData.ControlFlow===true&&_254.LearningObject.SequencingData.ControlForwardOnly===false){
this.LogSeq("`204`",_252);
if(_253.IsActive()){
this.LogSeq("`173`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_PREVIOUS,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}else{
this.LogSeq("`1569`",_252);
this.LogSeq("`180`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,null,SEQUENCING_REQUEST_PREVIOUS,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}
}else{
this.LogSeq("`1609`",_252);
this.LogSeq("`102`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-5",null,IntegrationImplementation.GetString("Please select a menu item to continue with {0}.",_254.GetTitle()));
this.LogSeqReturn(_256,_252);
return _256;
}
}else{
this.LogSeq("`1656`",_252);
this.LogSeq("`50`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-6",null,IntegrationImplementation.GetString("You have reached the beginning of the course."));
this.LogSeqReturn(_256,_252);
return _256;
}
break;
case NAVIGATION_REQUEST_FORWARD:
this.LogSeq("`799`",_252);
this.LogSeq("`188`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-7",null,IntegrationImplementation.GetString("The 'Forward' navigation request is not supported, try using 'Continue'."));
this.LogSeqReturn(_256,_252);
return _256;
case NAVIGATION_REQUEST_BACKWARD:
this.LogSeq("`788`",_252);
this.LogSeq("`189`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-7",null,IntegrationImplementation.GetString("The 'Backward' navigation request is not supported, try using 'Previous'."));
this.LogSeqReturn(_256,_252);
return _256;
case NAVIGATION_REQUEST_CHOICE:
this.LogSeq("`1100`",_252);
this.LogSeq("`196`",_252);
if(this.DoesActivityExist(_250,_252)){
var _257=this.GetActivityFromIdentifier(_250,_252);
var _258=this.Activities.GetParentActivity(_257);
if(_257.IsAvailable()===false){
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-7",null,IntegrationImplementation.GetString("The activity '{0}' was not selected to be delivered in this attempt.",_257));
this.LogSeqReturn(_256,_252);
return _256;
}
this.LogSeq("`2`",_252);
if(_257.IsTheRoot()||_258.LearningObject.SequencingData.ControlChoice===true){
this.LogSeq("`450`",_252);
if(!this.IsCurrentActivityDefined(_252)){
this.LogSeq("`59`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,null,SEQUENCING_REQUEST_CHOICE,null,_257,"");
this.LogSeqReturn(_256,_252);
return _256;
}
this.LogSeq("`125`",_252);
if(!this.AreActivitiesSiblings(_253,_257,_252)){
this.LogSeq("`365`",_252);
var _259=this.FindCommonAncestor(_253,_257,_252);
this.LogSeq("`1`",_252);
var _25a=this.GetPathToAncestorExclusive(_253,_259,true);
this.LogSeq("`938`",_252);
if(_25a.length>0){
this.LogSeq("`95`",_252);
for(var i=0;i<_25a.length;i++){
if(_25a[i].LearningObject.ItemIdentifier==_259.ItemIdentifier){
break;
}
this.LogSeq("`216`"+_25a[i].LearningObject.ItemIdentifier+")",_252);
if(_25a[i].IsActive()===true&&_25a[i].LearningObject.SequencingData.ControlChoiceExit===false){
this.LogSeq("`87`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-8",null,IntegrationImplementation.GetString("You must complete '{0}' before you can select another item.",_25a[i]));
this.LogSeqReturn(_256,_252);
return _256;
}
}
}else{
this.LogSeq("`1525`",_252);
this.LogSeq("`147`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-9",null,IntegrationImplementation.GetString("Nothing to open"));
this.LogSeqReturn(_256,_252);
return _256;
}
}
this.LogSeq("`206`",_252);
if(_253.IsActive()){
this.LogSeq("`55`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_CHOICE,null,_257,"");
this.LogSeqReturn(_256,_252);
return _256;
}else{
this.LogSeq("`1570`",_252);
this.LogSeq("`61`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,null,SEQUENCING_REQUEST_CHOICE,null,_257,"");
this.LogSeqReturn(_256,_252);
return _256;
}
}else{
this.LogSeq("`1604`",_252);
this.LogSeq("`99`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-10",null,IntegrationImplementation.GetString("Please select 'Next' or 'Previous' to move through {0}.",_258.GetTitle()));
this.LogSeqReturn(_256,_252);
return _256;
}
}else{
this.LogSeq("`1648`",_252);
this.LogSeq("`86`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-11",null,IntegrationImplementation.GetString("The activity you selected ({0}) does not exist.",_250));
this.LogSeqReturn(_256,_252);
return _256;
}
break;
case NAVIGATION_REQUEST_EXIT:
this.LogSeq("`1164`",_252);
this.LogSeq("`506`",_252);
if(this.IsCurrentActivityDefined(_252)){
this.LogSeq("`285`",_252);
if(_253.IsActive()){
this.LogSeq("`194`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,TERMINATION_REQUEST_EXIT,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}else{
this.LogSeq("`1603`",_252);
this.LogSeq("`75`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-12",null,IntegrationImplementation.GetString("The Exit navigation request is invalid because the current activity ({0}) is no longer active.",_253.GetTitle()));
this.LogSeqReturn(_256,_252);
return _256;
}
}else{
this.LogSeq("`1645`",_252);
this.LogSeq("`172`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The Exit navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_256,_252);
return _256;
}
break;
case NAVIGATION_REQUEST_EXIT_ALL:
this.LogSeq("`1087`",_252);
this.LogSeq("`275`",_252);
if(this.IsCurrentActivityDefined(_252)){
this.LogSeq("`193`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,TERMINATION_REQUEST_EXIT_ALL,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}else{
this.LogSeq("`1646`",_252);
this.LogSeq("`178`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The Exit All navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_256,_252);
return _256;
}
break;
case NAVIGATION_REQUEST_ABANDON:
this.LogSeq("`1085`",_252);
this.LogSeq("`505`",_252);
if(this.IsCurrentActivityDefined(_252)){
this.LogSeq("`289`",_252);
if(_253.IsActive()){
this.LogSeq("`182`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,TERMINATION_REQUEST_ABANDON,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}else{
this.LogSeq("`1575`",_252);
this.LogSeq("`154`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-12",null,IntegrationImplementation.GetString("The 'Abandon' navigation request is invalid because the current activity '{0}' is no longer active.",_253.GetTitle()));
this.LogSeqReturn(_256,_252);
return _256;
}
}else{
this.LogSeq("`1596`",_252);
this.LogSeq("`174`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The 'Abandon' navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_256,_252);
return _256;
}
break;
case NAVIGATION_REQUEST_ABANDON_ALL:
this.LogSeq("`1005`",_252);
this.LogSeq("`277`",_252);
if(this.IsCurrentActivityDefined(_252)){
this.LogSeq("`170`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,TERMINATION_REQUEST_ABANDON_ALL,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}else{
this.LogSeq("`1616`",_252);
this.LogSeq("`165`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("You cannot use 'Abandon All' if no item is currently open."));
this.LogSeqReturn(_256,_252);
return _256;
}
break;
case NAVIGATION_REQUEST_SUSPEND_ALL:
this.LogSeq("`995`",_252);
this.LogSeq("`540`",_252);
if(this.IsCurrentActivityDefined(_252)){
this.LogSeq("`166`",_252);
_256=new Sequencer_NavigationRequestProcessResult(_24f,TERMINATION_REQUEST_SUSPEND_ALL,SEQUENCING_REQUEST_EXIT,null,null,"");
this.LogSeqReturn(_256,_252);
return _256;
}else{
this.LogSeq("`1628`",_252);
this.LogSeq("`169`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-2",null,IntegrationImplementation.GetString("The 'Suspend All' navigation request is invalid because there is no current activity."));
this.LogSeqReturn(_256,_252);
return _256;
}
break;
default:
this.LogSeq("`96`",_252);
_256=new Sequencer_NavigationRequestProcessResult(NAVIGATION_REQUEST_NOT_VALID,null,null,"NB.2.1-13",null,IntegrationImplementation.GetString("Undefined Navigation Request"));
this.LogSeqReturn(_256,_252);
return _256;
}
}
function Sequencer_NavigationRequestProcessResult(_25c,_25d,_25e,_25f,_260,_261){
this.NavigationRequest=_25c;
this.TerminationRequest=_25d;
this.SequencingRequest=_25e;
this.Exception=_25f;
this.TargetActivity=_260;
this.ExceptionText=_261;
}
Sequencer_NavigationRequestProcessResult.prototype.toString=function(){
return "NavigationRequest="+this.NavigationRequest+", TerminationRequest="+this.TerminationRequest+", SequencingRequest="+this.SequencingRequest+", Exception="+this.Exception+", TargetActivity="+this.TargetActivity+", ExceptionText="+this.ExceptionText;
};
function Sequencer_ObjectiveRollupProcess(_262,_263){
Debug.AssertError("Calling log not passed.",(_263===undefined||_263===null));
var _264=this.LogSeqAudit("`1347`"+_262+")",_263);
this.LogSeq("`1067`",_264);
var _265=null;
this.LogSeq("`627`",_264);
var _265=_262.GetPrimaryObjective();
this.LogSeq("`1054`",_264);
if(_265!==null){
this.LogSeq("`693`",_264);
if(_265.GetSatisfiedByMeasure()===true){
this.LogSeq("`864`",_264);
this.ObjectiveRollupUsingMeasureProcess(_262,_264);
this.LogSeq("`1134`",_264);
this.LogSeqReturn("",_264);
return;
}
this.LogSeq("`590`",_264);
if(Sequencer_GetApplicableSetofRollupRules(_262,RULE_SET_SATISFIED).length>0||Sequencer_GetApplicableSetofRollupRules(_262,RULE_SET_NOT_SATISFIED).length>0){
this.LogSeq("`879`",_264);
this.ObjectiveRollupUsingRulesProcess(_262,_264);
this.LogSeq("`1137`",_264);
this.LogSeqReturn("",_264);
return;
}
this.LogSeq("`386`",_264);
this.ObjectiveRollupUsingDefaultProcess(_262,_264);
this.LogSeq("`1186`",_264);
this.LogSeqReturn("",_264);
return;
}else{
this.LogSeq("`1655`",_264);
this.LogSeq("`479`",_264);
this.LogSeqReturn("",_264);
return;
}
}
function Sequencer_ObjectiveRollupUsingDefaultProcess(_266,_267){
Debug.AssertError("Calling log not passed.",(_267===undefined||_267===null));
var _268=this.LogSeqAudit("`1018`"+_266+")",_267);
var _269;
var _26a;
var _26b;
this.LogSeq("`1224`",_268);
if(_266.IsALeaf()){
this.LogSeq("`1250`",_268);
this.LogSeqReturn("",_268);
return;
}
this.LogSeq("`1019`",_268);
var _26c=null;
var i;
this.LogSeq("`620`",_268);
var _26c=_266.GetPrimaryObjective();
this.LogSeq("`1078`",_268);
var _26e=_266.GetChildren();
this.LogSeq("`1041`",_268);
var _26f=true;
this.LogSeq("`1114`",_268);
var _270=true;
this.LogSeq("`1274`",_268);
var _271=true;
for(i=0;i<_26e.length;i++){
this.LogSeq("`1247`"+(i+1)+" - "+_26e[i],_268);
this.LogSeq("`1089`",_268);
_269=_26e[i].IsSatisfied();
this.LogSeq("`1076`",_268);
_26a=_26e[i].IsAttempted();
this.LogSeq("`756`",_268);
_26b=(_269===false||_26a===true);
this.LogSeq("`1533`"+_269+"`1694`"+_26a+"`1683`"+_26b,_268);
if(this.CheckChildForRollupSubprocess(_26e[i],ROLLUP_RULE_ACTION_SATISFIED,_268)){
this.LogSeq("`916`",_268);
_270=(_270&&(_269===true));
_271=false;
}
if(this.CheckChildForRollupSubprocess(_26e[i],ROLLUP_RULE_ACTION_NOT_SATISFIED,_268)){
this.LogSeq("`807`",_268);
_26f=(_26f&&_26b);
_271=false;
}
}
if(_271&&Control.Package.Properties.RollupEmptySetToUnknown){
this.LogSeq("`537`"+Control.Package.Properties.RollupEmptySetToUnknown+")",_268);
}else{
this.LogSeq("`1001`"+_26f+")",_268);
if(_26f===true){
this.LogSeq("`671`",_268);
_26c.SetProgressStatus(true,false,_266);
this.LogSeq("`652`",_268);
_26c.SetSatisfiedStatus(false,false,_266);
}
this.LogSeq("`1077`"+_270+")",_268);
if(_270===true){
this.LogSeq("`648`",_268);
_26c.SetProgressStatus(true,false,_266);
this.LogSeq("`645`",_268);
_26c.SetSatisfiedStatus(true,false,_266);
}
}
this.LogSeqReturn("",_268);
}
function Sequencer_ObjectiveRollupUsingMeasureProcess(_272,_273){
Debug.AssertError("Calling log not passed.",(_273===undefined||_273===null));
var _274=this.LogSeqAudit("`1025`"+_272+")",_273);
this.LogSeq("`1016`",_274);
var _275=null;
this.LogSeq("`617`",_274);
var _275=_272.GetPrimaryObjective();
this.LogSeq("`1054`",_274);
if(_275!==null){
this.LogSeq("`128`",_274);
if(_275.GetSatisfiedByMeasure()===true){
this.LogSeq("`302`",_274);
if(_275.GetMeasureStatus(_272,false)===false){
this.LogSeq("`628`",_274);
_275.SetProgressStatus(false,false,_272);
}else{
this.LogSeq("`1559`",_274);
this.LogSeq("`130`",_274);
if(_272.IsActive()===false||_272.GetMeasureSatisfactionIfActive()===true){
this.LogSeq("`105`",_274);
if(_275.GetNormalizedMeasure(_272,false)>=_275.GetMinimumSatisfiedNormalizedMeasure()){
this.LogSeq("`613`",_274);
_275.SetProgressStatus(true,false,_272);
this.LogSeq("`604`",_274);
_275.SetSatisfiedStatus(true,false,_272);
}else{
this.LogSeq("`1484`",_274);
this.LogSeq("`612`",_274);
_275.SetProgressStatus(true,false,_272);
this.LogSeq("`598`",_274);
_275.SetSatisfiedStatus(false,false,_272);
}
}else{
this.LogSeq("`1534`",_274);
this.LogSeq("`270`",_274);
_275.SetProgressStatus(false,false,_272);
}
}
}
this.LogSeq("`911`",_274);
this.LogSeqReturn("",_274);
return;
}else{
this.LogSeq("`1655`",_274);
this.LogSeq("`391`",_274);
this.LogSeqReturn("",_274);
return;
}
}
function Sequencer_ObjectiveRollupUsingRulesProcess(_276,_277){
Debug.AssertError("Calling log not passed.",(_277===undefined||_277===null));
var _278=this.LogSeqAudit("`1053`"+_276+")",_277);
var _279;
this.LogSeq("`1048`",_278);
var _27a=null;
this.LogSeq("`616`",_278);
var _27a=_276.GetPrimaryObjective();
this.LogSeq("`1059`",_278);
if(_27a!==null){
this.LogSeq("`296`",_278);
_279=this.RollupRuleCheckSubprocess(_276,RULE_SET_NOT_SATISFIED,_278);
this.LogSeq("`805`",_278);
if(_279===true){
this.LogSeq("`656`",_278);
_27a.SetProgressStatus(true,false,_276);
this.LogSeq("`636`",_278);
_27a.SetSatisfiedStatus(false,false,_276);
}
this.LogSeq("`328`",_278);
_279=this.RollupRuleCheckSubprocess(_276,RULE_SET_SATISFIED,_278);
this.LogSeq("`801`",_278);
if(_279===true){
this.LogSeq("`653`",_278);
_27a.SetProgressStatus(true,false,_276);
this.LogSeq("`644`",_278);
_27a.SetSatisfiedStatus(true,false,_276);
}
this.LogSeq("`947`",_278);
this.LogSeqReturn("",_278);
return;
}else{
this.LogSeq("`1641`",_278);
this.LogSeq("`432`",_278);
this.LogSeqReturn("",_278);
return;
}
}
function Sequencer_OverallRollupProcess(_27b,_27c){
Debug.AssertError("Calling log not passed.",(_27c===undefined||_27c===null));
var _27d=this.LogSeqAudit("`1369`"+_27b+")",_27c);
this.LogSeq("`256`",_27d);
var _27e=this.GetActivityPath(_27b,true);
this.LogSeq("`1115`",_27d);
if(_27e.length===0){
this.LogSeq("`895`",_27d);
this.LogSeqReturn("",_27d);
return;
}
this.LogSeq("`1043`",_27d);
var _27f;
var _280;
var _281;
var _282;
var _283;
var _284;
var _285;
var _286;
var _287;
var _288;
var _289;
var _28a;
var _28b;
var _28c=new Array();
var _28d=false;
for(var i=0;i<_27e.length;i++){
if(!_27e[i].IsALeaf()){
_27e[i].RollupDurations();
}
if(_28d){
continue;
}
_27f=_27e[i].GetPrimaryObjective();
_280=_27f.GetMeasureStatus(_27e[i],false);
_281=_27f.GetNormalizedMeasure(_27e[i],false);
_282=_27f.GetProgressStatus(_27e[i],false);
_283=_27f.GetSatisfiedStatus(_27e[i],false);
_284=_27e[i].GetAttemptProgressStatus();
_285=_27e[i].GetAttemptCompletionStatus();
this.LogSeq("`553`",_27d);
if(!_27e[i].IsALeaf()){
this.LogSeq("`563`",_27d);
this.MeasureRollupProcess(_27e[i],_27d);
}
this.LogSeq("`115`",_27d);
this.ObjectiveRollupProcess(_27e[i],_27d);
this.LogSeq("`126`",_27d);
this.ActivityProgressRollupProcess(_27e[i],_27d);
_286=_27f.GetMeasureStatus(_27e[i],false);
_287=_27f.GetNormalizedMeasure(_27e[i],false);
_288=_27f.GetProgressStatus(_27e[i],false);
_289=_27f.GetSatisfiedStatus(_27e[i],false);
_28a=_27e[i].GetAttemptProgressStatus();
_28b=_27e[i].GetAttemptCompletionStatus();
if(!this.LookAhead&&_27e[i].IsTheRoot()){
if(_28a!=_284||_28b!=_285){
var _28f=(_28a?(_28b?SCORM_STATUS_COMPLETED:SCORM_STATUS_INCOMPLETE):SCORM_STATUS_NOT_ATTEMPTED);
this.WriteHistoryLog("",{ev:"Rollup Completion",v:_28f,ai:_27e[i].ItemIdentifier});
}
if(_288!=_282||_289!=_283){
var _290=(_288?(_289?SCORM_STATUS_PASSED:SCORM_STATUS_FAILED):SCORM_STATUS_UNKNOWN);
this.WriteHistoryLog("",{ev:"Rollup Satisfaction",v:_290,ai:_27e[i].ItemIdentifier});
}
}
if(_289!=_283){
_27e[i].WasAutoSatisfied=_27b.WasAutoSatisfied;
}
if(_28b!=_285){
_27e[i].WasAutoCompleted=_27b.WasAutoCompleted;
}
if(i>0&&_286==_280&&_287==_281&&_288==_282&&_289==_283&&_28a==_284&&_28b==_285){
this.LogSeq("`901`",_27d);
_28d=true;
}else{
this.LogSeq("`865`",_27d);
_28c[_28c.length]=_27e[i];
}
}
this.LogSeq("`1261`",_27d);
this.LogSeqReturn("",_27d);
return _28c;
}
function Sequencer_OverallSequencingProcess(_291){
var _292=null;
var _293=null;
var _294=null;
var _295=null;
var _296=null;
this.ResetException();
var _297=this.LogSeqAudit("`1349`");
this.LogSeq("`1399`"+this.NavigationRequest,_297);
if(this.NavigationRequest===null){
var _298=this.GetExitAction(this.GetCurrentActivity(),_297);
this.LogSeq("`1035`"+_298,_297);
var _299="";
if(_298==EXIT_ACTION_EXIT_CONFIRMATION||_298==EXIT_ACTION_DISPLAY_MESSAGE){
var _29a=Control.Activities.GetRootActivity();
var _29b=(_29a.IsCompleted()||_29a.IsSatisfied());
if(_29b===true){
_299=IntegrationImplementation.GetString("The course is now complete. Please make a selection to continue.");
}else{
_299=IntegrationImplementation.GetString("Please make a selection.");
}
}
switch(_298){
case (EXIT_ACTION_EXIT_NO_CONFIRMATION):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
break;
case (EXIT_ACTION_EXIT_CONFIRMATION):
if(confirm("Would you like to exit the course now?")){
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
}else{
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE,null,_299);
}
break;
case (EXIT_ACTION_GO_TO_NEXT_SCO):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_CONTINUE,null,"");
break;
case (EXIT_ACTION_DISPLAY_MESSAGE):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE,null,_299);
break;
case (EXIT_ACTION_DO_NOTHING):
this.NavigationRequest=null;
break;
case (EXIT_ACTION_REFRESH_PAGE):
Control.RefreshPage();
break;
}
}
if(this.NavigationRequest==null){
this.LogSeqReturn("`1393`",_297);
return;
}
if(this.NavigationRequest.Type==NAVIGATION_REQUEST_DISPLAY_MESSAGE){
this.LogSeq("`696`",_297);
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT,null,"");
}
if(this.NavigationRequest.Type==NAVIGATION_REQUEST_EXIT_PLAYER){
this.LogSeq("`813`",_297);
Control.ExitScormPlayer();
return;
}
this.LogSeq("`763`",_297);
var _29c=this.NavigationRequestProcess(this.NavigationRequest.Type,this.NavigationRequest.TargetActivity,_297);
this.LogSeq("`624`",_297);
if(_29c.NavigationRequest==NAVIGATION_REQUEST_NOT_VALID){
this.LogSeq("`731`",_297);
this.Exception=_29c.Exception;
this.ExceptionText=_29c.ExceptionText;
this.LogSeq("`853`",_297);
this.LogSeqReturn("`1728`",_297);
return false;
}
_292=_29c.TerminationRequest;
_293=_29c.SequencingRequest;
_294=_29c.TargetActivity;
this.LogSeq("`368`",_297);
if(_292!==null){
this.LogSeq("`706`",_297);
var _29d=this.TerminationRequestProcess(_292,_297);
this.LogSeq("`603`",_297);
if(_29d.TerminationRequest==TERMINATION_REQUEST_NOT_VALID){
this.LogSeq("`702`",_297);
this.Exception=_29d.Exception;
this.ExceptionText=_29d.ExceptionText;
this.LogSeq("`833`",_297);
this.LogSeqReturn("`1728`",_297);
return false;
}
this.LogSeq("`701`",_297);
if(_29d.SequencingRequest!==null){
this.LogSeq("`39`",_297);
_293=_29d.SequencingRequest;
}
}
this.LogSeq("`1060`",_297);
if(_293!==null){
this.LogSeq("`732`",_297);
var _29e=this.SequencingRequestProcess(_293,_294,_297);
this.LogSeq("`608`",_297);
if(_29e.SequencingRequest==SEQUENCING_REQUEST_NOT_VALID){
this.LogSeq("`716`",_297);
this.Exception=_29e.Exception;
this.ExceptionText=_29e.ExceptionText;
this.LogSeq("`824`",_297);
this.LogSeqReturn("`1728`",_297);
return false;
}
this.LogSeq("`535`",_297);
if(_29e.EndSequencingSession===true){
this.LogSeq("`76`",_297);
Control.ExitScormPlayer();
this.LogSeqReturn("",_297);
return;
}
this.LogSeq("`582`",_297);
if(_29e.DeliveryRequest===null){
this.LogSeq("`826`",_297);
this.Exception="OP.1.4";
this.ExceptionText=IntegrationImplementation.GetString("Please make a selection.");
this.LogSeqReturn("",_297);
return;
}
this.LogSeq("`574`",_297);
_295=_29e.DeliveryRequest;
}
this.LogSeq("`1098`",_297);
if(_295!==null){
this.LogSeq("`782`",_297);
var _29f=this.DeliveryRequestProcess(_295,_297);
this.LogSeq("`633`",_297);
if(_29f.Valid===false){
this.LogSeq("`738`",_297);
this.Exception="OP.1.5";
this.ExceptionText=IntegrationImplementation.GetString("Please make a selection.");
this.LogSeq("`820`",_297);
this.LogSeqReturn("`1728`",_297);
return false;
}
this.LogSeq("`657`",_297);
this.ContentDeliveryEnvironmentProcess(_295,_297);
}
this.LogSeq("`944`",_297);
this.LogSeqReturn("",_297);
return;
}
function Sequencer_PreviousSequencingRequestProcess(_2a0){
Debug.AssertError("Calling log not passed.",(_2a0===undefined||_2a0===null));
var _2a1=this.LogSeqAudit("`1129`",_2a0);
var _2a2;
this.LogSeq("`502`",_2a1);
if(!this.IsCurrentActivityDefined(_2a1)){
this.LogSeq("`407`",_2a1);
_2a2=new Sequencer_PreviousSequencingRequestProcessResult(null,"SB.2.8-1",IntegrationImplementation.GetString("You cannot use 'Previous' at this time."));
this.LogSeqReturn(_2a2,_2a1);
return _2a2;
}
var _2a3=this.GetCurrentActivity();
this.LogSeq("`708`",_2a1);
if(!_2a3.IsTheRoot()){
var _2a4=this.Activities.GetParentActivity(_2a3);
this.LogSeq("`298`",_2a1);
if(_2a4.GetSequencingControlFlow()===false){
this.LogSeq("`596`",_2a1);
_2a2=new Sequencer_PreviousSequencingRequestProcessResult(null,"SB.2.8-2",IntegrationImplementation.GetString("Please select 'Next' or 'Previous' to move through {0}.",_2a4.GetTitle()));
this.LogSeqReturn(_2a2,_2a1);
return _2a2;
}
}
this.LogSeq("`131`",_2a1);
var _2a5=this.FlowSubprocess(_2a3,FLOW_DIRECTION_BACKWARD,false,_2a1);
this.LogSeq("`985`",_2a1);
if(_2a5.Deliverable===false){
this.LogSeq("`226`",_2a1);
_2a2=new Sequencer_PreviousSequencingRequestProcessResult(null,_2a5.Exception,_2a5.ExceptionText);
this.LogSeqReturn(_2a2,_2a1);
return _2a2;
}else{
this.LogSeq("`1682`",_2a1);
this.LogSeq("`323`",_2a1);
_2a2=new Sequencer_PreviousSequencingRequestProcessResult(_2a5.IdentifiedActivity,null,"");
this.LogSeqReturn(_2a2,_2a1);
return _2a2;
}
}
function Sequencer_PreviousSequencingRequestProcessResult(_2a6,_2a7,_2a8){
this.DeliveryRequest=_2a6;
this.Exception=_2a7;
this.ExceptionText=_2a8;
}
Sequencer_PreviousSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_RandomizeChildrenProcess(_2a9,_2aa,_2ab){
Debug.AssertError("Calling log not passed.",(_2ab===undefined||_2ab===null));
var _2ac=this.LogSeqAudit("`1325`"+_2a9+")",_2ab);
var _2ad;
this.LogSeq("`532`",_2ac);
if(_2a9.IsALeaf()){
this.LogSeq("`1184`",_2ac);
this.LogSeqReturn("",_2ac);
return;
}
this.LogSeq("`153`",_2ac);
if(_2a9.IsActive()===true||_2a9.IsSuspended()===true){
this.LogSeq("`1173`",_2ac);
this.LogSeqReturn("",_2ac);
return;
}
var _2ae=_2a9.GetRandomizationTiming();
switch(_2ae){
case TIMING_NEVER:
this.LogSeq("`845`",_2ac);
this.LogSeq("`1185`",_2ac);
break;
case TIMING_ONCE:
this.LogSeq("`856`",_2ac);
this.LogSeq("`778`",_2ac);
if(_2a9.GetRandomizedChildren()===false){
this.LogSeq("`429`",_2ac);
if(_2a9.GetActivityProgressStatus()===false){
this.LogSeq("`863`",_2ac);
if(_2a9.GetRandomizeChildren()===true){
this.LogSeq("`567`",_2ac);
_2ad=Sequencer_RandomizeArray(_2a9.GetAvailableChildren());
_2a9.SetAvailableChildren(_2ad);
_2a9.SetRandomizedChildren(true);
}
}
}
this.LogSeq("`1188`",_2ac);
break;
case TIMING_ON_EACH_NEW_ATTEMPT:
this.LogSeq("`694`",_2ac);
this.LogSeq("`872`",_2ac);
if(_2a9.GetRandomizeChildren()===true){
this.LogSeq("`581`",_2ac);
_2ad=Sequencer_RandomizeArray(_2a9.GetAvailableChildren());
_2a9.SetAvailableChildren(_2ad);
_2a9.SetRandomizedChildren(true);
if(_2aa===true){
Control.RedrawChildren(_2a9);
}
}
this.LogSeq("`1171`",_2ac);
break;
default:
this.LogSeq("`828`",_2ac);
break;
}
this.LogSeqReturn("",_2ac);
return;
}
function Sequencer_RandomizeArray(ary){
var _2b0=ary.length;
var orig;
var swap;
for(var i=0;i<_2b0;i++){
var _2b4=Math.floor(Math.random()*_2b0);
orig=ary[i];
swap=ary[_2b4];
ary[i]=swap;
ary[_2b4]=orig;
}
return ary;
}
function Sequencer_ResumeAllSequencingRequestProcess(_2b5){
Debug.AssertError("Calling log not passed.",(_2b5===undefined||_2b5===null));
var _2b6=this.LogSeqAudit("`1093`",_2b5);
var _2b7;
this.LogSeq("`494`",_2b6);
if(this.IsCurrentActivityDefined(_2b6)){
this.LogSeq("`401`",_2b6);
_2b7=new Sequencer_ResumeAllSequencingRequestProcessResult(null,"SB.2.6-1",IntegrationImplementation.GetString("A 'Resume All' sequencing request cannot be processed while there is a current activity defined."));
this.LogSeqReturn(_2b7,_2b6);
return _2b7;
}
this.LogSeq("`400`",_2b6);
if(!this.IsSuspendedActivityDefined(_2b6)){
this.LogSeq("`398`",_2b6);
_2b7=new Sequencer_ResumeAllSequencingRequestProcessResult(null,"SB.2.6-2",IntegrationImplementation.GetString("There is no suspended activity to resume."));
this.LogSeqReturn(_2b7,_2b6);
return _2b7;
}
this.LogSeq("`57`",_2b6);
var _2b8=this.GetSuspendedActivity(_2b6);
_2b7=new Sequencer_ResumeAllSequencingRequestProcessResult(_2b8,null,"");
this.LogSeqReturn(_2b7,_2b6);
return _2b7;
}
function Sequencer_ResumeAllSequencingRequestProcessResult(_2b9,_2ba,_2bb){
this.DeliveryRequest=_2b9;
this.Exception=_2ba;
this.ExceptionText=_2bb;
}
Sequencer_ResumeAllSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_RetrySequencingRequestProcess(_2bc){
Debug.AssertError("Calling log not passed.",(_2bc===undefined||_2bc===null));
var _2bd=this.LogSeqAudit("`1192`",_2bc);
var _2be;
var _2bf;
this.LogSeq("`489`",_2bd);
if(!this.IsCurrentActivityDefined(_2bd)){
this.LogSeq("`433`",_2bd);
_2be=new Sequencer_RetrySequencingRequestProcessResult(null,"SB.2.10-1",IntegrationImplementation.GetString("You cannot use 'Resume All' while the current item is open."));
this.LogSeqReturn(_2be,_2bd);
return _2be;
}
var _2c0=this.GetCurrentActivity();
this.LogSeq("`101`",_2bd);
if(_2c0.IsActive()||_2c0.IsSuspended()){
this.LogSeq("`435`",_2bd);
_2be=new Sequencer_RetrySequencingRequestProcessResult(null,"SB.2.10-2",IntegrationImplementation.GetString("A 'Retry' sequencing request cannot be processed while there is an active or suspended activity."));
this.LogSeqReturn(_2be,_2bd);
return _2be;
}
this.LogSeq("`968`",_2bd);
if(!_2c0.IsALeaf()){
this.LogSeq("`370`",_2bd);
flowSubProcessResult=this.FlowSubprocess(_2c0,FLOW_DIRECTION_FORWARD,true,_2bd);
this.LogSeq("`942`",_2bd);
if(flowSubProcessResult.Deliverable===false){
this.LogSeq("`414`",_2bd);
_2be=new Sequencer_RetrySequencingRequestProcessResult(null,"SB.2.10-3",IntegrationImplementation.GetString("You cannot 'Retry' this item because: {1}",_2c0.GetTitle(),flowSubProcessResult.ExceptionText));
this.LogSeqReturn(_2be,_2bd);
return _2be;
}else{
this.LogSeq("`1636`",_2bd);
this.LogSeq("`319`",_2bd);
_2be=new Sequencer_RetrySequencingRequestProcessResult(flowSubProcessResult.IdentifiedActivity,null,"");
this.LogSeqReturn(_2be,_2bd);
return _2be;
}
}else{
this.LogSeq("`1667`",_2bd);
this.LogSeq("`484`",_2bd);
_2be=new Sequencer_RetrySequencingRequestProcessResult(_2c0,null,"");
this.LogSeqReturn(_2be,_2bd);
return _2be;
}
}
function Sequencer_RetrySequencingRequestProcessResult(_2c1,_2c2,_2c3){
this.DeliveryRequest=_2c1;
this.Exception=_2c2;
this.ExceptionText=_2c3;
}
Sequencer_RetrySequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_RollupRuleCheckSubprocess(_2c4,_2c5,_2c6){
Debug.AssertError("Calling log not passed.",(_2c6===undefined||_2c6===null));
var _2c7=this.LogSeqAudit("`1255`"+_2c4+", "+_2c5+")",_2c6);
var _2c8;
var _2c9;
var _2ca;
var _2cb;
var _2cc=0;
var _2cd=0;
var _2ce=0;
this.LogSeq("`329`",_2c7);
var _2cf=Sequencer_GetApplicableSetofRollupRules(_2c4,_2c5);
if(_2cf.length>0){
this.LogSeq("`214`",_2c7);
_2c8=_2c4.GetChildren();
this.LogSeq("`1124`",_2c7);
for(var i=0;i<_2cf.length;i++){
this.LogSeq("`745`",_2c7);
contributingChldren=new Array();
_2cc=0;
_2cd=0;
_2ce=0;
this.LogSeq("`1111`",_2c7);
for(var j=0;j<_2c8.length;j++){
this.LogSeq("`974`",_2c7);
if(_2c8[j].IsTracked()===true){
this.LogSeq("`237`",_2c7);
_2c9=this.CheckChildForRollupSubprocess(_2c8[j],_2cf[i].Action,_2c7);
this.LogSeq("`744`",_2c7);
if(_2c9===true){
this.LogSeq("`156`",_2c7);
_2ca=this.EvaluateRollupConditionsSubprocess(_2c8[j],_2cf[i],_2c7);
this.LogSeq("`310`",_2c7);
if(_2ca==RESULT_UNKNOWN){
this.LogSeq("`719`",_2c7);
_2ce++;
if(_2cf[i].ChildActivitySet==CHILD_ACTIVITY_SET_ALL||_2cf[i].ChildActivitySet==CHILD_ACTIVITY_SET_NONE){
j=_2c8.length;
}
}else{
this.LogSeq("`1495`",_2c7);
this.LogSeq("`686`",_2c7);
if(_2ca===true){
this.LogSeq("`766`",_2c7);
_2cc++;
if(_2cf[i].ChildActivitySet==CHILD_ACTIVITY_SET_ANY||_2cf[i].ChildActivitySet==CHILD_ACTIVITY_SET_NONE){
j=_2c8.length;
}
}else{
this.LogSeq("`1494`",_2c7);
this.LogSeq("`754`",_2c7);
_2cd++;
if(_2cf[i].ChildActivitySet==CHILD_ACTIVITY_SET_ALL){
j=_2c8.length;
}
}
}
}
}
}
switch(_2cf[i].ChildActivitySet){
case CHILD_ACTIVITY_SET_ALL:
this.LogSeq("`927`",_2c7);
this.LogSeq("`541`",_2c7);
if(_2cd===0&&_2ce===0){
this.LogSeq("`1149`",_2c7);
_2cb=true;
}
break;
case CHILD_ACTIVITY_SET_ANY:
this.LogSeq("`931`",_2c7);
this.LogSeq("`689`",_2c7);
if(_2cc>0){
this.LogSeq("`1142`",_2c7);
_2cb=true;
}
break;
case CHILD_ACTIVITY_SET_NONE:
this.LogSeq("`914`",_2c7);
this.LogSeq("`546`",_2c7);
if(_2cc===0&&_2ce===0){
this.LogSeq("`1123`",_2c7);
_2cb=true;
}
break;
case CHILD_ACTIVITY_SET_AT_LEAST_COUNT:
this.LogSeq("`814`",_2c7);
this.LogSeq("`271`",_2c7);
if(_2cc>=_2cf[i].MinimumCount){
this.LogSeq("`1138`",_2c7);
_2cb=true;
}
break;
case CHILD_ACTIVITY_SET_AT_LEAST_PERCENT:
this.LogSeq("`786`",_2c7);
this.LogSeq("`117`",_2c7);
var _2d2=(_2cc/(_2cc+_2cd+_2ce));
if(_2d2>=_2cf[i].MinimumPercent){
this.LogSeq("`1122`",_2c7);
_2cb=true;
}
break;
default:
break;
}
this.LogSeq("`1144`",_2c7);
if(_2cb===true){
this.LogSeq("`267`",_2c7);
this.LogSeqReturn("`1732`",_2c7);
return true;
}
}
}
this.LogSeq("`424`",_2c7);
this.LogSeqReturn("`1728`",_2c7);
return false;
}
function Sequencer_GetApplicableSetofRollupRules(_2d3,_2d4){
var _2d5=new Array();
var _2d6=_2d3.GetRollupRules();
for(var i=0;i<_2d6.length;i++){
switch(_2d4){
case RULE_SET_SATISFIED:
if(_2d6[i].Action==ROLLUP_RULE_ACTION_SATISFIED){
_2d5[_2d5.length]=_2d6[i];
}
break;
case RULE_SET_NOT_SATISFIED:
if(_2d6[i].Action==ROLLUP_RULE_ACTION_NOT_SATISFIED){
_2d5[_2d5.length]=_2d6[i];
}
break;
case RULE_SET_COMPLETED:
if(_2d6[i].Action==ROLLUP_RULE_ACTION_COMPLETED){
_2d5[_2d5.length]=_2d6[i];
}
break;
case RULE_SET_INCOMPLETE:
if(_2d6[i].Action==ROLLUP_RULE_ACTION_INCOMPLETE){
_2d5[_2d5.length]=_2d6[i];
}
break;
}
}
return _2d5;
}
function Sequencer_SelectChildrenProcess(_2d8,_2d9){
Debug.AssertError("Calling log not passed.",(_2d9===undefined||_2d9===null));
var _2da=this.LogSeqAudit("`1377`"+_2d8+")",_2d9);
this.LogSeq("`561`",_2da);
if(_2d8.IsALeaf()){
this.LogSeq("`1239`",_2da);
this.LogSeqReturn("",_2da);
return;
}
this.LogSeq("`171`",_2da);
if(_2d8.IsActive()===true||_2d8.IsSuspended()===true){
this.LogSeq("`1253`",_2da);
this.LogSeqReturn("",_2da);
return;
}
var _2db=_2d8.GetSelectionTiming();
switch(_2db){
case TIMING_NEVER:
this.LogSeq("`886`",_2da);
this.LogSeq("`1246`",_2da);
break;
case TIMING_ONCE:
this.LogSeq("`893`",_2da);
this.LogSeq("`811`",_2da);
if(_2d8.GetSelectedChildren()===false){
this.LogSeq("`438`",_2da);
if(_2d8.GetActivityProgressStatus()===false){
this.LogSeq("`768`",_2da);
if(_2d8.GetSelectionCountStatus()===true){
this.LogSeq("`875`",_2da);
var _2dc=new Array();
var _2dd=_2d8.GetChildren();
var _2de=_2d8.GetSelectionCount();
if(_2de<_2dd.length){
var _2df=Sequencer_GetUniqueRandomNumbersBetweenTwoValues(_2de,0,_2dd.length-1);
this.LogSeq("`867`",_2da);
for(var i=0;i<_2df.length;i++){
this.LogSeq("`534`",_2da);
this.LogSeq("`339`",_2da);
_2dc[i]=_2dd[_2df[i]];
}
}else{
_2dc=_2dd;
}
this.LogSeq("`769`",_2da);
_2d8.SetAvailableChildren(_2dc);
_2d8.SetSelectedChildren(true);
}
}
}
this.LogSeq("`1240`",_2da);
break;
case TIMING_ON_EACH_NEW_ATTEMPT:
this.LogSeq("`735`",_2da);
this.LogSeq("`894`",_2da);
break;
default:
this.LogSeq("`836`",_2da);
break;
}
this.LogSeqReturn("",_2da);
}
function Sequencer_GetUniqueRandomNumbersBetweenTwoValues(_2e1,_2e2,_2e3){
if(_2e1===null||_2e1===undefined||_2e1<_2e2){
_2e1=_2e2;
}
if(_2e1>_2e3){
_2e1=_2e3;
}
var _2e4=new Array(_2e1);
var _2e5;
var _2e6;
for(var i=0;i<_2e1;i++){
_2e6=true;
while(_2e6){
_2e5=Sequencer_GetRandomNumberWithinRange(_2e2,_2e3);
_2e6=Sequencer_IsNumberAlreadyInArray(_2e5,_2e4);
}
_2e4[i]=_2e5;
}
_2e4.sort();
return _2e4;
}
function Sequencer_GetRandomNumberWithinRange(_2e8,_2e9){
var diff=_2e9-_2e8;
return Math.floor(Math.random()*(diff+_2e8+1));
}
function Sequencer_IsNumberAlreadyInArray(_2eb,_2ec){
for(var i=0;i<_2ec.length;i++){
if(_2ec[i]==_2eb){
return true;
}
}
return false;
}
function Sequencer_SequencingExitActionRulesSubprocess(_2ee){
Debug.AssertError("Calling log not passed.",(_2ee===undefined||_2ee===null));
var _2ef=this.LogSeqAudit("`1061`",_2ee);
this.LogSeq("`248`",_2ef);
var _2f0=this.GetCurrentActivity();
var _2f1=this.Activities.GetParentActivity(_2f0);
var _2f2;
if(_2f1!==null){
_2f2=this.GetActivityPath(_2f1,true);
}else{
_2f2=this.GetActivityPath(_2f0,true);
}
this.LogSeq("`1217`",_2ef);
var _2f3=null;
var _2f4=null;
this.LogSeq("`306`",_2ef);
for(var i=(_2f2.length-1);i>=0;i--){
this.LogSeq("`549`",_2ef);
_2f4=this.SequencingRulesCheckProcess(_2f2[i],RULE_SET_EXIT,_2ef);
this.LogSeq("`734`",_2ef);
if(_2f4!==null){
this.LogSeq("`413`",_2ef);
_2f3=_2f2[i];
this.LogSeq("`1501`",_2ef);
break;
}
}
this.LogSeq("`1187`",_2ef);
if(_2f3!==null){
this.LogSeq("`353`",_2ef);
this.TerminateDescendentAttemptsProcess(_2f3,_2ef);
this.LogSeq("`464`",_2ef);
this.EndAttemptProcess(_2f3,false,_2ef);
this.LogSeq("`344`",_2ef);
this.SetCurrentActivity(_2f3,_2ef);
}
this.LogSeq("`957`",_2ef);
this.LogSeqReturn("",_2ef);
return;
}
function Sequencer_SequencingPostConditionRulesSubprocess(_2f6){
Debug.AssertError("Calling log not passed.",(_2f6===undefined||_2f6===null));
var _2f7=this.LogSeqAudit("`1006`",_2f6);
var _2f8;
this.LogSeq("`336`",_2f7);
var _2f9=this.GetCurrentActivity();
if(_2f9.IsSuspended()){
this.LogSeq("`888`",_2f7);
_2f8=new Sequencer_SequencingPostConditionRulesSubprocessResult(null,null);
this.LogSeqReturn(_2f8,_2f7);
return _2f8;
}
this.LogSeq("`191`",_2f7);
var _2fa=this.SequencingRulesCheckProcess(_2f9,RULE_SET_POST_CONDITION,_2f7);
this.LogSeq("`764`",_2f7);
if(_2fa!==null){
this.LogSeq("`588`",_2f7);
if(_2fa==SEQUENCING_RULE_ACTION_RETRY||_2fa==SEQUENCING_RULE_ACTION_CONTINUE||_2fa==SEQUENCING_RULE_ACTION_PREVIOUS){
this.LogSeq("`47`",_2f7);
_2f8=new Sequencer_SequencingPostConditionRulesSubprocessResult(this.TranslateSequencingRuleActionIntoSequencingRequest(_2fa),null);
this.LogSeqReturn(_2f8,_2f7);
return _2f8;
}
this.LogSeq("`621`",_2f7);
if(_2fa==SEQUENCING_RULE_ACTION_EXIT_PARENT||_2fa==SEQUENCING_RULE_ACTION_EXIT_ALL){
this.LogSeq("`83`",_2f7);
_2f8=new Sequencer_SequencingPostConditionRulesSubprocessResult(null,this.TranslateSequencingRuleActionIntoTerminationRequest(_2fa));
this.LogSeqReturn(_2f8,_2f7);
return _2f8;
}
this.LogSeq("`750`",_2f7);
if(_2fa==SEQUENCING_RULE_ACTION_RETRY_ALL){
this.LogSeq("`27`",_2f7);
_2f8=new Sequencer_SequencingPostConditionRulesSubprocessResult(SEQUENCING_REQUEST_RETRY,TERMINATION_REQUEST_EXIT_ALL);
this.LogSeqReturn(_2f8,_2f7);
return _2f8;
}
}
this.LogSeq("`481`",_2f7);
_2f8=new Sequencer_SequencingPostConditionRulesSubprocessResult(null,null);
this.LogSeqReturn(_2f8,_2f7);
return _2f8;
}
function Sequencer_SequencingPostConditionRulesSubprocessResult(_2fb,_2fc){
this.TerminationRequest=_2fc;
this.SequencingRequest=_2fb;
}
Sequencer_SequencingPostConditionRulesSubprocessResult.prototype.toString=function(){
return "TerminationRequest="+this.TerminationRequest+", SequencingRequest="+this.SequencingRequest;
};
function Sequencer_SequencingRequestProcess(_2fd,_2fe,_2ff){
Debug.AssertError("Calling log not passed.",(_2ff===undefined||_2ff===null));
var _300=this.LogSeqAudit("`1287`"+_2fd+", "+_2fe+")",_2ff);
var _301;
switch(_2fd){
case SEQUENCING_REQUEST_START:
this.LogSeq("`1112`",_300);
this.LogSeq("`946`",_300);
var _302=this.StartSequencingRequestProcess(_300);
this.LogSeq("`692`",_300);
if(_302.Exception!==null){
this.LogSeq("`81`",_300);
_301=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_302.Exception,_302.ExceptionText,false);
this.LogSeqReturn(_301,_300);
return _301;
}else{
this.LogSeq("`1631`",_300);
this.LogSeq("`121`",_300);
_301=new Sequencer_SequencingRequestProcessResult(_2fd,_302.DeliveryRequest,null,null,"",false);
this.LogSeqReturn(_301,_300);
return _301;
}
break;
case SEQUENCING_REQUEST_RESUME_ALL:
this.LogSeq("`1020`",_300);
this.LogSeq("`878`",_300);
var _303=this.ResumeAllSequencingRequestProcess(_300);
this.LogSeq("`643`",_300);
if(_303.Exception!==null){
this.LogSeq("`71`",_300);
_301=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_303.Exception,_303.ExceptionText,false);
this.LogSeqReturn(_301,_300);
return _301;
}else{
this.LogSeq("`1625`",_300);
this.LogSeq("`109`",_300);
_301=new Sequencer_SequencingRequestProcessResult(_2fd,_303.DeliveryRequest,null,null,"",false);
this.LogSeqReturn(_301,_300);
return _301;
}
break;
case SEQUENCING_REQUEST_EXIT:
this.LogSeq("`1148`",_300);
this.LogSeq("`955`",_300);
var _304=this.ExitSequencingRequestProcess(_300);
this.LogSeq("`704`",_300);
if(_304.Exception!==null){
this.LogSeq("`85`",_300);
_301=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_304.Exception,_304.ExceptionText,false);
this.LogSeqReturn(_301,_300);
return _301;
}else{
this.LogSeq("`1626`",_300);
this.LogSeq("`124`",_300);
_301=new Sequencer_SequencingRequestProcessResult(_2fd,null,_304.EndSequencingSession,null,"",false);
this.LogSeqReturn(_301,_300);
return _301;
}
break;
case SEQUENCING_REQUEST_RETRY:
this.LogSeq("`1118`",_300);
this.LogSeq("`949`",_300);
var _305=this.RetrySequencingRequestProcess(_300);
if(_305.Exception!==null){
this.LogSeq("`80`",_300);
_301=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_305.Exception,_305.ExceptionText,false);
this.LogSeqReturn(_301,_300);
return _301;
}else{
this.LogSeq("`1619`",_300);
this.LogSeq("`114`",_300);
_301=new Sequencer_SequencingRequestProcessResult(_2fd,_305.DeliveryRequest,null,null,"",false);
this.LogSeqReturn(_301,_300);
return _301;
}
break;
case SEQUENCING_REQUEST_CONTINUE:
this.LogSeq("`1068`",_300);
this.LogSeq("`898`",_300);
var _306=this.ContinueSequencingRequestProcess(_300);
this.LogSeq("`661`",_300);
if(_306.Exception!==null){
this.LogSeq("`73`",_300);
_301=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_306.Exception,_306.ExceptionText,false);
this.LogSeqReturn(_301,_300);
return _301;
}else{
this.LogSeq("`1615`",_300);
this.LogSeq("`112`",_300);
_301=new Sequencer_SequencingRequestProcessResult(_2fd,_306.DeliveryRequest,null,null,"",false);
this.LogSeqReturn(_301,_300);
return _301;
}
break;
case SEQUENCING_REQUEST_PREVIOUS:
this.LogSeq("`1063`",_300);
this.LogSeq("`900`",_300);
var _307=this.PreviousSequencingRequestProcess(_300);
this.LogSeq("`666`",_300);
if(_307.Exception!==null){
this.LogSeq("`74`",_300);
_301=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_307.Exception,_307.ExceptionText,false);
this.LogSeqReturn(_301,_300);
return _301;
}else{
this.LogSeq("`1637`",_300);
this.LogSeq("`111`",_300);
_301=new Sequencer_SequencingRequestProcessResult(_2fd,_307.DeliveryRequest,null,null,"",false);
this.LogSeqReturn(_301,_300);
return _301;
}
break;
case SEQUENCING_REQUEST_CHOICE:
this.LogSeq("`1105`",_300);
this.LogSeq("`934`",_300);
var _308=this.ChoiceSequencingRequestProcess(_2fe,_300);
this.LogSeq("`679`",_300);
if(_308.Exception!==null){
this.LogSeq("`78`",_300);
_301=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,_308.Exception,_308.ExceptionText,_308.Hidden);
this.LogSeqReturn(_301,_300);
return _301;
}else{
this.LogSeq("`1630`",_300);
this.LogSeq("`119`",_300);
_301=new Sequencer_SequencingRequestProcessResult(_2fd,_308.DeliveryRequest,null,null,"",_308.Hidden);
this.LogSeqReturn(_301,_300);
return _301;
}
break;
}
this.LogSeq("`150`",_300);
_301=new Sequencer_SequencingRequestProcessResult(SEQUENCING_REQUEST_NOT_VALID,null,null,"SB.2.12-1","The sequencing request ("+_2fd+") is not recognized.",false);
this.LogSeqReturn(_301,_300);
return _301;
}
function Sequencer_SequencingRequestProcessResult(_309,_30a,_30b,_30c,_30d,_30e){
if(_30e===undefined){
Debug.AssertError("undefined hidden value");
}
this.SequencingRequest=_309;
this.DeliveryRequest=_30a;
this.EndSequencingSession=_30b;
this.Exception=_30c;
this.ExceptionText=_30d;
this.Hidden=_30e;
}
Sequencer_SequencingRequestProcessResult.prototype.toString=function(){
return "SequencingRequest="+this.SequencingRequest+", DeliveryRequest="+this.DeliveryRequest+", EndSequencingSession="+this.EndSequencingSession+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText+", Hidden="+this.Hidden;
};
function Sequencer_SequencingRulesCheckProcess(_30f,_310,_311){
Debug.AssertError("Calling log not passed.",(_311===undefined||_311===null));
var _312=this.LogSeqAudit("`1268`"+_30f+", "+_310+")",_311);
var _313;
var _314=Sequencer_GetApplicableSetofSequencingRules(_30f,_310);
this.LogSeq("`301`",_312);
if(_314.length>0){
this.LogSeq("`190`",_312);
this.LogSeq("`1189`",_312);
for(var i=0;i<_314.length;i++){
this.LogSeq("`415`",_312);
_313=this.SequencingRulesCheckSubprocess(_30f,_314[i],_312);
this.LogSeq("`791`",_312);
if(_313===true){
this.LogSeq("`211`",_312);
this.LogSeqReturn(_314[i].Action,_312);
return _314[i].Action;
}
}
}
this.LogSeq("`457`",_312);
this.LogSeqReturn("`1731`",_312);
return null;
}
function Sequencer_GetApplicableSetofSequencingRules(_316,_317){
var _318=new Array();
if(_317==RULE_SET_POST_CONDITION){
_318=_316.GetPostConditionRules();
}else{
if(_317==RULE_SET_EXIT){
_318=_316.GetExitRules();
}else{
var _319=_316.GetPreConditionRules();
for(var i=0;i<_319.length;i++){
switch(_317){
case RULE_SET_HIDE_FROM_CHOICE:
if(_319[i].Action==SEQUENCING_RULE_ACTION_HIDDEN_FROM_CHOICE){
_318[_318.length]=_319[i];
}
break;
case RULE_SET_STOP_FORWARD_TRAVERSAL:
if(_319[i].Action==SEQUENCING_RULE_ACTION_STOP_FORWARD_TRAVERSAL){
_318[_318.length]=_319[i];
}
break;
case RULE_SET_DISABLED:
if(_319[i].Action==SEQUENCING_RULE_ACTION_DISABLED){
_318[_318.length]=_319[i];
}
break;
case RULE_SET_SKIPPED:
if(_319[i].Action==SEQUENCING_RULE_ACTION_SKIP){
_318[_318.length]=_319[i];
}
break;
default:
Debug.AssertError("ERROR - invalid sequencing rule set - "+_317);
return null;
}
}
}
}
return _318;
}
function Sequencer_SequencingRulesCheckSubprocess(_31b,rule,_31d){
Debug.AssertError("Calling log not passed.",(_31d===undefined||_31d===null));
var _31e=this.LogSeqAudit("`1166`"+_31b+", "+rule+")",_31d);
this.LogSeq("`324`",_31e);
var _31f=new Array();
var _320;
var _321;
var i;
this.LogSeq("`733`",_31e);
for(i=0;i<rule.RuleConditions.length;i++){
this.LogSeq("`100`",_31e);
_320=this.EvaluateSequencingRuleCondition(_31b,rule.RuleConditions[i],_31e);
this.LogSeq("`697`",_31e);
if(rule.RuleConditions[i].Operator==RULE_CONDITION_OPERATOR_NOT){
this.LogSeq("`658`",_31e);
if(_320!="unknown"){
_320=(!_320);
}
}
this.LogSeq("`284`",_31e);
_31f[_31f.length]=_320;
}
this.LogSeq("`372`",_31e);
if(_31f.length===0){
this.LogSeq("`611`",_31e);
this.LogSeqReturn(RESULT_UNKNOWN,_31e);
return RESULT_UNKNOWN;
}
this.LogSeq("`62`",_31e);
if(rule.ConditionCombination==RULE_CONDITION_COMBINATION_ANY){
_321=false;
for(i=0;i<_31f.length;i++){
_321=Sequencer_LogicalOR(_321,_31f[i]);
}
}else{
_321=true;
for(i=0;i<_31f.length;i++){
_321=Sequencer_LogicalAND(_321,_31f[i]);
}
}
this.LogSeq("`618`",_31e);
this.LogSeqReturn(_321,_31e);
return _321;
}
function Sequencer_EvaluateSequencingRuleCondition(_323,_324,_325){
Debug.AssertError("Calling log not passed.",(_325===undefined||_325===null));
var _326=this.LogSeqAudit("`1305`"+_323+", "+_324+")",_325);
var _327=null;
switch(_324.Condition){
case SEQUENCING_RULE_CONDITION_SATISFIED:
_327=_323.IsSatisfied(_324.ReferencedObjective);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_STATUS_KNOWN:
_327=_323.IsObjectiveStatusKnown(_324.ReferencedObjective,false);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_KNOWN:
_327=_323.IsObjectiveMeasureKnown(_324.ReferencedObjective,false);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_GREATER_THAN:
_327=_323.IsObjectiveMeasureGreaterThan(_324.ReferencedObjective,_324.MeasureThreshold,false);
break;
case SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_LESS_THAN:
_327=_323.IsObjectiveMeasureLessThan(_324.ReferencedObjective,_324.MeasureThreshold,false);
break;
case SEQUENCING_RULE_CONDITION_COMPLETED:
_327=_323.IsCompleted(_324.ReferencedObjective,false);
break;
case SEQUENCING_RULE_CONDITION_ACTIVITY_PROGRESS_KNOWN:
_327=_323.IsActivityProgressKnown(_324.ReferencedObjective,false);
break;
case SEQUENCING_RULE_CONDITION_ATTEMPTED:
_327=_323.IsAttempted();
break;
case SEQUENCING_RULE_CONDITION_ATTEMPT_LIMIT_EXCEEDED:
_327=_323.IsAttemptLimitExceeded();
break;
case SEQUENCING_RULE_CONDITION_ALWAYS:
_327=true;
break;
default:
Debug.AssertError("ERROR - Encountered unsupported rule condition - "+_324);
_327=RESULT_UNKNOWN;
break;
}
_326.setReturn(_327+"",_326);
return _327;
}
function Sequencer_LogicalOR(_328,_329){
if(_328==RESULT_UNKNOWN){
if(_329===true){
return true;
}else{
return RESULT_UNKNOWN;
}
}else{
if(_329==RESULT_UNKNOWN){
if(_328===true){
return true;
}else{
return RESULT_UNKNOWN;
}
}else{
return (_328||_329);
}
}
}
function Sequencer_LogicalAND(_32a,_32b){
if(_32a==RESULT_UNKNOWN){
if(_32b===false){
return false;
}else{
return RESULT_UNKNOWN;
}
}else{
if(_32b==RESULT_UNKNOWN){
if(_32a===false){
return false;
}else{
return RESULT_UNKNOWN;
}
}else{
return (_32a&&_32b);
}
}
}
function Sequencer_StartSequencingRequestProcess(_32c){
Debug.AssertError("Calling log not passed.",(_32c===undefined||_32c===null));
var _32d=this.LogSeqAudit("`1208`",_32c);
var _32e;
this.LogSeq("`500`",_32d);
if(this.IsCurrentActivityDefined(_32d)){
this.LogSeq("`456`",_32d);
_32e=new Sequencer_StartSequencingRequestProcessResult(null,"SB.2.5-1",IntegrationImplementation.GetString("You cannot 'Start' an item that is already open."));
this.LogSeqReturn(_32e,_32d);
return _32e;
}
this.LogSeq("`317`",_32d);
var _32f=this.GetRootActivity(_32d);
if(_32f.IsALeaf()){
this.LogSeq("`239`",_32d);
_32e=new Sequencer_StartSequencingRequestProcessResult(_32f,null,"");
this.LogSeqReturn(_32e,_32d);
return _32e;
}else{
if(Control.Package.Properties.AlwaysFlowToFirstSco===true){
this.LogSeq("`315`",_32d);
this.LogSeq("`1033`",_32d);
var _330=this.GetOrderedListOfActivities(_32d);
this.LogSeq("`922`",_32d);
for(var _331 in _330){
if(_330[_331].IsDeliverable()===true){
_32e=new Sequencer_StartSequencingRequestProcessResult(_330[_331],null,"");
this.LogSeqReturn(_32e,_32d);
return _32e;
}
}
_32e=new Sequencer_StartSequencingRequestProcessResult(null,"SB.2.5-2.5","There are no deliverable activities in this course.");
this.LogSeqReturn(_32e,_32d);
return _32e;
}else{
this.LogSeq("`1668`",_32d);
this.LogSeq("`163`",_32d);
var _332=this.FlowSubprocess(_32f,FLOW_DIRECTION_FORWARD,true,_32d);
this.LogSeq("`963`",_32d);
if(_332.Deliverable===false){
this.LogSeq("`232`",_32d);
_32e=new Sequencer_StartSequencingRequestProcessResult(null,_332.Exception,_332.ExceptionText);
this.LogSeqReturn(_32e,_32d);
return _32e;
}else{
this.LogSeq("`1623`",_32d);
this.LogSeq("`325`",_32d);
_32e=new Sequencer_StartSequencingRequestProcessResult(_332.IdentifiedActivity,null,"");
this.LogSeqReturn(_32e,_32d);
return _32e;
}
}
}
}
function Sequencer_StartSequencingRequestProcessResult(_333,_334,_335){
this.DeliveryRequest=_333;
this.Exception=_334;
this.ExceptionText=_335;
}
Sequencer_StartSequencingRequestProcessResult.prototype.toString=function(){
return "DeliveryRequest="+this.DeliveryRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};
function Sequencer_TerminateDescendentAttemptsProcess(_336,_337){
Debug.AssertError("Calling log not passed.",(_337===undefined||_337===null));
var _338=this.LogSeqAudit("`1106`"+_336+")",_337);
this.LogSeq("`682`"+this.GetCurrentActivity()+"`1376`"+_336+")",_338);
var _339=this.FindCommonAncestor(_336,this.GetCurrentActivity(),_338);
this.LogSeq("`149`"+_339+"`972`",_338);
var _33a=this.GetPathToAncestorExclusive(this.GetCurrentActivity(),_339,false);
var _33b=new Array();
this.LogSeq("`520`",_338);
if(_33a.length>0){
this.LogSeq("`1047`",_338);
for(var i=0;i<_33a.length;i++){
this.LogSeq("`471`"+_33a[i].LearningObject.ItemIdentifier,_338);
_33b=_33b.concat(this.EndAttemptProcess(_33a[i],true,_338));
}
}
this.LogSeq("`729`",_338);
var _33d=this.GetMinimalSubsetOfActivitiesToRollup(_33b,null);
for(var _33e in _33d){
this.OverallRollupProcess(_33d[_33e],_338);
}
this.LogSeq("`1012`",_338);
this.LogSeqReturn("",_338);
return;
}
function Sequencer_TerminationRequestProcess(_33f,_340){
Debug.AssertError("Calling log not passed.",(_340===undefined||_340===null));
var _341=this.LogSeqAudit("`1284`"+_33f+")",_340);
var _342;
var _343=this.GetCurrentActivity();
var _344=this.Activities.GetParentActivity(_343);
var _345=this.GetRootActivity(_341);
var _346;
var _347=null;
var _348=false;
this.LogSeq("`366`",_341);
if(!this.IsCurrentActivityDefined(_341)){
this.LogSeq("`380`",_341);
_342=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-1",IntegrationImplementation.GetString("You cannot use 'Terminate' because no item is currently open."));
this.LogSeqReturn(_342,_341);
return _342;
}
this.LogSeq("`88`",_341);
if((_33f==TERMINATION_REQUEST_EXIT||_33f==TERMINATION_REQUEST_ABANDON)&&(_343.IsActive()===false)){
this.LogSeq("`381`",_341);
_342=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-2",IntegrationImplementation.GetString("The current activity has already been terminated."));
this.LogSeqReturn(_342,_341);
return _342;
}
switch(_33f){
case TERMINATION_REQUEST_EXIT:
this.LogSeq("`1125`",_341);
this.LogSeq("`383`",_341);
this.EndAttemptProcess(_343,false,_341);
this.LogSeq("`240`",_341);
this.SequencingExitActionRulesSubprocess(_341);
this.LogSeq("`1612`",_341);
var _349;
do{
this.LogSeq("`1104`",_341);
_349=false;
this.LogSeq("`586`"+this.GetCurrentActivity()+")",_341);
_347=this.SequencingPostConditionRulesSubprocess(_341);
this.LogSeq("`476`",_341);
if(_347.TerminationRequest==TERMINATION_REQUEST_EXIT_ALL){
this.LogSeq("`905`",_341);
_33f=TERMINATION_REQUEST_EXIT_ALL;
this.LogSeq("`673`",_341);
_348=true;
break;
}
this.LogSeq("`53`",_341);
if(_347.TerminationRequest==TERMINATION_REQUEST_EXIT_PARENT){
this.LogSeq("`283`",_341);
if(this.GetCurrentActivity()!==null&&this.GetCurrentActivity().IsTheRoot()===false){
this.LogSeq("`674`",_341);
this.SetCurrentActivity(this.Activities.GetParentActivity(this.GetCurrentActivity()),_341);
this.LogSeq("`774`",_341);
this.EndAttemptProcess(this.GetCurrentActivity(),false,_341);
this.LogSeq("`492`",_341);
_349=true;
}else{
this.LogSeq("`1565`",_341);
this.LogSeq("`355`",_341);
_342=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-4",IntegrationImplementation.GetString("An 'Exit Parent' sequencing request cannot be processed on the root of the activity tree."));
this.LogSeqReturn(_342,_341);
return _342;
}
}else{
this.LogSeq("`1608`",_341);
this.LogSeq("`8`",_341);
if(this.GetCurrentActivity()!==null&&this.GetCurrentActivity().IsTheRoot()===true&&_347.SequencingRequest!=SEQUENCING_REQUEST_RETRY){
this.LogSeq("`396`",_341);
_342=new Sequencer_TerminationRequestProcessResult(_33f,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_342,_341);
return _342;
}
}
this.LogSeq("`909`"+_349+")",_341);
}while(_349!==false);
if(!_348){
this.LogSeq("`54`",_341);
_342=new Sequencer_TerminationRequestProcessResult(_33f,_347.SequencingRequest,null,"");
this.LogSeqReturn(_342,_341);
return _342;
break;
}
case TERMINATION_REQUEST_EXIT_ALL:
this.LogSeq("`1065`",_341);
this.LogSeq("`236`",_341);
if(_343.IsActive()){
this.LogSeq("`818`",_341);
this.EndAttemptProcess(_343,false,_341);
}
this.LogSeq("`587`",_341);
this.TerminateDescendentAttemptsProcess(_345,_341);
this.LogSeq("`725`",_341);
this.EndAttemptProcess(_345,false,_341);
this.LogSeq("`350`",_341);
this.SetCurrentActivity(_345,_341);
this.LogSeq("`3`",_341);
if(_347!==null&&_347.SequencingRequest!==null){
this.LogSeq("`445`",_341);
_342=new Sequencer_TerminationRequestProcessResult(_33f,_347.SequencingRequest,null,"");
this.LogSeqReturn(_342,_341);
return _342;
}else{
this.LogSeq("`1027`",_341);
_342=new Sequencer_TerminationRequestProcessResult(_33f,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_342,_341);
return _342;
}
break;
case TERMINATION_REQUEST_SUSPEND_ALL:
this.LogSeq("`991`",_341);
this.LogSeq("`44`",_341);
this.LogSeq("`507`",_341);
this.EndAttemptProcess(_343,false,_341,true);
if(_343.IsActive()||_343.IsSuspended()){
this.LogSeq("`849`",_341);
this.SetSuspendedActivity(_343);
}else{
this.LogSeq("`1651`",_341);
this.LogSeq("`258`",_341);
if(!_343.IsTheRoot()){
this.LogSeq("`670`",_341);
this.SetSuspendedActivity(_344);
}else{
this.LogSeq("`1614`",_341);
this.LogSeq("`260`",_341);
Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-3","The suspend all termination request failed because there is no activity to suspend");
}
}
this.LogSeq("`274`",_341);
_346=this.GetActivityPath(this.GetSuspendedActivity(_341),true);
this.LogSeq("`1074`",_341);
if(_346.length===0){
this.LogSeq("`273`",_341);
_342=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-5",IntegrationImplementation.GetString("Nothing to suspend"));
this.LogSeqReturn(_342,_341);
return _342;
}
this.LogSeq("`1003`",_341);
for(var i=0;i<_346.length;i++){
this.LogSeq("`632`"+_346[i].GetItemIdentifier()+")",_341);
_346[i].SetActive(false);
this.LogSeq("`846`",_341);
_346[i].SetSuspended(true);
}
this.LogSeq("`348`",_341);
this.SetCurrentActivity(_345,_341);
this.LogSeq("`159`",_341);
_342=new Sequencer_TerminationRequestProcessResult(_33f,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_342,_341);
return _342;
break;
case TERMINATION_REQUEST_ABANDON:
this.LogSeq("`1083`",_341);
this.LogSeq("`796`",_341);
_343.SetActive(false);
this.LogSeq("`459`",_341);
_342=new Sequencer_TerminationRequestProcessResult(_33f,null,null,"");
this.LogSeqReturn(_342,_341);
return _342;
break;
case TERMINATION_REQUEST_ABANDON_ALL:
this.LogSeq("`994`",_341);
this.LogSeq("`281`",_341);
_346=this.GetActivityPath(_343,true);
this.LogSeq("`1082`",_341);
if(_346.length===0){
this.LogSeq("`280`",_341);
_342=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-6",IntegrationImplementation.GetString("Nothing to close"));
this.LogSeqReturn(_342,_341);
return _342;
}
this.LogSeq("`993`",_341);
for(var i=0;i<_346.length;i++){
this.LogSeq("`866`",_341);
_346[i].SetActive(false);
}
this.LogSeq("`356`",_341);
this.SetCurrentActivity(_345,_341);
this.LogSeq("`162`",_341);
_342=new Sequencer_TerminationRequestProcessResult(_33f,SEQUENCING_REQUEST_EXIT,null,"");
this.LogSeqReturn(_342,_341);
return _342;
break;
default:
this.LogSeq("`254`",_341);
_342=new Sequencer_TerminationRequestProcessResult(TERMINATION_REQUEST_NOT_VALID,null,"TB.2.3-7",IntegrationImplementation.GetString("The 'Termination' request {0} is not recognized.",_33f));
this.LogSeqReturn(_342,_341);
return _342;
break;
}
}
function Sequencer_TerminationRequestProcessResult(_34b,_34c,_34d,_34e){
this.TerminationRequest=_34b;
this.SequencingRequest=_34c;
this.Exception=_34d;
this.ExceptionText=_34e;
}
Sequencer_TerminationRequestProcessResult.prototype.toString=function(){
return "TerminationRequest="+this.TerminationRequest+", SequencingRequest="+this.SequencingRequest+", Exception="+this.Exception+", ExceptionText="+this.ExceptionText;
};

