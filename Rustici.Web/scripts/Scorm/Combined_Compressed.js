var SCORM_TRUE="true";
var SCORM_FALSE="false";
var SCORM_ERROR_NONE=0;
var SCORM_ERROR_GENERAL=101;
var SCORM_ERROR_INVALID_ARG=201;
var SCORM_ERROR_NO_CHILDREN=202;
var SCORM_ERROR_NO_COUNT=203;
var SCORM_ERROR_NOT_INITIALIZED=301;
var SCORM_ERROR_NOT_IMPLEMENTED=401;
var SCORM_ERROR_ELEMENT_IS_KEYWORD=402;
var SCORM_ERROR_READ_ONLY=403;
var SCORM_ERROR_WRITE_ONLY=404;
var SCORM_ERROR_INCORRECT_DATA_TYPE=405;
var SCORM_ErrorStrings=new Array(11);
SCORM_ErrorStrings[SCORM_ERROR_NONE]="No Error";
SCORM_ErrorStrings[SCORM_ERROR_GENERAL]="General exception";
SCORM_ErrorStrings[SCORM_ERROR_INVALID_ARG]="Invalid argument error";
SCORM_ErrorStrings[SCORM_ERROR_NO_CHILDREN]="Element cannot have children";
SCORM_ErrorStrings[SCORM_ERROR_NO_COUNT]="Element not an array - cannot have count";
SCORM_ErrorStrings[SCORM_ERROR_NOT_INITIALIZED]="Not Initialized";
SCORM_ErrorStrings[SCORM_ERROR_NOT_IMPLEMENTED]="Not implemented error";
SCORM_ErrorStrings[SCORM_ERROR_ELEMENT_IS_KEYWORD]="Invalid set value, element is a keyword";
SCORM_ErrorStrings[SCORM_ERROR_READ_ONLY]="Element is read only";
SCORM_ErrorStrings[SCORM_ERROR_WRITE_ONLY]="Element is write only";
SCORM_ErrorStrings[SCORM_ERROR_INCORRECT_DATA_TYPE]="Incorrect Data Type";
function DataModelSupport(_1,_2,_3){
this.Supported=_1;
this.SupportsRead=_2;
this.SupportsWrite=_3;
}
var arySupportedElements=new Array(49);
arySupportedElements["cmi.core._children"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.core.student_id"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.core.student_name"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.core.lesson_location"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.core.credit"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.core.lesson_status"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.core.entry"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.core.score._children"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.core.score.raw"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.core.total_time"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.core.lesson_mode"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.core.exit"]=new DataModelSupport(true,false,true);
arySupportedElements["cmi.core.session_time"]=new DataModelSupport(true,false,true);
arySupportedElements["cmi.suspend_data"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.launch_data"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.objectives._children"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.objectives._count"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.objectives.n.id"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.objectives.n.score._children"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.objectives.n.score.raw"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.objectives.n.score.max"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.objectives.n.score.min"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.objectives.n.status"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.core.score.max"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.core.score.min"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.comments"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.comments_from_lms"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.student_data._children"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.student_data.mastery_score"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.student_data.max_time_allowed"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.student_data.time_limit_action"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.student_preference._children"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.student_preference.audio"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.student_preference.language"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.student_preference.speed"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.student_preference.text"]=new DataModelSupport(true,true,true);
arySupportedElements["cmi.interactions._children"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.interactions._count"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.interactions.n.id"]=new DataModelSupport(true,false,true);
arySupportedElements["cmi.interactions.n.objectives._count"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.interactions.n.objectives.n.id"]=new DataModelSupport(true,false,true);
arySupportedElements["cmi.interactions.n.time"]=new DataModelSupport(true,false,true);
arySupportedElements["cmi.interactions.n.type"]=new DataModelSupport(true,false,true);
arySupportedElements["cmi.interactions.n.correct_responses._count"]=new DataModelSupport(true,true,false);
arySupportedElements["cmi.interactions.n.correct_responses.n.pattern"]=new DataModelSupport(true,false,true);
arySupportedElements["cmi.interactions.n.weighting"]=new DataModelSupport(true,false,true);
arySupportedElements["cmi.interactions.n.student_response"]=new DataModelSupport(true,false,true);
arySupportedElements["cmi.interactions.n.result"]=new DataModelSupport(true,false,true);
arySupportedElements["cmi.interactions.n.latency"]=new DataModelSupport(true,false,true);
var aryVocabularies=new Array(4);
aryVocabularies["exit"]=new Array(SCORM_EXIT_TIME_OUT,SCORM_EXIT_SUSPEND,SCORM_EXIT_LOGOUT,SCORM_EXIT_UNKNOWN);
aryVocabularies["status"]=new Array(SCORM_STATUS_PASSED,SCORM_STATUS_COMPLETED,SCORM_STATUS_FAILED,SCORM_STATUS_INCOMPLETE,SCORM_STATUS_BROWSED,SCORM_STATUS_NOT_ATTEMPTED);
aryVocabularies["interaction"]=new Array(SCORM_TRUE_FALSE,SCORM_CHOICE,SCORM_FILL_IN,SCORM_MATCHING,SCORM_PERFORMANCE,SCORM_SEQUENCING,SCORM_LIKERT,SCORM_NUMERIC);
aryVocabularies["result"]=new Array(SCORM_CORRECT,SCORM_WRONG,SCORM_UNANTICIPATED,SCORM_NEUTRAL);
var SCORM_CORE_CHILDREN="student_id,student_name,lesson_location,credit,lesson_status,entry,total_time,lesson_mode,exit,session_time,score";
var SCORM_CORE_SCORE_CHILDREN="raw,min,max";
var SCORM_STUDENT_DATA_CHILDREN="mastery_score,max_time_allowed,time_limit_action";
var SCORM_STUDENT_PREFERENCE_CHILDREN="audio,language,speed,text";
var SCORM_OBJECTIVES_CHILDREN="id,score,status";
var SCORM_OBJECTIVES_SCORE_CHILDREN="raw,min,max";
var SCORM_INTERACTIONS_CHILDREN="id,objectives,time,type,correct_responses,weighting,student_response,result,latency";
function RunTimeApi(_4,_5){
this.LearnerId=_4;
this.LearnerName=_5;
this.ErrorNumber=SCORM_ERROR_NONE;
this.ErrorString="";
this.ErrorDiagnostic="";
this.TrackedStartDate=null;
this.TrackedEndDate=null;
this.Initialized=false;
this.ScoCalledFinish=false;
this.RunTimeData=null;
this.LearningObject=null;
this.Activity=null;
this.StatusSetInCurrentSession=false;
this.LearnerPrefsArray=new Object();
}
RunTimeApi.prototype.GetNavigationRequest=RunTimeApi_GetNavigationRequest;
RunTimeApi.prototype.ResetState=RunTimeApi_ResetState;
RunTimeApi.prototype.InitializeForDelivery=RunTimeApi_InitializeForDelivery;
RunTimeApi.prototype.SetDirtyData=RunTimeApi_SetDirtyData;
RunTimeApi.prototype.WriteHistoryLog=RunTimeApi_WriteHistoryLog;
RunTimeApi.prototype.WriteHistoryReturnValue=RunTimeApi_WriteHistoryReturnValue;
RunTimeApi.prototype.WriteAuditLog=RunTimeApi_WriteAuditLog;
RunTimeApi.prototype.WriteAuditReturnValue=RunTimeApi_WriteAuditReturnValue;
RunTimeApi.prototype.WriteDetailedLog=RunTimeApi_WriteDetailedLog;
RunTimeApi.prototype.CloseOutSession=RunTimeApi_CloseOutSession;
RunTimeApi.prototype.NeedToCloseOutSession=RunTimeApi_NeedToCloseOutSession;
RunTimeApi.prototype.AccumulateTotalTimeTracked=RunTimeApi_AccumulateTotalTrackedTime;
RunTimeApi.prototype.InitTrackedTimeStart=RunTimeApi_InitTrackedTimeStart;
function RunTimeApi_GetNavigationRequest(){
return null;
}
function RunTimeApi_ResetState(){
this.ErrorNumber=SCORM_ERROR_NONE;
this.ErrorString="";
this.ErrorDiagnostic="";
this.TrackedStartDate=null;
this.TrackedEndDate=null;
this.Initialized=false;
this.Terminated=false;
this.RunTimeData=null;
this.LearningObject=null;
this.Activity=null;
this.StatusSetInCurrentSession=false;
this.ScoCalledFinish=false;
}
function RunTimeApi_InitializeForDelivery(_6){
this.ErrorNumber=SCORM_ERROR_NONE;
this.ErrorString="";
this.ErrorDiagnostic="";
this.TrackedStartDate=null;
this.TrackedEndDate=null;
this.Initialized=false;
this.StatusSetInCurrentSession=false;
this.ScoCalledFinish=false;
this.RunTimeData=_6.RunTime;
this.LearningObject=_6.LearningObject;
this.Activity=_6;
var _7=this.RunTimeData.Exit!=SCORM_EXIT_SUSPEND&&this.RunTimeData.Exit!=SCORM_EXIT_LOGOUT;
if(_7){
this.Activity.SetAttemptStartTimestampUtc(ConvertDateToIso8601String(new Date()));
this.Activity.SetAttemptAbsoluteDuration("PT0H0M0S");
this.Activity.SetAttemptExperiencedDurationTracked("PT0H0M0S");
this.Activity.SetAttemptExperiencedDurationReported("PT0H0M0S");
if(Control.Package.Properties.ResetRunTimeDataTiming==RESET_RT_DATA_TIMING_WHEN_EXIT_IS_NOT_SUSPEND||Control.Package.Properties.ResetRunTimeDataTiming==RESET_RT_DATA_TIMING_ON_EACH_NEW_SEQUENCING_ATTEMPT){
var _8={ev:"ResetRuntime",ai:this.Activity.ItemIdentifier,at:this.Activity.LearningObject.Title};
this.WriteHistoryLog("",_8);
this.RunTimeData.ResetState();
}
}
}
function RunTimeApi_SetDirtyData(){
this.Activity.DataState=DATA_STATE_DIRTY;
}
function RunTimeApi_WriteAuditLog(_9){
Debug.WriteRteAudit(_9);
}
function RunTimeApi_WriteAuditReturnValue(_a){
Debug.WriteRteAuditReturnValue(_a);
}
function RunTimeApi_WriteDetailedLog(_b){
Debug.WriteRteDetailed(_b);
}
function RunTimeApi_WriteHistoryLog(_c,_d){
HistoryLog.WriteEventDetailed(_c,_d);
}
function RunTimeApi_WriteHistoryReturnValue(_e,_f){
HistoryLog.WriteEventDetailedReturnValue(_e,_f);
}
function RunTimeApi_NeedToCloseOutSession(){
return ((this.Initialized===false)||this.ScoCalledFinish);
}
RunTimeApi.prototype.LMSInitialize=RunTimeApi_Initialize;
RunTimeApi.prototype.LMSFinish=RunTimeApi_Finish;
RunTimeApi.prototype.LMSSetValue=RunTimeApi_SetValue;
RunTimeApi.prototype.LMSGetValue=RunTimeApi_GetValue;
RunTimeApi.prototype.LMSCommit=RunTimeApi_Commit;
RunTimeApi.prototype.LMSGetLastError=RunTimeApi_GetLastError;
RunTimeApi.prototype.LMSGetErrorString=RunTimeApi_GetErrorString;
RunTimeApi.prototype.LMSGetDiagnostic=RunTimeApi_GetDiagnostic;
function RunTimeApi_Initialize(arg){
this.WriteAuditLog("`1685`"+arg+"')");
var _11={ev:"ApiInitialize"};
if(this.Activity){
_11.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_11);
var _11={};
if(this.Activity){
_11.activityIdentifier=this.Activity.ItemIdentifier;
}
var _12;
var _13;
arg=CleanExternalString(arg);
this.ClearErrorState();
_12=this.CheckForInitializeError(arg);
if(!_12){
_13=SCORM_FALSE;
}else{
this.Initialized=true;
_13=SCORM_TRUE;
}
this.WriteAuditReturnValue(_13);
return _13;
}
function RunTimeApi_Finish(arg){
this.WriteAuditLog("`1709`"+arg+"')");
var _15;
arg=CleanExternalString(arg);
this.ClearErrorState();
_15=this.CheckForFinishError(arg);
var _16=(_15&&(this.ScoCalledFinish===false));
if(_15===false){
returnValue=SCORM_FALSE;
}else{
var _17={ev:"ApiTerminate"};
if(this.Activity){
_17.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_17);
this.CloseOutSession();
this.SetDirtyData();
this.Initialized=false;
this.ScoCalledFinish=true;
returnValue=SCORM_TRUE;
}
if(_16===true&&Control.IsThereAPendingNavigationRequest()===false){
var _18=Control.Sequencer.GetExitAction(this.Activity,true,Control.Sequencer.LogSeqAudit("`1409`"));
if(_18!=EXIT_ACTION_DO_NOTHING){
window.setTimeout("Control.ScoHasTerminatedSoUnload();",150);
}
}
Control.SignalTerminated();
this.WriteAuditReturnValue(returnValue);
return returnValue;
}
function RunTimeApi_SetValue(_19,_1a){
this.WriteAuditLog("`1699`"+_19+"`1729`"+_1a+"')");
var _1b;
var _1c;
this.ClearErrorState();
_19=CleanExternalString(_19);
_1a=CleanExternalString(_1a);
var _1d=RemoveIndiciesFromCmiElement(_19);
var _1e=ExtractIndex(_19);
var _1f=ExtractSecondaryIndex(_19);
_1b=this.CheckForSetValueError(_19,_1a,_1d,_1e,_1f);
if(!_1b){
_1c=SCORM_FALSE;
}else{
this.StoreValue(_19,_1a,_1d,_1e,_1f);
this.SetDirtyData();
_1c=SCORM_TRUE;
}
this.WriteAuditReturnValue(_1c);
return _1c;
}
function RunTimeApi_GetValue(_20){
this.WriteAuditLog("`1701`"+_20+"')");
var _21;
var _22;
this.ClearErrorState();
_20=CleanExternalString(_20);
var _23=RemoveIndiciesFromCmiElement(_20);
var _24=ExtractIndex(_20);
var _25=ExtractSecondaryIndex(_20);
_22=this.CheckForGetValueError(_20,_23,_24,_25);
if(!_22){
_21="";
}else{
_21=this.RetrieveGetValueData(_20,_23,_24,_25);
if(_21===null){
_21="";
}
}
this.WriteAuditReturnValue(_21);
return _21;
}
function RunTimeApi_Commit(arg){
this.WriteAuditLog("`1711`"+arg+"')");
var _27;
arg=CleanExternalString(arg);
this.ClearErrorState();
_27=this.CheckForCommitError(arg);
if(_27===false){
returnValue=SCORM_FALSE;
}else{
returnValue=SCORM_TRUE;
}
this.WriteAuditReturnValue(returnValue);
return returnValue;
}
function RunTimeApi_GetLastError(){
this.WriteAuditLog("`1658`");
var _28=this.ErrorNumber;
this.WriteAuditReturnValue(_28);
return _28;
}
function RunTimeApi_GetErrorString(_29){
this.WriteAuditLog("`1602`"+_29+"')");
var _2a;
_29=CleanExternalString(_29);
if(SCORM_ErrorStrings[_29]===undefined||SCORM_ErrorStrings[_29]===null){
_2a="";
}else{
_2a=SCORM_ErrorStrings[_29];
}
this.WriteAuditReturnValue(_2a);
return _2a;
}
function RunTimeApi_GetDiagnostic(_2b){
this.WriteAuditLog("`1634`"+_2b+"')");
var _2c;
_2b=CleanExternalString(_2b);
if(_2b==this.ErrorNumber||_2b===""||_2b===null){
if(this.ErrorDiagnostic.length>0){
_2c=this.ErrorDiagnostic;
}else{
_2c="No diagnostic information is available.";
}
}else{
_2c="No diagnostic information available for error number ("+_2b+") ";
}
this.WriteAuditReturnValue(_2c);
return _2c;
}
RunTimeApi.prototype.SetErrorState=RunTimeApi_SetErrorState;
RunTimeApi.prototype.ClearErrorState=RunTimeApi_ClearErrorState;
RunTimeApi.prototype.CheckForInitializeError=RunTimeApi_CheckForInitializeError;
RunTimeApi.prototype.CheckForFinishError=RunTimeApi_CheckForFinishError;
RunTimeApi.prototype.CheckForCommitError=RunTimeApi_CheckForCommitError;
RunTimeApi.prototype.CheckForSetValueError=RunTimeApi_CheckForSetValueError;
RunTimeApi.prototype.StoreValue=RunTimeApi_StoreValue;
RunTimeApi.prototype.CheckForGetValueError=RunTimeApi_CheckForGetValueError;
RunTimeApi.prototype.RetrieveGetValueData=RunTimeApi_RetrieveGetValueData;
RunTimeApi.prototype.JoinCommentsArray=RunTimeApi_JoinCommentsArray;
RunTimeApi.prototype.IsValidVocabElement=RunTimeApi_IsValidVocabElement;
function RunTimeApi_SetErrorState(_2d,_2e){
if(_2d!=SCORM_ERROR_NONE){
this.WriteDetailedLog("`1281`"+_2d+" - "+_2e);
}
this.ErrorNumber=_2d;
this.ErrorDiagnostic=_2e;
}
function RunTimeApi_ClearErrorState(){
this.ErrorNumber=SCORM_ERROR_NONE;
this.ErrorDiagnostic="";
}
function RunTimeApi_CheckForInitializeError(arg){
this.WriteDetailedLog("`1420`");
if(arg!==""){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"Invalid argument to LMSInitialize (arg="+arg+")");
return false;
}
if(this.Initialized===true){
this.SetErrorState(SCORM_ERROR_GENERAL,"LMSInitialize has already been called.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForFinishError(arg){
this.WriteDetailedLog("`1496`");
if(this.Initialized===false){
this.SetErrorState(SCORM_ERROR_NOT_INITIALIZED,"Finished called when not initialized.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"Invalid argument to LMSFinish (arg="+arg+")");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForCommitError(arg){
this.WriteDetailedLog("`1487`");
if(this.Initialized===false){
this.SetErrorState(SCORM_ERROR_NOT_INITIALIZED,"Commit called when not initialized.");
return false;
}
if(arg!==""){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"Invalid argument to LMSCommit (arg="+arg+")");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_CheckForSetValueError(_32,_33,_34,_35,_36){
this.WriteDetailedLog("`1526`"+_32+", "+_33+", "+_34+", "+_35+", "+_36+") ");
if(this.Initialized===false){
this.SetErrorState(SCORM_ERROR_NOT_INITIALIZED,"SetValue called when not initialized. strElement-"+_32+" strValue-"+_33);
return false;
}
if(!(arySupportedElements[_34]===undefined||arySupportedElements[_34]===null)){
if(arySupportedElements[_34].Supported===false){
Debug.AssertError("Should not have any un-implemented vocab elements");
this.SetErrorState(SCORM_ERROR_NOT_IMPLEMENTED,"The parameter '"+_32+"' is not implemented.");
return false;
}
if(_34.search(/_children$/)>0||_34.search(/_count$/)>0){
this.SetErrorState(SCORM_ERROR_ELEMENT_IS_KEYWORD,"The parameter '"+_32+"' is a keyword and cannot be written to.");
return false;
}
if(arySupportedElements[_34].SupportsWrite===false){
this.SetErrorState(SCORM_ERROR_READ_ONLY,"The parameter '"+_32+"' is read-only.");
return false;
}
}else{
if(_34.search(/._children$/)>0){
_34=_34.replace("._children","");
if(arySupportedElements[_34]!==undefined&&arySupportedElements[_34]!==null){
this.SetErrorState(SCORM_ERROR_NO_CHILDREN,"The parameter '"+_34+"' does not support the _children keyword.");
return false;
}
}else{
if(_34.search(/._count$/)>0){
_34=_34.replace("._count","");
if(arySupportedElements[strBaseElement]!==undefined&&arySupportedElements[strBaseElement]!==null){
this.SetErrorState(SCORM_ERROR_NO_COUNT,"The parameter '"+_34+"' does not support the _count keyword.");
return false;
}
}
}
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The parameter '"+_32+"' is not recognized.");
return false;
}
var _37=null;
var _38;
switch(_34){
case "cmi.core.lesson_location":
this.WriteDetailedLog("`1460`");
if(_33.length>255){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.core.lesson_location may not be greater than 255 characters, your value ("+_33+") is "+_33.length+" characters.");
_37=false;
}
break;
case "cmi.core.lesson_status":
this.WriteDetailedLog("`1491`");
if(!this.IsValidVocabElement(_33,"status")){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"The value for cmi.core.lesson_status is not in the CMI vocabulary. Your value: "+_33);
_37=false;
}
if(_33==SCORM_STATUS_NOT_ATTEMPTED){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.core.lesson_status cannot be set to 'not attempted'.  This value may only be set by the LMS upon initialization.");
_37=false;
}
break;
case "cmi.core.exit":
this.WriteDetailedLog("`1669`");
if(!this.IsValidVocabElement(_33,"exit")){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"The value for cmi.core.exit is not in the CMI vocabulary. Your value: "+_33);
_37=false;
}
break;
case "cmi.core.session_time":
this.WriteDetailedLog("`1515`");
if(!IsValidCMITimeSpan(_33)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"The value for cmi.core.session_time is not formatted properly. Your value: "+_33);
_37=false;
}
break;
case "cmi.core.score.raw":
this.WriteDetailedLog("`1564`");
if(_33!==""){
if(IsValidCMIDecimal(_33)){
_38=parseFloat(_33);
if(_38<0||_38>100){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.core.score.raw must be a valid decimal between 0 and 100.  Your value is: "+_38);
_37=false;
}
}else{
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.core.score.raw must be a valid decimal.  Your value is: "+_33);
_37=false;
}
}
break;
case "cmi.core.score.max":
this.WriteDetailedLog("`1561`");
if(_33!==""){
if(IsValidCMIDecimal(_33)){
_38=parseFloat(_33);
if(_38<0||_38>100){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.core.score.max must be a valid decimal between 0 and 100.  Your value is: "+_38);
_37=false;
}
}else{
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.core.score.max must be a valid decimal.  Your value is: "+_33);
_37=false;
}
}
break;
case "cmi.core.score.min":
this.WriteDetailedLog("`1563`");
if(_33!==""){
if(IsValidCMIDecimal(_33)){
_38=parseFloat(_33);
if(_38<0||_38>100){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.core.score.min must be a valid decimal between 0 and 100.  Your value is: "+_38);
_37=false;
}
}else{
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.core.score.min must be a valid decimal.  Your value is: "+_33);
_37=false;
}
}
break;
case "cmi.suspend_data":
    this.WriteDetailedLog("`1513`");
// Hack - removing character limit for suspend data in symphony
/*if(_33.length>4096){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.core.suspend_data may not be greater than 4096 characters, your value is "+_33.length+" characters. Your value\n"+_33);
_37=false;
}*/
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1493`");
if(!IsValidCMIIdentifier(_33)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"The value '"+_33+"' is not a valid CMI Identifier");
_37=false;
}
if(!this.RunTimeData.IsValidObjectiveIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, objective indicies must be set sequentially starting with 0");
_37=false;
}
break;
case "cmi.objectives.n.status":
this.WriteDetailedLog("`1415`");
if(!this.RunTimeData.IsValidObjectiveIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, objective indicies must be set sequentially starting with 0");
_37=false;
}
if(!this.IsValidVocabElement(_33,"status")){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"The value for cmi.objectives.n.status is not in the CMI vocabulary. Your value: "+_33);
_37=false;
}
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1359`");
if(!this.RunTimeData.IsValidObjectiveIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, objective indicies must be set sequentially starting with 0");
_37=false;
}
if(_33!==""){
if(IsValidCMIDecimal(_33)){
_38=parseFloat(_33);
if(_38<0||_38>100){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.objectives.n.score.raw must be a valid decimal between 0 and 100.  Your value is: "+_38);
_37=false;
}
}else{
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.objectives.score.raw must be a valid decimal.  Your value is: "+_33);
_37=false;
}
}
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1357`");
if(!this.RunTimeData.IsValidObjectiveIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, objective indicies must be set sequentially starting with 0");
_37=false;
}
if(_33!==""){
if(IsValidCMIDecimal(_33)){
_38=parseFloat(_33);
if(_38<0||_38>100){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.objectives.n.score.min must be a valid decimal between 0 and 100.  Your value is: "+_38);
_37=false;
}
}else{
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.objectives.score.min must be a valid decimal.  Your value is: "+_33);
_37=false;
}
}
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1358`");
if(!this.RunTimeData.IsValidObjectiveIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, objective indicies must be set sequentially starting with 0");
_37=false;
}
if(_33!==""){
if(IsValidCMIDecimal(_33)){
_38=parseFloat(_33);
if(_38<0||_38>100){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.objectives.n.score.max must be a valid decimal between 0 and 100.  Your value is: "+_38);
_37=false;
}
}else{
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.objectives.score.max must be a valid decimal.  Your value is: "+_33);
_37=false;
}
}
break;
case "cmi.comments":
this.WriteDetailedLog("`1573`");
if(_33.length>4096){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.comments may not be greater than 4096 characters, your value ("+_33+") is "+_33.length+" characters.");
_37=false;
}
break;
case "cmi.student_preference.audio":
this.WriteDetailedLog("`1661`");
if(IsValidCMISInteger(_33)){
_38=parseInt(_33,10);
if(_38<-1||_38>100){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.student_preference.audio must be a valid integer between -1 and 100.  Your value is: "+_38);
_37=false;
}
}else{
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.student_preference.audio must be a valid signed integer.  Your value is: "+_33);
_37=false;
}
break;
case "cmi.student_preference.language":
this.WriteDetailedLog("`1574`");
if(_33.length>255){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.student_preference.language may not be greater than 255 characters, your value ("+_33+") is "+_33.length+" characters.");
_37=false;
}
break;
case "cmi.student_preference.speed":
this.WriteDetailedLog("`1639`");
if(IsValidCMISInteger(_33)){
_38=parseInt(_33,10);
if(_38<-100||_38>100){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.student_preference.audio must be a valid integer between -100 and 100.  Your value is: "+_38);
_37=false;
}
}else{
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.student_preference.audio must be a valid signed integer.  Your value is: "+_33);
_37=false;
}
break;
case "cmi.student_preference.text":
this.WriteDetailedLog("`1675`");
if(IsValidCMISInteger(_33)){
_38=parseInt(_33,10);
if(_38<-1||_38>1){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.student_preference.audio must be a valid integer between -1 and 1.  Your value is: "+_38);
_37=false;
}
}else{
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.student_preference.audio must be a valid signed integer.  Your value is: "+_33);
_37=false;
}
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1444`");
if(!IsValidCMIIdentifier(_33)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"The value '"+_33+"' is not a valid CMI Identifier");
_37=false;
}
if(!this.RunTimeData.IsValidInteractionIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, interaction indicies must be set sequentially starting with 0");
_37=false;
}
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1272`");
if(!IsValidCMIIdentifier(_33)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"The value '"+_33+"' is not a valid CMI Identifier");
_37=false;
}
if(!this.RunTimeData.IsValidInteractionIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, interaction indicies must be set sequentially starting with 0");
_37=false;
}
if(!this.RunTimeData.IsValidInteractionObjectiveIndex(_35,_36)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_36+"' is not valid, interaction objective indicies must be set sequentially starting with 0");
_37=false;
}
break;
case "cmi.interactions.n.time":
this.WriteDetailedLog("`1403`");
if(!this.RunTimeData.IsValidInteractionIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, interaction indicies must be set sequentially starting with 0");
_37=false;
}
if(!IsValidCMITime(_33)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.interactions.n.time must be a valid time.  Your value is: "+_33);
_37=false;
}
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1410`");
if(!this.RunTimeData.IsValidInteractionIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, interaction indicies must be set sequentially starting with 0");
_37=false;
}
if(!this.IsValidVocabElement(_33,"interaction")){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"The value for cmi.interactions.n.type is not in the CMI vocabulary. Your value: "+_33);
_37=false;
}
if(this.RunTimeData.Interactions[_35]!==undefined){
if(this.RunTimeData.Interactions[_35].LearnerResponse!==null){
if(!IsValidCMIFeedback(_33,this.RunTimeData.Interactions[_35].LearnerResponse)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.interactions.n.type must be consistent with previously recorded student response.  Your value is: "+_33);
_37=false;
}
}
for(var i=0;i<this.RunTimeData.Interactions[_35].CorrectResponses.length;i++){
if(!IsValidCMIFeedback(_33,this.RunTimeData.Interactions[_35].CorrectResponses[i])){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.interactions.n.type must be consistent with previously recorded correct response ("+i+" - "+this.RunTimeData.Interactions[_35].CorrectResponses[i]+").  Your value is: "+_33);
_37=false;
}
}
}
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`1013`");
if(!this.RunTimeData.IsValidInteractionIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, interaction indicies must be set sequentially starting with 0");
_37=false;
}
if(!this.RunTimeData.IsValidInteractionCorrectResponseIndex(_35,_36)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_36+"' is not valid, interaction correct response indicies must be set sequentially starting with 0");
_37=false;
}
if(this.RunTimeData.Interactions[_35]!==undefined){
if(!IsValidCMIFeedback(this.RunTimeData.Interactions[_35].Type,_33)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.interactions.n.student_response must be a valid CMIFeedback - value must be consistent with interaction type.  Your value is: "+_33);
_37=false;
}
}
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1329`");
if(!this.RunTimeData.IsValidInteractionIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, interaction indicies must be set sequentially starting with 0");
_37=false;
}
if(!IsValidCMIDecimal(_33)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.interactions.n.weighting must be a valid decimal.  Your value is: "+_33);
_37=false;
}
break;
case "cmi.interactions.n.student_response":
this.WriteDetailedLog("`1219`");
if(!this.RunTimeData.IsValidInteractionIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, interaction indicies must be set sequentially starting with 0");
_37=false;
}
if(this.RunTimeData.Interactions[_35]!==undefined){
if(!IsValidCMIFeedback(this.RunTimeData.Interactions[_35].Type,_33)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.interactions.n.student_response must be a valid CMIFeedback - value must be consistent with interaction type.  Your value is: "+_33);
_37=false;
}
}
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1378`");
if(!this.RunTimeData.IsValidInteractionIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, interaction indicies must be set sequentially starting with 0");
_37=false;
}
if(!this.IsValidVocabElement(_33,"result")&&!IsValidCMIDecimal(_33)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"The value for cmi.interactions.n.result is not in the CMI vocabulary. Your value: "+_33);
_37=false;
}
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1362`");
if(!this.RunTimeData.IsValidInteractionIndex(_35)){
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The index '"+_35+"' is not valid, interaction indicies must be set sequentially starting with 0");
_37=false;
}
if(!IsValidCMITimeSpan(_33)){
this.SetErrorState(SCORM_ERROR_INCORRECT_DATA_TYPE,"cmi.interactions.n.latency must be a valid timespan.  Your value is: "+_33);
_37=false;
}
break;
default:
this.WriteDetailedLog("`1605`");
this.SetErrorState(SCORM_ERROR_GENERAL,"Setting the data element you requested is not supported although it is listed as being supported, please contact technical support.  Element-"+_32);
_37=false;
break;
}
if(_37===null){
_37=true;
}else{
_37=false;
}
if(_37==true){
this.WriteDetailedLog("`1587`");
}
return _37;
}
function RunTimeApi_StoreValue(_3a,_3b,_3c,_3d,_3e){
this.WriteDetailedLog("`1707`"+_3a+", "+_3b+", "+_3c+", "+_3d+", "+_3e+") ");
switch(_3c){
case "cmi.core.lesson_location":
this.WriteDetailedLog("`1460`");
this.RunTimeData.Location=_3b;
break;
case "cmi.core.lesson_status":
this.WriteDetailedLog("`1490`");
this.WriteDetailedLog("`1428`"+this.StatusSetInCurrentSession+"`1590`"+this.RunTimeData.CompletionStatus);
if(this.StatusSetInCurrentSession||this.RunTimeData.CompletionStatus!=SCORM_STATUS_COMPLETED||(this.RunTimeData.CompletionStatus==SCORM_STATUS_COMPLETED&&this.RunTimeData.SuccessStatus==SCORM_STATUS_FAILED)||_3b=="passed"){
this.WriteDetailedLog("`1543`");
this.StatusSetInCurrentSession=true;
this.RunTimeData.CompletionStatusChangedDuringRuntime=true;
if(_3b=="passed"||_3b=="failed"){
this.RunTimeData.SuccessStatusChangedDuringRuntime=true;
}
this.RunTimeData.SuccessStatus=TranslateSingleStatusIntoSuccess(_3b);
this.RunTimeData.CompletionStatus=TranslateSingleStatusIntoCompletion(_3b);
var _3f={ev:"Set",v:_3b};
if(_3b=="passed"||_3b=="failed"){
_3f.k="success";
}else{
_3f.k="completion";
}
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3f);
}
break;
case "cmi.core.exit":
this.WriteDetailedLog("`1669`");
var _3f={ev:"Set",k:"cmi.exit",v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.Exit=_3b;
break;
case "cmi.core.session_time":
this.WriteDetailedLog("`1505`");
this.RunTimeData.SessionTime=ConvertCmiTimeSpanToIso8601TimeSpan(_3b);
this.WriteDetailedLog("`1710`"+this.RunTimeData.SessionTime);
var _3f={ev:"Set",k:"session time",vh:ConvertIso8601TimeSpanToHundredths(this.RunTimeData.SessionTime)};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3f);
break;
case "cmi.core.score.raw":
this.WriteDetailedLog("`1564`");
if(_3b===""){
_3b=null;
}
var _3f={ev:"Set",k:"score.raw",v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.ScoreRaw=_3b;
break;
case "cmi.core.score.max":
this.WriteDetailedLog("`1561`");
if(_3b===""){
_3b=null;
}
var _3f={ev:"Set",k:"score.max",v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.ScoreMax=_3b;
break;
case "cmi.core.score.min":
this.WriteDetailedLog("`1563`");
if(_3b===""){
_3b=null;
}
var _3f={ev:"Set",k:"score.min",v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.ScoreMin=_3b;
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1518`");
this.RunTimeData.SuspendData=_3b;
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1493`");
if(this.RunTimeData.Objectives.length<=_3d){
this.WriteDetailedLog("`1385`"+_3d);
this.RunTimeData.AddObjective();
}
var _3f={ev:"Set",k:"objectives id",i:_3d,v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.Objectives[_3d].Identifier=_3b;
break;
case "cmi.objectives.n.status":
this.WriteDetailedLog("`1415`");
if(this.RunTimeData.Objectives.length<=_3d){
this.WriteDetailedLog("`1385`"+_3d);
this.RunTimeData.AddObjective();
}
this.RunTimeData.Objectives[_3d].SuccessStatus=TranslateSingleStatusIntoSuccess(_3b);
this.RunTimeData.Objectives[_3d].CompletionStatus=TranslateSingleStatusIntoCompletion(_3b);
var _3f={ev:"Set",k:"objectives success",i:_3d,v:TranslateSingleStatusIntoSuccess(_3b)};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Objectives[_3d].Identifier){
_3f.intid=this.RunTimeData.Objectives[_3d].Identifier;
}
this.WriteHistoryLog("",_3f);
var _3f={ev:"Set",k:"objectives completion",i:_3d,v:TranslateSingleStatusIntoCompletion(_3b)};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Objectives[_3d].Identifier){
_3f.intid=this.RunTimeData.Objectives[_3d].Identifier;
}
this.WriteHistoryLog("",_3f);
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1359`");
if(this.RunTimeData.Objectives.length<=_3d){
this.WriteDetailedLog("`1385`"+_3d);
this.RunTimeData.AddObjective();
}
if(_3b===""){
_3b=null;
}
this.RunTimeData.Objectives[_3d].ScoreRaw=_3b;
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1357`");
if(this.RunTimeData.Objectives.length<=_3d){
this.WriteDetailedLog("`1385`"+_3d);
this.RunTimeData.AddObjective();
}
if(_3b===""){
_3b=null;
}
this.RunTimeData.Objectives[_3d].ScoreMin=_3b;
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1358`");
if(this.RunTimeData.Objectives.length<=_3d){
this.WriteDetailedLog("`1385`"+_3d);
this.RunTimeData.AddObjective();
}
if(_3b===""){
_3b=null;
}
this.RunTimeData.Objectives[_3d].ScoreMax=_3b;
break;
case "cmi.comments":
this.WriteDetailedLog("`1172`"+this.RunTimeData.Comments.length);
var _40=new ActivityRunTimeComment(null,null,null,null,null,null);
_40.SetCommentValue(_3b);
this.RunTimeData.Comments[this.RunTimeData.Comments.length]=_40;
break;
case "cmi.student_preference.audio":
this.WriteDetailedLog("`1661`");
this.RunTimeData.AudioLevel=_3b;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.AudioLevel=_3b;
}
break;
case "cmi.student_preference.language":
this.WriteDetailedLog("`1574`");
this.RunTimeData.LanguagePreference=_3b;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.LanguagePreference=_3b;
}
break;
case "cmi.student_preference.speed":
this.WriteDetailedLog("`1639`");
this.RunTimeData.DeliverySpeed=_3b;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.DeliverySpeed=_3b;
}
break;
case "cmi.student_preference.text":
this.WriteDetailedLog("`1675`");
this.RunTimeData.AudioCaptioning=_3b;
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
this.LearnerPrefsArray.AudioCaptioning=_3b;
}
break;
case "cmi.interactions.n.id":
this.WriteDetailedLog("`1444`");
if(this.RunTimeData.Interactions.length<=_3d){
this.WriteDetailedLog("`1312`"+_3d);
this.RunTimeData.AddInteraction();
}
var _3f={ev:"Set",k:"interactions id",i:_3d,v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.Interactions[_3d].Id=_3b;
break;
case "cmi.interactions.n.objectives.n.id":
this.WriteDetailedLog("`1272`");
if(this.RunTimeData.Interactions.length<=_3d){
this.WriteDetailedLog("`1312`"+_3d);
this.RunTimeData.AddInteraction();
}
var _3f={ev:"Set",k:"interactions objectives id",i:_3d,si:_3e,v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.Interactions[_3d].Objectives[_3e]=_3b;
break;
case "cmi.interactions.n.time":
this.WriteDetailedLog("`1403`");
if(this.RunTimeData.Interactions.length<=_3d){
this.WriteDetailedLog("`1312`"+_3d);
this.RunTimeData.AddInteraction();
}
var _41=ConvertCmiTimeToIso8601Time(_3b);
var _3f={ev:"Set",k:"interactions timestamp",i:_3d,v:_41};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_3d].Id){
_3f.intid=this.RunTimeData.Interactions[_3d].Id;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.Interactions[_3d].Timestamp=_41;
break;
case "cmi.interactions.n.type":
this.WriteDetailedLog("`1410`");
if(this.RunTimeData.Interactions.length<=_3d){
this.WriteDetailedLog("`1312`"+_3d);
this.RunTimeData.AddInteraction();
}
var _3f={ev:"Set",k:"interactions type",i:_3d,v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_3d].Id){
_3f.intid=this.RunTimeData.Interactions[_3d].Id;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.Interactions[_3d].Type=_3b;
break;
case "cmi.interactions.n.correct_responses.n.pattern":
this.WriteDetailedLog("`1028`");
if(this.RunTimeData.Interactions.length<=_3d){
this.WriteDetailedLog("`1312`"+_3d);
this.RunTimeData.AddInteraction();
}
var _3f={ev:"Set",k:"interactions correct_responses pattern",i:_3d,si:_3e,v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.Interactions[_3d].CorrectResponses[_3e]=_3b;
break;
case "cmi.interactions.n.weighting":
this.WriteDetailedLog("`1329`");
if(this.RunTimeData.Interactions.length<=_3d){
this.WriteDetailedLog("`1312`"+_3d);
this.RunTimeData.AddInteraction();
}
this.RunTimeData.Interactions[_3d].Weighting=_3b;
break;
case "cmi.interactions.n.student_response":
this.WriteDetailedLog("`1219`");
if(this.RunTimeData.Interactions.length<=_3d){
this.WriteDetailedLog("`1312`"+_3d);
this.RunTimeData.AddInteraction();
}
var _3f={ev:"Set",k:"interactions learner_response",i:_3d,v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_3d].Id){
_3f.intid=this.RunTimeData.Interactions[_3d].Id;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.Interactions[_3d].LearnerResponse=_3b;
break;
case "cmi.interactions.n.result":
this.WriteDetailedLog("`1378`");
if(this.RunTimeData.Interactions.length<=_3d){
this.WriteDetailedLog("`1312`"+_3d);
this.RunTimeData.AddInteraction();
}
if(_3b==SCORM_WRONG){
_3b=SCORM_INCORRECT;
}
var _3f={ev:"Set",k:"interactions result",i:_3d,v:_3b};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_3d].Id){
_3f.intid=this.RunTimeData.Interactions[_3d].Id;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.Interactions[_3d].Result=_3b;
break;
case "cmi.interactions.n.latency":
this.WriteDetailedLog("`1362`");
if(this.RunTimeData.Interactions.length<=_3d){
this.WriteDetailedLog("`1312`"+_3d);
this.RunTimeData.AddInteraction();
}
var _42=ConvertCmiTimeSpanToIso8601TimeSpan(_3b);
var _3f={ev:"Set",k:"interactions latency",i:_3d,vh:ConvertIso8601TimeSpanToHundredths(_42)};
if(this.Activity){
_3f.ai=this.Activity.ItemIdentifier;
}
if(this.RunTimeData.Interactions[_3d].Id){
_3f.intid=this.RunTimeData.Interactions[_3d].Id;
}
this.WriteHistoryLog("",_3f);
this.RunTimeData.Interactions[_3d].Latency=_42;
break;
default:
Debug.AssertError("Unrecognized data model element in StoreData");
this.SetErrorState(SCORM_ERROR_GENERAL,"Setting the data element you requested is not supported although it is listed as being supported, please contact technical support.  Element-"+strElement);
return false;
}
return true;
}
function RunTimeApi_CheckForGetValueError(_43,_44,_45,_46){
this.WriteDetailedLog("`1536`"+_43+", "+_44+", "+_45+", "+_46+") ");
if(this.Initialized===false){
this.SetErrorState(SCORM_ERROR_NOT_INITIALIZED,"GetValue called when not initialized. element-"+_43);
return false;
}
if(arySupportedElements[_44]!==undefined&&arySupportedElements[_44]!==null){
if(!arySupportedElements[_44].Supported){
this.SetErrorState(SCORM_ERROR_NOT_IMPLEMENTED,"The parameter '"+_43+"' is not implemented.");
return false;
}
if(!arySupportedElements[_44].SupportsRead){
this.SetErrorState(SCORM_ERROR_WRITE_ONLY,"The parameter '"+_43+"' is write-only.");
return false;
}
}else{
if(_44.search(/._children$/)>0){
strBaseElement=_44.replace("._children","");
if(arySupportedElements[strBaseElement]!==undefined&&arySupportedElements[_43]!==null){
this.SetErrorState(SCORM_ERROR_NO_CHILDREN,"The parameter '"+_43+"' does not support the _children keyword.");
return false;
}
}else{
if(_44.search(/._count$/)>0){
strBaseElement=_44.replace("._count","");
if(arySupportedElements[strBaseElement]!==undefined&&arySupportedElements[_43]!==null){
this.SetErrorState(SCORM_ERROR_NO_COUNT,"The parameter '"+_43+"' does not support the _count keyword.");
return false;
}
}
}
this.SetErrorState(SCORM_ERROR_INVALID_ARG,"The parameter '"+_43+"' is not recognized.");
return false;
}
this.WriteDetailedLog("`1587`");
return true;
}
function RunTimeApi_RetrieveGetValueData(_47,_48,_49,_4a){
this.WriteDetailedLog("`1541`"+_47+", "+_48+", "+_49+", "+_4a+") ");
var _4b="";
switch(_48){
case "cmi.core._children":
this.WriteDetailedLog("`1477`");
_4b=SCORM_CORE_CHILDREN;
break;
case "cmi.core.student_id":
this.WriteDetailedLog("`1553`");
_4b=this.LearnerId;
break;
case "cmi.core.student_name":
this.WriteDetailedLog("`1500`");
_4b=this.LearnerName;
break;
case "cmi.core.lesson_location":
this.WriteDetailedLog("`1457`");
_4b=this.RunTimeData.Location;
var _4c={ev:"Get",k:"location",v:(_4b==null?"<null>":_4b)};
if(this.Activity){
_4c.ai=this.Activity.ItemIdentifier;
}
this.WriteHistoryLog("",_4c);
break;
case "cmi.core.credit":
this.WriteDetailedLog("`1617`");
_4b=this.RunTimeData.Credit;
break;
case "cmi.core.lesson_status":
this.WriteDetailedLog("`1492`");
_4b=TranslateDualStausToSingleStatus(this.RunTimeData.CompletionStatus,this.RunTimeData.SuccessStatus);
break;
case "cmi.core.entry":
this.WriteDetailedLog("`1643`");
_4b=this.RunTimeData.Entry;
break;
case "cmi.core.score._children":
this.WriteDetailedLog("`1446`");
_4b=SCORM_CORE_SCORE_CHILDREN;
break;
case "cmi.core.score.raw":
this.WriteDetailedLog("`1564`");
_4b=this.RunTimeData.ScoreRaw;
break;
case "cmi.core.score.max":
this.WriteDetailedLog("`1561`");
_4b=this.RunTimeData.ScoreMax;
break;
case "cmi.core.score.min":
this.WriteDetailedLog("`1563`");
_4b=this.RunTimeData.ScoreMin;
break;
case "cmi.core.total_time":
this.WriteDetailedLog("`1547`");
_4b=ConvertIso8601TimeSpanToCmiTimeSpan(this.RunTimeData.TotalTime);
break;
case "cmi.core.lesson_mode":
this.WriteDetailedLog("`1520`");
_4b=this.RunTimeData.Mode;
break;
case "cmi.suspend_data":
this.WriteDetailedLog("`1518`");
_4b=this.RunTimeData.SuspendData;
break;
case "cmi.launch_data":
this.WriteDetailedLog("`1538`");
_4b=this.LearningObject.DataFromLms;
break;
case "cmi.objectives._children":
this.WriteDetailedLog("`1365`");
_4b=SCORM_OBJECTIVES_CHILDREN;
break;
case "cmi.objectives._count":
this.WriteDetailedLog("`1407`");
_4b=this.RunTimeData.Objectives.length;
break;
case "cmi.objectives.n.id":
this.WriteDetailedLog("`1493`");
if(this.RunTimeData.Objectives[_49]===null||this.RunTimeData.Objectives[_49]===undefined||this.RunTimeData.Objectives[_49].Identifier===null){
this.WriteDetailedLog("`960`");
_4b="";
}else{
_4b=this.RunTimeData.Objectives[_49].Identifier;
}
break;
case "cmi.objectives.n.status":
this.WriteDetailedLog("`1415`");
if(this.RunTimeData.Objectives.length<(_49+1)||this.RunTimeData.Objectives[_49]===null||this.RunTimeData.Objectives[_49]===undefined||this.RunTimeData.Objectives[_49].CompletionStatus===null||this.RunTimeData.Objectives[_49].SuccessStatus===null){
this.WriteDetailedLog("`960`");
_4b="";
}else{
_4b=TranslateDualStausToSingleStatus(this.RunTimeData.Objectives[_49].CompletionStatus,this.RunTimeData.Objectives[_49].SuccessStatus);
}
break;
case "cmi.objectives.n.score._children":
this.WriteDetailedLog("`1264`");
_4b=SCORM_OBJECTIVES_SCORE_CHILDREN;
break;
case "cmi.objectives.n.score.raw":
this.WriteDetailedLog("`1359`");
if(this.RunTimeData.Objectives.length<(_49+1)||this.RunTimeData.Objectives[_49]===null||this.RunTimeData.Objectives[_49]===undefined||this.RunTimeData.Objectives[_49].ScoreRaw===null){
this.WriteDetailedLog("`960`");
_4b="";
}else{
_4b=this.RunTimeData.Objectives[_49].ScoreRaw;
}
break;
case "cmi.objectives.n.score.max":
this.WriteDetailedLog("`1358`");
if(this.RunTimeData.Objectives.length<(_49+1)||this.RunTimeData.Objectives[_49]===null||this.RunTimeData.Objectives[_49]===undefined||this.RunTimeData.Objectives[_49].ScoreMax===null){
this.WriteDetailedLog("`960`");
_4b="";
}else{
_4b=this.RunTimeData.Objectives[_49].ScoreMax;
}
break;
case "cmi.objectives.n.score.min":
this.WriteDetailedLog("`1357`");
if(this.RunTimeData.Objectives.length<(_49+1)||this.RunTimeData.Objectives[_49]===null||this.RunTimeData.Objectives[_49]===undefined||this.RunTimeData.Objectives[_49].ScoreMin===null){
this.WriteDetailedLog("`960`");
_4b="";
}else{
_4b=this.RunTimeData.Objectives[_49].ScoreMin;
}
break;
case "cmi.comments":
this.WriteDetailedLog("`1573`");
_4b=this.JoinCommentsArray(this.RunTimeData.Comments);
break;
case "cmi.comments_from_lms":
this.WriteDetailedLog("`1417`");
_4b=this.JoinCommentsArray(this.RunTimeData.CommentsFromLMS);
break;
case "cmi.student_data._children":
this.WriteDetailedLog("`1315`");
_4b=SCORM_STUDENT_DATA_CHILDREN;
break;
case "cmi.student_data.mastery_score":
this.WriteDetailedLog("`1488`");
if(this.LearningObject.MasteryScore===null){
_4b="";
}else{
_4b=this.LearningObject.MasteryScore;
}
break;
case "cmi.student_data.max_time_allowed":
this.WriteDetailedLog("`1430`");
_4b=ConvertIso8601TimeSpanToCmiTimeSpan(this.LearningObject.MaxTimeAllowed);
break;
case "cmi.student_data.time_limit_action":
this.WriteDetailedLog("`1419`");
_4b=this.LearningObject.TimeLimitAction;
break;
case "cmi.student_preference._children":
this.WriteDetailedLog("`1231`");
_4b=SCORM_STUDENT_PREFERENCE_CHILDREN;
break;
case "cmi.student_preference.audio":
this.WriteDetailedLog("`1661`");
if(this.RunTimeData.AudioLevel===null){
_4b="";
}else{
_4b=Math.round(this.RunTimeData.AudioLevel);
}
break;
case "cmi.student_preference.language":
this.WriteDetailedLog("`1574`");
_4b=this.RunTimeData.LanguagePreference;
break;
case "cmi.student_preference.speed":
this.WriteDetailedLog("`1639`");
if(this.RunTimeData.DeliverySpeed===null){
_4b="";
}else{
_4b=Math.round(this.RunTimeData.DeliverySpeed);
}
break;
case "cmi.student_preference.text":
this.WriteDetailedLog("`1675`");
_4b=this.RunTimeData.AudioCaptioning;
break;
case "cmi.interactions._children":
this.WriteDetailedLog("`1321`");
_4b=SCORM_INTERACTIONS_CHILDREN;
break;
case "cmi.interactions._count":
this.WriteDetailedLog("`1380`");
_4b=this.RunTimeData.Interactions.length;
break;
case "cmi.interactions.n.objectives._count":
this.WriteDetailedLog("`1200`");
if(this.RunTimeData.Interactions.length<(_49+1)||this.RunTimeData.Interactions[_49]===null||this.RunTimeData.Interactions[_49]===undefined){
this.WriteDetailedLog("`1620`"+_49+"`1700`");
_4b=0;
}else{
_4b=this.RunTimeData.Interactions[_49].Objectives.length;
}
break;
case "cmi.interactions.n.correct_responses._count":
this.WriteDetailedLog("`1044`");
if(this.RunTimeData.Interactions.length<(_49+1)||this.RunTimeData.Interactions[_49]===null||this.RunTimeData.Interactions[_49]===undefined){
this.WriteDetailedLog("`1620`"+_49+"`1700`");
_4b=0;
}else{
_4b=this.RunTimeData.Interactions[_49].CorrectResponses.length;
}
break;
default:
Debug.AssertError("An unsupported data model element slipped through GetValue error detection.");
SetErrorInfo(SCORM_ERROR_GENERAL,"Getting the data element you requested is not supported although it is listed as being supported, please contact technical support.  Element-"+strElement);
_4b="";
break;
}
return _4b;
}
function RunTimeApi_CloseOutSession(){
this.WriteDetailedLog("`1653`");
var _4d=this.LearningObject.MasteryScore;
this.WriteDetailedLog("`1724`"+this.RunTimeData.Mode);
this.WriteDetailedLog("`1721`"+this.RunTimeData.Credit);
this.WriteDetailedLog("`1589`"+this.RunTimeData.CompletionStatus);
this.WriteDetailedLog("`1665`"+this.RunTimeData.SuccessStatus);
this.WriteDetailedLog("`1684`"+_4d);
this.WriteDetailedLog("`1723`"+this.RunTimeData.ScoreRaw);
if(this.RunTimeData.Mode==SCORM_MODE_REVIEW){
this.WriteDetailedLog("`1386`");
}else{
if(this.RunTimeData.Mode==SCORM_MODE_BROWSE&&this.RunTimeData.Credit==SCORM_CREDIT_NO){
}else{
if(this.RunTimeData.Credit==SCORM_CREDIT){
this.WriteDetailedLog("`1504`");
if(this.RunTimeData.CompletionStatus==SCORM_STATUS_UNKNOWN){
this.WriteDetailedLog("`910`");
if(_4d===null||this.RunTimeData.ScoreRaw===null||this.RunTimeData.ScoreRaw===""){
this.WriteDetailedLog("`951`");
this.RunTimeData.CompletionStatus=SCORM_STATUS_COMPLETED;
}else{
if(this.RunTimeData.ScoreRaw>=_4d){
this.WriteDetailedLog("`1227`");
this.RunTimeData.SuccessStatus=SCORM_STATUS_PASSED;
this.RunTimeData.CompletionStatus=SCORM_STATUS_COMPLETED;
}else{
this.WriteDetailedLog("`1178`");
this.RunTimeData.SuccessStatus=SCORM_STATUS_FAILED;
this.RunTimeData.CompletionStatus=SCORM_STATUS_COMPLETED;
}
}
}else{
if(Control.Package.Properties.ScoreOverridesStatus){
this.WriteDetailedLog("`762`");
if(_4d!==null&&_4d!==""&&this.RunTimeData.ScoreRaw!==null&&this.RunTimeData.ScoreRaw!==""){
if(this.RunTimeData.ScoreRaw>=_4d){
this.WriteDetailedLog("`1227`");
this.RunTimeData.SuccessStatus=SCORM_STATUS_PASSED;
this.RunTimeData.CompletionStatus=SCORM_STATUS_COMPLETED;
}else{
this.WriteDetailedLog("`1178`");
this.RunTimeData.SuccessStatus=SCORM_STATUS_FAILED;
this.RunTimeData.CompletionStatus=SCORM_STATUS_COMPLETED;
}
}
}
}
}
}
}
if(this.RunTimeData.ScoreRaw!==null&&this.RunTimeData.ScoreRaw!==""){
this.RunTimeData.ScoreScaled=NormalizeRawScore(this.RunTimeData.ScoreRaw,this.RunTimeData.ScoreMin,this.RunTimeData.ScoreMax);
}
if(this.RunTimeData.CompletionStatus==SCORM_STATUS_COMPLETED&&this.RunTimeData.SuccessStatus!=SCORM_STATUS_FAILED){
this.WriteDetailedLog("`819`");
this.RunTimeData.Credit=SCORM_CREDIT_NO;
this.RunTimeData.Mode=SCORM_MODE_REVIEW;
}
if(this.RunTimeData.CompletionStatus==SCORM_STATUS_COMPLETED||this.RunTimeData.Exit!=SCORM_EXIT_SUSPEND){
this.WriteDetailedLog("`1577`");
this.RunTimeData.Entry=SCORM_ENTRY_NORMAL;
}else{
this.WriteDetailedLog("`1583`");
this.RunTimeData.Entry=SCORM_ENTRY_RESUME;
}
if(this.RunTimeData.Exit==SCORM_EXIT_SUSPEND){
this.Activity.SetSuspended(true);
}else{
this.Activity.SetSuspended(false);
}
var _4e=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.SessionTime);
var _4f=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTime);
var _50=_4e+_4f;
var _51=ConvertHundredthsToIso8601TimeSpan(_50);
this.WriteDetailedLog("`1695`"+this.RunTimeData.SessionTime+" ("+_4e+"`1705`");
this.WriteDetailedLog("`1677`"+this.RunTimeData.TotalTime+" ("+_4f+"`1705`");
this.WriteDetailedLog("`1673`"+_51+" ("+_50+"`1705`");
this.RunTimeData.TotalTime=_51;
this.RunTimeData.SessionTime="";
this.AccumulateTotalTimeTracked();
this.WriteDetailedLog("`1498`"+this.RunTimeData.TotalTimeTracked);
}
function RunTimeApi_JoinCommentsArray(_52){
var _53="";
for(var i=0;i<_52.length;i++){
_53+=_52[i].GetCommentValue();
}
return _53;
}
function RunTimeApi_IsValidVocabElement(_55,_56){
var _57;
var i;
_57=aryVocabularies[_56];
if(_57===undefined||_57===null){
return false;
}else{
for(i=0;i<_57.length;i++){
if(_57[i]==_55){
return true;
}
}
return false;
}
}
function RunTimeApi_InitTrackedTimeStart(_59){
this.TrackedStartDate=new Date();
this.StartSessionTotalTime=_59.RunTime.TotalTime;
}
function RunTimeApi_AccumulateTotalTrackedTime(){
this.TrackedEndDate=new Date();
var _5a=Math.round((this.TrackedEndDate-this.TrackedStartDate)/10);
var _5b=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTimeTracked);
var _5c=_5a+_5b;
this.RunTimeData.TotalTimeTracked=ConvertHundredthsToIso8601TimeSpan(_5c);
this.Activity.ActivityEndedDate=this.TrackedEndDate;
var _5d=GetDateFromUtcIso8601Time(this.Activity.GetActivityStartTimestampUtc());
var _5e=GetDateFromUtcIso8601Time(this.Activity.GetAttemptStartTimestampUtc());
this.Activity.SetActivityAbsoluteDuration(ConvertHundredthsToIso8601TimeSpan((this.TrackedEndDate-_5d)/10));
this.Activity.SetAttemptAbsoluteDuration(ConvertHundredthsToIso8601TimeSpan((this.TrackedEndDate-_5e)/10));
var _5f=ConvertIso8601TimeSpanToHundredths(this.Activity.GetActivityExperiencedDurationTracked());
var _60=ConvertHundredthsToIso8601TimeSpan(_5f+_5a);
this.Activity.SetActivityExperiencedDurationTracked(_60);
var _61=ConvertIso8601TimeSpanToHundredths(this.Activity.GetActivityExperiencedDurationReported());
var _62=ConvertIso8601TimeSpanToHundredths(this.RunTimeData.TotalTime)-ConvertIso8601TimeSpanToHundredths(this.StartSessionTotalTime);
var _63=ConvertHundredthsToIso8601TimeSpan(_61+_62);
this.Activity.SetActivityExperiencedDurationReported(_63);
var _64=ConvertIso8601TimeSpanToHundredths(this.Activity.GetAttemptExperiencedDurationTracked());
var _65=ConvertHundredthsToIso8601TimeSpan(_64+_5a);
this.Activity.SetAttemptExperiencedDurationTracked(_65);
var _66=ConvertIso8601TimeSpanToHundredths(this.Activity.GetAttemptExperiencedDurationReported());
var _67=ConvertHundredthsToIso8601TimeSpan(_66+_62);
this.Activity.SetAttemptExperiencedDurationReported(_67);
}
function Sequencer(_68,_69){
this.LookAhead=_68;
this.Activities=_69;
this.NavigationRequest=null;
this.SuspendedActivity=null;
this.CurrentActivity=null;
this.ExceptionText="";
this.AtEndOfCourse=false;
this.AtStartOfCourse=false;
}
Sequencer.prototype.OverallSequencingProcess=Sequencer_OverallSequencingProcess;
Sequencer.prototype.SetSuspendedActivity=Sequencer_SetSuspendedActivity;
Sequencer.prototype.GetSuspendedActivity=Sequencer_GetSuspendedActivity;
Sequencer.prototype.Start=Sequencer_Start;
Sequencer.prototype.InitialRandomizationAndSelection=Sequencer_InitialRandomizationAndSelection;
Sequencer.prototype.GetCurrentActivity=Sequencer_GetCurrentActivity;
Sequencer.prototype.GetExceptionText=Sequencer_GetExceptionText;
Sequencer.prototype.GetExitAction=Sequencer_GetExitAction;
Sequencer.prototype.IsActivityLastOverall=Sequencer_IsActivityLastOverall;
Sequencer.prototype.IsActivityFirstOverall=Sequencer_IsActivityFirstOverall;
Sequencer.prototype.EvaluatePossibleNavigationRequests=Sequencer_EvaluatePossibleNavigationRequests;
Sequencer.prototype.InitializePossibleNavigationRequestAbsolutes=Sequencer_InitializePossibleNavigationRequestAbsolutes;
Sequencer.prototype.ContentDeliveryEnvironmentActivityDataSubProcess=Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess;
Sequencer.prototype.LogSeq=Sequencer_LogSeq;
Sequencer.prototype.LogSeqAudit=Sequencer_LogSeqAudit;
Sequencer.prototype.LogSeqReturn=Sequencer_LogSeqReturn;
Sequencer.prototype.WriteHistoryLog=Sequencer_WriteHistoryLog;
Sequencer.prototype.WriteHistoryReturnValue=Sequencer_WriteHistoryReturnValue;
function Sequencer_OverallSequencingProcess(){
var _6a=this.LogSeqAudit("`1015`");
this.ExceptionText="";
this.LogSeq("`1257`",_6a);
this.CurrentActivity.TransferRteDataToActivity();
if((this.CurrentActivity.LearningObject.ScormType===SCORM_TYPE_ASSET)&&this.CurrentActivity.WasLaunchedThisSession()){
this.LogSeq("`776`",_6a);
this.CurrentActivity.SetAttemptProgressStatus(true);
this.CurrentActivity.SetAttemptCompletionStatus(true);
}
this.LogSeq("`1506`",_6a);
this.RollupData(this.CurrentActivity);
this.LogSeq("`1398`",_6a);
if(Control.Package.Properties.FirstScoIsPretest===true){
if(this.IsActivityFirstOverall(this.CurrentActivity)){
if(this.CurrentActivity.IsSatisfied()===true){
this.LogSeq("`1023`",_6a);
this.MarkAllActivitiesComplete();
}
}
}
if(this.NavigationRequest===null){
var _6b=this.GetExitAction(this.GetCurrentActivity(),false,_6a);
this.LogSeq("`1222`"+_6b,_6a);
var _6c="";
if(_6b==EXIT_ACTION_EXIT_CONFIRMATION||_6b==EXIT_ACTION_DISPLAY_MESSAGE){
var _6d=Control.Activities.GetRootActivity();
var _6e=(_6d.IsCompleted()||_6d.IsSatisfied());
if(_6e===true){
_6c=IntegrationImplementation.GetString("The course is now complete. Please make a selection to continue.");
}else{
_6c=IntegrationImplementation.GetString("Please make a selection to continue.");
}
}
switch(_6b){
case (EXIT_ACTION_EXIT_NO_CONFIRMATION):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
break;
case (EXIT_ACTION_EXIT_CONFIRMATION):
if(confirm(IntegrationImplementation.GetString("Would you like to exit the course now?"))){
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
}else{
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE,null,_6c);
}
break;
case (EXIT_ACTION_GO_TO_NEXT_SCO):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_CONTINUE,null,"");
break;
case (EXIT_ACTION_DISPLAY_MESSAGE):
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE,null,_6c);
break;
case (EXIT_ACTION_DO_NOTHING):
this.NavigationRequest=null;
break;
case (EXIT_ACTION_REFRESH_PAGE):
Control.RefreshPage();
break;
}
}
if(this.NavigationRequest==null){
this.LogSeqReturn("`1393`",_6a);
return;
}
switch(this.NavigationRequest.Type){
case NAVIGATION_REQUEST_CONTINUE:
this.DoContinue();
break;
case NAVIGATION_REQUEST_PREVIOUS:
this.DoPrevious();
break;
case NAVIGATION_REQUEST_CHOICE:
this.DoChoice(this.NavigationRequest.TargetActivity);
break;
case NAVIGATION_REQUEST_EXIT:
break;
case NAVIGATION_REQUEST_EXIT_ALL:
Control.ExitScormPlayer();
break;
case NAVIGATION_REQUEST_SUSPEND_ALL:
Control.ExitScormPlayer();
break;
case NAVIGATION_REQUEST_ABANDON:
break;
case NAVIGATION_REQUEST_ABANDON_ALL:
Control.ExitScormPlayer();
break;
case NAVIGATION_REQUEST_DISPLAY_MESSAGE:
break;
case NAVIGATION_REQUEST_EXIT_PLAYER:
Control.ExitScormPlayer();
break;
default:
Debug.AssertError("Recieved an unrecognized navigation request - "+this.NavigationRequest);
break;
}
this.LogSeqReturn("",_6a);
}
function Sequencer_SetSuspendedActivity(_6f){
this.SuspendedActivity=_6f;
}
function Sequencer_GetSuspendedActivity(){
return this.SuspendedActivity;
}
function Sequencer_Start(){
var _70;
if(this.SuspendedActivity!==null){
_70=this.SuspendedActivity;
}else{
_70=this.GetFirstIncompleteActivity(this.Activities.SortedActivityList);
}
if(Control.Package.Properties.AlwaysFlowToFirstSco===true){
this.DeliverThisActivity(_70);
}else{
var _71=IntegrationImplementation.GetString("Please make a selection to continue.");
this.CurrentActivity=_70;
this.NavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_DISPLAY_MESSAGE,null,_71);
}
}
function Sequencer_InitialRandomizationAndSelection(){
}
function Sequencer_GetCurrentActivity(){
return this.CurrentActivity;
}
function Sequencer_GetExceptionText(){
return this.ExceptionText;
}
function Sequencer_GetExitAction(_72,_73,_74){
var _75=_72.RunTime.Exit;
if(_75==SCORM_EXIT_UNKNOWN){
_75=SCORM_EXIT_NORMAL;
}
var _76=this.IsActivityLastOverall(_72,_74);
var _77;
if(_73){
_77=((_72.RunTime.CompletionStatus==SCORM_STATUS_COMPLETED)||(_72.RunTime.SuccessStatus==SCORM_STATUS_PASSED));
}else{
_77=(_72.IsCompleted()==true||_72.IsSatisfied()==true);
}
var _78=Control.Activities.GetRootActivity();
var _79=(_78.IsCompleted()==true||_78.IsSatisfied()==true);
var _7a;
if(_76){
if(_79===true){
switch(_75){
case SCORM_EXIT_NORMAL:
_74.write("Using finalScoCourseSatisfiedNormalExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.FinalScoCourseSatisfiedNormalExitAction;
break;
case SCORM_EXIT_SUSPEND:
_74.write("Using finalScoCourseSatisfiedSuspendExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.FinalScoCourseSatisfiedSuspendExitAction;
break;
case SCORM_EXIT_TIME_OUT:
_74.write("Using finalScoCourseSatisfiedTimeoutExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.FinalScoCourseSatisfiedTimeoutExitAction;
break;
case SCORM_EXIT_LOGOUT:
_74.write("Using finalScoCourseSatisfiedLogoutExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.FinalScoCourseSatisfiedLogoutExitAction;
break;
}
}else{
switch(_75){
case SCORM_EXIT_NORMAL:
_74.write("Using finalScoCourseNotSatisfiedNormalExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.FinalScoCourseNotSatisfiedNormalExitAction;
break;
case SCORM_EXIT_SUSPEND:
_74.write("Using finalScoCourseNotSatisfiedSuspendExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.FinalScoCourseNotSatisfiedSuspendExitAction;
break;
case SCORM_EXIT_TIME_OUT:
_74.write("Using finalScoCourseNotSatisfiedTimeoutExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.FinalScoCourseNotSatisfiedTimeoutExitAction;
break;
case SCORM_EXIT_LOGOUT:
_74.write("Using finalScoCourseNotSatisfiedLogoutExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.FinalScoCourseNotSatisfiedLogoutExitAction;
break;
}
}
}else{
if(_77===true){
switch(_75){
case SCORM_EXIT_NORMAL:
_74.write("Using intermediateScoSatisfiedNormalExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.IntermediateScoSatisfiedNormalExitAction;
break;
case SCORM_EXIT_SUSPEND:
_74.write("Using intermediateScoSatisfiedSuspendExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.IntermediateScoSatisfiedSuspendExitAction;
break;
case SCORM_EXIT_TIME_OUT:
_74.write("Using intermediateScoSatisfiedTimeoutExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.IntermediateScoSatisfiedTimeoutExitAction;
break;
case SCORM_EXIT_LOGOUT:
_74.write("Using intermediateScoSatisfiedLogoutExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.IntermediateScoSatisfiedLogoutExitAction;
break;
}
}else{
switch(_75){
case SCORM_EXIT_NORMAL:
_74.write("Using intermediateScoNotSatisfiedNormalExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.IntermediateScoNotSatisfiedNormalExitAction;
break;
case SCORM_EXIT_SUSPEND:
_74.write("Using intermediateScoNotSatisfiedSuspendExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.IntermediateScoNotSatisfiedSuspendExitAction;
break;
case SCORM_EXIT_TIME_OUT:
_74.write("Using intermediateScoNotSatisfiedTimeoutExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.IntermediateScoNotSatisfiedTimeoutExitAction;
break;
case SCORM_EXIT_LOGOUT:
_74.write("Using intermediateScoNotSatisfiedLogoutExitAction parameter");
_7a=RegistrationToDeliver.Package.Properties.IntermediateScoNotSatisfiedLogoutExitAction;
break;
}
}
}
return _7a;
}
Sequencer.prototype.LogSeq=Sequencer_LogSeq;
Sequencer.prototype.LogSeqAudit=Sequencer_LogSeqAudit;
Sequencer.prototype.DeliverThisActivity=Sequencer_DeliverThisActivity;
Sequencer.prototype.GetFirstIncompleteActivity=Sequencer_GetFirstIncompleteActivity;
Sequencer.prototype.DoContinue=Sequencer_DoContinue;
Sequencer.prototype.DoPrevious=Sequencer_DoPrevious;
Sequencer.prototype.DoChoice=Sequencer_DoChoice;
Sequencer.prototype.RollupData=Sequencer_RollupData;
Sequencer.prototype.ActivityRollupProcess=Sequencer_ActivityRollupProcess;
Sequencer.prototype.MarkAllActivitiesComplete=Sequencer_MarkAllActivitiesComplete;
function Sequencer_LogSeq(str){
if(this.LookAhead===true){
Debug.WriteLookAheadDetailed(str);
}else{
Debug.WriteSequencingDetailed(str);
}
}
function Sequencer_LogSeqAudit(str){
if(this.LookAhead===true){
Debug.WriteLookAheadAudit(str);
}else{
Debug.WriteSequencingAudit(str);
}
}
function Sequencer_DeliverThisActivity(_7d){
if(Control.Package.Properties.ScoLaunchType!==LAUNCH_TYPE_POPUP_AFTER_CLICK&&Control.Package.Properties.ScoLaunchType!==LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR){
this.ContentDeliveryEnvironmentActivityDataSubProcess(_7d);
}
this.CurrentActivity=_7d;
var _7e=this.Activities.SortedActivityList;
for(var i=0;i<_7e.length;i++){
_7e[i].SetActive(false);
}
var _80=this.Activities.GetActivityPath(_7d,true);
for(var i=0;i<_80.length;i++){
_80[i].SetActive(true);
}
Control.DeliverActivity(_7d);
}
function Sequencer_GetFirstIncompleteActivity(_81){
var _82=null;
var _83;
var _84;
for(var i=0;i<_81.length;i++){
if(_81[i].IsDeliverable()===true){
if(_82===null){
_82=_81[i];
}
_83=_81[i].IsSatisfied();
_84=_81[i].IsCompleted();
if(_84==false||_84==RESULT_UNKNOWN||(_84==true&&_83==false)){
if(_81[i].RunTime!==null&&_81[i].RunTime.CompletionStatus!=SCORM_STATUS_BROWSED){
return _81[i];
}
}
}
}
if(_82!==null){
return _82;
}else{
Debug.AssertError("No Deliverable Activities Found");
}
}
function Sequencer_DoContinue(){
if(this.AtEndOfCourse===true){
return;
}
var _86;
if(this.AtStartOfCourse===true){
_86=-1;
}else{
_86=this.Activities.GetSortedIndexOfActivity(this.CurrentActivity);
}
var _87=null;
for(var i=(_86+1);i<this.Activities.SortedActivityList.length;i++){
if(this.Activities.SortedActivityList[i].IsDeliverable()===true){
_87=this.Activities.SortedActivityList[i];
break;
}
}
if(_87!==null){
this.AtEndOfCourse=false;
this.AtStartOfCourse=false;
this.DeliverThisActivity(_87);
}else{
this.AtEndOfCourse=true;
this.ExceptionText=IntegrationImplementation.GetString("You have reached the end of the course.");
}
}
function Sequencer_DoPrevious(){
if(this.AtStartOfCourse===true){
return;
}
var _89;
if(this.AtEndOfCourse===true){
_89=this.Activities.SortedActivityList.length;
}else{
_89=this.Activities.GetSortedIndexOfActivity(this.CurrentActivity);
}
var _8a=null;
for(var i=(_89-1);i>=0;i--){
if(this.Activities.SortedActivityList[i].IsDeliverable()===true){
_8a=this.Activities.SortedActivityList[i];
break;
}
}
if(_8a!==null){
this.AtEndOfCourse=false;
this.AtStartOfCourse=false;
this.DeliverThisActivity(_8a);
}else{
this.AtStartOfCourse=true;
this.ExceptionText=IntegrationImplementation.GetString("You have reached the beginning of the course.");
}
}
function Sequencer_DoChoice(_8c){
var _8d=null;
_8d=this.Activities.GetActivityFromIdentifier(_8c);
if(_8d!==null){
if(_8d.IsDeliverable()===true){
this.AtEndOfCourse=false;
this.AtStartOfCourse=false;
this.DeliverThisActivity(_8d);
}else{
Debug.AssertError("Chosen activity is not deliverable.");
}
}else{
}
}
function Sequencer_RollupData(_8e){
var _8f=this.Activities.GetActivityPath(_8e);
for(var i=0;i<_8f.length;i++){
this.ActivityRollupProcess(_8f[i]);
}
}
function Sequencer_ActivityRollupProcess(_91){
var _92=_91.GetChildren();
var _93=false;
var _94=true;
var _95=true;
var _96=true;
var _97=true;
var _98=true;
var _99;
var _9a;
var _9b;
var _9c;
var _9d;
var _9e;
var _9f=0;
var _a0=null;
var _a1;
var _a2;
var _a3;
var _a4;
var _a5;
var _a6;
var _a7;
var _a8;
var _a9;
var _aa=_91.GetPrimaryObjective();
if(!this.LookAhead&&_91.IsTheRoot()){
_a2=_aa.GetProgressStatus(_91,false);
_a3=_aa.GetSatisfiedStatus(_91,false);
_a4=_91.GetAttemptProgressStatus();
_a5=_91.GetAttemptCompletionStatus();
}
_aa.SetProgressStatus(false,false,_91);
_aa.SetSatisfiedStatus(false,false,_91);
_aa.SetMeasureStatus(false,_91);
_91.SetAttemptProgressStatus(false);
_91.SetAttemptCompletionStatus(false);
var _ab;
for(var i=0;i<_92.length;i++){
_99=(_92[i].IsAttempted()===true);
_9a=_92[i].IsSatisfied();
_9b=(_9a===false&&_99===true);
_9c=_92[i].IsCompleted();
_9d=(_9c===false||_99===true);
_9e=(_9c===true&&(_9b!==true));
_93=(_93===true||_99===true);
_95=(_95===true&&(_9a===true));
_94=(_94===true&&_9b===true);
_97=(_97===true&&(_9c===true));
_96=(_96===true&&_9d===true);
_98=(_98===true&&(_9e===true));
_ab=_92[i].GetPrimaryObjective();
if(_ab.GetMeasureStatus(_92[i],false)===true){
_9f++;
if(_a0===null){
_a0=0;
}
_a0=_a0+_ab.GetNormalizedMeasure(_92[i],false);
}
}
if(!_91.IsALeaf()){
_91.RollupDurations();
}
if(_a0!==null){
switch(Control.Package.Properties.ScoreRollupMode){
case (SCORE_ROLLUP_METHOD_SCORE_PROVIDED_BY_COURSE):
_a1=_a0;
break;
case (SCORE_ROLLUP_METHOD_AVERAGE_SCORE_OF_ALL_UNITS):
_a1=(_a0/_92.length);
break;
case (SCORE_ROLLUP_METHOD_AVERAGE_SCORE_OF_ALL_UNITS_WITH_SCORES):
_a1=(_a0/_9f);
break;
case (SCORE_ROLLUP_METHOD_FIXED_AVERAGE):
if(_91.IsTheRoot()){
var _a0=0;
for(var i=0;i<Control.Activities.ActivityList.length;i++){
var rt=Control.Activities.ActivityList[i].RunTime;
if(rt!==null&&rt!==undefined&&rt.ScoreScaled!==null){
_a0+=rt.ScoreScaled;
}
}
}
_a1=(_a0/Control.Package.Properties.NumberOfScoringObjects);
break;
default:
Debug.AssertError("Invalid Score Rollup Mode Detected-"+Control.Package.Properties.ScoreRollupMode);
break;
}
_a1=RoundToPrecision(_a1,7);
_aa.SetMeasureStatus(true,_91);
_aa.SetNormalizedMeasure(_a1,_91);
}
if(_93){
_91.SetAttemptProgressStatus(true);
_91.SetActivityProgressStatus(true);
}
if(_96===true||_93){
_91.SetAttemptProgressStatus(true);
_91.SetAttemptCompletionStatus(false);
}
switch(Control.Package.Properties.StatusRollupMode){
case (STATUS_ROLLUP_METHOD_STATUS_PROVIDED_BY_COURSE):
if(_97===true){
_91.SetAttemptProgressStatus(true);
_91.SetAttemptCompletionStatus(true);
}
break;
case (STATUS_ROLLUP_METHOD_COMPLETE_WHEN_ALL_UNITS_COMPLETE):
if(_97===true||_95===true||_94===true){
_91.SetAttemptProgressStatus(true);
if(_94===true&&Control.Package.Properties.CompletionStatOfFailedSuccessStat!==SCORM_STATUS_UNKNOWN){
_91.SetAttemptCompletionStatus(_97);
}else{
_91.SetAttemptCompletionStatus(true);
}
}
break;
case (STATUS_ROLLUP_METHOD_COMPLETE_WHEN_ALL_UNITS_SATISFACTORILY_COMPLETE):
if(_98===true){
_91.SetAttemptProgressStatus(true);
_91.SetAttemptCompletionStatus(true);
}
break;
case (STATUS_ROLLUP_METHOD_COMPLETE_WHEN_THRESHOLD_SCORE_IS_MET):
if(_a1>=Control.Package.Properties.ThresholdScore){
_91.SetAttemptProgressStatus(true);
_91.SetAttemptCompletionStatus(true);
}
break;
case (STATUS_ROLLUP_METHOD_COMPLETE_WHEN_ALL_UNITS_COMPLETE_AND_THRESHOLD_SCORE_IS_MET):
if((_97===true||_95===true||_94===true)&&(_a1>=Control.Package.Properties.ThresholdScore)){
_91.SetAttemptProgressStatus(true);
if(_94===true&&Control.Package.Properties.CompletionStatOfFailedSuccessStat!==SCORM_STATUS_UNKNOWN){
_91.SetAttemptCompletionStatus(_97);
}else{
_91.SetAttemptCompletionStatus(true);
}
}
break;
default:
Debug.AssertError("Invalid Status Rollup Mode Detected-"+Control.Package.Properties.StatusRollupMode);
break;
}
if(_94===true){
_aa.SetProgressStatus(true,false,_91);
_aa.SetSatisfiedStatus(false,false,_91);
}
if(_95===true){
_aa.SetProgressStatus(true,false,_91);
_aa.SetSatisfiedStatus(true,false,_91);
}
if(!this.LookAhead&&_91.IsTheRoot()){
_a6=_aa.GetProgressStatus(_91,false);
_a7=_aa.GetSatisfiedStatus(_91,false);
_a8=_91.GetAttemptProgressStatus();
_a9=_91.GetAttemptCompletionStatus();
if(_a8!=_a4||_a9!=_a5){
var _ae=(_a8?(_a9?SCORM_STATUS_COMPLETED:SCORM_STATUS_INCOMPLETE):SCORM_STATUS_NOT_ATTEMPTED);
this.WriteHistoryLog("",{ev:"Rollup Completion",v:_ae,ai:_91.ItemIdentifier});
}
if(_a6!=_a2||_a7!=_a3){
var _af=(_a6?(_a7?SCORM_STATUS_PASSED:SCORM_STATUS_FAILED):SCORM_STATUS_UNKNOWN);
this.WriteHistoryLog("",{ev:"Rollup Satisfaction",v:_af,ai:_91.ItemIdentifier});
}
}
}
function Sequencer_IsActivityLastOverall(_b0){
if(this.Activities.SortedActivityList[this.Activities.SortedActivityList.length-1]==_b0){
return true;
}else{
return false;
}
}
function Sequencer_IsActivityFirstOverall(_b1){
var _b2=this.Activities.SortedActivityList;
for(var i=0;i<_b2.length;i++){
if(_b2[i].IsDeliverable()===true){
if(_b1==_b2[i]){
return true;
}else{
return false;
}
}
}
}
function Sequencer_EvaluatePossibleNavigationRequests(_b4){
var _b5=Control.Package.Properties;
_b4[POSSIBLE_NAVIGATION_REQUEST_INDEX_START].WillSucceed=true;
_b4[POSSIBLE_NAVIGATION_REQUEST_INDEX_RESUME_ALL].WillSucceed=true;
_b4[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed=(this.AtEndOfCourse===false&&_b5.EnableFlowNav===true);
_b4[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed=(this.AtStartOfCourse===false&&_b5.EnableFlowNav===true);
_b4[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed=true;
_b4[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].WillSucceed=true;
_b4[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL].WillSucceed=true;
_b4[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON].WillSucceed=true;
_b4[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON_ALL].WillSucceed=true;
var _b6;
for(var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;i<_b4.length;i++){
_b6=this.Activities.GetActivityFromIdentifier(_b4[i].TargetActivityItemIdentifier);
_b4[i].WillSucceed=(_b6.IsDeliverable()&&_b5.EnableChoiceNav===true);
}
return _b4;
}
function Sequencer_InitializePossibleNavigationRequestAbsolutes(_b8,_b9,_ba){
}
function Sequencer_MarkAllActivitiesComplete(){
var _bb=this.Activities.SortedActivityList;
var _bc;
for(var i=0;i<_bb.length;i++){
_bc=_bb[i].GetPrimaryObjective();
_bb[i].SetAttemptProgressStatus(true);
_bb[i].SetAttemptCompletionStatus(true);
_bc.SetProgressStatus(true,false,_bb[i]);
_bc.SetSatisfiedStatus(true,false,_bb[i]);
}
}
function Sequencer_LogSeq(str,_bf){
if(_bf===null||_bf===undefined){
Debug.AssertError("`1537`");
}
str=str+"";
if(this.LookAhead===true){
return Debug.WriteLookAheadDetailed(str,_bf);
}else{
return Debug.WriteSequencingDetailed(str,_bf);
}
}
function Sequencer_LogSeqAudit(str,_c1){
str=str+"";
if(this.LookAhead===true){
return Debug.WriteLookAheadAudit(str,_c1);
return "";
}else{
return Debug.WriteSequencingAudit(str,_c1);
}
}
function Sequencer_LogSeqReturn(str,_c3){
if(_c3===null||_c3===undefined){
Debug.AssertError("`1537`");
}
str=str+"";
if(this.LookAhead===true){
return _c3.setReturn(str);
}else{
return _c3.setReturn(str);
}
}
function Sequencer_WriteHistoryLog(str,_c5){
HistoryLog.WriteEventDetailed(str,_c5);
}
function Sequencer_WriteHistoryReturnValue(str,_c7){
HistoryLog.WriteEventDetailedReturnValue(str,_c7);
}
function Sequencer_ContentDeliveryEnvironmentActivityDataSubProcess(_c8){
var _c9=ConvertDateToIso8601String(new Date());
if(_c8.IsSuspended()===false){
_c8.IncrementAttemptCount();
_c8.SetActivityProgressStatus(true);
var _ca={ev:"AttemptStart",an:_c8.GetAttemptCount(),ai:_c8.ItemIdentifier,at:_c8.LearningObject.Title};
this.WriteHistoryLog("",_ca);
_c8.SetAttemptStartTimestampUtc(_c9);
_c8.SetAttemptAbsoluteDuration("PT0H0M0S");
_c8.SetAttemptExperiencedDurationTracked("PT0H0M0S");
_c8.SetAttemptExperiencedDurationReported("PT0H0M0S");
}
this.SuspendedActivity=null;
var _cb=this.Activities.GetActivityPath(_c8,true);
for(var i=0;i<_cb.length;i++){
if(_cb[i].GetAttemptCount()==0){
_cb[i].SetActivityProgressStatus(true);
_cb[i].IncrementAttemptCount();
var _ca={ev:"AttemptStart",an:_cb[i].GetAttemptCount(),ai:_cb[i].ItemIdentifier,at:_cb[i].LearningObject.Title};
this.WriteHistoryLog("",_ca);
_cb[i].SetAttemptStartTimestampUtc(_c9);
_cb[i].SetAttemptAbsoluteDuration("PT0H0M0S");
_cb[i].SetAttemptExperiencedDurationTracked("PT0H0M0S");
_cb[i].SetAttemptExperiencedDurationReported("PT0H0M0S");
}
}
}

