function ActivityRepository(){
this.ActivityList=new Array();
this.ActivityTree=null;
this.SortedActivityList=new Array();
this.ActivityListByIdentifier=new Array();
}
ActivityRepository.prototype.InitializeFromRegistration=ActivityRepository_InitializeFromRegistration;
ActivityRepository.prototype.BuildActivity=ActivityRepository_BuildActivity;
ActivityRepository.prototype.GetActivityByDatabaseId=ActivityRepository_GetActivityByDatabaseId;
ActivityRepository.prototype.PreOrderTraversal=ActivityRepository_PreOrderTraversal;
ActivityRepository.prototype.GetSortedIndexOfActivity=ActivityRepository_GetSortedIndexOfActivity;
ActivityRepository.prototype.Clone=ActivityRepository_Clone;
ActivityRepository.prototype.TearDown=ActivityRepository_TearDown;
ActivityRepository.prototype.SetSequencer=ActivityRepository_SetSequencer;
ActivityRepository.prototype.GetActivityPath=ActivityRepository_GetActivityPath;
ActivityRepository.prototype.GetRootActivity=ActivityRepository_GetRootActivity;
ActivityRepository.prototype.DoesActivityExist=ActivityRepository_DoesActivityExist;
ActivityRepository.prototype.GetActivityFromIdentifier=ActivityRepository_GetActivityFromIdentifier;
ActivityRepository.prototype.GetParentActivity=ActivityRepository_GetParentActivity;
ActivityRepository.prototype.GetNumDeliverableActivities=ActivityRepository_GetNumDeliverableActivities;
function ActivityRepository_InitializeFromRegistration(_1,_2){
this.ActivityList=_1.Activities;
this.ActivityTree=this.BuildActivity(_1,_1.Package.LearningObjects[0],null,1);
this.PreOrderTraversal(this.ActivityTree,this.SortedActivityList);
var _3=_2[-1];
var _4="";
for(var i=0;i<this.ActivityList.length;i++){
_4="";
var _6=_2[this.ActivityList[i].DatabaseId];
if(_6!==null&&_6!==undefined){
_4=_6;
}else{
if(_3!==null&&_3!==undefined){
_4=_3;
}
}
if(_4!==""){
this.ActivityList[i].LearningObject.Parameters=MergeQueryStringParameters(_4,this.ActivityList[i].LearningObject.Parameters);
}
this.ActivityListByIdentifier[this.ActivityList[i].GetItemIdentifier()]=this.ActivityList[i];
}
}
function ActivityRepository_BuildActivity(_7,_8,_9,_a){
var _b=_7.FindActivityForThisScormObject(_8.DatabaseIdentifier);
_b.LearningObject=_8;
_b.ParentActivity=_9;
if(_b.Ordinal==0){
_b.Ordinal=_a;
}
var _c;
for(var _d in _b.ActivityObjectives){
_c=null;
for(var _e in _8.SequencingData.Objectives){
if(_8.SequencingData.Objectives[_e].Id==_b.ActivityObjectives[_d].Identifier){
_c=_8.SequencingData.Objectives[_e];
break;
}
}
if(_c===null){
if(_8.SequencingData.PrimaryObjective!==null&&_8.SequencingData.PrimaryObjective.Id==_b.ActivityObjectives[_d].Identifier){
_c=_8.SequencingData.PrimaryObjective;
}
}
if(_c!==null){
_b.ActivityObjectives[_d].SatisfiedByMeasure=_c.SatisfiedByMeasure;
_b.ActivityObjectives[_d].MinNormalizedMeasure=_c.MinNormalizedMeasure;
_b.ActivityObjectives[_d].Maps=_c.Maps;
}
identifier=_b.ActivityObjectives[_d].GetIdentifier();
if(_b.RunTime!==null&&identifier!==null&&identifier!==undefined&&identifier.length>0){
runTimeObjective=_b.RunTime.FindObjectiveWithId(identifier);
if(runTimeObjective===null){
_b.RunTime.AddObjective();
runTimeObjective=_b.RunTime.Objectives[_b.RunTime.Objectives.length-1];
runTimeObjective.Identifier=identifier;
}
}
}
for(var i=0;i<_8.Children.length;i++){
_b.ChildActivities[i]=this.BuildActivity(_7,_8.Children[i],_b,(i+1));
}
_b.StringIdentifier=_b.toString();
return _b;
}
function ActivityRepository_GetActivityByDatabaseId(_10){
if(_10===null){
return null;
}else{
for(var i=0;i<this.ActivityList.length;i++){
if(this.ActivityList[i].DatabaseId==_10){
return this.ActivityList[i];
}
}
}
Debug.AssertError("Searched for an activity by database id that did not exist.");
return null;
}
function ActivityRepository_PreOrderTraversal(_12,_13){
_13[_13.length]=_12;
for(var i=0;i<_12.ChildActivities.length;i++){
this.PreOrderTraversal(_12.ChildActivities[i],_13);
}
}
function ActivityRepository_GetSortedIndexOfActivity(_15){
for(var i=0;i<this.SortedActivityList.length;i++){
if(this.SortedActivityList[i]==_15){
return i;
}
}
Debug.AssertError("Activity not found in sorted list of activities");
return null;
}
function ActivityRepository_Clone(){
var _17=new ActivityRepository();
var _18;
var _19;
var _1a;
for(_1a=0;_1a<this.ActivityList.length;_1a++){
_18=this.ActivityList[_1a].Clone();
_17.ActivityList[_1a]=_18;
_17.ActivityListByIdentifier[_18.GetItemIdentifier()]=_18;
if(this.ActivityList[_1a].ParentActivity===null){
_17.ActivityTree=_17.ActivityList[_1a];
}
}
for(_1a=0;_1a<this.ActivityList.length;_1a++){
_19=this.ActivityList[_1a];
_18=_17.ActivityList[_1a];
if(_19.ParentActivity===null){
_18.ParentActivity=null;
}else{
_18.ParentActivity=_17.GetActivityFromIdentifier(_19.ParentActivity.GetItemIdentifier());
}
for(var _1b in _19.ChildActivities){
_18.ChildActivities[_1b]=_17.GetActivityFromIdentifier(_19.ChildActivities[_1b].GetItemIdentifier());
}
_18.AvailableChildren=new Array();
for(var _1c in _19.AvailableChildren){
_18.AvailableChildren[_1c]=_17.GetActivityFromIdentifier(_19.AvailableChildren[_1c].GetItemIdentifier());
}
}
_17.PreOrderTraversal(_17.ActivityTree,_17.SortedActivityList);
return _17;
}
function ActivityRepository_TearDown(){
for(var _1d in this.ActivityList){
this.ActivityList[_1d].TearDown();
this.ActivityList[_1d]=null;
}
this.ActivityList=null;
this.ActivityTree=null;
this.SortedActivityList=null;
}
function ActivityRepository_SetSequencer(_1e,_1f){
for(var _20 in this.ActivityList){
this.ActivityList[_20].SetSequencer(_1e,_1f);
}
}
function ActivityRepository_GetActivityPath(_21,_22){
var _23=new Array();
var _24=0;
if(_22){
_23[_24]=_21;
_24++;
}
while(_21.ParentActivity!==null){
_21=_21.ParentActivity;
_23[_24]=_21;
_24++;
}
return _23;
}
function ActivityRepository_GetRootActivity(){
var _25=null;
for(var i=0;i<this.SortedActivityList.length;i++){
if(this.SortedActivityList[i].ParentActivity===null){
_25=this.SortedActivityList[i];
break;
}
}
return _25;
}
function ActivityRepository_DoesActivityExist(_27){
if(this.ActivityListByIdentifier[_27]!==null&&this.ActivityListByIdentifier[_27]!==undefined){
return true;
}
return false;
}
function ActivityRepository_GetActivityFromIdentifier(_28){
if(this.ActivityListByIdentifier[_28]!==null&&this.ActivityListByIdentifier[_28]!==undefined){
return this.ActivityListByIdentifier[_28];
}
return null;
}
function ActivityRepository_GetParentActivity(_29){
var _2a=null;
if(_29!==null){
_2a=_29.ParentActivity;
}
return _2a;
}
function ActivityRepository_GetNumDeliverableActivities(){
var _2b=0;
for(var i=0;i<this.ActivityList.length;i++){
if(this.ActivityList[i].IsDeliverable()){
_2b++;
}
}
return _2b;
}
function Communications(){
this.IntervalFunctionID="";
this.FinalExitCalls=0;
this.FailedSubmissions=0;
this.Disabled=false;
this.StartPostDataProcess=Communications_StartPostDataProcess;
this.KillPostDataProcess=Communications_KillPostDataProcess;
this.SaveData=Communications_SaveData;
this.SaveDataNow=Communications_SaveDataNow;
this.SaveDataOnExit=Communications_SaveDataOnExit;
this.SaveDebugLog=Communications_SaveDebugLog;
this.SendDataToServer=Communications_SendDataToServer;
this.CheckServerResponse=Communications_CheckServerResponse;
this.CallFailed=Communications_CallFailed;
this.Disable=Communications_Disable;
}
function Communications_StartPostDataProcess(){
if(this.Disabled){
return;
}
Control.WriteDetailedLog("`1313`");
this.IntervalFunctionID=window.setInterval("Control.Comm.SaveData(false, false);",RegistrationToDeliver.Package.Properties.CommCommitFrequency);
}
function Communications_KillPostDataProcess(){
if(this.Disabled){
return;
}
Control.WriteDetailedLog("`1336`");
if(this.IntervalFunctionID!==""){
window.clearInterval(this.IntervalFunctionID);
this.IntervalFunctionID="";
}
}
function Communications_SaveData(_2d,_2e){
if(this.Disabled){
return;
}
Control.WriteDetailedLog("`1267`"+_2d+"`1720`"+_2e);
if(_2e===true){
this.FinalExitCalls++;
if(this.FinalExitCalls==1||Control.IsThereDirtyData()){
Control.WriteDetailedLog("`873`"+this.FinalExitCalls);
Control.MarkDirtyDataPosted();
var _2f=Control.GetXmlForDirtyData();
this.SendDataToServer(_2d,_2f,true);
}
}else{
if(Control.IsThereDirtyData()){
Control.WriteDetailedLog("`1511`");
Control.MarkDirtyDataPosted();
var _2f=Control.GetXmlForDirtyData();
this.SendDataToServer(_2d,_2f);
}
}
}
function Communications_SaveDataNow(_30){
if(this.Disabled){
return;
}
Control.WriteDetailedLog("`1469`");
this.KillPostDataProcess();
this.SaveData(true);
if(!_30){
this.StartPostDataProcess();
}
}
function Communications_SaveDataOnExit(){
if(this.Disabled){
return;
}
Control.WriteDetailedLog("`1404`");
this.KillPostDataProcess();
this.SaveData(true,true);
}
function Communications_SaveDebugLog(){
if(Debug.log.root&&Debug.log.root.childNodes.length>0){
var _31=Debug.log.toXml();
$.ajax({url:DEBUG_LOG_PERSIST_PAGE,type:"POST",data:_31,async:false});
}
}
function Communications_SendDataToServer(_32,_33,_34){
if(this.Disabled){
return true;
}
if(_34){
var _35=MergeQueryStringParameters(SCORM_RESULTS_PAGE,"isExitScormPlayer=true");
}else{
var _35=SCORM_RESULTS_PAGE;
}
_32=!!_32;
Control.WriteDetailedLog("`1350`"+_32+"`1692`"+_33);
$.ajax({type:"POST",url:_35,cache:false,dataType:"text",contentType:"text/xml",data:_33,async:!_32,success:function(_36,_37){
Control.WriteDetailedLog("`1548`"+_36);
return Control.Comm.CheckServerResponse(_36,true);
},error:function(req,_39,_3a){
Control.WriteDetailedLog("`565`"+req.status);
var _3b=Control.Comm.CallFailed();
if(_3b){
Control.Comm.SaveData(true);
}
}});
return true;
}
function Communications_CheckServerResponse(_3c,_3d){
if(this.Disabled){
return true;
}
var _3e;
_3c=String(_3c);
var _3f=/\<error present\=\"(true|false)\"\>/;
var _40=_3c.match(_3f);
if(_40===null||_40.length!=2){
Control.WriteDetailedLogError("`964`");
_3e=false;
}else{
var _41=(_40[1]=="false");
if(_41===false){
Control.WriteDetailedLogError("`374`"+_3c);
_3e=false;
}else{
_3e=true;
}
}
if(_3e===false){
var _42=this.CallFailed();
if(_3d&&_42){
this.SaveData(true);
}
}else{
this.FailedSubmissions=0;
Control.MarkPostedDataClean();
}
return _3e;
}
function Communications_CallFailed(){
if(this.Disabled){
return false;
}
this.FailedSubmissions++;
Control.MarkPostedDataDirty();
Control.WriteDetailedLog("`1088`"+this.FailedSubmissions);
if(this.FailedSubmissions>=RegistrationToDeliver.Package.Properties.CommMaxFailedSubmissions){
this.KillPostDataProcess();
Control.DisplayError("A fatal error has occurred, communication with the server has been lost.");
return false;
}
return true;
}
function Communications_Disable(){
this.Disabled=true;
}
var DATA_STATE_CLEAN="C";
var DATA_STATE_DIRTY="D";
var DATA_STATE_POSTED="P";
var ASCII_QUESTION=63;
var ASCII_TILDA=126;
var ASCII_BANG=33;
var ASCII_PIPE=124;
var ASCII_SHIFT_IN=15;
var ASCII_0=48;
var ASCII_1=49;
var ASCII_2=50;
var ASCII_3=51;
var ASCII_4=52;
var ASCII_5=53;
var ASCII_6=54;
var ASCII_7=55;
var ASCII_8=56;
var ASCII_D=68;
var RESULT_UNKNOWN="unknown";
var EXIT_ACTION_EXIT_NO_CONFIRMATION="exit,no confirmation";
var EXIT_ACTION_EXIT_CONFIRMATION="exit,confirmation";
var EXIT_ACTION_GO_TO_NEXT_SCO="continue";
var EXIT_ACTION_DISPLAY_MESSAGE="message page";
var EXIT_ACTION_DO_NOTHING="do nothing";
var EXIT_ACTION_REFRESH_PAGE="refresh page";
var POSSIBLE_NAVIGATION_REQUEST_INDEX_START=0;
var POSSIBLE_NAVIGATION_REQUEST_INDEX_RESUME_ALL=1;
var POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE=2;
var POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS=3;
var POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT=4;
var POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL=5;
var POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL=6;
var POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON=7;
var POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON_ALL=8;
var POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE=9;
var LAUNCH_TYPE_FRAMESET="frameset";
var LAUNCH_TYPE_POPUP="new window";
var LAUNCH_TYPE_POPUP_AFTER_CLICK="new window,after click";
var LAUNCH_TYPE_POPUP_WITHOUT_BROWSER_TOOLBAR="new window without browser toolbar";
var LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR="new window,after click,without browser toolbar";
var LAUNCH_TYPE_POPUP_WITH_MENU="new window with menubar";
var STANDARD_SCORM_11="SCORM 1.1";
var STANDARD_SCORM_12="SCORM 1.2";
var STANDARD_SCORM_2004_2ND_EDITION="SCORM 2004 2nd Edition";
var STANDARD_SCORM_2004_3RD_EDITION="SCORM 2004 3rd Edition";
var STANDARD_SCORM_2004_4TH_EDITION="SCORM 2004 4th Edition";
var STANDARD_AICC="AICC";
var SCORM_2004_2ND_EDITION="SCORM 2004 2nd Edition";
var STANDARD_SCORM_2004="SCORM 2004 3rd Edition";
var SCORM_TYPE_SCO="SCO";
var SCORM_TYPE_ASSET="Asset";
var SCORM_TYPE_OBJECTIVE="Objective";
var SCORM_TYPE_AGGREGATION="Aggregation";
var STATUS_DISPLAY_SUCCESS_ONLY="success only";
var STATUS_DISPLAY_COMPELTION_ONLY="completion only";
var STATUS_DISPLAY_SEPERATE="separate";
var STATUS_DISPLAY_COMBINED="combined";
var STATUS_DISPLAY_NONE="none";
var STATUS_ROLLUP_METHOD_STATUS_PROVIDED_BY_COURSE="STATUS_PROVIDED_BY_COURSE";
var STATUS_ROLLUP_METHOD_COMPLETE_WHEN_ALL_UNITS_COMPLETE="COMPLETE_WHEN_ALL_UNITS_COMPLETE";
var STATUS_ROLLUP_METHOD_COMPLETE_WHEN_ALL_UNITS_SATISFACTORILY_COMPLETE="COMPLETE_WHEN_ALL_UNITS_SATISFACTORILY_COMPLETE";
var STATUS_ROLLUP_METHOD_COMPLETE_WHEN_THRESHOLD_SCORE_IS_MET="COMPLETE_WHEN_THRESHOLD_SCORE_IS_MET";
var STATUS_ROLLUP_METHOD_COMPLETE_WHEN_ALL_UNITS_COMPLETE_AND_THRESHOLD_SCORE_IS_MET="COMPLETE_WHEN_ALL_UNITS_COMPLETE_AND_THRESHOLD_SCORE_IS_MET";
var SCORE_ROLLUP_METHOD_SCORE_PROVIDED_BY_COURSE="SCORE_PROVIDED_BY_COURSE";
var SCORE_ROLLUP_METHOD_AVERAGE_SCORE_OF_ALL_UNITS="AVERAGE_SCORE_OF_ALL_UNITS";
var SCORE_ROLLUP_METHOD_AVERAGE_SCORE_OF_ALL_UNITS_WITH_SCORES="AVERAGE_SCORE_OF_ALL_UNITS_WITH_SCORES";
var SCORE_ROLLUP_METHOD_FIXED_AVERAGE="FIXED_AVERAGE";
var RESET_RT_DATA_TIMING_NEVER="never";
var RESET_RT_DATA_TIMING_WHEN_EXIT_IS_NOT_SUSPEND="when exit is not suspend";
var RESET_RT_DATA_TIMING_ON_EACH_NEW_SEQUENCING_ATTEMPT="on each new sequencing attempt";
var INVALID_MENU_ITEM_ACTION_DISABLE="Disable";
var INVALID_MENU_ITEM_ACTION_HIDE="Hide";
var INVALID_MENU_ITEM_ACTION_SHOW_ENABLE="Show";
var LOOKAHEAD_SEQUENCER_MODE_DISABLE="DISABLED";
var LOOKAHEAD_SEQUENCER_MODE_ENABLE="ENABLED";
function Controller(){
this.ProcessedUnload=false;
this.MenuIsVisible=false;
this.Initialized=false;
this.ExitScormPlayerCalled=false;
this.PopupBlocked=false;
this.ExitDialogVisible=false;
this.Initialize=Controller_Initialize;
this.Unload=Controller_Unload;
this.CreateMenuItem=Controller_CreateMenuItem;
this.RenderMenuItem=Controller_RenderMenuItem;
this.RedrawChildren=Controller_RedrawChildren;
this.UpdateDisplay=Controller_UpdateDisplay;
this.RefreshPage=Controller_RefreshPage;
this.Activities=null;
this.ScoLoader=null;
this.ScoUnloaded=Controller_ScoUnloaded;
this.ExitScormPlayer=Controller_ExitScormPlayer;
this.ExitSco=Controller_ExitSco;
this.MarkPostedDataDirty=Controller_MarkPostedDataDirty;
this.MarkPostedDataClean=Controller_MarkPostedDataClean;
this.MarkDirtyDataPosted=Controller_MarkDirtyDataPosted;
this.GetXmlForDirtyData=Controller_GetXmlForDirtyData;
this.IsThereDirtyData=Controller_IsThereDirtyData;
this.DisplayError=Controller_DisplayError;
this.GetExceptionText=Controller_GetExceptionText;
this.TildaCounter=0;
this.QuestionCounter=0;
this.PipeCounter=0;
this.CheckForDebugCommand=Controller_CheckForDebugCommand;
this.CloseSco=Controller_CloseSco;
this.ReturnToLms=Controller_ReturnToLms;
this.GetReturnToLmsNavigationRequest=Controller_GetReturnToLmsNavigationRequest;
this.ToggleMenuVisibility=Controller_ToggleMenuVisibility;
this.TriggerReturnToLMS=Controller_TriggerReturnToLMS;
this.HideExitDialog=Controller_HideExitDialog;
this.Next=Controller_Next;
this.Previous=Controller_Previous;
this.Abandon=Controller_Abandon;
this.AbandonAll=Controller_AbandonAll;
this.Suspend=Controller_Suspend;
this.Exit=Controller_Exit;
this.ExitAll=Controller_ExitAll;
this.ChoiceRequest=Controller_ChoiceRequest;
this.ScoHasTerminatedSoUnload=Controller_ScoHasTerminatedSoUnload;
this.SignalTerminated=Controller_SignalTerminated;
this.TranslateRunTimeNavRequest=Controller_TranslateRunTimeNavRequest;
this.FindPossibleNavRequestForRuntimeNavRequest=Controller_FindPossibleNavRequestForRuntimeNavRequest;
this.GetMessageText=Controller_GetMessageText;
this.ClearPendingNavigationRequest=Controller_ClearPendingNavigationRequest;
this.IsThereAPendingNavigationRequest=Controller_IsThereAPendingNavigationRequest;
this.PendingNavigationRequest=null;
this.GetPreferredReturnToLmsAction=Controller_GetPreferredReturnToLmsAction;
this.UpdateGlobalLearnerPrefs=Controller_UpdateGlobalLearnerPrefs;
this.Sequencer=null;
this.LookAheadSequencer=null;
this.DeliverActivity=Controller_DeliverActivity;
this.PerformDelayedDeliveryInitialization=Controller_PerformDelayedDeliveryInitialization;
this.PossibleNavigationRequests=new Array();
this.InitializePossibleNavigationRequests=Controller_InitializePossibleNavigationRequests;
this.EvaluatePossibleNavigationRequests=Controller_EvaluatePossibleNavigationRequests;
this.FindPossibleChoiceRequestForActivity=Controller_FindPossibleChoiceRequestForActivity;
this.GetPossibleContinueRequest=Controller_GetPossibleContinueRequest;
this.GetPossiblePreviousRequest=Controller_GetPossiblePreviousRequest;
this.IsTargetValid=Controller_IsTargetValid;
this.ParseTargetStringIntoActivity=Controller_ParseTargetStringIntoActivity;
this.IsChoiceRequestValid=Controller_IsChoiceRequestValid;
this.IsJumpRequestValid=Controller_IsJumpRequestValid;
this.IsContinueRequestValid=Controller_IsContinueRequestValid;
this.IsPreviousRequestValid=Controller_IsPreviousRequestValid;
this.ParseTargetStringFromChoiceRequest=Controller_ParseTargetStringFromChoiceRequest;
this.CloneSequencer=Controller_CloneSequencer;
this.TearDownSequencer=Controller_TearDownSequencer;
this.Api=null;
this.WriteAuditLog=Controller_WriteAuditLog;
this.WriteDetailedLog=Controller_WriteDetailedLog;
this.WriteDetailedLogError=Controller_WriteDetailedLogError;
this.WriteHistoryLog=Controller_WriteHistoryLog;
this.WriteHistoryReturnValue=Controller_WriteHistoryReturnValue;
this.GetLaunchHistoryId=Controller_GetLaunchHistoryId;
this.SSPBuckets=null;
}
function Controller_Initialize(){
this.WriteAuditLog("`1632`");
this.Api=apiReference;
this.Package=RegistrationToDeliver.Package;
this.Activities=new ActivityRepository();
this.Activities.InitializeFromRegistration(RegistrationToDeliver,QuerystringAdditions);
this.Sequencer=new Sequencer(false,this.Activities);
this.Sequencer.GlobalObjectives=RegistrationToDeliver.GlobalObjectives;
this.Sequencer.SharedData=RegistrationToDeliver.SharedData;
this.Sequencer.Activities.SetSequencer(this.Sequencer,false);
this.SSPBuckets=RegistrationToDeliver.SSPBuckets;
this.SharedData=RegistrationToDeliver.SharedData;
if(SSP_ENABLED&&this.Api.SSPApi!=null){
this.Api.SSPApi.InitializeBuckets();
}
this.InitializePossibleNavigationRequests();
var _43=this.Activities.GetActivityByDatabaseId(RegistrationToDeliver.SuspendedActivity);
this.Sequencer.SetSuspendedActivity(_43);
this.Sequencer.InitialRandomizationAndSelection();
this.CreateMenuItem(null,this.Activities.ActivityTree,IntegrationImplementation.GetDocumentObjectForMenu());
this.RenderMenuItem(this.Activities.ActivityTree);
IntegrationImplementation.SetMenuToggleVisibility(this.Package.Properties.ShowCourseStructure);
if(this.Package.Properties.ShowCourseStructure===true&&this.Package.Properties.CourseStructureStartsOpen===true){
this.ToggleMenuVisibility();
}else{
IntegrationImplementation.HideMenu();
}
this.LookAheadSequencer=new Sequencer(true,this.Sequencer.Activities.Clone());
this.Comm=new Communications();
if(this.Package.LearningStandard.isAICC()||RegistrationToDeliver.TrackingEnabled===false){
this.Comm.Disable();
}
this.Comm.StartPostDataProcess();
var _44=window.location.toString();
IntermediatePage.PageHref=BuildFullUrl(IntermediatePage.PageHref,_44);
PopupLauncherPage.PageHref=BuildFullUrl(PopupLauncherPage.PageHref,_44);
this.ScoLoader=new ScoLoader(IntermediatePage,PopupLauncherPage,PathToCourse,RegistrationToDeliver.Package.Properties.ScoLaunchType,RegistrationToDeliver.Package.Properties.WrapScoWindowWithApi,RegistrationToDeliver.Package.LearningStandard);
this.Initialized=true;
this.Sequencer.Start();
this.EvaluatePossibleNavigationRequests();
}
function Controller_Unload(){
this.WriteAuditLog("`1693`");
this.WriteHistoryLog("",{ev:"UnloadSco"});
if(this.ProcessedUnload===false){
this.ProcessedUnload=true;
if(this.ExitScormPlayerCalled===false){
this.ExitSco();
this.ScoUnloaded();
this.Comm.SaveDataOnExit();
if(SHOULD_SAVE_CLIENT_DEBUG_LOGS&&RegistrationToDeliver.TrackingEnabled){
this.Comm.SaveDebugLog(true);
}
if(this.Package.Properties.PlayerLaunchType!=LAUNCH_TYPE_FRAMESET&&!this.PopupBlocked){
try{
if(window.opener&&window.opener!==null&&window.opener.closed===false){
window.opener.location=RedirectOnExitUrl;
}
}
catch(e){
}
window.close();
}
}
}
}
function Controller_CreateMenuItem(_45,_46,_47){
_46.MenuItem=new MenuItem(_45,_46,_47);
if(_45!==null){
_45.Children[_45.Children.length]=_46.MenuItem;
}
var _48=_46.GetAvailableChildren();
for(var _49 in _48){
this.CreateMenuItem(_46.MenuItem,_48[_49],_47);
}
}
function Controller_RenderMenuItem(_4a){
_4a.MenuItem.Render(_4a);
var _4b=_4a.GetAvailableChildren();
for(var _4c in _4b){
this.RenderMenuItem(_4b[_4c]);
}
}
function Controller_RedrawChildren(_4d){
_4d.MenuItem.ResynchChildren(_4d);
this.RenderMenuItem(_4d);
}
function Controller_UpdateDisplay(_4e,_4f){
var _50=this.WriteAuditLog("`1550`");
if(this.Package.Properties.LookaheadSequencerMode!==LOOKAHEAD_SEQUENCER_MODE_ENABLE){
_4e=false;
_4f=false;
}else{
if(_4e===undefined||_4e===null){
_4e=false;
}
if(_4f===undefined||_4f===null){
_4f=false;
}
}
var _51=0;
var _52;
this.WriteDetailedLog("`1310`",_50);
for(var _53 in this.Sequencer.Activities.ActivityList){
if(_4f){
var _54=this.LookAheadSequencer.Activities.ActivityList[_53];
}else{
var _54=this.Sequencer.Activities.ActivityList[_53];
}
if(_4e){
_52=this.FindPossibleChoiceRequestForActivity(this.LookAheadSequencer.Activities.ActivityList[_53]);
}else{
_52=this.FindPossibleChoiceRequestForActivity(this.Sequencer.Activities.ActivityList[_53]);
}
_54.SetHiddenFromChoice(_52.Hidden);
var _55=this.Sequencer.Activities.ActivityList[_53].MenuItem;
if(_55!==null){
_55.UpdateStateDisplay(_54,this.Sequencer.CurrentActivity,_52,_4f);
if(_55.Visible){
_51++;
}
}
}
if(_51>0&&this.Sequencer.CurrentActivity!=null){
var _56=this.Sequencer.CurrentActivity.MenuItem;
if(_56.Visible===false&&_56.CurrentDisplayState.ActiveDisplayed===true){
var _57=this.Sequencer.CurrentActivity.ParentActivity;
var _58=false;
while(_57!=null&&_58==false){
if(_57.MenuItem.Visible===true){
if(_4e){
_52=this.FindPossibleChoiceRequestForActivity(this.LookAheadSequencer.Activities.ActivityList[_53]);
}else{
_52=this.FindPossibleChoiceRequestForActivity(this.Sequencer.Activities.ActivityList[_53]);
}
_57.MenuItem.UpdateStateDisplay(_57,_57,_52,_4f);
_58=true;
}else{
_57=_57.ParentActivity;
}
}
}
}
this.WriteDetailedLog("`969`",_50);
IntegrationImplementation.UpdateControlState(IntegrationImplementation.GetDocumentObjectForControls(),this.PossibleNavigationRequests,this.Sequencer.GetCurrentActivity());
if(_51==0){
if(this.MenuIsVisible===true){
this.ToggleMenuVisibility();
}
}else{
if(this.Package.Properties.CourseStructureStartsOpen===true&&this.MenuIsVisible===false){
this.ToggleMenuVisibility();
}
}
this.WriteDetailedLog("`1389`",_50);
}
function Controller_RefreshPage(){
var _59=500;
var _5a=0;
var win=window;
while((win.Control===null)&&(win.parent!==null)&&(win.parent!=win)&&(_5a<=_59)){
_5a++;
win=win.parent;
}
if(win.Control===null){
Debug.AssertError("Could not locate the top level window.");
}else{
win.location.replace(win.location);
}
}
function Controller_ScoUnloaded(){
var _5c=this.WriteAuditLog("`1594`");
this.WriteHistoryLog("",{ev:"UnloadSco"});
if(this.Initialized===false){
return;
}
if(this.Api.Activity!==null&&this.Api.NeedToCloseOutSession()===false){
this.Api.CloseOutSession();
}
if((this.Api.Activity!=null)&&(this.Api.Activity.LearningObject.ScormType===SCORM_TYPE_ASSET)&&this.Api.Activity.WasLaunchedThisSession()){
this.Api.AccumulateTotalTimeTracked();
this.WriteDetailedLog("`1319`"+this.Api.RunTimeData.TotalTimeTracked);
var _5d=this.Sequencer.Activities.GetActivityPath(this.Api.Activity,false);
for(var i=0;i<_5d.length;i++){
_5d[i].RollupDurations();
}
}
if(this.PendingNavigationRequest===null){
if((this.Api!==null)&&(this.Api.RunTimeData!=null)&&(this.Api.RunTimeData.NavRequest!=SCORM_RUNTIME_NAV_REQUEST_NONE)){
this.WriteDetailedLog("`1308`"+this.Api.RunTimeData.NavRequest,_5c);
this.PendingNavigationRequest=this.TranslateRunTimeNavRequest(this.Api.RunTimeData.NavRequest);
}
}
this.Sequencer.NavigationRequest=this.PendingNavigationRequest;
this.ClearPendingNavigationRequest();
this.Sequencer.OverallSequencingProcess();
if(this.ExitScormPlayerCalled===false){
this.EvaluatePossibleNavigationRequests();
}
}
function Controller_ExitScormPlayer(){
this.ExitScormPlayerCalled=true;
if(Debug.ShowDebugLogAtExit){
if(Debug.DataIsAvailable()){
Debug.ShowAllAvailableData();
}
}
if(this.ProcessedUnload===false){
this.Comm.SaveDataOnExit();
if(SHOULD_SAVE_CLIENT_DEBUG_LOGS&&RegistrationToDeliver.TrackingEnabled){
this.Comm.SaveDebugLog(true);
}
if(this.Package.Properties.PlayerLaunchType==LAUNCH_TYPE_FRAMESET){
window.location=RedirectOnExitUrl;
}else{
window.opener.location=RedirectOnExitUrl;
window.close();
}
}else{
if(SHOULD_SAVE_CLIENT_DEBUG_LOGS&&RegistrationToDeliver.TrackingEnabled){
this.Comm.SaveDebugLog(true);
}
}
}
function Controller_ExitSco(){
this.ScoLoader.UnloadSco();
}
function Controller_MarkPostedDataDirty(){
this.WriteAuditLog("`1443`");
for(var _5f in this.Activities.ActivityList){
if(this.Activities.ActivityList[_5f].DataState==DATA_STATE_POSTED){
this.Activities.ActivityList[_5f].DataState=DATA_STATE_DIRTY;
}
this.Activities.ActivityList[_5f].MarkPostedObjectiveDataDirty();
}
if(this.Sequencer.GlobalObjectives!==null&&this.Sequencer.GlobalObjectives!==undefined){
for(var _60 in this.Sequencer.GlobalObjectives){
dataState=this.Sequencer.GlobalObjectives[_60].DataState;
if(dataState==DATA_STATE_POSTED){
this.Sequencer.GlobalObjectives[_60].DataState=DATA_STATE_DIRTY;
}
}
}
for(var _61 in this.SSPBuckets){
if(this.SSPBuckets[_61].DataState==DATA_STATE_POSTED){
this.SSPBuckets[_61].DataState=DATA_STATE_DIRTY;
}
}
}
function Controller_MarkPostedDataClean(){
this.WriteAuditLog("`1456`");
for(var _62 in this.Activities.ActivityList){
if(this.Activities.ActivityList[_62].DataState==DATA_STATE_POSTED){
this.Activities.ActivityList[_62].DataState=DATA_STATE_CLEAN;
}
this.Activities.ActivityList[_62].MarkPostedObjectiveDataClean();
}
if(this.Sequencer.GlobalObjectives!==null&&this.Sequencer.GlobalObjectives!==undefined){
for(var _63 in this.Sequencer.GlobalObjectives){
dataState=this.Sequencer.GlobalObjectives[_63].DataState;
if(dataState==DATA_STATE_POSTED){
this.Sequencer.GlobalObjectives[_63].DataState=DATA_STATE_CLEAN;
}
}
}
for(var _64 in this.SSPBuckets){
if(this.SSPBuckets[_64].DataState==DATA_STATE_POSTED){
this.SSPBuckets[_64].DataState=DATA_STATE_CLEAN;
}
}
}
function Controller_MarkDirtyDataPosted(){
this.WriteAuditLog("`1442`");
for(var _65 in this.Activities.ActivityList){
if(this.Activities.ActivityList[_65].IsAnythingDirty()){
this.Activities.ActivityList[_65].DataState=DATA_STATE_POSTED;
}
this.Activities.ActivityList[_65].MarkDirtyObjectiveDataPosted();
}
if(this.Sequencer.GlobalObjectives!==null&&this.Sequencer.GlobalObjectives!==undefined){
for(var _66 in this.Sequencer.GlobalObjectives){
dataState=this.Sequencer.GlobalObjectives[_66].DataState;
if(dataState==DATA_STATE_DIRTY){
this.Sequencer.GlobalObjectives[_66].DataState=DATA_STATE_POSTED;
}
}
}
for(var _67 in this.SSPBuckets){
if(this.SSPBuckets[_67].DataState==DATA_STATE_DIRTY){
this.SSPBuckets[_67].DataState=DATA_STATE_POSTED;
}
}
}
function Controller_GetXmlForDirtyData(){
this.WriteAuditLog("`1471`");
var _68=new ServerFormater();
var xml=new XmlElement("RTD");
xml.AddAttribute("RI",RegistrationToDeliver.Id);
if(this.Sequencer.GetSuspendedActivity()!==null){
xml.AddAttribute("SAI",this.Sequencer.GetSuspendedActivity().GetDatabaseIdentifier());
}
if(this.GetLaunchHistoryId()!==null){
xml.AddAttribute("LH",this.GetLaunchHistoryId());
}
for(var _6a in this.Activities.ActivityList){
if(this.Activities.ActivityList[_6a].IsAnythingDirty()||this.Activities.ActivityList[_6a].DataState==DATA_STATE_POSTED){
xml.AddElement(this.Activities.ActivityList[_6a].GetXml());
}
}
if(this.Sequencer.GlobalObjectives!==null&&this.Sequencer.GlobalObjectives!==undefined){
for(var _6b in this.Sequencer.GlobalObjectives){
dataState=this.Sequencer.GlobalObjectives[_6b].DataState;
if(dataState==DATA_STATE_DIRTY||dataState==DATA_STATE_POSTED){
xml.AddElement(this.Sequencer.GlobalObjectives[_6b].GetXml(RegistrationToDeliver.Id,_6b));
}
}
}
for(var _6c in this.SSPBuckets){
if(this.SSPBuckets[_6c].DataState==DATA_STATE_DIRTY||this.SSPBuckets[_6c].DataState==DATA_STATE_POSTED){
xml.AddElement(this.SSPBuckets[_6c].GetXml());
}
}
for(var _6d in this.SharedData){
if(this.SharedData[_6d].DataState==DATA_STATE_DIRTY||this.SharedData[_6d].DataState==DATA_STATE_POSTED){
xml.AddElement(this.SharedData[_6d].GetXml());
}
}
var _6e=HistoryLog.log.dom.getElementsByTagName("RTL")[0];
xml.AddElement((new XMLSerializer()).serializeToString(_6e));
var _6f=this.Activities.GetRootActivity();
var _70=new XmlElement("RS");
if(_6f.GetPrimaryObjective().ProgressStatus){
var _71=_6f.GetPrimaryObjective().SatisfiedStatus?"passed":"failed";
}else{
var _71="unknown";
}
_70.AddAttribute("SS",_71);
if(_6f.AttemptProgressStatus){
var _72=_6f.AttemptCompletionStatus?"complete":"incomplete";
}else{
var _72="unknown";
}
_70.AddAttribute("CS",_72);
_70.AddAttribute("MS",_68.ConvertBoolean(_6f.GetPrimaryObjective().MeasureStatus));
_70.AddAttribute("NM",_6f.GetPrimaryObjective().NormalizedMeasure);
_70.AddAttribute("ED",_68.ConvertTimeSpan(_6f.ActivityExperiencedDurationTracked));
xml.AddElement(_70);
return "<?xml version=\"1.0\"?>"+xml.toString();
}
function Controller_IsThereDirtyData(){
this.WriteAuditLog("`1507`");
for(var _73 in this.Activities.ActivityList){
if(this.Activities.ActivityList[_73].IsAnythingDirty()||this.Activities.ActivityList[_73].DataState==DATA_STATE_POSTED){
return true;
}
}
if(this.Sequencer.GlobalObjectives!==null&&this.Sequencer.GlobalObjectives!==undefined){
for(var _74 in this.Sequencer.GlobalObjectives){
dataState=this.Sequencer.GlobalObjectives[_74].DataState;
if(dataState==DATA_STATE_DIRTY||dataState==DATA_STATE_POSTED){
return true;
}
}
}
for(var _75 in this.SSPBuckets){
if(this.SSPBuckets[_75].DataState==DATA_STATE_DIRTY||this.SSPBuckets[_75].DataState==DATA_STATE_POSTED){
return true;
}
}
return false;
}
function Controller_DisplayError(_76){
this.WriteAuditLog("`1521`"+_76);
if(Debug.DataIsAvailable()){
if(confirm(_76+"\n\nPress 'OK' to display debug information to send to technical support, or press 'Cancel' to exit.")){
Debug.ShowAllAvailableData(true);
}
}else{
alert(_76);
}
return;
}
function Controller_GetExceptionText(){
if(typeof Debug!="undefined"){
this.WriteAuditLog("`1503`");
}
if(this.Sequencer!=null&&this.Sequencer!=undefined){
return this.Sequencer.GetExceptionText();
}else{
return "";
}
}
function Controller_CheckForDebugCommand(_77){
if(_77==ASCII_SHIFT_IN){
Debug.RecordControlAudit=true;
Debug.RecordControlDetailed=true;
Debug.RecordRteAudit=true;
Debug.RecordRteDetailed=true;
Debug.RecordSequencingAudit=true;
Debug.RecordSequencingDetailed=true;
Debug.RecordLookAheadAudit=true;
Debug.RecordLookAheadDetailed=true;
alert("Debugger set to Level 8 [Maximum]");
}else{
if(this.QuestionCounter==4){
if(_77==ASCII_QUESTION){
Debug.ShowAllAvailableData();
}else{
if(_77==ASCII_D){
Debug.ShowDebugLogAtExit=true;
}else{
var _78=-1;
if(_77==ASCII_0){
_78=0;
}else{
if(_77==ASCII_1){
_78=1;
}else{
if(_77==ASCII_2){
_78=2;
}else{
if(_77==ASCII_3){
_78=3;
}else{
if(_77==ASCII_4){
_78=4;
}else{
if(_77==ASCII_5){
_78=5;
}else{
if(_77==ASCII_6){
_78=6;
}else{
if(_77==ASCII_7){
_78=7;
}else{
if(_77==ASCII_8){
_78=8;
}
}
}
}
}
}
}
}
}
Debug.RecordControlAudit=_78>0;
Debug.RecordControlDetailed=_78>1;
Debug.RecordRteAudit=_78>2;
Debug.RecordRteDetailed=_78>3;
Debug.RecordSequencingAudit=_78>4;
Debug.RecordSequencingDetailed=_78>5;
Debug.RecordLookAheadAudit=_78>6;
Debug.RecordLookAheadDetailed=_78>7;
alert("Debugger set to Level "+_78);
}
}
this.QuestionCounter=0;
}else{
if(_77==ASCII_QUESTION){
this.QuestionCounter++;
}else{
if(_77==ASCII_TILDA){
this.TildaCounter++;
if(this.TildaCounter==5){
this.TildaCounter=0;
Debug.External.Invoke();
}
}else{
if(_77==ASCII_PIPE){
this.PipeCounter++;
if(this.PipeCounter==5){
this.PipeCounter=0;
launchTypeTemp=this.ScoLoader.ScoLaunchType;
this.ScoLoader.ScoLaunchType=LAUNCH_TYPE_POPUP_WITH_MENU;
this.ScoLoader.LoadSco(this.Sequencer.GetCurrentActivity());
this.ScoLoader.ScoLaunchType=launchTypeTemp;
}
}else{
if(_77!==0){
this.QuestionCounter=0;
this.TildaCounter=0;
}
}
}
}
}
}
}
function Controller_CloseSco(){
this.WriteAuditLog("`1309`");
if(this.PendingNavigationRequest===null){
if(this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT].WillSucceed===true){
this.PendingNavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT,null,"");
}else{
if(this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON]===true){
this.PendingNavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_ABANDON,null,"");
}
}
}
this.WriteHistoryLog("",{ev:"GUI Close",ac:this.PendingNavigationRequest.Type});
this.ScoLoader.UnloadSco();
}
function Controller_ReturnToLms(_79){
this.WriteAuditLog("`1249`");
this.Sequencer.ReturnToLmsInvoked=true;
if(_79===null||_79===undefined){
_79=this.GetPreferredReturnToLmsAction();
}
if(this.PendingNavigationRequest===null){
this.PendingNavigationRequest=this.GetReturnToLmsNavigationRequest(_79);
}
this.WriteHistoryLog("",{ev:"GUI ReturnToLms",ac:this.PendingNavigationRequest.Type});
this.ScoLoader.UnloadSco();
}
function Controller_GetReturnToLmsNavigationRequest(_7a){
var _7b=false;
var _7c=false;
var _7d=false;
if(this.Sequencer.CurrentActivity!=null){
_7b=this.Sequencer.CurrentActivity.LearningObject.SequencingData.HideSuspendAll;
_7c=this.Sequencer.CurrentActivity.LearningObject.SequencingData.HideAbandonAll;
_7d=this.Sequencer.CurrentActivity.LearningObject.SequencingData.HideExitAll;
}
if(this.Activities.GetNumDeliverableActivities()==1){
if(_7d===false&&this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].WillSucceed===true){
return new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
}else{
if(_7b===false&&this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL].WillSucceed===true){
return new NavigationRequest(NAVIGATION_REQUEST_SUSPEND_ALL,null,"");
}
}
}else{
if(_7a=="exit_all"){
if(_7d===false&&this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL].WillSucceed===true){
return new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
}
}else{
if(_7a=="suspend_all"){
if(_7b===false&&this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL].WillSucceed===true){
return new NavigationRequest(NAVIGATION_REQUEST_SUSPEND_ALL,null,"");
}
}
}
}
if(_7c===false&&this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON_ALL].WillSucceed===true){
return new NavigationRequest(NAVIGATION_REQUEST_ABANDON_ALL,null,"");
}else{
return new NavigationRequest(NAVIGATION_REQUEST_EXIT_PLAYER,null,"");
}
}
function Controller_ToggleMenuVisibility(){
this.WriteAuditLog("`1423`");
if(this.MenuIsVisible===true){
IntegrationImplementation.HideMenu();
this.MenuIsVisible=false;
}else{
IntegrationImplementation.ShowMenu(this.Package.Properties.CourseStructureWidth);
this.MenuIsVisible=true;
}
}
function Controller_HideExitDialog(){
IntegrationImplementation.HideExitDialog();
this.ExitDialogVisible=false;
}
function Controller_TriggerReturnToLMS(){
logParent=this.WriteAuditLog("`1465`");
var _7e=false;
var _7f=false;
var _80=false;
if(this.Sequencer.CurrentActivity!=null){
_7e=this.Sequencer.CurrentActivity.LearningObject.SequencingData.HideSuspendAll;
_7f=this.Sequencer.CurrentActivity.LearningObject.SequencingData.HideExitAll;
_80=this.Sequencer.CurrentActivity.LearningObject.SequencingData.HideAbandonAll;
}
if(_7e===true||_7f===true){
if(_7e===true){
if(_7f===true){
if(_80===true){
this.WriteDetailedLog("`837`",logParent);
}else{
this.ReturnToLms("abandon_all");
}
}else{
this.ReturnToLms("exit_all");
}
}else{
this.ReturnToLms("suspend_all");
}
}else{
if(this.Package.Properties.ReturnToLmsAction=="selectable"){
if(this.ExitDialogVisible===false){
IntegrationImplementation.ShowExitDialog();
this.ExitDialogVisible=true;
}
}else{
if(this.Package.Properties.ReturnToLmsAction=="legacy"){
if(this.Activities.GetNumDeliverableActivities()==1){
this.ReturnToLms("exit_all");
}else{
this.ReturnToLms("suspend_all");
}
}else{
this.ReturnToLms(this.Package.Properties.ReturnToLmsAction);
}
}
}
}
function Controller_Next(){
this.WriteAuditLog("`1269`");
this.WriteHistoryLog("",{ev:"GUI Continue"});
if(this.PendingNavigationRequest===null){
this.PendingNavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_CONTINUE,null);
}
this.ScoLoader.UnloadSco();
}
function Controller_Previous(){
this.WriteAuditLog("`1170`");
this.WriteHistoryLog("",{ev:"GUI Previous"});
if(this.PendingNavigationRequest===null){
this.PendingNavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_PREVIOUS,null,"");
}
this.ScoLoader.UnloadSco();
}
function Controller_Abandon(){
this.WriteAuditLog("`1207`");
this.WriteHistoryLog("",{ev:"GUI Abandon"});
if(this.PendingNavigationRequest===null){
this.PendingNavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_ABANDON,null,"");
}
this.ScoLoader.UnloadSco();
}
function Controller_AbandonAll(){
this.WriteAuditLog("`1116`");
this.WriteHistoryLog("",{ev:"GUI AbandonAll"});
if(this.PendingNavigationRequest===null){
this.PendingNavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_ABANDON_ALL,null,"");
}
this.ScoLoader.UnloadSco();
}
function Controller_Suspend(){
this.WriteAuditLog("`1215`");
this.WriteHistoryLog("",{ev:"GUI Suspend"});
if(this.PendingNavigationRequest===null){
this.PendingNavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_SUSPEND_ALL,null,"");
}
this.ScoLoader.UnloadSco();
}
function Controller_Exit(){
this.WriteAuditLog("`1265`");
this.WriteHistoryLog("",{ev:"GUI Exit"});
if(this.PendingNavigationRequest===null){
this.PendingNavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT,null,"");
}
this.ScoLoader.UnloadSco();
}
function Controller_ExitAll(){
this.WriteAuditLog("`1198`");
this.WriteHistoryLog("",{ev:"GUI ExitAll"});
if(this.PendingNavigationRequest===null){
this.PendingNavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
}
this.ScoLoader.UnloadSco();
}
function Controller_ChoiceRequest(_81){
this.WriteAuditLog("`1286`"+_81+"`1716`");
var _82={ev:"GUI Choice"};
var _83=null;
if(this.Activities){
_83=this.Activities.GetActivityFromIdentifier(_81);
if(_83){
_82.tai=_83.ItemIdentifier;
_82.tat=_83.LearningObject.Title;
}
}
this.WriteHistoryLog("",_82);
if(this.PendingNavigationRequest===null){
this.PendingNavigationRequest=new NavigationRequest(NAVIGATION_REQUEST_CHOICE,_81,"");
}
this.ScoLoader.UnloadSco();
}
function Controller_ScoHasTerminatedSoUnload(){
var _84=this.WriteAuditLog("`1453`");
this.ScoLoader.UnloadSco();
}
function Controller_SignalTerminated(){
if(this.ProcessedUnload===true&&this.ExitScormPlayerCalled===false){
if(this.Api.Activity!==null&&this.Api.NeedToCloseOutSession()===false){
this.Api.CloseOutSession();
}
var _85=this.GetPreferredReturnToLmsAction();
this.Sequencer.NavigationRequest=this.GetReturnToLmsNavigationRequest(_85);
this.Sequencer.OverallSequencingProcess();
this.Comm.SaveDataOnExit();
}else{
if(this.Package.Properties.FinishCausesImmediateCommit===true){
this.Comm.SaveDataNow(false);
}
}
}
function Controller_GetPreferredReturnToLmsAction(){
var _86="";
if(this.Package.Properties.ReturnToLmsAction=="selectable"){
if(confirm("Exiting Course.  Click 'OK' To save state and pick up where you left off or choose 'Cancel' to finish the Course.")){
_86="suspend_all";
}else{
_86="exit_all";
}
}else{
if(this.Package.Properties.ReturnToLmsAction=="legacy"){
if(this.Activities.GetNumDeliverableActivities()==1){
_86="exit_all";
}else{
_86="suspend_all";
}
}else{
_86=this.Package.Properties.ReturnToLmsAction;
}
}
return _86;
}
function Controller_TranslateRunTimeNavRequest(_87){
if(_87.substring(0,1)=="{"){
var _88=this.ParseTargetStringFromChoiceRequest(_87);
if(_87.substr(_87.indexOf("}")+1)=="choice"){
return new NavigationRequest(NAVIGATION_REQUEST_CHOICE,_88,"");
}else{
if(_87.substr(_87.indexOf("}")+1)=="jump"){
return new NavigationRequest(NAVIGATION_REQUEST_JUMP,_88,"");
}
}
}
switch(_87){
case SCORM_RUNTIME_NAV_REQUEST_CONTINUE:
return new NavigationRequest(NAVIGATION_REQUEST_CONTINUE,null,"");
case SCORM_RUNTIME_NAV_REQUEST_PREVIOUS:
return new NavigationRequest(NAVIGATION_REQUEST_PREVIOUS,null,"");
case SCORM_RUNTIME_NAV_REQUEST_EXIT:
return new NavigationRequest(NAVIGATION_REQUEST_EXIT,null,"");
case SCORM_RUNTIME_NAV_REQUEST_EXITALL:
return new NavigationRequest(NAVIGATION_REQUEST_EXIT_ALL,null,"");
case SCORM_RUNTIME_NAV_REQUEST_ABANDON:
return new NavigationRequest(NAVIGATION_REQUEST_ABANDON,null,"");
case SCORM_RUNTIME_NAV_REQUEST_ABANDONALL:
return new NavigationRequest(NAVIGATION_REQUEST_ABANDON_ALL,null,"");
case SCORM_RUNTIME_NAV_REQUEST_SUSPENDALL:
return new NavigationRequest(NAVIGATION_REQUEST_SUSPEND_ALL,null,"");
case SCORM_RUNTIME_NAV_REQUEST_NONE:
return null;
default:
Debug.AssertError("Unrecognized runtime navigation request");
break;
}
}
function Controller_FindPossibleNavRequestForRuntimeNavRequest(_89){
if(_89.substring(0,1)=="{"){
var _8a=this.ParseTargetStringFromChoiceRequest(_89);
if(_89.substr(_89.indexOf("}")+1)=="choice"){
var _8b=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;
for(var i=_8b;i<this.PossibleNavigationRequests.length;i++){
if(this.PossibleNavigationRequests[i].TargetActivityItemIdentifier==_8a){
return this.PossibleNavigationRequests[i];
}
}
}else{
if(_89.substr(_89.indexOf("}")+1)=="jump"){
var _8d=new NavigationRequest(NAVIGATION_REQUEST_JUMP,_8a,"");
_8d.WillSucceed=this.IsJumpRequestValid(_89);
return _8d;
}
}
}
switch(_89){
case SCORM_RUNTIME_NAV_REQUEST_CONTINUE:
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE];
case SCORM_RUNTIME_NAV_REQUEST_PREVIOUS:
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS];
case SCORM_RUNTIME_NAV_REQUEST_EXIT:
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT];
case SCORM_RUNTIME_NAV_REQUEST_EXITALL:
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL];
case SCORM_RUNTIME_NAV_REQUEST_ABANDON:
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON];
case SCORM_RUNTIME_NAV_REQUEST_ABANDONALL:
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON_ALL];
case SCORM_RUNTIME_NAV_REQUEST_SUSPENDALL:
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL];
case SCORM_RUNTIME_NAV_REQUEST_NONE:
return null;
default:
Debug.AssertError("Unrecognized runtime navigation request");
break;
}
}
function Controller_GetMessageText(){
var msg="";
try{
if(this.Sequencer.NavigationRequest!==null&&this.Sequencer.NavigationRequest!==undefined){
if(this.Sequencer.NavigationRequest.MessageToUser!==null&&this.Sequencer.NavigationRequest.MessageToUser!==undefined){
msg=this.Sequencer.NavigationRequest.MessageToUser;
}
}
}
catch(e){
}
return msg;
}
function Controller_ClearPendingNavigationRequest(){
this.WriteAuditLog("`1280`");
this.PendingNavigationRequest=null;
}
function Controller_IsThereAPendingNavigationRequest(){
return (this.PendingNavigationRequest!==null);
}
function Controller_DeliverActivity(_8f){
this.WriteAuditLog("`1258`");
this.WriteAuditLog("`1530`"+_8f);
this.WriteAuditLog("`1258`");
var _90=this.WriteAuditLog("`1476`"+_8f);
if(_8f.IsDeliverable()===false){
Debug.AssertError("ERROR - Asked to deliver a non-leaf activity - "+_8f);
}
if((Control.Package.Properties.ScoLaunchType!==LAUNCH_TYPE_POPUP_AFTER_CLICK)&&(Control.Package.Properties.ScoLaunchType!==LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR)){
this.Api.ResetState();
this.Api.InitializeForDelivery(_8f);
}
this.ScoLoader.LoadSco(_8f);
this.UpdateDisplay(false,false);
this.WriteDetailedLog("`1354`",_90);
}
function Controller_PerformDelayedDeliveryInitialization(_91){
this.Sequencer.ContentDeliveryEnvironmentActivityDataSubProcess(_91);
this.Api.ResetState();
this.Api.InitializeForDelivery(_91);
this.EvaluatePossibleNavigationRequests();
}
function Controller_InitializePossibleNavigationRequests(){
var _92=this.WriteAuditLog("`1216`");
if(Control.Package.Properties.LookaheadSequencerMode===LOOKAHEAD_SEQUENCER_MODE_DISABLE){
this.WriteDetailedLog("`1352`",_92);
var _93=true;
}else{
this.WriteDetailedLog("`1356`",_92);
var _93=RESULT_UNKNOWN;
}
this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_START]=new PossibleRequest(NAVIGATION_REQUEST_START,null,_93,"","");
this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_RESUME_ALL]=new PossibleRequest(NAVIGATION_REQUEST_RESUME_ALL,null,_93,"","");
this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE]=new PossibleRequest(NAVIGATION_REQUEST_CONTINUE,null,_93,"","");
this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS]=new PossibleRequest(NAVIGATION_REQUEST_PREVIOUS,null,_93,"","");
this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT]=new PossibleRequest(NAVIGATION_REQUEST_EXIT,null,_93,"","");
this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_EXIT_ALL]=new PossibleRequest(NAVIGATION_REQUEST_EXIT_ALL,null,_93,"","");
this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_SUSPEND_ALL]=new PossibleRequest(NAVIGATION_REQUEST_SUSPEND_ALL,null,_93,"","");
this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON]=new PossibleRequest(NAVIGATION_REQUEST_ABANDON,null,_93,"","");
this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_ABANDON_ALL]=new PossibleRequest(NAVIGATION_REQUEST_ABANDON_ALL,null,_93,"","");
var _94=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;
for(var _95 in this.Activities.SortedActivityList){
activity=this.Activities.SortedActivityList[_95];
itemId=activity.GetItemIdentifier();
this.PossibleNavigationRequests[_94]=new PossibleRequest(NAVIGATION_REQUEST_CHOICE,itemId,_93,"","");
_94++;
}
for(var id in this.PossibleNavigationRequests){
this.WriteDetailedLog("`1706`"+this.PossibleNavigationRequests[id].toString(),_92);
}
this.Sequencer.InitializePossibleNavigationRequestAbsolutes(this.PossibleNavigationRequests,this.Activities.ActivityTree,this.Activities.SortedActivityList);
}
function Controller_EvaluatePossibleNavigationRequests(_97,_98){
if(_98===undefined||_98===null){
_98=false;
}
if(this.Package.Properties.LookaheadSequencerMode===LOOKAHEAD_SEQUENCER_MODE_ENABLE){
var _99=this.WriteAuditLog("`1120`");
this.WriteDetailedLog("`1551`",_99);
this.TearDownSequencer(this.LookAheadSequencer);
this.WriteDetailedLog("`1663`",_99);
this.LookAheadSequencer=this.CloneSequencer(this.Sequencer);
this.LookAheadSequencer.LookAhead=true;
this.WriteDetailedLog("`1071`",_99);
this.LookAheadSequencer.Activities.SetSequencer(this.LookAheadSequencer,true);
this.WriteDetailedLog("`1152`",_99);
this.PossibleNavigationRequests=this.LookAheadSequencer.EvaluatePossibleNavigationRequests(this.PossibleNavigationRequests);
this.WriteDetailedLog("`1127`",_99);
if(_98===true){
if(this.PendingNavigationRequest===null){
if((this.Api!==null)&&(this.Api.RunTimeData!=null)&&(this.Api.RunTimeData.NavRequest!=SCORM_RUNTIME_NAV_REQUEST_NONE)){
var _9a=Control.FindPossibleNavRequestForRuntimeNavRequest(this.Api.RunTimeData.NavRequest);
this.WriteDetailedLog("`1298`"+_9a+"`749`",_99);
if(_9a.WillSucceed===true){
this.WriteDetailedLog("`1232`",_99);
Control.ScoHasTerminatedSoUnload();
}else{
this.WriteDetailedLog("`1032`",_99);
}
}
}
}
if(_97!==undefined&&_97!==null&&_97==true){
this.UpdateDisplay(true,true);
}else{
this.UpdateDisplay(true,false);
}
}else{
var _99=this.WriteAuditLog("`533`");
}
}
function Controller_FindPossibleChoiceRequestForActivity(_9b){
var _9c=_9b.GetItemIdentifier();
var _9d=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;
for(var i=_9d;i<this.PossibleNavigationRequests.length;i++){
if(this.PossibleNavigationRequests[i].TargetActivityItemIdentifier==_9c){
return this.PossibleNavigationRequests[i];
}
}
Debug.AssertError("Could not locate possible choice request for activity-"+_9b);
return null;
}
function Controller_GetPossibleContinueRequest(){
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE];
}
function Controller_GetPossiblePreviousRequest(){
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS];
}
function Controller_IsTargetValid(_9f){
var _a0=this.ParseTargetStringIntoActivity(_9f);
if(_a0===null){
return false;
}else{
return true;
}
}
function Controller_IsChoiceRequestValid(_a1){
var _a2=this.ParseTargetStringIntoActivity(_a1);
var _a3=_a2.GetItemIdentifier();
for(var i=POSSIBLE_NAVIGATION_REQUEST_INDEX_CHOICE;i<this.PossibleNavigationRequests.length;i++){
if(this.PossibleNavigationRequests[i].TargetActivityItemIdentifier==_a3){
return this.PossibleNavigationRequests[i].WillSucceed;
}
}
return false;
}
function Controller_IsJumpRequestValid(_a5){
var _a6=this.ParseTargetStringIntoActivity(_a5);
if(_a6!=null){
return _a6.IsAvailable();
}
return false;
}
function Controller_IsContinueRequestValid(){
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_CONTINUE].WillSucceed;
}
function Controller_IsPreviousRequestValid(){
return this.PossibleNavigationRequests[POSSIBLE_NAVIGATION_REQUEST_INDEX_PREVIOUS].WillSucceed;
}
function Controller_ParseTargetStringIntoActivity(_a7){
var _a8=this.ParseTargetStringFromChoiceRequest(_a7);
var _a9=this.Activities.GetActivityFromIdentifier(_a8);
return _a9;
}
function Controller_ParseTargetStringFromChoiceRequest(_aa){
return _aa.substring(_aa.indexOf("=")+1,_aa.indexOf("}"));
}
function Controller_CloneSequencer(_ab,_ac){
var _ad=new Sequencer(_ac,_ab.Activities.Clone());
if(_ab.SuspendedActivity===null){
_ad.SuspendedActivity=null;
}else{
_ad.SuspendedActivity=_ad.Activities.GetActivityFromIdentifier(_ab.SuspendedActivity.GetItemIdentifier());
}
if(_ab.CurrentActivity===null){
_ad.CurrentActivity=null;
}else{
_ad.CurrentActivity=_ad.Activities.GetActivityFromIdentifier(_ab.CurrentActivity.GetItemIdentifier());
}
_ad.GlobalObjectives=new Array();
for(var _ae in _ab.GlobalObjectives){
_ad.GlobalObjectives[_ad.GlobalObjectives.length]=_ab.GlobalObjectives[_ae].Clone();
}
if(_ab.AtEndOfCourse!==undefined){
_ad.AtEndOfCourse=_ab.AtEndOfCourse;
_ad.AtStartOfCourse=_ab.AtStartOfCourse;
}
_ad.NavigationRequest=null;
_ad.ChoiceTargetIdentifier=null;
_ad.Exception=null;
_ad.ExceptionText=null;
return _ad;
}
function Controller_TearDownSequencer(_af){
_af.LookAhead=null;
if(_af.Activities!==null){
_af.Activities.TearDown();
}
_af.Activities=null;
_af.NavigationRequest=null;
_af.ChoiceTargetIdentifier=null;
_af.SuspendedActivity=null;
_af.CurrentActivity=null;
_af.Exception=null;
_af.ExceptionText=null;
_af.GlobalObjectives=null;
}
function Controller_WriteAuditLog(str){
Debug.WriteControlAudit(str);
}
function Controller_WriteDetailedLog(str,_b2){
Debug.WriteControlDetailed(str,_b2);
}
function Controller_WriteDetailedLogError(str,_b4){
Debug.WriteControlDetailed(str,_b4,true);
}
function Controller_WriteHistoryLog(str,_b6){
HistoryLog.WriteEventDetailed(str,_b6);
}
function Controller_WriteHistoryReturnValue(str,_b8){
HistoryLog.WriteEventDetailedReturnValue(str,_b8);
}
function Controller_GetLaunchHistoryId(){
return LaunchHistoryId;
}
function BuildFullUrl(_b9,_ba){
if(_ba.indexOf("?")>-1){
var _bb=_ba.substr(0,_ba.indexOf("?"));
}else{
var _bb=_ba;
}
var _bc=_ba.substr(0,_bb.lastIndexOf("/"));
while(_b9.indexOf("../")>-1){
_b9=_b9.substr(3,_b9.length);
_bc=_bc.substr(0,_bc.lastIndexOf("/"));
}
return _bc+"/"+_b9;
}
function GetController(){
var _bd=500;
var _be=0;
var win=window;
while((win.Control===null)&&(win.parent!==null)&&(win.parent!=win)&&(_be<=_bd)){
_be++;
win=win.parent;
}
if(win.Control===null){
Debug.AssertError("Could not locate the Control object.");
}else{
return win.Control;
}
}
function Controller_UpdateGlobalLearnerPrefs(){
if(this.Api.LearnerPrefsArray!==null&&this.Api.LearnerPrefsArray!==undefined){
var _c0=this.Api.LearnerPrefsArray;
for(var _c1 in _c0){
for(var _c2 in this.Activities.ActivityList){
var _c3=this.Activities.ActivityList[_c2];
if(_c3.RunTime!==null&&_c3.RunTime[_c1]!==_c0[_c1]){
_c3.RunTime[_c1]=_c0[_c1];
_c3.RunTime.SetDirtyData();
}
}
}
}
}
function Debugger(_c4,_c5,_c6,_c7,_c8,_c9,_ca,_cb,_cc,_cd,_ce){
this.auditCount=0;
this.RecordControlAudit=_c4;
this.RecordControlDetailed=_c5;
if(this.RecordControlDetailed){
this.RecordControlAudit=true;
}
this.RecordRteAudit=_c6;
this.RecordRteDetailed=_c7;
if(this.RecordRteDetailed){
this.RecordRteAudit=true;
}
this.RecordSequencingAudit=_c8;
this.RecordSequencingDetailed=_c9;
if(this.RecordSequencingDetailed){
this.RecordSequencingAudit=true;
}
this.RecordLookAheadAudit=_cb;
this.RecordLookAheadDetailed=_cc;
if(this.RecordLookAheadDetailed){
this.RecordLookAheadAudit=true;
}
this.RecordSequencingSimple=_ca;
this.Write=Debugger_Write;
this.AssertError=Debugger_AssertError;
this.DataIsAvailable=Debugger_DataIsAvailable;
this.ShowAllAvailableData=Debugger_ShowAllAvailableData;
this.WriteControlAudit=Debugger_WriteControlAudit;
this.WriteControlDetailed=Debugger_WriteControlDetailed;
this.WriteRteAudit=Debugger_WriteRteAudit;
this.WriteRteAuditReturnValue=Debugger_WriteRteAuditReturnValue;
this.WriteRteDetailed=Debugger_WriteRteDetailed;
this.WriteSequencingAudit=Debugger_WriteSequencingAudit;
this.WriteSequencingAuditReturnValue=Debugger_WriteSequencingAuditReturnValue;
this.WriteSequencingDetailed=Debugger_WriteSequencingDetailed;
this.WriteSequencingSimpleAudit=Debugger_WriteSequencingSimpleAudit;
this.WriteSequencingSimpleReturnValue=Debugger_WriteSequencingSimpleReturnValue;
this.WriteSequencingSimpleDetailed=Debugger_WriteSequencingSimpleDetailed;
this.WriteLookAheadAudit=Debugger_WriteLookAheadAudit;
this.WriteLookAheadAuditReturnValue=Debugger_WriteLookAheadAuditReturnValue;
this.WriteLookAheadDetailed=Debugger_WriteLookAheadDetailed;
this.GetErrors=Debugger_GetErrors;
this.log=new Log(_cd,_ce);
this.currentControlEntry=null;
this.currentRunTimeEntry=null;
this.currentSequencingEntry=null;
this.currentLookAheadEntry=null;
this.currentSequencingSimpleEntry=null;
this.ErrorDataExists=false;
this.ShowDebugLogAtExit=false;
this.version=_cd;
this.includeTimestamps=_ce;
}
function Debugger_Write(str){
this.AssertError("Debug.Write function is deprecated. Remove all calls.");
}
function Debugger_AssertError(arg,_d1){
if(_d1===undefined||_d1===null||_d1===true){
Debug.WriteControlAudit("Code Asserted Error-"+arg);
if(confirm(arg+"\r\nWould you like to enter debug mode?")){
var x=a.b.c;
}
}
}
function Debugger_DataIsAvailable(){
var _d3=this.RecordControlAudit||this.RecordControlDetailed||this.RecordRteAudit||this.RecordRteDetailed||this.RecordSequencingAudit||this.RecordSequencingDetailed||this.RecordLookAheadAudit||this.RecordLookAheadDetailed||this.ErrorDataExists;
return _d3;
}
function Debugger_ShowAllAvailableData(_d4){
if(_d4===undefined||_d4===null){
_d4=false;
}
this.log.display(_d4);
}
function Debugger_WriteControlAudit(_d5,_d6,_d7){
if(_d7){
this.ErrorDataExists=true;
}
if(this.RecordControlAudit||_d7){
if(_d6===undefined||_d6===null){
this.currentControlEntry=this.log.startNew("c",_d5);
}else{
this.currentControlEntry=_d6.startNew("c",_d5);
}
this.currentControlEntry.setAttribute("id",this.auditCount++);
return this.currentControlEntry;
}else{
return disabledLogEntry;
}
}
function Debugger_WriteControlAuditReturnValue(str,_d9,_da){
if(this.currentControlEntry==null){
return;
}
if(this.RecordControlAudit||_da){
this.currentControlEntry.setReturn(str);
}
if(_d9!==null&&_d9!==undefined){
this.currentControlEntry=_d9;
}else{
this.AssertError("Debugger_WriteControlAuditReturnValue() called without parentEntry");
}
}
function Debugger_WriteControlDetailed(str,_dc,_dd){
if(this.currentControlEntry==null){
return;
}
if(_dd){
var _de=this.WriteControlAudit("Control ERROR",null,true);
_de.write(str);
}else{
if(this.RecordControlDetailed){
if(_dc!==undefined&&_dc!==null){
_dc.write(str);
}else{
this.currentControlEntry.write(str);
}
}
}
}
function Debugger_WriteRteAudit(_df,_e0){
if(this.RecordRteAudit){
if(_e0===undefined||_e0===null){
this.currentRunTimeEntry=this.log.startNew("rt",_df);
}else{
this.currentRunTimeEntry=_e0.startNew("rt",_df);
}
this.currentRunTimeEntry.setAttribute("id",this.auditCount++);
return this.currentRunTimeEntry;
}else{
return disabledLogEntry;
}
}
function Debugger_WriteRteAuditReturnValue(str,_e2){
if(this.currentRunTimeEntry==null){
return;
}
if(this.RecordRteAudit){
this.currentRunTimeEntry.setReturn(str);
}
if(_e2!==null&&_e2!==undefined){
this.currentRunTimeEntry=_e2;
}else{
}
}
function Debugger_WriteRteDetailed(str,_e4){
if(this.currentRunTimeEntry==null){
return;
}
if(this.RecordRteDetailed){
if(_e4===undefined||_e4===null){
this.currentRunTimeEntry.write(str);
}else{
_e4.write(str);
}
}
}
function Debugger_WriteSequencingAudit(_e5,_e6){
if(this.RecordSequencingAudit){
if(_e6===undefined||_e6===null){
this.currentSequencingEntry=this.log.startNew("s",_e5);
}else{
this.currentSequencingEntry=_e6.startNew("s",_e5);
}
this.currentSequencingEntry.setAttribute("id",this.auditCount++);
return this.currentSequencingEntry;
}else{
return disabledLogEntry;
}
}
function Debugger_WriteSequencingAuditReturnValue(str,_e8){
if(this.currentSequencingEntry==null){
return;
}
if(this.RecordSequencingAudit){
this.currentSequencingEntry.setReturn(str);
}
if(_e8!==null&&_e8!==undefined){
this.currentSequencingEntry=_e8;
}else{
this.AssertError("Debugger_WriteSequencingAuditReturnValue() called without parentEntry");
}
}
function Debugger_WriteSequencingDetailed(str,_ea){
if(this.currentSequencingEntry==null){
return;
}
if(this.RecordSequencingDetailed){
if(_ea===undefined||_ea===null){
this.currentSequencingEntry.write(str);
}else{
_ea.write(str);
}
}
}
function Debugger_WriteLookAheadAudit(_eb,_ec){
if(this.RecordLookAheadAudit){
if(_ec===undefined||_ec===null){
this.currentLookAheadEntry=this.log.startNew("l",_eb);
}else{
this.currentLookAheadEntry=_ec.startNew("l",_eb);
}
this.currentLookAheadEntry.setAttribute("id",this.auditCount++);
return this.currentLookAheadEntry;
}else{
return disabledLogEntry;
}
}
function Debugger_WriteLookAheadAuditReturnValue(str,_ee){
if(this.currentLookAheadEntry==null){
return;
}
if(this.RecordLookAheadAudit){
this.currentLookAheadEntry.setReturn(str);
}
if(_ee!==null&&_ee!==undefined){
this.currentLookAheadEntry=_ee;
}else{
this.AssertError("Debugger_WriteLookAheadAuditReturnValue() called without parentEntry");
}
}
function Debugger_WriteLookAheadDetailed(str,_f0){
if(this.currentLookAheadEntry==null){
return;
}
if(this.RecordLookAheadDetailed){
if(_f0===undefined||_f0===null){
this.currentLookAheadEntry.write(str);
}else{
_f0.write(str);
}
}
}
function Debugger_WriteSequencingSimpleAudit(_f1,_f2){
if(this.RecordSequencingSimple){
if(_f2===undefined||_f2===null){
this.currentSequencingSimpleEntry=this.log.startNew("ss",_f1);
}else{
this.currentSequencingSimpleEntry=_f2.startNew("ss",_f1);
}
this.currentSequencingSimpleEntry.setAttribute("id",this.auditCount++);
return this.currentSequencingSimpleEntry;
}else{
return disabledLogEntry;
}
}
function Debugger_WriteSequencingSimpleReturnValue(str,_f4){
if(this.currentSequencingSimpleEntry==null){
return;
}
if(this.RecordSequencingSimple){
this.currentSequencingSimpleEntry.setReturn(str);
}
if(_f4!==null&&_f4!==undefined){
this.currentSequencingSimpleEntry=_f4;
}else{
this.AssertError("Debugger_WriteLookAheadAuditReturnValue() called without parentEntry");
}
}
function Debugger_WriteSequencingSimpleDetailed(str,_f6){
if(this.currentSequencingSimpleEntry==null){
return;
}
if(this.RecordSequencingSimple){
if(_f6===undefined||_f6===null){
this.currentSequencingSimpleEntry.write(str);
}else{
_f6.write(str);
}
}
}
function Debugger_GetErrors(){
var _f7="//c[@f='Control ERROR']";
var _f8=this.log.dom.selectNodes(_f7);
var _f9=new Array();
for(var i=0;i<_f8.length;i++){
if(_f8[i].text!==undefined){
_f9[_f9.length]=_f8[i].text;
}else{
_f9[_f9.length]=_f8[i].textContent;
}
}
return _f9;
}
function DisabledLogEntry(){
}
DisabledLogEntry.prototype.write=function(){
};
DisabledLogEntry.prototype.error=function(){
};
DisabledLogEntry.prototype.startNew=function(){
return disabledLogEntry;
};
DisabledLogEntry.prototype.setAttribute=function(){
};
DisabledLogEntry.prototype.setReturn=function(){
};
disabledLogEntry=new DisabledLogEntry();
var objXmlHttp=null;
var Debug=null;
var ExternalConfig="";
var ExternalRegistrationId="";
var PathToCourse="";
var RedirectOnExitUrl="";
var LearnerName="";
var LearnerId="";
var IntermediatePage=null;
var PopupLauncherPage=null;
var RegistrationToDeliver=null;
var QueryStringAdditions=null;
var IntegrationImplementation=null;
var Control=null;
function HistoryLogger(_fb,_fc,_fd){
this.auditCount=0;
this.RecordHistory=_fb;
this.RecordHistoryDetailed=_fc;
if(this.RecordHistoryDetailed){
this.RecordHistory=true;
}
this.DataIsAvailable=HistoryLogger_DataIsAvailable;
this.ShowAllAvailableData=HistoryLogger_ShowAllAvailableData;
this.WriteEvent=HistoryLogger_WriteEvent;
this.WriteEventReturnValue=HistoryLogger_WriteEventReturnValue;
this.WriteEventDetailed=HistoryLogger_WriteEventDetailed;
this.WriteEventDetailedReturnValue=HistoryLogger_WriteEventDetailedReturnValue;
this.log=new Log(_fd,true,"RTL");
this.currentEntry=null;
this.DataExists=false;
this.version=_fd;
this.includeTimestamps=true;
}
function HistoryLogger_DataIsAvailable(){
return this.DataExists;
}
function HistoryLogger_ShowAllAvailableData(){
this.log.display();
}
function HistoryLogger_WriteEvent(_fe,_ff,_100){
if(this.RecordHistory){
var _101=(_ff!==undefined&&_ff!==null&&_ff.event?"":_fe);
if(_100===undefined||_100===null){
this.currentEntry=this.log.startNew("RT",_101);
}else{
this.currentEntry=_100.startNew("RT",_101);
}
this.currentEntry.setAttribute("id",this.auditCount++);
for(var _102 in _ff){
this.currentEntry.setAttribute(_102,_ff[_102]);
}
return this.currentEntry;
}else{
return disabledLogEntry;
}
}
function HistoryLogger_WriteEventReturnValue(str,atts,_105){
if(this.currentEntry==null){
return;
}
if(this.RecordHistory){
this.currentEntry.setReturn(str);
for(var _106 in atts){
this.currentEntry.setAttribute(_106,atts[_106]);
}
}
if(_105!==null&&_105!==undefined){
this.currentEntry=_105;
}else{
}
}
function HistoryLogger_WriteEventDetailed(args,atts,_109){
if(this.RecordHistoryDetailed){
var _10a=(atts!==undefined&&atts!==null&&atts.event?"":args);
if(_109===undefined||_109===null){
this.currentEntry=this.log.startNew("RT",_10a);
}else{
this.currentEntry=_109.startNew("RT",_10a);
}
this.currentEntry.setAttribute("id",this.auditCount++);
for(var _10b in atts){
this.currentEntry.setAttribute(_10b,atts[_10b]);
}
return this.currentEntry;
}else{
return disabledLogEntry;
}
}
function HistoryLogger_WriteEventDetailedReturnValue(str,atts,_10e){
if(this.currentEntry==null){
return;
}
if(this.RecordHistoryDetailed){
this.currentEntry.setReturn(str);
for(var _10f in atts){
this.currentEntry.setAttribute(_10f,atts[_10f]);
}
}
if(_10e!==null&&_10e!==undefined){
this.currentEntry=_10e;
}else{
}
}
Iso639LangCodes_LangCodes.prototype["aa"]=true;
Iso639LangCodes_LangCodes.prototype["aar"]=true;
Iso639LangCodes_LangCodes.prototype["ab"]=true;
Iso639LangCodes_LangCodes.prototype["abk"]=true;
Iso639LangCodes_LangCodes.prototype["ace"]=true;
Iso639LangCodes_LangCodes.prototype["ach"]=true;
Iso639LangCodes_LangCodes.prototype["ada"]=true;
Iso639LangCodes_LangCodes.prototype["ady"]=true;
Iso639LangCodes_LangCodes.prototype["ae"]=true;
Iso639LangCodes_LangCodes.prototype["af"]=true;
Iso639LangCodes_LangCodes.prototype["afa"]=true;
Iso639LangCodes_LangCodes.prototype["afh"]=true;
Iso639LangCodes_LangCodes.prototype["afr"]=true;
Iso639LangCodes_LangCodes.prototype["ak"]=true;
Iso639LangCodes_LangCodes.prototype["aka"]=true;
Iso639LangCodes_LangCodes.prototype["akk"]=true;
Iso639LangCodes_LangCodes.prototype["alb"]=true;
Iso639LangCodes_LangCodes.prototype["alb"]=true;
Iso639LangCodes_LangCodes.prototype["ale"]=true;
Iso639LangCodes_LangCodes.prototype["alg"]=true;
Iso639LangCodes_LangCodes.prototype["am"]=true;
Iso639LangCodes_LangCodes.prototype["amh"]=true;
Iso639LangCodes_LangCodes.prototype["an"]=true;
Iso639LangCodes_LangCodes.prototype["ang"]=true;
Iso639LangCodes_LangCodes.prototype["apa"]=true;
Iso639LangCodes_LangCodes.prototype["ar"]=true;
Iso639LangCodes_LangCodes.prototype["ara"]=true;
Iso639LangCodes_LangCodes.prototype["arc"]=true;
Iso639LangCodes_LangCodes.prototype["arg"]=true;
Iso639LangCodes_LangCodes.prototype["arm"]=true;
Iso639LangCodes_LangCodes.prototype["arm"]=true;
Iso639LangCodes_LangCodes.prototype["arn"]=true;
Iso639LangCodes_LangCodes.prototype["arp"]=true;
Iso639LangCodes_LangCodes.prototype["art"]=true;
Iso639LangCodes_LangCodes.prototype["arw"]=true;
Iso639LangCodes_LangCodes.prototype["as"]=true;
Iso639LangCodes_LangCodes.prototype["asm"]=true;
Iso639LangCodes_LangCodes.prototype["ast"]=true;
Iso639LangCodes_LangCodes.prototype["ath"]=true;
Iso639LangCodes_LangCodes.prototype["aus"]=true;
Iso639LangCodes_LangCodes.prototype["av"]=true;
Iso639LangCodes_LangCodes.prototype["ava"]=true;
Iso639LangCodes_LangCodes.prototype["ave"]=true;
Iso639LangCodes_LangCodes.prototype["awa"]=true;
Iso639LangCodes_LangCodes.prototype["ay"]=true;
Iso639LangCodes_LangCodes.prototype["aym"]=true;
Iso639LangCodes_LangCodes.prototype["az"]=true;
Iso639LangCodes_LangCodes.prototype["aze"]=true;
Iso639LangCodes_LangCodes.prototype["ba"]=true;
Iso639LangCodes_LangCodes.prototype["bad"]=true;
Iso639LangCodes_LangCodes.prototype["bai"]=true;
Iso639LangCodes_LangCodes.prototype["bak"]=true;
Iso639LangCodes_LangCodes.prototype["bal"]=true;
Iso639LangCodes_LangCodes.prototype["bam"]=true;
Iso639LangCodes_LangCodes.prototype["ban"]=true;
Iso639LangCodes_LangCodes.prototype["baq"]=true;
Iso639LangCodes_LangCodes.prototype["baq"]=true;
Iso639LangCodes_LangCodes.prototype["bas"]=true;
Iso639LangCodes_LangCodes.prototype["bat"]=true;
Iso639LangCodes_LangCodes.prototype["be"]=true;
Iso639LangCodes_LangCodes.prototype["bej"]=true;
Iso639LangCodes_LangCodes.prototype["bel"]=true;
Iso639LangCodes_LangCodes.prototype["bem"]=true;
Iso639LangCodes_LangCodes.prototype["ben"]=true;
Iso639LangCodes_LangCodes.prototype["ber"]=true;
Iso639LangCodes_LangCodes.prototype["bg"]=true;
Iso639LangCodes_LangCodes.prototype["bh"]=true;
Iso639LangCodes_LangCodes.prototype["bho"]=true;
Iso639LangCodes_LangCodes.prototype["bi"]=true;
Iso639LangCodes_LangCodes.prototype["bih"]=true;
Iso639LangCodes_LangCodes.prototype["bik"]=true;
Iso639LangCodes_LangCodes.prototype["bin"]=true;
Iso639LangCodes_LangCodes.prototype["bis"]=true;
Iso639LangCodes_LangCodes.prototype["bla"]=true;
Iso639LangCodes_LangCodes.prototype["bm"]=true;
Iso639LangCodes_LangCodes.prototype["bn"]=true;
Iso639LangCodes_LangCodes.prototype["bnt"]=true;
Iso639LangCodes_LangCodes.prototype["bo"]=true;
Iso639LangCodes_LangCodes.prototype["bo"]=true;
Iso639LangCodes_LangCodes.prototype["bod"]=true;
Iso639LangCodes_LangCodes.prototype["bod"]=true;
Iso639LangCodes_LangCodes.prototype["bos"]=true;
Iso639LangCodes_LangCodes.prototype["br"]=true;
Iso639LangCodes_LangCodes.prototype["bra"]=true;
Iso639LangCodes_LangCodes.prototype["bre"]=true;
Iso639LangCodes_LangCodes.prototype["bs"]=true;
Iso639LangCodes_LangCodes.prototype["btk"]=true;
Iso639LangCodes_LangCodes.prototype["bua"]=true;
Iso639LangCodes_LangCodes.prototype["bug"]=true;
Iso639LangCodes_LangCodes.prototype["bul"]=true;
Iso639LangCodes_LangCodes.prototype["bur"]=true;
Iso639LangCodes_LangCodes.prototype["bur"]=true;
Iso639LangCodes_LangCodes.prototype["byn"]=true;
Iso639LangCodes_LangCodes.prototype["ca"]=true;
Iso639LangCodes_LangCodes.prototype["cad"]=true;
Iso639LangCodes_LangCodes.prototype["cai"]=true;
Iso639LangCodes_LangCodes.prototype["car"]=true;
Iso639LangCodes_LangCodes.prototype["cat"]=true;
Iso639LangCodes_LangCodes.prototype["cau"]=true;
Iso639LangCodes_LangCodes.prototype["ce"]=true;
Iso639LangCodes_LangCodes.prototype["ceb"]=true;
Iso639LangCodes_LangCodes.prototype["cel"]=true;
Iso639LangCodes_LangCodes.prototype["ces"]=true;
Iso639LangCodes_LangCodes.prototype["ces"]=true;
Iso639LangCodes_LangCodes.prototype["ch"]=true;
Iso639LangCodes_LangCodes.prototype["cha"]=true;
Iso639LangCodes_LangCodes.prototype["chb"]=true;
Iso639LangCodes_LangCodes.prototype["che"]=true;
Iso639LangCodes_LangCodes.prototype["chg"]=true;
Iso639LangCodes_LangCodes.prototype["chi"]=true;
Iso639LangCodes_LangCodes.prototype["chi"]=true;
Iso639LangCodes_LangCodes.prototype["chk"]=true;
Iso639LangCodes_LangCodes.prototype["chm"]=true;
Iso639LangCodes_LangCodes.prototype["chn"]=true;
Iso639LangCodes_LangCodes.prototype["cho"]=true;
Iso639LangCodes_LangCodes.prototype["chp"]=true;
Iso639LangCodes_LangCodes.prototype["chr"]=true;
Iso639LangCodes_LangCodes.prototype["chu"]=true;
Iso639LangCodes_LangCodes.prototype["chv"]=true;
Iso639LangCodes_LangCodes.prototype["chy"]=true;
Iso639LangCodes_LangCodes.prototype["cmc"]=true;
Iso639LangCodes_LangCodes.prototype["co"]=true;
Iso639LangCodes_LangCodes.prototype["cop"]=true;
Iso639LangCodes_LangCodes.prototype["cor"]=true;
Iso639LangCodes_LangCodes.prototype["cos"]=true;
Iso639LangCodes_LangCodes.prototype["cpe"]=true;
Iso639LangCodes_LangCodes.prototype["cpf"]=true;
Iso639LangCodes_LangCodes.prototype["cpp"]=true;
Iso639LangCodes_LangCodes.prototype["cr"]=true;
Iso639LangCodes_LangCodes.prototype["cre"]=true;
Iso639LangCodes_LangCodes.prototype["crh"]=true;
Iso639LangCodes_LangCodes.prototype["crp"]=true;
Iso639LangCodes_LangCodes.prototype["cs"]=true;
Iso639LangCodes_LangCodes.prototype["cs"]=true;
Iso639LangCodes_LangCodes.prototype["csb"]=true;
Iso639LangCodes_LangCodes.prototype["cu"]=true;
Iso639LangCodes_LangCodes.prototype["cus"]=true;
Iso639LangCodes_LangCodes.prototype["cv"]=true;
Iso639LangCodes_LangCodes.prototype["cy"]=true;
Iso639LangCodes_LangCodes.prototype["cy"]=true;
Iso639LangCodes_LangCodes.prototype["cym"]=true;
Iso639LangCodes_LangCodes.prototype["cym"]=true;
Iso639LangCodes_LangCodes.prototype["cze"]=true;
Iso639LangCodes_LangCodes.prototype["cze"]=true;
Iso639LangCodes_LangCodes.prototype["da"]=true;
Iso639LangCodes_LangCodes.prototype["dak"]=true;
Iso639LangCodes_LangCodes.prototype["dan"]=true;
Iso639LangCodes_LangCodes.prototype["dar"]=true;
Iso639LangCodes_LangCodes.prototype["day"]=true;
Iso639LangCodes_LangCodes.prototype["de"]=true;
Iso639LangCodes_LangCodes.prototype["de"]=true;
Iso639LangCodes_LangCodes.prototype["del"]=true;
Iso639LangCodes_LangCodes.prototype["den"]=true;
Iso639LangCodes_LangCodes.prototype["deu"]=true;
Iso639LangCodes_LangCodes.prototype["deu"]=true;
Iso639LangCodes_LangCodes.prototype["dgr"]=true;
Iso639LangCodes_LangCodes.prototype["din"]=true;
Iso639LangCodes_LangCodes.prototype["div"]=true;
Iso639LangCodes_LangCodes.prototype["doi"]=true;
Iso639LangCodes_LangCodes.prototype["dra"]=true;
Iso639LangCodes_LangCodes.prototype["dsb"]=true;
Iso639LangCodes_LangCodes.prototype["dua"]=true;
Iso639LangCodes_LangCodes.prototype["dum"]=true;
Iso639LangCodes_LangCodes.prototype["dut"]=true;
Iso639LangCodes_LangCodes.prototype["dut"]=true;
Iso639LangCodes_LangCodes.prototype["dv"]=true;
Iso639LangCodes_LangCodes.prototype["dyu"]=true;
Iso639LangCodes_LangCodes.prototype["dz"]=true;
Iso639LangCodes_LangCodes.prototype["dzo"]=true;
Iso639LangCodes_LangCodes.prototype["ee"]=true;
Iso639LangCodes_LangCodes.prototype["efi"]=true;
Iso639LangCodes_LangCodes.prototype["egy"]=true;
Iso639LangCodes_LangCodes.prototype["eka"]=true;
Iso639LangCodes_LangCodes.prototype["el"]=true;
Iso639LangCodes_LangCodes.prototype["el"]=true;
Iso639LangCodes_LangCodes.prototype["ell"]=true;
Iso639LangCodes_LangCodes.prototype["ell"]=true;
Iso639LangCodes_LangCodes.prototype["elx"]=true;
Iso639LangCodes_LangCodes.prototype["en"]=true;
Iso639LangCodes_LangCodes.prototype["eng"]=true;
Iso639LangCodes_LangCodes.prototype["enm"]=true;
Iso639LangCodes_LangCodes.prototype["eo"]=true;
Iso639LangCodes_LangCodes.prototype["epo"]=true;
Iso639LangCodes_LangCodes.prototype["es"]=true;
Iso639LangCodes_LangCodes.prototype["est"]=true;
Iso639LangCodes_LangCodes.prototype["et"]=true;
Iso639LangCodes_LangCodes.prototype["eu"]=true;
Iso639LangCodes_LangCodes.prototype["eu"]=true;
Iso639LangCodes_LangCodes.prototype["eus"]=true;
Iso639LangCodes_LangCodes.prototype["eus"]=true;
Iso639LangCodes_LangCodes.prototype["ewe"]=true;
Iso639LangCodes_LangCodes.prototype["ewo"]=true;
Iso639LangCodes_LangCodes.prototype["fa"]=true;
Iso639LangCodes_LangCodes.prototype["fa"]=true;
Iso639LangCodes_LangCodes.prototype["fan"]=true;
Iso639LangCodes_LangCodes.prototype["fao"]=true;
Iso639LangCodes_LangCodes.prototype["fas"]=true;
Iso639LangCodes_LangCodes.prototype["fas"]=true;
Iso639LangCodes_LangCodes.prototype["fat"]=true;
Iso639LangCodes_LangCodes.prototype["ff"]=true;
Iso639LangCodes_LangCodes.prototype["fi"]=true;
Iso639LangCodes_LangCodes.prototype["fij"]=true;
Iso639LangCodes_LangCodes.prototype["fil"]=true;
Iso639LangCodes_LangCodes.prototype["fin"]=true;
Iso639LangCodes_LangCodes.prototype["fiu"]=true;
Iso639LangCodes_LangCodes.prototype["fj"]=true;
Iso639LangCodes_LangCodes.prototype["fo"]=true;
Iso639LangCodes_LangCodes.prototype["fon"]=true;
Iso639LangCodes_LangCodes.prototype["fr"]=true;
Iso639LangCodes_LangCodes.prototype["fra"]=true;
Iso639LangCodes_LangCodes.prototype["fre"]=true;
Iso639LangCodes_LangCodes.prototype["frm"]=true;
Iso639LangCodes_LangCodes.prototype["fro"]=true;
Iso639LangCodes_LangCodes.prototype["fry"]=true;
Iso639LangCodes_LangCodes.prototype["ful"]=true;
Iso639LangCodes_LangCodes.prototype["fur"]=true;
Iso639LangCodes_LangCodes.prototype["fy"]=true;
Iso639LangCodes_LangCodes.prototype["ga"]=true;
Iso639LangCodes_LangCodes.prototype["gaa"]=true;
Iso639LangCodes_LangCodes.prototype["gay"]=true;
Iso639LangCodes_LangCodes.prototype["gba"]=true;
Iso639LangCodes_LangCodes.prototype["gd"]=true;
Iso639LangCodes_LangCodes.prototype["gem"]=true;
Iso639LangCodes_LangCodes.prototype["geo"]=true;
Iso639LangCodes_LangCodes.prototype["geo"]=true;
Iso639LangCodes_LangCodes.prototype["ger"]=true;
Iso639LangCodes_LangCodes.prototype["ger"]=true;
Iso639LangCodes_LangCodes.prototype["gez"]=true;
Iso639LangCodes_LangCodes.prototype["gil"]=true;
Iso639LangCodes_LangCodes.prototype["gl"]=true;
Iso639LangCodes_LangCodes.prototype["gla"]=true;
Iso639LangCodes_LangCodes.prototype["gle"]=true;
Iso639LangCodes_LangCodes.prototype["glg"]=true;
Iso639LangCodes_LangCodes.prototype["glv"]=true;
Iso639LangCodes_LangCodes.prototype["gmh"]=true;
Iso639LangCodes_LangCodes.prototype["gn"]=true;
Iso639LangCodes_LangCodes.prototype["goh"]=true;
Iso639LangCodes_LangCodes.prototype["gon"]=true;
Iso639LangCodes_LangCodes.prototype["gor"]=true;
Iso639LangCodes_LangCodes.prototype["got"]=true;
Iso639LangCodes_LangCodes.prototype["grb"]=true;
Iso639LangCodes_LangCodes.prototype["grc"]=true;
Iso639LangCodes_LangCodes.prototype["gre"]=true;
Iso639LangCodes_LangCodes.prototype["gre"]=true;
Iso639LangCodes_LangCodes.prototype["grn"]=true;
Iso639LangCodes_LangCodes.prototype["gu"]=true;
Iso639LangCodes_LangCodes.prototype["guj"]=true;
Iso639LangCodes_LangCodes.prototype["gv"]=true;
Iso639LangCodes_LangCodes.prototype["gwi"]=true;
Iso639LangCodes_LangCodes.prototype["ha"]=true;
Iso639LangCodes_LangCodes.prototype["hai"]=true;
Iso639LangCodes_LangCodes.prototype["hat"]=true;
Iso639LangCodes_LangCodes.prototype["hau"]=true;
Iso639LangCodes_LangCodes.prototype["haw"]=true;
Iso639LangCodes_LangCodes.prototype["he"]=true;
Iso639LangCodes_LangCodes.prototype["heb"]=true;
Iso639LangCodes_LangCodes.prototype["her"]=true;
Iso639LangCodes_LangCodes.prototype["hi"]=true;
Iso639LangCodes_LangCodes.prototype["hil"]=true;
Iso639LangCodes_LangCodes.prototype["him"]=true;
Iso639LangCodes_LangCodes.prototype["hin"]=true;
Iso639LangCodes_LangCodes.prototype["hit"]=true;
Iso639LangCodes_LangCodes.prototype["hmn"]=true;
Iso639LangCodes_LangCodes.prototype["hmo"]=true;
Iso639LangCodes_LangCodes.prototype["ho"]=true;
Iso639LangCodes_LangCodes.prototype["hr"]=true;
Iso639LangCodes_LangCodes.prototype["hr"]=true;
Iso639LangCodes_LangCodes.prototype["hrv"]=true;
Iso639LangCodes_LangCodes.prototype["hrv"]=true;
Iso639LangCodes_LangCodes.prototype["hsb"]=true;
Iso639LangCodes_LangCodes.prototype["ht"]=true;
Iso639LangCodes_LangCodes.prototype["hu"]=true;
Iso639LangCodes_LangCodes.prototype["hun"]=true;
Iso639LangCodes_LangCodes.prototype["hup"]=true;
Iso639LangCodes_LangCodes.prototype["hy"]=true;
Iso639LangCodes_LangCodes.prototype["hy"]=true;
Iso639LangCodes_LangCodes.prototype["hye"]=true;
Iso639LangCodes_LangCodes.prototype["hye"]=true;
Iso639LangCodes_LangCodes.prototype["hz"]=true;
Iso639LangCodes_LangCodes.prototype["ia"]=true;
Iso639LangCodes_LangCodes.prototype["iba"]=true;
Iso639LangCodes_LangCodes.prototype["ibo"]=true;
Iso639LangCodes_LangCodes.prototype["ice"]=true;
Iso639LangCodes_LangCodes.prototype["ice"]=true;
Iso639LangCodes_LangCodes.prototype["id"]=true;
Iso639LangCodes_LangCodes.prototype["ido"]=true;
Iso639LangCodes_LangCodes.prototype["ie"]=true;
Iso639LangCodes_LangCodes.prototype["ig"]=true;
Iso639LangCodes_LangCodes.prototype["ii"]=true;
Iso639LangCodes_LangCodes.prototype["iii"]=true;
Iso639LangCodes_LangCodes.prototype["ijo"]=true;
Iso639LangCodes_LangCodes.prototype["ik"]=true;
Iso639LangCodes_LangCodes.prototype["iku"]=true;
Iso639LangCodes_LangCodes.prototype["ile"]=true;
Iso639LangCodes_LangCodes.prototype["ilo"]=true;
Iso639LangCodes_LangCodes.prototype["ina"]=true;
Iso639LangCodes_LangCodes.prototype["inc"]=true;
Iso639LangCodes_LangCodes.prototype["ind"]=true;
Iso639LangCodes_LangCodes.prototype["ine"]=true;
Iso639LangCodes_LangCodes.prototype["inh"]=true;
Iso639LangCodes_LangCodes.prototype["io"]=true;
Iso639LangCodes_LangCodes.prototype["ipk"]=true;
Iso639LangCodes_LangCodes.prototype["ira"]=true;
Iso639LangCodes_LangCodes.prototype["iro"]=true;
Iso639LangCodes_LangCodes.prototype["is"]=true;
Iso639LangCodes_LangCodes.prototype["is"]=true;
Iso639LangCodes_LangCodes.prototype["isl"]=true;
Iso639LangCodes_LangCodes.prototype["isl"]=true;
Iso639LangCodes_LangCodes.prototype["it"]=true;
Iso639LangCodes_LangCodes.prototype["ita"]=true;
Iso639LangCodes_LangCodes.prototype["iu"]=true;
Iso639LangCodes_LangCodes.prototype["ja"]=true;
Iso639LangCodes_LangCodes.prototype["jav"]=true;
Iso639LangCodes_LangCodes.prototype["jbo"]=true;
Iso639LangCodes_LangCodes.prototype["jpn"]=true;
Iso639LangCodes_LangCodes.prototype["jpr"]=true;
Iso639LangCodes_LangCodes.prototype["jrb"]=true;
Iso639LangCodes_LangCodes.prototype["jv"]=true;
Iso639LangCodes_LangCodes.prototype["ka"]=true;
Iso639LangCodes_LangCodes.prototype["ka"]=true;
Iso639LangCodes_LangCodes.prototype["kaa"]=true;
Iso639LangCodes_LangCodes.prototype["kab"]=true;
Iso639LangCodes_LangCodes.prototype["kac"]=true;
Iso639LangCodes_LangCodes.prototype["kal"]=true;
Iso639LangCodes_LangCodes.prototype["kam"]=true;
Iso639LangCodes_LangCodes.prototype["kan"]=true;
Iso639LangCodes_LangCodes.prototype["kar"]=true;
Iso639LangCodes_LangCodes.prototype["kas"]=true;
Iso639LangCodes_LangCodes.prototype["kat"]=true;
Iso639LangCodes_LangCodes.prototype["kat"]=true;
Iso639LangCodes_LangCodes.prototype["kau"]=true;
Iso639LangCodes_LangCodes.prototype["kaw"]=true;
Iso639LangCodes_LangCodes.prototype["kaz"]=true;
Iso639LangCodes_LangCodes.prototype["kbd"]=true;
Iso639LangCodes_LangCodes.prototype["kg"]=true;
Iso639LangCodes_LangCodes.prototype["kha"]=true;
Iso639LangCodes_LangCodes.prototype["khi"]=true;
Iso639LangCodes_LangCodes.prototype["khm"]=true;
Iso639LangCodes_LangCodes.prototype["kho"]=true;
Iso639LangCodes_LangCodes.prototype["ki"]=true;
Iso639LangCodes_LangCodes.prototype["kik"]=true;
Iso639LangCodes_LangCodes.prototype["kin"]=true;
Iso639LangCodes_LangCodes.prototype["kir"]=true;
Iso639LangCodes_LangCodes.prototype["kj"]=true;
Iso639LangCodes_LangCodes.prototype["kk"]=true;
Iso639LangCodes_LangCodes.prototype["kl"]=true;
Iso639LangCodes_LangCodes.prototype["km"]=true;
Iso639LangCodes_LangCodes.prototype["kmb"]=true;
Iso639LangCodes_LangCodes.prototype["kn"]=true;
Iso639LangCodes_LangCodes.prototype["ko"]=true;
Iso639LangCodes_LangCodes.prototype["kok"]=true;
Iso639LangCodes_LangCodes.prototype["kom"]=true;
Iso639LangCodes_LangCodes.prototype["kon"]=true;
Iso639LangCodes_LangCodes.prototype["kor"]=true;
Iso639LangCodes_LangCodes.prototype["kos"]=true;
Iso639LangCodes_LangCodes.prototype["kpe"]=true;
Iso639LangCodes_LangCodes.prototype["kr"]=true;
Iso639LangCodes_LangCodes.prototype["krc"]=true;
Iso639LangCodes_LangCodes.prototype["kro"]=true;
Iso639LangCodes_LangCodes.prototype["kru"]=true;
Iso639LangCodes_LangCodes.prototype["ks"]=true;
Iso639LangCodes_LangCodes.prototype["ku"]=true;
Iso639LangCodes_LangCodes.prototype["kua"]=true;
Iso639LangCodes_LangCodes.prototype["kum"]=true;
Iso639LangCodes_LangCodes.prototype["kur"]=true;
Iso639LangCodes_LangCodes.prototype["kut"]=true;
Iso639LangCodes_LangCodes.prototype["kv"]=true;
Iso639LangCodes_LangCodes.prototype["kw"]=true;
Iso639LangCodes_LangCodes.prototype["ky"]=true;
Iso639LangCodes_LangCodes.prototype["la"]=true;
Iso639LangCodes_LangCodes.prototype["lad"]=true;
Iso639LangCodes_LangCodes.prototype["lah"]=true;
Iso639LangCodes_LangCodes.prototype["lam"]=true;
Iso639LangCodes_LangCodes.prototype["lao"]=true;
Iso639LangCodes_LangCodes.prototype["lat"]=true;
Iso639LangCodes_LangCodes.prototype["lav"]=true;
Iso639LangCodes_LangCodes.prototype["lb"]=true;
Iso639LangCodes_LangCodes.prototype["lez"]=true;
Iso639LangCodes_LangCodes.prototype["lg"]=true;
Iso639LangCodes_LangCodes.prototype["li"]=true;
Iso639LangCodes_LangCodes.prototype["lim"]=true;
Iso639LangCodes_LangCodes.prototype["lin"]=true;
Iso639LangCodes_LangCodes.prototype["lit"]=true;
Iso639LangCodes_LangCodes.prototype["ln"]=true;
Iso639LangCodes_LangCodes.prototype["lo"]=true;
Iso639LangCodes_LangCodes.prototype["lol"]=true;
Iso639LangCodes_LangCodes.prototype["loz"]=true;
Iso639LangCodes_LangCodes.prototype["lt"]=true;
Iso639LangCodes_LangCodes.prototype["ltz"]=true;
Iso639LangCodes_LangCodes.prototype["lu"]=true;
Iso639LangCodes_LangCodes.prototype["lua"]=true;
Iso639LangCodes_LangCodes.prototype["lub"]=true;
Iso639LangCodes_LangCodes.prototype["lug"]=true;
Iso639LangCodes_LangCodes.prototype["lui"]=true;
Iso639LangCodes_LangCodes.prototype["lun"]=true;
Iso639LangCodes_LangCodes.prototype["luo"]=true;
Iso639LangCodes_LangCodes.prototype["lus"]=true;
Iso639LangCodes_LangCodes.prototype["lv"]=true;
Iso639LangCodes_LangCodes.prototype["mac"]=true;
Iso639LangCodes_LangCodes.prototype["mad"]=true;
Iso639LangCodes_LangCodes.prototype["mag"]=true;
Iso639LangCodes_LangCodes.prototype["mah"]=true;
Iso639LangCodes_LangCodes.prototype["mai"]=true;
Iso639LangCodes_LangCodes.prototype["mak"]=true;
Iso639LangCodes_LangCodes.prototype["mal"]=true;
Iso639LangCodes_LangCodes.prototype["man"]=true;
Iso639LangCodes_LangCodes.prototype["mao"]=true;
Iso639LangCodes_LangCodes.prototype["mao"]=true;
Iso639LangCodes_LangCodes.prototype["map"]=true;
Iso639LangCodes_LangCodes.prototype["mar"]=true;
Iso639LangCodes_LangCodes.prototype["mas"]=true;
Iso639LangCodes_LangCodes.prototype["may"]=true;
Iso639LangCodes_LangCodes.prototype["may"]=true;
Iso639LangCodes_LangCodes.prototype["mdf"]=true;
Iso639LangCodes_LangCodes.prototype["mdr"]=true;
Iso639LangCodes_LangCodes.prototype["men"]=true;
Iso639LangCodes_LangCodes.prototype["mg"]=true;
Iso639LangCodes_LangCodes.prototype["mga"]=true;
Iso639LangCodes_LangCodes.prototype["mh"]=true;
Iso639LangCodes_LangCodes.prototype["mi"]=true;
Iso639LangCodes_LangCodes.prototype["mi"]=true;
Iso639LangCodes_LangCodes.prototype["mic"]=true;
Iso639LangCodes_LangCodes.prototype["min"]=true;
Iso639LangCodes_LangCodes.prototype["mis"]=true;
Iso639LangCodes_LangCodes.prototype["mk"]=true;
Iso639LangCodes_LangCodes.prototype["mk"]=true;
Iso639LangCodes_LangCodes.prototype["mkd"]=true;
Iso639LangCodes_LangCodes.prototype["mkd"]=true;
Iso639LangCodes_LangCodes.prototype["mkh"]=true;
Iso639LangCodes_LangCodes.prototype["ml"]=true;
Iso639LangCodes_LangCodes.prototype["mlg"]=true;
Iso639LangCodes_LangCodes.prototype["mlt"]=true;
Iso639LangCodes_LangCodes.prototype["mn"]=true;
Iso639LangCodes_LangCodes.prototype["mnc"]=true;
Iso639LangCodes_LangCodes.prototype["mni"]=true;
Iso639LangCodes_LangCodes.prototype["mno"]=true;
Iso639LangCodes_LangCodes.prototype["mo"]=true;
Iso639LangCodes_LangCodes.prototype["moh"]=true;
Iso639LangCodes_LangCodes.prototype["mol"]=true;
Iso639LangCodes_LangCodes.prototype["mon"]=true;
Iso639LangCodes_LangCodes.prototype["mos"]=true;
Iso639LangCodes_LangCodes.prototype["mr"]=true;
Iso639LangCodes_LangCodes.prototype["mri"]=true;
Iso639LangCodes_LangCodes.prototype["mri"]=true;
Iso639LangCodes_LangCodes.prototype["ms"]=true;
Iso639LangCodes_LangCodes.prototype["ms"]=true;
Iso639LangCodes_LangCodes.prototype["msa"]=true;
Iso639LangCodes_LangCodes.prototype["msa"]=true;
Iso639LangCodes_LangCodes.prototype["mt"]=true;
Iso639LangCodes_LangCodes.prototype["mul"]=true;
Iso639LangCodes_LangCodes.prototype["mun"]=true;
Iso639LangCodes_LangCodes.prototype["mus"]=true;
Iso639LangCodes_LangCodes.prototype["mwl"]=true;
Iso639LangCodes_LangCodes.prototype["mwr"]=true;
Iso639LangCodes_LangCodes.prototype["my"]=true;
Iso639LangCodes_LangCodes.prototype["my"]=true;
Iso639LangCodes_LangCodes.prototype["mya"]=true;
Iso639LangCodes_LangCodes.prototype["mya"]=true;
Iso639LangCodes_LangCodes.prototype["myn"]=true;
Iso639LangCodes_LangCodes.prototype["myv"]=true;
Iso639LangCodes_LangCodes.prototype["na"]=true;
Iso639LangCodes_LangCodes.prototype["nah"]=true;
Iso639LangCodes_LangCodes.prototype["nai"]=true;
Iso639LangCodes_LangCodes.prototype["nap"]=true;
Iso639LangCodes_LangCodes.prototype["nau"]=true;
Iso639LangCodes_LangCodes.prototype["nav"]=true;
Iso639LangCodes_LangCodes.prototype["nb"]=true;
Iso639LangCodes_LangCodes.prototype["nbl"]=true;
Iso639LangCodes_LangCodes.prototype["nd"]=true;
Iso639LangCodes_LangCodes.prototype["nde"]=true;
Iso639LangCodes_LangCodes.prototype["ndo"]=true;
Iso639LangCodes_LangCodes.prototype["nds"]=true;
Iso639LangCodes_LangCodes.prototype["ne"]=true;
Iso639LangCodes_LangCodes.prototype["nep"]=true;
Iso639LangCodes_LangCodes.prototype["new"]=true;
Iso639LangCodes_LangCodes.prototype["ng"]=true;
Iso639LangCodes_LangCodes.prototype["nia"]=true;
Iso639LangCodes_LangCodes.prototype["nic"]=true;
Iso639LangCodes_LangCodes.prototype["niu"]=true;
Iso639LangCodes_LangCodes.prototype["nl"]=true;
Iso639LangCodes_LangCodes.prototype["nl"]=true;
Iso639LangCodes_LangCodes.prototype["nld"]=true;
Iso639LangCodes_LangCodes.prototype["nld"]=true;
Iso639LangCodes_LangCodes.prototype["nn"]=true;
Iso639LangCodes_LangCodes.prototype["nno"]=true;
Iso639LangCodes_LangCodes.prototype["no"]=true;
Iso639LangCodes_LangCodes.prototype["nob"]=true;
Iso639LangCodes_LangCodes.prototype["nog"]=true;
Iso639LangCodes_LangCodes.prototype["non"]=true;
Iso639LangCodes_LangCodes.prototype["nor"]=true;
Iso639LangCodes_LangCodes.prototype["nr"]=true;
Iso639LangCodes_LangCodes.prototype["nso"]=true;
Iso639LangCodes_LangCodes.prototype["nub"]=true;
Iso639LangCodes_LangCodes.prototype["nv"]=true;
Iso639LangCodes_LangCodes.prototype["nwc"]=true;
Iso639LangCodes_LangCodes.prototype["ny"]=true;
Iso639LangCodes_LangCodes.prototype["nya"]=true;
Iso639LangCodes_LangCodes.prototype["nym"]=true;
Iso639LangCodes_LangCodes.prototype["nyn"]=true;
Iso639LangCodes_LangCodes.prototype["nyo"]=true;
Iso639LangCodes_LangCodes.prototype["nzi"]=true;
Iso639LangCodes_LangCodes.prototype["oc"]=true;
Iso639LangCodes_LangCodes.prototype["oci"]=true;
Iso639LangCodes_LangCodes.prototype["oj"]=true;
Iso639LangCodes_LangCodes.prototype["oji"]=true;
Iso639LangCodes_LangCodes.prototype["om"]=true;
Iso639LangCodes_LangCodes.prototype["or"]=true;
Iso639LangCodes_LangCodes.prototype["ori"]=true;
Iso639LangCodes_LangCodes.prototype["orm"]=true;
Iso639LangCodes_LangCodes.prototype["os"]=true;
Iso639LangCodes_LangCodes.prototype["osa"]=true;
Iso639LangCodes_LangCodes.prototype["oss"]=true;
Iso639LangCodes_LangCodes.prototype["ota"]=true;
Iso639LangCodes_LangCodes.prototype["oto"]=true;
Iso639LangCodes_LangCodes.prototype["pa"]=true;
Iso639LangCodes_LangCodes.prototype["paa"]=true;
Iso639LangCodes_LangCodes.prototype["pag"]=true;
Iso639LangCodes_LangCodes.prototype["pal"]=true;
Iso639LangCodes_LangCodes.prototype["pam"]=true;
Iso639LangCodes_LangCodes.prototype["pan"]=true;
Iso639LangCodes_LangCodes.prototype["pap"]=true;
Iso639LangCodes_LangCodes.prototype["pau"]=true;
Iso639LangCodes_LangCodes.prototype["peo"]=true;
Iso639LangCodes_LangCodes.prototype["per"]=true;
Iso639LangCodes_LangCodes.prototype["per"]=true;
Iso639LangCodes_LangCodes.prototype["phi"]=true;
Iso639LangCodes_LangCodes.prototype["phn"]=true;
Iso639LangCodes_LangCodes.prototype["pi"]=true;
Iso639LangCodes_LangCodes.prototype["pl"]=true;
Iso639LangCodes_LangCodes.prototype["pli"]=true;
Iso639LangCodes_LangCodes.prototype["pol"]=true;
Iso639LangCodes_LangCodes.prototype["pon"]=true;
Iso639LangCodes_LangCodes.prototype["por"]=true;
Iso639LangCodes_LangCodes.prototype["pra"]=true;
Iso639LangCodes_LangCodes.prototype["pro"]=true;
Iso639LangCodes_LangCodes.prototype["ps"]=true;
Iso639LangCodes_LangCodes.prototype["pt"]=true;
Iso639LangCodes_LangCodes.prototype["pus"]=true;
Iso639LangCodes_LangCodes.prototype["qaa"]=true;
Iso639LangCodes_LangCodes.prototype["qtz"]=true;
Iso639LangCodes_LangCodes.prototype["qu"]=true;
Iso639LangCodes_LangCodes.prototype["que"]=true;
Iso639LangCodes_LangCodes.prototype["raj"]=true;
Iso639LangCodes_LangCodes.prototype["rap"]=true;
Iso639LangCodes_LangCodes.prototype["rar"]=true;
Iso639LangCodes_LangCodes.prototype["rm"]=true;
Iso639LangCodes_LangCodes.prototype["rn"]=true;
Iso639LangCodes_LangCodes.prototype["ro"]=true;
Iso639LangCodes_LangCodes.prototype["roa"]=true;
Iso639LangCodes_LangCodes.prototype["roh"]=true;
Iso639LangCodes_LangCodes.prototype["rom"]=true;
Iso639LangCodes_LangCodes.prototype["ron"]=true;
Iso639LangCodes_LangCodes.prototype["ru"]=true;
Iso639LangCodes_LangCodes.prototype["rum"]=true;
Iso639LangCodes_LangCodes.prototype["run"]=true;
Iso639LangCodes_LangCodes.prototype["rus"]=true;
Iso639LangCodes_LangCodes.prototype["rw"]=true;
Iso639LangCodes_LangCodes.prototype["sa"]=true;
Iso639LangCodes_LangCodes.prototype["sad"]=true;
Iso639LangCodes_LangCodes.prototype["sag"]=true;
Iso639LangCodes_LangCodes.prototype["sah"]=true;
Iso639LangCodes_LangCodes.prototype["sai"]=true;
Iso639LangCodes_LangCodes.prototype["sal"]=true;
Iso639LangCodes_LangCodes.prototype["sam"]=true;
Iso639LangCodes_LangCodes.prototype["san"]=true;
Iso639LangCodes_LangCodes.prototype["sas"]=true;
Iso639LangCodes_LangCodes.prototype["sat"]=true;
Iso639LangCodes_LangCodes.prototype["sc"]=true;
Iso639LangCodes_LangCodes.prototype["scc"]=true;
Iso639LangCodes_LangCodes.prototype["scc"]=true;
Iso639LangCodes_LangCodes.prototype["scn"]=true;
Iso639LangCodes_LangCodes.prototype["sco"]=true;
Iso639LangCodes_LangCodes.prototype["scr"]=true;
Iso639LangCodes_LangCodes.prototype["scr"]=true;
Iso639LangCodes_LangCodes.prototype["sd"]=true;
Iso639LangCodes_LangCodes.prototype["se"]=true;
Iso639LangCodes_LangCodes.prototype["sel"]=true;
Iso639LangCodes_LangCodes.prototype["sem"]=true;
Iso639LangCodes_LangCodes.prototype["sg"]=true;
Iso639LangCodes_LangCodes.prototype["sga"]=true;
Iso639LangCodes_LangCodes.prototype["sgn"]=true;
Iso639LangCodes_LangCodes.prototype["shn"]=true;
Iso639LangCodes_LangCodes.prototype["si"]=true;
Iso639LangCodes_LangCodes.prototype["sid"]=true;
Iso639LangCodes_LangCodes.prototype["sin"]=true;
Iso639LangCodes_LangCodes.prototype["sio"]=true;
Iso639LangCodes_LangCodes.prototype["sit"]=true;
Iso639LangCodes_LangCodes.prototype["sk"]=true;
Iso639LangCodes_LangCodes.prototype["sl"]=true;
Iso639LangCodes_LangCodes.prototype["sla"]=true;
Iso639LangCodes_LangCodes.prototype["slk"]=true;
Iso639LangCodes_LangCodes.prototype["slo"]=true;
Iso639LangCodes_LangCodes.prototype["slv"]=true;
Iso639LangCodes_LangCodes.prototype["sm"]=true;
Iso639LangCodes_LangCodes.prototype["sma"]=true;
Iso639LangCodes_LangCodes.prototype["sme"]=true;
Iso639LangCodes_LangCodes.prototype["smi"]=true;
Iso639LangCodes_LangCodes.prototype["smj"]=true;
Iso639LangCodes_LangCodes.prototype["smn"]=true;
Iso639LangCodes_LangCodes.prototype["smo"]=true;
Iso639LangCodes_LangCodes.prototype["sms"]=true;
Iso639LangCodes_LangCodes.prototype["sn"]=true;
Iso639LangCodes_LangCodes.prototype["sna"]=true;
Iso639LangCodes_LangCodes.prototype["snd"]=true;
Iso639LangCodes_LangCodes.prototype["snk"]=true;
Iso639LangCodes_LangCodes.prototype["so"]=true;
Iso639LangCodes_LangCodes.prototype["sog"]=true;
Iso639LangCodes_LangCodes.prototype["som"]=true;
Iso639LangCodes_LangCodes.prototype["son"]=true;
Iso639LangCodes_LangCodes.prototype["sot"]=true;
Iso639LangCodes_LangCodes.prototype["spa"]=true;
Iso639LangCodes_LangCodes.prototype["sq"]=true;
Iso639LangCodes_LangCodes.prototype["sqi"]=true;
Iso639LangCodes_LangCodes.prototype["sr"]=true;
Iso639LangCodes_LangCodes.prototype["srd"]=true;
Iso639LangCodes_LangCodes.prototype["srp"]=true;
Iso639LangCodes_LangCodes.prototype["srr"]=true;
Iso639LangCodes_LangCodes.prototype["ss"]=true;
Iso639LangCodes_LangCodes.prototype["ssa"]=true;
Iso639LangCodes_LangCodes.prototype["ssw"]=true;
Iso639LangCodes_LangCodes.prototype["st"]=true;
Iso639LangCodes_LangCodes.prototype["su"]=true;
Iso639LangCodes_LangCodes.prototype["suk"]=true;
Iso639LangCodes_LangCodes.prototype["sun"]=true;
Iso639LangCodes_LangCodes.prototype["sus"]=true;
Iso639LangCodes_LangCodes.prototype["sux"]=true;
Iso639LangCodes_LangCodes.prototype["sv"]=true;
Iso639LangCodes_LangCodes.prototype["sw"]=true;
Iso639LangCodes_LangCodes.prototype["swa"]=true;
Iso639LangCodes_LangCodes.prototype["swe"]=true;
Iso639LangCodes_LangCodes.prototype["syr"]=true;
Iso639LangCodes_LangCodes.prototype["ta"]=true;
Iso639LangCodes_LangCodes.prototype["tah"]=true;
Iso639LangCodes_LangCodes.prototype["tai"]=true;
Iso639LangCodes_LangCodes.prototype["tam"]=true;
Iso639LangCodes_LangCodes.prototype["tat"]=true;
Iso639LangCodes_LangCodes.prototype["te"]=true;
Iso639LangCodes_LangCodes.prototype["tel"]=true;
Iso639LangCodes_LangCodes.prototype["tem"]=true;
Iso639LangCodes_LangCodes.prototype["ter"]=true;
Iso639LangCodes_LangCodes.prototype["tet"]=true;
Iso639LangCodes_LangCodes.prototype["tg"]=true;
Iso639LangCodes_LangCodes.prototype["tgk"]=true;
Iso639LangCodes_LangCodes.prototype["tgl"]=true;
Iso639LangCodes_LangCodes.prototype["th"]=true;
Iso639LangCodes_LangCodes.prototype["tha"]=true;
Iso639LangCodes_LangCodes.prototype["ti"]=true;
Iso639LangCodes_LangCodes.prototype["tib"]=true;
Iso639LangCodes_LangCodes.prototype["tib"]=true;
Iso639LangCodes_LangCodes.prototype["tig"]=true;
Iso639LangCodes_LangCodes.prototype["tir"]=true;
Iso639LangCodes_LangCodes.prototype["tiv"]=true;
Iso639LangCodes_LangCodes.prototype["tk"]=true;
Iso639LangCodes_LangCodes.prototype["tkl"]=true;
Iso639LangCodes_LangCodes.prototype["tl"]=true;
Iso639LangCodes_LangCodes.prototype["tlh"]=true;
Iso639LangCodes_LangCodes.prototype["tli"]=true;
Iso639LangCodes_LangCodes.prototype["tmh"]=true;
Iso639LangCodes_LangCodes.prototype["tn"]=true;
Iso639LangCodes_LangCodes.prototype["to"]=true;
Iso639LangCodes_LangCodes.prototype["tog"]=true;
Iso639LangCodes_LangCodes.prototype["ton"]=true;
Iso639LangCodes_LangCodes.prototype["tpi"]=true;
Iso639LangCodes_LangCodes.prototype["tr"]=true;
Iso639LangCodes_LangCodes.prototype["ts"]=true;
Iso639LangCodes_LangCodes.prototype["tsi"]=true;
Iso639LangCodes_LangCodes.prototype["tsn"]=true;
Iso639LangCodes_LangCodes.prototype["tso"]=true;
Iso639LangCodes_LangCodes.prototype["tt"]=true;
Iso639LangCodes_LangCodes.prototype["tuk"]=true;
Iso639LangCodes_LangCodes.prototype["tum"]=true;
Iso639LangCodes_LangCodes.prototype["tup"]=true;
Iso639LangCodes_LangCodes.prototype["tur"]=true;
Iso639LangCodes_LangCodes.prototype["tut"]=true;
Iso639LangCodes_LangCodes.prototype["tvl"]=true;
Iso639LangCodes_LangCodes.prototype["tw"]=true;
Iso639LangCodes_LangCodes.prototype["twi"]=true;
Iso639LangCodes_LangCodes.prototype["ty"]=true;
Iso639LangCodes_LangCodes.prototype["tyv"]=true;
Iso639LangCodes_LangCodes.prototype["udm"]=true;
Iso639LangCodes_LangCodes.prototype["ug"]=true;
Iso639LangCodes_LangCodes.prototype["uga"]=true;
Iso639LangCodes_LangCodes.prototype["uig"]=true;
Iso639LangCodes_LangCodes.prototype["uk"]=true;
Iso639LangCodes_LangCodes.prototype["ukr"]=true;
Iso639LangCodes_LangCodes.prototype["umb"]=true;
Iso639LangCodes_LangCodes.prototype["und"]=true;
Iso639LangCodes_LangCodes.prototype["ur"]=true;
Iso639LangCodes_LangCodes.prototype["urd"]=true;
Iso639LangCodes_LangCodes.prototype["uz"]=true;
Iso639LangCodes_LangCodes.prototype["uzb"]=true;
Iso639LangCodes_LangCodes.prototype["vai"]=true;
Iso639LangCodes_LangCodes.prototype["ve"]=true;
Iso639LangCodes_LangCodes.prototype["ven"]=true;
Iso639LangCodes_LangCodes.prototype["vi"]=true;
Iso639LangCodes_LangCodes.prototype["vie"]=true;
Iso639LangCodes_LangCodes.prototype["vo"]=true;
Iso639LangCodes_LangCodes.prototype["vol"]=true;
Iso639LangCodes_LangCodes.prototype["vot"]=true;
Iso639LangCodes_LangCodes.prototype["wa"]=true;
Iso639LangCodes_LangCodes.prototype["wak"]=true;
Iso639LangCodes_LangCodes.prototype["wal"]=true;
Iso639LangCodes_LangCodes.prototype["war"]=true;
Iso639LangCodes_LangCodes.prototype["was"]=true;
Iso639LangCodes_LangCodes.prototype["wel"]=true;
Iso639LangCodes_LangCodes.prototype["wel"]=true;
Iso639LangCodes_LangCodes.prototype["wen"]=true;
Iso639LangCodes_LangCodes.prototype["wln"]=true;
Iso639LangCodes_LangCodes.prototype["wo"]=true;
Iso639LangCodes_LangCodes.prototype["wol"]=true;
Iso639LangCodes_LangCodes.prototype["xal"]=true;
Iso639LangCodes_LangCodes.prototype["xh"]=true;
Iso639LangCodes_LangCodes.prototype["xho"]=true;
Iso639LangCodes_LangCodes.prototype["yao"]=true;
Iso639LangCodes_LangCodes.prototype["yap"]=true;
Iso639LangCodes_LangCodes.prototype["yi"]=true;
Iso639LangCodes_LangCodes.prototype["yid"]=true;
Iso639LangCodes_LangCodes.prototype["yo"]=true;
Iso639LangCodes_LangCodes.prototype["yor"]=true;
Iso639LangCodes_LangCodes.prototype["ypk"]=true;
Iso639LangCodes_LangCodes.prototype["za"]=true;
Iso639LangCodes_LangCodes.prototype["zap"]=true;
Iso639LangCodes_LangCodes.prototype["zen"]=true;
Iso639LangCodes_LangCodes.prototype["zh"]=true;
Iso639LangCodes_LangCodes.prototype["zh"]=true;
Iso639LangCodes_LangCodes.prototype["zha"]=true;
Iso639LangCodes_LangCodes.prototype["zho"]=true;
Iso639LangCodes_LangCodes.prototype["zho"]=true;
Iso639LangCodes_LangCodes.prototype["znd"]=true;
Iso639LangCodes_LangCodes.prototype["zu"]=true;
Iso639LangCodes_LangCodes.prototype["zul"]=true;
Iso639LangCodes_LangCodes.prototype["zun"]=true;
Iso639LangCodes_LangCodes.prototype.IsValid=Iso639LangCodes_IsValid;
function Iso639LangCodes_IsValid(str){
if(str=="i"||str=="x"){
return true;
}
if(this[str]===true){
return true;
}
return false;
}
function Iso639LangCodes_LangCodes(){
}
function LogCompression(_111){
this.dictionary=_111;
this.dictKeyDelimiter=this.dictionary["delimiter"];
}
LogCompression.prototype.decompressXmltoString=function(_112){
var str=_112.toString();
var _114=this.dictionary;
var _115=new RegExp(this.dictKeyDelimiter+"([0-9]{1,4})"+this.dictKeyDelimiter,"g");
function getDictionaryValueforStrReplace(){
return _114[arguments[1]];
}
return str.replace(_115,getDictionaryValueforStrReplace);
};
LogCompression.prototype.decompressString=function(_116){
var _117=this.dictionary;
var _118=new RegExp(this.dictKeyDelimiter+"([0-9]{1,4})"+this.dictKeyDelimiter,"g");
function getDictionaryValueforStrReplace(){
return _117[arguments[1]];
}
return _116.replace(_118,getDictionaryValueforStrReplace);
};
function MenuItem(_119,_11a,_11b,_11c){
this.ParentMenuItem=_119;
this.Document=_11b;
this.ActivityId=_11a.GetItemIdentifier()+_11a.GetDatabaseIdentifier();
this.MenuElementId="MenuItem"+this.ActivityId;
this.DivTag=null;
if(_119===null){
this.Level=0;
}else{
this.Level=_119.Level+1;
}
this.Children=new Array();
this.Visible=true;
this.Enabled=true;
var node=this.Document.createElement("div");
node.id=this.MenuElementId;
this.DivTag=node;
this.CurrentDisplayState=null;
this.CurrentDisplayState=IntegrationImplementation.PopulateMenuItemDivTag(node,this.Document,this.ActivityId,_11a.GetTitle(),this.Level,_11a.IsDeliverable(),Control.Package.LearningStandard,Control.Package.Properties.StatusDisplay,_11a.GetItemIdentifier());
}
MenuItem.prototype.Render=MenuItem_Render;
MenuItem.prototype.UpdateStateDisplay=MenuItem_UpdateStateDisplay;
MenuItem.prototype.Hide=MenuItem_Hide;
MenuItem.prototype.Show=MenuItem_Show;
MenuItem.prototype.Enable=MenuItem_Enable;
MenuItem.prototype.Disable=MenuItem_Disable;
MenuItem.prototype.BumpUpLevel=MenuItem_BumpUpLevel;
MenuItem.prototype.BumpDownLevel=MenuItem_BumpDownLevel;
MenuItem.prototype.ResynchChildren=MenuItem_ResynchChildren;
function MenuItem_Render(_11e){
var _11f;
if(this.ParentMenuItem===null){
_11f=IntegrationImplementation.GetHtmlElementToInsertMenuWithin();
_11f.insertBefore(this.DivTag,null);
return;
}else{
var i;
for(i=0;i<this.ParentMenuItem.Children.length;i++){
if(this.ParentMenuItem.Children[i].ActivityId==this.ActivityId){
break;
}
}
if(i===0){
_11f=this.Document.getElementById(this.ParentMenuItem.MenuElementId);
}else{
var _121=this.ParentMenuItem.Children[i-1];
var _122=MenuItem_getLastSubmenuItem(_121);
_11f=this.Document.getElementById(_122.MenuElementId);
}
}
_11f.parentNode.insertBefore(this.DivTag,_11f.nextSibling);
if(_11e.DisplayInChoice()===true){
this.Show();
}else{
this.Hide();
}
}
function MenuItem_UpdateStateDisplay(_123,_124,_125,_126){
var _127=true;
var _128=true;
if(_123.DisplayInChoice()===true){
_127=true;
}else{
_127=false;
}
if(!_125.WillSucceed){
if(Control.Package.Properties.InvalidMenuItemAction==INVALID_MENU_ITEM_ACTION_DISABLE){
_128=false;
}else{
if(Control.Package.Properties.InvalidMenuItemAction==INVALID_MENU_ITEM_ACTION_HIDE){
_127=false;
}else{
if(Control.Package.Properties.InvalidMenuItemAction==INVALID_MENU_ITEM_ACTION_SHOW_ENABLE){
_127=true;
}
}
}
}else{
if(Control.Package.Properties.InvalidMenuItemAction==INVALID_MENU_ITEM_ACTION_DISABLE){
_128=true;
}else{
if(Control.Package.Properties.InvalidMenuItemAction==INVALID_MENU_ITEM_ACTION_HIDE){
_127=true;
}else{
if(Control.Package.Properties.InvalidMenuItemAction==INVALID_MENU_ITEM_ACTION_SHOW_ENABLE){
_127=true;
}
}
}
}
if(_127!=this.Visible){
if(_127==true){
this.Show();
}else{
this.Hide();
}
}
if(_123.IsTheRoot()===true&&Control.Package.Properties.ForceDisableRootChoice===true){
_128=false;
}
if(_128!=this.Enabled){
if(_128==true){
this.Enable(_123.GetItemIdentifier(),_123.GetTitle());
}else{
this.Disable();
}
}
this.CurrentDisplayState=IntegrationImplementation.UpdateMenuStateDisplay(this.DivTag,this.Document,_123,this.ActivityId,_123.IsDeliverable(),_124,_125,Control.Package.LearningStandard,Control.Package.Properties.StatusDisplay,this.CurrentDisplayState,_126);
}
function MenuItem_Hide(){
if(this.Visible===true){
this.DivTag.style.display="none";
this.BumpDownLevel(this);
}
this.Visible=false;
}
function MenuItem_Show(){
if(this.Visible===false){
this.DivTag.style.display="inline";
this.BumpUpLevel(this);
}
this.Visible=true;
}
function MenuItem_Enable(_129,_12a){
if(this.Enabled===false){
var _12b=navigator.appName;
var _12c=parseInt(navigator.appVersion);
if(_12b=="Microsoft Internet Explorer"&&_12c<6){
this.DivTag.onmouseover=function(){
this.style.cursor="hand";
window.status=_12a;
return true;
};
}else{
this.DivTag.onmouseover=function(){
this.style.cursor="pointer";
window.status=_12a;
return true;
};
}
this.DivTag.onclick=function(){
var _12d=GetController();
_12d.ChoiceRequest(_129);
return true;
};
}
this.Enabled=true;
}
function MenuItem_Disable(){
if(this.Enabled===true){
this.DivTag.onmouseover=function(){
this.style.cursor="default";
window.status="";
return true;
};
this.DivTag.onclick="";
}
this.Enabled=false;
}
function MenuItem_BumpUpLevel(_12e){
_12e.Level++;
IntegrationImplementation.UpdateIndentLevel(_12e.DivTag,_12e.ActivityId,_12e.Level);
for(var _12f in _12e.Children){
_12e.BumpUpLevel(_12e.Children[_12f]);
}
}
function MenuItem_BumpDownLevel(_130){
_130.Level--;
IntegrationImplementation.UpdateIndentLevel(_130.DivTag,_130.ActivityId,_130.Level);
for(var _131 in _130.Children){
_130.BumpDownLevel(_130.Children[_131]);
}
}
function MenuItem_ResynchChildren(_132){
var _133;
for(var _134 in this.Children){
_133=this.Children[_134].DivTag;
_133.parentNode.removeChild(_133);
}
this.Children=new Array();
var _135=_132.GetAvailableChildren();
for(var _136 in _135){
this.Children[_136]=_135[_136].MenuItem;
}
}
function MenuItem_getLastSubmenuItem(_137){
if(_137.Children.length===0){
return _137;
}else{
return MenuItem_getLastSubmenuItem(_137.Children[_137.Children.length-1]);
}
}
function ScoLoader(_138,_139,_13a,_13b,_13c,_13d){
this.IntermediatePage=_138;
this.PopupLauncherPage=_139;
this.PathToCourse=_13a;
this.ScoLaunchType=_13b;
this.WrapScoWindowWithApi=_13c;
this.Standard=_13d;
this.ContentFrame=ScoLoader_FindContentFrame(window);
if(this.ContentFrame===null){
Debug.AssertError("Unable to locate the content frame-"+IntegrationImplementation.CONTENT_FRAME_NAME);
}
}
ScoLoader.prototype.LoadSco=ScoLoader_LoadSco;
ScoLoader.prototype.UnloadSco=ScoLoader_UnloadSco;
ScoLoader.prototype.WriteHistoryLog=ScoLoader_WriteHistoryLog;
ScoLoader.prototype.WriteHistoryReturnValue=ScoLoader_WriteHistoryReturnValue;
function ScoLoader_WriteHistoryLog(str,atts){
HistoryLog.WriteEventDetailed(str,atts);
}
function ScoLoader_WriteHistoryReturnValue(str,atts){
HistoryLog.WriteEventDetailedReturnValue(str,atts);
}
function ScoLoader_LoadSco(_142){
var _143={ev:"LoadSco"};
if(_142){
_143.ai=_142.ItemIdentifier;
_143.at=_142.LearningObject.Title;
}
this.WriteHistoryLog("",_143);
_142.LaunchedThisSession=false;
if(_142.GetActivityStartTimestampUtc()===null){
_142.SetActivityStartTimestampUtc(ConvertDateToIso8601String(new Date()));
}
if(_142.GetAttemptStartTimestampUtc()===null){
_142.SetAttemptStartTimestampUtc(ConvertDateToIso8601String(new Date()));
}
var _144="";
if(this.Standard.isAICC()){
_144="AICC_SID="+escape(ExternalRegistrationId+"~"+escape(ExternalConfig)+"~"+_142.GetDatabaseIdentifier()+"~"+RegistrationToDeliver.Package.Id);
if(RegistrationToDeliver.TrackingEnabled===false){
_144+=escape("~false");
}else{
_144+=escape("~true");
}
_144+="&AICC_URL="+escape(AICC_RESULTS_PAGE);
}
var _145="";
if(this.PathToCourse.length>0&&_142.GetLaunchPath().toLowerCase().indexOf("http://")!=0&&_142.GetLaunchPath().toLowerCase().indexOf("https://")!=0){
_145=this.PathToCourse;
if(this.PathToCourse.lastIndexOf("/")!=(this.PathToCourse.length-1)){
_145+="/";
}
_145+=_142.GetLaunchPath();
}else{
_145=_142.GetLaunchPath();
}
var _146;
if(_144!==""){
_145=MergeQueryStringParameters(_145,_144);
}
if(this.ScoLaunchType==LAUNCH_TYPE_FRAMESET){
Control.WriteDetailedLog("`1433`"+_145);
this.ContentFrame.location=_145;
Control.Api.InitTrackedTimeStart(_142);
_142.SetLaunchedThisSession();
}else{
if(this.ScoLaunchType==LAUNCH_TYPE_POPUP||this.ScoLaunchType==LAUNCH_TYPE_POPUP_WITHOUT_BROWSER_TOOLBAR||this.ScoLaunchType==LAUNCH_TYPE_POPUP_WITH_MENU||this.ScoLaunchType==LAUNCH_TYPE_POPUP_AFTER_CLICK||this.ScoLaunchType==LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR){
if(this.ScoLaunchType==LAUNCH_TYPE_POPUP_WITHOUT_BROWSER_TOOLBAR||this.ScoLaunchType==LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR){
var _147="false";
}else{
var _147="true";
}
if(this.ScoLaunchType==LAUNCH_TYPE_POPUP_WITH_MENU){
var _148="yes";
}else{
var _148="no";
}
if(this.ScoLaunchType==LAUNCH_TYPE_POPUP_AFTER_CLICK||this.ScoLaunchType==LAUNCH_TYPE_POPUP_AFTER_CLICK_WITHOUT_BROWSER_TOOLBAR){
var _149="true";
}else{
var _149="false";
}
if(this.WrapScoWindowWithApi===true){
_146=MergeQueryStringParameters(this.PopupLauncherPage.PageHref,this.PopupLauncherPage.Parameters,"ScoUrl="+escape(_145),"LaunchAfterClick="+_149,"WrapApi=true","ShowToolbar="+_147,"ShowMenubar="+_148);
}else{
_146=MergeQueryStringParameters(this.PopupLauncherPage.PageHref,this.PopupLauncherPage.Parameters,"ScoUrl="+escape(_145),"LaunchAfterClick="+_149,"ShowToolbar="+_147,"ShowMenubar="+_148);
}
this.ContentFrame.location=_146;
}else{
Debug.AssertError("Invalid Sco Launch Type");
}
}
}
function ScoLoader_UnloadSco(_14a){
var path="";
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse===true){
Control.UpdateGlobalLearnerPrefs();
}
if(Control.Package.Properties.RollupRuntimeAtScoUnload===true){
window.setTimeout("Control.EvaluatePossibleNavigationRequests(true);",150);
}
if(_14a===true){
path=MergeQueryStringParameters(this.IntermediatePage.PageHref,this.IntermediatePage.Parameters,"MessageWaiting=true");
}else{
path=MergeQueryStringParameters(this.IntermediatePage.PageHref,this.IntermediatePage.Parameters);
}
if((this.ScoLaunchType==LAUNCH_TYPE_POPUP||this.ScoLaunchType==LAUNCH_TYPE_POPUP_AFTER_CLICK)&&this.ContentFrame.CloseSco){
Control.WriteDetailedLog("`1712`");
this.ContentFrame.CloseSco();
Control.WriteDetailedLog("`1351`"+path);
window.setTimeout("Control.ScoLoader.ContentFrame.location = '"+path+"'",250);
}else{
Control.WriteDetailedLog("`992`"+path);
this.ContentFrame.location=path;
}
}
function ScoLoader_FindContentFrame(wnd){
var _14d=null;
for(var i=0;i<wnd.frames.length;i++){
if(wnd.frames[i].name==IntegrationImplementation.CONTENT_FRAME_NAME){
_14d=wnd.frames[i];
return _14d;
}
_14d=ScoLoader_FindContentFrame(wnd.frames[i]);
if(_14d!==null){
return _14d;
}
}
return null;
}
var HUNDREDTHS_PER_SECOND=100;
var HUNDREDTHS_PER_MINUTE=HUNDREDTHS_PER_SECOND*60;
var HUNDREDTHS_PER_HOUR=HUNDREDTHS_PER_MINUTE*60;
var HUNDREDTHS_PER_DAY=HUNDREDTHS_PER_HOUR*24;
var HUNDREDTHS_PER_MONTH=HUNDREDTHS_PER_DAY*(((365*4)+1)/48);
var HUNDREDTHS_PER_YEAR=HUNDREDTHS_PER_MONTH*12;
var REG_EX_DIGITS=/\d+/g;
var REG_EX_CHILDREN=/._children$/;
var REG_EX_COUNT=/._count$/;
function ConvertIso8601TimeToUtcAnsiSql(_14f){
var _150=GetParsedIso8601Time(_14f);
var date=new Date();
if(_150["timezoneoffsetchar"]===""||_150["timezoneoffsetchar"]===null||_150["timezoneoffsetchar"]===undefined){
date.setFullYear(_150["year"]);
date.setMonth(_150["month"]-1);
date.setDate(_150["day"]);
date.setHours(_150["hours"]);
date.setMinutes(_150["minutes"]);
date.setSeconds(_150["seconds"]);
date.setMilliseconds(_150["milliseconds"]);
}else{
date.setUTCFullYear(_150["year"]);
date.setUTCMonth(_150["month"]-1);
date.setUTCDate(_150["day"]);
date.setUTCHours(_150["hours"]);
date.setUTCMinutes(_150["minutes"]);
date.setUTCSeconds(_150["seconds"]);
date.setUTCMilliseconds(_150["milliseconds"]);
if(_150["timezoneoffsetchar"]=="-"){
date.setUTCHours(date.getUTCHours()+new Number(_150["offsethours"]));
date.setUTCMinutes(date.getUTCMinutes()+new Number(_150["offsetminutes"]));
}else{
if(_150["timezoneoffsetchar"]=="+"){
date.setUTCHours(date.getUTCHours()-new Number(_150["offsethours"]));
date.setUTCMinutes(date.getUTCMinutes()-new Number(_150["offsetminutes"]));
}
}
}
var _152=date.getUTCFullYear()+"-"+ZeroPad(date.getUTCMonth()+1,2)+"-"+ZeroPad(date.getUTCDate(),2)+"T"+ZeroPad(date.getUTCHours(),2)+":"+ZeroPad(date.getUTCMinutes(),2)+":"+ZeroPad(date.getUTCSeconds(),2);
return _152;
}
function ConvertDateToIso8601String(date){
var _154=function(num){
return ((num<10)?"0":"")+num;
};
var str="";
str+=date.getUTCFullYear();
str+="-"+_154(date.getUTCMonth()+1);
str+="-"+_154(date.getUTCDate());
str+="T"+_154(date.getUTCHours())+":"+_154(date.getUTCMinutes());
var secs=Number(date.getUTCSeconds()+"."+_154(date.getUTCMilliseconds()).substr(0,2));
str+=":"+_154(secs);
str+="Z";
return str;
}
function GetParsedIso8601Time(_158){
var _159=/^(\d{4})(?:-(\d{2})(?:-(\d{2}))?)?(?:T(\d{2})(?::(\d{2})(?::(\d{2})(?:\.(\d*))?)?)?((?:([+-])(\d{2})(?::(\d{2}))?)|Z)?)?$/;
var _15a=_159.exec(_158);
var _15b={};
_15b.year="";
_15b.month="";
_15b.day="";
_15b.hours="";
_15b.minutes="";
_15b.seconds="";
_15b.milliseconds="";
_15b.timezoneoffsetchar="";
_15b.offsethours="";
_15b.offsetminutes="";
if(!_15a){
Debug.AssertError("ERROR - unable to recognize the date format '"+_158+"'.");
return _15b;
}
_15b.year=_15a[1]||"";
_15b.month=_15a[2]||"";
_15b.day=_15a[3]||"";
_15b.hours=_15a[4]||"";
_15b.minutes=_15a[5]||"";
_15b.seconds=_15a[6]||"";
_15b.milliseconds=((_15a[7]||"")+"000").substring(0,3);
var _15c=_15a[8];
if(_15c&&_15c=="Z"){
_15b.timezoneoffsetchar="Z";
}
if(_15c&&_15c!="Z"){
_15b.timezoneoffsetchar=_15a[9]||"";
_15b.offsethours=_15a[10]||"";
_15b.offsetminutes=_15a[11]||"";
}
return _15b;
}
function GetDateFromUtcIso8601Time(_15d){
var _15e=GetParsedIso8601Time(_15d);
Debug.AssertError("Expected "+_15d+" to be a UTC string",_15e.timezoneoffsetchar!=="Z");
var _15f=new Date();
_15f.setUTCFullYear(_15e.year);
_15f.setUTCMonth(_15e.month-1);
_15f.setUTCDate(_15e.day);
_15f.setUTCHours(_15e.hours);
_15f.setUTCMinutes(_15e.minutes);
_15f.setUTCSeconds(_15e.seconds);
_15f.setUTCMilliseconds(_15e.milliseconds);
return _15f;
}
function ConvertIso8601TimeSpanToCmiTimeSpan(_160){
var _161=ConvertIso8601TimeSpanToHundredths(_160);
var _162=ConvertHundredthsIntoSCORMTime(_161);
return _162;
}
function ConvertCmiTimeSpanToIso8601TimeSpan(_163){
var _164=ConvertCmiTimeSpanToHundredths(_163);
var _165=ConvertHundredthsToIso8601TimeSpan(_164);
return _165;
}
function ConvertCmiTimeToIso8601Time(_166){
dtmNow=new Date();
var year=dtmNow.getFullYear();
var _168=dtmNow.getMonth();
var day=dtmNow.getDate();
year=ZeroPad(year,4);
_168=ZeroPad((_168+1),2);
day=ZeroPad(day,2);
var _16a=year+"-"+_168+"-"+day+"T"+_166;
return _16a;
}
function ConvertCmiTimeSpanToHundredths(_16b){
if(_16b===""){
return 0;
}
var _16c;
var _16d;
var _16e;
var _16f;
var _170;
_16c=_16b.split(":");
_16d=_16c[0];
_16e=_16c[1];
_16f=_16c[2];
intTotalHundredths=(_16d*360000)+(_16e*6000)+(_16f*100);
intTotalHundredths=Math.round(intTotalHundredths);
return intTotalHundredths;
}
function ConvertIso8601TimeSpanToHundredths(_171){
if(_171===""){
return 0;
}
var _172=0;
var _173;
var _174;
var _175;
var _176=0;
var _177=0;
var _178=0;
var Days=0;
var _17a=0;
var _17b=0;
_171=new String(_171);
_173="";
_174="";
_175=false;
for(var i=1;i<_171.length;i++){
_174=_171.charAt(i);
if(IsIso8601SectionDelimiter(_174)){
switch(_174.toUpperCase()){
case "Y":
_17b=parseInt(_173,10);
break;
case "M":
if(_175){
_177=parseInt(_173,10);
}else{
_17a=parseInt(_173,10);
}
break;
case "D":
Days=parseInt(_173,10);
break;
case "H":
_178=parseInt(_173,10);
break;
case "S":
_176=parseFloat(_173);
break;
case "T":
_175=true;
break;
}
_173="";
}else{
_173+=""+_174;
}
}
_172=(_17b*HUNDREDTHS_PER_YEAR)+(_17a*HUNDREDTHS_PER_MONTH)+(Days*HUNDREDTHS_PER_DAY)+(_178*HUNDREDTHS_PER_HOUR)+(_177*HUNDREDTHS_PER_MINUTE)+(_176*HUNDREDTHS_PER_SECOND);
_172=Math.round(_172);
return _172;
}
function IsIso8601SectionDelimiter(str){
if(str.search(/[PYMDTHS]/)>=0){
return true;
}else{
return false;
}
}
function ConvertHundredthsToIso8601TimeSpan(_17e){
var _17f="";
var _180;
var _181;
var _182;
var _183;
var Days;
var _185;
var _186;
_180=_17e;
_186=Math.floor(_180/HUNDREDTHS_PER_YEAR);
_180-=(_186*HUNDREDTHS_PER_YEAR);
_185=Math.floor(_180/HUNDREDTHS_PER_MONTH);
_180-=(_185*HUNDREDTHS_PER_MONTH);
Days=Math.floor(_180/HUNDREDTHS_PER_DAY);
_180-=(Days*HUNDREDTHS_PER_DAY);
_183=Math.floor(_180/HUNDREDTHS_PER_HOUR);
_180-=(_183*HUNDREDTHS_PER_HOUR);
_182=Math.floor(_180/HUNDREDTHS_PER_MINUTE);
_180-=(_182*HUNDREDTHS_PER_MINUTE);
_181=Math.floor(_180/HUNDREDTHS_PER_SECOND);
_180-=(_181*HUNDREDTHS_PER_SECOND);
if(_186>0){
_17f+=_186+"Y";
}
if(_17f>0){
ScormTime+=_185+"M";
}
if(Days>0){
_17f+=Days+"D";
}
if((_180+_181+_182+_183)>0){
_17f+="T";
if(_183>0){
_17f+=_183+"H";
}
if(_182>0){
_17f+=_182+"M";
}
if((_180+_181)>0){
_17f+=_181;
if(_180>0){
_17f+="."+_180;
}
_17f+="S";
}
}
if(_17f===""){
_17f="T0H0M0S";
}
_17f="P"+_17f;
return _17f;
}
function ConvertHundredthsIntoSCORMTime(_187){
var _188;
var _189;
var _18a;
var _18b;
intTotalMilliseconds=(_187*10);
var _18c;
_18b=intTotalMilliseconds%1000;
_18a=((intTotalMilliseconds-_18b)/1000)%60;
intMinutes=((intTotalMilliseconds-_18b-(_18a*1000))/60000)%60;
_188=(intTotalMilliseconds-_18b-(_18a*1000)-(intMinutes*60000))/3600000;
if(_188==10000){
_188=9999;
intMinutes=(intTotalMilliseconds-(_188*3600000))/60000;
if(intMinutes==100){
intMinutes=99;
}
intMinutes=Math.floor(intMinutes);
_18a=(intTotalMilliseconds-(_188*3600000)-(intMinutes*60000))/1000;
if(_18a==100){
_18a=99;
}
_18a=Math.floor(_18a);
_18b=(intTotalMilliseconds-(_188*3600000)-(intMinutes*60000)-(_18a*1000));
}
intHundredths=Math.floor(_18b/10);
_18c=ZeroPad(_188,4)+":"+ZeroPad(intMinutes,2)+":"+ZeroPad(_18a,2)+"."+intHundredths;
if(_188>9999){
_18c="9999:99:99.99";
}
return _18c;
}
function RemoveIndiciesFromCmiElement(_18d){
return _18d.replace(REG_EX_DIGITS,"n");
}
function ExtractIndex(_18e){
var _18f="";
var _190;
_190=_18e.match(/\.\d+\./);
if(_190!==null&&_190.length>0){
_18f=_190[0].replace(/\./g,"");
_18f=parseInt(_18f,10);
}
return _18f;
}
function ExtractSecondaryIndex(_191){
var _192="";
var _193;
_193=_191.match(/\.\d+\./g);
if(_193!==null&&_193.length>1){
_192=_193[1].replace(/\./g,"");
_192=parseInt(_192,10);
}
return _192;
}
function TranslateDualStausToSingleStatus(_194,_195){
var _196=null;
switch(_195){
case (SCORM_STATUS_PASSED):
_196=SCORM_STATUS_PASSED;
break;
case (SCORM_STATUS_FAILED):
_196=SCORM_STATUS_FAILED;
break;
case (SCORM_STATUS_UNKNOWN):
if(_194==SCORM_STATUS_COMPLETED){
_196=SCORM_STATUS_COMPLETED;
}else{
if(_194==SCORM_STATUS_INCOMPLETE){
_196=SCORM_STATUS_INCOMPLETE;
}else{
if(_194==SCORM_STATUS_UNKNOWN||_194==SCORM_STATUS_NOT_ATTEMPTED){
_196=SCORM_STATUS_NOT_ATTEMPTED;
}else{
if(_194==SCORM_STATUS_BROWSED){
_196=SCORM_STATUS_BROWSED;
}
}
}
}
break;
}
if(_196===null){
Debug.AssertError("Invalid status combination encountered in GetValue - Success = "+_195+", Completion = "+_194);
return "";
}else{
return _196;
}
}
function TranslateSingleStatusIntoSuccess(_197){
var _198;
switch(_197){
case (SCORM_STATUS_PASSED):
_198=SCORM_STATUS_PASSED;
break;
case (SCORM_STATUS_COMPLETED):
_198=SCORM_STATUS_UNKNOWN;
break;
case (SCORM_STATUS_FAILED):
_198=SCORM_STATUS_FAILED;
break;
case (SCORM_STATUS_INCOMPLETE):
_198=SCORM_STATUS_UNKNOWN;
break;
case (SCORM_STATUS_BROWSED):
_198=SCORM_STATUS_UNKNOWN;
break;
case (SCORM_STATUS_NOT_ATTEMPTED):
_198=SCORM_STATUS_UNKNOWN;
break;
default:
Debug.AssertError("Unrecognized single status");
}
return _198;
}
function TranslateSingleStatusIntoCompletion(_199){
var _19a;
switch(_199){
case (SCORM_STATUS_PASSED):
_19a=SCORM_STATUS_COMPLETED;
break;
case (SCORM_STATUS_COMPLETED):
_19a=SCORM_STATUS_COMPLETED;
break;
case (SCORM_STATUS_FAILED):
if(Control.Package.Properties.CompletionStatOfFailedSuccessStat===SCORM_STATUS_COMPLETED||Control.Package.Properties.CompletionStatOfFailedSuccessStat===SCORM_STATUS_INCOMPLETE){
_19a=Control.Package.Properties.CompletionStatOfFailedSuccessStat;
}else{
_19a=SCORM_STATUS_COMPLETED;
}
break;
case (SCORM_STATUS_INCOMPLETE):
_19a=SCORM_STATUS_INCOMPLETE;
break;
case (SCORM_STATUS_BROWSED):
_19a=SCORM_STATUS_BROWSED;
break;
case (SCORM_STATUS_NOT_ATTEMPTED):
_19a=SCORM_STATUS_NOT_ATTEMPTED;
break;
default:
Debug.AssertError("Unrecognized single status");
}
return _19a;
}
function IsValidCMITimeSpan(_19b){
var _19c=/^\d?\d?\d\d:\d\d:\d\d(.\d\d?)?$/;
if(_19b.search(_19c)>-1){
return true;
}else{
return false;
}
}
function IsValidCMIDecimal(_19d){
if(_19d.search(/[^.\d-]/)>-1){
return false;
}
if(_19d.search("-")>-1){
if(_19d.indexOf("-",1)>-1){
return false;
}
}
if(_19d.indexOf(".")!=_19d.lastIndexOf(".")){
return false;
}
if(_19d.search(/\d/)<0){
return false;
}
return true;
}
function IsValidCMIIdentifier(_19e){
if(_19e.length>255){
return false;
}
if(_19e===""){
return false;
}
return true;
}
function IsValidCMISInteger(_19f){
if(_19f.search(/[^\d-]/)>-1){
return false;
}
if(_19f.search("-")>-1){
if(_19f.indexOf("-",1)>-1){
return false;
}
}
if(_19f.search(/\d/)<0){
return false;
}
_19f=parseInt(_19f,10);
if(_19f<-32768||_19f>32768){
return false;
}
return true;
}
function IsValidCMITime(_1a0){
var _1a1=/^\d\d:\d\d:\d\d(.\d\d?)?$/;
var _1a2;
var _1a3;
var _1a4;
var _1a5;
var _1a6;
var _1a7=0;
if(_1a0.search(_1a1)<0){
return false;
}
_1a2=_1a0.split(":");
_1a3=_1a2[0];
_1a4=_1a2[1];
_1a5=_1a2[2].split(".");
_1a6=_1a5[0];
if(_1a5.length>1){
_1a7=_1a5[1];
}
if(_1a3<0||_1a3>23){
return false;
}
if(_1a4<0||_1a4>59){
return false;
}
if(_1a6<0||_1a6>59){
return false;
}
if(_1a7<0||_1a7>99){
return false;
}
return true;
}
function IsValidCMIFeedback(_1a8,_1a9){
if(_1a9.length>4096){
return false;
}
if(RegistrationToDeliver.Package.Properties.ValidateInteractionResponses){
_1a9=_1a9.toLowerCase();
switch(_1a8){
case "true-false":
if(_1a9.search(/^[01tf]/)!==0){
return false;
}
break;
case "choice":
if(_1a9.search(/(^(([a-z0-9])|(([a-z0-9]\,)+[a-z0-9]))$)|(^\{(([a-z0-9])|(([a-z0-9]\,)+[a-z0-9]))\}$)/)!==0){
return false;
}
break;
case "fill-in":
if(_1a9.length>255){
return false;
}
break;
case "numeric":
if(!IsValidCMIDecimal(_1a9)){
return false;
}
break;
case "likert":
break;
case "matching":
if(_1a9.search(/(^[0-9a-z]\.[0-9a-z]$)|(^([0-9a-z]\.[0-9a-z]\,)+([0-9a-z]\.[0-9a-z])$)|(^\{[0-9a-z]\.[0-9a-z]\}$)|(^\{([0-9a-z]\.[0-9a-z]\,)+([0-9a-z]\.[0-9a-z])\}$)/)!==0){
return false;
}
break;
case "performance":
if(_1a9.length>255){
return false;
}
break;
case "sequencing":
if(_1a9.search(/(^[a-z0-9]$)|(^([a-z0-9]\,)+[a-z0-9]$)/)!==0){
return false;
}
break;
default:
break;
}
}
return true;
}
function NormalizeRawScore(_1aa,_1ab,_1ac){
var _1ad=null;
if(_1ab!==null&&_1ab!==undefined){
_1ab=parseFloat(_1ab);
}
if(_1ac!==null&&_1ac!==undefined){
_1ac=parseFloat(_1ac);
}
if(_1aa!==null&&_1aa!==undefined){
_1aa=parseFloat(_1aa);
if(_1ab!==null&&_1ab!==undefined&&_1ac!==null&&_1ac!==undefined&&_1aa>=_1ab&&_1aa<=_1ac){
if(_1ab==_1ac){
_1ad=1;
}else{
_1ad=((_1aa-_1ab)/(_1ac-_1ab));
}
}else{
if(_1aa>=0&&_1aa<=100){
_1ad=(_1aa/100);
}
}
}
if(_1ad!==null){
return RoundToPrecision(_1ad,7);
}else{
return null;
}
}
function ServerFormater(){
}
ServerFormater.prototype.ConvertBoolean=ServerFormater_ConvertBoolean;
ServerFormater.prototype.ConvertCompletionStatus=ServerFormater_ConvertCompletionStatus;
ServerFormater.prototype.ConvertCredit=ServerFormater_ConvertCredit;
ServerFormater.prototype.ConvertEntry=ServerFormater_ConvertEntry;
ServerFormater.prototype.ConvertExit=ServerFormater_ConvertExit;
ServerFormater.prototype.ConvertMode=ServerFormater_ConvertMode;
ServerFormater.prototype.ConvertSuccessStatus=ServerFormater_ConvertSuccessStatus;
ServerFormater.prototype.ConvertInteractionType=ServerFormater_ConvertInteractionType;
ServerFormater.prototype.ConvertInteractionResult=ServerFormater_ConvertInteractionResult;
ServerFormater.prototype.GetNumericInteractionResultId=ServerFormater_GetNumericInteractionResultId;
ServerFormater.prototype.ConvertTimeSpan=ServerFormater_ConvertTimeSpan;
ServerFormater.prototype.ConvertTime=ServerFormater_ConvertTime;
ServerFormater.prototype.ConvertSSPAllocationSuccess=ServerFormater_ConvertSSPAllocationSuccess;
ServerFormater.prototype.ConvertSSPPersistence=ServerFormater_ConvertSSPPersistence;
ServerFormater.prototype.TrimToLength=ServerFormater_TrimToLength;
function ServerFormater_ConvertBoolean(_1ae){
if(_1ae===true){
return "1";
}else{
if(_1ae===false){
return "0";
}else{
Debug.AssertError("Value is not a boolean");
}
}
}
function ServerFormater_ConvertCompletionStatus(_1af){
var _1b0=null;
switch(_1af){
case (SCORM_STATUS_UNKNOWN):
_1b0=1;
break;
case (SCORM_STATUS_COMPLETED):
_1b0=2;
break;
case (SCORM_STATUS_INCOMPLETE):
_1b0=3;
break;
case (SCORM_STATUS_BROWSED):
_1b0=4;
break;
case (SCORM_STATUS_NOT_ATTEMPTED):
_1b0=5;
break;
default:
Debug.AssertError("Unrecognized Completion Status Value");
_1b0=-1;
break;
}
return _1b0;
}
function ServerFormater_ConvertCredit(_1b1){
var _1b2=null;
switch(_1b1){
case (SCORM_CREDIT):
_1b2=1;
break;
case (SCORM_CREDIT_NO):
_1b2=2;
break;
default:
Debug.AssertError("Unrecognized Credit Value");
_1b2=-1;
break;
}
return _1b2;
}
function ServerFormater_ConvertEntry(_1b3){
var _1b4=null;
switch(_1b3){
case (SCORM_ENTRY_AB_INITO):
_1b4=1;
break;
case (SCORM_ENTRY_RESUME):
_1b4=2;
break;
case (SCORM_ENTRY_NORMAL):
_1b4=3;
break;
default:
Debug.AssertError("Unrecognized Entry Value");
_1b4=-1;
break;
}
return _1b4;
}
function ServerFormater_ConvertExit(_1b5){
var _1b6=null;
switch(_1b5){
case (SCORM_EXIT_TIME_OUT):
_1b6=1;
break;
case (SCORM_EXIT_SUSPEND):
_1b6=2;
break;
case (SCORM_EXIT_LOGOUT):
_1b6=3;
break;
case (SCORM_EXIT_NORMAL):
_1b6=4;
break;
case (SCORM_EXIT_UNKNOWN):
_1b6=5;
break;
default:
Debug.AssertError("Unrecognized Exit Value");
_1b6=-1;
break;
}
return _1b6;
}
function ServerFormater_ConvertMode(_1b7){
var _1b8=null;
switch(_1b7){
case (SCORM_MODE_NORMAL):
_1b8=1;
break;
case (SCORM_MODE_BROWSE):
_1b8=2;
break;
case (SCORM_MODE_REVIEW):
_1b8=3;
break;
default:
Debug.AssertError("Unrecognized Mode Value");
_1b8=-1;
break;
}
return _1b8;
}
function ServerFormater_ConvertSuccessStatus(_1b9){
var _1ba=null;
switch(_1b9){
case (SCORM_STATUS_UNKNOWN):
_1ba=1;
break;
case (SCORM_STATUS_PASSED):
_1ba=2;
break;
case (SCORM_STATUS_FAILED):
_1ba=3;
break;
default:
Debug.AssertError("Unrecognized Success Status Value");
_1ba=-1;
break;
}
return _1ba;
}
function ServerFormater_ConvertInteractionType(_1bb){
var _1bc=null;
switch(_1bb){
case (SCORM_TRUE_FALSE):
_1bc=1;
break;
case (SCORM_CHOICE):
_1bc=2;
break;
case (SCORM_FILL_IN):
_1bc=3;
break;
case (SCORM_MATCHING):
_1bc=6;
break;
case (SCORM_PERFORMANCE):
_1bc=7;
break;
case (SCORM_SEQUENCING):
_1bc=8;
break;
case (SCORM_LIKERT):
_1bc=5;
break;
case (SCORM_NUMERIC):
_1bc=9;
break;
case (SCORM_LONG_FILL_IN):
_1bc=4;
break;
case (SCORM_OTHER):
_1bc=10;
break;
default:
Debug.AssertError("Unrecognized Interaction Type Value");
_1bc=-1;
break;
}
return _1bc;
}
function ServerFormater_ConvertInteractionResult(_1bd){
var _1be=null;
switch(_1bd){
case (SCORM_CORRECT):
_1be=1;
break;
case (SCORM_UNANTICIPATED):
_1be=3;
break;
case (SCORM_INCORRECT):
_1be=2;
break;
case (SCORM_NEUTRAL):
_1be=4;
break;
default:
Debug.AssertError("Unrecognized Interaction Result Value");
_1be=-1;
break;
}
return _1be;
}
function ServerFormater_GetNumericInteractionResultId(){
return 5;
}
function ServerFormater_ConvertTimeSpan(_1bf){
return ConvertIso8601TimeSpanToHundredths(_1bf);
}
function ServerFormater_ConvertTime(_1c0){
return ConvertIso8601TimeToUtcAnsiSql(_1c0);
}
function ServerFormater_ConvertSSPAllocationSuccess(_1c1){
var _1c2=null;
switch(_1c1){
case (SSP_ALLOCATION_SUCCESS_FAILURE):
_1c2=3;
break;
case (SSP_ALLOCATION_SUCCESS_MINIMUM):
_1c2=1;
break;
case (SSP_ALLOCATION_SUCCESS_REQUESTED):
_1c2=2;
break;
case (SSP_ALLOCATION_SUCCESS_NOT_ATTEMPTED):
_1c2=4;
break;
default:
Debug.AssertError("Unrecognized SSPAllocationSuccess Value");
_1c2=-1;
break;
}
return _1c2;
}
function ServerFormater_ConvertSSPPersistence(_1c3){
var _1c4=null;
switch(_1c3){
case (SSP_PERSISTENCE_LEARNER):
_1c4=1;
break;
case (SSP_PERSISTENCE_COURSE):
_1c4=2;
break;
case (SSP_PERSISTENCE_SESSION):
_1c4=3;
break;
default:
Debug.AssertError("Unrecognized SSPPersistence Value");
_1c4=-1;
break;
}
return _1c4;
}
function ServerFormater_TrimToLength(str,len){
if(str!==null&&str.length>len){
str=str.substr(0,len);
}
return str;
}
function StringBuilder(_1c7){
if(_1c7!==null&&parseInt(_1c7,10)>0){
this.Contents=new Array(parseInt(_1c7,10));
}else{
this.Contents=new Array();
}
this.CurrentEntry=0;
}
StringBuilder.prototype.Append=StringBuilder_Append;
StringBuilder.prototype.AppendLine=StringBuilder_AppendLine;
StringBuilder.prototype.toString=StringBuilder_toString;
function StringBuilder_Append(str){
this.Contents[this.CurrentEntry]=str;
this.CurrentEntry++;
}
function StringBuilder_AppendLine(str){
this.Append(str+"\r\n");
}
function StringBuilder_toString(){
return this.Contents.join("").trim();
}
String.prototype.trim=String_Trim;
String.prototype.toBoolean=String_ToBoolean;
function String_Trim(){
return Trim(this);
}
function Trim(str){
str=str.replace(/^\s*/,"");
str=str.replace(/\s*$/,"");
return str;
}
function String_ToBoolean(){
var str=this;
str=new String(str).toLowerCase();
if(str=="1"||str.charAt(0)=="t"){
return true;
}else{
if(str=="0"||str.charAt(0)=="f"){
return false;
}else{
Debug.AssertError("Value '"+str+"' can not be converted to a boolean.");
}
}
}
function MergeQueryStringParameters(){
var _1cc=new Array();
var _1cd=new Array();
var url=null;
var _1cf=null;
var i;
var _1d1=0;
var _1d2=null;
for(i=0;i<arguments.length;i++){
if(arguments[i]!=null&&arguments[i].length>0){
_1d1++;
if(_1d2==null){
_1d2=arguments[i];
}
}
}
if(_1d1==1){
return _1d2;
}
for(i=0;i<arguments.length;i++){
var qs=arguments[i];
qs=new String(qs);
if(qs.indexOf("#")>-1){
if(qs.charAt(0)=="#"){
if(_1cf===null){
_1cf=qs.substring(1);
}
qs="";
}else{
var _1d4=qs.split("#");
if(_1cf===null){
_1cf=_1d4[1];
}
qs=_1d4[0];
}
}
if(qs.indexOf("?")>0){
var _1d5=qs.substring(0,qs.indexOf("?"));
var _1d6=qs.substring(qs.indexOf("?")+1,qs.length);
if(url===null){
url=_1d5;
}
qs=_1d6;
}
if(qs.indexOf("#")<0&&qs.indexOf("&")<0&&qs.indexOf("=")<0&&qs.indexOf("?")<0){
if(url===null){
url=qs;
}
qs="";
}
if(qs.charAt(0)=="?"){
qs=qs.substring(1);
}
if(qs.charAt(0)=="&"){
qs=qs.substring(1);
}
if(qs.indexOf("&")>-1){
var _1d7=qs.split("&");
for(var j=0;j<_1d7.length;j++){
AddQueryStringParm(_1cc,_1cd,_1d7[j]);
}
}else{
if(qs.length>0){
AddQueryStringParm(_1cc,_1cd,qs);
}
}
}
var _1d9="";
if(url!==null){
_1d9+=url;
}
var _1da=true;
for(i=0;i<_1cc.length;i++){
if(_1da){
_1d9+="?";
_1da=false;
}else{
_1d9+="&";
}
_1d9+=_1cc[i];
var _1db=_1cd[i];
if(_1db.length>0){
_1d9+="="+_1db;
}
}
if(_1cf!==null){
_1d9+="#"+_1cf;
}
return _1d9;
}
function AddQueryStringParm(_1dc,_1dd,_1de){
if(_1de.indexOf("=")>-1){
var parm=_1de.split("=");
var name=parm[0];
var _1e1=parm[1];
_1dc[_1dc.length]=name;
_1dd[_1dd.length]=_1e1;
}else{
_1dc[_1dc.length]=_1de;
_1dd[_1dd.length]="";
}
}
function CleanExternalString(str){
str=str+"";
str=new String(str);
str=str.toString();
return str;
}
function ZeroPad(num,_1e4){
var _1e5;
var _1e6;
var i;
_1e5=new String(num);
_1e6=_1e5.length;
if(_1e6>_1e4){
_1e5=_1e5.substr(0,_1e4);
}else{
for(i=_1e6;i<_1e4;i++){
_1e5="0"+_1e5;
}
}
return _1e5;
}
function RoundToPrecision(_1e8,_1e9){
return Math.round(_1e8*Math.pow(10,_1e9))/Math.pow(10,_1e9);
}
function XmlElement(_1ea){
this.ElementName=_1ea;
this.Attributes=new Array();
this.Elements=new Array();
}
XmlElement.prototype.AddAttribute=XmlElement_AddAttribute;
XmlElement.prototype.AddElement=XmlElement_AddElement;
XmlElement.prototype.Encode=XmlElement_Encode;
XmlElement.prototype.toString=XmlElement_toString;
function XmlElement_AddAttribute(_1eb,_1ec){
this.Attributes[_1eb]=this.Encode(_1ec);
}
function XmlElement_AddElement(_1ed){
this.Elements[this.Elements.length]=_1ed;
}
function XmlElement_toString(){
var xml=new StringBuilder(this.Attributes.length+this.Elements.length+2);
xml.Append("<"+this.ElementName+" ");
for(var _1ef in this.Attributes){
xml.Append(_1ef+"=\""+this.Attributes[_1ef]+"\" ");
}
if(this.Elements.length>0){
xml.AppendLine(">");
for(var i=0;i<this.Elements.length;i++){
xml.AppendLine(this.Elements[i]);
}
xml.AppendLine("</"+this.ElementName+">");
}else{
xml.AppendLine("/>");
}
return xml.toString().trim();
}
function XmlElement_Encode(str){
str=new String(str);
if(str!==null){
str=str.replace(/\&/g,"&amp;");
str=str.replace(/\</g,"&lt;");
str=str.replace(/\>/g,"&gt;");
str=str.replace(/\'/g,"&apos;");
str=str.replace(/\"/g,"&quot;");
if("a".replace(/a/,function(){
return "";
}).length==0){
str=str.replace(/(\w|\W)/g,XmlElement_CharacterEscape);
}else{
str=EscapeCharacters(str);
}
}
return str;
}
function XmlElement_CharacterEscape(s,_1f3){
var _1f4=_1f3.charCodeAt(0);
_1f4=new Number(_1f4);
if(_1f4>127){
return "&#x"+_1f4.toString(16)+";";
}else{
if(_1f4<32){
return "&amp;#x"+_1f4.toString(16)+";";
}else{
return _1f3;
}
}
}
function EscapeCharacters(str){
var _1f6="";
for(var c=0;c<str.length;c++){
var _1f8=str.charCodeAt(c);
if(_1f8>127){
_1f6+="&#x"+_1f8.toString(16)+";";
}else{
if(_1f8<32){
_1f6+="&amp;#x"+_1f8.toString(16)+";";
}else{
_1f6+=str.charAt(c);
}
}
}
return _1f6;
}
function ActivityObjective(_1f9,_1fa,_1fb,_1fc,_1fd,_1fe,_1ff,_200,_201,_202,_203,_204,_205,_206,_207,_208,_209,_20a,_20b){
this.Identifier=_1f9;
this.ProgressStatus=_1fa;
this.SatisfiedStatus=_1fb;
this.MeasureStatus=_1fc;
this.NormalizedMeasure=_1fd;
this.Primary=_1fe;
this.PrevProgressStatus=_1ff;
this.PrevSatisfiedStatus=_200;
this.PrevMeasureStatus=_201;
this.PrevNormalizedMeasure=_202;
this.FirstSuccessTimestampUtc=_203;
this.FirstNormalizedMeasure=_204;
this.ScoreRaw=_205;
this.ScoreMin=_206;
this.ScoreMax=_207;
this.CompletionStatus=_208;
this.CompletionStatusValue=_209;
this.ProgressMeasureStatus=_20a;
this.ProgressMeasure=_20b;
this.SatisfiedByMeasure=false;
this.MinNormalizedMeasure=1;
this.Maps=new Array();
this.DataState=DATA_STATE_CLEAN;
this.Sequencer=null;
}
ActivityObjective.prototype.GetXml=ActivityObjective_GetXml;
ActivityObjective.prototype.toString=ActivityObjective_toString;
ActivityObjective.prototype.ResetAttemptState=ActivityObjective_ResetAttemptState;
ActivityObjective.prototype.GetContributesToRollup=ActivityObjective_GetContributesToRollup;
ActivityObjective.prototype.GetMeasureStatus=ActivityObjective_GetMeasureStatus;
ActivityObjective.prototype.GetNormalizedMeasure=ActivityObjective_GetNormalizedMeasure;
ActivityObjective.prototype.SetMeasureStatus=ActivityObjective_SetMeasureStatus;
ActivityObjective.prototype.SetNormalizedMeasure=ActivityObjective_SetNormalizedMeasure;
ActivityObjective.prototype.SetProgressStatus=ActivityObjective_SetProgressStatus;
ActivityObjective.prototype.SetSatisfiedStatus=ActivityObjective_SetSatisfiedStatus;
ActivityObjective.prototype.GetSatisfiedByMeasure=ActivityObjective_GetSatisfiedByMeasure;
ActivityObjective.prototype.GetMinimumSatisfiedNormalizedMeasure=ActivityObjective_GetMinimumSatisfiedNormalizedMeasure;
ActivityObjective.prototype.GetProgressStatus=ActivityObjective_GetProgressStatus;
ActivityObjective.prototype.GetSatisfiedStatus=ActivityObjective_GetSatisfiedStatus;
ActivityObjective.prototype.GetScoreRaw=ActivityObjective_GetScoreRaw;
ActivityObjective.prototype.GetScoreMin=ActivityObjective_GetScoreMin;
ActivityObjective.prototype.GetScoreMax=ActivityObjective_GetScoreMax;
ActivityObjective.prototype.GetCompletionStatus=ActivityObjective_GetCompletionStatus;
ActivityObjective.prototype.GetCompletionStatusValue=ActivityObjective_GetCompletionStatusValue;
ActivityObjective.prototype.GetProgressMeasureStatus=ActivityObjective_GetProgressMeasureStatus;
ActivityObjective.prototype.GetProgressMeasure=ActivityObjective_GetProgressMeasure;
ActivityObjective.prototype.SetScoreRaw=ActivityObjective_SetScoreRaw;
ActivityObjective.prototype.SetScoreMin=ActivityObjective_SetScoreMin;
ActivityObjective.prototype.SetScoreMax=ActivityObjective_SetScoreMax;
ActivityObjective.prototype.SetCompletionStatus=ActivityObjective_SetCompletionStatus;
ActivityObjective.prototype.SetCompletionStatusValue=ActivityObjective_SetCompletionStatusValue;
ActivityObjective.prototype.SetProgressMeasureStatus=ActivityObjective_SetProgressMeasureStatus;
ActivityObjective.prototype.SetProgressMeasure=ActivityObjective_SetProgressMeasure;
ActivityObjective.prototype.GetIdentifier=ActivityObjective_GetIdentifier;
ActivityObjective.prototype.GetMaps=ActivityObjective_GetMaps;
ActivityObjective.prototype.SetDirtyData=ActivityObjective_SetDirtyData;
ActivityObjective.prototype.SetSequencer=ActivityObjective_SetSequencer;
ActivityObjective.prototype.Clone=ActivityObjective_Clone;
ActivityObjective.prototype.TearDown=ActivityObjective_TearDown;
ActivityObjective.prototype.GetSuccessStatusChangedDuringRuntime=ActivityObjective_GetSuccessStatusChangedDuringRuntime;
function ActivityObjective_GetXml(_20c,_20d){
var _20e=new ServerFormater();
var xml=new XmlElement("AO");
xml.AddAttribute("AI",_20c);
xml.AddAttribute("AOI",_20d);
xml.AddAttribute("I",this.Identifier);
xml.AddAttribute("PS",_20e.ConvertBoolean(this.ProgressStatus));
xml.AddAttribute("SS",_20e.ConvertBoolean(this.SatisfiedStatus));
xml.AddAttribute("MS",_20e.ConvertBoolean(this.MeasureStatus));
xml.AddAttribute("NM",this.NormalizedMeasure);
xml.AddAttribute("PPS",_20e.ConvertBoolean(this.PrevProgressStatus));
xml.AddAttribute("PSS",_20e.ConvertBoolean(this.PrevSatisfiedStatus));
xml.AddAttribute("PMS",_20e.ConvertBoolean(this.PrevMeasureStatus));
xml.AddAttribute("PNM",this.PrevNormalizedMeasure);
if(this.FirstSuccessTimestampUtc!==null){
xml.AddAttribute("FSTU",_20e.ConvertTime(this.FirstSuccessTimestampUtc));
}
if(this.FirstNormalizedMeasure!==null){
xml.AddAttribute("FNM",this.FirstNormalizedMeasure);
}
xml.AddAttribute("P",_20e.ConvertBoolean(this.Primary));
xml.AddAttribute("CS",_20e.ConvertBoolean(this.CompletionStatus));
xml.AddAttribute("CSV",_20e.ConvertBoolean(this.CompletionStatusValue));
if(this.ScoreRaw!==null){
xml.AddAttribute("SR",this.ScoreRaw);
}
if(this.ScoreMax!==null){
xml.AddAttribute("SM",this.ScoreMax);
}
if(this.ScoreMin!==null){
xml.AddAttribute("SMi",this.ScoreMin);
}
xml.AddAttribute("PrMS",_20e.ConvertBoolean(this.ProgressMeasureStatus));
if(this.ProgressMeasure!==null){
xml.AddAttribute("PM",this.ProgressMeasure);
}
return xml.toString();
}
function ActivityObjective_toString(){
return this.Identifier;
}
function ActivityObjective_ResetAttemptState(){
this.PrevProgressStatus=this.ProgressStatus;
this.PrevSatisfiedStatus=this.SatisfiedStatus;
this.PrevMeasureStatus=this.MeasureStatus;
this.PrevNormalizedMeasure=this.NormalizedMeasure;
this.ProgressStatus=false;
this.SatisfiedStatus=false;
this.MeasureStatus=false;
this.NormalizedMeasure=0;
this.ScoreRaw=null;
this.ScoreMin=null;
this.ScoreMax=null;
this.CompletionStatus=false;
this.CompletionStatusValue=false;
this.ProgressMeasureStatus=false;
this.ProgressMeasure=null;
this.SetDirtyData();
}
function ActivityObjective_GetContributesToRollup(){
var _210=this.Primary;
return _210;
}
function ActivityObjective_GetMeasureStatus(_211,_212){
if(_212===null||_212===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into GetMeasureStatus");
}
var _213;
if(_212===true&&(_211!=null&&_211!=undefined)&&_211.WasAttemptedDuringThisAttempt()===false){
_213=this.PrevMeasureStatus;
}else{
_213=this.MeasureStatus;
}
var _214=(_213===false)||Control.Package.LearningStandard.is20043rdOrGreater();
if(_214===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].ReadNormalizedMeasure===true){
var _217=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_217!==null){
_213=_217.MeasureStatus;
}
}
}
}
return _213;
}
function ActivityObjective_GetNormalizedMeasure(_218,_219){
if(_219===null||_219===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into GetNormalizedMeasure");
}
var _21a;
var _21b;
if(_219===true&&(_218!=null&&_218!=undefined)&&_218.WasAttemptedDuringThisAttempt()===false){
_21a=this.PrevMeasureStatus;
_21b=this.PrevNormalizedMeasure;
}else{
_21a=this.MeasureStatus;
_21b=this.NormalizedMeasure;
}
var _21c=(_21a===false)||Control.Package.LearningStandard.is20043rdOrGreater();
if(_21c===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].ReadNormalizedMeasure===true){
var _21f=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_21f!==null&&_21f.MeasureStatus===true){
_21b=_21f.NormalizedMeasure;
}
}
}
}
return _21b;
}
function ActivityObjective_SetMeasureStatus(_220,_221){
if(_221===null||_221===undefined){
Debug.AssertError("ERROR - activity must be passed into SetMeasureStatus");
}
this.MeasureStatus=_220;
if(_220===true){
_221.SetAttemptedDuringThisAttempt();
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].WriteNormalizedMeasure===true){
var _224=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_224===null){
this.Sequencer.AddGlobalObjective(maps[i].TargetObjectiveId,false,false,true,0);
}else{
_224.MeasureStatus=true;
_224.SetDirtyData();
}
}
}
}
this.SetDirtyData();
}
function ActivityObjective_SetNormalizedMeasure(_225,_226){
this.NormalizedMeasure=_225;
if(this.MeasureStatus===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].WriteNormalizedMeasure===true){
var _229=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_229===null){
this.Sequencer.AddGlobalObjective(maps[i].TargetObjectiveId,false,false,true,_225);
}else{
_229.NormalizedMeasure=_225;
_229.SetDirtyData();
}
}
}
}
if(_226===null||_226===undefined){
Debug.AssertError("ERROR - activity must be passed into SetNormalizedMeasure");
}
if(this.GetSatisfiedByMeasure()===true&&(_226.IsActive()===false||_226.GetMeasureSatisfactionIfActive()===true)){
var _22a=true;
var _22b;
if(this.GetNormalizedMeasure(_226,false)>=this.GetMinimumSatisfiedNormalizedMeasure()){
_22b=true;
}else{
_22b=false;
}
this.SetProgressStatus(_22a,true,_226);
this.SetSatisfiedStatus(_22b,true,_226);
}
if(this.ProgressStatus===true&&this.FirstNormalizedMeasure==0){
this.FirstNormalizedMeasure=_225;
}
this.SetDirtyData();
}
function ActivityObjective_SetProgressStatus(_22c,_22d,_22e,_22f,_230){
if(_22e===null||_22e===undefined){
Debug.AssertError("ERROR - activity must be passed into SetMeasureStatus");
}
if(this.GetSatisfiedByMeasure()===false||_22d===true){
this.ProgressStatus=_22c;
if(_22c===true){
_22e.SetAttemptedDuringThisAttempt();
}
var _231=_22c;
if(Control.Package.LearningStandard.is20044thOrGreater()){
_231=_231||(_22f===true);
}
if(_231){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].WriteSatisfiedStatus===true){
var _234=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_234===null){
this.Sequencer.AddGlobalObjective(maps[i].TargetObjectiveId,_22c,false,false,0);
}else{
_234.ProgressStatus=_22c;
_234.SetDirtyData();
}
}
}
}
this.SetDirtyData();
}
}
function ActivityObjective_SetSatisfiedStatus(_235,_236,_237){
if(this.GetSatisfiedByMeasure()===false||_236===true){
this.SatisfiedStatus=_235;
if(_235===true&&Control.Package.Properties.SatisfiedCausesCompletion===true){
_237.SetAttemptProgressStatus(true);
_237.SetAttemptCompletionStatus(true);
}
if(this.ProgressStatus===true){
if(this.FirstSuccessTimestampUtc===null&&_235===true){
this.FirstSuccessTimestampUtc=ConvertDateToIso8601String(new Date());
}
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].WriteSatisfiedStatus===true){
var _23a=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_23a===null){
this.Sequencer.AddGlobalObjective(maps[i].TargetObjectiveId,true,_235,false,0);
}else{
_23a.SatisfiedStatus=_235;
_23a.SetDirtyData();
}
}
}
}
this.SetDirtyData();
}
}
function ActivityObjective_GetSatisfiedByMeasure(){
var _23b=this.SatisfiedByMeasure;
return _23b;
}
function ActivityObjective_GetMinimumSatisfiedNormalizedMeasure(){
var _23c=this.MinNormalizedMeasure;
_23c=parseFloat(_23c);
return _23c;
}
function ActivityObjective_GetProgressStatus(_23d,_23e){
if(_23e===null||_23e===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into GetProgressStatus");
}
if(_23d===null||_23d===undefined){
Debug.AssertError("ERROR - activity must be passed into ActivityObjective_GetProgressStatus");
}
var _23f;
if(_23e===true&&_23d.WasAttemptedDuringThisAttempt()===false){
_23f=this.PrevProgressStatus;
}else{
_23f=this.ProgressStatus;
}
var _240=(_23f===false)||Control.Package.LearningStandard.is20043rdOrGreater();
if(_240===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].ReadSatisfiedStatus===true){
var _243=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_243!==null){
_23f=_243.ProgressStatus;
}
}
}
}
if(this.GetSatisfiedByMeasure()===true&&(_23d.IsActive()===false||_23d.GetMeasureSatisfactionIfActive()===true)){
if(this.GetMeasureStatus(_23d,_23e)===true){
_23f=true;
}else{
_23f=false;
}
}
return _23f;
}
function ActivityObjective_GetSatisfiedStatus(_244,_245){
if(_245===null||_245===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into GetSatisfiedStatus");
}
var _246;
var _247;
if(_245===true&&_244.WasAttemptedDuringThisAttempt()===false){
_247=this.PrevProgressStatus;
_246=this.PrevSatisfiedStatus;
}else{
_247=this.ProgressStatus;
_246=this.SatisfiedStatus;
}
var _248=(_247===false)||Control.Package.LearningStandard.is20043rdOrGreater();
if(_248===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].ReadSatisfiedStatus===true){
var _24b=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_24b!==null&&_24b.ProgressStatus===true){
_246=_24b.SatisfiedStatus;
}
}
}
}
if(_244===null||_244===undefined){
Debug.AssertError("ERROR - activity must be passed into ActivityObjective_GetSatisfiedStatus");
}else{
if(this.GetSatisfiedByMeasure()===true&&(_244.IsActive()===false||_244.GetMeasureSatisfactionIfActive()===true)){
if(this.GetMeasureStatus(_244,_245)===true){
if(this.GetNormalizedMeasure(_244,_245)>=this.GetMinimumSatisfiedNormalizedMeasure()){
_246=true;
}else{
_246=false;
}
}
}
}
return _246;
}
function ActivityObjective_GetScoreRaw(){
var _24c=this.ScoreRaw;
var _24d=Control.Package.LearningStandard.is20044thOrGreater();
if(_24d===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].ReadRawScore===true){
var _250=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_250!==null&&_250.ScoreRaw!==null){
_24c=_250.ScoreRaw;
}
}
}
}
return _24c;
}
function ActivityObjective_SetScoreRaw(_251){
this.ScoreRaw=_251;
if(_251!==null){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].WriteRawScore===true){
var _254=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_254!==null){
_254.ScoreRaw=_251;
_254.SetDirtyData();
}
}
}
}
this.SetDirtyData();
}
function ActivityObjective_GetScoreMin(){
var _255=this.ScoreMin;
var _256=Control.Package.LearningStandard.is20044thOrGreater();
if(_256===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].ReadMinScore===true){
var _259=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_259!==null&&_259.ScoreMin!==null){
_255=_259.ScoreMin;
}
}
}
}
return _255;
}
function ActivityObjective_SetScoreMin(_25a){
this.ScoreMin=_25a;
if(_25a!==null){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].WriteMinScore===true){
var _25d=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_25d!==null){
_25d.ScoreMin=_25a;
_25d.SetDirtyData();
}
}
}
}
this.SetDirtyData();
}
function ActivityObjective_GetScoreMax(){
var _25e=this.ScoreMax;
var _25f=Control.Package.LearningStandard.is20044thOrGreater();
if(_25f===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].ReadMaxScore===true){
var _262=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_262!==null&&_262.ScoreMax!==null){
_25e=_262.ScoreMax;
}
}
}
}
return _25e;
}
function ActivityObjective_SetScoreMax(_263){
this.ScoreMax=_263;
if(_263!==null){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].WriteMaxScore===true){
var _266=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_266!==null){
_266.ScoreMax=_263;
_266.SetDirtyData();
}
}
}
}
this.SetDirtyData();
}
function ActivityObjective_GetCompletionStatus(_267,_268){
if(_268===null||_268===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into GetCompletionStatus");
}
if(_267===null||_267===undefined){
Debug.AssertError("ERROR - activity must be passed into ActivityObjective_GetCompletionStatusValue");
}
var _269=this.CompletionStatus;
var _26a=Control.Package.LearningStandard.is20044thOrGreater();
if(_26a===true){
var maps=this.Maps;
var _26c;
for(var i=0;i<maps.length;i++){
if(maps[i].ReadCompletionStatus===true){
_26c=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_26c!==null){
_269=_26c.CompletionStatus;
}
}
}
}
return _269;
}
function ActivityObjective_GetCompletionStatusValue(_26e,_26f){
if(_26f===null||_26f===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into GetCompletionStatusValue");
}
if(_26e===null||_26e===undefined){
Debug.AssertError("ERROR - activity must be passed into ActivityObjective_GetCompletionStatusValue");
}
var _270=this.CompletionStatusValue;
var _271=Control.Package.LearningStandard.is20044thOrGreater();
if(_271===true){
var maps=this.GetMaps("ReadCompletionStatus",true);
for(var i=0;i<maps.length;i++){
var _274=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_274!==null){
_270=_274.CompletionStatusValue;
}
}
}
return _270;
}
function ActivityObjective_SetCompletionStatus(_275){
this.CompletionStatus=_275;
var maps=this.GetMaps("WriteCompletionStatus",true);
for(var i=0;i<maps.length;i++){
var _278=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_278!==null){
_278.CompletionStatus=_275;
_278.SetDirtyData();
}
}
this.SetDirtyData();
}
function ActivityObjective_SetCompletionStatusValue(_279){
this.CompletionStatusValue=_279;
var maps=this.GetMaps("WriteCompletionStatus",true);
for(var i=0;i<maps.length;i++){
var _27c=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_27c!==null){
_27c.CompletionStatusValue=_279;
_27c.SetDirtyData();
}
}
this.SetDirtyData();
}
function ActivityObjective_GetProgressMeasureStatus(){
var _27d=this.ProgressMeasureStatus;
var _27e=Control.Package.LearningStandard.is20044thOrGreater();
if(_27e===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].ReadProgressMeasure===true){
var _281=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_281!==null){
_27d=_281.ProgressMeasureStatus;
}
}
}
}
return _27d;
}
function ActivityObjective_GetProgressMeasure(){
var _282=this.ProgressMeasure;
var _283=Control.Package.LearningStandard.is20044thOrGreater();
if(_283===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].ReadProgressMeasure===true){
var _286=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_286!==null&&_286.ProgressMeasureStatus!==false){
_282=_286.ProgressMeasure;
}
}
}
}
return _282;
}
function ActivityObjective_SetProgressMeasureStatus(_287){
this.ProgressMeasureStatus=_287;
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].WriteProgressMeasure===true){
var _28a=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_28a!==null){
_28a.ProgressMeasureStatus=_287;
_28a.SetDirtyData();
}
}
}
this.SetDirtyData();
}
function ActivityObjective_SetProgressMeasure(_28b){
this.ProgressMeasure=_28b;
if(this.ProgressMeasureStatus===true){
var maps=this.Maps;
for(var i=0;i<maps.length;i++){
if(maps[i].WriteProgressMeasure===true){
var _28e=this.Sequencer.GetGlobalObjectiveByIdentifier(maps[i].TargetObjectiveId);
if(_28e!==null){
_28e.ProgressMeasure=_28b;
_28e.SetDirtyData();
}
}
}
}
this.SetDirtyData();
}
function ActivityObjective_GetIdentifier(){
var _28f=this.Identifier;
return _28f;
}
function ActivityObjective_GetMaps(_290,_291){
var maps=this.Maps;
if(maps.length>0&&_290!==null&&_290!==undefined){
if(_291===null&&_291===undefined){
_291=true;
}
var _293=new Array();
for(var i=0;i<maps.length;i++){
if(maps[i][_290]===_291){
_293[_293.length]=maps[i];
}
}
return _293;
}
return maps;
}
function ActivityObjective_SetDirtyData(){
this.DataState=DATA_STATE_DIRTY;
}
function ActivityObjective_SetSequencer(_295){
this.Sequencer=_295;
}
function ActivityObjective_Clone(){
var _296=new ActivityObjective(this.Identifier,this.ProgressStatus,this.SatisfiedStatus,this.MeasureStatus,this.NormalizedMeasure,this.Primary,null,null,null,null,null,null,this.ScoreRaw,this.ScoreMin,this.ScoreMax,this.CompletionStatus,this.CompletionStatusValue,this.ProgressMeasureStatus,this.ProgressMeasure);
_296.SatisfiedByMeasure=this.SatisfiedByMeasure;
_296.MinNormalizedMeasure=this.MinNormalizedMeasure;
_296.CompletionStatusSetAtRuntime=this.CompletionStatusSetAtRuntime;
_296.Maps=this.Maps;
return _296;
}
function ActivityObjective_TearDown(){
this.Identifier=null;
this.ProgressStatus=null;
this.SatisfiedStatus=null;
this.MeasureStatus=null;
this.NormalizedMeasure=null;
this.ScoreRaw=null;
this.ScoreMin=null;
this.ScoreMax=null;
this.CompletionStatus=null;
this.CompletionStatusValue=null;
this.ProgressMeasure=null;
this.Primary=null;
this.SatisfiedByMeasure=null;
this.MinNormalizedMeasure=null;
this.Maps=null;
this.DataState=null;
this.Sequencer=null;
this.ProgressMeasureStatus=null;
this.ProgressMeasure=null;
}
function ActivityObjective_GetSuccessStatusChangedDuringRuntime(_297){
var _298=_297.RunTime.FindObjectiveWithId(this.Identifier);
if(_298!==null){
if(_298.SuccessStatusChangedDuringRuntime===true){
return true;
}
}
return false;
}
function ActivityRunTimeComment(_299,_29a,_29b,_29c,_29d){
this.Comment=_299;
this.Language=_29a;
this.Location=_29b;
this.TimestampUtc=_29c;
this.Timestamp=_29d;
}
ActivityRunTimeComment.prototype.GetXml=ActivityRunTimeComment_GetXml;
ActivityRunTimeComment.prototype.toString=ActivityRunTimeComment_toString;
ActivityRunTimeComment.prototype.GetCommentValue=ActivityRunTimeComment_GetCommentValue;
ActivityRunTimeComment.prototype.SetCommentValue=ActivityRunTimeComment_SetCommentValue;
function ActivityRunTimeComment_GetXml(_29e,_29f,_2a0){
var _2a1=new ServerFormater();
var xml=new XmlElement("ARTC");
xml.AddAttribute("AI",_29e);
xml.AddAttribute("I",_29f);
xml.AddAttribute("FL",_2a1.ConvertBoolean(_2a0));
if(this.Comment!==null){
xml.AddAttribute("C",_2a1.TrimToLength(this.Comment,4000));
}
if(this.Language!==null){
xml.AddAttribute("L",this.Language);
}
if(this.Location!==null){
xml.AddAttribute("Lo",_2a1.TrimToLength(this.Location,250));
}
if(this.Timestamp!==null&&this.Timestamp!=""){
xml.AddAttribute("TU",_2a1.ConvertTime(this.Timestamp));
xml.AddAttribute("T",this.Timestamp);
}
return xml.toString();
}
function ActivityRunTimeComment_toString(){
return "ActivityRunTimeComment";
}
function ActivityRunTimeComment_GetCommentValue(){
var _2a3="";
if(this.Language!==null&&this.Language!==undefined){
_2a3=this.Language;
}
_2a3+=this.Comment;
return _2a3;
}
function ActivityRunTimeComment_SetCommentValue(_2a4){
var _2a5="";
var _2a6="";
var _2a7=_2a4.indexOf("}");
if(_2a4.indexOf("{lang=")===0&&_2a7>0){
_2a5=_2a4.substr(0,_2a7+1);
if(_2a4.length>=(_2a7+2)){
_2a6=_2a4.substring(_2a7+1);
}
}else{
_2a6=_2a4;
}
this.Language=_2a5;
this.Comment=_2a6;
}
function ActivityRunTimeInteraction(Id,Type,_2aa,_2ab,_2ac,_2ad,_2ae,_2af,_2b0,_2b1,_2b2){
this.Id=Id;
if(Type===""){
Type="";
}
this.Type=Type;
this.TimestampUtc=_2aa;
this.Timestamp=_2ab;
this.Weighting=_2ac;
if(_2ad===""){
_2ad=null;
}
this.Result=_2ad;
if(_2ae===""){
_2ae=null;
}
this.Latency=_2ae;
this.Description=_2af;
this.LearnerResponse=_2b0;
this.CorrectResponses=_2b1;
this.Objectives=_2b2;
}
ActivityRunTimeInteraction.prototype.GetXml=ActivityRunTimeInteraction_GetXml;
ActivityRunTimeInteraction.prototype.toString=ActivityRunTimeInteraction_toString;
function ActivityRunTimeInteraction_GetXml(_2b3,_2b4){
var _2b5=new ServerFormater();
var xml=new XmlElement("ARTI");
xml.AddAttribute("AI",_2b3);
xml.AddAttribute("I",_2b4);
xml.AddAttribute("Id",_2b5.TrimToLength(this.Id,4000));
if(this.Type!==null){
xml.AddAttribute("T",_2b5.ConvertInteractionType(this.Type));
}
if(this.Timestamp!==null){
xml.AddAttribute("TU",_2b5.ConvertTime(this.Timestamp));
xml.AddAttribute("Ti",this.Timestamp);
}
if(this.Weighting!==null){
xml.AddAttribute("W",this.Weighting);
}
if(this.Result!==null&&this.Result!==""){
if(IsValidCMIDecimal(this.Result)){
xml.AddAttribute("R",_2b5.GetNumericInteractionResultId());
xml.AddAttribute("RN",this.Result);
}else{
xml.AddAttribute("R",_2b5.ConvertInteractionResult(this.Result));
}
}
if(this.Latency!==null){
xml.AddAttribute("L",_2b5.ConvertTimeSpan(this.Latency));
}
if(this.Description!==null){
xml.AddAttribute("D",_2b5.TrimToLength(this.Description,500));
}
if(this.LearnerResponse!==null){
xml.AddAttribute("LR",_2b5.TrimToLength(this.LearnerResponse,7800));
}
var _2b7;
var i;
for(i=0;i<this.CorrectResponses.length;i++){
_2b7=new XmlElement("CR");
_2b7.AddAttribute("AI",_2b3);
_2b7.AddAttribute("II",_2b4);
_2b7.AddAttribute("I",i);
_2b7.AddAttribute("V",_2b5.TrimToLength(this.CorrectResponses[i],7800));
xml.AddElement(_2b7.toString());
}
var _2b9;
for(i=0;i<this.Objectives.length;i++){
_2b9=new XmlElement("O");
_2b9.AddAttribute("AI",_2b3);
_2b9.AddAttribute("II",_2b4);
_2b9.AddAttribute("I",i);
_2b9.AddAttribute("Id",_2b5.TrimToLength(this.Objectives[i],4000));
xml.AddElement(_2b9.toString());
}
return xml.toString();
}
function ActivityRunTimeInteraction_toString(){
return "ActivityRunTimeInteraction";
}
function ActivityRunTimeObjective(_2ba,_2bb,_2bc,_2bd,_2be,_2bf,_2c0,_2c1,_2c2){
this.Identifier=_2ba;
this.SuccessStatus=_2bb;
this.CompletionStatus=_2bc;
this.ScoreScaled=_2bd;
this.ScoreRaw=_2be;
this.ScoreMax=_2bf;
this.ScoreMin=_2c0;
this.ProgressMeasure=_2c1;
this.Description=_2c2;
this.SuccessStatusChangedDuringRuntime=false;
this.MeasureChangedDuringRuntime=false;
this.ProgressMeasureChangedDuringRuntime=false;
this.CompletionStatusChangedDuringRuntime=false;
}
ActivityRunTimeObjective.prototype.GetXml=ActivityRunTimeObjective_GetXml;
ActivityRunTimeObjective.prototype.toString=ActivityRunTimeObjective_toString;
function ActivityRunTimeObjective_GetXml(_2c3,_2c4){
var _2c5=new ServerFormater();
var xml=new XmlElement("ARTO");
xml.AddAttribute("AI",_2c3);
xml.AddAttribute("I",_2c4);
if(this.Identifier!==null){
xml.AddAttribute("Id",_2c5.TrimToLength(this.Identifier,4000));
}
xml.AddAttribute("SS",_2c5.ConvertSuccessStatus(this.SuccessStatus));
xml.AddAttribute("CS",_2c5.ConvertCompletionStatus(this.CompletionStatus));
if(this.ScoreScaled!==null){
xml.AddAttribute("SSc",this.ScoreScaled);
}
if(this.ScoreRaw!==null){
xml.AddAttribute("SR",this.ScoreRaw);
}
if(this.ScoreMax!==null){
xml.AddAttribute("SM",this.ScoreMax);
}
if(this.ScoreMin!==null){
xml.AddAttribute("SMi",this.ScoreMin);
}
if(this.ProgressMeasure!==null){
xml.AddAttribute("PM",this.ProgressMeasure);
}
if(this.Description!==null){
xml.AddAttribute("D",_2c5.TrimToLength(this.Description,500));
}
return xml.toString();
}
function ActivityRunTimeObjective_toString(){
return "ActivityRunTimeObjective - "+this.Identifier;
}
function ActivityRunTimeSSPBucket(Id,_2c8,_2c9,_2ca,_2cb,_2cc,_2cd,Data){
this.Id=Id;
this.BucketType=_2c8;
this.Persistence=_2c9;
this.SizeMin=_2ca;
this.SizeRequested=_2cb;
this.Reducible=_2cc;
this.TotalSpace=_2cd;
this.Data=Data;
}
ActivityRunTimeSSPBucket.prototype.toString=function(){
return "Id="+this.Id+", BucketType="+this.BucketType+", Persistence="+this.Persistence+", SizeMin="+this.SizeMin+", SizeRequested="+this.SizeRequested+", Reducible="+this.Reducible+", TotalSpace="+this.TotalSpace+", Data="+this.Data;
};
var SCORM_MODE_NORMAL="normal";
var SCORM_MODE_REVIEW="review";
var SCORM_MODE_BROWSE="browse";
var SCORM_STATUS_PASSED="passed";
var SCORM_STATUS_COMPLETED="completed";
var SCORM_STATUS_FAILED="failed";
var SCORM_STATUS_INCOMPLETE="incomplete";
var SCORM_STATUS_BROWSED="browsed";
var SCORM_STATUS_NOT_ATTEMPTED="not attempted";
var SCORM_STATUS_UNKNOWN="unknown";
var SCORM_EXIT_TIME_OUT="time-out";
var SCORM_EXIT_SUSPEND="suspend";
var SCORM_EXIT_LOGOUT="logout";
var SCORM_EXIT_NORMAL="normal";
var SCORM_EXIT_UNKNOWN="";
var SCORM_CREDIT="credit";
var SCORM_CREDIT_NO="no-credit";
var SCORM_ENTRY_AB_INITO="ab-initio";
var SCORM_ENTRY_RESUME="resume";
var SCORM_ENTRY_NORMAL="";
var SCORM_TRUE_FALSE="true-false";
var SCORM_CHOICE="choice";
var SCORM_FILL_IN="fill-in";
var SCORM_MATCHING="matching";
var SCORM_PERFORMANCE="performance";
var SCORM_SEQUENCING="sequencing";
var SCORM_LIKERT="likert";
var SCORM_NUMERIC="numeric";
var SCORM_LONG_FILL_IN="long-fill-in";
var SCORM_OTHER="other";
var SCORM_CORRECT="correct";
var SCORM_WRONG="wrong";
var SCORM_INCORRECT="incorrect";
var SCORM_UNANTICIPATED="unanticipated";
var SCORM_NEUTRAL="neutral";
var SCORM_RUNTIME_NAV_REQUEST_CONTINUE="continue";
var SCORM_RUNTIME_NAV_REQUEST_PREVIOUS="previous";
var SCORM_RUNTIME_NAV_REQUEST_CHOICE="choice";
var SCORM_RUNTIME_NAV_REQUEST_JUMP="jump";
var SCORM_RUNTIME_NAV_REQUEST_EXIT="exit";
var SCORM_RUNTIME_NAV_REQUEST_EXITALL="exitAll";
var SCORM_RUNTIME_NAV_REQUEST_ABANDON="abandon";
var SCORM_RUNTIME_NAV_REQUEST_ABANDONALL="abandonAll";
var SCORM_RUNTIME_NAV_REQUEST_SUSPENDALL="suspendAll";
var SCORM_RUNTIME_NAV_REQUEST_NONE="_none_";
function ActivityRunTime(_2cf,_2d0,_2d1,Exit,_2d3,Mode,_2d5,_2d6,_2d7,_2d8,_2d9,_2da,_2db,_2dc,_2dd,_2de,_2df,_2e0,_2e1,_2e2,_2e3,_2e4,_2e5){
this.CompletionStatus=_2cf;
this.Credit=_2d0;
this.Entry=_2d1;
this.Exit=Exit;
this.Location=_2d3;
this.Mode=Mode;
this.ProgressMeasure=_2d5;
this.ScoreRaw=_2d6;
this.ScoreMax=_2d7;
this.ScoreMin=_2d8;
this.ScoreScaled=_2d9;
this.SuccessStatus=_2da;
this.SuspendData=_2db;
this.TotalTime=_2dc;
this.TotalTimeTracked=_2dd;
this.AudioLevel=_2de;
this.LanguagePreference=_2df;
this.DeliverySpeed=_2e0;
this.AudioCaptioning=_2e1;
this.Comments=_2e2;
this.CommentsFromLMS=_2e3;
this.Interactions=_2e4;
this.Objectives=_2e5;
this.LookAheadCompletionStatus=_2cf;
this.LookAheadSuccessStatus=_2da;
this.CompletionStatusChangedDuringRuntime=false;
this.SuccessStatusChangedDuringRuntime=false;
this.SessionTime="";
this.NavRequest=SCORM_RUNTIME_NAV_REQUEST_NONE;
this.DataState=DATA_STATE_CLEAN;
}
ActivityRunTime.prototype.ResetState=ActivityRunTime_ResetState;
ActivityRunTime.prototype.GetXml=ActivityRunTime_GetXml;
ActivityRunTime.prototype.toString=ActivityRunTime_toString;
ActivityRunTime.prototype.SetDirtyData=ActivityRunTime_SetDirtyData;
ActivityRunTime.prototype.IsValidObjectiveIndex=ActivityRunTime_IsValidObjectiveIndex;
ActivityRunTime.prototype.IsValidInteractionIndex=ActivityRunTime_IsValidInteractionIndex;
ActivityRunTime.prototype.IsValidInteractionObjectiveIndex=ActivityRunTime_IsValidInteractionObjectiveIndex;
ActivityRunTime.prototype.IsValidInteractionCorrectResponseIndex=ActivityRunTime_IsValidInteractionCorrectResponseIndex;
ActivityRunTime.prototype.AddObjective=ActivityRunTime_AddObjective;
ActivityRunTime.prototype.AddInteraction=ActivityRunTime_AddInteraction;
ActivityRunTime.prototype.AddComment=ActivityRunTime_AddComment;
ActivityRunTime.prototype.FindObjectiveWithId=ActivityRunTime_FindObjectiveWithId;
function ActivityRunTime_ResetState(){
this.CompletionStatus=SCORM_STATUS_UNKNOWN;
this.Entry=SCORM_ENTRY_AB_INITO;
this.Exit=SCORM_EXIT_UNKNOWN;
this.Location=null;
if(RegistrationToDeliver.LessonMode!==SCORM_MODE_REVIEW&&RegistrationToDeliver.LessonMode!==SCORM_MODE_BROWSE){
this.Mode=SCORM_MODE_NORMAL;
}
this.ProgressMeasure=null;
this.ScoreRaw=null;
this.ScoreMax=null;
this.ScoreMin=null;
this.ScoreScaled=null;
this.SuccessStatus=SCORM_STATUS_UNKNOWN;
this.SuspendData=null;
this.TotalTime="PT0H0M0S";
this.TotalTimeTracked="PT0H0M0S";
if(Control.Package.Properties.MakeStudentPrefsGlobalToCourse!==true){
this.AudioLevel=1;
this.LanguagePreference="";
this.DeliverySpeed=1;
this.AudioCaptioning=0;
}
this.Comments=new Array();
this.CommentsFromLMS=new Array();
this.Interactions=new Array();
this.Objectives=new Array();
this.LookAheadCompletionStatus=this.CompletionStatus;
this.LookAheadSuccessStatus=this.SuccessStatus;
this.SessionTime="";
this.NavRequest=SCORM_RUNTIME_NAV_REQUEST_NONE;
this.CompletionStatusChangedDuringRuntime=false;
this.SuccessStatusChangedDuringRuntime=false;
this.SetDirtyData();
}
function ActivityRunTime_GetXml(_2e6){
var _2e7=new ServerFormater();
var xml=new XmlElement("ART");
xml.AddAttribute("AI",_2e6);
xml.AddAttribute("CS",_2e7.ConvertCompletionStatus(this.CompletionStatus));
xml.AddAttribute("C",_2e7.ConvertCredit(this.Credit));
xml.AddAttribute("E",_2e7.ConvertEntry(this.Entry));
xml.AddAttribute("Ex",_2e7.ConvertExit(this.Exit));
if(this.Location!==null){
xml.AddAttribute("L",_2e7.TrimToLength(this.Location,1000));
}
xml.AddAttribute("M",_2e7.ConvertMode(this.Mode));
if(this.ProgressMeasure!==null){
xml.AddAttribute("PM",this.ProgressMeasure);
}
if(this.ScoreRaw!==null){
xml.AddAttribute("SR",this.ScoreRaw);
}
if(this.ScoreMax!==null){
xml.AddAttribute("SM",this.ScoreMax);
}
if(this.ScoreMin!==null){
xml.AddAttribute("SMi",this.ScoreMin);
}
if(this.ScoreScaled!==null){
xml.AddAttribute("SS",this.ScoreScaled);
}
xml.AddAttribute("SuS",_2e7.ConvertSuccessStatus(this.SuccessStatus));
if(this.SuspendData!==null){
xml.AddAttribute("SD",_2e7.TrimToLength(this.SuspendData,64000));
}
xml.AddAttribute("TT",_2e7.ConvertTimeSpan(this.TotalTime));
xml.AddAttribute("TTT",_2e7.ConvertTimeSpan(this.TotalTimeTracked));
xml.AddAttribute("AL",this.AudioLevel);
xml.AddAttribute("LP",_2e7.TrimToLength(this.LanguagePreference,250));
xml.AddAttribute("DS",this.DeliverySpeed);
xml.AddAttribute("AC",this.AudioCaptioning);
var i;
for(i=0;i<this.Comments.length;i++){
xml.AddElement(this.Comments[i].GetXml(_2e6,i,false));
}
for(i=0;i<this.CommentsFromLMS.length;i++){
xml.AddElement(this.CommentsFromLMS[i].GetXml(_2e6,i,true));
}
for(i=0;i<this.Interactions.length;i++){
xml.AddElement(this.Interactions[i].GetXml(_2e6,i));
}
for(i=0;i<this.Objectives.length;i++){
xml.AddElement(this.Objectives[i].GetXml(_2e6,i));
}
return xml.toString();
}
function ActivityRunTime_toString(){
return "RunTimeData - CompletionStatus="+this.CompletionStatus+", SuccessStatus="+this.SuccessStatus;
}
function ActivityRunTime_SetDirtyData(){
this.DataState=DATA_STATE_DIRTY;
}
function ActivityRunTime_IsValidObjectiveIndex(_2ea){
_2ea=parseInt(_2ea,10);
if(_2ea<=this.Objectives.length){
return true;
}else{
return false;
}
}
function ActivityRunTime_IsValidInteractionIndex(_2eb){
_2eb=parseInt(_2eb,10);
if(_2eb<=this.Interactions.length){
return true;
}else{
return false;
}
}
function ActivityRunTime_IsValidInteractionObjectiveIndex(_2ec,_2ed){
_2ec=parseInt(_2ec,10);
_2ed=parseInt(_2ed,10);
if(this.Interactions[_2ec]){
if(_2ed<=this.Interactions[_2ec].Objectives.length){
return true;
}else{
return false;
}
}else{
if(_2ed===0){
return true;
}else{
return false;
}
}
}
function ActivityRunTime_IsValidInteractionCorrectResponseIndex(_2ee,_2ef){
_2ee=parseInt(_2ee,10);
_2ef=parseInt(_2ef,10);
if(this.Interactions[_2ee]){
if(_2ef<=this.Interactions[_2ee].CorrectResponses.length){
return true;
}else{
return false;
}
}else{
if(_2ef===0){
return true;
}else{
return false;
}
}
}
function ActivityRunTime_AddObjective(){
this.Objectives[this.Objectives.length]=new ActivityRunTimeObjective(null,SCORM_STATUS_UNKNOWN,SCORM_STATUS_UNKNOWN,null,null,null,null,null,null);
}
function ActivityRunTime_AddInteraction(){
this.Interactions[this.Interactions.length]=new ActivityRunTimeInteraction(null,null,null,null,null,null,null,null,null,new Array(),new Array());
}
function ActivityRunTime_AddComment(){
this.Comments[this.Comments.length]=new ActivityRunTimeComment(null,null,null,null,null);
}
function ActivityRunTime_FindObjectiveWithId(id){
for(var i=0;i<this.Objectives.length;i++){
if(this.Objectives[i].Identifier==id){
return this.Objectives[i];
}
}
return null;
}
function Activity(_2f2,_2f3,_2f4,_2f5,_2f6,_2f7,_2f8,_2f9,_2fa,_2fb,_2fc,_2fd,_2fe,_2ff,_300,_301,_302,_303,_304,_305,_306,_307,_308,_309,_30a,_30b,_30c,_30d,_30e){
this.StringIdentifier=null;
this.DatabaseId=_2f2;
this.ItemIdentifier=_2f3;
this.ScormObjectDatabaseId=_2f4;
this.ActivityProgressStatus=_2f5;
this.ActivityAttemptCount=_2f6;
this.AttemptProgressStatus=_2f7;
this.AttemptCompletionAmountStatus=_2f8;
this.AttemptCompletionAmount=_2f9;
this.AttemptCompletionStatus=_2fa;
this.Active=_2fb;
this.Suspended=_2fc;
this.Included=_2fd;
this.Ordinal=_2fe;
this.SelectedChildren=_2ff;
this.RandomizedChildren=_300;
this.ActivityObjectives=_301;
this.RunTime=_302;
this.PrevAttemptProgressStatus=_303;
this.PrevAttemptCompletionStatus=_304;
this.AttemptedDuringThisAttempt=_305;
this.FirstCompletionTimestampUtc=_306;
this.ActivityStartTimestampUtc=_307;
this.AttemptStartTimestampUtc=_308;
this.ActivityAbsoluteDuration=_309;
this.AttemptAbsoluteDuration=_30a;
this.ActivityExperiencedDurationTracked=_30b;
this.AttemptExperiencedDurationTracked=_30c;
this.ActivityExperiencedDurationReported=_30d;
this.AttemptExperiencedDurationReported=_30e;
this.IsDurations=ConvertIso8601TimeSpanToHundredths(_309)>=0;
this.ActivityEndedDate=null;
this.Sequencer=null;
this.LookAheadActivity=false;
this.LearningObject=null;
this.ParentActivity=null;
this.ChildActivities=new Array();
this.AvailableChildren=null;
this.DataState=DATA_STATE_CLEAN;
this.MenuItem=null;
this.CachedPrimaryObjective=null;
this.HiddenFromChoice=false;
this.HasSeqRulesRelevantToChoice=null;
this.HasChildActivitiesDeliverableViaFlow=false;
this.LaunchedThisSession=false;
}
Activity.prototype.GetXml=Activity_GetXml;
Activity.prototype.toString=Activity_toString;
Activity.prototype.GetTitle=Activity_GetTitle;
Activity.prototype.GetItemIdentifier=Activity_GetItemIdentifier;
Activity.prototype.GetDatabaseIdentifier=Activity_GetDatabaseIdentifier;
Activity.prototype.GetLaunchPath=Activity_GetLaunchPath;
Activity.prototype.IsDeliverable=Activity_IsDeliverable;
Activity.prototype.TransferRteDataToActivity=Activity_TransferRteDataToActivity;
Activity.prototype.IsTheRoot=Activity_IsTheRoot;
Activity.prototype.IsALeaf=Activity_IsALeaf;
Activity.prototype.IsActive=Activity_IsActive;
Activity.prototype.IsSuspended=Activity_IsSuspended;
Activity.prototype.HasSuspendedChildren=Activity_HasSuspendedChildren;
Activity.prototype.SetActive=Activity_SetActive;
Activity.prototype.SetSuspended=Activity_SetSuspended;
Activity.prototype.IsTracked=Activity_IsTracked;
Activity.prototype.IsCompletionSetByContent=Activity_IsCompletionSetByContent;
Activity.prototype.IsObjectiveSetByContent=Activity_IsObjectiveSetByContent;
Activity.prototype.GetAttemptProgressStatus=Activity_GetAttemptProgressStatus;
Activity.prototype.SetAttemptProgressStatus=Activity_SetAttemptProgressStatus;
Activity.prototype.SetAttemptCompletionStatus=Activity_SetAttemptCompletionStatus;
Activity.prototype.GetAttemptCompletionStatus=Activity_GetAttemptCompletionStatus;
Activity.prototype.GetChildren=Activity_GetChildren;
Activity.prototype.GetSequencingControlFlow=Activity_GetSequencingControlFlow;
Activity.prototype.GetSequencingControlChoice=Activity_GetSequencingControlChoice;
Activity.prototype.GetSequencingControlChoiceExit=Activity_GetSequencingControlChoiceExit;
Activity.prototype.GetSequencingControlForwardOnly=Activity_GetSequencingControlForwardOnly;
Activity.prototype.GetPreventActivation=Activity_GetPreventActivation;
Activity.prototype.GetConstrainedChoice=Activity_GetConstrainedChoice;
Activity.prototype.GetSelectionTiming=Activity_GetSelectionTiming;
Activity.prototype.GetSelectionCountStatus=Activity_GetSelectionCountStatus;
Activity.prototype.GetSelectionCount=Activity_GetSelectionCount;
Activity.prototype.GetRandomizationTiming=Activity_GetRandomizationTiming;
Activity.prototype.GetRandomizeChildren=Activity_GetRandomizeChildren;
Activity.prototype.GetLimitConditionAttemptControl=Activity_GetLimitConditionAttemptControl;
Activity.prototype.GetActivityProgressStatus=Activity_GetActivityProgressStatus;
Activity.prototype.SetActivityProgressStatus=Activity_SetActivityProgressStatus;
Activity.prototype.GetAttemptCount=Activity_GetAttemptCount;
Activity.prototype.GetLimitConditionAttemptLimit=Activity_GetLimitConditionAttemptLimit;
Activity.prototype.GetPreConditionRules=Activity_GetPreConditionRules;
Activity.prototype.GetPostConditionRules=Activity_GetPostConditionRules;
Activity.prototype.GetExitRules=Activity_GetExitRules;
Activity.prototype.IsSatisfied=Activity_IsSatisfied;
Activity.prototype.IsObjectiveStatusKnown=Activity_IsObjectiveStatusKnown;
Activity.prototype.IsObjectiveMeasureKnown=Activity_IsObjectiveMeasureKnown;
Activity.prototype.IsObjectiveMeasureGreaterThan=Activity_IsObjectiveMeasureGreaterThan;
Activity.prototype.IsObjectiveMeasureLessThan=Activity_IsObjectiveMeasureLessThan;
Activity.prototype.GetObjectiveMeasure=Activity_GetObjectiveMeasure;
Activity.prototype.IsCompleted=Activity_IsCompleted;
Activity.prototype.IsActivityProgressKnown=Activity_IsActivityProgressKnown;
Activity.prototype.IsAttempted=Activity_IsAttempted;
Activity.prototype.IsAttemptLimitExceeded=Activity_IsAttemptLimitExceeded;
Activity.prototype.GetObjectives=Activity_GetObjectives;
Activity.prototype.FindObjective=Activity_FindObjective;
Activity.prototype.GetPrimaryObjective=Activity_GetPrimaryObjective;
Activity.prototype.GetRollupObjectiveMeasureWeight=Activity_GetRollupObjectiveMeasureWeight;
Activity.prototype.GetMeasureSatisfactionIfActive=Activity_GetMeasureSatisfactionIfActive;
Activity.prototype.GetRollupRules=Activity_GetRollupRules;
Activity.prototype.ApplyRollupRule=Activity_ApplyRollupRule;
Activity.prototype.GetRollupObjectiveSatisfied=Activity_GetRollupObjectiveSatisfied;
Activity.prototype.GetRequiredForSatisfied=Activity_GetRequiredForSatisfied;
Activity.prototype.GetRequiredForNotSatisfied=Activity_GetRequiredForNotSatisfied;
Activity.prototype.RollupProgressCompletion=Activity_RollupProgressCompletion;
Activity.prototype.GetRequiredForCompleted=Activity_GetRequiredForCompleted;
Activity.prototype.GetRequiredForIncomplete=Activity_GetRequiredForIncomplete;
Activity.prototype.IncrementAttemptCount=Activity_IncrementAttemptCount;
Activity.prototype.SetRandomizedChildren=Activity_SetRandomizedChildren;
Activity.prototype.SetSelectedChildren=Activity_SetSelectedChildren;
Activity.prototype.GetRandomizedChildren=Activity_GetRandomizedChildren;
Activity.prototype.GetSelectedChildren=Activity_GetSelectedChildren;
Activity.prototype.GetActivityListBetweenChildren=Activity_GetActivityListBetweenChildren;
Activity.prototype.IsActivityAnAvailableChild=Activity_IsActivityAnAvailableChild;
Activity.prototype.IsActivityAnAvailableDescendent=Activity_IsActivityAnAvailableDescendent;
Activity.prototype.IsActivityTheLastAvailableChild=Activity_IsActivityTheLastAvailableChild;
Activity.prototype.IsActivityTheFirstAvailableChild=Activity_IsActivityTheFirstAvailableChild;
Activity.prototype.GetFirstAvailableChild=Activity_GetFirstAvailableChild;
Activity.prototype.GetNextActivity=Activity_GetNextActivity;
Activity.prototype.GetPreviousActivity=Actvity_GetPreviousActivity;
Activity.prototype.InitializeAvailableChildren=Activity_InitializeAvailableChildren;
Activity.prototype.GetAvailableChildren=Activity_GetAvailableChildren;
Activity.prototype.SetAvailableChildren=Activity_SetAvailableChildren;
Activity.prototype.IsAvailable=Activity_IsAvailable;
Activity.prototype.InitializeForNewAttempt=Activity_InitializeForNewAttempt;
Activity.prototype.ResetAttemptState=Activity_ResetAttemptState;
Activity.prototype.RollupDurations=Activity_RollupDurations;
Activity.prototype.SetDirtyData=Activity_SetDirtyData;
Activity.prototype.IsAnythingDirty=Activity_IsAnythingDirty;
Activity.prototype.MarkPostedObjectiveDataDirty=Activity_MarkPostedObjectiveDataDirty;
Activity.prototype.MarkPostedObjectiveDataClean=Activity_MarkPostedObjectiveDataClean;
Activity.prototype.MarkDirtyObjectiveDataPosted=Activity_MarkDirtyObjectiveDataPosted;
Activity.prototype.SetSequencer=Activity_SetSequencer;
Activity.prototype.Clone=Activity_Clone;
Activity.prototype.TearDown=Activity_TearDown;
Activity.prototype.DisplayInChoice=Activity_DisplayInChoice;
Activity.prototype.SetHiddenFromChoice=Activity_SetHiddenFromChoice;
Activity.prototype.SetLaunchedThisSession=Activity_SetLaunchedThisSession;
Activity.prototype.WasLaunchedThisSession=Activity_WasLaunchedThisSession;
Activity.prototype.SetAttemptedDuringThisAttempt=Activity_SetAttemptedDuringThisAttempt;
Activity.prototype.WasAttemptedDuringThisAttempt=Activity_WasAttemptedDuringThisAttempt;
Activity.prototype.GetMinProgressMeasure=Activity_GetMinProgressMeasure;
Activity.prototype.GetCompletionProgressWeight=Activity_GetCompletionProgressWeight;
Activity.prototype.GetCompletedByMeasure=Activity_GetCompletedByMeasure;
Activity.prototype.GetAttemptCompletionAmount=Activity_GetAttemptCompletionAmount;
Activity.prototype.SetAttemptCompletionAmount=Activity_SetAttemptCompletionAmount;
Activity.prototype.GetAttemptCompletionAmountStatus=Activity_GetAttemptCompletionAmountStatus;
Activity.prototype.SetAttemptCompletionAmountStatus=Activity_SetAttemptCompletionAmountStatus;
Activity.prototype.GetCompletionStatusChangedDuringRuntime=Activity_GetCompletionStatusChangedDuringRuntime;
Activity.prototype.GetSuccessStatusChangedDuringRuntime=Activity_GetSuccessStatusChangedDuringRuntime;
Activity.prototype.GetActivityStartTimestampUtc=Activity_GetActivityStartTimestampUtc;
Activity.prototype.SetActivityStartTimestampUtc=Activity_SetActivityStartTimestampUtc;
Activity.prototype.GetAttemptStartTimestampUtc=Activity_GetAttemptStartTimestampUtc;
Activity.prototype.SetAttemptStartTimestampUtc=Activity_SetAttemptStartTimestampUtc;
Activity.prototype.GetActivityAbsoluteDuration=Activity_GetActivityAbsoluteDuration;
Activity.prototype.SetActivityAbsoluteDuration=Activity_SetActivityAbsoluteDuration;
Activity.prototype.GetAttemptAbsoluteDuration=Activity_GetAttemptAbsoluteDuration;
Activity.prototype.SetAttemptAbsoluteDuration=Activity_SetAttemptAbsoluteDuration;
Activity.prototype.GetActivityExperiencedDurationTracked=Activity_GetActivityExperiencedDurationTracked;
Activity.prototype.SetActivityExperiencedDurationTracked=Activity_SetActivityExperiencedDurationTracked;
Activity.prototype.GetAttemptExperiencedDurationTracked=Activity_GetAttemptExperiencedDurationTracked;
Activity.prototype.SetAttemptExperiencedDurationTracked=Activity_SetAttemptExperiencedDurationTracked;
Activity.prototype.GetActivityExperiencedDurationReported=Activity_GetActivityExperiencedDurationReported;
Activity.prototype.SetActivityExperiencedDurationReported=Activity_SetActivityExperiencedDurationReported;
Activity.prototype.GetAttemptExperiencedDurationReported=Activity_GetAttemptExperiencedDurationReported;
Activity.prototype.SetAttemptExperiencedDurationReported=Activity_SetAttemptExperiencedDurationReported;
Activity.prototype.UsesDefaultSatisfactionRollupRules=Activity_UsesDefaultSatisfactionRollupRules;
Activity.prototype.UsesDefaultCompletionRollupRules=Activity_UsesDefaultCompletionRollupRules;
function Activity_GetXml(){
var _30f=new ServerFormater();
var xml=new XmlElement("A");
xml.AddAttribute("DI",this.DatabaseId);
xml.AddAttribute("II",this.ItemIdentifier);
xml.AddAttribute("APS",_30f.ConvertBoolean(this.ActivityProgressStatus));
xml.AddAttribute("AAC",this.ActivityAttemptCount);
xml.AddAttribute("AtPS",_30f.ConvertBoolean(this.AttemptProgressStatus));
xml.AddAttribute("ACS",_30f.ConvertBoolean(this.AttemptCompletionStatus));
xml.AddAttribute("ACAS",_30f.ConvertBoolean(this.AttemptCompletionAmountStatus));
xml.AddAttribute("ACA",this.AttemptCompletionAmount);
xml.AddAttribute("A",_30f.ConvertBoolean(this.Active));
xml.AddAttribute("S",_30f.ConvertBoolean(this.Suspended));
xml.AddAttribute("I",_30f.ConvertBoolean(this.Included));
xml.AddAttribute("O",this.Ordinal);
xml.AddAttribute("SC",_30f.ConvertBoolean(this.SelectedChildren));
xml.AddAttribute("RC",_30f.ConvertBoolean(this.RandomizedChildren));
xml.AddAttribute("PAPS",_30f.ConvertBoolean(this.PrevAttemptProgressStatus));
xml.AddAttribute("PACS",_30f.ConvertBoolean(this.PrevAttemptCompletionStatus));
xml.AddAttribute("ADTA",_30f.ConvertBoolean(this.AttemptedDuringThisAttempt));
if(this.FirstCompletionTimestampUtc!==null){
xml.AddAttribute("FCTU",this.FirstCompletionTimestampUtc);
}
if(this.IsDurations){
if(this.ActivityStartTimestampUtc!==null){
xml.AddAttribute("ASTU",this.ActivityStartTimestampUtc);
}
if(this.AttemptStartTimestampUtc!==null){
xml.AddAttribute("AtSTU",this.AttemptStartTimestampUtc);
}
xml.AddAttribute("AAD",_30f.ConvertTimeSpan(this.ActivityAbsoluteDuration));
xml.AddAttribute("AtAD",_30f.ConvertTimeSpan(this.AttemptAbsoluteDuration));
xml.AddAttribute("AEDT",_30f.ConvertTimeSpan(this.ActivityExperiencedDurationTracked));
xml.AddAttribute("AtEDT",_30f.ConvertTimeSpan(this.AttemptExperiencedDurationTracked));
xml.AddAttribute("AEDR",_30f.ConvertTimeSpan(this.ActivityExperiencedDurationReported));
xml.AddAttribute("AtEDR",_30f.ConvertTimeSpan(this.AttemptExperiencedDurationReported));
}
for(var i=0;i<this.ActivityObjectives.length;i++){
xml.AddElement(this.ActivityObjectives[i].GetXml(this.DatabaseId,i));
}
if(this.RunTime!==null){
xml.AddElement(this.RunTime.GetXml(this.DatabaseId));
}
return xml.toString();
}
function Activity_toString(){
var str=this.GetTitle()+" ("+this.GetItemIdentifier()+")";
return str;
}
function Activity_GetTitle(){
return this.LearningObject.Title;
}
function Activity_GetItemIdentifier(){
if(this.LearningObject.ItemIdentifier!==null&&this.LearningObject.ItemIdentifier!==""){
return this.LearningObject.ItemIdentifier;
}else{
return this.DatabaseId;
}
}
function Activity_GetDatabaseIdentifier(){
return this.DatabaseId;
}
function Activity_GetLaunchPath(){
return MergeQueryStringParameters(this.LearningObject.Href,this.LearningObject.Parameters);
}
function Activity_IsDeliverable(){
return (this.RunTime!==null);
}
function Activity_TransferRteDataToActivity(){
var _313=this.GetObjectives();
var _314;
var _315=this.GetPrimaryObjective();
var _316=false;
var id;
if(this.IsTracked()){
for(var i=0;i<_313.length;i++){
id=_313[i].GetIdentifier();
_314=this.RunTime.FindObjectiveWithId(id);
if(_314!==null){
if(_314.SuccessStatusChangedDuringRuntime===true){
if(_314.SuccessStatus==SCORM_STATUS_UNKNOWN){
_313[i].SetProgressStatus(false,false,this,true);
}else{
if(_314.SuccessStatus==SCORM_STATUS_PASSED){
_313[i].SetProgressStatus(true,false,this,true);
_313[i].SetSatisfiedStatus(true,false,this);
}else{
if(_314.SuccessStatus==SCORM_STATUS_FAILED){
_313[i].SetProgressStatus(true,false,this,true);
_313[i].SetSatisfiedStatus(false,false,this);
}else{
Debug.AssertError("Invalid success status ("+_314.SuccessStatus+") encountered in a RTE objective at position "+i);
}
}
}
}
if(_314.MeasureChangedDuringRuntime===true){
if(_314.ScoreScaled===null){
_313[i].SetMeasureStatus(false,this);
}else{
_313[i].SetMeasureStatus(true,this);
_313[i].SetNormalizedMeasure(_314.ScoreScaled,this);
}
}
_313[i].SetScoreRaw(_314.ScoreRaw);
_313[i].SetScoreMin(_314.ScoreMin);
_313[i].SetScoreMax(_314.ScoreMax);
if(_314.ProgressMeasureChangedDuringRuntime===true){
if(_315.GetIdentifier()===id){
_316=true;
if(_314.ProgressMeasure===null){
this.SetAttemptCompletionAmountStatus(false);
}else{
this.SetAttemptCompletionAmountStatus(true);
this.SetAttemptCompletionAmount(_314.ProgressMeasure);
}
}
if(_314.ProgressMeasure===null){
_313[i].SetProgressMeasureStatus(false);
}else{
_313[i].SetProgressMeasureStatus(true);
_313[i].SetProgressMeasure(_314.ProgressMeasure);
}
}
if(_314.CompletionStatusChangedDuringRuntime===true){
var _319=false;
var _31a=false;
if(_314.CompletionStatus==SCORM_STATUS_UNKNOWN){
_319=false;
}else{
if(_314.CompletionStatus==SCORM_STATUS_NOT_ATTEMPTED){
_319=true;
_31a=false;
}else{
if(_314.CompletionStatus==SCORM_STATUS_COMPLETED){
_319=true;
_31a=true;
}else{
if(_314.CompletionStatus==SCORM_STATUS_INCOMPLETE){
_319=true;
_31a=false;
}else{
if(_314.CompletionStatus==SCORM_STATUS_BROWSED){
_319=true;
_31a=false;
}else{
Debug.AssertError("Invalid completion status-"+_314.CompletionStatus);
}
}
}
}
}
_313[i].SetCompletionStatus(_319);
if(_319===true){
_313[i].SetCompletionStatusValue(_31a);
}
if(_315.GetIdentifier()===id){
this.SetAttemptProgressStatus(_319);
if(_319===true){
this.SetAttemptCompletionStatus(_31a);
}
}
}
}else{
if(id.length>0){
Debug.AssertError("Sequencing objective not found in runtime, id="+_313[i].GetIdentifier());
}
}
}
var _31b;
var _31c;
if(this.Sequencer.LookAhead===true){
_31b=this.RunTime.LookAheadSuccessStatus;
_31c=this.RunTime.LookAheadCompletionStatus;
}else{
_31b=this.RunTime.SuccessStatus;
_31c=this.RunTime.CompletionStatus;
}
if(_31b==SCORM_STATUS_UNKNOWN){
_315.SetProgressStatus(false,false,this);
}else{
if(_31b==SCORM_STATUS_PASSED){
_315.SetProgressStatus(true,false,this);
_315.SetSatisfiedStatus(true,false,this);
}else{
if(_31b==SCORM_STATUS_FAILED){
_315.SetProgressStatus(true,false,this);
_315.SetSatisfiedStatus(false,false,this);
}else{
Debug.LogSeq("`1375`"+_31b);
}
}
}
if(this.RunTime.ScoreScaled===null){
_315.SetMeasureStatus(false,this);
if(Control.Package.Properties.ScaleRawScore==true){
var _31d=NormalizeRawScore(this.RunTime.ScoreRaw,this.RunTime.ScoreMin,this.RunTime.ScoreMax);
if(_31d!==null&&_31d!==undefined){
_315.SetMeasureStatus(true,this);
_315.SetNormalizedMeasure(_31d,this);
}else{
Debug.LogSeq("`1042`"+this.Runtime.ScoreRaw+"`1708`"+this.RunTime.ScoreMin+"`1715`"+this.RunTime.ScoreMax);
}
}
}else{
_315.SetMeasureStatus(true,this);
_315.SetNormalizedMeasure(this.RunTime.ScoreScaled,this);
}
_315.SetScoreRaw(this.RunTime.ScoreRaw);
_315.SetScoreMin(this.RunTime.ScoreMin);
_315.SetScoreMax(this.RunTime.ScoreMax);
if(this.RunTime.ProgressMeasure===null){
if(_316!==true){
this.SetAttemptCompletionAmountStatus(false);
_315.SetProgressMeasureStatus(false);
}
}else{
this.SetAttemptCompletionAmountStatus(true);
this.SetAttemptCompletionAmount(this.RunTime.ProgressMeasure);
_315.SetProgressMeasureStatus(true);
_315.SetProgressMeasure(this.RunTime.ProgressMeasure);
}
var _31e=false;
var _31f=false;
if(this.RunTime.CompletionStatusChangedDuringRuntime===true){
if(_31c==SCORM_STATUS_UNKNOWN){
_31e=false;
_31f=false;
}else{
if(_31c==SCORM_STATUS_NOT_ATTEMPTED){
_31e=true;
_31f=false;
}else{
if(_31c==SCORM_STATUS_COMPLETED){
_31e=true;
_31f=true;
}else{
if(_31c==SCORM_STATUS_INCOMPLETE){
_31e=true;
_31f=false;
}else{
if(_31c==SCORM_STATUS_BROWSED){
_31e=true;
_31f=false;
}else{
Debug.AssertError("Invalid completion status-"+_31c);
}
}
}
}
}
this.SetAttemptProgressStatus(_31e);
if(_31e===true){
this.SetAttemptCompletionStatus(_31f);
}
}
if(this.LookAheadActivity===false){
this.SetDirtyData();
}
}else{
}
}
function Activity_IsTheRoot(){
var _320=this.ParentActivity;
var _321=(_320===null);
return _321;
}
function Activity_IsALeaf(){
if(this.ChildActivities.length===0){
return true;
}else{
return false;
}
}
function Activity_IsActive(){
var _322;
_322=this.Active;
return _322;
}
function Activity_IsSuspended(){
var _323;
_323=this.Suspended;
return _323;
}
function Activity_HasSuspendedChildren(){
var _324=this.GetChildren();
for(var i=0;i<_324.length;i++){
if(_324[i].IsSuspended()){
return true;
}
}
return false;
}
function Activity_SetActive(_326){
this.Active=_326;
this.SetDirtyData();
}
function Activity_SetSuspended(_327){
this.Suspended=_327;
this.SetDirtyData();
}
function Activity_IsTracked(){
var _328=this.LearningObject.SequencingData.Tracked;
return _328;
}
function Activity_IsCompletionSetByContent(){
var _329=false;
if(Control.Package.Properties.ForceObjectiveCompletionSetByContent==true){
_329=true;
}else{
_329=this.LearningObject.SequencingData.CompletionSetByContent;
}
return _329;
}
function Activity_IsObjectiveSetByContent(){
var _32a=false;
if(Control.Package.Properties.ForceObjectiveCompletionSetByContent==true){
_32a=true;
}else{
_32a=this.LearningObject.SequencingData.ObjectiveSetByContent;
}
return _32a;
}
function Activity_GetAttemptProgressStatus(){
if(this.IsTracked()===false){
return false;
}
var _32b=this.AttemptProgressStatus;
return _32b;
}
function Activity_SetAttemptProgressStatus(_32c){
this.AttemptProgressStatus=_32c;
if(_32c===true){
this.SetAttemptedDuringThisAttempt();
}
var _32d=this.GetPrimaryObjective();
_32d.SetCompletionStatus(_32c);
this.SetDirtyData();
}
function Activity_SetAttemptCompletionStatus(_32e){
if(this.FirstCompletionTimestampUtc===null&&_32e===true){
this.FirstCompletionTimestampUtc=ConvertDateToIso8601String(new Date());
}
this.AttemptCompletionStatus=_32e;
var _32f=this.GetPrimaryObjective();
_32f.SetCompletionStatusValue(_32e);
this.SetDirtyData();
}
function Activity_GetAttemptCompletionStatus(){
if(this.IsTracked()===false){
return false;
}
var _330=this.AttemptCompletionStatus;
return _330;
}
function Activity_GetChildren(){
return this.ChildActivities;
}
function Activity_GetSequencingControlFlow(){
var _331=this.LearningObject.SequencingData.ControlFlow;
return _331;
}
function Activity_GetSequencingControlChoice(){
var _332=this.LearningObject.SequencingData.ControlChoice;
return _332;
}
function Activity_GetSequencingControlChoiceExit(){
var _333=this.LearningObject.SequencingData.ControlChoiceExit;
return _333;
}
function Activity_GetSequencingControlForwardOnly(){
var _334=this.LearningObject.SequencingData.ControlForwardOnly;
return _334;
}
function Activity_GetPreventActivation(){
var _335=this.LearningObject.SequencingData.PreventActivation;
return _335;
}
function Activity_GetConstrainedChoice(){
var _336=this.LearningObject.SequencingData.ConstrainChoice;
return _336;
}
function Activity_GetSelectionTiming(){
var _337=this.LearningObject.SequencingData.SelectionTiming;
return _337;
}
function Activity_GetSelectionCountStatus(){
var _338=this.LearningObject.SequencingData.SelectionCountStatus;
return _338;
}
function Activity_GetSelectionCount(){
var _339=this.LearningObject.SequencingData.SelectionCount;
return _339;
}
function Activity_GetRandomizationTiming(){
var _33a=this.LearningObject.SequencingData.RandomizationTiming;
return _33a;
}
function Activity_GetRandomizeChildren(){
var _33b=this.LearningObject.SequencingData.RandomizeChildren;
return _33b;
}
function Activity_GetLimitConditionAttemptControl(){
var _33c=this.LearningObject.SequencingData.LimitConditionAttemptControl;
return _33c;
}
function Activity_GetActivityProgressStatus(){
if(this.IsTracked()===false){
return false;
}
var _33d=this.ActivityProgressStatus;
return _33d;
}
function Activity_SetActivityProgressStatus(_33e){
this.ActivityProgressStatus=_33e;
this.SetDirtyData();
}
function Activity_GetAttemptCount(){
var _33f=this.ActivityAttemptCount;
return _33f;
}
function Activity_GetLimitConditionAttemptLimit(){
var _340=this.LearningObject.SequencingData.LimitConditionAttemptLimit;
return _340;
}
function Activity_GetPreConditionRules(){
var _341=this.LearningObject.SequencingData.PreConditionSequencingRules;
return _341;
}
function Activity_GetPostConditionRules(){
var _342=this.LearningObject.SequencingData.PostConditionSequencingRules;
return _342;
}
function Activity_GetExitRules(){
var _343=this.LearningObject.SequencingData.ExitSequencingRules;
return _343;
}
function Activity_IsSatisfied(_344,_345){
if(_345===null||_345===undefined){
_345=false;
}
if(this.IsTracked()===false){
return RESULT_UNKNOWN;
}
var _346;
if(_344===""||_344===undefined||_344===null){
_346=this.GetPrimaryObjective();
}else{
_346=this.FindObjective(_344);
}
if(_346===null||_346===undefined){
Debug.AssertError("Sequencing rule references a bad objective.");
}
if(_346.GetProgressStatus(this,_345)===true){
if(_346.GetSatisfiedStatus(this,_345)===true){
return true;
}else{
return false;
}
}else{
return RESULT_UNKNOWN;
}
}
function Activity_IsObjectiveStatusKnown(_347,_348){
if(_348===null||_348===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into IsObjectiveStatusKnown");
}
if(this.IsTracked()===false){
return false;
}
var _349=this.FindObjective(_347);
if(_349.GetProgressStatus(this,_348)===true){
return true;
}else{
return false;
}
}
function Activity_IsObjectiveMeasureKnown(_34a,_34b){
if(_34b===null||_34b===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into IsObjectiveMeasureKnown");
}
if(this.IsTracked()===false){
return false;
}
var _34c=this.FindObjective(_34a);
if(_34c.GetMeasureStatus(this,_34b)===true){
return true;
}else{
return false;
}
}
function Activity_IsObjectiveMeasureGreaterThan(_34d,_34e,_34f){
if(_34f===null||_34f===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into IsObjectiveMeasureGreaterThan");
}
if(this.IsTracked()===false){
return RESULT_UNKNOWN;
}
var _350=this.FindObjective(_34d);
if(_350.GetMeasureStatus(this,_34f)===true){
if(_350.GetNormalizedMeasure(this,_34f)>_34e){
return true;
}else{
return false;
}
}else{
return RESULT_UNKNOWN;
}
}
function Activity_IsObjectiveMeasureLessThan(_351,_352,_353){
if(_353===null||_353===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into IsObjectiveMeasureLessThan");
}
if(this.IsTracked()===false){
return RESULT_UNKNOWN;
}
var _354=this.FindObjective(_351);
Debug.AssertError("The objective referenced by a sequencing rule was not found. Does the definition of the sequencing rule in the manifest erroneously reference the name of the global objective instead of the local name of the objective? Problematic reference="+_351,(_354===null));
if(_354.GetMeasureStatus(this,_353)===true){
if(_354.GetNormalizedMeasure(this,_353)<_352){
return true;
}else{
return false;
}
}else{
return RESULT_UNKNOWN;
}
}
function Activity_GetObjectiveMeasure(_355){
if(_355===null||_355===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into GetObjectiveMeasure");
}
if(this.IsTracked()===false){
return RESULT_UNKNOWN;
}
var _356=this.GetPrimaryObjective();
if(_356.GetMeasureStatus(this,_355)===true){
return _356.GetNormalizedMeasure(this,_355);
}else{
return RESULT_UNKNOWN;
}
}
function Activity_IsCompleted(_357,_358){
if(_358===null||_358===undefined){
_358=false;
}
if(this.IsTracked()===false){
return RESULT_UNKNOWN;
}
var _359=false;
var _35a=false;
if(Control.Package.LearningStandard.is20044thOrGreater()){
_359=true;
}
if(Control.Package.LearningStandard.is20044thOrGreater()===true&&this.GetCompletedByMeasure()===true){
_35a=true;
}
var _35b;
var _35c;
var _35d;
var _35e;
var _35f=null;
if(_359===true&&_357!==""&&_357!==undefined&&_357!==null){
_35f=this.FindObjective(_357);
if(_35f===null||_35f===undefined){
Debug.AssertError("Sequencing rule references a bad objective.");
}
}
if(_359===true&&_35f!==null&&_35f!==undefined){
_35b=_35f.GetCompletionStatus(this,_358);
_35c=_35f.GetCompletionStatusValue(this,_358);
if(_35a===true){
_35d=_35f.GetProgressMeasureStatus();
_35e=_35f.GetProgressMeasure();
}
}else{
var _360=this.GetPrimaryObjective();
var _361=_360.GetMaps("ReadCompletionStatus",true);
if(_359===true&&_361.length>0){
_35b=_360.GetCompletionStatus(this,_358);
_35c=_360.GetCompletionStatusValue(this,_358);
if(_35a===true){
_35d=_360.GetProgressMeasureStatus();
_35e=_360.GetProgressMeasure();
}
}else{
if(_358===true&&this.WasAttemptedDuringThisAttempt()===false){
_35b=this.PrevAttemptProgressStatus;
_35c=this.PrevAttemptCompletionStatus;
}else{
_35b=this.AttemptProgressStatus;
_35c=this.AttemptCompletionStatus;
}
if(_35a===true){
_35d=this.GetAttemptCompletionAmountStatus();
_35e=this.GetAttemptCompletionAmount();
}
}
}
if(_35a===true&&(this.IsActive()===false||this.GetMeasureSatisfactionIfActive()===true)){
if(_35d===true){
if(_35e>=this.GetMinProgressMeasure()){
return true;
}else{
return false;
}
}else{
return RESULT_UNKNOWN;
}
}
if(_35b===true){
if(_35c===true){
return true;
}else{
return false;
}
}else{
return RESULT_UNKNOWN;
}
}
function Activity_IsActivityProgressKnown(_362,_363){
if(_363===null||_363===undefined){
Debug.AssertError("ERROR - canLookAtPreviousAttempt must be passed into IsActivityProgressKnown");
}
if(this.IsTracked()===false){
return false;
}
var _364=false;
if(Control.Package.LearningStandard.is20044thOrGreater()){
_364=true;
}
var _365=this.ActivityProgressStatus;
var _366;
var _367;
var _368=null;
if(Control.Package.LearningStandard.is20044thOrGreater()===true&&this.GetCompletedByMeasure()===true){
_366=this.GetAttemptCompletionAmountStatus();
}else{
if(_364===true&&_362!==""&&_362!==undefined&&_362!==null){
_368=this.FindObjective(_362);
if(_368===null||_368===undefined){
Debug.AssertError("Sequencing rule references a bad objective.");
}
}
if(_364===true&&_368!==null&&_368!==undefined){
_366=_368.GetCompletionStatus(this,_363);
}else{
var _369=this.GetPrimaryObjective();
var _36a=_369.GetMaps("ReadCompletionStatus",true);
if(_364===true&&_36a.length>0){
_366=_369.GetCompletionStatus(this,_363);
}else{
if(_363===true&&this.WasAttemptedDuringThisAttempt()===false){
_366=this.PrevAttemptProgressStatus;
}else{
_366=this.AttemptProgressStatus;
}
}
}
}
if(_364===true){
return _366;
}else{
return (_366===true&&_365===true)?true:false;
}
}
function Activity_IsAttempted(){
if(this.IsTracked()===false){
return RESULT_UNKNOWN;
}
if(this.ActivityProgressStatus===true||Control.Package.LearningStandard.is20044thOrGreater()===true){
if(this.GetAttemptCount()>0){
return true;
}
}else{
return RESULT_UNKNOWN;
}
return false;
}
function Activity_IsAttemptLimitExceeded(){
if(this.IsTracked()===false){
return RESULT_UNKNOWN;
}
if(this.ActivityProgressStatus===true){
if(this.GetLimitConditionAttemptControl()===true){
if(this.GetAttemptCount()>=this.GetLimitConditionAttemptLimit()){
return true;
}
}
}else{
return RESULT_UNKNOWN;
}
return false;
}
function Activity_GetObjectives(){
return this.ActivityObjectives;
}
function Activity_FindObjective(_36b){
var _36c=this.GetObjectives();
for(var i=0;i<_36c.length;i++){
if(_36b===""||_36b===null){
if(_36c[i].GetContributesToRollup()===true){
return _36c[i];
}
}else{
if(_36c[i].GetIdentifier()==_36b){
return _36c[i];
}
}
}
return null;
}
function Activity_GetPrimaryObjective(){
if(this.CachedPrimaryObjective===null){
var _36e=null;
var _36f=this.GetObjectives();
for(var i=0;i<_36f.length;i++){
if(_36f[i].GetContributesToRollup()===true){
_36e=_36f[i];
break;
}
}
this.CachedPrimaryObjective=_36e;
}
if(this.CachedPrimaryObjective===null){
Debug.AssertError("Could not find a primary objective.");
}
return this.CachedPrimaryObjective;
}
function Activity_GetRollupObjectiveMeasureWeight(){
var _371=this.LearningObject.SequencingData.RollupObjectiveMeasureWeight;
_371=parseFloat(_371);
return _371;
}
function Activity_GetMeasureSatisfactionIfActive(){
var _372=this.LearningObject.SequencingData.MeasureSatisfactionIfActive;
return _372;
}
function Activity_GetRollupRules(){
var _373=this.LearningObject.SequencingData.RollupRules;
return _373;
}
function Activity_ApplyRollupRule(_374){
if(_374.Action==RULE_SET_SATISFIED||_374.Action==RULE_SET_NOT_SATISFIED){
this.LearningObject.UsesDefaultSatisfactionRollupRules=true;
}else{
this.LearningObject.UsesDefaultCompletionRollupRules=true;
}
var _375=this.LearningObject.SequencingData.RollupRules.length;
this.LearningObject.SequencingData.RollupRules[_375]=_374;
}
function Activity_GetRollupObjectiveSatisfied(){
var _376=this.LearningObject.SequencingData.RollupObjectiveSatisfied;
return _376;
}
function Activity_GetRequiredForSatisfied(){
var _377=this.LearningObject.SequencingData.RequiredForSatisfied;
return _377;
}
function Activity_GetRequiredForNotSatisfied(){
var _378=this.LearningObject.SequencingData.RequiredForNotSatisfied;
return _378;
}
function Activity_RollupProgressCompletion(){
var _379=this.LearningObject.SequencingData.RollupProgressCompletion;
return _379;
}
function Activity_GetRequiredForCompleted(){
var _37a=this.LearningObject.SequencingData.RequiredForCompleted;
return _37a;
}
function Activity_GetRequiredForIncomplete(){
var _37b=this.LearningObject.SequencingData.RequiredForIncomplete;
return _37b;
}
function Activity_IncrementAttemptCount(){
this.ActivityAttemptCount++;
}
function Activity_SetRandomizedChildren(_37c){
this.RandomizedChildren=_37c;
this.SetDirtyData();
}
function Activity_SetSelectedChildren(_37d){
this.SelectedChildren=_37d;
this.SetDirtyData();
}
function Activity_GetRandomizedChildren(){
var _37e=this.RandomizedChildren;
return _37e;
}
function Activity_GetSelectedChildren(){
var _37f=this.SelectedChildren;
return _37f;
}
function Activity_GetActivityListBetweenChildren(_380,_381,_382){
var _383=this.GetAvailableChildren();
var _384=new Array();
var _385=null;
var _386=null;
for(var i=0;i<_383.length;i++){
if(_380==_383[i]){
_385=i;
}
if(_381==_383[i]){
_386=i;
}
}
if(_385==_386){
if(_382){
_384[0]=_383[_385];
}
}else{
if(_385<_386){
if(_382){
_386++;
}
_384=_383.slice(_385,_386);
}else{
if(_385>_386){
if(!_382){
_386++;
}
_384=_383.slice(_386,_385+1);
}
}
}
return _384;
}
function Activity_IsActivityAnAvailableChild(_388){
var _389=this.GetAvailableChildren();
for(var i=0;i<_389.length;i++){
if(_389[i]==_388){
return true;
}
}
return false;
}
function Activity_IsActivityAnAvailableDescendent(_38b){
return Activity_SearchAllAvailableDescendents(this,_38b);
}
function Activity_SearchAllAvailableDescendents(_38c,_38d){
var _38e=_38c.GetAvailableChildren();
for(var i=0;i<_38e.length;i++){
if(_38e[i]==_38d){
return true;
}
if(Activity_SearchAllAvailableDescendents(_38e[i],_38d)){
return true;
}
}
return false;
}
function Activity_IsActivityTheLastAvailableChild(_390){
var _391=this.GetAvailableChildren();
if(_391[_391.length-1]==_390){
return true;
}
return false;
}
function Activity_IsActivityTheFirstAvailableChild(_392){
var _393=this.GetAvailableChildren();
if(_393[0]==_392){
return true;
}
return false;
}
function Activity_GetFirstAvailableChild(){
var _394=this.GetAvailableChildren();
return _394[0];
}
function Activity_GetNextActivity(_395){
var _396=null;
var _397=_395.ParentActivity;
var _398=_397.GetAvailableChildren();
for(var i=0;i<_398.length;i++){
if(_398[i]==_395){
_396=i;
break;
}
}
if(_396!==null&&_396<(_398.length-1)){
return _398[_396+1];
}
return null;
}
function Actvity_GetPreviousActivity(_39a){
var _39b=null;
var _39c=_39a.ParentActivity;
var _39d=_39c.GetAvailableChildren();
for(var i=0;i<_39d.length;i++){
if(_39d[i]==_39a){
_39b=i;
break;
}
}
if(_39b!==null&&_39b>0){
return _39d[_39b-1];
}
return null;
}
function Activity_InitializeAvailableChildren(){
var _39f=this.GetChildren();
var _3a0=new Array();
for(var i=0;i<_39f.length;i++){
if(_39f[i].Included===true){
_3a0[_3a0.length]=_39f[i];
}
}
if(_3a0.length===0){
this.SetAvailableChildren(_39f);
}else{
_3a0.sort(function(_3a2,_3a3){
var _3a4=_3a2.Ordinal;
var _3a5=_3a3.Ordinal;
if(_3a4<_3a5){
return -1;
}
if(_3a4>_3a5){
return 1;
}
return 0;
});
this.SetAvailableChildren(_3a0);
}
}
function Activity_GetAvailableChildren(){
if(this.AvailableChildren===null){
this.InitializeAvailableChildren();
}
return this.AvailableChildren;
}
function Activity_SetAvailableChildren(_3a6){
this.AvailableChildren=_3a6;
var _3a7=this.GetChildren();
var i;
for(i=0;i<_3a7.length;i++){
_3a7[i].Ordinal=0;
_3a7[i].Included=false;
}
for(i=0;i<this.AvailableChildren.length;i++){
this.AvailableChildren[i].Ordinal=(i+1);
this.AvailableChildren[i].Included=true;
}
this.SetDirtyData();
}
function Activity_IsAvailable(){
return this.Included;
}
function Activity_InitializeForNewAttempt(_3a9,_3aa){
var i;
this.AttemptedDuringThisAttempt=false;
var _3ac=this.GetObjectives();
if(_3a9&&this.LearningObject.SequencingData.UseCurrentAttemptObjectiveInformation===true){
_3a9=true;
for(i=0;i<_3ac.length;i++){
_3ac[i].ResetAttemptState();
}
}else{
_3a9=false;
}
if(_3aa&&this.LearningObject.SequencingData.UseCurrentAttemptProgressInformation===true){
_3aa=true;
this.ResetAttemptState();
}else{
_3aa=false;
}
var _3ad=this.GetChildren();
for(i=0;i<_3ad.length;i++){
_3ad[i].InitializeForNewAttempt(_3a9,_3aa);
}
if(SSP_ENABLED&&Control.Api.SSPApi){
Control.Api.SSPApi.ResetBucketsForActivity(this.DatabaseId);
}
this.SetDirtyData();
}
function Activity_ResetAttemptState(){
this.PrevAttemptProgressStatus=this.AttemptProgressStatus;
this.PrevAttemptCompletionStatus=this.AttemptCompletionStatus;
this.AttemptProgressStatus=false;
this.AttemptCompletionAmountStatus=false;
this.AttemptCompletionAmount=0;
this.AttemptCompletionStatus=false;
this.SetDirtyData();
}
function Activity_RollupDurations(){
var _3ae=null;
var _3af=null;
var _3b0=null;
var _3b1=null;
var _3b2=0;
var _3b3=0;
var _3b4=0;
var _3b5=0;
var _3b6=this.GetChildren();
for(var i=0;i<_3b6.length;i++){
if(_3b6[i].GetActivityStartTimestampUtc()){
if(!_3ae||_3b6[i].GetActivityStartTimestampUtc()<_3ae){
_3ae=_3b6[i].GetActivityStartTimestampUtc();
}
}
if(_3b6[i].ActivityEndedDate){
if(!_3b0||_3b6[i].ActivityEndedDate>_3b0){
_3b0=_3b6[i].ActivityEndedDate;
}
}
if(_3b6[i].GetActivityExperiencedDurationTracked()){
_3b2+=ConvertIso8601TimeSpanToHundredths(_3b6[i].GetActivityExperiencedDurationTracked());
}
if(_3b6[i].GetActivityExperiencedDurationReported()){
_3b3+=ConvertIso8601TimeSpanToHundredths(_3b6[i].GetActivityExperiencedDurationReported());
}
if(!this.GetAttemptStartTimestampUtc()||(_3b6[i].GetAttemptStartTimestampUtc()&&_3b6[i].GetAttemptStartTimestampUtc()>=this.GetAttemptStartTimestampUtc())){
if(!this.GetAttemptStartTimestampUtc()&&_3b6[i].GetAttemptStartTimestampUtc()){
if(!_3af||_3b6[i].GetAttemptStartTimestampUtc()<_3af){
_3af=_3b6[i].GetAttemptStartTimestampUtc();
}
}
if(_3b6[i].ActivityEndedDate){
if(!_3b1||_3b6[i].ActivityEndedDate>_3b1){
_3b1=_3b6[i].ActivityEndedDate;
}
}
if(_3b6[i].GetAttemptExperiencedDurationTracked()){
_3b4+=ConvertIso8601TimeSpanToHundredths(_3b6[i].GetAttemptExperiencedDurationTracked());
}
if(_3b6[i].GetAttemptExperiencedDurationReported()){
_3b5+=ConvertIso8601TimeSpanToHundredths(_3b6[i].GetAttemptExperiencedDurationReported());
}
}
}
if(!this.IsALeaf()&&_3ae!==null){
this.SetActivityStartTimestampUtc(_3ae);
if(!this.GetAttemptStartTimestampUtc()){
this.SetAttemptStartTimestampUtc(_3af);
}
this.ActivityEndedDate=_3b0;
var _3b8=GetDateFromUtcIso8601Time(this.GetActivityStartTimestampUtc());
var _3b9=GetDateFromUtcIso8601Time(this.GetAttemptStartTimestampUtc());
this.SetActivityAbsoluteDuration(ConvertHundredthsToIso8601TimeSpan((_3b0-_3b8)/10));
this.SetAttemptAbsoluteDuration(ConvertHundredthsToIso8601TimeSpan((_3b1-_3b9)/10));
this.SetActivityExperiencedDurationTracked(ConvertHundredthsToIso8601TimeSpan(_3b2));
this.SetActivityExperiencedDurationReported(ConvertHundredthsToIso8601TimeSpan(_3b3));
this.SetAttemptExperiencedDurationTracked(ConvertHundredthsToIso8601TimeSpan(_3b4));
this.SetAttemptExperiencedDurationReported(ConvertHundredthsToIso8601TimeSpan(_3b5));
}
}
function Activity_SetDirtyData(){
this.DataState=DATA_STATE_DIRTY;
}
function Activity_IsAnythingDirty(){
if(this.DataState==DATA_STATE_DIRTY){
return true;
}
for(var i=0;i<this.ActivityObjectives.length;i++){
if(this.ActivityObjectives[i].DataState==DATA_STATE_DIRTY){
return true;
}
}
return false;
}
function Activity_MarkPostedObjectiveDataDirty(){
for(var i=0;i<this.ActivityObjectives.length;i++){
if(this.ActivityObjectives[i].DataState==DATA_STATE_POSTED){
this.ActivityObjectives[i].SetDirtyData();
}
}
}
function Activity_MarkPostedObjectiveDataClean(){
for(var i=0;i<this.ActivityObjectives.length;i++){
if(this.ActivityObjectives[i].DataState==DATA_STATE_POSTED){
this.ActivityObjectives[i].DataState=DATA_STATE_CLEAN;
}
}
}
function Activity_MarkDirtyObjectiveDataPosted(){
for(var i=0;i<this.ActivityObjectives.length;i++){
if(this.ActivityObjectives[i].DataState==DATA_STATE_DIRTY){
this.ActivityObjectives[i].DataState=DATA_STATE_POSTED;
}
}
}
function Activity_SetSequencer(_3be,_3bf){
this.Sequencer=_3be;
this.LookAheadActivity=_3bf;
for(var i=0;i<this.ActivityObjectives.length;i++){
this.ActivityObjectives[i].SetSequencer(_3be);
}
}
function Activity_Clone(){
var _3c1=new Activity(this.DatabaseId,this.ItemIdentifier,this.ScormObjectDatabaseId,this.ActivityProgressStatus,this.ActivityAttemptCount,this.AttemptProgressStatus,this.AttemptCompletionAmountStatus,this.AttemptCompletionAmount,this.AttemptCompletionStatus,this.Active,this.Suspended,this.Included,this.Ordinal,this.SelectedChildren,this.RandomizedChildren,null,null,this.PrevAttemptProgressStatus,this.PrevAttemptCompletionStatus,this.AttemptedDuringThisAttempt,this.FirstCompletionTimestampUtc,this.ActivityStartTimestampUtc,this.AttemptStartTimestampUtc,this.ActivityAbsoluteDuration,this.AttemptAbsoluteDuration,this.ActivityExperiencedDurationTracked,this.AttemptExperiencedDurationTracked,this.ActivityExperiencedDurationReported,this.AttemptExperiencedDurationReported);
_3c1.StringIdentifier=this.toString();
_3c1.ActivityObjectives=new Array();
for(var _3c2 in this.ActivityObjectives){
_3c1.ActivityObjectives[_3c2]=this.ActivityObjectives[_3c2].Clone();
}
_3c1.RunTime=this.RunTime;
_3c1.LearningObject=this.LearningObject;
_3c1.DataState=DATA_STATE_CLEAN;
_3c1.LaunchedThisSession=this.LaunchedThisSession;
_3c1.AttemptedDuringThisAttempt=this.AttemptedDuringThisAttempt;
_3c1.UsesDefaultRollupRules=this.UsesDefaultRollupRules;
return _3c1;
}
function Activity_TearDown(){
this.StringIdentifier=null;
this.DatabaseId=null;
this.ScormObjectDatabaseId=null;
this.ActivityProgressStatus=null;
this.ActivityAttemptCount=null;
this.AttemptProgressStatus=null;
this.AttemptCompletionAmountStatus=null;
this.AttemptCompletionAmount=null;
this.AttemptCompletionStatus=null;
this.FirstCompletionTimestampUtc=null;
this.ActivityStartTimestampUtc=null;
this.AttemptStartTimestampUtc=null;
this.ActivityAbsoluteDuration=null;
this.AttemptAbsoluteDuration=null;
this.ActivityExperiencedDurationTracked=null;
this.AttemptExperiencedDurationTracked=null;
this.ActivityExperiencedDurationReported=null;
this.AttemptExperiencedDurationReported=null;
this.Active=null;
this.Suspended=null;
this.Included=null;
this.Ordinal=null;
this.SelectedChildren=null;
this.RandomizedChildren=null;
this.RunTime=null;
this.Sequencer=null;
this.LookAheadActivity=null;
this.LearningObject=null;
this.ParentActivity=null;
this.AvailableChildren=null;
this.DataState=null;
this.MenuItem=null;
this.ChildActivities=null;
this.CachedPrimaryObjective=null;
this.HiddenFromChoice=null;
for(var _3c3 in this.ActivityObjectives){
this.ActivityObjectives[_3c3].TearDown();
this.ActivityObjectives[_3c3]=null;
}
this.ActivityObjectives=null;
}
function Activity_DisplayInChoice(){
if(this.LearningObject.Visible===false){
return false;
}else{
if(this.IsAvailable()===false){
return false;
}else{
if(this.HiddenFromChoice===true){
return false;
}
}
}
return true;
}
function Activity_SetHiddenFromChoice(_3c4){
this.HiddenFromChoice=_3c4;
}
function Activity_WasLaunchedThisSession(){
return this.LaunchedThisSession;
}
function Activity_SetLaunchedThisSession(){
this.LaunchedThisSession=true;
}
function Activity_WasAttemptedDuringThisAttempt(){
return this.AttemptedDuringThisAttempt;
}
function Activity_SetAttemptedDuringThisAttempt(){
this.AttemptedDuringThisAttempt=true;
}
function Activity_GetMinProgressMeasure(){
return this.LearningObject.CompletionThreshold;
}
function Activity_GetCompletionProgressWeight(){
var _3c5=this.LearningObject.CompletionProgressWeight;
_3c5=parseFloat(_3c5);
return _3c5;
}
function Activity_GetCompletedByMeasure(){
return this.LearningObject.CompletedByMeasure;
}
function Activity_GetAttemptCompletionAmount(){
return this.AttemptCompletionAmount;
}
function Activity_SetAttemptCompletionAmount(_3c6){
this.AttemptCompletionAmount=_3c6;
}
function Activity_GetAttemptCompletionAmountStatus(){
return this.AttemptCompletionAmountStatus;
}
function Activity_SetAttemptCompletionAmountStatus(_3c7){
this.AttemptCompletionAmountStatus=_3c7;
}
function Activity_GetCompletionStatusChangedDuringRuntime(){
if(this.RunTime!==null){
return this.RunTime.CompletionStatusChangedDuringRuntime;
}
return false;
}
function Activity_GetSuccessStatusChangedDuringRuntime(){
if(this.RunTime!==null){
return this.RunTime.SuccessStatusChangedDuringRuntime;
}
return false;
}
function Activity_GetActivityStartTimestampUtc(){
return this.ActivityStartTimestampUtc;
}
function Activity_SetActivityStartTimestampUtc(_3c8){
this.ActivityStartTimestampUtc=_3c8;
this.SetDirtyData();
}
function Activity_GetAttemptStartTimestampUtc(){
return this.AttemptStartTimestampUtc;
}
function Activity_SetAttemptStartTimestampUtc(_3c9){
this.AttemptStartTimestampUtc=_3c9;
this.SetDirtyData();
}
function Activity_GetActivityAbsoluteDuration(){
return this.ActivityAbsoluteDuration;
}
function Activity_SetActivityAbsoluteDuration(_3ca){
this.ActivityAbsoluteDuration=_3ca;
this.SetDirtyData();
}
function Activity_GetAttemptAbsoluteDuration(){
return this.AttemptAbsoluteDuration;
}
function Activity_SetAttemptAbsoluteDuration(_3cb){
this.AttemptAbsoluteDuration=_3cb;
this.SetDirtyData();
}
function Activity_GetActivityExperiencedDurationTracked(){
return this.ActivityExperiencedDurationTracked;
}
function Activity_SetActivityExperiencedDurationTracked(_3cc){
this.ActivityExperiencedDurationTracked=_3cc;
this.SetDirtyData();
}
function Activity_GetAttemptExperiencedDurationTracked(){
return this.AttemptExperiencedDurationTracked;
}
function Activity_SetAttemptExperiencedDurationTracked(_3cd){
this.AttemptExperiencedDurationTracked=_3cd;
this.SetDirtyData();
}
function Activity_GetActivityExperiencedDurationReported(){
return this.ActivityExperiencedDurationReported;
}
function Activity_SetActivityExperiencedDurationReported(_3ce){
this.ActivityExperiencedDurationReported=_3ce;
this.SetDirtyData();
}
function Activity_GetAttemptExperiencedDurationReported(){
return this.AttemptExperiencedDurationReported;
}
function Activity_SetAttemptExperiencedDurationReported(_3cf){
this.AttemptExperiencedDurationReported=_3cf;
this.SetDirtyData();
}
function Activity_UsesDefaultSatisfactionRollupRules(){
return this.LearningObject.UsesDefaultSatisfactionRollupRules;
}
function Activity_UsesDefaultCompletionRollupRules(){
return this.LearningObject.UsesDefaultCompletionRollupRules;
}
function CosmeticPage(_3d0,_3d1,_3d2){
this.FrameName=_3d0;
this.PageHref=_3d1;
this.Parameters=_3d2;
}
function GlobalObjective(_3d3,ID,_3d5,_3d6,_3d7,_3d8,_3d9,_3da,_3db,_3dc,_3dd,_3de,_3df){
Debug.AssertError("Global Objective not created with all parameters (is the call missing the index?).",(_3d8===null||_3d8===undefined));
this.Index=_3d3;
this.ID=ID;
this.ProgressStatus=_3d5;
this.SatisfiedStatus=_3d6;
this.MeasureStatus=_3d7;
this.NormalizedMeasure=_3d8;
this.ScoreRaw=_3d9;
this.ScoreMin=_3da;
this.ScoreMax=_3db;
this.CompletionStatus=_3dc;
this.CompletionStatusValue=_3dd;
this.ProgressMeasureStatus=_3de;
this.ProgressMeasure=_3df;
this.DataState=DATA_STATE_CLEAN;
}
GlobalObjective.prototype.GetXml=GlobalObjective_GetXml;
GlobalObjective.prototype.Clone=GlobalObjective_Clone;
GlobalObjective.prototype.SetDirtyData=GlobalObjective_SetDirtyData;
GlobalObjective.prototype.ResetState=GlobalObjective_ResetState;
function GlobalObjective_GetXml(_3e0){
var _3e1=new ServerFormater();
var xml=new XmlElement("GO");
xml.AddAttribute("RI",_3e0);
xml.AddAttribute("ROI",this.Index);
xml.AddAttribute("I",this.ID);
xml.AddAttribute("PS",_3e1.ConvertBoolean(this.ProgressStatus));
xml.AddAttribute("SS",_3e1.ConvertBoolean(this.SatisfiedStatus));
xml.AddAttribute("MS",_3e1.ConvertBoolean(this.MeasureStatus));
xml.AddAttribute("NM",this.NormalizedMeasure);
xml.AddAttribute("CS",_3e1.ConvertBoolean(this.CompletionStatus));
xml.AddAttribute("CSV",_3e1.ConvertBoolean(this.CompletionStatusValue));
if(this.ScoreRaw!==null){
xml.AddAttribute("SR",this.ScoreRaw);
}
if(this.ScoreMax!==null){
xml.AddAttribute("SM",this.ScoreMax);
}
if(this.ScoreMin!==null){
xml.AddAttribute("SMi",this.ScoreMin);
}
xml.AddAttribute("PrMS",_3e1.ConvertBoolean(this.ProgressMeasureStatus));
if(this.ProgressMeasure!==null){
xml.AddAttribute("PM",this.ProgressMeasure);
}
return xml.toString();
}
function GlobalObjective_Clone(){
var _3e3=new GlobalObjective(this.Index,this.ID,this.ProgressStatus,this.SatisfiedStatus,this.MeasureStatus,this.NormalizedMeasure,this.ScoreRaw,this.ScoreMin,this.ScoreMax,this.CompletionStatus,this.CompletionStatusValue,this.ProgressMeasureStatus,this.ProgressMeasure);
return _3e3;
}
function GlobalObjective_SetDirtyData(){
this.DataState=DATA_STATE_DIRTY;
}
function GlobalObjective_ResetState(){
this.ProgressStatus=false;
this.SatisfiedStatus=false;
this.MeasureStatus=false;
this.NormalizedMeasure=0;
this.ScoreRaw=null;
this.ScoreMin=null;
this.ScoreMax=null;
this.CompletionStatus=false;
this.CompletionStatusValue=false;
this.ProgressMeasureStatus=false;
this.ProgressMeasure=null;
this.SetDirtyData();
}
function LearningObject(_3e4,Href,_3e6,_3e7,_3e8,_3e9,_3ea,_3eb,_3ec,_3ed,_3ee,_3ef,_3f0,_3f1,_3f2,_3f3,_3f4,_3f5,_3f6,_3f7,_3f8,_3f9){
this.Title=_3e4;
this.Href=Href;
this.Parameters=_3e6;
this.DataFromLms=_3e7;
this.MasteryScore=_3e8;
this.MaxTimeAllowed=_3e9;
this.TimeLimitAction=_3ea;
this.Prerequisites=_3eb;
this.Visible=_3ec;
this.CompletedByMeasure=_3ed;
this.CompletionThreshold=_3ee;
this.CompletionProgressWeight=_3ef;
this.PersistState=_3f0;
this.ItemIdentifier=_3f1;
this.ResourceIdentifier=_3f2;
this.ExternalIdentifier=_3f3;
this.DatabaseIdentifier=_3f4;
this.ScormType=_3f5;
this.SSPBuckets=_3f6;
this.SequencingData=_3f7;
this.SharedDataMaps=_3f8;
this.Children=_3f9;
this.UsesDefaultSatisfactionRollupRules=false;
this.UsesDefaultCompletionRollupRules=false;
}
LearningObject.prototype.GetScaledPassingScore=LearningObject_GetScaledPassingScore;
function LearningObject_GetScaledPassingScore(){
if(this.SequencingData!==null&&this.SequencingData.PrimaryObjective!==null){
if(this.SequencingData.PrimaryObjective.SatisfiedByMeasure===true){
if(this.SequencingData.PrimaryObjective.MinNormalizedMeasure!==null){
return this.SequencingData.PrimaryObjective.MinNormalizedMeasure;
}else{
return 1;
}
}
}
return null;
}
function LearningStandard(_3fa){
this.value=_3fa;
}
LearningStandard.prototype.isAICC=LearningStandard_isAICC;
LearningStandard.prototype.is12=LearningStandard_is12;
LearningStandard.prototype.is2004=LearningStandard_is2004;
LearningStandard.prototype.is20043rdOrGreater=LearningStandard_is20043rdOrGreater;
LearningStandard.prototype.is20044thOrGreater=LearningStandard_is20044thOrGreater;
function LearningStandard_isAICC(){
if(this.value==STANDARD_AICC){
return true;
}
return false;
}
function LearningStandard_is12(){
if(this.value==STANDARD_SCORM_12){
return true;
}
return false;
}
function LearningStandard_is2004(){
if(this.value==STANDARD_SCORM_2004_2ND_EDITION||this.value==STANDARD_SCORM_2004_3RD_EDITION||this.value==STANDARD_SCORM_2004_4TH_EDITION){
return true;
}
return false;
}
function LearningStandard_is20043rdOrGreater(){
if(this.value==STANDARD_SCORM_2004_3RD_EDITION||this.value==STANDARD_SCORM_2004_4TH_EDITION){
return true;
}
return false;
}
function LearningStandard_is20044thOrGreater(){
if(this.value==STANDARD_SCORM_2004_4TH_EDITION){
return true;
}
return false;
}
LearningStandard.prototype.toString=function(){
return this.value;
};
var NAVIGATION_REQUEST_START="START";
var NAVIGATION_REQUEST_RESUME_ALL="RESUME ALL";
var NAVIGATION_REQUEST_CONTINUE="CONTINUE";
var NAVIGATION_REQUEST_PREVIOUS="PREVIOUS";
var NAVIGATION_REQUEST_FORWARD="FORWARD";
var NAVIGATION_REQUEST_BACKWARD="BACKWARD";
var NAVIGATION_REQUEST_CHOICE="CHOICE";
var NAVIGATION_REQUEST_EXIT="EXIT";
var NAVIGATION_REQUEST_EXIT_ALL="EXIT ALL";
var NAVIGATION_REQUEST_SUSPEND_ALL="SUSPEND ALL";
var NAVIGATION_REQUEST_ABANDON="ABANDON";
var NAVIGATION_REQUEST_ABANDON_ALL="ABANDON ALL";
var NAVIGATION_REQUEST_NOT_VALID="INVALID";
var NAVIGATION_REQUEST_JUMP="JUMP";
var NAVIGATION_REQUEST_DISPLAY_MESSAGE="DISPLAY MESSAGE";
var NAVIGATION_REQUEST_EXIT_PLAYER="EXIT PLAYER";
function NavigationRequest(type,_3fc,_3fd){
this.Type=type;
this.TargetActivity=_3fc;
this.MessageToUser=_3fd;
}
NavigationRequest.prototype.toString=function(){
return "Type="+this.Type+", TargetActivity="+this.TargetActivity+", MessageToUser="+this.MessageToUser;
};
NavigationRequest.prototype.toDescriptiveString=function(){
var ret="";
ret+=this.Type;
if(this.TargetActivity!==null){
ret+=" targeting \""+this.TargetActivity+"\".";
}
return ret;
};
function PackageProperties(_3ff,_400,_401,_402,_403,_404,_405,_406,_407,_408,_409,_40a,_40b,_40c,_40d,_40e,_40f,_410,_411,_412,_413,_414,_415,_416,_417,_418,_419,_41a,_41b,_41c,_41d,_41e,_41f,_420,_421,_422,_423,_424,_425,_426,_427,_428,_429,_42a,_42b,_42c,_42d,_42e,_42f,_430,_431,_432,_433,_434,_435,_436,_437,_438,_439,_43a,_43b,_43c,_43d,_43e,_43f,_440,_441,_442,_443,_444,_445,_446,_447,_448,_449,_44a,_44b,_44c){
this.ShowFinishButton=_3ff;
this.ShowCloseItem=_400;
this.ShowHelp=_401;
this.ShowProgressBar=_402;
this.UseMeasureProgressBar=_403;
this.ShowCourseStructure=_404;
this.CourseStructureStartsOpen=_405;
this.ShowNavBar=_406;
this.ShowTitleBar=_407;
this.EnableFlowNav=_408;
this.EnableChoiceNav=_409;
this.DesiredWidth=_40a;
this.DesiredHeight=_40b;
this.DesiredFullScreen=_40c;
this.RequiredWidth=_40d;
this.RequiredHeight=_40e;
this.RequiredFullScreen=_40f;
this.CourseStructureWidth=_410;
this.ScoLaunchType=_411;
this.PlayerLaunchType=_412;
this.IntermediateScoSatisfiedNormalExitAction=_413;
this.IntermediateScoSatisfiedSuspendExitAction=_414;
this.IntermediateScoSatisfiedTimeoutExitAction=_415;
this.IntermediateScoSatisfiedLogoutExitAction=_416;
this.IntermediateScoNotSatisfiedNormalExitAction=_417;
this.IntermediateScoNotSatisfiedSuspendExitAction=_418;
this.IntermediateScoNotSatisfiedTimeoutExitAction=_419;
this.IntermediateScoNotSatisfiedLogoutExitAction=_41a;
this.FinalScoCourseSatisfiedNormalExitAction=_41b;
this.FinalScoCourseSatisfiedSuspendExitAction=_41c;
this.FinalScoCourseSatisfiedTimeoutExitAction=_41d;
this.FinalScoCourseSatisfiedLogoutExitAction=_41e;
this.FinalScoCourseNotSatisfiedNormalExitAction=_41f;
this.FinalScoCourseNotSatisfiedSuspendExitAction=_420;
this.FinalScoCourseNotSatisfiedTimeoutExitAction=_421;
this.FinalScoCourseNotSatisfiedLogoutExitAction=_422;
this.PreventRightClick=_423;
this.PreventWindowResize=_424;
this.StatusDisplay=_425;
this.ScoreRollupMode=_426;
this.NumberOfScoringObjects=_427;
this.StatusRollupMode=_428;
this.ThresholdScore=_429;
this.FirstScoIsPretest=_42a;
this.WrapScoWindowWithApi=_42b;
this.FinishCausesImmediateCommit=_42c;
this.DebugControlAudit=_42d;
this.DebugControlDetailed=_42e;
this.DebugRteAudit=_42f;
this.DebugRteDetailed=_430;
this.DebugSequencingAudit=_431;
this.DebugSequencingDetailed=_432;
this.DebugSequencingSimple=_433;
this.DebugLookAheadAudit=_434;
this.DebugLookAheadDetailed=_435;
this.DebugIncludeTimestamps=_436;
this.CaptureHistory=_437;
this.CaptureHistoryDetailed=_438;
this.CommMaxFailedSubmissions=_439;
this.CommCommitFrequency=_43a;
this.InvalidMenuItemAction=_43b;
this.AlwaysFlowToFirstSco=_43c;
this.LogoutCausesPlayerExit=_43d;
this.ResetRunTimeDataTiming=_43e;
this.ValidateInteractionResponses=_43f;
this.LookaheadSequencerMode=_440;
this.ScoreOverridesStatus=_441;
this.ScaleRawScore=_442;
this.RollupEmptySetToUnknown=_443;
this.ReturnToLmsAction=_444;
this.UseQuickLookaheadSequencer=_445;
this.ForceDisableRootChoice=_446;
this.RollupRuntimeAtScoUnload=_447;
this.ForceObjectiveCompletionSetByContent=_448;
this.InvokeRollupAtSuspendAll=_449;
this.CompletionStatOfFailedSuccessStat=_44a;
this.SatisfiedCausesCompletion=_44b;
this.MakeStudentPrefsGlobalToCourse=_44c;
}
function Package(Id,_44e,_44f,_450,_451,_452){
this.Id=Id;
this.ObjectivesGlobalToSystem=_44e;
this.LearningStandard=new LearningStandard(_44f);
this.Properties=_450;
this.SharedDataGlobalToSystem=_451;
this.LearningObjects=_452;
}
function PossibleRequest(_453,_454,_455,_456,_457){
this.NavigationRequest=_453;
this.TargetActivityItemIdentifier=_454;
this.WillSucceed=_455;
this.Exception=_456;
this.ExceptionText=_457;
this.TargetActivity=null;
this.SequencingRequest=null;
this.TerminationSequencingRequest=null;
this.Hidden=false;
this.Disabled=false;
this.PreConditionSkipped=false;
this.PreConditionStopForwardTraversal=false;
this.PreConditionStopForwardTraversalViolation=false;
this.PreConditionDisabled=false;
this.PreConditionHiddenFromChoice=false;
this.LimitConditionViolation=false;
this.IsVisibleViolation=false;
this.PreventActivationViolation=false;
this.ControlChoiceViolation=false;
this.ForwardOnlyViolation=false;
this.ChoiceExitViolation=false;
this.ConstrainedChoiceViolation=false;
this.NoDeliverablieActivityViolation=false;
this.WillAlwaysSucceed=false;
this.WillNeverSucceed=false;
}
PossibleRequest.prototype.toString=function(){
return "Navigation Request = "+this.NavigationRequest+", TargetActivityItemIdentifier="+this.TargetActivityItemIdentifier;
};
PossibleRequest.prototype.GetErrorString=function(){
var ret=this.ExceptionText;
return ret.trim();
};
PossibleRequest.prototype.GetExceptionReason=function(){
var ret=this.ExceptionText;
ret.trim();
ret+="["+this.Exception+"], ";
ret+=", WillAlwaysSucceed = ";
ret+=this.WillAlwaysSucceed.toString();
ret+=", WillNeverSucceed = ";
ret+=this.WillNeverSucceed.toString();
ret+=", PreConditionSkipped = ";
ret+=this.PreConditionSkipped.toString();
ret+=", PreConditionStopForwardTraversal = ";
ret+=this.PreConditionStopForwardTraversal.toString();
ret+=", PreConditionDisabled = ";
ret+=this.PreConditionDisabled.toString();
ret+=", PreConditionHiddenFromChoice = ";
ret+=this.PreConditionHiddenFromChoice.toString();
ret+=", LimitConditionViolation = ";
ret+=this.LimitConditionViolation.toString();
ret+=", IsVisibleViolation = ";
ret+=this.IsVisibleViolation.toString();
ret+=", PreventActivationViolation = ";
ret+=this.PreventActivationViolation.toString();
ret+=", ControlChoiceViolation = ";
ret+=this.ControlChoiceViolation.toString();
ret+=", ForwardOnlyViolation = ";
ret+=this.ForwardOnlyViolation.toString();
ret+=", ChoiceExitViolation = ";
ret+=this.ChoiceExitViolation.toString();
ret+=", ConstrainedChoiceViolation = ";
ret+=this.ConstrainedChoiceViolation.toString();
return ret;
};
PossibleRequest.prototype.ResetForNewEvaluation=function(){
this.WillSucceed=true;
this.Hidden=false;
this.Disabled=false;
this.PreConditionSkipped=false;
this.PreConditionStopForwardTraversal=false;
this.PreConditionDisabled=false;
this.PreConditionHiddenFromChoice=false;
this.LimitConditionViolation=false;
this.IsVisibleViolation=false;
this.PreventActivationViolation=false;
this.ControlChoiceViolation=false;
this.ForwardOnlyViolation=false;
this.ChoiceExitViolation=false;
this.ConstrainedChoiceViolation=false;
};
function Registration(Id,_45b,_45c,_45d,_45e,_45f,_460,_461,_462){
this.Id=Id;
this.SuspendedActivity=_45b;
this.TrackingEnabled=_45c;
this.LessonMode=_45d;
this.Package=_45e;
this.Activities=_45f;
this.GlobalObjectives=_460;
this.SSPBuckets=_461;
this.SharedData=_462;
}
Registration.prototype.FindActivityForThisScormObject=Registration_FindActivityForThisScormObject;
function Registration_FindActivityForThisScormObject(_463){
for(var i=0;i<this.Activities.length;i++){
if(this.Activities[i].ScormObjectDatabaseId==_463){
return this.Activities[i];
}
}
Debug.AssertError("Registration_FindActivityForThisScormObject could not find the activity for learning object "+_463);
return null;
}
function SequencingData(_465,_466,_467,_468,_469,_46a,_46b,_46c,_46d,_46e,_46f,_470,_471,_472,_473,_474,_475,_476,_477,_478,_479,_47a,_47b,_47c,_47d,_47e,_47f,_480,_481,_482,_483,_484,_485,_486,_487,_488,_489,_48a,_48b,_48c,_48d,_48e,_48f){
this.Identifier=_465;
this.Identifierref=_466;
this.ControlChoice=_467;
this.ControlChoiceExit=_468;
this.ControlFlow=_469;
this.ControlForwardOnly=_46a;
this.UseCurrentAttemptObjectiveInformation=_46b;
this.UseCurrentAttemptProgressInformation=_46c;
this.ConstrainChoice=_46d;
this.PreventActivation=_46e;
this.PreConditionSequencingRules=_46f;
this.PostConditionSequencingRules=_470;
this.ExitSequencingRules=_471;
this.LimitConditionAttemptControl=_472;
this.LimitConditionAttemptLimit=_473;
this.LimitConditionAttemptAbsoluteDurationControl=_474;
this.LimitConditionAttemptAbsoluteDurationLimit=_475;
this.RollupRules=_476;
this.RollupObjectiveSatisfied=_477;
this.RollupObjectiveMeasureWeight=_478;
this.RollupProgressCompletion=_479;
this.MeasureSatisfactionIfActive=_47a;
this.RequiredForSatisfied=_47b;
this.RequiredForNotSatisfied=_47c;
this.RequiredForCompleted=_47d;
this.RequiredForIncomplete=_47e;
this.PrimaryObjective=_47f;
this.Objectives=_480;
this.SelectionTiming=_481;
this.SelectionCountStatus=_482;
this.SelectionCount=_483;
this.RandomizationTiming=_484;
this.RandomizeChildren=_485;
this.Tracked=_486;
this.CompletionSetByContent=_487;
this.ObjectiveSetByContent=_488;
this.HidePrevious=_489;
this.HideContinue=_48a;
this.HideExit=_48b;
this.HideAbandon=_48c;
this.HideSuspendAll=_48d;
this.HideAbandonAll=_48e;
this.HideExitAll=_48f;
}
function SequencingObjectiveMap(_490,_491,_492,_493,_494,_495,_496,_497,_498,_499,_49a,_49b,_49c,_49d,_49e){
this.TargetObjectiveId=_490;
this.ReadSatisfiedStatus=_491;
this.ReadNormalizedMeasure=_492;
this.ReadRawScore=_493;
this.ReadMinScore=_494;
this.ReadMaxScore=_495;
this.ReadCompletionStatus=_496;
this.ReadProgressMeasure=_497;
this.WriteSatisfiedStatus=_498;
this.WriteNormalizedMeasure=_499;
this.WriteRawScore=_49a;
this.WriteMinScore=_49b;
this.WriteMaxScore=_49c;
this.WriteCompletionStatus=_49d;
this.WriteProgressMeasure=_49e;
}
SequencingObjectiveMap.prototype.toString=function(){
return "TargetObjectiveId="+this.TargetObjectiveId+", ReadSatisfiedStatus="+this.ReadSatisfiedStatus+", ReadNormalizedMeasure="+this.ReadNormalizedMeasure+", ReadRawScore="+this.ReadRawScore+", ReadMinScore="+this.ReadMinScore+", ReadMaxScore="+this.ReadMaxScore+", ReadCompletionStatus="+this.ReadCompletionStatus+", ReadProgressMeasure="+this.ReadProgressMeasure+", WriteSatisfiedStatus="+this.WriteSatisfiedStatus+", WriteNormalizedMeasure="+this.WriteNormalizedMeasure+", WriteRawScore="+this.WriteRawScore+", WriteMinScore="+this.WriteMinScore+", WriteMaxScore="+this.WriteMaxScore+", WriteCompletionStatus="+this.WriteCompletionStatus+", WriteProgressMeasure="+this.ReadProgressMeasure;
};
function SequencingObjective(Id,_4a0,_4a1,Maps){
this.Id=Id;
this.SatisfiedByMeasure=_4a0;
this.MinNormalizedMeasure=_4a1;
this.Maps=Maps;
}
SequencingObjective.prototype.toString=function(){
var ret="Id="+this.Id+", SatisfiedByMeasure="+this.SatisfiedByMeasure+", MinNormalizedMeasure="+this.MinNormalizedMeasure;
ret+="Maps:";
for(var map in this.Maps){
ret+="{"+map+"}"+this.Maps[map]+"  ";
}
return ret;
};
function SequencingRollupRuleCondition(_4a5,_4a6){
this.Operator=_4a5;
this.Condition=_4a6;
}
SequencingRollupRuleCondition.prototype.toString=function(){
var ret="";
if(this.Operator==RULE_CONDITION_OPERATOR_NOT){
ret+="NOT ";
}
ret+=this.Condition;
return ret;
};
function SequencingRollupRule(_4a8,_4a9,_4aa,_4ab,_4ac,_4ad){
this.ConditionCombination=_4a8;
this.ChildActivitySet=_4a9;
this.MinimumCount=_4aa;
this.MinimumPercent=_4ab;
this.Action=_4ac;
this.Conditions=_4ad;
}
SequencingRollupRule.prototype.toString=function(){
var ret="If ";
if(this.ChildActivitySet==CHILD_ACTIVITY_SET_AT_LEAST_COUNT){
ret+="At Least "+this.MinimumCount+" Child Activities Meet ";
}else{
if(this.ChildActivitySet==CHILD_ACTIVITY_SET_AT_LEAST_PERCENT){
ret+="At Least "+this.MinimumPercent+" Percent of Child Activities Meet ";
}else{
if(this.ChildActivitySet==CHILD_ACTIVITY_SET_ALL){
ret+="All Child Activities Meet ";
}else{
if(this.ChildActivitySet==CHILD_ACTIVITY_SET_ANY){
ret+="Any Child Activity Meets ";
}else{
if(this.ChildActivitySet==CHILD_ACTIVITY_SET_NONE){
ret+="No Child Activity Meets ";
}
}
}
}
}
if(this.ConditionCombination==RULE_CONDITION_COMBINATION_ANY){
ret+=" Any Condition ";
}else{
ret+=" All Conditions ";
}
ret+=" THEN "+this.Action;
if(this.Conditions.length>1){
ret+=". Conditions: ";
for(var _4af in this.Conditions){
ret+="{"+_4af+"} "+this.Conditions[_4af]+"; ";
}
}else{
ret+=". Condition: "+this.Conditions[0];
}
return ret;
};
function SequencingRuleCondition(_4b0,_4b1,_4b2,_4b3){
this.Condition=_4b0;
this.ReferencedObjective=_4b1;
this.MeasureThreshold=_4b2;
this.Operator=_4b3;
}
SequencingRuleCondition.prototype.toString=function(){
var ret="";
if(this.ReferencedObjective!=null&&this.ReferencedObjective.length>0){
ret+="Objective "+this.ReferencedObjective+" ";
}else{
ret+="Activity ";
}
if(this.Operator==RULE_CONDITION_OPERATOR_NOT){
ret+="NOT ";
}
ret+=this.Condition;
if(this.Condition==SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_GREATER_THAN||this.Condition==SEQUENCING_RULE_CONDITION_OBJECTIVE_MEASURE_LESS_THAN){
ret+=" "+this.MeasureThreshold;
}
return ret;
};
function SequencingRule(_4b5,_4b6,_4b7){
this.ConditionCombination=_4b5;
this.Action=_4b6;
this.RuleConditions=_4b7;
}
SequencingRule.prototype.toString=function(){
var ret="If "+this.ConditionCombination+" condition(s) evaluate to true, then "+this.Action+".  ";
if(this.RuleConditions.length>1){
ret+="Conditions: ";
for(var _4b9 in this.RuleConditions){
ret+="{"+_4b9+"} "+this.RuleConditions[_4b9]+"; ";
}
}else{
ret+="Condition: "+this.RuleConditions[0];
}
return ret;
};
function SharedDataMap(_4ba,_4bb,_4bc){
this.Id=_4ba;
this.ReadSharedData=_4bb;
this.WriteSharedData=_4bc;
}
SharedDataMap.prototype.toString=function(){
return "Id="+this.Id+", ReadSharedData="+this.ReadSharedData+", WriteSharedData="+this.WriteSharedData;
};
function SharedData(_4bd,_4be,Data){
this.SharedDataId=_4bd;
this.SharedDataValId=_4be;
this.Data=Data;
this.DataState=DATA_STATE_CLEAN;
}
SharedData.prototype.toString=function(){
return "SharedDataId="+this.SharedDataId+", SharedDataValId="+this.SharedDataValId+", Data="+this.Data;
};
SharedData.prototype.GetXml=SharedData_GetXml;
SharedData.prototype.GetData=SharedData_GetData;
SharedData.prototype.WriteData=SharedData_WriteData;
SharedData.prototype.SetDirtyData=SharedData_SetDirtyData;
function SharedData_GetXml(){
var _4c0=new ServerFormater();
var xml=new XmlElement("SD");
xml.AddAttribute("SDVI",this.SharedDataValId);
xml.AddAttribute("D",_4c0.TrimToLength(this.Data,64000));
return xml.toString();
}
function SharedData_GetData(){
return this.Data;
}
function SharedData_WriteData(_4c2){
this.Data=_4c2;
this.SetDirtyData();
}
function SharedData_SetDirtyData(){
this.DataState=DATA_STATE_DIRTY;
}
function SSPBucketDefinition(Id,_4c4,_4c5,_4c6,_4c7,_4c8){
this.Id=Id;
this.BucketType=_4c4;
this.Persistence=_4c5;
this.SizeMin=_4c6;
this.SizeRequested=_4c7;
this.Reducible=_4c8;
}
SSPBucketDefinition.prototype.toString=function(){
return "Id="+this.Id+", BucketType="+this.BucketType+", Persistence="+this.Persistence+", SizeMin="+this.SizeMin+", SizeRequested="+this.SizeRequested+", Reducible="+this.Reducible;
};
var SSP_ALLOCATION_SUCCESS_FAILURE="failure";
var SSP_ALLOCATION_SUCCESS_MINIMUM="minimum";
var SSP_ALLOCATION_SUCCESS_REQUESTED="requested";
var SSP_ALLOCATION_SUCCESS_NOT_ATTEMPTED="not attempted";
var SSP_PERSISTENCE_LEARNER="learner";
var SSP_PERSISTENCE_COURSE="course";
var SSP_PERSISTENCE_SESSION="session";
function SSPBucket(_4c9,Id,_4cb,_4cc,_4cd,_4ce,_4cf,_4d0,_4d1,Data){
this.BucketIndex=CleanExternalString(_4c9);
this.Id=CleanExternalString(Id);
this.BucketType=CleanExternalString(_4cb);
this.Persistence=CleanExternalString(_4cc);
this.SizeMin=parseInt(CleanExternalString(_4cd));
this.SizeRequested=parseInt(CleanExternalString(_4ce));
this.Reducible=CleanExternalString(_4cf).toBoolean();
this.LocalActivityId=CleanExternalString(_4d0);
this.AllocationSuccess=CleanExternalString(_4d1);
this.Data=CleanExternalString(Data);
this.DataState=DATA_STATE_CLEAN;
}
SSPBucket.prototype.toString=function(){
return "BucketIndex="+this.Index+"Id="+this.Id+", BucketType="+this.BucketType+", Persistence="+this.Persistence+", SizeMin="+this.SizeMin+", SizeRequested="+this.SizeRequested+", Reducible="+this.Reducible+", LocalActivityId="+this.LocalActivityId+", AllocationSuccess="+this.AllocationSuccess+", Data="+this.Data;
};
SSPBucket.prototype.GetXml=SSPBucket_GetXml;
SSPBucket.prototype.IsVisible=SSPBucket_IsVisible;
SSPBucket.prototype.CurrentlyUsedStorage=SSPBucket_CurrentlyUsedStorage;
SSPBucket.prototype.GetBucketState=SSPBucket_GetBucketState;
SSPBucket.prototype.GetData=SSPBucket_GetData;
SSPBucket.prototype.WriteData=SSPBucket_WriteData;
SSPBucket.prototype.SetDirtyData=SSPBucket_SetDirtyData;
SSPBucket.prototype.AppendData=SSPBucket_AppendData;
SSPBucket.prototype.SizeAllocated=SSPBucket_SizeAllocated;
SSPBucket.prototype.ResetData=SSPBucket_ResetData;
function SSPBucket_GetXml(){
var _4d3=new ServerFormater();
var xml=new XmlElement("SSP");
xml.AddAttribute("IN",this.BucketIndex);
xml.AddAttribute("ID",_4d3.TrimToLength(this.Id,4000));
xml.AddAttribute("BT",_4d3.TrimToLength(this.BucketType,4000));
xml.AddAttribute("P",_4d3.ConvertSSPPersistence(this.Persistence));
xml.AddAttribute("SM",this.SizeMin);
xml.AddAttribute("SR",this.SizeRequested);
xml.AddAttribute("R",_4d3.ConvertBoolean(this.Reducible));
xml.AddAttribute("LAI",this.LocalActivityId);
xml.AddAttribute("AS",_4d3.ConvertSSPAllocationSuccess(this.AllocationSuccess));
xml.AddAttribute("D",this.Data);
return xml.toString();
}
function SSPBucket_IsVisible(_4d5){
if(this.LocalActivityId==""||this.LocalActivityId==null||this.LocalActivityId==_4d5){
return true;
}
return false;
}
function SSPBucket_CurrentlyUsedStorage(){
return this.Data.length*2;
}
function SSPBucket_GetBucketState(){
var _4d6="";
var _4d7=this.SizeAllocated();
_4d6="{totalSpace="+((_4d7!=null)?_4d7:0)+"}";
_4d6+="{used="+this.CurrentlyUsedStorage()+"}";
if(this.BucketType!=null&&this.BucketType.length>0){
_4d6+="{type="+this.BucketType+"}";
}
return _4d6;
}
function SSPBucket_GetData(_4d8,size){
if(_4d8==null||_4d8.length==0){
_4d8=0;
}else{
_4d8=parseInt(_4d8,10);
}
if(size==null||size.length==0){
size=0;
}else{
size=parseInt(size,10);
}
_4d8=_4d8/2;
size=size/2;
var str=new String();
if(size>0){
return this.Data.substr(_4d8,size);
}else{
return this.Data.substr(_4d8);
}
}
function SSPBucket_WriteData(_4db,_4dc){
if(_4db==null||_4db.length==0){
_4db=0;
}else{
_4db=parseInt(_4db,10);
}
_4db=_4db/2;
if(_4db==0){
this.Data=_4dc;
}else{
var _4dd=this.Data.slice(0,_4db);
var _4de="";
if(_4db+_4dc.length<this.Data.length){
_4de=this.Data.slice(_4db+_4dc.length);
}
this.Data=_4dd+_4dc+_4de;
}
this.SetDirtyData();
}
function SSPBucket_AppendData(_4df){
this.Data+=_4df;
this.SetDirtyData();
}
function SSPBucket_SetDirtyData(){
this.DataState=DATA_STATE_DIRTY;
}
function SSPBucket_SizeAllocated(){
switch(this.AllocationSuccess){
case SSP_ALLOCATION_SUCCESS_FAILURE:
return null;
break;
case SSP_ALLOCATION_SUCCESS_MINIMUM:
return this.SizeMin;
break;
case SSP_ALLOCATION_SUCCESS_REQUESTED:
return this.SizeRequested;
break;
case SSP_ALLOCATION_SUCCESS_NOT_ATTEMPTED:
return 0;
break;
}
Debug.AssertError("Invalid allocation success");
}
function SSPBucket_ResetData(){
this.Data="";
this.SetDirtyData();
}

dictionary_ll={"delimiter":"`","1498":"New Tracked Total Time: ","441":"[SB.2.9.2]2.3.2. Exit Choice Flow Tree Traversal Subprocess (Next Activity: the activity identified by the traversal)","27":"[TB.2.2]3.3.1. Exit Sequencing Post Condition Rules Subprocess (Termination Request: Exit All; Sequencing Request: Retry) (Terminate all active activities and move the current activity to the root of the activity tree; then perform an 'in-process' start)","1421":"Setting Current Activity to ","1106":"Terminate Descendent Attempts Process [UP.3](","775":"[RB.1.3 a]6.1. Set the Attempt Progress Status for the activity to True","1520":"Element is: lesson mode","396":"[TB.2.3]3.3.5.1.1. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: Exit; Exception: n/a","1638":"Root Activity is ","1615":"[SB.2.12]5.3. Else","1220":" is not a descendent and will be hidden.","1383":") that is disabled, disabling.","648":"[RB.1.2 c]7.3.1. Set the Objective Progress Status for the target objective to True","1093":"Resume All Sequencing Request Process [SB.2.6]","49":"[SB.2.1]4.3.1.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: the first activity in the activity's list of Available Children; Traversal Direction: Forward; Exception: n/a) (Start at the beginning of a forward only cluster)","1677":"Previous Time: ","1584":"WillNeverSucceed = ","375":"[SB.2.9]12.9.1.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-6) (Nothing to deliver)","1051":" (and all of its descendents) should be hidden. ","176":"[NB.2.1]1.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-1)","4":"[SB.2.9]12.5.5. If the target activity is Not an available descendent of the activity to consider And the target activity is Not the activity to considered And the target activity is Not the constrained activity Then (Make sure the target activity is within the set of 'flow' constrained choices)","1052":"[RB.1.1]4. Set the target objective to Undefined","897":" because it has a stop forward traversal precondition rule.","1708":"Min Score: ","796":"[TB.2.3]6.1. Set Activity is Active for the Current Activity to False","186":"[RB.1.4]1.2.3. Initialize status change to False (Determine if the appropriate children contributed to rollup; if they did, the status of the activity should be changed.)","856":"[SR.2]4. Case: the Randomization Timing for the activity is Once","1384":"[SB.2.9]11.10. Break All Cases","1168":"Element is: adl nav request valid continue","1698":"The activity ","1030":"[RB.1.3 a]5.1.1 completed = activity.getcompleted","416":"[UP.1]3.1.1. Exit Limit Conditions Check Process (Limit Condition Violated: True) (Limit conditions have been violated)","1461":"' corresponds to activity ","474":"[SB.2.1]3.2.1. If the activity is the last activity in the activity's parent's list of Available Children Then","1499":"Element is: score.scaled","519":"[UP.4]4.a. Performance Optimization - Deferring rollup to parent process to ensure minimal rollup set.","723":"[SB.2.9]2. If the target activity is not the root of the activity tree Then","1402":"Element is: interactions.type","753":"[SB.2.11]3. If the Current Activity is the root of the activity tree Then","475":"[RB.1.1 b]5.2.1.2. Set the Attempt Completion Amount to the total weighted measure divided by counted measures","1540":") (Nothing to deliver)","754":"[RB.1.4]1.2.2.2.2.5.1. Add a False value to the contributing children bag","1692":", strPostData=","148":"[SB.2.12]9.Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: SB.2.12-1) (Invalid sequencing request)","8":"[TB.2.3]3.3.5.1. If the Current Activity is the Root of the Activity Tree And the sequencing request returned by the Sequencing Post Condition Rule Subprocess is Not Retry Then If the attempt on the root of the Activity Tree is ending without a Retry, the Sequencing Session also ends","1071":"Setting sequencer pointer for cloned activities","1422":"[SB.2.9]9.4. Break All Cases","952":"[SB.2.12]8.1. Apply the Jump Sequencing Request Process","1299":"Adding new Interaction at position ","1300":"RunTimeApi_ValidPerformanceResponse","1585":"[RB.1.1]6.3.2. Else","1289":"Element is: comments_from_lms._count","953":"[EPNR] 2.1 Run the Termination Request Process for Exit","1315":"Element is: student_data._children","1639":"Element is: speed","1729":"', '","1238":"Element is: comments_from_lms.timestamp","1031":"[RB.1.2 c]6.1.1 satisfied = activity.getsatisfied","1616":"[NB.2.1]11.2. Else","470":"[TB.2.3]3.3.3. If the Sequencing Post Condition Rule Subprocess returned a termination request of Exit All Then","640":"Scaled Passing Score is not specified, using the success status recorded by the SCO-","70":"[RB.1.3 b]1.2. Apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Activity Progress Known; and a Rollup Action of Incomplete (Define the default not incomplete rule)","954":") is not a leaf Then - Can only deliver leaf activities","451":"[SB.2.9.2]2.1. If the activity is the root activity of the tree Then (Cannot walk off the root of the activity tree)","1053":"Objective Rollup Using Rules Process [RB.1.2 b](","955":"[SB.2.12]3.1. Apply the Exit Sequencing Request Process","1693":"Control Unload","834":"[SB.2.9]15.2. Apply the End Attempt Process to the common ancestor","705":"[UP.1]3. If the Limit Condition Attempt Control for the activity is True Then","1169":"[EPNR] A precondition rule indicates that ","963":"[SB.2.5]3.2. If the Flow Subprocess returns False Then","658":"[UP.2.1]2.2.1. Negate the rule condition (Negating 'unknown' results in 'unknown')","531":"[SB.2.2]2. Apply the Sequencing Rules Check Process to the activity and its Skipped sequencing rules","1316":"Element is: objectives.n.score.max","1403":"Element is: interactions.time","1254":"RunTimeApi_ValidMultipleChoiceResponse","898":"[SB.2.12]5.1. Apply the Continue Sequencing Request Process","1720":", isExit=","429":"[SR.2]4.1. If the Activity Progress Status for the activity is False Then (If the activity has not been attempted yet)","1255":"Rollup Rule Check Subprocess [RB.1.4](","367":"[RB.1.4.1]2.2. If the Rollup Condition Operator for the Rollup Condition is Not Then (Negating 'unknown' results in 'unknown')","755":"[SB.2.1]3.2. If the activity is a leaf Or consider children is False Then","1586":"[SB.2.2]3.3.2. Else","927":"[RB.1.4]1.2.4. Case: the Rollup Child Activity Set is All","887":"[RB.1.2 c]6.1.4 all statisfied = all satisfied AND satisfied","991":"[TB.2.3]5. Case: termination request is Suspend All","1032":"Nav request will NOT succeed. Leaving SCO loaded.","606":"[DB.2.1]1.4. Set Suspended Activity to Undefined (Clear the Suspended Activity attribute)","1555":"[RB.1.4.2]3.1.3. Else","1170":"Control Recieved Previous Request From GUI","1587":"Call is error free.","13":"[SB.2.9]11.8.1.3. If Activity is Active for the activity is False And(the activity is Not the common ancestor And adlseq:preventActivation for the activity is True) Then (If the activity being considered is not already active, make sure we are allowed to activate it)","554":"[SB.2.9]12.5.4. Set activity to consider to the activity identified by the Choice FlowSubprocess","19":"[SB.2.9]9.3.3. If Activity is Active for the activity is False And (the activity is Not the common ancestor And adlseq:preventActivation for the activity is True) Then (If the activity being considered is not already active, make sure we are allowed to activate it)","992":"UnLoading Sco and launching intermediate page from ","1462":"[EPNR] 4.1 Hiding request ","1640":"[NB.2.1]1.2. Else","776":"This activity utilized a launchable Asset, so automatically complete it","1641":"[RB.1.2 b]4. Else","977":"[SB.2.9]11.5. If constrained activity is Defined Then","1317":"[RB.1.4.2]1. Set included to False","149":"[UP.3]2. Form the activity path as the ordered series of activities from the Current Activity to the common ancestor, exclusive of the Current Activity and the common ancestor (","629":"[SB.2.9]6.1. Find the common ancestor of the Current Activity and the target activity","1072":"[EPNR] There is no logically previous activity.","1221":"Element is: objectives.completion_status","1423":"Control ToggleMenuVisibility","607":"[SB.2.4]2.2. Exit Choice Activity Traversal Subprocess (Reachable: True; Exception: n/a )","641":"[RB.1.2 b]4.4.2. Set the Objective Satisfied Status for the target objective to True","307":"[SB.2.9]16.1. Exit Choice Sequencing Request Process (Delivery Request: for the activity identified by the Flow Subprocess; Exception: n/a)","983":"[EPNR] Clearing out possible navigation request data","766":"[RB.1.4]1.2.2.2.2.4.1. Add a True value to the contributing children bag","1171":"[SR.2]5.2. Exit Randomize Children Process","1521":"Control DisplayError - ","430":"[EPNR] 5.1 Overriding continue status based on 3rd Edition GUI requirements, parent's flow = true, continue is enabled","669":", had reached its attempt limit and cannot be delivered. Disable previous button.","1256":"' does not represent a valid activity.","442":"[SB.2.9]1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-1) (Nothing to deliver)","555":"[DB.2]2.1. Apply the Clear Suspended Activity Subprocess to the activity identified for delivery","964":"ERROR - Invalid server response received from the LMS.","1054":"[RB.1.2 a]3. If target objective is Defined Then","875":"[SR.1]4.1.1.1. Initialize child list as an Empty ordered list","66":"[SB.2.2]3.3.1. If the previous traversal direction is Backward And the Traversal Direction returned by the Flow Tree Traversal Subprocess is Backward Then (Make sure the recursive call considers the correct direction)","1556":"[DB.2.1]1.3.1.2. Else","397":"[SB.2.9]12.4.2. If constrained activity is Undefined Then (Find the closest constrained activity to the current activity)","1301":"[SB.2.9]9.4.1. traverse is Backward","1440":"End Attempt Process [UP.4](","549":"[TB.2.1]3.1. Apply the Sequencing Rules Check Process to the activity and the set of Exit actions","876":"[SB.2.9]15.3. Set the Current Activity to the target activity","1094":" and all its descendents to skipped. IsActive=","1013":"Element is: interactions.correct responses.pattern","389":"[SB.2.1]4.2.1.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: Nil; Traversal Direction: n/a; Exception: SB.2.1-4)","984":"[SB.2.9]10.3. For each activity on the activity path","993":"[TB.2.3]7.3. For each activity in the activity path","1709":"LMSFinish('","630":"[RB.1.4.2]3.1.3.2.1.2. If the Sequencing Rules Check Process does not return Nil Then","810":"[DB.2]5.1.1.1.1. Set Activity is Suspended for the activity to False","1172":"Element is: comments, storing at position ","857":"[UP.4]1.1.1.2.1. For all objectives associated with the activity","1404":"Communications_SaveDataOnExit","1173":"[SR.2]2.1. Exit Randomize Children Process","615":"[UP.4]1.1.1.2.1.1. If the Objective Contributes to Rollup for the objective is True Then","354":"activity.IsDeliverable = false and activity.GetSequencingControlFlow = false.  Setting possibleNavRequest.WillNeverSucceed = true.","597":" (and all of its descendents) will be disabled because its attempt limit has been reached. ","384":"[SB.2.2]1.1. Exit Flow Activity Traversal Subprocess (Deliverable: False; Next Activity: the activity; Exception: SB.2.2-1)","767":"[RB.1.3]2.2. Set the Attempt Completion Status for the activity to False","985":"[SB.2.8]4. If the Flow Subprocess returns False Then","348":"[TB.2.3]5.6. Set the Current Activity to the root of the activity tree (Move the current activity to the root of the activity tree)","28":"[SB.2.12]5.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: Value from Continue Sequencing Request Process; Exception: the exception identified by the Continue Sequencing Request Process)","236":"[TB.2.3]4.1.If Activity is Active for the Current Activity is True Then (Has the completion subprocess and rollup been applied to the current activity yet?)","1033":"[SB.2.5]2.5.1. Get the ordered list of activities","1371":"Element is: interactions.n.type","1522":") for the activity Then","10":"[SB.2.9]8.1. Form the activity list as the ordered sequence of activities from the Current Activity to the target activity, exclusive of the target activity (We are attempted to walk toward the target activity. Once we reach the target activity, we don't need to test it.)","1055":"[SB.2.9]12.2. If the activity path is Empty Then","1385":"Adding new objective at index ","1257":"Transferring RTE data to Activity data","756":"[RB.1.2 c]6.3 not satisfied = (satisfied === false || attempted === true)","35":"[SB.2.2]3.3.1.1. Apply the Flow Activity Traversal Subprocess to the activity identified by the Flow Tree Traversal Subprocess in the traversal direction and a previous traversal direction of n/a (Recursive call \u2013 make sure the 'next' activity is OK)","527":"[DB.1.1]4. For each activity in the activity path - Make sure each activity along the path is allowed","1258":"**************************************","1095":"[RB.1.4.2]2.1.3.2.1.2.1. Set included to False","26":"[EPNR] Encountered a cluster that must be entered forward only. This traversal is beyond the capabilities of the 'quick' look ahead sequencer. If this navigation request results in an error message, then this course requires the full look ahead sequencer.","1617":"Element is: credit","1073":"[SB.2.9]9.2. If the activity path is Empty Then","1618":"[SB.2.12]8.3. Else","68":"[NB.2.1]13.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit; Sequencing Request: Jump; Target Activity: the activity specified by the Jump navigation request; Exception: n/a)","207":"[NB.2.1]1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Start; Target Activity: n/a; Exception: n/a)","349":"WARNING: The value 'logout' has been deprecated by ADL and should no longer be used. This value may lead to unpredictable behavior.","1500":"Element is: student name","569":"[SB.2.9]11.4.1.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: ","1107":"Clear Suspended Activity Subprocess [DB.2.1](","724":"[SB.2.9.1]2. If the Choice Flow Tree Traversal Subprocess returned Nil Then","246":"Either activity.HasSeqRulesRelevantToChoice = false or possibleNavRequest.WillNeverSucceed = true.  Setting possibleNavRequest.WillAlwaysSucceed = false.","51":"[SB.2.9]13. Apply the Flow Subprocess to the target activity in the Forward direction with consider children equal to True (The identified activity is a cluster. Enter the cluster and attempt to find a descendent leaf to deliver)","677":"[UP.1]2.1. Exit Limit Conditions Check Process (Limit Condition Violated: False)","143":"[UP.1]2. If the Activity is Active for the activity is True Or the Activity is Suspended for the activity is True Then (Only need to check activities that will begin a new attempt)","1386":"Mode is review so don't change","598":"[RB.1.2 a]3.1.2.1.2.2. Set the Objective Satisfied Status for the target objective to False","820":"[OP.1]1.5.2.2. Continue Loop - wait for the next navigation request","1387":"[RB.1.3 a]6. If All Incomplete","616":"[RB.1.2 b]2. Get the primary objective (For each objective associated with the activity)","136":"[SB.2.7]3. Apply the Flow Subprocess to the Current Activity in the Forward direction with consider children equal to False (Flow in a forward direction to the next allowed activity)","1074":"[TB.2.3]5.4. If the activity path is Empty Then","443":"[SB.2.4]1.1. Apply the Sequencing Rules Check Process to the activity and the Stop Forward Traversal sequencing rules","1122":"[RB.1.4]1.2.8.1.1. Set status change to True","689":"[RB.1.4]1.2.5.1. If the contributing children bag contains a value of True Then","223":"7.1.1.3.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception ","369":"[SB.2.9]7.1. Set common ancestor is the root of the activity tree (No, choosing the target will start the sequencing session)","1318":"Element is: objectives.n.score.min","562":"[SB.2.8]2.1.1. Exit Flow Tree Traversal Subprocess (Delivery Request: n/a; Exception: SB.2.8-2)","1424":" and all of its descendents.","532":"[SR.2]1. If the activity does not have children Then (Cannot apply randomization to a leaf activity)","1541":"RetrieveGetValueData (","520":"[UP.3]3. If the activity path is Not Empty Then (There are some activities that need to be terminated)","1441":"Element is: adl nav request","462":"[SB.2.1]4.1.If the activity is the root activity of the tree Then (Cannot walk off the root of the activity tree)","1108":"[RB.1.4]1.2.10. If status change is True Then","864":"[RB.1.2]3.1.1 Invoke the Objective Rollup Process Using Measure","479":"[RB.1.2 a]4.1. Exit Objective Roll up Process (No objective contributes to rollup, so we cannot set anything)","284":"[UP.2.1]2.3. Add the value of rule condition to the rule condition bag (Add the evaluation of this condition to the set of evaluated conditions)","1501":"[TB.2.1]3.2.2. Break For","385":"[SB.2.2]5.1. Exit Flow Activity Traversal Subprocess (Deliverable: False; Next Activity: the activity; Exception: SB.2.2-2)","1275":"Element is: objectives.success_status","797":"[RB.1.3]2.1. Set the Attempt Progress Status for the activity to True","124":"[SB.2.12]3.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: n/a; End Sequencing Session: the result of the Exit Sequencing Request Process; Exception: n/a)","1123":"[RB.1.4]1.2.6.1.1. Set status change to True","1259":"Element is: adl.nav.request_valid.jump","533":"Bypassing Lookahead Sequencer processing because PackageProperties.LookaheadSequencerMode = disabled","1481":"Element is: adl.data.type","757":"[RB.1.3 b]3.1. Set the Attempt Progress Status for the activity to True  ","256":"[RB.1.5]1. Form the activity path as the ordered series of activities from the root of the activity tree to the activity, inclusive, in reverse order.","590":"[RB.1.2]3.2 If the Activity has rollup rules that have the action Satisfied or Not Satisfied","1056":"[RB.1.3]3. Exit Activity Progress Rollup Process","1174":"[SB.2.1]1. Set reversed direction to False","1239":"[SR.1]1.1. Exit Select Children Process","1463":"RunTimeApi_ValidIdentifier","678":"[EPNR] 3. For each possible navigation request that hasn't already been excluded","1341":"Suspended Activity is not defined","865":"[RB.1.5] Adding activity to collection of rolled up activities.","1523":"[RB.1.4.2]3.1.3.2. Else","463":"[NB.2.1]1.1. If the Current Activity is Not Defined Then (Make sure the sequencing session has not already begun)","404":"[SB.2.1]4.3.2.1. Exit Flow Tree Traversal Subprocess (Next Activity: Nil; Traversal Direction: n/a; Exception: SB.2.1-2)","1619":"[SB.2.12]4.3. Else","697":"[UP.2.1]2.2. If the Rule Condition Operator for the Rule Condition is Not Then","940":") and we're starting a new attempt on the root activity.","1222":"No API Runtime Nav Request, exit action=","1502":"[DB.2]5.1. If Activity (","36":"[SB.2.12]5.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Continue Sequencing Request Process; End Sequencing Session: as identified by the Continue Sequencing Request Process;; Exception: n/a)","798":"[SB.2.1]4.2.3.2. Exit Flow Tree Traversal Subprocess (Next Activity: ","208":"[DB.1.1]2. Form the activity path as the ordered series of activities from the root of the activity tree to the activity specified in the delivery request, inclusive","1524":"RB.1.1]5.1.2. Break For","608":"[OP.1]1.4.2. If the Sequencing Request Process returned sequencing request Not Valid Then","89":"[UP.1]3.1. If the Activity Progress Status for the activity is True And the Activity Attempt Count for the activity is greater than or equal (>=) to the Limit Condition Attempt Limit for the activity Then","166":"[NB.2.1]12.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Suspend All; Sequencing Request: Exit; Target Activity: n/a; Exception: n/a)","251":"[SB.2.9]4.1. Apply the Sequencing Rules Check Process to the activity and the Hide from Choice sequencing rules (Cannot choose something that is hidden)","1354":"Exiting Control Deliver Activity","217":"[SB.2.9]2.1. If the Available Children for the parent of the target activity does not contain the target activity Then (The activity is currently not available)","623":"Completion Threshold is not specified, using the completion status recorded by the SCO-","417":"[SB.2.9]4.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-3) (Nothing to deliver)","1276":"[EPNR] Cannot flow backwards through ","758":" because it has a stop forward traversal precondition rule and is a leaf.","870":"[RB.1.3 a]5.1.5. Child should be included in incomplete rollup","670":"[TB.2.3]5.2.1.1. Set the Suspended Activity to the parent of the Current Activity","858":"[EPNR] 2.2.1 Run the Termination Request Process For Suspend All","1665":"SuccessStatus = ","1588":"[SB.2.2]6.3.2. Else","350":"[TB.2.3]4.4. Set the Current Activity to the root of the activity tree (Move the current activity to the root of the activity tree)","405":"[SB.2.9]11.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)","200":"[DB.2]1.If the Activity is Active for the Current Activity is True Then (If the attempt on the current activity has not been terminated, we cannot deliver new content)","117":"[RB.1.4]1.2.8.1. If the percentage (normalized between 0..1, inclusive) of True values contained in the contributing children bag equals or exceeds the Rollup Minimum Percent of the rule Then","1277":"[EPNR] 3.2.1 Set WillSucceed to false","1620":"No interaction at ","799":"[NB.2.1]5. Case: navigation request is Forward (Behavior not defined)","706":"[OP.1]1.3.1. Apply the Termination Request Process to the termination request","679":"[SB.2.12]7.2. If the Choice Sequencing Request Process returns an exception Then","376":"[SB.2.9]11.3.1.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-7) (Nothing to deliver)","649":"[RB.1.2 b]4.4.1. Set the Objective Progress Status for the target objective to True","784":"[RB.1.4]1.2.9. Case: the Rollup Child Activity Set is At Least Percent","237":"[RB.1.4]1.2.2.2.1. Apply Check Child for Rollup Subprocess to the child and the Rollup Action (Make sure this child contributes to the status of its parent)","1014":") that has a limit condition violation, disabling.","1151":"[SB.2.1]2.3. Set reversed direction to True","264":"[SB.2.1]3.1. If the activity is the last activity in a forward preorder tree traversal of the activity tree Then (Cannot walk off the activity tree)","308":"[UP.5]1.Apply the Sequencing Rules Check Process to the activity and the Disabled sequencing rules (Make sure the activity is not disabled)","994":"[TB.2.3]7. Case: termination request is Abandon All","1152":"Performing sequencing look ahead evaluation","1589":"CompletionStatus = ","309":"[UP.4]1.1.1.  If the Activity is Suspended for the activity is False Then (The sequencer will not affect the state of suspended activities)","690":"[RB.1.4.1]3.1. Exit Evaluate Rollup Conditions Subprocess (Evaluation: Unknown)","581":"[SR.2]5.1.1. Randomly reorder the activities contained in Available Children for the activity","1096":"[SB.2.4]2.1. If the activity has a parent Then","1097":"Choice Activity Traversal Subprocess [SB.2.4](","821":"[SB.2.10]3.a.1 If objectives global to system is false (obj global=","1124":"[RB.1.4]1.2. For each rule in the rules list","167":"[SB.2.1]3.1.2. Exit Flow Tree Traversal Subprocess (Next Activity: n/a; End Sequencing Session: True; Exception: n/a) - Continuing past the last activity, exiting the course","1621":"[SB.2.9]11.9. Else","340":"Suspending Activity (no nav request because the package property LogoutCausesPlayerExit precludes logout from causing a suspend all)","768":"[SR.1]4.1.1. If the Selection Count Status for the activity is True Then","1015":"OverallSequencingProcess for SCORM 1.1 / SCORM 1.2","1678":"[SB.2.9]6. Else","1153":"Element is: comments_from_learner.timestamp","570":"[SB.2.9]6. If the Current Activity is Defined Then (Has the sequencing session already begun?)","928":") and we're starting a new attempt on the root activity (","161":"[NB.2.1]2.1.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-3)","1666":"[SB.2.9]16. Else","1464":"Element is: adl.data.store","521":"[SB.2.9]12.5.3. Apply the Choice Flow Subprocess to the constrained activity in the traverse direction","1057":"[RB.1.2 b]4. If target objective is Defined Then","537":"[SCORM Engine Extension] Child Set is Empty and Control.Package.Properties.RollupEmptySetToUnknown=","1203":"[UP.4]2. Else (The activity has children)","1442":"Control MarkDirtyDataPosted","484":"[SB.2.10]4.1. Exit Retry Sequencing Request Process (Delivery Request: the Current Activity; Exception: n/a)","1109":"Element is: learner_preference.delivery_speed","759":"[RB.1.3 a]7.2. Set the Attempt Completion Status for the activity to True","842":" does not allow previous navigation. Previous button is disabled.","707":"[SB.2.9]9.3.2. If the Choice Activity Traversal Subprocess returns False Then","1319":"New Tracked Total Time for Asset: ","995":"[NB.2.1]12. Case: navigation request is Suspend All","1260":"[EPNR] The logically next activity is ","1525":"[NB.2.1]7.1.1.2.4. Else","326":"[RB.1.2 b]4.3. Apply the Rollup Rule Check Subprocess to the activity and the Satisfied rollup action Process all Satisfied rules last","212":"[SB.2.9]9. Case: Current Activity and common ancestor are the same Or Current Activity is Not Defined (Case #3 - path to the target is forward in the activity tree)","1355":"Element is: adl nav request jump","941":"[SB.2.9.2]2. If the traversal direction is Backward Then","1590":", CompletionStatus=","1388":" do not have a common ancestor","1503":"Control GetExceptionText","835":"[SB.2.9]14.2. Apply the End Attempt Process to the common ancestor","1465":"Control TriggerReturnToLMS","1642":"[RB.1.2]3.2 Else ","452":"[RB.1.1]6.3.2.1 Set the Objective Measure Status for the target objective to False (No children contributed weight.)","609":"[UP.5]4.1. Exit Check Activity Process (Result: True) (Limit Condition Has Been Violated)","744":"[RB.1.4]1.2.2.2.2. If Check Child for Rollup Subprocess returned True Then","1679":"[SB.2.7]5. Else","1034":"[SB.2.9]4. For each activity in the activity path","1098":"[OP.1]1.5. If there is a delivery request Then","1075":"Element is: learner_preference.audio_captioning","1716":"' From GUI","327":"[SB.2.1]3.3.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: the first activity in the activity's list of Available Children (","7":"[SB.2.9]11.5.5. If the target activity is Not an available descendent of the activity to consider And the target activity is Not the activity to consider And the target activity is Not the constrained activity Then (Make sure the target activity is within the set of 'flow' constrained choices)","877":"[RB.1.1]6.1.1.2. For each objective associated with the child","1356":"Lookahead Sequencer Mode Enabled","316":"[SB.2.1]4.2.1.1. If Sequencing Control Forward Only for the parent of the activity is True Then (Test the control mode before traversing)","1223":"Element is: adl.nav.request_valid.choice","213":"[NB.2.1]3.2.1. If the Activity is Active for the Current Activity is True Then (If the current activity has not been terminated, terminate the current the activity)","111":"[SB.2.12]6.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Previous Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)","1482":") (Violates control mode)","341":"[SB.2.9]11.1. Form the activity path as the ordered series of activities from the Current Activity to the target activity, inclusive","1058":"[SB.2.9]10.2. If the activity path is Empty Then","843":"[TB.2.3]5.1.2. Set the Suspended Activity to the Current Activity","1542":"Element is: learner_id","1240":"[SR.1]4.2. Exit Select Children Process","1702":"Initialize('","1261":"[RB.1.5]4. Exit Overall Rollup Process","1526":"CheckForSetValueError (","1204":"Element is: objectives.n.progress_measure","370":"[SB.2.10]3.1. Apply the Flow Subprocess to the Current Activity in the Forward direction with consider children equal to True","822":"[UP.4]2.1.1. Set the Activity is Suspended for the activity to True","242":"[SB.2.9]11.6. Form the activity path as the ordered series of activities from the common ancestor to the target activity, exclusive of the target activity","100":"[UP.2.1]2.1. Evaluate the rule condition by applying the appropriate tracking information for the activity to the Rule Condition (Evaluate each condition against the activity's tracking information)","1262":"Not tracked...not transfering RTE data","1466":"RunTimeApi_ValidCharString","942":"[SB.2.10]3.2. If the Flow Subprocess returned False Then","1643":"Element is: entry","1527":"Completion Threshold = ","259":"[RB.1.1 b]4.1.1. Increment counted measures by the adlcp:progressWeight for the child (The child is included, account for it in the weighted average)","610":" does not allow choice exit requests. Hiding all activities that are not its descendents.","156":"[RB.1.4]1.2.2.2.2.1. Apply the Evaluate Rollup Conditions Subprocess to the child and the Rollup Conditions for the rule (Evaluate the rollup conditions on the child activity)","785":" since it only allows forward navigation. Previous button is disabled.","324":"[UP.2.1]1. Initialize rule condition bag as an Empty collection (This is used to keep track of the evaluation of the rule's conditions)","34":"[NB.2.1]3.3.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-4) (Flow is not enabled or the current activity is the root of the activity tree)","996":"Completion Threshold is known (Completion Treshold=","1263":"[RB.1.3 a]1. If the activity is a leaf","1591":"[NB.2.1]3.2.2. Else","1699":"LMSSetValue('","691":"[UP.1]3.1. If the Activity Progress Status for the activity is True (currently ","1644":"[RB.1.1]6.3. Else","698":"Scaled Score exceeds Scaled Passing Score so setting success status to passed.","144":"[SB.2.9.2]1.2.2. Exit Choice Flow Tree Traversal Subprocess (Next Activity: the results of the recursive Choice Flow Tree Traversal Subprocess) (Return the result of the recursion)","1557":"ScaledPassingScore = ","276":"[SB.2.2]6. If the activity is not a leaf node in the activity tree Then (Cannot deliver a non-leaf activity; enter the cluster looking for a leaf)","1125":"[TB.2.3]3. Case: termination request is Exit","1389":"Exiting Control Update Display","247":"[UP.4]4. Apply the Overall Rollup Process to the activity (Ensure that any status change to this activity is propagated through the entire activity tree)","943":"[RB.1.1]5.1.1. Set the target objective to the objective","760":"[EPNR] 1.1. If the navigation request fails, set its WillSucceed to false","121":"[SB.2.12]1.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Start Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)","1425":"RunTimeApi_ValidTimeInterval","386":"[RB.1.2]3.3 Invoke the Objective Rollup Process Using Default - Neither Sub Process is Applicable, so use the Default Rules","157":"[SB.2.2]6.2.1. Exit Flow Activity Traversal Subprocess (Deliverable: False; Next Activity: the activity; Exception: exception identified by the Flow Tree Traversal Subprocess)","371":"[SB.2.9]6.1. Set common ancestor is the root of the activity tree (No, choosing the target will start the sequencing session)","1645":"[NB.2.1]8.2. Else","14":"[SB.2.9]12.8.1.3. If Activity is Active for the activity is False And(the activity is Not the common ancestor And adlseq:preventActivation for the activity is True) Then (If the activity being considered is not already active, make sure we are allowed to activate it)","94":"[NB.2.1]14. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-13) (Undefined navigation request)","1126":"[SB.2.1]2.1. traversal direction is Backward","725":"[TB.2.3]4.3. Apply the End Attempt Process to the root of the activity tree","406":"[SB.2.9]11.7.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)","1016":"[RB.1.2 a]1. Set the target objective to Undefined","726":"[RB.1.3 b]5.2. Set the Attempt Completion Status for the activity to True  ","460":"[UP.4]3. Set the Activity is Active for the activity to False (The current attempt on this the activity has ended)","692":"[SB.2.12]1.2. If the Start Sequencing Request Process returns an exception Then","269":"[RB.1.4]1.2.8.1. If the count of True values contained in the contributing children bag equals or exceeds the Rollup Minimum Count of the rule Then","65":"[SB.2.5]3.2.1. Exit Start Sequencing Request Process (Delivery Request: n/a; End Sequencing Session: as identified by the Flow Subprocess; Exception: the exception identified by the Flow Subprocess) (Nothing to deliver)","209":"[SB.2.9]10. Case: Current Activity and common ancestor are the same Or Current Activity is Not Defined (Case #3 - path to the target is forward in the activity tree)","986":"[SB.2.7]4. If the Flow Subprocess returns False Then","1543":"Allowing status change","202":"[SB.2.9]11.4.1. If the Sequencing Control Choice Exit for the activity is False Then - Make sure an activity that should not exit will exit if the target is delivered","1302":"[EPNR] The logically next activity,","1175":"[SB.2.10]3.a.2 Reset any global objectives","888":"[TB.2.2]1.1. Exit Sequencing Post Condition Rules Subprocess","650":"Scaled Score is less than Scaled Passing Score so setting success status to failed.","563":"[RB.1.5]3.1.1. Apply the Measure Rollup Process to the activity (Rollup the activity's measure)","1405":" is the last overall activity","671":"[RB.1.2 c]7.1. Set the Objective Progress Status for the target objective to True","1127":"Done Evaluating Possible Navigation Requests","699":"[SB.2.9]10.3.2. If the Choice Activity Traversal Subprocess returns False Then","1264":"Element is: objectives.score._children","1320":"The runtime navigation request of ","1390":"Element is: adl.data._children","1241":"Element is: learner_preference.language","1592":"[SB.2.1]3.2.2. Else","1242":"Element is: objectives.progress_measure","418":"[SB.2.9]9.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)","878":"[SB.2.12]2.1. Apply the Resume All Sequencing Request Process","1391":"RunTimeApi_ValidFillInResponse","419":"[SB.2.9]4.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-4) (Nothing to deliver)","582":"[OP.1]1.4.4. If the Sequencing Request Process did not identify an activity for delivery Then","431":"[RB.1.2 b]5.1. Exit Objective Rollup Using Rules Process No objective contributes to rollup, so we cannot set anything","906":"[EPNR] 2.2 if the termination request process return false","280":"[TB.2.3]7.2.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-6) Nothing to abandon","727":"[SB.2.4]1.2. If the Sequencing Rules Check Process does not return Nil Then","708":"[SB.2.8]2. If the activity is not the root activity of the activity tree Then","1571":"RunTimeApi_ValidTime","700":"[DB.2]5.1.1.2.5. If shared data global to system is false (shared data global=","1290":"Element is: objectives.n.description","907":"[SB.2.9]12. If the target activity is a leaf activity Then","823":"[DB.2]8. Exit Content Delivery Environment Process (Exception: n/a)","1572":"Element is: _version","538":"[RB.1.4]1.2.5.1. If the contributing children bag does not contain a value of False Or Unknown Then","177":"[NB.2.1]2.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-1)","466":"[SB.2.9]12.5.1. If the target activity is Forward in the activity tree relative to the constrained activity Then","1154":"Element is: comments_from_learner.n.comment","1372":"RunTimeApi_ValidNumericResponse","786":"[RB.1.4]1.2.8. Case: the Rollup Child Activity Set is At Least Percent","485":"[SB.2.9]13.1. Exit Choice Sequencing Request Process (Delivery Request: the target activity; Exception: n/a)","218":"[UP.4]4.c. Performance Optimization - Deferring rollup or activities affected by write maps to parent process to ensure minimal rollup set. (not in pseudo code)","360":"[EPNR] 4. Set any requests that are invalid due to Control Choice, Prevent Activation or Constrained Choice violations to hidden","680":"[SB.2.9.2]2.1.1. Exit Choice Flow Tree Traversal Subprocess (Next Activity: Nil)","319":"[SB.2.10]3.3.1. Exit Retry Sequencing Request Process (Delivery Request: the activity identified by the Flow Subprocess; Exception: n/a)","997":"[RB.1.1 b]3. Set the counted measures to Zero (0.0)","1558":"[RB.1.4.2]2.1.3. Else","978":"[RB.1.1]6.1.1.1. Set rolled-up objective to Undefined","313":"[RB.1.3 b]2. Apply the Rollup Rule Check Subprocess to the activity and the Incomplete rollup action (Process all Incomplete rules first.)","1128":"[RB.1.4.2]3.1.3.1.1.1. Set included to False","215":"[EPNR] 2. If the current activity is active (we are going to need to terminate it for internally navigating sequencing requests (continue, previous, choice, exit)","168":"[NB.2.1]2.1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Resume All; Target Activity: n/a; Exception: n/a) ","1224":"[RB.1.2 c]0.5. If the activity is a leaf","617":"[RB.1.2 a]2. Get the primary objective (For each objective associated with the activity)","325":"[SB.2.5]3.3.1. Exit Start Sequencing Request Process (Delivery Request: the activity identified by the Flow Subprocess; Exception: n/a)","777":"[RB.1.2 c]6.1.5 all not satisfied = all not satisfied AND not satisfied","571":"[SB.2.9]10.3.1.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: ","899":", is disabled and cannot be delivered. Disable next button.","787":"[SB.2.9]3.1.\tIf the activity is Not the root of the activity tree Then","998":"[SB.2.9]8.5. For each activity on the activity list","153":"[SR.2]2. If Activity is Suspended for the activity is True Or the Activity is Activefor the activity is True Then (Cannot apply randomization to a suspended or active activity)","1017":"Initializing Possible Navigation Request Absolutes","1129":"Previous Sequencing Request Process [SB.2.8]","1035":"[OP.1]x. No API Runtime Nav Request, exit action=","1130":"Adding new Comment From Learner at position ","342":"[DB.2.1]1.2. Form an activity path as the ordered series of activities from the Suspended Activity to the common ancestor, inclusive","1646":"[NB.2.1]9.2. Else","556":"[RB.1.1]6.1.1. If Tracked for the child is True Then (Only include tracked children). Tracked = ","761":"[RB.1.3 b]5.1. Set the Attempt Progress Status for the activity to True  ","583":"[SB.2.7]2.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: Nil; Exception: SB.2.7-2 )","557":"[SB.2.3]4.1. Candidate activity is the activity identified by the Flow Tree Traversal Subprocess","824":"[OP.1]1.4.2.2. Continue Loop - wait for the next navigation request","522":" (and all of its descendents) will be hidden because its Prevent Activation attribute is set to true. ","599":"[RB.1.4.2]4. Exit Check Child for Rollup Subprocess (Child is Included in Rollup: included)","836":"[SR.1]6. Exit Select Children Process (Undefined timing attribute)","929":"[RB.1.1 b]1. Set the total weighted measure to Zero (0.0)","1036":"[EPNR] 3.1.2 Make sure the activity is not hidden","584":"[DB.1.1]3.1. Exit Delivery Request Process (Delivery Request: Not Valid; Exception: DB.1.1-2)","203":"[NB.2.1]7.1.1.4. If the Activity is Active for the Current Activity is True Then (If the current activity has not been terminated, terminate the current the activity)","1291":"[UP.4]5. Exit End Attempt Subprocess","1504":"Sco was taken for credit","1342":"Adding new Objective at position ","181":"[SB.2.2]3.1. Apply the Flow Tree Traversal Subprocess to the activity in the traversal direction and the previous traversal direction with consider children equal to False","1018":"Objective Rollup Using Default Process [RB.1.2 c](","591":"[RB.1.4.2]3.1.1. Set included to True (Default Behavior \u2013 adlseq:requiredFor[xxx] == always)","1505":"Element is: session time","1593":"Progress Measure = ","126":"[RB.1.5]3.3. Apply the Activity Progress Rollup Process to the activity (Apply the appropriate behavior described in section RB.1.3, based on the activity's defined sequencing information)","717":"[UP.5]2.1. Exit Check Activity Process (Result: True) (Activity is Disabled)","444":"[RB.1.3 a]4.2.1. If the Attempt Completion Amount is greater than or equal (>=) to the adlcp:minProgressMeasure  Then","1544":"[RB.1.3 a]4.2.2. Else ","11":"[SB.2.9]9.1. Form the activity list as the ordered sequence of activities from the Current Activity to the target activity, exclusive of the target activity (We are attempted to walk toward the target activity. Once we reach the target activity, we don't need to test it.)","1647":"[SB.2.2]6.3. Else","127":"[SB.2.9.1]1. Apply the Choice Flow Tree Traversal Subprocess to the activity in the traversal direction (Attempt to move away from the activity, 'one' activity in the specified direction)","1303":"Element is: objectives.score.scaled","1392":"[SB.2.9]12.10. Break All Cases","1559":"[RB.1.2 a]3.1.2. Else","21":"[NB.2.1]13.1. If the activity specified by the Jump navigation request exists within the activity tree And Available Children for the parent of the activity contains the activity Then (Make sure the target activity exists in the activity tree and is available)","572":"[SB.2.9]11.4. For each activity on the activity path (Walk up the tree to the common ancestor)","1037":"[NB.2.1]2. Case: navigation request is Resume All","1648":"[NB.2.1]7.2. Else","672":"[RB.1.1]6.3.1.1 Set the Objective Measure Status for the target objective to True","445":"[TB.2.3]4.5.1 Sequencing Request: is the sequencing request returned by the Sequencing Post Condition Rule Subprocess","494":"[SB.2.6]1. If the Current Activity is Defined Then (Make sure the sequencing session has not already begun)","1059":"[RB.1.2 b]3. If target objective is Defined Then","281":"[TB.2.3]7.1. Form the activity path as the ordered series of all activities from the Current Activity to the root of the activity tree, inclusive","762":"Status has been set, checking to override to passed/failed based on score","573":"[SB.2.9]11.9.1.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: ","908":"[RB.1.2]2. For each objective associated with the activity","195":"[SB.2.9]10.3.1.1. If the Sequencing Control Choice Exit for the activity is False Then (Make sure an activity that should not exit will exit if the target is delivered)","372":"[UP.2.1]3. If the rule condition bag is Empty Then (If there are no defined conditions for the rule, the rule does not apply)","1321":"Element is: interactions._children","1622":"[SB.2.9]12.9. Else","1304":"cmi.comments_from_learner.n.comment","550":"[RB.1.4]1.2.4.1. Break  (No action; do not change status unless some child contributed to rollup)","956":"[SB.2.9.2]1. If the traversal direction is Forward Then","1649":"[NB.2.1]2.2. Else","1176":"Element is: adl.nav.request_valid.continue","1623":"[SB.2.5]3.3. Else ","1560":"[NB.2.1]7.1.1.5. Else","44":"[TB.2.3]5.1. If (the Activity is Active for the Current Activity is True) Or (the Activity is Suspended for the Current Activity is True) Then (If the current activity is active or already suspended, suspend it and all of its descendents)","351":"[RB.1.3]3. Apply the Rollup Rule Check Subprocess to the activity and the Completed rollup action Process all Completed rules last.","930":"[RB.1.4]1.2.5. Case: the Rollup Child Activity Set is All","114":"[SB.2.12]4.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Retry Sequencing Request Process; End Sequencing Session: n/a) ; Exception: n/a)","1278":" is a descendent and will be hidden. ","788":"[NB.2.1]6. Case: navigation request is Backward (Behavior not defined)","574":"[OP.1]1.4.5. Delivery request is for the activity identified by the Sequencing Request Process","778":"[SR.2]4.05. If the activity's children have not already been randomized","150":"[SB.2.12]8.Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: SB.2.12-1) (Invalid sequencing request)","50":"[NB.2.1]4.3.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-6) (Cannot move backward from the root of the activity tree)","243":"[UP.4]1.1.1.1.1. If the Attempt Progress Status for the activity is False Then (Did the content inform the sequencer of the activity's completion status?)","1177":"[EPNR] The logically previous activity is ","543":"[DB.2]5.1.1.2.4.1. Reset any global objectives and initialize the activity tree for a new attempt.","67":"[DB.2]4.Form the activity path as the ordered series of activities from the root of the activity tree to the activity identified for delivery, inclusive (Begin all attempts required to deliver the identified activity)","1243":"[EPNR] Disable the forward siblings of ","965":"[SB.2.9]11.9.1. For each activity on the activity path","631":"[SB.2.9]5.1. Find the common ancestor of the Current Activity and the target activity","811":"[SR.1]4.05. If children have not been selected for this activity yet","1443":"Control MarkPostedDataDirty","728":"[RB.1.5]3.2. Apply the appropriate Objective Rollup Process to the activity","651":"CompletedByMeasure is not enabled, using the completion status recorded by the SCO-","486":"[SB.2.11]1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)","88":"[TB.2.3]2. If (the termination request is Exit Or Abandon) And Activity is Active for the Current Activity is False Then (If the current activity has already been terminated, there is nothing to terminate)","169":"[NB.2.1]12.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-2)","1279":"Element is: comments_from_lms.comment","1322":"[RB.1.1]2. Set valid date to False","1426":"Element is: progress_measure","957":"[TB.2.1]5. Exit Sequencing Exit Action Rules Subprocess","248":"[TB.2.1]1. Form the activity path as the ordered series of activities from the root of the activity tree to the parent of the Current Activity, inclusive","1545":"Element is: total_time","285":"[[NB.2.1]8.1.1. If the Activity is Active for the Current Activity is True Then (Make sure the current activity has not already been terminated)","1343":"[RB.1.2 c]6.1 if child activity (","286":"[SB.2.9]10. Case: Target activity is the common ancestor of the Current Activity (Case #4 - path to the target is backward in the activity tree)","1076":"[RB.1.2 c]6.2 attempted = activity.getattempted","105":"[RB.1.2 a]3.1.2.1.1. If the Objective Normalized Measure for the target objective is greater than or equal (>=) to the Objective Minimum Satisfied Normalized Measure for the target objective Then","1205":"RunTimeApi_IsValidArrayOfShortIdentifiers","292":"[UP.1]1.1. Exit Limit Conditions Check Process (Limit Condition Violated: False) (Activity is not tracked, no limit conditions can be violated)","1717":"SetValue('","1624":"[NB.2.1]13.2. Else","1305":"Evaluate Sequencing Rule Condition(","112":"[SB.2.12]5.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Continue Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)","1077":"[RB.1.2 c]7.3. If All Satisfied. (allSatisfied=","889":"[RB.1.2 a]2. For each objective associated with the activity","487":"[DB.2]5.1.1.2.1. Increment the Activity Attempt Count for the activity (Begin a new attempt on the activity)","287":"[SB.2.9]11.Case: Target activity is forward from the common ancestor activity (Case #5 - target is a descendent activity of the common ancestor)","1178":"Score less than mastery, setting to failed","592":"[UP.4]1.1.1.2.1. Get the primary objective (For all objectives associated with the activity)","859":"[RB.1.3 a]6. Exit Activity Progress Rollup Using Measure Process","182":"[NB.2.1]10.1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Abandon; Sequencing Request: Exit; Target Activity: n/a; Exception: n/a)","1265":"Control Recieved Exit Request From GUI","1703":". Exception=","1155":"[EPNR] There is no logically next activity.","745":"[RB.1.4]1.2.1. Initialize contributing children bag as an empty collection","1292":"Setting completion status to browsed","544":"controlChoiceExitIsUsed = true.  Setting WillAlwaysSucceed = false for all possible nav. requests.","769":"[SR.1]4.1.1.3. Set Available Children for the activity to the child list","1506":"Rolling up activity data","1467":"Element is: delivery speed","1625":"[SB.2.12]2.3. Else","162":"[TB.2.3]7.5.Exit Termination Request Process (Termination Request: Valid; Sequencing Request: Exit; Exception: n/a) Inform the sequencer that the sequencing session has ended","1704":") is tracked","789":" since it does not allow flow navigation. Previous button is disabled.","1406":"Suspended Activity is defined","1573":"Element is: comments","118":"[RB.1.4.2]3.1.3.1. If (the Rollup Action is Completed And adlseq:requiredForCompleted is ifAttempted) Or (the Rollup Action is Incomplete And adlseq:requiredForIncomplete is ifAttempted) Then","860":"[UP.5]4. If the Limit Conditions Check Process returns True Then","746":"[RB.1.3 a]6.2. Set the Attempt Completion Status for the activity to False","158":"[SB.2.2]3.2.1. Exit Flow Activity Traversal Subprocess (Deliverable: False; Next Activity: the activity; Exception: exception identified by the Flow Tree Traversal Subprocess)","204":"[NB.2.1]4.2.1.1. If the Activity is Active for the Current Activity is True Then (If the current activity has not been terminated, terminate the current the activity)","20":"[SB.2.1]3.1. If the activity is the last available activity in a forward preorder tree traversal of the activity tree Or (the activity is the Root of the Activity Tree And consider children is False) Then (Walking off the tree causes the sequencing session to end)","1561":"Element is: score.max","693":"[RB.1.2]3.1. If Objective Satisfied By Measure for the target objective is True","999":"Element is: interactions.n.correct_responses._count","659":"[SB.2.9]12.4.1. If the activity is not the last activity in the activity path Then","866":"[TB.2.3]7.3.1. Set Activity is Active for the activity to False","931":"[RB.1.4]1.2.5. Case: the Rollup Child Activity Set is Any","626":"[RB.1.1]5. Get the primary objective (For each objective associated with the activity)","1626":"[SB.2.12]3.3. Else","600":"[RB.1.1]6.1.1.3.2. If the Objective Measure Status for the rolled-up objective is True Then","1650":"[EPNR] Selecting ","1179":"Element is: comments from learner.location","673":"[TB.2.3]3.3.3.2. Break to the next Case (Process an Exit All Termination Request)","480":"[SB.2.13]1. If the Current Activity is not Defined Then (Make sure the sequencing session has already begun.)","55":"[NB.2.1]7.1.1.3.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit; Sequencing Request: Choice; Target Activity: the activity specified by the Choice navigation request; Exception: n/a) ","1266":"Element is: comments_from_lms.location","1444":"Element is: interactions.id","747":"[UP.5]5. Exit Check Activity Process (Result: False) (Activity is allowed)","301":"[UP.2]1. If the activity includes Sequencing Rules with any of the specified Rule Actions Then (Make sure the activity has rules to evaluate)","1019":"[RB.1.2 c]1. Set the target objective to Undefined","15":"SB.2.9]11.9.1.1. If Activity is Active for the activity is False And (the activity is Not the common ancestor And adlseq:preventActivation for the activity is True) Then (If the activity being considered is not already active, make sure we are allowed to activate it)","1393":"No navigation request, exiting","1507":"Control IsThereDirtyData","790":"[RB.1.4.1]2. For each Rollup Condition in the set of Rollup Conditions","1594":"Control ScoUnloaded","729":"[UP.3]3.a Rollup the rollup set accumulated during the EndAttempt processes","966":"[RB.1.4.1]2.3. Add the value of the rollup condition (","660":"[EPNR] 2.5. For each possible navigation request that hasn't already been excluded","488":"[SB.2.3]3. If the Flow Tree Traversal Subprocess does not identify an activity Then (No activity to move to)","1705":" hundredths)","1323":"Element is: interactions.n.latency","140":"[SB.2.9]8.5.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Choice Activity Traversal Subprocess) (Nothing to deliver)","1280":"Control ClearPendingNavigationRequest","1244":"Element is: comments_from_lms._children","575":"[SB.2.1]3.1.1. Apply the Terminate Descendent Attempt Process to the root of the activity tree","1344":" is not the last overall activity","489":"[SB.2.10]1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)","297":"[SB.2.7]2.1. If Sequencing Control Flow for the parent of the activity is False Then (Confirm a 'flow' traversal is allowed from the activity)","1156":"Choice Sequencing Request Process [SB.2.9](","90":"[RB.1.4.2]2.1.2.1. If  Activity Progress Status for the activity is False Or (Activity Attempt Count for the activity is greater than (>) Zero (0) And Activity is Suspended for the activity is True) Then","328":"[RB.1.2 b]3.3. Apply the Rollup Rule Check Subprocess to the activity and the Satisfied rollup action Process all Satisfied rules last","1651":"[TB.2.3]5.2. Else","694":"[SR.2]5. Case: the Randomization Timing for the activity is On Each New Attempt","1694":", attempted = ","652":"[RB.1.2 c]7.2. Set the Objective Satisfied Status for the target objective to False","812":"[UP.4]2.2.1. Set the Activity is Suspended for the activity to False","900":"[SB.2.12]6.1. Apply the Previous Sequencing Request Process","1324":"Element is: objectives.description","681":"[SB.2.9]12.8.1.2. If the Choice Activity Traversal Subprocess returns False Then","1180":"Element is: nteractions.n.learner_response","1267":"Communications Save Data, synchronous=","1181":"Element is: learner_preference.audio_level","178":"[NB.2.1]9.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-2)","1407":"Element is: objectives._count","653":"[RB.1.2 b]3.4.1. Set the Objective Progress Status for the target objective to True","495":"[RB.1.4.1]5. Exit Evaluate Rollup Conditions Subprocess (Evaluation: the value of combined rule evaluation)","298":"[SB.2.8]2.1. If Sequencing Control Flow for the parent of the activity is False Then (Confirm a 'flow' traversal is allowed from the activity)","1206":"Element is: comments_from_learner.comment","1483":"Element is: deliver speed","31":"[SB.2.2]3.2.1. Exit Flow Activity Traversal Subprocess (Deliverable: False; Next Activity: the activity;End Sequencing Session: as identified by the Flow Tree Traversal Subprocess;  Exception: exception identified by the Flow Tree Traversal Subprocess)","958":"[SB.2.9]12.4.2.1.1.Set constrained activity to activity","1245":" is a descendent and will be disabled. ","1706":"Initialized ","464":"[TB.2.1]4.2. Apply the End Attempt Process to the exit target (End the current attempt on the 'exiting' activity)","1246":"[SR.1]3.1. Exit Select Children Process","1325":"Randomize Children Process [SR.2](","151":"[RB.1.2 b]3.1. If Objective Contributes to Rollup for the objective is True Then (Identify the objective that may be altered based on the activity's children's rolled up status)","63":"[UP.4]1.1.1.2.1.1.1. If the Objective Progress Status for the objective is False And Success Status was not changed during runtime Then (Did the content inform the sequencer of the activity's rolled-up objective status?)","813":"[OP.1]x. Navigation request is exit player, exit sequencing process.","879":"[RB.1.2]3.2.1 Invoke the Objective Rollup Process Using Rules","1652":"[SB.2.9]8.4. Else","718":"[RB.1.3 b]3.2. Set the Attempt Completion Status for the activity to False  ","1225":"' does not correspond to valid activity.","1306":"Initial Selection and Randomization","134":"[RB.1.3 a]4. If adlcp:completedbyMeasure for the target objective is True Then (If the completion is determined by measure, test the rolled-up progress against the defined threshold.)","352":"[SB.2.13]2. Exit Jump Sequencing Request Process (Delivery Request: the activity identified by the target activity; Exception: n/a)","1110":"Adding new Interaction Objective at position ","1508":") Violates control mode.","1131":"[SB.2.1]3.3. Else Entering a cluster Forward","1226":"Element is: comments_from_learner._count","216":"[NB.2.1]7.1.1.2.3.1.1. If Activity is Active for the activity is True And the Sequencing Control Choice Exit for the activity is False Then (Activity Identifier-","1546":") is tracked, tracked=","1326":"The common ancestor of activities ","1000":"[UP.4]1.1. If Tracked for the activity is True Then","398":"[SB.2.6]2.1. Exit Resume All Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.6-2) (Nothing to deliver)","1207":"Control Recieved Abandon Request From GUI","24":"[SB.2.1]2. If (previous traversal direction is Defined And is Backward) And the activity is the last activity in the activity's parent's list of Available Children Then (Test if we have skipped all of the children in a forward only cluster moving backward)","909":"TB.2.3]3.5. Until processed exit is False (processed exit=","910":"Current status is not attempted so changing based on score","719":"[RB.1.4]1.2.2.2.2.2.1. Add an Unknown value to the contributing children bag","861":"[UP.4]1.1.05 Transfer data from the RTE to the sequencing engine","1078":"[RB.1.2 c]3. Get the set of applicable children","187":"[UP.4]1.1.1.2.1.1.1. If the Objective Progress Status for the objective is False Then (Did the content inform the sequencer of the activity's rolled-up objective status?)","1595":"[SB.2.1]4.3.2. Else","106":"[RB.1.4.2]3.1.2. If (the Rollup Action is Completed And adlseq:requiredForCompleted is ifNotSuspended) Or (the Rollup Action is Incomplete And adlseq:requiredForIncomplete is ifNotSuspended) Then","1373":"RunTimeApi_ValidLocalizedString","730":"[EPNR] Current activity is undefined, not checking context dependent rules.","779":"[RB.1.3]4.2. Set the Attempt Completion Status for the activity to True","1468":"[RB.1.2 a]2.1.2. Break For","1469":"Communications_SaveDataNow","1627":" are not siblings.","446":"[SB.2.9]8.3. If the target activity occurs after the Current Activity in preorder traversal of the activity tree Then","748":"[SB.2.9]5.If the target activity is not the root of the activity tree Then","1060":"[OP.1]1.4. If there is a sequencing request Then","1470":"Element is: success status","1727":" and ","9":"[SB.2.2]3.3.2.1. Apply the Flow Activity Traversal Subprocess to the activity identified by the Flow Tree Traversal Subprocess in the traversal direction and a previous traversal direction of previous traversal direction (Recursive call \u2013 make sure the 'next' activity is OK)","862":"[DB.1.1]4.1. Apply the Check Activity Process to the activity - ","1182":"[RB.1.2]3.3 Exit Objective Roll up Process","1427":"Element is: progress measure","1038":"[RB.1.2 b]2.Set the target objective to Undefined","1099":"[RB.1.4.1]4. Apply the Condition Combination (","270":"[RB.1.2 a]3.1.2.2.1. Set the Objective Progress Status for the target objective to False (Incomplete information, do not evaluate objective status)","1039":"[RB.1.1]3. Set the counted measures to Zero (0.0)","1484":"[RB.1.2 a]3.1.2.1.2. Else","310":"[RB.1.4]1.2.2.2.2.2. If Evaluate Rollup Conditions Subprocess returned Unknown Then (Account for a possible 'unknown' condition evaluation)","890":"[RB.1.2 b]3. For each objective associated with the activity","1408":"[SB.2.9]10.4. Break All Cases","545":"[RB.1.1]6.1.1.3.1. Increment counted measures by the Rollup Objective Measure Weight for the child","844":"[RB.1.4.2]3. If the Rollup Action is Completed Or Incomplete Then","1374":") divided by counted measures (","1132":") And the Activity Attempt Count (currently ","1596":"[NB.2.1]10.2. Else ","825":"[RB.1.1 b]5.2.1.1. Set the Attempt Completion Amount Status to True","1471":"Control GetXmlForDirtyData","1281":"SCORM ERROR FOUND - Set Error State: ","1574":"Element is: language","911":"[RB.1.2 a]3.2. Exit Objective Rollup Using Measure Process","188":"[NB.2.1]5.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-7)","661":"[SB.2.12]5.2. If the Continue Sequencing Request Process returns an exception Then","1428":"StatusSetInCurrentSession = ","1157":"[RB.1.1]6.1. For each child of the activity","1357":"Element is: objectives.score.min","230":"[UP.4]4.5 Find all the activities that read the global objectives written by this activity and invoke the overall rollup process on them (not in pseudo code)","277":"[NB.2.1]11.1. If the Current Activity is Defined Then (If the sequencing session has already begun, unconditionally abandon all active activities)","453":"[UP.4]4.b. Apply the Overall Rollup Process to the parents of activities affected by write maps (not in pseudo code)","632":"[TB.2.3]5.5.1. Set Activity is Active for the activity to False (Activity Identifier=","967":"[SB.2.9]11.8.1. For each activity on the activity path","1472":" would flow to a cluster (","654":"[UP.4]1.1.1.2.1.1.1.2. Set the Objective Satisfied Status for the objective to True","1680":"The activities ","1001":"[RB.1.2 c]7. If All Not Satified. (allNotSatisfied=","170":"[NB.2.1]11.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Abandon All; Sequencing Request: Exit; Target Activity: n/a; Exception: n/a)","826":"[OP.1]1.4.4.1. Continue Loop - wait for the next navigation request","282":"[UP.4]1.1.1.1. If the Completion Set by Content for the activity is False Then (Should the sequencer set the completion status of the activity? )","880":"[SB.2.9]14.3. Set the Current Activity to the target activity","1445":"Element is: objectives.n.id","1528":"Element is: audio level","627":"[RB.1.2]2. Get the primary objective (For each objective associated with the activity)","1247":"[RB.1.2 c]Retrieving Status for child #","1446":"Element is: score._children","1183":"Element is: adl.nav.request_valid.previous","1597":"[SB.2.1]4.2.3. Else","72":"[SB.2.9.2]1.1. If the activity is the last available activity in a forward preorder tree traversal of the activity tree Or (the activity is the Root of the Activity Tree) Then (Cannot walk off the activity tree)","390":"[SB.2.9]10.3.3.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-6) (Nothing to deliver)","1184":"[SR.2]1.1. Exit Randomize Children Process","1598":"[SB.2.9.2]1.3. Else","1185":"[SR.2]3.1. Exit Randomize Children Process","1307":"[SB.2.9]8.4.1. traverse is Backward","1133":"Continue Sequencing Request Process [SB.2.7]","701":"[OP.1]1.3.3. If Termination Request Process returned a sequencing request Then","224":"[SB.2.1]3.2.1.2. Exit Flow Tree Traversal Subprocess (Return the results of the recursive Flow Tree Traversal Subprocess) (Return the result of the recursion)","968":"[SB.2.10]3. If the Current Activity is not a leaf Then","107":"[UP.4]2.1. If the activity includes any child activity whose Activity is Suspended attribute is True Then (The suspended status of the parent is dependent on the suspended status of its children)","59":"[NB.2.1]7.1.1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Choice; Target Activity: the activity specified by the Choice navigation request; Exception: n/a)","1529":"Element is: adl.data.id","108":"[RB.1.4.2]3.1.3.2.1. If (the Rollup Action is Completed And adlseq:requiredForCompleted is ifNotSkipped) Or (the Rollup Action is Incomplete And adlseq:requiredForIncomplete is ifNotSkipped) Then","867":"[SR.1]4.1.1.2. Iterate Selection Count, for the activity, times","1509":"Element is: audion level","257":"[SB.2.9.2]1.1. If the activity is the last activity in a forward preorder tree traversal of the activity tree Then (Cannot walk off the activity tree)","399":"[SB.2.9]11.4.2. If constrained activity is Undefined Then (Find the closest constrained activity to the current activity)","1485":"Element is: adl.data.n.id","189":"[NB.2.1]6.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-7)","1345":"[RB.1.4]1.2.0. Rule Description: ","731":"[OP.1]1.2.1. Handle the navigation request exception Behavior not specified","510":"[SB.2.11]2.1. Exit Exit Sequencing Request Process (End Sequencing Session: False; Exception: SB.2.11-2)","201":"[SB.2.1]3.2.2.2.Exit Flow Tree Traversal Subprocess (Next Activity: the activity identified by the traversal; Traversal Direction: traversal direction; Exception: n/a)","1447":"[EPNR] Cannot flow through ","682":"[UP.3]1. Find the activity that is the common ancestor of the Current Activity (","1134":"[RB.1.2]3.1.2 Exit Objective Roll up Process","732":"[OP.1]1.4.1. Apply the Sequencing Request Process to the sequencing request","513":"[SB.2.9]8.5.1. Apply the Choice Activity Traversal Subprocess to the activity in the traverse direction","219":"[SB.2.9]4.1. If the Sequencing Control Mode Choice for the parent of the target activity is False Then (Confirm that control mode allow 'choice' of the target)","709":"[SB.2.9]8.5.2. If the Choice Activity Traversal Subprocess returns False Then","912":"[RB.1.2 b]2.1.1. Set the target objective to the objective","163":"[SB.2.5]3.1. Apply the Flow Subprocess to the root of the activity tree in the Forward direction with consider children equal to True (Attempt to flow into the activity tree)","1409":"Pre-evaluation of exit action","1599":"[SB.2.1]3.3.2. Else","432":"[RB.1.2 b]4.1. Exit Objective Rollup Using Rules Process No objective contributes to rollup, so we cannot set anything","288":"[SB.2.9]12.Case: Target activity is forward from the common ancestor activity (Case #5 - target is a descendent activity of the common ancestor)","1448":"Current Activity is defined","43":"[SB.2.1]4.3.1.2.1. Exit Flow Tree Traversal Subprocess (Next Activity: the last activity in the activity's list of Available Children; Traversal Direction: Backward; Exception: n/a) Start at the end of the cluster if we are backing into it","400":"SuspendedActivityDefined[SB.2.6]2. If the Suspended Activity is Not Defined Then (Make sure there is something to resume)","881":"[RB.1.3 a]5.1.4.1 all completed = all completed AND completed","1293":"Navigation Request Process [NB.2.1](","1308":"API Runtime Nav Request Detected = ","1667":"[SB.2.10]4. Else","539":"[UP.1] 4 - 9 Note - duration and time based limit controls are not evaluated in this implementation","514":"[EPNR] 2.2 There's a rare situation where the suspend all termination request can fail, so check for it","551":") for the activity is greater than or equal (>=) to the Limit Condition Attempt Limit (currently ","1135":"[RB.1.4]1.2.9.1.1. Set status change to True","969":"Calling Integration Implementation UpdateControl State","1186":"[RB.1.2]3.4 Exit Objective Roll up Process","1309":"Control Recieved Close Sco From GUI","1358":"Element is: objectives.score.max","987":"[SB.2.9]11.3. For each activity on the activity path","662":"[UP.4]1.1.1.2.1.1.1.1. Set the Objective Progress Status for the objective to True","25":"[SB.2.1]4.2.2.1. Apply the Flow Tree Traversal Subprocess to the activity's parent in the Backward direction and a previous traversal direction of n/a with consider children equal to False (Recursion - Move to the activity's parent's next backward sibling)","1208":"Start Sequencing Request Process [SB.2.5]","1227":"Score exceeds mastery, setting to passed","407":"[SB.2.8]1.1. Exit Previous Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.8-1 ) (Nothing to deliver)","490":"[SB.2.9]12.1. Exit Choice Sequencing Request Process (Delivery Request: the target activity; Exception: n/a)","1724":"Mode = ","564":"[SB.2.2]4. Apply the Check Activity Process to the activity (Make sure the activity is allowed)","145":"[RB.1.2 a]2.1. If Objective Contributes to Rollup for the objective is True Then (Identify the objective that may be altered based on the activity's children's rolled up measure)","101":"[SB.2.10]2. If the Activity is Active for the Current Activity is True Or the Activity is Suspended for the Current Activity is True Then (Cannot retry an activity that is still active or suspended)","800":" will be disabled because its parent does not allow choice requests (","314":"[SB.2.9]3. Form the activity path as the ordered series of activities from the root of the activity tree to the target activity, inclusive","791":"[UP.2]1.2.2. If the Sequencing Rule Check Subprocess returns True Then","252":"[SB.2.9]14. If the Flow Subprocess returns False Then (Nothing to deliver, but we succeeded in reaching the target activity - move the current activity)","801":"[RB.1.2 b]3.4. If the Rollup Rule Check Subprocess returned True Then","1079":"[SB.2.1]4.3. Else (Entering a cluster Backward)","1486":"Recorded SuccessStatus = ","1681":"[EPNR] Setting ","770":"[DB.2.1]1.3.1.2.1.1. Set Activity is Suspended for the activity to False","710":"[SB.2.9]9.5.2. If the Choice Activity Traversal Subprocess returns False Then","642":"[SB.2.9]15.1. Apply the Terminate Descendent Attempts Process to the common ancestor","711":"[UP.4]1.1.1.1.1.2. Set the Attempt Completion Status for the activity to True","814":"[RB.1.4]1.2.7. Case: the Rollup Child Activity Set is At Least Count","420":"[EPNR] 3.1.1 Run the sequencing request process for that sequencing request returned by the termination request process","1294":"Element is: interactions.n.timestamp","1730":" is ","913":"Activity Progress Rollup Using Measure Process [RB.1.3 a](","633":"[OP.1]1.5.2. If the Delivery Request Process returned delivery request Not Valid Then","506":"[NB.2.1]8.1. If the Current Activity is Defined Then (Make sure the sequencing session has already begun)","1310":"Updating display for each menu item","1158":"[RB.1.1]6.1.1.2. Get the primary objective.","1530":"Deliverying Activity - ","1531":"[RB.1.4.2]2.1.3.2. Else","1728":"false","141":"[SB.2.9]9.3.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Choice Activity Traversal Subprocess) (Nothing to deliver)","643":"[SB.2.12]2.2. If the Resume All Sequencing Request Process returns an exception Then","695":"[RB.1.4]1.2.6.1. If the contributing children bag contains a value of True Then","882":" does not allow flow navigation. Flow navigation is disabled.","1429":"RunTimeApi_ValidOtheresponse","1628":"[NB.2.1]12.2. Else","1228":"Element is: comments from leaner.comment","353":"[TB.2.1]4.1. Apply the Terminate Descendent Attempts Process to the exit target (End the current attempt on all active descendents)","238":"[SB.2.4]2.1.2.1. Exit Choice Activity Traversal Subprocess (Reachable: False; Exception: SB.2.4-3) (Cannot walk backward from the root of the activity tree)","122":"[RB.1.1]6.1.1.3.2.1. Add the product of Objective Normalized Measure for the rolled-up objective multiplied by the Rollup Objective Measure Weight for the child to the total weighted measure","1209":"[RB.1.1 b]4.1.2.2. Set valid data to True","299":"[SB.2.9.2]1.3.1. Traverse the tree, forward preorder, one activity to the next activity, in the activity's parent's list of Available Children","471":"[UP.3]3.1.1. Apply the End Attempt Process to the activity (End the current attempt on each activity) Activity-","733":"[UP.2.1]2. For each Rule Condition for the Sequencing Rule for the activity","845":"[SR.2]3. Case: the Randomization Timing for the activity is Never","1311":"Element is: objectives.score_scaled","1600":"Element is: version","29":"[SB.2.1]3.2.1.1. Apply the Flow Tree Traversal Subprocess to the activity's parent in the Forward direction and a previous traversal direction of n/a with consider children equal to False (Recursion - Move to the activity's parent's next forward sibling)","48":"[SB.2.3]4.2. Apply the Flow Activity Traversal Subprocess to the candidate activity in the traversal direction and a previous traversal direction of n/a (Validate the candidate activity and traverse until a valid leaf is encountered)","311":"[RB.1.4.1]3. If the rollup condition bag is Empty Then (If there are no defined conditions for the rule, cannot determine the rule applies)","1159":"Element is: comments_from_learner._children","125":"[NB.2.1]7.1.1.2. If the activity specified by the Choice navigation request is Not a sibling of the Current Activity Then (We are always allowed to choose a sibling of the current activity)","802":"[DB.2]5.1.1.2.4. If objectives global to system is false (obj global=","1359":"Element is: objectives.score.raw","663":"[SB.2.9]11.3.1. If the activity is not the last activity in the activity path Then","1187":"[TB.2.1]4. If exit target is Not Null Then","239":"[SB.2.5]2.1. Exit Start Sequencing Request Process (Delivery Request: the root of the activity tree; Exception: n/a) (Only one activity, it must be a leaf)","1160":"[RB.1.1 b]4. For each child of the activity","1210":"Element is: interactions.learner_response","979":"[DB.2]5.1.1. If Tracked for the activity is True Then","1473":"controlChoiceExitIsUsed = ","240":"[TB.2.3]3.2. Apply the Sequencing Exit Action Rules Subprocess to the Current Activity (Check if any of the current activity's ancestors need to terminate)","1700":", returning 0","815":"[RB.1.4.2]2. If the Rollup Action is Satisfied Or Not Satisfied Then","1248":"[EPNR] The logically previous activity,","552":"[SB.2.1]2.2. activity is the first activity in the activity's parent's list of Available Children","683":"[SB.2.9]11.8.1.2. If the Choice Activity Traversal Subprocess returns False Then","1710":"Stored as: ","980":"[SB.2.1]3. If the traversal direction is Forward Then","76":"[OP.1]1.4.3.1. Exit Overall Sequencing Process - the sequencing session has terminated; return control to LTS (Exiting from the root of the activity tree ends the sequencing session; return control to the LTS)","1327":"Delivery Request Process [DB.1.1](","1532":"Element is: launch_data","1575":"[NB.2.1]10.1.2. Else","1562":"CompletedByMeasure = ","467":"[SB.2.9]11.5.1. If the target activity is Forward in the activity tree relative to the constrained activity Then","496":"[SB.2.9]8. Case: Current Activity and target activity are identical (Case #1 - select the current activity)","846":"[TB.2.3]5.5.2. Set Activity is Suspended for the activity to True","497":"[SB.2.9]7. Case: Current Activity and target activity are identical (Case #1 - select the current activity)","1410":"Element is: interacitons.type","1229":"Element is: objectives.n.score._children","1111":"[RB.1.4]1.2.2. For each child of the activity","1268":"Sequencing Rules Check Process [UP.2](","1474":"[RB.1.2 b]3.1.2. Break For","1188":"[SR.2]4.2. Exit Randomize Children Process","1682":"[SB.2.8]5. Else","837":"The Return To LMS button should be not be available for selection.","1449":"Generating Exit Nav Request","1375":"ERROR - invalid success status-","981":"[SB.2.4]1. If the traversal direction is Forward Then","1136":"[DB.2]7. Set Suspended Activity to undefined","433":"[SB.2.10]1.1. Exit Retry Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.10-1) (Nothing to deliver)","624":"[OP.1]1.2. If the Navigation Request Process returned navigation request Not Valid Then","220":"[SB.2.11]3.1. Exit Exit Sequencing Request Process (End Sequencing Session: True; Exception: n/a) (The sequencing session has ended, return control to the LTS)","827":"[RB.1.3 b]5. If the Rollup Rule Check Subprocess returned True Then","1211":"[RB.1.3 a]4. initialize completed to true","1394":"[EPNR] A precondition rule on ","356":"[TB.2.3]7.4. Set the Current Activity to the root of the activity tree Move the current activity to the root of the activity tree","1563":"Element is: score.min","37":"[RB.1.4.1]2.1. Evaluate the rollup condition by applying the appropriate tracking information for the activity to the Rollup Condition. (Evaluate each condition against the activity's tracking information. This evaluation may result in 'unknown'.)","123":"[SB.2.12]8.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request:  the result of the Jump Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)","1212":"RunTimeApi_IsValidArrayOfLocalizedStrings","838":"[EPNR] Current activity is undefined, flow navigation is disabled.","914":"[RB.1.4]1.2.6. Case: the Rollup Child Activity Set is None","847":"[RB.1.3 a]5.1.5.1. all incomplete = all incomplete AND incomplete","932":"[SB.2.9]8.1. Break All Cases (Nothing to do in this case)","1601":"Suspending Activity","1137":"[RB.1.2]3.2.2 Exit Objective Roll up Process","1360":"Measure Rollup Process [RB.1.1](","1668":"[SB.2.5]3. Else ","944":"[OP.1]2. End Loop - wait for the next navigation request","333":"[DB.2.1]1.3.1. For each activity in the activity path (Walk down the tree setting each of the identified activities to not suspended)","1683":", notSatisfied=","763":"[OP.1]1.1. Apply the Navigation Request Process to the navigation request","664":"[SB.2.9]11.4.1. If the activity is not the last activity in the activity path Then","988":"[RB.1.3 a]3.  Set Attempt Completion Status to False","1430":"Element is: max time allowed","816":"[EPNR] Check rules that rely on the context of the current activity.","131":"[SB.2.8]3. Apply the Flow Subprocess to the Current Activity in the Backward direction with consider children equal to False (Flow in a backward direction to the next allowed activity)","1602":"LMSGetErrorString('","498":"[SB.2.9]1. If there is no target activity Then (There must be a target activity for choice) targetActivity=","278":"[RB.1.4.2]2.1.2.1. If Activity Attempt Count for the activity is greater than (>) Zero (0) And Activity is Suspended for the activity is True Then","1450":"Control CreateMenuItem for ","5":"[SB.2.9]11.5.5. If the target activity is Not an available descendent of the activity to consider And the target activity is Not the activity to considered And the target activity is Not the constrained activity Then (Make sure the target activity is within the set of 'flow' constrained choices)","1080":"[DB.2]5. For each activity in the activity path","749":") from the runtime will succeed now after re-evaluating with current data.","1061":"Sequencing Exit Action Rules Subprocess [TB.2.1]","1684":"MasteryScore = ","523":"[SB.2.9]11.5.3. Apply the Choice Flow Subprocess to the constrained activity in the traverse direction","447":"[SB.2.9]9.3. If the target activity occurs after the Current Activity in preorder traversal of the activity tree Then","1533":"[RB.1.2 c] satisfied = ","1312":"Adding new interaction at position ","734":"[TB.2.1]3.2. If the Sequencing Rules Check Process does not return Nil Then","1411":"Element is: time_limit_action","1475":"[UP.4]1. If the activity (","1040":"[RB.1.3 a]2. Set Attempt Progress Status to False","735":"[SR.1]5. Case: the Selection Timing for the activity is On Each New Attempt","634":"[RB.1.1 b]4.1. If Tracked for the child is True Then (Only include tracked children.)","190":"[UP.2]1.1. Initialize rules list by selecting the set of Sequencing Rules for the activity that have any of the specified Rule Actions (maintaining original rule ordering","989":"Evaluate Possible Navigation Requests Process [EPNR]","265":"[SB.2.9.2]2.3.1. Traverse the tree, reverse preorder, one activity to the previous activity, from the activity's parent's list of Available Children","1547":"Element is: total time","712":"[SB.2.7]2. If the activity is not the root activity of the activity tree Then","119":"[SB.2.12]7.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Choice Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)","736":"[RB.1.2 c]6.1.3 not satisfied = (satisfied === false || attempted === true)","434":"[SB.2.1]4.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: Nil; Traversal Direction: n/a; Exception: SB.2.1-3)","771":"[RB.1.3 a]5.1.3 incomplete = (completed === false || attempted === true)","511":"[SB.2.11]1.1. Exit Exit Sequencing Request Process (End Sequencing Session: False; Exception: SB.2.11-1)","435":"[SB.2.10]2.1. Exit Retry Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.10-2) (Nothing to deliver)","901":"[RB.1.5] No tracking data changed, stopping further rollup.","1576":"Element is: location","391":"[RB.1.2 a]4.1. Exit Objective Rollup Using Measure Process (No objective contributes to rollup, so we cannot set anything)","945":"[RB.1.2]2.1.1. Set the target objective to the objective","585":"[SB.2.4]2.1.1. If Sequencing Control Forward Only for the parent of the activity is True Then","300":"[SB.2.1]3.2.2.1. Traverse the tree, forward preorder, one activity to the next activity, in the activity's parent's list of Available Children","946":"[SB.2.12]1.1. Apply the Start Sequencing Request Process","611":"[UP.2.1]3.1. Exit Sequencing Rule Check Subprocess (Result: Unknown) (No rule conditions)","334":"[NB.2.1]2.1.1. If the Suspended Activity is Defined Then (Make sure the previous sequencing session ended with a suspend all request)","1431":"Recorded CompletionStatus = ","817":"[RB.1.4]1.2.8. Case: the Rollup Child Activity Set is At Least Count","1564":"Element is: score.raw","191":"[TB.2.2]2. Apply the Sequencing Rules Check Process to the Current Activity and the set of Post Condition actions (Apply the post condition rules to the current activity)","1189":"[UP.2]1.2. For each rule in the rules list","249":"[SB.2.9]9.1. Form the activity path as the ordered series of activities from the common ancestor to the target activity, exclusive of the target activity","1376":") and the identified activity (","1653":"Close Out Session","674":"[TB.2.3]3.3.4.1.1. Set the Current Activity to the parent of the Current Activity","868":", is disabled and cannot be delivered. Disable previous button.","377":"[UP.1]1. If Tracked for the activity is False Then (If the activity is not tracked, its limit conditions cannot be violated)","1695":"Session Time: ","803":"[RB.1.2 b]4.2. If the Rollup Rule Check Subprocess returned True Then","1081":"[NB.2.1]3. Case: navigation request is Continue","1082":"[TB.2.3]7.2. If the activity path is Empty Then","271":"[RB.1.4]1.2.7.1. If the count of True values contained in the contributing children bag equals or exceeds the Rollup Minimum Count of the rule Then","454":"[RB.1.5]3.1.2. Apply the Completion Measure Rollup Process to the activity (Rollup the activity\u2019s progress measure.)","1685":"LMSInitialize('","839":"[DB.1.1]3. If the activity path is Empty Then - Nothing to deliver","883":"[RB.1.3 a]5.1.4. Child should be included in completed rollup","618":"[UP.2.1]5. Exit Sequencing Rule Check Subprocess (Result: the value of rule evaluation) ","260":"[TB.2.3]5.2.2.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-3) (Nothing to suspend)","320":"[SB.2.11]2. If the Activity is Active for the Current Activity is True Then (Make sure the current activity has already been terminated)","266":"DB.2]1.1. Exit Content Delivery Environment Process (Exception: DB.2-1) (Delivery request is invalid - The Current Activity has not been terminated)","828":"[SR.2]6. Exit Randomize Children Process Undefined timing attribute","1629":"Deliver activity: ","780":"[RB.1.3 a]7.1. Set the Attempt Progress Status for the activity to True","1083":"[TB.2.3]6. Case: termination request is Abandon","47":"[TB.2.2]3.1.1. Exit Sequencing Post Condition Rules Subprocess (Sequencing Request: the value returned by the Sequencing Rules Check Process; Termination Request: n/a) (Attempt to override any pending sequencing request with this one)","421":"[SB.2.9]5.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-4) (Nothing to deliver)","152":"[RB.1.2 b]2.1. If Objective Contributes to Rollup for the objective is True Then (Identify the objective that may be altered based on the activity's children's rolled up status)","1346":"RunTimeApi_ValidTrueFalseResponse","1603":"[NB.2.1]8.1.2. Else","1041":"[RB.1.2 c]4. initialize all not satisfied to true","1138":"[RB.1.4]1.2.7.1.1. Set status change to True","179":"[NB.2.1]3.1.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-2)","959":"[RB.1.3 a]4.2.2.1. Set the Attempt Progress Status True","1577":"Next entry is normal","64":"[SB.2.7]4.1. Exit Continue Sequencing Request Process (Delivery Request: n/a; End Sequencing Session: as identified by the Flow Subprocess; Exception: the exception identified by the Flow Subprocess) (Nothing to deliver)","193":"[NB.2.1]9.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit All; Sequencing Request: Exit; Target Activity: n/a; Exception: n/a)","1548":"Server Responded With:","1062":"[RB.1.3]5. Exit Activity Progress Rollup Process","1313":"Communications_StartPostDataProcess","829":"[RB.1.1]6.1.1.3.2.2. Set valid data to True - Normalized Measure = ","1100":"[NB.2.1]7. Case: navigation request is Choice ","499":"[RB.1.2]3.2.1 Apply Objective Rollup Using Rules Process (The default is included in the Rules process now)","1063":"[SB.2.12]6. Case: sequencing request is Previous","1549":"Element is: learner id","1654":"[SB.2.2]3.3. Else","1578":"RunTimeApi_ValidReal","507":"[TB.2.3]5.1.0.1. Apply the End Attempt Process to the Current Activity upon Return To LMS (Not in p-code)","684":"[RB.1.4.2]2.1.3.1.1. If Activity Attempt Count for the activity is Zero (0) Then","132":"[SB.2.9]12.8.1.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Choice Activity Traversal Subprocess) (Nothing to deliver)","1042":"WARNING - Unable to normalize score - Raw Score: ","891":"[RB.1.2 b]2. For each objective associated with the activity","1347":"Objective RollupProcess [RB.1.2](","1655":"[RB.1.2 a]4. Else","1269":"Control Recieved Next Request From GUI","792":"[DB.2.1]1.3.1.1.1. Set Activity is Suspended for the activity to False","1230":"Element is: learner_preference._children","1361":"[RB.1.4.2] Set included to False","1669":"Element is: exit","343":"[SB.2.9]11.1. Form the activity path as the ordered series of activities from the Current Activity to the common ancestor, inclusive","1328":"RunTimeApi_ValidLongFillInResponse","1084":"[RB.1.3 a]2. Get the set of applicable children","17":"[SB.2.9]10.3.3. If Activity is Active for the activity is False And (the activity is Not the common ancestor And adlseq:preventActivation for the activity is True) Then (If the activity being considered is not already active, make sure we are allowed to activate it)","612":"[RB.1.2 a]3.1.2.1.2.1. Set the Objective Progress Status for the target objective to True","772":"[OP.1]x. Navigation request is display message, exit sequencing process.","655":"[RB.1.2 b]4.2.1. Set the Objective Progress Status for the target objective to True","1451":"Element is: adl.data._count","225":"[SB.2.2]6.3.3. Exit Flow Activity Traversal Subprocess - (Return the results of the recursive Flow Activity Traversal Subprocess) Possible exit from recursion","1412":"[SB.2.9]11.4. Break All Cases","476":"TB.2.3]3.3.3. If the Sequencing Post Condition Rule Subprocess returned a termination request of Exit All Then","244":"[SB.2.9]12.6. Form the activity path as the ordered series of activities from the common ancestor to the target activity, exclusive of the target activity","1249":"Control Recieved Return To Lms From GUI","164":"[DB.2]5.1.1.2.3. Initialize Objective Progress Information and Attempt Progress Information required for the new attempt. Initialize tracking information for the new attempt.","422":"[SB.2.9]2.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-2) (Nothing to deliver)","508":"[SB.2.9]12.8.1.1. Apply the Choice Activity Traversal Subprocess to the activity in the Forward direction","1213":"Jump Sequencing Request Process [SB.2.13]","83":"[TB.2.2]3.2.1. Exit Sequencing Post Condition Rules Subprocess (Sequencing Request: n/a; Termination Request: the value returned by the Sequencing Rules Check Process) (Terminate the appropriate activity(s))","226":"[SB.2.8]4.1. Exit Previous Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Flow Subprocess) (Nothing to deliver)","1190":"[RB.1.1]6.3.3. Exit Measure Rollup Process","1718":"). Equals ","685":"[SB.2.9.2]1.1.1. Exit Choice Flow Tree Traversal Subprocess (Next Activity: Nil)","183":"TB.2.3]3.4. If the Current Activity is the Root of the Activity Tree AND the sequencing request returned by the Sequencing Post Condition Rule Subprocess is not Retry Then","1377":"Select Children Process [SR.1](","500":"[SB.2.5]1. If the Current Activity is Defined Then (Make sure the sequencing session has not already begun)","848":"[RB.1.3]2. If the Rollup Rule Check Subprocess returned True Then","1413":"Element is: completion_status","1362":"Element is: interactions.latency","42":"[SB.2.12]1.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Start Sequencing Request Process; End Sequencing Session: as identified by the Start Sequencing Request Process; Exception: n/a)","436":"[SB.2.9]15.4. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-9) (Nothing to deliver)","279":"[RB.1.4.2]3.1.2.1. If Activity Attempt Count for the activity is greater than (>) Zero (0) And Activity is Suspended for the activity is True Then","408":"[SB.2.9]12.7.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)","1395":"Check Activity Process [UP.5](","129":" since it does not allow flow navigation. Stopping here, continue button stays enabled even though request won't succeed. Continue results in user being prompted to select a child item.","71":"[SB.2.12]2.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Resume All Sequencing Request Process)","576":"Progress Measure is less than Completion Threshold so setting completion status to incomplete.","1396":"RunTimeApi_ValidLongIdentifier","1630":"[SB.2.12]7.3. Else","1329":"Element is: interactions.weighting","1656":"[NB.2.1]4.3. Else","1282":"Element is: objectives.n.score.scaled","1487":"Checking for Commit Error","644":"[RB.1.2 b]3.4.2. Set the Objective Satisfied Status for the target objective to True","619":"Progress Measure exceeds Completion Threshold so setting completion status to completed.","1064":"[SB.2.9]11.7. If the activity path is Empty Then","1452":"Control RenderMenuItem for ","1670":"The identifier '","210":"[DB.2]2. If the activity identified for delivery is not equal to the Suspended Activity Then (Content is about to be delivered, clear any existing suspend all state)","1112":"[SB.2.12]1. Case: sequencing request is Start","737":"[UP.4]1.1.1.1.1.1. Set the Attempt Progress Status for the activity to True","304":"[SB.2.9]12.8. If the target activity is forward in the activity tree relative to the Current Activity Then (Walk toward the target activity)","915":"User requested a Suspend All, setting exit type to suspend","1579":"[SB.2.9]11.5.2. Else","635":"[RB.1.4.2]2.1.3.2.1.2. If the Sequencing Rules Check Process does not return Nil Then","601":"[SB.2.9]9.3.3.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: ","1476":"Control DeliverActivity - ","1604":"[NB.2.1]7.1.2. Else","1043":"[RB.1.5]3. For each activity in the activity path","750":"[TB.2.2]3.3. If the Sequencing Rules Check Process returned Retry All Then","540":"[NB.2.1]12.1. If the Current Activity is Defined Then (If the sequencing session has already begun)","468":"[RB.1.4.2]2.1.3.2.1.1. Apply the Sequencing Rules Check Process to the activity and its Skipped sequencing rules","437":"[SB.2.13]1.1. Exit Jump Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.13-1) (Nothing to deliver.)","593":"[SB.2.9]11.5.5.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: ","79":"[NB.2.1]13.1.2. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-11) (Target activity does not exist.)","933":"[SB.2.9]7.1. Break All Cases (Nothing to do in this case)","1191":"Activity Progress Rollup Process [RB.1.3](","738":"[OP.1]1.5.2.1. Handle the delivery request exception Behavior not specified","528":"[SB.2.9]11.5.1.1. traverse is Forward ('Flow' in a forward direction to see what activity comes next)","1510":" is before the activity ","60":"[NB.2.1]7.1.1.5.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Choice; Target Activity: the activity specified by the Choice navigation request; Exception: n/a)","636":"[RB.1.2 b]3.2.2. Set the Objective Satisfied Status for the target objective to False","53":"[TB.2.3]3.3.4. If the Sequencing Post Condition Rule Subprocess returned a termination request of Exit Parent Then (If we exit the parent of the current activity, move the current activity to the parent of the current activity.)","1511":"Found Dirty Data to Save","86":"[NB.2.1]7.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-11) (Target activity does not exist)","1330":"[EPNR] Disable the descendents of ","1432":"[SB.2.9]8.6. Break All Cases","1139":"Element is: comments_from_learner.n.location","1488":"Element is: mastery score","1283":"Checking for valid choice nav request","960":"Objective element is undefined, returning empty string.","645":"[RB.1.2 c]7.3.2. Set the Objective Satisfied Status for the target objective to True","289":"[NB.2.1]10.1.1. If the Activity is Active for the Current Activity is True Then (Make sure the current activity has not already been terminated)","793":" is not a descendent of the previous/next activity and will be hidden.","1489":"Flow Subprocess [SB.2.3](","91":"[RB.1.3]1. Using Measure:  If the activity has Completed by Measure equal to True, then the rolled-up progress measure is compared against the Minimum Progress Measure and Measure Satisfaction if Active:","675":"[DB.2]5.1.1.2.5.1. Reset any shared data associated with this attempt on content.","509":"[SB.2.3]1. The candidate activity is the activity The candidate activity is where we start 'flowing' from","1671":") is a leaf Then","916":"[RB.1.2 c]6.4 all statisfied = all satisfied AND satisfied","1192":"Retry Sequencing Request Process [SB.2.10]","1020":"[SB.2.12]2. Case: sequencing request is Resume All","586":"[TB.2.3]3.3.2. Apply the Sequencing Post Condition Rules Subprocess to the Current Activity (","465":"[NB.2.1]2.1. If the Current Activity is Not Defined Then (Make sure the sequencing session has not already begun)","686":"[RB.1.4]1.2.2.2.2.4. If Evaluate Rollup Conditions Subprocess returned True Then","1044":"Element is: interactions.correct_responses._count","1696":"GetLastError()","1161":"Element is: interactions.n.learner_response","1250":"[RB.1.2 c]0.5.1 Exit, nothing to rollup","1214":"Exit Sequencing Request Process [SB.2.11]","293":"[RB.1.2 b]4.1. Apply the Rollup Rule Check Subprocess to the activity and the Not Satisfied rollup action Process all Not Satisfied rules first","1140":"[NB.2.1]13. Case: navigation request is Jump","892":"[DB.2]5.1.2. Set Activity is Active for the activity to True","1284":"Termination Request Process [TB.2.3](","1251":"Element is: objectives.n.success_status","1193":"Element is: adl nav request valid previous","1295":"Element is: interactions.n.weighting","211":"[UP.2]1.2.2.1. Exit Sequencing Rules Check Process (Action: Rule Action for the rule) (Stop at the first rule that evaluates to true - perform the associated action)","387":"[RB.1.1]6.3.1 If counted measures is greater than (>) Zero (0.0) Then (Set the rolled-up measure for the target objective.)","1657":"[SB.2.9]9.4. Else","849":"[TB.2.3]5.1.1. Set the Suspended Activity to the Current Activity","1085":"[NB.2.1]10. Case: navigation request is Abandon","1194":") is Active for the activity is False Then","392":"[EPNR] Check each activity for sequencing rules that are independent of context (i.e. independent of the current activity)","1113":"Element is: comments_from_learner.n.timestamp","1672":"GetErrorString('","1215":"Control Recieved Suspend Request From GUI","501":"[RB.1.1]6.1.1.4.1. Exit Measure Rollup Process (One of the children does not include a rolled-up objective)","294":"[EPNR] 2.3 If there's an exit all request, the termination request process performs sequencing actions that are relevant later on, so do those.","850":" will be hidden because its isVisible attribute is set to false. ","917":"[RB.1.1]5. For each objective associated with the activity","1231":"Element is: student_preference._children","1021":"[RB.1.3 b]6. Exit Activity Progress Rollup Process","364":"[RB.1.1 b]5.2.2.1. Set the Attempt Completion Amount Status for the target objective to False (No children contributed weight.)","1453":"In ScoHasTerminatedSoUnload","751":"[EPNR] 2.5.1.1. Activity is disabled, mark it and its children as disabled","1065":"[TB.2.3]4. Case: termination request is Exit All","196":"[NB.2.1]7.1. If the activity specified by the Choice navigation request exists within the activity tree Then (Make sure the target activity exists in the activity tree)","1550":"Control Update Display","1433":"Loading Sco In Frameset at: ","604":"[RB.1.2 a]3.1.2.1.1.2. Set the Objective Satisfied Status for the target objective to True","97":"[RB.1.4.2]2.1.3.2.1. If (the Rollup Action is Satisfied And adlseq:requiredForSatisfied is ifNotSkipped) Or (the Rollup Action is Not Satisfied And adlseq:requiredForNotSatisfied is ifNotSkipped) Then","393":"[UP.5]3. Apply the Limit Conditions Check Process to the activity (Make the activity does not violate any limit condition)","1086":"[TB.2.3]5.1.1. Apply the Overall Rollup Process","918":"[SB.2.9]11.4.2.1.1.Set constrained activity to activity: '","871":"[RB.1.3 a]4.2.1.2. Set the Attempt Completion Status to True  ","1002":"[RB.1.1 b]6. Exit Completion Measure Rollup Process","594":"[RB.1.4.2]2.1.1. Set included to True (Default Behavior \u2013 adlseq:requiredFor[xxx] == always)","1162":"[NB.2.1]1.Case: navigation request is Start","423":"[RB.1.4.2]3.1. If the Rollup Progress Completion value for the activity is True Then (Test the progress rollup control)","321":"[RB.1.1 b]5.1. Set the Attempt Completion Amount Status to False (No progress state rolled-up, cannot determine the rolled-up progress.)","1285":"[RB.1.3 a]1.1 Exit, nothing to rollup","1580":"[NB.2.1]2.1.2. Else ","1045":"Evaluate Rollup Conditions Subprocess [RB.1.4.1](","109":"[SB.2.12]2.3.1. Exit Sequencing Request Process (Sequencing Request: Valid; Delivery Request: the result of the Resume All Sequencing Request Process; End Sequencing Session: n/a; Exception: n/a)","1711":"LMSCommit('","1114":"[RB.1.2 c]5. initialize all satisfied to true","1605":"Element Not Matched","637":"[SB.2.2]6.2. If the Flow Tree Traversal Subprocess does not identify an activity Then","110":"[RB.1.2 b]1.1. Apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Satisfied; and a Rollup Action of Satisfied (Define the default satisfied rule )","830":"[DB.2]9. Exit Content Delivery Environment Process (Exception: n/a)","355":"[TB.2.3]3.3.4.2.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-4)","1490":"Element is: lesson_status","546":"[RB.1.4]1.2.6.1. If the contributing children bag does not contain a value of True Or Unknown Then","1":"[NB.2.1]7.1.1.2.2. Form the activity path as the ordered series of activities from the Current Activity to the common ancestor (The common ancestor will not terminate as a result of processing the choice sequencing request, unless the common ancestor is the Current Activity - the current activity should always be included in the activity path)","1141":"Flow Activity Traversal Subprocess [SB.2.2](","1491":"Element is: lesson_statuc","1003":"[TB.2.3]5.5. For each activity in the activity path","102":"[NB.2.1]4.2.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-5) (Violates control mode)","1492":"Element is: lesson status","1658":"LMSGetLastError()","577":"Scaled Passing Score is known, but score is unknown, therefore SuccessStaus is unknown as well","61":"[NB.2.1]7.1.1.4.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Choice; Target Activity: the activity specified by the Choice navigation request; Exception: n/a)","329":"[RB.1.4]1. If the activity includes Rollup Rules with the specified Rollup Action Then  (Make sure the activity has rules to evaluate)","665":"[[SB.2.1]4.3.1.1. If Sequencing Control Forward Only for the activity is True Then","872":"[SR.2]5.1. If Randomize Children for the activity is True Then","455":"TB.2.3]3.4.1. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: Exit; Exception n/a)","1232":"Nav request will succeed. Unloading SCO.","456":"[SB.2.5]1.1. Exit Start Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.5-1) (Nothing to deliver)","961":"[RB.1.1]1. Set the total weighted measure to Zero (0.0)","1004":"[SB.2.9]11.3. Set constrained activity to Undefined","1142":"[RB.1.4]1.2.5.1.1. Set status change to True","1087":"[NB.2.1]9. Case: navigation request is Exit All","401":"[SB.2.6]1.1. Exit Resume All Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.6-1) (Nothing to deliver)","1434":"[SB.2.9]9.6. Break All Cases","1088":"Communications Call Failed, Failed Submissions=","1115":"[RB.1.5]2. If the activity path is Empty Then","52":"[SB.2.9]14. Apply the Flow Subprocess to the target activity in the Forward direction with consider children equal to True (The identified activity is a cluster. Enter the cluster and attempt to find a descendent leaf to deliver)","1631":"[SB.2.12]1.3. Else","1363":"Element is: completion_threshold","1606":"[SB.2.9.2]2.3. Else","92":"[RB.1.1 b]4.1.2.1. Add the product of Attempt Completion Amount multiplied by the adlcp:progressWeight to the total weighted measure (Only include progress that has been reported or previously rolled-up)","142":"[SB.2.9]9.5.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Choice Activity Traversal Subprocess) (Nothing to deliver)","41":"[SB.2.3]3.1. Exit Flow Subprocess (Identified Activity: candidate activity; Deliverable: False; End Sequencing Session: as identified by the Flow Tree Traversal Subprocess; Exception: exception identified by the Flow Tree Traversal Subprocess)","1216":"Initializing Possible Navigation Requests","272":"activity.HasSeqRulesRelevantToChoice = false and possibleNavRequest.WillNeverSucceed = false.  Setting possibleNavRequest.WillAlwaysSucceed = true.","69":"[RB.1.2 b]1.2. Apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Objective Status Known; and a Rollup Action of Not Satisfied (Define the default not satisfied rule )","344":"[TB.2.1]4.3. Set the Current Activity to the exit target (Move the current activity to the activity that identified for termination)","481":"[TB.2.2]4. Exit Sequencing Post Condition Rules Subprocess (Sequencing Request: n/a; Sequencing Request; n/a)","1659":"[NB.2.1]3.3. Else","357":"[SB.2.2]7. Exit Flow Activity Traversal Subprocess (Deliverable: True; Next Activity: the activity; Exception: n/a ) Found a leaf","970":"[SB.2.4]2. If the traversal direction is Backward Then","1005":"[NB.2.1]11. Case: navigation request is Abandon All","919":"[RB.1.2 a]2.1.1. Set the target objective to the objective","947":"[RB.1.2 b]3.5. Exit Objective Rollup Using Rules Process","1607":"[SB.2.4]2.1.2. Else","1066":"Element is: interactions.corret_responses._count","893":"[SR.1]4. Case: the Selection Timing for the activity is Once","477":"[SB.2.9.2]1.2. If the activity is the last activity in the activity's parent's list of Available Children Then","1378":"Element is: interactions.result","1331":"[SB.2.9]8.3.1. traverse is Forward","103":"[RB.1.1]6.2.1. Set the Objective Measure Status for the target objective to False (No tracking state rolled-up, cannot determine the rolled-up measure. Total of all objectivemeasureweight values = ","1686":"[RB.1.3]2. Else","1565":"[TB.2.3]3.3.4.2. Else","1364":"RunTimeApi_ValidMatchingResponse","1566":"[SB.2.1]4.3.1.2. Else","1006":"Sequencing Post Condition Rules Subprocess [TB.2.2]","23":"[RB.1.2 b]1. If the activity does not include Rollup Rules with the Not Satisfied  rollup action And the activity does not include Rollup Rules with the Satisfied  rollup action Then (If no objective rollup rules are defined, use the default rollup rules.)  ","84":"[SB.2.12]8.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Jump Sequencing Request Process)","1348":"Element is: interactions.n.result","77":"[UP.4]1.1.1.1.1. If the Attempt Progress Status for the activity is False  And Completion Status was not changed during runtime  Then (Did the content inform the sequencer of the activity's completion status?)","1608":"[TB.2.3]3.3.5. Else","98":"[RB.1.4.2]2.1.2. If (the Rollup Action is Satisfied And adlseq:requiredForSatisfied is ifNotSuspended) Or (the Rollup Action is Not Satisfied And adlseq:requiredForNotSatisfied is ifNotSuspended) Then","1365":"Element is: objectives._children","587":"[TB.2.3]4.2. Apply the Terminate Descendent Attempts Process to the root of the activity tree","884":"[RB.1.3 a]4.2.2.2. Set the Attempt Completion Status to False","1089":"[RB.1.2 c]6.1 satisfied = activity.getsatisfied","773":"[DB.2]5.1.1.2.2.1. Set Activity Progress Status for the activity to True","613":"[RB.1.2 a]3.1.2.1.1.1. Set the Objective Progress Status for the target objective to True","205":"[SB.2.3]3.1. Exit Flow Subprocess (Identified Activity: candidate activity; Deliverable: False; Exception: exception identified by the Flow Tree Traversal Subprocess)","448":"[SB.2.9.2]1.3.2. Exit Choice Flow Tree Traversal Subprocess (Next Activity: the activity identified by the traversal)","253":"[SB.2.9]11.1. Form the activity path as the ordered series of activities from the Current Activity to the common ancestor, excluding the common ancestor","438":"[SR.1]4.1. If the Activity Progress Status for the activity is False Then (If the activity has not been attempted yet)","971":"[SB.2.1]4. If the traversal direction is Backward Then","972":") (The current activity must have already been exited)","1101":"[RB.1.4.2]3.1.3.2.1.2.1. Set included to False","1349":"Overall Sequencing Process [OP.1]","137":"[SB.2.9.2]2.2.1.1. Exit Choice Flow Tree Traversal Subprocess (Next Activity: the results of the recursive Choice Flow Tree Traversal Subprocess) (Return the result of the recursion)","620":"[RB.1.2 c]2. Get the primary objective (For each objective associated with the activity)","120":"[RB.1.4]1.2.9.1. If the percentage (normalized between 0..1, inclusive) of True values contained in the contributing children bag equals or exceeds the Rollup Minimum Percent of the rule Then","457":"[UP.2]2. Exit Sequencing Rules Check Process (Action: Nil) (No rules evaluated to true - do not perform any action)","62":"[UP.2.1]4. Apply the Rule Combination for the Sequencing Rule to the rule condition bag to produce a single combined rule evaluation ('And' or 'Or' the set of evaluated conditions, based on the sequencing rule definition)","254":"[TB.2.3]8. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-7) Undefined termination request","1143":"[SB.2.12]8. Case: sequencing request is Jump","948":"Activity Progress Rollup Using Rules Process [RB.1.3 b](","365":"[NB.2.1]7.1.1.2.1. Find the common ancestor of the Current Activity and the activity specified by the Choice navigation request","512":"[SB.2.10]3.a. (step not in pseudo code) Reset activity data for the re-tried activity and its children (","578":"[SB.2.9]5. If the Current Activity is Defined Then (Has the sequencing session already begun?)","529":"[SB.2.9]12.5.1.1. traverse is Forward ('Flow' in a forward direction to see what activity comes next)","920":"[SB.2.2]5. If the Check Activity Process returns True Then","378":"[SB.2.9]12.4.1.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-7) (Nothing to deliver)","702":"[OP.1]1.3.2.1. Handle the termination request exception Behavior not specified","1567":"[RB.1.1]6.1.1.4. Else","1046":"Choice Flow Tree Traversal Subprocess [SB.2.9.2](","16":"SB.2.9]12.9.1.1. If Activity is Active for the activity is False And (the activity is Not the common ancestor And adlseq:preventActivation for the activity is True) Then (If the activity being considered is not already active, make sure we are allowed to activate it)","146":"[RB.1.2 c]2.1. If Objective Contributes to Rollup for the objective is True Then (Identify the objective that may be altered based on the activity's children's rolled up measure)","214":"[RB.1.4]1.1. Initialize rules list by selecting the set of Rollup Rules for the activity that have the specified Rollup Actions, maintaining original rule ordering","1116":"Control Recieved Abandon All Request From GUI","534":"[SR.1]4.1.1.2.1. Randomly select (without replacement) an activity from the children of the activity","261":"[RB.1.5]3.3. Apply the appropriate Activity Progress Rollup Process to the activity (R.S.:Apply the Activity Progress Rollup Process to the activity)","1090":"[NB.2.1]4. Case: navigation request is Previous","1712":"Closing Sco","1534":"[RB.1.2 a]3.1.2.2. Else","1609":"[NB.2.1]4.2.2. Else","831":"[SB.2.9]8.2. If the activity list is Empty Then (Nothing to choose)","472":"[SB.2.9.2]2.2. If the activity is the first activity in the activity's parent's list of Available Children Then","267":"[RB.1.4] 1.2.9.1.Exit RollupRule Check Subprocess (Evaluation: True) (Stop at the first rule that evaluates to true - perform the associated action)","1102":"[RB.1.1]6. If target objective is Defined Then","1332":"Generating Suspend All Nav Request","764":"[TB.2.2]3. If the Sequencing Rules Check Process does not return Nil Then","894":"[SR.1]5.1. Exit Select Children Process (Undefined behavior)","361":"[SB.2.9]8. Case: Current Activity and the target activity are siblings (Case #2 - same cluster; move toward the target activity)","1379":"Current Activity is not defined","1217":"[TB.2.1]2. Initialize exit target to Null","56":"[NB.2.1]7.1.1.4.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit; Sequencing Request: Choice; Target Activity: the activity specified by the Choice navigation request; Exception: n/a) ","502":"[SB.2.8]1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)","1103":"[RB.1.3 a]3. initialize all incomplete to true","197":"[SB.2.9]11.4.1.1. If the Sequencing Control Choice Exit for the activity is False Then - Make sure an activity that should not exit will exit if the target is delivered","273":"[TB.2.3]5.4.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-5) (Nothing to suspend)","373":"[RB.1.3 a]4.1. If the Attempt Completion Amount Status is False Then (No progress amount known, so the status is unreliable.)","1333":"Choice Flow Subprocess [SB.2.9.1](","720":"[EPNR] 2.2.1 Set the possible navigations that result in sequencing to false","302":"[RB.1.2 a]3.1.1. If the Objective Measure Status for the target objective is False Then (No Measure known, so objective status is unreliable)","171":"[SR.1]2. If Activity is Suspended for the activity is True Or the Activity is Active for the activity is True Then (Cannot apply selection to a suspended or active activity)","1512":" has suspended children.","305":"[SB.2.9]11.8. If the target activity is forward in the activity tree relative to the Current Activity Then (Walk toward the target activity)","1551":"Tearing down sequencer","1493":"Element is: objectives.id","558":"[SB.2.9]11.5.4. Set activity to consider to the activity identified by the Choice FlowSubprocess","921":"[RB.1.2 c]2.1.1. Set the target objective to the objective","1707":"StoreValue (","99":"[NB.2.1]7.1.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-10) (Violates control mode)","1494":"[RB.1.4]1.2.2.2.2.5. Else","565":"ERROR - Server side error occurred when saving state data, HTTP response not 200 (OK). Status: ","1022":"[SB.2.9]3.\tFor each activity in the activity path\t","1414":"Element is: interactions.n.id","1568":"RunTimeApi_IsValidUrn","1195":"Element is: comments_from_learner.location","32":"[SB.2.2]6.2.1. Exit Flow Activity Traversal Subprocess (Deliverable: False; Next Activity: the activity; End Sequencing Session: as identified by the Flow Tree Traversal Subprocess; Exception: exception identified by the Flow Tree Traversal Subprocess)","713":"[SB.2.9.1]2.1. Exit Choice Flow Subprocess (Identified Activity the activity)","832":"[SB.2.9]9.2. If the activity list is Empty Then (Nothing to choose)","1117":"[EPNR] 3.2.2 Run the Delivery Request Process","1334":"Element is: objectives.n.score.raw","1163":" (and all its descendents) will be skipped.","666":"[SB.2.12]6.2. If the Previous Sequencing Request Process returns an exception Then","18":"[SB.2.3]4.3.Exit Flow Subprocess (Identified Activity: the activity identified by the Flow Activity Traversal Subprocess; Deliverable: as identified by the Flow Activity Traversal Subprocess; Exception: exception identified by the Flow Activity Traversal Subprocess)","794":"[EPNR] Evaluate all precondition rules that could affect the activity.","409":"[EPNR] 5.2 Overriding continue status based on 3rd Edition GUI requirements, parent's flow = false, continue is disabled","1023":"Pretest satisfied, marking all activities complete","1673":"New Total Time: ","30":"[SB.2.2]6.3.2.1. Apply the Flow Activity Traversal Subprocess to the activity identified by the Flow Tree Traversal Subprocess in the traversal direction and a previous traversal direction of n/a (Recursive call \u2013 Make sure the identified activity is OK)","934":"[SB.2.12]7.1. Apply the Choice Sequencing Request Process","1024":"[EPNR] Using the 'Quick' Lookahead Sequencing Mode","231":"[DB.2]3. Apply the Terminate Descendent Attempts Process to the activity identified for delivery (Make sure that all attempts that should end are terminated)","1196":"Element is: comments_from_learner_children","1296":"[RB.1.1 b]2. Set valid data to False","115":"[RB.1.5]3.2. Apply the appropriate Objective Rollup Process to the activity (Apply the appropriate behavior described in section RB.1.2, based on the activity's defined sequencing information)","345":"[SB.2.9]12.1. Form the activity path as the ordered series of activities from the Current Activity to the common ancestor, inclusive","138":"[SB.2.9]10.3.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Choice Activity Traversal Subprocess) (Nothing to deliver)","491":"[RB.1.1]6.3.2. Set the Objective Normalized Measure for the target objective to the total weighted measure (","1091":"[DB.2.1]1.3.1.1. If the activity is a leaf Then","781":"[UP.5]2. If the Sequencing Rules Check Process does not return Nil Then","335":"[RB.1.3]1. Apply the Rollup Rule Check Subprocess to the activity and the Incomplete rollup action Process all Incomplete rules first","227":"[SB.2.7]4.1. Exit Continue Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Flow Subprocess) (Nothing to deliver)","714":"[EPNR] 3.2.2 Set will succed to the results of the delivery request process (","1415":"Element is: objectives.status","588":"[TB.2.2]3.1. If the Sequencing Rules Check Process returned Retry, Continue, Or Previous Then","6":"[SB.2.3]2. Apply the Flow Tree Traversal Subprocess to the candidate activity in the traversal direction and a previous traversal direction of n/a with consider children equal to the consider children flag (Attempt to move away from the starting activity, one activity in the specified direction)","139":"[SB.2.9.2]1.2.1.  Apply the Choice Flow Tree Traversal Subprocess to the activity's parent in the Forward direction (Recursion - Move to the activity's parent's next forward sibling)","1118":"[SB.2.12]4. Case: sequencing request is Retry","1144":"[RB.1.4]1.2.9. If status change is True Then","232":"[SB.2.5]3.2.1. Exit Start Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Flow Subprocess) (Nothing to deliver)","1270":"Element is: interactions.n.description","1335":"Element is: adl nav request choice","553":"[RB.1.5]3.1. If the activity has children Then (Only apply Measure Rollup to non-leaf activities)","172":"[NB.2.1]8.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-2) ","804":"[RB.1.2 b]4.4. If the Rollup Rule Check Subprocess returned True Then","54":"[TB.2.3]3.6. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: is the sequencing request returned by the Sequencing Post Condition Rule Subprocess, if one exists, otherwise n/a; Exception: n/a)","1687":"GetDiagnostic('","80":"[SB.2.12]4.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Retry Sequencing Request Process)","1104":"[TB.2.3]3.3.1. Set the processed exit to False","1314":"GetSequencingControlChoice = false.","579":"[SB.2.9]12.4. For each activity on the activity path (Walk up the tree to the common ancestor)","530":"[SB.2.9]12.5.5.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-8)","667":"[DB.1.1]5. Exit Delivery Request Process (Delivery Request: Valid; Exception: n/a)","192":"[NB.2.1]3.2.2.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Continue; Target Activity: n/a; Exception: n/a)","1416":"[RB.1.3 a]7. If All Satisfied","336":"[TB.2.2]1. If Activity is Suspended for the Current Activity is True Then (Do not apply post condition rules to a suspended activity)","1513":"Element is: suspend_data","1454":"Checking for GetValue Error","752":"[SB.2.9]4.If the target activity is not the root of the activity tree Then","379":"[SB.2.1]4.3.1. If the activity's list of Available Children is Not Empty Then (Make sure this activity has a child activity)","1366":"Element is: scaled_passing_score","1697":" are siblings.","840":"Setting WillNeverSucceed = true on all child activities. (Count = ","1435":"Element is: adl.data.n.store","73":"[SB.2.12]5.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Continue Sequencing Request Process)","95":"[NB.2.1]7.1.1.2.3.1. For each activity in the activity path (Make sure that 'choosing' the target will not force an active activity to terminate, if that activity does not allow choice to terminate it)","1164":"[NB.2.1]8. Case: navigation request is Exit","1713":"Activities ","1455":"Element is: adl.nav.request","295":"[UP.4]1.1.1.2.  If the Objective Set by Content for the activity is False Then (Should the sequencer set the objective status of the activity?)","1145":"Content Delivery Environment Process [DB.2](","1632":"Control Initialize","96":"[NB.2.1]13. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-13) (Undefined navigation request)","541":"[RB.1.4]1.2.4.1. If the contributing children bag does not contain a value of False Or Unknown Then","1047":"[UP.3]3.1. For each activity in the activity path","559":"[SB.2.4]1.2.1. Exit Choice Activity Traversal Subprocess (Reachable: False; Exception: SB.2.4-1)","1218":"Element is: comments_from_lms.n.timestamp","1610":"[RB.1.3 a]4.2. Else","902":"[RB.1.4]1.2.4. Case: the contributing children bag is empty","255":"[SB.2.9]15. If the Flow Subprocess returns False Then (Nothing to deliver, but we succeeded in reaching the target activity - move the current activity)","646":"[RB.1.1]6.1.1.2.1. If Objective Contributes to Rollup for the objective is True Then","198":"[SB.2.9]11.3.1.1. If the Sequencing Control Choice Exit for the activity is False Then (Make sure an activity that should not exit will exit if the target is delivered)","973":"Element is: interactions.n.correct_responses.n.pattern","1552":"Suspended Activity is ","1436":"Element is: audio captioning","1688":"[EPNR] Disable ","290":"[SB.2.9]11. Case: Target activity is the common ancestor of the Current Activity (Case #4 - path to the target is backward in the activity tree)","1197":"Element is: objectives.n.completion_status","656":"[RB.1.2 b]3.2.1. Set the Objective Progress Status for the target objective to True","394":"[SB.2.9]3.1.1.1.\tExit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-2)\t(Nothing to deliver.)","595":"[SB.2.9]11.4.1.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: ","1611":"[DB.2]5.1.1.2. Else","739":"[SB.2.9]12.4.2.1. If adlseq:constrainedChoice for the activity is True Then","1350":"In SendDataToServer, synchronous=","1722":"Commit('","449":"Invoking ScoHasTerminatedSoUnload from Terminate, scheduled for 150 ms. Control.IsThereAPendingNavigationRequest() = ","159":"[TB.2.3]5.7. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: Exit; Exception: n/a) Inform the sequencer that the sequencing session has ended","621":"[TB.2.2]3.2. If the Sequencing Rules Check Process returned Exit Parent Or Exit All Then","0":"[SB.2.3]4.3.Exit Flow Subprocess (Identified Activity: the activity identified by the Flow Activity Traversal Subprocess; Deliverable: as identified by the Flow Activity Traversal Subprocess;End Sequencing Session: value identified by the Flow Activity Traversal Subprocess; Exception: exception identified by the Flow Activity Traversal Subprocess)","589":"[DB.1.1]1.1. Exit Delivery Request Process (Delivery Request: Not Valid; Exception: DB.1.1-1)","922":"[SB.2.5]2.5.2. Loop to find the first deliverable activity","885":"[RB.1.1]6.1.1.2.1.1. Set rolled-up objective to the objective","1165":"Element is: comments from learner.timestamp","358":"[DB.2]5.1.1.2.2. If Activity Attempt Count for the activity is equal to One (1) Then (Is this the first attempt on the activity?)","1025":"Objective Rollup Using Measure Process [RB.1.2 a](","1026":" (and all of its descendents) should be disabled. ","1674":"[SB.2.9]15. Else","805":"[RB.1.2 b]3.2. If the Rollup Rule Check Subprocess returned True Then","368":"[OP.1]1.3. If there is a termination request Then (If the current activity is active, end the attempt on the current activity)","1119":"Check Child for Rollup Subprocess [RB.1.4.2](","505":"[NB.2.1]10.1. If the Current Activity is Defined Then (Make sure the sequencing session has already begun)","2":"[NB.2.1]7.1.1. If the activity specified by the Choice navigation request is the root of the activity tree Or the Sequencing Control Choice for the parent of the activity specified by the Choice navigation request is True Then (Validate that a 'choice' sequencing request can be processed on the target activity)","93":"[RB.1.4.2]3.1.2.1. If Activity Progress Status for the activity is False Or (Activity Attempt Count for the activity is greater than (>) Zero (0) And Activity is Suspended for the activity is True) Then","1535":"; Traversal Direction: ","1581":"[SB.2.9]12.5.2. Else","57":"[SB.2.6]3.Exit Resume All Sequencing Request Process (Delivery Request: the activity identified by the Suspended Activity; Exception: n/a) (The Delivery Request Process validates that the Suspended Activity can be delivered)","1380":"Element is: interactions._count","851":"[RB.1.3]4. If the Rollup Rule Check Subprocess returned True Then","715":", had reached its attempt limit and cannot be delivered. Disable next button.","1437":"Checking for Terminate Error","806":"[RB.1.3 b]3. If the Rollup Rule Check Subprocess returned True Then  ","410":"[SB.2.9]12.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)","1198":"Control Recieved Exit All Request From GUI","1336":"Communications_KillPostDataProcess","104":"[RB.1.4.2]2.1.3.1. If (the Rollup Action is Satisfied And adlseq:requiredForSatisfied is ifAttempted) Or (the Rollup Action is Not Satisfied And adlseq:requiredForNotSatisfied is ifAttempted) Then","1417":"Element is: comments from lms","605":"[DB.2.1]1. If the Suspended Activity is Defined Then Make sure there is something to clear","1367":"Element is: completion threshold","74":"[SB.2.12]6.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Previous Sequencing Request Process)","923":"Activity Progress Rollup Using Default Process [RB.1.3 a](","450":"[NB.2.1]7.1.1.1. If the Current Activity is Not Defined Then (Attempt to start the sequencing session through choice)","312":"[SB.2.9]15.1. Exit Choice Sequencing Request Process (Delivery Request: for the activity identified by the Flow Subprocess; Exception: n/a)","1337":"Element is: interactions.timestamp","160":"[SB.2.2]6.1. Apply the Flow Tree Traversal Subprocess to the activity in the traversal direction and a previous traversal direction of n/a with consider children equal to True","852":"[RB.1.3]1.1 Apply Activity Progress Rollup Using Measure Process ","1633":" ; Exception: n/a)","1286":"Control Recieved Choice Request For '","1199":"Element is: interactions.n.objectives.n.id","853":"[OP.1]1.2.2. Continue Loop - wait for the next navigation request","1634":"LMSGetDiagnostic('","1351":"Launching intermediate page from ","1635":"[EPNR] 3.1.2. Else","40":"[NB.2.1]3.2. If the Current Activity is not the root of the activity tree And the Sequencing Control Flow for the parent of the Current Activity is True Then (Validate that a 'flow' sequencing request can be processed from the current activity)","85":"[SB.2.12]3.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Exit Sequencing Request Process)","1200":"Element is: interactions.objectives._count","602":"[DB.2.1]1.1. Find the common ancestor of the identified activity and the Suspended Activity","535":"[OP.1]1.4.3. If the Sequencing Request Process returned a request to end the sequencing session Then","542":"[RB.1.1]7.Exit Measure Rollup Process No objective contributes to rollup, so we cannot set anything","1514":"RunTimeApi_ValidLanguage","482":"[NB.2.1]3.1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)","935":"[RB.1.3 a]4.2.1.1. Set the Attempt Progress Status True  ","1418":" will Stop Forward Traversal.","424":"[RB.1.4]2. Exit Rollup Rule Check Subprocess (Evaluation: False) No rules evaluated to true - do not perform any action","1007":"[DB.2.1]1.3. If the activity path is Not Empty Then","949":"[SB.2.12]4.1. Apply the Retry Sequencing Request Process","1368":"Element is: scaled passing score","46":"[NB.2.1]7.1.1.3. If Activity is Active for the Current Activity is True And the Sequencing Control Choice Exit for the Current Activity is False Then (The Choice target is a sibling to the Current Activity, check if the Current Activity)","1233":"[RB.1.1]6.2. If valid data is False Then","1515":"Element is: session_time","469":"[RB.1.4.2]3.1.3.2.1.1. Apply the Sequencing Rules Check Process to the activity and its Skipped sequencing rules","1456":"Control MarkPostedDataClean","1660":"[SB.2.9.1]3. Else","807":"[RB.1.2 c]6.5 all not satisfied = all not satisfied AND not satisfied","1582":"WillAlwaysSucceed = ","903":"[RB.1.3 a]4.1.1. Set the Attempt Completion Status to False","1477":"Element is: core._children","1689":"[SB.2.9]7. Else","58":" only allows it immediate siblings to be selected (constrained choice). Only activities that are logically next or previous and their descendents (plus the root) are all valid targets for choice, hiding all other activities.","221":"[SB.2.9]5.1. If the Sequencing Control Mode Choice for the parent of the target activity is False Then (Confirm that control mode allow 'choice' of the target)","1719":"GetValue('","962":"[RB.1.1]6.1.1.3. If rolled-up objective is Defined Then","1731":"null","547":"[SB.2.4]2.1.1.1. Exit Choice Activity Traversal Subprocess (Reachable: False; Exception: SB.2.4-2)","1478":"[RB.1.2 b]2.1.2. Break For","1732":"true","1067":"[RB.1.2]1. Set the target objective to Undefined","1252":"Element is: comments_from_lms.n.comment","936":"SCO requested a Suspend All, setting exit type to suspend","974":"[RB.1.4]1.2.2.1. If Tracked for the child is True Then","833":"[OP.1]1.3.2.2. Continue Loop - wait for the next navigation request","337":"[SB.2.1]4.2.1. If reversed direction is False Then (Only test 'forward only' if we are not going to leave this forward only cluster.)","395":"[SB.2.1]3.3.1. If the activity's list of Available Children is Not Empty Then Make sure this activity has a child activity","1234":" has Sequencing Control Choice = false).","524":"[RB.1.1 b]5.2.1. If counted measures is greater than (>) Zero (0.0) Then (Set the rolled-up progress.)","39":"[OP.1]1.3.3.1. Replace any pending sequencing request by the sequencing request returned by the Termination Request Process (There can only be one pending sequencing request. Use the one returned by the termination request process, if it exists)","515":"[SB.2.9]11.5.2.1. traverse is Backward ('Flow' in a backward direction to see what activity comes next)","1271":"Limit Conditions Check Process [UP.1](","194":"[NB.2.1]8.1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit; Sequencing Request: Exit; Target Activity: n/a) ; Exception: n/a)","1583":"Next entry is resume","402":"[NB.2.1]7.1.1.2. If the activity specified by the Choice navigation request is Not a sibling of the Current Activity Then","411":"[SB.2.9]10.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)","950":"[RB.1.2 b]4.5. Exit Objective Rollup Using Rules Process","1008":"[SB.2.9]12.3. Set constrained activity to Undefined","1352":"Lookahead Sequencer Mode Disabled","1725":"[EPNR] ","268":"[SB.2.1]4.2.3.1. Traverse the tree, reverse preorder, one activity to the previous activity, from the activity's parent's list of Available Children","1536":"CheckForGetValueError (","1690":"[UP.4]2.2. Else","716":"[OP.1]1.4.2.1. Handle the sequencing request exception Behavior not specified","338":"[SB.2.9.1]3.1. Exit Choice Flow Subprocess (Identified Activity the activity identified by the Choice Flow Tree Traversal Subprocess)","1636":"[SB.2.10]3.3. Else","774":"[TB.2.3]3.3.4.1.2. Apply the End Attempt Process to the Current Activity","1287":"Sequencing Request Process [SB.2.12](","412":"[RB.1.4.2]2.1. If the Rollup Objective Satisfied value for the activity is True Then (Test the objective rollup control)","1516":"Element is: learner name","1092":"[RB.1.3 a]1. If activity is not tracked, return","1272":"Element is: interactions.objectives.id","322":"[RB.1.3 b]4. Apply the Rollup Rule Check Subprocess to the activity and the Completed rollup action (Process all Completed rules last.) ","1701":"LMSGetValue('","380":"[TB.2.3]1.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-1)","323":"[SB.2.8]5.1. Exit Previous Sequencing Request Process (Delivery Request: the activity identified by the Flow Subprocess; Exception: n/a)","869":"[EPNR] 2.5.1. Check for disabled and limit condition violations","184":"[NB.2.1]4.1.1.Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-2)","38":" will not succeed. Not initiating SCO unload yet. The EvaluatePossibleNavigationRequests will check to see if the nav request will succeed after re-evaluating all dirty data and if it will succeed, the SCO will be unloaded and it will be invoked.","1438":" would flow to an activity (","1166":"Sequencing Rules Check Subprocess [UP.2.1](","795":"[EPNR] 1. Run the navigation request process for each possible request","1369":"Overall Rollup Process [RB.1.5](","1479":"[RB.1.2 c]2.1.2. Break For","516":"[SB.2.9]10.3.1. Apply the Choice Activity Traversal Subprocess to the activity in the Forward direction","1068":"[SB.2.12]5. Case: sequencing request is Continue","1397":"' represents a valid activity.","206":"[NB.2.1]7.1.1.3. If the Activity is Active for the Current Activity is True Then (If the current activity has not been terminated, terminate the current the activity)","1167":"[RB.1.3]4.5. Rolling up using Default Rules","1235":"Element is: comments_from_lms.n.location","1297":"Element is: interactions.description","548":"[RB.1.4]1.2.7.1. If the contributing children bag does not contain a value of True Or Unknown Then","782":"[OP.1]1.5.1. Apply the Delivery Request Process to the delivery request","222":"[SB.2.9]3.3.1\tExit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-3)\tNothing to deliver. (Cannot choose something that is hidden.)","818":"[TB.2.3]4.1.1. Apply the End Attempt Process to the Current Activity","1495":"[RB.1.4]1.2.2.2.2.3. Else","315":"[SB.2.5]2.5 Rustici Extension - This course has a property that indicates we should always flow to the first SCO, so find it and return it","503":"activity.HasChildActivitiesDeliverableViaFlow = false.  Setting possibleNavRequest.WillNeverSucceed = true.","425":"[SB.2.9]8.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-5) (Nothing to deliver)","473":"[SB.2.1]4.2.2. If the activity is the first activity in the activity's parent's list of Available Children Then","536":") to the rollup condition bag (Add the evaluation of this condition the set of evaluated conditions)","303":"[EPNR] The current activity's parent only allows Forward Traversal. Disable all of the parent's children that are before the active activity.","165":"[NB.2.1]11.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-2) ","426":"[SB.2.1]3.3.2.1. Exit Flow Tree Traversal Subprocess (Next Activity Nil; Traversal Direction: n/a; Exception: SB.2.1-2)","904":"[DB.1.1]4.2. If the Check Activity Process return True Then","740":"[SB.2.9]4.2. If the Sequencing Rules Check Process does not return Nil Then","1236":"[RB.1.1 b]5. If valid data is False Then","113":"[RB.1.3 b]1.1. Apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Completed; and a Rollup Action of Completed (Define the default completed rule)","1419":"Element is: time limit action","1553":"Element is: student id","924":"[RB.1.2 b]3.1.1. Set the target objective to the objective","1105":"[SB.2.12]7. Case: sequencing request is Choice","1273":"[RB.1.3 a]5. for each applicable child","1496":"Checking for Finish Error","1517":"[RB.1.2]2.1.2. Break For","330":"[RB.1.4.2]2.1.3.1.1. If Activity Progress Status for the activity is False Or Activity Attempt Count for the activity is Zero (0) Then","1069":"[SB.2.9]12.7. If the activity path is Empty Then","696":"[OP.1]x. Navigation request is display message, translating to an exit request.","603":"[OP.1]1.3.2. If the Termination Request Process returned termination request Not Valid Then","1146":"Element is: interactions.n.objectives._count","982":"[SB.2.9]12.5. If constrained activity is Defined Then","1398":"Checking for first SCO pretest","317":"[SB.2.5]2. If the root of the activity tree is a leaf Then (Before starting, make sure the activity tree contains more than one activity)","854":" is a forward sibling so it and its descendents will be disabled.","1457":"Element is: lesson location","258":"[TB.2.3]5.2.1. If the Current Activity is not the root of the activity tree Then (Make sure the current activity is not the root of the activity tree)","1518":"Element is: suspend data","808":"[RB.1.1 b]4.1.2. If the Attempt Completion Amount Status is True Then","1219":"Element is: interactions.student_response","262":"[RB.1.4] 1.2.10.1.Exit RollupRule Check Subprocess (Evaluation: True) (Stop at the first rule that evaluates to true - perform the associated action)","721":"[EPNR] 3.1.3. Run the sequencing request process for that navigation request","1147":"[RB.1.4.2]2.1.3.1.1.1. Set included to False","566":"[SB.2.7]2.1.1. Exit Flow Tree Traversal Subprocess (Delivery Request: n/a; Exception: SB.2.7-2)","135":"[SB.2.9.2]2.2.1. Apply the Choice Flow Tree Traversal Subprocess to the activity's parent in the Backward direction (Recursion \u2013 Move to the activity's parent's next backward sibling)","1519":"Element is: learner_name","560":"[SB.2.11]4. Exit Exit Sequencing Request Process (End Sequencing Session: False; Exception: n/a)","250":"[SB.2.9]3.2.\tApply the Sequencing Rules Check Process to the activity and the Hide from Choice sequencing rules\t(Cannot choose something that is hidden.)","331":"[SB.2.9]2.\tForm the activity path as the ordered series of activities from root of the activity tree to the target activity, inclusive","873":"About to save final data upon player exit, final exit calls = ","332":"[RB.1.4.2]3.1.3.1.1. If Activity Progress Status for the activity is False Or Activity Attempt Count for the activity is Zero (0) Then","1148":"[SB.2.12]3. Case: sequencing request is Exit","951":"Missing mastery score or raw score, setting to completed","1009":"[SB.2.9]9.3. For each activity on the activity path","1612":"[TB.2.3]3.3. Repeat","458":"[SB.2.2]1. If Sequencing Control Flow for the parent of the activity is False Then (Confirm that 'flow' is enabled)","413":"[TB.2.1]3.2.1. Set the exit target to the activity (Stop at the first activity that has an exit rule evaluating to true)","841":"[EPNR] 3.2. If the Sequencing Request Process returns an exception","81":"[SB.2.12]1.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Start Sequencing Request Process)","346":"[SB.2.9]10.1. Form the activity path as the ordered series of activities from the Current Activity to the target activity, inclusive","722":" because it has a stop forward traversal precondition rule and is a cluster.","567":"[SR.2]4.1.1.1. Randomly reorder the activities contained in Available Children for the activity","374":"ERROR - LMS was unable to successfully save state date, see the LMS response for specific error information. Server Response=","173":"[NB.2.1]4.2.1.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit; Sequencing Request: Previous; Target Activity: n/a; Exception: n/a)","990":"Adding new Interaction Correct Response at position ","291":") to the rollup condition bag to produce a single combined rule evaluation ('And' or 'Or' set of evaluated conditions, on the rollup definition)","180":"[NB.2.1]4.2.1.2.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: n/a; Sequencing Request: Previous; Target Activity: n/a; Exception: n/a)","1338":" does not have suspended children.","855":"[RB.1.3]2.1. Apply Activity Progress Rollup Using Measure Process","975":"[SB.2.9]12.8.1. For each activity on the activity path","1370":"ERROR - invalid rollup condition","1253":"[SR.1]2.1. Exit Select Children Process","1661":"Element is: audio","1149":"[RB.1.4]1.2.4.1.1. Set status change to True","296":"[RB.1.2 b]3.1. Apply the Rollup Rule Check Subprocess to the activity and the Not Satisfied rollup action Process all Not Satisfied rules first","439":"[SB.2.1]3.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: Nil; Traversal Direction: n/a; Exception: SB.2.1-1)","116":"[DB.2]5.1.1.1. If Activity is Suspended for the activity is True Then (If the previous attempt on the activity ended due to a suspension, clear the suspended state; do not start a new attempt)","1537":"failed to pass logEntry","561":"[SR.1]1. If the activity does not have children Then (Cannot apply selection to a leaf activity)","461":"[RB.1.3 a]5.1. Set the Attempt Progress Status False (Incomplete information, do not evaluate completion status.) ","1339":"RunTimeApi_ValidSequencingResponse","366":"[TB.2.3]1. If the Current Activity is Not Defined Then (If the sequencing session has not begun, there is nothing to terminate)","525":"[EPNR] Run the Termination Request Process To Move Current Runtime Data to Sequencer and Invoke Rollup","1691":"[SB.2.3]4. Else","45":"[SB.2.2]6.3.1. If the traversal direction is Backward And the traversal direction returned by the Flow Tree Traversal Subprocess is Forward Then (Check if we are flowing backward through a forward only cluster - must move forward instead)","1569":"[NB.2.1]4.2.1.2. Else","492":"[TB.2.3]3.3.4.1.3. Set processed exit to True (Need to evaluate post conditions on the new current activity)","676":"), but Progress Measure is unknown, therefore CompletionStatus is unknown as well","233":"[SB.2.2]3.3.3. Exit Flow Activity Traversal Subprocess -(Return the results of the recursive Flow Activity Traversal Subprocess) Possible exit from recursion","1480":"Element is: success_status","359":"[SB.2.2]3. If the Sequencing Rules Check Process does not return Nil Then (Activity is skipped, try to go to the 'next' activity)","381":"[TB.2.3]2.1. Exit Termination Request Process (Termination Request: Not Valid; Sequencing Request: n/a; Exception: TB.2.3-2)","783":"[RB.1.5]3.3. Apply the Activity Progress Rollup Process to the activity","274":"[TB.2.3]5.3. Form the activity path as the ordered series of all activities from the Suspended Activity to the root of the activity tree, inclusive","819":"Sco is completed so resetting credit to no-credit and mode to review","647":"[SB.2.9]14.1. Apply the Terminate Descendent Attempts Process to the common ancestor","1538":"Element is: launch data","937":"[RB.1.4]1.2.6. Case: the Rollup Child Activity Set is Any","459":"[TB.2.3]6.2. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: n/a; Exception: n/a)","874":"[DB.1.1]1. If the activity specified by the delivery request (","12":"NB.2.1]4.2.1. If the Sequencing Control Flow for the parent of the Current Activity is True And the Sequencing Control Forward Only for the parent of the Current Activity is False Then (Validate that a 'flow' sequencing request can be processed from the current activity)","1027":"[TB.2.3]4.5.2 Otherwise an Exit sequencing request","1048":"[RB.1.2 b]1.Set the target objective to Undefined","388":"[RB.1.4.1]1.Initialize rollup condition bag as an Empty collection (This is used track of the evaluation rule's conditions)","133":"[SB.2.9]11.8.1.2.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: the exception identified by the Choice Activity Traversal Subprocess) (Nothing to deliver)","3":"[TB.2.3]4.5. Exit Termination Request Process (Termination Request: Valid; Sequencing Request: is the sequencing request returned by the Sequencing Post Condition Rule Subprocess, if one exists, otherwise an Exit sequencing request; Exception: n/a) Inform the sequencer that the sequencing session has ended","362":"[SB.2.9]9. Case: Current Activity and the target activity are siblings (Case #2 - same cluster; move toward the target activity)","1539":" is after the activity ","638":"[SB.2.2]3.2. If the Flow Tree Traversal Subprocess does not identify an activity Then","1028":"Element is: interactions.correct_responses.pattern","1726":"[EPNR]","1714":"Terminate('","1120":"Control Evaluate Possible Navigation Requests","199":"[SB.2.9]12.4.1.1. If the Sequencing Control Choice Exit for the activity is False Then - Make sure an activity that should not exit will exit if the target is delivered","1662":"[RB.1.3 a]5. Else","687":"[RB.1.4.2]3.1.3.1.1. If Activity Attempt Count for the activity is Zero (0) Then","1399":"[OP.1]1. Navigation Request = ","318":"[SB.2.7]5.1. Exit Continue Sequencing Request Process (Delivery Request: the activity identified by the Flow Subprocess; Exception: n/a )","1150":"[RB.1.4.1]2.2.1. Negate the rollup condition","1675":"Element is: text","703":"[SB.2.12]8.2. If the Jump Sequencing Request Process returns an exception Then","668":"[SB.2.9]10.3.1. If the activity is not the last activity in the activity path Then","283":"[TB.2.3]3.3.4.1. If the Current Activity is Not the root of the activity tree Then (The root of the activity tree does not have a parent to exit)","174":"[NB.2.1]10.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-2)","347":"[RB.1.1]5.1. If Objective Contributes to Rollup for the objective is True Then (Find the target objective for the rolled-up measure)","1049":"[RB.1.3 a]5.1.2 attempted = activity.getattempted","228":"Skipping ContentDeliveryEnvironmentActivityDataSubProcess because content is LAUNCH AFTER CLICK.  This method will get called when activity is actually viewed","363":" will be disabled because it is a cluster that does not allow flow navigation and thus its children must be selected explicitly.","414":"[SB.2.10]3.2.1. Exit Retry Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.10-3) (Nothing to deliver)","905":"[TB.2.3]3.3.3.1. Change the termination request to Exit All","78":"[SB.2.12]7.2.1. Exit Sequencing Request Process (Sequencing Request: Not Valid; Delivery Request: n/a; End Sequencing Session: n/a; Exception: the exception identified by the Choice Sequencing Request Process)","1721":"Credit = ","863":"[SR.2]4.1.1. If Randomize Children for the activity is True Then","517":"[SB.2.9]12.5.2.1. traverse is Backward ('Flow' in a backward direction to see what activity comes next)","925":"[RB.1.4]1.2.7. Case: the Rollup Child Activity Set is None","704":"[SB.2.12]3.2. If the Exit Sequencing Request Process returns an exception Then","339":"[SR.1]4.1.1.2.2. Add the selected activity to the child list, retaining the original (from the activity) relative order of activities","1029":") that does not permit flow navigation, disabling.","622":"[RB.1.2 b]3. Get the primary objective (For each objective associated with the activity)","229":"[SB.2.1]4.2.2.2. Exit Flow Tree Traversal Subprocess (Return the results of the recursive Flow Tree Traversal Subprocess) (Return the result of the recursion)","1400":"[RB.1.1]6.1.1.2.1.2. Break For","765":"[SB.2.1]4.2. If the activity is a leaf Or consider children is False Then","306":"[TB.2.1]3. For each activity in the activity path (Evaluate all exit rules along the active path, starting at the root of the activity tree)","130":"[RB.1.2 a]3.1.2.1. If Activity is Active for the activity is False Or (Activity is Active for the activity is True And adlseq:measureSatisfactionIfActive for the activity is True ) Then","526":"[SB.2.9]9.3.1. Apply the Choice Activity Traversal Subprocess to the activity in the Forward direction","1070":"[SB.2.9]11.2. If the activity path is Empty Then","87":"[NB.2.1]7.1.1.2.3.1.1.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-8) (Violates control mode)","504":"[SB.2.7]1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)","415":"[UP.2]1.2.1. Apply the Sequencing Rule Check Subprocess to the activity and the rule (Evaluate each rule, one at a time)","234":"[DB.2]6. The activity identified for delivery becomes the current activity Set Current Activity to the activity identified for delivery. Identified Activity=","241":"[NB.2.1]4.2. If the Current Activity is not the root of the activity tree Then (There is no activity logically 'previous' to the root of the activity tree)","403":"[DB.2.1]1.3.1.2.1. If the activity does not include any child activity whose Activity is Suspended attribute is True Then","625":"[EPNR] 3.1 If there is a sequencing request returned by the termination request process","382":"[SB.2.9]12.8.1.3.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-6) (Nothing to deliver)","568":"[DB.1.1]4.2.1. Exit Delivery Request Process (Delivery Request: Not Valid; Exception: DB.1.1-3)","580":"[SB.2.9]11.8.1.3.1. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: ","75":"[NB.2.1]8.1.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-12) (Activity has already terminated )","245":"[SB.2.9]10.1. Form the activity path as the ordered series of activities from the common ancestor to the target activity, exclusive of the target activity","657":"[OP.1]1.5.3. Apply the Content Delivery Environment Process to the delivery request","895":"[RB.1.5]2.1. Exit Overall Rollup Process - Nothing to rollup","1010":"[SB.2.9]9.5. For each activity on the activity list","1613":"[RB.1.1 b]5.2. Else","741":"[SB.2.9]3.3.\tIf the Sequencing Rules Check Process does not return Nil Then","383":"[TB.2.3]3.1. Apply the End Attempt Process to the Current Activity  (Ensure the state of the current activity is up to date)","483":"[NB.2.1]4.1. If the Current Activity is Not Defined Then (Make sure the sequencing session has already begun)","1340":"[SB.2.9]9.3.1. traverse is Forward","1011":"[DB.2.1]2. Exit Clear Suspended Activity Subprocess","639":"[RB.1.2 b]4.2.2. Set the Objective Satisfied Status for the target objective to False","1401":"RunTimeApi_ValidLikeRTResponse","1201":"[RB.1.4.2]2.1.2.1.1. Set included to False","1723":"Score = ","1381":"[EPNR] The current activity is ","22":"[SB.2.2]6.3.1.1. Apply the Flow Activity Traversal Subprocess to the activity identified by the Flow Tree Traversal Subprocess in the Forward direction with the previous traversal direction of Backward (Recursive call \u2013 Make sure the identified activity is OK)","1237":"Flow Tree Traversal Subprocess [SB.2.1](","1663":"Cloning sequencer","1458":", Objective Measure Weight=","275":"[NB.2.1]9.1. If the Current Activity is Defined Then (If the sequencing session has already begun, unconditionally terminate all active activities)","614":"[SB.2.4]1.3. Exit Choice Activity Traversal Subprocess (Reachable: True; Exception: n/a )","896":"[RB.1.2 c]2. For each objective associated with the activity","33":"[RB.1.3 b]1. If the activity does not include Rollup Rules with the Incomplete rollup action And the activity does not include Rollup Rules with the Completed  rollup action Then (If no progress rollup rules are defined, use the default rollup rules.) ","809":"[RB.1.3]4.1. Set the Attempt Progress Status for the activity to True","926":"[SB.2.9]13. If the target activity is a leaf activity Then","1274":"[RB.1.2 c]6. for each applicable child","1420":"Checking for Initialize Error","1202":"Checking for valid choice/jump nav request","1664":"[RB.1.2 b]5. Else","742":"[SB.2.9]11.4.2.1. If adlseq:constrainedChoice for the activity is True Then","154":"[NB.2.1]10.1.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-12)","1459":"Clearing Suspended Activity","1497":"Element is: core._version","147":"[NB.2.1]7.1.1.2.4.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: NB.2.1-9)","1288":" is currently active, stop disabling.","1050":"[RB.1.2 c]6.1.2 attempted = activity.getattempted","235":"[SB.2.9]11.8.1.1. Apply the Choice Activity Traversal Subprocess to the activity in the Forward direction (to check for Stop Forward Traversal violations) i=","1614":"[TB.2.3]5.2.2. Else","628":"[RB.1.2 a]3.1.1.1. Set the Objective Progress Status for the target objective to False","155":"[RB.1.2]2.1. If Objective Contributes to Rollup for the objective is True Then (Identify the objective that may be altered based on the activity's children's rolled up measure)","1121":"Completion Measure Rollup Process [RB.1.1 b](","1353":"[RB.1.3 a]5.1 if child activity (","1439":"Element is: max_time_allowed","688":"[RB.1.1]6.3.1. Set the Objective Measure Status for the target objective to True","1637":"[SB.2.12]6.3. Else","175":"[NB.2.1]7.1.1.2.3.1.1.1. Exit Navigation Request Process (Navigation Request: Not Valid; Termination Request: n/a; Sequencing Request: n/a; Target Activity: n/a; Exception: ","886":"[SR.1]3. Case: the Selection Timing for the activity is Never","82":"[NB.2.1]13.2.1. Exit Navigation Request Process (Navigation Request: Not Valid; Sequencing Request: n/a; Termination Request: n/a; Target Activity: n/a; Exception: NB.2.1-11) (Target activity does not exist.)","427":"[SB.2.7]1.1. Exit Continue Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.7-1) (Nothing to deliver)","1554":"[RB.1.1 b]5.2.2. Else ","1298":"Checking to see if the nav request (","596":"[SB.2.8]2.1.1. Exit Flow Tree Traversal Subprocess (Next Activity: Nil; Exception: SB.2.8-2)","1570":"[NB.2.1]7.1.1.4. Else","185":"[NB.2.1]3.2.1.1. Exit Navigation Request Process (Navigation Request: Valid; Termination Request: Exit; Sequencing Request: Continue; Target Activity: n/a; Exception: n/a)","938":"[NB.2.1]7.1.1.2.3. If the activity path is Not Empty Then","1012":"[UP.3]4. Exit Terminate Descendent Attempts Process","428":"UP.1]10. Exit Limit Conditions Check Process (Limit Condition Violated: False) (No limit conditions have been violated)","939":"is the root, therefore the common ancestor of activities ","1382":"RunTimeApi_ValidShortIdentifier","263":"[SB.2.9]3.1.1.\tIf the Available Children for the parent of the activity does not contain the activity Then\t(The activity is currently not available.)","493":"[EPNR] There is either no previous activity or all previous activities are skipped, disable previous button.","478":"[RB.1.1]6.3.1.2. Set the Objective Normalized Measure for the target objective to the total weighted measure (","976":"[SB.2.9]12.9.1. For each activity on the activity path","1460":"Element is: lesson_location","440":"[SB.2.9]14.4. Exit Choice Sequencing Request Process (Delivery Request: n/a; Exception: SB.2.9-9) (Nothing to deliver)","518":"[SB.2.9]9.5.1. Apply the Choice Activity Traversal Subprocess to the activity in the traverse direction","1715":"Max Score: ","128":"[RB.1.2 a]3.1. If Objective Satisfied by Measure for the target objective is True Then (If the objective is satisfied by measure, test the rolled-up measure against the defined threshold)","1676":"Element is: mode","743":"[EPNR] 1.2. If the navigation request succeeds, set its WillSucceed to true"};

/***********
Assignment Hack!
SCORM has this strange limitation on the length of a learner response. 
The learner response is first limited to 4096 characters across all response types. Not sure why.
Secondly, the limitation for the SCORM_FILL_IN interaction is 255 characters.
So, when we exceed either of these limits, it will quietly fail and stick NULL in the db. 

There is a SCORM_LONG_FILL_IN type, however, this is only available in SCORM 2004, we are using 1.2

The database field the learner response is placed in is an NTEXT field. We should be able to 
store up to 1 GB of data in this field. In this case a 4096 max character limit is silly.

This overrides the IsValidCMIFeedback function from Core/ScormUtilities.js to ignore
the max length restriction.

This way we will be able to store full assignment responses in the interactions table
*********/

function IsValidCMIFeedback(interactionType, value){
	
	//be sure to check length on server
		
    //no response can be greater than 4096 characters
    // WHY? Removing the character limit. Serverside can deal with it if there is an issue
    /*
	if (value.length > 4096){
		//WriteToDebug("WARNING: value is greater than 4096 characters");
		return false;
	}
    */

	if (RegistrationToDeliver.Package.Properties.ValidateInteractionResponses)
	{
		//even though things are technically supposed to be lower case, allow upper case letters to come in
		value = value.toLowerCase();
		
		switch(interactionType){
			
			case "true-false":
				//WriteToDebug("Type is true-false");
				//must start with one of these characters - 0,1,t,f
				if (value.search(/^[01tf]/) !== 0){
					return false;
				}
				break;
				
			case "choice":
			
				//WriteToDebug("Type is choice");
				//comma delimited list of single digit lower case letters or numbers surrounded by optional {}
				if (value.search(/(^(([a-z0-9])|(([a-z0-9]\,)+[a-z0-9]))$)|(^\{(([a-z0-9])|(([a-z0-9]\,)+[a-z0-9]))\}$)/) !== 0){
					return false;
				}
				break;	
				
			case "fill-in":
				//WriteToDebug("Type is fill-in");
			    //any alphanumeric string up to 255 chars
			    // WHY ONLY 255? Removing this restriction
                /*
				if (value.length > 255){
					return false;
				}*/
				break;
				
			case "numeric":
				//WriteToDebug("Type is numeric");
				if (! IsValidCMIDecimal(value)){
					return false;
				}
				break;
				
			case "likert":
				//WriteToDebug("Type is likert");
				
				//single character - can be blank
				//if ( ! ((value.length == 1 && value.search(/^[0-9a-z]$/) === 0) || (value.length === 0))){
				//	return false;
				//}			
				
				//above validation was an error, there are no restrictions on the likert format in SCORM 1.2
				
				break;
				
			case "matching":
				//WriteToDebug("Type is matching");
				if (value.search(/(^[0-9a-z]\.[0-9a-z]$)|(^([0-9a-z]\.[0-9a-z]\,)+([0-9a-z]\.[0-9a-z])$)|(^\{[0-9a-z]\.[0-9a-z]\}$)|(^\{([0-9a-z]\.[0-9a-z]\,)+([0-9a-z]\.[0-9a-z])\}$)/) !== 0){
					return false;
				}				
				break;
				
			case "performance":
				//WriteToDebug("Type is performance");
				if (value.length > 255){
					return false;
				}
				break;
				
			case "sequencing":
				//WriteToDebug("Type is sequencing");
				//comma delimited list of single digit lower case letters or numbers
				if (value.search(/(^[a-z0-9]$)|(^([a-z0-9]\,)+[a-z0-9]$)/) !== 0){
					return false;
				}			
				break;
				
			default:
				//we are unable to determine the type (possibly b/c it hasn't yet been set) so just accept anything
				//we will re-check this value when the type is set
				break;
		}
	}
	
	return true;
}

// Hack number 2!
// Removing a window.close() statement from ExitScormPlayer
// This eventually gets called from LMSFinish. 
// 
// Looks like there must be a setting this.Package.Properties.PlayerLaunchType == LAUNCH_TYPE_FRAMESET
// But for now, rather than muck around trying to get that set properly, lets just remove the
// window.close call.
//
// ALSO!! - For some reason RedirectOnExitUrl points to AutoClose.aspx... which... is just a blank page
// that has window.close() in the body.onload!!!
// 
// WHHHY? I Have noooo idea. 
//
// Killing that too because that's what is actually being hit to close the window. Not the window.close()
// statement. Going to kill them both because we REEALLLY don't want that window to close!
// 
// Buuuuut... This isn't enough since there is a prior call to a redirect to intermediate.aspx.... See hack 3.
function Controller_ExitScormPlayer() {
    //don't need to worry about unloading the sco here...this function shouldn't get called until that has happened

    //TODO: should this be an integration function?
    //TODO: can we always count on the current window being the player window?

    this.ExitScormPlayerCalled = true;

    if (Debug.ShowDebugLogAtExit) {
        if (Debug.DataIsAvailable()) Debug.ShowAllAvailableData();
    }

    //we don't want to redirect the browser at all if it is already unloading
    if (this.ProcessedUnload === false) {

        // Either Unload() or ExitScormPlayer() should call SaveDataOnExit().  If Unload hasn't been called, do it here.
        this.Comm.SaveDataOnExit();

        // Calling this after save data call to capture any potential problems during the communcations postback
        if (SHOULD_SAVE_CLIENT_DEBUG_LOGS && RegistrationToDeliver.TrackingEnabled) {
            this.Comm.SaveDebugLog(true);
        }

        // JUST NO.. NO... DON'T DO ANYTHING! JUST STOP!
        /*
        if (this.Package.Properties.PlayerLaunchType == LAUNCH_TYPE_FRAMESET) {
            window.location = RedirectOnExitUrl;
        }
        else {
            window.opener.location = RedirectOnExitUrl;
            window.close();
        }
        */
    } else {
        if (SHOULD_SAVE_CLIENT_DEBUG_LOGS && RegistrationToDeliver.TrackingEnabled) {
            this.Comm.SaveDebugLog(true);
        }
    }
}


// HACK NUMBER 3
// So this function reaaaaallllly wants to redirect to another page. 
// Symphony reeeeaaallllllly doesn't want that...
// So don't do it.
function ScoLoader_UnloadSco(messageToDisplay) {

    var path = "";

    if (Control.Package.Properties.MakeStudentPrefsGlobalToCourse === true) {
        Control.UpdateGlobalLearnerPrefs()
    }

    if (Control.Package.Properties.RollupRuntimeAtScoUnload === true) {
        window.setTimeout("Control.EvaluatePossibleNavigationRequests(true);", 150);
    }

    if (messageToDisplay === true) {
        path = MergeQueryStringParameters(this.IntermediatePage.PageHref, this.IntermediatePage.Parameters, "MessageWaiting=true");
    }
    else {
        path = MergeQueryStringParameters(this.IntermediatePage.PageHref, this.IntermediatePage.Parameters);
    }

    // ..... NO!
    // Navigation handled by Symphony... Stop this crazyness!
    /*
    if ((this.ScoLaunchType == LAUNCH_TYPE_POPUP ||
		   this.ScoLaunchType == LAUNCH_TYPE_POPUP_AFTER_CLICK) &&
		 this.ContentFrame.CloseSco) {

        Control.WriteDetailedLog("Closing Sco");
        this.ContentFrame.CloseSco();

        Control.WriteDetailedLog("Launching intermediate page from " + path);
        window.setTimeout("Control.ScoLoader.ContentFrame.location = '" + path + "'", 250);
    }
    else {
        Control.WriteDetailedLog("UnLoading Sco and launching intermediate page from " + path);
        this.ContentFrame.location = path;
    }
    */
}

/// Hack number 4!
/// There is potential for scorm to disable itself if the connection is lost. 
/// Allow the user the choice to either wait, or exit the course. 
function Communications_CallFailed() {

    if (this.Disabled) { return false; }

    this.FailedSubmissions++;
    Control.MarkPostedDataDirty();

    Control.WriteDetailedLog("Communications Call Failed, Failed Submissions=" + this.FailedSubmissions);



    if (this.FailedSubmissions >= RegistrationToDeliver.Package.Properties.CommMaxFailedSubmissions) {
        this.KillPostDataProcess();

        if (typeof (this.HandleCallFailed) == 'function') {
            this.HandleCallFailed();
        } else {
            alert("A fatal error has occurred, communication with the server has been lost. This window will now be closed. You can try again when your internet connection is restored.");
            window.top.close();
        };
        

        return false;
    }

    return true;
}

function Communications_SendDataToServer(synchronous, strXmlPostData, isExit) {

    if (this.Disabled) { return true; }


    if (isExit) {
        var resultPostbackPage = MergeQueryStringParameters(SCORM_RESULTS_PAGE, "isExitScormPlayer=true");
    } else {
        var resultPostbackPage = SCORM_RESULTS_PAGE;
    }

    //make sure synchronous is a valid boolean (use the NOT operator twice)
    synchronous = !!synchronous

    Control.WriteDetailedLog("In SendDataToServer, synchronous=" + synchronous + ", strPostData=" + strXmlPostData);

    // A couple notes about the .ajax call below.  Return type is text because although the string is xml, we
    // expect to manually parse the string rather than view it as an xml object.  The content type has to
    // be explicitly set to text/xml or the java-side code won't read the input stream properly
    $.ajax({
        type: 'POST',
        url: resultPostbackPage,
        cache: false,
        dataType: 'text',
        contentType: 'text/xml',
        data: strXmlPostData,
        async: !synchronous,
        success: function (response, textStatus) {
            Control.WriteDetailedLog("Server Responded With:" + response);
            if (typeof (Control.Comm.ClearCallFailed) == 'function') {
                Control.Comm.ClearCallFailed();
            }
            return Control.Comm.CheckServerResponse(response, true);
        },
        error: function (req, textStatus, errorThrown) {
            Control.WriteDetailedLog("ERROR - Server side error occurred when saving state data, HTTP response not 200 (OK). Status: " + req.status);
            var canTryAgain = Control.Comm.CallFailed();
            if (canTryAgain) {
                Control.Comm.SaveData(true);
            }
        }
    });

    return true;
}