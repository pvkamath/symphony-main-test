﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.Reflection;
using SubSonic.Utilities;
using System.Diagnostics;
using SubSonic;
using System.Data;
using System.Configuration;
using Symphony.Core.Controllers;
using log4net;
using log4net.Config;
using System.IO;
using Models = Symphony.Core.Models;
using Symphony.Core.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Reflection;

namespace Symphony.TaskRunner
{
    class Program
    {
        static ILog Log = LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            Log.Info("Acquiring mutex");
            // Mutex handling pattern from: http://stackoverflow.com/questions/229565/what-is-a-good-pattern-for-using-a-global-mutex-in-c/229567
            //
            // get application GUID as defined in AssemblyInfo.cs
            string appGuid = ((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), false).GetValue(0)).Value.ToString();

            // unique id for global mutex - Global prefix means it is global to the machine
            string mutexId = string.Format("Global\\{{{0}}}", appGuid);

            // Need a place to store a return value in Mutex() constructor call
            bool createdNew;

            // edited by Jeremy Wiebe to add example of setting up security for multi-user usage
            // edited by 'Marc' to work also on localized systems (don't use just "Everyone") 
            var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
            var securitySettings = new MutexSecurity();
            securitySettings.AddAccessRule(allowEveryoneRule);

            // edited by MasonGZhwiti to prevent race condition on security settings via VanNguyen
            using (var mutex = new Mutex(false, mutexId, out createdNew, securitySettings))
            {
                // edited by acidzombie24
                var hasHandle = false;
                try
                {
                    try
                    {
                        // note, you may want to time out here instead of waiting forever
                        // edited by acidzombie24
                        hasHandle = mutex.WaitOne(0, false);
                        if (hasHandle == false)
                            throw new TimeoutException("Deployment Runner already in progress. New deployments will automatically be run when complete.");
                    }
                    catch (AbandonedMutexException)
                    {
                        Log.Info("Mutex was previously abandoned.");
                        // Log the fact the mutex was abandoned in another process, it will still get aquired
                        hasHandle = true;
                    }

                    Log.Info("Mutex acquired. Running reployments.");
                    RunDeployments();
                }
                catch (Exception e)
                {
                    Log.Error("Mutex could not be acquired.");
                    Log.Error(e);
                }
                finally
                {
                    Log.Info("Releasing mutex.");
                    // edited by acidzombie24, added if statemnet
                    if (hasHandle)
                        mutex.ReleaseMutex();
                }
            }
        }

        static void RunDeployments()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            try
            {
                Symphony.Core.Controllers.ArtisanController artisanController = new ArtisanController();

                Log.Info("Looking for new queued deployments to run.");
                // Will only ever get one deployment at a time
                List<Task> tasks = artisanController.StartNextQueuedDeployment();

                // Will run one deployment at a time until no deployments are left.
                while (tasks.Count > 0)
                {
                    Log.Info("Found " + tasks.Count + " deployments. Running deployments...");
                    Task.WaitAll(tasks.ToArray());
                    Log.Info("Deployments complete.");
                    Log.Info("Looking for recently added deployments.");
                    // Will grab the next deployment on the list
                    tasks = artisanController.StartNextQueuedDeployment();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if (ex.InnerException != null)
                {
                    Log.Error(ex.InnerException.Message);
                }
                Log.Error(ex);
            }
            sw.Stop();
            Log.Info("No additional deployments found. Deployments complete.");
            Log.Info("Total time: " + sw.Elapsed.Minutes + ":" + sw.Elapsed.Seconds);
        }
    }
}
