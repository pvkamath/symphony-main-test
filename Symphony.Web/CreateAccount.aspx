﻿<%@ Page Title="Symphony Create Account" Language="C#" MasterPageFile="~/Unauthenticated.Master" AutoEventWireup="true"
    CodeBehind="CreateAccount.aspx.cs" Inherits="Symphony.Web.CreateAccount" %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>

<asp:Content ID="Header" ContentPlaceHolderID="Header" runat="server">
    <script type="text/javascript">
        Ext.onReady(function () {
            var w = new Ext.Window({
                contentEl: Ext.get('wrapper'),
                closable: false,
                padding: '15'
            });
            w.show();
        });
    </script>
    <style type="text/css">
        .email {
            width: 320px;
        }

        .success {
            color: Green;
        }

        .error {
            color: Red;
        }
    </style>
</asp:Content>

<asp:Content ID="Body" ContentPlaceHolderID="Body" runat="server">
    <div id="wrapper">
        <url:form id="form1" runat="server">
            <h3>Create account</h3>
            <table>
                <tr>
                    <td>
                        <label for="username">Username:</label></td>
                    <td>
                        <asp:TextBox ID="username" runat="server" CssClass="email" />                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="password">Password</label></td>
                    <td>
                        <asp:TextBox TextMode="Password" ID="password" runat="server" CssClass="email" /></td>
                </tr>
                <tr>
                    <td>
                        <label for="email">Email Address:</label></td>
                    <td>
                        <asp:TextBox ID="email" runat="server" CssClass="email" /></td>
                </tr>
                <tr>
                    <td>
                        <label for="employeeid">Employee ID:</label></td>
                    <td>
                        <asp:TextBox ID="employeeid" runat="server" CssClass="email" /></td>
                </tr>
                <tr>
                    <td>
                        <label for="firstname">Firstname:</label></td>
                    <td>
                        <asp:TextBox ID="firstname" runat="server" CssClass="email" /></td>
                </tr>
                <tr>
                    <td>
                        <label for="lastname">Lastname:</label></td>
                    <td>
                        <asp:TextBox ID="lastname" runat="server" CssClass="email" /></td>
                </tr>
                <tr>
                    <td>
                        <label for="lastname">Job Role:</label></td>
                    <td>
                        <asp:DropDownList ID="jobRoles" runat="server" style="width: 100%">
                            <asp:ListItem Selected="True" Value="Select"> Select a Job Role ... </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="lastname">Location:</label></td>
                    <td>
                        <asp:DropDownList ID="location" runat="server" style="width: 100%">
                            <asp:ListItem Selected="True" Value="Select"> Select a Location ... </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right">
                        <asp:Button Text="Send" runat="server" OnClick="CreateAccountLink" />
                    </td>
                </tr>
            </table>
            <br />
            <div style="text-align: center; width: 100%; margin-top: 10px;">
                <asp:Literal ID="error" runat="server"></asp:Literal>
                <asp:Literal ID="success" runat="server"></asp:Literal>
            </div>
        </url:form>
    </div>
</asp:Content>