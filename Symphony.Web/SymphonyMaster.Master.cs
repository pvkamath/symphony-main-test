﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Symphony.Core.Controllers;
using System.IO;
using log4net;
using Symphony.Core.CNRService;
using Symphony.Core;
using Symphony.Core.Models;
using System.Runtime.Serialization;
using Symphony.Web.HttpHandlers;
using Data = Symphony.Core.Data;
using System.Text;
using System.Reflection;
using Security = System.Web.Security;

namespace Symphony.Web
{
    public partial class SymphonyMaster : System.Web.UI.MasterPage
    {
        private static Dictionary<string, string> _cache = new Dictionary<string, string>();
        private static Dictionary<string, DateTime> _dtCached = new Dictionary<string, DateTime>();
        private static object _enumLock = new object();

        /// <summary>
        /// If in debug mode, return a string to add to ext file name to get the uncompressed version
        /// </summary>
        public string ExtDebug
        {
            get
            {
#if DEBUG
                return "-debug";
#else
                return "";
#endif
            }
        }

        private string _v = "";
        public string AssemblyVersion
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_v))
                {
                    _v = Assembly.GetExecutingAssembly().GetName().Version.ToString(4).Replace(".", "_");
                }
                return _v;
            }
        }

        static string EnumJson { get; set; }

        static SymphonyMaster()
        {
            EnumJson = EnumManager.SerializeToJson();
        }

        SymphonyController c = new SymphonyController();
        ILog Log = LogManager.GetLogger(typeof(_Default));

        [DataContract]
        private class InitialState
        {
            [DataMember(Name = "User")]
            public InitialStateUser User { get; set; }

            [DataMember(Name = "ActualUser")]
            public InitialStateUser ActualUser { get; set; }

            [DataMember(Name = "Portal")]
            public InitialStatePortal Portal { get; set; }

            [DataMember(Name = "Aliases")]
            public InitialStateAliases Aliases { get; set; }

            [DataMember(Name = "Settings")]
            public InitialStateSettings Settings { get; set; }

            [DataMember(Name = "NetTimeZones")]
            public IEnumerable<NetTimeZone> NetTimeZones { get; set; }

            [DataMember(Name = "Modules")]
            public Modules Modules { get; set; }

            [DataMember(Name = "UserCertificateFields")]
            public List<string> UserCertificateFields { get; set; }

            [DataMember(Name = "CustomerDetails")]
            public Customer Customer { get; set; }

            [DataMember(Name ="Qlik")]
            public QlikInfo Qlik { get; set; }
        }

        [DataContract]
        private class QlikInfo
        {
            [DataMember(Name ="environment")]
            public string Environment { get; set; }

            [DataMember(Name ="instance")]
            public string Instance { get; set; }
        }

        [DataContract]
        private class NetTimeZone
        {
            [DataMember(Name = "displayName")]
            public string DisplayName { get; set; }

            [DataMember(Name = "zoneId")]
            public string ZoneId { get; set; }
        }

        [DataContract]
        private class InitialStateUser
        {
            [DataMember(Name = "id")]
            public int Id { get; set; }

            [DataMember(Name = "customerId")]
            public int CustomerId { get; set; }

            [DataMember(Name = "customerDomain")]
            public string CustomerDomain { get; set; }

            [DataMember(Name = "customerNewHireTransitionPeriod")]
            public int CustomerNewHireTransitionPeriod { get; set; }

            [DataMember(Name = "fullName")]
            public string FullName { get; set; }

            [DataMember(Name = "isGtmOrganizer")]
            public bool IsGtmOrganizer { get; set; }

            [DataMember(Name = "isGtwOrganizer")]
            public bool IsGtwOrganizer { get; set; }

            [DataMember(Name = "isClassroomInstructor")]
            public bool IsClassroomInstructor { get; set; }

            [DataMember(Name = "isClassroomManager")]
            public bool IsClassroomManager { get; set; }

            [DataMember(Name = "isClassroomResourceManager")]
            public bool IsClassroomResourceManager { get; set; }

            [DataMember(Name = "isClassroomSupervisor")]
            public bool IsClassroomSupervisor { get; set; }

            [DataMember(Name = "isSalesChannelAdmin")]
            public bool IsSalesChannelAdmin { get; set; }

            [DataMember(Name = "isCustomerManager")]
            public bool IsCustomerManager { get; set; }

            [DataMember(Name = "isCustomerJobRoleManager")]
            public bool IsCustomerJobRoleManager { get; set; }

            [DataMember(Name = "isCustomerLocationManager")]
            public bool IsCustomerLocationManager { get; set; }

            [DataMember(Name = "isCustomerAdministrator")]
            public bool IsCustomerAdministrator { get; set; }

            [DataMember(Name = "isTrainingAdministrator")]
            public bool IsTrainingAdministrator { get; set; }

            [DataMember(Name = "isTrainingManager")]
            public bool IsTrainingManager { get; set; }

            [DataMember(Name = "isArtisanAuthor")]
            public bool IsArtisanAuthor { get; set; }

            [DataMember(Name = "isArtisanPackager")]
            public bool IsArtisanPackager { get; set; }

            [DataMember(Name = "isArtisanViewer")]
            public bool IsArtisanViewer { get; set; }

            [DataMember(Name = "isReportingAnalyst")]
            public bool IsReportingAnalyst { get; set; }

            [DataMember(Name = "isReportingLocationAnalyst")]
            public bool IsReportingLocationAnalyst { get; set; }

            [DataMember(Name = "isReportingJobRoleAnalyst")]
            public bool IsReportingJobRoleAnalyst { get; set; }

            [DataMember(Name = "isReportingDeveloper")]
            public bool IsReportingDeveloper { get; set; }

            [DataMember(Name = "isReportingSupervisor")]
            public bool IsReportingSupervisor { get; set; }

            [DataMember(Name = "isSuperUser")]
            public bool IsSuperUser { get; set; }

            [DataMember(Name = "cnrToken")]
            public string CNRToken { get; set; }

            [DataMember(Name = "canDeployMultiple")]
            public bool CanDeployMultiple { get; set; }

            [DataMember(Name = "canChangePassword")]
            public bool CanChangePassword { get; set; }

            [DataMember(Name = "hasPasswordMask")]
            public bool HasPasswordMask { get; set; }

            [DataMember(Name = "allowSelfAccountCreation")]
            public bool AllowSelfAccountCreation { get; set; }

            [DataMember(Name = "hasValidationParameters")]
            public bool HasValidationParameters { get; set; }

            [DataMember(Name = "isUserValidationParametersRequired")]
            public bool IsUserValidtionParametersRequired { get; set; }

            [DataMember(Name = "hasNmlsNumber")]
            public bool HasNmlsNumber { get; set; }

            [DataMember(Name = "onlineStatus")]
            public int? OnlineStatus { get; set; }

            [DataMember(Name = "enforcePasswordReset")]
            public bool EnforcePasswordReset { get; set; }

            [DataMember(Name = "isPasswordResetRequired")]
            public bool IsPasswordResetRequired { get; set; }

        }

        [DataContract]
        private class InitialStatePortal
        {
            [DataMember(Name = "launchPageBase")]
            public string LaunchBasePage { get; set; }

            [DataMember(Name = "artisanUrl")]
            public string ArtisanUrl { get; set; }

            [DataMember(Name = "reportingLaunchUrl")]
            public string ReportingLaunchUrl { get; set; }

            [DataMember(Name = "reportingLoginUrl")]
            public string ReportingLoginUrl { get; set; }

            [DataMember(Name = "reportingUrl")]
            public string ReportingUrl { get; set; }

            [DataMember(Name = "reportingDefaultUrl")]
            public string ReportingDefaultUrl { get; set; }

            [DataMember(Name = "ssoEnabled")]
            public bool SsoEnabled { get; set; }

            [DataMember(Name = "ssoTimeoutRedirect")]
            public string SsoTimeoutRedirect { get; set; }

            [DataMember(Name = "helpUrl")]
            public string HelpUrl { get; set; }

            [DataMember(Name = "portalSettings")]
            public List<PortalSetting> PortalSettings { get; set; }
        }

        [DataContract]
        private class InitialStateAliases
        {
            [DataMember(Name = "jobRole")]
            public string JobRole { get; set; }

            [DataMember(Name = "jobRoles")]
            public string JobRoles { get; set; }

            [DataMember(Name = "location")]
            public string Location { get; set; }

            [DataMember(Name = "locations")]
            public string Locations { get; set; }

            [DataMember(Name = "audience")]
            public string Audience { get; set; }

            [DataMember(Name = "audiences")]
            public string Audiences { get; set; }

            [DataMember(Name = "username")]
            public string Username { get; set; }

            [DataMember(Name = "course")]
            public string Course { get; set; }

            [DataMember(Name = "courses")]
            public string Courses { get; set; }
        }

        [DataContract]
        private class InitialStateSettings
        {
            [DataMember(Name = "timeout")]
            public int Timeout { get; set; }

            [DataMember(Name = "modules")]
            public string Modules { get; set; }

            [DataMember(Name = "sessionId")]
            public string SessionId { get; set; }

            [DataMember(Name = "aspxauth")]
            public string Aspxauth { get; set; }

            /// <summary>
            /// Temporary flag to allow toggling server side ui logic. 
            /// Can be removed after fully tested. 
            /// This only includes the new fields within the models
            /// as part of the returned data. Does not use these fields
            /// client side.
            /// </summary>
            [DataMember(Name = "isServerSideUiLogic")]
            public bool IsServerSideUiLogic { get; set; }
            /// <summary>
            /// Temporary flag to allow toggling server side ui logic logging. 
            /// Displays detailed logs in console about any deprecated ui functions
            /// being used, or functionality wrapped in toggles that should be removed
            /// </summary>
            [DataMember(Name = "isServerSideUiLogicLogging")]
            public bool IsServerSideUiLogicLogging { get; set; }
            /// <summary>
            /// Temporary flag to enable the usage of server side ui logic fields,
            /// isServerSideUiLogic must be true in order for this to be enabled.
            /// </summary>
            [DataMember(Name = "isServerSideUiLogicPortal")]
            public bool IsServerSideUiLogicPortal { get; set; }
            /// <summary>
            /// Flag to indicate if we should automtically pop up a window
            /// to prompt the user to decide if they want to redirect to
            /// the alternate landing page
            /// </summary>
            [DataMember(Name = "isShowRedirectPopup")]
            public bool IsShowRedirectPopup { get; set; }
            /// <summary>
            /// Flag to indicate if we should show a button in the toolbar
            /// to jump to the alternate landing page.
            /// </summary>
            [DataMember(Name = "isShowRedirectButton")]
            public bool IsShowRedirectButton { get; set; }
            /// <summary>
            /// the lasnding page to redirect to
            /// </summary>
            [DataMember(Name = "redirect")]
            public string Redirect { get; set; }
        }

        protected override void OnInit(EventArgs e)
        {
            AddExtTheme(this.header, (new SymphonyController()).ApplicationName);
            AddCustomSkin();
            AddQuerySkin(this.header);
            base.OnInit(e);
        }

        private void AddCustomSkin()
        {
            // the skin folder can be in the root (legacy use) or subfolder (new use)
            string[] filenames = new string[] {                 
                "/skins/symphony_main/" + c.ApplicationName + ".css", 
                "/skins/symphony_main/" + c.ApplicationName + "/main.css" };

            foreach (string filename in filenames)
            {
                if (File.Exists(Server.MapPath("~" + filename)))
                {
                    Literal control = new Literal();
                    control.Text = "<link href=\"" + filename + "\" rel=\"stylesheet\" type=\"text/css\" />";
                    this.header.Controls.Add(control);
                }
            }
        }

        public static void AddExtTheme(Control parent, string customer)
        {
            // no trailing slash on this one please
            string folderName = "/skins/symphony_main/" + customer + "/extjs/css";
            string extFolder = HttpContext.Current.Server.MapPath("~/" + folderName);
            if (Directory.Exists(extFolder))
            {
                foreach (string css in Directory.GetFiles(extFolder, "*.css"))
                {
                    Literal control = new Literal();
                    string url = folderName + "/" + Path.GetFileName(css);
                    if (css.ToLowerInvariant().IndexOf("ie6") > -1)
                    {
                        control.Text = "<!--[if IE 6]><link href=\"" + url + "\" rel=\"stylesheet\" type=\"text/css\" /><![endif]-->";
                    }
                    else
                    {
                        control.Text = "<link href=\"" + url + "\" rel=\"stylesheet\" type=\"text/css\" />";
                    }
                    parent.Controls.Add(control);
                }
            }
        }
        public static void AddQuerySkin(Control parent)
        {
            if (!Skins.HasQuerySkin())
            {
                return;
            }
            
            string skinFolder = Skins.GetSkinFolder();
            
            string masterPageName = Path.GetFileNameWithoutExtension(parent.Page.MasterPageFile);
            string pageName = Path.GetFileNameWithoutExtension(parent.Page.AppRelativeVirtualPath);

            string masterPageSkinPath = Path.Combine(skinFolder, masterPageName);
            string pageSkinPath = Path.Combine(skinFolder, pageName);
            string globalSkinPath = Path.Combine(skinFolder, "global");

            string cssTemplate = "<link href=\"{0}\" rel=\"stylesheet\" type=\"text/css\" />";
            string jsTemplate = "<script src=\"{0}\" type=\"text/javascript\"></script>";

            LoadCustomSkinFiles(parent, globalSkinPath + "/css", "css", cssTemplate);
            LoadCustomSkinFiles(parent, masterPageSkinPath + "/css", "css", cssTemplate);
            LoadCustomSkinFiles(parent, pageSkinPath + "/css", "css", cssTemplate);
            
            LoadCustomSkinFiles(parent, globalSkinPath + "/js", "js", jsTemplate);
            LoadCustomSkinFiles(parent, masterPageSkinPath + "/js", "js", jsTemplate);
            LoadCustomSkinFiles(parent, pageSkinPath + "/js", "js", jsTemplate);
        }

        private static void LoadCustomSkinFiles(Control parent, string path, string extension, string template)
        {
            string absolutePath = HttpContext.Current.Server.MapPath("~/" + path);
            if (Directory.Exists(absolutePath))
            {
                foreach (string file in Directory.GetFiles(absolutePath, "*." + extension))
                {
                    Literal control = new Literal();
                    string url = path + "/" + Path.GetFileName(file);

                    control.Text = string.Format(template, url);

                    parent.Controls.Add(control);
                }
            }
        }

        // used by specific sub-pages to prevent redirection when used in standalone mode
        public bool PreventRedirect { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // triggers binding so we can use <%# instead of <%= and avoid issues with control collections
            Page.Header.DataBind();

            // allow a query string to prevent redirect
            if (!string.IsNullOrEmpty(Request.QueryString[Login.NEVER_REDIRECT]))
            {
                if (Request.QueryString[Login.NEVER_REDIRECT].ToLower() == "true")
                {
                    this.PreventRedirect = true;
                }
            }
            
            Symphony.Core.Data.Customer customer = new Symphony.Core.Data.Customer(c.CustomerID);
            User user = new User();

            try
            {
                user = (new CustomerController()).GetUser(c.CustomerID, c.UserID, c.UserID).Data;
            }
            catch (Exception ex)
            {
                // Don't need to deal with this since this whole page needs to load, then redirect via JS if the user is not logged in. 
            }

            bool isShowRedirectPopup = false;
            bool isShowRedirectButton = false;
            bool isAutoRedirect = false;
            string redirect = "";

            int redirectType = user.RedirectType.HasValue && user.RedirectType > (int)RedirectType.Symphony ? user.RedirectType.Value :
                customer.RedirectType.HasValue ? customer.RedirectType.Value :
                (int)RedirectType.Symphony;

            string loginRedirect = !string.IsNullOrWhiteSpace(user.LoginRedirect) ? user.LoginRedirect : customer.LoginRedirect;


            if (!PreventRedirect && redirectType > (int)RedirectType.Symphony && !string.IsNullOrWhiteSpace(loginRedirect))
            {
                Uri redirectUri;
                bool isValidRedirect = Uri.TryCreate(loginRedirect, UriKind.Absolute, out redirectUri)
                    && (redirectUri.Scheme == Uri.UriSchemeHttp || redirectUri.Scheme == Uri.UriSchemeHttps);

                if (redirectUri != null)
                {
                    redirect = redirectUri.AbsoluteUri;

                    switch (redirectType)
                    {
                        case (int)RedirectType.OptIn:
                            if (user.IsRedirectOptIn.HasValue && user.IsRedirectOptIn.Value)
                            {
                                isAutoRedirect = true;
                            }
                            else if (!user.IsRedirectOptIn.HasValue)
                            {
                                isShowRedirectPopup = true;
                            }
                            isShowRedirectButton = true;
                            break;
                        case (int)RedirectType.Force:
                            isShowRedirectButton = true;
                            isAutoRedirect = true;
                            break;
                        case (int)RedirectType.Simple:
                            var roles = SymphonyRoleProvider.GetRolesForUser(customer.SubDomain, user.Username);
                            if (roles.Length == 0 || (roles.Length == 1 && roles[0] == Roles.CustomerUser))
                            {
                                isAutoRedirect = true;
                            }
                            break;
                    }

                    string deepQuery = Request.QueryString["query"];

                    if (string.IsNullOrWhiteSpace(deepQuery) && isAutoRedirect)
                    {
                        Response.Redirect(redirect);
                    }
                }
            }

            string modulesArray = "['" + string.Join("','", (customer.Modules ?? string.Empty).Split(',')) + "']";

            string audience = string.IsNullOrEmpty(customer.AudienceAlias) ? "Audience" : customer.AudienceAlias;
            string jobrole = string.IsNullOrEmpty(customer.JobRoleAlias) ? "Job Role" : customer.JobRoleAlias;
            string location = string.IsNullOrEmpty(customer.LocationAlias) ? "Location" : customer.LocationAlias;
            string course = string.IsNullOrEmpty(customer.CourseAlias) ? "Course" : customer.CourseAlias;

            string usernameAlias = string.IsNullOrEmpty(customer.UserNameXAlias) ? "Username" : customer.UserNameXAlias;
            

            string audiences = SubSonic.Sugar.Strings.SingularToPlural(audience);
            string jobroles = SubSonic.Sugar.Strings.SingularToPlural(jobrole);
            string locations = SubSonic.Sugar.Strings.SingularToPlural(location);
            string courses = SubSonic.Sugar.Strings.SingularToPlural(course);

            Customer customerModel = new Customer();
            customerModel.CopyFrom(customer);

            var authCookie = HttpContext.Current.Request.Cookies[System.Web.Security.FormsAuthentication.FormsCookieName];
           
            // the initial state of the application for this particular user
            InitialState state = new InitialState()
            {
                UserCertificateFields = new UserController().GetValidUserCertificateFields(),
                Aliases = new InitialStateAliases()
                {
                    Audience = audience,
                    Audiences = audiences,
                    JobRole = jobrole,
                    JobRoles = jobroles,
                    Location = location,
                    Locations = locations,
                    Username = usernameAlias,
                    Course = course,
                    Courses = courses
                },
                Portal = new InitialStatePortal()
                {
                    ArtisanUrl = ConfigurationManager.AppSettings["ArtisanUrl"],
                    LaunchBasePage = ConfigurationManager.AppSettings["LaunchPageUrl"],
                    ReportingDefaultUrl = ConfigurationManager.AppSettings["ReportingDefaultUrl"],
                    ReportingLaunchUrl = ConfigurationManager.AppSettings["ReportingLaunchUrl"],
                    ReportingLoginUrl = ConfigurationManager.AppSettings["ReportingLoginUrl"],
                    ReportingUrl = ConfigurationManager.AppSettings["ReportingUrl"],
                    SsoEnabled = customer.SsoEnabled,
                    SsoTimeoutRedirect = string.IsNullOrEmpty(customer.SSOTimeoutRedirect) ? "/SsoTimeoutLanding.aspx" : customer.SSOTimeoutRedirect,
                    HelpUrl = customer.HelpUrl,
                    PortalSettings = (new PortalController()).GetListPortalSetting(customer.Id).Data
                },
                Settings = new InitialStateSettings()
                {
                    Timeout = (int)Security.FormsAuthentication.Timeout.TotalMinutes,
                    Modules = modulesArray,
                    SessionId = Session.SessionID,
                    Aspxauth = authCookie == null ? string.Empty : authCookie.Value,
                    IsServerSideUiLogic = SymphonyController.IsServerSideUiLogic,
                    IsServerSideUiLogicLogging = SymphonyController.IsServerSideUiLogicLogging,
                    IsServerSideUiLogicPortal = SymphonyController.IsServerSideUiLogicPortal,
                    IsShowRedirectButton = isShowRedirectButton,
                    IsShowRedirectPopup = isShowRedirectPopup,
                    Redirect = redirect
                },
                User = new InitialStateUser()
                {
                    Id = c.UserID,
                    CustomerId = c.CustomerID,
                    CustomerDomain = c.ApplicationName,
                    FullName = user.FullName.Replace("'", "\'"),
                    IsGtmOrganizer = c.HasRole(Roles.GTMOrganizer),
                    IsGtwOrganizer = c.HasRole(Roles.GTWOrganizer),
                    IsClassroomInstructor = c.HasRole(Roles.ClassroomInstructor),
                    IsClassroomManager = c.HasRole(Roles.ClassroomManager),
                    IsClassroomResourceManager = c.HasRole(Roles.ClassroomResourceManager),
                    IsClassroomSupervisor = c.HasRole(Roles.ClassroomSupervisor),
                    IsSalesChannelAdmin = c.UserIsSalesChannelAdmin,
                    IsCustomerManager = c.HasRole(Roles.CustomerManager),
                    IsCustomerJobRoleManager = c.HasRole(Roles.CustomerJobRoleManager),
                    IsCustomerLocationManager = c.HasRole(Roles.CustomerLocationManager),
                    IsCustomerAdministrator = c.HasRole(Roles.CustomerAdministrator),
                    IsTrainingAdministrator = c.HasRole(Roles.CourseAssignmentAdministrator),
                    IsTrainingManager = c.HasRole(Roles.CourseAssignmentTrainingManager),
                    IsArtisanAuthor = c.HasRole(Roles.ArtisanAuthor),
                    IsArtisanPackager = c.HasRole(Roles.ArtisanPackager),
                    IsArtisanViewer = c.HasRole(Roles.ArtisanViewer),
                    IsReportingAnalyst = c.HasRole(Roles.ReportingAnalyst),
                    IsReportingLocationAnalyst = c.HasRole(Roles.ReportingLocationAnalyst),
                    IsReportingJobRoleAnalyst = c.HasRole(Roles.ReportingJobRoleAnalyst),
                    IsReportingDeveloper = c.HasRole(Roles.ReportingDeveloper),
                    IsReportingSupervisor = c.HasRole(Roles.ReportingSupervisor),
                    IsSuperUser = false,//c.ApplicationName == "be",//c.HasRole(Roles.SuperUser),
                    CNRToken = c.CNRToken,
                    CustomerNewHireTransitionPeriod = customer.NewHireTransitionPeriod.HasValue ? customer.NewHireTransitionPeriod.Value : 0,
                    CanDeployMultiple = c.CanDeployMultipleCourses(customer.SubDomain),
                    CanChangePassword = customer.CanUsersChangePassword,
                    HasPasswordMask = customer.HasPasswordMask,
                    AllowSelfAccountCreation = customer.AllowSelfAccountCreation,
                    HasValidationParameters = c.HasValidationParameters,
                    IsUserValidtionParametersRequired = c.IsUserValidationParametersRequired,
                    HasNmlsNumber = !string.IsNullOrWhiteSpace(user.NmlsNumber),
                    IsPasswordResetRequired = user.IsPasswordResetRequired,
                    EnforcePasswordReset = user.EnforcePasswordReset,
                    OnlineStatus = user.OnlineStatus
                },
                ActualUser = new InitialStateUser()
                {
                    Id = c.ActualUserID,
                    CustomerId = c.ActualCustomerID,
                    IsSalesChannelAdmin = c.ActualUserIsSalesChannelAdmin,
                    CustomerDomain = c.ActualApplicationName
                    //FullName = c.UserFullName.Replace("'", "\'")
                },
                NetTimeZones = TimeZoneInfo.GetSystemTimeZones().Select(tz => new NetTimeZone() { DisplayName = tz.DisplayName, ZoneId = tz.Id }),
                Modules = c.Modules,
                Customer = customerModel,
                Qlik = new QlikInfo()
                {
                    Environment = ConfigurationManager.AppSettings["QlikEnvironment"],
                    Instance = ConfigurationManager.AppSettings["QlikInstance"]
                }
            };

            this.userDetails.Text = string.Format(@"Symphony = {0}", FileUploader.Serialize(state));
            this.enumJson.Text = EnumJson;

#if DEBUG
            Handlers.Scripts scripts = new Handlers.Scripts();
            this.javascript.Text = scripts.GenerateScripts(HttpContext.Current, "SymphonyJavascriptFiles");
#else

            this.javascript.Text = "<script type='text/javascript' src='/Handlers/Scripts.ashx?target=SymphonyJavascriptFiles&compress=true&v=" + AssemblyVersion + "'></script>";
#endif
        }





    }
}
