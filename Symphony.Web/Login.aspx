﻿<%@ Page Title="Symphony Login" Language="C#" MasterPageFile="~/Unauthenticated.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Symphony.Web.Login" %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>
<asp:Content ID="Header" ContentPlaceHolderID="Header" runat="server">

    <script type="text/javascript">
        window.lc = <%# listCustomersBySalesChannelCodeJson %>;
        window.janrainLogin = "<%# isJanrain %>" === "True";
        window.ssoLoginUiType = <%# (int)ssoLoginUiType %>;
        window.ssoTimeoutRedirect = '<%# ssoTimeoutRedirect %>';
      
        Ext.onReady(function () {
            
            var queries = {};
            if (window.location.search) {
                $.each(window.location.search.substr(1).split('&'),function(c,q){
                    var i = q.split('=');
                    queries[i[0].toString()] = i[1].toString();
                });
            }

            if (queries.customer == "ocl" || window.location.href.indexOf("login/ocl") > 0) {
                $('#ctl00_Body_login_PasswordRecoveryLink').attr('href', 'https://www.proschools.com/customer/account/forgotpassword/');
            }

            if (queries.skin == "proschools") {
                $('#ctl00_Body_login_PasswordRecoveryLink').hide();
            }

            var me = this;
            LoadFacil();
            var w = new Ext.Window({
                contentEl: Ext.get('form1Wrapper'),
                closable: false,
                resizable: false,
                padding: '15',
                listeners: {
                    afterrender: function () {
                        if (!window.janrainLogin) {
                            $(".sso-login ").hide();
                        } 

                        window.setTimeout(function () {
                            var el = document.getElementById('ctl00_Body_login_UserName');
                            if (!el) { el = document.getElementById('Body_login_UserName'); }
                            el.focus();
                        }, 700);
                    }
                }
            });
            w.show();
            // fix for weird bug in IE that makes the top and bottom borders not show
            window.setTimeout(function () { 
                w.setWidth(w.getWidth() + 1); 
            }, 1); 
            
            // Will update the login ui if Janrain enabled
            // Will load the appropriate mode, standard, mixed, or forced.
            // If new login forms are added, add their initialization here
            // ensuring that only one will run
            //janrainInitLoginUi();

            // If the customer is configured to use SSO, log them out
            // at the customer's sp to keep the authentication in sync
            /*if (window.ssoTimeoutRedirect) {
                (new Image()).src = window.ssoTimeoutRedirect;
            }*/
        });

        function LoadFacil() {
            if (lc.length > 0) {
                var rdiv = $('.symphony-logo')[0].closest("tr");
                var dh = Ext.DomHelper;
                var facdata = [];
                for(var i = 0; i < window.lc.length; i++) {
                    var newfac = [window.lc[i], window.lc[i]];
                    facdata.push(newfac);
                }
                var newtr = {
                    id: 'facilityTr',
                    tag: 'tr',
                    children: [
                        {tag: 'td', html: 'Facility:', align: 'right' },
                        {tag: 'td', id: 'facilityTd', align: 'left', style: 'padding-left: 4px;'}
                    ]
                };
                var newelm = dh.insertAfter(
                        rdiv,
                        newtr
                    );
                var p = new Ext.Panel({
                    renderTo: 'facilityTd',
                    bodyStyle: 'background-color: #dfe8f6',
                    border: false,
                    frame: false,
                    items: [{
                        xtype: 'combo',
                        name: 'txtFacility',
                        mode: 'local',
                        hideTrigger: true,
                        width: 150,
                        store: new Ext.data.SimpleStore({
                            data: facdata,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: true
                    }]
                });
            }
        }

    </script>
    <style type="text/css">
        input{ margin:4px; }
        .title 
        {
            height: 55px;
            background-image: url(/images/symphony_logo.png);
            background-repeat:no-repeat;
            background-position:50% 80%;
            vertical-align:top;
        }
        .box
        {
            margin:5px;
        }
        /* applies to the button */
        .x-btn-text
        {
            margin-right:12px;
            width:145px;
        }
        .forgot-password
        {
            padding-top:12px;
            padding-right:28px;
        }
        #janrainView .janrainHeader > div
        {
            font-size: 14px;
        }
            .sso-login
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="Body" runat="server">    
    <div id="form1Wrapper"  class="x-hidden" style="position:relative">
        <url:form id="form1" runat="server" >
            <div id="loginDiv">
                <asp:Login ID="login" runat="server" 
                    TitleText="Welcome to Symphony"
                    TitleTextStyle-CssClass="title symphony-logo" 
                    DisplayRememberMe="false" 
                    TextBoxStyle-CssClass="box"
                    TextBoxStyle-Width="145"
                    LoginButtonStyle-CssClass="x-btn-text"
                    OnAuthenticate="OnAuthenticate"
                    
                    HyperLinkStyle-HorizontalAlign="Right"
                    HyperLinkStyle-CssClass="forgot-password"
                    >
                </asp:Login>

            </div>
            <div id="facilityDiv">
                
            </div>
        </url:form>
    </div>

    <div class="x-window-mc sso-login hidden">
        <div class="main">
            <p class="title symphony-logo">Welcome to Symphony</p>
            <!-- The following links are meant to simulate elements already existing on your page. -->
	        <!-- Add the class 'capture_modal_open' to an anchor tag to initiate signin. -->
	        <a href="#" id="capture_signin_link" class="capture_modal_open janrain-button">Student Log In</a>
            <a href="#" id="login-symphony" class="janrain-button">Regulatory Log In</a>
        </div>
        
	    <!-- Add the class 'capture_end_session' for the log out link -->
        <div class="loading" style="display: none">
            <p>Loading... please wait...</p>
            <br />
            <img src="../images/ajax-loader.gif" />
        </div>

        
        <div class="customerSelector" style="display: none">
            <p class="title">Please select a user to log in as:</p>
            <div class="customers">
                <div class="customer janrain-button">
                    <h2 class="customerName"></h2>
                    <p class="userName"></p>
                </div>
            </div>
            <a href="#" id="cancel_login">Cancel Login</a>
        </div>

	    <a href="#" id="capture_signout_link" class="capture_end_session" style="display:none">Sign Out</a>
    </div>
	
    
    <script type="text/javascript">
        /*
         * jQuery postMessage - v0.5 - 9/11/2009
         * http://benalman.com/projects/jquery-postmessage-plugin/
         *
         * Copyright (c) 2009 "Cowboy" Ben Alman
         * Dual licensed under the MIT and GPL licenses.
         * http://benalman.com/about/license/
         */
        !function(e){"$:nomunge";var n,t,r,a,i,o=1,s=this,c=!1,l="postMessage",u="addEventListener",f=s[l];e.getUrlParam=i=function(e,n){var t=!1;if(-1!=e.search(n+"=")){var r=e.substring(e.indexOf("?")+1),a=r.split("&");for(index=0;index<a.length;++index)if(-1!=a[index].search(n)){var i=a[index].split("=");t=i[1];break}}return t},e[l]=function(n,t,r){if(t){var a=e.getUrlParam(t,"parentUrl");a&&(t=a),n="string"==typeof n?n:e.param(n),r=r||parent,f?r[l](n,t.replace(/([^:]+:\/\/[^\/]+).*/,"$1")):t&&(r.location=t.replace(/#.*$/,"")+"#"+ +new Date+o++ +"&"+n)}},e.receiveMessage=a=function(i,o,l){f?(i&&(r&&a(),r=function(n){return"string"==typeof o&&n.origin!==o||e.isFunction(o)&&o(n.origin)===c?c:void i(n)}),s[u]?s[i?u:"removeEventListener"]("message",r,c):s[i?"attachEvent":"detachEvent"]("onmessage",r)):(n&&clearInterval(n),n=null,i&&(l="number"==typeof o?o:"number"==typeof l?l:100,n=setInterval(function(){var e=document.location.hash,n=/^#?\d+&/;e!==t&&n.test(e)&&(t=e,i({data:e.replace(n,"")}))},l)))}}(jQuery);

        // passes height around so stuff can resize when embedded
        jQuery(function($){
            // Update the iframe height on the parent page
            $.postMessage({ iframeHeight: $('body').outerHeight(true) }, window.location.href, parent);
        });
	</script>
</asp:Content>


