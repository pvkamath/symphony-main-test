﻿using System;
using System.Web;
using System.Security.Cryptography.X509Certificates;
using ComponentPro.Saml2;
using Model = Symphony.Core.Models;
using Controllers = Symphony.Core.Controllers;

namespace Symphony.Web
{
    public partial class SamlAuthentication : System.Web.UI.Page
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                #region Receive SAML Response

                // Create a SAML response from the HTTP request.
                ComponentPro.Saml2.Response samlResponse = ComponentPro.Saml2.Response.Create(Request);

                if (samlResponse.Issuer == null)
                {
                    throw new ApplicationException("No issuer specified on the SAML response.");
                }

                // Lookup issuer url in customer db
                var customer = new Controllers.CustomerController().GetCustomerByUrl(samlResponse.Issuer.NameIdentifier);
                if (customer == null)
                {
                    throw new ApplicationException("No customer found with the issuer ID " + samlResponse.Issuer.NameIdentifier);
                }
                else if (customer.SsoEnabled == false)
                {
                    throw new ApplicationException("This customer does not have SSO enabled.");
                }
                else if (customer.CertificateFile == null || customer.CertificateFile.Length == 0)
                {
                    throw new ApplicationException("This customer does not have a verification certificate uploaded.");
                }

                if (samlResponse.IsSigned())
                {
                    if (customer.CertificateFile == null || customer.CertificateFile.Length == 0)
                    {
                        throw new ApplicationException("This customer does not have a certificate on file.");
                    }

                    X509Certificate2 x509Certificate = new X509Certificate2(customer.CertificateFile);
                    if (!samlResponse.Validate(x509Certificate))
                    {
                        throw new ApplicationException("Couldn't validate the certificate; SAML response signature is not valid.");
                    }
                }
                else
                {
                    throw new ApplicationException("SAML response not signed.");
                }

                #endregion

                #region Process the response

                // Success?
                if (!samlResponse.IsSuccess())
                {
                    throw new ApplicationException("SAML response is not success");
                }

                Assertion samlAssertion;

                // Get the asserted identity.
                if (samlResponse.GetAssertions().Count > 0)
                {
                    samlAssertion = samlResponse.GetAssertions()[0];
                }
                else
                {
                    throw new ApplicationException("No assertions found in the SAML response");
                }

                // Get the subject name identifier.
                string userName;

                if (samlAssertion.Subject.NameId != null)
                {
                    userName = samlAssertion.Subject.NameId.NameIdentifier;
                }
                else
                {
                    throw new ApplicationException("Name identifier not found in subject");
                }

                Controllers.UserController controller = new Controllers.UserController();
                Model.AuthenticationResult loginResult = controller.Login(customer.SubDomain, userName, true);
                if (loginResult.Success)
                {
                    // Redirect to the requested URL.
                    Response.Redirect(samlResponse.RelayState, false);
                    //Response.Redirect("/home/" + loginResult.CustomerSubDomain + "/" + finalQuery);
                }
                else
                {
                    throw new ApplicationException(loginResult.Message);
                }

                #endregion
            }

            catch (Exception exception)
            {
                Response.Write(string.Format("An error occurred: {0}", exception.Message));
            }
        }
    }
}