﻿using System;
using System.Web;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.IO;
using System.Net;
using System.Configuration;
using log4net;

namespace Symphony.Web.Saml
{
    public class SamlParams
    {
        private static string defaultIdPKeyFile = "App_Data/certs/Symphony.pfx";
        private static string defaultIdPKeyPassword = "HbA7ba083QueAs";

        private static string defaultSPCertFile = "App_Data/certs/Symphony.cer";

        private static string defaultIdPCertKey = "IdPCertKey";
        private static string defaultSPCertKey = "SPCertKey";

        private static string defaultSsoSessionKey = "symphonysso";

        private static string defaultSamlRedirectKey = "fromSymphonyLogin";

        private static ILog log = LogManager.GetLogger(typeof(SamlParams));

        public static string IdPKeyFile
        {
            get
            {
                string idPKeyFile = ConfigurationManager.AppSettings["IdPKeyFile"];
                if (!string.IsNullOrEmpty(idPKeyFile))
                {
                    return idPKeyFile;
                }
                else
                {
                    log.Error("No configration set for IdKeyFile - Using default");
                    return defaultIdPKeyFile;
                }
            }
        }

        public static string IdPKeyPassword
        {
            get
            {
                string idPKeyPassword = ConfigurationManager.AppSettings["IdPKeyPassword"];
                if (!string.IsNullOrEmpty(idPKeyPassword))
                {
                    return idPKeyPassword;
                }
                else
                {
                    log.Error("No configration set for IdPKeyPassword - Using default");
                    return defaultIdPKeyPassword;
                }
            }
        }

        public static string SPCertFile
        {
            get
            {
                string sPCertFile = ConfigurationManager.AppSettings["SPCertFile"];
                if (!string.IsNullOrEmpty(sPCertFile))
                {
                    return sPCertFile;
                }
                else
                {
                    log.Error("No configration set for SPCertFile - Using default");
                    return defaultSPCertFile;
                }
            }
        }

        public static string IdPCertKey
        {
            get
            {
                string idPCertKey = ConfigurationManager.AppSettings["IdPCertKey"];
                if (!string.IsNullOrEmpty(idPCertKey))
                {
                    return idPCertKey;
                }
                else
                {
                    log.Error("No configration set for IdPCertKey - Using default");
                    return defaultIdPCertKey;
                }
            }
        }

        public static string SPCertKey
        {
            get
            {
                string sPCertKey = ConfigurationManager.AppSettings["SPCertKey"];
                if (!string.IsNullOrEmpty(sPCertKey))
                {
                    return sPCertKey;
                }
                else
                {
                    log.Error("No configration set for SPCertKey - Using default");
                    return defaultSPCertKey;
                }
            }
        }

        public static string SsoSessionKey
        {
            get
            {
                string ssoSessionKey = ConfigurationManager.AppSettings["SsoSessionKey"];
                if (!string.IsNullOrEmpty(ssoSessionKey))
                {
                    return ssoSessionKey;
                }
                else
                {
                    log.Error("No configration set for SsoSessionKey - Using default");
                    return defaultSsoSessionKey;
                }
            }
        }

        public static string SamlRedirectKey
        {
            get
            {
                string samlRedirectKey = ConfigurationManager.AppSettings["SamlRedirectKey"];
                if (!string.IsNullOrEmpty(samlRedirectKey))
                {
                    return samlRedirectKey;
                }
                else
                {
                    log.Error("No configration set for SamlRedirectKey - Using default");
                    return defaultSamlRedirectKey;
                }
            }
        }




        
    }
}