﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Symphony.Core.Models;
using Symphony.Core;
using System.Configuration;
using SubSonic;
using System.Text;
using Newtonsoft.Json;

namespace Symphony.Web.Saml
{
    public class SamlDatabaseCacheProvider : ComponentPro.Saml.ISamlCacheProvider
    {
        static log4net.ILog Log = log4net.LogManager.GetLogger("SamlDatabaseCacheProvider");

        public object Add(string key, object value, System.Web.Caching.CacheDependency dependencies, DateTime absoluteExpiration, TimeSpan slidingExpiration, System.Web.Caching.CacheItemPriority priority, System.Web.Caching.CacheItemRemovedCallback onRemoveCallback)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { throw new NotImplementedException(); }
        }

        public object Get(string key)
        {
            throw new NotImplementedException();
        }

        public System.Collections.IDictionaryEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public void Insert(string key, object value, System.Web.Caching.CacheDependency dependencies, DateTime absoluteExpiration, TimeSpan slidingExpiration, System.Web.Caching.CacheItemPriority priority, System.Web.Caching.CacheItemRemovedCallback onRemoveCallback)
        {
            throw new NotImplementedException();
        }

        public void Insert(string key, object value, TimeSpan slidingExpiration)
        {
            string _type = value.GetType().ToString();
            try
            {
                var cacheObj = new Symphony.Core.Data.SAMLCache();

                cacheObj.Key = key;
                cacheObj.ValueX = value.ToString();
                cacheObj.ValidDurationInMS = (int) slidingExpiration.TotalMilliseconds;
                
                cacheObj.Save();
            }

            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public void Insert(string key, object value, System.Web.Caching.CacheDependency dependencies, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            throw new NotImplementedException();
        }

        public void Insert(string key, object value, System.Web.Caching.CacheDependency dependencies)
        {
            throw new NotImplementedException();
        }

        public void Insert(string key, object value)
        {
            throw new NotImplementedException();
        }

        public object Remove(string key)
        {
            try
            {

                SqlQuery query = Select.AllColumnsFrom<Symphony.Core.Data.SAMLCache>()
                    .Where(Symphony.Core.Data.SAMLCache.KeyColumn)
                    .IsEqualTo(key);

                var res = query.ExecuteSingle<Symphony.Core.Data.SAMLCache>();
                if (res != null)
                {
                    Symphony.Core.Data.SAMLCache.Destroy(res.Id);
                }

                return res.ValueX;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public T Remove<T>(string key)
        {
            try
            {
                
                SqlQuery query = Select.AllColumnsFrom<Symphony.Core.Data.SAMLCache>()
                    .Where(Symphony.Core.Data.SAMLCache.KeyColumn)
                    .IsEqualTo(key);

                var res = query.ExecuteSingle<Symphony.Core.Data.SAMLCache>();
                Symphony.Core.Data.SAMLCache.Destroy(res.Id);

                return JsonConvert.DeserializeObject<T>(res.ValueX);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public object this[string key]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public static void CleanSAMLCache( object StatObj )
        {
            var expiryLimit = ConfigurationManager.AppSettings["CACHE_LIFETIME_MINUTES"];

            // NOTE: The CodingHorror overload that takes a params list is not processing correctly
            // so we have to append the value from the config. Even though this is a config value, we take 
            // precautions and ensure the value passed can be parsed as a valid integer

            try
            {
                var intCheck = int.Parse(expiryLimit);
            }
            catch
            {
                Log.Error("Invalid expiryLimit value: " + expiryLimit);
                return;
            }

            string query = @"DELETE FROM SAMLCache
                WHERE DATEADD(millisecond, ValidDurationInMS, SQLTime) < GETUTCDATE()";
            
            var ch = new CodingHorror();
            ch.Execute(query);
        }
    }
}