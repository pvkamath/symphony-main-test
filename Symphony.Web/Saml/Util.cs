﻿using System;
using System.Web.Configuration;
using System.Diagnostics;
using System.Web.UI;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using System.Web;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Linq;

using ComponentPro.Saml;
using ComponentPro.Saml.Binding;
using ComponentPro.Saml2;
using ComponentPro.Saml2.Binding;


using Symphony.Core.Models;
using Symphony.Core.Controllers;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.Serialization;
using Symphony.Core;
using Newtonsoft.Json;


namespace Symphony.Web.Saml
{
    public class Util
    {
        // The query string parameter identifying the SP to IdP binding in use.
        public const string BindingQueryParameter = "binding";

        public static string GetAbsoluteUrl(HttpContext context, string relativeUrl)
        {
            return new Uri(context.Request.Url, VirtualPathUtility.ToAbsolute(relativeUrl)).ToString();
        }

        public static Uri GetRequestOrigin(HttpContext context)
        {
            Uri requestOrigin = new Uri(ServiceProviderController.GetTestServiceProviderUrl());

            if (context.Request.Headers.AllKeys.Contains("Origin"))
            {
                string origin = context.Request.Headers["Origin"];

                if (!origin.Contains("chrome-extension"))
                {
                    requestOrigin = new Uri(context.Request.Headers["Origin"]);
                }
            }
            else if (context.Request.UrlReferrer != null)
            {
                requestOrigin = context.Request.UrlReferrer;
            }

            return requestOrigin;
        }

        // Receive the authentication request from the service provider.
        public static void ReceiveAuthnRequest(HttpContext context, out AuthnRequest authnRequest, out string relayState)
        {
            // Determine the service provider to identity provider binding type.
            // We use a query string parameter rather than having separate endpoints per binding.
            string bindingType = context.Request.QueryString[BindingQueryParameter];

            // Look up for the appropriate artifact SP url
            //ServiceProvider provider = ServiceProviderController.GetServiceProvider(GetRequestOrigin(context), context.Request.Url).Data;

            if (string.IsNullOrEmpty(bindingType))
            {
                bindingType = SamlBindingUri.HttpRedirect;
            }
            else
            {
                if (bindingType == SamlBindingUri.HttpArtifact)
                {
                    if (context.Request.HttpMethod == "POST")
                    {
                        bindingType = SamlBindingUri.HttpPost;
                    }
                    else
                    {
                        bindingType = SamlBindingUri.HttpRedirect;
                    }
                }
            }

            switch (bindingType)
            {
                case SamlBindingUri.HttpRedirect:
                    X509Certificate2 x509Certificate = (X509Certificate2)context.Application[SamlParams.SPCertKey];

                    authnRequest = AuthnRequest.Create(context.Request.RawUrl, x509Certificate.PrivateKey);
                    relayState = authnRequest.RelayState;
                    break;
                //case SamlBindingUri.HttpArtifact:
                case SamlBindingUri.HttpPost:
                    authnRequest = AuthnRequest.CreateFromHttpPost(context.Request);
                    relayState = authnRequest.RelayState;
                    break;

                // NOTE April 21, 2016 - This needs to be revisted at some point. 
                // For now, just se the same flow for POST and Artifact.
                /*case SamlBindingUri.HttpArtifact:
                    // Receive the artifact.
                    Saml2ArtifactType0004 httpArtifact = Saml2ArtifactType0004.CreateFromHttpArtifactQueryString(context.Request);

                    // Create an artifact resolve request.
                    ArtifactResolve artifactResolve = new ArtifactResolve();
                    artifactResolve.Id = Util.GetRandomId();
                    artifactResolve.Issuer = new Issuer(Util.GetAbsoluteUrl(context, "~/"));
                    artifactResolve.Artifact = new Artifact(httpArtifact.ToString());

                    // Send the artifact resolve request and receive the artifact response.
                    // Artifact binding will be broken for now.     
                    string artifactServiceProviderUrl = httpArtifact.RelayState; // Relay state must be a url now, and must both handle artifacts and the redirect.
                    ServiceProviderController.GetServiceProviderFromNameIdentifier(httpArtifact.n
                    ArtifactResponse artifactResponse = ArtifactResponse.SendSamlMessageReceiveAftifactResponse(artifactServiceProviderUrl, artifactResolve);

                    // Extract the authentication request from the artifact response.
                    authnRequest = new AuthnRequest(artifactResponse.Message);
                    relayState = httpArtifact.RelayState;
                    break;*/

                default:
                    throw new Exception("Invalid binding type");

            }

            // If using HTTP redirect the message isn't signed as the generated query string is too long for most browsers.
            if (bindingType != SamlBindingUri.HttpRedirect)
            {
                if (authnRequest.IsSigned())
                {
                    // Verify the request's signature.
                    X509Certificate2 x509Certificate = (X509Certificate2)context.Application[SamlParams.SPCertKey];

                    if (!authnRequest.Validate(x509Certificate))
                    {
                        throw new ApplicationException("The authentication request signature failed to verify.");
                    }
                }
            }
        }

        // Create a SAML response with the user's local identity, if any, or indicating an error.
        public static ComponentPro.Saml2.Response CreateSamlResponse(HttpContext context, string consumerServiceUrl, bool isDisableTheme = false)
        {
            ComponentPro.Saml2.Response samlResponse = new ComponentPro.Saml2.Response();
            string issuerUrl = Util.GetAbsoluteUrl(context, "~/");

            samlResponse.Issuer = new Issuer(issuerUrl);
            //samlResponse.Id = context.Request.Url.GetLeftPart(UriPartial.Authority) + "/Handlers/SamlIdp.ashx";
            samlResponse.Id = Util.GetRandomId();
            if (context.User.Identity.IsAuthenticated)
            {
                samlResponse.Status = new Status(SamlPrimaryStatusCode.Success, null);

                Assertion samlAssertion = new Assertion();
                samlAssertion.Issuer = samlResponse.Issuer;
                Subject subject = new Subject(new NameId(context.User.Identity.Name));
                SubjectConfirmation subjectConfirmation = new SubjectConfirmation(SamlSubjectConfirmationMethod.Bearer);
                SubjectConfirmationData subjectConfirmationData = new SubjectConfirmationData();
                subjectConfirmationData.Recipient = consumerServiceUrl;
                subjectConfirmation.SubjectConfirmationData = subjectConfirmationData;
                subject.SubjectConfirmations.Add(subjectConfirmation);
                samlAssertion.Subject = subject;

                AuthnStatement authnStatement = new AuthnStatement();
                authnStatement.AuthnContext = new AuthnContext();
                authnStatement.AuthnContext.AuthnContextClassRef = new AuthnContextClassRef(SamlAuthenticateContext.Password);

                samlAssertion.Statements.Add(authnStatement);
                AttributeStatement attributeStatement = new AttributeStatement();
                var cc = new CustomerController();
                var user = new Core.Data.User(cc.UserID);

                List<LoginSystem> currentLoginSystems = HttpContext.Current.Session["UserLoginSystem"] as List<LoginSystem>;
                List<LoginSystem> loginSystemRecord = new List<LoginSystem>();
                if (currentLoginSystems != null)
                {
                    foreach (LoginSystem ls in currentLoginSystems)
                    {
                        loginSystemRecord.Add(new LoginSystem()
                        {
                            Id = ls.Id,
                            CustomerName = ls.CustomerName,
                            CustomerSubDomain = ls.CustomerSubDomain,
                            JanrainId = ls.JanrainId,
                            SystemName = ls.SystemName,
                            UserEmail = ls.UserEmail,
                            UserFirstName = ls.UserFirstName,
                            UserLastName = ls.UserLastName,
                            SystemCodeName = ls.SystemCodeName,
                            UserFullName = ls.UserFullName,
                            UserId = ls.UserId
                        });
                    }

                }
                else // We are an authorized user, but not using an email. add a UserLoginSystem for the current user 
                {
                    loginSystemRecord.Add(new LoginSystem()
                    {
                        Id = 4,
                        CustomerName = user.Customer.Name,
                        CustomerSubDomain = user.Customer.SubDomain,
                        JanrainId = user.JanrainUserUuid,
                        SystemName = "Symphony",
                        UserEmail = user.Email,
                        UserFirstName = user.FirstName,
                        UserLastName = user.LastName,
                        SystemCodeName = "symphony",
                        UserFullName = user.FullName,
                        UserId = user.Id
                    });
                }

                var serialized = Utilities.Serialize(loginSystemRecord);
                attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("loginrecord", SamlAttributeNameFormat.Basic, null, serialized));

                Symphony.Core.Data.Customer c = new Core.Data.Customer(cc.CustomerID);
                if (c.ThemeID.HasValue)
                {
                    var theme = new Core.Data.Theme(c.ThemeID);
                    attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("themeCodeName", SamlAttributeNameFormat.Basic, null, theme.CodeName));
                }
                else
                {
                    // always add it
                    attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("themeCodeName", SamlAttributeNameFormat.Basic, null, string.Empty));
                }

                attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("userId", SamlAttributeNameFormat.Basic, null, user.Id.ToString()));
                attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("firstName", SamlAttributeNameFormat.Basic, null, user.FirstName));
                attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("lastName", SamlAttributeNameFormat.Basic, null, user.LastName));
                attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("username", SamlAttributeNameFormat.Basic, null, user.Username));
                attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("email", SamlAttributeNameFormat.Basic, null, user.Email));
                attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("customerId", SamlAttributeNameFormat.Basic, null, cc.CustomerID.ToString()));
                attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("customerSubdomain", SamlAttributeNameFormat.Basic, null, cc.CustomerSubdomain.ToString()));

                var customer = cc.GetCustomer(cc.CustomerID, cc.UserID, cc.CustomerID);

                if (isDisableTheme)
                {
                    customer.Data.Theme = null;
                    customer.Data.CustomTheme = null;
                    customer.Data.AssociatedImageData = null;
                }

                attributeStatement.Attributes.Add(new ComponentPro.Saml2.Attribute("customer", SamlAttributeNameFormat.Basic, null, JsonConvert.SerializeObject(customer)));

                samlAssertion.Statements.Add(attributeStatement);

                samlResponse.Assertions.Add(samlAssertion);
            }
            else
            {
                samlResponse.Status = new Status(SamlPrimaryStatusCode.Responder, SamlSecondaryStatusCode.AuthnFailed, "The user is not authenticated at the identity provider");
            }

            return samlResponse;
        }

        // Send the SAML response over the specified binding.
        public static void SendSamlResponse(HttpContext context, ComponentPro.Saml2.Response samlResponse, SsoAuthnState ssoState)
        {
            // Sign the SAML response 
            X509Certificate2 x509Certificate = (X509Certificate2)context.Application[SamlParams.IdPCertKey];

            samlResponse.Sign(x509Certificate);

            bool hasQuery = ssoState.AssertionConsumerServiceURL == null ? false : ssoState.AssertionConsumerServiceURL.Contains('?');
            string relayStateQuery = "RelayState=" + HttpUtility.UrlEncode(ssoState.RelayState);

            switch (ssoState.IdpProtocolBinding)
            {
                case SamlBinding.HttpPost:
                    samlResponse.SendPostBindingForm(context.Response.OutputStream, ssoState.AssertionConsumerServiceURL, ssoState.RelayState);
                    break;
                case SamlBinding.HttpArtifact:
                    // Create the artifact.
                    string identificationUrl = Util.GetAbsoluteUrl(context, "~/");
                    Saml2ArtifactType0004 httpArtifact = new Saml2ArtifactType0004(SamlArtifact.GetSourceId(identificationUrl), SamlArtifact.GetHandle());

                    // Cache the authentication request for subsequent sending using the artifact resolution protocol. Sliding expiration time is 1 hour.
                    SamlSettings.CacheProvider.Insert(httpArtifact.ToString(), samlResponse.GetXml().OuterXml, new TimeSpan(1, 0, 0));

                    // Send the artifact.
                    if (ssoState.IsRedirectArtifact)
                    {
                        string artifactResponseString = HttpUtility.UrlEncode(httpArtifact.ToString());
                        context.Response.Redirect(ssoState.AssertionConsumerServiceURL + (hasQuery ? "&" : "?") + "ArtifactResponse=" + artifactResponseString + "&" + relayStateQuery, false);
                        context.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        httpArtifact.SendPostForm(context.Response.OutputStream, ssoState.AssertionConsumerServiceURL, ssoState.RelayState);
                    }
                    break;
                case SamlBinding.HttpRedirect:
                    string samlResponseString = HttpUtility.UrlEncode(samlResponse.ToBase64String());

                    context.Response.Redirect(ssoState.AssertionConsumerServiceURL + (hasQuery ? "&" : "?") + "SAMLResponse=" + samlResponseString + "&" + relayStateQuery, false);
                    context.ApplicationInstance.CompleteRequest();
                    break;


            }


        }


        private static void LoadCertificate(HttpApplicationState application, string cacheKey, string fileName, string password)
        {
            X509Certificate2 cert = new X509Certificate2(fileName, password, X509KeyStorageFlags.MachineKeySet);
            application[cacheKey] = cert;
        }

        public static void LoadCertificates(HttpApplicationState application)
        {
            ServicePointManager.ServerCertificateValidationCallback = ValidateRemoteServerCertificate;

            LoadCertificate(application, SamlParams.IdPCertKey, Path.Combine(HttpRuntime.AppDomainAppPath, SamlParams.IdPKeyFile), SamlParams.IdPKeyPassword);
            LoadCertificate(application, SamlParams.SPCertKey, Path.Combine(HttpRuntime.AppDomainAppPath, SamlParams.SPCertFile), null);
        }

        private static bool ValidateRemoteServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            X509Certificate2 cert = new X509Certificate2(certificate);
            return cert.Verify();
        }

        internal static string GetRandomId()
        {
            return "_" + Guid.NewGuid().ToString().Replace("-", "");
        }
    }
}