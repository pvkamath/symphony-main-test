﻿using ComponentPro.Saml2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Xml;
using System.IO.Compression;
using System.Text;

namespace Symphony.Web.Saml
{
    [DataContract(Name = "ssoAuthnState")]
    public class SsoAuthnState
    {
        private AuthnRequest _authnRequest;

        public AuthnRequest AuthnRequest
        {
            get { return _authnRequest; }
            set { _authnRequest = value; }
        }

        [DataMember(Name = "authnXml")]
        public string AuthnXml
        {
            get; set;
        }

        [DataMember(Name = "relayState")]
        public string RelayState
        {
            get; set;
        }

        [DataMember(Name = "idpProtocolBinding")]
        public SamlBinding IdpProtocolBinding
        {
           get; set; 
        }

        [DataMember(Name = "isRedirectArtifact")]
        public bool IsRedirectArtifact
        {
            get;
            set;
        }

        [DataMember(Name = "assertionConsumerServiceUrl")]
        public string AssertionConsumerServiceURL
        {
            get; set;
        }

        [DataMember(Name = "consumerServiceRedirectUrl")]
        public string ConsumerServiceRedirectUrl
        {
            get; set; 
        }

        [DataMember(Name = "isSamlRedirect")]
        public bool IsSamlRedirect
        {
            get;
            set;
        }

        public static void SaveToCookie(SsoAuthnState state, string sessionKey)
        {
            // Add a cookie to hold reference to the session key itself
            // this way we can prevent multiple SSO sessions being stored for abandoned requests (mostly a test concern)
            HttpCookie ssoKeyCookie = new HttpCookie("ssoKey");
            ssoKeyCookie.Value = sessionKey;

            HttpCookie cookie = new HttpCookie(sessionKey);
            //cookie.Expires = DateTime.UtcNow.AddMinutes(5);

            state.AuthnXml = state.AuthnRequest.GetXml().OuterXml;

            string data = Symphony.Core.Utilities.Serialize(state);

            cookie.Value = System.Convert.ToBase64String(Zip(data));

            HttpContext.Current.Response.Cookies.Add(cookie);
            HttpContext.Current.Response.Cookies.Add(ssoKeyCookie);
        }

        public static SsoAuthnState RestoreFromCookie(string sessionKey)
        {
            try
            {
                if (!HttpContext.Current.Request.Cookies.AllKeys.Contains(sessionKey))
                {
                    return null;
                }

                HttpCookie cookie = HttpContext.Current.Request.Cookies[sessionKey];

                string data = Unzip(System.Convert.FromBase64String(cookie.Value));

                SsoAuthnState state = Symphony.Core.Utilities.Deserialize<SsoAuthnState>(data);

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(state.AuthnXml);

                state.AuthnRequest = new AuthnRequest(xml.DocumentElement);

                return state;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static void Destroy(string sessionKey)
        {
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains(sessionKey))
            {
                HttpCookie cookie = new HttpCookie(SamlParams.SsoSessionKey);
                cookie.Expires = DateTime.Now.AddDays(-365d);
                HttpContext.Current.Response.Cookies.Set(cookie);
            }

            if (HttpContext.Current.Request.Cookies.AllKeys.Contains("ssoKey"))
            {
                HttpCookie cookie = new HttpCookie("ssoKey");
                cookie.Value = null;
                HttpContext.Current.Response.Cookies.Set(cookie);
            }
            
        }
        public static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }
        public static byte[] Zip(string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);

            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    //msi.CopyTo(gs);
                    CopyTo(msi, gs);
                }

                return mso.ToArray();
            }
        }
        public static string Unzip(byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    //gs.CopyTo(mso);
                    CopyTo(gs, mso);
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }


    }
}