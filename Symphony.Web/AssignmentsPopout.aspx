﻿<%@ Page Title="" MasterPageFile="~/SymphonyMaster.Master" Language="C#" AutoEventWireup="true" CodeBehind="AssignmentsPopout.aspx.cs" Inherits="Symphony.Web.AssignmentsPopout" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <script type="text/javascript" src="/scripts/symphony/portal.js"></script>

    <script type="text/javascript">
        <asp:Literal ID="assignments" runat="server"></asp:Literal>
    </script>
    <script type="text/javascript" src="/scripts/symphony/Assignments/Popout.js"></script>
</asp:Content>