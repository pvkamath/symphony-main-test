﻿<%@ Page Title="Welcome to Symphony" MasterPageFile="~/SymphonyMaster.Master" Language="C#" AutoEventWireup="true" CodeBehind="SalesforceAPITest.aspx.cs" Inherits="Symphony.Web._Default" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    
    <style>
        .APITest 
        {
          clear: both;  
          padding-top: 10px; 
        }
    </style>


    <script type="text/javascript">
        var basePath = "/services/salesforce.svc/api/3/";
        var results = {};
        var time = new Date().getTime();
        var testData = {
            userPost: {
                firstName: "SF Test First1",
                lastName: "SF Test Last1",
                email: "andrew.mcgowan@authtestagain.com",
                addresses: [{
                    name: "Mailing",
                    street: "123 Main St",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }, {
                    name: "Other",
                    street: "123 Other Street",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "432890",
                    country: "USA"
                }],
                phone: "432-234-1234",
                mobile: "213-123-1233",
                fax: "257-234-2432",
                janrainUserUuid: "7ea21393-81e7-4739-8e3c-71050ea70660",
                salesforceContactId: "ABCDE" + time,
                salesforceAccountName: "OnCourse Learning",
                salesforceAccountId: "ZYXWV" + time,
                password: 'testpassword1',
                isAuthRequired: true
            },
            userPost1: {
                firstName: "Andrew-NewTestUser1",
                lastName: "McGowan-NewTestuser1",
                email: "andrew@NEWTESTUSER1-1.com",
                addresses: [{
                    name: "Mailing",
                    street: "123 Main St",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }, {
                    name: "Other",
                    street: "123 Other Street",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "432890",
                    country: "USA"
                }],
                phone: "432-234-1234",
                mobile: "213-123-1233",
                fax: "257-234-2432",
                janrainUserUuid: "JID1y9hfhn3489qph",
                salesforceContactId: "CID1y9hfhn3489qph",
                salesforceAccountName: "Salesforce",
                salesforceAccountId: "AID1y9hfhn3489qph"
            },
            userPost2: {
                firstName: "Janelle",
                lastName: "McGowan",
                email: "janelle@test.com",
                addresses: [{
                    name: "Mailing",
                    street: "123 Main St",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }, {
                    name: "Other",
                    street: "123 Other Street",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "432890",
                    country: "USA"
                }],
                phone: "432-234-1234",
                mobile: "213-123-1233",
                fax: "257-234-2432",
                janrainUserUuid: "12345678-1234-1234-4321-123456789123",
                salesforceContactId: "1235",
                salesforceAccountName: "Salesforce",
                salesforceAccountId: "5556"
            },
            userPostFail: {
                firstName: "Andrew",
                lastName: "McGowan",
                email: "mr.mcwake@gmail.com",
                addresses: [{
                    name: "Mailing",
                    street: "123 Main St",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }, {
                    name: "Other",
                    street: "123 Other Street",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "432890",
                    country: "USA"
                }],
                phone: "432-234-1234",
                mobile: "213-123-1233",
                fax: "257-234-2432",
                janrainUserUuid: "12345678-1234-1234-4321-123456789123",
                salesforceContactId: "ABCDE",
                salesforceAccountName: "OnCourse Learning",
                salesforceAccountId: "ZYXWV"
            },
            userPut: {
                firstName: "UPDATED SF Test First",
                salesforceContactId: "003a000001WFPGQAA5",
                lastName: "SF Test Last",
                email: "sftestuser@test.com",
                addresses: [{
                    name: "Updated Mailing",
                    street: "123 Main St",
                    city: "fdsaWaukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }, {
                    name: "Osdfsther",
                    street: "123 Other Street",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "432890",
                    country: "USA"
                }],
                phone: "432-234-1234",
                mobile: "213-123-1233",
                fax: "257-234-2432",
                isEnabled: true,
                password: 'testpassword1'
            },
            companyPost: {
                name: time + " SF Test Company",
                addresses: [{
                    name: "Billing",
                    street: "123 Main St\rSte 200",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }, {
                    name: "Shipping",
                    street: "123 Main St\rSte 200",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }],
                phone: "234-324-2342",
                fax: "234-234-2341",
                salesForceAccountId: "001a000001N8StsAAF",
                isEnabled: true
            },
            companyPostFail: {
                name: "be",
                addresses: [{
                    name: "Billing",
                    street: "123 Main St\rSte 200",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }, {
                    name: "Shipping",
                    street: "123 Main St\rSte 200",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }],
                phone: "234-324-2342",
                fax: "234-234-2341",
                salesForceAccountId: "001a000001N8StsAAF",
                isEnabled: true
            },
            companyPut: {
                name: "ABC New Company Updated",
                addresses: [{
                    name: "Billing new",
                    street: "123 Mainfdsa St\rSte 200",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }, {
                    name: "Shipping New",
                    street: "123 Main St\rSte 200",
                    city: "Waukesha",
                    state: "WI",
                    postalCode: "53188",
                    country: "USA"
                }],
                phone: "234-123-2342",
                fax: "234-234-2341",
                salesForceAccountId: "001a000001N8StsAAF",
                isEnabled: true
            },
            entitlementPut1: {
                entitlements: [{
                    entitlementId: 2,
                    salesforceEntitlementId: "SFE0001064",
                    salesforceProductId: "PRO00001064",
                    productId: "1063",
                    productCategory: "Course",
                    entitlementStartDate: "2017-01-01",
                    entitlementDuration: "12",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100001",
                    partnerId: 17,
                    partnerCode: "mcgowan7",
                    partnerDisplayName: "McGowan7"
                }]
            },
            registrationPost1: {
                registrantId: 9759,
                classId: 591,
            },
            registrationPost2: {
                registrantId: 420,
                classId: 680,
            },
            registrationPost3: {
                registrantId: 234524,
                classId: 680,
            },
            registrationPost4: {
                registrantId: 9758,
                classId: 43543543,
            },
            registrationPost5: {
                registrantId: 548543,
                classId: 435443,
            },
            registrationPost6: {
                registrantId: "5435afdsa",
                classId: 680,
            },
            registrationPost7: {
                registrantId: 9758,
                classId: "fdsafdsa",
            },
            registrationPost8: {
                registrantId: "fdsafsad",
                classId: "fsdafa",
            },
            registrationPost9: {
                classId: 680,
            },
            registrationPost10: {
                classId: 9758,
            },
            registrationPost10: {

            },
            entitlementPost2: {
                entitlements: [{
                    salesforceEntitlementId: "SF123456789",
                    salesforceProductId: "BN-4",
                    productId: "BN-4",
                    productCategory: "Bundle",
                    entitlementPurchaseDate: "2014-12-01",
                    entitlementDuration: "12",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100001",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementPost2: {
                entitlements: [{
                    salesforceEntitlementId: "SF123456789",
                    salesforceProductId: "BN-4",
                    productId: "BN-4",
                    productCategory: "Bundle",
                    entitlementPurchaseDate: "2014-12-01",
                    entitlementDuration: "12",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100001",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementPost5: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0001063",
                    salesforceProductId: "SF-BN-2",
                    productId: "SF-BN-2",
                    productCategory: "Bundle",
                    entitlementPurchaseDate: "2014-12-01",
                    entitlementDuration: "12",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100001",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementPost3: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0001063",
                    salesforceProductId: "SF-3",
                    productId: "SF-3",
                    productCategory: "Bundle",
                    entitlementPurchaseDate: "2014-12-01",
                    entitlementDuration: "12",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100001",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementPost4: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0001063",
                    salesforceProductId: "SF-2",
                    productId: "SF-2",
                    productCategory: "Bundle",
                    entitlementPurchaseDate: "2014-12-01",
                    entitlementDuration: "12",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100001",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementPost1: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0001063",
                    salesforceProductId: "123456",
                    productId: "432423",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2016-01-01",
                    entitlementDuration: "12",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100001",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementCopyTest: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0000001",
                    salesforceProductId: "PRO0000001",
                    productId: "938",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2014-08-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100001",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementDiffDateSamePartnerSameProduct: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0000009",
                    salesforceProductId: "PRO0000009",
                    productId: "938",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2015-09-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100011",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementOverlappingDateSamePartnerSameProduct: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0000019",
                    salesforceProductId: "PRO0000019",
                    productId: "938",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2014-09-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100011",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementDiffDateSamePartnerDiffProduct: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0000009",
                    salesforceProductId: "PRO0000009",
                    productId: "922",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2014-09-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100011",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementSameDateDiffPartner: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0000045",
                    salesforceProductId: "PRO0000045",
                    productId: "938",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2014-08-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100045",
                    partnerId: 45,
                    partnerCode: "mcgowan45",
                    partnerDisplayName: "McGowan45"
                }]
            },
            entitlementMergeTest: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0000002",
                    salesforceProductId: "PRO0000002",
                    productId: "939",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2014-08-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100002",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementMergeFail: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0000003",
                    salesforceProductId: "PRO0000003",
                    productId: "937",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2014-08-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100003",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementMergeFail2: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0000003",
                    salesforceProductId: "PRO0000003",
                    productId: "938",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2014-08-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100003",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementCopyDateChange: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0000004",
                    salesforceProductId: "PRO0000004",
                    productId: "940",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2014-09-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100004",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementCopyPartnerChange: {
                entitlements: [{
                    salesforceEntitlementId: "SFE0000005",
                    salesforceProductId: "PRO0000005",
                    productId: "921",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2014-08-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100005",
                    partnerId: 13,
                    partnerCode: "mcgowan1",
                    partnerDisplayName: "McGowan1"
                }]
            },
            entitlementPost: {
                entitlements: [{
                    salesforceEntitlementId: "a0Bf0000000E14v",
                    salesforceProductId: "01t30000002srKW",
                    productId: "922",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2014-06-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "123456789",
                    partnerCode: "windermere"
                }, {
                    salesforceEntitlementId: "a0Bf0000000E16v",
                    salesforceProductId: "01t30000002srJW",
                    productId: "920",
                    productCategory: "Live Lab",
                    entitlementPurchaseDate: "2014-06-02",
                    entitlementDuration: "12",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "123456789",
                    partnerCode: "windermere"
                }]
            },
            entitlementPutChangeProduct: {
                entitlements: [{
                    entitlementId: 0,
                    salesforceEntitlementId: "SFE0000001",
                    salesforceProductId: "PRO0000001",
                    productId: "922",
                    productCategory: "Course",
                    entitlementPurchaseDate: "2016-11-02",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "100001",
                    partnerId: 12,
                    partnerCode: "mcgowan",
                    partnerDisplayName: "McGowan"
                }]
            },
            entitlementPut: {
                entitlements: [{
                    entitlementId: 0,
                    salesforceEntitlementId: "a0Bf0000000E14v",
                    salesforceProductId: "01t30000002srKW",
                    productId: "922",
                    productCategory: "Courdfse",
                    entitlementPurchaseDate: "2013-02-01",
                    entitlementDuration: "4",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "123456789",
                    partnerCode: "windermere",
                    partnerId: 141,
                    partnerDisplayName: "Windermere"
                }, {
                    entitlementId: 0,
                    salesforceEntitlementId: "a0Bf0000000E16v",
                    salesforceProductId: "01t30000002srJW",
                    productId: "920",
                    productCategory: "Livefdsa Lab",
                    entitlementPurchaseDate: "2013-02-01",
                    entitlementDuration: "8",
                    entitlementDurationUnits: "Months",
                    isEnabled: true,
                    paymentRefId: "123456789",
                    partnerCode: "windermere",
                    partnerId: 141,
                    partnerDisplayName: "Windermere"
                }]
            },
            entitlementDelete: {
                entitlements: [{
                    entitlementId: "114"
                }]
            }
        };
         
        function RunTest(userId, customerId, method, path, data) {

        }

        function runTest(tests, id) {
            if (tests.length > id) {
                var test = $(tests[id]);
                var url = basePath + test.attr('url');
                var type = test.attr('type');
                var dataKey = test.attr('data');
                var rData = testData.hasOwnProperty(dataKey) ? testData[dataKey] : null;
                var object = test.attr('object');
                var idSpecified = test.attr('idSpecified');

                if (object === 'entitlement') {
                    if (url.indexOf("{id}") > -1) {
                        url = url.replace("{id}", results.user.post.userId);
                    }

                    if (results.hasOwnProperty('entitlement')) {
                        if (results.entitlement.hasOwnProperty('post')) {
                            var entitlementId = results.entitlement.post.entitlements[0].entitlementId;
                            url = url.replace("{entitlementId}", entitlementId);
                        }
                    }

                    if (type === 'delete' && !idSpecified) {
                        if (url.indexOf("{id}") > -1) {
                            rData = {
                                entitlements: []
                            }
                            for (i = 0; i < results.entitlement.post.entitlements.length; i++) {
                                rData.entitlements.push({ entitlementId: results.entitlement.post.entitlements[i].entitlementId });
                            }
                        }
                    } else if (type === 'put') {
                        
                        for (i = 0; i < rData.entitlements.length; i++) {
                            rData.entitlements[i].entitlementId = rData.entitlements[i].entitlementId == 0
                                ? results.entitlement.post.entitlements[i].entitlementId
                                : rData.entitlements[i].entitlementId;
                        }
                    }
                } else {
                    if (type === 'delete' && !idSpecified) {
                        switch (object) {
                            case 'user':
                                var lastUserId = results.user.post.userId;
                                url += lastUserId;
                                break;
                            case 'company':
                                var lastCompanyId = results.company.post.companyId;
                                url += lastCompanyId;
                                break;
                        }
                    }
                }

                $.ajax({
                    type: type,
                    data: JSON.stringify(rData),
                    contentType: 'application/json',
                    dataType: 'json',
                    url: url,
                    headers: {
                        "X-Symphony-Target-Customer": "1"
                    },
                    success: function (data, status, xhr) {
                        var count = 1;
                        if (data.hasOwnProperty('products')) {
                            count = data.products.length;
                        } else if (data.hasOwnProperty('entitlements')) {
                            count = data.entitlements.length;
                        }

                        var responseData = '<div style="width: 50%; float: left"><h4>Response Data (Count: ' + count + '):</h4><pre>' + JSON.stringify(data, null, 4) + '</pre></div>';
                        var requestData = '<div style="width: 50%; float: left"><h4>Request Data:</h4><pre>' + JSON.stringify(rData, null, 4) + '</pre></div>';
                        var request = '<div style="width: 100%"><h1 style="font-size: 18px; padding-top: 10px; border-top: 1px solid black; margin-top: 20px;">Request: ' + type.toUpperCase() + ' ' + url + '<span style="display: block; padding: 5px; float: right; background: lightgreen">' + xhr.status + ': ' + status + '</span></h1> </div>';

                        test.append(request + requestData + responseData);

                        if (!results.hasOwnProperty(object)) {
                            results[object] = {};
                        }
                        results[object][type] = data;
                    },
                    error: function (xhr, status, error) {
                        var count = 1;
                        var data = xhr.responseJSON;

                        if (data && data.hasOwnProperty('products')) {
                            count = data.products.length;
                        } else if (data && data.hasOwnProperty('entitlements')) {
                            count = data.entitlements.length;
                        }
                        var request = '<div style="width: 100%"><h1 style="font-size: 18px; padding-top: 10px; border-top: 1px solid black; margin-top: 20px;">Request: ' + type.toUpperCase() + ' ' + url + '<span style="display: block; padding: 5px; float: right; background: #D66">' + xhr.status + ': ' + status + '</span></h1> </div>';
                        var requestError = '<div style="width: 100%">' + status + '<br>' + error + '</div>';
                        var responseData = '<div style="width: 50%; float: left"><h4>Response Data (Count: ' + count + '):</h4><pre>' + JSON.stringify(data, null, 4) + '</pre></div>';
                        var requestData = '<div style="width: 50%; float: left"><h4>Request Data:</h4><pre>' + JSON.stringify(rData, null, 4) + '</pre></div>';


                        test.append(request + requestError + requestData + responseData);
                    },
                    complete: function () {
                        runTest(tests, id + 1);
                    }
                });
            }
        }

        $(document).ready(function () {
            var apiTests = $(".APITest");
            runTest(apiTests, 0);
        });
    </script>
    
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="tests" style="width: 70%">
        
        <div class="APITest" object="user" url="user/" type="post" data="userPost">

        <!--<div class="APITest" object="entitlement" url="user/9759/entitlement" type="post" data="entitlementPost2"></div>

        <div class="APITest" object="entitlement" url="user/9760/entitlement" type="post" data="entitlementPost2"></div>

        <div class="APITest" object="entitlement" url="user/9759/entitlement/114" type="delete" data="entitlementDelete" idSpecified="true"></div>
        -->

        <!-- Product Details have checked out
        <div class="APITest" object="product" url="product/SF-1" type="get"></div>
        <div class="APITest" object="product" url="product/session" type="get"></div>
        <div class="APITest" object="product" url="product?since=2012/01/01" type="get"></div>
        <div class="APITest" object="product" url="product/session?since=2012/01/01" type="get"></div>
        -->
        <!-- ENTITLEMENT CREATION CHECKS HAVE CHECKED OUT!!-->

        <!--<div class="APITest" object="user" url="user/" type="post" data="userPost1"></div>
        <div class="APITest" object="user" url="user/" type="post" data="userPost2"></div>

        <div class="APITest" object="entitlement" url="user/9772/entitlement" type="post" data="entitlementPost2"></div>
        <div class="APITest" object="entitlement" url="user/9772/entitlement" type="post" data="entitlementPost3"></div>-->
        <!-- SHould merge -->
        <!--<div class="APITest" object="entitlement" url="user/9772/entitlement" type="post" data="entitlementPost4"></div>
        <!-- Should not merge --> 
        <!--<div class="APITest" object="entitlement" url="user/9772/entitlement" type="post" data="entitlementPost5"></div>

        <div class="APITest" object="entitlement" url="user/9772/entitlement" type="get"></div>
        <div class="APITest" object="entitlement" url="user/9772/entitlement/45" type="get" idSpecified="true"></div>

        <div class="APITest" object="entitlement" url="user/9772/entitlement/45" type="delete" data="entitlementDelete" idSpecified="true"></div>
        <!-- USER TESTS HAVE CHECKED OUT 
        <div class="APITest" object="user" url="user/" type="post" data="userPost"></div>
        <div class="APITest" object="user" url="user/9763" type="get"></div>
        <div class="APITest" object="user" url="user/9769" type="put" data="userPut"></div>
        <div class="APITest" object="user" url="user/9763" type="get"></div>
        <div class="APITest" object="user" url="user/9761" type="put" data="userPut"></div>
        <div class="APITest" object="user" url="user/" type="delete"></div>
        <div class="APITest" object="user" url="user/9761" type="delete" idSpecified="true"></div>
        -->
        <!-- COMPANY TESTS HAVE CHECKED OUT 
        <div class="APITest" object="company" url="company/1" type="get"></div>
        <div class="APITest" object="company" url="company" type="post" data="companyPost"></div>
        <div class="APITest" object="company" url="company/140" type="put" data="companyPut"></div>
        <div class="APITest" object="company" url="company/" type="delete"></div>
        <div class="APITest" object="company" url="company/140" type="delete" idSpecified="true"></div>
        -->














        <!--<div class="APITest" object="product" url="product" type="get"></div>
        <div class="APITest" object="product" url="product/SKU102393-HG1" type="get"></div>
        <div class="APITest" object="product" url="product/session" type="get"></div>
        <div class="APITest" object="product" url="product?since=2012/01/01" type="get"></div>
        <div class="APITest" object="product" url="product/session?since=2012/01/01" type="get"></div>


        <div class="APITest" object="entitlement" url="user/9756/entitlement" type="post" data="entitlementPost1"></div>

        <div class="APITest" object="trainingprogram" url="product/details/SKU102393-HG1" type="get"></div>
        <div class="APITest" object="registration" url="registration/SKU102393-HG1" type="post" data="registrationPost1"></div>
        <div class="APITest" object="registration" url="registration/SKU102393-HG1/591" type="get"></div>-->
        


        <!--<div class="APITest" object="trainingprogram" url="trainingprogram/1134" type="get"></div>
        <div class="APITest" object="registration" url="registration/1134/680" type="get"></div>
        <div class="APITest" object="registration" url="registration/1134" type="post" data="registrationPost1"></div>
        <div class="APITest" object="registration" url="registration/1134" type="post" data="registrationPost2"></div>
        <div class="APITest" object="registration" url="registration/1134" type="post" data="registrationPost3"></div>
        <div class="APITest" object="registration" url="registration/1134" type="post" data="registrationPost4"></div>
        <div class="APITest" object="registration" url="registration/1134" type="post" data="registrationPost5"></div>
        <div class="APITest" object="registration" url="registration/1134" type="post" data="registrationPost6"></div>
        <div class="APITest" object="registration" url="registration/1134" type="post" data="registrationPost7"></div>
        <div class="APITest" object="registration" url="registration/1134" type="post" data="registrationPost8"></div>
        <div class="APITest" object="registration" url="registration/1134" type="post" data="registrationPost9"></div>
        <div class="APITest" object="registration" url="registration/1134" type="post" data="registrationPost10"></div>-->
        <!--<div class="APITest" object="product" url="product?since=2012/01/01" type="get"></div>
        <div class="APITest" object="product" url="product/session?since=2012/01/01" type="get"></div>


        <!--<div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementPost1"></div>-->
        
        <!--<div class="APITest" object="entitlement" url="user/9758/entitlement/1" type="delete" data="entitlementDelete"></div>
        <!--<div><h3>TP: 938, Date: 2014-08-02, Partner: mcgowan<br />New Entitlement - Should create a copy of the training program and return the new training program id</h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementCopyTest"></div>
        <div><h3>TP: 938, Date: 2015-09-02, Partner: mcgowan<br />Different date, same partner, same product ID - should create another new copy of the training program (ID returned should be different than above)</h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementDiffDateSamePartnerSameProduct"></div>
        <div><h3>TP: 938, Date: 2014-09-02, Partner: mcgowan<br />Overlapping Date, Same partner, same product id - again, should create another new copy of the training program</h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementOverlappingDateSamePartnerSameProduct"></div>
        <div><h3>TP: 922, Date: 2014-09-02, Partner: mcgowan<br />Different date, same partner, different product id - should just create a new copy</h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementDiffDateSamePartnerDiffProduct"></div>
        <div><h3>TP: 938, Date: 2014-08-02, Partner: mcgowan45<br />Same date, different partner, same product id - should create another new copy</h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementSameDateDiffPartner"></div>
        <div><h3>TP: 939, Date: 2014-08-02, Partner: mcgowan45<br />Different product, same date, same partner - should merge</h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementMergeTest"></div>
        <div><h3>TP: 937, Date: 2014-08-02, Partner: mcgowan<br />Different product, same date, same partner, same courses. Will not merge. Will return existing entitlement </h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementMergeFail"></div>
        <div><h3>TP: 938, Date: 2014-08-02, Partner: mcgowan<br />Same product, same date, same partner, will not merge. </h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementMergeFail2"></div>
        <div><h3>TP: 940, Date: 2014-09-02, Partner: mcgowan<br />New product, new date, same partner, Exepected: Success - NEW TP </h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementCopyDateChange"></div>
        <div><h3>TP: 921, Date: 2014-08-02, Partner: mcgowan1<br />New product, same date, new partner. Expected: Succeess - NEW TP </h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement" type="post" data="entitlementCopyPartnerChange"></div>
        <div><h3>Change Existing product on an entitlement to a different product </h3></div>
        <div class="APITest" object="entitlement" url="user/9758/entitlement/{entitlementId}" type="put" data="entitlementPutChangeProduct"></div>
        -->
        <!--<div class="APITest" object="user" url="user/9758" type="get"></div>
        <div class="APITest" object="user" url="user/" type="post" data="userPost"></div>
        <div class="APITest" object="user" url="user/9763" type="put" data="userPut"></div>
        <div class="APITest" object="user" url="user/" type="delete"></div>

        <div class="APITest" object="user" url="user/" type="post" data="userPostFail"></div>
        <div class="APITest" object="user" url="user/43721980" type="put" data="userPut"></div>
        <div class="APITest" object="user" url="user/abacasdf" type="put" data="userPut"></div>

        <div class="APITest" object="company" url="company/1" type="get"></div>
        <div class="APITest" object="company" url="company" type="post" data="companyPost"></div>
        <div class="APITest" object="company" url="company/140" type="put" data="companyPut"></div>
        <div class="APITest" object="company" url="company/" type="delete"></div>
        <div class="APITest" object="company" url="company" type="post" data="companyPostFail"></div>
        <div class="APITest" object="product" url="product" type="get"></div>
      
        
        <div class="APITest" object="product" url="product/940" type="get"></div>
        <div class="APITest" object="product" url="product/session" type="get"></div>
        <div class="APITest" object="product" url="product?since=2012/01/01" type="get"></div>
        <div class="APITest" object="product" url="product/session?since=2012/01/01" type="get"></div>

        <div class="APITest" object="entitlement" url="user/{id}/entitlement" type="post" data="entitlementPost"></div>
        <div class="APITest" object="entitlement" url="user/{id}/entitlement" type="get"></div>
        <div class="APITest" object="entitlement" url="user/{id}/entitlement/{entitlementId}" type="get"></div>
        <div class="APITest" object="entitlement" url="user/{id}/entitlement/{entitlementId}" type="put" data="entitlementPut"></div>
        <div class="APITest" object="entitlement" url="user/{id}/entitlement/{entitlementId}" type="delete" data="entitlementDelete"></div>
 -->
    </div>
</asp:Content>
