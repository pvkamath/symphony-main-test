﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Symphony.Core;
using System.Configuration;
using ComponentPro.Saml;
using Symphony.Web.Saml;
using System.Threading;
using Hangfire;

namespace Symphony.Web
{
    public class Global : System.Web.HttpApplication
    {
        FM.IceLink.Server iceLinkServer;

        static log4net.ILog Log = log4net.LogManager.GetLogger("App_Start");

        System.Threading.Timer SAMLCacheTimer;

        protected void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            HttpContext.Current.Application[Symphony.Web.ApplicationProcessor.APPLICATION_LOCKER] = new object();
            log4net.Config.XmlConfigurator.Configure();

            Symphony.Core.Controllers.ReportController.EnableSchedules();

            SamlSettings.CacheProvider = new SamlDatabaseCacheProvider();

            if (ConfigurationManager.AppSettings["EnableIcelink"] == "True")
            {
                iceLinkServer = new FM.IceLink.Server();
                iceLinkServer.EnableRelay((r) =>
                {
                    if (r.Username == "test")
                    {
                        return new FM.IceLink.RelayAuthenticateResult(FM.IceLink.STUN.CreateLongTermKey(r.Username, r.Realm, "pa55w0rd!"));
                    }
                    return null;
                });

                System.Threading.ThreadPool.QueueUserWorkItem(args =>
                {
                    while (true)
                    {
                        try
                        {

                            iceLinkServer.Start();

                            Log.Warn("Icelink server has started");
                            break;
                        }
                        catch
                        {
                            Log.Warn("Icelink server could not start... retrying");
                        }

                        new System.Threading.AutoResetEvent(false).WaitOne();

                        System.Threading.Thread.Sleep(1000);
                    }
                });
            }

            
            EnumManager.RegisterEnum<ArtisanAssetType>();
            EnumManager.RegisterEnum<ArtisanPageType>();
            EnumManager.RegisterEnum<ArtisanQuestionType>();
            EnumManager.RegisterEnum<CourseCompletionType>();
            EnumManager.RegisterEnum<CourseType>();
            EnumManager.RegisterEnum<HierarchyType>();
            EnumManager.RegisterEnum<ImportType>();
            EnumManager.RegisterEnum<RegistrationStatusType>();
            EnumManager.RegisterEnum<RegistrationType>();
            EnumManager.RegisterEnum<SyllabusType>();
            EnumManager.RegisterEnum<CategoryType>();
            EnumManager.RegisterEnum<UserStatusType>();
            EnumManager.RegisterEnum<ArtisanSectionType>();
            EnumManager.RegisterEnum<ArtisanCourseCompletionMethod>();
            EnumManager.RegisterEnum<ArtisanCourseTimeoutMethod>();
            EnumManager.RegisterEnum<ArtisanCourseMarkingMethod>();
            EnumManager.RegisterEnum<ArtisanCourseReviewMethod>();
            EnumManager.RegisterEnum<ArtisanTestType>();
            EnumManager.RegisterEnum<ArtisanCourseNavigationMethod>();
            EnumManager.RegisterEnum<RetestMode>();
            EnumManager.RegisterEnum<EventType>();
            EnumManager.RegisterEnum<ReportSchedule>();
            EnumManager.RegisterEnum<ReportStatus>();
            EnumManager.RegisterEnum<MessageBoardType>();
            EnumManager.RegisterEnum<SsoType>();
            EnumManager.RegisterEnum<CertificateType>();
            EnumManager.RegisterEnum<AssignmentResponseStatus>();
            EnumManager.RegisterEnum<TrainingProgramBundleStatus>();
            EnumManager.RegisterEnum<SsoLoginUiType>();
            EnumManager.RegisterEnum<RelativeTimeMode>();
            EnumManager.RegisterEnum<CourseUnlockMode>();
            EnumManager.RegisterEnum<SessionStatus>();
            EnumManager.RegisterEnum<ReportSystem>();
            EnumManager.RegisterEnum<LibraryFilterType>();
            EnumManager.RegisterEnum<ReportSystem>();
            EnumManager.RegisterEnum<LicenseExpirationRule>();
            EnumManager.RegisterEnum<ArtisanMasteryLevel>();
            EnumManager.RegisterEnum<ArtisanContentType>();
            EnumManager.RegisterEnum<RedirectType>();
            EnumManager.RegisterEnum<ExternalSystemFormType>();
            EnumManager.RegisterEnum<ScheduleParameterOptions>();
            EnumManager.RegisterEnum<ComparisonOperators>();
            EnumManager.RegisterEnum<EntityType>();
            EnumManager.RegisterEnum<FieldType>();
            EnumManager.RegisterEnum<ArtisanCourseBookmarkingMethod>();

            EnumManager.SerializePrefix = "Symphony.";

            DisplayValue.BuildLookupDictionaries();
            Symphony.Web.Saml.Util.LoadCertificates(Application);

            try
            {
                string timerIntervalValue = ConfigurationManager.AppSettings["SamlCacheCleanUpIntervalInMS"];
                if (string.IsNullOrEmpty(timerIntervalValue))
                {
                    timerIntervalValue = "3600000";
                }

                int timerInterval = int.Parse(timerIntervalValue);
                var StateObj = new Object(); // empty object as we are running this for the life of the app.

                System.Threading.TimerCallback SAMLCacheCallback = new System.Threading.TimerCallback(SamlDatabaseCacheProvider.CleanSAMLCache);

                SAMLCacheTimer = new System.Threading.Timer(SAMLCacheCallback, StateObj, timerInterval, timerInterval);
            }
            catch (Exception ex)
            {
                Log.Error("Error in SAMLCache Timer: ", ex);
            }
            

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
            try
            {
                iceLinkServer.Stop();
            }
            catch { }
        }
    }
}