﻿<%@ Page Title="Symphony Reset Password" Language="C#" MasterPageFile="~/Unauthenticated.Master" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Symphony.Web.ResetPassword" %>
<asp:Content ID="Header" ContentPlaceHolderID="Header" runat="server">

    <script type="text/javascript">
        Ext.onReady(function() {
            var w = new Ext.Window({
                contentEl: Ext.get('wrapper'),
                closable: false,
                padding: '15'
            });
            w.show();
        });
    </script>

    <style type="text/css">
        <style type="text/css">
        .success
        {
            color:#00ff00;
        }
        .error
        {
            color:Red;
        }
    </style>
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="Body" runat="server">
    <div id="wrapper" class="x-hidden">
        <form id="Form1" runat="server">
        <h3>
            Reset Password</h3>
        <br />
        <table id="input" runat="server">
            <tr>
                <td><label for="password1">New Password: </label></td>
                <td><asp:TextBox TextMode="Password" ID="password1" runat="server" /></td>
            </tr>
            <tr>
                <td><label for="password2">Confirm Password: </label></td>
                <td><asp:TextBox TextMode="Password" ID="password2" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="2" align="right"><asp:Button ID="reset" Text="Reset" runat="server" OnClick="Reset" /></td>
            </tr>
        </table>
        
        <asp:Literal ID="error" runat="server"></asp:Literal>
        <asp:Literal ID="success" runat="server"></asp:Literal>
        </form>
    </div>
</asp:Content>

