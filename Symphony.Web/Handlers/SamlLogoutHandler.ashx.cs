﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Symphony.Web.Saml;
using ComponentPro.Saml2;
using log4net;
using Symphony.Core.Controllers;
using Symphony.Core.Models;
using System.Security.Cryptography.X509Certificates;
using Data = Symphony.Core.Data;

namespace Symphony.Web.Handlers
{
    public class SamlLogoutHandler : IHttpHandler, IRequiresSessionState
    {

        private ILog Log = LogManager.GetLogger(typeof(SamlLogoutHandler));

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                // http://xacmlinfo.org/2013/06/28/how-saml2-single-logout-works/
                
                X509Certificate2 x509Certificate = (X509Certificate2)context.Application[Saml.SamlParams.SPCertKey];

                // No longer verifying the signature
                //LogoutRequest logoutRequest = LogoutRequest.Create(context.Request, x509Certificate.PublicKey.Key);

                UserController.Logout(context);

                LogoutResponse logoutResponse = new LogoutResponse();
                logoutResponse.Issuer = new Issuer(Util.GetAbsoluteUrl(context, "~/"));
                logoutResponse.Status = new Status(SamlPrimaryStatusCode.Success, null);

                string logoutUrl = "";
                string relayState = "";
                if (context.Request.QueryString.AllKeys.Contains("RelayState"))
                {
                    relayState = context.Request.QueryString["RelayState"];
                }

                if (!string.IsNullOrEmpty(relayState))
                {
                    if (relayState.StartsWith("http") || relayState.StartsWith("//"))
                    {
                        logoutUrl = relayState;
                    }
                }

                // TODO: add lookup of service provider (logout url), then customer (sso timeout url), and use those as fallbacks if relaystate is not specified;
                // use the order relayState, customer logout url, customer timeout url

                if (string.IsNullOrEmpty(logoutUrl))
                {
                    throw new Exception("Could not determine logout url for the service provider. Please include the logout url as the \"RelayState\" query paramter in the request.");
                }

                if (logoutUrl.IndexOf("?") == -1)
                {
                    // ensure a query param - bug in the component pro lib if the redirect url has no query params
                    logoutUrl += "?";
                }

                logoutResponse.Redirect(context.Response, logoutUrl, logoutUrl, x509Certificate.PrivateKey);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw e;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}