﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Web;
using System.IO;
using log4net;
using System.Text;
using EcmaScript.NET;

namespace Symphony.Web.Handlers
{

    public class YUIErrorReporter : ErrorReporter
    {
        ILog Log = LogManager.GetLogger(typeof(ErrorReporter));

        public void Error(string message, string sourceName, int line, string lineSource, int lineOffset)
        {
            Log.Error(message + " in " + sourceName + " at line " + line + " line source " + lineSource + " offset " + lineOffset);
        }
        public EcmaScriptRuntimeException RuntimeError(string message, string sourceName, int line, string lineSource, int lineOffset)
        {
            return new EcmaScriptRuntimeException(new Exception(message + " in " + sourceName + " at line " + line + " line source " + lineSource + " offset " + lineOffset));
        }
        public void Warning(string message, string sourceName, int line, string lineSource, int lineOffset)
        {
            Log.Warn(message + " in " + sourceName + " at line " + line + " line source " + lineSource + " offset " + lineOffset);
        }
    }

    /// <summary>
    /// Summary description for Scripts
    /// </summary>
    public class Scripts : IHttpHandler
    {
        ILog Log = LogManager.GetLogger(typeof(Scripts));

        private const string TargetPath = "/scripts/{0}.txt";

        private static ConcurrentDictionary<string, string> _cache = new ConcurrentDictionary<string, string>();
        private static ConcurrentDictionary<string, DateTime> _dtCached = new ConcurrentDictionary<string, DateTime>();

        private static object _dtCachedLock = new object();
        private static object _cacheLock = new object();


        public void ProcessRequest(HttpContext context)
        {
            bool debug = false;
            bool refresh = false;
            bool log = false;
            bool compress = true;
            
            try
            {
                string output = string.Empty;
                string target = context.Request.QueryString["target"];
                string path = context.Server.MapPath(string.Format(TargetPath, target));

                // attempt to parse the query string debug param
                bool.TryParse(context.Request.QueryString["debug"], out debug);
                bool.TryParse(context.Request.QueryString["refresh"], out refresh);
                bool.TryParse(context.Request.QueryString["log"], out log);
                bool.TryParse(context.Request.QueryString["compress"], out compress);

#if DEBUG
                debug = true;
                
                if (context.Request.QueryString.AllKeys.Contains("compress")) {
                    log = true;
                    debug = false;
                }
#endif

                if (!File.Exists(path))
                {
                    throw new Exception("The specified target is invalid.");
                }

                if (debug)
                {
                    output = ReadScripts(context, path);
                }
                else
                {
                    bool empty = false;

                    lock (_dtCachedLock) {
                        empty = !_dtCached.ContainsKey(target) || File.GetLastWriteTimeUtc(path) > _dtCached[target];
                    }

                    if (empty || refresh)
                    {
                        output = ReadScripts(context, path);

                        Yahoo.Yui.Compressor.JavaScriptCompressor compressor = new Yahoo.Yui.Compressor.JavaScriptCompressor();

                        if (compress)
                        {
                            compressor.CompressionType = Yahoo.Yui.Compressor.CompressionType.Standard;
                        }
                        else
                        {
                            compressor.CompressionType = Yahoo.Yui.Compressor.CompressionType.None;
                        }

                        if (log)
                        {
                            compressor.LoggingType = Yahoo.Yui.Compressor.LoggingType.Debug;
                            compressor.ErrorReporter = new YUIErrorReporter();
                        }     

                        //compressor.DisableOptimizations = true;
                        lock (_cacheLock)
                        {
                            output = compressor.Compress(output);
                            _cache.AddOrUpdate(target, output, (k, v) => output);
                        }

                        lock (_dtCachedLock)
                        {
                            _dtCached.AddOrUpdate(target, DateTime.UtcNow, (k, v) => DateTime.UtcNow);
                        }
                    }
                    else
                    {
                        lock (_cacheLock)
                        {
                            output = _cache[target];
                        }
                    }
                    // cache for 30 days
                    AddCacheHeaders(context, new TimeSpan(30, 0, 0, 0));
                }

                context.Response.ContentType = "text/javascript";
                context.Response.Write(output);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                context.Response.Write("Failed to load JS: " + ex.Message);
            }
        }

        /// <summary>
        /// Adds appropriate caching headers to the context response
        /// </summary>
        /// <param name="context">The context to modify</param>
        /// <param name="refresh">How long to cache the content</param>
        private void AddCacheHeaders(HttpContext context, TimeSpan refresh)
        {
            context.Response.Expires = (int)refresh.TotalMinutes;
            context.Response.Cache.SetExpires(DateTime.Now.Add(refresh));
            context.Response.Cache.SetMaxAge(refresh);
            context.Response.Cache.SetCacheability(HttpCacheability.Private);
            context.Response.Cache.SetValidUntilExpires(true);
        }

        /// <summary>
        /// Reads in the specified path, splits on new lines, and then reads in the contents of each file listed
        /// </summary>
        /// <param name="context">Current HTTP context</param>
        /// <param name="path">The path to a text file containing a list of js files to be read</param>
        /// <returns>The contents of the files listed in the file specified by path</returns>
        private string ReadScripts(HttpContext context, string path, bool debug = false)
        {
            StringBuilder sb = new StringBuilder();
            string[] files = File.ReadAllLines(path);
            foreach (string file in files)
            {
                if (file.Trim().StartsWith("#"))
                {
                    continue;

                }
                if (!string.IsNullOrEmpty(file.Trim()))
                {
                    string temp = context.Server.MapPath(file);
                    if (File.Exists(temp))
                    {
                        if (debug)
                        {
                            string scriptTemplate = "<script type='text/javascript' src='{0}'></script>";
                            sb.AppendFormat(scriptTemplate, file);
                        }
                        else
                        {
                            string js = File.ReadAllText(temp);
                            sb.AppendLine(js);
                        }
                    }
                    else
                    {
                        throw new Exception(temp);
                        //Log.Warn("File " + temp + " was not found.");
                    }
                }
            }
            return sb.ToString();
        }

        public string GenerateScripts(HttpContext context, string target)
        {
            string path = context.Server.MapPath(string.Format(TargetPath, target));
            
            if (!File.Exists(path))
            {
                throw new Exception("The specified target is invalid.");
            }

            return ReadScripts(context, path, true);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}