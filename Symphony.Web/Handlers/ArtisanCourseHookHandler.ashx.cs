﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Symphony.Core.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using log4net;
using SubSonic;
using Symphony.Core.Controllers;
using Models = Symphony.Core.Models;
using System.Reflection;

using Symphony.Core;

using System.IO;
using Newtonsoft.Json;

namespace Symphony.Web.Handlers
{
    /// <summary>
    /// Responsible for any on-the-fly alterations of this course's settings
    /// 
    /// TODO: Make this use ArtisanController.GetArtisanHookJS
    /// ArtisanController.GetArtisanHookJS is used to generate the js hook file
    /// for bulk downloadable courses (So parameters can be set and applied accross the board to all courses)
    /// Since it was close to release the regular handler was left alone
    /// to avoid creating any bugs here.
    /// 
    /// NOTE!!! If we are changing this file and GetArtisanHookJS is not being used, we should update GetArtisanHookJS as well!!!!
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ArtisanCourseHookHandler : IHttpHandler
    {
        ILog Log = LogManager.GetLogger(typeof(ArtisanCourseHookHandler));

        public void ProcessRequest(HttpContext context)
        { 
            try
            {
                string web_path = context.Request["href"];
                string web_path_without_domain = string.Empty;
                bool needsInit = false;

                if (!string.IsNullOrEmpty(web_path))
                {
                    // get the path without whatever file we're looking at, just care about the folder
                    web_path = web_path.Substring(0, web_path.LastIndexOf("/"));

                    try
                    {
                        Uri uri = new Uri(web_path);
                        web_path_without_domain = uri.AbsolutePath;
                    }
                    catch (Exception ex)
                    {
                        Log.WarnFormat("Failed to determine URI from web_path: {0}, error was: {1}", web_path, ex.Message);
                    }

                    int registrationTrainingProgramId = 0;
                    int registrationUserId = 0;
                    string registration = context.Request.QueryString["registration"]; // Added to the request by Artisan.Utilities.js
                    bool isForcedUser = false;
                    bool isLMSNone = false; // Security measure to disconnect the lms if the registration user id is different from the actual user id. 
                    
                    // Registration MIGHT be null if this is an old course with an old Artisan.Utitlities.js
                    if (!string.IsNullOrEmpty(registration))
                    {
                        string[] parts = registration.Split('!');
                        foreach (string p in parts)
                        {
                            string[] keyValuePair = p.Split('|');

                            if (keyValuePair.Count() > 1 && keyValuePair[0] == "TrainingProgramId")
                            {
                                int.TryParse(keyValuePair[1], out registrationTrainingProgramId);
                            } 
                            else if (keyValuePair.Count() > 1 && keyValuePair[0] == "UserId") 
                            {
                                int.TryParse(keyValuePair[1], out registrationUserId);
                            }
                        }
                    }

                   
                    SymphonyController symphony = new SymphonyController();

                    int actualUserId = symphony.ActualUserID; // The actual user - not the shadowed user - the registration will be under the actual user 
                    int shadowUserId = symphony.UserID;       // The user we are pretending to be (In most cases this will be the same as the regular user id)

                    if (symphony.ActualUserID != registrationUserId && registrationUserId > 0)
                    {
                        // If we have registration id and it is not equal to the actual user
                        // it would have already been validated by deliver.aspx.
                        // Override the current user with the registration
                        actualUserId = registrationUserId;
                        shadowUserId = registrationUserId;
                        isForcedUser = true;
                    }

                    User user = new User(actualUserId);
                    
                    User shadowUser = new User(shadowUserId);
                    // We want the customer information from the user we are pretending to be so we can test different customers easily
                    // This is only used for loading customer overrides.
                    Customer customer = new Customer(shadowUser.CustomerID);
                    // The actual customer - just for setting the start time for the actual user
                    Customer actualCustomer = new Customer(user.CustomerID);

                    

                    int courseId = 0, trainingProgramId = 0;
                    context.Response.ContentType = "text/javascript";
                    // look up the course in Rustici via the web_path property
                    ConnectionStringSettings cns = ConfigurationManager.ConnectionStrings["OnlineTraining"];
                    if (cns == null)
                    {
                        throw new Exception("The 'OnlineTraining' connection string is not defined.");
                    }
                    using (SqlConnection conn = new SqlConnection(cns.ConnectionString))
                    {
                        

                        string sql = @"
select 
	r.course_id, r.training_program_id, r.[user_id]
from
	scormregistration r
join
	scormpackage
on
	scormpackage.scorm_package_id = r.scorm_package_id 
where 
    (web_path like '%' + @web_path1 + '%' or web_path like '%' + @web_path2 + '%') and [user_id] = @user_id";

                        if (registrationTrainingProgramId > 0)
                        {
                            sql += " and [training_program_id] = @training_program_id";
                        }


                        using (SqlCommand cmd = new SqlCommand(sql))
                        {
                            cmd.Connection = conn;
                            cmd.Parameters.AddWithValue("@web_path1", web_path);
                            cmd.Parameters.AddWithValue("@web_path2", web_path_without_domain);
                            cmd.Parameters.AddWithValue("@user_id", user.Id);

                            if (registrationTrainingProgramId > 0)
                            {
                                cmd.Parameters.AddWithValue("@training_program_id", registrationTrainingProgramId);
                            }

                            cmd.Connection.Open();

                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                courseId = reader.GetInt32(0);
                                trainingProgramId = reader.GetInt32(1);
                            }
                            cmd.Connection.Close();

                            if (courseId == 0)
                            {
                                Log.WarnFormat("Failed to find a course for the paths {0} and {1} and the user {2}.", web_path, web_path_without_domain, user.Id);
                                // bail out
                                return;
                            }
                        }
                    }


                    OnlineCourse oc = new OnlineCourse(courseId);
                    ArtisanAudit deliveredCourseVersion;
                    ArtisanAudit currentCourseVersion;
                    ArtisanCourse artisanCourse = null;

                    if (oc.Id > 0)
                    {
                        string packageGuid = Utilities.ExtractGuid(web_path_without_domain);
                        
                        deliveredCourseVersion = Select.AllColumnsFrom<ArtisanAudit>()
                            .Where(ArtisanAudit.PackageGuidColumn).IsEqualTo(packageGuid)
                            .And(ArtisanAudit.OnlineCourseIDColumn).IsEqualTo(oc.Id)
                            .ExecuteTypedList<ArtisanAudit>()
                            .FirstOrDefault();

                        currentCourseVersion = Select.AllColumnsFrom<ArtisanAudit>()
                            .Where(ArtisanAudit.ArtisanCourseIDColumn).IsEqualTo(oc.PublishedArtisanCourseID)
                            .And(ArtisanAudit.OnlineCourseIDColumn).IsEqualTo(oc.Id)
                            .ExecuteTypedList<ArtisanAudit>()
                            .FirstOrDefault();

                        Assembly symphonyCore = Assembly.GetAssembly(typeof(Symphony.Core.Controllers.SymphonyController));
                        AssemblyName name = new AssemblyName(symphonyCore.FullName);
                        Version version = name.Version;
                        
                        string buildVersion = version.ToString();

                        if (deliveredCourseVersion != null && deliveredCourseVersion.ArtisanCourseID > 0)
                        {
                            string deliveredCourseVersionJson = JsonConvert.SerializeObject(deliveredCourseVersion);
                            context.Response.Write(string.Format("window.Artisan.App.DeliveredCourse = {0};", deliveredCourseVersionJson));
                        
                            artisanCourse = new ArtisanCourse(deliveredCourseVersion.ArtisanCourseID);
                        }

                        if (currentCourseVersion != null && currentCourseVersion.ArtisanCourseID > 0)
                        {
                            string currentCourseVersionJson = JsonConvert.SerializeObject(currentCourseVersion);
                            context.Response.Write(string.Format("window.Artisan.App.CurrentCourse = {0};", currentCourseVersionJson));
                        }

                        context.Response.Write(string.Format("window.Artisan.App.SymphonyVersion = '{0}';", buildVersion));

                        // Validation settings
                        bool isValidationEnabled = oc.IsValidationEnabled.HasValue ? oc.IsValidationEnabled.Value : false;
                        int validationInterval = oc.ValidationInterval.HasValue ? oc.ValidationInterval.Value : 0;
                        // Course timing override
                        bool isCourseTimingDisabled = false;
                        bool isScormDisabled = false;
                        bool preTestValidation = false;
                        bool postTestValidation = false;

                        if (trainingProgramId > 0)
                        {
                            TrainingProgram tp = new TrainingProgram(trainingProgramId);
                            validationInterval = tp.ValidationInterval.HasValue ? tp.ValidationInterval.Value : validationInterval;
                            isValidationEnabled = tp.IsValidationEnabled.HasValue ? tp.IsValidationEnabled.Value : isValidationEnabled;
                            preTestValidation = tp.PreTestValidation.HasValue ? tp.PreTestValidation.Value : preTestValidation;
                            postTestValidation = tp.PostTestValidation.HasValue ? tp.PostTestValidation.Value : postTestValidation;

                            isCourseTimingDisabled = tp.IsCourseTimingDisabled;

                            if (tp.SessionCourseID > 0)
                            {
                                List<Models.ClassroomClass> sessionClasses = (new ClassroomController()).GetRegisteredClassesForCourses(user.Id, new int[] { tp.SessionCourseID.Value }).Data;
                                sessionClasses = sessionClasses.Where(s => s.MinClassDate < DateTime.UtcNow && s.MaxClassDate.Value.AddMinutes(s.DailyDuration) > DateTime.UtcNow).ToList();
                                if (sessionClasses.Count == 0)
                                {
                                    isCourseTimingDisabled = true;
                                    isScormDisabled = true;
                                }
                            }

                            if (isCourseTimingDisabled)
                            {
                                needsInit = true;
                            }
                        }

                        if (isValidationEnabled && validationInterval <= 0)
                        {
                            validationInterval = 300;
                        }

                        context.Response.Write(String.Format(@"window.Artisan.IsValidationEnabled = {0}; 
                                                                window.Artisan.ValidationInterval = {1}; 
                                                                window.Artisan.ValidationURL = '/services/customer.svc/validate/{2}/{3}/{4}';
                                                                window.Artisan.IsCourseTimingDisabled = {5};
                                                                window.Artisan.IsScormDisabled = {6};
                                                                window.Artisan.PostTestValidation = {7};
                                                                window.Artisan.PreTestValidation = {8};",
                            isValidationEnabled.ToString().ToLower(),
                            validationInterval,
                            user.Id, 
                            courseId, 
                            trainingProgramId,
                            isCourseTimingDisabled.ToString().ToLower(),
                            isScormDisabled.ToString().ToLower(),
                            postTestValidation.ToString().ToLower(),
                            preTestValidation.ToString().ToLower()));

                        context.Response.Write(@"
var maxAttempts = 3;
var attempts = maxAttempts;

var securityOptions = [{
    field: 'dob',
    msg: 'Please enter your date of birth: (DD/MM/YY)',
    validate: function (value) {
        var regex = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
        if (!value.match(regex)) {
            alert('Please enter a value in the format DD/MM/YY');
            return false;
        }
        return value;
    }
}, {
    field: 'ssn',
    msg: 'Please enter your social security number:',
    validate: function (value) {
        var value = value.replace(/[^0-9]/g, '');
        if (!value) {
            alert('Please enter a serries of 9 digits.');
            return false;
        }
        return value;
    }
}];

function validateTimeout() {
    attempts = maxAttempts;
    setTimeout(function() {
        validateUser(null, function() {
            validateTimeout();
        });
    }, window.Artisan.ValidationInterval * 1000);
}

function validateUser(optionOverride, callback) {
    if (window.Artisan.IsValidationEnabled) {
        var option = (optionOverride && typeof optionOverride !== 'undefined') ? optionOverride : securityOptions[Math.floor(Math.random() * 2)];
        var result = prompt(option.msg);
        var data = {};

        if (result) {
            data[option.field] = option.validate(result);
        } else {
            $.ajax({
                url: window.Artisan.ValidationURL + '/' + (maxAttempts - attempts),
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json',
                method: 'POST'
            });
            alert('You could not be validated, this session has ended.');
            top.close();
            return;
        }

        if (!data[option.field]) {
            validateUser(option);
            return;
        }

        attempts--;

        $.ajax({
            url: window.Artisan.ValidationURL + '/' + (maxAttempts - attempts),
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            method: 'POST',
            success: function (data) {
                if (data.success) {
                    if(callback && typeof callback === 'function') {
                        callback(data);
                    }
                } else {
                    if (attempts > 0) {
                        alert('You did not enter the correct value. You have ' + attempts + ' more ' + (attempts > 1 ? 'tries' : 'try') + ' remaining.');
                        validateUser(option);
                    } else {
                        alert('You could not be validated. This session has ended.');
                        top.close();
                    }
                }
            },
            error: function() {
                alert('You could not be validated. This session has ended.');
                top.close();
            }
        });
    }
}

validateTimeout();
");

                        if (artisanCourse == null)
                        {
                            // Best effort... no other way to find the actual course id that 
                            // is being delivered. (Until adding the audit table - courses
                            // without an audit entry will have to rely on published id)
                            artisanCourse = new ArtisanCourse(oc.PublishedArtisanCourseID);
                        }

                        if (oc.ArtisanCourseID > 0 && artisanCourse.Id > 0)
                        {
                            List<string> parameterCodes = new List<string>();
                            if (!string.IsNullOrEmpty(artisanCourse.Parameters))
                            {
                                parameterCodes = Symphony.Core.Utilities.Deserialize<List<string>>(artisanCourse.Parameters);
                            }

                            List<Symphony.Core.Models.OnlineCourseParameterOverride> onlineCourseOverrides = Select.AllColumnsFrom<OnlineCourseOverrideDetail>()
                                        .Where(OnlineCourseOverrideDetail.Columns.OnlineCourseID).IsEqualTo(oc.Id)
                                        .And(OnlineCourseOverrideDetail.Columns.Code).In(parameterCodes.ToArray())
                                        .ExecuteTypedList<Symphony.Core.Models.OnlineCourseParameterOverride>();

                            string courseOverrideJSON = Symphony.Core.Utilities.Serialize(onlineCourseOverrides);
                            context.Response.Write(String.Format("window.Artisan.OnlineCourseParameterOverrides = {0};", courseOverrideJSON));

                            if (trainingProgramId > 0)
                            {
                                List<Symphony.Core.Models.TrainingProgramParameterOverride> trainingProgramOverrides = Select.AllColumnsFrom<TrainingProgramParameterOverrideDetail>()
                                        .Where(TrainingProgramParameterOverrideDetail.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                                        .And(TrainingProgramParameterOverrideDetail.Columns.Code).In(parameterCodes.ToArray())
                                        .ExecuteTypedList<Symphony.Core.Models.TrainingProgramParameterOverride>();

                                string trainingProgramOverrideJSON = Symphony.Core.Utilities.Serialize(trainingProgramOverrides);
                                context.Response.Write(String.Format("window.Artisan.TrainingProgramParameterOverrides = {0};", trainingProgramOverrideJSON));
                            }

                            context.Response.Write(@"
function overrideArtisanCourseParameters(overrides) {
    var parameterOverrideMap = {};

    for (var i = 0; i < overrides.length; i++) {
        parameterOverrideMap[overrides[i].code.replace(/[{!}]/g, '')] = overrides[i];
    }

    var sectionJson = JSON.stringify(window.Artisan.CourseJson.sections);
    var regex = /(<span.+?artisan-parameter.+?{!)([a-z0-9_]+)(.+?>)(.*?)(<\/span>)/g;
    
    sectionJson = sectionJson.replace(regex, function(match, startSpan, code, endSpan, current, closeSpan) {
        var value = parameterOverrideMap[code] ? parameterOverrideMap[code].value : current;

        var replacement = startSpan + code + endSpan + value + closeSpan;
        
        replacement = JSON.stringify(replacement);
        replacement = replacement.substring(1, replacement.length - 1);

        return replacement;
    });

    window.Artisan.CourseJson.sections = JSON.parse(sectionJson);
}

var needsInit = false;

if (window.Artisan.OnlineCourseParameterOverrides && window.Artisan.OnlineCourseParameterOverrides.length) {
    overrideArtisanCourseParameters(window.Artisan.OnlineCourseParameterOverrides);
    needsInit = true;
}

if (window.Artisan.TrainingProgramParameterOverrides && window.Artisan.TrainingProgramParameterOverrides.length) {
    overrideArtisanCourseParameters(window.Artisan.TrainingProgramParameterOverrides);
    needsInit = true;
}

if (needsInit) {
    Artisan.App.init()
}");
                            
                        }

                        string overridePath = context.Server.MapPath("~/CourseOverrides/" + oc.Id + ".js");
                        if (System.IO.File.Exists(overridePath))
                        {
                            context.Response.Write(overridePath);
                        }
                        else
                        {
                            OnlineCourseRollup rollup = Select.AllColumnsFrom<OnlineCourseRollup>()
                               .Where(OnlineCourseRollup.CourseIDColumn).IsEqualTo(courseId)
                               .And(OnlineCourseRollup.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                               .And(OnlineCourseRollup.UserIDColumn).IsEqualTo(user.Id)
                               .ExecuteSingle<OnlineCourseRollup>();

                            TrainingProgram tp;
                            if (trainingProgramId > 0)
                            {
                                tp = new TrainingProgram(trainingProgramId);
                            }
                            else
                            {
                                tp = new TrainingProgram();
                            }


                            // Use customer level overrides if there aren't any at the course level
                            oc.ShowPretestOverride = tp.ShowPretestOverride ?? oc.ShowPretestOverride ?? customer.ShowPretestOverride;
                            oc.ShowPostTestOverride = tp.ShowPostTestOverride ?? oc.ShowPostTestOverride ?? customer.ShowPostTestOverride;
                            oc.PassingScoreOverride = tp.PassingScoreOverride ?? oc.PassingScoreOverride ?? customer.PassingScoreOverride;
                            oc.SkinOverride = tp.SkinOverride ?? oc.SkinOverride ?? customer.SkinOverride;
                            oc.PreTestTypeOverride = tp.PreTestTypeOverride ?? oc.PreTestTypeOverride ?? customer.PreTestTypeOverride;
                            oc.PostTestTypeOverride = tp.PostTestTypeOverride ?? oc.PostTestTypeOverride ?? customer.PostTestTypeOverride;
                            oc.NavigationMethodOverride = tp.NavigationMethodOverride ?? oc.NavigationMethodOverride ?? customer.NavigationMethodOverride;
                            oc.CertificateEnabledOverride = tp.CertificateEnabledOverride ?? oc.CertificateEnabledOverride ?? customer.CertificateEnabledOverride;
                            oc.DisclaimerOverride = tp.DisclaimerOverride ?? oc.DisclaimerOverride ?? customer.DisclaimerOverride;
                            oc.ContactEmailOverride = tp.ContactEmailOverride ?? oc.ContactEmailOverride ?? customer.ContactEmailOverride;
                            oc.ShowObjectivesOverride = tp.ShowObjectivesOverride ?? oc.ShowObjectivesOverride ?? customer.ShowObjectivesOverride;
                            oc.PretestTestOutOverride = tp.PretestTestOutOverride ?? oc.PretestTestOutOverride ?? customer.PretestTestOutOverride;

                            oc.RandomizeAnswersOverride = tp.RandomizeAnswersOverride ?? oc.RandomizeAnswersOverride ?? customer.RandomizeAnswersOverride;
                            oc.RandomizeQuestionsOverride = tp.RandomizeQuestionsOverride ?? oc.RandomizeQuestionsOverride ?? customer.RandomizeQuestionsOverride;
                            oc.ReviewMethodOverride = tp.ReviewMethodOverride ?? oc.ReviewMethodOverride ?? customer.ReviewMethodOverride;
                            oc.MarkingMethodOverride = tp.MarkingMethodOverride ?? oc.MarkingMethodOverride ?? customer.MarkingMethodOverride;
                            oc.InactivityMethodOverride = tp.InactivityMethodOverride ?? oc.InactivityMethodOverride ?? customer.InactivityMethodOverride;
                            oc.InactivityTimeoutOverride = tp.InactivityTimeoutOverride ?? oc.InactivityTimeoutOverride ?? customer.InactivityTimeoutOverride;
                            oc.IsForceLogoutOverride = tp.IsForceLogoutOverride ?? oc.IsForceLogoutOverride ?? customer.IsForceLogoutOverride;
                            
                            oc.MinimumPageTimeOverride = tp.MinimumPageTimeOverride ?? oc.MinimumPageTimeOverride ?? customer.MinimumPageTimeOverride;
                            oc.MaximumQuestionTimeOverride = tp.MaximumQuestionTimeOverride ?? oc.MaximumQuestionTimeOverride ?? customer.MinimumPageTimeOverride;
                            oc.MaxTimeOverride = tp.MaxTimeOverride ?? oc.MaxTimeOverride ?? customer.MaxTimeOverride;

                            oc.MinTimeOverride = tp.MinTimeOverride ?? oc.MinTimeOverride ?? customer.MinTimeOverride;
                            oc.MinLoTimeOverride = tp.MinLoTimeOverride ?? oc.MinLoTimeOverride ?? customer.MinLoTimeOverride;
                            oc.MinScoTimeOverride = tp.MinScoTimeOverride ?? oc.MinScoTimeOverride ?? customer.MinScoTimeOverride;

                            oc.ThemeFlavorOverrideID = tp.ThemeFlavorOverrideID ?? oc.ThemeFlavorOverrideID ?? customer.ThemeFlavorOverrideID;


                            //oc.RetestMode = oc.RetestMode ?? customer.RetestMode;
                            //oc.Retries = oc.Retries ?? customer.Retries;

                            int retries = tp.Retries ?? oc.Retries ?? customer.Retries ?? int.MaxValue - 1; //.onlin== null ? (customer.OnlineCourseRetries == null ? int.MaxValue : customer.OnlineCourseRetries.Value) : oc.Retries.Value;

                            // there was a bug in old artisan courses; rather than having to re-deploy all courses, 
                            // it's fixed in the course for future releases, but also back-ported here
                            string monkeyPatch = @"
// monkey patch
Artisan.App.removeSpecial = function (type) {
    var json = Artisan.CourseJson;
    for (var i = 0; i < json.sections.length; i++) {
        if (json.sections[i].sectionType == type) {
            json.sections.splice(i, 1);
        }
    }
}; 
Artisan.App.hasPosttest = function () {
    var json = Artisan.CourseJson;
    for (var i = 0; i < json.sections.length; i++) {
        if (json.sections[i].sectionType == ArtisanSectionType.Posttest) {
            return true;
        }
    }
    return false;
};

if (!Artisan.App.setTheme) {
    Artisan.App.setTheme = function (cssPath) {
        if (document.getElementById('artisan_theme')) {
            document.getElementById('aritsan_theme').setAttribute('href', cssPath);
        } else {
            var cssElement = $('link[href=""./css/theme.css""]');
            cssElement.attr('href', cssPath);
        }
    }
}
";
                            bool applyMonkeyPatch = false; 

                            bool attemptLimitReached = false;
                            if (rollup != null && rollup.Id > 0 && rollup.TestAttemptCount >= retries + 1)
                            {
                                attemptLimitReached = true;
                            }

                            StringBuilder builder = new StringBuilder();
                            string prefix = "Artisan.App";
                            bool isAssignmentComplete = true;

                            // only apply assignments if necessary, which is when we have a session-based TP
                            if (tp.SessionCourseID.HasValue && tp.SessionCourseID > 0)
                            {
                                // Get transcript details to determine if we should clear assignment responses
                                
                                CourseAssignmentController courseAssignment = new CourseAssignmentController();
                                Models.Course course = new Models.Course();
                                course.CopyFrom(oc);
                                // Online course doesn't have a course type so not copied. We know it's an online course though!
                                course.CourseTypeID = (int)Symphony.Core.CourseType.Online;

                                Symphony.Core.Controllers.CourseController.AddTranscriptDetails(user.Id, new List<Core.Models.Course>() { course }, trainingProgramId);
                                if (course.IsAssignmentRequired)
                                {
                                    isAssignmentComplete = false;
                                    if (course.IsAssignmentMarked)
                                    {
                                        builder.AppendFormat("{0}.setIsAllowRetryAssignment(true);", prefix);
                                    }
                                }

                                //if (course.IsAssignmentMarked)
                                //{

                                // show assignment responses
                                var responses = new AssignmentController().GetLastAssignmentResponses(trainingProgramId, courseId, user.Id);
                                if (responses.Success)
                                {
                                    builder.AppendFormat("if({0}.setAssignmentResults){{ {0}.setAssignmentResults({1}); }}", prefix, JsonConvert.SerializeObject(responses.Data));
                                }
                            }
                            //}

                            if (oc.PretestTestOutOverride.HasValue)
                            {
                                builder.AppendFormat("{0}.setPretestTestOut({1});", prefix, oc.PretestTestOutOverride.Value.ToString().ToLower());
                            }

                            if (oc.PassingScoreOverride.HasValue)
                            {
                                builder.AppendFormat("{0}.setPassingScore({1});", prefix, oc.PassingScoreOverride.Value);
                            }
                            if (oc.NavigationMethodOverride.HasValue)
                            {
                                builder.AppendFormat("{0}.setNavigationMethod({1});", prefix, oc.NavigationMethodOverride.Value);
                            }
                            if (oc.PreTestTypeOverride.HasValue)
                            {
                                builder.AppendFormat("{0}.setPretestType({1});", prefix, oc.PreTestTypeOverride.Value);
                            }
                            if (oc.PostTestTypeOverride.HasValue)
                            {
                                builder.AppendFormat("{0}.setPosttestType({1});", prefix, oc.PostTestTypeOverride.Value);
                            }
                            if ((oc.ShowPostTestOverride.HasValue && oc.ShowPostTestOverride == false) || attemptLimitReached)
                            {
                                applyMonkeyPatch = true;
                                if (attemptLimitReached)
                                {
                                    builder.Append("if(Artisan.App.hasPosttest && Artisan.App.hasPosttest()){");
                                    builder.Append("window.setTimeout(function(){ alert('You have reached the test attempt limit and the post test has been removed; please contact your manager if you wish to re take the test for this course.'); }, 3000);");
                                    builder.Append("}");
                                }
                                builder.AppendFormat("{0}.removePosttest();", prefix);
                            }
                            if (oc.ShowObjectivesOverride.HasValue && oc.ShowObjectivesOverride == false)
                            {
                                applyMonkeyPatch = true;
                                builder.AppendFormat("{0}.removeObjectives();", prefix);
                            }
                            if (oc.ShowPretestOverride.HasValue && oc.ShowPretestOverride == false)
                            {
                                builder.AppendFormat("{0}.removePretest();", prefix);
                            }
                            if (oc.SkinOverride.HasValue || oc.ThemeFlavorOverrideID.HasValue)
                            {
                                // example value:
                                // 'http://localhost:63997/Handlers/ArtisanSkinHandler.ashx?href=http://localhost:63997/skins/artisan_content/default/skins/be_smooth/skin.css'
                                string port = (context.Request.Url.Port == 80 || context.Request.Url.Port == 443 ? "" : ":" + context.Request.Url.Port);
                                string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Host + port;
                                string handlerUrl = baseUrl + "/Handlers/ArtisanSkinHandler.ashx?href=";
                                string dynamicHandlerUrlTemplate = baseUrl + "/skins/artisan_content/default/skins/{0}/{1}/skin.css&themeName={0}&themeFlavorName={1}";
                                
                                int themeId = oc.SkinOverride ?? artisanCourse.ThemeID;
                                int themeFlavorId = oc.ThemeFlavorOverrideID ?? artisanCourse.ThemeFlavorID ?? 0;

                                ArtisanTheme originalTheme = new ArtisanTheme(artisanCourse.ThemeID);
                                ArtisanTheme theme = new ArtisanTheme(themeId);

                                Theme themeFlavor = new Theme(themeFlavorId);

                                string staticSkinUrl = handlerUrl + baseUrl + theme.Folder + "/skins/" + theme.SkinName + "/skin.css";
                                string dynamicSkinUrl = string.Format(handlerUrl + dynamicHandlerUrlTemplate, theme.SkinName, themeFlavor.CodeName);

                                bool isValidOverride = !theme.IsDynamic || theme.IsDynamic && themeFlavor.Id > 0 && !string.IsNullOrWhiteSpace(themeFlavor.CodeName);

                                if (oc.SkinOverride.HasValue && isValidOverride)
                                {
                                    string cssUrl = theme.IsDynamic ? dynamicSkinUrl : staticSkinUrl;
                                    builder.AppendFormat("{0}.setSkin('{1}');", prefix, cssUrl);
                                }
                            }

                            string setTestParameterStartLoop = @"
                                var json = Artisan.CourseJson;
                                for (var i = 0; i < json.sections.length; i++) {
                                    if (json.sections[i].sectionType == ArtisanSectionType.Posttest || json.sections[i].sectionType == ArtisanSectionType.PreTest) {
                            ";
                            string setTestParameter = "json.sections[i].{0} = {1};";
                            string setTestParameterEndLoop = @"
                                    }
                                }
                            ";

                            string setCourseJsonParameter = "Artisan.CourseJson.{0} = {1};";

                            if (oc.RandomizeAnswersOverride.HasValue)
                            {
                                builder.Append(setTestParameterStartLoop);
                                builder.AppendFormat(setTestParameter, "isRandomizeAnswers", oc.RandomizeAnswersOverride.Value.ToString().ToLower());
                                builder.Append(setTestParameterEndLoop);
                            }
                            if (oc.RandomizeQuestionsOverride.HasValue)
                            {
                                builder.Append(setTestParameterStartLoop);
                                builder.AppendFormat(setTestParameter, "isReRandomizeOrder", oc.RandomizeQuestionsOverride.Value.ToString().ToLower());
                                builder.Append(setTestParameterEndLoop);
                            }
                            if (oc.ReviewMethodOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "reviewMethod", oc.ReviewMethodOverride.Value);
                            }
                            if (oc.MarkingMethodOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "markingMethod", oc.MarkingMethodOverride.Value);
                            }
                            if (oc.InactivityMethodOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "timeoutMethod", oc.InactivityMethodOverride.Value);
                            }
                            if (oc.InactivityTimeoutOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "inactivityTimeout", oc.InactivityTimeoutOverride.Value);
                            }
                            if (oc.IsForceLogoutOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "isForceLogout", oc.IsForceLogoutOverride.Value.ToString().ToLower());
                            }
                            if (oc.MinimumPageTimeOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "minimumPageTime", oc.MinimumPageTimeOverride.Value);
                            }
                            if (oc.MaximumQuestionTimeOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "maximumQuestionTime", oc.MaximumQuestionTimeOverride.Value);
                            }
                            if (oc.MaxTimeOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "maxTime", oc.MaxTimeOverride.Value);
                            }
                            if (oc.MinTimeOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "minTime", oc.MinTimeOverride.Value);
                            }
                            if (oc.MinLoTimeOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "minLoTime", oc.MinLoTimeOverride.Value);
                            }
                            if (oc.MinScoTimeOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "minScoTime", oc.MinScoTimeOverride.Value);
                            }
                            if (oc.CertificateEnabledOverride.HasValue)
                            {
                                builder.AppendFormat(setCourseJsonParameter, "certificateEnabled", oc.CertificateEnabledOverride.Value.ToString().ToLower());
                            }

                            var url = context.Request.Url;
                            string host = url.AbsoluteUri.Replace(url.AbsolutePath, "")
                                                         .Replace(url.Query, "");
                            string certificateUrl = host + "/Handlers/CertificateHandler.ashx?certificateTypeId=" + CertificateType.Online + "&trainingProgramId={!training_program_id}&courseOrClassId={!course_id}&userId={!user_id}&score={!score}";

                            builder.Append("Artisan.CourseJson.useSymphonyCertificate = true;");
                            builder.Append("Artisan.CourseJson.certificateUrl = '" + certificateUrl + "';");

                            if (
                               rollup != null && rollup.Id > 0 &&
                               rollup.Completion.HasValue && rollup.Completion.Value &&
                               rollup.Success.HasValue && rollup.Success.Value &&
                               tp.CourseUnlockModeID == (int)CourseUnlockMode.AfterCourseCompletion)
                            {
                                builder.Append("window.Artisan.IsCourseTimingDisabled = true;");

                                if (isAssignmentComplete)
                                {
                                    builder.Append("window.Artisan.IsScormDisabled = true;");
                                }

                                builder.Append("window.Artisan.IsCourseUnlocked = true;");
                                builder.AppendFormat("{0}.setNavigationMethod({1});", prefix, (int)ArtisanCourseNavigationMethod.FreeForm);
                            }

                            if (isForcedUser && registrationUserId > 0)
                            {
                                builder.Append("window.Artisan.IsForcedRegistrationUserId = true;");
                            }
                            if (isLMSNone)
                            {
                                var message = "You are not authorized to take this course. Your score and progress in this course will not be recorded. If you believe this is an error, please logout and try again.";
                                
                                // Kill everything
                                builder.AppendLine("parent.objLMS.Finish();");
                                
                                builder.AppendLine("parent.top.API.CloseOutSession();");
                                builder.AppendLine("parent.top.API.Initialized = false;");
                                builder.AppendLine("parent.top.API.ScoCalledFinish = true;");
                                builder.AppendLine("parent.top.Control.Comm.KillPostDataProcess()");
                                builder.AppendLine("parent.top.Control.Comm.Disable()");
   
                                builder.AppendFormat("alert('{0}');", message);
                            }

                            if (builder.Length > 0)
                            {
                                if (applyMonkeyPatch)
                                {
                                    context.Response.Write(monkeyPatch);
                                }
                                context.Response.Write(builder.ToString());
                            }

                            if (builder.Length > 0 || needsInit)
                            {
                                context.Response.Write("Artisan.App.init();");
                            }
                        }
                    }


                    if (SymphonyController.IsServerSideUiLogic)
                    {
                        // Set start times for TP/Course
                        CourseAssignmentController courseAssignmentController = new CourseAssignmentController();
                        courseAssignmentController.UpdateCourseUserStartTime(actualCustomer.Id, user.Id, courseId, trainingProgramId);
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorJS(context, "Server Error: Could not load the artisan course hook handler. Course overrides have not been applied.", ex);
            }
        }

        /// <summary>
        /// If debug flag is set, append alert message to pop up during playback to indicate there was an error
        /// when the debug is not set, only a console error message will appear
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        private void LogErrorJS(HttpContext context, string message, Exception ex = null)
        {
            Log.Error(message, ex);

            if (ex != null)
            {
                message += "\r\n" + ex.ToString();
            }

            message = message.Replace("\r\n", "\\r\\n");

            string logError = string.Format("if (console && console.error) { console.error(\"{0}\"); }", message);
            context.Response.Write(logError);
#if DEBUG || IsQaServer
            string alertError = string.Format("alert(\"{0}\");", message);
            context.Response.Write(alertError);
#endif
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
