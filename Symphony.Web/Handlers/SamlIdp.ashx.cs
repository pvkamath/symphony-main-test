﻿using ComponentPro.Saml2.Metadata;
using Symphony.Core;
using Symphony.Core.Data;
using Symphony.Web.Saml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Web;
using System.Xml;

namespace Symphony.Web.Handlers
{
    /// <summary>
    /// Summary description for SamlIdp
    /// </summary>
    public class SamlIdp : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            // Create a new instance of the EntityDescriptor class.
            EntityDescriptor entityDescriptor = new EntityDescriptor();

            // Set ID 
            entityDescriptor.EntityId = new EntityIdType(Util.GetAbsoluteUrl(context, "~/Handlers/SamlIdp.ashx"));
            string protocol = "urn:oasis:names:tc:SAML:2.0:protocol";
            string bindings = "urn:oasis:names:tc:SAML:2.0:bindings";

            string subdomain = context.Request.QueryString["customer"] ?? "be";
            Symphony.Core.Data.Customer customer = new Symphony.Core.Data.Customer(Symphony.Core.Data.Customer.SubDomainColumn.ColumnName, subdomain);

            //entityDescriptor.Organization
            // Create a new instance of the AttributeAuthorityDescriptor class.
            AttributeAuthorityDescriptor attributeAuthorityDescriptor = new AttributeAuthorityDescriptor();
            // Add that AttributeAuthorityDescriptor to the entity descriptor.
            attributeAuthorityDescriptor.ProtocolSupportEnumeration = protocol;
            entityDescriptor.AttributeAuthorityDescriptors.Add(attributeAuthorityDescriptor);

            // Set binding type and location.
            AttributeService attributeService = new AttributeService();
            attributeService.Binding = bindings + ":SOAP";

            attributeService.Location = Utilities.ParseCustomerUrl(ConfigurationManager.AppSettings["SSO.AttributeService.Location"], customer, HttpContext.Current);
            attributeAuthorityDescriptor.AttributeServices.Add(attributeService);

            // Load the key to sign
            //string fileXCert = Path.Combine(HttpRuntime.AppDomainAppPath, SamlParams.IdPKeyFile);
            //context.Response.Write(SamlParams.IdPKeyPassword);
            //return;

            X509Certificate2 x509Certificate = new X509Certificate2(Path.Combine(HttpRuntime.AppDomainAppPath, SamlParams.IdPKeyFile), SamlParams.IdPKeyPassword, X509KeyStorageFlags.MachineKeySet);

            IdpSsoDescriptor ssoDescriptor = new IdpSsoDescriptor();
            ssoDescriptor.ProtocolSupportEnumeration = protocol;

            foreach (var config in new string[] { "Redirect", "POST", "Artifact" })
            {
                SingleLogoutService slo = new SingleLogoutService();
                slo.Location = Utilities.ParseCustomerUrl(ConfigurationManager.AppSettings["SSO.SingleLogoutService.Location"], customer, HttpContext.Current);
                slo.Binding = bindings + ":HTTP-" + config;
                ssoDescriptor.SingleLogoutServices.Add(slo);
            }

            SingleSignOnService sso = new SingleSignOnService();
            sso.Location = Utilities.ParseCustomerUrl(ConfigurationManager.AppSettings["SSO.SingleSignOnService.Location"], customer, HttpContext.Current);
            sso.Binding = bindings + ":HTTP-Redirect";
            ssoDescriptor.SingleSignOnServices.Add(sso);

            SpSsoDescriptor spSso = new SpSsoDescriptor();
            entityDescriptor.IdpSsoDescriptors.Add(ssoDescriptor);

            ContactPerson person = new ContactPerson();
            person.Type = "support";
            person.Company = "OnCourse Learning";
            person.EmailAddresses.Add("support@oncourselearning.com");
            person.GivenName = "OnCourse Learning";

            entityDescriptor.ContactPeople.Add(person);

            #region Signing key descriptor if needed

            // Load certificate to sign
            /*KeyInfoX509Data certKeyInfoX509Data = new KeyInfoX509Data(x509Certificate);
            KeyInfo certKeyInfo = new KeyInfo();
            certKeyInfo.AddClause(certKeyInfoX509Data);

            // Add key descriptor
            KeyDescriptor keyDesc = new KeyDescriptor();
            keyDesc.Use = "signing";
            keyDesc.KeyInfo = certKeyInfo.GetXml();

            ssoDescriptor.KeyDescriptors.Add(keyDesc);*/

            #endregion

            //ssoDescriptor.Sign(x509Certificate);        
            entityDescriptor.Sign(x509Certificate); // Sign the entity descriptor if needed 

            string xml = entityDescriptor.GetXml().OuterXml;

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xml);

            context.Response.AddHeader("Content-type", "text/xml");
            xmlDocument.Save(context.Response.OutputStream);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}