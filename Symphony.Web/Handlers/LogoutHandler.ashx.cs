﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using Symphony.Core.Controllers;
using Model = Symphony.Core.Models;

namespace Symphony.Web.Handlers
{
    /// <summary>
    /// Summary description for LogoutHandler
    /// </summary>
    public class LogoutHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            UserController.Logout(context);
            context.Response.Write("OK");           
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}