﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Symphony.Core;
using Symphony.Core.Models;
using Data = Symphony.Core.Data;
using Symphony.Core.Controllers;
using RazorEngine;
using log4net;
using System.IO;
using SubSonic;
using Newtonsoft.Json;


namespace Symphony.Web.Handlers
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class CertificateHandler : IHttpHandler
    {
        // we need a composite model to house the user and tp data, as well as the validation
        public class ValidationComposite
        {
            public int UserId;
            public int TrainingProgramId;
            public int CourseId;
            public string url;
            public ValidationField VF;
        }

        public void ProcessRequest(HttpContext context)
        {
            ILog Log = LogManager.GetLogger(typeof(CertificateHandler));

            context.Response.ContentType = "text/html";

            try
            {
                CertificateController certificateController = new CertificateController();

                string trainingProgramId = context.Request["trainingProgramId"];
                string courseOrClassId = context.Request["courseOrClassId"];
                string userId = context.Request["userId"];
                string certificateTypeId = context.Request["certificateTypeId"];
                string scoreOverride = context.Request["score"];
                string renderCertificate = context.Request["renderCertificate"];
                string isCertificatePreviewString = context.Request["isCertificatePreview"];
                string primarylicenseIdOverrideString = context.Request["licenseOverrideId"];
                string certificateTemplateOverrideString = context.Request.Form["certificateTemplateOverride"];

                int tpid, cid, uid, primaryLicenseIdOverride;

                CertificateType certificateType;

                bool isRenderCertificate = false;
                bool isCertificatePreview = false;

                Data.CertificateTemplate certificateTemplateOverride = null;

                primaryLicenseIdOverride = 0;
                if (!string.IsNullOrEmpty(primarylicenseIdOverrideString))
                {
                    int.TryParse(primarylicenseIdOverrideString, out primaryLicenseIdOverride);
                }

                if (!string.IsNullOrEmpty(renderCertificate))
                {
                    bool.TryParse(renderCertificate, out isRenderCertificate);
                }

                if (!string.IsNullOrEmpty(isCertificatePreviewString))
                {
                    bool.TryParse(isCertificatePreviewString, out isCertificatePreview);
                }

                if (!string.IsNullOrEmpty(certificateTemplateOverrideString))
                {
                    try
                    {
                        certificateTemplateOverride = JsonConvert.DeserializeObject<Data.CertificateTemplate>(System.Uri.UnescapeDataString(certificateTemplateOverrideString));
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Invalid JSON for certificate template override. Json provided: " + certificateTemplateOverrideString, e);
                    }
                }

                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Invalid training program id.");
                }
                if (!int.TryParse(courseOrClassId, out cid))
                {
                    throw new Exception("Invalid course id.");
                }
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id.");
                }
                if (!Enum.TryParse<CertificateType>(certificateTypeId, out certificateType))
                {
                    throw new Exception("Invalid certificate type.");
                }
                
                // load up the training program to check if it is the old Cert style or the new:
                var tp = new Data.TrainingProgram(tpid);
                // get the tp rollup to see if we need post program validation
                var tpru = (new RollupController()).GetRollup(tpid, uid);

                if (tp != null && tp.PostTestValidation.HasValue && tp.PostTestValidation.Value 
                    && tpru != null && !(tpru.PostValidationComplete.HasValue && tpru.PostValidationComplete.Value))
                {
                    
                    // send to the validation flows
                    string baseLayoutPath = System.Web.HttpContext.Current.Server.MapPath("/Certificates/PostProgramValidation.html");
                    string baseLayoutName = "data";
                    string baseLayout = System.IO.File.ReadAllText(baseLayoutPath);

                    // get a random validation q:
                    var question = ValidationController.GetRandomValidationQuestion();

                    var cmpst = new ValidationComposite()
                    {
                        VF = question,
                        UserId = uid,
                        CourseId = cid,
                        TrainingProgramId = tpid,
                        url = "/services/customer.svc/posttpvalidate"
                    };

                    Razor.Compile(baseLayout, typeof(ValidationComposite), baseLayoutName);

                    string dataLayout = "/Certificates/data.html";
                    string layoutPath = System.Web.HttpContext.Current.Server.MapPath(dataLayout);
                    string layout = System.IO.File.ReadAllText(layoutPath);

                    string result = Razor.Parse<ValidationComposite>(layout, cmpst);

                    context.Response.Write(result);
                }
                else
                {
                    Certificate certificate;
                    if (string.IsNullOrEmpty(scoreOverride))
                    {
                        scoreOverride = null;
                    }

                    if (isCertificatePreview)
                    {
                        scoreOverride = "100";
                    }

                    certificate = (new CertificateController()).GetCertificate(certificateType, tpid, cid, uid, scoreOverride, primaryLicenseIdOverride, isCertificatePreview);

                    bool requiresUserLicenseNumber = (certificate.TrainingProgram.HasLicenses && certificate.TrainingProgram.PrimaryLicense.HasUserLicenseNumber);

                    if (certificate.UserDataFields.Count == 0 && !requiresUserLicenseNumber)
                    {
                        // No fields to collect so just render it out.
                        isRenderCertificate = true;
                    }

                    if (isRenderCertificate) // Render out the certificate
                    {

                        // load the user entered fields
                        Symphony.Core.Data.User user = new Symphony.Core.Data.User(uid);
                        //List<Symphony.Core.Data.LicenseDataField> fields = new List<Core.Data.LicenseDataField>();
                        foreach (string key in context.Request.Form.Keys)
                        {
                            string name = string.Empty;
                            if (key.StartsWith("data-"))
                            {
                                name = key.Substring("data-".Length);
                                certificate.LicenseDataMeta[name] = context.Request.Form[key];

                                UserDataField field = Select.AllColumnsFrom<Data.UserDataField>()
                                    .Where(Data.UserDataField.CodeNameColumn).IsEqualTo(name)
                                    .And(Data.UserDataField.CustomerIDColumn).In(new int[] { user.CustomerID, 0 }) // User data fields with customer id 0 are global to all customers
                                    .ExecuteSingle<UserDataField>();

                                if (field != null && field.ID > 0)
                                {
                                    var map = Select.AllColumnsFrom<Core.Data.UserDataFieldUserMap>()
                                        .Where(Core.Data.UserDataFieldUserMap.UserIDColumn).IsEqualTo(uid)
                                        .And(Core.Data.UserDataFieldUserMap.UserDataFieldIDColumn).IsEqualTo(field.ID)
                                        .ExecuteSingle<Core.Data.UserDataFieldUserMap>();

                                    if (map == null)
                                    {
                                        map = new Data.UserDataFieldUserMap();
                                    }

                                    map.UserValue = context.Request.Form[key];
                                    map.UserID = uid;
                                    map.UserDataFieldID = field.ID;
                                    map.Save();
                                }
                            }
                            else if (key.StartsWith("user-"))
                            {
                                name = key.Substring("user-".Length);
                                certificate.LicenseUserMeta[name] = context.Request.Form[key];

                                if ((new Symphony.Core.Controllers.UserController()).IsValidUserCertificateField(name))
                                {
                                    if (name.ToLower() == "socialsecuritynumber")
                                    {
                                        // DS: This is "extra check" for "SocialSecurityNumber" field is required to 
                                        // prevent adding a new column in Data.Core.User (or schema), so all we need to do is to map "socialsecuritynumber" to
                                        // "EncryptedSSN" field of Models.User type. This is done by the code below.
                                        Symphony.Core.Models.User usernew = new Core.Models.User();
                                        usernew.SocialSecurityNumber = context.Request.Form[key];
                                        user.SetColumnValue("EncryptedSSN", usernew.EncryptedSSN);
                                    }
                                    else
                                    {
                                        user.SetColumnValue(name, context.Request.Form[key]);
                                    }
                                }
                            }
                            else if (key == "licensenumber")
                            {
                                if (certificate.TrainingProgram != null && certificate.TrainingProgram.PrimaryLicense != null)
                                {
                                    var userLicenseNumber = SubSonic.Select.AllColumnsFrom<Core.Data.UserLicenseNumber>()
                                        .Where(Core.Data.UserLicenseNumber.Columns.UserID).IsEqualTo(userId)
                                        .And(Core.Data.UserLicenseNumber.Columns.LicenseID).IsEqualTo(certificate.TrainingProgram.PrimaryLicense.Id)
                                        .ExecuteSingle<Core.Data.UserLicenseNumber>();

                                    if (userLicenseNumber == null)
                                    {
                                        userLicenseNumber = new Data.UserLicenseNumber();
                                    }

                                    userLicenseNumber.UserID = user.Id;
                                    userLicenseNumber.LicenseID = certificate.TrainingProgram.PrimaryLicense.Id;
                                    userLicenseNumber.LicenseNumber = context.Request.Form[key];
                                    userLicenseNumber.Save();

                                    // add back into the cert
                                    certificate.TrainingProgram.PrimaryLicense.UserLicenseNumber = userLicenseNumber.LicenseNumber;
                                }
                            }
                        }

                        user.Save();

                        // update the cert user so those properties work as expected
                        certificate.User = new Symphony.Core.Controllers.UserController().GetUser(user.Id);

                        // new template needs to be here so we have all the information from the certificate loaded up
                        if (tp.CertificateTemplateID != null || certificateTemplateOverride != null)
                        {
                            // render out the new style certificate:
                            Data.CertificateTemplate certificateTemplate = certificateTemplateOverride != null ? certificateTemplateOverride : tp.CertificateTemplate;

                            CertificateGenerated generatedCertificate = new CertificateGenerated
                            {
                                Html = new CertificateController().GenerateCertificate(tp.CertificateTemplateID.HasValue ? tp.CertificateTemplateID.Value : 0, uid, tpid, certificate, certificateTemplateOverride),
                                Width = certificateTemplate.Width,
                                Height = certificateTemplate.Height,
                                IsPreview = isCertificatePreview,
                                BgImagePath = certificateTemplate.BgImagePath
                            };

                            string baseLayoutPath = System.Web.HttpContext.Current.Server.MapPath("/Certificates/certificate_template_wrapper.html");
                            string baseLayoutName = "wrapper";
                            string baseLayout = System.IO.File.ReadAllText(baseLayoutPath);

                            Razor.Compile(baseLayout, typeof(CertificateGenerated), baseLayoutName);

                            string wrapperLayout = "/Certificates/wrapper.html";
                            string layoutPath = System.Web.HttpContext.Current.Server.MapPath(wrapperLayout);
                            string layout = System.IO.File.ReadAllText(layoutPath);

                            layout = layout += generatedCertificate.Html;

                            string result = Razor.Parse<CertificateGenerated>(layout, generatedCertificate);

                            context.Response.Write(result);
                        }
                        else
                        {
                            // Load up the proper template
                            string baseLayoutPath = System.Web.HttpContext.Current.Server.MapPath("/Certificates/certificate_template.html");
                            string baseLayoutName = "certificate";
                            string baseLayout = System.IO.File.ReadAllText(baseLayoutPath);

                            Razor.Compile(baseLayout, typeof(Certificate), baseLayoutName);

                            string certificatePath = CertificateController.DEFAULT_PATH.Replace('/', '\\');

                            // If training program, use the training program cert if set. 
                            // If course, use the training program cert if the course either does not have a cert set, or is set to default.
                            // Training program cert will only be applied if the cert is accessed within the context of a training program.
                            if (certificateType == CertificateType.TrainingProgram && !string.IsNullOrEmpty(certificate.TrainingProgram.CertificatePath))
                            {
                                certificatePath = certificate.TrainingProgram.CertificatePath;
                            }
                            else if (certificateType != CertificateType.TrainingProgram)
                            {
                                if (!string.IsNullOrEmpty(certificate.Course.CertificatePath) && certificate.Course.CertificatePath != certificatePath)
                                {
                                    certificatePath = certificate.Course.CertificatePath;
                                }
                                else if (!string.IsNullOrEmpty(certificate.TrainingProgram.CertificatePath))
                                {
                                    certificatePath = certificate.TrainingProgram.CertificatePath;
                                }
                            }

                            string layoutPath = System.Web.HttpContext.Current.Server.MapPath(Path.Combine(certificatePath, certificateType.ToString().ToLower() + ".html"));

                            // If the specific cert for the course type and certificate type
                            // does not exist, travel upwards to the default directory until one
                            // is found.
                            while (!System.IO.File.Exists(layoutPath))
                            {
                                string fileName = Path.GetFileName(layoutPath);
                                DirectoryInfo dir = Directory.GetParent(layoutPath).Parent;
                                layoutPath = Path.Combine(dir.Name, fileName);

                                if (!layoutPath.Contains(CertificateController.DEFAULT_PATH.Replace('/', '\\')))
                                {
                                    throw new Exception("Certificate layout could not be found.");
                                }
                            }

                            string customerLayout = layoutPath.Replace("\\Default\\", string.Format("\\{0}\\Default\\", certificate.Customer.SubDomain));

                            if (System.IO.File.Exists(customerLayout))
                            {
                                layoutPath = customerLayout;
                            }

                            certificate.TemplatePath = layoutPath;

                            string layout = System.IO.File.ReadAllText(layoutPath);

                            string result = Razor.Parse<Certificate>(layout, certificate);

                            context.Response.Write(result);
                        }
                    }
                    else // Prompt for data first
                    {
                        // Load up the data template
                        // still send the certificate model to the template
                        // in order to prompt for data

                        string baseLayoutPath = System.Web.HttpContext.Current.Server.MapPath("/Certificates/data_template.html");
                        string baseLayoutName = "data";
                        string baseLayout = System.IO.File.ReadAllText(baseLayoutPath);

                        Razor.Compile(baseLayout, typeof(Certificate), baseLayoutName);

                        string dataLayout = "/Certificates/data.html";

                        certificate.Url = context.Request.Url.PathAndQuery + "&renderCertificate=true";

                        string layoutPath = System.Web.HttpContext.Current.Server.MapPath(dataLayout);

                        certificate.TemplatePath = layoutPath;

                        string layout = System.IO.File.ReadAllText(layoutPath);

                        if (!string.IsNullOrEmpty(certificateTemplateOverrideString))
                        {
                            if (certificate.UserDataFields == null)
                            {
                                certificate.UserDataFields = new List<UserDataField>();
                            }
                            certificate.UserDataFields.Add(new UserDataField
                            {
                                CodeName = "certificateTemplateOverride",
                                Xtype = "hidden",
                                Name = "certificateTemplateOverride",
                                _Name = "certificateTemplateOverride",
                                Value = certificateTemplateOverrideString
                            });
                        }

                        string result = Razor.Parse<Certificate>(layout, certificate);

                        context.Response.Write(result);
                    }
                }
            }
            catch (RazorEngine.Templating.TemplateCompilationException ex)
            {
                Log.Error(ex);

                foreach (System.CodeDom.Compiler.CompilerError e in ex.Errors)
                {
                    context.Response.Write("<p>" + e.ErrorText + "</p>");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                do
                {
                    context.Response.Write("<p>" + ex.Message + "</p>");
                    ex = ex.InnerException;
                } while (ex != null);


            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}