﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.Script.Serialization;
using Symphony.Core.Controllers;
using Model = Symphony.Core.Models;
using Symphony.Core;
using Symphony.Core.Models;

namespace Symphony.Web.Handlers
{
    /// <summary>
    /// Summary description for AjaxLogin
    /// </summary>
    public class LoginHandler : IHttpHandler
    {
        UserController controller = new UserController();

        public void ProcessRequest(HttpContext context)
        {

            string token = context.Request["token"];
            string detailed = context.Request["detailed"];
            AuthenticationResult loginResult;

            if (!string.IsNullOrWhiteSpace(token))
            {

                AuthenticationToken userToken = AuthenticationToken.CreateAuthenticationTokenFromTokenString(token);
                if (userToken != null) // CreateAuthentationTokenFromTokenString returns null if its not a valid token 
                {
                    loginResult = controller.Login(userToken.LoginUser.Customer, userToken.LoginUser.UserName, true);
                    if (loginResult.Success)
                    {
                        loginResult.Message = "OK";
                    }
                }
                else
                {
                    loginResult = new AuthenticationResult()
                    {
                        Success = false,
                        Message = "Invalid authentication Token"
                    };
                }
                if (string.IsNullOrEmpty(detailed) || detailed.ToLower() == "false")
                {
                    context.Response.Write(loginResult.Message);
                }
                else if (detailed == "token")
                {
                    //A new token is being requested so we're going to create a new result
                    AuthenticationTokenResult tokenResult = new AuthenticationTokenResult()
                    {
                        Success = loginResult.Success,
                        Message = loginResult.Message
                    };
                    if (loginResult.Success)
                    {
                        tokenResult.LoginData = userToken.LoginUser;
                    }
                    context.Response.Write(Utilities.Serialize(tokenResult));
                }
                else
                {
                    context.Response.Write(Utilities.Serialize(loginResult));
                }
            }
            else
            {
                
                string customer = context.Request["customer"];
                string userName = context.Request["userName"];
                string password = context.Request["password"];

                loginResult = controller.Login(customer, userName, password);
                if (loginResult.Success)
                {
                    loginResult.Message = "OK";
                }
                if (string.IsNullOrEmpty(detailed) || detailed.ToLower() == "false" )
	            {
                    context.Response.Write( loginResult.Message);
	            }
                else if (detailed == "token")
                {
                    AuthenticationTokenResult tokenResult = new AuthenticationTokenResult()
                    {
                        Success = loginResult.Success,
                        Message = loginResult.Message,
                        UserId = loginResult.UserId.ToString(),
                        LoginData = new AuthenticationTokenData()
                        {
                            UserName = userName,
                            Customer = customer
                        }
                    };
                    context.Response.Write(Utilities.Serialize(tokenResult));
                }
                else
                {
                    context.Response.Write(Utilities.Serialize(loginResult));
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}