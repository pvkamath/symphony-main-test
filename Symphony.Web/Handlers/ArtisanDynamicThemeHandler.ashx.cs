﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Data = Symphony.Core.Data;
using Symphony.Core.Controllers;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using Symphony.Core;
using log4net;

namespace Symphony.Web.Handlers
{
    /// <summary>
    /// Responsibly for any on-the-fly alterations of this course's settings
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ArtisanDynamicThemeHandler : IHttpHandler
    {
        ILog Log = LogManager.GetLogger(typeof(ArtisanDynamicThemeHandler));
        
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string themeName = HttpContext.Current.Request.Params["themeName"];
                string themeFlavor = HttpContext.Current.Request.Params["themeFlavorName"];


                if (string.IsNullOrWhiteSpace(themeName))
                {
                    throw new Exception("Invalid theme.");
                }
                if (string.IsNullOrWhiteSpace(themeFlavor))
                {
                    throw new Exception("Invalid theme flavor");
                }

                string themeCss = new ArtisanController().GenerateDynamicThemeCss(themeName, themeFlavor);

                context.Response.ContentType = "text/css";

                context.Response.Write(themeCss);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
