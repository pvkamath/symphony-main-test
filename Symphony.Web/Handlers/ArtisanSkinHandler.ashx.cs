﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Symphony.Core.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using Symphony.Core;
using Symphony.Core.Controllers;
using log4net;

namespace Symphony.Web.Handlers
{
    /// <summary>
    /// Responsible for any on-the-fly alterations of this course's theme
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ArtisanCourseSkinHandler : IHttpHandler
    {
        ILog Log = LogManager.GetLogger(typeof(ArtisanCourseSkinHandler));
        
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string cssPath = context.Request["href"];
                string themeName = HttpContext.Current.Request.Params["themeName"];
                string themeFlavor = HttpContext.Current.Request.Params["themeFlavorName"];

                context.Response.ContentType = "text/css";
                if (!string.IsNullOrEmpty(cssPath))
                {
                    Uri uri = new Uri(cssPath);
                    if (uri.Host == context.Request.Url.Host && uri.Port == context.Request.Url.Port)
                    {
                        string css = string.Empty;
                        string file = context.Server.MapPath(uri.LocalPath);

                        string resource = uri.LocalPath.Substring(1).Replace("/", ".");
                        Symphony.Core.Generators.PackageBuilder builder = new Core.Generators.PackageBuilder();
                        
                        if (!string.IsNullOrWhiteSpace(themeName) && !string.IsNullOrWhiteSpace(themeFlavor))
                        {
                            css = new ArtisanController().GenerateDynamicThemeCss(themeName, themeFlavor);
                        }
                        else if (resource == "css.artisan.default.css" && builder.IsResource(resource))
                        {
                            css = builder.GetResourceText(resource);
                        }
                        else if (System.IO.File.Exists(file))
                        {
                            css = System.IO.File.ReadAllText(file);
                        }

                        if (!string.IsNullOrWhiteSpace(css)) {
                            string folder = uri.LocalPath.Substring(0, uri.LocalPath.LastIndexOf("/"));
                            //string parentFolder = folder.Substring(0, folder.LastIndexOf("/"));
                            //string imagesFolder = parentFolder + "/images";
                            string port = (uri.Port == 80 || uri.Port == 443 ? "" : ":" + uri.Port);
                            context.Response.Write(Utilities.RebaseCSS(css, uri.Scheme + "://" + uri.Host + port, "/" + folder + "/"));
                        }
                    }
                    else
                    {
                        // future to-do: download the file and use it...
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
