﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Symphony.Web.Saml;
using ComponentPro.Saml2;
using log4net;
using Symphony.Core.Controllers;
using Symphony.Core.Models;
using Symphony.Core;

namespace Symphony.Web.Handlers
{
    /// <summary>
    /// SamlHandler will process a saml request. If the user is logged in, then the saml response will
    /// be returned to the requester as an authenticated user
    /// 
    /// If the user is not logged in, then we redirect to a login page. 
    /// 
    /// After the user logs in, the login page will redirect back here and load the previous request 
    /// from the cookie. At this point the request will be returned as an authenticated user.
    /// 
    /// </summary>
    public class SamlHandler : IHttpHandler, IRequiresSessionState
    {

        private ILog Log = LogManager.GetLogger(typeof(SamlHandler));

        public void ProcessRequest(HttpContext context)
        {
            string customer = "be";
            try
            {
                SymphonyController symphony = new SymphonyController();
                
                if (!string.IsNullOrEmpty(context.Request["customer"]))
                {
                    customer = context.Request["customer"];
                }

                // Receive the authentication request
                AuthnRequest authnRequest = null;
                string relayState = null;

                string sessionKey;
                if (context.Request.QueryString.AllKeys.Contains(SamlParams.SsoSessionKey))
                {
                    sessionKey = context.Request.QueryString[SamlParams.SsoSessionKey];
                }
                else if (context.Request.Cookies["ssoKey"] != null && context.Request.Cookies[context.Request.Cookies["ssoKey"].Value.ToString()] != null)
                {
                    // geef, that's an ugly condition.
                    // we have a session key and a session tracked with it
                    sessionKey = context.Request.Cookies["ssoKey"].Value.ToString();
                }
                else
                {
                    sessionKey = Guid.NewGuid().ToString();
                }

                // Get the saved SSO state, if any.
                // If there isn't saved state then receive the authentication request.
                // If there is saved state then we've just completed a local login in response 
                // to a prior authentication request.
                SsoAuthnState ssoState = SsoAuthnState.RestoreFromCookie(sessionKey);
                if (ssoState != null)
                {
                    authnRequest = ssoState.AuthnRequest;
                    relayState = ssoState.RelayState;

                    ssoState.IsSamlRedirect = false;
                    SsoAuthnState.SaveToCookie(ssoState, sessionKey);
                }

                if (!context.Request.QueryString.AllKeys.Contains(SamlParams.SamlRedirectKey))
                {

                    Util.ReceiveAuthnRequest(context, out authnRequest, out relayState);

                    if (ssoState != null)
                    {
                        ssoState.RelayState = relayState;
                    }

                    if (authnRequest == null)
                    {
                        return;
                    }
                }

                bool requireLocallogin = false;
                bool allowCreate = false;

                if (authnRequest.NameIdPolicy != null)
                {
                    allowCreate = authnRequest.NameIdPolicy.AllowCreate;
                }

                if (symphony.UserID == 0)
                {
                    requireLocallogin = true;
                }

                if (ssoState == null)
                {
                    ssoState = new SsoAuthnState();
                    ssoState.AuthnRequest = authnRequest;
                    ssoState.RelayState = relayState;

                    ssoState.IdpProtocolBinding = SamlBinding.HttpPost;

                    try
                    {
                        ssoState.IdpProtocolBinding = SamlBindingUri.UriToBinding(authnRequest.ProtocolBinding);
                    }
                    catch (Exception e)
                    {
                        Log.Error("Could not determine idp to sp saml binding. Using POST as default. " + e.Message);         
                    }

                    ssoState.AssertionConsumerServiceURL = authnRequest.AssertionConsumerServiceUrl;

                    string nameIdentifier = authnRequest.Issuer.NameIdentifier;
                    if (!string.IsNullOrEmpty(nameIdentifier))
                    {
                        // Look up the name identifier and override any details.
                        ServiceProvider provider = ServiceProviderController.GetServiceProviderFromNameIdentifier(nameIdentifier).Data;
                        if (provider != null)
                        {
                            if (!string.IsNullOrEmpty(provider.AssertionUrl))
                            {
                                ssoState.AssertionConsumerServiceURL = provider.AssertionUrl;
                            }

                            if (!string.IsNullOrEmpty(provider.Method)) {
                                switch (provider.Method)
                                {
                                    case SamlBindingUri.HttpPost:
                                        ssoState.IdpProtocolBinding = SamlBinding.HttpPost;
                                        break;
                                    case SamlBindingUri.HttpArtifact:
                                        ssoState.IdpProtocolBinding = SamlBinding.HttpArtifact;
                                        break;
                                    case SamlBindingUri.HttpRedirect:
                                        ssoState.IdpProtocolBinding = SamlBinding.HttpRedirect;
                                        break;
                                }
                            }

                            ssoState.IsRedirectArtifact = provider.IsRedirectArtifact;
                        }
                    }
                }

                if (requireLocallogin)
                {
                    ssoState.IsSamlRedirect = true;
                    SsoAuthnState.SaveToCookie(ssoState, sessionKey);
                    string query = string.Format("customer={0}&{4}=1&{1}={2}{3}", customer, SamlParams.SsoSessionKey, sessionKey, Skins.BuildQuerySkin("&"), Login.NEVER_REDIRECT);
                    
                    System.Web.Security.FormsAuthentication.RedirectToLoginPage(query);
                    return;
                }

                // TODO - AM - 06/05/16 - This is duplicated from above, we can clean this up to make one call, but 
                // since we are deploying this tonight I am making as few changes as possible
                ServiceProvider currentServiceProvider = null;
                string currentNameIdentifier = "";
                bool isDisableTheme = false;

                if (ssoState.AuthnRequest != null && ssoState.AuthnRequest.Issuer != null && !string.IsNullOrEmpty(ssoState.AuthnRequest.Issuer.NameIdentifier))
                {
                    currentNameIdentifier = ssoState.AuthnRequest.Issuer.NameIdentifier;
                    currentServiceProvider = ServiceProviderController.GetServiceProviderFromNameIdentifier(ssoState.AuthnRequest.Issuer.NameIdentifier).Data;
                }

                if (currentServiceProvider == null)
                {
                    Log.Info("Could not find the service provider matching the name identifier: '" + currentNameIdentifier + "' will default to include the theme.");
                }
                else
                {
                    isDisableTheme = currentServiceProvider.IsDisableTheme;
                }
                // END CLEANUP TOOD

                ComponentPro.Saml2.Response samlResponse = Util.CreateSamlResponse(context, ssoState.AssertionConsumerServiceURL, isDisableTheme);

                SsoAuthnState.Destroy(sessionKey);

                Util.SendSamlResponse(context, samlResponse, ssoState);

            }
            catch (Exception e)
            {
                Log.Error(e);
                System.Web.Security.FormsAuthentication.RedirectToLoginPage("samlFailure=1&customer=" + customer + "&" + Login.NEVER_REDIRECT + "=1" + Skins.BuildQuerySkin("&"));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}