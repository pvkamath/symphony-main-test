﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using ComponentPro.Saml;
using ComponentPro.Saml2;
using ComponentPro.Saml2.Binding;
using System.Xml;
using Symphony.Web.Saml;

namespace Symphony.Web.Handlers
{
    /// <summary>
    /// Summary description for SamlArtifactResolve
    /// </summary>
    public class SamlArtifactHandler : IHttpHandler
    {
        private ILog Log = LogManager.GetLogger(typeof(SamlArtifactHandler));
       
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                // Create an artifact resolve from the request with XML data extracted from the request stream.
                ArtifactResolve artifactResolve = ArtifactResolve.Create(HttpContext.Current.Request);

                // Create the artifact type 0004.
                Saml2ArtifactType0004 httpArtifact = new Saml2ArtifactType0004(artifactResolve.Artifact.ArtifactValue);
                XmlElement msg;

                // Remove the artifact state from the cache.
                var cached = ((SamlDatabaseCacheProvider)SamlSettings.CacheProvider).Remove(httpArtifact.ToString());

                if (cached == null)
                {
                    throw new Exception("No artifact found.");
                }

                var xdoc = new XmlDocument();
                xdoc.LoadXml(cached.ToString());
                msg = xdoc.DocumentElement;

                // Create an artifact response containing the cached SAML message.
                ArtifactResponse artifactResponse = new ArtifactResponse();
                artifactResponse.Id = Saml.Util.GetRandomId();
                artifactResponse.Issuer = new Issuer(Symphony.Web.Saml.Util.GetAbsoluteUrl(context, "~/"));
                artifactResponse.Message = msg;

                // Send the artifact response.
                artifactResponse.Send(context.Response);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw e;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}