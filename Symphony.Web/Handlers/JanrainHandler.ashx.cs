﻿using System;
using System.Web;
using System.Net;

using System.Web.Security;
using System.Web.Script.Serialization;
using Symphony.Core.Controllers;
using Model = Symphony.Core.Models;
using Symphony.Core;
using Symphony.Core.Models;
using System.Configuration;
using Symphony.Core.ThirdParty.Janrain;
using System.Xml;
using System.Collections.Generic;
using log4net;
using System.ServiceModel.Web;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web.SessionState;
using System.Linq;
using Symphony.Web.Saml;

namespace Symphony.Web.Handlers
{
    /// <summary>
    /// Summary description for AjaxLogin
    /// </summary>
    public class JanrainHandler : IHttpHandler, IRequiresSessionState
    {
        UserController controller = new UserController();
        ILog Log = LogManager.GetLogger(typeof(JanrainHandler));

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string uuid = context.Request["uuid"];
                string accessToken = context.Request["accessToken"];
                string userId = context.Request["userid"];

                // Going to validate the access token received from Janrain
                // If we can access the user id provided then the user must
                // have a valid session created through oauth.
                string baseUrl = ConfigurationManager.AppSettings["JanrainBaseUrl"];
                string url = baseUrl + "/entity";

                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";

                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("access_token", accessToken);
                data.Add("uuid", uuid);
                data.Add("type_name", "user");

                StringBuilder sb = new StringBuilder();
                foreach (KeyValuePair<string, string> e in data)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append('&');
                    }
                    sb.Append(System.Web.HttpUtility.UrlEncode(e.Key, Encoding.UTF8));
                    sb.Append('=');
                    sb.Append(HttpUtility.UrlEncode(e.Value, Encoding.UTF8));
                }

                string postData = sb.ToString();

                byte[] postDataBytes = System.Text.Encoding.ASCII.GetBytes(postData);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = postDataBytes.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(postDataBytes, 0, postDataBytes.Length);
                requestStream.Close();

                WebResponse response = request.GetResponse();

                Stream responseStream = response.GetResponseStream();

                StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.Default);

                string content = reader.ReadToEnd();
                
                reader.Close();
                responseStream.Close();
                response.Close();

                JanrainResult jResult = Utilities.Deserialize<JanrainResult>(content);

                // If we were able to access the user with the access token given
                // Stat will == "ok". Anything else will mean the user was not 
                // authenticated by janrain
                if (jResult.Stat != "ok")
                {
                    throw new WebFaultException<string>("Invalid Janrain session.", System.Net.HttpStatusCode.Forbidden);
                }

                AuthenticationResult loginResult = null;

                // If a user id was sent in, use that specific user to login with
                if (!string.IsNullOrEmpty(userId))
                {
                    int uid;
                    if (!int.TryParse(userId, out uid))
                    {
                        throw new WebFaultException<string>("Invalid user id.", System.Net.HttpStatusCode.Forbidden);
                    }

                    loginResult = PerformLogin(uuid, uid);
                    if (loginResult.Success)
                    {
                        string redirect = Login.ProcessLoginResult(loginResult);
                        HttpContext.Current.Response.AddHeader("X-Symphony-Redirect", redirect);
                    }

                    return;
                }

                List<User> users = controller.GetJanrainCapableUsers(uuid);

                if (users.Count == 0)
                    throw new WebFaultException<string>("No users match the Janrain ID.", System.Net.HttpStatusCode.Forbidden);

                // Single user found, log them in while we are here
                if (users.Count == 1)
                {
                    loginResult = PerformLogin(uuid, users[0].ID);
                    if (loginResult.Success)
                    {
                        string redirect = Login.ProcessLoginResult(loginResult);
                        HttpContext.Current.Response.AddHeader("X-Symphony-Redirect", redirect);
                    }
                }

                UserController userController = new UserController();
                User user = users.FirstOrDefault();
                Symphony.Core.Data.Customer cust = new Core.Data.Customer(user.CustomerID);

                if (loginResult == null)
                {
                    // Deal with potential that this is a SAML request. THis likely would never happen since saml should
                    // replace this, but just in case.
                    string ssoSessionKey = "";
                    if (HttpContext.Current.Request.QueryString.AllKeys.Contains(SamlParams.SsoSessionKey))
                    {
                        ssoSessionKey = HttpContext.Current.Request.QueryString[SamlParams.SsoSessionKey];
                    }
                    SsoAuthnState state = SsoAuthnState.RestoreFromCookie(ssoSessionKey);
                    if (state != null && state.IsSamlRedirect)
                    {
                        // Use the first user in the list for the saml sign on
                        loginResult = PerformLogin(uuid, users[0].ID);
                        // Get the saml redirect
                        string redirect = Login.ProcessLoginResult(loginResult);
                        HttpContext.Current.Response.AddHeader("X-Symphony-Redirect", redirect);
                    }
                    else
                    {
                        // Authenticate using the first user we found. 
                        // If there are multiple login systems, we will be presented with the 
                        // multilogin page anyway allowing the user to choose. Otherwise
                        // if there is one user we are logged in. 
                        userController.Login(cust.SubDomain, user.Username, true);

                        //new Symphony.ExternalSystemIntegration.Controllers.ExternalLoginController().UpdateLoginSystems(cust, user, password);
                    }
                }
                // Return all users found as options to log in as. 
                // If only 1 returned, the ui will assume login success 
                // and redirect to that user.
                context.Response.AddHeader("Content-Type", "application/json");
                context.Response.Write(Utilities.Serialize(users));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw new Exception(ex.Message);
            }            
            
        }

        private AuthenticationResult PerformLogin(string uuid, int userId)
        {
            Model.AuthenticationResult loginResult = controller.JanrainLogin(uuid, userId);

            if (!loginResult.Success)
                throw new WebFaultException<string>("The user found could not be logged in.", System.Net.HttpStatusCode.Forbidden);

            Log.Info("UserID " + userId + " logged in with " + uuid + " from Janrain");

            return loginResult;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}