﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CizerRedirect.aspx.cs" Inherits="Symphony.Web.CizerRedirect" %>

<html>
<body>
<div style="width:100%;text-align:center;padding-top:100px;font-size:16px">Loading, please wait...</div>
<%
    var userController = new Symphony.Core.Controllers.UserController();
    var customer = new Symphony.Core.Data.Customer(userController.CustomerID);
    var token = userController.GetCNRToken(userController.UserID, customer);
	var newUser = new Symphony.Core.Data.User(userController.UserID);
		
	var UserInfo = "?t=" + token + "bid" + userController.CustomerID + " " + userController.UserID + " " + newUser.FirstName.Replace("'", "") + " " + newUser.LastName.Replace("'", "");
        
	var query = "https://reporting.betraining.com/SCNR_SSO/cnr_sso.aspx" + UserInfo;
%>

<script type="text/javascript">
    window.loadNext = function () {
        window.location.href = "<%=query%>";
}
</script>

<iframe style="height:0px;width:0px;border:0px" frameborder="0" seamless='seamless' src="<%=query%>" onload="loadNext()"></iframe>

</body>