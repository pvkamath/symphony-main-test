﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Symphony.Core.Data;
using Symphony.Web.Services;
using System.Text;
using System.Collections;
using System.Data;
using Symphony.Core;

namespace Symphony.Web
{
    public partial class CreateAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Load data for the DropDownList control only once, when the 
            // page is first loaded.
            if (!IsPostBack)
            {
                // Specify the data source and field names for the Text 
                // and Value properties of the items (ListItem objects) 
                // in the DropDownList control.
                jobRoles.DataSource = CreateJobRoleDataSource();
                jobRoles.DataTextField = "JobRoleName";
                jobRoles.DataValueField = "JobRoleID";

                // Bind the data to the control.
                jobRoles.DataBind();

                // Set the default selected item, if desired.
                jobRoles.SelectedIndex = 0;

                // Specify the data source and field names for the Text 
                // and Value properties of the items (ListItem objects) 
                // in the DropDownList control.
                location.DataSource = CreateLocationDataSource();
                location.DataTextField = "LocationName";
                location.DataValueField = "LocationID";

                // Bind the data to the control.
                location.DataBind();

                // Set the default selected item, if desired.
                location.SelectedIndex = 0;

            }
        }

        protected void CreateAccountLink(object sender, EventArgs e)
        {
            
            this.success.Text = string.Empty;
            this.error.Text = string.Empty;
            try
            {
                Symphony.Core.Models.UserSecurity user = new Symphony.Core.Models.UserSecurity();

                if (this.jobRoles.SelectedItem.Value == "invalid" || this.location.SelectedItem.Value == "invalid")
                    throw new Exception("Please select values from the dropdowns above");

                int lid = Int32.Parse(this.location.SelectedItem.Value);
                int jid = Int32.Parse(this.jobRoles.SelectedItem.Value);

                // Populate user fields from form
                user.Email = this.email.Text;
                user.Username = this.username.Text;
                user.Password = this.password.Text;
                user.FirstName = this.firstname.Text;
                user.LastName = this.lastname.Text;
                user.EmployeeNumber = this.employeeid.Text;
                
                // Assign parsed ID's here
                user.LocationID = lid;
                user.JobRoleID = jid;

                user.JobRole = this.jobRoles.SelectedItem.Text;
                user.Location = this.location.SelectedItem.Text;
                
                // Small data validation
                if (user.Email == string.Empty || user.Username == string.Empty || user.Password == string.Empty ||
                    user.EmployeeNumber == string.Empty || user.FirstName == string.Empty || user.LastName == string.Empty)
                    throw new Exception("Please fill in the entire form.");

                // Provide user defaults
                user.MiddleName = "";
                user.Notes = "";
                user.StatusID = 1;
                user.LocationID = 0;
                user.ReportingSupervisorID = 0;
                user.JobRoleID = 0;
                user.TimeZone = "";
                user.IsLockedOut = false;
                List<string> perm = new List<string>();
                perm.Add("Customer - User");
                user.ApplicationPermissions = perm;

                Customer c = GetCustomerFromURL();
                user.CustomerID = c.Id;

                CustomerService cs = new CustomerService();

                Core.Models.SingleResult<Core.Models.User> newUser = cs.SaveUser(c.Id.ToString(), "0", user);

                if (newUser.Error != null)
                    throw new Exception(newUser.Error);

                cs.SaveJobRoleUsersOverride(jid.ToString(), new int[1] { newUser.Data.ID });
                cs.SaveLocationUsersOverride(lid.ToString(), new int[1] { newUser.Data.ID });

                this.success.Text = "<span class='success'>This user has been created! Please <a href='/login/" + c.SubDomain + Skins.BuildQuerySkin("?") + "'>log in with your new account</a>.</span>";
            }
            catch (Exception ex)
            {
                this.error.Text = "<span class='error'>" + Server.HtmlEncode(ex.Message) + "</span>";
            }
            
        }

        ICollection CreateLocationDataSource()
        {

            // Create a table to store data for the DropDownList control.
            DataTable dt = new DataTable();

            // Define the columns of the table.
            dt.Columns.Add(new DataColumn("LocationName", typeof(String)));
            dt.Columns.Add(new DataColumn("LocationID", typeof(String)));

            dt.Rows.Add(CreateRow("Select a location ...", "invalid", dt));

            // populate dropdown
            Array arr = GetCustomerFromURL().Locations.ToArray();
            foreach (Location location in arr)
            {
                dt.Rows.Add(CreateRow(location.Name, location.Id.ToString(), dt));
            }

            // Create a DataView from the DataTable to act as the data source
            // for the DropDownList control.
            DataView dv = new DataView(dt);
            return dv;
        }

        ICollection CreateJobRoleDataSource()
        {

            // Create a table to store data for the DropDownList control.
            DataTable dt = new DataTable();

            // Define the columns of the table.
            dt.Columns.Add(new DataColumn("JobRoleName", typeof(String)));
            dt.Columns.Add(new DataColumn("JobRoleID", typeof(String)));

            dt.Rows.Add(CreateRow("Select a job role ...", "1", dt));

            // populate dropdown
            Array arr = GetCustomerFromURL().JobRoles.ToArray();
            foreach (JobRole job in arr)
            {
                dt.Rows.Add(CreateRow(job.Name, job.Id.ToString(), dt));
            }

            // Create a DataView from the DataTable to act as the data source
            // for the DropDownList control.
            DataView dv = new DataView(dt);
            return dv;

        }

        DataRow CreateRow(String Text, String Value, DataTable dt)
        {

            // Create a DataRow using the DataTable defined in the 
            // CreateDataSource method.
            DataRow dr = dt.NewRow();

            // Set the fields with the appropriate value. 
            dr[0] = Text;
            dr[1] = Value;

            return dr;
        }

        // Extracts the customer subdomain from URL - the cookie value isn't safe to use since we haven't logged in yet
        protected Customer GetCustomerFromURL()
        {
            string customerSubdomain = HttpContext.Current.Request.QueryString["customer"];
            
            return new Customer(Customer.Columns.SubDomain, customerSubdomain);
        }
    }
}
