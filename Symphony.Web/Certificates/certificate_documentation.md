﻿# Certificates

### Certificate Types

Certificate types are all contained within `/Certificates/Default`. Each folder within default will represent a different certificate type. This is the base structure for the certificate. The Default certificate must have a template for each course type: `classroom.html`, `online.html`, and `trainingprogram.html`. If a certificate does not have a template for a course type, it will inherit from the next level up that has the course type template. 

##### Example:

```
/Certificates/Default
    trainingprogram.html
    classroom.html
    online.html
    /NMLS
        trainingprogram.html
        classroom.html
        online.html
        /Special TrainingProgram NMLS
            trainingprogram.html
    /Credit Hour Rollup
        trainingprogram.html
        classroom.html
        online.html
```

`/Certificates/Default/NMLS/Special TrainingProgram NMLS` only contains a training program template. If this was used on an online course, the template used would be `/Certificates/Default/NMLS/online.html`

### UI

The certificate picker for this example will look like this:

![Image](https://www.evernote.com/shard/s229/sh/7825f0cc-7a61-4ccc-9ce6-f7498ccd5134/34b0541eed130531f7da8af72d0f5182/res/98c7c895-01de-481a-9720-34a9e2181392/skitch.png)

One node for each of the certificate types. These certificate types will be available to all customers.

### Overriding

Each certificate type is global to all customers, however each customer can create their own template to override the base template for that certificate type. 

##### Example
The Zoologic customer wants to create their own Default template, and their own NMLS template. To do this they would set up a folder structure as follows:
```
/Certificates/zoologic/Default
    classroom.html
    online.html
    trainingprogram.html
    /NMLS
        classroom.html
        online.html
        trainingprogram.html
```
This will still show the same list of certificate types in the certificate selection window, however, when the certificate is generated the templates for Zoologic will be loaded instead. 

The customer does not require all three course type templates in each certificate type if they only need to override one of them. They may be empty folders entirely as the customer may want to override a certificate type that is multiple levels deep. So if Zoologic only wants to override the NMLS training program template:

```
/Certificates/zoologic
    /Default
        /NMLS
            trainingprogram.html
```
`/Default` is empty and we only use `trainingprogram.html` in the NMLS certificate folder. 

### Theming
##### default.css
Default css is stored in `/Certificates/css/default.css`. This file will load for all certificates regardless of type, course type or customer.
##### base.css
The next css file that loads will be the default customer css file `base.css`. For Zoologic that would be `/Certificates/zoologic/css/base.css/`. This file will load for all certificates regardless of type or course type for this specific customer. So if Zoologic wants to ensure the same watermark and colors are used on all certificates, this would be the place to put these styles.
##### Course type css (online.css, classroom.css, trainingprogram.css)
These styles load next to theme a specific course type. These styles will be loaded for the specific course type for the customer. So here Zoologic could ensure online courses use red text, classroom course certificates use green text, and trainingprogram certificates use blue text. 

##### Certificate type css
The certificate type can also have specific css applied. If Zoologic wants all NMLS certificates to have a different background, they can place the css styles for that here. This just requires mirroring the folder structure of the certificate.

```
/Certificates/zoologic/css
    /NMLS
        trainingprogram.css
```

This would apply to the NMLS certificate for a training program in the zoologic customer.

##### Full CSS Tree structure
```
/Certificates
    /css
        default.css                    // Applies to all certificates   
    /zoologic
        /css
           base.css                   // Applies to all Zoologic certificates
           trainingprogram.css        // Applies to all zoologic training program certificates
           online.css                 // Applies to all zoologic online course certificates
           classroom.css              // Applies to all zoologic classroom certificates
           /NMLS
               trainingprogram.css    // Applies to all zoologic nmls training program certificates 
```
Styles to not need to be replicated from one to the next. The full tree that applies to the course will be loaded. If we load a Zoologic NMLS Training Program Certificate we will load all files that apply:

1. /Certificates/css/default.css
2. /Certificates/zoologic/css/base.css
3. /Certificates/zoologic/css/trainingprogram.css
4. /Certificates/zoologic/css/NMLS/trainingprogram.css

##### Special CSS
You may find that you want to load a CSS file in one template and it doesn't fit the folder structure outlined above. This can be done directly from the template file using a render section:
```
@section Styles {
    <link type="text/css" rel="stylesheet" href="path/to/my/style.css" />
}
```
This will load specifically to the template this is placed in. 

### Javascript
If you desire to do anything with Javascript, you can, the same principals that apply to CSS also apply to JS files. Just swap `css` with `js` in all paths.

Specific JS can be loaded as well using a render section just like CSS
```
@section Scripts {
    <script type="text/javascript" src="/path/to/my/javascript.js"></script>
}
```
### Base Template
The base template that all templates will inherit from is `/Certificates/certificate_template.html`

This file will handle adding the proper CSS and JS files for the template. It also has several render sections defined that can be overridden in template files.

##### BottomLayer
This is the bottom of the certificates. By default this displays `<div class="certificate_logo"></div>` used to display a watermark/background. Templates can override this to display whatever they choose. 

##### PrintButton
By default this displays 
```
<div id="print_certificate" style="position:absolute;top:830px;width:670px;z-index:10;" align="center"><a href="javascript:void(0);" onClick="javascript:window.print();"><img src="/images/print.button.gif" border="0"></a></div>
```


##### Styles
As mentioned above, specific styles to load.

##### Scripts
As mentioned above, specific scripts to load.







