﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Symphony.Core.Controllers;

namespace Symphony.Web
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Action = "./" + Request.QueryString["customer"];
        }

        protected void SendPasswordLink(object sender, EventArgs e)
        {
            string emailAddress = this.email.Text;
            string customer = Request.QueryString["customer"];
            this.success.Text = string.Empty;
            this.error.Text = string.Empty;
            try
            {
                UserController.SendPasswordResetLink(customer, emailAddress);
                this.success.Text = "<span class='success'>The reset email has been sent. Please check your email for further instructions.</span>";
            }
            catch (Exception ex)
            {
                this.error.Text = "<span class='error'>" + Server.HtmlEncode(ex.Message) + "</span>";
            }
        }
    }
}
