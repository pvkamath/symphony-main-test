﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="Symphony.Web.ReportViewer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Symphony Report Viewer</title>
    <script type="text/javascript">
        var Symphony = Symphony || {};

        if (!Symphony.Reporting) {
            Symphony.Reporting = {};
        }
    </script>

    <script type="text/javascript" src="/scripts/extjs-4.2.1.883/ext-all-debug.js"></script>
    
    <script type="text/javascript" src="/scripts/extjs-3-to-4-migration-pack-v2/ext3-core-compat.js"></script>
    <script type="text/javascript" src="/scripts/extjs-3-to-4-migration-pack-v2/ext3-compat.js"></script>
    
    <link href="/scripts/extjs-4.2.1.883/resources/css/ext-all.css" rel="stylesheet" type="text/css" />
    <link href="/scripts/extjs-3-to-4-migration-pack-v2/ext3-compat.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/scripts/symphony/ux/PagingStore.js"></script>
    <script type="text/javascript" src="/scripts/symphony/ux/BufferView.js"></script>
    <script type="text/javascript" src="/scripts/symphony/Reporting/ReportViewer/ReportViewer.js"></script>

    <script type="text/javascript">
        var queueId = <%= Request.QueryString["queueId"] ?? "null" %>;
        if (queueId == null){
            alert("Unable to load report because a valid queue ID was not provided.");
        }
        else {
            Ext.Ajax.request({
                url: '/services/reporting.svc/queueEntries/' + queueId,
                success: function (response) {

                    var result = JSON.parse(response.responseText);
                    var queueEntry = result.data;
                    Ext.Ajax.request({
                        url: '/services/reporting.svc/dataset/' + queueId,
                        success: function (response) {
                            var reportJson = JSON.parse(JSON.parse(response.responseText));
                            var viewport = new Ext.Viewport({
                                layout: 'fit',
                                items: [{
                                    xtype: 'reporting.reportviewer',
                                    reportJson: reportJson,
                                    queueEntry: queueEntry
                                }]
                            });
                        },
                        failure: function (response) {

                        }
                    });

                },
                failure: function (response) {

                }
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
