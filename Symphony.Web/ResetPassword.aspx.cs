﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Symphony.Core.Controllers;
using Symphony.Core;

namespace Symphony.Web
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Reset(object sender, EventArgs e)
        {
            //TODO: actually save the new password
            try
            {
                string email = Request.QueryString["email"];
                string guid = Request.QueryString["guid"];
                string customer = Request.QueryString["customer"];

                if (this.password1.Text != this.password2.Text)
                {
                    throw new Exception("Password mismatch.");
                }
                if (string.IsNullOrEmpty(this.password1.Text))
                {
                    throw new Exception("A password is required.");
                }

                string subdomain = UserController.ResetPassword(customer, email, guid, this.password1.Text);

                this.success.Text = "<span class='success'>Your password has been reset. <a href='/login/" + subdomain + Skins.BuildQuerySkin("?") + "'>Login</a></span>";
                this.input.Visible = false;
            }
            catch (Exception ex)
            {
                this.error.Text = "<span class='error'>" + ex.Message + "</span>";
            }

        }
    }
}
