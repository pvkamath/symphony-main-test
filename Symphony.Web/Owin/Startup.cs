﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Hangfire;
using Microsoft.Owin;
using Owin;
using System.Configuration;
using Hangfire.SqlServer;
using Hangfire.Dashboard;
using Symphony.Core.Extensions;

[assembly: OwinStartup(typeof(Symphony.Web.Owin.Startup))]

namespace Symphony.Web.Owin
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var workerCountConfig = ConfigurationManager.AppSettings["HangfireWorkerCount"];
            var disabled = ConfigurationManager.AppSettings["DisableHangfire"];
            bool isDisabled = false;
            if (!string.IsNullOrWhiteSpace(disabled))
            {
                isDisabled = bool.Parse(disabled);
            }
            int workerCount = 0;
            if (!int.TryParse(workerCountConfig, out workerCount))
            {
                workerCount = Environment.ProcessorCount;
            }
            if (!isDisabled)
            {
                // set up the global config
                var connectionString = ConfigurationManager.ConnectionStrings["Symphony"];
                GlobalConfiguration.Configuration.UseSqlServerStorage(connectionString.ConnectionString);

                app.UseHangfireDashboard("/hangfire", new DashboardOptions()
                {
                    AuthorizationFilters = new[] { new HangfireDashboardAuthorizationFilter() }
                });

                // options here are required to support multi-server environments
                var serverOptions = new BackgroundJobServerOptions
                {
                    // set the server name
                    // (machine + guid) truncated to 36 because there's a 50-char db limit and hangfire appends ":{process-id}" on this, which 
                    // can be up to single-digit billions (10 chars) and i like to play it a little safe
                    ServerName = string.Format("{0}.{1}", Environment.MachineName, Guid.NewGuid().ToString()).Truncate(36),
                    WorkerCount = workerCount,
                    Queues = new string[] { "critical", "default" }

                };

                app.UseHangfireServer(serverOptions);
            }
        }
    }
}