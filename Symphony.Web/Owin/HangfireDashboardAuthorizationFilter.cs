﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using Microsoft.Owin;
using Hangfire.Dashboard;
using Symphony.Core;
using Symphony.Core.Controllers;

namespace Symphony.Web.Owin
{
    public class HangfireDashboardAuthorizationFilter : IAuthorizationFilter
    {
        public bool Authorize(IDictionary<string, object> owinEnvironment)
        {
            // In case you need an OWIN context, use the next line,
            // `OwinContext` class is the part of the `Microsoft.Owin` package.
            var context = new OwinContext(owinEnvironment);

            // Only allow saleschannel admins to view the dashboard
            SymphonyController controller = new SymphonyController();
            return controller.UserIsSalesChannelAdmin;
        }
    }
}