﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Symphony.Core;
using Symphony.Core.Controllers;
using Symphony.Core.Models;
using Symphony.Core.Controllers.Salesforce;
using Salesforce = Symphony.Core.Models.Salesforce;
using log4net;
using Newtonsoft.Json;

using Data = Symphony.Core.Data;


namespace Symphony.Web
{
    public partial class Register : System.Web.UI.Page
    {

        private ILog log = LogManager.GetLogger(typeof(Register));

        private Customer customer;
        private TrainingProgram trainingProgram;
        private User user;
        private Profession profession;

        private Core.Controllers.UserController userController = new Core.Controllers.UserController();

        int customerId;
        int trainingProgramId;
        int userId;
        int professionId;

        List<string> validCustomers = new List<string>{"be"};

        protected override void OnPreLoad(EventArgs e)
        {
            var master = (SymphonyMaster)this.Master;
            master.PreventRedirect = true;
            base.OnPreLoad(e);
        }

        private void Init()
        {
            string customerParam = Request.Params["customerDomain"];
            string trainingProgramParam = Request.Params["trainingprogram"];
            string userParam = Request.Params["user"];
            string professionParam = Request.Params["profession"];

            // TODO: Update valid customers with config. 

            try
            {
                Data.Customer c;
                if (int.TryParse(customerParam, out customerId))
                {
                    c = new Data.Customer(customerId);
                }
                else
                {
                    c = new Data.Customer("SubDomain", customerParam);
                }

                if (c == null || c.Id == 0)
                {
                    throw new Exception("Customer does not exist");
                }

                customer = new Customer();
                customer.CopyFrom(c);
                customerId = c.Id;

                // Confirm customer has this feature enabled
                if (!validCustomers.Contains(customer.SubDomain))
                {
                    throw new Exception("Automatic registration not available for this customer.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Could not find customer: " + customerParam, ex);
            }

            try
            {
                Data.TrainingProgram tp;
                if (int.TryParse(trainingProgramParam, out trainingProgramId))
                {
                    tp = new Data.TrainingProgram(trainingProgramId);
                }
                else
                {
                    tp = new Data.TrainingProgram("Sku", trainingProgramParam);
                }

                if (tp == null || tp.Id == 0 || tp.CustomerID != customerId)
                {
                    throw new Exception("Training program does not exist.");
                }

                trainingProgram = new Symphony.Core.Models.TrainingProgram();
                trainingProgram.CopyFrom(tp);
                trainingProgramId = trainingProgram.Id;
            }
            catch (Exception ex)
            {
                throw new Exception("Could not find training program: " + customerParam, ex);
            }

            try
            {
                Data.User u;
                if (int.TryParse(userParam, out userId))
                {
                    u = new Data.User(userId);
                }
                else
                {
                    u = SubSonic.Select.AllColumnsFrom<Data.User>()
                        .Where(Data.User.CustomerIDColumn).IsEqualTo(customerId)
                        .And(Data.User.UsernameColumn)
                        .IsEqualTo(userParam).ExecuteSingle<Data.User>();
                }

                if (u == null || u.Id == 0 || u.CustomerID != customerId)
                {
                    throw new Exception("User does not exist.");
                }

                user = new User();
                user.CopyFrom(u);
                userId = u.Id;
            }
            catch (Exception ex)
            {
                throw new Exception("Could not find user: " + userParam, ex);
            }

            try
            {
                Data.Profession p;
                if (int.TryParse(professionParam, out professionId))
                {
                    p = new Data.Profession(professionId);
                }
                else
                {
                    p = new Data.Profession("Name", professionParam);
                }

                if (p == null || p.Id == 0)
                {
                    throw new Exception("Profession does not exist.");
                }

                profession = new Profession();
                profession.CopyFrom(p);
                professionId = p.Id;
            }
            catch (Exception ex)
            {
                throw new Exception("Could not find profession: " + userParam, ex);
            }
        }

        private void Login(string customerSubDomain, string username)
        {
            AuthenticationResult result = userController.Login(customerSubDomain, username, true, true);
            this.Context.Items[ApplicationProcessor.CONTEXT_USERDATA] = result.UserData;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            throw new Exception("Registration disabled.");

            try
            {
                Init();

                Login("be", "admin");

                RollupController rollupController = new RollupController();
                Data.TrainingProgramRollup rollup = rollupController.GetRollupObserveChildren(trainingProgram.Id, user.ID);

                if (rollup == null || rollup.Id == 0)
                {
                    try
                    {
                        Request.Headers.Add("X-Symphony-Target-Customer", customer.SubDomain);

                        EntitlementController entitlementController = new EntitlementController();

                        Salesforce.EntitlementList entitlements = new Salesforce.EntitlementList
                        {
                            Entitlements = new List<Salesforce.Entitlement>
                            {
                                new Salesforce.Entitlement{
                                    EntitlementDuration = 12,
                                    EntitlementDurationUnits = "Months",
                                    EntitlementPurchaseDate = DateTime.UtcNow.ToString(),
                                    IsEnabled = true,
                                    ProductId = trainingProgram.Sku,
                                    IsSuppressRollup = true
                                }
                            }
                        };

                        Salesforce.EntitlementList result = entitlementController.PostEntitlements(user.ID, entitlements);

                        if (result.Entitlements.FirstOrDefault() == null)
                        {
                            throw new Exception("No data was returned after creating entitlements. There should always be 1 entitlement result after creating entitlements.");
                        }

                        Data.TrainingProgram trainingProgramResult = result.Entitlements.FirstOrDefault().TrainingProgram;

                        if (trainingProgramResult == null || trainingProgramResult.Id == 0)
                        {
                            throw new Exception("The enrolment did not return any valid training programs in the result. There should always be exactly 1.");
                        }

                        try
                        {
                            // This does not run asynchronously. We need to run this immediately to ensure the profession is saved.
                            rollupController.RunTrainingProgramRollupForUser(trainingProgramResult.Id, user.ID, profession.Id);
                            rollup = rollupController.GetRollup(trainingProgramResult.Id, user.ID);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("The temporary rollup could not be saved. This requires the user to select their profession which is not allowed. This needs to be resolved.", ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Could not save entitlements", ex);
                    }
                }
                else
                {
                    try
                    {
                        rollup.ProfessionID = profession.Id;
                        rollup.Save(user.Username);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("The training program rollup could not be updated. The user would have received the same profession accreditations as before.", ex);
                    }
                }

                Login(customer.SubDomain, user.Username);

                PortalController portalController = new PortalController();
                TrainingProgram fullTrainingProgramDetails = portalController.GetTrainingProgramDetails(user.ID, rollup.TrainingProgramID, false).Data;

                this.trainingProgramData.Text = string.Format("var trainingProgram = {0}", JsonConvert.SerializeObject(fullTrainingProgramDetails));
            }
            catch (Exception ex)
            {
                log.Error("Failed to launch course.", ex);
                throw new Exception("Failed to launch course", ex);
            }
        }
    }
}