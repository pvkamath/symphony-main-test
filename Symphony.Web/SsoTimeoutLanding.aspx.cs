﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using Symphony.Core;
using log4net;
using Symphony.Core.CNRService;
using Symphony.Core.Data;
using System.Web.Security;
using SubSonic;
using Model = Symphony.Core.Models;
using Controllers = Symphony.Core.Controllers;

namespace Symphony.Web
{
    public partial class SsoTimeoutLanding : System.Web.UI.Page
    {
        ILog Log = LogManager.GetLogger(typeof(Login));

        protected void Page_Load(object sender, EventArgs e)
        {
            string subdomain = GetCustomer();
            if (!string.IsNullOrEmpty(subdomain))
            {
                Customer customer = new Customer(Customer.SubDomainColumn.ColumnName, subdomain);
                string message = customer.SSOTimeoutMessage;
                if (string.IsNullOrEmpty(message))
                {
                    message = "You have been logged out of Symphony. Please visit your company portal to log in again.";
                }
                this.text.Text = message;
            }
        }

        /// <summary>
        /// Gets the customer id from the query string or cookies
        /// </summary>
        /// <returns></returns>
        public static string GetCustomer()
        {
            var request = HttpContext.Current.Request;
            string customer = request.QueryString["customer"] ?? string.Empty;
            if (string.IsNullOrEmpty(customer) && request.Cookies["customer"] != null)
            {
                customer = request.Cookies["customer"].Value;
            }
            return customer ?? string.Empty;
        }
    }
}
