﻿<%@ Page Title="Symphony Login" Language="C#" MasterPageFile="~/Unauthenticated.Master" AutoEventWireup="true" CodeBehind="SsoTimeoutLanding.aspx.cs" Inherits="Symphony.Web.SsoTimeoutLanding" %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>
<asp:Content ID="Header" ContentPlaceHolderID="Header" runat="server">
    <script type="text/javascript">
        // Clear janrain session if there is one
        function janrainCaptureWidgetOnLoad() {
            janrain.capture.ui.endCaptureSession();
        }
    </script>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="Body" runat="server">
    <script src="/scripts/janrain/login.js" id="janrainCaptureDevScript"></script>
    <asp:Literal runat="server" ID="text" />
</asp:Content>
