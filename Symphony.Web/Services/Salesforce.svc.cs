﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models.Salesforce;
using log4net;
using Symphony.Core.Controllers.Salesforce;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using Symphony.Core;
using System.ServiceModel.Web;
using Newtonsoft.Json;

namespace Symphony.Web.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class SalesforceService : BaseService, ISalesforceService
    {
        ILog Log = LogManager.GetLogger(typeof(SalesforceService));

        private void SetStatus(HttpStatusCode code)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = code;
        }

        #region Users
        public User GetUser(string id)
        {
            try
            {
                int i = int.Parse(id);
                return (new UserController()).GetUser(UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply a valid User ID", HttpStatusCode.NotFound);
                }
                else
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
                }
            }
        }

        public List<User> FindUsers(string search, string orderBy, string orderDir, int pageIndex, int pageSize)
        {
            try
            {
                OrderDirection orderDirEnum = OrderDirection.Default;
                switch (orderDir.ToLowerInvariant())
                {
                    case "asc":
                        orderDirEnum = OrderDirection.Ascending;
                        break;
                    case "desc":
                        orderDirEnum = OrderDirection.Descending;
                        break;
                }

                return (new UserController()).FindUsers(search, orderBy, orderDirEnum, pageIndex, pageSize);

            }
            catch (Exception ex)
            {
                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply valid search parameters", HttpStatusCode.NotFound);
                }
                else
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
                }
            }
        }


        public User PostUser(User user)
        {
            try
            {
                UserController userController = new UserController();

                User userResponse = (new UserController()).PostUser(UserID, user);


                SetStatus(HttpStatusCode.Created);

                return userResponse;
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                SetStatus(HttpStatusCode.BadRequest);

                if (user == null)
                {
                    user = new User();
                }

                user.SetIssue("Error", "ERROR", ex.ToString());

                return user;
            }
        }

        public User PutUser(string id, User user)
        {
            try
            {
                int i = int.Parse(id);
                User userResponse = (new UserController()).PutUser(UserID, i, user);
                SetStatus(HttpStatusCode.Created);
                return userResponse;
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply a valid User ID", HttpStatusCode.NotFound);
                }
                else
                {
                    SetStatus(HttpStatusCode.BadRequest);

                    if (user == null)
                    {
                        user = new User();
                    }

                    user.SetIssue("Error", "ERROR", ex.ToString());
                    return user;
                }
            }
        }

        public User DeleteUser(string id)
        {
            try
            {
                int i = int.Parse(id);
                return (new UserController()).DeleteUser(UserID, i);
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply a valid User ID", HttpStatusCode.NotFound);
                }
                else
                {
                    User user = new User();

                    SetStatus(HttpStatusCode.BadRequest);

                    user.SetIssue("Error", "NOT_FOUND", ex.ToString());

                    return user;
                }
            }
        }
        #endregion

        #region Companies
        public Company GetCompany(string id)
        {
            try
            {
                int i;
                if (int.TryParse(id, out i))
                {
                    // Look up by customer id if the string parses to an int. 
                    return (new CompanyController()).GetCompany(UserID, CustomerID, i);
                }

                // Assume if the company id does not parse to int, it's a domain and look up by domain
                return (new CompanyController()).GetCompany(UserID, CustomerID, id);
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply a valid Company ID", HttpStatusCode.NotFound);
                }
                else
                {
                    throw new WebFaultException<string>(ex.ToString(), HttpStatusCode.BadRequest);
                }
            }
        }

        public Company PostCompany(Company company)
        {
            try
            {
                company = (new CompanyController()).PostCompany(UserID, CustomerID, company);
                SetStatus(HttpStatusCode.Created);

                return company;
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                SetStatus(HttpStatusCode.BadRequest);

                if (company == null)
                {
                    company = new Company();
                }

                company.SetIssue("Error", "INTERNAL_ERROR", ex.ToString());
                return company;
            }
        }

        public Company PutCompany(string id, Company company)
        {
            try
            {
                int i = int.Parse(id);

                company = (new CompanyController()).PutCompany(UserID, CustomerID, i, company);

                SetStatus(HttpStatusCode.Created);

                return company;
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply a valid Company ID", HttpStatusCode.NotFound);
                }
                else
                {
                    if (company == null)
                    {
                        company = new Company();
                    }

                    SetStatus(HttpStatusCode.BadRequest);

                    company.SetIssue("Error", "NOT_FOUND", ex.ToString());

                    return company;
                }
            }
        }

        public Company DeleteCompany(string id)
        {
            try
            {
                int i = int.Parse(id);
                return (new CompanyController()).DeleteCompany(UserID, CustomerID, i);
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply a valid Company ID", HttpStatusCode.NotFound);
                }
                else
                {
                    Company company = new Company();

                    SetStatus(HttpStatusCode.BadRequest);

                    company.SetIssue("Error", "NOT_FOUND", ex.ToString());

                    return company;
                }
            }
        }
        #endregion

        #region Products
        public ProductList GetProducts(string since)
        {
            try
            {

                DateTime? sinceDate = null;
                if (!string.IsNullOrEmpty(since))
                {
                    sinceDate = DateTime.Parse(since);
                }

                return (new ProductController()).GetProducts(UserID, CustomerID, sinceDate);
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        public Product GetProduct(string id)
        {
            try
            {
                return (new ProductController()).GetProduct(UserID, CustomerID, id);
            }
            catch (Exception ex)
            {

                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply a valid Product ID", HttpStatusCode.NotFound);
                }
                else
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
                }
            }
        }

        public ProductList GetProductSession(string since)
        {
            try
            {
                DateTime? sinceDate = null;
                if (!string.IsNullOrEmpty(since))
                {
                    sinceDate = DateTime.Parse(since);
                }
                return (new ProductController()).GetProductSession(UserID, CustomerID, sinceDate);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region Entitlements
        public EntitlementList GetEntitlements(string userId)
        {
            try
            {
                int id = int.Parse(userId);
                return (new EntitlementController()).GetEntitlements(id);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
            }
        }
        public EntitlementList GetEntitlement(string userId, string entitlementId)
        {
            try
            {
                int uid = int.Parse(userId);
                int eid = int.Parse(entitlementId);

                return (new EntitlementController()).GetEntitlement(uid, eid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
            }
        }
        public EntitlementList PostEntitlements(string userId, EntitlementList entitlements)
        {
            try
            {
                int uid = int.Parse(userId);

                EntitlementList entitlementResult = (new EntitlementController()).PostEntitlements(uid, entitlements);

                SetStatus(HttpStatusCode.Created);

                return entitlementResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
            }
        }
        public EntitlementList PutEntitlements(string userId, string entitlementId, EntitlementList entitlements)
        {
            throw new WebFaultException<string>("You cannot update entitlements once they are created.", HttpStatusCode.BadRequest);
        }
        public EntitlementList DeleteEntitlements(string userId, string entitlementId, EntitlementList entitlements)
        {
            try
            {
                int uid = int.Parse(userId);
                return (new EntitlementController()).DeleteEntitlements(uid, entitlements);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region TrainingPrograms
        public TrainingProgram GetTrainingProgram(string trainingProgramSku)
        {
            try
            {
                return (new TrainingProgramController()).GetTrainingProgram(UserID, CustomerID, trainingProgramSku);
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply a valid Training Program ID", HttpStatusCode.NotFound);
                }
                else
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
                }
            }
        }

        public Registrations GetRegistrations(string trainingProgramSku, string classId)
        {
            try
            {
                int cid = int.Parse(classId);
                return (new TrainingProgramController()).GetRegistrations(UserID, CustomerID, trainingProgramSku, cid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply a valid Training Program and Class ID", HttpStatusCode.NotFound);
                }
                else
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
                }
            }
        }

        public Registration PostRegistration(string trainingProgramSku, Registration registration)
        {
            try
            {
                return (new TrainingProgramController()).PostRegistration(UserID, CustomerID, trainingProgramSku, registration);
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                Type exceptionType = ex.GetType();

                if (exceptionType == typeof(FormatException) ||
                    exceptionType == typeof(NullReferenceException))
                {
                    throw new WebFaultException<string>("Please supply a valid Training Program ID", HttpStatusCode.NotFound);
                }
                else if (exceptionType == typeof(WebFaultException))
                {
                    throw ex;
                }
                else
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.BadRequest);
                }
            }
        }
        #endregion
    }
}
