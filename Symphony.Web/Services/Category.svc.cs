﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using Symphony.Core;

namespace Symphony.Web.Services
{
    // NOTE: If you change the class name "Category" here, you must also update the reference to "Category" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class CategoryService : BaseService, ICategoryService
    {
        ILog Log = LogManager.GetLogger(typeof(CategoryService));

        private static Dictionary<string, CategoryType> categoryTypes = new Dictionary<string, CategoryType>()
        {
            { "classroom", CategoryType.ClassroomCourse },
            { "online", CategoryType.OnlineCourse },
            { "trainingprogram", CategoryType.TrainingProgram },
            { "artisan", CategoryType.Artisan }
        };


        private static CategoryType GetCategoryType(string categoryTypeString)
        {
            if (categoryTypes.ContainsKey(categoryTypeString))
            {
                return categoryTypes[categoryTypeString];
            }
            else
            {
                throw new Exception("Invalid category specified");
            }
        }

        public MultipleResult<Category> GetCategories(string categoryTypeString, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return CategoryController.GetCategories(CustomerID, searchText, GetCategoryType(categoryTypeString));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<Category>(ex);
            }
        }

        public PagedResult<Category> GetSecondaryCategoriesPaged(string categoryTypeString, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return CategoryController.GetSecondaryCategoriesPaged(CustomerID, searchText, GetCategoryType(categoryTypeString), sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Category>(ex);
            }
        }

        public MultipleResult<Category> GetCategoriesFromRelativeCategoryId(string categoryTypeString, string categoryId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int cId;

                if (!int.TryParse(categoryId, out cId))
                {
                    throw new Exception("Invalid category id.");
                }

                List<Category> categories = CategoryController.GetRelativeCategories(CustomerID, cId, searchText);

                return new MultipleResult<Category>(categories);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<Category>(ex);
            }
        }

        public PagedResult<Category> GetCategoriesPaged(string categoryTypeString, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return CategoryController.GetCategoriesPaged(CustomerID, searchText, GetCategoryType(categoryTypeString), sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Category>(ex);
            }
        }

        public SingleResult<Category> GetCategory(string categoryTypeString, string categoryId)
        {
            try
            {
                int c = int.Parse(categoryId);
                return CategoryController.GetCategory(CustomerID, c, GetCategoryType(categoryTypeString));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Category>(ex);
            }
        }

        public SingleResult<Category> SaveCategory(string categoryTypeString, string categoryId, Category category)
        {
            try
            {
                int c = int.Parse(categoryId);
                return CategoryController.SaveCategory(CustomerID, c, GetCategoryType(categoryTypeString), category);
            } catch (Exception ex) {
                Log.Error(ex);
                return new SingleResult<Category>(ex);
            }
        }

        public SingleResult<Category> DeleteCategory(string categoryTypeString, string categoryId)
        {
            try
            {
                int c = int.Parse(categoryId);
                return CategoryController.DeleteCategory(CustomerID, c, GetCategoryType(categoryTypeString));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Category>(ex);
            }
        }

    }
}
