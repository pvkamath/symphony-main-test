﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using Symphony.Core;

namespace Symphony.Web.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class TrainingProgramService : BaseService, ITrainingProgram
    {
        ILog Log = LogManager.GetLogger(typeof(ITrainingProgram));

        public SingleResult<TrainingProgramFeedback> SetTrainingProgramFeedback(string id, TrainingProgramFeedback feedback)
        {
            try
            {
                int tpId = int.Parse(id);
                return TrainingProgramFeedbackController.SaveTrainingProgramFeedback(tpId, feedback);
            }
            catch (Exception ex)
            {
                return new SingleResult<TrainingProgramFeedback>(ex);
            }
        }

        public MultipleResult<TrainingProgramFeedback> GetTrainingProgramFeedbackFromUser(string id)
        {
            try
            {
                int tpId = int.Parse(id);
                return TrainingProgramFeedbackController.GetFeedbackForTrainingProgram(tpId);
            }
            catch (Exception ex)
            {
                return new MultipleResult<TrainingProgramFeedback>(ex);
            }
        }

        public MultipleResult<TrainingProgramFeedback> GetTrainingProgramFeedback(string id)
        {
            try
            {
                int tpId = int.Parse(id);
                return TrainingProgramFeedbackController.GetFeedbackForTrainingProgram(tpId);
            }
            catch (Exception ex)
            {
                return new MultipleResult<TrainingProgramFeedback>(ex);
            }
        }

        public SingleResult<TrainingProgramFeedbackSummary> GetTrainingProgramFeedbackSummary(string trainingProgramId)
        {
            try
            {
                var res = TrainingProgramFeedbackController.GetTrainingProgramFeedbackSummary(int.Parse(trainingProgramId));
                return res;
            }
            catch (Exception ex)
            {
                return new SingleResult<TrainingProgramFeedbackSummary>(ex);
            }
        }
    }
}
