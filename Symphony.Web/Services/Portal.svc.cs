﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Collections;
using System.Collections.Generic;
using Symphony.Core.Models;
using System.ServiceModel.Channels;
using Symphony.Core;
using System.Web;
using System.IO;
using log4net;
using Symphony.Core.Controllers;
using System.Configuration;
using Data = Symphony.Core.Data;

namespace Symphony.Web.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class PortalService : BaseService, IPortalService
    {
        public ILog Log = LogManager.GetLogger(typeof(PortalService));

        public Stream DownloadPublicDocument(string documentId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(documentId, out id))
                {
                    return (new CourseAssignmentController()).DownloadPublicDocument(CustomerID, UserID, id);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public SingleResult<GTMLoginInfo> GetGTMLoginInfo(string meetingId)
        {
            try
            {
                return (new PortalController()).GetGTMLoginInfo(CustomerID, UserID, int.Parse(meetingId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<GTMLoginInfo>(ex);
            }
        }

        public SingleResult<ClassroomClass> GetClassDetails(string classId)
        {
            try
            {
                return (new PortalController()).GetClassDetails(CustomerID, UserID, int.Parse(classId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ClassroomClass>(ex);
            }
        }

        public PagedResult<PublicDocument> GetPublicDocuments(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new PortalController()).FindPublicDocumentsForCustomer(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<PublicDocument>(ex);
            }
        }

        public PagedResult<PublicCourse> GetPublicCourses(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new PortalController()).FindPublicCoursesForUser(UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<PublicCourse>(ex);
            }
        }

        public PagedResult<UpcomingEvent> GetUpcomingEvents(string searchText, string filter, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new PortalController()).FindUpcomingEventsForUser(UserID, searchText ?? "", bool.Parse(filter), sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<UpcomingEvent>(ex);
            }
        }

        public MultipleResult<CalendarWeek> GetUpcomingEventsCalendar(string year, string month, string filter)
        {
            try
            {
                return (new PortalController()).FindUpcomingEventsForUser(UserID, int.Parse(year), int.Parse(month), bool.Parse(filter));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<CalendarWeek>(ex);
            }
        }

        public PagedResult<TrainingProgram> GetTrainingProgramsForUser(string userId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                if (!controller.UserIsSalesChannelAdmin && !controller.HasRole(Roles.CourseAssignmentAdministrator))
                {
                    throw new Exception("You do not have permission to list training programs for users.");
                }

                int uid;
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid User Id");
                }

                Symphony.Core.Data.User user = new Core.Data.User(userId);

                bool isOptimized = bool.Parse(ConfigurationManager.AppSettings["optimizeFindTrainingProgramsForUser"]);

                Log.Debug("Starting Find training programs for user");
                PagedResult<TrainingProgram> result;

                DateTime startTime = DateTime.Now;
                if (isOptimized)
                {
                    Log.Debug("Using Optimized function");

                    // we want all training programs regardless of start and end dates
                    DateTime startDate = new DateTime(1900, 1, 1);
                    DateTime endDate = new DateTime(9999, 12, 31);

                    result = (new PortalController()).FindTrainingProgramsForUserNew(uid, user.NewHireIndicator, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, startDate, endDate);
                }
                else
                {
                    Log.Debug("Using old function");
                    result = (new PortalController()).FindTrainingProgramsForUser(uid, user.NewHireIndicator, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
                }
                TimeSpan span = DateTime.Now.Subtract(startTime);

                Log.Debug("Completed - Duration (ms): " + span.TotalMilliseconds);

                return result;

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<TrainingProgram>(ex);
            }
        }


        public PagedResult<TrainingProgramSummaryDetail> ListTrainingPrograms(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new PortalController()).ListTrainingProgramsForUser(UserID, UserIsNewHire, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<TrainingProgramSummaryDetail>(e);
            }
        }

        public PagedResult<TrainingProgramSummaryDetail> ListAllTrainingPrograms(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return ListAllTrainingProgramsForUser(UserID, searchText, start, limit, sort, dir);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<TrainingProgramSummaryDetail>(e);
            }
        }

        public PagedResult<TrainingProgramSummaryDetail> ListAllTrainingProgramsForUser(string userId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int uId = 0;
                if (!int.TryParse(userId, out uId)) {
                    throw new Exception("Valid user id required");
                }

                return ListAllTrainingProgramsForUser(uId, searchText, start, limit, sort, dir); 
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<TrainingProgramSummaryDetail>(e);
            }
        }

        private PagedResult<TrainingProgramSummaryDetail> ListAllTrainingProgramsForUser(int userId, string searchText, int start, int limit, string sort, string dir)
        {
            DateTime startDate = new DateTime(1900, 1, 1);
            DateTime endDate = new DateTime(9999, 12, 31);
            return (new PortalController()).ListTrainingProgramsForUser(userId, UserIsNewHire, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, startDate, endDate);
        }

        public PagedResult<TrainingProgram> GetTrainingPrograms(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                bool isOptimized = bool.Parse(ConfigurationManager.AppSettings["optimizeFindTrainingProgramsForUser"]);

                Log.Debug("Starting Find training programs for user");
                PagedResult<TrainingProgram> result;

                DateTime startTime = DateTime.Now;
                if (isOptimized)
                {
                    Log.Debug("Using Optimized function");
                    result = (new PortalController()).FindTrainingProgramsForUserNew(UserID, UserIsNewHire, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
                }
                else
                {
                    Log.Debug("Using old function");
                    result = (new PortalController()).FindTrainingProgramsForUser(UserID, UserIsNewHire, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
                }
                TimeSpan span = DateTime.Now.Subtract(startTime);

                Log.Debug("Completed - Duration (ms): " + span.TotalMilliseconds);
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<TrainingProgram>(ex);
            }

            /*List<TrainingProgram> result = new List<TrainingProgram>();
            TrainingProgram tp1 = new TrainingProgram();
            tp1.Id = 1;
            tp1.Name = "Test Program";
            tp1.CourseCount = 4;
            TrainingProgram tp2 = new TrainingProgram();
            tp2.Id = 2;
            tp2.Name = "Dummy Program";
            tp2.CourseCount = 6;
            result.Add(tp1); 
            result.Add(tp2);

            return new PagedResult<TrainingProgram>(result, 0, 10, 2);*/
            //return new SingleResult<TrainingProgram>(tp1);
        }

        public SingleResult<TrainingProgram> GetTrainingProgramDetails(string trainingProgramId, bool manage)
        {
            try
            {
                int i = 0;
                if (int.TryParse(trainingProgramId, out i))
                {
                    return (new PortalController()).GetTrainingProgramDetails(UserID, i, manage);
                }
                throw new Exception("The specified id was invalid.");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<TrainingProgram>(ex);
            }
        }

        public SingleResult<TrainingProgram> GetTrainingProgramDetailsForUser(string trainingProgramId, string userId)
        {
            try
            {
                int i = 0;
                int uid = 0;

                if (!int.TryParse(trainingProgramId, out i))
                {
                    throw new Exception("The specified training program id was invalid.");
                }

                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("The specified user id was invalid.");
                }

                Data.User user = new Data.User(uid);
                bool isUserInCurrentCustomer = user.CustomerID == CustomerID;


                // Saleschannel admins can access any user
                // CourseAssignmentAdministrators can only access users in their own customer
                // All other users can only access their own training programs.
                if (
                        uid != UserID &&
                        !controller.UserIsSalesChannelAdmin &&
                        (!HasRole(Roles.CourseAssignmentAdministrator) || !isUserInCurrentCustomer)
                    )
                {
                     throw new Exception("You do not have permission to access training program details for this user.");
                }
                

                return (new PortalController()).GetTrainingProgramDetailsAsAdmin(uid, i, false);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<TrainingProgram>(ex);
            }
        }

        public PagedResult<Class> GetClassesForCourse(int courseId, DateTime minDate, DateTime maxDate, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new PortalController()).GetClassesForCourse(courseId, UserID, minDate, maxDate, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Class>(ex);
            }
        }

        public SingleResult<bool> RegisterForClass(string classId)
        {
            try
            {
                int i = 0;
                if (int.TryParse(classId, out i))
                {
                    return (new PortalController()).RegisterForClass(UserID, i);
                }
                throw new InvalidCastException("The specified ID was invalid.");

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> UnregisterForClass(string registrationId)
        {
            try
            {
                int i = 0;
                if (int.TryParse(registrationId, out i))
                {
                    return (new PortalController()).UnregisterForClass(UserID, i);
                }
                throw new InvalidCastException("The specified ID was invalid.");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public Stream GetTrainingProgramFile(string fileId)
        {
            try
            {
                int ifileId = 0;
                if (int.TryParse(fileId, out ifileId))
                {
                    return (new PortalController()).GetTrainingProgramFile(CustomerID, UserID, ifileId);
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public PagedResult<Symphony.Core.Models.Message> GetMessages(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new PortalController()).FindMessagesForUser(UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Symphony.Core.Models.Message>(ex);
            }
        }

        public SingleResult<Meeting> UpdateMeeting(string meetingId, Meeting meeting)
        {
            try
            {
                return (new PortalController()).UpdateMeeting(CustomerID, UserID, meetingId, meeting);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Meeting>(ex);
            }

        }

        public SingleResult<List<TranscriptEntry>> GetTranscript()
        {
            try
            {
                return (new PortalController()).GetTranscript(UserID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<List<TranscriptEntry>>(ex);
            }
        }
        public SingleResult<List<TranscriptEntryCourse>> GetTranscriptCoursesForUser(string userId)
        {
            try
            {
                int uid;
                if (!int.TryParse(userId, out uid)) {
                    throw new Exception("Use a valid user id");
                }

                return (new PortalController()).GetTranscriptCourses(uid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<List<TranscriptEntryCourse>>(ex);
            }
        }
        public SingleResult<List<TranscriptEntryCourse>> GetTranscriptCoursesForUser(string userId, string trainingProgramId)
        {
            try
            {
                int uid, tpid;
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Use a valid user id");
                }
                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Use a valid training program id");
                }

                return (new PortalController()).GetTranscriptCourses(uid, tpid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<List<TranscriptEntryCourse>>(ex);
            }
        }
        public SingleResult<List<TranscriptEntryCourse>> GetTranscriptCourses()
        {
            try
            {
                return (new PortalController()).GetTranscriptCourses(UserID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<List<TranscriptEntryCourse>>(ex);
            }
        }

        public PagedResult<Symphony.Core.Models.User> GetUsers(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new PortalController()).FindUsers(CustomerID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Symphony.Core.Models.User>(ex);
            }
        }

        public SingleResult<bool> ReadMessage(string messageId)
        {
            try
            {
                int i = 0;
                if (int.TryParse(messageId, out i))
                {
                    return (new PortalController()).ReadMessage(UserID, i);
                }
                throw new InvalidCastException("The specified ID was invalid.");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<int> GetUnreadCount()
        {
            try
            {
                return (new PortalController()).GetUnreadCount(UserID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<int>(ex);
            }
        }

        public PagedResult<Symphony.Core.Models.Meeting> GetMeetings(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new PortalController()).FindMeetingsForUser(UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Meeting>(ex);
            }
        }

        public SingleResult<bool> DeleteAllMessages()
        {
            try
            {
                return (new PortalController()).DeleteAllMessages(UserID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteMessage(string messageId)
        {
            try
            {
                int i = 0;
                if (int.TryParse(messageId, out i))
                {
                    return (new PortalController()).DeleteMessage(UserID, i);
                }
                throw new InvalidCastException("The specified ID was invalid.");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteMeeting(string meetingId)
        {
            try
            {
                int i = 0;
                if (int.TryParse(meetingId, out i))
                {
                    return (new PortalController()).DeleteMeeting(UserID, i);
                }
                throw new InvalidCastException("The specified ID was invalid.");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> ChangePassword(PasswordChange passwordChange)
        {
            try
            {
                return (new PortalController()).ChangePassword(passwordChange);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }

        }

        public SingleResult<bool> RecoverPassword(PasswordRecover recover)
        {
            try
            {
                UserController.SendPasswordResetLink(recover.Subdomain, recover.Email);
                return new SingleResult<bool>(true);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SimpleSingleResult<string> SwitchUser(string userId)
        {
            try
            {
                int i;
                if (int.TryParse(userId, out i))
                {
                    return (new PortalController()).SwitchUser(i);
                }
                throw new InvalidCastException("The specified ID was invalid.");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SimpleSingleResult<string>(ex);
            }

        }

        public SingleResult<bool> ExitUser()
        {
            try
            {
                return (new PortalController()).ExitUser();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }

        }

        public PagedResult<PublicCourse> Test()
        {
            return new PagedResult<PublicCourse>(
                new List<PublicCourse>() { new PublicCourse() },
                1,
                2,
                10
            );
        }

        public MultipleResult<PortalSetting> GetPortalSettings(string customerId)
        {

            try
            {
                int custid = 0;
                int.TryParse(customerId, out custid);
                return (new PortalController()).GetListPortalSetting(custid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

    }
}
