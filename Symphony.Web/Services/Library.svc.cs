using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.Web;
using System.IO;
using Symphony.Core;

namespace Symphony.Web.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class LibraryService : BaseService, ILibraryService
    {
        ILog Log = LogManager.GetLogger(typeof(LibraryService));

        public PagedResult<Library> GetLibraries(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return new LibraryController().GetLibraries(searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<Library>(e);
            }
        }

        public PagedResult<Library> GetLibrariesForCustomer(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int cid = 0;
                if (!int.TryParse(customerId, out cid))
                {
                    throw new Exception("Invalid customer id");
                }
                return new LibraryController().GetLibraries(cid, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<Library>(e);
            }
        }

        public PagedResult<LibraryGrant> GetLibrariesForUser(string userId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int uId;
                if (!int.TryParse(userId, out uId))
                {
                    throw new Exception("User id must be an integer.");
                }

                return new LibraryController().GetLibrariesForUser(uId, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<LibraryGrant>(e);
            }
        }

        public PagedResult<LibraryGrantHierarchy> GetHierarchiesForLibrary(string libraryId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int lId;
                if (!int.TryParse(libraryId, out lId))
                {
                    throw new Exception("Library id must be an integer.");
                }

                return new LibraryController().GetHierarchiesForLibrary(lId, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<LibraryGrantHierarchy>(e);
            }
        }

        public SingleResult<Library> GetLibrary(string libraryId)
        {
            try
            {
                int lId;
                if (!int.TryParse(libraryId, out lId))
                {
                    throw new Exception("Library id must be an integer.");
                }

                return new LibraryController().GetLibrary(lId);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new SingleResult<Library>(e);
            }
        }

        public PagedResult<LibraryCategory> GetLibraryCategories(string libraryId, string filter, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int lId;
                if (!int.TryParse(libraryId, out lId)) {
                    throw new Exception("Library id must be an integer.");
                }

                return new LibraryController().GetLibraryCategories(lId, UserID, CustomerID, filter, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<LibraryCategory>(e);
            }
        }

        public PagedResult<LibraryCategory> GetLibraryCategoriesCrossLibrary(string filter, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int lId = 0;
                return new LibraryController().GetLibraryCategories(lId, UserID, CustomerID, filter, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<LibraryCategory>(e);
            }
        }

        public PagedResult<LibraryItem> GetLibraryItems(string libraryId, string filter, string categoryId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int lId, cId;
                if (!int.TryParse(libraryId, out lId))
                {
                    throw new Exception("Library id must be an integer.");
                }
                if (!int.TryParse(categoryId, out cId))
                {
                    throw new Exception("Category id must be an integer.");
                }

                return new LibraryController().GetLibraryItems(lId, UserID, CustomerID, filter, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<LibraryItem>(e);
            }
        }

        public PagedResult<LibraryItem> GetLibraryItems(string libraryId, string filter, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {

                int lId;
                if (!int.TryParse(libraryId, out lId))
                {
                    throw new Exception("Library id must be an integer.");
                }
                return new LibraryController().GetLibraryItems(lId, UserID, CustomerID, filter, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<LibraryItem>(e);
            }
        }

        public PagedResult<LibraryItem> GetLibraryItemsCrossLibrary(string filter, string categoryId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int lId = 0, cId;
                if (!int.TryParse(categoryId, out cId))
                {
                    throw new Exception("Category id must be an integer.");
                }

                return new LibraryController().GetLibraryItems(lId, UserID, CustomerID, filter, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<LibraryItem>(e);
            }
        }

        public PagedResult<LibraryItem> GetLibraryItemsCrossLibrary(string filter, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {

                int lId = 0;
                return new LibraryController().GetLibraryItems(lId, UserID, CustomerID, filter, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<LibraryItem>(e);
            }
        }
        public MultipleResult<LibraryFilterOption> GetLibraryFilterOptions(string userId)
        {
            try
            {
                int uId;
                if (!int.TryParse(userId, out uId))
                {
                    throw new Exception("User id must be an integer.");
                }
                return new LibraryController().GetLibraryFilterOptions(uId);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new MultipleResult<LibraryFilterOption>(e);
            }
        }
        public SingleResult<Library> SaveLibrary(string libraryId, Library library)
        {
            try
            {
                int lId;
                if (!int.TryParse(libraryId, out lId))
                {
                    throw new Exception("Library id must be an integer.");
                }
                return new LibraryController().SaveLibrary(lId, library);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new SingleResult<Library>(e);
            }
        }

        public MultipleResult<LibraryGrantHierarchy> SaveLibraryGrantHierarchies(string libraryId, List<LibraryGrantHierarchy> grants)
        {
            try
            {
                int lId;
                if (!int.TryParse(libraryId, out lId)) {
                    throw new Exception("Library id must be an integer.");
                }
                return new LibraryController().SaveLibraryGrantHierarchies(lId, grants);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new MultipleResult<LibraryGrantHierarchy>(e);
            }
        }

        public SimpleSingleResult<bool> DeleteLibraryGrantHierarchies(string libraryId, List<LibraryGrantHierarchy> grants)
        {
            try
            {
                int lId;
                if (!int.TryParse(libraryId, out lId))
                {
                    throw new Exception("Library id must be an integer.");
                }
                return new LibraryController().DeleteLibraryGrantHierarchies(lId, grants);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new SimpleSingleResult<bool>(e);
            }
        }

        public SimpleSingleResult<bool> DeleteLibrary(string libraryId)
        {
            try
            {
                int lId;
                if (!int.TryParse(libraryId, out lId))
                {
                    throw new Exception("Library id must be an integer.");
                }

                return new LibraryController().DeleteLibrary(lId);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new SimpleSingleResult<bool>(e);
            }
        }

        public SimpleSingleResult<DisplayIconState> SaveFavorite(string itemId, string userId)
        {
            try
            {
                int iId, uId;

                if (!int.TryParse(itemId, out iId))
                {
                    throw new Exception("Item id must be an integer.");
                }
                if (!int.TryParse(userId, out uId))
                {
                    throw new Exception("User id must be an integer.");
                }

                return new LibraryController().SaveFavorite(iId, uId);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new SimpleSingleResult<DisplayIconState>(e);
            }
        }

        public SimpleSingleResult<DisplayIconState> DeleteFavorite(string itemId, string userId)
        {
            try
            {
                int iId, uId;

                if (!int.TryParse(itemId, out iId))
                {
                    throw new Exception("Item id must be an integer.");
                }
                if (!int.TryParse(userId, out uId))
                {
                    throw new Exception("User id must be an integer.");
                }

                return new LibraryController().DeleteFavorite(iId, uId);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new SimpleSingleResult<DisplayIconState>(e);
            }
        }


        public MultipleResult<LibraryItem> SaveLibraryItems(string libraryId, List<LibraryItem> items)
        {
            try
            {
                int lId;
                if (!int.TryParse(libraryId, out lId))
                {
                    throw new Exception("Library id must be an integer.");
                }

                return new LibraryController().SaveLibraryItems(lId, items);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new MultipleResult<LibraryItem>(e);
            }
        }

        public MultipleResult<LibraryItem> DeleteLibraryItems(string libraryId, List<LibraryItem> items)
        {
            try
            {
                int lId;
                if (!int.TryParse(libraryId, out lId))
                {
                    throw new Exception("Library id must be an integer.");
                }

                return new LibraryController().DeleteLibraryItems(lId, items);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new MultipleResult<LibraryItem>(e);
            }
        }
    }
}
