using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.Web;
using System.IO;
using Symphony.Core;

namespace Symphony.Web.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class BookService : BaseService, IBookService
    {
        ILog Log = LogManager.GetLogger(typeof(BookService));

        public PagedResult<Book> GetAllBooks(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                if (!(HasRole(Roles.CourseAssignmentAdministrator) || HasRole(Roles.CourseAssignmentTrainingManager)))
                {
                    throw new Exception("Permission denied.");
                }
                return (new BookController()).FindAllBooks(UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Book>(ex);
            }
        }

        public PagedResult<Book> FindTrainingProgramBooks(string trainingProgramId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int tpId = -1;
                int.TryParse(trainingProgramId, out tpId);
                if (tpId < 0)
                {
                    throw new Exception("Training program does not exists.");
                }
                if (!(HasRole(Roles.CourseAssignmentAdministrator) || HasRole(Roles.CourseAssignmentTrainingManager)))
                {
                    throw new Exception("Permission denied.");
                }
                return (new BookController()).FindTrainingProgramBooks(tpId, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Book>(ex);
            }
        }

        public SingleResult<Book> GetBook(string bookId)
        {
            try
            {
                if (!(HasRole(Roles.CourseAssignmentAdministrator) || HasRole(Roles.CourseAssignmentTrainingManager)))
                {
                    throw new Exception("Permission denied.");
                }
                return (new BookController()).GetBook(int.Parse(bookId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Book>(ex);
            }
        }

        public SingleResult<Book> SaveBook(string bookId, Book book)
        {
            try
            {
                if (!(HasRole(Roles.CourseAssignmentAdministrator) || HasRole(Roles.CourseAssignmentTrainingManager)))
                {
                    throw new Exception("Permission denied.");
                }
                return (new BookController()).SaveBook(int.Parse(bookId), book);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Book>(ex);
            }
        }

    }
}
