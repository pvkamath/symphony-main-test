using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Security;
using System.IO;

namespace Symphony.Web.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class AuthService : IAuthService
    {
        public bool GetStatus()
        {
            try
            {
                HttpCookie authenticationCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authenticationCookie != null)
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authenticationCookie.Value);
                    if (ticket != null && !ticket.Expired)
                    {
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                // Something went wrong, like the user isn't logged in! No need to log, just return false.
            }

            return false;
        }
    }
}
