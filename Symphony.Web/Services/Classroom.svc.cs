﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using Symphony.Core;

namespace Symphony.Web.Services
{
    // NOTE: If you change the class name "Classroom" here, you must also update the reference to "Classroom" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class ClassroomService : BaseService, IClassroomService
    {
        ILog Log = LogManager.GetLogger(typeof(ClassroomService));

        public Stream GetCourseFile(string fileId)
        {
            try
            {
                int ifileId = 0;
                if (int.TryParse(fileId, out ifileId))
                {
                    string name = string.Empty;
                    Stream s = (new ClassroomController()).GetCourseFile(CustomerID, UserID, ifileId, ref name);
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + name.Replace(" ", "_").Replace(";", "_"));
                    return s;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public SingleResult<bool> SendMessage(List<Message> messages)
        {
            try
            {
                if ((new NotificationController()).QuickSendNotification(CustomerID, UserID, messages).Count == messages.Count)
                {
                    return new SingleResult<bool>(true);
                }
                return new SingleResult<bool>(false);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public MultipleResult<User> GetCTManagers()
        {
            try
            {
                return new MultipleResult<User>(UserController.GetUsersByRole(CustomerID, UserID, Roles.ClassroomManager, ""));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<User>(ex);
            }
        }

        public PagedResult<Course> GetCourses(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindCourses(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Course>(ex);
            }
        }

        public PagedResult<ClassroomClass> GetClasses(string searchText, int start, int limit, string sort, string dir, string filter)
        {
            try
            {
                ClassesFilter classFilter = null;
                if (!string.IsNullOrEmpty(filter))
                {
                    classFilter = Utilities.Deserialize<ClassesFilter>(filter);
                }
                return (new ClassroomController()).FindClasses(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, classFilter);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ClassroomClass>(ex);
            }
        }

        public PagedResult<ClassOrOnlineCourse> GetInstructedClasses(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                bool optimizeInstructedClasses = false;

                try
                {
                    optimizeInstructedClasses = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["optimizeInstructedClasses"]);
                }
                catch (Exception e)
                {
                    Log.Error("optimizeInstructedClasses flag not added to webconfig - assuming not optimized.");
                }
                PagedResult<ClassOrOnlineCourse> courses = null;

                Log.Info("Staring FindInstructedClasses");
                DateTime startTime = DateTime.Now;

                if (optimizeInstructedClasses)
                {
                    Log.Info("OPTIMIZED!!    :)");
                    courses = (new ClassroomController()).FindInstructedClassesOptimized(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
                }
                else
                {
                    Log.Info("Not optimized....... :(");
                    courses = (new ClassroomController()).FindInstructedClasses(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
                }

                Log.Info("Getting Instructed Classed Took: " + (DateTime.Now - startTime).TotalMilliseconds + "ms");
                return courses;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ClassOrOnlineCourse>(ex);
            }
        }

        public PagedResult<ClassOrOnlineCourse> GetEnrolledClasses(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindEnrolledClasses(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ClassOrOnlineCourse>(ex);
            }
        }

        public SingleResult<ClassOrOnlineCourse> GetEnrolledClass(string trainingProgramId, string classId, string courseTypeId)
        {
            try
            {
                int tpid = 0;
                CourseType courseType = CourseType.Online;
                int cid = 0;
                if (!int.TryParse(trainingProgramId, out tpid)) {
                    throw new Exception ("Invalid training program id.");
                }
                if (!int.TryParse(classId, out cid)) {
                    throw new Exception ("Invalid course id.");
                }
                if (!Enum.TryParse<CourseType>(courseTypeId, out courseType)) {
                    throw new Exception ("Invalid course type id.");
                }
                
                return (new ClassroomController()).FindEnrolledClass(CustomerID, UserID, tpid, cid, courseType);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ClassOrOnlineCourse>(ex);
            }
        }

        public PagedResult<Venue> GetVenues(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindVenues(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Venue>(ex);
            }
        }

        public PagedResult<Room> GetRooms(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindRooms(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Room>(ex);
            }
        }

        public PagedResult<PresentationType> GetPresentationTypes(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindPresentationTypes(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<PresentationType>(ex);
            }
        }

        public PagedResult<Core.Models.CourseCompletionType> GetCourseCompletionTypes(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindCourseCompletionTypes(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Core.Models.CourseCompletionType>(ex);
            }
        }

        public PagedResult<State> GetStates(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindStates(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<State>(ex);
            }
        }

        public PagedResult<Core.Models.TimeZone> GetTimeZones(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindTimeZones(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Core.Models.TimeZone>(ex);
            }
        }

        public PagedResult<Course> GetOnlineCourses(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindOnlineCourses(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Course>(ex);
            }
        }

        public PagedResult<Course> GetOnlineSurveys(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindOnlineSurveys(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Course>(ex);
            }
        }

        public PagedResult<ClassInstructor> GetClassInstructors(string classId, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).GetClassInstructors(CustomerID, UserID, classId, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ClassInstructor>(ex);
            }
        }

        public PagedResult<ClassResource> GetClassResources(string classId, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).GetClassResources(CustomerID, UserID, classId, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ClassResource>(ex);
            }
        }

        public PagedResult<ClassDate> GetClassDates(string classId, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).GetClassDates(CustomerID, UserID, classId, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ClassDate>(ex);
            }
        }

        public PagedResult<Registration> GetRegistrations(string classId, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).GetRegistrations(CustomerID, UserID, classId, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Registration>(ex);
            }
        }

        public PagedResult<Room> GetVenueRooms(string venueId, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).GetVenueRooms(CustomerID, UserID, venueId, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Room>(ex);
            }
        }

        //public PagedResult<VenueResourceManager> GetVenueResourceManagers(string venueId, int start, int limit, string sort, string dir)
        //{
        //    try
        //    {
        //        return (new ClassroomController()).GetVenueResourceManagers(CustomerID, UserID, venueId, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex);
        //        return new PagedResult<VenueResourceManager>(ex);
        //    }
        //}

        //public PagedResult<Resource> GetVenueResources(string venueId, int start, int limit, string sort, string dir)
        //{
        //    try
        //    {
        //        return (new ClassroomController()).GetVenueResources(CustomerID, UserID, venueId, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex);
        //        return new PagedResult<Resource>(ex);
        //    }
        //}

        public MultipleResult<WebinarRegistrationAttempt> CreateRegistrations(string classId)
        {
            try
            {
                return (new ClassroomController()).CreateRegistrations(CustomerID, UserID, int.Parse(classId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<WebinarRegistrationAttempt>(ex);
            }
        }

        public PagedResult<User> GetClassInstructorList(List<ClassInstructor> exclude, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).GetClassInstructorList(CustomerID, UserID, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, exclude.Select(e => e.InstructorID).ToList());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<Resource> GetClassResourceList(List<ClassResource> exclude, string venueId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).GetClassResourceList(CustomerID, UserID, int.Parse(venueId), searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, exclude.Select(e => e.ResourceID).ToList());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Resource>(ex);
            }
        }

        public PagedResult<User> GetRegistrantList(List<Registration> exclude, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).GetRegistrantList(CustomerID, UserID, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, exclude.Select(e => e.RegistrantID).ToList());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<User> GetVenueResourceManagerList(List<VenueResourceManager> exclude, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).GetVenueResourceManagerList(CustomerID, UserID, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, exclude.Select(e => e.ResourceManagerID).ToList());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public SingleResult<List<CalendarWeek>> GetCalendarWeeks(string year, string month, string filter)
        {
            try
            {
                return (new ClassroomController()).GetCalendarWeeks(CustomerID, UserID, year, month, filter.Split('_'));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<List<CalendarWeek>>(ex);
            }
        }

        public SingleResult<Course> GetCourse(string courseId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(courseId, out id))
                {
                    return (new ClassroomController()).GetCourse(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid course id.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Course>(ex);
            }
        }

        public SingleResult<ClassroomClass> GetClass(string classId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(classId, out id))
                {
                    return (new ClassroomController()).GetClass(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid class id.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ClassroomClass>(ex);
            }
        }

        public SingleResult<Venue> GetVenue(string venueId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(venueId, out id))
                {
                    return (new ClassroomController()).GetVenue(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid venue id.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Venue>(ex);
            }
        }

        public SingleResult<Resource> GetResource(string resourceId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(resourceId, out id))
                {
                    return (new ClassroomController()).GetResource(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid room id.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Resource>(ex);
            }
        }

        public SingleResult<Room> GetRoom(string roomId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(roomId, out id))
                {
                    return (new ClassroomController()).GetRoom(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid room id.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Room>(ex);
            }
        }

        public SingleResult<CalendarClass> SaveCalendarClass(CalendarClass klass)
        {
            try
            {
                return (new ClassroomController()).SaveCalendarClass(CustomerID, UserID, klass);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<CalendarClass>(ex);
            }
        }

        public SingleResult<Course> SaveCourse(string courseId, Course course)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(courseId, out i))
                {
                    throw new Exception("Invalid courseId specified.");
                }
                return (new ClassroomController()).SaveCourse(CustomerID, UserID, i, course);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Course>(ex);
            }
        }

        public SingleResult<ClassroomClass> SaveClass(string classId, ClassroomClass klass)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(classId, out i))
                {
                    throw new Exception("Invalid classId specified.");
                }
                return (new ClassroomController()).SaveClass(CustomerID, UserID, i, klass);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ClassroomClass>(ex);
            }
        }

        public SingleResult<ClassroomClass> UpdateRegistrations(string classId, ClassroomClass klass)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(classId, out i))
                {
                    throw new Exception("Invalid classId specified.");
                }
                return (new ClassroomController()).UpdateRegistrations(CustomerID, UserID, i, klass);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ClassroomClass>(ex);
            }
        }

        public SingleResult<Venue> SaveVenue(string venueId, Venue venue)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(venueId, out i))
                {
                    throw new Exception("Invalid venueId specified.");
                }
                return (new ClassroomController()).SaveVenue(CustomerID, UserID, i, venue);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Venue>(ex);
            }
        }

        public SingleResult<ClassInstructor> SaveClassInstructor(string classInstructorId, ClassInstructor classInstructor)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(classInstructorId, out i))
                {
                    throw new Exception("Invalid classInstructorId specified.");
                }
                return (new ClassroomController()).SaveClassInstructor(CustomerID, UserID, i, classInstructor);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ClassInstructor>(ex);
            }
        }

        public SingleResult<ClassResource> SaveClassResource(string classResourceId, ClassResource classResource)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(classResourceId, out i))
                {
                    throw new Exception("Invalid classResourceId specified.");
                }
                return (new ClassroomController()).SaveClassResource(CustomerID, UserID, i, classResource);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ClassResource>(ex);
            }
        }

        public SingleResult<VenueResourceManager> SaveVenueResourceManager(string venueResourceManagerId, VenueResourceManager venueResourceManager)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(venueResourceManagerId, out i))
                {
                    throw new Exception("Invalid venueResourceManagerId specified.");
                }
                return (new ClassroomController()).SaveVenueResourceManager(CustomerID, UserID, i, venueResourceManager);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<VenueResourceManager>(ex);
            }
        }

        public SingleResult<Resource> SaveResource(string resourceId, Resource resource)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(resourceId, out i))
                {
                    throw new Exception("Invalid resourceId specified.");
                }
                return (new ClassroomController()).SaveResource(CustomerID, UserID, i, resource);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Resource>(ex);
            }
        }

        public SingleResult<Room> SaveRoom(string roomId, Room room)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(roomId, out i))
                {
                    throw new Exception("Invalid roomId specified.");
                }
                return (new ClassroomController()).SaveRoom(CustomerID, UserID, i, room);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Room>(ex);
            }
        }

        public SingleResult<Registration> SaveRegistration(string registrationId, Registration registration)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(registrationId, out i))
                {
                    throw new Exception("Invalid registrationId specified.");
                }
                return (new ClassroomController()).SaveRegistration(CustomerID, UserID, i, registration);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Registration>(ex);
            }
        }

        public SingleResult<List<ClassDate>> RescheduleClass(string classId, ClassDate classDate)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(classId, out i))
                {
                    throw new Exception("Invalid classId specified.");
                }
                return (new ClassroomController()).RescheduleClass(CustomerID, UserID, i, classDate);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<List<ClassDate>>(ex);
            }
        }

        public SingleResult<bool> DeleteCourse(string courseId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(courseId, out i))
                {
                    throw new Exception("Invalid courseId specified.");
                }
                return (new ClassroomController()).DeleteCourse(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteClass(string classId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(classId, out i))
                {
                    throw new Exception("Invalid classId specified.");
                }
                return (new ClassroomController()).DeleteClass(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteVenue(string venueId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(venueId, out i))
                {
                    throw new Exception("Invalid venueId specified.");
                }
                return (new ClassroomController()).DeleteVenue(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteClassInstructor(string classInstructorId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(classInstructorId, out i))
                {
                    throw new Exception("Invalid classInstructorId specified.");
                }
                return (new ClassroomController()).DeleteClassInstructor(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteClassResource(string classResourceId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(classResourceId, out i))
                {
                    throw new Exception("Invalid classResourceId specified.");
                }
                return (new ClassroomController()).DeleteClassResource(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteVenueResourceManager(string venueResourceManagerId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(venueResourceManagerId, out i))
                {
                    throw new Exception("Invalid venueResourceManagerId specified.");
                }
                return (new ClassroomController()).DeleteVenueResourceManager(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteResource(string resourceId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(resourceId, out i))
                {
                    throw new Exception("Invalid resourceId specified.");
                }
                return (new ClassroomController()).DeleteResource(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteRoom(string roomId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(roomId, out i))
                {
                    throw new Exception("Invalid roomId specified.");
                }
                return (new ClassroomController()).DeleteRoom(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteRegistration(string registrationId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(registrationId, out i))
                {
                    throw new Exception("Invalid registrationId specified.");
                }
                return (new ClassroomController()).DeleteRegistration(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public PagedResult<CourseFile> GetCourseFiles(string courseId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindCourseFiles(CustomerID, UserID, int.Parse(courseId), searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<CourseFile>(ex);
            }
        }

        public SingleResult<bool> DeleteCourseFile(string courseFileId)
        {
            try
            {
                return (new ClassroomController()).DeleteCourseFile(CustomerID, UserID, int.Parse(courseFileId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public MultipleResult<PendingRegistration> GetPendingRegistrations()
        {
            try
            {
                return (new ClassroomController()).GetPendingRegistrations(CustomerID, UserID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<PendingRegistration>(ex);
            }
        }

        public SingleResult<bool> SavePendingRegistrations(List<PendingRegistration> registrations)
        {
            try
            {
                return (new ClassroomController()).SavePendingRegistrations(CustomerID, UserID, registrations);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public MultipleResult<User> GetUsersByHierarchy(string hierarchyId, List<string> nodeIds)
        {
            try
            {
                int iHierarchyId = 0;
                if (!int.TryParse(hierarchyId, out iHierarchyId))
                {
                    throw new Exception("Invalid hierarchy ID specified.");
                }

                return (new ClassroomController()).GetUsersByHierarchy(CustomerID, UserID, iHierarchyId, nodeIds.Select(s=>int.Parse(s)).ToList());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<User>(ex);
            }
        }

        public SingleResult<ClassroomClass> SaveThirdPartyClass(ThirdPartyClass klass)
        {
            try
            {
                return (new ClassroomController()).SaveThirdPartyClass(CustomerID, UserID, klass);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ClassroomClass>(ex);
            }
        }

        public PagedResult<Category> GetCategories(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ClassroomController()).FindCategories(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Category>(ex);
            }
        }

        public MultipleResult<Category> SaveCategories(List<Category> categories)
        {
            try
            {
                return (new ClassroomController()).SaveCategories(CustomerID, UserID, categories);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<Category>(ex);
            }
        }

        public MultipleResult<SessionAssignedUser> SaveSessionRegistrations(string sessionId, List<SessionAssignedUser> users)
        {
            try
            {
                int sId;
                if (!int.TryParse(sessionId, out sId)) {
                    throw new Exception("Invalid session id.");
                }

                return (new ClassroomController()).SaveSessionRegistrations(sId, users);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<SessionAssignedUser>(ex);
            }
        }

        public MultipleResult<SessionAssignedUser> SaveSessionRegistrationsAuto(List<SessionAssignedUser> users)
        {
            try
            {
                return (new ClassroomController()).SaveSessionRegistrations(users, false);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<SessionAssignedUser>(ex);
            }
        }
    }
}
