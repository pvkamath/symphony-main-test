﻿using System;
using System.ServiceModel;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.Net;
using System.ServiceModel.Web;

namespace Symphony.Web.Services
{
    /// <summary>
    /// ProctorService is a service class that defines an API for performing actions on objects that
    /// are part of the proctoring module.
    /// </summary>
#if DEBUG
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
#endif
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ProctorService : BaseService, IProctorService
    {
        private ProctorController Controller;
        private ILog Log;

        public ProctorService()
        {
            Controller = new ProctorController();
            Log = LogManager.GetLogger(typeof(ProctorService));
        }

        /// <summary>
        /// Gets or sets the HttpStatusCode to return with the response.
        /// </summary>
        public HttpStatusCode StatusCode
        {
            get
            {
                return WebOperationContext.Current.OutgoingResponse.StatusCode;
            }
            set
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = value;
            }
        }

        /// <summary>
        /// Finds a ProctorForm that matches the provided id.
        /// </summary>
        /// <param name="id">Id of the ProctorForm to return.</param>
        /// <returns>The found ProctorForm. Will return 404 and a result set that wraps a null value
        /// if there is no matching entity.</returns>
        public SingleResult<ProctorForm> FindProctorForm(string id)
        {
            try
            {
                Log.InfoFormat("Handling a request to find a proctor form with id {0}.", id);

                int idValue = Convert.ToInt32(id);
                if (idValue <= 0)
                {
                    throw new ArgumentOutOfRangeException("id", "ID must be a positive integer.");
                }

                var result = Controller.FindProctorForm(idValue);
                if (result.Data == null)
                {
                    StatusCode = HttpStatusCode.NotFound;
                    result.Success = false;
                    result.Error = "Could not find the proctor form.";

                    Log.Error("Could not find the proctor form, returning 404.");
                }

                return result;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to find a proctor form.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<ProctorForm>(ex);
            }
        }

        /// <summary>
        /// Finds the set of ProctorForms that match the query parameters.
        /// </summary>
        /// <param name="searchText">Optional filter text to apply to the query.</param>
        /// <param name="start">Offsets the results by this amount.</param>
        /// <param name="limit">Maximum number of results to return.</param>
        /// <param name="sort">A parameter to sort on, matching one of the ProctorForm's data members.</param>
        /// <param name="dir">The direction to sort on. One of 'ASC' or 'DESC'.</param>
        /// <returns>A result set wrapping all of the matching ProctorForms.</returns>
        public PagedResult<ProctorForm> QueryProctorForms(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                Log.Info("Handling a request to query proctor forms.");

                return Controller.QueryProctorForms(new PagedQueryParams<ProctorForm>(Log, searchText, start, limit, sort, dir));
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to query proctor forms.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new PagedResult<ProctorForm>(ex);
            }
        }

        /// <summary>
        /// Creates a ProctorForm. See below for more information.
        /// </summary>
        /// <param name="model">Model data to create the ProctorForm with.</param>
        /// <returns>The newly-created model, wrapped in a result set.</returns>
        public SingleResult<ProctorForm> CreateProctorFormWithoutId(ProctorForm model)
        {
            return CreateProctorForm(model);
        }

        /// <summary>
        /// Creates a ProctorForm.
        /// </summary>
        /// <param name="id">Shim parameter, will always be 0.</param>
        /// <param name="model">Model data to create the ProctorForm with.</param>
        /// <returns>The newly-created model, wrapped in a result set.</returns>
        public SingleResult<ProctorForm> CreateProctorFormWithId(string id, ProctorForm model)
        {
            // TLDR; ext appends 0 to POST requests erroneously so we #deal like this.
            // https://www.sencha.com/forum/showthread.php?263395-4.2.0-Rest-proxy-appending-integer-IDs-to-URL-when-saving-new-Model-instance
            return CreateProctorForm(model);
        }

        private SingleResult<ProctorForm> CreateProctorForm(ProctorForm model)
        {
            try
            {
                Log.Info("Handling a request to create a new proctor form.");

                return Controller.CreateProctorForm(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to create a new proctor form.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<ProctorForm>(ex);
            }
        }

        /// <summary>
        /// Updates the ProctorForm matching the specified id with the provided model data.
        /// </summary>
        /// <param name="id">Id of the ProctorForm to update.</param>
        /// <param name="model">Model data to update the ProctorForm with.</param>
        /// <returns>The updated model, wrapped in a result set.</returns>
        public SingleResult<ProctorForm> UpdateProctorForm(string id, ProctorForm model)
        {
            try
            {
                Log.Info("Handling a request to update a proctor form.");

                return Controller.UpdateProctorForm(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to update a proctor form.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<ProctorForm>(ex);
            }
        }

        /// <summary>
        /// Deletes the ProctorForm matching the specified id.
        /// </summary>
        /// <param name="id">Id of the ProctorForm to delete.</param>
        /// <param name="model">Model data for the ProctorForm..</param>
        /// <returns>The deleted model, wrapped in a result set.</returns>
        public SingleResult<ProctorForm> DeleteProctorForm(string id, ProctorForm model)
        {
            try
            {
                Log.Info("Handling a request to delete a proctor form.");

                return Controller.DeleteProctorForm(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to delete a proctor form.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<ProctorForm>(ex);
            }
        }

        /// <summary>
        /// Saves a record of a proctor's information.
        /// </summary>
        /// <param name="response">The resposne to the ProctorForm.</param>
        /// <returns>A result set indicating whether or not the response was saved without error.</returns>
        public SimpleSingleResult<bool> SaveProctor(ProctorFormResponse response)
        {
            try
            {
                Log.Info("Handling a request to save a proctor form response.");

                return Controller.SaveProctor(response);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to save a proctor form response.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<bool>(ex);
            }
        }
    }
}
