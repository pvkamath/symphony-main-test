﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using Symphony.Core;
using System.Data;
using Symphony.Core.ThirdParty.Microsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Symphony.Web.Services
{
    // NOTE: If you change the class name "CourseAssignment" here, you must also update the reference to "CourseAssignment" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class CourseAssignmentService : BaseService, ICourseAssignmentService
    {
        ILog Log = LogManager.GetLogger(typeof(CourseAssignmentService));

        public SingleResult<bool> DeletePublicDocument(string publicDocumentId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(publicDocumentId, out id))
                {
                    return (new CourseAssignmentController()).DeletePublicDocument(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid course id.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public Stream DownloadPublicDocument(string documentId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(documentId, out id))
                {
                    return (new CourseAssignmentController()).DownloadPublicDocument(CustomerID, UserID, id);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public PagedResult<LockedUser> GetLockedUsers(string searchText, int start, int limit, string sort, string dir, bool lockedOnly)
        {
            try
            {
                return (new CourseAssignmentController()).GetLockedUsers(CustomerID, UserID, lockedOnly, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<LockedUser>(ex);
            }
        }

        public MultipleResult<OnlineCourseRegistration> GetLockedUserCourses(string userId, bool lockedOnly)
        {
            try
            {
                return (new CourseAssignmentController()).GetLockedUserCourses(CustomerID, UserID, int.Parse(userId), lockedOnly);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<OnlineCourseRegistration>(ex);
            }
        }

        public PagedResult<TrainingProgram> GetTrainingPrograms(string searchText, int start, int limit, string sort, string dir, string filter)
        {
            try
            {
                TrainingProgramFilter f = null;
                if (!String.IsNullOrEmpty(filter))
                {
                    f = Symphony.Core.Utilities.Deserialize<TrainingProgramFilter>(filter);
                }
                return (new CourseAssignmentController()).FindTrainingPrograms(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, f);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<TrainingProgram>(ex);
            }
        }

        public PagedResult<TrainingProgram> GetTrainingProgramsForTemplate(string templateId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(templateId, out i))
                {
                    throw new Exception("Invalid template id.");
                }
                return (new CourseAssignmentController()).FindTrainingProgramsForTemplate(CustomerID, i, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 0 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<TrainingProgram>(ex);
            }
        }


        public PagedResult<TrainingProgram> GetTrainingProgramsHoldByCustomer(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                var usedCustomerId = -1;
                int.TryParse(customerId, out usedCustomerId);

                return (new CourseAssignmentController()).GetTrainingProgramsHoldByCustomer(usedCustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<TrainingProgram>(ex);
            }

        }

        public PagedResult<Category> GetCategories(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CourseAssignmentController()).FindCategories(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Category>(ex);
            }
        }

        public MultipleResult<Category> SaveCategories(List<Category> categories)
        {
            try
            {
                return (new CourseAssignmentController()).SaveCategories(CustomerID, UserID, categories);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<Category>(ex);
            }
        }

        public PagedResult<Course> GetCourses(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CourseAssignmentController()).FindCourses(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Course>(ex);
            }
        }

        public SingleResult<Course> GetCourse(string courseId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(courseId, out id))
                {
                    return (new CourseAssignmentController()).GetCourse(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid course id.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Course>(ex);
            }
        }

        public SingleResult<Course> GetCourseLaunchDetails(string courseId, string trainingProgramId)
        {
            try
            {
                int cid = int.Parse(courseId);
                int tpid = int.Parse(trainingProgramId);
                
                return (new CourseAssignmentController()).GetCourse(CustomerID, UserID, cid, tpid, null);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new SingleResult<Course>(e);
            }
        }


        public SingleResult<Course> SaveCourse(string courseId, Course course)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(courseId, out i))
                {
                    throw new Exception("Invalid courseId specified.");
                }
                return (new CourseAssignmentController()).SaveCourse(CustomerID, UserID, i, course);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Course>(ex);
            }
        }

        public PagedResult<PublicDocument> GetPublicDocuments(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CourseAssignmentController()).FindPublicDocuments(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<PublicDocument>(ex);
            }
        }

        public SingleResult<PublicDocument> GetPublicDocument(string publicDocumentId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(publicDocumentId, out id))
                {
                    return (new CourseAssignmentController()).GetPublicDocument(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid course id.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<PublicDocument>(ex);
            }
        }


        public SingleResult<PublicDocument> SavePublicDocument(string publicDocumentId, PublicDocument document)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(publicDocumentId, out i))
                {
                    throw new Exception("Invalid courseId specified.");
                }
                return (new CourseAssignmentController()).SavePublicDocument(CustomerID, UserID, i, document);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<PublicDocument>(ex);
            }
        }

        public PagedResult<PublicDocumentCategory> GetPublicDocumentCategories(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CourseAssignmentController()).FindPublicDocumentCategories(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<PublicDocumentCategory>(ex);
            }
        }

        public MultipleResult<PublicDocumentCategory> SavePublicDocumentCategories(List<PublicDocumentCategory> categories)
        {
            try
            {
                return (new CourseAssignmentController()).SavePublicDocumentCategories(CustomerID, UserID, categories);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<PublicDocumentCategory>(ex);
            }
        }

        public MultipleResult<Location> GetLocations()
        {
            try
            {
                return (new CourseAssignmentController()).GetLocations(CustomerID, UserID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<Location>(ex);
            }
        }

        public MultipleResult<JobRole> GetJobRoles()
        {
            try
            {
                return (new CourseAssignmentController()).GetJobRoles(CustomerID, UserID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<JobRole>(ex);
            }
        }

        public MultipleResult<Audience> GetAudiences()
        {
            try
            {
                return (new CourseAssignmentController()).GetAudiences(CustomerID, UserID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<Audience>(ex);
            }
        }

        public PagedResult<OnlineCourseRollup> GetOnlineCourseRollups(string trainingProgramId, string onlineCourseId, string searchText, int start, int limit, string sort, string dir, bool lockedOnly)
        {
            try
            {
                int tpId = 0;
                if (!int.TryParse(trainingProgramId, out tpId))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                int ocId = 0;
                if (!int.TryParse(onlineCourseId, out ocId))
                {
                    throw new Exception("Invalid onlineCourseId specified.");
                }
                return (new CourseAssignmentController()).GetOnlineCourseRollups(tpId, ocId, lockedOnly, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<OnlineCourseRollup>(ex);
            }
        }

        public SingleResult<OnlineCourseRollup> GetOnlineCourseRollup(string onlineCourseRollupId)
        {
            try
            {
                int rId = 0;
                if (!int.TryParse(onlineCourseRollupId, out rId))
                {
                    throw new Exception("Invalid course rollup id specified");
                }
                return (new CourseAssignmentController()).GetOnlineCourseRollup(rId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<OnlineCourseRollup>(ex);
            }
        }

        public PagedResult<OnlineCourseRegistration> GetOnlineCourseRegistrations(string trainingProgramId, string onlineCourseId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int tpId = 0;
                if (!int.TryParse(trainingProgramId, out tpId))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                int ocId = 0;
                if (!int.TryParse(onlineCourseId, out ocId))
                {
                    throw new Exception("Invalid onlineCourseId specified.");
                }
                return (new CourseAssignmentController()).GetOnlineCourseRegistrations(tpId, ocId, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<OnlineCourseRegistration>(ex);
            }
        }

        public SingleResult<bool> SaveOnlineCourseRegistrations(string trainingProgramId, string onlineCourseId, OnlineCourseRegistration[] registrations)
        {
            try
            {
                int tpId = 0;
                if (!int.TryParse(trainingProgramId, out tpId))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                int ocId = 0;
                if (!int.TryParse(onlineCourseId, out ocId))
                {
                    throw new Exception("Invalid onlineCourseId specified.");
                }
                return (new CourseAssignmentController()).SaveOnlineCourseRegistrations(tpId, ocId, registrations);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<OnlineCourseRegistration> ResetAttemptCount(string onlineCourseRollupId, string notes)
        {
            try
            {
                int ocrId = 0;
                if (!int.TryParse(onlineCourseRollupId, out ocrId))
                {
                    throw new Exception("Invalid onlineCourseRollupId specified.");
                }
                return (new CourseAssignmentController()).ResetAttemptCount(ocrId, notes);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<OnlineCourseRegistration>(ex);
            }
        }

        public SingleResult<OnlineCourseRegistration> DeleteRegistration(string onlineCourseRollupId, string notes, string cleanUpAssignments)
        {
            try
            {
                int ocrId = 0;
                bool clnUpAssignments = cleanUpAssignments.ToLower() == "true";
                if (!int.TryParse(onlineCourseRollupId, out ocrId))
                {
                    throw new Exception("Invalid onlineCourseRollupId specified.");
                }
                return (new CourseAssignmentController()).DeleteRegistration(ocrId, notes, clnUpAssignments);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<OnlineCourseRegistration>(ex);
            }
        }

        public PagedResult<Course> FindAvailableTrainingProgramCourses(List<CourseRecord> exclude, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CourseAssignmentController()).GetAvailableTrainingProgramCourses(CustomerID, exclude, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Course>(ex);
            }
        }

        public PagedResult<Course> FindAssignedTrainingProgramCourses(string trainingProgramId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(trainingProgramId, out i))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                return (new CourseAssignmentController()).GetAssignedTrainingProgramCourses(CustomerID, i, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Course>(ex);
            }
        }

        public PagedResult<TrainingProgramFile> FindTrainingProgramFiles(string trainingProgramId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(trainingProgramId, out i))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                return (new CourseAssignmentController()).GetTrainingProgramFiles(CustomerID, i, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<TrainingProgramFile>(ex);
            }
        }

        public SingleResult<TrainingProgram> SaveTrainingProgram(string trainingProgramId, string isCheckForRollupStr, TrainingProgram trainingProgram)
        {
            try
            {
                int i = 0;
                bool isCheckForRollup = false;

                bool.TryParse(isCheckForRollupStr, out isCheckForRollup);

                if (!int.TryParse(trainingProgramId, out i))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                return (new CourseAssignmentController()).SaveTrainingProgram(CustomerID, UserID, i, trainingProgram, isCheckForRollup);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<TrainingProgram>(ex);
            }
        }

        public SingleResult<bool> DeleteTrainingProgram(string trainingProgramId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(trainingProgramId, out i))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                return (new CourseAssignmentController()).DeleteTrainingProgram(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> UnassignTrainingProgram(string trainingProgramId, string userId)
        {
            try
            {
                int tpId = 0;
                int uid = 0;
                if (!int.TryParse(trainingProgramId, out tpId)) {
                    throw new Exception("Invalid trainingProgramId specificed.");
                }
                if (!int.TryParse(userId, out uid)) {
                    throw new Exception("Invalid userId specified.");
                }

                return (new CourseAssignmentController()).UnassignTrainingProgram(CustomerID, UserID, tpId, uid, false);
            }
            catch (Exception ex) {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> UnassignTrainingProgramBundle(string trainingProgramId, string userId)
        {
            try
            {
                int tpId = 0;
                int uid = 0;
                if (!int.TryParse(trainingProgramId, out tpId)) {
                    throw new Exception("Invalid trainingProgramId specificed.");
                }
                if (!int.TryParse(userId, out uid)) {
                    throw new Exception("Invalid userId specified.");
                }

                return (new CourseAssignmentController()).UnassignTrainingProgram(CustomerID, UserID, tpId, uid, true);
            }
            catch (Exception ex) {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }
        public SingleResult<bool> DeleteNotificationTemplate(string notificationTemplateId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(notificationTemplateId, out i))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                return (new CourseAssignmentController()).DeleteNotificationTemplate(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public PagedResult<User> GetSpecificUsers(int[] userIds, int start, int limit, string sort, string dir)
        {
            try
            {
                // loop through the query parameters and get the users
                return (new CourseAssignmentController()).GetSpecificUsers(CustomerID, userIds, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public SingleResult<bool> UpdateTrainingProgramLive(string trainingProgramId, string isLive)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(trainingProgramId, out i))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                bool b = false;
                if (!bool.TryParse(isLive, out b))
                {
                    throw new Exception("Invalid isLive specified.");
                }
                return (new CourseAssignmentController()).UpdateTrainingProgramLive(CustomerID, UserID, i, b);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> UpdateCourseUserStartTime(string courseId)
        {
            try
            {
                int cid = 0;
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id.");
                }

                return (new CourseAssignmentController()).UpdateCourseUserStartTime(controller.ActualCustomerID, controller.ActualUserID, cid, 0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> UpdateTrainingProgramUserStartTime(string trainingProgramId, string courseId)
        {
            try
            {
                int tpid = 0;
                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Invalid training program id.");
                }
                int cid = 0;
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id.");
                }
                return (new CourseAssignmentController()).UpdateCourseUserStartTime(controller.ActualCustomerID, controller.ActualUserID, cid, tpid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> SaveCourseExtension(string trainingProgramId, string courseId, string userId, string minutes)
        {
            try
            {
                int tpid = 0;
                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Invalid training program id.");
                }
                int cid = 0;
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id.");
                }
                int uid = 0;
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id.");
                }
                double m = 0;
                if (!double.TryParse(minutes, out m))
                {
                    throw new Exception("Invalid amount of time to extend.");
                }

                return (new CourseAssignmentController()).SaveCourseExtension(tpid, cid, uid, m);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteCourseExtension(string trainingProgramId, string courseId, string userId)
        {
            try
            {
                int tpid = 0;
                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Invalid training program id.");
                }
                int cid = 0;
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id.");
                }
                int uid = 0;
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id.");
                }
                
                return (new CourseAssignmentController()).DeleteCourseExtension(tpid, cid, uid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteTrainingProgramFile(string trainingProgramFileId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(trainingProgramFileId, out i))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                return (new CourseAssignmentController()).DeleteTrainingProgramFile(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> UpdateTrainingProgramNewHire(string trainingProgramId, string isNewHire)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(trainingProgramId, out i))
                {
                    throw new Exception("Invalid trainingProgramId specified.");
                }
                bool b = false;
                if (!bool.TryParse(isNewHire, out b))
                {
                    throw new Exception("Invalid isNewHire specified.");
                }
                return (new CourseAssignmentController()).UpdateTrainingProgramNewHire(CustomerID, UserID, i, b);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public MultipleResult<ScheduleParameter> GetScheduleParameters()
        {
            try
            {
                return (new CourseAssignmentController()).GetScheduleParameters();

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<ScheduleParameter>(ex);
            }
        }

        public PagedResult<NotificationTemplate> FindNotificationTemplates(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CourseAssignmentController()).GetNotificationTemplates(CustomerID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<NotificationTemplate>(ex);
            }
        }

        public PagedResult<NotificationTemplate> FindNotificationTemplatesDetail(string searchText, int start, int limit, string sort, string dir, string filter)
        {
            try
            {
                List<GenericFilter> filters = new List<GenericFilter>();

                if (!string.IsNullOrEmpty(filter))
                {
                    try
                    {
                        filters = Symphony.Core.Utilities.Deserialize<List<GenericFilter>>(filter);
                    }
                    catch (Exception e)
                    {
                        Log.Error("Could not convert " + filter + " to List<GenericFilter> - ignoring the filters. Error: " + e);
                    }
                }

                return (new CourseAssignmentController()).GetNotificationTemplatesDetail(CustomerID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, filters);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<NotificationTemplate>(ex);
            }
        }

        public MultipleResult<NotificationTemplate> GetNotificationTemplatesForTrainingProgram(string trainingProgramId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(trainingProgramId, out i))
                {
                    throw new Exception("Invalid training program id specified.");
                }
                return (new CourseAssignmentController()).GetNotificationTemplatesForTrainingProgram(i);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<NotificationTemplate>(ex);
            }
        }

        public MultipleResult<NotificationTemplate> GetUserCreatableSystemTemplates()
        {
            try
            {
                return (new CourseAssignmentController()).GetUserCreatableSystemTemplates();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<NotificationTemplate>(ex);
            }
        }

        public SingleResult<NotificationTemplate> GetNotificationTemplate(string notificationTemplateId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(notificationTemplateId, out i))
                {
                    throw new Exception("Invalid notificationTemplateId specified.");
                }
                return (new CourseAssignmentController()).GetNotificationTemplate(CustomerID, i);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<NotificationTemplate>(ex);
            }
        }

        public SingleResult<NotificationTemplate> SaveNotificationTemplate(string notificationTemplateId, NotificationTemplate notificationTemplate)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(notificationTemplateId, out i))
                {
                    throw new Exception("Invalid notificationTemplateId specified.");
                }
                return (new CourseAssignmentController()).SaveNotificationTemplate(CustomerID, i, notificationTemplate);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<NotificationTemplate>(ex);
            }
        }

        public SimpleSingleResult<string> PreviewNotificationTemplate(string templateId, string scheduleParameterId, string template)
        {
            try
            {

                return new SimpleSingleResult<string>(NotificationTemplateController.PreviewTemplate(int.Parse(templateId), int.Parse(scheduleParameterId), template));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SimpleSingleResult<string>(ex);
            }
        }

        public SingleResult<bool> SaveMessage(QuickMessage message)
        {
            try
            {
                return (new CourseAssignmentController()).SendMessage(message);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> ClearTemplateHistory(string templateId)
        {
            try
            {
                return (new CourseAssignmentController()).ClearTemplateHistory(CustomerID, int.Parse(templateId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public PagedResult<Message> FindTemplateHistory(string templateId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CourseAssignmentController()).GetTemplateHistory(CustomerID, int.Parse(templateId), searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Message>(ex);
            }
        }

        public Stream ExportTemplateHistory(string templateId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                PagedResult<Message> results = (new CourseAssignmentController()).GetTemplateHistory(CustomerID, int.Parse(templateId), searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
                DataSet ds = Utilities.ConvertToDataSet<Message>(results.Data);
                SetDownloadHeaders("template_" + templateId + ".xml");
                return ExcelFormatter.CreateWorkbookStream(ds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }
        
        public SimpleSingleResult<string> PackageCourses(BulkDownloadRequest bulkDownloadRequest)
        {
            try
            {
                return (new CourseController()).GetPackagesFileName(bulkDownloadRequest.CourseIDs, bulkDownloadRequest.CourseOverride, bulkDownloadRequest.ParameterOverrides);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw (ex);
            }
        }

        public Stream DownloadCourses(string fileName)
        {
            try
            {
                fileName = fileName + ".zip";
                return (new CourseController()).GetPackages(fileName);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw (ex);
            }
        }

        public MultipleResult<ScormRecord> GetOnlineCourseStatus(string onlineCourseRollupId)
        {
            try
            {
                int rid;

                if (!int.TryParse(onlineCourseRollupId, out rid))
                {
                    throw new Exception("Invalid online course rollup id");
                }


                return new CourseAssignmentController().GetOnlineCourseStatus(rid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw (ex);
            }
        }

        public MultipleResult<ScormRecord> UpdateOnlineCourseStatus(ScormCourseProgress progress, string onlineCourseRollupId)
        {
            try
            {
                int rid;

                if (!int.TryParse(onlineCourseRollupId, out rid))
                {
                    throw new Exception("Invalid online course rollup id");
                }

                return new CourseAssignmentController().UpdateOnlineCourseStatus(progress, rid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw (ex);
            }
        }

        public SimpleSingleResult<bool> SaveLaunchMeta(LaunchMeta launchMeta, string userId, string courseId, string trainingProgramId)
        {
            try
            {
                int uid, cid, tpid;

                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id.");
                }
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id.");
                }
                if (!int.TryParse(trainingProgramId, out tpid)) {
                    throw new Exception("Invalid training program id.");
                }

                return new CourseAssignmentController().SaveLaunchMeta(uid, cid, tpid, launchMeta);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw (ex);
            }
        }


        #region TrainingProgramBundles
        public PagedResult<TrainingProgramBundle> GetTrainingProgramBundles(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CourseAssignmentController()).GetTrainingProgramBundles(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<TrainingProgramBundle>(ex);
            }
        }

        public SingleResult<TrainingProgramBundle> GetTrainingProgramBundle(string trainingProgramBundleId)
        {
            try
            {
                int i;
                if (!int.TryParse(trainingProgramBundleId, out i)) {
                    throw new Exception("Invalid trainingProgramBundleId");
                }
                return (new CourseAssignmentController()).GetTrainingProgramBundle(CustomerID, UserID, i);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<TrainingProgramBundle>(ex);
            }
        }

        public SimpleSingleResult<bool> DeleteTrainingProgramBundle(string trainingProgramBundleId)
        {
            try
            {
                int i;
                if (!int.TryParse(trainingProgramBundleId, out i))
                {
                    throw new Exception("Invalid trainingProgramBundleId");
                }
                return (new CourseAssignmentController()).DeleteTrainingProgramBundle(CustomerID, UserID, i);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SimpleSingleResult<bool>(ex);
            }
        }

        public SingleResult<TrainingProgramBundle> SaveTrainingProgramBundle(string trainingProgramBundleId, TrainingProgramBundle trainingProgramBundle)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(trainingProgramBundleId, out i))
                {
                    throw new Exception("Invalid trainingProgramBundleId specified.");
                }
                return (new CourseAssignmentController()).SaveTrainingProgramBundle(CustomerID, UserID, i, trainingProgramBundle);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<TrainingProgramBundle>(ex);
            }
        }
        #endregion


        public SingleResult<DisplayLaunchStep> GetLaunchStep(string codename, string trainingProgramId, string courseId, string userId)
        {
            try
            {
                int tpId, cId, uId;
                if (!int.TryParse(trainingProgramId, out tpId))
                {
                    throw new Exception("Invalid training program id.");
                }
                if (!int.TryParse(courseId, out cId))
                {
                    throw new Exception("Invalid course id");
                }
                if (!int.TryParse(userId, out uId))
                {
                    throw new Exception("Invalid user id");
                }

                return new CourseAssignmentController().GetLaunchStep(codename, tpId, cId, uId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<DisplayLaunchStep>(ex);
            }
        }

        public SingleResult<LaunchMeta> SaveProfession(LaunchMeta launchMeta)
        {
            try
            {
                if (string.IsNullOrEmpty(launchMeta.ProfessionJsonString))
                {
                    throw new Exception("professionJsonString field missing");
                }

                JObject professionData = JsonConvert.DeserializeObject<JObject>(launchMeta.ProfessionJsonString);

                int pId, uId, tpId;

                if (professionData["professionId"] == null || !int.TryParse(professionData["professionId"].ToString(), out pId))
                {
                    throw new Exception("professionJsonString missing valid profession id");
                }
                if (professionData["trainingProgramId"] == null || !int.TryParse(professionData["trainingProgramId"].ToString(), out tpId))
                {
                    throw new Exception("professionJsonString missing valid training program id");
                }
                if (professionData["userId"] == null || !int.TryParse(professionData["userId"].ToString(), out uId))
                {
                    throw new Exception("professionJsonString missing valid profession id");
                }

                return new CourseAssignmentController().SaveProfession(tpId, uId, pId, launchMeta);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<LaunchMeta>(ex);
            }
        }
        public SingleResult<LaunchMeta> SaveAffidavit(LaunchMeta launchMeta)
        {
            try
            {
                if (string.IsNullOrEmpty(launchMeta.AffidavitJsonString))
                {
                    throw new Exception("affidavitJsonString field missing");
                }
                JObject affidavitData = JsonConvert.DeserializeObject<JObject>(launchMeta.AffidavitJsonString);

                int cId, uId, tpId;

                // Course id is not required - if not set, this is an affidavit for the training program
                if (affidavitData["courseId"] == null || !int.TryParse(affidavitData["courseId"].ToString(), out cId))
                {
                    cId = 0;
                }

                if (affidavitData["trainingProgramId"] == null || !int.TryParse(affidavitData["trainingProgramId"].ToString(), out tpId))
                {
                    throw new Exception("affidavitJsonString missing valid training program id");
                }
                if (affidavitData["userId"] == null || !int.TryParse(affidavitData["userId"].ToString(), out uId))
                {
                    throw new Exception("affidavitJsonString missing valid user id");
                }

                return new CourseAssignmentController().SaveAffidavitResponse(tpId, uId, cId, launchMeta);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<LaunchMeta>(ex);
            }
        }
        public SingleResult<LaunchMeta> SaveProctor(LaunchMeta launchMeta)
        {
            try
            {
                if (string.IsNullOrEmpty(launchMeta.ProctorJsonString))
                {
                    throw new Exception("proctorJsonString field missing");
                }
                JObject proctorData = JsonConvert.DeserializeObject<JObject>(launchMeta.ProctorJsonString);

                int cId, uId, tpId, pfId;

                if (proctorData["courseId"] == null || !int.TryParse(proctorData["courseId"].ToString(), out cId))
                {
                    throw new Exception("proctorJsonString missing valid course id");
                }
                if (proctorData["trainingProgramId"] == null || !int.TryParse(proctorData["trainingProgramId"].ToString(), out tpId))
                {
                    throw new Exception("proctorJsonString missing valid training program id");
                }
                if (proctorData["userId"] == null || !int.TryParse(proctorData["userId"].ToString(), out uId))
                {
                    throw new Exception("proctorJsonString missing valid user id");
                }
                if (proctorData["proctorFormId"] == null || !int.TryParse(proctorData["proctorFormId"].ToString(), out pfId)) {
                    throw new Exception("proctorJsonString missing valid proctor form id");
                }

                return new CourseAssignmentController().SaveProctor(tpId, cId, uId, pfId, launchMeta);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<LaunchMeta>(ex);
            }
        }
        public SingleResult<bool> SaveValidationParameters(LaunchMeta launchMeta)
        {
            try
            {
                if (string.IsNullOrEmpty(launchMeta.ValidationJsonString))
                {
                    throw new Exception("validationJsonString field missing");
                }

                JObject validationData = JsonConvert.DeserializeObject<JObject>(launchMeta.ValidationJsonString);

                int uId;

                if (validationData["userId"] == null || !int.TryParse(validationData["userId"].ToString(), out uId))
                {
                    throw new Exception("validationJsonString missing valid user id");
                }
                if (validationData["dob"] == null || string.IsNullOrEmpty(validationData["dob"].ToString()))
                {
                    throw new Exception("validationJsonString missing DOB");
                }
                if (validationData["ssn"] == null || string.IsNullOrEmpty(validationData["ssn"].ToString()))
                {
                    throw new Exception("proctorJsonString missing valid user id");
                }

                return new CourseAssignmentController().SaveValidationParameters(uId, validationData["ssn"].ToString(), validationData["dob"].ToString());
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new SingleResult<bool>(e);
            }
        }


        public SingleResult<bool> SavePreValidation(LaunchMeta launchMeta)
        {
            try
            {
                JObject preValidationData = JsonConvert.DeserializeObject<JObject>(launchMeta.PreValidationJsonString);

                int uId;
                int tpid;

                if (preValidationData["userId"] == null || !int.TryParse(preValidationData["userId"].ToString(), out uId))
                {
                    throw new Exception("Missing valid user id");
                }

                if (preValidationData["validationKey"] == null || string.IsNullOrEmpty(preValidationData["validationKey"].ToString()))
                {
                    throw new Exception("missing validation key");
                }

                if (preValidationData["validationValue"] == null || string.IsNullOrEmpty(preValidationData["validationValue"].ToString()))
                {
                    throw new Exception("missing validation input");
                }

                if (preValidationData["trainingProgramId"] == null ||
                    !int.TryParse(preValidationData["trainingProgramId"].ToString(), out tpid))
                {
                    throw new Exception("missing validation input");
                }

                return new CourseAssignmentController().SavePreValidation(uId, preValidationData["validationKey"].ToString(), preValidationData["validationValue"].ToString(), tpid);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new SingleResult<bool>(e);
            }
        }

        public MultipleResult<TrainingProgram> GetSessionTrainingProgramsForUsers(string userIds)
        {
            try
            {
                string[] uIdStrings = userIds.Split(',');

                List<int> uIds = uIdStrings.Select(i =>
                {
                    int uId;
                    if (int.TryParse(i, out uId))
                    {
                        return uId;
                    }
                    return 0;
                }).Where(i => i != 0).ToList();

                if (uIds.Count == 0)
                {
                    throw new Exception("At least 1 user id is required");
                }

                return (new CourseAssignmentController()).GetTrainingProgramsForUsers(uIds);
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new MultipleResult<TrainingProgram>(e);
            }
        }

        public PagedResult<SessionAssignedUser> GetSessionAssignedUsers(
            string trainingProgramIds, string sessionCourseId, string dateRange, string showActive, string showInRangeOnly, string  registrationStatus, string userIds,
            string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                string[] tpIdStrings = trainingProgramIds.Split(',');
                string[] uIdStrings = userIds.Split(',');

                bool? showActiveB = Utilities.ParseNullableBool(showActive);
                bool? showInRangeOnlyB = Utilities.ParseNullableBool(showInRangeOnly);
                int? registrationStatusI = Utilities.ParseNullableInt(registrationStatus);
                int? sessionCourseIdI = Utilities.ParseNullableInt(sessionCourseId);

                List<int> tpIds = tpIdStrings.Select(i =>
                {
                    int tpId;
                    if (int.TryParse(i, out tpId))
                    {
                        return tpId;
                    }
                    return 0;
                }).Where(i => i != 0).ToList();

                List<int> uIds = uIdStrings.Select(i =>
                {
                    int uid;
                    if (int.TryParse(i, out uid))
                    {
                        return uid;
                    }
                    return 0;
                }).Where(i => i != 0).ToList();

                return (new CourseAssignmentController()).GetSessionUsers(tpIds, sessionCourseIdI, dateRange, showActiveB, showInRangeOnlyB, registrationStatusI, uIds, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception e)
            {
                Log.Error(e);
                return new PagedResult<SessionAssignedUser>(e);
            }
        }

        public SingleResult<OnlineCourseRollup> GenerateProctorKey(string rollupId, string trainingProgramId, string courseId, string userId)
        {
            try
            {
                int rId = int.Parse(rollupId);
                int tpid = int.Parse(trainingProgramId);
                int cid = int.Parse(courseId);
                int uid = int.Parse(userId);
                return new CourseAssignmentController().GenerateProctorKey(rId, cid, tpid, uid);
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
