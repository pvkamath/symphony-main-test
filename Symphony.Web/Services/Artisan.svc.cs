﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using Symphony.Core;

namespace Symphony.Web.Services
{
    // NOTE: If you change the class name "Artisan" here, you must also update the reference to "Artisan" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class ArtisanService : BaseService, IArtisanService
    {
        ILog Log = LogManager.GetLogger(typeof(ArtisanService));

        public PagedResult<ArtisanCourse> GetCourses(string searchText, int start, int limit, string sort, string dir, string filter)
        {
            try
            {
                CategoryFilter f = null;
                if (!String.IsNullOrEmpty(filter))
                {
                    f = Symphony.Core.Utilities.Deserialize<CategoryFilter>(filter);
                }
                return (new ArtisanController()).FindCourses(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, f);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ArtisanCourse>(ex);
            }
        }

        public PagedResult<ArtisanCourse> GetPublishedCourses(string searchText, int start, int limit, string sort, string dir, bool hidePrevious)
        {
            try
            {
                return (new ArtisanController()).FindPublishedCourses(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, hidePrevious);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ArtisanCourse>(ex);
            }
        }

        public PagedResult<ArtisanTheme> GetThemes(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ArtisanController()).FindThemes(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ArtisanTheme>(ex);
            }
        }

        public SingleResult<ArtisanCourse> GetCourse(string courseId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(courseId, out id))
                {
                    return (new ArtisanController()).GetCourse(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid courseId specified.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ArtisanCourse>(ex);
            }
        }

        public SingleResult<ArtisanCourse> SaveCourse(string courseId, ArtisanCourse course)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(courseId, out i))
                {
                    throw new Exception("Invalid courseId specified.");
                }
                return (new ArtisanController()).SaveCourse(CustomerID, UserID, i, course);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ArtisanCourse>(ex);
            }
        }

        public SingleResult<bool> DeleteCourse(string courseId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(courseId, out i))
                {
                    throw new Exception("Invalid courseId specified.");
                }
                return (new ArtisanController()).DeleteCourse(CustomerID, UserID, i);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public PagedResult<ArtisanTemplate> GetTemplates(int pageType, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ArtisanController()).FindTemplates(CustomerID, UserID, pageType, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ArtisanTemplate>(ex);
            }
        }

        public SingleResult<ArtisanTemplate> GetTemplate(string templateId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(templateId, out id))
                {
                    return (new ArtisanController()).GetTemplate(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid templateId specified.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ArtisanTemplate>(ex);
            }
        }

        public PagedResult<ArtisanAsset> GetAssets(int assetTypeId, string searchText, int start, int limit, string sort, string dir, string filter)
        {
            try
            {
                List<ArtisanAssetFilter> f = null;
                if (!String.IsNullOrEmpty(filter))
                {
                    f = Symphony.Core.Utilities.Deserialize<List<ArtisanAssetFilter>>(filter);
                }
                return (new ArtisanController()).FindAssets(CustomerID, UserID, assetTypeId, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, f);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ArtisanAsset>(ex);
            }
        }

        public SingleResult<ArtisanAsset> GetAsset(string assetId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(assetId, out id))
                {
                    return (new ArtisanController()).GetAsset(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid assetId specified.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ArtisanAsset>(ex);
            }
        }

        public PagedResult<Core.Models.ArtisanAssetType> GetAssetTypes(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ArtisanController()).FindAssetTypes(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Core.Models.ArtisanAssetType>(ex);
            }
        }

        public SingleResult<Core.Models.ArtisanAssetType> GetAssetType(string assetTypeId)
        {
            try
            {
                int id = 0;
                if (int.TryParse(assetTypeId, out id))
                {
                    return (new ArtisanController()).GetAssetType(CustomerID, UserID, id);
                }
                else
                {
                    throw new Exception("Invalid assetTypeId specified.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Core.Models.ArtisanAssetType>(ex);
            }
        }

        public SimpleSingleResult<string> PackageCourse(string courseId, string preview, string deploy, string update, string target, ArtisanCourse course)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(courseId, out i))
                {
                    throw new Exception("Invalid courseId specified.");
                }
                course.Id = i;
                bool bPreview = bool.Parse(preview);
                bool bDeploy = bool.Parse(deploy);
                bool bUpdate = bool.Parse(update);

                // target will be either "symphony" or "mars"
                ArtisanDeploymentTarget eTarget = ArtisanDeploymentTarget.Symphony;
                if (target == "mars")
                {
                    eTarget = ArtisanDeploymentTarget.Mars;
                }
                return new ArtisanController().PackageCourse(CustomerID, UserID, course, bPreview, bDeploy, bUpdate, eTarget);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SimpleSingleResult<string>(ex);
            }
        }
        
        public SingleResult<ArtisanCourse> PublishCourse(string courseId, ArtisanCourse course)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(courseId, out i))
                {
                    throw new Exception("Invalid courseId specified.");
                }
                course.Id = i;

                return new ArtisanController().PublishCourse(CustomerID, UserID, course);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ArtisanCourse>(ex);
            }
        }

        public SingleResult<ArtisanCourse> DuplicateCourse(string courseId, ArtisanCourse course)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(courseId, out i))
                {
                    throw new Exception("Invalid courseId specified.");
                }
                course.Id = i;

                return new ArtisanController().DuplicateCourse(CustomerID, UserID, course);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ArtisanCourse>(ex);
            }
        }

        public Stream ExportCourse(string courseId, string sContentOnly)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(courseId, out i))
                {
                    throw new Exception("Invalid courseId specified.");
                }

                bool contentOnly = false;
                bool.TryParse(sContentOnly, out contentOnly);
                FileInfo fi = new ArtisanController().ExportCourse(CustomerID, UserID, i, contentOnly);


                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + courseId + ".doc");
                return new FileStream(fi.FullName, FileMode.Open);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public Stream GetPackage(string courseId)
        {
            int i = 0;
            if (!int.TryParse(courseId, out i))
            {
                throw new Exception("Invalid courseId specified.");
            }
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + courseId + ".zip");
            return new ArtisanController().GetPackage(CustomerID, UserID, i);
            
        }

        public PagedResult<ArtisanDeploymentPackage> GetDeployments(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // TODO: add check for permissions
                return (new ArtisanController()).FindDeployments(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ArtisanDeploymentPackage>(ex);
            }
        }

        public MultipleResult<ArtisanDeploymentInfo> GetDeployment(string id)
        {
            try
            {
                int i = int.Parse(id);
                // TODO: add check for permissions
                return (new ArtisanController()).GetDeployment(i, CustomerID, UserID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<ArtisanDeploymentInfo>(ex);
            }
        }

        public MultipleResult<ArtisanSourceImportInfo> GetArtisanSourceImportInfo(string id, string searchText)
        {
            try
            {
                int i = int.Parse(id);
                return (new ArtisanController()).GetArtisanSourceImportInfo(i, searchText);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<ArtisanSourceImportInfo>(ex);
            }
 
        }

        public SingleResult<ArtisanDeploymentPackage> ApplyDeployment(ArtisanDeploymentPackage deployment)
        {
            try
            {
                return new ArtisanController().ApplyDeployment(CustomerID, UserID, deployment);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ArtisanDeploymentPackage>(ex);
            }
        }

        public SingleResult<ArtisanAsset> SaveAsset(string assetId, ArtisanAsset asset)
        {
            try
            {
                int id = 0;
                if (int.TryParse(assetId, out id))
                {
                    return (new ArtisanController()).SaveAsset(id, asset);
                }
                else
                {
                    throw new Exception("Invalid assetId specified.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ArtisanAsset>(ex);
            }
        }

        public SingleResult<ArtisanAsset> CreateAsset(ArtisanAsset asset)
        {
            try
            {
                return (new ArtisanController()).SaveAsset(0, asset);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ArtisanAsset>(ex);
            }
        }

        public MultipleResult<MediaPlayer> GetMediaPlayers()
        {
            try
            {
                return (new ArtisanController()).GetMediaPlayers();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<MediaPlayer>(ex);
            }
        }


        #region Parameters

        public PagedResult<Parameter> GetParameters(string searchText, string codes, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ArtisanController()).GetParameters(CustomerID, UserID, codes, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Parameter>(ex);
            }
        }

        public PagedResult<ParameterSet> GetParameterSets(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ArtisanController()).GetParameterSets(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ParameterSet>(ex);
            }
        }

        public PagedResult<ParameterValue> GetParameterValues(string parameterId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int id = int.Parse(parameterId);
                return (new ArtisanController()).GetParameterValues(id, CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ParameterValue>(ex);
            }
        }

        public PagedResult<ParameterValue> GetParameterValuesForOption(string parameterSetOptionId, string codes, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int id = int.Parse(parameterSetOptionId);
                return (new ArtisanController()).GetParameterValuesForOption(id, CustomerID, UserID, codes, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ParameterValue>(ex);
            }
        }


        public SingleResult<ParameterSet> SaveParameterSet(string parameterSetId, ParameterSet parameterSet)
        {
            try
            {
                int id = int.Parse(parameterSetId);
                return (new ArtisanController()).SaveParameterSet(id, parameterSet);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ParameterSet>(ex);
            }
        }

        public SingleResult<Parameter> SaveParameter(string parameterId, Parameter parameter)
        {
            try
            {
                int id = int.Parse(parameterId);
                return (new ArtisanController()).SaveParameter(id, parameter);
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                if (ex.Message.IndexOf("uc_ParameterOption") >= 0)
                {
                    return new SingleResult<Parameter>(new Exception("Only value is permitted per option."));
                }

                return new SingleResult<Parameter>(ex);
            }
        }

        public PagedResult<ParameterSetOption> GetParameterSetOptions(string parameterSetId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int id = int.Parse(parameterSetId);
                return (new ArtisanController()).GetParameterSetOptions(id, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ParameterSetOption>(ex);
            }
        }

        public PagedResult<ParameterSetOption> GetParameterSetOptions(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ArtisanController()).GetParameterSetOptions(searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ParameterSetOption>(ex);
            }
        }

        #endregion

        #region ImportProSchool

        public SingleResult<int> ImportProSchool()
        {
            try
            {
                //return new SingleResult<int>((new ArtisanController()).ImportArtisanCourses());
                return new SingleResult<int>(0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<int>(ex);
            }
        }

        public SingleResult<ArtisanImportJob> GetArtisanImportJob(string id)
        {
            try
            {
                var i = int.Parse(id);
                return ((new ArtisanController()).GetArtisanImportJob(i));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ArtisanImportJob>(ex);
            }
        }

        #endregion
    }
}
