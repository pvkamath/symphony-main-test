﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using Symphony.Core;
using System.Data;
using Symphony.Core.ThirdParty.Microsoft;
using System.Net;
using System.ServiceModel.Web;

namespace Symphony.Web.Services
{
    /// <summary>
    /// <see cref="AffidavitService"/> is a service class that defines an API for performing
    /// actions on objects that are part of the affidavits module.
    /// </summary>
#if DEBUG
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
#endif
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class AffidavitService : BaseService, IAffidavitService
    {
        private AffidavitController Controller;
        private ILog Log;

        public AffidavitService()
        {
            Controller = new AffidavitController();
            Log = LogManager.GetLogger(typeof(AffidavitService));
        }

        /// <summary>
        /// Gets or sets the HttpStatusCode to return with the response.
        /// </summary>
        public HttpStatusCode StatusCode
        {
            get
            {
                return WebOperationContext.Current.OutgoingResponse.StatusCode;
            }
            set
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = value;
            }
        }

        /// <summary>
        /// Finds an Affidavit that matches the provided id.
        /// </summary>
        /// <param name="id">Id of the Affidavit to return.</param>
        /// <returns>The found affidavit. Will return 404 and a result set that wraps a null value
        /// if there is no matching entity.</returns>
        public SingleResult<AffidavitFinalExam> FindAffidavit(string id)
        {
            try
            {
                Log.InfoFormat("Handling a request to find an affidavit with id {0}.", id);

                int idValue = Convert.ToInt32(id);
                if (idValue <= 0)
                {
                    throw new ArgumentOutOfRangeException("id", "Affidavit must be a positive integer.");
                }

                var result = Controller.FindAffidavit(idValue);
                if (result.Data == null)
                {
                    StatusCode = HttpStatusCode.NotFound;
                    result.Success = false;
                    result.Error = "Could not find the affidavit.";

                    Log.Error("Could not find the affidavit, returning 404.");
                }

                return result;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to find an affidavit.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<AffidavitFinalExam>(ex);
            }
        }
        
        /// <summary>
        /// Finds the set of Affidavits that match the query parameters.
        /// </summary>
        /// <param name="searchText">Optional filter text to apply to the query. Applies to the name and description of the accreditation board.</param>
        /// <param name="start">Offsets the results by this amount.</param>
        /// <param name="limit">Maximum number of results to return.</param>
        /// <param name="sort">A parameter to sort on, matching one of the Affidavit's data members.</param>
        /// <param name="dir">The direction to sort on. One of 'ASC' or 'DESC'.</param>
        /// <returns>A result set wrapping all of the matching Affidavits.</returns>
        public PagedResult<AffidavitFinalExam> QueryAffidavits(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                Log.Info("Handling a request to query affidavits.");

                return Controller.QueryAffidavits(new PagedQueryParams<AffidavitFinalExam>(Log, searchText, start, limit, sort, dir));
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to query affidavits.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new PagedResult<AffidavitFinalExam>(ex);
            }
        }

        /// <summary>
        /// Creates an Affidavit. See below for more information.
        /// </summary>
        /// <param name="model">Model data to create the Affidavit with.</param>
        /// <returns>The newly-created model, wrapped in a result set.</returns>
        public SingleResult<AffidavitFinalExam> CreateAffidavitWithoutId(AffidavitFinalExam model)
        {
            return CreateAffidavit(model);
        }

        /// <summary>
        /// Creates an Affidavit.
        /// </summary>
        /// <param name="id">Shim parameter, will always be 0.</param>
        /// <param name="model">Model data to create the Affidavit with.</param>
        /// <returns>The newly-created model, wrapped in a result set.</returns>
        public SingleResult<AffidavitFinalExam> CreateAffidavitWithId(string id, AffidavitFinalExam model)
        {
            // TLDR; ext appends 0 to POST requests erroneously so we #deal like this.
            // https://www.sencha.com/forum/showthread.php?263395-4.2.0-Rest-proxy-appending-integer-IDs-to-URL-when-saving-new-Model-instance
            return CreateAffidavit(model);
        }

        private SingleResult<AffidavitFinalExam> CreateAffidavit(AffidavitFinalExam model)
        {
            try
            {
                Log.Info("Handling a request to create a new affidavit.");

                return Controller.CreateAffidavit(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to create a new affidavit.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<AffidavitFinalExam>(ex);
            }
        }

        /// <summary>
        /// Updates the Affidavit matching the specified id with the provided model data.
        /// </summary>
        /// <param name="id">Id of the Affidavit to update.</param>
        /// <param name="model">Model data to update the Affidavit with.</param>
        /// <returns>The updated model, wrapped in a result set.</returns>
        public SingleResult<AffidavitFinalExam> UpdateAffidavit(string id, AffidavitFinalExam model)
        {
            try
            {
                Log.Info("Handling a request to update an affidavit.");

                return Controller.UpdateAffidavit(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to update an affidavit.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<AffidavitFinalExam>(ex);
            }
        }

        /// <summary>
        /// Deletes the Affidavit matching the specified id.
        /// </summary>
        /// <param name="id">Id of the Affidavit to delete.</param>
        /// <param name="model">Model data for the Affidavit..</param>
        /// <returns>The deleted model, wrapped in a result set.</returns>
        public SingleResult<AffidavitFinalExam> DeleteAffidavit(string id, AffidavitFinalExam model)
        {
            try
            {
                Log.Info("Handling a request to delete an affidavit.");

                return Controller.DeleteAffidavit(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to delete an affidavit.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<AffidavitFinalExam>(ex);
            }
        }

        /// <summary>
        /// Saves a record of a response/agreement to an Affidavit.
        /// </summary>
        /// <param name="response">The resposne to the Afifdavit.</param>
        /// <returns>A result set indicating whether or not the response was saved without error.</returns>
        public SimpleSingleResult<bool> SaveAffidavitResponse(AffidavitFinalExamResponse response)
        {
            try
            {
                Log.Info("Handling a request to save an affidavit response.");

                return Controller.SaveAffidavitResponse(response);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to save an affidavit response.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<bool>(ex);
            }
        }
    }
}
