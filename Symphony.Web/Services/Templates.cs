﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Symphony.Web
{
    public class Templates
    {
        public const string GenericGETParameters = "start={start}&limit={limit}&sort={sort}&dir={dir}";
        public const string GenericGETParametersWithSearch = "searchText={searchText}&" + GenericGETParameters;
        public const string GenericGETParametersFilterable = "filter={filter}&" + GenericGETParametersWithSearch;
    }
}
