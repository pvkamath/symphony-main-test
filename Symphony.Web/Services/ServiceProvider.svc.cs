﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using Symphony.Core;

namespace Symphony.Web.Services
{
    // NOTE: If you change the class name "ServiceProvider" here, you must also update the reference to "ServiceProvider" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class ServiceProviderService : BaseService, IServiceProviderService
    {
        ILog Log = LogManager.GetLogger(typeof(IServiceProviderService));

        public PagedResult<ServiceProvider> GetServiceProviders(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return ServiceProviderController.GetServiceProviders(searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ServiceProvider>(ex);
            }
        }


        public SingleResult<ServiceProvider> PostServiceProvider(string id, ServiceProvider provider)
        {
            try
            {
                int pid = int.Parse(id);

                return ServiceProviderController.SaveServiceProvider(pid, provider);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ServiceProvider>(ex);
            }
        }
    }
}
