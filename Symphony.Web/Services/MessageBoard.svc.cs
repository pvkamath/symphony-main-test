﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using Symphony.Core;

namespace Symphony.Web.Services
{
    // NOTE: If you change the class name "MessageBoard" here, you must also update the reference to "MessageBoard" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class MessageBoardService : BaseService, IMessageBoardService
    {
        ILog Log = LogManager.GetLogger(typeof(MessageBoardService));

        public PagedResult<MessageBoard> GetMessageBoards(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new MessageBoardController()).GetMessageBoards(CustomerID, UserID, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<MessageBoard>(ex);
            }
        }

        public SingleResult<MessageBoard> GetMessageBoard(string messageBoardId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(messageBoardId, out i)) {
                    throw new Exception("Invalid messageBoardId specified.");
                }
                return (new MessageBoardController()).GetMessageBoard(CustomerID, UserID, i);
            }
            catch (Exception ex) 
            {
                Log.Error(ex);
                return new SingleResult<MessageBoard>(ex);
            }
        }

        public SingleResult<MessageBoard> GetMessageBoardForClass(string messageBoardType, string classId)
        {
            try
            {
                MessageBoardType type;
                int id = 0;

                if (!Enum.TryParse<MessageBoardType>(messageBoardType, out type))
                {
                    throw new Exception("Invalid message board type.");
                }
                
                if (!int.TryParse(classId, out id))
                {
                    throw new Exception("Invalid class id specified.");
                }
                return (new MessageBoardController()).GetMessageBoard(CustomerID, UserID, id, type);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<MessageBoard>(ex);
            }
        }

        public SingleResult<MessageBoard> UpdateMessageBoard(string messageBoardId, MessageBoard messageBoard)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(messageBoardId, out i))
                {
                    throw new Exception("Invalid messageBoardId specified.");
                }
                return (new MessageBoardController()).UpdateMessageBoard(CustomerID, UserID, i, messageBoard);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<MessageBoard>(ex);
            }
        }

        public PagedResult<MessageBoardTopic> GetMessageBoardTopicsLastAdded(string messageBoardId, string searchText, int start, int limit, string sort, string dir, string lastAdded)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(messageBoardId, out i))
                {
                    throw new Exception("Invalid messageBoardId specified.");
                }
                int lastAddedId = 0;
                if (!int.TryParse(lastAdded, out lastAddedId))
                {
                    throw new Exception("Invalid topicId specified.");
                }
                return (new MessageBoardController()).GetMessageBoardTopics(CustomerID, UserID, i, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, lastAddedId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<MessageBoardTopic>(ex);
            }
        }

        public SingleResult<MessageBoardTopic> GetMessageBoardTopic(string messageBoardId, string messageBoardTopicId)
        {
            try
            {
                int id = 0;
                if (!int.TryParse(messageBoardId, out id))
                {
                    throw new Exception("Invalid messageBoardId specified.");
                }

                int topicId = 0;
                if (!int.TryParse(messageBoardTopicId, out topicId))
                {
                    throw new Exception("Invalid topicId specified.");
                }

                return (new MessageBoardController()).GetMessageBoardTopic(CustomerID, UserID, id, topicId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<MessageBoardTopic>(ex);
            }
        }

        public PagedResult<MessageBoardTopic> GetMessageBoardTopics(string messageBoardId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(messageBoardId, out i)) {
                    throw new Exception("Invalid messageBoardId specified.");
                }
                return (new MessageBoardController()).GetMessageBoardTopics(CustomerID, UserID, i, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, 0);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<MessageBoardTopic>(ex);
            }
        }

        public PagedResult<MessageBoardPost> GetMessageBoardPosts(string messageBoardTopicId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(messageBoardTopicId, out i))
                {
                    throw new Exception("Invalid messageBoardTopicId specified.");
                }
                return (new MessageBoardController()).GetMessageBoardPosts(CustomerID, UserID, i, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<MessageBoardPost>(ex);
            }
        }

        public SingleResult<MessageBoardPost> GetMessageBoardPost(string messageBoardTopicId, string messageBoardPostId)
        {
            try
            {
                int postId = 0;
                if (!int.TryParse(messageBoardPostId, out postId))
                {
                    throw new Exception("Invalid messageBoardPostId specified.");
                }
                int topicId = 0;
                if (!int.TryParse(messageBoardTopicId, out topicId))
                {
                    throw new Exception("Invalid messageBoardTopicId specified.");
                }
                return (new MessageBoardController()).GetMessageBoardPost(CustomerID, UserID, topicId, postId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<MessageBoardPost>(ex);
            }
        }

        public SingleResult<MessageBoardPost> UpdateMessageBoardPost(string messageBoardPostId, MessageBoardPost messageBoardPost)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(messageBoardPostId, out i))
                {
                    throw new Exception("Invalid messageBoardPostId specified.");
                }
                return (new MessageBoardController()).UpdateMessageBoardPost(CustomerID, UserID, i, messageBoardPost);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<MessageBoardPost>(ex);
            }
        }
        public SingleResult<MessageBoardPost> DeleteMessageBoardPost(string messageBoardPostId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(messageBoardPostId, out i))
                {
                    throw new Exception("Invalid messageBoardPostId specified.");
                }
                return (new MessageBoardController()).DeleteMessageBoardPost(CustomerID, UserID, i);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<MessageBoardPost>(ex);
            }
        }
        public SingleResult<MessageBoardTopic> UpdateMessageBoardTopic(string messageBoardTopicId, MessageBoardTopic messageBoardTopic)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(messageBoardTopicId, out i))
                {
                    throw new Exception("Invalid messageBoardTopicID specified.");
                }
                return (new MessageBoardController()).UpdateMessageBoardTopic(CustomerID, UserID, i, messageBoardTopic);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<MessageBoardTopic>(ex);
            }
        }
        public SingleResult<MessageBoardTopic> DeleteMessageBoardTopic(string messageBoardTopicId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(messageBoardTopicId, out i))
                {
                    throw new Exception("Invalid messageBoardTopicID specified.");
                }
                return (new MessageBoardController()).DeleteMessageBoardTopic(CustomerID, UserID, i);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<MessageBoardTopic>(ex);
            }
        }
    }
}
