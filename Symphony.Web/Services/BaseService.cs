﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core;
using Symphony.Core.Controllers;

namespace Symphony.Web
{
    public class BaseService
    {
        public SymphonyController controller = new SymphonyController();

        public int UserID
        {
            get
            {
                return controller.UserID;
            }
        }

        public bool UserIsNewHire
        {
            get
            {
                return controller.UserIsNewHire;
            }
        }
        
        public int CustomerID
        {
            get
            {
                return controller.CustomerID;
            }
        }

        public Dictionary<string, string> UserAttributes
        {
            get
            {
                return controller.Attributes;
            }
        }

        public bool HasRole(string role)
        {
            return new SymphonyController().HasRole(role);
        }

        protected OrderDirection GetOrderDirection(string orderDir)
        {
            string dir = (orderDir ?? "").ToLower();
            switch (dir)
            {
                case "desc":
                    return OrderDirection.Descending;
                case "asc":
                    return OrderDirection.Ascending;
            }
            return OrderDirection.Default;
        }

        protected void SetDownloadHeaders(string filename)
        {
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(filename));
        }
    }
}
