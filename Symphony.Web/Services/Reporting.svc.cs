﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using Symphony.Core;

namespace Symphony.Web.Services
{
    // NOTE: If you change the class name "Reporting" here, you must also update the reference to "Reporting" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class ReportingService : BaseService, IReportingService
    {
        ILog Log = LogManager.GetLogger(typeof(ReportingService));

        public PagedResult<ReportJobRole> GetJobRoles(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportJobRole(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportJobRole>(ex);
            }
        }

        public PagedResult<ReportLocation> GetLocations(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportLocation(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportLocation>(ex);
            }
        }

        public PagedResult<ReportTrainingProgram> GetTrainingPrograms(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetTrainingProgram(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportTrainingProgram>(ex);
            }
        }

        public PagedResult<ReportAudience> GetAudiences(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportAudience(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportAudience>(ex);
            }
        }

        public PagedResult<ReportUser> GetUsers(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportUser(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportUser>(ex);
            }
        }

        public PagedResult<ReportUser> GetSupervisors(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportSupervisors(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportUser>(ex);
            }
        }

        public PagedResult<ReportUser> GetSymphonyUsers(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportSymphonyUser(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportUser>(ex);
            }
        }

        public PagedResult<ReportTypes> GetReportTypes(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportTypes(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportTypes>(ex);
            }
        }

        public PagedResult<ReportTemplate> GetReportTemplates(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportTemplates(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportTemplate>(ex);
            }
        }

        public SingleResult<ReportTemplate> GetReportTemplate(string reportTemplateId)
        {
            try
            {
                int rId;
                if (!int.TryParse(reportTemplateId, out rId)) {
                    throw new Exception("Invalid report template id.");
                }

                return (new ReportController()).GetReportTemplate(CustomerID, UserID, rId);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ReportTemplate>(ex);
            }
        }


        public SingleResult<ReportTemplate> SaveReportTemplate(string reportTemplateId, ReportTemplate reportTemplate)
        {
            try
            {
                int rId;

                if (!int.TryParse(reportTemplateId, out rId))
                {
                    throw new Exception("Invalid report template id.");
                }

                return (new ReportController()).SaveReportTemplate(CustomerID, UserID, rId, reportTemplate);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ReportTemplate>(ex);
            }
        }

        public MultipleResult<Customer> GetRepCustForSalesChannels(string reportTemplateId)
        {
            try
            {
                int rId = -1;

                if (!int.TryParse(reportTemplateId, out rId))
                {
                    throw new Exception("Invalid report template id.");
                }

                return (new ReportController()).GetReportPermCustomersForSalesChannels(rId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<Customer>(ex);
            }
        }

        public PagedResult<ReportEntity> GetReportEntities(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportEntities(CustomerID, UserID, 0, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportEntity>(ex);
            }
        }

        public PagedResult<ReportEntity> GetReportEntitiesForTemplate(string templateId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int tid = 0;
                if (!int.TryParse(templateId, out tid)) {
                    throw new Exception("Invalid template id");
                }
                return (new ReportController()).GetReportEntities(CustomerID, UserID, tid, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportEntity>(ex);
            }
        }

        public MultipleResult<ReportProcedure> GetReportProcedures(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportProcedures(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<ReportProcedure>(ex);
            }
        }

        public SingleResult<Dictionary<string, List<int>>> GetHierarchies(string userId)
        {
            try
            {
                int uid = 0;
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id.");
                }
                return new SingleResult<Dictionary<string, List<int>>>((new ReportController()).GetAvailableHierarchies(uid));

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Dictionary<string, List<int>>>(ex);
            }
        }

        public PagedResult<ReportCourse> GetCourses(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportCourse(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportCourse>(ex);
            }
        }

        public SingleResult<ReportJobs> SaveJobDetail(string jobId, ReportJobDetail job)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(jobId, out i))
                {
                    throw new Exception("Invalid job specified.");
                }
                return (new ReportController()).SaveJobDetail(CustomerID, UserID, int.Parse(jobId), job);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ReportJobs>(ex);
            }
        }


        public SingleResult<Report> SaveReport(string reportId, bool run, Report report)
        {
            try
            {
                int rId = 0;
                if (!int.TryParse(reportId, out rId))
                {
                    throw new Exception("Invalid report id specified.");
                }
                return (new ReportController()).SaveReport(CustomerID, UserID, rId, run, report);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Report>(ex);
            }
        }


        public SingleResult<Report> GetReport(string reportId)
        {
            try
            {
                int rId = 0;
                if (!int.TryParse(reportId, out rId))
                {
                    throw new Exception("Invalid report id specified.");
                }
                return (new ReportController()).GetReport(CustomerID, UserID, rId);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Report>(ex);
            }
        }

        public PagedResult<Report> GetReports(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).FindReports(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Report>(ex);
            }
        }

        public PagedResult<ReportQueues> GetReportQueueEntries(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).FindReportQueueEntries(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportQueues>(ex);
            }
        }

        public SingleResult<ReportJobDetail> GetJobDetail(string jobId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(jobId, out i))
                {
                    throw new Exception("Invalid job specified.");
                }
                return (new ReportController()).GetJobDetail(CustomerID, UserID, int.Parse(jobId));

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ReportJobDetail>(ex);
            }
        }



        public SingleResult<CalendarClass> SaveCalendarClass(CalendarClass klass)
        {
            try
            {
                return (new ClassroomController()).SaveCalendarClass(CustomerID, UserID, klass);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<CalendarClass>(ex);
            }
        }


        public SingleResult<ReportJobs> GetQueue(string queueId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(queueId, out i))
                {
                    throw new Exception("Invalid venueId specified.");
                }
                return (new ReportController()).GetQueue(CustomerID, UserID, int.Parse(queueId));

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ReportJobs>(ex);
            }
        }

        public string SaveQueueTest(string jobId, ReportJobDetail job)
        {
            string test = "";
            try
            {
                //int i = 0;
                //ReportJobDetail jr = new ReportJobDetail();
                //jr.Job.Name = "Test";
                //test = jr.Job.Name;
                //if (!int.TryParse(jobId, out i))
                //{
                //    throw new Exception("Invalid queue specified.");
                //}
                return test;
                

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return test;
            }
        }

        public PagedResult<ReportJobs> GetQueueListing(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetQueueListing(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportJobs>(ex);
            }
        }


        public PagedResult<ReportJobs> GetReportJobListing(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new ReportController()).GetReportJobListing(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ReportJobs>(ex);
            }
        }

        public SingleResult<List<ReportScormRegistration>> GetScormRegistration(string userId)
        {
            try
            {
                return (new ReportController()).GetScormRegistrationForUser(int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<List<ReportScormRegistration>>(ex);
            }
        }

        public Stream DownloadXLS(string queueID)
        {
            try
            {
                int id = 0;
                if (int.TryParse(queueID, out id))
                {
                    return (new ReportController()).DownloadXLS(CustomerID.ToString(), id.ToString());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public Stream DownloadCSV(string queueID)
        {
            try
            {
                int id = 0;
                if (int.TryParse(queueID, out id))
                {
                    return (new ReportController()).DownloadCSV(CustomerID.ToString(), id.ToString());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public Stream DownloadPDF(string queueID)
        {
            try
            {
                int id = 0;
                if (int.TryParse(queueID, out id))
                {
                    return (new ReportController()).DownloadPDF(CustomerID.ToString(), id.ToString());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        public string GetReportDataSet(string queueID)
        {
            try
            {
                int id = 0;
                if (int.TryParse(queueID, out id))
                {
                    return (new ReportController()).GetReportDataInJSON(CustomerID, int.Parse(queueID));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return null;
        }

        //GetReportQueueStatus
        public SingleResult<ReportQueues> GetQueueEntry(string queueId)
        {
            try
            {
                int i = 0;
                if (!int.TryParse(queueId, out i))
                {
                    throw new Exception("Invalid queueId specified.");
                }
                return (new ReportController()).GetReportQueueStatus(CustomerID, UserID, int.Parse(queueId));

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<ReportQueues>(ex);
            }
        }

    } // end of Servuce
}
