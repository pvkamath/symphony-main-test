using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.Web;
using System.IO;
using Symphony.Core;

namespace Symphony.Web.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class NetworkService : BaseService, INetworkService
    {
        ILog Log = LogManager.GetLogger(typeof(NetworkService));

        public PagedResult<Network> GetAllNetworks(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new NetworkController()).FindAllNetworks(UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Network>(ex);
            }
        }

        public SingleResult<Network> GetNetwork(string networkId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new NetworkController()).GetNetwork(int.Parse(networkId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Network>(ex);
            }
        }

        public SingleResult<Network> SaveNetwork(string networkId, Network network)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                if (new SymphonyController().SalesChannelID == 0)
                {
                    throw new Exception("You don't have permission to save network.");
                }
                return (new NetworkController()).SaveNetwork(int.Parse(networkId), network);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Network>(ex);
            }
        }
    }
}
