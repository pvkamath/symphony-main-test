﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using log4net;
using Symphony.Core.Controllers;
using Symphony.Core.Models;
using System.Collections.Generic;
using System.IO;

namespace Symphony.Web.Services
{
    /// <summary>
    /// Certificate service is a WCF service that defines an API for performing CRUD operations
    /// on certificate templates and for generating course certificates for users upon completion
    /// of their chosen training program or course.
    /// </summary>
#if DEBUG
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
#endif
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CertificateService : BaseService, ICertificateService
    {
        private CertificateController Controller;
        private ILog Log;

        public CertificateService()
        {
            Controller = new CertificateController();
            Log = LogManager.GetLogger(typeof(CertificateService));
        }

        /// <summary>
        /// Gets or sets the HttpStatusCode to return with the response.
        /// </summary>
        public HttpStatusCode StatusCode
        {
            get
            {
                return WebOperationContext.Current.OutgoingResponse.StatusCode;
            }
            set
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = value;
            }
        }

        /// <summary>
        /// Handles requests to finds a certificate template that matches the provided id.
        /// </summary>
        /// <param name="id">
        /// Id of the certificate template to return.
        /// </param>
        /// <returns>
        /// The matching certificate template.
        /// </returns>
        public SingleResult<CertificateTemplate> FindCertificateTemplate(string id)
        {
            try
            {
                Log.InfoFormat("Handling a request to find a certificate template with id {0}.", id);

                int idValue = Convert.ToInt32(id);
                if (idValue <= 0)
                {
                    throw new ArgumentOutOfRangeException("id", "Id must be a positive integer.");
                }

                var result = Controller.FindCertificateTemplate(idValue);
                if (result.Data == null)
                {
                    result.Success = false;
                    result.Error = "Could not find the certificate template.";

                    StatusCode = HttpStatusCode.NotFound;
                    Log.Error("Could not find the certificate template.");
                }

                return result;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to find a certificate template.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<CertificateTemplate>(ex);
            }
        }

        /// <summary>
        /// Handles requests to find the set of certificate templates that match the query parameters.
        /// </summary>
        /// <param name="searchText">
        /// Optional filter text to apply to the query. Applies to the name and description of the
        /// certificate template.
        /// </param>
        /// <param name="start">
        /// The first row of results to be returned in the paginated query.
        /// </param>
        /// <param name="limit">
        /// Maximum number of results to return.
        /// </param>
        /// <param name="sort">
        /// A parameter to sort on, matching one of the certificate template's columns.
        /// </param>
        /// <param name="dir">
        /// The direction to sort on. One of 'ASC' or 'DESC'.
        /// </param>
        /// <returns>
        /// A result set wrapping the returned certificate templates.
        /// </returns>
        public MultipleResult<CertificateTemplate> QueryCertificateTemplates()//string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                Log.Info("Handling a request to query certificate templates.");
                Controller.GetAllCertificates();

                return Controller.QueryCertificateTemplates(); //new PagedQueryParams<CertificateTemplate>(Log, searchText, start, limit, sort, dir));
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to query certificate templates.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new MultipleResult<CertificateTemplate>(ex);
            }
        }

        /// <summary>
        /// Handles requests to create a new certificate template.
        /// </summary>
        /// <param name="model">
        /// Model to create the new certificate template from.
        /// </param>
        /// <returns>
        /// A result set wrapping the new certificate template.
        /// </returns>
        public SingleResult<CertificateTemplate> CreateCertificateTemplate(CertificateTemplate model)
        {
            try
            {
                Log.Info("Handling a request to create a new certificate template.");

                return Controller.CreateCertificateTemplate(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to create a new certificate template.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<CertificateTemplate>(ex);
            }
        }

        /// <summary>
        /// Handles requests to update a certificate template.
        /// </summary>
        /// <param name="id">
        /// Id of the certificate template to update.
        /// </param>
        /// <param name="model">
        /// Model to update the certificate template with.
        /// </param>
        /// <returns>
        /// A result set wrapping the updated certificate template.
        /// </returns>
        public SingleResult<CertificateTemplate> UpdateCertificateTemplate(string id, CertificateTemplate model)
        {
            try
            {
                Log.Info("Handling a request to update a certificate template.");

                int val = Convert.ToInt32(id);
                if (val != model.ID)
                {
                    throw new ArgumentException("Query string id does not match the model id.");
                }

                return Controller.UpdateCertificateTemplate(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to update a certificate template.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<CertificateTemplate>(ex);
            }
        }

        /// <summary>
        /// Handles requests to delete a certificate template.
        /// </summary>
        /// <param name="id">
        /// Id of the certificate template to delete.
        /// </param>
        /// <param name="model">
        /// Model to delete.
        /// </param>
        /// <returns>
        /// The deleted model, wrapped in a result set.
        /// </returns>
        public SingleResult<CertificateTemplate> DeleteCertificateTemplate(string id, CertificateTemplate model)
        {
            try
            {
                Log.Info("Handling a request to delete a certificate template.");

                int val = Convert.ToInt32(id);
                if (val != model.ID)
                {
                    throw new ArgumentException("Query string id does not match the model id.");
                }

                return Controller.DeleteCertificateTemplate(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to delete a certificate template.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<CertificateTemplate>(ex);
            }
        }

        public SingleResult<TreeNode> GetCertificateTemplateParameters()
        {
            try
            {
                Log.Info("Handling a request to get certificate template parameters.");

                var tree = NotificationTemplateController.GetTemplateParametersForCertificateTemplate();

                return new SingleResult<TreeNode>(tree);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling a request to get certificate template parameters.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<TreeNode>(ex);
            }
        }

        public SimpleSingleResult<string> GenerateCourseCertificate(string id)
        {
            try
            {
                Log.Info("Handling a request to generate a course certificate.");

                int val = Convert.ToInt32(id);

                return Controller.GenerateCourseCertificate(val, Controller.UserID);
            }
            catch (Exception ex)
            {
                return new SimpleSingleResult<string>(ex);
            }
        }

        public SimpleSingleResult<string> GenerateTrainingProgramCertificate(string id)
        {
            try
            {
                Log.Info("Handling a request to generate a training program certificate.");

                int val = Convert.ToInt32(id);

                return Controller.GenerateCourseCertificate(val, Controller.UserID);
            }
            catch (Exception ex)
            {
                return new SimpleSingleResult<string>(ex);
            }
        }

        public MultipleResult<CertificateAsset> GetAssetList()
        {
            var res = Controller.GetAssetList();
            return res;
        }

        public SingleResult<SimpleGeneratedCertificate> Preview(CertificateTemplate cp)
        {
            var generated = Controller.GeneratePreviewCertificate(cp);
            return new SingleResult<SimpleGeneratedCertificate>(new SimpleGeneratedCertificate(){ Body = generated });
        }

        public MultipleResult<CertificateTemplate> GetCertificates()
        {
            try
            {
                return (new CertificateController()).GetAllCertificates();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<CertificateTemplate>(ex);
            }
        }

        public SingleResult<SimpleGeneratedCertificate> Render(string certificateTemplateId, string userId, string trainingProgramId)
        {

            var generated = Controller.GenerateCertificate(int.Parse(certificateTemplateId), int.Parse(userId), int.Parse(trainingProgramId), new Certificate());

            return new SingleResult<SimpleGeneratedCertificate>(new SimpleGeneratedCertificate() { Body = generated });
        }

        public SingleResult<CertificateTemplate> ReparentTemplate(string templateId, string parentId)
        {
            try
            {
                return Controller.ReparentTemplate(int.Parse(templateId), int.Parse(parentId));
            }
            catch (Exception ex)
            {
                return new SingleResult<CertificateTemplate>(ex);
            }
        }
    }
}
