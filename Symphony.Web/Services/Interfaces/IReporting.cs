﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    // NOTE: If you change the interface name "IReporting" here, you must also update the reference to "IReporting" in Web.config.
    [ServiceContract]
    public interface IReportingService
    {

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/JobRoles/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportJobRole> GetJobRoles(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Locations/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportLocation> GetLocations(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/TrainingPrograms/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportTrainingProgram> GetTrainingPrograms(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Audiences/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportAudience> GetAudiences(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Users/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportUser> GetUsers(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Supervisors/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportUser> GetSupervisors(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SymphonyUsers/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportUser> GetSymphonyUsers(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/ReportTypes/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportTypes> GetReportTypes(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Courses/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportCourse> GetCourses(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/QueueList/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportJobs> GetQueueListing(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/JobList/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportJobs> GetReportJobListing(string searchText, int start, int limit, string sort, string dir);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Queue/{queueId}")]
        SingleResult<ReportJobs> GetQueue(string queueId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Job/{jobId}")]
        SingleResult<ReportJobs> SaveJobDetail(string jobId, ReportJobDetail jobs);  //SaveJob

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/JobDetail/{jobId}")]
        SingleResult<ReportJobDetail> GetJobDetail(string jobId);  //get

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/{reportId}?run={run}")]
        SingleResult<Report> SaveReport(string reportId, bool run, Report report);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/{reportId}")]
        SingleResult<Report> GetReport(string reportId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Report> GetReports(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/queueEntries/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportQueues> GetReportQueueEntries(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/queueEntries/{queueId}")]
        SingleResult<ReportQueues> GetQueueEntry(string queueId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetScormRegistration/{userId}")]
        SingleResult<List<ReportScormRegistration>> GetScormRegistration(string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/XLS/{queueId}")]
        Stream DownloadXLS(string queueId);

        [OperationContract]
        [WebGet(UriTemplate = "/reports/PDF/{queueId}")]
        Stream DownloadPDF(string queueId);

        [OperationContract]
        [WebGet(UriTemplate  = "/reports/CSV/{queueId}")]
        Stream DownloadCSV(string queueId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/dataset/{queueId}", BodyStyle = WebMessageBodyStyle.Bare)]
        string GetReportDataSet(string queueId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reporttemplates/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportTemplate> GetReportTemplates(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reporttemplate/{reportTemplateId}")]
        SingleResult<ReportTemplate> GetReportTemplate(string reportTemplateId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reporttemplatecustbysc/{reportTemplateId}")]
        MultipleResult<Customer> GetRepCustForSalesChannels(string reportTemplateId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reporttemplate/{reportTemplateId}")]
        SingleResult<ReportTemplate> SaveReportTemplate(string reportTemplateId, ReportTemplate reportTemplate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reportentities/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportEntity> GetReportEntities(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reportentities/{templateId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ReportEntity> GetReportEntitiesForTemplate(string templateId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reportprocedures/?" + Templates.GenericGETParametersWithSearch)]
        MultipleResult<ReportProcedure> GetReportProcedures(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/hierarchies/{userId}")]
        SingleResult<Dictionary<string, List<int>>> GetHierarchies(string userId);

    }
}
