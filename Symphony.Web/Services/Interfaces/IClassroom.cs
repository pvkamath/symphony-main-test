﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    // NOTE: If you change the interface name "IClassroom" here, you must also update the reference to "IClassroom" in Web.config.
    [ServiceContract]
    public interface IClassroomService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/coursefiles/download/{fileId}")]
        Stream GetCourseFile(string fileId);

        [OperationContract]
        [WebInvoke(Method="POST", ResponseFormat=WebMessageFormat.Json, UriTemplate= "/webinarregistration/{classId}")]
        MultipleResult<WebinarRegistrationAttempt> CreateRegistrations(string classId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/pendingregistrations/")]
        MultipleResult<PendingRegistration> GetPendingRegistrations();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/pendingregistrations/update/")]
        SingleResult<bool> SavePendingRegistrations(List<PendingRegistration> registrations);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Course> GetCourses(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/?" + Templates.GenericGETParametersWithSearch + "&filter={filter}")]
        PagedResult<ClassroomClass> GetClasses(string searchText, int start, int limit, string sort, string dir, string filter);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/instructedClasses/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ClassOrOnlineCourse> GetInstructedClasses(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/enrolledClasses/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ClassOrOnlineCourse> GetEnrolledClasses(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/enrolledClasses/{trainingprogramId}/{courseId}/{courseTypeId}")]
        SingleResult<ClassOrOnlineCourse> GetEnrolledClass(string trainingProgramId, string courseId, string courseTypeId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/venues/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Venue> GetVenues(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/rooms/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Room> GetRooms(string searchText, int start, int limit, string sort, string dir);       

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/presentationTypes/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<PresentationType> GetPresentationTypes(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courseCompletionTypes/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<CourseCompletionType> GetCourseCompletionTypes(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/states/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<State> GetStates(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/timeZones/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Core.Models.TimeZone> GetTimeZones(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/onlineCourses/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Course> GetOnlineCourses(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/onlineSurveys/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Course> GetOnlineSurveys(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/{classId}/classInstructors/?" + Templates.GenericGETParameters)]
        PagedResult<ClassInstructor> GetClassInstructors(string classId, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/{classId}/classResources/?" + Templates.GenericGETParameters)]
        PagedResult<ClassResource> GetClassResources(string classId, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/{classId}/classDates/?" + Templates.GenericGETParameters)]
        PagedResult<ClassDate> GetClassDates(string classId, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/{classId}/registrations/?" + Templates.GenericGETParameters)]
        PagedResult<Registration> GetRegistrations(string classId, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/venues/{venueId}/rooms/?" + Templates.GenericGETParameters)]
        PagedResult<Room> GetVenueRooms(string venueId, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/instructors/select/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetClassInstructorList(List<ClassInstructor> exclude, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/resources/select/{venueId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Resource> GetClassResourceList(List<ClassResource> exclude, string venueId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/registrants/select/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetRegistrantList(List<Registration> exclude, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/venues/resourcemanagers/select/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetVenueResourceManagerList(List<VenueResourceManager> exclude, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/calendar/{year}/{month}/{filter}/?")]
        SingleResult<List<CalendarWeek>> GetCalendarWeeks(string year, string month, string filter);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messages/send/")]
        SingleResult<bool> SendMessage(List<Message> messages);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/ctmanagers/")]
        MultipleResult<User> GetCTManagers();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/{courseId}")]
        SingleResult<Course> GetCourse(string courseId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/{classId}")]
        SingleResult<ClassroomClass> GetClass(string classId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/venues/{venueId}")]
        SingleResult<Venue> GetVenue(string venueId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/resources/{resourceId}")]
        SingleResult<Resource> GetResource(string resourceId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/rooms/{roomId}")]
        SingleResult<Room> GetRoom(string roomId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/calendar/?")]
        SingleResult<CalendarClass> SaveCalendarClass(CalendarClass klass);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/{courseId}")]
        SingleResult<Course> SaveCourse(string courseId, Course course);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/{classId}")]
        SingleResult<ClassroomClass> SaveClass(string classId, ClassroomClass klass);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/{classId}/registrations")]
        SingleResult<ClassroomClass> UpdateRegistrations(string classId, ClassroomClass klass);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/venues/{venueId}")]
        SingleResult<Venue> SaveVenue(string venueId, Venue venue);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classInstructors/{classInstructorId}")]
        SingleResult<ClassInstructor> SaveClassInstructor(string classInstructorId, ClassInstructor classInstructor);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classResources/{classResourceId}")]
        SingleResult<ClassResource> SaveClassResource(string classResourceId, ClassResource classResource);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/venueResourceManagers/{venueResourceManagerId}")]
        SingleResult<VenueResourceManager> SaveVenueResourceManager(string venueResourceManagerId, VenueResourceManager venueResourceManager);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/resources/{resourceId}")]
        SingleResult<Resource> SaveResource(string resourceId, Resource resource);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/rooms/{roomId}")]
        SingleResult<Room> SaveRoom(string roomId, Room room);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/registrations/{registrationId}")]
        SingleResult<Registration> SaveRegistration(string registrationId, Registration registration);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/{classId}/reschedule/?")]
        SingleResult<List<ClassDate>> RescheduleClass(string classId, ClassDate classDate);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/delete/{courseId}")]
        SingleResult<bool> DeleteCourse(string courseId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/delete/{classId}")]
        SingleResult<bool> DeleteClass(string classId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/venues/delete/{venueId}")]
        SingleResult<bool> DeleteVenue(string venueId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classInstructors/delete/{classInstructorId}")]
        SingleResult<bool> DeleteClassInstructor(string classInstructorId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classResources/delete/{classResourceId}")]
        SingleResult<bool> DeleteClassResource(string classResourceId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/venueResourceManagers/delete/{venueResourceManagerId}")]
        SingleResult<bool> DeleteVenueResourceManager(string venueResourceManagerId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/resources/delete/{resourceId}")]
        SingleResult<bool> DeleteResource(string resourceId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/rooms/delete/{roomId}")]
        SingleResult<bool> DeleteRoom(string roomId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/registrations/delete/{registrationId}")]
        SingleResult<bool> DeleteRegistration(string registrationId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/coursefiles/{courseId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<CourseFile> GetCourseFiles(string courseId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/coursefiles/delete/{courseFileId}")]
        SingleResult<bool> DeleteCourseFile(string courseFileId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/getregistrants/{hierarchyId}")]
        MultipleResult<User> GetUsersByHierarchy(string hierarchyId, List<string> nodeIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/quickadd/")]
        SingleResult<ClassroomClass> SaveThirdPartyClass(ThirdPartyClass klass);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/categories/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Category> GetCategories(string searchText, int start, int limit, string sort, string dir);

        // annoyingly, WCF can't tell the diff between GET and POST; it attempts to map this to the GET method if the query parameters aren't specified
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/categories/save/")]
        MultipleResult<Category> SaveCategories(List<Category> categories);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/registrations/session/auto")]
        MultipleResult<SessionAssignedUser> SaveSessionRegistrationsAuto(List<SessionAssignedUser> users);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/registrations/session/{sessionId}")]
        MultipleResult<SessionAssignedUser> SaveSessionRegistrations(string sessionId, List<SessionAssignedUser> users);
    }
}
