using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    [ServiceContract]
    public interface IBookService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/books/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Book> GetAllBooks(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tpbooks/?tpid={tpid}&" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Book> FindTrainingProgramBooks(string tpid, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/book/{bookId}")]
        SingleResult<Book> SaveBook(string bookId, Book book);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/book/{bookId}")]
        SingleResult<Book> GetBook(string bookId);
    }
}