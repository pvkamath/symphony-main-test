﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    [ServiceContract]
    public interface ITrainingProgram
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/{trainingProgramId}/feedback")]
        SingleResult<TrainingProgramFeedback> SetTrainingProgramFeedback(string trainingProgramId, TrainingProgramFeedback feedback);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/{trainingProgramId}/feedback/user")] // "<url>/?uid=<uid>" to get non-current user? 
        MultipleResult<TrainingProgramFeedback> GetTrainingProgramFeedbackFromUser(string trainingProgramId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/{trainingProgramId}/feedback")]
        MultipleResult<TrainingProgramFeedback> GetTrainingProgramFeedback(string trainingProgramId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/{trainingProgramId}/feedback/summary")]
        SingleResult<TrainingProgramFeedbackSummary> GetTrainingProgramFeedbackSummary(string trainingProgramId);
    }
}