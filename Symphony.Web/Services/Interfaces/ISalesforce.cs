﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models.Salesforce;
using System.IO;

namespace Symphony.Web.Services
{
    [ServiceContract]
    public interface ISalesforceService
    {
        #region Users
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/user/{id}")]
        User GetUser(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/users/?search={search}&orderBy={orderBy}&orderDir={orderDir}&pageSize={pageSize}&pageIndex={pageIndex}")]
        List<User> FindUsers(string search, string orderBy, string orderDir, int pageIndex, int pageSize);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/user/")]
        User PostUser(User user);

        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/user/{id}")]
        User PutUser(string id, User user);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/user/{id}")]
        User DeleteUser(string id);
        #endregion

        #region Companies
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/company/{id}")]
        Company GetCompany(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/company/")]
        Company PostCompany(Company company);

        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/company/{id}")]
        Company PutCompany(string id, Company company);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/company/{id}")]
        Company DeleteCompany(string id);     
        #endregion

        #region Products
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/product?since={since}")]
        ProductList GetProducts(string since);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/product/{id}")]
        Product GetProduct(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/product/session?since={since}")]
        ProductList GetProductSession(string since);
        #endregion

        #region Entitlements
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/user/{userId}/entitlement")]
        EntitlementList GetEntitlements(string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/user/{userId}/entitlement/{entitlementId}")]
        EntitlementList GetEntitlement(string userId, string entitlementId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/user/{userId}/entitlement")]
        EntitlementList PostEntitlements(string userId, EntitlementList entitlement);

        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/user/{userId}/entitlement/{entitlementId}")]
        EntitlementList PutEntitlements(string userId, string entitlementId, EntitlementList entitlement);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/user/{userId}/entitlement/{entitlementId}")]
        EntitlementList DeleteEntitlements(string userId, string entitlementId, EntitlementList entitlement);
        #endregion

        #region TrainingPrograms
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/product/details/{trainingProgramSku}")]
        TrainingProgram GetTrainingProgram(string trainingProgramSku);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/registration/{trainingProgramSku}")]
        Registration PostRegistration(string trainingProgramSku, Registration registration);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/api/3/registration/{trainingProgramSku}/{classId}")]
        Registrations GetRegistrations(string trainingProgramSku, string classId);
        #endregion

    }
}
