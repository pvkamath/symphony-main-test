﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    // NOTE: If you change the interface name "IAssignment" here, you must also update the reference to "IAssignment" in Web.config.
    [ServiceContract]
    public interface IAssignmentService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assignments/users/{trainingProgramId}/{courseId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<UserAssignment> GetUsersWithAssignments(string trainingProgramId, string courseId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assignments/{trainingProgramId}/{courseId}/{userId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Assignment> GetAssignments(string trainingProgramId, string courseId, string userId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assignment/responses/{trainingProgramId}/{courseId}/{sectionId}/{attemptId}/{userId}")]
        MultipleResult<AssignmentResponse> GetAssignmentResponses(string trainingProgramId, string courseId, string sectionId, string attemptId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assignment/responses/{trainingProgramId}/{courseId}/{sectionId}/{attemptId}/{userId}")]
        MultipleResult<AssignmentResponse> PostAssignmentResponses(string trainingProgramId, string courseId, string sectionId, string attemptId, string userId, List<AssignmentResponse> responses);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assignment/correct/{trainingProgramId}/{courseId}/{userId}/")]
        SimpleSingleResult<bool> PostAssignmentResponsesCorrect(string trainingProgramId, string courseId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assignment/incorrect/{trainingProgramId}/{courseId}/{userId}/")]
        SimpleSingleResult<bool> PostAssignmentResponsesIncorrect(string trainingProgramId, string courseId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assignment/unmarked/{trainingProgramId}/{courseId}/{userId}/")]
        SimpleSingleResult<bool> PostAssignmentResponsesUnmarked(string trainingProgramId, string courseId, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assignment/status/{trainingProgramId}/{courseId}/{userId}/")]
        SimpleSingleResult<AssignmentStatus> GetAssignmentStatus(string trainingProgramId, string courseId, string userId);

    }
}