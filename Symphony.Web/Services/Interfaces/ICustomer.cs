﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using Symphony.Core;
using System.IO;

namespace Symphony.Web.Services
{
    // NOTE: If you change the interface name "IClassroom" here, you must also update the reference to "ICourseAssignment" in Web.config.
    [ServiceContract]
    public interface ICustomerService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/export/customer/{customerId}/{exportTypeId}/")]
        Stream Export(string customerId, string exportTypeId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/{userId}/customer/{customerId}/delete/")]
        SingleResult<bool> DeleteUser(string customerId, string userId);
        
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/{userId}/customer/{customerId}/reset/")]
        SingleResult<bool> ResetUser(string customerId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/locations/{locationId}/map/")]
        SingleResult<bool> SaveLocationUsers(string locationId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jobroles/{jobRoleId}/map/")]
        SingleResult<bool> SaveJobRoleUsers(string jobRoleId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/audiences/{audienceId}/map/")]
        SingleResult<bool> SaveAudienceUsers(string audienceId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/locations/{locationId}/map/delete/")]
        SingleResult<bool> DeleteLocationUsers(string locationId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jobroles/{jobRoleId}/map/delete/")]
        SingleResult<bool> DeleteJobRoleUsers(string jobRoleId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/audiences/{audienceId}/map/delete/")]
        SingleResult<bool> DeleteAudienceUsers(string audienceId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/importhistory/customer/{customerId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ImportHistory> GetImportHistory(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/customer/{customerId}?" + Templates.GenericGETParametersWithSearch + "&filter={filter}")]
        PagedResult<User> GetUsers(string customerId, string searchText, int start, int limit, string sort, string dir, string filter);

        [OperationContract(Name="GetUsersAdmin")]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/?" + Templates.GenericGETParametersWithSearch + "&filter={filter}")]
        PagedResult<User> GetUsers(string searchText, int start, int limit, string sort, string dir, string filter);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/authors/?" + Templates.GenericGETParametersWithSearch + "&filter={filter}")]
        MultipleResult<Author> GetAuthors(string searchText, int start, int limit, string sort, string dir, string filter);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/themes/?" + Templates.GenericGETParametersWithSearch + "&filter={filter}")]
        PagedResult<Theme> FindThemes(string searchText, int start, int limit, string sort, string dir, string filter);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/themes/{themeId}")]
        SingleResult<Theme> GetTheme(string themeId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/themes/")]
        SingleResult<Theme> CreateTheme(Theme theme);

        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/themes/{themeId}")]
        SingleResult<Theme> UpdateTheme(string themeId, Theme theme);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/themes/{themeId}")]
        SingleResult<Theme> DeleteTheme(string themeId, Theme theme);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/importresults/customer/{customerId}/{importId}/")]
        Stream GetImportResults(string customerId, string importId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/customer/{customerId}/filtered?" + Templates.GenericGETParametersWithSearch + "&filter={filter}")]
        PagedResult<User> GetUsersFiltered(List<int> exclude, string customerId, string searchText, int start, int limit, string sort, string dir, string filter);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/customer/{customerId}/unassigned/{hierarchyName}/{hierarchyId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetUsersUnassigned(string hierarchyName, string hierarchyId, string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/customer/{customerId}/location/{locationId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetUsersForLocation(string customerId, string locationId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/customer/{customerId}/jobrole/{jobRoleId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetUsersForJobRole(string customerId, string jobRoleId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/customer/{customerId}/audience/{audienceId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetUsersForAudience(string customerId, string audienceId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jobroles/customer/{customerId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<JobRole> GetJobRoles(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/locations/customer/{customerId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Location> GetLocations(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/audiences/customer/{customerId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Audience> GetAudiences(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/supervisors/customer/{customerId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetSupervisors(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reportingsupervisors/customer/{customerId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetReportingSupervisors(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/customer/{customerId}/roles/?roles={roles}&" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetUsersByRoles(string customerId, string roles, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customers/saleschannel/{salesChannelId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Customer> GetCustomers(string salesChannelId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customers/?" + Templates.GenericGETParametersFilterable)]
        PagedResult<Customer> GetAllCustomers(string searchText, int start, int limit, string sort, string dir, string filter);

        [OperationContract(Name="GetSelectedCustomers")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customers/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Customer> GetAllCustomers(string searchText, int start, int limit, string sort, string dir, int[] selectedIds);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customers/{customerId}/networked/reporting/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Customer> GetNetworkedReportingCustomers(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customers/{customerId}/networked/trainingprogram/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Customer> GetNetworkedTrainingProgramCustomers(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customers/{customerId}/networked/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Customer> GetNetworkedCustomers(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/administrators/saleschannel/{salesChannelId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetAdministrators(string salesChannelId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customercarereps/customer/{customerId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetCustomerCareReps(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accountexecutives/customer/{customerId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetAccountExecutives(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/userstatuses/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<UserStatus> GetUserStatuses(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/saleschannels/")]
        MultipleResult<SalesChannel> GetSalesChannels();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/saleschannel/{salesChannelId}")]
        SingleResult<SalesChannel> GetSalesChannel(string salesChannelId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/saleschannels/{salesChannelId}")]
        SingleResult<SalesChannel> SaveSalesChannel(string salesChannelId, SalesChannel salesChannel);

        

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customers/{customerId}")]
        SingleResult<Customer> GetCustomer(string customerId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customers/{customerId}/settings")]
        SingleResult<CustomerSettings> GetCustomerSettings(string customerId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customers/{customerId}")]
        SingleResult<Customer> SaveCustomer(string customerId, Customer customer);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/customers/{customerId}/settings")]
        SingleResult<CustomerSettings> SaveCustomerSettings(string customerId, CustomerSettings customer);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/administrators/{userId}")]
        SingleResult<User> GetAdministrator(string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/administrators/{userId}")]
        SingleResult<User> SaveAdministrator(string userId, User customer);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/{userId}/customer/{customerId}")]
        SingleResult<User> GetUser(string customerId, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/currentuser/")]
        SingleResult<User> GetCurrentUser();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/{userId}/customer/{customerId}")]
        SingleResult<User> SaveUser(string customerId, string userId, UserSecurity user);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/locations/{locationId}/customer/{customerId}")]
        SingleResult<Location> GetLocation(string customerId, string locationId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jobroles/{jobRoleId}/customer/{customerId}")]
        SingleResult<JobRole> GetJobRole(string customerId, string jobRoleId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/audiences/{audienceId}/customer/{customerId}")]
        SingleResult<Audience> GetAudience(string customerId, string audienceId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/locations/{locationId}/customer/{customerId}")]
        SingleResult<Location> SaveLocation(string customerId, string locationId, Location location);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jobroles/{jobRoleId}/customer/{customerId}")]
        SingleResult<JobRole> SaveJobRole(string customerId, string jobRoleId, JobRole jobRole);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/audiences/{audienceId}/customer/{customerId}")]
        SingleResult<Audience> SaveAudience(string customerId, string audienceId, Audience audience);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/locations/delete/{locationId}")]
        SingleResult<bool> DeleteLocation(string locationId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jobroles/delete/{jobRoleId}")]
        SingleResult<bool> DeleteJobRole(string jobRoleId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/audiences/delete/{audienceId}")]
        SingleResult<bool> DeleteAudience(string audienceId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/{userId}/toggleaccountexec/")]
        SingleResult<User> ToggleAccountExec(string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/{userId}/togglecustomercare/")]
        SingleResult<User> ToggleCustomerCare(string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/gtmorganizers?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<GTMOrganizer> GetGTMOrganizers(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/gtmorganizers/{gtmOrganizerId}")]
        SingleResult<GTMOrganizer> GetGTMOrganizer(string gtmOrganizerId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/gtmorganizers/delete/{gtmOrganizerId}")]
        SingleResult<bool> DeleteGTMOrganizer(string gtmOrganizerId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/gtmorganizers/{gtmOrganizerId}")]
        SingleResult<GTMOrganizer> SaveGTMOrganizer(string gtmOrganizerId, GTMOrganizer gtmOrganizer);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/login/{customer}/{username}")]
        SingleResult<AuthenticationResult> Login(string customer, string username, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/currentuseraccountexec")]
        SingleResult<User> GetAccountExecutive();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/currentusercustomercare")]
        SingleResult<User> GetCustomerCareRep();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/validate/{userid}/{courseId}/{trainingProgramId}/{attempt}")]
        SingleResult<bool> ValidateUser(string userid, string courseId, string trainingProgramId, string attempt, UserSecurity userSecurity);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/posttpvalidate/{userid}/{courseId}/{trainingProgramId}/{attempt}")]
        SingleResult<bool> PostTPValidateUser(string userid, string courseId, string trainingProgramId, string attempt, UserSecurity userSecurity);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/usersreportprm/{userid}/customer/{customerId}/prmtype/{permissionTypeId}")]
        MultipleResult<UserReportingPermission> GetUserReportingPermissions(string customerId, string userId, string permissionTypeId);  

        #region User Meta Data
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/user/datafields/{userId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<UserDataField> GetUserDataFields(string userId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/user/datafields/{userDataFieldId}")]
        SingleResult<UserDataField> SaveUserDataField(string userDataFieldId, UserDataField userDataField);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/user/datafields/{userDataFieldId}")]
        SimpleSingleResult<bool> DeleteUserDataField(string userDataFieldId);
        #endregion


        #region Applications

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/applications/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Application> GetApplications(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/applications/{applicationId}/")]
        SingleResult<Application> GetApplication(string applicationId);

        [OperationContract]         
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/applications/{applicationId}/")]
        SingleResult<Application> SaveApplication(string applicationId, Application application);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/applications/{applicationId}/delete")]
        SingleResult<bool> DeleteApplication(string applicationId);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Tile> GetTiles(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/{TileId}/")]
        SingleResult<Tile> GetTile(string TileId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/{TileId}/")]
        SingleResult<Tile> SaveTile(string TileId, Tile Tile);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/{TileId}/delete")]
        SingleResult<bool> DeleteTile(string TileId);
        

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/customer/{customerId}/location/{locationId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Tile> GetTilesForLocation(string customerId, string locationId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/customer/{customerId}/jobrole/{jobRoleId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Tile> GetTilesForJobRole(string customerId, string jobRoleId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/customer/{customerId}/audience/{audienceId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Tile> GetTilesForAudience(string customerId, string audienceId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/customer/{customerId}/unassigned/{hierarchyName}/{hierarchyId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Tile> GetTilesUnassigned(string hierarchyName, string hierarchyId, string customerId, string searchText, int start, int limit, string sort, string dir);


        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/locations/{locationId}/map/")]
        SingleResult<bool> SaveLocationTiles(string locationId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/jobroles/{jobRoleId}/map/")]
        SingleResult<bool> SaveJobRoleTiles(string jobRoleId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/audiences/{audienceId}/map/")]
        SingleResult<bool> SaveAudienceTiles(string audienceId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/locations/{locationId}/map/delete/")]
        SingleResult<bool> DeleteLocationTiles(string locationId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/jobroles/{jobRoleId}/map/delete/")]
        SingleResult<bool> DeleteJobRoleTiles(string jobRoleId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/audiences/{audienceId}/map/delete/")]
        SingleResult<bool> DeleteAudienceTiles(string audienceId, int[] userIds);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/applications/{applicationId}/users/{userId}/")]
        MultipleResult<UserTileAssignment> GetUserAssignedTilesForApp(string applicationId, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/unassigned/users/{userId}/")]
        MultipleResult<UserTileAssignment> GetUserUnassignedTiels(string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/applications/users/{userId}/")]
        MultipleResult<UserApplicationAssignment> GetAssignedApplications(string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/users/{userId}/map/")]
        SingleResult<bool> SaveUserTiles(string userId, int[] tileIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/tiles/users/{userId}/map/delete/")]
        SingleResult<bool> DeleteUserTiles(string userId, int[] tileIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/applications/{applicationId}/users/{userId}/credentials/{credentialId}")]
        SingleResult<Credentials> saveCredentials(string applicationId, string userId, string credentialId, Credentials credentials);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/{userId}/appLinks/")]
        MultipleResult<AppLink> UserAppLinks(string userId);
        [OperationContract]

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/{userId}/groups/")]
        MultipleResult<AppGroup> UserAppGroups(string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/applications/appLinks/")]
        MultipleResult<AppLink> AllAppLinks();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/applications/appLinks/{tileId}")]
        SingleResult<bool> ResolveAppLink(string tileId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/user/{userId}/onlinestatus/")]
        SingleResult<OnlineStatus> GetOnlineStatus(string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/user/{userId}/onlinestatus/")]
        SingleResult<OnlineStatus> UpdateOnlineStatus(string userId, OnlineStatus status);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/externalsystems/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ExternalSystem> GetExternalSystems(string searchText, int start, int limit, string sort, string dir);

        #endregion


    }
}