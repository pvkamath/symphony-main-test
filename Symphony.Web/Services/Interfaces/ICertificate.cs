﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;

namespace Symphony.Web.Services
{
    [ServiceContract]
    public interface ICertificateService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificateTemplates/{id}")]
        SingleResult<CertificateTemplate> FindCertificateTemplate(string id); 
         
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificateTemplates/?")]// + Templates.GenericGETParametersWithSearch)]
        MultipleResult<CertificateTemplate> QueryCertificateTemplates();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificates/")] 
        MultipleResult<CertificateTemplate> GetCertificates();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificateTemplates/")]
        SingleResult<CertificateTemplate> CreateCertificateTemplate(CertificateTemplate model);

        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificateTemplates/{id}")]
        SingleResult<CertificateTemplate> UpdateCertificateTemplate(string id, CertificateTemplate model);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificateTemplates/{id}")]
        SingleResult<CertificateTemplate> DeleteCertificateTemplate(string id, CertificateTemplate model);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificates/parameters/")]
        SingleResult<TreeNode> GetCertificateTemplateParameters();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificates/course/{id}")]
        SimpleSingleResult<string> GenerateCourseCertificate(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificates/trainingProgram/{id}")]
        SimpleSingleResult<string> GenerateTrainingProgramCertificate(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificates/assets")]
        MultipleResult<CertificateAsset> GetAssetList();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/certificates/preview")]
        SingleResult<SimpleGeneratedCertificate> Preview(CertificateTemplate cp);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/template/{id}/reparent/{parentId}")]
        SingleResult<CertificateTemplate> ReparentTemplate(string id, string parentId); 
    }
}
