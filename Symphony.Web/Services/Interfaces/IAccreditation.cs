﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;

namespace Symphony.Web.Services
{
    [ServiceContract]
    public interface IAccreditationService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditationBoards/{id}")] 
        SingleResult<AccreditationBoard> FindAccreditationBoard(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditationBoards/{id}/artisanAccreditations/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ArtisanCourseAccreditation> QueryArtisanAccreditationsForAccreditationBoard(string id, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditationBoards/{id}/trainingProgramAccreditations/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<TrainingProgramAccreditation> QueryTrainingProgramAccreditationsForAccreditationBoard(string id, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditationBoards/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<AccreditationBoard> QueryAccreditationBoards(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditationBoards/")]
        SingleResult<AccreditationBoard> CreateAccreditationBoardWithoutId(AccreditationBoard model);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditationBoards/{id}")]
        SingleResult<AccreditationBoard> CreateAccreditationBoardWithId(string id, AccreditationBoard model);

        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditationBoards/{id}")]
        SingleResult<AccreditationBoard> UpdateAccreditationBoard(string id, AccreditationBoard model);
         
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditationBoards/{id}/surveys/?" + Templates.GenericGETParametersWithSearch + "&exclude={exclude}")]
        PagedResult<AccreditationBoardSurvey> QueryAvailableAccreditationBoardSurveys(string id, string searchText, int start, int limit, string sort, string dir, string exclude);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditations/artisan/{artisanCourseId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ArtisanCourseAccreditation> QueryArtisanAccreditations(string artisanCourseId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditations/courseassignment/{courseId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<OnlineCourseAccreditation> QueryOnlineAccreditations(string courseId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/accreditations/{id}")]
        SingleResult<OnlineCourseAccreditation> DeleteAccreditationBoard(string id);
    }
}

