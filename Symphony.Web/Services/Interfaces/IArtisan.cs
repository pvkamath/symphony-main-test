﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    // NOTE: If you change the interface name "IArtisan" here, you must also update the reference to "IArtisan" in Web.config.
    [ServiceContract]
    public interface IArtisanService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/?" + Templates.GenericGETParametersWithSearch + "&filter={filter}")]
        PagedResult<ArtisanCourse> GetCourses(string searchText, int start, int limit, string sort, string dir, string filter);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publishedCourses/?" + Templates.GenericGETParametersWithSearch + "&hidePrevious={hidePrevious}")]
        PagedResult<ArtisanCourse> GetPublishedCourses(string searchText, int start, int limit, string sort, string dir, bool hidePrevious);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/themes/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ArtisanTheme> GetThemes(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/{courseId}")]
        SingleResult<ArtisanCourse> GetCourse(string courseId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/{courseId}")]
        SingleResult<ArtisanCourse> SaveCourse(string courseId, ArtisanCourse course);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/delete/{courseId}")]
        SingleResult<bool> DeleteCourse(string courseId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/templates/?pageType={pageType}&" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ArtisanTemplate> GetTemplates(int pageType, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/templates/{templateId}")]
        SingleResult<ArtisanTemplate> GetTemplate(string templateId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assets/?assetTypeId={assetTypeId}&" + Templates.GenericGETParametersWithSearch + "&filter={filter}")]
        PagedResult<ArtisanAsset> GetAssets(int assetTypeId, string searchText, int start, int limit, string sort, string dir, string filter);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assets/{assetId}")]
        SingleResult<ArtisanAsset> GetAsset(string assetId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assetTypes/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ArtisanAssetType> GetAssetTypes(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assetTypes/{assetTypeId}")]
        SingleResult<ArtisanAssetType> GetAssetType(string assetTypeId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/package/{courseId}?preview={preview}&deploy={deploy}&update={update}&target={target}")]
        SimpleSingleResult<string> PackageCourse(string courseId, string preview, string deploy, string update, string target, ArtisanCourse course);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/publish/{courseId}")]
        SingleResult<ArtisanCourse> PublishCourse(string courseId, ArtisanCourse course);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/duplicate/{courseId}")]
        SingleResult<ArtisanCourse> DuplicateCourse(string courseId, ArtisanCourse course);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/export/{courseId}?contentOnly={contentOnly}")]
        Stream ExportCourse(string courseId, string contentOnly);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/package/{courseId}")]
        Stream GetPackage(string courseId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assets/{assetId}")]
        SingleResult<ArtisanAsset> SaveAsset(string assetId, ArtisanAsset asset);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assets/")]
        SingleResult<ArtisanAsset> CreateAsset(ArtisanAsset asset);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/deployments/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ArtisanDeploymentPackage> GetDeployments(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/deployment/{id}")]
        MultipleResult<ArtisanDeploymentInfo> GetDeployment(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/deployment")]
        SingleResult<ArtisanDeploymentPackage> ApplyDeployment(ArtisanDeploymentPackage course);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/sourceimportinfo/?id={id}&searchText={searchText}")]
        MultipleResult<ArtisanSourceImportInfo> GetArtisanSourceImportInfo(string id, string searchText);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/mediaplayers/")]
        MultipleResult<MediaPlayer> GetMediaPlayers();

        #region Parameters

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/parameters/?codes={codes}&" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Parameter> GetParameters(string searchText, string codes, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/parameters/{parameterId}/values?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ParameterValue> GetParameterValues(string parameterId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/parameterValues/{parameterSetOptionId}/?codes={codes}&" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ParameterValue> GetParameterValuesForOption(string parameterSetOptionId, string codes, string searchText, int start, int limit, string sort, string dir);

        /*[OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/parameters/code/{parameterCode}/values?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ParameterValue> GetParameterValues(string parameterCode, string searchText, int start, int limit, string sort, string dir);*/

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/parametersets/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ParameterSet> GetParameterSets(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/parametersets/{parameterSetId}/options?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ParameterSetOption> GetParameterSetOptions(string parameterSetId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract(Name="GetAllParameterSetOptions")]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/parametersets/options?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ParameterSetOption> GetParameterSetOptions(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/parameters/{parameterId}")]
        SingleResult<Parameter> SaveParameter(string parameterId, Parameter parameter);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/parametersets/{parameterSetId}")]
        SingleResult<ParameterSet> SaveParameterSet(string parameterSetId, ParameterSet parameterSet);

        
        #endregion

        #region courseImporter

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/importproschool/")]
        SingleResult<int> ImportProSchool();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/artisanimportjob/{id}")]
        SingleResult<ArtisanImportJob> GetArtisanImportJob(string id);

        #endregion
    }
}
