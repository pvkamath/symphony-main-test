﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    // NOTE: If you change the interface name "ICategory" here, you must also update the reference to "ICategory" in Web.config.
    [ServiceContract]
    public interface ILicenseService
    {

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/datafield/?" + Templates.GenericGETParametersWithSearch)]
        MultipleResult<LicenseDataField> GetDataFields(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/license/{licenseID}")]
        SingleResult<License> GetLicense(string licenseID);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/licenseUserAssignments/{licenseId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<LicenseAssignment> GetLicenseUserAssignments(string licenseId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/licenseJobRoleAssignments/{licenseId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<LicenseAssignment> GetLicenseJobRoleAssignments(string licenseId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/licenseLocationAssignments/{licenseId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<LicenseAssignment> GetLicenseLocationAssignments(string licenseId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/licenseAudienceAssignments/{licenseId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<LicenseAssignment> GetLicenseAudienceAssignments(string licenseId, string searchText, int start, int limit, string sort, string dir);

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/license/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<License> GetLicenses(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/license/{licenseID}")]
        SingleResult<License> SaveLicense(string licenseID, License license);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprogram/{trainingProgramID}?search={searchText}")]
        MultipleResult<TrainingProgramLicense> GetLicensesForTrainingProgram(string trainingProgramID, string searchText);
        
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprogram/{trainingProgramID}")]
        MultipleResult<TrainingProgramLicense> SaveLicensesForTrainingProgram(string trainingProgramID, List<TrainingProgramLicense> licenses);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/license/{licenseID}")]
        SingleResult<bool> DeleteLicense(string licenseID);

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jurisdiction/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Jurisdiction> GetJurisdictions(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jurisdiction/{jurisdictionID}")]
        SingleResult<Jurisdiction> GetJurisdiction(string jurisdictionID);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jurisdiction/{jurisdictionID}")]
        SingleResult<Jurisdiction> SaveJurisdiction(string jurisdictionID, Jurisdiction jurisdiction);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jurisdiction/{jurisdictionID}")]
        SingleResult<Jurisdiction> DeleteJurisdiction(string jurisdictionID);

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/profession/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Profession> GetProfessions(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/profession/{professionID}")]
        SingleResult<Profession> GetProfession(string professionID);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/profession/{professionID}")]
        SingleResult<Profession> SaveProfession(string professionID, Profession profession);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/profession/{professionID}")]
        SingleResult<Profession> DeleteProfession(string professionID);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/list")]
        MultipleResult<SimpleLicenseList> GetLicensesList();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/selfsave/{licenseID}")]
        SingleResult<LicenseAssignment> UserSelfSave(string licenseId, LicenseAssignment la);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assignment/delete/{id}")]
        SingleResult<Boolean> DeleteLicenseAssignment(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/notes")]
        SingleResult<LicenseAssignmentNote> SaveLicenseAssignmentNote(LicenseAssignmentNote note);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/notes/{licenseAssignmentId}")]
        MultipleResult<LicenseAssignmentNote> GetNotesForLicenseAssignment(string licenseAssignmentId);


        //-------------------------------- 
        // License Reporting APIS
        //-------------------------------- 
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/{entityType}/{entityId}")]
        MultipleResult<EntityLicenseReport> GetEntityLicenseReport(string entityType, string entityId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/users/{entityType}/{entityId}")]
        MultipleResult<EntityLicenseReport> GetUserLicenseReportForEntity(string entityType, string entityId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/user/{userId}")]
        MultipleResult<LicenseAssignment> GetUserLicenseReport(string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/expiration/{entityType}")]
        SingleResult<LicenseSummaryReport> GetExpirationLicenseReportForEntity(string entityType);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/reports/status/{entityType}")]
        SingleResult<LicenseSummaryReport> GetStatusLicenseReportForEntity(string entityType);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/assignments/documents/{licenseAssignmentId}")]
        MultipleResult<LicenseAssignmentDocumentInfo> GetLicenseAssignmentDocuments(string licenseAssignmentId);
    }
}

