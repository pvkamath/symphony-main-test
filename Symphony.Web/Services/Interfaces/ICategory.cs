﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    // NOTE: If you change the interface name "ICategory" here, you must also update the reference to "ICategory" in Web.config.
    [ServiceContract]
    public interface ICategoryService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/categories/{categoryTypeString}/?" + Templates.GenericGETParametersWithSearch)]
        MultipleResult<Category> GetCategories(string categoryTypeString, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/categories/{categoryTypeString}/paged/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Category> GetCategoriesPaged(string categoryTypeString, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/secondarycategories/{categoryTypeString}/paged/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Category> GetSecondaryCategoriesPaged(string categoryTypeString, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/categories/{categoryTypeString}/{id}")]
        SingleResult<Category> GetCategory(string categoryTypeString, string id);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/categories/{categoryTypeString}/{id}")]
        SingleResult<Category> SaveCategory(string categoryTypeString, string id, Category category);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/categories/{categoryTypeString}/{id}")]
        SingleResult<Category> DeleteCategory(string categoryTypeString, string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/categories/{categoryTypeString}/relativesof/{categoryId}/?" + Templates.GenericGETParametersWithSearch)]
        MultipleResult<Category> GetCategoriesFromRelativeCategoryId(string categoryTypeString, string categoryId, string searchText, int start, int limit, string sort, string dir);

    }
}

