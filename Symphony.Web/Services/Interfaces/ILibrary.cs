using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    [ServiceContract]
    public interface ILibraryService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/libraries/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Library> GetLibraries(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/libraries/grants/user/{userId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<LibraryGrant> GetLibrariesForUser(string userId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/grants/{libraryId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<LibraryGrantHierarchy> GetHierarchiesForLibrary(string libraryId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/{libraryId}")]
        SingleResult<Library> GetLibrary(string libraryId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/{libraryId}/categories/?" + Templates.GenericGETParametersFilterable)]
        PagedResult<LibraryCategory> GetLibraryCategories(string libraryId, string filter, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/categories/?" + Templates.GenericGETParametersFilterable)]
        PagedResult<LibraryCategory> GetLibraryCategoriesCrossLibrary(string filter, string searchText, int start, int limit, string sort, string dir);

        [OperationContract(Name="GetLibraryCoursesInCategory")]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/{libraryId}/category/{categoryId}/items/?" + Templates.GenericGETParametersFilterable)]
        PagedResult<LibraryItem> GetLibraryItems(string libraryId, string categoryId, string filter, string searchText, int start, int limit, string sort, string dir);

        [OperationContract(Name = "GetAllLibraryCourses")]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/{libraryId}/items/?" + Templates.GenericGETParametersFilterable)]
        PagedResult<LibraryItem> GetLibraryItems(string libraryId, string filter, string searchText, int start, int limit, string sort, string dir);

        [OperationContract(Name = "GetLibraryCoursesInCategoryCrossLibrary")]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/category/{categoryId}/items/?" + Templates.GenericGETParametersFilterable)]
        PagedResult<LibraryItem> GetLibraryItemsCrossLibrary(string categoryId, string filter, string searchText, int start, int limit, string sort, string dir);

        [OperationContract(Name = "GetAllLibraryCoursesCrossLibrary")]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/items/?" + Templates.GenericGETParametersFilterable)]
        PagedResult<LibraryItem> GetLibraryItemsCrossLibrary(string filter, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/filters/{userId}/")]
        MultipleResult<LibraryFilterOption> GetLibraryFilterOptions(string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/{libraryId}/items/")]
        MultipleResult<LibraryItem> SaveLibraryItems(string libraryId, List<LibraryItem> items);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/{libraryId}/items/")]
        MultipleResult<LibraryItem> DeleteLibraryItems(string libraryId, List<LibraryItem> items);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/{libraryId}")]
        SingleResult<Library> SaveLibrary(string libraryId, Library library);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/grants/{libraryId}/")]
        MultipleResult<LibraryGrantHierarchy> SaveLibraryGrantHierarchies(string libraryId, List<LibraryGrantHierarchy> grants);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/grants/{libraryId}/")]
        SimpleSingleResult<bool> DeleteLibraryGrantHierarchies(string libraryId, List<LibraryGrantHierarchy> grants);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/{libraryId}")]
        SimpleSingleResult<bool> DeleteLibrary(string libraryId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/item/{itemId}/user/{userId}")]
        SimpleSingleResult<DisplayIconState> SaveFavorite(string itemId, string userId);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/library/item/{itemId}/user/{userId}")]
        SimpleSingleResult<DisplayIconState> DeleteFavorite(string itemId, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/libraries/customer/{customerId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Library> GetLibrariesForCustomer(string customerId, string searchText, int start, int limit, string sort, string dir);
    } 
}