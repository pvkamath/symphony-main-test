﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    // NOTE: If you change the interface name "ICourseAssignment" here, you must also update the reference to "ICourseAssignment" in Web.config.
    [ServiceContract]
    public interface ICourseAssignmentService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publicdocuments/delete/{publicDocumentId}")]
        SingleResult<bool> DeletePublicDocument(string publicDocumentId);
        
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publicdocuments/binary/{publicDocumentId}")]
        Stream DownloadPublicDocument(string publicDocumentId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publicdocumentcategories/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<PublicDocumentCategory> GetPublicDocumentCategories(string searchText, int start, int limit, string sort, string dir);

        // annoyingly, WCF can't tell the diff between GET and POST; it attempts to map this to the GET method if the query parameters aren't specified
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publicdocumentcategories/save/")]
        MultipleResult<PublicDocumentCategory> SavePublicDocumentCategories(List<PublicDocumentCategory> categories);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/lockedusers/?" + Templates.GenericGETParametersWithSearch + "&lockedOnly={lockedOnly}")]
        PagedResult<LockedUser> GetLockedUsers(string searchText, int start, int limit, string sort, string dir, bool lockedOnly);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/lockedusercourses/{userId}?lockedOnly={lockedOnly}")]
        MultipleResult<OnlineCourseRegistration> GetLockedUserCourses(string userId, bool lockedOnly);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/?" + Templates.GenericGETParametersWithSearch + "&filter={filter}")]
        PagedResult<TrainingProgram> GetTrainingPrograms(string searchText, int start, int limit, string sort, string dir, string filter);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/template/{templateId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<TrainingProgram> GetTrainingProgramsForTemplate(string templateId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingproholdbycustomer/{customerId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<TrainingProgram> GetTrainingProgramsHoldByCustomer(string customerId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Course> GetCourses(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/{courseId}")]
        SingleResult<Course> GetCourse(string courseId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/{courseId}/{trainingProgramId}")]
        SingleResult<Course> GetCourseLaunchDetails(string courseId, string trainingProgramId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/{courseId}")]
        SingleResult<Course> SaveCourse(string courseId, Course course);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publicdocuments/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<PublicDocument> GetPublicDocuments(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publicdocuments/{publicDocumentId}")]
        SingleResult<PublicDocument> GetPublicDocument(string publicDocumentId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publicdocuments/{publicDocumentId}")]
        SingleResult<PublicDocument> SavePublicDocument(string publicDocumentId, PublicDocument document);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/locations/")]
        MultipleResult<Location> GetLocations();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/jobroles/")]
        MultipleResult<JobRole> GetJobRoles();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/audiences/")]
        MultipleResult<Audience> GetAudiences();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/{trainingProgramId}/onlinecourserollups/{onlineCourseId}/?" + Templates.GenericGETParametersWithSearch + "&lockedOnly={lockedOnly}")]
        PagedResult<OnlineCourseRollup> GetOnlineCourseRollups(string trainingProgramId, string onlineCourseId, string searchText, int start, int limit, string sort, string dir, bool lockedOnly);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/onlinecourserollup/{onlineCourseRollupId}/")]
        SingleResult<OnlineCourseRollup> GetOnlineCourseRollup(string onlineCourseRollupId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/{trainingProgramId}/onlinecourseregistrations/{onlineCourseId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<OnlineCourseRegistration> GetOnlineCourseRegistrations(string trainingProgramId, string onlineCourseId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/{trainingProgramId}/onlinecourseregistrations/{onlineCourseId}/")]
        SingleResult<bool> SaveOnlineCourseRegistrations(string trainingProgramId, string onlineCourseId, OnlineCourseRegistration[] registrations);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/resetattempts/{onlineCourseRollupId}?notes={notes}")]
        SingleResult<OnlineCourseRegistration> ResetAttemptCount(string onlineCourseRollupId, string notes);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/deleteregistration/{onlineCourseRollupId}?notes={notes}&ca={clnUpAssignmnets}")]
        SingleResult<OnlineCourseRegistration> DeleteRegistration(string onlineCourseRollupId, string notes, string clnUpAssignmnets);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/courses/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Course> FindAvailableTrainingProgramCourses(List<CourseRecord> exclude, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/{trainingProgramId}/courses/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Course> FindAssignedTrainingProgramCourses(string trainingProgramId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/{trainingProgramId}?isCheckForRollup={isCheckForRollupStr}")]
        SingleResult<TrainingProgram> SaveTrainingProgram(string trainingProgramId, string isCheckForRollupStr, TrainingProgram trainingProgram);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprogramfiles/{trainingProgramId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<TrainingProgramFile> FindTrainingProgramFiles(string trainingProgramId, string searchText, int start, int limit, string sort, string dir);

/*        [OperationContract]
        [WebInvoke(Method ="GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/{id}/surveys")]
        MultipleResult<Course> */

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "trainingprogramfiles/delete/{trainingProgramFileId}")]
        SingleResult<bool> DeleteTrainingProgramFile(string trainingProgramFileId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/{trainingProgramId}/islive/{isLive}")]
        SingleResult<bool> UpdateTrainingProgramLive(string trainingProgramId, string isLive);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/{trainingProgramId}/isnewhire/{isNewHire}")]
        SingleResult<bool> UpdateTrainingProgramNewHire(string trainingProgramId, string isNewHire);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/courses/userstarttime/{courseId}/")]
        SingleResult<bool> UpdateCourseUserStartTime(string courseId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/userstarttime/{trainingProgramId}/{courseId}/")]
        SingleResult<bool> UpdateTrainingProgramUserStartTime(string trainingProgramId, string courseId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/extension/{trainingProgramId}/{courseId}/{userId}/{minutes}")]
        SingleResult<bool> SaveCourseExtension(string trainingProgramId, string courseId, string userId, string minutes);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/extension/{trainingProgramId}/{courseId}/{userId}")]
        SingleResult<bool> DeleteCourseExtension(string trainingProgramId, string courseId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/delete/{trainingProgramId}")]
        SingleResult<bool> DeleteTrainingProgram(string trainingProgramId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/unassign/{trainingProgramId}/{userId}")]
        SingleResult<bool> UnassignTrainingProgram(string trainingProgramId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/unassign/{trainingProgramId}/{userId}/bundle")]
        SingleResult<bool> UnassignTrainingProgramBundle(string trainingProgramId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/notificationtemplates/delete/{notificationTemplateId}")]
        SingleResult<bool> DeleteNotificationTemplate(string notificationTemplateId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/notificationtemplates/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<NotificationTemplate> FindNotificationTemplates(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/notificationtemplates/trainingprogram/{trainingProgramId}")]
        MultipleResult<NotificationTemplate> GetNotificationTemplatesForTrainingProgram(string trainingProgramId);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/notificationtemplates/detail?" + Templates.GenericGETParametersFilterable)]
        PagedResult<NotificationTemplate> FindNotificationTemplatesDetail(string searchText, int start, int limit, string sort, string dir, string filter);



        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/notificationtemplates/{notificationTemplateId}")]
        SingleResult<NotificationTemplate> GetNotificationTemplate(string notificationTemplateId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/notificationtemplates/{notificationTemplateId}")]
        SingleResult<NotificationTemplate> SaveNotificationTemplate(string notificationTemplateId, NotificationTemplate notificationTemplate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/notificationtemplates/system/")]
        MultipleResult<NotificationTemplate> GetUserCreatableSystemTemplates();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/scheduleparameters/")]
        MultipleResult<ScheduleParameter> GetScheduleParameters();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/specificusers/?" + Templates.GenericGETParameters)]
        PagedResult<User> GetSpecificUsers(int[] userIds, int start, int limit, string sort, string dir);
        
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/previewnotificationtemplate/{templateId}/{scheduleParameterId}")]
        SimpleSingleResult<string> PreviewNotificationTemplate(string templateId, string scheduleParameterId, string template);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/categories/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Category> GetCategories(string searchText, int start, int limit, string sort, string dir);

        // annoyingly, WCF can't tell the diff between GET and POST; it attempts to map this to the GET method if the query parameters aren't specified
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/categories/save/")]
        MultipleResult<Category> SaveCategories(List<Category> categories);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messages/")]
        SingleResult<bool> SaveMessage(QuickMessage message);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/templatehistory/{templateId}")]
        SingleResult<bool> ClearTemplateHistory(string templateId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/templatehistory/{templateId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Message> FindTemplateHistory(string templateId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/templatehistory/export/{templateId}?" + Templates.GenericGETParametersWithSearch)]
        Stream ExportTemplateHistory(string templateId, string searchText, int start, int limit, string sort, string dir);

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/downloadcourses/")]
        SimpleSingleResult<string> PackageCourses(BulkDownloadRequest bulkDownloadRequest);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/downloadcourses/{fileName}")]
        Stream DownloadCourses(string fileName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/coursestatus/{onlineCourseRollupId}/")]
        MultipleResult<ScormRecord> GetOnlineCourseStatus(string onlineCourseRollupId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/coursestatus/{onlineCourseRollupId}/")]
        MultipleResult<ScormRecord> UpdateOnlineCourseStatus(ScormCourseProgress progress, string onlineCourseRollupId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/sessions/assignedusers/?" + Templates.GenericGETParametersWithSearch + 
            "&trainingProgramIds={trainingProgramIds}&sessionCourseId={sessionCourseId}&dateRange={dateRange}&showActive={showActive}&showInRangeOnly={showInRangeOnly}&registrationStatusId={registrationStatusId}&userIds={userIds}")]
        PagedResult<SessionAssignedUser> GetSessionAssignedUsers(
            string trainingProgramIds, string sessionCourseId, string dateRange, string showActive, string showInRangeOnly, string registrationStatusId, string userIds,
            string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/sessions/trainingprograms/?userIds={userIds}")]
        MultipleResult<TrainingProgram> GetSessionTrainingProgramsForUsers(string userIds);


        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/launchmeta/{userId}/{courseId}/{trainingProgramId}/")]
        SimpleSingleResult<bool> SaveLaunchMeta(LaunchMeta launchMeta, string userId, string courseId, string trainingProgramId);


        #region TrainingProgramBundles
        
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprogrambundle/{trainingProgramBundleId}")]
        SingleResult<TrainingProgramBundle> SaveTrainingProgramBundle(string trainingProgramBundleId, TrainingProgramBundle trainingProgramBundle);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprogrambundle/{trainingProgramBundleId}")]
        SimpleSingleResult<bool> DeleteTrainingProgramBundle(string trainingProgramBundleId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprogrambundle/{trainingProgramBundleId}")]
        SingleResult<TrainingProgramBundle> GetTrainingProgramBundle(string trainingProgramBundleId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprogrambundles/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<TrainingProgramBundle> GetTrainingProgramBundles(string searchText, int start, int limit, string sort, string dir);

        #endregion


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/launchstep/{codename}/{trainingProgramId}/{courseId}/{userId}")]
        SingleResult<DisplayLaunchStep> GetLaunchStep(string codename, string trainingProgramId, string courseId, string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/launchstep/profession/")]
        SingleResult<LaunchMeta> SaveProfession(LaunchMeta meta);


        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/launchstep/proctor/")]
        SingleResult<LaunchMeta> SaveProctor(LaunchMeta meta);


        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/launchstep/affidavit/")]
        SingleResult<LaunchMeta> SaveAffidavit(LaunchMeta meta);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/launchstep/validation/")]
        SingleResult<bool> SaveValidationParameters(LaunchMeta meta);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/launchstep/prevalidation/")]
        SingleResult<bool> SavePreValidation(LaunchMeta meta);

        // Proctor Key
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/proctorkey/generate/{rollupId}/{trainingProgramId}/{courseId}/{userId}")]
        SingleResult<OnlineCourseRollup> GenerateProctorKey(string rollupId, string trainingProgramId, string courseId, string userId); 

    }
}
