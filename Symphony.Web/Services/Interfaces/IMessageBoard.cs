﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    // NOTE: If you change the interface name "IMessageBoard" here, you must also update the reference to "IMessageBoard" in Web.config.
    [ServiceContract]
    public interface IMessageBoardService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messageBoard/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<MessageBoard> GetMessageBoards(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messageBoard/{messageBoardId}")]
        SingleResult<MessageBoard> GetMessageBoard(string messageBoardId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messageBoard/{messageBoardType}/{classId}")]
        SingleResult<MessageBoard> GetMessageBoardForClass(string messageBoardType, string classId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messageBoard/{messageBoardId}")]
        SingleResult<MessageBoard> UpdateMessageBoard(string messageBoardId, MessageBoard messageBoard);

        
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/topic/added/{messageBoardId}/{lastAdded}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<MessageBoardTopic> GetMessageBoardTopicsLastAdded(string messageBoardId, string searchText, int start, int limit, string sort, string dir, string lastAdded);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/topic/{messageBoardId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<MessageBoardTopic> GetMessageBoardTopics(string messageBoardId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/topic/{messageBoardId}/{topicId}")]
        SingleResult<MessageBoardTopic> GetMessageBoardTopic(string messageBoardId, string topicId);
        
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/topic/{topicId}")]
        SingleResult<MessageBoardTopic> UpdateMessageBoardTopic(string topicId, MessageBoardTopic messageBoardTopic);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/topic/{topicId}")]
        SingleResult<MessageBoardTopic> DeleteMessageBoardTopic(string topicId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/post/{postId}")]
        SingleResult<MessageBoardPost> UpdateMessageBoardPost(string postId, MessageBoardPost messageBoardPost);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/post/{postId}")]
        SingleResult<MessageBoardPost> DeleteMessageBoardPost(string postId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/post/{topicId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<MessageBoardPost> GetMessageBoardPosts(string topicId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/post/{topicId}/{postId}")]
        SingleResult<MessageBoardPost> GetMessageBoardPost(string topicId, string postId);


    }
}
