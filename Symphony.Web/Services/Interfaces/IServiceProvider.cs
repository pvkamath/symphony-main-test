﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    // NOTE: If you change the interface name "IServiceProvider" here, you must also update the reference to "IServiceProvider" in Web.config.
    [ServiceContract]
    public interface IServiceProviderService
    {

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/serviceprovider/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ServiceProvider> GetServiceProviders(string searchText, int start, int limit, string sort, string dir);


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/serviceprovider/{id}")]
        SingleResult<ServiceProvider> PostServiceProvider(string id, ServiceProvider provider);

    }
}

