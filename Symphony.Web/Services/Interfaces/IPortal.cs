﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;
using System.IO;

namespace Symphony.Web.Services
{
    [ServiceContract]
    public interface IPortalService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publicdocuments/download/{publicDocumentId}")]
        Stream DownloadPublicDocument(string publicDocumentId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/gtmlogin/{meetingId}")]
        SingleResult<GTMLoginInfo> GetGTMLoginInfo(string meetingId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classdetails/{classId}")]
        SingleResult<ClassroomClass> GetClassDetails(string classId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publicdocuments/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<PublicDocument> GetPublicDocuments(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/publiccourses/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<PublicCourse> GetPublicCourses(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/upcomingevents/{filter}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<UpcomingEvent> GetUpcomingEvents(string searchText, string filter, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/upcomingevents/calendar/{year}/{month}/{filter}?")]
        MultipleResult<CalendarWeek> GetUpcomingEventsCalendar(string year, string month, string filter);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<TrainingProgram> GetTrainingPrograms(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/list/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<TrainingProgramSummaryDetail> ListTrainingPrograms(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/list/all/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<TrainingProgramSummaryDetail> ListAllTrainingPrograms(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/list/all/{userId}/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<TrainingProgramSummaryDetail> ListAllTrainingProgramsForUser(string userId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprograms/{userId}?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<TrainingProgram> GetTrainingProgramsForUser(string userId, string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprogramdetails/{trainingProgramId}?manage={manage}")]
        SingleResult<TrainingProgram> GetTrainingProgramDetails(string trainingProgramId, bool manage);

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprogramdetails/{trainingProgramId}/{userId}")]
        SingleResult<TrainingProgram> GetTrainingProgramDetailsForUser(string trainingProgramId, string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/?courseId={courseId}&minDate={minDate}&maxDate={maxDate}&" + Templates.GenericGETParameters)]
        PagedResult<Class> GetClassesForCourse(int courseId, DateTime minDate, DateTime maxDate, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/register/{classId}")]
        SingleResult<bool> RegisterForClass(string classId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/classes/unregister/{registrationId}")]
        SingleResult<bool> UnregisterForClass(string registrationId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/trainingprogramfiles/download/{fileId}")]
        Stream GetTrainingProgramFile(string fileId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messages/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Message> GetMessages(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messages/unread/")]
        SingleResult<int> GetUnreadCount();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messages/read/{messageId}")]
        SingleResult<bool> ReadMessage(string messageId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/meetings/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<Meeting> GetMeetings(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/meetings/{meetingId}")]
        SingleResult<Meeting> UpdateMeeting(string meetingId, Meeting meeting);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/users/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<User> GetUsers(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/transcript/")]
        SingleResult<List<TranscriptEntry>> GetTranscript();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/transcript/courses/")]
        SingleResult<List<TranscriptEntryCourse>> GetTranscriptCourses();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/transcript/courses/{userId}")]
        SingleResult<List<TranscriptEntryCourse>> GetTranscriptCoursesForUser(string userId);

        [OperationContract(Name="GetTrainingProgramTranscriptCoursesForUser")]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/transcript/courses/{userId}/{trainingProgramId}/")]
        SingleResult<List<TranscriptEntryCourse>> GetTranscriptCoursesForUser(string userId, string trainingProgramId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/test/")]
        PagedResult<PublicCourse> Test();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messages/delete/")]
        SingleResult<bool> DeleteAllMessages();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/messages/delete/{messageId}")]
        SingleResult<bool> DeleteMessage(string messageId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/meetings/delete/{meetingId}")]
        SingleResult<bool> DeleteMeeting(string meetingId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/passwordchange")]
        SingleResult<bool> ChangePassword(PasswordChange passwordChange);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/recoverpassword")]
        SingleResult<bool> RecoverPassword(PasswordRecover recover);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/switchuser/{userId}")]
        SimpleSingleResult<string> SwitchUser(string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/exituser")]
        SingleResult<bool> ExitUser();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/portalsettings/{customerId}")]
        MultipleResult<PortalSetting> GetPortalSettings(string customerId);

    }
}
