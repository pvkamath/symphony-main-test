﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;

namespace Symphony.Web.Services
{
    [ServiceContract]
    public interface IProctorService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/proctorForms/{id}")]
        SingleResult<ProctorForm> FindProctorForm(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/proctorForms/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<ProctorForm> QueryProctorForms(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/proctorForms/")]
        SingleResult<ProctorForm> CreateProctorFormWithoutId(ProctorForm model);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/proctorForms/{id}")]
        SingleResult<ProctorForm> CreateProctorFormWithId(string id, ProctorForm model);

        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/proctorForms/{id}")]
        SingleResult<ProctorForm> UpdateProctorForm(string id, ProctorForm model);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/proctorForms/{id}")]
        SingleResult<ProctorForm> DeleteProctorForm(string id, ProctorForm model);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/proctorForms/responses")]
        SimpleSingleResult<bool> SaveProctor(ProctorFormResponse model);
    }
}
