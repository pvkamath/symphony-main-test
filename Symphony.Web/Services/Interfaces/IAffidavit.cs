﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Symphony.Core.Models;

namespace Symphony.Web.Services
{
    [ServiceContract]
    public interface IAffidavitService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/affidavits/{id}")]
        SingleResult<AffidavitFinalExam> FindAffidavit(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/affidavits/?" + Templates.GenericGETParametersWithSearch)]
        PagedResult<AffidavitFinalExam> QueryAffidavits(string searchText, int start, int limit, string sort, string dir);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/affidavits/")]
        SingleResult<AffidavitFinalExam> CreateAffidavitWithoutId(AffidavitFinalExam model);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/affidavits/{id}")]
        SingleResult<AffidavitFinalExam> CreateAffidavitWithId(string id, AffidavitFinalExam model);

        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/affidavits/{id}")]
        SingleResult<AffidavitFinalExam> UpdateAffidavit(string id, AffidavitFinalExam model);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/affidavits/{id}")]
        SingleResult<AffidavitFinalExam> DeleteAffidavit(string id, AffidavitFinalExam model);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/affidavits/responses")]
        SimpleSingleResult<bool> SaveAffidavitResponse(AffidavitFinalExamResponse model);
    }
}
