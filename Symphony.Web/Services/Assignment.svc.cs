﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using Symphony.Core;

namespace Symphony.Web.Services
{
    // NOTE: If you change the class name "Assignment" here, you must also update the reference to "Assignment" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class AssignmentService : BaseService, IAssignmentService
    {
        ILog Log = LogManager.GetLogger(typeof(AssignmentService));

       

        public PagedResult<UserAssignment>  GetUsersWithAssignments(string trainingProgramId, string courseId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int tpid, cid;
                if (!int.TryParse(trainingProgramId, out tpid)) {
                    throw new Exception("Invalid training program id.");
                }
                if (!int.TryParse(courseId, out cid)) {
                    throw new Exception("Invalid course id");
                }
                return (new AssignmentController()).GetUsersWithAssignments(tpid, cid, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<UserAssignment>(ex);
            }
        }

        public PagedResult<Assignment> GetAssignments(string trainingProgramId, string courseId, string userId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int tpid, cid, uid;
                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Invalid training program id.");
                }
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id");
                }
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id");
                }
                return (new AssignmentController()).GetAssignments(tpid, cid, uid, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Assignment>(ex);
            }
        }

        public MultipleResult<AssignmentResponse> GetAssignmentResponses(string trainingProgramId, string courseId, string sectionId, string attemptId, string userId)
        {
            try
            {
                int tpid, cid, uid, sid, aid;
                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Invalid training program id.");
                }
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id");
                }
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id");
                }
                if (!int.TryParse(sectionId, out sid))
                {
                    throw new Exception("Invalid section id");
                }
                if (!int.TryParse(attemptId, out aid))
                {
                    throw new Exception("Invalid attempt id");
                }
                return (new AssignmentController()).GetAssignmentResponses(tpid, cid, sid, aid, uid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<AssignmentResponse>(ex);
            }

        }

        /*public SimpleSingleResult<bool> PostAssignmentGrade(string onlineCourseAssignmentId, string gradeId)
        {
            try
            {
                int aid;
                AssignmentResponseStatus status;

                if (!int.TryParse(onlineCourseAssignmentId, out aid))
                {
                    throw new Exception("Invalid assignment id.");
                }
                if (!Enum.TryParse<AssignmentResponseStatus>(gradeId, out status))
                {
                    throw new Exception("Invalid assignment grade id");
                }

                return (new AssignmentController()).PostAssignmentGrade(aid, status);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SimpleSingleResult<bool>(ex);
            }
        }*/

        public MultipleResult<AssignmentResponse> PostAssignmentResponses(string trainingProgramId, string courseId, string sectionId, string attemptId, string userId, List<AssignmentResponse> assignmentResponses)
        {
            try
            {
                int tpid, cid, uid, sid, aid;
                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Invalid training program id.");
                }
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id");
                }
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id");
                }
                if (!int.TryParse(sectionId, out sid))
                {
                    throw new Exception("Invalid section id");
                }
                if (!int.TryParse(attemptId, out aid))
                {
                    throw new Exception("Invalid attempt id");
                }

                return (new AssignmentController()).PostAssignmentResponses(tpid, cid, sid, aid, uid, assignmentResponses);
                
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<AssignmentResponse>(ex);
            }

        }

        public SimpleSingleResult<bool> PostAssignmentResponsesCorrect(string trainingProgramId, string courseId, string userId)
        {
            try
            {
                int tpid, cid, uid;
                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Invalid training program id.");
                }
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id");
                }
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id");
                }
                

                return (new AssignmentController()).PostForcedAssignmentResponses(tpid, cid, uid, AssignmentResponseStatus.Correct, true);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SimpleSingleResult<bool>(ex);
            }

        }

        public SimpleSingleResult<bool> PostAssignmentResponsesIncorrect(string trainingProgramId, string courseId, string userId)
        {
            try
            {
                int tpid, cid, uid;
                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Invalid training program id.");
                }
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id");
                }
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id");
                }


                return (new AssignmentController()).PostForcedAssignmentResponses(tpid, cid, uid, AssignmentResponseStatus.Incorrect, false);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SimpleSingleResult<bool>(ex);
            }

        }

        public SimpleSingleResult<bool> PostAssignmentResponsesUnmarked(string trainingProgramId, string courseId, string userId)
        {
            try
            {
                int tpid, cid, uid;
                if (!int.TryParse(trainingProgramId, out tpid))
                {
                    throw new Exception("Invalid training program id.");
                }
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id");
                }
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id");
                }


                return (new AssignmentController()).PostForcedAssignmentResponses(tpid, cid, uid, AssignmentResponseStatus.Unmarked, false);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SimpleSingleResult<bool>(ex);
            }

        }

        public SimpleSingleResult<AssignmentStatus> GetAssignmentStatus(string trainingProgramId, string courseId, string userId)
        {
            try
            {
                int tpId, cid, uid;
                if (!int.TryParse(trainingProgramId, out tpId))
                {
                    throw new Exception("Invalid training program id");
                }
                if (!int.TryParse(courseId, out cid))
                {
                    throw new Exception("Invalid course id.");
                }
                if (!int.TryParse(userId, out uid))
                {
                    throw new Exception("Invalid user id");
                }
                return (new AssignmentController()).GetAssignmentStatus(tpId, cid, uid);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SimpleSingleResult<AssignmentStatus>(ex);
            }
        }
    }
}
