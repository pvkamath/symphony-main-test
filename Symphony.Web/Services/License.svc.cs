﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;
using Symphony.Core;

namespace Symphony.Web.Services
{
    // NOTE: If you change the class name "License" here, you must also update the reference to "License" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class LicenseService : BaseService, ILicenseService
    {
        ILog Log = LogManager.GetLogger(typeof(ILicenseService));

        public MultipleResult<LicenseDataField> GetDataFields(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new LicenseController()).GetLicenseDataFields(searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<LicenseDataField>(ex);
            }
        }

        public PagedResult<License> GetLicenses(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new LicenseController()).GetLicenses(searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<License>(ex);
            }
        }
        
        public MultipleResult<TrainingProgramLicense> GetLicensesForTrainingProgram(string trainingProgramID, string searchText)
        {
            try
            {
                int id = int.Parse(trainingProgramID);
                return (new LicenseController()).GetLicensesForTrainingProgram(id, searchText);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<TrainingProgramLicense>(ex);
            }
        }

        public MultipleResult<TrainingProgramLicense> SaveLicensesForTrainingProgram(string trainingProgramID, List<TrainingProgramLicense> licenses)
        {
            try
            {
                int id = int.Parse(trainingProgramID);
                return (new LicenseController()).SaveLicensesForTrainingProgram(id, licenses);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<TrainingProgramLicense>(ex);
            }
        }

        public SingleResult<License> GetLicense(string licenseID)
        {
            try
            {
                int id = int.Parse(licenseID);
                return LicenseController.GetLicense(id);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<License>(ex);
            }
        }

        public PagedResult<LicenseAssignment> GetLicenseUserAssignments(string licenseId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int licenseid = int.Parse(licenseId);
                return LicenseController.GetLicenseUserAssignments(licenseid, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<LicenseAssignment>(ex);
            }
        }

        public PagedResult<LicenseAssignment> GetLicenseJobRoleAssignments(string licenseId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int licenseid = int.Parse(licenseId);
                return LicenseController.GetLicenseJobRoleAssignments(licenseid, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<LicenseAssignment>(ex);
            }
        }

        public PagedResult<LicenseAssignment> GetLicenseLocationAssignments(string licenseId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int licenseid = int.Parse(licenseId);
                return LicenseController.GetLicenseLocationAssignments(licenseid, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<LicenseAssignment>(ex);
            }
        }

        public PagedResult<LicenseAssignment> GetLicenseAudienceAssignments(string licenseId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int licenseid = int.Parse(licenseId);
                return LicenseController.GetLicenseAudienceAssignments(licenseid, searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<LicenseAssignment>(ex);
            }
        }

        public SingleResult<License> SaveLicense(string licenseId, License license)
        {
            try
            {
                int c = int.Parse(licenseId);

                bool reparent = false;
                if (HttpContext.Current.Request["reparent"] != null)
                {
                    reparent = bool.Parse(HttpContext.Current.Request["reparent"]);
                }
                
                return LicenseController.SaveLicense(c, license, reparent);

            } catch (Exception ex) {
                Log.Error(ex);
                return new SingleResult<License>(ex);
            }
        }

        public SingleResult<bool> DeleteLicense(string licenseID)
        {
            try
            {
                int c = int.Parse(licenseID);
                return LicenseController.DeleteLicense(c);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public PagedResult<Profession> GetProfessions(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new LicenseController()).GetProfessions(searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Profession>(ex);
            }
        }

        public SingleResult<Profession> GetProfession(string professionID)
        {
            try
            {
                int c = int.Parse(professionID);
                return LicenseController.GetProfession(c);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Profession>(ex);
            }
        }

        public SingleResult<Profession> SaveProfession(string professionID, Profession profession)
        {
            try
            {
                int c = int.Parse(professionID);
                return LicenseController.SaveProfession(c, profession);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Profession>(ex);
            }
        }

        public SingleResult<Profession> DeleteProfession(string professionID)
        {
            try
            {
                int c = int.Parse(professionID);
                //return LicenseController.DeleteProfession();
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Profession>(ex);
            }
        }

        public PagedResult<Jurisdiction> GetJurisdictions(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new LicenseController()).GetJurisdictions(searchText, sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Jurisdiction>(ex);
            }
        }

        public SingleResult<Jurisdiction> GetJurisdiction(string jurisdictionID)
        {
            try
            {
                int c = int.Parse(jurisdictionID);
                return LicenseController.GetJurisdiction(c);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Jurisdiction>(ex);
            }
        }

        public SingleResult<Jurisdiction> SaveJurisdiction(string jurisdictionID, Jurisdiction jurisdiction)
        {
            try
            {
                int c = int.Parse(jurisdictionID);
                return LicenseController.SaveJurisdiction(c, jurisdiction);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Jurisdiction>(ex);
            }
        }

        public SingleResult<Jurisdiction> DeleteJurisdiction(string jurisdictionID)
        {
            try
            {
                int c = int.Parse(jurisdictionID);
                return LicenseController.DeleteJurisdiction();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Jurisdiction>(ex);
            }
        }

        public MultipleResult<SimpleLicenseList> GetLicensesList()
        {
            return LicenseController.GetSimpleLicenseList();
        }

        public SingleResult<LicenseAssignment> UserSelfSave(string licenseId, LicenseAssignment la)
        {
            var lid = int.Parse(licenseId);
            // this _is_ self save, afterall. 
            la.UserId = UserID;
            la.CustomerId = CustomerID;

            var t = LicenseController.UserSelfSave(lid, la);
            return t;
        }

        public SingleResult<Boolean> DeleteLicenseAssignment(string id)
        {
            try
            {
                var laId = int.Parse(id);
                return LicenseController.DeleteLicenseAssignmentById(laId);
            }
            catch (Exception e)
            {
                return new SingleResult<Boolean>(e);
            }
        }
        
        private int _ParseUserId(string userId)
        {
            // Using 0 as a default to represent the current user
            int uid = int.Parse(userId);
            if (uid == 0)
            {
                uid = UserID;
            }

            return uid;
        }

        public MultipleResult<LicenseAssignmentNote> GetNotesForLicenseAssignment(string licenseAssignmentId)
        {
            var lid = int.Parse(licenseAssignmentId);

            try
            {
                return LicenseController.GetLicenseNotes(lid);
            } 
            catch (Exception e)
            {
                return new MultipleResult<LicenseAssignmentNote>(e);
            }
        }

        public SingleResult<LicenseAssignmentNote> SaveLicenseAssignmentNote(LicenseAssignmentNote note)
        {
            try
            {
                var result = LicenseController.SaveLicenseNote(note);
                return result;
            }
            catch (Exception e)
            {
                return new SingleResult<LicenseAssignmentNote>(e);
            }
        }

        public MultipleResult<EntityLicenseReport> GetEntityLicenseReport(string entityType, string entityId)
        {
            try
            {
                return LicenseController.GetEntityLicenseReport(entityType, int.Parse(entityId));
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new MultipleResult<EntityLicenseReport>(e);
            }
        }

        public MultipleResult<EntityLicenseReport> GetUserLicenseReportForEntity(string entityType, string entityId)
        {
            try
            {
                return LicenseController.GetUserLicenseReportForEntity(entityType, int.Parse(entityId));
            }
            catch (Exception e)
            {
                MultipleResult<EntityLicenseReport> err = new MultipleResult<EntityLicenseReport>(e);
                return err;
            }
        }

        public MultipleResult<LicenseAssignment> GetUserLicenseReport(string userId)
        {
            try
            {
                return LicenseController.GetUserLicenseReport(int.Parse(userId));
            }
            catch (Exception e)
            {
                return new MultipleResult<LicenseAssignment>(e);
            }

        }

        public SingleResult<LicenseSummaryReport> GetExpirationLicenseReportForEntity(string entityType)
        {
            try
            {
                return LicenseController.GetExpirationReportForEntity(entityType);
            }
            catch (Exception e)
            {
                return new SingleResult<LicenseSummaryReport>(e);
            }
        }

        public SingleResult<LicenseSummaryReport> GetStatusLicenseReportForEntity(string entityType)
        {
            try 
            {
                return LicenseController.GetLicenseStatusSummaryReport(entityType);
            }
            catch (Exception e)
            {
                return new SingleResult<LicenseSummaryReport>(e);
            }
        }

        public MultipleResult<LicenseAssignmentDocumentInfo> GetLicenseAssignmentDocuments(string licenseAssignmentId)
        {
            var laId = int.Parse(licenseAssignmentId);
            try
            {
                return LicenseController.GetLicenseAssignmentDocuments(laId);
            }
            catch (Exception e)
            {
                return new MultipleResult<LicenseAssignmentDocumentInfo>(e);
            }
        }
    }
}
