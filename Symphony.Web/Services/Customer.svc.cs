﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Symphony.Core.Models;
using log4net;
using Symphony.Core.Controllers;
using System.ServiceModel.Activation;
using System.Web;
using System.IO;
using Symphony.Core;

namespace Symphony.Web.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class CustomerService : BaseService, ICustomerService
    {
        ILog Log = LogManager.GetLogger(typeof(CustomerService));
        CustomerController Controller = new CustomerController();

        public Stream GetImportResults(string customerId, string importId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator) && !HasRole(Roles.CustomerManager))
                {
                    throw new Exception("Permission denied.");
                }
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }

                int iimportId = 0;
                if (int.TryParse(importId, out iimportId))
                {
                    string name = string.Empty;
                    Stream s = (new CustomerController()).GetImportResults(CustomerID, UserID, iimportId, ref name);
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + name.Replace(" ", "_").Replace(";", "_"));
                    return s;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public Stream Export(string customerId, string exportTypeId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator) && !HasRole(Roles.CustomerManager))
                {
                    throw new Exception("Permission denied.");
                }
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }

                int iexportType = 0;
                if (int.TryParse(exportTypeId, out iexportType))
                {
                    string name = string.Empty;
                    Stream s = (new CustomerController()).Export(CustomerID, UserID, iexportType, ref name);
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + name.Replace(" ", "_").Replace(";", "_"));
                    return s;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        // Ovveride for self account creation - no public web URL to 
        public SingleResult<bool> SaveLocationUsersOverride(string locationId, int[] userIds)
        {
            try
            {
                return (new CustomerController()).SaveLocationUsers(CustomerID, UserID, int.Parse(locationId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> SaveLocationUsers(string locationId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).SaveLocationUsers(CustomerID, UserID, int.Parse(locationId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        // Used for self account creation - we don't check the perms of the logged in user becasue there is none
        // No public facing URL for this, to prevent security issues
        public SingleResult<bool> SaveJobRoleUsersOverride(string jobRoleId, int[] userIds)
        {
            try
            {
                return (new CustomerController()).SaveJobRoleUsers(CustomerID, UserID, int.Parse(jobRoleId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> SaveJobRoleUsers(string jobRoleId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).SaveJobRoleUsers(CustomerID, UserID, int.Parse(jobRoleId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> SaveAudienceUsers(string audienceId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).SaveAudienceUsers(CustomerID, UserID, int.Parse(audienceId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteLocationUsers(string locationId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteLocationUsers(CustomerID, UserID, int.Parse(locationId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteJobRoleUsers(string jobRoleId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteJobRoleUsers(CustomerID, UserID, int.Parse(jobRoleId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteAudienceUsers(string audienceId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteAudienceUsers(CustomerID, UserID, int.Parse(audienceId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteUser(string customerId, string userId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator) && !HasRole(Roles.CustomerManager))
                {
                    throw new Exception("Permission denied.");
                }
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).DeleteUser(icustomerId, UserID, int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> ResetUser(string customerId, string userId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator) && !HasRole(Roles.CustomerManager))
                {
                    throw new Exception("Permission denied.");
                }
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).ResetUser(icustomerId, UserID, int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public PagedResult<ImportHistory> GetImportHistory(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindImportHistory(icustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<ImportHistory>(ex);
            }
        }
        public PagedResult<User> GetUsers(string searchText, int start, int limit, string sort, string dir, string filter)
        {
            try
            {
                List<GenericFilter> f = null;
                if (!String.IsNullOrEmpty(filter))
                {
                    f = Symphony.Core.Utilities.Deserialize<List<GenericFilter>>(filter);
                }

                return (new CustomerController()).FindUsers(searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, f);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<User> GetUsers(string customerId, string searchText, int start, int limit, string sort, string dir, string filter)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }

                CustomerUserFilter f = null;
                if (!String.IsNullOrEmpty(filter))
                {
                    f = Symphony.Core.Utilities.Deserialize<CustomerUserFilter>(filter);
                }

                int jobRoleId = 0;
                int locationId = 0;

                // look at job role and location
                if (HasRole("Customer - Job Role Manager"))
                {
                    Symphony.Core.Data.User u = new Symphony.Core.Data.User(Symphony.Core.Data.User.Columns.Id, UserID);
                    jobRoleId = u.JobRoleID;
                }
                if (HasRole("Customer - Location Manager"))
                {
                    Symphony.Core.Data.User u = new Symphony.Core.Data.User(Symphony.Core.Data.User.Columns.Id, UserID);
                    locationId = u.LocationID;
                }

                return (new CustomerController()).FindUsers(new List<int>() { }, icustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, f, jobRoleId, locationId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public MultipleResult<Author> GetAuthors(string searchText, int start, int limit, string sort, string dir, string filter)
        {
            try
            {
                if (!HasRole(Roles.ArtisanAuthor))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).FindAuthours(CustomerID, UserID, searchText, start, limit, sort, dir, filter);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<Author>(ex);
            }
 
        }

        public PagedResult<User> GetCustomerCareReps(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                Symphony.Core.Data.Customer c = new Symphony.Core.Data.Customer(customerId);
                return (new CustomerController()).FindCustomerCareReps(c.SalesChannelID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<User> GetAccountExecutives(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                Symphony.Core.Data.Customer c = new Symphony.Core.Data.Customer(customerId);
                return (new CustomerController()).FindAccountExecutives(c.SalesChannelID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<User> GetUsersFiltered(List<int> exclude, string customerId, string searchText, int start, int limit, string sort, string dir, string filter)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }

                CustomerUserFilter f = null;
                if (!String.IsNullOrEmpty(filter))
                {
                    f = Symphony.Core.Utilities.Deserialize<CustomerUserFilter>(filter);
                }

                int jobRoleId = 0;
                int locationId = 0;

                // look at job role and location
                if (HasRole("Customer - Job Role Manager"))
                {
                    Symphony.Core.Data.User u = new Symphony.Core.Data.User(Symphony.Core.Data.User.Columns.Id, UserID);
                    jobRoleId = u.JobRoleID;
                }
                if (HasRole("Customer - Location Manager"))
                {
                    Symphony.Core.Data.User u = new Symphony.Core.Data.User(Symphony.Core.Data.User.Columns.Id, UserID);
                    locationId = u.LocationID;
                }
                return (new CustomerController()).FindUsers(exclude, icustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, f, jobRoleId, locationId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<User> GetUsersUnassigned(string hierarchyName, string hierarchyId, string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                HierarchyType htype = (HierarchyType)Enum.Parse(typeof(HierarchyType), hierarchyName, true);


                return (new CustomerController()).FindUnassignedUsers(htype, int.Parse(hierarchyId), icustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<User> GetUsersByRoles(string customerId, string roles, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindUsersByRoles(icustomerId, UserID, roles.Split(','), searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<User> GetUsersForAudience(string customerId, string audienceId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindUsersForAudience(icustomerId, int.Parse(audienceId), UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<User> GetUsersForJobRole(string customerId, string jobRoleId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindUsersForJobRole(icustomerId, int.Parse(jobRoleId), UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<User> GetUsersForLocation(string customerId, string locationId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindUsersForLocation(icustomerId, int.Parse(locationId), UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<Location> GetLocations(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindLocations(icustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Location>(ex);
            }
        }

        public PagedResult<JobRole> GetJobRoles(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindJobRoles(icustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<JobRole>(ex);
            }
        }

        public PagedResult<Audience> GetAudiences(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                var res = (new CustomerController()).FindAudiences(icustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);

                return res;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Audience>(ex);
            }
        }

        public PagedResult<UserStatus> GetUserStatuses(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CustomerController()).FindUserStatuses(CustomerID, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<UserStatus>(ex);
            }
        }

        public PagedResult<User> GetSupervisors(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindSupervisors(icustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<User> GetReportingSupervisors(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindReportingSupervisors(icustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public PagedResult<Customer> GetCustomers(string salesChannelId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                SymphonyController sc = new SymphonyController();
                if (sc.SalesChannelID == 0 && !sc.CanViewMultipleCustomers(sc.ApplicationName))
                {
                    throw new Exception("You don't have permission to list customers.");
                }
                return (new CustomerController()).FindCustomers(int.Parse(salesChannelId), UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Customer>(ex);
            }
        }

        public PagedResult<Customer> GetAllCustomers(string searchText, int start, int limit, string sort, string dir, string filter)
        {
            List<GenericFilter> f = null;
            if (!string.IsNullOrWhiteSpace(filter))
            {
                try
                {
                    f = Utilities.Deserialize<List<GenericFilter>>(filter);
                }
                catch (Exception e)
                {
                    // Just don't bother filtering in this case.
                    Log.Debug(e);
                }
            }
            return GetAllCustomers(searchText, start, limit, sort, dir, null, f);
        }

        public PagedResult<Customer> GetAllCustomers(string searchText, int start, int limit, string sort, string dir, int[] selectedIds = null)
        {
            return GetAllCustomers(searchText, start, limit, sort, dir, selectedIds, null);
        }

        public PagedResult<Customer> GetAllCustomers(string searchText, int start, int limit, string sort, string dir, int[] selectedIds = null, List<GenericFilter> filter = null)
        {
            try
            {
                SymphonyController sc = new SymphonyController();
                if (sc.SalesChannelID == 0 && !sc.CanViewMultipleCustomers(sc.ApplicationName))
                {
                    throw new Exception("You don't have permission to list all customers.");
                }
                return (new CustomerController()).FindAllCustomers(UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit, selectedIds, filter);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Customer>(ex);
            }
        }

        public PagedResult<Customer> GetNetworkedReportingCustomers(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            return GetNetworkedCustomers(true, false, customerId, searchText, start, limit, sort, dir);
        }
        public PagedResult<Customer> GetNetworkedTrainingProgramCustomers(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            return GetNetworkedCustomers(false, true, customerId, searchText, start, limit, sort, dir);
        }

        public PagedResult<Customer> GetNetworkedCustomers(string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            return GetNetworkedCustomers(false, false, customerId, searchText, start, limit, sort, dir);  
        }

        private PagedResult<Customer> GetNetworkedCustomers(bool isReporting, bool isTrainingProgram, string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int cid;
                if (!int.TryParse(customerId, out cid)) {
                    throw new Exception("Invalid customer id.");
                }
                return (new CustomerController()).GetNetworkedCustomers(isReporting, isTrainingProgram, cid, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Customer>(ex);
            }
        }

        public PagedResult<User> GetAdministrators(string salesChannelId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                if (new SymphonyController().SalesChannelID == 0)
                {
                    throw new Exception("You don't have permission to list administrators.");
                }
                return (new CustomerController()).FindAdministrators(int.Parse(salesChannelId), UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<User>(ex);
            }
        }

        public SingleResult<Customer> GetCustomer(string customerId)
        {
            try
            {
                if (new SymphonyController().SalesChannelID == 0)
                {
                    throw new Exception("You don't have permission to get customers.");
                }
                return (new CustomerController()).GetCustomer(CustomerID, UserID, int.Parse(customerId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Customer>(ex);
            }
        }

        public SingleResult<CustomerSettings> GetCustomerSettings(string customerId)
        {
            try
            {
                return (new CustomerController()).GetCustomerSettings(CustomerID, UserID, int.Parse(customerId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<CustomerSettings>(ex);
            }
        }

        public SingleResult<Customer> SaveCustomer(string customerId, Customer customer)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                if (new SymphonyController().SalesChannelID == 0)
                {
                    throw new Exception("You don't have permission to save customers.");
                }
                return (new CustomerController()).SaveCustomer(CustomerID, UserID, int.Parse(customerId), customer);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Customer>(ex);
            }
        }

        public SingleResult<CustomerSettings> SaveCustomerSettings(string customerId, CustomerSettings customerSetting)
        {
            try
            {
                return (new CustomerController()).SaveCustomerSettings(CustomerID, UserID, int.Parse(customerId), customerSetting);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<CustomerSettings>(ex);
            }
        }

        public SingleResult<User> GetAdministrator(string userId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                if (new SymphonyController().SalesChannelID == 0)
                {
                    throw new Exception("You don't have permission to get administrators.");
                }
                return (new CustomerController()).GetAdministrator(CustomerID, UserID, int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<User>(ex);
            }
        }

        public SingleResult<User> SaveAdministrator(string userId, User administrator)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                if (new SymphonyController().SalesChannelID == 0)
                {
                    throw new Exception("You don't have permission to save administrators.");
                }
                return (new CustomerController()).SaveAdministrator(CustomerID, UserID, int.Parse(userId), administrator);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<User>(ex);
            }
        }

        public MultipleResult<SalesChannel> GetSalesChannels()
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                if (new SymphonyController().SalesChannelID == 0)
                {
                    throw new Exception("You don't have permission to list sales channels.");
                }
                var res = (new CustomerController()).GetSalesChannels(CustomerID, UserID);

                return res;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<SalesChannel>(ex);
            }
        }

        public SingleResult<SalesChannel> GetSalesChannel(string salesChannelId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                if (new SymphonyController().SalesChannelID == 0)
                {
                    throw new Exception("You don't have permission to list sales channels.");
                }
                return (new CustomerController()).GetSalesChannel(int.Parse(salesChannelId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<SalesChannel>(ex);
            }
 
        }

        public SingleResult<SalesChannel> SaveSalesChannel(string salesChannelId, SalesChannel salesChannel)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                bool reparent = false;
                if (HttpContext.Current.Request["reparent"] != null)
                {
                    reparent = bool.Parse(HttpContext.Current.Request["reparent"]);
                }
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).SaveSalesChannel(CustomerID, UserID, int.Parse(salesChannelId), salesChannel, reparent);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<SalesChannel>(ex);
            }
        }

        public SingleResult<User> GetUser(string customerId, string userId)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).GetUser(icustomerId, UserID, int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<User>(ex);
            }
        }

        public MultipleResult<UserReportingPermission> GetUserReportingPermissions(string customerId, string userId, string permissionTypeId)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).GetUserReportingPermissions(icustomerId, UserID, int.Parse(userId), int.Parse(permissionTypeId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<UserReportingPermission>(ex);
            }
        }

        public SingleResult<User> GetCurrentUser()
        {
            try
            {
                return (new CustomerController()).GetUser(CustomerID, UserID, UserID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<User>(ex);
            }
        }

        public SingleResult<User> GetAccountExecutive()
        {
            try
            {
                return (new CustomerController()).GetAccountExecutive();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<User>(ex);
            }
        }

        public SingleResult<User> GetCustomerCareRep()
        {
            try
            {
                return (new CustomerController()).GetCustomerCareRep();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<User>(ex);
            }
        }

        public SingleResult<User> SaveUser(string customerId, string userId, UserSecurity user)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin) && !(new Symphony.Core.Data.Customer(user.CustomerID).AllowSelfAccountCreation))
                {
                    icustomerId = CustomerID;
                }
                
                return (new CustomerController()).SaveUser(icustomerId, UserID, int.Parse(userId), user);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<User>(ex);
            }
        }

        public SingleResult<bool> ValidateUser(string userId, string courseId, string trainingProgramId, string attempt, UserSecurity user)
        {
            try
            {
                int uid = int.Parse(userId);
                int cid = int.Parse(courseId);
                int tpid = int.Parse(trainingProgramId);
                int iattempt = int.Parse(attempt);
                return new SingleResult<bool>((new UserController()).ValidateUser(uid, cid, tpid, iattempt, user));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> PostTPValidateUser(string userId, string courseId, string trainingProgramId, string attempt, UserSecurity user)
        {
            try
            {
                int uid = int.Parse(userId);
                int cid = int.Parse(courseId);
                int tpid = int.Parse(trainingProgramId);
                int iattempt = int.Parse(attempt);

                var val = new UserController();
                var res = val.ValidateUser(uid, cid, tpid, iattempt, user);

                if (res == true)
                {
                    // save that the user has been validated
                    val.SavePostValidationState(uid, tpid, res);
                }

                return new SingleResult<bool>(res);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<Location> SaveLocation(string customerId, string locationId, Location location)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                bool reparent = false;
                if (HttpContext.Current.Request["reparent"] != null)
                {
                    reparent = bool.Parse(HttpContext.Current.Request["reparent"]);
                }
                return (new CustomerController()).SaveLocation(icustomerId, UserID, int.Parse(locationId), reparent, location);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Location>(ex);
            }
        }

        public SingleResult<JobRole> SaveJobRole(string customerId, string jobRoleId, JobRole jobRole)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                bool reparent = false;
                if (HttpContext.Current.Request["reparent"] != null)
                {
                    reparent = bool.Parse(HttpContext.Current.Request["reparent"]);
                }
                return (new CustomerController()).SaveJobRole(icustomerId, UserID, int.Parse(jobRoleId), reparent, jobRole);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<JobRole>(ex);
            }
        }

        public SingleResult<Audience> SaveAudience(string customerId, string audienceId, Audience audience)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                bool reparent = false;
                if (HttpContext.Current.Request["reparent"] != null)
                {
                    reparent = bool.Parse(HttpContext.Current.Request["reparent"]);
                }
                return (new CustomerController()).SaveAudience(icustomerId, UserID, int.Parse(audienceId), reparent, audience);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Audience>(ex);
            }
        }

        public SingleResult<JobRole> GetJobRole(string customerId, string jobRoleId)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).GetJobRole(icustomerId, UserID, int.Parse(jobRoleId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<JobRole>(ex);
            }
        }

        public SingleResult<Location> GetLocation(string customerId, string locationId)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).GetLocation(icustomerId, UserID, int.Parse(locationId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Location>(ex);
            }
        }

        public SingleResult<Audience> GetAudience(string customerId, string audienceId)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).GetAudience(icustomerId, UserID, int.Parse(audienceId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Audience>(ex);
            }
        }

        public SingleResult<bool> DeleteJobRole(string jobRoleId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteJobRole(CustomerID, UserID, int.Parse(jobRoleId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteLocation(string locationId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteLocation(CustomerID, UserID, int.Parse(locationId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteAudience(string audienceId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteAudience(CustomerID, UserID, int.Parse(audienceId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<User> ToggleAccountExec(string userId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    throw new Exception("You don't have permission to change account executive status.");
                }
                return (new CustomerController()).ToggleAccountExec(CustomerID, UserID, int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<User>(ex);
            }
        }

        public SingleResult<User> ToggleCustomerCare(string userId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    throw new Exception("You don't have permission to change customer care status.");
                }
                return (new CustomerController()).ToggleCustomerCare(CustomerID, UserID, int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<User>(ex);
            }
        }

        public PagedResult<GTMOrganizer> GetGTMOrganizers(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).FindGTMOrganizers(UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<GTMOrganizer>(ex);
            }
        }

        public SingleResult<GTMOrganizer> GetGTMOrganizer(string gtmOrganizerId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).GetGTMOrganizer(UserID, int.Parse(gtmOrganizerId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<GTMOrganizer>(ex);
            }
        }

        public SingleResult<bool> DeleteGTMOrganizer(string gtmOrganizerId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteGTMOrganizer(int.Parse(gtmOrganizerId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<GTMOrganizer> SaveGTMOrganizer(string gtmOrganizerId, GTMOrganizer gtmOrganizer)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).SaveGTMOrganizer(UserID, int.Parse(gtmOrganizerId), gtmOrganizer);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<GTMOrganizer>(ex);
            }
        }

        public SingleResult<AuthenticationResult> Login(string customer, string username, string password)
        {
            UserController controller = new UserController();
            AuthenticationResult loginResult = controller.Login(customer, username, password);
            return new SingleResult<AuthenticationResult>(loginResult);
        }

        


        public SingleResult<OnlineStatus> GetOnlineStatus(string userId)
        {
            try
            {
                int uid = int.Parse(userId);
                return (new UserController().GetOnlineStatus(uid));
            }
            catch (Exception e)
            {
                return new SingleResult<OnlineStatus>(e);
            }
        }

        public SingleResult<OnlineStatus> UpdateOnlineStatus(string userId, OnlineStatus onlineStatus)
        {
            try
            {
                int uid = int.Parse(userId);
                return (new UserController().UpdateOnlineStatus(uid, onlineStatus));
            }
            catch (Exception e)
            {
                return new SingleResult<OnlineStatus>(e);
            }
        }

        public PagedResult<ExternalSystem> GetExternalSystems(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CustomerController()).GetExternalSystems(searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception e)
            {
                return new PagedResult<ExternalSystem>(e);
            }
        }

        #region Applications
        public PagedResult<Application> GetApplications(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CustomerController()).FindApplications(UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Application>(ex);
            }
        }


        public SingleResult<Application> GetApplication(string applicationId)
        {
            try
            {
                return (new CustomerController()).GetApplication(UserID, int.Parse(applicationId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Application>(ex);
            }
        }

        public SingleResult<Application> SaveApplication(string applicationId, Application application)
        {
            try
            {
                return (new CustomerController()).SaveApplication(UserID, int.Parse(applicationId), application);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Application>(ex);
            }
        }

        public SingleResult<bool> DeleteApplication(string applicationId)
        {
            try
            {
                return (new CustomerController()).DeleteApplication(int.Parse(applicationId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> ResolveAppLink(string tileId)
        {
            return (new CustomerController()).ResolveAppLink(int.Parse(tileId));


            //   return new SingleResult<bool>(false); //Fully expect to redirect before we get here;
        }

        #endregion

        #region Tiles
        public PagedResult<Tile> GetTiles(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                return (new CustomerController()).FindTiles(UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Tile>(ex);
            }
        }


        public SingleResult<Tile> GetTile(string tileId)
        {
            try
            {
                return (new CustomerController()).GetTile(UserID, int.Parse(tileId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Tile>(ex);
            }
        }

        public SingleResult<Tile> SaveTile(string tileId, Tile tile)
        {
            try
            {
                return (new CustomerController()).SaveTile(UserID, int.Parse(tileId), tile);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Tile>(ex);
            }
        }
        public SingleResult<bool> DeleteTile(string tileId)
        {
            try
            {
                return (new CustomerController()).DeleteTile(int.Parse(tileId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }


        public PagedResult<Tile> GetTilesForAudience(string customerId, string audienceId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindTilesForAudience(icustomerId, int.Parse(audienceId), UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Tile>(ex);
            }
        }

        public PagedResult<Tile> GetTilesForJobRole(string customerId, string jobRoleId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindTilesForJobRole(icustomerId, int.Parse(jobRoleId), UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Tile>(ex);
            }
        }

        public PagedResult<Tile> GetTilesForLocation(string customerId, string locationId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                return (new CustomerController()).FindTilesForLocation(icustomerId, int.Parse(locationId), UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Tile>(ex);
            }
        }

        public PagedResult<Tile> GetTilesUnassigned(string hierarchyName, string hierarchyId, string customerId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                // the customer id is only used if the user is a sales channel admin
                int icustomerId = int.Parse(customerId);
                if (!(new SymphonyController().UserIsSalesChannelAdmin))
                {
                    icustomerId = CustomerID;
                }
                HierarchyType htype = (HierarchyType)Enum.Parse(typeof(HierarchyType), hierarchyName, true);


                return (new CustomerController()).FindUnassignedTiles(htype, int.Parse(hierarchyId), icustomerId, UserID, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<Tile>(ex);
            }
        }

        public SingleResult<bool> SaveLocationTiles(string locationId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).SaveLocationTiles(CustomerID, UserID, int.Parse(locationId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> SaveJobRoleTiles(string jobRoleId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).SaveJobRoleTiles(CustomerID, UserID, int.Parse(jobRoleId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> SaveAudienceTiles(string audienceId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).SaveAudienceTiles(CustomerID, UserID, int.Parse(audienceId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteLocationTiles(string locationId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteLocationTiles(CustomerID, UserID, int.Parse(locationId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteJobRoleTiles(string jobRoleId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteJobRoleTiles(CustomerID, UserID, int.Parse(jobRoleId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteAudienceTiles(string audienceId, int[] userIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteAudienceTiles(CustomerID, UserID, int.Parse(audienceId), userIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }


        public MultipleResult<UserTileAssignment> GetUserAssignedTilesForApp(string applicationId, string userId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).GetAssignedTiles(int.Parse(userId), int.Parse(applicationId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<UserTileAssignment>(ex);
            }
        }

        public MultipleResult<UserTileAssignment> GetUserUnassignedTiels(string userId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).GetUnassignedTiles(int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<UserTileAssignment>(ex);
            }
        }

        public MultipleResult<UserApplicationAssignment> GetAssignedApplications(string userId)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).GetAssignedApplications(int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<UserApplicationAssignment>(ex);
            }
        }

        public SingleResult<bool> SaveUserTiles(string userId, int[] tileIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).SaveUserTiles(CustomerID, UserID, int.Parse(userId), tileIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }

        public SingleResult<bool> DeleteUserTiles(string userId, int[] tileIds)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).DeleteUserTiles(CustomerID, UserID, int.Parse(userId), tileIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<bool>(ex);
            }
        }
        public SingleResult<Credentials> saveCredentials(string applicationId, string userId, string credentialId, Credentials credentials)
        {
            try
            {
                if (!HasRole(Roles.CustomerAdministrator))
                {
                    throw new Exception("Permission denied.");
                }
                return (new CustomerController()).SaveCredentials(CustomerID, UserID, int.Parse(applicationId), int.Parse(userId), int.Parse(credentialId), credentials);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<Credentials>(ex);
            }

        }

        public MultipleResult<AppLink> UserAppLinks(string userId)
        {
            try
            {
                return (new CustomerController()).GetUserAppLinks(CustomerID, UserID, int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<AppLink>(ex);
            }


        }

        public MultipleResult<AppGroup> UserAppGroups(string userId)
        {
            try
            {
                return (new CustomerController()).GetUserGroups(int.Parse(userId));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<AppGroup>(ex);
            }
        }
        public MultipleResult<AppLink> AllAppLinks()
        {
            try
            {

                return (new CustomerController()).GetAllApplinks();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new MultipleResult<AppLink>(ex);
            }
        }

        #endregion

        #region User Profile
        public PagedResult<UserDataField> GetUserDataFields(string userId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                int uId;
                if (!int.TryParse(userId, out uId))
                {
                    throw new Exception("User id must be an integer.");
                }

                return (new CustomerController()).GetUserDataFields(uId, searchText ?? "", sort, GetOrderDirection(dir), start / (limit == 0 ? 1 : limit), limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new PagedResult<UserDataField>(ex);
            }
        }
        public SingleResult<UserDataField> SaveUserDataField(string userDataFieldId, UserDataField userDataField)
        {
            try
            {
                int udfId;
                if (!int.TryParse(userDataFieldId, out udfId))
                {
                    throw new Exception("User data field id must be an integer.");
                }

                return (new CustomerController()).SaveUserDataField(udfId, userDataField);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SingleResult<UserDataField>(ex);
            }
        }
        public SimpleSingleResult<bool> DeleteUserDataField(string userDataFieldId)
        {
            try
            {
                int udfId;
                if (!int.TryParse(userDataFieldId, out udfId))
                {
                    throw new Exception("User data field id must be an integer.");
                }

                return (new CustomerController()).DeleteUserDataField(udfId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new SimpleSingleResult<bool>(ex);
            }
        }
        #endregion

        #region Themes

        /// <summary>
        /// Handles request to retrieve a set of themes matching query parameters.
        /// </summary>
        /// <param name="searchText">
        /// Query parameter that will be applied to the seach results.
        /// </param>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="sort"></param>
        /// <param name="dir"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public PagedResult<Theme> FindThemes(string searchText, int start, int limit, string sort, string dir, string filter)
        {
            try
            {
                Log.Info("Handling request to retrieve a set of themes.");

                List<GenericFilter> f = null;
                if (!String.IsNullOrEmpty(filter))
                {
                    f = Utilities.Deserialize<List<GenericFilter>>(filter);
                }

                string query = searchText ?? "";
                int page = start / (limit == 0 ? 1: limit);
                var sortDir = GetOrderDirection(dir);

                return Controller.FindThemes(query, sort, sortDir, page, limit, f);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling request to retrieve a set of themes.", ex);
                return new PagedResult<Theme>(ex);
            }
        }

        /// <summary>
        /// Handles request to get a single theme.
        /// </summary>
        /// <param name="themeId">
        /// Id of the theme to return.
        /// </param>
        /// <returns>
        /// Result set wrapping the theme.
        /// </returns>
        public SingleResult<Theme> GetTheme(string themeId)
        {
            try
            {
                Log.InfoFormat("Handling request to retrieve a theme with id {0}.", themeId);

                var id = int.Parse(themeId);

                return Controller.GetTheme(id);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling request to retrieve a theme with id " + themeId + ".", ex);
                return new SingleResult<Theme>(ex);
            }
        }

        /// <summary>
        /// Handles requests to create a theme.
        /// </summary>
        /// <param name="theme">
        /// Data to create the theme with.
        /// </param>
        /// <returns>
        /// Result set wrapping the new theme.
        /// </returns>
        public SingleResult<Theme> CreateTheme(Theme theme)
        {
            try
            {
                Log.Info("Handling request to create a theme.");

                return Controller.CreateTheme(theme);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling request to create a theme.", ex);
                return new SingleResult<Theme>(ex);
            }
        }

        /// <summary>
        /// Handles requests to update a theme.
        /// </summary>
        /// <param name="themeId">
        /// Id of the theme to update.
        /// </param>
        /// <param name="theme">
        /// Data to update the theme with.
        /// </param>
        /// <returns>
        /// Result set wrapping the updated theme.
        /// </returns>
        public SingleResult<Theme> UpdateTheme(string themeId, Theme theme)
        {
            try
            {
                Log.InfoFormat("Handling request to update theme with id {0}.", themeId);

                var id = int.Parse(themeId);
                if (id != theme.ID)
                {
                    throw new Exception("Query string id does not match theme id.");
                }

                return Controller.UpdateTheme(theme);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling request to update theme with id " + themeId + ".", ex);
                return new SingleResult<Theme>(ex);
            }
        }

        /// <summary>
        /// Handles requests to delete a theme.
        /// </summary>
        /// <param name="themeId">
        /// Id of the theme to delete.
        /// </param>
        /// <param name="theme">
        /// The theme to delete.
        /// </param>
        /// <returns>
        /// Result set wrapping the deleted theme.
        /// </returns>
        public SingleResult<Theme> DeleteTheme(string themeId, Theme theme)
        {
            try
            {
                Log.InfoFormat("Handling request to delete theme with id {0}.", themeId);

                var id = int.Parse(themeId);
                if (id != theme.ID)
                {
                    throw new Exception("Query string id does not match theme id.");
                }

                return Controller.DeleteTheme(theme);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while handling request to delete theme with id " + themeId + ".", ex);
                return new SingleResult<Theme>(ex);
            }
        }

        #endregion
    }
}
