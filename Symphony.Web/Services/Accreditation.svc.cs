﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using log4net;
using Symphony.Core.Controllers;
using Symphony.Core.Models;

namespace Symphony.Web.Services
{
    /// <summary>
    /// <see cref="AccreditationService"/> is a service class that defines an API for performing
    /// actions on objects that are part of the accreditation module.
    /// </summary>
#if DEBUG
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
#else
#endif
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class AccreditationService : BaseService, IAccreditationService
    {
        private AccreditationController Controller;
        private ILog Log;

        public AccreditationService()
        {
            Controller = new AccreditationController();
            Log = LogManager.GetLogger(typeof(AccreditationService));
        }

        /// <summary>
        /// Gets or sets the HttpStatusCode to return with the response.
        /// </summary>
        public HttpStatusCode StatusCode
        {
            get
            {
                return WebOperationContext.Current.OutgoingResponse.StatusCode;
            }
            set
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = value;
            }
        }

        /// <summary>
        /// Finds an AccreditationBoard that matches the provided id.
        /// </summary>
        /// <param name="id">Id of the AccreditationBoard to return.</param>
        /// <returns>The found accreditation board. Will return 404 and a result set that wraps a
        /// null value if there is no matching entitty.</returns>
        public SingleResult<AccreditationBoard> FindAccreditationBoard(string id)
        {
            try
            {
                Log.InfoFormat("Handling a request to find an accreditation board with id {0}.", id);

                int idValue = Convert.ToInt32(id);
                if (idValue <= 0)
                {
                    throw new ArgumentOutOfRangeException("id", "Accreditation board id must be a positive integer.");
                }

                var result = Controller.FindAccreditationBoard(idValue);
                if (result.Data == null)
                {
                    StatusCode = HttpStatusCode.NotFound;
                    result.Success = false;
                    result.Error = "Could not find the accreditation board.";

                    Log.Error("Could not find the accreditation board, returning 404.");
                }

                return result;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while finding the accreditation board.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<AccreditationBoard>(ex);
            }
        }

        /// <summary>
        /// Finds the set of accreditations associated with a specified accreditation board.
        /// </summary>
        /// <param name="searchText">Optional filter text to apply to the query.</param>
        /// <param name="start">Offsets the results by this amount.</param>
        /// <param name="limit">Maximum number of results to return.</param>
        /// <param name="sort">A parameter to sort on, matching one of an Accreditation's data members.</param>
        /// <param name="dir">The direction to sort on. One of 'ASC' or 'DESC'.</param>
        /// <returns>A result set wrapping all of the matching Accreditations.</returns>
        public PagedResult<ArtisanCourseAccreditation> QueryArtisanAccreditationsForAccreditationBoard(string id, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                Log.InfoFormat("Handling a request to query artisan accreditations for an accreditation board width id {0}.", id);

                int idValue = Convert.ToInt32(id);
                if (idValue <= 0)
                {
                    throw new ArgumentOutOfRangeException("id", "Accreditation board id must be a positive integer.");
                }

                return Controller.QueryArtisanCourseAccreditationsForAccreditationBoard(idValue, new PagedQueryParams<ArtisanCourseAccreditation>(Log, searchText, start, limit, sort, dir));
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while querying artisan accreditations for an accreditation board.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new PagedResult<ArtisanCourseAccreditation>(ex);
            }
        }

        /// <summary>
        /// Finds the set of training program accreditations associated with a specified accreditation board.
        /// </summary>
        /// <param name="searchText">Optional filter text to apply to the query.</param>
        /// <param name="start">Offsets the results by this amount.</param>
        /// <param name="limit">Maximum number of results to return.</param>
        /// <param name="sort">A parameter to sort on, matching one of an Accreditation's data members.</param>
        /// <param name="dir">The direction to sort on. One of 'ASC' or 'DESC'.</param>
        /// <returns>A result set wrapping all of the matching Accreditations.</returns>
        public PagedResult<TrainingProgramAccreditation> QueryTrainingProgramAccreditationsForAccreditationBoard(string id, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                Log.InfoFormat("Handling a request to query training program accreditations for an accreditation board width id {0}.", id);

                int idValue = Convert.ToInt32(id);
                if (idValue <= 0)
                {
                    throw new ArgumentOutOfRangeException("id", "Accreditation board id must be a positive integer.");
                }

                return Controller.QueryTrainingProgramAccreditationsForAccreditationBoard(idValue, new PagedQueryParams<TrainingProgramAccreditation>(Log, searchText, start, limit, sort, dir));
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while querying training program accreditations for an accreditation board.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new PagedResult<TrainingProgramAccreditation>(ex);
            }
        }

        /// <summary>
        /// Finds the set of accreditation boards that match the query parameters.
        /// </summary>
        /// <param name="searchText">Optional filter text to apply to the query. Applies to the
        /// name and primary contact of the accreditation board.</param>
        /// <param name="start">Offsets the results by this amount.</param>
        /// <param name="limit">Maximum number of results to return.</param>
        /// <param name="sort">A parameter to sort on, matching one of the AccreditationBoard's
        /// data members.</param>
        /// <param name="dir">The direction to sort on. One of 'ASC' or 'DESC'.</param>
        /// <returns>A result set wrapping all of the matching AccreditationBoards.</returns>
        public PagedResult<AccreditationBoard> QueryAccreditationBoards(string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                Log.Info("Handling a request to query accreditation boards.");

                return Controller.QueryAccreditationBoards(new PagedQueryParams<AccreditationBoard>(Log, searchText, start, limit, sort, dir));
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while querying accreditation boards.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new PagedResult<AccreditationBoard>(ex);
            }
        }

        /// <summary>
        /// Creates an accreditation board. See below for more information.
        /// </summary>
        /// <param name="model">Model data to create the accreditation board with.</param>
        /// <returns>The newly-created model, wrapped in a result set.</returns>
        public SingleResult<AccreditationBoard> CreateAccreditationBoardWithoutId(AccreditationBoard model)
        {
            return CreateAccreditationBoard(model);
        }

        /// <summary>
        /// Creates an accreditation board.
        /// </summary>
        /// <param name="id">Shim parameter, will always be 0.</param>
        /// <param name="model">Model data to create the accreditation board with.</param>
        /// <returns>The newly-created model, wrapped in a result set.</returns>
        public SingleResult<AccreditationBoard> CreateAccreditationBoardWithId(string id, AccreditationBoard model)
        {
            // TLDR; ext appends 0 to POST requests erroneously so we #deal like this.
            // https://www.sencha.com/forum/showthread.php?263395-4.2.0-Rest-proxy-appending-integer-IDs-to-URL-when-saving-new-Model-instance
            return CreateAccreditationBoard(model);
        }

        private SingleResult<AccreditationBoard> CreateAccreditationBoard(AccreditationBoard model)
        {
            try
            {
                Log.Info("Handling a request to create a new accreditation board.");

                return Controller.CreateAccreditationBoard(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while creating a new accreditation board.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<AccreditationBoard>(ex);
            }
        }

        /// <summary>
        /// Updates the accreditation board matching the specified id with the provided model data.
        /// </summary>
        /// <param name="id">Id of the accreditation board to update.</param>
        /// <param name="model">Model data to update the accreditation boardw with.</param>
        /// <returns></returns>
        public SingleResult<AccreditationBoard> UpdateAccreditationBoard(string id, AccreditationBoard model)
        {
            try
            {
                Log.Info("Handling a request to update an accreditation board.");

                return Controller.UpdateAccreditationBoard(model);

            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while updating an accreditation board.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new SingleResult<AccreditationBoard>(ex);
            }
        }

        /// <summary>
        /// Finds the set of available accreditation board surveys that match the query parameters.
        /// </summary>
        /// <param name="id">Id of the accreditation board to retrieve surveys for.</param>
        /// <param name="searchText">Optional filter text to apply to the query.</param>
        /// <param name="start">Offsets the results by this amount.</param>
        /// <param name="limit">Maximum number of results to return.</param>
        /// <param name="sort">A parameter to sort on, matching one of the course record's data members.</param>
        /// <param name="dir">The direction to sort on. One of 'ASC' or 'DESC'.</param>
        /// <returns>A result set wrapping all of the matching course records.</returns>
        public PagedResult<AccreditationBoardSurvey> QueryAvailableAccreditationBoardSurveys(string id, string searchText, int start, int limit, string sort, string dir, string exclude)
        {
            try
            {
                Log.InfoFormat("Handling a request to query surveys available to an accreditation board.", id);

                int idVal = Convert.ToInt32(id);
                if (idVal < 0)
                {
                    throw new ArgumentOutOfRangeException("id", "Accreditation board id must be a positive integer.");
                }

                var excludeIds = new List<int>();
                if (exclude != null)
                {
                    excludeIds = exclude.Split('.').Select(s => Convert.ToInt32(s)).ToList();
                }

                if (string.IsNullOrWhiteSpace(sort))
                {
                    sort = "name";
                    dir = "ASC";
                }

                return Controller.QueryAvailableAccreditationBoardSurveys(idVal, excludeIds, new PagedQueryParams<AccreditationBoardSurvey>(Log, searchText, start, limit, sort, dir));
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while querying surveys availble to an accreditation board.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new PagedResult<AccreditationBoardSurvey>(ex);
            }
        }

        /// <summary>
        /// Finds the set of accreditations tied to an Artisan course that match the query parameters.
        /// </summary>
        /// <param name="artisanCourseId">Id of the Artian course to return accreditatinos for.</param>
        /// <param name="searchText">Optional filter text to apply to the query. Currently doesn't
        /// apply to anything.</param>
        /// <param name="start">Offsets the results by this amount.</param>
        /// <param name="limit">Maximum number of results to return.</param>
        /// <param name="sort">A parameter to sort on, matching one of the ArtisanAccreditation's
        /// data members.</param>
        /// <param name="dir">The direction to sort on. One of 'ASC' or 'DESC'.</param>
        /// <returns>A result set wrapping all of the matching ArtisanAccreditation.</returns>
        public PagedResult<ArtisanCourseAccreditation> QueryArtisanAccreditations(string artisanCourseId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                Log.InfoFormat("Handling a request to query accreditations assigned to an Artisan course with id {0}.", artisanCourseId);

                int courseIdValue = Convert.ToInt32(artisanCourseId);
                if (courseIdValue <= 0)
                {
                    throw new ArgumentOutOfRangeException("artisanCourseId", "Artisan course id must be a positive integer.");
                }

                return Controller.QueryArtisanAccreditations(courseIdValue, new PagedQueryParams<ArtisanCourseAccreditation>(Log, searchText, start, limit, sort, dir));
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while querying accreditations assigned to an Artisan course.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new PagedResult<ArtisanCourseAccreditation>(ex);
            }
        }

        /// <summary>
        /// Finds the set of accreditations tied to an online course that match the query parameters.
        /// </summary>
        /// <param name="onlineCourseId">Id of the online course to return accreditatinos for.</param>
        /// <param name="searchText">Optional filter text to apply to the query. Currently doesn't
        /// apply to anything.</param>
        /// <param name="start">Offsets the results by this amount.</param>
        /// <param name="limit">Maximum number of results to return.</param>
        /// <param name="sort">A parameter to sort on, matching one of the ArtisanAccreditation's
        /// data members.</param>
        /// <param name="dir">The direction to sort on. One of 'ASC' or 'DESC'.</param>
        /// <returns>A result set wrapping all of the matching ArtisanAccreditation.</returns>
        public PagedResult<OnlineCourseAccreditation> QueryOnlineAccreditations(string onlineCourseId, string searchText, int start, int limit, string sort, string dir)
        {
            try
            {
                Log.InfoFormat("Handling a request to query accreditations assigned to an online course with id {0}.", onlineCourseId);

                int onlineCourseIdValue = Convert.ToInt32(onlineCourseId);
                if (onlineCourseIdValue <= 0)
                {
                    throw new ArgumentOutOfRangeException("onlineCourseId", "Online course id must be a positive integer.");
                }

                return Controller.QueryOnlineAccreditations(onlineCourseIdValue, new PagedQueryParams<OnlineCourseAccreditation>(Log, searchText, start, limit, sort, dir));
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while querying accreditations assigned to an online course.", ex);

                StatusCode = HttpStatusCode.InternalServerError;
                return new PagedResult<OnlineCourseAccreditation>(ex);
            }
        }

        public SingleResult<OnlineCourseAccreditation> DeleteAccreditationBoard(string id)
        {
            SingleResult<OnlineCourseAccreditation> result;
            try
            {
                int abId = int.Parse(id);
                result = Controller.DeleteAccreditationBoard(abId);
            }
            catch (Exception ex)
            {
                result = new SingleResult<OnlineCourseAccreditation>(ex);
            }
            return result;
        }
    }
}
