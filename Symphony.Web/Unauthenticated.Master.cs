﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Symphony.Core;

namespace Symphony.Web
{
    public partial class site : System.Web.UI.MasterPage
    {
        static string EnumJson { get; set; }

        static site()
        {
            EnumJson = EnumManager.SerializeToJson();
        }

        protected override void OnInit(EventArgs e)
        {
            this.enumJson.Text = "Symphony = {}; " + EnumJson;


            SymphonyMaster.AddExtTheme(this.Header, Login.GetCustomer());

            string[] filenames = new string[] { 
                "/skins/symphony_login/" + Login.GetCustomer() + ".css",
                "/skins/symphony_main/" + Login.GetCustomer() + "/login.css"
            };
            foreach (string filename in filenames)
            {
                Literal link = new Literal();

                if (File.Exists(Server.MapPath("~" + filename)))
                {
                    link.Text = "<link href=\"" + filename + "\" rel=\"stylesheet\" type=\"text/css\" />";
                    this.Header.Controls.Add(link);
                }
            }

            SymphonyMaster.AddQuerySkin(this.Header);

            base.OnInit(e);
        }
    }
}
