﻿//var SymphonyMobile = SymphonyMobile || {};

(function () {
    //var activeCourses = [];
    //var courseCompletionCallbacks = [];
    var dateRegex = /\/Date\(-?(\d+[\-|\+]?\d{0,4})\)\//;

    SymphonyMobile.helpers = {
        parseDate: function (value, addLocalOffset) {
            if (!value) { return value; }
            var ticksAndZone = value.match(dateRegex)[1];
            var delim = ticksAndZone.indexOf('-') > -1 ? '-' : '+';
            var parts = ticksAndZone.split(delim);
            var ticks = parseInt(parts[0], 10);
            var dt = new Date(ticks);
            if (addLocalOffset) {
                var offset = dt.getTimezoneOffset();
                dt = dt.add(Date.MINUTE, offset);
            }
            return dt;
        },

        formatUsDate: function (date) {
            if (!date) { return 'N/A'; }
            return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
        },

        getCustomerFromUrl: function () {
            var url = document.location.href;
            var idx = url.lastIndexOf("/") + 1;
            var customer = url.substring(idx, url.length);
            return customer.replace(/[^A-Za-z0-9]/g, '');
        },

        getCertificateUrl: function (o, type) {
            var domain = window.location.host;
            // type can be "onlinecourse", "classroomcourse" or "trainingprogram"
            var path = 'resources/certificates/' + type + '.html?';
            var params = [];
            for (var prop in o) {
                params.push(prop + '=' + o[prop]);
            }
            params.push('customer=' + this.getCustomerFromUrl());
            var url = path + params.join('&');
            return url;
        },

        getLaunchUrl: function (userId, courseId, trainingProgramId) {
            var urlTemplate = SymphonyMobile.global.launchPageBase + "&registration=UserId|{UserID}!CourseId|{CourseID}!TrainingProgramId|{TrainingProgramID}";
            var url = urlTemplate
                .replace("{UserID}", userId)
                .replace("{CourseID}", courseId)
                .replace("{TrainingProgramID}", trainingProgramId || 0);
            return url;
        },

        launchOnlineCourse: function (userId, courseId, trainingProgramId) {
            Ext.Viewport.setMasked({
                xtype: 'loadmask',
                message: 'Your course is currently in progress. Close the course window to re-active this screen.'
            });

            var launchUrl = this.getLaunchUrl(userId, courseId, trainingProgramId);
            var courseWindow = window.open(launchUrl, 'Online Course');
            var interval = window.setInterval(function () {
                if (courseWindow.closed) {
                    Ext.Viewport.setMasked(false);
                    window.clearInterval(interval);
                }
            }, 500);

        }

    };

} ());
