﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Symphony.Web.mobile.index" %>

<!DOCTYPE HTML>
<html manifest="" lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Symphony Mobile</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="resources/icons/icon-114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="resources/icons/icon-72.png" />
    <link rel="apple-touch-icon-precomposed" href="resources/icons/icon-57.png" />
    <link rel="apple-touch-icon" href="resources/icons/icon-50.png" />
    <style type="text/css">
        html, body {
            height: 100%;
            background-color: #1985D0
        }

        #appLoadingIndicator {
            position: absolute;
            top: 50%;
            margin-top: -15px;
            text-align: center;
            width: 100%;
            height: 30px;
            -webkit-animation-name: appLoadingIndicator;
            -webkit-animation-duration: 0.5s;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-direction: linear;
        }

        #appLoadingIndicator > * {
            background-color: #FFFFFF;
            display: inline-block;
            height: 30px;
            -webkit-border-radius: 15px;
            margin: 0 5px;
            width: 30px;
            opacity: 0.8;
        }

        @-webkit-keyframes appLoadingIndicator{
            0% {
                opacity: 0.8
            }
            50% {
                opacity: 0
            }
            100% {
                opacity: 0.8
            }
        }
        
        /*.x-toolbar-lighter {
            background-image: none;
            background-color: #1468A2;
            background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #3EA3F4), color-stop(3%, #3686C9), color-stop(100%, #31699C))  !important;
            background-image: -webkit-linear-gradient(top, #3EA3F4, #3686C9 3%, #31699C) !important;
            background-image: linear-gradient(top, #3EA3F4, #3686C9 3%, #31699C) !important;
        }*/
    </style>

    <script type="text/javascript">
        var SymphonyMobile = SymphonyMobile || {};
        SymphonyMobile.global = {}; // necessary for sencha to build
    </script>
    
	<script type="text/javascript">
        // also necessary for sencha; has to be a different script block
        <asp:Literal id="litGlobal"  runat="server"/>
    </script>
    <!-- The line below must be kept intact for Sencha Command to build your application -->
    <script id="microloader" type="text/javascript" src="sdk/microloader/development.js"></script>
</head>
<body>
    <div id="appLoadingIndicator">
        <div></div>
        <div></div>
        <div></div>
    </div>
</body>
</html>
