Ext.define('SymphonyMobile.view.TrainingFileList', {
    extend: 'Ext.List',
    xtype: 'trainingfilelist',
    config: {
        Title: 'Files',
        padding: 0,
        itemTpl: '<a class="lnkFile" href="/services/portal.svc/trainingprogramfiles/download/{id}" target="_blank">{filename}</a> ({description}) <span style="float:right">{createdOn:date("m/d/Y")}</span>',
        files: null,
        store: {
            model: 'SymphonyMobile.model.TrainingFile'
        }
    },

    initialize: function () {
        this.callParent();

        this.getStore().setData(this.getFiles());
    }
});