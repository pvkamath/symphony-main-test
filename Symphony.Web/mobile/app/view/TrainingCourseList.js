Ext.define('SymphonyMobile.view.TrainingCourseList', {
    extend: 'Ext.List',
    xtype: 'trainingcourselist',

    config: {
        title: 'Courses',
        itemTpl: '{name}',
        emptyText: '&nbsp; No course available',
        courses: null,
        store: {
            model: 'SymphonyMobile.model.TrainingCourse'
        },
        onItemDisclosure: true
    },

    initialize: function () {
        this.callParent();

        var courses = this.getCourses();
        this.getStore().setData(courses);
    }
});