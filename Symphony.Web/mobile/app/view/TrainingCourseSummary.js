Ext.define('SymphonyMobile.view.TrainingCourseSummary', {
    extend: 'Ext.Panel',
    xtype: 'trainingcoursesummary',

    requires: [
         'Ext.LoadMask'
    ],

    config: {
        title: 'Training Course Summary',
        layout: 'fit',
        scrollable: 'vertical',
        styleHtmlContent: true,
        html: '<div class="summary"></div>',
        course: null,
        items: [{
            xtype: 'toolbar',
            docked: 'bottom',
            layout: 'fit',
            style: 'padding: 5px',
            items: [{
                text: 'Launch',
                itemId: 'launch',
                //ui: 'confirm',
                disabled: true,
                scope: this
            }]
        }]
    },

    initialize: function () {
        this.callParent();

        var launchId = Ext.id();

        var course = this.getCourse();
        var courseType = course.courseTypeId == 1 ? 'classroomcourse' : 'onlinecourse';
        var certInfo = {
            userName: SymphonyMobile.global.userFullName,
            name: course.name,
            score: course.score,
            date: SymphonyMobile.helpers.formatUsDate(course.endDate)
        };
        var certUrl = SymphonyMobile.helpers.getCertificateUrl(certInfo, courseType);
        var certHtml = '<img src="/images/bullet_white.png" alt="Not complete" />';
        if (course.passed) {
            certHtml = '<a href="' + certUrl + '" target="_blank"><img src="/images/medal_gold_3.png" ext:qtip="View Certificate" alt="View Certificate" title="View Certificate" /></a>';
        }

        var optionHtml = "";
        if (course.courseTypeId == 2) {
            optionHtml = "<h3>Options</h3><div>";
            if (course.retries != null && course.attemptCount >= course.retries) {
                optionHtml += 'Unavailable (Max attempts reached)';
            }
            else if (course.passed && course.retestMode == 1) { // "1" -> RetestMode.onlyOnFailure
                optionHtml += 'Completed (You have passed this course, and it does\'t allow retries.)';
            }
            else {
                var launcher = this.down('#launch');
                
                if (window.navigator.standalone) {
                    launcher.hide();
                    var url = SymphonyMobile.helpers.getLaunchUrl(SymphonyMobile.global.userId, course.id, SymphonyMobile.global.currentProgramId);
                    optionHtml += '<a id="" href="' + url + '">Launch</a>';
                } else {
                    launcher.setDisabled(false);
                    launcher.setHandler(function () {
                        SymphonyMobile.helpers.launchOnlineCourse(SymphonyMobile.global.userId, course.id, SymphonyMobile.global.currentProgramId);
                    });
                    optionHtml += '<a id="" href="javascript:SymphonyMobile.helpers.launchOnlineCourse('
                        + SymphonyMobile.global.userId + ',' + course.id + ',' + SymphonyMobile.global.currentProgramId + ')">Launch</a>';
                }
            }
            optionHtml += "</div>";
        }

        optionHtml += '<div id="' + launchId + '"></div>';

        if (!course.dueDate || course.dueDate.getFullYear() == 2039 || course.dueDate.getFullYear() == 2040) {
            course.dueDate = 'Not Set';
        } else {
            course.dueDate = SymphonyMobile.helpers.formatUsDate(course.dueDate);
        }

        if (!course.score) {
            course.score = "N/A";
        }

        var tpl = new Ext.XTemplate(
            '<div class="summary">',
              '<h3 style="padding-top:0">Course Name</h3>',
              '<div>{name}</div>',
              optionHtml,
              '<h3>Due Date</h3>',
              '<div>{dueDate}</div>',
              '<h3>Score</h3>',
              '<div>{score}', certHtml, '</div>',
            '</div>');

        /*var tpl = new Ext.XTemplate(
        '<table class="tabOverview">',
        '<tr><td class="lbl">Name </td><td>{name}</td></tr>',
        '<tr><td class="lbl">Description </td><td>{description}</td></tr>',
        '<tr><td class="lbl">Cost </td><td>{cost}</td></tr>',
        '<tr><td class="lbl">Start Date </td><td>{startDate}</td></tr>',
        '<tr><td class="lbl">End Date </td><td>{endDate}</td></tr>',
        '<tr><td class="lbl">Objective </td><td>{courseObjective}</td></tr>',
        '<tr><td class="lbl">Credit </td><td>{credit}</td></tr>',
        '<tr><td class="lbl">Attempt Date </td><td>{attemptDate)}</td></tr>',
        '<tr><td class="lbl">Attempt Count </td><td>{attemptCount}</td></tr>',
        '<tr><td class="lbl">Score </td><td>{score}</td></tr>',
        '</table>');*/
        this.setHtml(tpl.apply(course));
    }
});
