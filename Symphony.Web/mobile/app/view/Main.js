Ext.define("SymphonyMobile.view.Main", {
    extend: 'Ext.navigation.View',
    xtype: 'mainpanel',
    
    requires: [
        'SymphonyMobile.view.TrainingProgramList',
        'SymphonyMobile.view.TrainingProgramDetail',
        'Ext.TitleBar'
    ],
    
    config: {
        tabBarPosition: 'bottom',

        navigationBar: {
            items: [
                {
                    xtype: 'button',
                    text: 'Logout',
                    name: 'logout',
                    align: 'right'
                }
            ]
        },

        items: [{
            xtype: 'trainingprogramlist'
        }]
    }
});
