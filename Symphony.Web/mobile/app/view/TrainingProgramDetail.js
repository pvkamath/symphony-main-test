Ext.define('SymphonyMobile.view.TrainingProgramDetail', {
    extend: 'Ext.TabPanel',
    xtype: 'trainingprogramdetail',

    config: {
        title: 'Training Program Detail',
        fullscreen: true,
        tabBarPosition: 'bottom',
        defaults: {
            styleHtmlContent: true
        },
        programDetail: null
    },

    initialize: function () {
        this.callParent();

        var programDetail = this.getProgramDetail(); //programDetail is coming from raw data, so we have to parse it's dates
        programDetail.startDate = SymphonyMobile.helpers.parseDate(programDetail.startDate);
        programDetail.endDate = SymphonyMobile.helpers.parseDate(programDetail.endDate);
        programDetail.dueDate = SymphonyMobile.helpers.parseDate(programDetail.dueDate);

        this.setItems([
            {
                xtype: 'panel',
                title: 'Courses',
                layout: 'fit',
                iconCls: 'compose',
                items: [{
                    xtype: 'trainingcoursegroup',
                    title: 'Courses',                    
                    programDetail: programDetail
                }]
            },
            {
                xtype: 'trainingfilelist',               
                title: 'Files',
                iconCls: 'organize', 
                files: programDetail.files
            },
            {
                xtype: 'trainingprogramsummary',                 
                title: 'Summary',
                iconCls: 'more',
                programDetail: programDetail
            }
        ]);

    }
});
