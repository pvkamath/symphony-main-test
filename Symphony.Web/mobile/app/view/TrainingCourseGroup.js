Ext.define('SymphonyMobile.view.TrainingCourseGroup', {
    extend: 'Ext.Panel',
    alias: 'widget.trainingcoursegroup',

    requires: [
        'SymphonyMobile.layout.Accordion'
    ],

    config: {
        title: 'Courses',
        padding: 0,
        layout: {
            type: 'accordion',
            mode: 'SINGLE'
        },
        defaults: {
            collapsed: true,
            layout: 'fit'
        },
        programDetail: null       
    },

    initialize: function () {
        this.callParent();

        var programDetail = this.getProgramDetail();
        
        this.setItems([
            {
                title: 'Required Courses',
                flex: 1,
                collapsed: false,
                items: [
                    {
                        xtype: 'trainingcourselist',
                        courses: programDetail.requiredCourses
                    }
                ]
            },
            {
                title: 'Elective Courses',
                items: [
                    {
                        xtype: 'trainingcourselist',
                        courses: programDetail.electiveCourses
                    }
                ]
            },
            {
                title: 'Optional Courses',
                items: [
                    {
                        xtype: 'trainingcourselist',
                        courses: programDetail.optionalCourses
                    }
                ]
            },
            {
                title: 'Final Accessment',
                items: [
                    {
                        xtype: 'trainingcourselist',
                        courses: programDetail.finalAccessment
                    }
                ]
            }
        ]);
    }
});
