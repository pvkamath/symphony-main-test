Ext.define('SymphonyMobile.view.TrainingProgramList', {
    extend: 'Ext.List',
    xtype: 'trainingprogramlist',
    config: {
        title: 'Training Programs',
        itemTpl: '{name} ({courseCount} courses)',
        emptyText: '<div style="padding:1em; text-align:center">No Training Programs Available</div>',
        store: 'TrainingPrograms',
        onItemDisclosure: true
    }
});
