﻿Ext.define("SymphonyMobile.view.Login", {
    extend: 'Ext.form.Panel',
    xtype: 'loginpanel',
    requires: [
        'Ext.form.*',
        'Ext.field.*',
        'Ext.Button'
    ],
    config: {
        items: [{
            xtype: 'fieldset',
            title: 'Symphony Login',
            //instructions: 'Please enter the information above and tap Login.',
            defaults: {
                required: true,
                labelAlign: 'left',
                labelWidth: '40%'
            },
            items: [{
                xtype: 'textfield',
                name: 'userName',
                label: 'Name',
                autoCapitalize: false
            }, {
                xtype: 'passwordfield',
                name: 'password',
                label: 'Password'
            }]
        }, {
            xtype: 'button',
            text: 'Login',
            ui: 'confirm',
            scope: this
        }]
    }
});
