Ext.define('SymphonyMobile.view.TrainingProgramSummary', {
    extend: 'Ext.Panel',
    xtype: 'trainingprogramsummary',
    config: {
        title: 'Training Program Summary',
        //styleHtmlContent: false,
        layout: 'fit',
        scrollable: 'vertical',
        padding: 0,
        programDetail: null,
        html: ''
    },

    initialize: function () {
        this.callParent();

        var me = this;
        var programDetail = this.getProgramDetail();

        var dueDate = programDetail.dueDate;
        if (!dueDate || dueDate.getFullYear() == 2039 || dueDate.getFullYear() == 2040) {
            dueDate = programDetail.endDate;
        }

        var certInfo = {
            userName: SymphonyMobile.global.userFullName,
            name: programDetail.name,
            date: SymphonyMobile.helpers.formatUsDate(programDetail.endDate)
        };
        var certUrl = SymphonyMobile.helpers.getCertificateUrl(certInfo, 'trainingprogram');

        var templateInfo = {
            description: programDetail.description ? programDetail.description : 'No description provided',
            dueDate: dueDate,
            requiredCount: programDetail.requiredCourses.length,
            enforceOrder: programDetail.enforceOrder,
            minimumElectives: programDetail.minimumElectives,
            electiveCount: programDetail.electiveCourses.length,
            optionalCount: programDetail.optionalCourses.length,
            assessmentSet: programDetail.finalAssessments.length > 0 ? 'Set' : 'Not Set',
            certificate: programDetail.requiredCourses.length || programDetail.electiveCourses.length ?
                    (
                        programDetail.completed ?
                        '<a href="' + certUrl + '" target="_blank" id="training_program_cert_' + programDetail.id + '"><img src="/images/medal_gold_3.png" alt="View certificate" /></a>' :
                        'Not complete'
                    ) : '' // no courses required = no certificate
        };
        var tpl = new Ext.XTemplate(
            '<div class="summary">',
                '<div>',
                    '<h3 style="padding-top:0">Description</h3>',
                    '<div>{description}</div>',
                '</div>',
                '<div>',
                    '<h3>Due Date</h3>',
                    '<div>{dueDate:date("m/d/Y")}</div>',
                '</div>',
                '<div>',
                    '<h3>Required Courses</h3>',
                    '<div><span id="requiredCount">{requiredCount}</span></div>',
                    '<div class="order">',
                    '    <span id="enforceOrder">{enforceOrder}</span>',
                    '</div>',
                '</div>',
                '<div>',
                    '<h3>Elective Courses</h3>',
                    '<div><span>Take</span> <span id="electiveMin">{minimumElectives}</span> <span>of</span> <span class="count">{electiveCount}</span></div>',
                '</div>',
                '<div>',
                    '<h3>Optional Courses</h3>',
                    '<div><span class="count">{optionalCount}</span></div>',
                '</div>',
                '<div>',
                    '<h3>Assessment</h3>',
                    '<div><span class="set">{assessmentSet}</span></div>',
                '</div>',
                '<div id="certContainer">',
                    '<h3>Certificate</h3>',
                    '<div><span class="cert">{certificate}</span></div>',
                '</div>',
            '</div>');
        /*var tpl = new Ext.XTemplate(
        '<table class="tabOverview">',
        '<tr><td class="lbl">Name </td><td>{name}</td></tr>',
        '<tr><td class="lbl">Description </td><td>{description}</td></tr>',
        '<tr><td class="lbl">Category </td><td>{categoryName}</td></tr>',
        '<tr><td class="lbl">Owner </td><td>{ownerName}</td></tr>',
        '<tr><td class="lbl">Courses </td><td>{courseCount}</td></tr>',
        '<tr><td class="lbl">Cost </td><td>{cost}</td></tr>',
        '<tr><td class="lbl">Start Date </td><td>{startDate:date("m/d/Y")}</td></tr>',
        '<tr><td class="lbl">End Date </td><td>{endDate:date("m/d/Y")}</td></tr>',
        '</table>');*/
        me.setHtml(tpl.apply(templateInfo));
    }
});
