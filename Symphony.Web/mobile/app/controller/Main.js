﻿Ext.define('SymphonyMobile.controller.Main', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            main: 'mainpanel'
        },
        control: {
            'trainingprogramlist': {
                itemsingletap: 'showDetail'
            },
            'button[name=logout]': {
                tap: 'doLogout'
            }
        }
    },

    showDetail: function (list, index, target, record) {
        SymphonyMobile.global.currentProgramId = record.data.id;
        this.getMain().push({
            xtype: 'trainingprogramdetail',
            title: record.data.name,
            programDetail: record.raw
        });
        return false;
    },

    doLogout: function () {
        var mask = new Ext.LoadMask(Ext.getBody(), { msg: "Logging out..." });
        mask.show();

        Ext.Ajax.request({
            url: '/handlers/logoutHandler.ashx',
            success: function (response) {
                if (response.responseText == "OK") {
                    window.location.reload();
                }
                else {
                    Ext.Msg.alert('Error', 'Unable to logout');
                    mask.hide();
                }
            },
            failure: function () {
                Ext.Msg.alert('Error', 'Unable to logout');
                mask.hide();
            }
        });
    }
});
