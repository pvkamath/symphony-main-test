﻿Ext.define('SymphonyMobile.controller.TrainingCourseList', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            main:'mainpanel'
        },
        control: {
            'trainingcourselist': {
                itemsingletap: 'showDetail'
            }
        }
    },

    showDetail: function (list, index, target, record) {
        this.getMain().push({
            xtype: 'trainingcoursesummary',
            title: record.data.name,
            data: record.data,
            course: record.data
        });
    }
});
