﻿Ext.define('SymphonyMobile.controller.Login', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            loginPanel: 'loginpanel'
        },
        control: {
            'loginpanel button': {
                tap: 'doLogin'
            }
        }
    },

    doLogin: function () {
        var me = this;
        var formValues = this.getLoginPanel().getValues();
        if (!formValues.userName) {
            Ext.Msg.alert('Error', 'Please type in your user name.');
            return;
        }
        if (!formValues.password) {
            Ext.Msg.alert('Error', 'Please type in your password.');
            return;
        }

        var mask = new Ext.LoadMask(Ext.getBody(), { msg: "Please wait..." });
        mask.show();

        var customer = SymphonyMobile.helpers.getCustomerFromUrl();

        Ext.Ajax.request({
            url: '/handlers/loginhandler.ashx',
            params: { userName: formValues.userName, password: formValues.password, customer: customer },
            success: function (response) {
                if (response.responseText == "OK") {
                    window.location.reload();
                }
                else {
                    Ext.Msg.alert('Error', response.responseText);
                    mask.hide();
                }
            },
            failure: function () {
                Ext.Msg.alert('Error', 'Bad username or password');
                mask.hide();
            }
        });
    }
});
