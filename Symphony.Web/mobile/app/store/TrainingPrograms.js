﻿Ext.define('SymphonyMobile.store.TrainingPrograms', {
    extend: 'Ext.data.Store',

    config: {
        model: 'SymphonyMobile.model.TrainingProgram',
        //data: [{ name: 'Dummy Program', courseCount: 4 }, { name: 'Test Program', courseCount: 2}],
        proxy: {
            type: 'ajax',
            url: '/services/portal.svc/trainingprograms/',
            reader: {
                type: 'json',
                rootProperty: 'data'
            }
        },
        autoLoad: true
    }
    
});
