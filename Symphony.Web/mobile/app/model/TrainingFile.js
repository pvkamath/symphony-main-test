﻿Ext.define('SymphonyMobile.model.TrainingFile', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
                { name: 'id', type:'int' },
                { name: 'filename', type:'string' },
                { name: 'title', type:'string' },
                { name: 'description', type: 'string' },
                { name: 'createdOn', type: 'string', convert: function (value, record) { return SymphonyMobile.helpers.parseDate(value) } }
            ]
    }
});
