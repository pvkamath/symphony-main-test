﻿Ext.define('SymphonyMobile.model.TrainingProgram', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name: 'name', type: 'string' },
            { name: 'courseCount', type: 'int' },
            { name: 'categoryId', type: 'int' },
            { name: 'categoryName', type: 'string' },
            { name: 'completed', type: 'boolean' },
            { name: 'cost', type: 'float' },
            { name: 'courseCount', type: 'int' },
            { name: 'dateCompleted', type: 'string', convert: function (value, record) { return SymphonyMobile.helpers.parseDate(value) } },
            { name: 'description', type: 'string' },
            { name: 'dueDate', type: 'string', convert: function (value, record) { return SymphonyMobile.helpers.parseDate(value) } },
            { name: 'duplicateFromId', type: 'int' },
            //{ name: 'electiveCourses', type: 'string' },
            { name: 'endDate', type: 'string', convert: function (value, record) { return SymphonyMobile.helpers.parseDate(value) } },
            { name: 'enforceRequiredOrder', type: 'boolean' },
            //{ name: 'files', type: 'string' },
            //{ name: 'finalAssessments', type: 'string' },
            { name: 'id', type: 'int' },
            { name: 'internalCode', type: 'string' },
            { name: 'isLive', type: 'boolean' },
            { name: 'isNewHire', type: 'boolean' },
            { name: 'minimumElectives', type: 'int' },
            { name: 'name', type: 'string' },
            //{ name: 'optionalCourses', type: 'string' },
            { name: 'ownerId', type: 'int' },
            { name: 'ownerName', type: 'string' },
            { name: 'primaryAssignment', type: 'int' },
            //{ name: 'requiredCourses', type: 'string' },
            { name: 'secondaryAssignment', type: 'string' },
            { name: 'startDate', type: 'string', convert: function (value, record) { return SymphonyMobile.helpers.parseDate(value) } }
        ]
        /*associations: [
            { type: 'hasMany', model: 'SymphonyMobile.model.TrainingCourse', name: 'requiredCourses' },
            { type: 'hasMany', model: 'SymphonyMobile.model.TrainingCourse', name: 'electiveCourses' },
            { type: 'hasMany', model: 'SymphonyMobile.model.TrainingCourse', name: 'optionalCourses' },
            { type: 'hasMany', model: 'SymphonyMobile.model.TrainingCourse', name: 'finalAssessments' },
            { type: 'hasMany', model: 'SymphonyMobile.model.TrainingFile', name: 'files' }
        ]*/
    }
});

/*
{ name: 'categoryId', type:'int' },
{ name: 'categoryName', type:'string' },
{ name: 'completed', type:'boolean' },
{ name: 'cost', type: 'float' },
{ name: 'courseCount', type:'int' },
{ name: 'dateCompleted', type:'string' },
{ name: 'description', type:'string' },
{ name: 'dueDate', type:'string'  },
{ name: 'duplicateFromId', type:'int' },
{ name: 'electiveCourses', type:'string' }, 
{ name: 'endDate', type:'string' },
{ name: 'enforceRequiredOrder', type:'boolean' },
{ name: 'files', type:'string' }, 
{ name: 'finalAssessments', type:'string' },
{ name: 'id', type:'int' },
{ name: 'internalCode', type:'string' },
{ name: 'isLive', type:'boolean' },
{ name: 'isNewHire', type:'boolean' },
{ name: 'minimumElectives', type:'int' }, 
{ name: 'name', type:'string' }, 
{ name: 'optionalCourses', type:'string' }, 
{ name: 'ownerId', type:'int' },
{ name: 'ownerName', type:'string' },
{ name: 'primaryAssignment', type:'int' },
{ name: 'requiredCourses', type:'string' },
{ name: 'secondaryAssignment', type: 'string' },
{ name: 'startDate', type: 'string' }
*/