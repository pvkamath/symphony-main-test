﻿Ext.define('SymphonyMobile.model.TrainingCourse', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
                { name: 'name' },
                { name: 'description' },
                { name: 'courseTypeId' },
                { name: 'id' },
                { name: 'attemptCount' },
                { name: 'attemptDate', convert: function (value, record) { return SymphonyMobile.helpers.parseDate(value) } },
                { name: 'categoryId' },
                { name: 'categoryName' },
                { name: 'cost' },
                { name: 'courseCompletionTypeDescription' },
                { name: 'courseObjective' },
                { name: 'createdOn' },
                { name: 'credit' },
                { name: 'description' },
                { name: 'dailyDuration' },
                { name: 'endDate', convert: function (value, record) { return SymphonyMobile.helpers.parseDate(value) } },
                { name: 'numberOfDays' },
                { name: 'perStudentFee' },
                { name: 'retries' },
                { name: 'retestMode' },
                { name: 'score' },
                { name: 'passed' },
                { name: 'startDate', convert: function (value, record) { return SymphonyMobile.helpers.parseDate(value) } },
                { name: 'dueDate', convert: function (value, record) { return SymphonyMobile.helpers.parseDate(value) } }
            ]
    }
});


/*{"courseTypeId":1,
"id":18,
"attemptCount":0,
"attemptDate":"\/Date(1280286000000-0700)\/",
"attendedIndicator":true,
"categoryId":1,
"categoryName":null,
"classId":175,
"contactEmailOverride":null,
"cost":0,
"courseCompletionTypeDescription":null,
"courseCompletionTypeId":1,
"courseObjective":"To teach about.....",
"createdOn":"\/Date(1232495252153-0800)\/",
"credit":0.00,
"dailyDuration":480,
"description":"The Basics of BankersEdge New LMS",
"disclaimerOverride":null,
"endDate":"\/Date(1280286000000-0700)\/",
"hasClasses":false,
"hasEnded":true,
"hasStarted":true,
"internalCode":"1",
"isStartingSoon":true,
"isTest":false,
"keywords":null,
"modifiedOn":"\/Date(1281306475777-0700)\/",
"name":"Introduction to Symphony",
"navigationMethodOverride":null,
"numberOfDays":1,
"onlineCourseId":0,
"onlineCourseName":null,
"passed":1,
"passingScoreOverride":null,
"perStudentFee":0.00,
"posttestTypeOverride":null,
"presentationTypeDescription":null,
"presentationTypeId":1,
"pretestTestOutOverride":null,
"pretestTypeOverride":null,
"publicIndicator":false,
"registrationId":3509,
"registrationStatusId":4,
"retestMode":0,
"retries":null,
"score":"100",
"showObjectivesOverride":null,
"showPosttestOverride":null,
"showPretestOverride":null,
"skinOverride":null,
"startDate":"\/Date(1280286000000-0700)\/",
"theme":null,
"version":0,
"webinarKey":""
,"webinarLaunchUrl":"https:\/\/www.gotomeeting.com\/join\/\/",
"webinarRegistrationId":"",
"dueDate":"\/Date(-2208988800000)\/",
"sortOrder":0,
"syllabusTypeId":1}*/