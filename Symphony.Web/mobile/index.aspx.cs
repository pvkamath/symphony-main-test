﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Symphony.Core.Controllers;
using System.Configuration;
using Symphony.Core;

namespace Symphony.Web.mobile
{
    public partial class index : System.Web.UI.Page
    {
        SymphonyController controller = new SymphonyController();

        protected void Page_Load(object sender, EventArgs e)
        {
            string customer = Request.QueryString["customer"] ?? string.Empty;
            if (string.IsNullOrEmpty(customer) && Context.Request.Cookies["customer"] != null)
            {
                customer = Context.Request.Cookies["customer"].Value;
                Response.Redirect("/mobile/" + customer);
            }
            
            bool token = false;
            string userId = string.Empty;
            string fullName = string.Empty;
            string launchPageBase = string.Empty;
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                token = true;
                userId = controller.UserID.ToString();
                fullName = controller.UserFullName.Replace(@"""", "'");
            }
            launchPageBase = ConfigurationManager.AppSettings["LaunchPageUrl"];


            string globalText = string.Format(@"SymphonyMobile.global = {{
                token : {0},
                userId: {1},
                userFullName: {2},
                launchPageBase: {3}, 
                currentProgramId: 0  //TODO: shouldn't use a global variable for this
            }};", Utilities.Serialize(token), Utilities.Serialize(userId), Utilities.Serialize(fullName), Utilities.Serialize(launchPageBase));
            this.litGlobal.Text = globalText;

        }

    }
}