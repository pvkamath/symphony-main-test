Ext.application({
    name: 'SymphonyMobile',

    requires: [
        'Ext.MessageBox'
    ],

    controllers: ['Login', 'Main', 'TrainingCourseList'],
    views: ['Login', 'Main', 'TrainingCourseGroup', 'TrainingCourseList', 'TrainingFileList', 'TrainingProgramSummary', 'TrainingCourseSummary'],
    stores: ['TrainingPrograms'],
    models: ['TrainingFile', 'TrainingCourse', 'TrainingProgram'],

    icon: {
        '57': 'resources/icons/icon-57.png',
        '72': 'resources/icons/icon-72.png',
        '114': 'resources/icons/icon-114.png',
        '144': 'resources/icons/icon-144.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function () {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        // Initialize the main view
        //Ext.Viewport.add(Ext.create('SymphonyMobile.view.Main'));
        if (SymphonyMobile.global.token) {
            Ext.Viewport.add({
                xclass: 'SymphonyMobile.view.Main'
            });
        }
        else {
            Ext.Viewport.add({
                xclass: 'SymphonyMobile.view.Login'
            });
        }
    },

    onUpdated: function () {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function (buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
