﻿using Symphony.Core.Models;
using Symphony.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Symphony.ExternalSystemIntegration.Controllers;
using Symphony.Core.Controllers;
using Data = Symphony.Core.Data;
using SubSonic;
using log4net;

namespace Symphony.Web
{
    public partial class MultiLogin : System.Web.UI.Page
    {
        protected string userDetails = "";
        protected string customerDetails = "";
        protected string userLoginSystem = "";

        private ILog Log = LogManager.GetLogger(typeof(MultiLogin));

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current == null || HttpContext.Current.Items == null || HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] == null)
            {
                Response.Redirect("/login.aspx" + Skins.BuildQuerySkin("?"));
            }

            List<LoginSystem> currentLoginSystems = HttpContext.Current.Session["UserLoginSystem"] as List<LoginSystem>;
            ExternalLoginController externalLoginController = new ExternalLoginController();

            Data.Customer customer = null;
            User user = null;

            if (this.Context.User.Identity.IsAuthenticated)
            {
                UserController userController = new UserController();

                user = userController.GetUser(userController.UserID);
                customer = new Data.Customer(user.CustomerID);

                // Do this first if we are authenticated so we don't have to go through all the additional login processing
                // Just double check that the user being logged is actually linked to this account
                // *** NOTE*** this is temporary as we are using the JanrainID - this will become the MasterUserId and 
                // we will need to ensure the master user id matches up with the other user. 
                if (!string.IsNullOrEmpty(Request.QueryString["n"]) && !string.IsNullOrEmpty(Request.QueryString["d"]))
                {
                    string newSubDomain = Request.QueryString["d"];
                    string newUser = Request.QueryString["n"];
                    bool allowLogin = false;

                    if (user.Username != newUser || customer.SubDomain != newSubDomain)
                    { // Redirecting to a different symphony account that is connected to the account that initially logged in
                        User matchingUser = Select.AllColumnsFrom<Data.User>()
                            .InnerJoin(Data.Customer.IdColumn, Data.User.CustomerIDColumn)
                            .Where(Data.Customer.SubDomainColumn).IsEqualTo(newSubDomain)
                            .And(Data.User.UsernameColumn).IsEqualTo(newUser)
                            .ExecuteSingle<User>();

                        if (matchingUser != null && matchingUser.ID > 0 && matchingUser.JanrainUserUuid == user.JanrainUserUuid)
                        {
                            allowLogin = true;
                        }
                    }
                    else // Redirecting to the exact user that initially tried to log in
                    {
                        allowLogin = true;
                    }

                    if (allowLogin)
                    {
                        // Force login of a user 
                        // Skip updating the external systems during this login so we can
                        // retain the original set of external systems when the user returns
                        // to the multi login page.
                        userController.Login(newSubDomain, newUser, true, true);
                        Response.Redirect("/home/" + newSubDomain + Skins.BuildQuerySkin("?"));
                    }
                    else
                    {
                        Log.Error("User attempted to log in to symphony using an account that is not connected to them. UserID: " + user.ID + " Username: " + user.Username + " tried to log in as " + newUser + " in the customer " + newSubDomain + ".");
                        // We can carry on after this and render the rest of the multi login page
                    }
                }


                // retrieve one more time: UserLoginSystem information.
                // this ensures we have their *current* login info
                
                if (currentLoginSystems == null)
                {
                    user.LoginSystems = currentLoginSystems;
                    externalLoginController.UpdateLoginSystems(customer, user, null, true);
                    currentLoginSystems = HttpContext.Current.Session["UserLoginSystem"] as List<LoginSystem>;
                }
                else
                {
                    user.LoginSystems = currentLoginSystems;
                }

                // we should, regardless of the "current" login systems, always re-check symphony for coursework

                externalLoginController.RefreshSymphonyLoginSystems(customer, user, null, true);

                userDetails = Symphony.Core.Utilities.Serialize(user);
                Customer customerModel = new Customer();
                customerModel.CopyFrom(customer);
                customerDetails = Symphony.Core.Utilities.Serialize(customerModel);
            }


            if (currentLoginSystems == null && !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/login.aspx" + Skins.BuildQuerySkin("?"));
            }
            else if (externalLoginController.CheckForSymphonyRedirect(user))
            {
                if (customer != null && customer.Id > 0 && user != null && user.ID > 0)
                {
                    Response.Redirect("/home/" + customer.SubDomain + Skins.BuildQuerySkin("?"));
                }
                else
                {
                    Response.Redirect("/login.aspx" + Skins.BuildQuerySkin("?"));
                }
            }
            
            userLoginSystem = Symphony.Core.Utilities.Serialize(currentLoginSystems.Where(c => c.IsDisplayed).ToList());
        }
    }
}