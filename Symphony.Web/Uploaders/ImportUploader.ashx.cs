﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;
using Symphony.Core;

namespace Symphony.Web.HttpHandlers
{
    public class ImportUploader : FileUploader
    {
        public override object Save(string filename, string title, string description, HttpPostedFile file)
        {
            CustomerController cc = new CustomerController();
            if (cc.UserID != 0 && cc.CustomerID != 0 && (cc.HasRole(Roles.CustomerManager) || cc.HasRole(Roles.CustomerAdministrator)))
            {
                string customer = HttpContext.Current.Request.Form["customerId"];
                string preview = HttpContext.Current.Request.Form["preview"];
                bool bpreview = false;
                bool.TryParse(preview, out bpreview);
                
                int icustomerId = cc.CustomerID;
                if (!string.IsNullOrEmpty(customer) && cc.UserIsSalesChannelAdmin)
                {
                    // non-sales-admins always are forced to work within their own customer
                    icustomerId = int.Parse(customer);
                }

                byte[] contents = new byte[file.ContentLength];
                file.InputStream.Read(contents, 0, file.ContentLength);
                cc.BeginImport(int.Parse(HttpContext.Current.Request.Form["importType"]), cc.CustomerID, cc.UserID, contents, !bpreview);
                return 0;
            }
            else
            {
                throw new Exception("Permission to upload has been denied. You do not have the proper role.");
            }
            
        }
    }
}
