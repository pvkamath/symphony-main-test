﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;

namespace Symphony.Web.HttpHandlers
{
    public class ArtisanAssetUploader : FileUploader
    {
        public override object Save(string filename, string name, string description, HttpPostedFile file)
        {
            int customerId = int.Parse(HttpContext.Current.Request.Form["customerId"]);

            SingleResult<ArtisanAsset> asset = (new ArtisanController()).UploadArtisanAsset(customerId, filename, name, description, file);

            if (asset.Data != null)
            {
                return asset.Data.Id;
            }
            else
            {
                throw new Exception(asset.Error);
            }
        }
    }
}
