﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;

namespace Symphony.Web.HttpHandlers
{
    public class TrainingProgramFileUploader : FileUploader
    {
        public override object Save(string filename, string title, string description, HttpPostedFile file)
        {
            int trainingProgramId = int.Parse(HttpContext.Current.Request.Form["trainingProgramId"]);
            TrainingProgramFile tpFile = (new CourseAssignmentController()).UploadTrainingProgramFile(trainingProgramId, filename, title, description, file);
            return tpFile.Id;
        }
    }
}
