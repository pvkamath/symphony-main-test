﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;

namespace Symphony.Web.HttpHandlers
{
    public class CertificateAssetsUploader : FileUploader
    {
        public override object Save(string filename, string name, string description, HttpPostedFile file)
        {
            var asset = (new CertificateController()).UploadAssets(filename, name, file);
            return asset.Id;
        }
    }
}
