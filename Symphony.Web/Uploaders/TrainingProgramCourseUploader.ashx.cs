﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;

namespace Symphony.Web.HttpHandlers
{
    public class TrainingProgramCourseUploader : FileUploader
    {
        public override object Save(string filename, string title, string description, HttpPostedFile file)
        {
            int courseId = int.Parse(HttpContext.Current.Request.Form["courseId"]);
            int customerId = int.Parse(HttpContext.Current.Request.Form["customerId"]);
            int userId = int.Parse(HttpContext.Current.Request.Form["userId"]);
            CourseAssignmentController controller = new CourseAssignmentController();
            Course course = controller.ImportCourse(customerId, userId, courseId, file);
            //OnlineCourse tpFile = (new PortalController()).UploadTrainingProgramFile(trainingProgramId, filename, title, description, file);
            //return tpFile.Id > 0;
            return course.Id;
        }
    }
}
