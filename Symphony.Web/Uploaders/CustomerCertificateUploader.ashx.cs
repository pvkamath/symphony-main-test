﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;

namespace Symphony.Web.HttpHandlers
{
    public class CustomerCertificateUploader : FileUploader
    {
        public override object Save(string filename, string title, string description, HttpPostedFile file)
        {
            int customerId = int.Parse(HttpContext.Current.Request.Form["customerId"]);

            string certName = new CustomerController().SaveCustomerCertificate(customerId, file);

            return certName;
        }
    }
}
