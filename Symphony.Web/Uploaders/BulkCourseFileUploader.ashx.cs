﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Symphony.Core.Controllers;
using Symphony.Core.Models;
using System.Web.SessionState;

namespace Symphony.Web.HttpHandlers
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class BulkCourseFileUploader : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "text/plain";
            try
            {
                CourseAssignmentController controller = new CourseAssignmentController();
                List<ImportAttempt<Course>> results = controller.ImportCourses(controller.CustomerID, controller.UserID, context.Request.Files[0]);

                context.Response.Write(FileUploader.Serialize(results));
                context.Response.Flush();
            }
            catch (Exception ex)
            {
                //Log.Error(ex);
                context.Response.Write("{\"error\":" + FileUploader.Serialize(ex.Message) + ",\"success\":false}");
                context.Response.Flush();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
