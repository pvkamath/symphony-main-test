﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using log4net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.SessionState;

namespace Symphony.Web.HttpHandlers
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class FileUploader : IHttpHandler, IRequiresSessionState
    {
        ILog Log = LogManager.GetLogger(typeof(FileUploader));

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                string filename = Path.GetFileName(context.Request.Files[0].FileName);


                //TODO: pull out the title and description...need to discuss w/ Brian to determine proper approach here 
                object result = Save(filename, context.Request.Form["title"], context.Request.Form["description"], context.Request.Files[0]);

                if (result.GetType() == typeof(int))
                {
                    // most of the time, we just respond with an int indicating the id of the item uploaded
                    context.Response.Write("{\"error\":null,\"success\":true, \"id\":" + (int)result + "}");
                }
                else if (result.GetType() == typeof(string))
                {
                    context.Response.Write("{\"error\":null,\"success\":true, \"value\":\"" + result + "\"}");
                }
                else
                {
                    // however, for advanced use cases, allow serializing of full objects
                    //context.Response.Write("{\"error\":null,\"success\":true, " + Serialize(result)  + "}");
                    context.Response.Write(Serialize(result));
                }
                context.Response.Flush();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                context.Response.Write("{\"error\":" + Serialize(ex.Message) + ",\"success\":false}");
                context.Response.Flush();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public virtual object Save(string filename, string title, string description, HttpPostedFile file)
        {
            return 0;
        }

        public static string Serialize(object o)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(o.GetType());
                serializer.WriteObject(ms, o);
                byte[] bytes = ms.ToArray();
                return Encoding.UTF8.GetString(bytes, 0, bytes.Length);
            }
        }
    }
}
