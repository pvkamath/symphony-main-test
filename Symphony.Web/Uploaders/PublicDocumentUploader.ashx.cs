﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;

namespace Symphony.Web.HttpHandlers
{
    public class PublicDocumentUploader : FileUploader
    {
        public override object Save(string filename, string title, string description, HttpPostedFile file)
        {
            int publicDocumentId = int.Parse(HttpContext.Current.Request.Form["publicDocumentId"]);
            int customerId = int.Parse(HttpContext.Current.Request.Form["customerId"]);
            int userId = int.Parse(HttpContext.Current.Request.Form["userId"]);

            PublicDocument document = new CourseAssignmentController().ImportPublicDocument(customerId, userId, publicDocumentId, file);
            return document.Id;
        }
    }
}
