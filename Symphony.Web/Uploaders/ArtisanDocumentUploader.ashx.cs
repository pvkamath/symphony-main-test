﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;

namespace Symphony.Web.HttpHandlers
{
    public class ArtisanDocumentUploader : FileUploader
    {
        public override object Save(string filename, string name, string description, HttpPostedFile file)
        {
            int tempVal; // used for parsing nullable ints

            int customerId = int.Parse(HttpContext.Current.Request["customerId"]);
            int sectionId = int.Parse(HttpContext.Current.Request.Form["sectionId"]);
            int courseId = int.Parse(HttpContext.Current.Request.Form["courseId"]);
            int? width = Int32.TryParse(HttpContext.Current.Request.Form["width"], out tempVal) ? tempVal : (int?)null;
            int? height = Int32.TryParse(HttpContext.Current.Request.Form["height"], out tempVal) ? tempVal : (int?)null;
            string scaleType = HttpContext.Current.Request.Form["scaleType"];
            int? cropX = Int32.TryParse(HttpContext.Current.Request.Form["cropX"], out tempVal) ? tempVal : (int?)null;
            int? cropY = Int32.TryParse(HttpContext.Current.Request.Form["cropY"], out tempVal) ? tempVal : (int?)null;
                
            return (new ArtisanController()).ImportDocument(customerId, filename, file, courseId, sectionId, width, height, scaleType, cropX, cropY);
        }
    }
}

