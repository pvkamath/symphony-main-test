﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;

namespace Symphony.Web.HttpHandlers
{
    public class CourseFileUploader : FileUploader
    {
        public override object Save(string filename, string title, string description, HttpPostedFile file)
        {
            int courseId = int.Parse(HttpContext.Current.Request.Form["courseId"]);
            int customerId = int.Parse(HttpContext.Current.Request.Form["customerId"]);
            CourseFile tpFile = (new ClassroomController()).UploadCourseFile(customerId, courseId, filename, title, description, file);
            return tpFile.Id;
        }
    }
}
