﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;

namespace Symphony.Web.HttpHandlers
{
    public class LicenseAssignmentUploader : FileUploader
    {
        public override object Save(string filename, string title, string description, HttpPostedFile file)
        {
            int licenseAssignmentId = int.Parse(HttpContext.Current.Request.Form["licenseAssignmentId"]);

            LicenseAssignmentDocumentInfo docInfo = new LicenseController().SaveAssignmentDocument(licenseAssignmentId, file);
            return docInfo.ID;
        }
    }
}
