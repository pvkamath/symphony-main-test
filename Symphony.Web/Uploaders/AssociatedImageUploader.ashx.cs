﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;
using Symphony.Core;

namespace Symphony.Web.HttpHandlers
{
    public class AssociatedImageUploader : FileUploader
    {
        public override object Save(string filename, string title, string description, HttpPostedFile file)
        {
            CustomerController cc = new CustomerController();
            if (cc.UserID != 0 && cc.CustomerID != 0)
            {
                int customerId = cc.CustomerID;
                string association = HttpContext.Current.Request.QueryString["association"];
                string associationId = HttpContext.Current.Request.QueryString["associationId"];
                                
                return new AssociatedImageController().SaveAssociatedImage(customerId, association, int.Parse(associationId), file.InputStream);
            }
            else
            {
                throw new Exception("Permission to upload has been denied. You do not have the proper role.");
            }
            
        }
    }
}
