﻿<%@ Page Title="Symphony Login" Language="C#" MasterPageFile="~/Unauthenticated.Master" AutoEventWireup="true" CodeBehind="SelectExternalSystem.aspx.cs" Inherits="Symphony.Web.SelectExternalLogin" %>
<asp:Content ID="Header" ContentPlaceHolderID="Header" runat="server">
    <link type="text/css" rel="Stylesheet" href="/skins/_default.css" />
    <script type="text/javascript">
        var dummyLoginSystems = [
            {
                id: 10,
                systemName: 'Symphony',
                userEmail: 'test@test.com',
                courses: [
                    { name: 'Test Course 1' },
                    { name: 'Test Course 2' },
                    { name: 'Test Course 3' }
                ]
            }, {
                id: 20,
                systemName: 'Pro Schools',
                userEmail: 'test@test.com',
                courses: [
                    { name: 'Test Course 4' },
                    { name: 'Test Course 5' },
                    { name: 'Test Course 6' }
                ]
            }, {
                id: 30,
                systemName: 'Training Pro',
                userEmail: 'test@test.com',
                courses: [
                    { name: 'Test Course 7' },
                    { name: 'Test Course 8' },
                    { name: 'Test Course 9' }
                ]
            }, {
                id: 40,
                systemName: 'Career Web Schools',
                userEmail: 'test@test.com',
                courses: [
                    { name: 'Test Course 6' },
                    { name: 'Test Course 7' },
                    { name: 'Test Course 8' }
                ]
            }
        ];


        LoginSelector = {
            
            init: function () {
                var container = new Ext.Viewport({
                    layout: {
                        type: 'vbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [{
                        layout: 'fit',
                        height: 300,
                        width: 400,
                        title: 'Select your coursework',
                        border: true,
                        frame: true,
                        items: [{
                            region: 'center',
                            xtype: 'panel',
                            ref: '../inner',
                            border: true,
                            layout: 'accordion',
                            animate: true,
                            fill: true
                        }],
                        renderTo: Ext.getBody()
                    }]
                });

                var loginSystems = LoginSelector.getExternalSystems();

                for (var i = 0; i < loginSystems.length; i++) {
                    var grid = LoginSelector.getExternalSystemGrid(loginSystems[i]);
                    container.inner.add(grid);
                }
                container.doLayout();
            },
            getExternalSystems: function() {
                return dummyLoginSystems;
            },
            getExternalSystemGrid: function (externalSystem) {
                var externalSystemCode = externalSystem.systemName.replace(' ', '').toLowerCase(),
                    courseData = externalSystem.courses,
                    data = [];

                for (var i = 0; i < courseData.length; i++) {
                    data.push([courseData[i].name]);
                }

                var store = new Ext.data.SimpleStore({
                    fields: ['name'],
                    data: data
                });

                return new Ext.grid.GridPanel({
                    title: String.format('{0} ({1} Course{2})',
                        externalSystem.systemName,
                        courseData.length,
                        (courseData.length == 1 ? '' : 's')),
                    tools: [{
                        id: 'right',
                        qtip: 'Launch ' + externalSystem.systemName,
                        handler: function (e, toolEl, panel, tc) {
                            LoginSelector.launch(externalSystem);
                        }
                    }],
                    bbar: {
                        items: ['->', {
                            text: 'Launch ' + externalSystem.systemName,
                            iconCls: 'x-button-play',
                            xtype: 'button',
                            handler: function () {
                                LoginSelector.launch(externalSystem);
                            }
                        }]
                    },
                    store: store,
                    border: true,
                    autoExpandColumn: 'name',
                    recordDef: Ext.data.Record.create([
                        { name: 'name' }
                    ]),
                    columns: [
                        { header: 'Courses', dataIndex: 'name', id: 'name' }
                    ]
                });
            },
            launch: function (externalSystem) {
                // Put launch code here!
                Ext.Msg.alert('Launching', 'Launch ' + externalSystem.systemName + ' not yet implemented!');
            }
        }

        Ext.onReady(function () {
            Ext.QuickTips.init();
            LoginSelector.init();
        })
        
    </script>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="Body" runat="server">    

</asp:Content>



