﻿<%@ Page Title="Symphony Login" Language="C#" MasterPageFile="~/Unauthenticated.Master" AutoEventWireup="true" CodeBehind="MultiLogin.aspx.cs" Inherits="Symphony.Web.MultiLogin" %>
<asp:Content ID="Header" ContentPlaceHolderID="Header" runat="server">
    <link type="text/css" rel="Stylesheet" href="/skins/_default.css" />
    <style type="text/css">
        .course-grid-container {
            display: none;
        }
        #page_logo {
            visibility: hidden;
        }
        .centered {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            var loginSystemCodeNameMap = {};
            var loginSystemFormContainer = $('#externalForms');
            for(var i = 0; i < LoginSystems.length; i++) {
                loginSystemCodeNameMap[LoginSystems[i].systemCodeName] = LoginSystems[i];
                loginSystemFormContainer.append(LoginSystems[i].loginForm);
            }

            var p1 = qs["p"];

            if (loginSystemCodeNameMap[p1]) {
                LoginSelector.launch(loginSystemCodeNameMap[p1], true);
            }

            if (LoginSystems[0].isAutoNavigate || LoginSystems.length == 1) { // Checking length just in case autonavigate wasn't set if there is only one, always autonav
                LoginSelector.launch(LoginSystems[0]);
            } else if (LoginSystems.length > 1) { // Display each system
                $('.progress').hide();
                $('.course-grid-container').css('display', 'block');
                $('#page_logo').css('visibility', 'visible');
            } else { // No systems, show error
                $('.progress').hide();
                $('#page_logo').show();
                $('#multi_login_error').show();
                
                var errorDetails = {
                    allowNavigateToProSchools: alNPr,
                    allowNavigateToTrainingPro: alNTp,
                    customerExternalSystemCount: cES,
                    queryString: qs,
                    windowLocation: window.location,
                    externalSystems: LoginSelector.getExternalSystems(),
                    userDetails: UserDetails,
                    customerDetails: CustomerDetails,
                    landingPageProschools: LndPgPr
                }

                var errorDetailString = JSON.stringify(errorDetails, null, "    ");

                $('#multi_login_error .error_details pre').html(errorDetailString);

                if (!UserDetails && CustomerDetails) {
                    $('#multi_login_error .user_info').html(UserDetails.firstName + ' ' + UserDetails.lastName + ' (' + UserDetails.email + ')');
                    $('#multi_login_error h1').html('Welcome to ' + CustomerDetails.name);
                    $('#multi_login_error a').attr('href', '/home/' + CustomerDetails.subdomain);
                } else {
                    $('#multi_login_error .warning_msg').hide();
                    $('#multi_login_error .error_msg').show();
                }
            }

        });

        var qs = (function(a) {
            if (a == "") return {};
            var b = {};
            for (var i = 0; i < a.length; ++i)
            {
                var p=a[i].split('=', 2);
                if (p.length == 1)
                    b[p[0]] = "";
                else
                    b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
            }
            return b;
        })(window.location.search.substr(1).split('&'));

        LoginSelector = {
            codeNameCounts:{},
            init: function () {
                var loginSystems = LoginSelector.getExternalSystems();

                for (var i = 0; i < loginSystems.length; i++) {
                    var grid = LoginSelector.getExternalSystemGrid(loginSystems[i]);
                }
            },
            getExternalSystems: function () {
                return LoginSystems;
            },
            getCodeName: function(externalSystem) {
                var codeName = externalSystem.systemCodeName + (externalSystem.customerName ? externalSystem.customerName.replace(' ', '').toLowerCase() : '');

                if (LoginSelector.codeNameCounts[codeName]) {
                    LoginSelector.codeNameCounts[codeName]++;
                } else {
                    LoginSelector.codeNameCounts[codeName] = 1;
                }

                if (LoginSelector.codeNameCounts[codeName] > 1) {
                    codeName += "-"+LoginSelector.codeNameCounts[codeName];
                }

                return codeName;
            },
            getExternalSystemGrid: function (externalSystem) {
                var externalSystemCode = externalSystem.domCodeName,
                    courseData = externalSystem.trainingPrograms,
                    data = [];
                
                for (var i = 0; i < courseData.length; i++) {
                    data.push([courseData[i].name, courseData[i].isActive]);
                }

                var store = new Ext.data.SimpleStore({
                    fields: ['name', 'isActive'],
                    data: data
                });

                var headerTemplate = "<span class='header'><span class='system'>{0}</span><span class='customer'>{1}</span></span><span class='name'>{2}</span><span class='courses'>{3}</span>";

                var html = String.format(headerTemplate,
                            externalSystem.systemName,
                            externalSystem.customerName ? ' - ' + externalSystem.customerName : '',
                            externalSystem.userFullName,
                            courseData.length == 1 ? courseData.length + ' Course' : courseData.length + ' Courses');


                return new Ext.Panel({
                    layout: 'border',
                    renderTo: externalSystemCode,
                    width: 600,
                    height: 200,
                    items: [{
                        xtype: 'panel',
                        region: 'north',
                        layout: 'border',
                        border: false,
                        height: 70,
                        cls: 'x-panel-header',
                        items: [{
                            xtype: 'panel',
                            region: 'center',
                            border: false,
                            layout: 'border',
                            style: 'padding: 3px',
                            
                            items: [{
                                region: 'center',
                                xtype: 'panel',
                                html: html,
                                layout: 'fit',
                                border: false
                            }, {
                                region: 'east',
                                xtype: 'button',
                                text: 'Launch',
                                width: 120,
                                border: false,
                                cls: 'x-button-big',
                                iconCls: 'x-button-play-big',
                                scale: 'large',
                                handler: function () {
                                    LoginSelector.launch(externalSystem);
                                }
                            }]
                        }]
                    }, {
                        xtype: 'grid',
                        region: 'center',
                        store: store,
                        border: false,
                        autoExpandColumn: 'name',
                        viewConfig: {
                            autoFill: true,
                            scrollOffset: 20
                        },
                        recordDef: Ext.data.Record.create([
                            { name: 'name', name: 'isActive' }
                        ]),
                        columns: [
                            { header: 'Courses', dataIndex: 'name', id: 'name', flex: 1 }
                        ].concat(
                            externalSystem.isSymphony ?
                            [{
                                header: 'Active', 
                                dataIndex: 'isActive',
                                align: 'center',
                                renderer: function(value, meta, record) {
                                    if (value) {
                                        return '<img src="/images/tick.png"/>';
                                    }
                                    return '<img src="/images/bullet_white.png"/>';
                                }
                            }] : []
                        )
                    }]
                });
            },
            launch: function (externalSystem, isIFrameLaunch) {
                switch(externalSystem.formType) {
                    case Symphony.ExternalSystemFormType.eval:
                        eval(externalSystem.loginForm);
                        break;
                    case Symphony.ExternalSystemFormType.submit:
                        $('#' + externalSystem.formId).submit();
                        break;
                    case Symphony.ExternalSystemFormType.iFrame:
                        if (isIFrameLaunch) {
                            $('#' + externalSystem.formId).submit();
                        } else {
                            $('#' + externalSystem.frameId).load(function() { location.href = externalSystem.landingPage });
                            $('#' + externalSystem.frameId).attr('src', '/MultiLogin.aspx?p=' + externalSystem.systemCodeName);
                        }
                        break;
                }
            }
        }

        Ext.onReady(function () {
            var loginSystems = LoginSelector.getExternalSystems();
            for (var i = 0; i < loginSystems.length; i++) {
                loginSystems[i].domCodeName = LoginSelector.getCodeName(loginSystems[i]);

                var courseGrid = $('<div/>').addClass('course-grid').attr('id', loginSystems[i].domCodeName)

                if (loginSystems[i].isSymphony) {
                    courseGrid.addClass('isSymphony');
                }

                $('.course-grid-container').append(courseGrid);
            }
            
            var symphonyGrids = $('.isSymphony');
            symphonyGrids.remove();
            var first = $('.course-grid:first');
            if (first && first.length > 0) {
                first.before(symphonyGrids);
            } else {
                $('.course-grid-container').append(symphonyGrids);
            }

            Ext.QuickTips.init();
            LoginSelector.init();


        })

    </script>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="Body" runat="server">
    <script type="text/javascript">
        var UserDetails = <%= userDetails %>;
        var CustomerDetails = <%= customerDetails %>;
        var LoginSystems = <%= userLoginSystem %>;

    </script>
    <img src="/scripts/tiny_mce/themes/advanced/skins/default/img/loaderMultiLogin.gif" alt="Please wait..." class="centered progress" />
    
    <div class="course-grid-container multilogin">
        <h1 class="title">Welcome to Symphony</h1>
        <h2>Please select your course work:</h2>
    </div>
    <div id="multi_login_error" class="multilogin" style="display: none">
        <h1 class="title">Welcome to Symphony</h1>
        <div class="warning_msg">
            <p>
            <br />
            You are currently logged in as <span class="user_info"></span> however, you have no training assigned to you. If this is unexpected please contact an administrator, or try logging in again. 
            <br /><br />
            <a href="/home/unkown" class="button">Proceed to Symphony</a>
            </p>
        </div>
        <div class="error_msg" style="display: none">
            <p>
                <br />
                There is an error with your account. Please contact an administrator to resolve.
            </p>

        </div>
        <div class="error_details" style="display: none">
            <pre>

            </pre>
        </div>
    </div>
    <div id="externalForms" style="display: none">

    </div>
</asp:Content>



