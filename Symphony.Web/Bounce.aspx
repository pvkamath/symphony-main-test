﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<% 	
    if (!string.IsNullOrEmpty(Page.Request["Redirect"]))
    {
        Page.Response.SetCookie(new HttpCookie("symphony-bounce", "true"));
        Page.Response.Redirect(Page.Request["Redirect"]);
    }
%>