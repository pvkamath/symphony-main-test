﻿<%@ Page Title="Welcome to Symphony" MasterPageFile="~/SymphonyMaster.Master" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Symphony.Web._Default" %>
<%@ MasterType  virtualPath="~/SymphonyMaster.Master"%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="server">
    <script type="text/javascript">
        var createCookie = function (name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else var expires = "";
            document.cookie = name + "=" + value + expires + "; path=/";
        }
        if (!window.Symphony.User.id) {
            createCookie('history_token', window.location.hash.substring(1), 1);
            if (window.location.href.indexOf('home') > -1) {
                window.location.href = window.location.href.replace('home', 'login');
            } else {
                window.location.href = '/Login.aspx';
            }
        }
    </script>
    <link href="/scripts/video-js/video-js.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/scripts/symphony/portal.js?v=<%# this.Master.AssemblyVersion %>"></script>
    <script type="text/javascript" src="/scripts/video-js/video.js?v=<%# this.Master.AssemblyVersion %>"></script>
</asp:Content>
