﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Symphony.Core.Controllers;
using System.IO;
using log4net;
using Symphony.Core.CNRService;
using Symphony.Core;
using System.Runtime.Serialization;
using Symphony.Web.HttpHandlers;
using Data = Symphony.Core.Data;
using SubSonic;
using System.Xml;
using System.Text;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace Symphony.Web
{
    public partial class TemplateEditor : System.Web.UI.Page
    {
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                this.litError.Text = string.Empty;

                if (!this.IsPostBack)
                {
                    List<Data.ArtisanTemplate> templates = Select.AllColumnsFrom<Data.ArtisanTemplate>().ExecuteTypedList<Data.ArtisanTemplate>();
                    this.ddlTemplates.DataSource = templates;
                    this.ddlTemplates.DataTextField = Data.ArtisanTemplate.Columns.Name;
                    this.ddlTemplates.DataValueField = Data.ArtisanTemplate.Columns.Id;
                    this.ddlTemplates.DataBind();

                    //foreach (Data.ArtisanTemplate template in templates)
                    //{
                    //    template.Html = CleanTemplate(template.Html);
                    //    template.Save();
                    //}

                    LoadTemplate();
                }
            }
            catch (Exception ex)
            {
                this.litError.Text = ex.Message;
            }
            base.OnLoad(e);
        }

        protected void ChangeTemplate(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.ddlTemplates.SelectedValue))
            {
                LoadTemplate();
            }
        }

        protected void SaveTemplate(object sender, EventArgs e)
        {
            Data.ArtisanTemplate template = new Data.ArtisanTemplate(this.ddlTemplates.SelectedValue);
            template.Html = this.txtContent.Text;
            template.CssText = this.txtCss.Text;
            template.Description = this.txtDescription.Text;
            template.Save();

            LoadTemplate();
        }

        private void LoadTemplate()
        {
            Data.ArtisanTemplate template = new Data.ArtisanTemplate(this.ddlTemplates.SelectedValue);
            
            try
            {
                this.txtContent.Text = CleanTemplate(template.Html);
            }
            catch (Exception ex)
            {
                this.litError.Text = ex.Message;
                this.txtContent.Text = template.Html;
            }

            this.txtCss.Text = template.CssText;
            this.txtDescription.Text = template.Description;
        }

        private string CleanTemplate(string html)
        {
            XmlDocument xdoc = new XmlDocument();
            HtmlDocument hdoc = new HtmlAgilityPack.HtmlDocument();

            //html = html.Replace("<span class=\"placeholder\">", "").Replace("</span>", "");

            hdoc.LoadHtml(html);
            HtmlNodeCollection nodes = hdoc.DocumentNode.SelectNodes("//div[@class]");
            if (nodes != null)
            {
                foreach (HtmlNode node in nodes)
                {
                    if (node.Attributes["class"].Value.IndexOf("p ") > -1)
                    {
                        if (node.Attributes["style"] != null)
                        {
                            string style = node.Attributes["style"].Value;
                            style = Regex.Replace(style, @"max-height:\s?\d{1,3}px;\s?", "");
                            style = Regex.Replace(style, @"height:\s?\d{1,3}px;\s?", "");
                            node.Attributes["style"].Value = style;
                        }
                    }
                }
            }

            /*hdoc.OptionOutputAsXml = true;

            xdoc.LoadXml(hdoc.DocumentNode.InnerHtml); //template.Html);
            StringBuilder sb = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(sb, new XmlWriterSettings()
            {
                OmitXmlDeclaration = true,
                Indent = true
            }))
            {
                xdoc.WriteContentTo(writer);
            }
            return sb.ToString();*/
            return hdoc.DocumentNode.InnerHtml;
        }
    }
}
