﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Symphony.Core.Controllers;
using System.IO;
using log4net;
using Symphony.Core.CNRService;
using Symphony.Core;
using Symphony.Core.Models;
using System.Runtime.Serialization;
using Symphony.Web.HttpHandlers;
using Data = Symphony.Core.Data;
using System.Text;
using Newtonsoft.Json;

namespace Symphony.Web
{
    public partial class AssignmentsPopout : System.Web.UI.Page
    {
        protected override void OnPreLoad(EventArgs e)
        {
            var master = (SymphonyMaster)this.Master;
            master.PreventRedirect = true;
            base.OnPreLoad(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int courseId, trainingProgramId;

            if (!int.TryParse(HttpContext.Current.Request.Params["trainingProgramId"], out trainingProgramId))
            {
                throw new Exception("Invalid training program id.");
            }
            if (!int.TryParse(HttpContext.Current.Request.Params["courseId"], out courseId))
            {
                throw new Exception("Invalid course id.");
            }

            PortalController portalController = new PortalController();
            TrainingProgram trainingProgram = portalController.GetTrainingProgramDetails(SymphonyController.GetUserID(), trainingProgramId, false).Data;
            string trainingProgramJson = Utilities.Serialize(trainingProgram);

            Course course = trainingProgram.RequiredCourses
                .Concat(trainingProgram.OptionalCourses)
                .Concat(trainingProgram.RequiredCourses)
                .Concat(trainingProgram.FinalAssessments)
                .Where(c => c.CourseID == courseId)
                .ToList()
                .FirstOrDefault();

            if (course == null || course.Id == 0)
            {
                throw new Exception("The course requested does not belong to the training program requested.");
            }

            string courseJson = Utilities.Serialize(course);
                        
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("var trainingProgram = {0}, course = {1};", trainingProgramJson, courseJson));
            
            this.assignments.Text = sb.ToString();
        }
    }
}
