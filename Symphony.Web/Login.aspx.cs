﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using Symphony.Core;
using log4net;
using Symphony.Core.CNRService;
//using Symphony.Core.Data;
using System.Web.Security;
using SubSonic;
using Model = Symphony.Core.Models;
using Data = Symphony.Core.Data;
using Controllers = Symphony.Core.Controllers;
using Symphony.Web.Saml;
using Symphony.Core.Controllers;

namespace Symphony.Web
{
    public partial class Login : System.Web.UI.Page
    {
        Controllers.SymphonyController c = new Controllers.SymphonyController();
        ILog Log = LogManager.GetLogger(typeof(Login));

        protected bool isJanrain = false;
        protected int ssoLoginUiType;
        protected string ssoTimeoutRedirect = "";
        protected string listCustomersBySalesChannelCodeJson = "[]";
        public static string NEVER_REDIRECT = "never_redirect";

        protected override void OnInit(EventArgs e)
        {
            Literal link = new Literal();
            string filename = "/skins/" + GetCustomer() + ".css";
            if (System.IO.File.Exists(Server.MapPath("~" + filename)))
            {
                link.Text = "<link href=\"" + filename + "\" rel=\"stylesheet\" type=\"text/css\" />";
                Header.Controls.Add(link);
            }

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e) 
        {

            string sc = Request.QueryString["sc"];

            // janrainLogin.Visible = false;
            if (!Page.IsPostBack)
            {                
                // on the load of the page, if there's no customer specified, but
                // there is a customer cookie, use that to build a nice url
                // DON'T replace this with a call to GetCustomer(), because this relies on checking
                // the query string, etc, to determine if it needs to redirect
                string customerSubDomain = Request.QueryString["customer"] ?? string.Empty;
                bool doMobile = true;
                bool.TryParse(ConfigurationManager.AppSettings["doMobile"], out doMobile);
                if (string.IsNullOrEmpty(customerSubDomain) && Context.Request.Cookies["customer"] != null && string.IsNullOrEmpty(sc))
                {
                    customerSubDomain = Context.Request.Cookies["customer"].Value;
                    if (Request.Browser.IsMobileDevice && doMobile)  //with 51Degrees Foundation, IsMobileDevice==true for iPad
                    {
                        Response.Redirect("/mobile/" + customerSubDomain);
                    }
                    else
                    {
                        Response.Redirect("/login/" + customerSubDomain);
                    }
                }
                else
                {
                    if (Request.Browser.IsMobileDevice && doMobile)                                                                                                
                    {
                        Response.Redirect("/mobile/" + customerSubDomain);
                    }
                }
                this.login.TitleText = "Welcome to Symphony";
                //var c = new Controllers.CustomerController().GetCustomer(customerSubDomain);

                Data.Customer c = new Data.Customer(Data.Customer.Columns.SubDomain, customerSubDomain);

                if (!string.IsNullOrEmpty(sc))
                {
                    
                    string salesChannelCode = sc;
                    var salesChannel = new Controllers.CustomerController().GetSalesChannel(salesChannelCode);

                    if (salesChannel.Data != null && salesChannel.Data.ID > 0)
                    {
                        List<string> listOfCustomersInSalesChannel = new Controllers.CustomerController().GetListOfCustomers(salesChannelCode);
                        if (listOfCustomersInSalesChannel != null)
                        {
                            listCustomersBySalesChannelCodeJson = Utilities.Serialize(listOfCustomersInSalesChannel);
                        }
                    }
                }

                if (c.SsoEnabled && c.SsoType == (int)SsoType.Janrain)                                                                                                                                   
                {
                    this.isJanrain = true;
                }

                ssoLoginUiType = c.SsoLoginUiType.HasValue ? c.SsoLoginUiType.Value : 0;

                // username aliasing
                string alias = c.UserNameXAlias;
                this.login.UserNameLabelText = string.IsNullOrEmpty(alias) ? "Username:" : alias + ":";

                if (c.SelfRegistrationIndicator)
                {
                    this.login.PasswordRecoveryText = "Forgot your password?";
                    this.login.PasswordRecoveryUrl = ResolveUrl("~/forgotpassword/" + customerSubDomain + Skins.BuildQuerySkin("?"));
                }

                if (c.AllowSelfAccountCreation)
                {
                    this.login.HelpPageText = "Create a new account";
                    this.login.HelpPageUrl = ResolveUrl("~/createaccount/" + customerSubDomain + Skins.BuildQuerySkin("?"));
                }

                // if you visit this page, and you're not logging in, we're considering you logged out
                UserController.Logout(Context);

                // Hit the customer's sso logout as well
                if (!string.IsNullOrWhiteSpace(c.SSOTimeoutRedirect))
                {
                    this.ssoTimeoutRedirect = c.SSOTimeoutRedirect + "?customer=" + c.SubDomain;
                }

                Page.Header.DataBind();
            }
        }

        /// <summary>
        /// Gets the customer id from the query string or cookies
        /// </summary>
        /// <returns></returns>
        public static string GetCustomer()
        {
            var request = HttpContext.Current.Request;
            string customer = request.QueryString["customer"] ?? string.Empty;
            if (string.IsNullOrEmpty(customer) && request.Cookies["customer"] != null)
            {
                customer = request.Cookies["customer"].Value;
            }

            if (string.IsNullOrEmpty(customer))
            {
                customer = request.Form["customer"];
            }

            string sc = HttpContext.Current.Request.QueryString["sc"];

            if (!string.IsNullOrEmpty(sc))
            {
                customer = "";

                var salesChannel = new Controllers.CustomerController().GetSalesChannel(sc);
                if (salesChannel.Data != null)
                {
                    string customerName = HttpContext.Current.Request.Form["txtFacility"];

                    if (salesChannel.Data.ID > 0 && !string.IsNullOrEmpty(customerName))
                    {
                        string subdomainBySalesChannel = new Controllers.CustomerController().GetCustomerNameBySalesChannel(sc, customerName);
                        if (subdomainBySalesChannel != null)
                        {
                            customer = subdomainBySalesChannel.ToString();
                        }
                    }
                }
            }
            return customer ?? string.Empty;
        }

        protected void OnAuthenticate(object sender, System.Web.UI.WebControls.AuthenticateEventArgs e)
        {
            
            string customer = GetCustomer();
            string username = login.UserName;
            string password = login.Password;

            if (string.IsNullOrEmpty(username))
            {
                username = Request.Form["username"];
            }

            if (string.IsNullOrEmpty(password))
            {
                username = Request.Form["password"];
            }

            Controllers.UserController controller = new Controllers.UserController();
            Model.AuthenticationResult loginResult = controller.Login(customer, username, password);
            e.Authenticated = loginResult.Success;
            if (e.Authenticated)
            {
                string redirect = ProcessLoginResult(loginResult);

                // If it's a saml request, we only ever want to redirect back to samlhandler.
                // If we can't redirect to the handler, then we need to start fresh
                if (HttpContext.Current.Request.QueryString.AllKeys.Contains(Login.NEVER_REDIRECT) &&
                    !redirect.ToLower().Contains("samlhandler.ashx"))
                {
                    login.FailureText = "Log in session expired. Please refresh and try again.";
                    e.Authenticated = false;
                }
                else
                {
                    HttpContext.Current.Response.Redirect(redirect);
                }
            }
            else
            {
                login.FailureText = loginResult.Message;
            }

        }

        public static string ProcessLoginResult(Model.AuthenticationResult loginResult)
        {
            string forcedUrl = HttpContext.Current.Request.QueryString["ForceReturnUrl"];
            if (!string.IsNullOrEmpty(forcedUrl))
            {
                return forcedUrl;
            }

            // Check if this is a saml request or not
            // SamlHandler processes the request, if the user is logged in, the response is returned
            // if the user is not logged in, it redirects to the login page. After successful login
            // we need to redirect back to the SamlHandler, which will load the request again, but
            // this time the user will be logged in and the request will be properly returned
            //
            // Doing this first now so it will override the multi login redirect if that is 
            // also enabled. This way we will return the first user found to log in as in the
            // system that requested a login through symphony.
            string ssoSessionKey = "";

            if (HttpContext.Current.Request.QueryString.AllKeys.Contains(SamlParams.SsoSessionKey))
            {
                ssoSessionKey = HttpContext.Current.Request.QueryString[SamlParams.SsoSessionKey];
            }
            SsoAuthnState state = SsoAuthnState.RestoreFromCookie(ssoSessionKey);
            

            if (state != null && state.IsSamlRedirect)
            {
                string samlRedirectUrl = System.Web.Security.FormsAuthentication.GetRedirectUrl(loginResult.UserData.Username, false);

                if (samlRedirectUrl.ToLower().Contains("samlhandler.ashx"))
                {
                    state.IsSamlRedirect = false;
                    SsoAuthnState.SaveToCookie(state, ssoSessionKey);

                    if (string.IsNullOrEmpty(samlRedirectUrl) || !samlRedirectUrl.Contains('?'))
                    {
                        samlRedirectUrl += "?";
                    }
                    else
                    {
                        samlRedirectUrl += "&";
                    }

                    samlRedirectUrl += Saml.SamlParams.SamlRedirectKey + "=true&" + Saml.SamlParams.SsoSessionKey + "=" + ssoSessionKey;

                    return samlRedirectUrl;
                }
            }

            
            if ((loginResult.HasExternalSystems) || loginResult.AuthenticatedButNotSymphonyUser)
            {
                return "/MultiLogin.aspx" + Skins.BuildQuerySkin("?");
            }
            
            string url = HttpContext.Current.Request.QueryString["ReturnUrl"];
            string finalQuery = string.Empty;
            if (!string.IsNullOrEmpty(url))
            {
                string query = "";
                if (url.IndexOf("?") > -1)
                {
                    query = url.Substring(url.IndexOf("?") + 1);
                }
                string[] pairs = query.Split('&');

                foreach (string pair in pairs)
                {
                    if (pair.IndexOf("=") > -1)
                    {
                        string key = pair.Substring(0, pair.IndexOf("="));
                        if (key == "query")
                        {
                            // need to decode twice...once since it's in the original query string, and another time for the 2nd pass
                            finalQuery = HttpContext.Current.Server.UrlDecode(pair.Substring(pair.IndexOf("=") + 1));
                            continue;
                        }
                    }
                }
            }

            var loggedInCustomer = new Symphony.Core.Data.Customer(loginResult.CustomerId);

            string[] roles = SymphonyRoleProvider.GetRolesForUser(loginResult.CustomerSubDomain, loginResult.UserData.Username);
            string redirect = string.Empty;

            bool isAdvancedRedirect = (loggedInCustomer.RedirectType == (int)RedirectType.Force || loggedInCustomer.RedirectType == (int)RedirectType.OptIn);
            
            SymphonyController symphony = new SymphonyController();
            bool preventSimpleRedirect = roles.Length > 1 || symphony.UserIsSalesChannelAdmin;

            // if there's a redirect, and we *don't* have a bypass, and the user only has 1 role, redirect them
            // Don't handle a redirect here if the customer is set up with advanced redirect.
            if (!preventSimpleRedirect && !isAdvancedRedirect && !string.IsNullOrEmpty(loggedInCustomer.LoginRedirect) && string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["admin"]) || !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["portal"]))
            {
                redirect = loggedInCustomer.LoginRedirect;
                if (redirect.IndexOf("?") == -1)
                {
                    redirect += "?token=";
                }
                else
                {
                    redirect += "&token=";
                }
                redirect += HttpContext.Current.Response.Cookies[FormsAuthentication.FormsCookieName].Value;
                redirect += "&logoutRedirectUrl=" + HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
            }
            else
            {
                redirect = "/home/" + loginResult.CustomerSubDomain + "/" + finalQuery;
            }

            if (loggedInCustomer.IsExternalSystemLoginEnabled)
            {
                /*
                List<ExternalSystem> systems = Select.AllColumnsFrom<Data.ExternalSystem>()
                    .InnerJoin(Data.CustomerExternalSystem.ExternalSystemIDColumn, Data.ExternalSystem.IdColumn)
                    .Where(Data.CustomerExternalSystem.CustomerIDColumn).IsEqualTo(loggedInCustomer.Id)
                    .ExecuteTypedList<ExternalSystem>();

                if (systems.Count() > 0)
                {
                    var symphonyLandingUrl = redirect;
                    redirect = "/selectExternalSystem.aspx?redirect=" + redirect;
                }
                */ 
            }

            string prefix = "&";
            if (redirect.IndexOf("?") == -1)
            {
                prefix = "?";
            } 

            return redirect + Skins.BuildQuerySkin(prefix);
        }
    }
}
