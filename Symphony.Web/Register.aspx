﻿<%@ Page Title="Welcome to Symphony" MasterPageFile="~/SymphonyMaster.Master" Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Symphony.Web.Register" %>
<%@ MasterType  virtualPath="~/SymphonyMaster.Master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <script type="text/javascript" src="/scripts/symphony/portal.js?v=<%# this.Master.AssemblyVersion %>"></script>

    <script type="text/javascript">
        <asp:Literal ID="trainingProgramData" runat="server"></asp:Literal>

        Symphony.Settings.hidePortal = true;
        Symphony.Settings.isServerSideLogic = true;
        Symphony.Settings.isServerSideLogicPortal = true;

        (function () {

            var trainingProgramRecord = Ext.create('trainingProgram', trainingProgram);
            var courseRecord = Ext.create('course', trainingProgram.requiredCourses[0]);

            var launchCourse = function () {
                var link = courseRecord.get('displayCourseLink');

                var params = {};
                if (link.queryParams) {
                    for (var i = 0; i < link.queryParams.length; i++) {
                        params[link.queryParams[i].key] = link.queryParams[i].value;
                    }
                }

                Symphony.LinkHandlers.launch(null, null, link, courseRecord, params, function() {
                    window.top.close();   
                }, true);  
            }

            Ext.onReady(function () {
                Symphony.App = new Ext.Viewport({
                    layout: 'fit',
                    listeners: {
                        afterrender: function() {
                            launchCourse();
                        }
                    }
                });
            });
        })();

    </script>
</asp:Content>

