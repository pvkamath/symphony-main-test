﻿<%@ Page Title="Symphony Password Recovery" Language="C#" MasterPageFile="~/Unauthenticated.Master" AutoEventWireup="true"
    CodeBehind="ForgotPassword.aspx.cs" Inherits="Symphony.Web.ForgotPassword" %>

<asp:Content ID="Header" ContentPlaceHolderID="Header" runat="server">

    <script type="text/javascript">
        Ext.onReady(function() {
            var w = new Ext.Window({
                contentEl: Ext.get('wrapper'),
                closable: false,
                padding: '15'
            });
            w.show();
        });
    </script>

    <style type="text/css">
        .email
        {
            width:320px;
        }
        .success
        {
            color:Green;
        }
        .error
        {
            color:Red;
        }
    </style>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="Body" runat="server">
    <div id="wrapper" class="x-hidden">
        <form runat="server" id="submit">
        <h3>Forgot your password?</h3>
        <br />
        <p>Enter your email address below to receive an email containing a link to reset your password.</p>
        <br />
        <label for="email">Email Address:</label> <asp:TextBox ID="email" runat="server" CssClass="email" /><asp:Button Text="Send" runat="server" OnClick="SendPasswordLink" />
        <br />
        <div style="text-align:center;width:100%;margin-top:10px;">
        <asp:Literal ID="error" runat="server"></asp:Literal>
        <asp:Literal ID="success" runat="server"></asp:Literal>
        </div>
        </form>
    </div>
</asp:Content>
