﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportParametersTest.aspx.cs" Inherits="Symphony.Web.ReportTestHarness.ReportParametersTest" %>
<%@ Import Namespace="System.Web.Services" %>
<script type="text/C#" runat="server">
    [WebMethod]
    public static string SayHello(string name)
    {
        return "Hello " + name;
    }
</script>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <script type="text/javascript" src="../scripts/jquery-1.10.2.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            // our code goes here
            $("#btnGetJobrole").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/JobRoles/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'JobRolekey: ' + data.data[i].jobRolekey.toString() + ' JobDetailLevelName: ' + data.data[i].jobRoleLevelDetail + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });

            // our code goes here
            $("#btnGetUser").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/Users/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data.data.length);
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'UserKey: ' + data.data[i].userkey.toString() + ' LastName: ' + data.data[i].lastName.toString() + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });


            // our code goes here
            $("#btnGetSupervisor").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/Supervisors/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data.data.length);
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'UserKey: ' + data.data[i].userkey.toString() + ' LastName: ' + data.data[i].lastName.toString() + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });

            // our code goes here
            $("#btnGetSymphonyUser").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/SymphonyUsers/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data.data.length);
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'userId: ' + data.data[i].userId.toString() + ' LastName: ' + data.data[i].lastName.toString() + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });



            // our code goes here
            $("#btnGetLocation").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/Locations/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'locationKey: ' + data.data[i].locationkey.toString() + ' Location: ' + data.data[i].locationLevelDetail.toString() + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });  // End of Get Test function

            // our code goes here
            $("#btnGetTrainingProgram").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/TrainingPrograms/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data.data.length);
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'trainingProgramKey: ' + data.data[i].trainingProgramkey.toString() + ' Training Program: ' + data.data[i].name + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });

            $("#btnGetReportTypes").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/ReportTypes/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data.data.length);
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'Id: ' + data.data[i].id.toString() + ' Name: ' + data.data[i].name + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });  // End of Get Test function

            $("#btnGetAudience").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/Audiences/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data.data.length);
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'Audiencekey: ' + data.data[i].audiencekey.toString() + ' Audience: ' + data.data[i].audienceLevelDetail + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });


            // Save Job
            $("#btnSaveJob").click(function (event) {
                var job = new Object();
                job.name = "report job name";
                job.isScheduled = 1;
                var queue = new Object();
                queue.reportType = 10;
                var trigger = new Object();
                trigger.cronSchedule = "0 1 2 3";
                var parameters = new Object();
                parameters.courseType = 2;

                var jobDetail = new Object({ 'job': job, 'queue': queue, 'trigger': trigger, 'parameters': parameters });

                $.ajax({
                    method: 'POST',
                    url: '/services/reporting.svc/Job/0',
                    data: JSON.stringify(jobDetail),

                    contentType: 'application/json',
                    dataType: 'json',
                    success: function (result) {
                        alert('Inside result');
                        var row = '';
                        row = '<p>' + 'id: ' + result.data.id.toString() + ' name: ' + result.data.name + '</p>';
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });


            // Save Job
            $("#btnSaveReport").click(function (event) {
                var report = new Object();
                report.name = "report job name";
                report.isScheduled = 1;
                report.reportCode = "coursesummary";
                report.parameters = "{[some JSON string]}";
                report.scheduleType = 0;
                report.scheduleDayOfWeek = 3;

                $.ajax({
                    method: 'POST',
                    url: '/services/reporting.svc/Reports/0',
                    data: JSON.stringify(report),

                    contentType: 'application/json',
                    dataType: 'json',
                    success: function (result) {
                        alert('Inside result');
                        var row = '';
                        row = '<p>' + 'id: ' + result.data.id.toString() + ' name: ' + result.data.name + '</p>';
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });


            $("#btnSaveQueue").click(function (event) {
                $.ajax({
                    method: 'POST',
                    url: '/services/reporting.svc/Queue/2',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        var row = '';
                        row = '<p>' + 'id: ' + result.data.id.toString() + ' name: ' + result.data.name + '</p>';
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });

            $("#btnGetQueueList").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/QueueList/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data.data.length);
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'queueId: ' + data.data[i].id.toString() + ' name: ' + data.data[i].name + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });

            $("#btnGetReportJobDetail").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/JobDetail/25',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data);
                        row = '<p>' + 'queueId: ' + data.data.job.id.toString() + ' name: ' + data.data.job.name + '</p>';

                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });

            $("#btnGetReport").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/Reports/25',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data);
                        row = '<p>' + 'Id: ' + data.data.id.toString() + ' name: ' + data.data.name + '</p>';

                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);Bouaphet11

                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });


            $("#btnGetJobList").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/JobList/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data.data.length);
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'Id: ' + data.data[i].id.toString() + ' job name: ' + data.data[i].name + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });



            $("#btnGetQueue").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/Queue/3',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        row = '<p>' + 'id: ' + data.data.id.toString() + ' name: ' + data.data.name + '</p>';
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });

            $("#btnGetXLS").click(function (event) {
                window.open("/services/reporting.svc/Reports/XLS/1");
            });
            
            $("#btnGetPDF").click(function (event) {
                window.open("/services/reporting.svc/Reports/PDF/1");
            });

            $("#btnGetTestResultForUser").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/GetScormRegistration/6778',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data.data.length);
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'ScormRegistrationId: ' + data.data[i].scormRegistrationId.toString() + ' TestResult: ' + data.data[i].testResult + '</p>';
                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);

                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });

            // our code goes here
            $("#btnGetCourse").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/Courses/',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        alert(data.data.length);
                        for (var i = 0; i < data.data.length; i++) {
                            row += '<p>' + 'courseKey: ' + data.data[i].coursekey.toString() + ' Course: ' + data.data[i].name.toString() + '</p>';

                        }
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });  // End of Get Test function

            // our code goes here
            $("#btnGetReportData").click(function (event) {
                $.ajax({
                    method: 'GET',
                    url: '/services/reporting.svc/ReportDataInJSON/1',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var row = '';
                        row = data.substr(0, 255);
                        alert(row);
                        $('#outputString').html(row);
                        //$("#tableview").html(row);
                    },
                    error: function (xhr) {
                        try {
                            // a try/catch is recommended as the error handler
                            // could occur in many events and there might not be
                            // a JSON response from the server
                            var json = $.parseJSON(xhr.responseText);
                            alert(json.errorMessage);
                        } catch (e) {
                            alert('something bad happened');
                            alert(xhr.responseText);
                        }
                    }
                });
            });  // End of Get Test function

        });                       // End main script
   </script>


</head>
<body>
    <form id="Form1" runat="server">
    Standard Parameters<br />
    <asp:button ID="btnGetJobrole" runat="server" text="GetJobRole" />
    <asp:button ID="btnGetUser" runat="server" text="GetUser" />
    <asp:button ID="btnGetTrainingProgram" runat="server" text="GetTrainingProgram" />
    <asp:button ID="btnGetAudience" runat="server" text="GetAudience" />
    <asp:button ID="btnGetLocation" runat="server" text="GetLocation" />
    <asp:button ID="btnGetCourse" runat="server" text="GetCourse" />
    <asp:button ID="btnGetReportTypes" runat="server" 
        text="Get ReportTypes" />
    <br />
    <br />
    Scheduler<br />
    <asp:button ID="btnSaveJob" runat="server" text="Save Job Detail" />
    <asp:button ID="btnGetReportJobDetail" runat="server" text="Get Job Detail " />

    <asp:button ID="btnGetReport" runat="server" text="Get Report " />
    <asp:button ID="btnSaveReport" runat="server" text="Save Report " />
    

    <asp:button ID="btnGetQueueList" runat="server" text="GetQueueList" />
 <%--   <asp:button ID="btnSaveQueue" runat="server" text="SaveQueue" />--%>
    
    <asp:button ID="btnGetJobList" runat="server" text="GetJobList" />
    <asp:button ID="btnGetQueue" runat="server" text="GetQueue" />
    <%--<asp:button ID="Button1" runat="server" text="GetJob - Not done" />--%>
    <br />
    <br />
    Processer<br />
    <asp:button ID="btnProcessQueue" runat="server" text="ProcessQueue" />
    <asp:button ID="btnCreateXLS" runat="server" text="CreateXLS" />
    <asp:button ID="btnCreatePDF" runat="server" text="CreatePDF" />
    <asp:button ID="btnGetXLS" runat="server" text="GetXLS" />
    <asp:button ID="btnGetPDF" runat="server" text="GetPDF" />
    <asp:button ID="btnGetReportData" runat="server" 
        text="GetQueueResultData (for Grid)" />
    <br />
    <br />
    Delete<br />
    <asp:button ID="btnDeleteQueue" runat="server" text="DeleteQueue - not done" />
    <asp:button ID="btnDeleteXLS" runat="server" text="DeleteXLS - not done" />
    <asp:button ID="btnDeletePDF" runat="server" text="DeletePDF - not done" />

    <br />
    <br />

    Misc<br />
    <asp:button ID="btnGetSymphonyUser" runat="server" text="GetSymphonyUser" />
    <asp:button ID="btnGetTestResultForUser" runat="server" text="GetArtisanTestResultForUser" />
    <asp:button ID="btnGetSupervisor" runat="server" 
        text="GetSymphonySupervisor" />



    <table id="tableview">
    </table>
    <p id="outputString">Output Area.</p>
    </form>
</body>
</html>

