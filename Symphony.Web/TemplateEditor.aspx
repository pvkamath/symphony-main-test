﻿<%@ Page Title="Welcome to Artisan" MasterPageFile="~/SymphonyMaster.Master" Language="C#" AutoEventWireup="true" CodeBehind="TemplateEditor.aspx.cs" Inherits="Symphony.Web.TemplateEditor" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Literal runat="server" ID="litError"></asp:Literal>
    <asp:DropDownList runat="server" id="ddlTemplates" AutoPostBack="true" OnSelectedIndexChanged="ChangeTemplate" />    
    <br />
    <asp:TextBox TextMode="MultiLine" Rows="5" Columns="150" ID="txtDescription" runat="server"></asp:TextBox>
    <br />
    <asp:TextBox TextMode="MultiLine" Rows="20" Columns="150" ID="txtContent" runat="server"></asp:TextBox>
    <br />
    <asp:TextBox TextMode="MultiLine" Rows="20" Columns="150" ID="txtCss" runat="server"></asp:TextBox>
    <br />
    
    <asp:Button OnClick="SaveTemplate" Text="Save Changes" runat="server" />
</asp:Content>