﻿<%@ Page Language="C#" %>
<%@ Import Namespace="System.Transactions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <% 
        using (TransactionScope scope = new TransactionScope())
        {
            var user = new Symphony.Core.Data.User(147);
            user.Notes += "test";
            user.Save();
            scope.Complete();
            Response.Write("Success");
        }
    %>

    </div>
    </form>
</body>
</html>
