﻿<%@ Page Title="Symphony Video Conference" Language="C#" MasterPageFile="~/SymphonyMaster.Master" AutoEventWireup="true" CodeBehind="VideoChat.aspx.cs" Inherits="Symphony.Web.VideoChat" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <script type="text/javascript" src="/scripts/symphony/VideoChat/IceLink.js"></script>
    <script type="text/javascript" src="/scripts/symphony/VideoChat/VideoChatViewport.js"></script>
    
    <script type="text/javascript">
        
        var params = Symphony.parseQueryString(window.location.search.substring(1));


        var conferenceChannel = params.channel;

        Ext.onReady(function () {
            if (Symphony.Modules.hasModule(Symphony.Modules.VideoChat)) {

                Symphony.VideoChat.Notification.init(function () {
                    if (params.invite || params.userId) {
                        if (!params.userId) {
                            Ext.Msg.alert("Error", "userId is a required query string param when sending a chat invite.");
                            return;
                        }

                        conferenceChannel = Symphony.VideoChat.Notification.invite(params.userId, true);
                    }

                    if (conferenceChannel === false) {
                        return;
                    } else if (typeof (conferenceChannel) == 'string') {
                        var videoViewport = new Symphony.VideoChat.VideoChatViewport({
                            renderTo: 'VideoChat',
                            onRenderFn: function (cmp) {
                                Symphony.VideoChat.IceLink.init(cmp, cmp.getVideoElement(), conferenceChannel, cmp.chatHandler);
                            }
                        });
                    } else {
                        Ext.Msg.alert("Error", "Could not establish communication link. Provide either the conference channel or user id you wish to connect to.");
                    }
                });
            } else {
                Ext.Msg.alert("Error", "Video chat has not been enabled in Symphony for this customer. Please contact an administrator if you would like to use this feature.");
            }
        });
       
    </script>
    
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="VideoChat" style="height:100%; display: block;">
    
    </div>
</asp:Content>
