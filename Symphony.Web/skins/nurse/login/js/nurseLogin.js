﻿(function () {
    function addNurseLinks() {
        var links = [{
            id: "nurse-forgot",
            url: "http://www.nurse.com/forgotpassword",
            text: "Trouble Signing In?"
        }, {
            id: "nurse-signup",
            url: "http://www.nurse.com/SignUp?noPop=1 ",
            text: "Not a member? Sign up!"
        }];

        var linksHtml = [];

        // build the links
        for (var i = 0; i < links.length; i++) {
            linksHtml.push('<span class="nurse-link" id="' + links[i].id + '"><a href="' + links[i].url + '">' + links[i].text + '</a></span>');
        }

        // build the table row
        var tblHtml = '<tr><td>' + linksHtml.join('\n') + '</td></tr>';

        // add the new row
        $("#ctl00_Body_login").append(tblHtml);
    };

    $(document).ready(function () {
        if (window.top !== window.self) {
            $('body').css('backgroundColor', '#fff');
            $('#page_logo').hide();

        } else {
            $("#form1Wrapper").addClass("top");
            addNurseLinks();
        }
    });

})();