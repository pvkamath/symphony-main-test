
/*************************************************\
 ** FILE:  shared/scripts/scoPersistence.js
 **
 ** INCLUDED IN:
 **  1. the SCO-frameSET.
 **
 ** IMPLEMENTS:
 ** Data encoding so we can send it to LMS
 **
 ** ASSUMES:
 **	1. zooApiWrappers.js	Communications with LMS.
 **  2. scoUtils.js			Utils, may be needed.
 ** -----------------------------------------------
 
 PROBLEM:
 if no data is stored on the LMS, dicts will be empty.
 But we want to ADD things to them, so the dicts in
 use should be initialized to something that stays there.
 BUT if we lazyly doi this too early, then
 these will be empty dictioanries.
 
 
 SOLUTION:
 when ZooLMSInitialize is called, we must then
 EXPLICITLY load teh data from the LMS,
 not lazyly.
 
 
 \**************************************************/
var dictsArrayVAR = null;



var DEBUG = false; /*!<variable name="DEBUG" type="Global" />!*/
function isIncomplete(){
    var b = !isCommitted(); // NOT READY
    return b
}

function isNotTest() // NOT READY
{
    return true
}



function sendDataToLMS(scoreString, statusString)//-----------------------------------------
//Called at the end of a session for instance,
// to persist data into the LMS.
//----------------------------------------
//
{

    zooSendStatusToLMS(statusString);
    
    var encString = encodeDicts();
    zooLMSSetValue("cmi.suspend_data", encString);
    
    
}


function scoStatus_persistence()//         =====================
// return the status
// This affects whether answers can be changed
// or whether we go to review mode.
// 
// Returns 'completed' if  setCookieCOMMIT has been called.
// --------------------
{
    var aString = zooGetStatusFromLMS(); /*!<variable name="aString" type="Global" />!*/
    return aString;
}


function updateSessionTime()//       ===================
// Pass on information about the session time
// spent to the LMS.
{
    zooUpdateSessionTime(); // in zooApiWrappers.js
}


///////////////////// LMS INTERACTION: ////////////////////////////   

function setStatusIncomplete(){
    zooSendStatusToLMS('incomplete');
}




function dictsArray()//       ==========
// Return an array of 3 objects that hold all
// user given data, including instance ids.
// Automatically initialize to 3 empty dicts,
// and assume the LMSInitialize replaces the
// these with the persisted data.
{

    if (dictsArrayVAR) {
        return dictsArrayVAR;
    }
    
    emptyDictsArray(); // side effect   
    return dictsArrayVAR;
}


function emptyDictsArray()//       ==================
{
    var d1 = newDict();
    var d2 = newDict();
    var d3 = newDict();
    var d4 = newDict();
    
    var result = [d1, d2, d3, d4];
    dictsArrayVAR = result;
    return result;
}



function resetDictsArray()//       ==================
{
    setDictsArray(null);
}


function setDictsArray(anArray){
    dictsArrayVAR = anArray;
}



//------------------------------------------


function restoreDicts()//       ============
// Read the persisted userdata and 
// store it into a global array of
// 4 dicts. Return the persisted
// array containing the 4 dicts.
//
// CONTEXT: Called by LMSInitialize because
// only AFTER that is the LMS able to give 
// us data.
{
    var str0 = zooLMSGetValue('cmi.suspend_data');
    if (str0 == null) {
        setDictsArray(emptyDictsArray());
        
        return (null);
    }
    var str = unescape(str0);
    
    if (str == "") {
        setDictsArray(emptyDictsArray());
        
        return (null);
    }
    
    var dicts = decodeDicts(str);
    setDictsArray(dicts);
    
    return dicts;
}



function initDict(aDict){
    // if the dict had null as a value
    // for something, meaning it does not
    // have that key, then store "" into
    // it. 
    
    var nops = scoInfo().length - 1;
    for (pin = 1; pin <= nops; pin++) {
        var akey = pin.toString();
        if (aDict[akey] == null) {
            aDict[akey] = "";
        }
    }
}

function newDict(){
    var dict = new Object();
    initDict(dict);
    return dict;
}


function sep()//       ===
// the thing used to separate fields in the encoded
// suspend-data
{
    // #sco133.
    return ('###'); // balance sheet uses '|' internally
}



function decodeDicts(aString){
    var arr;
    var d1 = newDict();
    var d2 = newDict();
    var d3 = newDict();
    var d4 = newDict();
    
    
    // showProps(d1); alert("scoPersistence::usersAnswersDIC \n" + aString);
    // showProps(d2); alert("dict 2");
    // showProps(d3); alert("dict 3");
    // showProps(d4); alert("dict 4");
    
    var k;
    var v;
    var size = 0;
    var i = 0;
    var j = 0;
    //var parts	= aString.split(/\|/);       
    var parts = aString.split(sep()); //  #sco133. 
    //showProps(parts); alert("SEE PARTS " + tparts);
    
    size = parts[i];
    i = i + 1;
    
    for (j = i; j < i + (size * 2); j = j + 2) {
        k = parts[j];
        v = parts[j + 1];
        if (v == '_') 
            v = "";
        d1[k] = v;
    }
    
    i = i + (size * 2);
    size = parts[i];
    i = i + 1;
    
    for (j = i; j < i + (size * 2); j = j + 2) {
        k = parts[j];
        v = parts[j + 1];
        if (v == '_') 
            v = "";
        d2[k] = v;
    }
    
    i = i + (size * 2);
    size = parts[i];
    i = i + 1;
    for (j = i; j < i + (size * 2); j = j + 2) {
        k = parts[j];
        v = parts[j + 1];
        if (v == '_') 
            v = "";
        d3[k] = v;
    }
    
    i = i + (size * 2);
    size = parts[i];
    i = i + 1;
    for (j = i; j < i + (size * 2); j = j + 2) {
        k = parts[j];
        v = parts[j + 1];
        if (v == '_') 
            v = "";
        d4[k] = v;
    }
    
    arr = [d1, d2, d3, d4];
    dictsArrayVAR = arr;
    
    // showProps(d1 );
    // alert("DECODE Dicts:\n" + aString);
    
    return arr;
}



function encodeDicts(){


    //  encodeDictTEST( uAnswersPRIVATE () , "TESTUserAnswers") ;
    
    var sp = sep(); // sco133.
    var str = encodeDict(uAnswersPRIVATE(), "UserAnswers");
    str += sp + encodeDict(caDict(), "CorrectAnswers");
    str += sp + encodeDict(ixDict(), "InstanceIds");
    str += sp + encodeDict(propsDict(), "Properties");
    
    
    //showProps(uAnswersPRIVATE ()  );
    //alert("EncodeDicts:\n" + str);
    
    return str;
}



function encodeDictTEST(aDict, nString)//       ===========
{
    initDict(aDict);
    
    var siz = 0;
    var str = "";
    for (k in aDict) 
        siz++;
    
    var sizStr = siz.toString() + "";
    
    var keyStr = "";
    
    str = sizStr + "";
    
    for (k in aDict) {
        var val = aDict[k];
        if (val == null) 
            val = "_";
        if (val == "") 
            val = "_";
        
        keyStr += k.toString() + " -> ";
        str += sep() + k.toString() + sep() + val.toString();
    }
    
    alert("KEYS = " + keyStr);
    return str;
}




function encodeDict(aDict, nString)//       ===========
{
    var siz = 0;
    var str = "";
    for (k in aDict) 
        siz++;
    
    var sizStr = siz.toString() + "";
    
    str = sizStr + "";
    
    for (k in aDict) {
        var val = aDict[k];
        
        if (val == null) 
            val = "_";
        if (val == "") 
            val = "_";
        
        var keyPart = k.toString();
        var valPart = val.toString();
        
        str += sep() + keyPart + sep() + valPart; // sco133
    }
    
    
    return str;
}



//*********    **************

function uAnswersAt(keyObject){
    return uAnswersPRIVATE()[keyObject]
}

function uAnswersAtPut(keyObject, valueObject){
    uAnswersPRIVATE()[keyObject] = valueObject
}

function uAnswersPRIVATE()//       =============
// user-given answers + marks on visited
// pages. Never call this from any other
// file than this. Instead use the 2
// functions above.


{
    var result = dictsArray()[0];
    return result;
}




function caDict()//       ======
// Correct answers, with page-id as the key.
//
{
    return dictsArray()[1];
}



function ixDict()//       ======
// index of the chosen random exercise
//
{
    return dictsArray()[2];
}


function propsDict()//       ======
//  
//
{
    return dictsArray()[3];
}


function shouldResetData()//        ===============
// Return true if the system should forget
// user answers from previous sessions. This
// should return true if the user is to RETAKE
// a test. If we return false, also put the
// system into REVIEW-MODE, so user can not
// 
{
    // 1. If the SCO was never 'completed'
    // always remember answers given previously:
    if (isIncomplete()) 
        return false;
    
    // 2. If the sco is not a test, we should never
    // reset its data:
    if (isNotTest()) 
        return false;
    
    
    // 3. If the user says no to reset, dont
    //    do it:
    
    var reset = confirm("You have already " +
    "\ncompleted this test!\n" +
    "\nDo you want to forget \nyour previous answers " +
    "\nand RETAKE the test ?");
    
    if (!reset) 
        return false;
    
    setReviewMode();
    return false;
}




function restoreStatus()//        ===============
// Read the status from the LMS and set the
// viewing mode appropriately: 
// A) If the status is null or incomplete, do nothing.
//    If the user exited properly the last time, the
//    previous answers will show.
// B) If the status is Completed, and the sco is not a test,
//    do nothing, meaning the user answers given in the
//    previous session will show.
//
// C) If the status is Completed, and the sco is of type TEST,
//    it means the LMS allows users to retake this test. Ask
//    the user if she wants to retake it
{
    // 1. If the SCO was never 'completed'
    // always remember answers given previously:
    if (isIncomplete()) 
        return false;
    
    // 2. If the sco is not a test, we should never
    // reset its data:
    if (isNotTest()) 
        return false;
    
    
    // 3. If the user says no to reset, dont
    //    do it:
    
    var reset = confirm("You have already " +
    "\ncompleted this test!\n" +
    "\nDo you want to forget \nyour previous answers " +
    "\nand RETAKE the test ?");
    
    if (!reset) 
        return false;
    
    setReviewMode();
    return false;
}
