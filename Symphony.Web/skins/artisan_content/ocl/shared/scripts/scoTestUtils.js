// JavaScript Document
// File name : scoTestUtils.js


// ============= PUBLIC INTERFACE ========
// by Carlos
//
// Called from other frames to effect changes
// in the test-navigation  frame
//



function setQuestionNumber(qn){
    questionNumber = qn;
}

var questionNumber = 1;

function getQuestionNumber(){

    return questionNumber;
}

function setNumberOfQuestions(noq){
    numberOfQuestions = noq;
}

var numberOfQuestions = 10;


function getNumberOfQuestionsXX(){
    return numberOfQuestions;
}

function getNumberOfQuestions(){
    var result = parent.getNumberOfQuestions(); // get it from index.js
    return result
}




function setState(aState) {
// test   mode                = 1
// test question review       = 2
// test results               = 3
// test question  remediation = 4
    stateVar = aState;
}

var stateVar = 1;


function getState(){
    return stateVar;
}

function refreshNavigationBar(){ 
//alert("refreshNavigationBar () to be implemented by Carlos");
    buildToolBar();
}

function refreshButtons(){

    var nofButtons = 4;
    var imgWidth = 38;
    var imgHeight = 35;
    getButtons();
    
    for (i = 1; i <= nofButtons; i++) {
    
        buttonType = buttonArray[i];
        isOff = (buttonIsDisabled[i]);
        isShowing = !buttonType;
        buttonName = (isOff) ? buttonNames[buttonType] + '_off' : buttonNames[buttonType];
        buttonImg = (isShowing) ? (sharedImgFolder + 'spacer.gif') : (sharedImgFolder + buttonName + '.gif');
        
        buttonObject = MM_findObj('button' + i);
        buttonObject.src = buttonImg;
        if (isIE) 
            (buttonObject.height = (isShowing) ? 1 : imgHeight);
        if (isIE) 
            (buttonObject.width = (isShowing) ? 1 : imgWidth);
        buttonObject.alt = (isRemediation()) ? buttonToolTipsReview[buttonType] : buttonToolTips[buttonType];
    }
    
    setLayout();
    return true;
}

function setLayout(){

    getButtons();
    
    var buttonCount = 0;
    for (i = 1; i <= 4; i++) {
        if (buttonArray[i]) 
            buttonCount++;
    }
    
    
    prevqSpan = MM_findObj('prevq');
    questionSpan = MM_findObj('qspan');
    nextqSpan = MM_findObj('nextq');
    navigationSpan = MM_findObj('navigation');
    noqOffset = 34 * (getNumberOfQuestions() - 10); // calculates the offset for tests that have more/less than 10 questions
    widthAdjustment = -2; // change this when gotta adjust the width of the buttons layer
    eventHorizon = 737 - (34 * (4 - buttonCount)) + noqOffset + widthAdjustment;
    buttonWidths = 296 - (34 * (4 - buttonCount)) + widthAdjustment;
    
    nprevqSpanXorigin = 267 + (34 * (buttonCount - 3)) + widthAdjustment;
    qspanXorigin = nprevqSpanXorigin + 46;
    nextqSpanXorigin = qspanXorigin + (getNumberOfQuestions() * 34);
    
    if ((windowObj.getWindowWidth() > eventHorizon) && isms()) {
        //		alert("I am beyond");
        // screen is bigger than needed
        distanceA = Math.abs(nextqSpanXorigin - qspanXorigin);
        distanceB = Math.abs(qspanXorigin - nprevqSpanXorigin);
        
        nextqSpanXorigin = windowObj.getWindowWidth() - 50;
        qspanXorigin = nextqSpanXorigin - distanceA;
        nprevqSpanXorigin = qspanXorigin - distanceB;
        buttonWidths = nprevqSpanXorigin + (28 * (4 - buttonCount)) - ((buttonCount == 4) ? 5 : 0) - ((buttonCount == 3) ? 34 : 0);
    }
    
    
    if (!isIE) {
        prevqSpan.x = nprevqSpanXorigin;
        questionSpan.x = qspanXorigin;
        nextqSpan.x = nextqSpanXorigin;
        
        if (navigationSpan.clip) { // for #mozilla
            navigationSpan.clip.width = buttonWidths;
        }
        
        
    }
    else {
        prevqSpan.style.left = nprevqSpanXorigin;
        questionSpan.style.left = qspanXorigin;
        nextqSpan.style.left = nextqSpanXorigin;
        navigationSpan.style.width = buttonWidths;
    }
    
}

function refreshCurrentQuestionMarker(qNumber){

    NOQ = getNumberOfQuestions();
    for (var i = 1; i <= NOQ; i++) {
        markerStateImgObj = MM_findObj('qmarkimg_' + i);
        isCurrentSRC = (i == qNumber) ? ("current.gif") : ("nounderscore.gif");
        markerStateImgObj.src = (isIE) ? (sharedImgFolder + isCurrentSRC) : (getBaseURL(markerStateImgObj.src) + isCurrentSRC);
    }
}

function setPageNoToState(pageNo, state)
// modify the array holding the state
// so that buttons will display differently
//
// STATES: 
// notchanged = 1 (square)
// changed = 2 (square - red dot)
// correct = 3 (check)
// incorrect = 4 (x)
// noanswer = 5 (question mark)

{
    //alert("setPageNoToState () to be implemented by Carlos");
    
    if (parent.isReviewMode()) {
        if (state == 2) 
            return; // sco133
    }
    buttonStatesVAR[pageNo] = state;
    buttonStateImgObj = MM_findObj('qstateimg_' + pageNo);
    buttonStateImgObj.src = (isIE) ? (sharedImgFolder + qStateImageArray[state]) : (getBaseURL(buttonStateImgObj.src) + qStateImageArray[state]);
}

function getBaseURL(aURL){
    var aURLString = String(aURL);
    var urlArray = aURLString.split("/");
    baseUrl = '';
    for (i = 0; i < urlArray.length - 1; i++) 
        baseUrl += (urlArray[i] + '/');
    return baseUrl;
}

function getPageNoState(pageNo){
    return buttonStatesVAR[pageNo];
}

//=============== 2.IMPLEMENTATION  ============
// by Carlos
//

// question states

var buttonStatesVAR = new buttonStates();

function buttonStates(){
    tempObj = new Object();
    for (i = 1; i <= getNumberOfQuestions(); i++) {
        tempObj[i] = 1;
    }
    return tempObj;
}


buttonNames = ['spacer', 'submit', 'delete', 'exit', 'results', 'back', 'firstaid', 'prefs', 'refresh', 'previous', 'next'];
buttonToolTips = ['', 'submit this test for correction and scoring', 'close and discard this test without scoring', 'exit this test', 'see the test results', 'return to the test question', 'learn material for the current question', 'preferences', 'refresh the current question', 'go to the previous question in this test', 'go to the next question in this test'];
buttonToolTipsReview = ['', 'submit this test for correction and scoring', 'close and discard this test without scoring', 'exit this test', 'see the test results', 'return to the test question', 'learn material for the current question', 'preferences', 'refresh the current page', 'go to the previous question in this test', 'go to the next question in this test'];

buttonActions = ['', "scoSubmit();", "scoDelete();", "scoExit();", "scoResults();", "scoBack();", "scoFirstAid();", "scoPrefs();", "scoRefresh();", "scoGoToPreviousQuestion()", "scoGoToNextQuestion()"];

// page constants
var buttonArray; // buttons to be included in the toolbar
var buttonIsDisabled; // disabled array
// functions to be implemented later

// test state functions

function testMode(){
    if (getState() == 1) 
        return 1;
    return 2
}




function isReportPage(){
    // this works only in review mode
    // report page is the equivalent to the TOC in 
    // a regular sco 
    
    if (getState() == 3) 
        return true;
    return false;
}



function isRemediation(){
    // this works only in review mode
    // remediation is when the user is seeing the page that is linked to
    // the question being reviewed... close to a dyk page in a regular sco
    
    if (getState() == 4) 
        return true;
    return false;
}


// navigation functions

function scoGoToNextQuestion(){
    // this function works already in current version
    noqs = getNumberOfQuestions();
    
    loadNextPage();
    
    // ***********************************************************************************************
    // Carlos addition to disable the next/previous buttons when u reach the beggining/end of a test
    //
    if (!isIE) 
        return checkNextPreviousButtons(parent.currentPageIndex(), noqs);
    
    // end of carlos addition
    // ***********************************************************************************************
}

function scoGoToPreviousQuestion(){
    // this function works already in current version
    noqs = getNumberOfQuestions();
    
    loadPreviousPage();
    
    // ***********************************************************************************************
    // Carlos addition to disable the next/previous buttons when u reach the beggining/end of a test
    //
    if (!isIE) 
        return checkNextPreviousButtons(parent.currentPageIndex(), noqs);
    
    // end of carlos addition
    // ***********************************************************************************************
}



function checkNextPreviousButtons(currentPage, totalPages){

    //		markerStateImgObj.src = (isms())?(sharedImgFolder + isCurrentSRC):(getBaseURL(markerStateImgObj.src) + isCurrentSRC);
    
    if (currentPage <= 1) {
        PQ.isEnabled = false;
        buttonObject = MM_findObj("PQ");
        buttonObject.src = (isIE) ? (sharedImgFolder + "previous_off.gif") : (getBaseURL(buttonObject.src) + "previous_off.gif");
    }
    else {
        PQ.isEnabled = true;
        buttonObject = MM_findObj("PQ");
        buttonObject.src = (isIE) ? (sharedImgFolder + "previous.gif") : (getBaseURL(buttonObject.src) + "previous.gif");
    }
    
    if (currentPage >= totalPages) {
        NQ.isEnabled = false;
        buttonObject = MM_findObj("NQ");
        buttonObject.src = (isIE) ? (sharedImgFolder + "next_off.gif") : (getBaseURL(buttonObject.src) + "next_off.gif");
    }
    else {
        NQ.isEnabled = true;
        buttonObject = MM_findObj("NQ");
        buttonObject.src = (isIE) ? (sharedImgFolder + "next.gif") : (getBaseURL(buttonObject.src) + "next.gif");
    }
    
    //	alert(PQ.isEnabled + " ::: " + NQ.isEnabled);

}

function mouseOver_NB(navButton){
    buttonType = (navButton == 1) ? "PQ" : ((navButton == 2) ? ("NQ") : "");
    imgName = (buttonType == "PQ") ? "previous" : "next";
    isEnabled = eval(buttonType + ".isEnabled");
    buttonImg = (isEnabled) ? (sharedImgFolder + imgName + '_down.gif') : (sharedImgFolder + imgName + '_off.gif');
    buttonObject = MM_findObj(eval(buttonType + ".myName"));
    buttonObject.src = buttonImg;
    
    feedbackGraphic = (buttonType == "PQ") ? "images/feedbackbar_13.gif" : "images/feedbackbar_14.gif";
    
    if (isEnabled) {
        feedBackObject = MM_findObj('feedback');
        feedBackObject.src = feedbackGraphic;
    }
    
}

function mouseOut_NB(navButton){
    buttonType = (navButton == 1) ? "PQ" : ((navButton == 2) ? ("NQ") : "");
    imgName = (buttonType == "PQ") ? "previous" : "next";
    isEnabled = eval(buttonType + ".isEnabled");
    buttonImg = (isEnabled) ? (sharedImgFolder + imgName + '.gif') : (sharedImgFolder + imgName + '_off.gif');
    buttonObject = MM_findObj(eval(buttonType + ".myName"));
    buttonObject.src = buttonImg;
    
    feedbackGraphic = "images/feedbackbar_0.gif";
    
    if (isEnabled) {
        feedBackObject = MM_findObj('feedback');
        feedBackObject.src = feedbackGraphic;
    }
    
}

function mouseDown_NB(navButton){
    buttonType = (navButton == 1) ? "PQ" : ((navButton == 2) ? ("NQ") : "");
    imgName = (buttonType == "PQ") ? "previous" : "next";
    isEnabled = eval(buttonType + ".isEnabled");
    buttonImg = (isEnabled) ? (sharedImgFolder + imgName + '_down.gif') : (sharedImgFolder + imgName + '_off.gif');
    buttonObject = MM_findObj(eval(buttonType + ".myName"));
    buttonObject.src = buttonImg;
}

function onClick_NB(navButton){

    buttonType = (navButton == 1) ? "PQ" : ((navButton == 2) ? ("NQ") : "");
    NQAction = "scoGoToNextQuestion()";
    PQAction = "scoGoToPreviousQuestion()";
    myAction = eval(buttonType + "Action");
    isEnabled = eval(buttonType + ".isEnabled");
    
    if (!isEnabled) 
        return;
    
    eval(myAction);
}
