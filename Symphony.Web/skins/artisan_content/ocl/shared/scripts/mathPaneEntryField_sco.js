// ************************************************************
// DEBUGGING
// ************************************************************


function debug(varArray) {
	var obj;
//	alert(varArray);
	textToOutput = '';
	for (i=0;i<varArray.length;i++) {
		textToOutput	+= String(varArray[i])+' :: ';
		textToOutput	+= varArray[i]+'<br>';
	}
	MM_setTextOfLayer ('debug','',textToOutput);

}

_ISNETSCAPE = isNetscape();


// ************************************************************
// ************************************************************


function charsOfString (aString)
//==============================   wtnow40
// Return the characters of the 
// argument string as an array  
//                              
{ 	var str = aString ;
	var arr = new Array ();
	for (i = 0; i < str.length; i++)
	{	arr[i] = str.charAt(i);
	}
	return arr;
}

var restoredDigits_VAR; 

function cleanUserAnswer() {
	mask = normalize(NEF_format,'0123456789');
	var arr = charsOfString (mask);
//	debArr = [mask,arr];
//	debug(debArr);
	for (i = 0; i < arr.length; i++)
	{
	valueOfFieldNo_is (i + 1, '');
	}
}


function padZeros(aString,decimals,mask) {
	var stringedNumber = String(aString);
	var left,right;
	if (stringedNumber.indexOf('.')!=-1) {
		right = stringedNumber.split('.')[1];
		deficit = decimals - right.length;
	} else {
		aString+=".";
		deficit = decimals;
	}
	
	for(var i=0;i<deficit;i++) {
		aString += '0';
	}
	
	//negative padding (meaning zeroes to the left of the number
	if (aString.length!=mask.length) {
		alert(aString.length + " ::: " + mask.length);
		alert("not the same, padding!!!");
		maskLength = mask.length;
		aStringLength = aString.length;
		for (i=0;i<(maskLength-aStringLength);i++)
			{ 
				alert( mask.length-aString.length + " delta zeroes");
				alert( i + " more zeroes");
				aString = "0" + aString;
				alert( "mask ::: " + mask + "\naString ::: " + aString);
			}
	}
	
	return aString;
}


function restoreDigits(aString,varFormat) { 

	toRestore = retrievePageAnswer ();
	
	alert("restoreDigits toRestore ::: " + String(toRestore));
	alert("aString toRestore ::: " + String(aString));

	answerIsNotEmpty = (String(toRestore)==String(aString));

	alert("answerIsNotEmpty ::: " + answerIsNotEmpty);

	if (!answerIsNotEmpty) aString="0";

 if (aString == "") 
 {
 	if (parent.isReviewMode()) { setReadOnly(NEF_format)};
	return;
 }

 	mask = normalize(varFormat,'0123456789.');
	aString =padZeros(aString,numberOfDecimals(),mask);

alert( "restoring ::: " + aString + "\n" + "format ::: " + varFormat + "\n mask ::: " + mask);
	
// if (getDigits(aString.getMaskFromNumber())!=getDigits(mask)) 
//  { 
//   if (parent.isReviewMode()) setReadOnly(NEF_format);
//   return;
//  };
//	aString = String(aString);

alert( "after String restoring ::: " + aString + "\n" + "format ::: " + varFormat);

	restoredDigits_VAR = aString;
 
 if (aString == null) return;
 var str = aString.replace(/\./, "");
 var arr = charsOfString (str);
 for (i = (arr.length); i >0 ; i--)
 { 
 	var digit = arr[i-1];
	valueOfFieldNo_is (i, digit);
 }

   if (parent.isReviewMode()) { setReadOnly(NEF_format) };
}

function startUp()
{ 
//================

  var noOfDigits = numberOfDigits ();
  var dspan = isNetscape() ? spanNamed("digitForm") : spanNamed("digitSpan");
  
  setHandlers (noOfDigits);
}

var win2 ;
 
function showStatusMsg (aString)
{
	defaultStatus = aString;
}

function updateTotalAnswer ()
//  Helps to set the handlers as well as to 
//  collect the total answers. Specific to each page.
{ 
  var total = totalAnswer(); // calculates it.
  total = answerDigits ()  ;  
  // better to store the digits even if incomplete.
  
  if (total == null)
  { 	showStatusMsg("Your answer is incomplete!");
  } else
   {	showStatusMsg("Your answer:  " + total);  
   } 

}


function answerDigits ()   // #zw2.
{
 var noOfDigits     = numberOfDigits() ; 
 var noOfDecimals   = numberOfDecimals() ;
 var digitPower     = noOfDigits ;
 var total 			= "";
 var power  ;  
 var numberTotal 	= 0;
 
 for (i = 1; i <= noOfDigits ; i++)
 {	currentNumberValue = 0;	
 	var fvalue = valueOfFieldNoBasic (i);
    total = total + "" + fvalue;
	if (isNaN(fvalue)){
		currentNumberValue = 0 }
	else {
		currentNumberValue = Number(fvalue)
	}
	
	numberTotal+=currentNumberValue*Math.pow( 10, digitPower-i);
	
// alert(	'currentNumberValue: ' + currentNumberValue + '\n' +
// 		'fvalue: ' + fvalue + '\n' +
//		'digitPower: ' + digitPower + '\n' +
//		'digitPower-i: ' + (digitPower-i) + '\n' +
//		'total: ' + total + '\n' +
//		'numberTotal: ' + numberTotal + '\n' 
//		);
 }
 
 // Damon's stuff start

 var variablesArrayNumber = isNetscape ? MM_findObj('digitForm').variableNumber.value : MM_findObj('trigerButton').parentElement.all.variableNumber.value;
 var format = eval(variablesArray[variablesArrayNumber] + "_format");
 
 if (format == "111-11") {
 
	numberTotal_string = "";
	
 	for (i = 1; i <= noOfDigits ; i++) {
	
		fvalue = valueOfFieldNoBasic(i);
		if (fvalue == " " || fvalue == "") {
			fvalue = "0";
		} else {
			fvalue = fvalue + "";
		}
		numberTotal_string = numberTotal_string + fvalue;
  		
 	}
		
	theLeft = Number(numberTotal_string.substr(0,3));
	theRight = Number(numberTotal_string.substr(3,2));
	
	theLeftPart = theLeft / 100.0;
	theTickPart = theRight * 0.0003125;
	
	result = theLeftPart + theTickPart;
	
	return result;
	
 }
  
 // Damon's stuff end
 
 numberTotal = numberTotal/Math.pow(10,noOfDecimals);
  
 return numberTotal;
 
}

function totalAnswer ()
{
 var noOfDigits      = numberOfDigits() ; 
 var noOfDecimals    = numberOfDecimals() ;
 var digitPower     = noOfDigits - noOfDecimals - 1;
 var total = "";
 var power;  

 for (i = 1; i <= noOfDigits ; i++)
 {	var fvalue = valueOfFieldNo (i);
   
   if (fvalue == null)  return null;
   if (isNaN(fvalue)) return null;
  
  total = total + "" + fvalue;
  //digitPower--;
  //if (digitPower == -1) total = total + ".";
 }
 return total
}


function valueOfFieldNo_is (anInteger, aString)
{   var i = anInteger;
	var digitName = "digitSpan.document.digitForm.digit_" + i ;
	var field = elementNamed (digitName);
 
	 field.value  = aString  ; 
 
}

function valueOfFieldNoRESTORED (anInteger)
{   var i = anInteger;
    var result = restoredDigits_VAR.charAt(  anInteger - 1);
 
		return result;
}

function valueOfFieldNoBasic (anInteger)
{   var i = anInteger;
	var digitName = "digitSpan.document.digitForm.digit_" + i ;
	var field = elementNamed (digitName);
	if (field == null) 
	{ 
		return ""  ;
	 }
	var value = field.value   ; 
	return value;
}

function valueOfFieldNo (anInteger)
//===================================
// Return the fieldvalue as an integer
// or null if it contains no digit. 
{
 var i = anInteger; 
 var value =  valueOfFieldNoBasic (i);	
	
 if ((typeof value == "string")&&(value == "")) return null;  
 if (value == null)   return null ; 
 if (isNaN(value)) return null ; 
 if (value == " ")  return null ; 

 return value - 0;
}

function setHandlers (anInteger)
//==============================
{
 

 var noOfDigits = anInteger ; 
 for (i = 1; i <= noOfDigits ; i++)
 {	var digitName = "digitSpan.document.digitForm.digit_" + i ;
	var field = elementNamed (digitName);
	field ["onkeydown"] =  (_ISNETSCAPE)?myKeyDownHandler_NN:myKeyDownHandler;
	field ["onkeyup"] =  (_ISNETSCAPE)?myKeyUpHandler_MM:myKeyUpHandler;
	field ["onmouseover"] =  myMouseOverHandler;
 }
} 
 
 function setFocusToFirstCell ()
 {
 var nextFieldSpec 	= "digitSpan.document.digitForm." + "digit_1";
 var nextField 		= elementNamed(nextFieldSpec);
 nextField.focus();
 nextField.select();
}
 
function mouseDownOnField (anInput)
{ 
	anInput.select();
}

var previousValues_GLOBAL = new Object();
 var eventInput_GLOBAL;
  var value_GLOBAL;
  
  
function valueOf_is_ (aString1, aString2)
{	  previousValues_GLOBAL [aString1] = aString2;
      updateTotalAnswer();
}

function valueOf(aString1)
{
 var aString2 = previousValues_GLOBAL [aString1];
 if (aString2 == null) 
{ 
return "";
}
 return aString2;
}

function valueOf2(aString1)
{
 var aString2 = previousValues_GLOBAL [aString1];
 if (aString2 == null) 
{ return "";
}
 
 return aString2;
}
 
function eventSource (e)
 { var anObject ;
 	if (isNetscape())
    {anObject = e.target;
	
	} else
	{anObject = e.srcElement;
	}
	return anObject
 }
 
function keyCode (e)
 { var anObject ;
 	if (isNetscape())
    {anObject = e.which;
	
	} else
	{ 
	anObject = e.keyCode;
	}
	return anObject
 }
 
 function isUpArrow(anInteger)
 {  
  
 if (anInteger == 38)
 { 
  return true;
 }
 return false;
 } 

 function isDownArrow(anInteger)
 {  
  
 if (anInteger == 40)
 { 
  return true;
 }
 return false;
 } 
 
 function isBackArrow(anInteger)
 {  
  
 if (anInteger == 37)
 { 
  return true;
 }
 return false;
 } 
 
 function isForwardArrow(anInteger)
 {  
 if (anInteger == 39)
 { 
  return true;
 }
 return false;
 } 
 
 function isBackSpace(  anInteger)
 {  
 if (anInteger == 8) return true;
 if (anInteger == 37)
 {   return true;
 }
 return false;
 } 
 
 function isDigit(aString, anInteger)
//====================================
// Tells whether a keypress is acceptable
 { 
	if (anInteger == 38) return true;
	if (anInteger == 40) return true;
	if (anInteger == 39) return true;
	if (anInteger == 37) return true;
	if (anInteger == 9) return true;
	if (anInteger == 16) return true;
	if (anInteger == 8) return true;
	if (anInteger == 32) return true;
  
 var reg = new RegExp("\\d");
 var aBoolean = reg.test(aString);
 return aBoolean;
 } 
 
  
  
function myKeyPressHandler(e1)
//=============================
{	var e;
 
 	if (isNetscape()) 
 	{ e = e1 ; }
 else {e = window.event };
 
 }
 
 
 function mapLetterToDigit (aChar, kc)
 {
 
 if (isNetscape()) return aChar;
  if (kc == 96) return '0';
 if (aChar == 'a') return '1';
  if (aChar == 'b') return '2';
   if (aChar == 'c') return '3';
    if (aChar == 'd') return '4';
	 if (aChar == 'e') return '5';
	  if (aChar == 'f') return '6';
	   if (aChar == 'g') return '7';
	    if (aChar == 'h') return '8';
		 if (aChar == 'i') return '9';
return aChar;		   
 }
 

function selectInputNamed (anInput)
//============================
{
var spec =  "digitSpan.document.digitForm." + anInput ; 
var elem = elementNamed(spec);
elem.focus();
elem.select(); 
}

function resetInput (anInput)
//============================
{
	var spec =  "digitSpan.document.digitForm." + anInput ; 
	var elem = elementNamed(spec);
	if (!isNetscape())
	{elem.value = valueOf(elem.name);
	}
}


function myKeyDownHandler_NN(e1)
//=============================
{	
//	alert('NN');
	if (localIsReviewMode ())  
	{ 
			alertReview();
			return false; 
	} 

	var e;
 	if (isNetscape()) 
 		{ e = e1 ; }
	else {e = window.event };
	var kc 		= keyCode(e) ;
 
	if ( kc == 13)
	{
	 return kc;
	}

	var keyChar0 	= String.fromCharCode(kc);
	var keyChar 	= mapLetterToDigit(keyChar0, kc);
  
	var input 			= eventSource (e);
	eventInput_GLOBAL 	= input; 
	var value 			= input.value ;
 
	var oldValue ;

 
	if (!isNetscape()) {
		if (isBackArrow(kc)||isDownArrow(kc))
		{	//goToPreviousCell(input);
			return false;
		}
		if (isForwardArrow(kc)||isUpArrow(kc))
		{	goToNextCell(input);
			return true;
		}
		if (isBackSpace(kc)) {
			if (value == "") {
				goToPreviousCell(input);
				return true;
			} else {
				return true;
			}
		}
	} else {
		if (isBackSpace(kc)) {
		return true;
		}
		if (isBackArrow(kc)||isDownArrow(kc))
		{	goToPreviousCell(input);
			return false;
		}
		if (isForwardArrow(kc)||isUpArrow(kc))
		{	goToNextCell(input);
			return true;
		}

	}

	if ( keyCode(e) == 13) 
	{// enter
		if (! (pageType () == 'question'))
		{
			window.top.indicateCorrectnessBasic();
			flip = true;
			return true ;  
		}
	}
//	debARR = [isDigit(keyChar, keyCode(e)),keyChar];
//	debug(debARR);
	
	if (!isDigit(keyChar, keyCode(e)))
		{ 
			if (isNetscape())
			{
				// alert("nevermind");
				// + "\n         to go to next cell,"
				// + "\n         and Shift-Tab to go back."); 
			} else
			{
				// alert("nevermind");
				// + "TIP:  You can use the left and right"
				// + "\n         arrow-keys to move between"
				// + "\n         the cells." ); 
			}
 
			oldValue =  valueOf(input.name);
 
			if (!isNetscape()) input.value = ""
			input.focus();
			input.select();
			e.keyCode = 0;
//			setTimeout("resetInput('" + input.name + "')", 22);
			return false;
		} else {
			if (!isNetscape()) return true;
		}
	inputChanged(input)
	return true;
} 








function myKeyUpHandler_MM(e1)
//=========================
 {
  //alert(e1);
	if (localIsReviewMode ())  
	{ 
			alertReview();
			return false; 
	} 

	var e;
 	if (isNetscape()) 
 	{ e = e1 ; }
 	else 
	{e = window.event 
	};

	var kc 		= keyCode(e) ;
	var input = eventInput_GLOBAL ;  
	var value 			= input.value ;

// only for netscape
	if (isBackArrow(kc)||isDownArrow(kc))
		{	goToPreviousCell(input);
			return true;
		}

	if (isNetscape()) {	
		if (isForwardArrow(kc)) 
		{	goToNextCell(input);
			return true;
		}
		if (isBackSpace(kc) && value == "") 
		{	goToPreviousCell(input);
			return true;
		}
	}

	inputChanged(input);
	// DELETE
	userAnswerChanged();
    return true;
}


function myKeyDownHandler(e1)
//=============================
{	
//	alert('IE');
	var e;
 	if (isNetscape()) 
 		{ e = e1 ; }
	else {e = window.event };
	var kc 		= keyCode(e) ;
 
	if ( kc == 13)
	{
	 return kc;
	}

	var keyChar0 	= String.fromCharCode(kc);
	var keyChar 	= mapLetterToDigit(keyChar0, kc);
  
	var input 			= eventSource (e);
	eventInput_GLOBAL 	= input; 
	var value 			= input.value ;
 
	var oldValue ;

 
	if (!isNetscape()) {
		if (isBackArrow(kc)||isDownArrow(kc))
		{	//goToPreviousCell(input);
			return false;
		}
		if (isForwardArrow(kc)||isUpArrow(kc))
		{	goToNextCell(input);
			return true;
		}
		if (isBackSpace(kc)) {
			if (value == "") {
				goToPreviousCell(input);
				return true;
			} else {
				return true;
			}
		}
	} else {
		if (isBackSpace(kc)) {
		return true;
		}
		if (isBackArrow(kc)||isDownArrow(kc))
		{	goToPreviousCell(input);
			return false;
		}
		if (isForwardArrow(kc)||isUpArrow(kc))
		{	goToNextCell(input);
			return true;
		}

	}

	if ( keyCode(e) == 13) 
	{// enter
		if (! (pageType () == 'question'))
		{
			window.top.indicateCorrectnessBasic();
			flip = true;
			return true ;  
		}
	}
//	debARR = [isDigit(keyChar, keyCode(e)),keyChar];
//	debug(debARR);
	
	if (!isDigit(keyChar, keyCode(e)))
		{ 
			if (isNetscape())
			{
				// alert("nevermind");
				// + "\n         to go to next cell,"
				// + "\n         and Shift-Tab to go back."); 
			} else
			{
				// alert("nevermind");
				// + "TIP:  You can use the left and right"
				// + "\n         arrow-keys to move between"
				// + "\n         the cells." ); 
			}
 
			oldValue =  valueOf(input.name);
 
			if (!isNetscape()) input.value = ""
			input.focus();
			input.select();
			e.keyCode = 0;
//			setTimeout("resetInput('" + input.name + "')", 22);
			return false;
		} else {
			if (!isNetscape()) return true;
		}
	inputChanged(input)
	return true;
} 


function myKeyUpHandler(e1)
//=========================
 {
  //alert(e1);
 
	var e;
 	if (isNetscape()) 
 	{ e = e1 ; }
 	else 
	{e = window.event 
	};

	var kc 		= keyCode(e) ;
	var input = eventInput_GLOBAL ;  
	var value 			= input.value ;

// only for netscape
	if (isBackArrow(kc)||isDownArrow(kc))
		{	goToPreviousCell(input);
			return true;
		}

	if (isNetscape()) {	
		if (isForwardArrow(kc)) 
		{	goToNextCell(input);
			return true;
		}
		if (isBackSpace(kc) && value == "") 
		{	goToPreviousCell(input);
			return true;
		}
	}

	if(!isNetscape()) inputChanged(input);
	// DELETE
	userAnswerChanged();
    return true;
}

function getEvent (eventOrNull)
{
	var e;
 	if (isNetscape()) 
 	{ e = eventOrNull ; }
 	else 
	{e = window.event 
	};
return e;
}

function myMouseOverHandler(e1)
//=========================
 {
	var e = getEvent(e1) ;
    return true;
}

function goToPreviousCell(anInput)
//==============================
{
 
var nameArray 		= anInput.name.split("_")   ;
var fieldIndex 		= nameArray[1] - 0 ;
var nextFieldIndex 	= fieldIndex - 1 ;
var nextFieldName 	= "digit_" + nextFieldIndex ; 
var nextFieldSpec 	= "digitSpan.document.digitForm." + nextFieldName;
var nextField 		= elementNamed(nextFieldSpec);

if (nextField == null)
{return true;
}

focusField(nextField);   

return true ;
}

function goToNextCell(anInput)
{ 
var nameArray 		= anInput.name.split("_")   ;
var fieldIndex 		= nameArray[1] - 0 ;
var nextFieldIndex 	= fieldIndex + 1 ;
var nextFieldName 	= "digit_" + nextFieldIndex ; 
var nextFieldSpec 	= "digitSpan.document.digitForm." + nextFieldName;
var nextField 		= elementNamed(nextFieldSpec);

if (nextField == null)
{return true;
}
focusField(nextField);
return true ;
}

function focusField(anInput)
//===========================
{  
	defaultStatus = '�2002 Zoologic, Inc.';
	anInput.focus();			  
	anInput.select();
}


function inputChanged(anInput)
{ 
if (anInput == null) return true;

if(anInput.value == "") { 
valueOf_is_(anInput.name, "");
return true;
}

var nameArray 		= anInput.name.split("_")   ;
var fieldIndex 		= nameArray[1] - 0 ;
var nextFieldIndex 	= fieldIndex + 1 ;
var nextFieldName 	= "digit_" + nextFieldIndex ; 
var nextFieldSpec 	= "digitSpan.document.digitForm." + nextFieldName;
var nextField 		= elementNamed(nextFieldSpec);

valueOf_is_(anInput.name, anInput.value);
if (nextField == null)
{ return true;
}
   
focusField(nextField)
return true ;
}

function isNetscape ()
{
	var 	appName 	=  navigator.appName.toLowerCase();
	var 	index   	= appName.indexOf("netscape");
	var 	aBoolean 	= (index >= 0);
	return 	aBoolean; 
}

function writeToSpanNamed(aString)
{
	var aSpan = spanNamed(aString);
	if (isNetscape())
	{	aSpan.document.writeln( "<b>Hello</b>");
    	aSpan.document.close();
	} else
	{ 
		document.all.innerHTML = "<b>Hello</b>";
	}
} 

 
function elementNamed (aString1 )
//==============================
// Works cross-browser to return an element when
// you specify its path in Netscape -format.
// EXAMPLE:
//  elementNamed("digitSpan.document.digitForm.digit_2" );
//
{	
	var arr = aString1.split(".");
	var last = arr[arr.length - 1];
	var anObject;
	if (isNetscape())
	{	anObject  = MM_findObj(last) ;
	} else
	{	
	 anObject  = eval("document.all." + last);
	}
	return anObject;
}



function spanNamed (aString)
//==========================
{	var aSpan;
	if (isNetscape())
	{	aSpan = spanNamedNetscape(aString);
	} else
	{	
	aSpan = spanNamedIe(aString);
	}
	return aSpan;
}

function spanNamedNetscape (aString)
{	var stringToEval = "document.NTS_InputDialogue.document.forms.digitForm." + aString;
 
 	var aSpan = MM_findObj(aString);
	return aSpan;
}


function spanNamedIe (aString) 
{ 	var aSpan = eval("document.all." + aString); // + ".style")  
	return aSpan;
}