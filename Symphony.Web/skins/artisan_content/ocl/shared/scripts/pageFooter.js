// Auto updating page footer

function off_renderPageFooter() {
	//do nothing!!!
	
}

function renderPageFooter() {
	
	var dateObject=new Date();
	var year=dateObject.getYear();
	if (year < 1000) year+=1900;
//	var day=dateObject.getDay();
//	var month=dateObject.getMonth();
//	var daymonth=dateObject.getDate();
//	if (daymonth<10) daymonth="0"+daymonth;
//	var dayarray=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
//	var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
	
footerHTML = '<table width="600" border="0" cellpadding="0" cellspacing="0" class="tableFooter">';
footerHTML+= '<tr bgcolor="#000000">';
footerHTML+= '<td height="1"><img src="images/spacer.gif" width="1" height="1"></td>';
footerHTML+= '</tr>';
footerHTML+= '<tr bgcolor="#FFFFFF">';
footerHTML+= '<td height="5"><img src="images/spacer.gif" width="1" height="1"></td>';
footerHTML+= '</tr>';
footerHTML+= '<tr>';
footerHTML+= '<td width="16%"><div align="center"><span class="pageFooter">Copyright &copy; '+year+' SS&amp;C Technologies, Inc. All rights reserved.</span></div></td>';
footerHTML+= '</tr>';
footerHTML+= '<tr bgcolor="#FFFFFF"><td><img src="images/spacer.gif" width="1" height="15"></td>';
footerHTML+= '</tr>';
footerHTML+= '</table>';

	document.write(footerHTML);

}

function renderPageFooter_simple() {
	
	var dateObject=new Date();
	var year=dateObject.getYear();
	if (year < 1000) year+=1900;
//	var day=dateObject.getDay();
//	var month=dateObject.getMonth();
//	var daymonth=dateObject.getDate();
//	if (daymonth<10) daymonth="0"+daymonth;
//	var dayarray=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
//	var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
	
footerHTML= '<div align="center"><span class="pageFooter">Copyright &copy; '+year+' SS&amp;C Technologies, Inc. All rights reserved.</span></div>';

	document.write(footerHTML);

}