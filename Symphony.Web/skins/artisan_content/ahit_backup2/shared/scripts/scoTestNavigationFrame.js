/***********************************************************\
 **
 ** FILE: 		sco_00/scripts/mavigationFrame.js
 **
 ** WHAT:		Functions or wrappers called by
 **              navigationFrame.htm to implement
 **              WITHIN-SCO navigation.
 **
 ** WHY:			This is part of the Zoologics template
 **				for creating an SCO.
 **
 \********************************************************* */
function showCourseTitle(htmlString){
    // called by the frameset when it has loaded
    
    // oleg
    if (true) 
        return;
    
    var pattern = /index.htm/;
    var gurl = parent.document.location.toString().replace(pattern, 'images/coursetitle.gif');
    
    var html = '<img  height=24 src="' + gurl + '">';
    
    
    var st = getsp("courseTitleSpan");
    
    if (st.innerHTML) {
        st.innerHTML = html;
    }
    else {
        st.document.writeln(html);
        st.document.close();
    }
    
}

function testNavigationFrameLoaded(){

    MM_preloadImages("images/notchanged.gif", "images/changed.gif", "images/correct.gif", "images/incorrect.gif", "images/noanswer.gif", 'images/spacer.gif', 'images/submit.gif', 'images/delete.gif', 'images/exit.gif', 'images/results.gif', 'images/back.gif', 'images/firstaid.gif', 'images/prefs.gif', 'images/refresh.gif');
    buildToolBar();
    
    if (isIE) {
        noqs = getNumberOfQuestions();
        checkNextPreviousButtons(parent.currentPageIndex(), noqs);
    }
    
    parent.navbarLoaded(); // #sco130. For netscape we must tell parent when everything is ready
    // do this after toolbar is built.

}


function gotoTOC(){
    // do nothing
    // loadAPage(1); 

}


var hideStackVAR = new Array();
hideStackVAR[0] = 0; // top of stack;
function hideStack(){
    return hideStackVAR;
}


function hideStackIsEmpty(aString){
    var hs = hideStack();
    var toppen = hs[0];
    
    
    if (toppen <= 0) {
        return true;
    }
    
    return false;
}

function pushToHideStack(aString){
    var hs = hideStack();
    var toppen = hs[0];
    toppen = toppen + 1;
    hs[0] = toppen;
    hs[toppen] = aString;
    
    
}


function popHideStack(aString){
    var hs = hideStack();
    var toppen = hs[0];
    if (toppen == 0) 
        return;
    
    toppen = toppen - 1;
    hs[0] = toppen;
    if (toppen <= 0) {
        toggleNextAndPrevious();
    }
    
}


function outOfSequenceHyperlink(aString){

    if (hideStackIsEmpty()) {
    
        toggleNextAndPrevious();
        
    }
    else {
    };
    
    pushToHideStack(aString);
    
    //  setTimeout('hideNextAndPrevious ()' , 10000);

}

function go_back(){
    var aSco = parent.getSco();
    if (aSco == null) {
        return;
    }
    popHideStack();
    
    
    return aSco.goBack();
}


function toggleNextAndPrevious(){

    // do nothing
    //	reverseVisibility('OnLayer'); //
    //	reverseVisibility('OffLayer'); // this lines had replaced all the code below to make it work with nn and ie

    //if(document.all) {
    //  if(  document.all.OnLayer.style.display == "none")
    //    {	document.all.OnLayer.style.display   = "inline";
    //		document.all.OffLayer.style.display  = "none";
    //	 
    //	}
    //	else 
    //    {	document.all.OnLayer.style.display   ="none";
    //		document.all.OffLayer.style.display ="inline";
    // 
    //	}
    //} else {
    //	// netscape implementation of switching visibility - carlos
    //	reverseVisibility('OnLayer');
    //	reverseVisibility('OffLayer');
    //}

}


function refresh(){
    parent.location = parent.location;
    
}



function gotoPageEntered(){
    checkboxClicked();
    
}

function checkboxClicked(){

    var pf;
    
    if (document.all) 
        pf = document.all.pageNoField;
    if (!pf) 
        pf = getsp('OnLayer').document.forms[0].pageNoField; // added the getsp('OnLayer') to make this work with netscape - carlos
    var targetPno = pf.value - 0;
    if (targetPno <= 0) 
        return;
    
    
    loadAPage(targetPno);
    
}




/*************************************************\
 **
 ** FUNCTION(s): 	loadNextPage()   & loadPreviousPage()
 ** Called when the button is pressed.
 ** Forwards the call to the parent (i.e. the frameset).
 **
 \***/
// MACROMEDIA SUPPORT FUNCTIONS

function MM_preloadImages(){ //v3.0
    var d = document;
    if (d.images) {
        if (!d.MM_p) 
            d.MM_p = new Array();
        var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
        for (i = 0; i < a.length; i++) 
            if (a[i].indexOf("#") != 0) {
                d.MM_p[j] = new Image;
                d.MM_p[j++].src = a[i];
            }
    }
}

function MM_reloadPage(init){ //reloads the window if Nav4 resized
    if (init == true) 
        with (navigator) {
            if ((appName == "Netscape") && (parseInt(appVersion) == 4)) {
                document.MM_pgW = innerWidth;
                document.MM_pgH = innerHeight;
                onresize = MM_reloadPage;
            }
        }
    else 
        if (innerWidth != document.MM_pgW || innerHeight != document.MM_pgH) 
            location.reload();
}

MM_reloadPage(true);

function MM_swapImgRestore(){ //v3.0
    var i, x, a = document.MM_sr;
    for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) 
        x.src = x.oSrc;
}

function MM_findObj(n, d){ //v4.01
    var p, i, x;
    if (!d) 
        d = document;
    if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document;
        n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) 
        x = d.all[n];
    for (i = 0; !x && i < d.forms.length; i++) 
        x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) 
        x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) 
        x = d.getElementById(n);
    return x;
}

function MM_swapImage(){ //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments;
    document.MM_sr = new Array;
    for (i = 0; i < (a.length - 2); i += 3) 
        if ((x = MM_findObj(a[i])) != null) {
            document.MM_sr[j++] = x;
            if (!x.oSrc) 
                x.oSrc = x.src;
            x.src = a[i + 2];
        }
}

function MM_setTextOfLayer(objName, x, newText){ //v4.01
    if ((obj = MM_findObj(objName)) != null) 
        with (obj) 
if (document.layers) {
            document.write(unescape(newText));
            document.close();
        }
        else 
            innerHTML = unescape(newText);
}


// QUESTION BUILDER FUNCTIONS

qStateImageArray = ["", "notchanged.gif", "changed.gif", "correct.gif", "incorrect.gif", "noanswer.gif"];

function buildQuestionCell(qNumber, qState){

    qState = getPageNoState(qNumber);
    //currentQuestion = (currentQuestion==null)?1:currentQuestion;
    isCurrentSRC = (getQuestionNumber() == qNumber) ? (sharedImgFolder + "current.gif") : (sharedImgFolder + "nounderscore.gif");
    // create a new cell and the table that contains this question 
    openCellHTML = '<td><table border="0" cellpadding="0" cellspacing="0" width="24"><tr><td><img name="topq" src="' + sharedImgFolder + 'topq.gif" width="24" height="4" border="0" alt=""></td></tr><tr><td>';
    // create the mini table that has the elements like the number, state and underlining
    openQuestionHTML = '<table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="24"><tr><td>'
    // state image :: checkmark, square, etc
    stateImage = '<img id="qstateimg_' + qNumber + '" name="qstateimg_' + qNumber + '" src="' + sharedImgFolder + qStateImageArray[qState] + '" width="10" height="15" border="0" alt="' + "go to the question number " + qNumber + ' in this test">';
    midHTML1 = '</td><td>';
    // question number
    openQuestionAction = '<a href="javascript:scoGoToQuestion(' + qNumber + ');">';
    
    numberImage = '<img src="' + sharedImgFolder + qNumber + '.gif" width="14" height="15" border="0" alt="' + "go to the question number " + qNumber + ' in this test">';
    closeQuestionAction = '</a>';
    midHTML2 = '</td></tr><tr><td colspan="2">';
    // underline image
    isCurrentImage = '<img id="qmarkimg_' + qNumber + '" name="qmarkimg_' + qNumber + '" src="' + isCurrentSRC + '" width="24" height="2" border="0" alt="">';
    // close question HTML
    closeQuestionHTML = '</td></tr></table>';
    // close cellHTML
    closeCellHTML = '</td></tr><tr><td><img name="bottomq" src="' + sharedImgFolder + 'bottomq.gif" width="24" height="14" border="0" alt=""></td></tr></table></td>';
    // assemble all the html and return
    qCellHTML = openCellHTML + openQuestionHTML + openQuestionAction + stateImage + closeQuestionAction + midHTML1 + openQuestionAction + numberImage + closeQuestionAction + midHTML2 + isCurrentImage + closeQuestionHTML + closeCellHTML;
    
    return qCellHTML;
}

function buildQuestionTable(){
    openqTableHTML = '<table border="0" cellpadding="0" cellspacing="0" ><tr>';
    closeTableHTML = '</tr></table>';
    qSpacerHTML = '<td><img src="' + sharedImgFolder + 'qspacer.gif" width="10" height="35"></td>';
    
    cellsHTML = '';
    
    for (i = 1; i <= getNumberOfQuestions(); i++) {
        cellsHTML += buildQuestionCell(i);
        cellsHTML += (i <= getNumberOfQuestions()) ? qSpacerHTML : '';
    }
    
    qTableHTML = openqTableHTML + cellsHTML + closeTableHTML;
    return qTableHTML;
}

// buttons

function buildButton(buttonNumber){

    buttonType = buttonArray[buttonNumber];
    imgName = buttonNames[buttonType];
    imgSrc = sharedImgFolder + imgName;
    imgAction = buttonActions[buttonType];
    imgToolTip = buttonToolTips[buttonType];
    isDisabled = !(buttonIsDisabled[buttonNumber]);
    HTML = '';
    
    if (!buttonArray[buttonNumber]) 
        return HTML;
    
    if (isDisabled) {
    
        HTML += '<a ';
        HTML += 'onMouseUp="document.images[\'' + imgName + '\'].src=\'' + imgSrc + '.gif\'"';
        HTML += 'onMouseDown="document.images[\'' + imgName + '\'].src=\'' + imgSrc + '_down.gif\'"';
        HTML += 'onMouseOver="document.images[\'' + imgName + '\'].src=\'' + imgSrc + '_over.gif\';document.images[\'feedback\'].src=\'images/feedbackbar_' + buttonType + '.gif\'"';
        //	HTML += 'onClick="' + imgAction + '"';
        HTML += 'onMouseOut="document.images[\'' + imgName + '\'].src=\'' + imgSrc + '.gif\';document.images[\'feedback\'].src=\'images/feedbackbar_0.gif\'"';
        HTML += 'href="javascript:' + imgAction + '">';
        HTML += '<img name="' + imgName + '" src="' + imgSrc + '.gif" width="38" height="35" border="0" alt="' + imgToolTip + '">';
        HTML += '</a>';
        
    }
    else {
        HTML = '<img name="submit" src="' + imgSrc + '_off.gif" width="38" height="35" border="0">';
    }
    
    return HTML;
}

var previousQButtonHTML = '<a href="javascript:scoGoToPreviousQuestion()" onMouseDown="MM_swapImage(\'previous\',\'\',\'images/previous_down.gif\',1)" onMouseUp="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'previous\',\'\',\'images/previous_over.gif\',1);" onMouseOut="MM_swapImgRestore();"><img src="images/previous.gif" alt="previous question" name="previous" width="30" height="35" border="0" onMouseOut="MM_swapImgRestore()"></a>';
var nextQButtonHTML = '<a href="javascript:scoGoToNextQuestion()" onMouseDown="MM_swapImage(\'next\',\'\',\'images/next_down.gif\',1)" onMouseUp="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'next\',\'\',\'images/next_over.gif\',1);" onMouseOut="MM_swapImgRestore();"><img src="images/next.gif" alt="next question" name="next" width="30" height="35" border="0" onMouseOut="MM_swapImgRestore()"></a>';

var navButtonHTML = ['', previousQButtonHTML, nextQButtonHTML]

function buildNavButton(buttonType){
    return navButtonHTML[buttonType]
}

function buildToolBar_OLD(){
    //	alert ('getButtons()');
    getButtons();
    
    // buils the division line on top of the toolbar
    divisionTableHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    divisionTableHTML += '<tr><td bgcolor="#336699"><img src="images/spacer.gif" width="1" height="1"></td></tr>';
    divisionTableHTML += '<tr><td bgcolor="#FFFFFF"><img src="images/spacer.gif" width="1" height="4"></td></tr>';
    divisionTableHTML += '</table>';
    divisionTableHTML = escape(divisionTableHTML);
    
    // builds the toolbar
    toolBarHTML = '<table border="0" cellpadding="0" cellspacing="0">';
    toolBarHTML += '<tr><td width="5"><img src="images/spacer.gif" width="1" height="4"></td>';
    toolBarHTML += '<td>' + buildButton(1) + '</td>';
    toolBarHTML += '<td width="2"><img src="images/spacer.gif" width="1" height="4"></td>';
    toolBarHTML += '<td>' + buildButton(2) + '</td>';
    toolBarHTML += '<td><img src="images/feedbackbar_0.gif" name="feedback" width="130" height="35" id="feedback"></td>';
    toolBarHTML += '<td>' + buildButton(3) + '</td>';
    toolBarHTML += '<td width="2"><img src="images/spacer.gif" width="1" height="4"></td>';
    toolBarHTML += '<td>' + buildButton(4) + '</td>';
    toolBarHTML += '<td>' + buildNavButton(1) + '</td>';
    toolBarHTML += '<td width="16"><img src="images/openleft.gif" alt="" name="openleft" width="16" height="35" border="0"></td>';
    toolBarHTML += '<td width="17">' + buildQuestionTable() + '</td>';
    toolBarHTML += '<td width="16"><img name="closeright" src="images/closeright.gif" width="16" height="35" border="0" alt=""></td>';
    toolBarHTML += '<td>' + buildNavButton(2) + '</td>';
    toolBarHTML += '</tr></table>';
    toolBarHTML = escape(toolBarHTML);
    
    navigationSpanInsideHTML = divisionTableHTML + toolBarHTML;
    
    MM_setTextOfLayer('navigation', '', navigationSpanInsideHTML);
    
}

var toolBarInitialized = false;

function buildToolBar(){

    var nofButtons = 4;
    var imgWidth = 38;
    var navButtonWidth = 30;
    var imgHeight = 35;
    getButtons();
    
    for (i = 1; i <= nofButtons; i++) {
    
        buttonType = buttonArray[i];
        isOff = (buttonIsDisabled[i]);
        isShowing = !buttonType;
        buttonName = (isOff) ? buttonNames[buttonType] + '_off' : buttonNames[buttonType];
        buttonImg = (isShowing) ? (sharedImgFolder + 'spacer.gif') : (sharedImgFolder + buttonName + '.gif');
        
        buttonObject = MM_findObj('button' + i);
        buttonObject.src = buttonImg;
        
        finalWidth = (i <= 4) ? (imgWidth) : (navButtonWidth);
        
        if (isIE) 
            (buttonObject.height = (isShowing) ? 1 : imgHeight);
        if (isIE) 
            (buttonObject.width = (isShowing) ? 1 : finalWidth);
        buttonObject.alt = buttonToolTips[buttonType];
        
    }
    
    // set positions for qspan and nextq
    // each additional question 34 pixels
    
    var buttonCount = 0;
    for (i = 1; i <= 4; i++) {
        if (buttonArray[i]) 
            buttonCount++;
    }
    
    setLayout();
    
    MM_setTextOfLayer('qspan', '', buildQuestionTable());
    toolBarInitialized = true;
}


function mouseUpButton(buttonNumber){

    buttonType = buttonArray[buttonNumber];
    isOff = (buttonIsDisabled[buttonNumber]);
    isShowing = !buttonType;
    buttonName = (isOff) ? buttonNames[buttonType] + '_off' : buttonNames[buttonType];
    buttonImg = (isShowing) ? (sharedImgFolder + 'spacer.gif') : (sharedImgFolder + buttonName + '.gif');
    
    buttonObject = MM_findObj('button' + buttonNumber);
    buttonObject.src = buttonImg;
    
}

function mouseDownButton(buttonNumber){

    buttonType = buttonArray[buttonNumber];
    isOff = (buttonIsDisabled[buttonNumber]);
    isShowing = !buttonType;
    buttonName = (isOff) ? buttonNames[buttonType] + '_off' : buttonNames[buttonType] + '_down';
    buttonImg = (isShowing) ? (sharedImgFolder + 'spacer.gif') : (sharedImgFolder + buttonName + '.gif');
    
    buttonObject = MM_findObj('button' + buttonNumber);
    buttonObject.src = buttonImg;
    
}

function mouseOverButton(buttonNumber){

    buttonType = buttonArray[buttonNumber];
    isOff = (buttonIsDisabled[buttonNumber]);
    isShowing = !buttonType;
    buttonName = (isOff) ? buttonNames[buttonType] + '_off' : buttonNames[buttonType] + '_over';
    buttonImg = (isShowing) ? (sharedImgFolder + 'spacer.gif') : (sharedImgFolder + buttonName + '.gif');
    
    //	alert("over it");
    buttonObject = MM_findObj('button' + buttonNumber);
    buttonObject.src = buttonImg;
    
    if (!isOff) {
        feedBackObject = MM_findObj('feedback');
        feedBackObject.src = 'images/feedbackbar_' + buttonType + ".gif";
    }
    
}

function mouseOutButton(buttonNumber){

    buttonType = buttonArray[buttonNumber];
    isOff = (buttonIsDisabled[buttonNumber]);
    isShowing = !buttonType;
    buttonName = (isOff) ? buttonNames[buttonType] + '_off' : buttonNames[buttonType];
    buttonImg = (isShowing) ? (sharedImgFolder + 'spacer.gif') : (sharedImgFolder + buttonName + '.gif');
    
    buttonObject = MM_findObj('button' + buttonNumber);
    buttonObject.src = buttonImg;
    
    if (!isOff) {
        feedBackObject = MM_findObj('feedback');
        feedBackObject.src = 'images/feedbackbar_0.gif';
    }
}



function scoButtonAction(buttonNumber){

    buttonType = buttonArray[buttonNumber];
    imgAction = buttonActions[buttonType];
    isDisabled = (buttonIsDisabled[buttonNumber]);
    
    if (!isDisabled) 
        eval(imgAction);
    else 
        return;
}

function getButtons(){

    // creates an array with the required buttons
    // maximum number of buttons is 4
    // if less the space is left blank
    
    buttonArray = [0, 1, 2, 8, 0];
    buttonIsDisabled = [false, false, false, false, false];
    
    if (!testMode()) 
        return;
    
    if (testMode() == 1) {
        // Test Mode
        buttonArray = [0, 1, 2, 8, 0];
    }
    else {
        if (testMode() == 2) {
            // Review Mode
            buttonArray = [0, 3, 4, 6, 8];
            if (isRemediation()) {
                buttonArray = [0, 3, 4, 5, 8];
            }
            if (isReportPage()) {
                buttonArray = [0, 3, 4, 6, 8];
                buttonIsDisabled = [false, false, true, true, false]
            }
            
        }
    }
    
    return true;
}

