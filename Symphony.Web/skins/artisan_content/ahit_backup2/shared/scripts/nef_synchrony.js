// **************************************************************************
// Carlos Code

function getDigits( aString ) {
	var length = aString.length;
	var digits = 0;
	for (i=0;i<length;i++) {
		if( aString.substr(i,1)=="1" )	{ digits++ };
	}
	return digits;
//	alert("digits: "+digits);
}

function getDecimals( aString ) {
	var length = aString.length;
	var stringArray = aString.split('.');
	var decimalPlace = 0;
//	alert ( 'stringArray :\n' + stringArray + '\n' + stringArray.length );
	if (stringArray.length == 1) {
		return 0;
	} else {
		decimalPlaces = getDigits(stringArray[1]);
		return decimalPlaces;
	}
}

//  End Carlos
// **************************************************************************


// Assume that generated code always creates 'digitSpan' layer for numeric field
function getDocument() {
	if (document.all) // IE
		return document;
	else
		return document.digitSpan.document;
}

// Page interface functions

function getUserAnswer() {
	return NAF_GetValue();
}

function setUserAnswer(value) {
	NAF_SetValue(value);
}

function disableAnswerWidgets() {
	NAF_Disable();
}

function displayNumericAnswerField(mask) {
	NAF_SetMask(mask);
	NAF_OnChange('userAnswerChanged()');
	NAF_Display();
}

function getEvaluativeFeedback() {
	return '';
}

// Numeric anwer field functions

var NAF_digits = 0;
var NAF_result = 0;
var NAF_mask = '1,111.11';
var NAF_value = '';
var NAF_isOpen = false;
var NAF_isDisabled = false;
var NAF_onChange = null;

function NAF_GetValue() {
	return (NAF_value == '' ? null : NAF_value);
}

function NAF_SetValue(newValue) {
	NAF_value = (newValue == null ? '' : newValue);
	if (NAF_isOpen)
		NAF_ShowValue();
}

function NAF_SetMask(newMask) {
	NAF_mask = newMask;
}

function NAF_OnChange(str) {
	NAF_onChange = str;
}

function NAF_Disable() {
	NAF_isDisabled = true;
	if (NAF_isOpen)
		NAF_DisableDigits();
}


function NAF_Display() {
	var openForm = '<form id=digitForm name=digitForm>';
	var closeForm = '</form>';
	
	var text = '';

	text += '<table id="answerWidget" name="answerWidget" border="0" cellpadding="0" cellspacing="0">';
	text += '<tr>';

	text += '<td nowrap>';
	text += '<font face="Arial"><b>';
	text += 'Answer =&nbsp;';
	text += '</b></font>';
	text += '</td>';

	var decimalPart = false;
	for (var i = 0; i < NAF_mask.length; i++) {
		var ch = NAF_mask.substr(i, 1);
		switch (ch) {
			case '9':
			case '1':
				text += '<td nowrap>';
				text += '<input type="text"';
				if (document.all) // IE
					text += ' class="numericInputFieldIE"';
				else
					text += ' class="numericInputFieldNN"';
				text += ' name="NAF_Digit' + NAF_digits + '" id="NAF_Digit' + NAF_digits + '"';
				text += ' autocomplete="off" maxlength="1" size="1"';
				text += ' onfocus="select()"';
				if (document.all) //IE
					text += ' onkeydown="NAF_KeyDownIE(' + NAF_digits + ')"';
				text += '>';
				text += '</td>';
				NAF_digits++;
				break;
			case ' ':
				text += '<td nowrap>';
				text += '<font face="Arial"><b>&nbsp;';
				text += '</b></font>';
				text += '</td>';
				break;
			default:
				text += '<td nowrap>';
				text += '<font face="Arial"><b>';
				text += ch;
				text += '</b></font>';
				text += '</td>';
				break;
		}
	}

	text += '<td nowrap>&nbsp;&nbsp;</td>';

	text += '<td nowrap>';

	text += '</tr>';
	text += '</table>';
	text = openForm + text + closeForm;
	alert(text);
	MM_setTextOfLayer_original('digitSpan','',text);
//	getDocument().write(text);
//	getDocument().close();
	setTimeout('NAF_PostOpen()', 1);
}

function NAF_PostOpen() {
	// Do this separately for Netscape, which does not allow to specify onKeyDown handler inside <input> tag
	if (! document.all) {
		for (var i = 0; i < NAF_digits; i++)
			NAF_GetDigit(i).onkeydown = NAF_KeyDownNN;
	}

	NAF_isOpen = true;
	NAF_ShowValue();
	if (NAF_isDisabled)
		NAF_DisableDigits();
	else
		NAF_SetFocusTo(0);
}

function NAF_GetDigit(index) {
	return eval('getDocument().forms.digitForm.NAF_Digit' + index);
}

function NAF_CalculateValue() {
	NAF_value = '';
	var allEmpty = true;

	var noOfDigits     = NAF_digits; 
	var noOfDecimals   = getDecimals(NAF_mask) ;
	var digitPower     = noOfDigits-1 ;
	var total 			= "";
	var numberTotal 	= 0;

	 for (var i = 0; i < noOfDigits; i++)
	 {	currentNumberValue = 0;
	 	var fvalue = NAF_GetDigit(i).value;
	    total = total + "" + fvalue;
		if (isNaN(fvalue)){
			currentNumberValue = 0 }
		else {
			currentNumberValue = Number(fvalue)
		}
		
		numberTotal+=currentNumberValue*Math.pow( 10, digitPower-i);
	 }
	numberTotal = numberTotal/Math.pow(10,noOfDecimals);
//	alert ('numberTotal:: ' + numberTotal);
	NAF_value = numberTotal;

	// Set empty string (no result) if all fields are empty
	if (allEmpty) {
		NAF_value = '';
		return;
	}
}

function NAF_ShowValue() {
	if (NAF_value == '') {
		for (var i = NAF_digits - 1; i >= 0; i--)
			NAF_GetDigit(i).value = '';
	}
	else {
		for (var i = NAF_digits - 1, j = NAF_value.length - 1; i >= 0 && j >= 0; i--, j--)
			NAF_GetDigit(i).value = NAF_value.substr(j, 1);
		for (var i = NAF_digits - NAF_value.length - 1; i >= 0; i--)
			NAF_GetDigit(i).value = '0';
	}
}

function NAF_KeyDownIE(index) {
	var keyCode = window.event.keyCode;
	var result = NAF_PrivateKeyDown(keyCode, index);
	if (result == false)
		window.event.returnValue = false;
	return result;
}

function NAF_KeyDownNN(e) {
	if (e.target.name.substr(0, 9) != 'NAF_Digit')
		return false;
	var index = parseInt(e.target.name.substr(9), 10);
	if (index == NaN)
		return false;
	var keyCode = e.which;
	return NAF_PrivateKeyDown(keyCode, index);
}

function NAF_PrivateKeyDown(keyCode, index) {
	// numeric keys '0' to '9' (regular and extended)
	if (keyCode >= 48 && keyCode <= 57 || keyCode >= 96 && keyCode <= 105) {
		setTimeout('NAF_ValueChanged()', 1);
		var next = index + 1;
		NAF_SetFocusTo(next);
		return true;
	}
	// BACKSPACE
	if (keyCode == 8) {
		setTimeout('NAF_ValueChanged()', 1);
		NAF_SetFocusTo(index - 1);
		return true;
	}
	// LEFT ARROW
	if (keyCode == 37) {
		NAF_SetFocusTo(index - 1);
		return true;
	}
	// RIGHT ARROW
	if (keyCode == 39) {
		NAF_SetFocusTo(index + 1);
		return true;
	}
	// HOME
	if (keyCode == 36) {
		NAF_SetFocusTo(0);
		return true;
	}
	// END
	if (keyCode == 35) {
		NAF_SetFocusTo(NAF_digits - 1);
		return true;
	}
	// DEL
	if (keyCode == 46) {
		setTimeout('NAF_ValueChanged()', 1);
		return true;
	}
	// TAB, ENTER
	if (keyCode == 9 || keyCode == 13)
		return true;

	return false;
}

function NAF_ValueChanged() {
	NAF_CalculateValue();
	OnFieldChange();
	if (NAF_onChange != '')
		eval(NAF_onChange);
}

function NAF_DisableDigits() {
	for (var i = 0; i < NAF_digits; i++)
		NAF_GetDigit(i).disabled = true;
}

function NAF_SetFocusTo(index) {
	setTimeout('NAF_PrivateSetFocusTo(' + index + ')', 1);
}

function NAF_PrivateSetFocusTo(index) {
	if (! NAF_isDisabled && 0 <= index && index < NAF_digits)
		NAF_GetDigit(index).focus();
}
