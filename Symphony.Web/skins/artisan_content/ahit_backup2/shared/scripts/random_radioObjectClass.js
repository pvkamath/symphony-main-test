//Random Objects declaration

function randomObject(bottomValue,topValue,precision) {
	this.precision = precision;
	this.bottomValue = bottomValue;
	this.topValue = topValue;
	this.value = getRandom(bottomValue,topValue,precision);
	this.generate = generate;
	this.updateRange = updateRange;
	this.setValue = setValue;
	this.toString = randomObject_toString;
	this.formatString = formatStringOne;
}

function formatStringOne(roundToPlaces,commas,symbol) {
	return formatString(this.value,roundToPlaces,commas,symbol);
}

function generate() {
	this.value = getRandom(this.bottomValue,this.topValue,this.precision);
}

function updateRange(bottom,top,precision) {
	this.bottomValue = (bottom)?bottom:this.bottomValue;
	this.topValue = (top)?top:this.topValue;
	this.precision = (precision)?precision:this.precision;
}

function setValue(newValue) {
	this.value = newValue;
}

function getRandom(rangeFrom,rangeTo,precision) {
	reciprocal = 1/precision;
	rangeFrom *= reciprocal;
	rangeTo *= reciprocal;
	number = (Math.floor(Math.random()*(rangeTo-rangeFrom+1)+rangeFrom))/reciprocal;
	return number;
}

function randomObject_toString() {
	stringValue = this.value;
	return Number(stringValue);
}

function displayNumericAnswerFieldSTATIC(mask,retryisEnabled,radioEnabled) {
	NAF_SetMask(mask,retryisEnabled);
	NAF_OnChange('userAnswerChanged()');
	NAF_DisplayNORetry(retryisEnabled,radioEnabled);
	setTimeout('NAF_PostOpen()', 1); 
	init();
}

function displayRadioOptions(radioOptions,x,y) {
	return writeRadioOptions(radioOptions,x,y);
}

function NAF_DisplayNORetry(retryisEnabled,radioEnable) {
	var text = '';
		text += '<table id="answerWidget" name="answerWidget" border="0" cellpadding="0" cellspacing="0">';
		text += '<tr>';
		text += '<td nowrap>';
		text += '<font face="Arial"><b>';
		text += 'Answer&nbsp;&nbsp;=&nbsp;&nbsp;';
		text += '</b></font>';
		text += '</td>';
	var decimalPart = false;
	for (var i = 0; i < NAF_mask.length; i++) {
		var ch = NAF_mask.substr(i, 1);
		switch (ch) {
			case '9':
			case '1':
				text += '<td nowrap>';
				text += '<input type="text"';
//				if (document.all) 
					text += ' class="numericInputFieldIE"';
//				else
//					text += ' class="numericInputFieldNN"';
				text += ' name="NAF_Digit' + NAF_digits + '" id="NAF_Digit' + NAF_digits + '"';
				text += ' autocomplete="off" maxlength="1" size="1"';
				text += ' onfocus="select()"';
				if (isIE) 
					text += ' onkeydown="NAF_KeyDownIE(' + NAF_digits + ')"';
				text += '>';
				text += '</td>';
				NAF_digits++;
				break;
			case ' ':
				text += '<td nowrap>';
				text += '<font face="Arial"><b>&nbsp;';
				text += '</b></font>';
				text += '</td>';
				break;
			case 'x':
				if (radioEnable) {
					text += '<td nowrap>';
					text += '<input name="radioAnswer" type="hidden" id="radioAnswer" value="">';
					text += '</td>';
					break;
				}
			default:
				text += '<td nowrap>';
				text += '<font face="Arial"><b>';
				text += ch;
				text += '</b></font>';
			text += '</td>';
			break;
		}
	}

	text += '</tr>';
	text += '</table>';
 
	text += '</span>'; 
	
	getDocument().write(text);
}


function writeRadioOptions(radioOptions, startX, startY){
    text = '';
    radioChoices = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'];
    for (i = 0; i < radioOptions.length; i++) {
        text += '<SPAN id="answer_' + radioChoices[i] + '"' + ' name = "answer_' + radioChoices[i] + '"';
        text += ' STYLE="position:absolute; left:' + startX + 'px; top:' + (startY + (i * 25)) + 'px; width: 565px;" class="standard">';
        text += '<FORM name="answer_' + radioChoices[i] + '">';
        text += '<INPUT type=radio ';
        text += 'name="answer_' + radioChoices[i] + '"	value="' + radioChoices[i] + '"';
        text += ' onClick="setRadioAnswer(\'' + radioChoices[i] + '\');';
        text += 'userAnswerChangedFUNKY();">';
        text += radioChoices[i] + ') ';
        text += radioOptions[i] + '</FORM></SPAN>\n';
    }
    return text;
}



function userAnswerChangedFUNKY ()
{ 
	var ua;
	if (! localIsReviewMode())
	{ 
		getst("correctMsg").visibility = "hidden";	
		getst("incorrectMsg").visibility = "hidden";
		getst("missingMsg").visibility = "hidden";
	}
	
	var numericAnswer = NAF_GetValue();
	var radioPart = getRadioAnswer();
	ua = numericAnswer+radioPart;
	
	NAF_SetValue(numericAnswer);
	cacheUserAnswer(ua);
}

function	getUserAnswer ()
{
	var numericAnswer = NAF_GetValue();
	var radioPart = getRadioAnswer();
	ua = numericAnswer+radioPart;
	if (! ua)
	ua = retrievePageAnswer(); 
	return ua;
}

function getRadioAnswer() {
	rObject = eval('getDocument().forms.digitForm.radioAnswer');
	return rObject.value;
}

function setRANRADIOUserAnswer(uaString) {

	var ua = retrievePageAnswer ();
	if (!ua) ua = "";
	
	var numericUA = Number(ua);
	var hasRadioAnswer = (isNaN(numericUA)) ? true : false;
	var radioChoiceVAR = ua.charAt(ua.length-1);
	var numericToRestore = hasRadioAnswer ? (ua.slice(0,-1)) : (ua);
	
	if (! parent.isReviewMode())	
	{ 
		getst("correctMsg").visibility = "hidden";	
		getst("incorrectMsg").visibility = "hidden";
		getst("missingMsg").visibility = "hidden";
	}
	if (hasNumericAnswer ())
	{ 
		if (! ua) 
		{ return;
		}
		setUserAnswerBASIC(numericToRestore);
	} 
	
	setRadioAnswer(radioChoiceVAR);
}


function disableAnswerWidgets() {

	NAF_Disable();
	disableRadioButtons();
}

function disableRadioButtons() {
	
	var rbs = findRadioButtons();
	for (key in rbs)	{
	rbs[key].disabled = true;
	}
}

function setRadioAnswer(radioChoice) {
	rObject = eval('getDocument().forms.digitForm.radioAnswer');
	rObject.value = radioChoice;
	var rbs = findRadioButtons();
	for (key in rbs)	{

	if (key==radioChoice) {
	rbs[key].checked = true;

	} else {
	rbs[key].checked = false;

	}
	}
	
}

function findRadioButtons()
{ 	if (isIE) 
 { 
	return findRadioButtonsMS();
 }
 else
 	{ 
	return findRadioButtonsMS();
	}
}
 
function findRadioButtonsNS ()
{ 
	var rbs = new Object();
	if (getsp("answer_a"))
		rbs['a'] = getsp("answer_a").document.forms[0].elements[0];
	if (getsp("answer_b"))
		rbs['b'] = getsp("answer_b").document.forms[0].elements[0];
	if (getsp("answer_c"))
		rbs['c'] = getsp("answer_c").document.forms[0].elements[0];
	if (getsp("answer_d"))
		rbs['d'] = getsp("answer_d").document.forms[0].elements[0];
	if (getsp("answer_e"))
		rbs['e'] = getsp("answer_e").document.forms[0].elements[0];
	if (getsp("answer_f"))
		rbs['f'] = getsp("answer_f").document.forms[0].elements[0];
	if (getsp("answer_g"))
		rbs['g'] = getsp("answer_g").document.forms[0].elements[0];
	return rbs;
}

function findRadioButtonsMS ()
{	
	if (document.forms[0] == null)
	{ 
		isFBmodeVAR = false; 
		return new Object(); 
	}
	var rbs = new Object();
	rbs['a'] = document.forms['answer_a'].elements[0];
	if (!document.forms['answer_b']) return rbs;
	rbs['b'] = document.forms['answer_b'].elements[0];
	if (! document.forms['answer_c']) return rbs;
	rbs['c'] = document.forms['answer_c'].elements[0];
	if (! document.forms['answer_d']) return rbs;
	rbs['d'] = document.forms['answer_d'].elements[0];
	if (! document.forms['answer_e']) return rbs;
	rbs['e'] = document.forms['answer_e'].elements[0];
	if (! document.forms['answer_f']) return rbs;
	rbs['f'] = document.forms['answer_f'].elements[0];
	if (! document.forms['answer_g']) return rbs;
	rbs['g'] = document.forms['answer_g'].elements[0];
	return rbs;
}

function getMaskFromCorrectAnswer(aString) {
	mask=aString.replace(/\d/g,"1");
	return mask;
}

function getTrieFromformattedCorrectAnswer(aString) {
	mask=aString.replace(/\D/g,"");
	return mask;
}

function getNewRandomSet() {
	setUserAnswer(null);
	resetPersistedData();
	window.location.reload();
	return;
}

function persistRandomVariableSet () { 
	var randomEvaluateStringToSave='';
	for(i=1;i<=randomVariables;i++) {
		randomEvaluateStringToSave+= 'rVar'+i+((i==randomVariables)?'':',');
	}
	var tempData = eval('['+randomEvaluateStringToSave+"].join('|')");
	parent.scoPropertySet ('#'+uniQuestionID, tempData);

}

function getRandomVariableSet () { 
	var tempData = parent.scoPropertyGet('#'+uniQuestionID);
	if (tempData) {
		tempData=tempData.split('|'); 
	} else {
		return null;
	}
 return tempData;
}

function resetPersistedData() {
	parent.scoPropertySet ('#'+uniQuestionID, null);
}

function showSavedData() {
	savedData = getRandomVariableSet();
	alert("savedData ::: " + savedData);
	return;
}

function parseQuestion_old(aString) {
	var reg = /rVar\d/g;
	var varArray = qString.match(reg);
	var arraySize = (varArray.length)?varArray.length:null;
	var parsedString = aString;
	if(arraySize) {
		for(i=0;i<arraySize;i++) {
		parsedString = parsedString.replace(varArray[i],eval(varArray[i]));
		}
	} else {
		return parsedString;
	}
	return parsedString;
}
