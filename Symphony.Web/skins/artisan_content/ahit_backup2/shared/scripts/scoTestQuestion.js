




function enableDrag(aBoolean){


    for (var i = 1; i <= 21; i++) {
        var ob = MM_findObj("txtField" + i);
        if (!(ob == null)) 
            ob.readOnly = !aBoolean;
    }
}


function currentPageIndex(){

    if (parent == this) 
        return 1;
    if (parent) {
        if (parent.currentPageIndex) 
            return parent.currentPageIndex();
    }
    return 1;
}

function setUserAnswer(value){

    if (localIsReviewMode()) {
        restoreUserInputs();
        correctAnswer();
        
        alertReview();
        
        return;
    }
    NAF_SetValue(value);
    cacheUserAnswer(value);
}



function correctAnswer(arg){
    var ua = getUserAnswer();
    ua = retrievePageAnswer();
    if (!parent.isReviewMode()) {
        if (showsVisualFeedBack()) 
            showsVisualFeedBack();
    }
    
    if (ua == null) {
        getst("missingMsg").visibility = 'visible';
        
        return
    }
    if (ua == "") {
        getst("missingMsg").visibility = 'visible';
        
        return
    }
    if ( ua == trie() ) {
        getst("correctMsg").visibility = 'visible';
        
        return;
    }
    getst("incorrectMsg").visibility = 'visible';
    
    
    showFeedback();
}




function finishPageDisplay(){

    if (parent == this) {
        showAnswerButtons();
        mathPaneInit();
        restoreUserInputs();
        return;
    }
    if (!parent.isTest()) {
        showAnswerButtons();
        mathPaneInit();
        restoreUserInputs();
        return;
    }
    if (parent.isReviewMode()) {
    
    
    
    
        restoreUserInputs();
        correctAnswer();
        showAnswer();
        enableDrag(false);
        NAF_isDisabled = true;
        disableAnswerWidgets();
        return;
    }
    else {
        enableDrag(true);
    }
    
    mathPaneInit();
    if (!parent.isReviewMode()) 
        restoreUserInputs();
}


function isAnswerButtonsVisible(){


    return false;
}





function retrievePageAnswer(){
    var pindex
    if (parent == this) {
        pindex = 1;
    }
    else {
        pindex = parent.currentPageIndex();
    }
    var result = localUAnswersAt(pindex);
    if (result == 'LESSON') 
        return null;
    if (result == 'EXERCISE') 
        return null;
    return result
}


function cacheUserAnswer(aString){
    var pindex = currentPageIndex();
    var correctA = trie();
    
    var pindex;
    if (!(parent == this)) {
        parent.setPageNoToState(pindex, 2);
        pindex = parent.currentPageIndex();
    }
    else {
        pindex = 1;
    }
    localuAnswersAtPut(pindex, aString);
    if (cad()) 
        cad()[pindex] = correctA;
    
}


function getTitle(arg){
    var etitle = getExerciseTitle(arg);
    var lno = getLessonNumber();
    var realTitle = etitle + " " + lno;
    if (document.m_lessonTitle) 
        return m_lessonTitle;
    
    return "Question " + parent.currentPageIndex();
}

function getExerciseNumber(){




    return ""
}

var isFBmodeVAR = false;




function isEvaluativeFeedbackMode(){

    if (parent == this) 
        return true;
    
    if (!parent.isTest()) 
        return true;
    return false;
}

function scoPageLoaded(idString, classString, windowObject, pageNoString){
    init();
	hideAnswer();
    scoPageLoadedCOMMON(idString, classString, windowObject, pageNoString);
    
    
    if (parent.markExerciseVisited) {
        parent.markExerciseVisited(pageId());
        
        
    }
    
    if (parent.isReviewMode()) {
        return;
    }
    
    getst("correctMsg").visibility = "hidden";
    getst("incorrectMsg").visibility = "hidden";
    getst("missingMsg").visibility = "hidden";
    
    
    
    
    
    
    
    
    
    
//    browserArrayMain = getBrowser();
//    browserNameMain = browserArrayMain[0];
//    browserPlatformMain = navigator.platform;
    
    
    
    setTimeout("blockit()", 222);
    
}



function blockit(){
 
// document.oncontextmenu = mouseClick;
// FIX FOR MAC:
  document.oncontextmenu =   function () {return false;}

    if (true) 
        return;
     
    
    
    if (browserNameMain == "msie") {
        // THIS MAY HAVE prevented drag and drop
        // BUT if we do only this then right click gets enabled
        
        // document.onmousedown = mouseClick; 
        
        // Therefore try this instead:
        document.onmousedown = right;
        
        // The above helps, after we also modified
        // function right(e)    to only block RIGHT-clicks.
        // Else frag-and-drop would break
    }
    
//    var isie = true;
//    var isnn = !isie;
//    if (document.all) 
//        isie = true;
//    if (isms) 
	//var isie = isIE; 
	//var isIE = ismoz; 
    
    document.onkeydown = key;
    document.onkeyup = key;
    
    
//    if (!(isIE || isIE)) {
//        window.document.layers = right;
//        window.captureEvents(Event.KEYPRESS);
//        window.captureEvents(Event.MOUSEDOWN);
//        window.captureEvents(Event.MOUSEUP);
//        window.onMouseDown = right;
//        window.onMouseUp = right;
//    }
}









function key(k){
    return true;
    var msg = "Time: " + " " + parent.getSessionTime() + " ";
    if (isie) {
    
    
    
    
        if (event.keyCode == 67 || event.keyCode == 17 || event.keyCode == 93) {
        
        
            return false;
        }
    }
    
    if (isnn) {
    
    
        return false;
    }
}




function mouseClick(e){
    browserArray = getBrowser();
    browserName = browserArray[0];
    browserPlatform = navigator.platform;
    
    
    
    if ((browserName == "safari") && (browserPlatform != "Win32")) {
        return true;
    };
    if ((browserName == "firefox") && (e.which == 3)) {
        return right(e);
        
    }
    if ((browserName == "msie") && (event.button == 2)) {
        return right(e);
    }
    if (isms()) {
        return right(e);
    }
    
    return true;
}

function right(e){

    if (parent == null) {
        return false;
    }
    if (parent == this) {
        return false;
    }
    if (!parent.isTest()) {
        return false;
    }
    
    if (!isms()) { // Blocks the right click   only for IE
        return false;
    }
    
    var button = event.button;
    
    if ((event.button == 2) || (event.button == 0)) { // blocks only Right-click. 
        // Blocking left click would disable drag-and-drop
        
        var msg = "This is question " + currentPageIndex() + "\nof \'" + parent.getCourseTitle() + "\'";
        msg = "Time:" + " " + parent.getSessionTime() + " ";
        alert(msg);
        
        return false; // block it
    }
    
    return true; // dont block it
}





function currentInstanceId(){
    if (!hasNumericAnswer()) 
        return null;
    
    var id = pageId();
    var insid;
    if (ixd()) {
        insid = ixd()[id];
        return insid
    }
    return null;
}


function showFeedback(){
    if (hasNumericAnswer()) {
        return;
    }
    if (isEvaluativeFeedbackMode()) {
        showFeedbackBasic();
    }
}


function showFeedbackBasic(){
    var msg = getEvaluativeFeedback();
    
    var wname = 'feedback';
    var url = '#';
    var features = 'toolbar=no,status=no,menubar=no,scrollbars=auto,' + 'width=372,height=134,resizable=no';
    
    var shPath = '../../../../../shared/';
    var ptype = pageType();
    if (ptype == 'HtmlExercise') {
        shPath = "../shared/";
    }
    
    var url = shPath + "html/fback.htm";
    var win = window.open(url, wname, features);
    
    
    if (true) 
        return;
    
    
    
    var content = '<html><head><title>Incorrect answer feedback</title></head>' +
    '\n<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">\n' +
    '<img src="' +
    shPath +
    'images/WV_P2_Feedback_Label.gif" width="353" height="45">\n' +
    '\n <font face="arial" size=-1>' +
    '<div style="margin-left:6;margin-right:6;text-weight:bold">' +
    msg +
    '</div></font>\n</body></html>';
    
    win.document.writeln(content);
    win.document.close();
    
}


function alertReview(){

    if (parent == this) 
        return;
    parent.alertAlreadySubmitted();
    correctAnswer();
}


function localIsReviewMode(){

    if (parent == this) 
        return false;
    if (parent.isReviewMode) 
        return (parent.isReviewMode());
    return false
}


function getExercisePageNumber(arg){



    var pno = pageNumb();
    
    if (!pno) {
        pno = document.m_exerciseNumber
    };
    if (!pno) {
        pno = m_exerciseNumber;
    };
    
    if (!pno) {
        pno = getExerciseTitle();
    }
    
    return pno
}


function getEvaluativeFeedback(){
    return '';
}



function localIsCommitted(){
    if (parent.isCommitted) 
        return parent.isCommitted();
    return false
}

function disableAnswerWidgets(){
    NAF_Disable();
    disableTxtFields();
    disableRadioButtons();
}


function disableTxtFields(){
    for (var i = 1; i <= 21; i++) {
        var ob = MM_findObj("txtField" + i);
        if (!(ob == null)) 
            ob.readOnly = true;
    }
}

function disableRadioButtons(){
    if (document.forms[0] == null) {
        isFBmodeVAR = false;
        return new Object();
    }
    
    for (var i = 0; i < document.forms.length; i++) {
        var ob = document.forms[i].elements[0];
        if (!(ob == null)) 
            ob.disabled = true;
    }
}




function isReviewMode(){





    if (parent == this) 
        return true;
    
    if (parent.isReviewMode) 
        return parent.isReviewMode();
    return true;
}
