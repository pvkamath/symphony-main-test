// file : scoDialogueUtils.js
// all the supporting functions for the red dialogue pages

//

function scoExitLesson() {
	parent.exitButtonClicked ();
	return;
}

function scoGoBackToLesson() {
	// copy of go_back in the sconavigationFrame.js
	var aSco = parent.getSco();
 	if (aSco == null) 
 	  {  return; 
 	  }	  
//	  popHideStack();
 	return 	aSco.goBack ( ); 
}
