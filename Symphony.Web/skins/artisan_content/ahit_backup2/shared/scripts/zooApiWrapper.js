
function zooSetLessonLocationFINISH(){
    // sco374
    
    if (isTest()) 
        return;
    
    zooLMSSetValue('cmi.core.lesson_location', "" +
    currentPageIndex() +
    "");
}


function zooUpdateSessionTimeFINISH(){
    zooSetLessonLocationFINISH();
    
    var timeSpan = getSessionTime();
    LMSSetValue("cmi.core.session_time", timeSpan);
    return (timeSpan);
}




var isFirstPageZUSTB = true; 
//    do not do this on course-load

function zooUpdateSessionTimeBASIC(){
    var timeSpan = getSessionTime();
    if (isFirstPageZUSTB) {
        isFirstPageZUSTB = false
    }
    else { 
		// sco374  now happens onlyon finish
        //LMSSetValue("cmi.core.session_time",timeSpan);
    }
    return (timeSpan);
}




////////////////////////////////////////////// end sco374



function getSessionTimeTotalSeconds() 
// sco219
//       ================  
// return the no of seconds spent in THIS session
{
    var nowTime = new Date();
    var nowHour = nowTime.getHours();
    var nowMinutes = nowTime.getMinutes();
    var nowSeconds = nowTime.getSeconds();
    
    var nowMilliseconds = nowTime.getTime();
    var elms = nowMilliseconds - startMilliseconds;
    
    var totalSeconds = Math.round(elms / 1000);
    return totalSeconds;
}




function getSessionTime()
//       ================  
// return a string indciating the elapsed
// session time.
{
    var totalSeconds = getSessionTimeTotalSeconds();
    
    var hours = Math.floor(totalSeconds / 3600);
    var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
    var seconds = totalSeconds - ((3600 * hours) + (60 * minutes));
    
    if ((hours + "").length == 1) 
        hours = "0" + hours;
    if ((minutes + "").length == 1) 
        minutes = "0" + minutes;
    if ((seconds + "").length == 1) 
        seconds = "0" + seconds;
    
    var timeSpan = hours + ":" +
    minutes +
    ":" +
    seconds;
    
    return (timeSpan);
}

// ===================================================== end sco219


function deb(aString){
}


function zGetLmsInfo(){
    // var coreChildren = LMSGetValue("cmi.core._children") ;
    // var scoreChildren = LMSGetValue("cmi.core.score._children");
}


function zNotifyErrors(operationName){
    // return true if there was an error
    //
    if (!isAPIAdapterValid()) {
        alert("LMS connection was broken");
        return true;
    }
    
    var code = zooLMSGetLastError();
    if (code == '0') { 
		// art (operationName + " - NO LMS ERROR");
        return false;
    }
    
    var msg = zooLMSGetErrorString(code);
    alert(operationName + " LMS-ERROR: " +    "\n\n" +    msg +    "  code: " +    code);
    return true;
    
}

function zooGetScoreFromLMS(statusString)
//       ================
{
    var aString = LMSGetValue("cmi.core.score.raw");
    return aString;
}



function zooSendScoreToLMS(scoreInteger)
//       ==============
{

    var value = scoreInteger.toString();
    
    var assessmentScore = value;
    
    ///////////// sco335
    //  var scoreChildren   = LMSGetValue("cmi.core.score._children");
    //  var ix = scoreChildren.indexOf("raw") ;
    //  var hasRawScoreSupport      = (ix >= 0)  ;
    /////////// sco335 
    
    //if (hasRawScoreSupport) 
	
    if (true) {
        LMSSetValue("cmi.core.score.raw", assessmentScore);
    }
    else {
        if (!isAPIAdapterValid()) 
            return;
        art("This LMS \n" + " does not support cmi.core.score.raw.");
    }
    
    // sco335.
    // zNotifyErrors ("cmi.core.score.raw");
}





function zooSendStatusToLMS(statusString)
//       ================
{

    LMSSetValue("cmi.core.lesson_status", statusString);
    
    
    ////// sco335 //////
    ////    var getBack = LMSGetValue("cmi.core.lesson_status" );
    //   	zNotifyErrors ("cmi.core.score.raw");
    //////  sco335

}



function zooGetStatusFromLMS(statusString)
//       ================
{
    var aString = LMSGetValue("cmi.core.lesson_status");
    return aString;
}


// read the current time on the computer clock when the page is opened
var startTime = new Date();
var startHour = startTime.getHours();
var startMinutes = startTime.getMinutes();
var startSeconds = startTime.getSeconds();

var startMilliseconds = startTime.getTime();


function zooUpdateSessionTime(){
    var val = zooUpdateSessionTimeBASIC();
    //alert("UPDATE SESSION TIME = " + val );
}




// ***********  1.  LIFECYCLE: *******************


function zooLMSInitialize(emptyString)
//       ================
//

{
    LMSInitialize("");
    
    restoreInstanceIds();
    
    
}



function zooLMSInitializeBASIC(emptyString)
//       ================
// does not randomize instance-ids, because
// in tests this would cauuse a problem
//

{
    LMSInitialize("");
    
    //	restoreInstanceIds();
    
    var dicts = restoreDicts();
    var pts = noOfInstances();
    
}



function zooLMSFinish(emptyString)
//       ============
{

    zooLMSCommit();
    var sc = getSco();
    exitScoWithoutConfirmation(sc);
    
    LMSFinish("");
}




// ************ 2.   STATE: ***********************


function zooLMSSetValue(keyString, valueString)
//       ==============
// Send the data to the LMS if possble. 
// The arguments must be valid SCORM data elements..
// IF API is found, CHECK for the error, and alert 
// the user is LMS returns an error code.
//	
{


    if (isAPIAdapterValid()) {
        LMSSetValue(keyString, valueString);
        
        // sco335: do not slow down by asking for latest error
        //		var errorCode	=  zooLMSGetLastError ( );
        //        var errorInt = errorCode - 1 + 1; 
        var errorInt = 0; // sco335.
        if (!(errorInt == 0)) // sco55 
        {
            alert("LMSSetValue ERROR: " +
            errorCode +
            "\n\nwhen storing " +
            keyString +
            " -> " +
            valueString);
        }
    }
}


function zooLMSGetValue(keyString)
//       ============== 
//  If API is valid, use it, and check that
//  there was no error condition.
//  If API is NOT found,  return the cached value, 
//  for developmental purposes.
//
{
    if (isAPIAdapterValid()) {
        var result = LMSGetValue(keyString);
        
        // sco335
        //		var errorCode	=  zooLMSGetLastError ( );
        //		var errorInt = errorCode - 1 + 1; 
        var errorInt = 0;
        // sco335.
        
        if (!(errorInt == 0)) // sco55 
        {
            alert("LMSGetValue 1 ERROR: |" +
            errorInt +
            "|\n\nwhen retrieving " +
            keyString);
            return null;
        }
        return result;
    }
    
    return result;
}


function zooLMSCommit(){ 
//     ============
    var result = LMSCommit();
    
    // sco335
    //	var errorCode	=  zooLMSGetLastError();
    //	if (! (errorCode == 0))         // sco160 
    //	{ alert("LMSCommit ERROR: " 
    //				 	+ errorCode  
    //			);
    //      return false;
    //    } 
    // sco335
    
    return true;
}



//************  3.  ERROR HANDLING: *******************



function zooLMSGetLastError()
//       ==================
// Return a string that converts into an integer,
// describing the last error. Return '0' if the
// most recent API-call was succesfull.
// 
{
    var result = LMSGetLastError();
    return result;
}



function zooLMSGetErrorString(errorCode)
//       ====================
{
    var result = LMSGetErrorString(errorCode);
    return result;
}


function zooLMSGetDiagnostic(anInteger_orEmptyString){
    // Return a description of the error. The arg is either 
    // an error code returned previously or empty string in 
    // which case the most recent error is described
    //
    
    var result = LMSGetDiagnostic(anInteger_orEmptyString);
    return result;
}
