<?xml version="1.0"?>
<!-- The above must be on the FIRST line for Netscape -->

<xsl:stylesheet 
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
 xmlns:zoo="http://www.zoologic.com/"
  version="1.0"  >

<xsl:output method="html" encoding="UTF-8" />



<xsl:template match="/">
    <html>
    <head><title><xsl:value-of select="month/name"/></title></head>
    <body bgcolor="#fffbf0">
 
<h2> 
 <xsl:apply-templates select="title"/>
</h2>

<table>
	<xsl:apply-templates/>
</table>
</body>
</html>
</xsl:template> 




 
<xsl:template match = "title" >
  <h2>  <xsl:value-of select = "." />
  </h2>
</xsl:template>  







<xsl:template match = "url" >
 <xsl:variable name="element_name"  select="."/>
 <xsl:variable name="posi"  select="position()" />

<tr>
<td>
      <xsl:value-of select = "position() - 1" />
<xsl:text > : </xsl:text>
</td>
	<td>
	<a href="{$element_name}">


               <xsl:value-of select = "." />
       </a>
	</td></tr> 
</xsl:template> 

 

</xsl:stylesheet>
