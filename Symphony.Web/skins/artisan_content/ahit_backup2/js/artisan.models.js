(function () {
    var models = Artisan.Utilities.ns('Artisan.Models');

    //var API = parent;
    var LMS = parent.objLMS;

    ArtisanSectionType =
    {
        Sco: 1,
        LearningObject: 2,
        Objectives: 3,
        Survey: 4,
        Pretest: 5,
        Posttest: 6
    };

    ArtisanQuestionType =
    {
        MultipleChoice: 1,
        TrueFalse: 2,
        FillInTheBlank: 3,
        AllThatApply: 4,
        ImageQuestion: 5,
        Matching: 6,
        CategoryMatching: 7,
        LongAnswer: 8
    };

    ArtisanTestType =
    {
        GameBoard: 1,
        GameWheel: 2,
        Sequential: 3,
        Random: 4
    };

    ArtisanCourseCompletionMethod =
    {
        Navigation: 1,
        Test: 2,
        InlineQuestions: 3,
        TestForceRestart: 4
    };

    ArtisanCourseNavigationMethod =
    {
        Sequential: 1,
        FreeForm: 2,
        FullSequential: 3
    };

    ArtisanPageType =
    {
        Content: 1,
        Question: 2,
        Objectives: 3,
        Introduction: 4,
        Survey: 5
    };

    ArtisanCourseTimeoutMethod = {
        None: 0,
        ShowAlert: 1,
        Automatic: 2
    };

    ArtisanCourseMarkingMethod = {
        ShowAnswers: 1,
        HideAnswers: 2,
        HideAnswersAndFeedback: 3
    };

    ArtisanCourseReviewMethod = {
        ShowReview: 1,
        HideReview: 2
    };

    Artisan.Models.Timer = Class.extend({
        init: function (type) {
            this.interval = null;
            this.timeout = null;
            this.running = false;
            this.timeRemaining = null;
            this.timeoutFunction = null;
            this.id = null;

            if (type) {
                type = type.toLowerCase();
            }

            if (type != 'lo' && type != 'sco') {
                type = 'Course';
            } else {
                type = type.charAt(0).toUpperCase() + type.slice(1);
            }

            this.type = type;

            if (!type) {
                this.typeChar = 'c';
            } else {
                this.typeChar = this.type.toLowerCase().charAt(0);
            }
        },

        getTimeRemaining: function () {
            var minutes = Math.floor(this.timeRemaining / 60),
                seconds = this.timeRemaining % 60;

            return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
        },

        isRunning: function () {
            return this.running;
        },

        startTimeout: function (duration) {
            var me = this;
            setTimeout(function () {
                if (typeof (me.timeoutFunction) === 'function') {
                    me.timeoutFunction();
                }
                me.stop();
            }, duration * 1000);
        },

        startInterval: function () {
            var me = this;

            me.clearInterval();

            me.interval = setInterval(function () {
                if (me.isRunning()) {
                    me.timeRemaining--;

                    if (typeof (me.intervalFunction) === 'function') {
                        me.intervalFunction(me);
                    }
                }

                Artisan.App.course.record.addTime(me.typeChar, me.id, 1);

                if (Artisan.App.course.record.getTime(me.typeChar, me.id) % 30 == 0) {
                    Artisan.App.course.record.saveDataChunk();
                }

                me.updateDisplay();
            }, 1000);
        },

        clearInterval: function () {
            clearInterval(this.interval);
            this.interval = null;
        },

        pause: function () {
            Artisan.App.course.record.saveDataChunk();
            this.running = false;
            clearTimeout(this.timeout);
            this.timeout = null;
        },

        resume: function () {
            if (!this.running) {
                startTimeout(this.timeRemaining);
                this.running = true;
            }
        },

        updateDisplay: function () {
            var display = $('#' + this.type + '-time-remaining'),
                counterText = display.find('.counter-text'),
                timerDisplays = $('#timer-displays');

            if (this.timeRemaining > 0 && this.running) {
                display.addClass('running');
                timerDisplays.addClass('active');
            } else {
                if (this.timeRemaining >= 0 && display.hasClass('running')) {
                    var me = this;
                    // If it's 0, or manually stopped
                    // show the last value for 1 second
                    // So timer will actually show 0, then disapear
                    //
                    // Double checks to make sure the timer hasn't been reset between starting this timeout
                    // and it actually firing
                    setTimeout(function () {
                        if (me.timeRemaining <= 0) {
                            display.removeClass('running');
                        }
                    }, 1000);
                } else {
                    // if already negative, remove it right away.
                    display.removeClass('running');
                }
            }

            counterText.html(this.getTimeRemaining());

            // hide the entire panel if no displays are active
            var activeDisplays = timerDisplays.find('.running');
            if (!activeDisplays.length) {
                timerDisplays.removeClass('active');
            }
        },

        start: function (id, timeoutFunction, duration, intervalFunction) {
            // Set by the training program to disable any course timing functions
            if (Artisan.IsCourseTimingDisabled) {
                return;
            }

            var me = this,
                timeUsed;

            Artisan.App.course.record.saveDataChunk();

            me.stop();

            me.id = id;
            timeUsed = Artisan.App.course.record.getTime(me.typeChar, me.id);
            duration = duration - timeUsed;
            me.timeRemaining = duration;

            if (me.timeRemaining > 0) {
                me.timeoutFunction = timeoutFunction;
                me.startTimeout(me.timeRemaining)
                me.running = true;
            }

            me.startInterval();

            me.updateDisplay();
        },

        autoLogout: function (time) {
            if (this.type && this.id) {
                Artisan.App.course.record.addTime(this.typeChar, this.id, -time);
                Artisan.App.course.record.saveDataChunk();
            }
        },

        stop: function () {
            Artisan.App.course.record.saveDataChunk();

            this.running = false;

            clearTimeout(this.timeout);
            this.timeout = null;

            this.timeRemaining = 0;
            this.updateDisplay();
        }
    });

    // a Container is an object with sections and pages
    Artisan.Models.Container = Class.extend({
        init: function (config, parent) {
            this.config = config;
            this.sections = [];
            this.pages = [];
            this.parent = parent;
            if (config.sections) {
                for (var i = 0; i < config.sections.length; i++) {
                    this.sections.push(new models.Container(config.sections[i], this));
                }
            }
            if (config.pages) {
                for (var i = 0; i < config.pages.length; i++) {
                    if (config.pages[i].isReview) {
                        this.pages.push(new models.ReviewPage(this, config.pages[i].sectionId, config.pages[i].record));
                    } else {
                        this.pages.push(new models.Page(this, config.pages[i]));
                    }
                }
            }
        },
        getName: function () {
            return this.config.name;
        },
        getId: function () {
            return this.config.id;
        },
        getSectionAt: function (index) {
            return this.sections[index];
        },
        getSection: function (id) {
            if (this.getId() == id) {
                return this;
            }
            for (var i = 0; i < this.sections.length; i++) {
                if (this.sections[i].getId() == id) {
                    return this.sections[i];
                } else if (this.sections[i].sections) {
                    var result = this.sections[i].getSection(id);
                    if (result) {
                        return result;
                    }
                }
            }
            return null;
        },
        getSectionByName: function (name) {
            if (this.getName() == name) {
                return this;
            }
            for (var i = 0; i < this.sections.length; i++) {
                if (this.sections[i].getName() == name) {
                    return this.sections[i];
                } else if (this.sections[i].sections) {
                    var result = this.sections[i].getSectionByName(name);
                    if (result) {
                        return result;
                    }
                }
            }
            return null;
        },
        getFirstSectionWithPages: function () {
            if (this.hasPages()) {
                return this;
            }
            if (this.sections && this.sections.length) {
                for (var i = 0; i < this.sections.length; i++) {
                    var result = this.sections[i].getFirstSectionWithPages();
                    if (result) {
                        return result;
                    }
                }
            }
            return null;
        },
        getLastSectionWithPages: function () {
            // Ignore survey section, this must be last
            if (this.config.sectionType && this.config.sectionType == ArtisanSectionType.Survey) {
                return null;
            }

            if (this.hasPages()) {
                return this;
            }
            if (this.sections && this.sections.length) {
                for (var i = 0; i < this.sections.length; i++) {
                    var result = this.sections[this.sections.length - 1 - i].getLastSectionWithPages();
                    if (result) {
                        return result;
                    }
                }
            }
            return null;
        },
        getSections: function () {
            return this.sections;
        },
        getParent: function () {
            return this.parent;
        },
        hasPages: function () {
            return (this.pages ? this.pages.length > 0 : false);
        },
        getPage: function (id) {
            var page = null;
            if (this.pages) {
                for (var i = 0; i < this.pages.length; i++) {
                    if (this.pages[i].getId() == id) {
                        page = this.pages[i];
                        break;
                    }
                }
            }
            if (!page && this.sections) {
                for (var i = 0; i < this.sections.length; i++) {
                    page = this.sections[i].getPage(id);
                    if (page) {
                        break;
                    }
                }
            }
            return page;
        },
        getPageAt: function (index) {
            return this.pages[index];
        },
        getLastPage: function () {
            return this.getPageAt(this.pages.length - 1);
        },
        getFirstPage: function () {
            return this.getPageAt(0)
        },
        getFirstQuestionPage: function () {
            for (var i = 0; i < this.pages.length; i++) {
                var page = this.getPageAt(i);
                if (page.isQuestion()) {
                    return page;
                }
            }
            return null;
        },
        getNextQuestionPage: function (id) {
            for (var i = 0; i < this.pages.length; i++) {
                var page = this.getPageAt(i);
                if (page && page.getId() == id) {
                    for (var j = i; j < this.pages.length; j++) {
                        page = this.getPageAt(j);
                        if (page.isQuestion()) {
                            return page;
                        }
                    }
                }
            }
            return null;
        },
        getNextPage: function (id) {
            for (var i = 0; i < this.pages.length; i++) {
                var page = this.getPageAt(i);
                if (page && page.getId() == id) {
                    return this.getPageAt(i + 1);
                }
            }
        },
        getPreviousPage: function (id) {
            for (var i = 0; i < this.pages.length; i++) {
                var page = this.getPageAt(i);
                if (page && page.getId() == id) {
                    return this.getPageAt(i - 1);
                }
            }
        },
        isTestSection: function () {
            return this.config.sectionType == ArtisanSectionType.Posttest || this.config.sectionType == ArtisanSectionType.Pretest;
        },
        isPretest: function () {
            return this.config.sectionType == ArtisanSectionType.Pretest;
        },
        isTestForceRestart: function () {
            var parent = this.getParent();

            if (!parent) {
                parent = this;
            }

            while (parent && !parent.config.hasOwnProperty('completionMethod')) {
                parent = parent.getParent();
            }

            return parent.config.completionMethod == ArtisanCourseCompletionMethod.TestForceRestart &&
                   this.isTestSection() &&
                   !this.isPretest();
        },
        getTotalPageCount: function () {
            return this.getAllPages().length;
        },
        getTotalPageIndex: function (learningObjectId, pageId) {
            var allPages = this.getAllPages();
            for (var i = 0; i < allPages.length; i++) {
                if (allPages[i].parent.getId() == learningObjectId && allPages[i].getId() == pageId) {
                    return i;
                }
            }
            return -1;
        },
        getAllPages: function (section) {
            var total = [];
            if (!section) {
                section = this;
            }
            if (section.pages && section.pages.length) {
                for (var i = 0; i < section.pages.length; i++) {
                    total.push(section.pages[i]);
                }
            }
            // loop through all sub-sections to get their pages
            if (section.sections && section.sections.length) {
                for (var i = 0; i < section.sections.length; i++) {
                    total = total.concat(this.getAllPages(section.sections[i]));
                }
            }
            return total;
        }
    });

    // a content or question object
    Artisan.Models.Page = Class.extend({
        init: function (parent, config) {
            this.parent = parent;
            this.config = config;
            if (!this.config.id)
                this.config.id = Artisan.Models.Page.generateId();
        },
        getHtml: function () {
            if (!this.isContentPage()) {
                var template = $('.artisan-page-wrap').html();
                return template.replace('{!content}', this.config.html);
            }
            else { // content pages already have wrapper divs
                return this.config.html;
            }
        },
        getRawHtml: function () {
            return this.config.html;
        },
        getHtmlContent: function () {
            return Artisan.Utilities.extractContent(this.config.html);
        },
        isGradable: function () {
            return this.config.isGradable;
        },
        getName: function () {
            return this.config.name;
        },
        getId: function () {
            return this.config.id;
        },
        getReferencePageId: function () {
            return this.config.referencePageId;
        },
        getIncorrectFeedback: function () {
            return this.config.incorrectResponse;
        },
        getCorrectFeedback: function () {
            return this.config.correctResponse;
        },
        checkAnswers: function (userAnswers) {
            switch (this.config.questionType) {
                case ArtisanQuestionType.LongAnswer:
                    var results = { correct: [], incorrect: [] };
                    // Just assume this is correct for now.
                    results.correct.push(this.config.answers[0]);
                    break;
                case ArtisanQuestionType.MultipleChoice:
                case ArtisanQuestionType.TrueFalse:
                case ArtisanQuestionType.AllThatApply:
                    var results = { correct: [], incorrect: [] };
                    for (var i = 0; i < this.config.answers.length; i++) {
                        var found = false;
                        for (var j = 0; j < userAnswers.length; j++) {
                            if (userAnswers[j] == this.config.answers[i].id) {
                                if (this.config.answers[i].isCorrect) {
                                    // found in the list from the user, and correct
                                    results.correct.push(this.config.answers[i]); // Answer selected by the user, and the answer *should have been* selected � GREEN CHECK
                                } else {
                                    // found in the list from the user, but not correct
                                    results.incorrect.push(this.config.answers[i]); // Answer selected by the user, and the answer *should not have been* selected � RED X
                                }
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            if (this.config.answers[i].isCorrect) {
                                // not found, but should have been
                                results.incorrect.push(this.config.answers[i]);
                            } else {
                                // not found, and shouldn't have been
                                results.correct.push(this.config.answers[i]); // Answer *not* selected by the user, and the answer *should not have been* selected - NOTHING
                            }
                        }
                    }
                    break;
                case ArtisanQuestionType.FillInTheBlank:
                    var results = { correct: [], incorrect: [] };
                    for (var i = 0; i < this.config.answers.length; i++) {
                        var found = false;
                        for (var j = 0; j < userAnswers.length; j++) {
                            if (Artisan.Utilities.trim(userAnswers[j]).toLowerCase() == Artisan.Utilities.trim(this.config.answers[i].text).toLowerCase()) {
                                results.correct.push(this.config.answers[i]);
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            results.incorrect.push(this.config.answers[i]);
                        }
                    }
                    break;
                case ArtisanQuestionType.ImageQuestion:
                    var results = { correct: [], incorrect: [] };
                    var answers = JSON.parse(this.config.answers[0].text).dimensions;

                    for (var i = 0; i < answers.length; i++) {
                        var answer = answers[i];
                        var found = false;
                        for (var j = 0; j < userAnswers.length; j++) {
                            var userAnswer = userAnswers[j];

                            if ((userAnswer.x >= answer.x && userAnswer.x <= answer.x + answer.width)
					            &&
					            (userAnswer.y >= answer.y && userAnswer.y <= answer.y + answer.height)) {

                                results.correct.push(answer);
                                userAnswer.found = true;
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            // they missed it
                            results.incorrect.push(answer);
                        }
                    }
                    // flag the ones they added that weren't correct
                    for (var i = 0; i < userAnswers.length; i++) {
                        var userAnswer = userAnswers[i];
                        if (!userAnswer.found) {
                            results.incorrect.push(userAnswer);
                        }
                    }
                    break;
                case ArtisanQuestionType.Matching:
                    var results = { correct: [], incorrect: [] };
                    for (var i = 0; i < userAnswers.length; i++) {
                        // userAnswers and this.config.answers are parallel arrays
                        if (userAnswers[i].answerId == userAnswers[i].userAnswer) {
                            results.correct.push(this.config.answers[i]);
                        }
                        else {
                            results.incorrect.push(this.config.answers[i]);
                        }
                    }
                    break;
                case ArtisanQuestionType.CategoryMatching:
                    var results = { correct: [], incorrect: [] };
                    // Loop through the categories
                    for (var i = 0; i < this.config.answers.length; i++) {
                        var correct = true;
                        for (var j = 0; j < userAnswers.length; j++) {
                            var matchIds = userAnswers[j].matchId.split('-');
                            var ansId = matchIds[0];
                            var matchIndex = matchIds[1];

                            // Check all items that belong in this category to make sure none are missing
                            var belongsInThisCategory = (this.config.answers[i].id == ansId);

                            // Check all items dragged into this category to make they all belong
                            var draggedToThisCategory = (this.config.answers[i].id == userAnswers[j].userAnswer);

                            if (belongsInThisCategory || draggedToThisCategory) {
                                if (ansId != userAnswers[j].userAnswer) {
                                    correct = false;
                                    break;
                                }
                            }
                        }

                        if (correct) {
                            results.correct.push(this.config.answers[i]);
                        }
                        else {
                            results.incorrect.push(this.config.answers[i]);
                        }
                    }
                    break;
            }
            return results;
        },
        isAllThatApply: function () {
            return this.config.questionType == ArtisanQuestionType.AllThatApply;
        },
        isMultipleChoice: function () {
            return this.config.questionType == ArtisanQuestionType.MultipleChoice;
        },
        isTrueFalse: function () {
            return this.config.questionType == ArtisanQuestionType.TrueFalse;
        },
        isFillInTheBlank: function () {
            return this.config.questionType == ArtisanQuestionType.FillInTheBlank;
        },
        isImageQuestion: function () {
            return this.config.questionType == ArtisanQuestionType.ImageQuestion;
        },
        isMatching: function () {
            return this.config.questionType == ArtisanQuestionType.Matching;
        },
        isCategoryMatching: function () {
            return this.config.questionType == ArtisanQuestionType.CategoryMatching;
        },
        isLongAnswer: function () {
            return this.config.questionType == ArtisanQuestionType.LongAnswer;
        },
        isQuestion: function () {
            return (this.config.questionType ? true : false);
        },

        isIntroduction: function () {
            return this.config.isIntro;
        },
        isGameStart: function () {
            return this.config.isGameStart;
        },
        isReviewPage: function () {
            return this.isReview;
        },
        isContentPage: function () {
            return (this.config.pageType == ArtisanPageType.Content);
        },
        isSurveyPage: function () {
            return (this.config.pageType == ArtisanPageType.Survey);
        },
        getCourse: function () {
            var entity = this.parent;
            while (entity.parent != null) {
                entity = entity.parent;
            }
            return entity;
        }
    });

    // static members for Page class
    Artisan.Models.Page.idCounter = -1;
    Artisan.Models.Page.generateId = function () {
        return Artisan.Models.Page.idCounter--;
    };

    // a special type of Page
    Artisan.Models.ReviewPage = Artisan.Models.Page.extend({
        init: function (parent, sectionId, record) {
            this.isReview = true;
            this.sectionId = sectionId; // needed so we know which answers to review
            this.record = record;
            this._super(parent, {}); // no config to pass in
        },

        getHtml: function () {
            return $('.artisan-page-wrap').html().replace('{!content}', this.getRawHtml());
        },
        getRawHtml: function () {
            var section = this.record.course.getSection(this.sectionId);
            if (!section || !section.pages)
                return;

            var isPretest = section.config.sectionType == ArtisanSectionType.Pretest;
            var isPosttest = section.config.sectionType == ArtisanSectionType.Posttest;
            var isQuiz = section.config.isQuiz;
            var isNavigationCompletion = this.record.course.isNavigationMethod();
            var isTestOut = section.config.testOut;
            var isCertificateEnabeled = this.record.course.config.certificateEnabled;

            var templateCls = (isPretest ?
                                (isTestOut ? 'artisan-review-pretest-testout' : 'artisan-review-pretest') :
                                (isQuiz && !Artisan.App.state.isPreviousPageReview ? 'artisan-review-quiz' : 'artisan-review-posttest'));



            if (isNavigationCompletion && (!isQuiz || Artisan.App.state.isPreviousPageReview)) {
                // override only if this is the last review page and the last section
                // of the course. Otherwise it might be a review for a quiz
                templateCls = 'artisan-review-navigation';
            }

            // Show nocert version if no cert is enabled
            // Should only be needed for artisan-review-pretest-testout, artisan-review-posttest, and artisan-review-navigation
            // artisan-review-quiz and artisan-review-pretest never show certs
            if (templateCls !== 'artisan-review-pretest' && templateCls !== 'artisan-review-quiz' && !isCertificateEnabeled) {
                templateCls += '-nocert';
            }

            var template = $('.' + templateCls).html();

            this.record.buildAnswerCache();

            var reviewData = this.record.calculateReview(this.sectionId);

            // Insert by Nu
            if (isPretest) {
                LMS.SetSpeedPreference(reviewData.score);
            }

            var tempPreTestScore = 0;
            tempPreTestScore = LMS.GetSpeedPreference();
            if (!(tempPreTestScore != undefined || tempPreTestScore != null || tempPreTestScore != "")) {
                tempPreTestScore = 0;
            }

            if (tempPreTestScore < 0) {  	//just in case it's negative
                tempPreTestScore = 0;
            }

            if (!(isPretest && !isTestOut) && !(isQuiz && !Artisan.App.state.isPreviousPageReview)) { // don't set score or status if it's a non-testout pretest, or quiz
                // jerod, 9/19/2013; removed this rule, we always send it - the logic for whether or not we record it is in the backend
                /*if (!Artisan.newScoreMustBeHigher || reviewData.score > LMS.GetScore()) {
                LMS.SetScore(reviewData.score, tempPreTestScore, 100);
                // when this happens, clear the data chunk of answered questions and reset the bookmark
                }*/

                // OK, so, apparently some LMSes see the min/max as not pass/fail values, but as min/max the *student* got and then somehow add them together???
                // soooo, we set to 100, 0 to ensure that'll always set to 100
                LMS.SetScore(reviewData.score, 100, 0);

                // ok, we abuse the speed preference here
                LMS.SetSpeedPreference(tempPreTestScore);

                if (reviewData.passed) {
                    LMS.SetPassed();
                } else {
                    LMS.SetFailed();
                }

                // this is abuse, but seems to force things to save properly
                // per Nu, 5/9/2014
                LMS.CommitData();
                d = new Date()

                // add in a mini pause to handle some internet traffic issues
                while (1) {
                    mill = new Date();
                    diff = mill - d; //difference in milliseconds
                    if (diff > 2000) { break; }
                }
                // commit it again just in case it didn't go through because of slow a PC or internet connection
                LMS.CommitData();
            }

            var reviewQuestionsTemplate = $('.artisan-review-missed-questions').html() || '';

            var templateParams = {};
            var href = decodeURIComponent(window.top.location.href);
            var params = href.slice(href.indexOf('?') + 1).split('&');

            for (var i = 0; i < params.length; i++) {
                var param = params[i].split('=');
                if (param && param.length && param.length >= 2) {
                    var innerParams = param[1].split('!');

                    for (var j = 0; j < innerParams.length; j++) {
                        var innerParam = innerParams[j].split('|');
                        templateParams[innerParam[0]] = innerParam[1];
                    }
                }
            }

            var certificateUrl = "certificate.html?score={!score}&name={!user}&course={!course}";
            if (Artisan.App.course.config.useSymphonyCertificate) {
                certificateUrl = Artisan.App.course.config.certificateUrl;
            }

            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!score}', reviewData.score);
            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!user}', LMS.GetStudentName() || 'Student Name');
            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!course}', this.getCourse().getName());
            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!course_id}', templateParams.CourseId);
            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!training_program_id}', templateParams.TrainingProgramId || 0);
            certificateUrl = Artisan.Utilities.replaceAll(certificateUrl, '{!user_id}', templateParams.UserId || 0);

            var templates = [template, reviewQuestionsTemplate];
            for (var i = 0; i < templates.length; i++) {
                var t = templates[i];
                t = Artisan.Utilities.replaceAll(t, '{!answered_count}', reviewData.answered);
                t = Artisan.Utilities.replaceAll(t, '{!correct_count}', reviewData.correct);
                t = Artisan.Utilities.replaceAll(t, '{!total_count}', reviewData.total);
                t = Artisan.Utilities.replaceAll(t, '{!score}', reviewData.score);
                t = Artisan.Utilities.replaceAll(t, '{!passing_score}', reviewData.passingScore);
                t = Artisan.Utilities.replaceAll(t, '{!user}', LMS.GetStudentName() || 'Student Name');
                t = Artisan.Utilities.replaceAll(t, '{!course}', this.getCourse().getName());
                t = Artisan.Utilities.replaceAll(t, '{!certificate_url}', certificateUrl);
                templates[i] = t;
            }
            template = templates[0];
            reviewQuestionsTemplate = templates[1];

            if (isPretest) {
                template = Artisan.Utilities.replaceAll(template, '{!review_questions}', '');
            } else {
                template = Artisan.Utilities.replaceAll(template, '{!review_questions}', reviewQuestionsTemplate);
            }

            // Delete the answers so test can be taken again
            if (isQuiz && !Artisan.App.state.isPreviousPageReview) {
                this.record.resetQuiz(section.config.id);
            } else {
                this.record.reset();

                // This should send all the data to scorm
                // and trigger the course complete flags
                if (window.top.API && window.top.API.LMSFinish) {
                    window.top.API.LMSFinish("");
                }
            }

            return template;
        },
        getHtmlContent: function () {
            return this.getHtml();
        },
        isPassed: function () {
            var reviewData = this.record.calculateReview(this.sectionId);
            return reviewData.passed;
        }
    });


    // global id counter
    var id = -1;

    // a special type of Container
    Artisan.Models.Course = Artisan.Models.Container.extend({
        init: function (config, dataChunk) {
            this.record = new Artisan.Models.CourseRecord(this, dataChunk);

            for (var i = 0; i < config.sections.length; i++) {
                // the item we're adding is a single-level SCO (such as pre-test, objectives, etc)
                if (config.sections[i].pages && config.sections[i].pages.length > 0) {

                    var sectionType = config.sections[i].sectionType;
                    var testType = config.sections[i].testType;

                    // if pretest or posttest, adjust for test type and add a review page
                    if (sectionType == ArtisanSectionType.Pretest || sectionType == ArtisanSectionType.Posttest) {

                        // recordthe fact that the first page of a test is an intro page
                        config.sections[i].pages[0].isIntro = true;

                        // if Random, GameBoard, or GameWheel, use 20 random questions
                        if (testType == ArtisanTestType.Random || testType == ArtisanTestType.GameBoard || testType == ArtisanTestType.GameWheel) {
                            var origPages = Artisan.Utilities.clone(config.sections[i].pages);
                            var questionPages = [];

                            if (sectionType == ArtisanSectionType.Pretest && this.record.pretestOrder && !config.sections[i].isReRandomizeOrder) {
                                $.each(this.record.pretestOrder, function (index, id) {
                                    var page = $.grep(config.sections[i].pages, function (p) { return p.id == id; })[0];
                                    questionPages.push(page);
                                });
                            }
                            else if (sectionType == ArtisanSectionType.Posttest && this.record.posttestOrder && !config.sections[i].isReRandomizeOrder) {
                                $.each(this.record.posttestOrder, function (index, id) {
                                    var page = $.grep(config.sections[i].pages, function (p) { return p.id == id; })[0];
                                    questionPages.push(page);
                                });
                            }
                            else {
                                this.randomizePages(config.sections[i]);

                                var questionPages = config.sections[i].pages.slice(config.sections[i].pages[0].isIntro ? 1 : 0,
                                                                             config.sections[i].pages[config.sections[i].pages.length - 1].isReview ? config.sections[i].pages.length - 1 : config.sections[i].pages.length);

                                if (sectionType == ArtisanSectionType.Pretest)
                                    this.record.pretestOrder = $.map(questionPages, function (p) { return p.id; });
                                else if (sectionType == ArtisanSectionType.Posttest)
                                    this.record.posttestOrder = $.map(questionPages, function (p) { return p.id; });
                            }
                            config.sections[i].pages = []; // rebuild the pages array with randomly ordered pages
                            config.sections[i].pages.push(origPages[0]); // start with the intro page
                            if (testType == ArtisanTestType.GameBoard)
                                config.sections[i].pages.push({ html: "Select a question from the Gameboard to get started!", isGameStart: true });
                            if (testType == ArtisanTestType.GameWheel)
                                config.sections[i].pages.push({ html: "Click the Spin the Wheel button to get started!", isGameStart: true });
                            for (var j = 0; j < questionPages.length; j++) {
                                config.sections[i].pages.push(questionPages[j]); // add the random/trimmed question pages
                            }
                        }

                        // add the test review page
                        config.sections[i].pages.push(new models.ReviewPage(config.sections[i], config.sections[i].id, this.record));
                    }

                    // We do a special wrap;
                    // since the item we're adding is a single-level SCO (such as pre-test, objectives, etc), 
                    // then we'll put it in a container SCO with the same name
                    // this way, we can always assume we're dealing with at least 2 levels of nesting
                    var original = config.sections[i];
                    config.sections[i] = Artisan.Utilities.clone(config.sections[i]);
                    delete config.sections[i].pages;
                    config.sections[i].sections = [original];
                    config.sections[i].isWrapper = true;
                    config.sections[i].id = id--;
                } else {
                    // Look for quizes
                    var courseRecord = this.record;
                    var me = this;
                    var sectionSearch = function (section) {
                        for (var j = 0; j < section.sections.length; j++) {
                            var innerSection = section.sections[j];

                            if (innerSection.sections && innerSection.sections.length) {
                                sectionSearch(innerSection);
                            }

                            if (innerSection.pages && innerSection.pages.length > 0 && innerSection.isQuiz) {
                                innerSection.pages[0].isIntro = true;
                                innerSection.pages.push(new models.ReviewPage(innerSection, innerSection.id, courseRecord));

                                me.randomizePages(innerSection);
                            }
                        }
                    }
                    if (config.sections[i].sections && config.sections[i].sections.length) {
                        sectionSearch(config.sections[i]);
                    }
                }
            }

            this._super(config);

            // if completion is Inline Questions then add a review page to the end of the last section
            // we don't need to worry about a posttest being at the end because it would be removed at package time
            if (config.completionMethod == ArtisanCourseCompletionMethod.InlineQuestions || config.completionMethod == ArtisanCourseCompletionMethod.Navigation) {
                var section = this.getLastSectionWithPages();
                if (section != null) {
                    section.pages.push(new models.ReviewPage(section, section.getId(), this.record));
                }
            }
        },
        // Takes a section config object. If calling this a Artisan.Models.Container
        // pass in the Artisan.Models.Container.config parameters. This will rebuild
        // the configuration, then you need to recreate the Artisan.Models.Container.
        randomizePages: function (sectionConfig, max) {
            max = max ? max : sectionConfig.maxQuestions ? sectionConfig.maxQuestions : 20;

            var hasIntro = sectionConfig.pages[0].isIntro;
            var hasReview = sectionConfig.pages[sectionConfig.pages.length - 1].isReview;
            var currentPages = sectionConfig.pages.slice(hasIntro ? 1 : 0, hasReview ? sectionConfig.pages.length - 1 : sectionConfig.pages.length);
            var originalPages, usedPages;

            if (sectionConfig.originalPages) {
                originalPages = sectionConfig.originalPages;
            } else {
                originalPages = currentPages;
                sectionConfig.originalPages = originalPages;
            }

            if (sectionConfig.usedPages) {
                usedPages = sectionConfig.usedPages;
            } else {
                usedPages = {};
            }

            originalPages.sort(Artisan.Utilities.randomSort);
            var usedPagesArray = [];
            var newPages = $.map(originalPages, function (p) {
                if (!usedPages.hasOwnProperty(p.id)) {
                    return p;
                } else {
                    usedPagesArray.push(p);
                }
            });

            if (newPages.length > max) {
                newPages = newPages.slice(0, max);
            } else {
                var usedPagesRequired = max - newPages.length;
                for (var i = 0; i < usedPagesRequired && i < usedPagesArray.length; i++) {
                    newPages.push(usedPagesArray[i]);
                }
                newPages.sort(Artisan.Utilities.randomSort);
            }

            $.each(newPages, function (i, p) {
                if (!usedPages.hasOwnProperty(p.id)) {
                    usedPages[p.id] = true;
                }
            });

            if (hasIntro) {
                newPages.unshift(sectionConfig.pages[0]);
            }

            if (hasReview) {
                newPages.push(new models.ReviewPage(sectionConfig, sectionConfig.id, this.record));
            }

            if (this.config && (this.config.completionMethod == ArtisanCourseCompletionMethod.InlineQuestions || this.config.completionMethod == ArtisanCourseCompletionMethod.Navigation)) {
                var section = this.getLastSectionWithPages();
                if (section != null && section.getId() == sectionConfig.id) {
                    newPages.push(new models.ReviewPage(section, section.getId(), this.record));
                }
            }

            sectionConfig.usedPages = usedPages;
            sectionConfig.pages = newPages;
        },
        isSequentialNavigation: function () {
            return this.config.navigationMethod == ArtisanCourseNavigationMethod.Sequential || this.config.navigationMethod == ArtisanCourseNavigationMethod.FullSequential;
        },
        isFullSequentialNavigation: function () {
            return this.config.navigationMethod == ArtisanCourseNavigationMethod.FullSequential;
        },
        getPassingScore: function () {
            return this.config.passingScore || 80;
        },
        setPassingScore: function (val) {
            this.config.passingScore = val;
        },
        setNavigationMethod: function (val) {
            this.config.navigationMethod = val;
        },
        setCompletionMethod: function (val) {
            this.config.completionMethod = val;
        },
        getSco: function (id) {
            return this.getSection(id);
        },
        getFirstScoWithContent: function () {
            return this.getFirstSectionWithPages().getParent();
        },
        getLastScoWithContent: function () {
            return this.getLastSectionWithPages().getParent();
        },
        getScos: function () {
            return this._getScos(this);
        },
        _getScos: function (section) {
            var result = [];
            if (!section.hasPages()) {
                var sco = section;
                result.push(sco);
                if (section.sections) {
                    sco.scos = [];
                    for (var i = 0; i < section.sections.length; i++) {
                        sco.scos = sco.scos.concat(this._getScos(section.sections[i]));
                    }
                }
            }
            return result;
        },
        getLearningObjects: function (scoId) {
            var sco = this.getSection(scoId);
            var result = [];
            for (var i = 0; i < sco.sections.length; i++) {
                var section = sco.sections[i];
                if (section.hasPages()) {
                    result.push(section);
                }
            }
            return result;
        },
        getLearningObject: function (learningObjectId) {
            return this.getSection(learningObjectId);
        },
        getNextLearningObject: function (learningObjectId) {
            return this._getNextSection(this, learningObjectId, false, 'forward', function (section) {
                return section.hasPages();
            });
        },
        getPreviousLearningObject: function (learningObjectId) {
            return this._getNextSection(this, learningObjectId, false, 'reverse', function (section) {
                return section.hasPages();
            });
        },
        getNextSco: function (scoId) {
            return this._getNextSection(this, scoId, false, 'forward', function (section) {
                return !section.hasPages();
            });
        },
        getPreviousSco: function (scoId) {
            return this._getNextSection(this, scoId, false, 'reverse', function (section) {
                return !section.hasPages();
            });
        },
        getQuestionPageCount: function (section, gradableOnly, excludeQuiz) {
            if (!section) {
                section = this.config;
            }
            if (typeof gradableOnly == 'undefined') {
                gradableOnly = this.config.completionMethod == ArtisanCourseCompletionMethod.InlineQuestions;
            }
            if (typeof excludeQuiz == 'undefined') {
                excludeQuiz = false;
            }

            var count = 0;
            // add all pages from this section
            if (section.pages && section.pages.length && (excludeQuiz ? !section.isQuiz : true)) {
                count += $.grep(section.pages, function (p) { return p.questionType && (gradableOnly ? p.isGradable : true) }).length;
            }
            // loop through all sub-sections to get their pages
            if (section.sections && section.sections.length) {
                for (var i = 0; i < section.sections.length; i++) {
                    count += this.getQuestionPageCount(section.sections[i], gradableOnly, excludeQuiz);
                }
            }
            return count;
        },
        // used for review
        getFirstQuestionPage: function (section) {
            if (section.sectionType == ArtisanSectionType.Posttest || section.sectionType == ArtisanSectionType.Pretest) {
                // only look within the current section for post/pre tests
                return section.getFirstQuestionPage();
            } else {
                var total = this.getAllQuestionPages(this, true);
                return total[0];
            }
        },
        getNextQuestionPage: function (section, pageId) {
            if (section.sectionType == ArtisanSectionType.Posttest || section.sectionType == ArtisanSectionType.Pretest) {
                // only look within the current section for post/pre tests
                return section.getNextQuestionPage(pageId);
            } else {
                var total = this.getAllQuestionPages(this, true);
                for (var i = 0; i < total.length; i++) {
                    if (total[i].getId() == pageId) {
                        return total[i + 1];
                    }
                }
                return null;
            }
        },

        getAllQuestionPages: function (section, gradableOnly) {
            var total = [];
            if (!section) {
                section = this.config;
            }
            if (section.pages && section.pages.length) {
                for (var i = 0; i < section.pages.length; i++) {
                    var p = section.pages[i];
                    // question page
                    if (p.isQuestion()) {
                        // only track gradable
                        if (gradableOnly) {
                            if (p.isGradable()) {
                                total.push(p);
                            }
                        } else {
                            total.push(p);
                        }
                    }
                }
            }
            // loop through all sub-sections to get their pages
            if (section.sections && section.sections.length) {
                for (var i = 0; i < section.sections.length; i++) {
                    total = total.concat(this.getAllQuestionPages(section.sections[i], gradableOnly));
                }
            }
            return total;
        },
        _getNextSection: function (parentSection, sectionId, takeNext, direction, validator) {
            var len = parentSection.sections.length;
            for (var i = 0; i < len; i++) {
                // simple toggle to either search from the top to bottom (forward) or bottom to top (reverse) so "next" and "previous" use the same codebase
                var section = direction == 'forward' ? parentSection.sections[i] : parentSection.sections[len - i - 1];
                // if we copy an object, it'll show in our list here with the same id twice (think pre-test), so keep going if we run into the same id twice
                if (takeNext && validator(section)) {
                    return section;
                }
                if (section.getId() == sectionId) {
                    takeNext = true;
                }
                var found = this._getNextSection(section, sectionId, takeNext, direction, validator);
                if (found) {
                    return found;
                }
            }
            return null;
        },
        isNavigationMethod: function () {
            return (this.config.completionMethod == ArtisanCourseCompletionMethod.Navigation);
        },
        isTestMethod: function () {
            return (this.config.completionMethod == ArtisanCourseCompletionMethod.Test || this.config.completionMethod == ArtisanCourseCompletionMethod.TestForceRestart);
        },
        isTestForceRestartMethod: function () {
            return (this.config.completionMethod == ArtisanCourseCompletionMethod.TestForceRestart);
        },
        isInlineQuestionsMethod: function () {
            return (this.config.completionMethod == ArtisanCourseCompletionMethod.InlineQuestions);
        },
        hasSurvey: function () {
            // Survey is always at the end
            return (this.sections && this.sections.length > 0 && this.sections[this.sections.length - 1].config.sectionType == ArtisanSectionType.Survey);
        },
        getSurveyPage: function () {
            if (this.hasSurvey() == false) {
                return null;
            }

            // Survey is always at the end, and has exactly one page
            return this.sections[this.sections.length - 1].sections[0].pages[0];
        },
        getSurveySection: function () {
            if (this.hasSurvey() == false) {
                return null;
            }

            // Survey is always at the end
            return this.sections[this.sections.length - 1];
        }
    });

    // the CourseRecord class is for recording everything a student does while taking a course
    // (unlike the Course class, which only contains information about a course independent from student interaction)
    Artisan.Models.CourseRecord = Class.extend({
        init: function (course, dataChunk) {
            this.course = course;
            this.answeredQuestions = [];
            this.visitedScos = {};
            this.visitedLos = {};
            this.visitedPages = {};
            this.timeSpent = {};
            if (dataChunk) {
                this.parseDataChunk(dataChunk);
            }
        },
        answeredQuestions: null,
        pretestOrder: null,
        posttestOrder: null,
        visitedScos: null,
        visitedLos: null,
        visitedPages: null,
        timeSpent: null,
        recordInteraction: function (state, userAnswers) {
            var page = state.page;
            var id = page.getId();
            var learningObjectId = state.learningObject.getId();
            var results = page.checkAnswers(userAnswers);
            var isCorrect = (!results.incorrect.length);
            var correctAnswers = $.grep(page.config.answers, function (a) { return a.isCorrect == true; });
            var response;

            var QuestionForSectionType = state.learningObject.config.sectionType;

            var PrefixID;
            if (QuestionForSectionType == 6) {
                PrefixID = 'P'  // Post-test
            }
            else if (QuestionForSectionType == 5) {
                PrefixID = 'R'  // Pre-test
            }
            else {
                PrefixID = 'I'
            }

            if (page.isAllThatApply()) {
                // translate each id to its corresponding index char
                lmsResponses = [];
                $(userAnswers).each(function (index, ua) {
                    var ans = $.grep(page.config.answers, function (a) { return a.id == ua; })[0];
                    lmsResponses.push({ 'Short': Artisan.Utilities.indexToChar(ans.sort), 'Long': ans.text });
                });

                // translate each id to its corresponding index char
                var correctResponse = [];
                $(correctAnswers).each(function (index, ca) {
                    correctResponse.push({ 'Short': Artisan.Utilities.indexToChar(ca.sort), 'Long': ca.text });
                });

                var user_answers = $.map(lmsResponses, function (r) { return r.Short });
                var correct_answers = $.map(correctResponse, function (r) { return r.Short });

                LMS.RecordFillInInteraction(id, user_answers, isCorrect, correct_answers, QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId);

                //only store the chars in our array
                response = $.map(lmsResponses, function (r) { return r.Short });
            }
            else if (page.isMultipleChoice() || page.isTrueFalse()) {
                var responseId = (userAnswers[0] ? userAnswers[0] : '');
                var selectedAnswer = $.grep(page.config.answers, function (a) { return a.id == responseId; })[0]; // can only have one selected answer
                var correctAnswer = correctAnswers[0]; // can only have one correct answer
                isCorrect = (correctAnswer.id == responseId);

                if (page.isMultipleChoice()) {
                    var selectedAnswerChar = Artisan.Utilities.indexToChar(selectedAnswer.sort);
                    var correctAnswerChar = Artisan.Utilities.indexToChar(correctAnswer.sort);
                    //var selectedAnswerRI = API.CreateResponseIdentifier(selectedAnswerChar, selectedAnswer.text);
                    //var correctAnswerRI = API.CreateResponseIdentifier(correctAnswerChar, correctAnswer.text);

                    LMS.RecordFillInInteraction(id, selectedAnswerChar, isCorrect, correctAnswerChar, QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId);

                    response = selectedAnswerChar;
                }
                else if (page.isTrueFalse()) {
                    var blnResponse = (selectedAnswer.text == 'True');
                    var blnCorrectResponse = (correctAnswer.text == 'True');

                    LMS.RecordFillInInteraction(id, blnResponse, isCorrect, blnCorrectResponse, QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId);
                    response = blnResponse;
                }
            }
            else if (page.isImageQuestion()) {
                var strResponse = JSON.stringify(userAnswers); // (userAnswers[0] ? userAnswers[0] : '');
                var strCorrectResponse = correctAnswers[0].text;
                var strIsCorrectAnswer;
                if (isCorrect) {
                    strIsCorrectAnswer = 'Correct';
                }
                else {
                    strIsCorrectAnswer = 'Incorrect';
                }
                LMS.RecordFillInInteraction(id, 'image response - ' + strIsCorrectAnswer, isCorrect, 'Correct', QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId);
                response = JSON.parse(strResponse);
            }
            else if (page.isFillInTheBlank()) {
                var strResponse = userAnswers.join(',');

                var correctResponses = [];
                $(correctAnswers).each(function (index, ca) {
                    correctResponses.push(ca.text);
                });
                var strCorrectResponse = correctResponses.join(',');

                LMS.RecordFillInInteraction(id, strResponse, isCorrect, strCorrectResponse, QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId);
                response = userAnswers;
            }
            else if (page.isMatching()) {
                // Compress responses for data chunk
                var correctResponse = {};
                var userResponse = {};
                $(page.config.answers).each(function (index, ans) {
                    correctResponse[ans.id] = ans.id;
                    userResponse[ans.id] = $.map(userAnswers, function (a, i) {
                        if (a.answerId == ans.id) { return a.userAnswer; }
                    });
                });

                LMS.RecordFillInInteraction(id, JSON.stringify(userResponse), isCorrect, JSON.stringify(correctResponse), QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId);
                response = userResponse;
            }
            else if (page.isCategoryMatching()) {
                // Compress responses for data chunk
                var correctResponse = {};
                var userResponse = {};
                $(page.config.answers).each(function (index, ans) {
                    correctResponse[ans.id] = $.map(userAnswers, function (a, i) {
                        if (a.matchId.split('-')[0] == ans.id) { return a.matchId; }
                    });
                    userResponse[ans.id] = $.map(userAnswers, function (a, i) {
                        if (a.userAnswer == ans.id) { return a.matchId; }
                    });
                });

                LMS.RecordFillInInteraction(id, JSON.stringify(userResponse), isCorrect, JSON.stringify(correctResponse), QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId);
                response = userResponse;
            }
            else if (page.isLongAnswer()) {
                var strResponse = userAnswers.join(',');

                var correctResponses = [];
                $(correctAnswers).each(function (index, ca) {
                    correctResponses.push(ca.text);
                });
                var strCorrectResponse = correctResponses.join(',');

                LMS.RecordFillInInteraction(id, strResponse, isCorrect, strCorrectResponse, QuestionForSectionType, QuestionForSectionType, QuestionForSectionType, learningObjectId);
                response = userAnswers;
            }

            // create the answer to be stored locally
            var answerObj = {
                id: id,
                isGradable: page.isGradable(),
                learningObjectId: learningObjectId,
                response: response,
                isCorrect: isCorrect
            };

            // check to see if this question has been answered before; this deals with mal-formed courses that have questions that are identical,
            // i.e., have the same ID and everything, and are both gradeable or weird situations where something gets half-saved, etc
            var hit = false;
            for (var i = 0; i < this.answeredQuestions.length; i++) {
                if (answerObj.id == this.answeredQuestions[i].id && answerObj.learningObjectId == this.answeredQuestions[i].learningObjectId) {
                    Artisan.Utilities.extend(this.answeredQuestions[i], answerObj);
                    hit = true;
                    break;
                }
            }

            if (!hit) {
                // no hit on the existing questions, so throw it on the end
                this.answeredQuestions.push(answerObj);
            }

            return isCorrect;
        },
        getFailedQuestions: function () {
            if (this.course.isNavigationMethod()) {
                return [];
            }

            var result = [];
            for (var i = 0; i < this._answeredQuestionsCache.length; i++) {
                var q = this._answeredQuestionsCache[i];
                // only add it if:
                if (this.course.isInlineQuestionsMethod()) {
                    if (q.isGradable && !q.isCorrect) {
                        result.push(q);
                    }
                } else {
                    var section = this.course.getSection(q.learningObjectId);
                    var isPosttest = section.config.sectionType == ArtisanSectionType.Posttest;

                    if (section.isTestSection() && isPosttest && !q.isCorrect) {
                        result.push(q);
                    }
                }
            }
            return result;
        },
        setBookmark: function (state) {
            if (this.bookmarkDisabled) {
                return;
            }
            var bookmark = state.sco.getId() + '|' + state.learningObject.getId() + '|' + state.page.getId() + '|' + state.gameTotal;
            this.saveDataChunk();
            LMS.SetBookmark(bookmark);
        },
        getBookmark: function () {
            return LMS.GetBookmark();
        },
        clearBookmark: function () {
            LMS.SetBookmark(''); // has to be an empty string
        },
        disableBookmark: function () {
            this.bookmarkDisabled = true;
            this.clearBookmark();
        },
        buildAnswerCache: function () {
            // Store the answers for the questions review
            this._answeredQuestionsCache = this.answeredQuestions.slice(0); // copy
        },
        reset: function () {
            this.answeredQuestions.length = 0; // empty the array
            this.clearBookmark();
            this.saveDataChunk();
            LMS.CommitData();
        },
        resetQuiz: function (learningObjectId) {
            this.answeredQuestions = $.grep(this.answeredQuestions, function (q) { return q.learningObjectId != learningObjectId });
        },
        saveDataChunk: function () {
            LMS.SetDataChunk(this.buildDataChunk());
        },
        buildDataChunk: function () {
            var compressedData = {
                v: {},      // visited scos, only applies to sequential navigation courses
                vl: {},     // visited los, only applies to full sequential navigation courses
                vp: {},     // visited pages, only applies for turning off timeouts if the page has been visited
                q: {},      // answered question data
                pre: null,  // pretest question order (if random)
                pst: null,   // posttest question order (if random)
                t: {} // timing values for course/sco/lo
            };
            $.each(this.answeredQuestions, function (index, question) {
                var idKey = question.learningObjectId + '-' + question.id;
                compressedData.q[idKey] = { r: question.response, c: question.isCorrect, g: question.isGradable };
            });

            compressedData.v = this.visitedScos;
            compressedData.vl = this.visitedLos;
            compressedData.vp = this.visitedPages;

            compressedData.pre = this.pretestOrder;
            compressedData.pst = this.posttestOrder;

            compressedData.t = this.timeSpent;

            return JSON.stringify(compressedData);
        },
        parseDataChunk: function (data) {
            var me = this;
            if (!data) {
                return;
            }

            var dataObj = JSON.parse(data);

            if (dataObj) {
                if (dataObj.q) { // answered question data
                    me.answeredQuestions = [];
                    $.each(dataObj.q, function (idKey, value) {
                        var ids = idKey.split('-');
                        var hit = false;
                        var answerObj = {
                            id: parseInt(ids[1]),
                            learningObjectId: parseInt(ids[0]),
                            response: value.r,
                            isCorrect: value.c,
                            isGradable: value.g
                        };
                        // ensure no duplicates
                        for (var i = 0; i < me.answeredQuestions.length; i++) {
                            if (answerObj.id == me.answeredQuestions[i].id && answerObj.learningObjectId == me.answeredQuestions[i].learningObjectId) {
                                Artisan.Utilities.extend(me.answeredQuestions[i], answerObj);
                                hit = true;
                                break;
                            }
                        }
                        if (!hit) {
                            me.answeredQuestions.push(answerObj);
                        }
                    });
                }
                if (dataObj.pre) { // pretest question order (if random)
                    me.pretestOrder = dataObj.pre;
                }
                if (dataObj.pst) { // posttest question order (if random)
                    me.posttestOrder = dataObj.pst;
                }

                if (dataObj.v) { // visited scos
                    me.visitedScos = dataObj.v;
                }
                if (dataObj.vl) { //visited los
                    me.visitedLos = dataObj.vl;
                }
                if (dataObj.vp) { // visited pages
                    me.visitedPages = dataObj.vp;
                }
                if (dataObj.t) { // time spent per course/sco/lo
                    me.timeSpent = dataObj.t;
                }

            }
        },
        // To be called every time a question is answered in a post test
        // ONLY WHEN TestForceRestart is enabled. This way we keep track
        // of the current score as the student progresses through the test
        // Then if the student is booted from the course or leaves we will
        // have a score saved.
        calculateInProgressPostTestScore: function (sectionId) {
            this.buildAnswerCache();

            var reviewData = this.calculateReview(sectionId);

            // Coppied from the review getHtml method. 
            // This will get called every 30 seconds while in a post test

            // OK, so, apparently some LMSes see the min/max as not pass/fail values, but as min/max the *student* got and then somehow add them together???
            // soooo, we set to 100, 0 to ensure that'll always set to 100
            LMS.SetScore(reviewData.score, 100, 0);

            // ok, we abuse the speed preference here
            LMS.SetSpeedPreference(0);

            if (reviewData.passed) {
                LMS.SetPassed();
            } else {
                LMS.SetFailed();
            }

            // this is abuse, but seems to force things to save properly
            // per Nu, 5/9/2014
            LMS.CommitData();

        },
        calculateReview: function (sectionId) {
            var total, answeredQuestions;
            var passingScore = this.course.getPassingScore();
            var section = sectionId ? this.course.getSection(sectionId) : null;

            var isQuizReview = !Artisan.App.state.isPreviousPageReview && section.config.isQuiz;

            if (!this._answeredQuestionsCache || this._answeredQuestionsCache.length == 0) {
                this.buildAnswerCache();
            }

            // If it's navigation mode, and we arn't at a quiz review page
            if (this.course.isNavigationMethod() && !isQuizReview) {
                return {
                    total: 1,
                    answered: 1,
                    correct: 1,
                    score: 100,
                    passingScore: passingScore,
                    passed: true
                }
            }

            if (section && (this.course.isTestMethod() || isQuizReview)) { // pretest or posttest or quiz
                if (!section.pages) {
                    return;
                }

                if (section.config.passingScore > 0) {
                    passingScore = section.config.passingScore;
                }

                total = $.grep(section.pages, function (p) { return p.config.questionType }).length;
                answeredQuestions = $.grep(this._answeredQuestionsCache, function (q) { return q.learningObjectId == sectionId });
            }
            else { // inline questions review
                total = this.course.getQuestionPageCount(null, true, true);
                answeredQuestions = $.grep(this._answeredQuestionsCache, function (q) { return q.isGradable });
            }

            var answered = answeredQuestions.length;
            var correct = $.grep(answeredQuestions, function (q) { return q.isCorrect == true }).length;
            var score = Math.round((correct / total) * 100);

            var preTestKeepingScore

            return {
                total: total,
                answered: answered,
                correct: correct,
                score: score,
                passingScore: passingScore,
                passed: score >= passingScore
            };
        },
        addVisitedSco: function (scoId) {
            this.visitedScos[scoId] = true;
        },
        hasVisitedSco: function (scoId) {
            return this.visitedScos[scoId];
        },
        addVisitedLo: function (loId) {
            this.visitedLos[loId] = true;
        },
        hasVisitedLo: function (loId) {
            return this.visitedLos[loId];
        },
        addVisitedPage: function (pageId) {
            this.visitedPages[pageId] = true;
        },
        hasVisitedPage: function (pageId) {
            return this.visitedPages[pageId];
        },

        // Time helpers for data chunk
        setLoTime: function (id) {
            this.setTime('l', id);
        },
        setCourseTime: function (id) {
            this.setTime('c', id);
        },
        setScoTime: function (id) {
            this.setTime('s', id);
        },
        getLoTime: function (id) {
            return this.getTime('l', id);
        },
        getCourseTime: function (id) {
            return this.getTime('c', id);
        },
        getScoTime: function (id, time) {
            return this.getTime('s', id, time);
        },
        addLoTime: function (id, time) {
            return this.addTime('l', id, time);
        },
        addCourseTime: function (id, time) {
            return this.addTime('c', id, time);
        },
        addScoTime: function (id, time) {
            return this.addTime('s', id, time);
        },
        addTime: function (type, id, time) {
            if (type != 'c' && type != 'l' && type != 's') {
                return;
            }

            var currentTime = this.getTime(type, id);

            currentTime += time;

            this.setTime(type, id, currentTime);
        },
        setTime: function (type, id, time) {
            if (type != 'c' && type != 'l' && type != 's') {
                return;
            }
            this.timeSpent[type + id] = time;
        },
        getTime: function (type, id) {
            if (!this.timeSpent[type + id]) {
                return 0;
            }

            return this.timeSpent[type + id];
        }
    });
}());