// *******************************************************************************************************************
// *******************************************************************************************************************

// windowObject Class

function WindowObjectClass(){
    //properties
	this.browser = isIE;
    this.screenWidth = screen.width;
    this.screenHeight = screen.height;
    this.windowWidth = (this.browser) ? (document.body.clientWidth) : (window.innerWidth);
    this.windowHeight = (this.browser) ? (document.body.clientHeight) : (window.innerHeight);
    this.windowLeft = (this.browser) ? (window.screenLeft) : (window.screenX);
    this.windowTop = (this.browser) ? (window.screenTop) : (window.screenY);
    this.pixelToPercentRatio = this.windowWidth / (1 * 100);
    this.percentToPixelRatio = (1 * 100) / this.windowWidth;
    
    // accessors (read only)
    
    this.getScreenWidth = getScreenWidth;
    this.getScreenHeight = getScreenHeight;
    this.getWindowWidth = getWindowWidth;
    this.getWindowHeight = getWindowHeight;
    this.getWindowLeft = getWindowLeft;
    this.getWindowTop = getWindowTop;
    this.getPixelToPercentRatio = getPixelToPercentRatio;
    this.getPercentToPixelRatio = getPercentToPixelRatio;
    
    // debuggin methods
    this.showProperties = showProperties;
}

function getScreenWidth(){
    return this.screenWidth;
}

function getScreenHeight(){
    return this.screenHeight;
}

function getWindowWidth(){
    this.windowWidth = (this.browser) ? (document.body.clientWidth) : (window.innerWidth);
    return this.windowWidth;
}

function getWindowHeight(){
    this.windowHeight = (this.browser) ? (document.body.clientHeight) : (window.innerHeight);
    return this.windowHeight
}

function getWindowLeft(){
    this.windowLeft = (this.browser) ? (window.screenLeft) : (window.screenX);
    return this.windowLeft;
}

function getWindowTop(){
    this.windowTop = (this.browser) ? (window.screenTop) : (window.screenY);
    return this.windowTop;
}

function getPixelToPercentRatio(){
    return this.getWindowWidth() / (1 * 100);
}

function getPercentToPixelRatio(){
    return (1 * 100) / this.getWindowWidth();
}

function showProperties(){
	alert (	"getScreenWidth ::: " + this.getWindowWidth() + "\n" 
	+ "getScreenHeight ::: " + this.getScreenHeight() + "\n" 
	+ "getWindowWidth ::: " + this.getWindowWidth() + "\n" 
	+ "getWindowHeight ::: " + this.getWindowHeight() + "\n" 
	+ "getWindowLeft ::: " + this.getWindowLeft() + "\n" 
	+ "getWindowTop ::: " + this.getWindowTop() + "\n" 
	+ "getPixelToPercentRatio ::: " + this.getPixelToPercentRatio() + "\n" 
	+ "getPercentToPixelRatio ::: " + this.getPercentToPixelRatio() + "\n"
    
}

var windowObj;

function createWindowObj(){
    windowObj = new WindowObjectClass;
}

// *******************************************************************************************************************
// *******************************************************************************************************************
