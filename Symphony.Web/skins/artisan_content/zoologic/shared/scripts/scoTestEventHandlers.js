// **************************************************
// File name : scoTestEventHandlers.js  
//
// These functions are called by the event handlers
// in the HTML-page for the test navigation frame
//
// by Panu

// **************************************************



function disab(){
    if (parent.contentFrame.enableDrag) {
        parent.contentFrame.enableDrag(false);
        parent.contentFrame.NAF_isDisabled = true;
        parent.contentFrame.disableAnswerWidgets();
    }
}

function enab(){
    if (parent.contentFrame.enableDrag) {
        parent.contentFrame.enableDrag(true);
        parent.contentFrame.NAF_isDisabled = false;
    }
}


function scoGoToQuestion(qNumber){
    // Triggered when a check/number -box for a page is 
    // clicked. Should cause the showing of  the correct page
    
    
    
    
    var st = parent.getState();
    if (st == 'review') {
        disab();
    }
    
    if (st == 'remediation') {
        parent.goToReviewMode();
        disab();
        
    }
    
    if (st == 'results') {
        parent.goToReviewMode();
        disab();
    }
    
    
    if (st == null) // == test modes
    {
        enab();
    }
    
    
    
    showPageAtIndex(qNumber);
    
    // ***********************************************************************************************
    // Carlos addition to disable the next/previous buttons when u reach the beggining/end of a test
    
 	if (!isIE) 
		return noqs = getNumberOfQuestions();
    
    checkNextPreviousButtons(parent.currentPageIndex(), noqs);
    
    // end of carlos addition
    // *************************************************************************************************


}



function loadNextPage(){

    var st = parent.getState();
    
    if (st == 'remediation') {
        parent.goToReviewMode()
    }
    if (st == 'results') {
        parent.goToReviewMode()
    }
    
    var aSco = parent.getSco();
    if (aSco == null) { // afer refresh
        return;
    }
    
    noqs = getNumberOfQuestions();
    
    if (parent.currentPageIndex() == noqs) {
        alert("This is the last question of the test.");
        return
    }
    
    aSco.loadYourNextPage();
    
    var newPindex = currentPageIndex();
    setQuestionNumber(newPindex);
    refreshCurrentQuestionMarker(newPindex); // Carlos... so it only updates the image for the current marker
    // refreshNavigationBar();

}



function currentPageIndex(){
    return parent.currentPageIndex()
}

function loadPreviousPage(){


    var st = parent.getState();
    if (st == 'remediation') {
        parent.goToReviewMode()
    }
    var aSco = parent.getSco();
    if (aSco == null) { // afer refresh
        return;
    }
    
    aSco.loadYourPreviousPage();
    
    
    var newPindex = currentPageIndex();
    setQuestionNumber(newPindex);
    refreshCurrentQuestionMarker(newPindex); // Carlos... so it only updates the image for the current marker
    // refreshNavigationBar();
}





////////////////// Toolbar functions ////////////////////////

function scoSubmit(){
    parent.submitButtonClicked()
}

function scoDelete(){
    parent.deleteButtonClicked()
}


function scoExit(){
    parent.exitButtonClicked()
}


function scoResults(){
    parent.resultsButtonClicked()
}


function scoFirstAid(){
    parent.firstAidButtonClicked()
}

function scoPrefs(){
    parent.prefsButtonClicked()
}

function scoRefresh(){
    parent.resfreshButtonClicked()
}


//-----------------
function scoBack(){
    go_back();
}



//================= IMPLEMENTATION ========================

/*
 IN lessons, you go to a page by page-number like 5.1
 However in Tests we only have exercises, so it is easier
 to go to the page in the given position directly
 #sco117.
 */
function showPageAtIndex(pindex)//       ==============
{


    var theSco = parent.getSco();
    
    var nurl = parent.scoInfo()[pindex];
    if (!nurl) {
        alert("Invalid page-index: " + pnoString);
        return
    }
    
    setQuestionNumber(pindex);
    refreshCurrentQuestionMarker(pindex); // Carlos... so it only updates the image for the current marker
    // refreshNavigationBar();
    
    
    parent.currentPageIndexIs(pindex);
    //	pageIndex_VAR	= index  ; 
    defaultStatus = pindex + ". " + nurl;
    
    theSco.loadUrlInSameDir(nurl)
    
}


