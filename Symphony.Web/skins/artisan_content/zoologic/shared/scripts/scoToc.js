/*************************************************\
 ** File:   shared/scripts/scoToc.js
 ** ------------------------------
 
 \************************************************/
function scoPageLoaded(idString, classString, windowObject, pageNoString){
    scoPageLoadedCOMMON(idString, classString, windowObject, pageNoString);
    
    if (parent.uploadData) 
        parent.uploadData();
    
}


function pindexOfPageNo2(pno){
    if (parent.pageNoToIndexMap) {
        return parent.pageNoToIndexMap()[pno];
    }
    return 1; // helpos when viewing pages without the frameset
}



function hasBeenVisitedL(pindex){
    if (parent.hasPageBeenVisited) {
        return parent.hasPageBeenVisited(pindex)
    }
    return false;
    
    
}


function hasBeenVisitedE(pno){
    if (parent.hasExercisesBeenVisited) {
        return parent.hasExercisesBeenVisited(pno);
    }
    return false;
    
}



function finishPageDisplay(){
    // assume less than 41 pages
    
    // #sco80
    for (pno = 2; pno <= 41; pno++) {
    
        var pindex = pindexOfPageNo2(pno);
        var iname = 'imgcmark' + pno;
        
        if (hasBeenVisitedL(pindex)) {
            document.images[iname].src = "../shared/images/WV_Check_Mark.gif";
        }
        if (hasBeenVisitedE(pno)) { // art("YES e " + pno )
            iname = 'imgecmark' + pno;
            var image = document.images[iname];
            if (image) 
                image.src = "../shared/images/WV_Check_MarkE.gif";
        }
        
        
    }
    
    //  showProps(parent.uaDict());
    //  showProps (parent.exercisesOfLesson(3) ); 

}






function lessonIconClicked(urlString){

    parent.getSco().loadUrlInSameDir(urlString);
    
}


function lessonLinkClicked(urlString){
    parent.getSco().loadUrlInSameDir(urlString);
    
}

function exerciseIconClicked(pindex){

    exerciseLinkClicked(pindex);
    
}


function exerciseLinkClicked(pindex){
    var urlString = parent.scoInfo()[pindex];
    parent.getSco().loadUrlInSameDir(urlString);
}

