// Copyright 2001, 2002 Zoologic, Inc. 

// Creates a random object with a value within the given range
function randomObject(bottomValue,topValue,precision) {
	//member variables
	this.precision = precision;//(!precision) ? precision:1; 
	this.bottomValue = bottomValue;
	this.topValue = topValue;
	this.value = getRandom(bottomValue,topValue,precision);
	
	// member functions
	this.generate = generate;
	this.updateRange = updateRange;
	this.setValue = setValue;
	this.toString = randomObject_toString;
	this.formatString = formatStringOne;
}


function formatStringOne(roundToPlaces,commas,symbol) {
	return formatString(this.value,roundToPlaces,commas,symbol);
}

function generate() {
	this.value = getRandom(this.bottomValue,this.topValue,this.precision);
}

function updateRange(bottom,top,precision) {
	this.bottomValue = (bottom)?bottom:this.bottomValue;
	this.topValue = (top)?top:this.topValue;
	this.precision = (precision)?precision:this.precision;
}

function setValue(newValue) {
	this.value = newValue;
}
function getRandom(rangeFrom,rangeTo,precision) {
	reciprocal = 1/precision;
	rangeFrom *= reciprocal;
	rangeTo *= reciprocal;
	number = (Math.floor(Math.random()*(rangeTo-rangeFrom+1)+rangeFrom))/reciprocal;
	return number;
}

function randomObject_toString() {
	stringValue = this.value;
	return Number(stringValue);
}





// ***********************************************************************************************
// ***********************************************************************************************
// GENETICALLY MODIFIED NEF FUNCTIONS SO THEY WORK WITH A PAGE MODEL VERSION OF THE CODE	

function displayNumericAnswerFieldSTATIC(mask,retryisEnabled) {
	NAF_SetMask(mask,retryisEnabled);
	NAF_OnChange('userAnswerChanged()');
	NAF_DisplayNORetry(retryisEnabled);
}

function NAF_DisplayNORetry(retryisEnabled) {
	
	var text = '';

		text += '<table id="answerWidget" name="answerWidget" border="0" cellpadding="0" cellspacing="0">';
		text += '<tr>';
	
		text += '<td nowrap>';
		text += '<font face="Arial"><b>';
		text += 'Answer&nbsp;&nbsp;=&nbsp;&nbsp;';
		text += '</b></font>';
		text += '</td>';

	var decimalPart = false;
	for (var i = 0; i < NAF_mask.length; i++) {
		var ch = NAF_mask.substr(i, 1);
		switch (ch) {
			case '9':
			case '1':
				text += '<td nowrap>';
				text += '<input type="text"';
//				if (document.all) // IE
					text += ' class="numericInputFieldIE"';
//				else
//					text += ' class="numericInputFieldNN"';
				text += ' name="NAF_Digit' + NAF_digits + '" id="NAF_Digit' + NAF_digits + '"';
				text += ' autocomplete="off" maxlength="1" size="1"';
				text += ' onfocus="select()"';
				if (isIE) //IE
					text += ' onkeydown="NAF_KeyDownIE(' + NAF_digits + ')"';
				text += '>';
				text += '</td>';
				NAF_digits++;
				break;
			case ' ':
				text += '<td nowrap>';
				text += '<font face="Arial"><b>&nbsp;';
				text += '</b></font>';
				text += '</td>';
				break;
			default:
				text += '<td nowrap>';
				text += '<font face="Arial"><b>';
				text += ch;
				text += '</b></font>';
				text += '</td>';
				break;
		}
	}

	text += '<td nowrap>&nbsp;&nbsp;</td>';
	text += '<td nowrap>';
	text += '<span id="AnswerButtons" name="AnswerButtons" class="answerButtonsSpan">';

	if (isAnswerButtonsVisible()) {

		text += '<a href="javascript:correctAnswer()" >';
		text += '<img name="AnswerButtonsImage" '
		
 	+  ' src="../../../../shared/images/correctAnswer.gif" border="0">';
	
	
	
		text += '</a>';
	}
	text += '</span>';
	text += '</td>';

	text += '<td nowrap>&nbsp;</td>';

	text += '<td nowrap>';
	text += '<span id="AnswerButtons2" name="AnswerButtons2" class="answerButtonsSpan">';
	if (isAnswerButtonsVisible()) {
		text += '<a href="javascript:showAnswer()" >';
		text += '<img name="AnswerButtons2Image" ' 
		
		+
		 ' src="../../../../shared/images/showAnswer.gif" border="0">';
	
		text += '</a>';
	}
	text += '</span>';
	text += '</td>';

	text += '<td nowrap>&nbsp;</td>';

	text += '<td nowrap>';
	text += '<span name="AnswerButtons3" id="AnswerButtons3" class="answerButtonsSpan">';

	if (retryisEnabled)
	{ 
		if (isAnswerButtonsVisible()) 
		{
			text += '<a href="javascript:getNewRandomSet()" >';
			text += '<img name="AnswerButtons3Image" '
	
	
			+ ' src="../shared/images/tryagainup.gif" border="0">';
	
	
			text += '</a>';
		}
	}

	text += '</span>';
	text += '</td>';

	text += '</tr>';
	text += '</table>';
	 
	text += '</span>'; 

	getDocument().write(text);
	setTimeout('NAF_PostOpen()', 1);
}
























// MASK DETECTION AND FORMATTING FUNCTIONS

function getMaskFromCorrectAnswer(aString) {
	mask=aString.replace(/\d/g,"1");
	return mask;
}

function getTrieFromformattedCorrectAnswer(aString) {
	mask=aString.replace(/\D/g,"");
	return mask;
}



// WIDGET RESETING FUNCTIONS

function getNewRandomSet() {
//	alert(	"calculateCorrectAnswer ::: " + calculateCorrectAnswer() + "\n" +
//			"formattedCorrectAnswer ::: " + formattedCorrectAnswer() + "\n" +
//			"getMaskFromCorrectAnswer ::: " + getMaskFromCorrectAnswer(formattedCorrectAnswer()) + "\n" +
//			"getTrieFromformattedCorrectAnswer ::: " + getTrieFromformattedCorrectAnswer(formattedCorrectAnswer())
//	);
	setUserAnswer(null);
	resetPersistedData();
	window.location.reload();

	return;
}


// DATA PERSISTENCE FUNCTIONS

function persistRandomVariableSet () {  
	var randomEvaluateStringToSave='';
	for(i=1;i<=randomVariables;i++) {
		randomEvaluateStringToSave+= 'rVar'+i+((i==randomVariables)?'':',');
	}
	var tempData = eval('['+randomEvaluateStringToSave+"].join('|')");
	parent.scoPropertySet ('#'+uniQuestionID, tempData );
}

function getRandomVariableSet () {  
	var tempData =  parent.scoPropertyGet('#'+uniQuestionID);
	if (tempData) {
		tempData=tempData.split('|'); 
	} else {
		return null;
	}
  return tempData;
}

function resetPersistedData() {
  parent.scoPropertySet ('#'+uniQuestionID, null );
}

function showSavedData() {
	savedData = getRandomVariableSet();
	alert("savedData ::: " + savedData);
	return;
}



// NOT USED NOW MAYBE IN THE FUTURE

function parseQuestion_old(aString) {
var reg = /rVar\d/g;
var varArray = qString.match(reg);
var arraySize = (varArray.length)?varArray.length:null;
var parsedString = aString;

if(arraySize) {
	for(i=0;i<arraySize;i++) {
		parsedString = parsedString.replace(varArray[i],eval(varArray[i]));
	}
} else {
	return parsedString;
}
return parsedString;
}
