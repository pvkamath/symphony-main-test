
function ismoz()
{ 
// Some places  especially in Assessments
// call this function. It is needed for page-numbers
// show correctly, and to exit a test correctly.
// Seems to fix those for both Safari and FF. - panu

var result = isMoz; // further below
return result;
 
} 



// ########################################################################################
//                                      MM Things
// ########################################################################################


function MM_findObj(n, d){
//alert("MM_find");
    var p, i, x;
    if (!d) 
        d = document;
    if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document;
        n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) 
        x = d.all[n];
    for (i = 0; !x && i < d.forms.length; i++) 
        x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) 
        x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) 
        x = d.getElementById(n);
    return x;
}

function MM_scanStyles(obj, prop){ //v9.0
//    alert("MM_scanStyles ::: obj : " + obj + " ::: prop : " + prop);
    var inlineStyle = null;
    var ccProp = prop;
    var dash = ccProp.indexOf("-");
    while (dash != -1) {
        ccProp = ccProp.substring(0, dash) + ccProp.substring(dash + 1, dash + 2).toUpperCase() + ccProp.substring(dash + 2);
        dash = ccProp.indexOf("-");
    }
    inlineStyle = eval("obj.style." + ccProp);
    if (inlineStyle) 
        return inlineStyle;
    var ss = document.styleSheets;
    for (var x = 0; x < ss.length; x++) {
        var rules = ss[x].cssRules;
        for (var y = 0; y < rules.length; y++) {
            var z = rules[y].style;
            if (z[prop] && (rules[y].selectorText == '*[ID"' + obj.id + '"]' || rules[y].selectorText == '#' + obj.id)) {
                return z[prop];
            }
        }
    }
    return "";
}

function MM_getProp(obj, prop){ //v8.0
//    alert("MM_getProp ::: obj : " + obj + " ::: prop : " + prop);
    if (!obj) 
        return ("");
    if (prop == "L") 
        return obj.offsetLeft;
    else 
        if (prop == "T") 
            return obj.offsetTop;
        else 
            if (prop == "W") 
                return obj.offsetWidth;
            else 
                if (prop == "H") 
                    return obj.offsetHeight;
                else {
                    if (typeof(window.getComputedStyle) == "undefined") {
                        if (typeof(obj.currentStyle) == "undefined") {
                            if (prop == "P") 
                                return MM_scanStyles(obj, "position");
                            else 
                                if (prop == "Z") 
                                    return MM_scanStyles(obj, "z-index");
                                else 
                                    if (prop == "V") 
                                        return MM_scanStyles(obj, "visibility");
                        }
                        else {
                            if (prop == "P") 
                                return obj.currentStyle.position;
                            else 
                                if (prop == "Z") 
                                    return obj.currentStyle.zIndex;
                                else 
                                    if (prop == "V") 
                                        return obj.currentStyle.visibility;
                        }
                    }
                    else {
                        if (prop == "P") 
                            return window.getComputedStyle(obj, null).getPropertyValue("position");
                        else 
                            if (prop == "Z") 
                                return window.getComputedStyle(obj, null).getPropertyValue("z-index");
                            else 
                                if (prop == "V") 
                                    return window.getComputedStyle(obj, null).getPropertyValue("visibility");
                    }
                }
}

function MM_reloadPage(init){ //Updated by PVII. Reloads the window if Nav4 resized
    if (init == true) 
        with (navigator) {
            if ((appName == "Netscape") && (parseInt(appVersion) == 4)) {
                document.MM_pgW = innerWidth;
                document.MM_pgH = innerHeight;
                onresize = MM_reloadPage;
            }
        }
    else 
        if (innerWidth != document.MM_pgW || innerHeight != document.MM_pgH) 
            location.reload();
}

MM_reloadPage(true);

function MM_jumpMenu(targ, selObj, restore){ //v3.0
    eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
    if (restore) 
        selObj.selectedIndex = 0;
}

function MM_setTextOfLayer(objName, x, newText){ // modified by Carlos
    preParagraphHTML = '<table width="595" border="0" cellspacing="0" cellpadding="0"><tr><td>';
    postParagraphHTML = '</td></tr></table>';
    paragraphText = '';
    paragraphIndex = objName.indexOf("paragraph");
    isParagraph = (paragraphIndex >= 0);
    if (isParagraph && isNetscape()) {
        paragraphText = preParagraphHTML + newText + postParagraphHTML;
        MM_setTextOfLayer_original(objName, x, paragraphText);
    }
    else {
        MM_setTextOfLayer_original(objName, x, newText);
    }
}

function MM_setTextOfLayer_original(objName, x, newText){ //v4.01
    if ((obj = MM_findObj(objName)) != null) 
        with (obj) 
if (document.layers) {
            document.write(unescape(newText));
            document.close();
        }
        else 
            innerHTML = unescape(newText);
}

function MM_goToURL(){ //v3.0
    var i, args = MM_goToURL.arguments;
    document.MM_returnValue = false;
    for (i = 0; i < (args.length - 1); i += 2) 
        eval(args[i] + ".location='" + args[i + 1] + "'");
}


function MM_showHideLayers(){ //v3.0
    var i, p, v, obj, args = MM_showHideLayers.arguments;
    for (i = 0; i < (args.length - 2); i += 3) 
        if ((obj = MM_findObj(args[i])) != null) {
            v = args[i + 2];
            if (obj.style) {
                obj = obj.style;
                v = (v == 'show') ? 'visible' : (v = 'hide') ? 'hidden' : v;
            }
            obj.visibility = v;
        }
}

function MM_swapImgRestore(){ //v3.0
    var i, x, a = document.MM_sr;
    for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) 
        x.src = x.oSrc;
}

function MM_preloadImages(){ //v3.0
    var d = document;
    if (d.images) {
        if (!d.MM_p) 
            d.MM_p = new Array();
        var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
        for (i = 0; i < a.length; i++) 
            if (a[i].indexOf("#") != 0) {
                d.MM_p[j] = new Image;
                d.MM_p[j++].src = a[i];
            }
    }
}


function MM_swapImage(){ //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments;
    document.MM_sr = new Array;
    for (i = 0; i < (a.length - 2); i += 3) 
        if ((x = MM_findObj(a[i])) != null) {
            document.MM_sr[j++] = x;
            if (!x.oSrc) 
                x.oSrc = x.src;
            x.src = a[i + 2];
        }
}

// ########################################################################################
//                                      Browser Sniffer
// ########################################################################################


function getBrowser(obj){
    // Returns ( browser , broswer version , engine , engine version )
    browserPlatformMain = navigator.platform
    
    var b = new Array("unknown", "unknown", "unknown", "unknown", browserPlatformMain);
    
    (isEmpty(obj) ? browser = navigator.userAgent.toLowerCase() : browser = obj);
    if (browser.search(/opera[\/\s](\d+(\.?\d)*)/) != -1) {
    
        b[0] = "opera";
        b[1] = browser.match(/opera[\/\s](\d+(\.?\d)*)/)[1];
        b[2] = "opera";
        b[3] = b[1];
        return b;
    }
    else 
        if (browser.search(/firefox[\/\s](\d+([\.-]\d)*)/) != -1) {
        
            b[0] = "firefox";
            b[1] = browser.match(/firefox[\/\s](\d+([\.-]\d)*)/)[1];
            b[2] = "gecko";
            b[3] = getGeckoVersion();
            return b;
        }
        else 
            if (browser.search(/msn\s(\d+(\.?\d)*)/) != -1) {
            
                b[0] = "msn";
                b[1] = browser.match(/msn\s(\d+(\.?\d)*)/)[1];
                b[2] = "msie";
                b[3] = getMSIEVersion();
                return b;
            }
            else 
                if (browser.search(/msie\s(\d+(\.?\d)*)/) != -1) {
                
                    b[0] = "msie";
                    b[1] = getMSIEVersion();
                    b[2] = "msie";
                    b[3] = b[1];
                    return b;
                }
                else 
                    if (browser.search(/netscape6[\/\s](\d+([\.-]\d)*)/) != -1) {
                    
                        b[0] = "netscape";
                        b[1] = browser.match(/netscape6[\/\s](\d+([\.-]\d)*)/)[1];
                        b[2] = "gecko";
                        b[3] = getGeckoVersion();
                        return b;
                    }
                    else 
                        if (browser.search(/netscape\/(7\.\d*)/) != -1) {
                        
                            b[0] = "netscape";
                            b[1] = browser.match(/netscape\/(7\.\d*)/)[1];
                            b[2] = "gecko";
                            b[3] = getGeckoVersion();
                            return b;
                        }
                        else 
                            if (browser.search(/netscape4\/(\d+([\.-]\d)*)/) != -1) {
                            
                                b[0] = "netscape";
                                b[1] = browser.match(/netscape4\/(\d+([\.-]\d)*)/)[1];
                                b[2] = "mozold";
                                b[3] = b[1];
                                return b;
                            }
                            else 
                                if ((browser.search(/mozilla\/(4.\d*)/) != -1) && (browser.search(/msie\s(\d+(\.?\d)*)/) == -1)) {
                                    b[0] = "netscape";
                                    b[1] = browser.match(/mozilla\/(4.\d*)/)[1];
                                    b[2] = "mozold";
                                    b[3] = b[1];
                                    return b;
                                }
                                else 
                                    if (browser.search(/chrome[\/](\d+(\.?\d)*)/) != -1) {
                                        b[0] = "chrome";
                                        b[1] = browser.match(/chrome[\/](\d+(\.?\d)*) /)[1];
                                        b[2] = "AppleWebKit";
                                        b[3] = b[1];
                                        return b;
                                    }
                                    else 
                                        if (browser.search(/(\d+(\.?\d)*) safari/) != -1) {
                                            b[0] = "safari";
                                            b[1] = browser.match(/(\d+(\.?\d)*) safari/)[1];
                                            b[2] = "AppleWebKit";
                                            b[3] = b[1];
                                            return b;
                                        }
                                        else {
                                            b[0] = "unknown";
                                            return b;
                                        }
}



function getMajorVersion(v){
    return (isEmpty(v) ? -1 : (hasDot(v) ? v : v.match(/(\d*)(\.\d*)*/)[1]))
}

function getMinorVersion(v){
    return (!isEmpty(v) ? (!hasDot(v) ? v.match(/\.(\d*([-\.]\d*)*)/)[1] : 0) : -1)
}

function getOS(obj){
    var os = new Array("unknown", "unknown");
    (isEmpty(obj) ? browser = navigator.userAgent.toLowerCase() : browser = obj);
    return os;
}

function getGeckoVersion(){
    return browser.match(/gecko\/([0-9]+)/)[1];
}

function getMSIEVersion(){
    return browser.match(/msie\s(\d+(\.?\d)*)/)[1];
}

function getFullUAString(obj){
    (isEmpty(obj) ? browser = navigator.userAgent.toLowerCase() : browser = obj);
    return browser;
}

function hasFlashPlugin(obj){
    (isEmpty(obj) ? browser = navigator.userAgent.toLowerCase() : browser = obj);
    var f = new Array("0", "0");
    var brwEng = getBrowser(obj)[2];
    return f;
}


function isEmpty(input){
    return (input == null || input == "")
}

function hasDot(input){
    return (input.search(/\./) == -1)
}




//alert("scoUtils.js ::: ");

// ########################################################################################
//                                     Browser Sniffer
// ########################################################################################


var browsers = new Array( "msie" , "safari" , "firefox" , "chrome" , "opera", "netscape" );


BrowserName = getBrowser()[0];
BrowserVersion = getBrowser()[1];
BrowserEngine = getBrowser()[2];
BrowserEngineVersion = getBrowser()[3];

	isBrowserIE = 			(BrowserName==browsers[0])?true:false;
	isBrowserSafari = 		(BrowserName==browsers[1])?true:false;
	isBrowserFirefox = 		(BrowserName==browsers[2])?true:false;
	isBrowserChrome = 		(BrowserName==browsers[3])?true:false;
	isBrowserOpera = 		(BrowserName==browsers[4])?true:false;
	isBrowserNetscape = 	(BrowserName==browsers[5])?true:false;


//alert ( "isChrome :: " + isChrome );


//alert( BrowserName + " ::: " + BrowserVersion + " ::: " + BrowserEngine + " ::: " + BrowserEngineVersion );

function isIECompatible(){
    isIE5Plus = ( isBrowserIE || isBrowserChrome ) ? true : false;
    //	alert(isIE5Plus);
    return isIE5Plus;
}

function isMozCompatible(){
    isFirefox = (isBrowserFirefox || isBrowserSafari ) ? true : false;
    return isFirefox;
}

function isNetscape(){
//    alert("isNetscape()");
    return isMozCompatible();
}

// ########################################################################################


isMoz = isMozCompatible();
isIE = isIECompatible();

function isms(){
    return isIE;
}

function moz(){
    return isMoz;
}

 
 
// ########################################################################################
//                                     Start Usual scoUtils.js
// ########################################################################################








var countVAR = 0;
function art(aString){






}


function store(keyString, valueString){
    storeData()[keyString] = valueString;
}

function load(keyString){
    var result = storeData()[keyString];
    return result
}

function storeData(){
    return mydata_VAR;
};

var mydata_VAR = new Object();


function getsp(arg1){
//    var line = 'document.layers.' + arg1;
//    if (isIE) {
//        line = 'document.all.' + arg1;
//    }
    if (document.getElementById) {
        return document.getElementById(arg1);
    }
//    if (document.layers) {
//        return eval(line);
//    }
    return document.getElementById(arg1);
}

function getst(arg2){

    //	alert("::: inside getst ::: " + arg2)
    
    if (!getsp(arg2)) 
        return null;
//    var line = 'document.layers.' + arg2;
//    if (isIE) {
//        line = 'document.all.' + arg2 + '.style';
//    }
//    if (isMoz) {
//        line = document.getElementById(arg2).style;
//    }
	line = document.getElementById(arg2).style;
	return eval(line);
}

function reverseVisibility(arg){
    //	alert("::: inside reverseVisibility ::: " + arg)
    
    var st = getst(arg);
    if (!st) 
        return;
    if (st.visibility == "hidden") {
        st.visibility = "visible";
        return;
    }
    if (st.visibility == "hide") {
        st.visibility = "visible";
        return;
    }
    st.visibility = "hidden";
}

function hideVisibility(arg){
    var st = getst(arg);
    if (!st) 
        return;
    st.visibility = "hidden";
}

function showProps(anObject){
    var str = "<B>" + anObject + "</B>\n<BR>==============\n" +
    "<TABLE border = 1>";
    var i = 1;
    for (p in anObject) {
        if (!(anObject[p] == "nosuch things")) {
            var type = typeof anObject[p];
            
            var value = anObject[p];
            var valueValue = "";
            if (value) 
                valueValue = value.value + "";
            if (value == "") 
                value = "&nbsp;";
            str = str +
            "<TR><TD>" +
            i +
            ". </TD><TD>" +
            p +
            "</TD><TD> = </TD><TD>" +
            type +
            "</TD><TD>" +
            value;
            str = str + "</TD></TR>\n";
        }
        i++
    }
    str = "<HTML><BODY>" + str + "</TABLE></BODY></HTML>";
    win2 = window.open("", "props");
    win2.document.open();
    win2.document.writeln(str);
    win2.document.close();
    return str;
}

















// ***********************************************************************************
// **																				**
// **         Drag and Drop needed for numeric entry fields in lesson pages			**
// **																				**
// ***********************************************************************************


function MM_dragLayer(objId, x, hL, hT, hW, hH, toFront, dropBack, cU, cD, cL, cR, targL, targT, tol, dropJS, et, dragJS){ //v9.01
    //Copyright 2005-2006 Adobe Macromedia Software LLC and its licensors. All rights reserved.
    //alert("MM_dragLayer "+ objId +","+ x +","+ hL +","+ hT +","+ hW +","+ hH +","+ toFront +","+ dropBack +","+ cU +","+ cD +","+ cL +","+ cR +","+ targL +","+ targT +","+ tol +","+ dropJS +","+ et +","+ dragJS);
/*
		alert( "MM_dragLayer ::: "  
		  + "\n objId ::: " +  objId 
		  + "\n x ::: " +  x 
		  + "\n hL ::: " +  hL 
		  + "\n hT ::: " +  hT 
		  + "\n hW ::: " +  hW 
		  + "\n hH ::: " +  hH 
		  + "\n toFront ::: " +  toFront 
		  + "\n dropBack ::: " +  dropBack 
		  + "\n cU ::: " +  cU 
		  + "\n cD ::: " +  cD 
		  + "\n cL ::: " +  cL 
		  + "\n cR ::: " +  cR 
		  + "\n targL ::: " +  targL 
		  + "\n targT ::: " +  targT 
		  + "\n tol ::: " + tol 
		  + "\n dropJS ::: " + dropJS 
		  + "\n et ::: " + et 
		  + "\n dragJS ::: " + dragJS 
		  + "\n MM_dragLayer.arguments.length ::: " + MM_dragLayer.arguments.length);
*/
    var i, j, aLayer, retVal, curDrag = null, curLeft, curTop, IE = isIE;
    var NS = isMoz; //(!IE && document.getElementById);
    if (!IE && !NS) 
        return false;
    retVal = true;
    if (IE && event) 
        event.returnValue = true;
    if (MM_dragLayer.arguments.length > 1) {
        curDrag = document.getElementById(objId);
        if (!curDrag) 
            return false;
        if (!document.allLayers) {
            document.allLayers = new Array();
            with (document) {
                if (NS) {
                    var spns = getElementsByTagName("span");
                    var all = getElementsByTagName("div");
                    for (i = 0; i < spns.length; i++) 
                        if (MM_getProp(spns[i], 'P')) 
                            allLayers[allLayers.length] = spns[i];
                }
                for (i = 0; i < all.length; i++) {
                    if (MM_getProp(all[i], 'P')) 
                        allLayers[allLayers.length] = all[i];
                }
            }
        }
        curDrag.MM_dragOk = true;
        curDrag.MM_targL = targL;
        curDrag.MM_targT = targT;
        curDrag.MM_tol = Math.pow(tol, 2);
        curDrag.MM_hLeft = hL;
        curDrag.MM_hTop = hT;
        curDrag.MM_hWidth = hW;
        curDrag.MM_hHeight = hH;
        curDrag.MM_toFront = toFront;
        curDrag.MM_dropBack = dropBack;
        curDrag.MM_dropJS = dropJS;
        curDrag.MM_everyTime = et;
        curDrag.MM_dragJS = dragJS;
        
        curDrag.MM_oldZ = MM_getProp(curDrag, 'Z');
        curLeft = MM_getProp(curDrag, 'L');
        if (String(curLeft) == "NaN") 
            curLeft = 0;
        curDrag.MM_startL = curLeft;
        curTop = MM_getProp(curDrag, 'T');
        if (String(curTop) == "NaN") 
            curTop = 0;
        curDrag.MM_startT = curTop;
        curDrag.MM_bL = (cL < 0) ? null : curLeft - cL;
        curDrag.MM_bT = (cU < 0) ? null : curTop - cU;
        curDrag.MM_bR = (cR < 0) ? null : curLeft + cR;
        curDrag.MM_bB = (cD < 0) ? null : curTop + cD;
        curDrag.MM_LEFTRIGHT = 0;
        curDrag.MM_UPDOWN = 0;
        curDrag.MM_SNAPPED = false; //use in your JS!
        document.onmousedown = MM_dragLayer;
        document.onmouseup = MM_dragLayer;
        if (NS) 
            document.captureEvents(Event.MOUSEDOWN | Event.MOUSEUP);
    }
    else {
        var theEvent = ((NS) ? objId.type : event.type);
        if (theEvent == 'mousedown') {
            var mouseX = (NS) ? objId.pageX : event.clientX + document.body.scrollLeft;
            var mouseY = (NS) ? objId.pageY : event.clientY + document.body.scrollTop;
            var maxDragZ = null;
            document.MM_maxZ = 0;
            for (i = 0; i < document.allLayers.length; i++) {
                aLayer = document.allLayers[i];
                var aLayerZ = MM_getProp(aLayer, 'Z');
                if (aLayerZ > document.MM_maxZ) 
                    document.MM_maxZ = aLayerZ;
                var isVisible = (MM_getProp(aLayer, 'V')).indexOf('hid') == -1;
                if (aLayer.MM_dragOk != null && isVisible) 
                    with (aLayer) {
                        var parentL = 0;
                        var parentT = 0;
                        if (NS) {
                            parentLayer = aLayer.parentNode;
                            while (parentLayer != null && parentLayer != document && MM_getProp(parentLayer, 'P')) {
                                parentL += parseInt(MM_getProp(parentLayer, 'L'));
                                parentT += parseInt(MM_getProp(parentLayer, 'T'));
                                parentLayer = parentLayer.parentNode;
                                if (parentLayer == document) 
                                    parentLayer = null;
                            }
                        }
                        else 
                            if (IE) {
                                parentLayer = aLayer.parentElement;
                                while (parentLayer != null && MM_getProp(parentLayer, 'P')) {
                                    parentL += MM_getProp(parentLayer, 'L');
                                    parentT += MM_getProp(parentLayer, 'T');
                                    parentLayer = parentLayer.parentElement;
                                }
                            }
                        var tmpX = mouseX - ((MM_getProp(aLayer, 'L')) + parentL + MM_hLeft);
                        var tmpY = mouseY - ((MM_getProp(aLayer, 'T')) + parentT + MM_hTop);
                        if (String(tmpX) == "NaN") 
                            tmpX = 0;
                        if (String(tmpY) == "NaN") 
                            tmpY = 0;
                        var tmpW = MM_hWidth;
                        if (tmpW <= 0) 
                            tmpW += MM_getProp(aLayer, 'W');
                        var tmpH = MM_hHeight;
                        if (tmpH <= 0) 
                            tmpH += MM_getProp(aLayer, 'H');
                        if ((0 <= tmpX && tmpX < tmpW && 0 <= tmpY && tmpY < tmpH) &&
                        (maxDragZ == null ||
                        maxDragZ <= aLayerZ)) {
                            curDrag = aLayer;
                            maxDragZ = aLayerZ;
                        }
                    }
            }
            if (curDrag) {
                document.onmousemove = MM_dragLayer;
                curLeft = MM_getProp(curDrag, 'L');
                curTop = MM_getProp(curDrag, 'T');
                if (String(curLeft) == "NaN") 
                    curLeft = 0;
                if (String(curTop) == "NaN") 
                    curTop = 0;
                MM_oldX = mouseX - curLeft;
                MM_oldY = mouseY - curTop;
                document.MM_curDrag = curDrag;
                curDrag.MM_SNAPPED = false;
                if (curDrag.MM_toFront) {
                    var newZ = parseInt(document.MM_maxZ) + 1;
                    eval('curDrag.' + ('style.') + 'zIndex=newZ');
                    if (!curDrag.MM_dropBack) 
                        document.MM_maxZ++;
                }
                retVal = false;
                if (!NS) 
                    event.returnValue = false;
            }
        }
        else 
            if (theEvent == 'mousemove') {
                if (document.MM_curDrag) 
                    with (document.MM_curDrag) {
                        var mouseX = (NS) ? objId.pageX : event.clientX + document.body.scrollLeft;
                        var mouseY = (NS) ? objId.pageY : event.clientY + document.body.scrollTop;
                        var newLeft = mouseX - MM_oldX;

                        var newTop = mouseY - MM_oldY;
                        if (MM_bL != null) 
                            newLeft = Math.max(newLeft, MM_bL);
                        if (MM_bR != null) 
                            newLeft = Math.min(newLeft, MM_bR);
                        if (MM_bT != null) 
                            newTop = Math.max(newTop, MM_bT);
                        if (MM_bB != null) 
                            newTop = Math.min(newTop, MM_bB);
                        MM_LEFTRIGHT = newLeft - MM_startL;
                        MM_UPDOWN = newTop - MM_startT;
                        if (NS) {
                            style.left = newLeft + "px";
                            style.top = newTop + "px";
                        }
                        else {
                            style.pixelLeft = newLeft;
                            style.pixelTop = newTop;
                        }
                        if (MM_dragJS) 
                            eval(MM_dragJS);
                        retVal = false;
                        if (!NS) 
                            event.returnValue = false;
                    }
            }
            else 
                if (theEvent == 'mouseup') {
                    document.onmousemove = null;
                    if (NS) 
                        document.releaseEvents(Event.MOUSEMOVE);
                    if (NS) 
                        document.captureEvents(Event.MOUSEDOWN); //for mac NS
                    if (document.MM_curDrag) 
                        with (document.MM_curDrag) {
                            if (typeof MM_targL == 'number' && typeof MM_targT == 'number' &&
                            (Math.pow(MM_targL - (MM_getProp(document.MM_curDrag, 'L')), 2) +
                            Math.pow(MM_targT - (MM_getProp(document.MM_curDrag, 'T')), 2)) <=
                            MM_tol) {
                                if (NS) {
                                    style.left = MM_targL + "px";
                                    style.top = MM_targT + "px";
                                }
                                else {
                                    style.pixelLeft = MM_targL;
                                    style.pixelTop = MM_targT;
                                }
                                MM_SNAPPED = true;
                                MM_LEFTRIGHT = MM_startL - MM_targL;
                                MM_UPDOWN = MM_startT - MM_targT;
                            }
                            if (MM_everyTime || MM_SNAPPED) 
                                eval(MM_dropJS);
                            if (MM_dropBack) {
                                style.zIndex = MM_oldZ;
                            }
                            retVal = false;
                            if (!NS) 
                                event.returnValue = false;
                        }
                    document.MM_curDrag = null;
                }
        if (NS) 
            document.routeEvent(objId);
    }
    return retVal;
}

















// ########################################################################################
//                                     ToolTips
// ########################################################################################


var qTipTag = "area,a"; // area,checkbox,radio,img,a,label,input Which tag do you want to qTip-ize? Keep it lowercase! //
var qTipX = 0; //This is qTip's X offset//
var qTipY = 15; //This is qTip's Y offset//

//There's No need to edit anything below this line//

tooltip = {
    name: "qTip",
    offsetX: qTipX,
    offsetY: qTipY,
    tip: null
}

tooltip.init = function(){
    //	alert("yo mama");
    var tipNameSpaceURI = "";
    if (!tipContainerID) {
        var tipContainerID = "qTip";
    }
    var tipContainer = document.getElementById(tipContainerID);
//    alert("before !!!" + tipContainer);
    if (!tipContainer) {
        tipContainer = document.createElementNS ? document.createElementNS(tipNameSpaceURI, "div") : document.createElement("div");
        tipContainer.setAttribute("id", tipContainerID);
        document.getElementsByTagName("body").item(0).appendChild(tipContainer);
    }
//    alert("after !!!" + tipContainer);
    if (!document.getElementById) 
        return;
    this.tip = document.getElementById(this.name);
    if (this.tip) 
        document.onmousemove = function(evt){
            tooltip.move(evt)
        };
    
    var a, sTitle, elements;
    
    var elementList = qTipTag.split(",");
    for (var j = 0; j < elementList.length; j++) {
        elements = document.getElementsByTagName(elementList[j]);
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                a = elements[i];
                sTitle = a.getAttribute("title");
                if (sTitle) {
                    a.setAttribute("tiptitle", sTitle);
                    a.removeAttribute("title");
                    a.removeAttribute("alt");
                    a.onmouseover = function(){
                        tooltip.show(this.getAttribute('tiptitle'))
                    };
                    a.onmouseout = function(){
                        tooltip.hide()
                    };
                }
            }
        }
    }
}

tooltip.move = function(evt){
    var x = 0, y = 0;
    if (document.all) {//IE
        x = (document.documentElement && document.documentElement.scrollLeft) ? document.documentElement.scrollLeft : document.body.scrollLeft;
        y = (document.documentElement && document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
        x += window.event.clientX;
        y += window.event.clientY;
        
    }
    else {//Good Browsers
        x = evt.pageX;
        y = evt.pageY;
    }
    this.tip.style.left = (x + this.offsetX) + "px";
    this.tip.style.top = (y + this.offsetY) + "px";
}

tooltip.show = function(text){
    if (!this.tip) 
        return;
    this.tip.innerHTML = text;
    this.tip.style.display = "block";
}

tooltip.hide = function(){
    if (!this.tip) 
        return;
    this.tip.innerHTML = "";
    this.tip.style.display = "none";
}

//window.onload = function () {
//	tooltip.init ();
//}
