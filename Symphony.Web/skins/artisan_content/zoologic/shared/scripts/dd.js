// MOZILLA CODE INCLUDED
// NETSCAPE DROPPED

//alert("dd.js isIE ::: "+ isIE + " :::");
//alert("dd.js isMoz ::: "+ isMoz);


// **********************************************************************************************
// **********************************************************************************************
// **********************************************************************************************


function OnDrop(objName) {
    var obj = sources[objName];
    var snap;
    obj.currentPos = getSpanRectangle(obj.object);
    snap = GetFirstUnder(objName,targets);
    if (snap && (!snap.tag || snap.tag == objName) )  {
            setSpanRectangle (obj.object,snap.currentPos);
            obj.currentPos = snap.currentPos;
            if (obj.tag) obj.tag.tag = null;
            snap.tag = objName;
            obj.tag = snap;
    } else  {
            setSpanRectangle(obj.object,obj.originalPos);
            obj.currentPos = obj.originalPos;
            if (obj.tag)  obj.tag.tag = null; 
            obj.tag = null;
    }
	resetHighlights();
	userAnswerChanged();
	if (calculateTotal()) calculateTotal();
}

function OnDrag(objName) {
    // This function is called multiple times while dragging an object.
    // Use obj projerties MM_UPDOWN or MM_LEFTRIGHT for tracking.-ea.
	// var layerRef = MM_findObj(layername); 
	// var curVertPos = layerRef.MM_UPDOWN;
	// document.tracking.curPosField.value = curVertPos;
	var itm;
    var obj = sources[objName];
    var snap;
    obj.currentPos = getSpanRectangle(obj.object);

	snap = GetFirstUnder(objName,targets);

	resetHighlights();

	if (snap && (!snap.tag || snap.tag == objName) )  {
		Htarget = eval ( 'document.getElementById( "' + snap.name + '" )');
		Htarget.style.backgroundColor = "#FFCC00";
		Htarget.style.border="1px solid #000";
		
		
	}

}

// ************************************************************************************************************************
// *************                                                                                NEW STUFF                                                                            ******************
// ************************************************************************************************************************

function creatediv(id, html, width, height, left, top) {
   var newdiv = document.createElement('div');
   newdiv.setAttribute('id', id);
   if (width) {
       newdiv.style.width = width;
   }
   if (height) {
       newdiv.style.height = height;
   }
   if ((left || top) || (left && top)) {
       newdiv.style.position = "absolute";
       if (left) {
           newdiv.style.left = left;
       }
       if (top) {
           newdiv.style.top = top;
       }
   }
   newdiv.style.background = "#fff";
   newdiv.style.border = "1px solid #ddd";
   if (html) {
       newdiv.innerHTML = html;
   } else {
       newdiv.innerHTML = "";
   }
   document.body.appendChild(newdiv);
}

function resetHighlights () {
	counterX=0;	counterY=0;
	originX=10;	originY=440;
	width=265; height=245;
	spaceX=width+10; spaceY=height+10;
	for (itm in targets) {
			text="";
			for (itm2 in targets) {
				text+=targets[itm2].name + "<br>" + targets[itm2].originalColor + "<br>";
			}
			
			resetHighlight = eval ( 'document.getElementById ("' + targets[itm].name + '" )');

//			if (!targets[itm].originalColor) {
//				targets[itm].originalColor=getBakgroundColor(resetHighlight);
//				creatediv("yoyo3",text,180,100,800,100);
//			}
//			creatediv("yoyo3",text,180,100,800,100);
			oldHighLightColor = targets[itm].originalColor;
			resetHighlight.style.background = oldHighLightColor; //"#66CCFF";
			resetHighlight.style.border="0px";
			
//			texty ="objName : " + targets[itm].objName + "<br>";
//			texty +="key : " + targets[itm].key  + "<br>";
//			texty +="type : " + targets[itm].type + "<br>";
//			texty +="this.name : " + targets[itm].name + "<br>";
//			texty +="this.enabled : " + targets[itm].enabled + "<br>";
//			texty +="this.object : " + targets[itm].object + "<br>";
//			texty +="this.object.innerhtml : " + targets[itm].object.innerhtml + "<br>";
//			texty +="this.object.onmousedown : " + targets[itm].object.onmousedown + "<br>";
//			texty +="this.originalPos : " + targets[itm].originalPos + "<br>";
//			texty +="this.currentPos : " + targets[itm].currentPos + "<br>";
//			texty +="this.tag : " + targets[itm].tag + "<br>";
//			texty +="this.extra : " + targets[itm].extra + "<br>";
//			texty +="this.originalColor : " + targets[itm].originalColor + "<br>";
			
			
//			creatediv("yoyo",texty,width,height,originX+(counterX*spaceX),originY+(counterY*spaceY));

//			if (counterX<3) {
//				counterX++; 
//			} else {
//				counterX = 0 ;
//				counterY++; 
//			};
	}
}

function getBakgroundColor(pageElement) {
	color = pageElement.style.backgroundColor;
//	color2 = pageElement.style.background;
//	creatediv("yo","color : " + color,180,30,10,40);
//	creatediv("yo","color2 : " + color2,180,30,10,70);
//	if (color=="") creatediv("yodsd","yo mama is empty " + color2,180,30,10,100);
	return color;
}

// ************************************************************************************************************************
// *************                                                                                NEW STUFF                                                                            ******************
// ************************************************************************************************************************


function GetFirstUnder(objName,items) {
    var itm, obj, obj2
    obj = sources[objName];
    for (itm in items)   {
        obj2 = items[itm];
        if (typeof(obj2) != "function" && obj2.name != objName) {
        	if (obj.currentPos.overlaps(obj2.currentPos)) 
            	return obj2;
        }
    }
    return null;        
}


function getAnswer() {
    var out ="",obj;
	var key ="";
    for (itm in targets) {
        obj = targets[itm];
        if (obj.tag)
            out += itm + "=" + obj.tag + "|";
    }
    return out;
}

function setAnswer(answer) {
    var a = answer.split("|");
    resetPositions();
    for (var i in a) {
        if (a[i]) { // make sure it's not an empty string
           var pair = a[i].split("=");
           //alert("0:"+pair[0]+", 1:"+pair[1]);
           // t=s
           if (pair[1] != 'null' && pair[0] != 'null')  {
                setSpanRectangle(sources[pair[1]].object,targets[pair[0]].currentPos);
                OnDrop(pair[1]);
           }
        }
    }
}

function resetPositions() {
    for (var i in targets) {
        targets[i].tag = null;
    }
    
    for (i in sources) {
        setSpanRectangle(sources[i].object,sources[i].originalPos);
        sources[i].currentPos = sources[i].originalPos;
        sources[i].tag = null;
    }
}

// setEnableAll()  if argument is missing, it'll toggle between states (enable/disable)
function setEnableAll(enabled) {
    for (var i in sources) { 
        if (arguments.length < 1) 
            sources[i].object.MM_dragOk = (sources[i].object.MM_dragOk==null)?true:null;
        else 
            sources[i].object.MM_dragOk = (enabled)?true:null;
   }
}

function getChecksum() {
    var checksum = 0,obj;
    for (var itm in sources) {
		obj = sources[itm];
        if(obj.tag)
			(checkKeysinTarget(obj.tag.key,obj.key))?checksum++:checksum--;
    }
    return checksum;
}

function getChecksum() {
    var checksum = 0,obj;
    for (var itm in sources) {
        obj = sources[itm];
        if(obj.tag)(obj.tag.key==obj.key)?checksum++:checksum--;
    }
    return checksum;
}

function checkKeysinTarget(targetKey,sourceKey) {
	arr = targetKey.split(',');
	for (i=0;i<arr.length;i++) {
		if (arr[i]==sourceKey) return true;
	}
	return false;
}




// **********************************************************************************************
// **********************************************************************************************
// **********************************************************************************************




function getNumberValue(aString){
    //alert ("getNumberValue");
    
    if (String(aString).indexOf("px") != -1) {
        lenght = aString.length;
        aString = aString.slice(0, lenght - 2);
        return Number(aString.valueOf());
    }
    return Number(aString);
}

function rectangle(left, top, width, height){
    //alert ("rectangle " + left +","+ top +","+ width +","+ height);
    
    MOZ = isMoz;
    left = MOZ ? getNumberValue(left) : left;
    top = MOZ ? getNumberValue(top) : top;
    width = MOZ ? getNumberValue(width) : width;
    height = MOZ ? getNumberValue(height) : height;
    
    this.l = left;
    this.t = top;
    this.r = left + width;
    this.b = top + height;
    this.w = width;
    this.h = height;
    this.tolerance = 0;
    this.overlaps = rectangle_overlaps;
    this.toString = rectangle_toString;
    
    //	attrib = "rectangle attributes ::: " + "\n";
    //	attrib+= "left :: " + this.l + "\n"; 
    //	attrib+= "top :: " + this.t + "\n";
    //	attrib+= "right :: " + this.r + "\n";
    //	attrib+= "bottom :: " + this.b + "\n";
    //	attrib+= "width :: " + this.w + "\n";
    //	attrib+= "height :: " + this.h + "\n";
    //	attrib+= "tolerance :: " + this.tolerance + "\n";

    //	alert(attrib);

}


function rectangle_overlaps(r2, tolerance){
    //alert ("rectangle_overlaps");
    if (tolerance) 
        this.tolerance = tolerance;
    
    return !(this.l > r2.r + this.tolerance || r2.l + this.tolerance > this.r || this.t > r2.b + this.tolerance || r2.t + this.tolerance > this.b)
}

function rectangle_toString(){
    //alert ("rectangle_toString");
    return " l:" + this.l + " r:" + this.r + " w:" + this.w + " h:" + this.h;
}

function draggableObject(objName, key, type){
    //alert ("draggableObject");
    
    this.name = objName;
    this.enabled = true;
    this.object = MM_findObj(objName);
    this.originalPos = getSpanRectangle(this.object);
    this.currentPos = this.originalPos;
    if (type != "t") 
        this.object.onMouseDown = DragMe(objName);
    this.tag = null;
    this.extra = null;
    this.key = (key) ? key : objName;
    this.toString = draggableObject_toString;
	this.originalColor = getBakgroundColor(MM_findObj(objName));
	this.stuff = null;

}

function draggableObject_toString(){
    return "." + this.name;
}


/*function getSpanRectangle(obj) {
 alert("first");
 if (document.all)
 {
 //		 alert("this has a doc.all!!!");
 return new rectangle(obj.style.posLeft,obj.style.posTop,obj.style.posWidth,obj.style.posHeight);
 }
 else
 {
 //		 alert("NOOOOOOOO!!!");
 return new rectangle(obj.style.left,obj.style.top,obj.style.width,obj.style.height);
 }
 }
 */


function getSpanRectangle(obj){
//	alert ("getSpanRectangle" + obj);
    
    if (isIE) {
        //	alert("isms");
        return new rectangle(obj.style.posLeft, obj.style.posTop, obj.style.posWidth, obj.style.posHeight);
    }
    if (isMoz ) {
        //	alert("MOZ " + obj.style.left +","+ obj.style.top +","+  obj.style.width +","+  obj.style.height);
        return new rectangle(obj.style.left, obj.style.top, obj.style.width, obj.style.height);
    }
    //	alert("something else");
    return new rectangle(obj.left, obj.top, obj.clip.width, obj.clip.height);
}

function setSpanRectangle(obj, rec){
//alert ("setSpanRectangle");
    
    if (isIE) {
//		alert("isIE");
		obj.style.left = rec.l;
        obj.style.top = rec.t;
    }
    if (isMoz) {
//		alert("isMoz");
        //	alert(rec.l);
        //	alert(rec.t);
        obj.style.left = rec.l;
        obj.style.top = rec.t;
        return;
    }
    obj.left = rec.l;
    obj.top = rec.t;
}


function quickSpan(id, l, t, h, w, additionalstyle, content){
    //alert ("quickSpan");
    document.write("<span id='" + id + "' name='" + id + "' style='position:absolute;cursor:hand;left:" + l + ";top:" + t + ";height:" + h + "; width:" + w + ";" + additionalstyle + "'>" + content + "</span>");
    return id;
}


function DragMe(objName){
    //alert ("DragMe "+objName);
    MM_dragLayer(objName, '', 0, 0, 0, 0, true, false, -1, -1, -1, -1, 1, 1, 0, "OnDrop('" + objName + "');", true, "OnDrag('" + objName + "');")
}


function MM_dragLayer(objId, x, hL, hT, hW, hH, toFront, dropBack, cU, cD, cL, cR, targL, targT, tol, dropJS, et, dragJS){ //v9.01
    //Copyright 2005-2006 Adobe Macromedia Software LLC and its licensors. All rights reserved.
    //alert("MM_dragLayer "+ objId +","+ x +","+ hL +","+ hT +","+ hW +","+ hH +","+ toFront +","+ dropBack +","+ cU +","+ cD +","+ cL +","+ cR +","+ targL +","+ targT +","+ tol +","+ dropJS +","+ et +","+ dragJS);
/*
		alert( "MM_dragLayer ::: "  
		  + "\n objId ::: " +  objId 
		  + "\n x ::: " +  x 
		  + "\n hL ::: " +  hL 
		  + "\n hT ::: " +  hT 
		  + "\n hW ::: " +  hW 
		  + "\n hH ::: " +  hH 
		  + "\n toFront ::: " +  toFront 
		  + "\n dropBack ::: " +  dropBack 
		  + "\n cU ::: " +  cU 
		  + "\n cD ::: " +  cD 
		  + "\n cL ::: " +  cL 
		  + "\n cR ::: " +  cR 
		  + "\n targL ::: " +  targL 
		  + "\n targT ::: " +  targT 
		  + "\n tol ::: " + tol 
		  + "\n dropJS ::: " + dropJS 
		  + "\n et ::: " + et 
		  + "\n dragJS ::: " + dragJS 
		  + "\n MM_dragLayer.arguments.length ::: " + MM_dragLayer.arguments.length);
*/
    var i, j, aLayer, retVal, curDrag = null, curLeft, curTop, IE = isIE;
    var NS = isMoz; //(!IE && document.getElementById);
    if (!IE && !NS) 
        return false;
    retVal = true;
    if (IE && event) 
        event.returnValue = true;
    if (MM_dragLayer.arguments.length > 1) {
        curDrag = document.getElementById(objId);
        if (!curDrag) 
            return false;
        if (!document.allLayers) {
            document.allLayers = new Array();
            with (document) {
                if (NS) {
                    var spns = getElementsByTagName("span");
                    var all = getElementsByTagName("div");
                    for (i = 0; i < spns.length; i++) 
                        if (MM_getProp(spns[i], 'P')) 
                            allLayers[allLayers.length] = spns[i];
                }
                for (i = 0; i < all.length; i++) {
                    if (MM_getProp(all[i], 'P')) 
                        allLayers[allLayers.length] = all[i];
                }
            }
        }
        curDrag.MM_dragOk = true;
        curDrag.MM_targL = targL;
        curDrag.MM_targT = targT;
        curDrag.MM_tol = Math.pow(tol, 2);
        curDrag.MM_hLeft = hL;
        curDrag.MM_hTop = hT;
        curDrag.MM_hWidth = hW;
        curDrag.MM_hHeight = hH;
        curDrag.MM_toFront = toFront;
        curDrag.MM_dropBack = dropBack;
        curDrag.MM_dropJS = dropJS;
        curDrag.MM_everyTime = et;
        curDrag.MM_dragJS = dragJS;
        
        curDrag.MM_oldZ = MM_getProp(curDrag, 'Z');
        curLeft = MM_getProp(curDrag, 'L');
        if (String(curLeft) == "NaN") 
            curLeft = 0;
        curDrag.MM_startL = curLeft;
        curTop = MM_getProp(curDrag, 'T');
        if (String(curTop) == "NaN") 
            curTop = 0;
        curDrag.MM_startT = curTop;
        curDrag.MM_bL = (cL < 0) ? null : curLeft - cL;
        curDrag.MM_bT = (cU < 0) ? null : curTop - cU;
        curDrag.MM_bR = (cR < 0) ? null : curLeft + cR;
        curDrag.MM_bB = (cD < 0) ? null : curTop + cD;
        curDrag.MM_LEFTRIGHT = 0;
        curDrag.MM_UPDOWN = 0;
        curDrag.MM_SNAPPED = false; //use in your JS!
        document.onmousedown = MM_dragLayer;
        document.onmouseup = MM_dragLayer;
        if (NS) 
            document.captureEvents(Event.MOUSEDOWN | Event.MOUSEUP);
    }
    else {
        var theEvent = ((NS) ? objId.type : event.type);
        if (theEvent == 'mousedown') {
            var mouseX = (NS) ? objId.pageX : event.clientX + document.body.scrollLeft;
            var mouseY = (NS) ? objId.pageY : event.clientY + document.body.scrollTop;
            var maxDragZ = null;
            document.MM_maxZ = 0;
            for (i = 0; i < document.allLayers.length; i++) {
                aLayer = document.allLayers[i];
                var aLayerZ = MM_getProp(aLayer, 'Z');
                if (aLayerZ > document.MM_maxZ) 
                    document.MM_maxZ = aLayerZ;
                var isVisible = (MM_getProp(aLayer, 'V')).indexOf('hid') == -1;
                if (aLayer.MM_dragOk != null && isVisible) 
                    with (aLayer) {
                        var parentL = 0;
                        var parentT = 0;
                        if (NS) {
                            parentLayer = aLayer.parentNode;
                            while (parentLayer != null && parentLayer != document && MM_getProp(parentLayer, 'P')) {
                                parentL += parseInt(MM_getProp(parentLayer, 'L'));
                                parentT += parseInt(MM_getProp(parentLayer, 'T'));
                                parentLayer = parentLayer.parentNode;
                                if (parentLayer == document) 
                                    parentLayer = null;
                            }
                        }
                        else 
                            if (IE) {
                                parentLayer = aLayer.parentElement;
                                while (parentLayer != null && MM_getProp(parentLayer, 'P')) {
                                    parentL += MM_getProp(parentLayer, 'L');
                                    parentT += MM_getProp(parentLayer, 'T');
                                    parentLayer = parentLayer.parentElement;
                                }
                            }
                        var tmpX = mouseX - ((MM_getProp(aLayer, 'L')) + parentL + MM_hLeft);
                        var tmpY = mouseY - ((MM_getProp(aLayer, 'T')) + parentT + MM_hTop);
                        if (String(tmpX) == "NaN") 
                            tmpX = 0;
                        if (String(tmpY) == "NaN") 
                            tmpY = 0;
                        var tmpW = MM_hWidth;
                        if (tmpW <= 0) 
                            tmpW += MM_getProp(aLayer, 'W');
                        var tmpH = MM_hHeight;
                        if (tmpH <= 0) 
                            tmpH += MM_getProp(aLayer, 'H');
                        if ((0 <= tmpX && tmpX < tmpW && 0 <= tmpY && tmpY < tmpH) &&
                        (maxDragZ == null ||
                        maxDragZ <= aLayerZ)) {
                            curDrag = aLayer;
                            maxDragZ = aLayerZ;
                        }
                    }
            }
            if (curDrag) {
                document.onmousemove = MM_dragLayer;
                curLeft = MM_getProp(curDrag, 'L');
                curTop = MM_getProp(curDrag, 'T');
                if (String(curLeft) == "NaN") 
                    curLeft = 0;
                if (String(curTop) == "NaN") 
                    curTop = 0;
                MM_oldX = mouseX - curLeft;
                MM_oldY = mouseY - curTop;
                document.MM_curDrag = curDrag;
                curDrag.MM_SNAPPED = false;
                if (curDrag.MM_toFront) {
                    var newZ = parseInt(document.MM_maxZ) + 1;
                    eval('curDrag.' + ('style.') + 'zIndex=newZ');
                    if (!curDrag.MM_dropBack) 
                        document.MM_maxZ++;
                }
                retVal = false;
                if (!NS) 
                    event.returnValue = false;
            }
        }
        else 
            if (theEvent == 'mousemove') {
                if (document.MM_curDrag) 
                    with (document.MM_curDrag) {
                        var mouseX = (NS) ? objId.pageX : event.clientX + document.body.scrollLeft;
                        var mouseY = (NS) ? objId.pageY : event.clientY + document.body.scrollTop;
                        var newLeft = mouseX - MM_oldX;
                        var newTop = mouseY - MM_oldY;
                        if (MM_bL != null) 
                            newLeft = Math.max(newLeft, MM_bL);
                        if (MM_bR != null) 
                            newLeft = Math.min(newLeft, MM_bR);
                        if (MM_bT != null) 
                            newTop = Math.max(newTop, MM_bT);
                        if (MM_bB != null) 
                            newTop = Math.min(newTop, MM_bB);
                        MM_LEFTRIGHT = newLeft - MM_startL;
                        MM_UPDOWN = newTop - MM_startT;
                        if (NS) {
                            style.left = newLeft + "px";
                            style.top = newTop + "px";
                        }
                        else {
                            style.pixelLeft = newLeft;
                            style.pixelTop = newTop;
                        }
                        if (MM_dragJS) 
                            eval(MM_dragJS);
                        retVal = false;
                        if (!NS) 
                            event.returnValue = false;
                    }
            }
            else 
                if (theEvent == 'mouseup') {
                    document.onmousemove = null;
                    if (NS) 
                        document.releaseEvents(Event.MOUSEMOVE);
                    if (NS) 
                        document.captureEvents(Event.MOUSEDOWN); //for mac NS
                    if (document.MM_curDrag) 
                        with (document.MM_curDrag) {
                            if (typeof MM_targL == 'number' && typeof MM_targT == 'number' &&
                            (Math.pow(MM_targL - (MM_getProp(document.MM_curDrag, 'L')), 2) +
                            Math.pow(MM_targT - (MM_getProp(document.MM_curDrag, 'T')), 2)) <=
                            MM_tol) {
                                if (NS) {
                                    style.left = MM_targL + "px";
                                    style.top = MM_targT + "px";
                                }
                                else {
                                    style.pixelLeft = MM_targL;
                                    style.pixelTop = MM_targT;
                                }
                                MM_SNAPPED = true;
                                MM_LEFTRIGHT = MM_startL - MM_targL;
                                MM_UPDOWN = MM_startT - MM_targT;
                            }
                            if (MM_everyTime || MM_SNAPPED) 
                                eval(MM_dropJS);
                            if (MM_dropBack) {
                                style.zIndex = MM_oldZ;
                            }
                            retVal = false;
                            if (!NS) 
                                event.returnValue = false;
                        }
                    document.MM_curDrag = null;
                }
        if (NS) 
            document.routeEvent(objId);
    }
    return retVal;
}
