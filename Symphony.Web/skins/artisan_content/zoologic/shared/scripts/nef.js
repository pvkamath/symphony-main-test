// hopefully this wont change :-)

/* 
.numericInputFieldIE {
	BORDER-TOP-WIDTH: 1px; BACKGROUND-POSITION: center 50%; FONT-WEIGHT: bold; BORDER-LEFT-WIDTH: 1px; FONT-SIZE: 11px; BORDER-LEFT-COLOR: #333333; BORDER-BOTTOM-WIDTH: 1px; BORDER-BOTTOM-COLOR: #333333; WIDTH: 15px; COLOR: #000000; BORDER-TOP-COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; HEIGHT: 20px; TEXT-ALIGN: center; BORDER-RIGHT-WIDTH: 1px; BORDER-RIGHT-COLOR: #666666
}
.numericInputFieldNN {
	BACKGROUND-POSITION: center 50%; FONT-WEIGHT: bold; BORDER-LEFT-COLOR: #333333; BORDER-BOTTOM-COLOR: #333333; WIDTH: 15px; COLOR: #000000; BORDER-TOP-COLOR: #666666; HEIGHT: 20px; TEXT-ALIGN: center; BORDER-RIGHT-COLOR: #666666
}
.answerButtonsSpan {
	VISIBILITY: hidden; POSITION: relative
}
*/

var widgetDigits = 3;
var widgetDecimals = 1;

function numberOfDigits()
// The number of fields for digits on this page,
// including the decimals.
{ alert("numberOfDigits called... returned :: " + widgetDigits);
	return widgetDigits;
}

function numberOfDecimals()
// Number of fields holding
// decimal parts about it.
{ alert("numberOfDecimals called... returned :: " + widgetDecimals);
	return widgetDecimals;
}

function setWidgetDigits(aNumber) {
alert("setWidgetDigits called... returned :: " + aNumber);
widgetDigits=aNumber;
}

function setWidgetDecimals(aNumber) {
alert("setWidgetDecimals called... returned :: " + aNumber);
widgetDecimals=aNumber;
}

function getDigits( aString ) {
	var length = aString.length;
	var digits = 0;
	for (i=0;i<length;i++) {
		if( aString.substr(i,1)=="1" )	{ digits++ };
	}
	return digits;
//	alert("digits: "+digits);
}

function getDecimals( aString ) {
	var length = aString.length;
	var stringArray = aString.split('.');
	var decimalPlace = 0;
//	alert ( 'stringArray :\n' + stringArray + '\n' + stringArray.length );
	if (stringArray.length == 1) {
		return 0;
	} else {
		decimalPlaces = getDigits(stringArray[1]);
		return decimalPlaces;
	}
}

function generateWidget(mask,layerName) {

	setWidgetDigits(getDigits(mask));
	setWidgetDecimals(getDecimals(mask));
	generatedHTML = generateNEFHTML(mask);

	if (layerName) {
		alert('there is layerName');
		MM_setTextOfLayer_original(layerName,'',generatedHTML);
	} else {
		alert('there is no layerName');
		MM_setTextOfLayer_original('digitSpan','',generatedHTML);
	}

	setUpWidget();
}

function setUpWidget() {
	alert(MM_findObj('digitForm'));
	alert(MM_findObj('digitForm').digit_3);
	starUp();
//	setFocusToFirstCell();
}

function isNetscape ()
{
	var 	appName 	=  navigator.appName.toLowerCase();
	var 	index   	= appName.indexOf("netscape");
	var 	aBoolean 	= (index >= 0);
	return 	aBoolean; 
}

_ISNETSCAPE = isNetscape();

function generateNEFHTML ( varFormat ) {

alert('generateNEFHTML invoqued');
alert(varFormat);

	var length = varFormat.length;
	var currentDigit = 0;
	var WidgetHTML = '';
	var tableHeaderHTML = '<table border="0" cellspacing="0" cellpadding="0"><tr>';
	var tableFooterHTML = '</tr></table>';
	var eachDigitHeaderHTML = '<td>';
	var eachDigitFooterHTML = '</td>';
	var INPUT_STYLE = _ISNETSCAPE?'numericInputFieldNN':'numericInputFieldIE';
	// open table tag
	WidgetHTML += tableHeaderHTML;
	
	// add each cell of the table and fill it with the proper content(text field or format)
	for ( i=0;i<length;i++) {
		currentCharacter = varFormat.substr(i,1);
		currentCellHTML = '';
		if (currentCharacter=='1') {
			currentDigit++;
		}

		switch (currentCharacter) {
			case "1" :
				currentCellHTML+=eachDigitHeaderHTML;
				currentCellHTML+='<input onFocus="mouseDownOnField(this)" name="digit_'+currentDigit+'" tabindex="'+currentDigit+'" maxlength="1"  size="1" type="text"  value="" class="'+ INPUT_STYLE + '" >';
				currentCellHTML+=eachDigitFooterHTML;
				break;
			case ".":
			case ",":
			case "'":
			case "$":
			case "%":
			case "-":
			case " ":
				currentCellHTML+=eachDigitHeaderHTML+'<span class="inputText">&nbsp;'+currentCharacter+'&nbsp;</span>' + eachDigitFooterHTML;
				break;
			default :
				currentCellHTML+=eachDigitHeaderHTML+'<span class="inputText">'+currentCharacter+'</span>' + eachDigitFooterHTML;
		}
		
		WidgetHTML += currentCellHTML;
		
	}
	
	// close table
	WidgetHTML+=tableFooterHTML;
	
	//generate form only if is explicitly asked for
	openForm = '<form name="digitForm" method="post" action="" id="digitForm">';
	closeForm = '</form>';

	WidgetHTMLwForm = openForm + WidgetHTML + closeForm;
	WidgetHTML = WidgetHTMLwForm;
	
	alert(WidgetHTML);
	// return value
	return WidgetHTML;	
}

// *********************************************************************************
// **                   Checks if the user entry value is aceptable
// **
// *********************************************************************************

function checkInterval ( theField ) {
	netscapeBrowser = isNetscape();
	userEntry = Number(answerDigits());
	bottomValue = netscapeBrowser ? document.NTS_InputDialogue.document.forms.digitForm.elements.bottom.value : Number(theField.parentElement.all.bottom.value);
	topValue = netscapeBrowser ? document.NTS_InputDialogue.document.forms.digitForm.elements.top.value : Number(theField.parentElement.all.top.value);
	variableNumber = netscapeBrowser ? document.NTS_InputDialogue.document.forms.digitForm.elements.variableNumber.value : Number(theField.parentElement.all.variableNumber.value);

//alert( userEntry );
//alert( bottomValue );
//alert( topValue );
//alert( variableNumber );

	if ( ( userEntry < bottomValue ) || ( userEntry > topValue ) ) {
		alert ( errorArray[variableNumber] );
		setFocusToFirstCell();
	}
	else {
		variablesValue[ variableNumber ]= userEntry ;
		setVar();
		if (netscapeBrowser) {
			MM_showHideLayers('NTS_InputDialogue','','hide');
		}
		else {
			MM_showHideLayers('inputDialogue','','hide');
		}
	}

}

// *********************************************************************************
// **                         Lesson Link Translator
// **
// *********************************************************************************

function linkCode() {

args=linkCode.arguments;
linkHTML = args[0];
linkText = args[1];

translatedCode = (isNetscape()) ? linkText : linkHTML;

return translatedCode;

}

// *********************************************************************************
// **                           DAMON'S FORMATING FUNCTION
// **
// *********************************************************************************

function formatString(aNumber,roundToPlaces,commas,symbol) {

	theInput = aNumber * 1.0; // Cast the input to a number datatype.

	// Handle ticks
	if (symbol == "ticks") {
	
		theInput = theInput * 100; // Convert from percent
	
		aTick = 1/32; // Define a tick
		aHalfTick = 1/64; // Define a half-tick
		
		// Split ticks from integer part
		integerPart = Math.floor(theInput);
		tickPart = theInput - integerPart;
		
		theTicks = Math.round(tickPart / aTick) + "";
		
		// Figure-out if a half-tick is necessary
		
		theRemainder = tickPart - (theTicks * aTick);
		
		if ( theRemainder == aHalfTick ) {
			isHalfTick = "+";
		} else {
			isHalfTick = "";
		}
		
		
		// Pad zeros onto the front of the ticks part
		if ( theTicks.length == 1 ) {
			theTicks_display = "0" + theTicks;
		} else {
			theTicks_display = theTicks + "";
		}
		
		// Re-assemble tick string
		tick_string = integerPart + " - " + theTicks_display + isHalfTick;
	
		return tick_string;
	}

	// Modify inputs and commas if format is 'percent'.
	if (symbol == "%") {
		commas = false;
		theInput = theInput*100;
	}

	// Round to roundToPlaces
	multiplier = Math.pow(10,roundToPlaces);
	scaled2 = Math.round(multiplier*theInput);
	roundedNumber = (scaled2/multiplier) + ""; // Putting + "" forces Javascript to cast this var as a string.
	end = roundedNumber.indexOf(".");

	// Is the number negative? If so, compensate by removing the minus sign.
	if (roundedNumber<0) { 
		roundedNumber = roundedNumber * -1;
		isNegative = "-";
	} else {
		isNegative = "";
	}
	
	// Add commas
	displaynumber = roundedNumber + "";
	index = 1;
	revString = "";
	ltz_part = "";
	
	decimalIndex = displaynumber.indexOf(".");
	
	if (decimalIndex==-1) {
		end = displaynumber.length-1;
	} else {
		end = displaynumber.indexOf(".") - 1;
		ltz_part = displaynumber.substring(end+1,displaynumber.length);
	}
	
	if (commas==true) {
		for (i=end;i>-1;i--) {
			if (index==3) {
				revString = revString + displaynumber.substr(i,1) + ",";
				index = 1;
			} else {
				revString = revString + displaynumber.substr(i,1);
				index++;
			}
		}
	
		// Reverse the string
		fwdString = "";
		for (i=revString.length ; i>-1 ; i-- ) {
			fwdString = fwdString + revString.substr(i,1);
		}
	
		fwdString = fwdString + ltz_part;
	
		if (fwdString.substr(0,1)==",") {
			fwdString = fwdString.substr(1,fwdString.length);
		}
		
	} else {
		fwdString = displaynumber + "";
	}
	
	displaynumber = fwdString + "";
	
	// Find how many zeros need to be padded to the end of the number
	end = displaynumber.length;
	decimalIndex = displaynumber.indexOf(".");
	
	if (decimalIndex==-1) {
		zerosNeeded = roundToPlaces;
	} else {
		zerosNeeded = roundToPlaces-(end-decimalIndex)+1;
	}
	
	// Pad zeros to the end of the string
	padding = "";
	if ((zerosNeeded == roundToPlaces) && (roundToPlaces != 0)) {
		displaynumber = displaynumber + ".";
	}
	
	for (i=1; i<=zerosNeeded; i++) {
		padding = padding + "0";
	}
	
	displaynumber = displaynumber + padding;
	
	// Add currency or "%" sign
	if (symbol == "%") {
		displaynumber = isNegative + displaynumber + "%"; //&#037; \u00E0
	} else {
		displaynumber = isNegative + symbol + displaynumber;
	}
	
	// Add zero to beginning if necessary
	theIndexOfDot = displaynumber.indexOf(".");
	
	if (theIndexOfDot == 0) {
		displaynumber = "0" + displaynumber;
	}
	
	if (displaynumber.substr(0,2) == "-.") {
		displaynumber = "-0." + displaynumber.substr(2);
	}

	if (displaynumber.substr(0,2) == "$.") {
		displaynumber = "$0." + displaynumber.substr(2);
	}

	return displaynumber;
}


// *********************************************************************************
// **                    DAMON'S ALIGNMENT CORRECTION FUNCTIONS
// **
// *********************************************************************************

function alignTagBegin(alignment) {
	if (document.all) {
		theTag = "<div align=\'" + alignment + "\'>";
	} else {
		theTag = "<span class='total'>";
	}
	return theTag;
}

function alignTagEnd() {
	if (document.all) {
		theTag = "</div>";
	} else {
		theTag = "</span>";
	}
	return theTag;
}

