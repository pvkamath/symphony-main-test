


function testNavigationFrameTEST(){
    notify("Starting testNavigationFrameTEST. ");
    
    ////////////////////////////////////////
    // Set number of questions
    
    setNumberOfQuestions(7);
    notify("No of questions was set to 7.");
    
    
    //////////////////////////////////
    // change current question number
    //
    setQuestionNumber(2);
    notify("The current question was set to 2");
    
    
    //////////////////////////////////////////
    // PAGE NUMBER STATES 
    // 
    
    notify("Setting   PAGENUMBER STATES: \n" +
    "// notchanged = 1 (square) \n" +
    "// changed    = 2 (square - red dot) \n" +
    "// correct    = 3 (check) \n" +
    "// incorrect  = 4 (x) \n" +
    "// noanswer   = 5 (question mark) \n")
    
    setPageNoToState(3, 1);
    notify("Page 3 was set to state  NOTCHANGED  = 1 (square) ");
    
    setPageNoToState(3, 2);
    notify("Page 3 was set to state  CHANGED     = 2 (square - red dot) ");
    
    setPageNoToState(3, 3);
    notify("Page 3 was set to state  CORRECT    = 3 (check) ");
    
    setPageNoToState(3, 4);
    notify("Page 3 was set to state  INCORRECT  = 4 (x) ");
    
    setPageNoToState(3, 5);
    notify("Page 3 was set to state  NOANSWER   = 5 (question mark)");
    
    
    setPageNoToState(3, 1);
    notify("Page 3 was set back to state  NOTCHANGED  = 1 (square) ");
    
    
    /////////////////////////////
    // change NAVBAR STATES 
    // MISSING: what about other states than 2
    //
    
    notify("Setting   NAVBAR  STATES: \n" +
    "//NAVBAR STATES: function setState (aState)  \n" +
    "// test   mode                = 1 \n" +
    "// test question review       = 2 \n" +
    "// test results               = 3 \n" +
    "// test question  remediation = 4 \n")
    
    
    setState(1);
    notify("The navbar was set to state TEST        = 1 ");
    
    setState(2);
    notify("The navbar was set to state  REVIEW     = 2  ");
    
    setState(3);
    notify("The navbar was set to state RESULTS     = 3 ");
    
    setState(4);
    notify("The navbar was set to state REMEDIATION = 4 ");
    
}


function notify(aString){
    refreshNavigationBar();
    alert(aString +
    "\n\nLook at the navigation bar now." +
    " \n\nClick OK");
}
