


function scoGetTestDuration() // sco219
{
    // used to be 35 minutes
    // see also scoGetSessionTime();
    //
    var secs = parent.scoGetSessionTime();
    var mins = Math.round(secs / 60);
    var str = mins + ' minutes';
    return str;
    
}


function scoGetLevel(){
    var basic = parent.scoDifficulty().toString().toLowerCase();
    
    if (basic == "difficult") 
        return "Advanced";
    if (basic == "medium") 
        return "Intermediate";
    if (basic == "easy") 
        return "Basic";
    return "";
}



// =================================== end sco219 



function scoGetUserName(){
    return parent.scoGetUserName();
}

function scoGetDate(){
    //	return "March 26, 1977";
    return parent.scoGetDate();
}

function scoGetScore(){
    //	return  "80%";
    return parent.scoGetScore();
}


function scoGetCourseName(){
    return parent.courseTitle2();
}


function getQuestionResultData(){
    // to be created ... expected format [correct, incorrect, unanwered]
    //	return [8,0,2];
    
    var result = parent.calculateScoreStats();
    return result;
}

function scoGetQuestionResults(){
    var HTML = '';
    var resultsArray = getQuestionResultData();
    HTML = resultsArray[0] + ' correct<br>';
    HTML += resultsArray[1] + ' incorrect<br>';
    HTML += resultsArray[2] + ' unanswered';
    
    return HTML;
}


function getQuestionState(qNumber){
    return parent.navigationFrame.getPageNoState(qNumber);
}


function getLessonTitle(qNumber){
    var qURL = parent.scoInfo()[qNumber];
    var lURL = parent.lessonMap()[qURL];
    var lTitle = parent.lessonTitleMap()[lURL];
    
    return lTitle;
}

// Navigation

function scoExitTest(){

    // $Revision:   $   $Author:  $
    //  parent.scoUnloaded();
    
    parent.exitButtonClicked();
}


function scoContinueTest(){ // called when user decides to take the test.
    // var THESCO = parent.getSco();
    // THESCO.loadYourNextPage ();  // #sco130.
    parent.doContinue();
}


function scoWelcomeReviewTest(){
    parent.doReview()
}

function scoWelcomeRetakeTest(){
    // sco340
    setTimeout('parent.doRetake ()', 50);
}
