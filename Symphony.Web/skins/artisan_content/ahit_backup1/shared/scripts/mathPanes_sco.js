//**************************************************
//// NEW METHODS FOR NUMBER AND STRING OBJECTS ////
//**************************************************

var INPUTBOXINITILIZED = false;

function normalize(n,permittedChars) {
    n = (n)?n+"":""; var o = "", strCheck = permittedChars, s;
    var i = n.length;
    while (i-->0) o=((permittedChars.indexOf(s=n.charAt(i))!=-1)?s:"")+o;
    return (o==""||o=="-")?0:o;
}

function getMaskFromNumber() {
	var originalObject = this;
	var castedString = String(originalObject);
	var mask = '';
	for (i=0;i<castedString.length;i++) {
		currentChar = castedString.substr(i,1);
		mask+= (currentChar<'0' || currentChar>'9')?currentChar:'1';
	}
	return mask;
}

Number.prototype.getMaskFromNumber = getMaskFromNumber;
String.prototype.getMaskFromNumber = getMaskFromNumber;
String.prototype.formatString = formatString;
Number.prototype.formatString = formatString;


function numberOfDigits()
// The number of fields for digits on this page,
// including the decimals.
{ return widgetDigits;
}

function numberOfDecimals()
// Number of fields holding
// decimal parts about it.
{ return widgetDecimals;
}

function pageType ()
{ return 'solution';
}


function setWidgetDigits(aNumber) {
widgetDigits=aNumber;
}

function setWidgetDecimals(aNumber) {
widgetDecimals=aNumber;
}

function showInputBox ( variableNumber , varFormat , varRange ){

	// updates or generates (IE & NTS ) the html for the dialogue box
	generateWidgetHTML(variableNumber , varFormat , varRange);	

	//sets the variables for the "jumping" between input fields
	setWidgetDigits(getDigits(varFormat));
	setWidgetDecimals(getDecimals(varFormat));
	
	//ataches the event handlers to the input fields
	startUp();
	
//	restoreDigits(2223.45,varFormat); 
	// sets the focus to the first input field
	
	INPUTBOXINITILIZED = true;
	setFocusToFirstCell();
}

function generateWidgetHTML () {
args=generateWidgetHTML.arguments;

variableNumber = args[0];
varFormat = args[1];
varRange = args[2];
//alert('generateWidget invoqued');
	if (isNetscape()) {
		updatedHTML = generateNTSWidgetHTML(variableNumber , varFormat , varRange);
		Netscape_InputDialogue = MM_findObj('NTS_InputDialogue');
		Netscape_InputDialogue.document.write(updatedHTML);
		Netscape_InputDialogue.document.close();
	} else {
		updatedHTML = generateIEWidgetHTML(varFormat);
		WidgetSpanObject = MM_findObj('allTheInputFields');
		WidgetSpanObject.innerHTML = updatedHTML;
		formObject = MM_findObj('digitForm');
		formObject.variableNumber.value = variableNumber;
		formObject.bottom.value = varRange[0];
		formObject.top.value = varRange[1];
	};
}

function generateNTSWidgetHTML ( ) {
	
//	alert ('generatesNTSWidgetHTML called!!!');

	args=generateNTSWidgetHTML.arguments;

	variableNumber = args[0];
	varFormat = args[1];
	varRange = args[2];
	
	var WidgetHTML = '';
//	var promptMessage = promptArray[variableNumber];
	var bottomValue = varRange[0];
	var topValue = varRange[1];

	var headerHTML = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><tr><td>'
				   + '<span name="digitSpan" id="digitSpan">'
				   + '<form name="digitForm" method="post" action="" id="digitForm">'
				   + '<div align="center">'
				   + '<table border="0" cellspacing="5" cellpadding="0"><tr><td>'
				   + '<span name="allTheInputFields" id="allTheInputFields">';

	var newInputFields = generateIEWidgetHTML(varFormat);


//					+ '<a name=\'trigerButton\' href="javascript:;" onMouseUp="checkInterval(this)" onMouseOut="MM_swapImgRestore()" onMouseDown="MM_swapImage(\'checkButton1\',\'\',\'../shared/images/check_on.gif\',1)"><img name="checkButton1" border="0" src="../shared/images/check_off.gif" width="40" height="20"></a>'
	var footerHTML  = '</span>'
					+ '</td>'
					+ '<td valign="middle">'
					+ '<span name="dataSpan" id="dataSpan">'
					+ '<input type="hidden" name="variableNumber" value="1">'
					+ '<input type="hidden" name="top" value="1">'
					+ '<input type="hidden" name="bottom" value="1">'
					+ '</span>'
					+ '</td></tr>'
					+ '</table>'
					+ '</div>'
					+ '</form>'
					+ '</span>'
					+ '</td></tr>'
					+ '</table>';
	
	WidgetHTML = headerHTML + newInputFields + footerHTML;
//	alert (WidgetHTML);	
	return WidgetHTML;
}

function generateIEWidgetHTML ( varFormat ) {

	var _ISNETSCAPE = isNetscape();
	var length = varFormat.length;
	var currentDigit = 0;
	var WidgetHTML = '';
	var tableHeaderHTML = '<table border="0" cellspacing="0" cellpadding="0"><tr>';
	var tableFooterHTML = '</tr></table>';
	var eachDigitHeaderHTML = '<td>';
	var eachDigitFooterHTML = '</td>';
	var inputFormat = 'NEFinput';
	// open table tag
	WidgetHTML += tableHeaderHTML;
	
	// add each cell of the table and fill it with the proper content(text field or format)
	for ( i=0;i<length;i++) {
		currentCharacter = varFormat.substr(i,1);
		currentCellHTML = '';
		if (currentCharacter=='1') {
			currentDigit++;
		}

		switch (currentCharacter) {
			case "1" :
				currentCellHTML+=eachDigitHeaderHTML;
				currentCellHTML+='<input onFocus="mouseDownOnField(this)" name="digit_'+currentDigit+'" tabindex="'+currentDigit+'" maxlength="1"  size="1" type="text"  value="" class="' + inputFormat + '" >';
				currentCellHTML+=eachDigitFooterHTML;
				break;
			case ".":
			case ",":
			case "'":
			case "$":
			case "%":
			case "-":
			case " ":
				currentCellHTML+='<td><span class="NEFinputText">&nbsp;'+currentCharacter+'&nbsp;</span></td>';
				break;
			default :
				if (_ISNETSCAPE) {
					currentCellHTML+='<td><font size="3" face="Arial, Helvetica, sans-serif"><strong>'+currentCharacter+'</strong></font></td>';
				} else {
					currentCellHTML+='<td><span class="NEFinputText">'+currentCharacter+'</span></td>';
				}				
		}
		
		WidgetHTML += currentCellHTML;
		
	}
	
	// close table
	WidgetHTML+=tableFooterHTML;
	

	// return value
	return WidgetHTML;	
}


function getDigits( aString ) {
	var length = aString.length;
	var digits = 0;
	for (i=0;i<length;i++) {
		if( aString.substr(i,1)=="1" )	{ digits++ };
	}
	return digits;
//	alert("digits: "+digits);
}

function getDecimals( aString ) {
	var length = aString.length;
	var stringArray = aString.split('.');
	var decimalPlace = 0;
//	alert ( 'stringArray :\n' + stringArray + '\n' + stringArray.length );
	if (stringArray.length == 1) {
		return 0;
	} else {
		decimalPlaces = getDigits(stringArray[1]);
		return decimalPlaces;
	}
}

// *********************************************************************************
// **                   Checks if the user entry value is aceptable
// **
// *********************************************************************************


function checkInterval ( theField ) {
	netscapeBrowser = isNetscape();
	userEntry = Number(answerDigits());
	bottomValue = netscapeBrowser ? document.NTS_InputDialogue.document.forms.digitForm.elements.bottom.value : Number(theField.parentElement.all.bottom.value);
	topValue = netscapeBrowser ? document.NTS_InputDialogue.document.forms.digitForm.elements.top.value : Number(theField.parentElement.all.top.value);
	variableNumber = netscapeBrowser ? document.NTS_InputDialogue.document.forms.digitForm.elements.variableNumber.value : Number(theField.parentElement.all.variableNumber.value);

alert( userEntry );
alert( bottomValue );
alert( topValue );
alert( variableNumber );

	if ( ( userEntry < bottomValue ) || ( userEntry > topValue ) ) {
		alert ( errorArray[variableNumber] );
		setFocusToFirstCell();
	}
	else {
		variablesValue[ variableNumber ]= userEntry ;
		setVar();
		if (netscapeBrowser) {
			MM_showHideLayers('NTS_InputDialogue','','hide');
		}
		else {
			MM_showHideLayers('inputDialogue','','hide');
		}
	}

}

// *********************************************************************************
// **                         Lesson Link Translator
// **
// *********************************************************************************

function linkCode() {

args=linkCode.arguments;
linkHTML = args[0];
linkText = args[1];

translatedCode = (isNetscape()) ? linkText : linkHTML;

return translatedCode;

}

// *********************************************************************************
// **                           DAMON'S FORMATING FUNCTION
// **
// *********************************************************************************
function formatString(aNumber,roundToPlaces,commas,symbol) {

	aNumber = (aNumber)?aNumber:this;
	roundToPlaces = (roundToPlaces>0)?roundToPlaces:0;
	commas = (commas=='undefined' || commas==false)?false:true;
	symbol = (symbol)?symbol:"$";
	
	theInput = Number(aNumber); // Cast the input to a number datatype.

	// Handle ticks
	if (symbol == "ticks") {
	
		theInput = theInput * 100; // Convert from percent
	
		aTick = 1/32; // Define a tick
		aHalfTick = 1/64; // Define a half-tick
		
		// Split ticks from integer part
		integerPart = Math.floor(theInput);
		tickPart = theInput - integerPart;
		
		theTicks = Math.round(tickPart / aTick) + "";
		
		// Figure-out if a half-tick is necessary
		
		theRemainder = tickPart - (theTicks * aTick);
		
		if ( theRemainder == aHalfTick ) {
			isHalfTick = "+";
		} else {
			isHalfTick = "";
		}
		
		
		// Pad zeros onto the front of the ticks part
		if ( theTicks.length == 1 ) {
			theTicks_display = "0" + theTicks;
		} else {
			theTicks_display = theTicks + "";
		}
		
		// Re-assemble tick string
		tick_string = integerPart + " - " + theTicks_display + isHalfTick;
	
		return tick_string;
	}

	// Modify inputs and commas if format is 'percent'.
	if (symbol == "%") {
		commas = false;
		theInput = theInput*100;
	}

	// Round to roundToPlaces
	multiplier = Math.pow(10,roundToPlaces);
	scaled2 = Math.round(multiplier*theInput);
	roundedNumber = (scaled2/multiplier) + ""; // Putting + "" forces Javascript to cast this var as a string.
	end = roundedNumber.indexOf(".");

	// Is the number negative? If so, compensate by removing the minus sign.
	if (roundedNumber<0) { 
		roundedNumber = roundedNumber * -1;
		isNegative = "-";
	} else {
		isNegative = "";
	}
	
	// Add commas
	displaynumber = roundedNumber + "";
	index = 1;
	revString = "";
	ltz_part = "";
	
	decimalIndex = displaynumber.indexOf(".");
	
	if (decimalIndex==-1) {
		end = displaynumber.length-1;
	} else {
		end = displaynumber.indexOf(".") - 1;
		ltz_part = displaynumber.substring(end+1,displaynumber.length);
	}
	
	if (commas==true) {
		for (i=end;i>-1;i--) {
			if (index==3) {
				revString = revString + displaynumber.substr(i,1) + ",";
				index = 1;
			} else {
				revString = revString + displaynumber.substr(i,1);
				index++;
			}
		}
	
		// Reverse the string
		fwdString = "";
		for (i=revString.length ; i>-1 ; i-- ) {
			fwdString = fwdString + revString.substr(i,1);
		}
	
		fwdString = fwdString + ltz_part;
	
		if (fwdString.substr(0,1)==",") {
			fwdString = fwdString.substr(1,fwdString.length);
		}
		
	} else {
		fwdString = displaynumber + "";
	}
	
	displaynumber = fwdString + "";
	
	// Find how many zeros need to be padded to the end of the number
	end = displaynumber.length;
	decimalIndex = displaynumber.indexOf(".");
	
	if (decimalIndex==-1) {
		zerosNeeded = roundToPlaces;
	} else {
		zerosNeeded = roundToPlaces-(end-decimalIndex)+1;
	}
	
	// Pad zeros to the end of the string
	padding = "";
	if ((zerosNeeded == roundToPlaces) && (roundToPlaces != 0)) {
		displaynumber = displaynumber + ".";
	}
	
	for (i=1; i<=zerosNeeded; i++) {
		padding = padding + "0";
	}
	
	displaynumber = displaynumber + padding;
	
	// Add currency or "%" sign
	if (symbol == "%") {
		displaynumber = isNegative + displaynumber + "%"; //&#037; \u00E0
	} else {
		displaynumber = isNegative + symbol + displaynumber;
	}
	
	// Add zero to beginning if necessary
	theIndexOfDot = displaynumber.indexOf(".");
	
	if (theIndexOfDot == 0) {
		displaynumber = "0" + displaynumber;
	}
	
	if (displaynumber.substr(0,2) == "-.") {
		displaynumber = "-0." + displaynumber.substr(2);
	}

	if (displaynumber.substr(0,2) == "$.") {
		displaynumber = "$0." + displaynumber.substr(2);
	}

	return displaynumber;
}


// *********************************************************************************
// **                    DAMON'S ALIGNMENT CORRECTION FUNCTIONS
// **
// *********************************************************************************

function alignTagBegin(alignment) {
	if (document.all) {
		theTag = "<div align=\'" + alignment + "\'>";
	} else {
		theTag = "<span class='total'>";
	}
	return theTag;
}

function alignTagEnd() {
	if (document.all) {
		theTag = "</div>";
	} else {
		theTag = "</span>";
	}
	return theTag;
}

