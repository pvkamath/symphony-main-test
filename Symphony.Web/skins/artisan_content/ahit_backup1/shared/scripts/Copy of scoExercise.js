// scripts/scoExercise.js
//alert("scripts/scoExercise.js");

function reverseVisibility2(arg){
    var st = getst(arg);
    if (!st) 
        return false;
    if (st.visibility == "hidden") {
        st.visibility = "visible";
        st.display = "block";
        return true;
    }
    if (st.visibility == "hide") {
        st.visibility = "visible";
        st.display = "none";
        return true;
    }
    st.visibility = "hidden";
    st.display = "none";
    return true;
}

function hideVisibility2(arg){
    var st = getst(arg);
    if (!st) 
        return false;
    st.visibility = "hidden";
    st.display = "none";
    return true;
}

function showAnswer(arg){
    for (i = 0; i < 241; i++) {
        var was = reverseVisibility2("explanation" + i);
        if (!was) {
            return
        }
    }
}

function hideAnswer(arg){
    for (i = 0; i < 241; i++) {
        var was = hideVisibility2("explanation" + i);
        if (!was) {
            return
        }
    }
}
 
function scoPageLoaded(idString, classString, windowObject, pageNoString, pageTitle){
    init();
    scoPageLoadedCOMMON(idString, classString, windowObject, pageNoString, pageTitle);
    if (parent.markExerciseVisited) {
        parent.markExerciseVisited(pageId());
    }
    showProgressLocal();
	setTooltips();
	hideAnswer();
	//tooltip.init ();
}

















function setTooltips2() {

//	buttonONE = '<a href="javaScript:correctAnswer()"><IMG SRC="../shared/images/correctanswer.gif" alt="Check Answer" title="Check Answer" border=0></a>';
//	buttonTWO = '<a href="javaScript:showAnswer()"><IMG SRC="../shared/images/showanswer.gif" alt="Show Answer" title="Show Answer" border=0 ></a>';
//	buttonTHREE = '<a href="javascript:getNewRandomSet()"><IMG SRC="../shared/images/tryagainup.gif" alt="Get New Question" title="Get New Question" border=0 ></a>';

//AnswerButtons
//AnswerButtons2
//AnswerButtons3

	var images  = 	document.getElementsByTagName("img");
	var checkIMG ;
    var answerIMG ;
    var retryIMG ;
	
	var IMGs, imageObj, imageSRC;
	
	for (IMGs in images) {
		imageObj = images[IMGs];
		imageSRC = imageObj.src;

//		alert ("imageObj ::: " + imageObj + " \n imageSRC ::: " + imageSRC);
		
		if (imageSRC) {
			if (imageSRC.indexOf("correctAnswer.gif") != -1)  { 
				checkIMG = imageObj;
			};
	
			if (imageSRC.indexOf("showAnswer.gif") != -1)  { 
				answerIMG = imageObj;
			};

			if (imageSRC.indexOf("tryagainup.gif") != -1)  { 
				retryIMG = imageObj;
			};
		}
		
	}


    checkIMG.alt = "Check Answer";
    checkIMG.title = "Check Answer";
    answerIMG.alt = "Show Answer";
    answerIMG.title = "Show Answer";
    if (retryIMG) {
        retryIMG.alt = "Get New Question";
        retryIMG.title = "Get New Question";
	    retryIMG.name = "retryButtonImage";

	}

    checkIMG.name = "checkButtonImage";
    answerIMG.name = "answerButtonImage";


}






function setTooltips() {

	var links  = document.links; 

	var checkIMG ;
    var answerIMG ;
    var retryIMG ;
	var IMGs, imageObj, imageSRC;


	for (IMGs in links) {
		imageObj = links[IMGs];
		imageHREF = imageobj.href;

		if (imageHREF) {
			if (imageHREF.indexOf("correctAnswer") != -1)  { 
					imageObj.alt = "Check Answer";
					imageObj.title = "Check Answer";
			};
	
			if (imageHREF.indexOf("showAnswer") != -1)  { 
				imageObj.alt = "Show Answer";
				imageObj.title = "Show Answer";
					
			};

			if (imageHREF.indexOf("getNew") != -1)  { 
				imageObj.alt="Get New Question";
				imageObj.title="Get New Question";
			};
		}
		
	}

}











function showProgressLocal(){
    if (!parent.topNavigationFrame) 
        return; // PORTAL FIX 
    if (parent.topNavigationFrame.showProgress) {
        var stats = parent.calculateScoreStats();
        var corrects = stats[0];
        var wrongs = stats[1];
        var noAnswers = stats[2];
        var visitedPages = parent.visitedPages();
        var score = corrects / (corrects + wrongs + noAnswers);
        parent.topNavigationFrame.showProgress(score, visitedPages);
    }
}


 
 
function cacheUserAnswer(aString){

    var correctA = trie();
    var pindex = currentPindex();
    localuAnswersAtPut(pindex, aString);
    
    if (cad()) 
        cad()[pindex] = correctA;
    closeFbWindow();
    window.onFocus = raiseFbWindow;
}


function closeFbWindow(){
    if (FBWIN == null) {
        return;
    }
    FBWIN.close();
}

function raiseFbWindow(){
    if (FBWIN == null) {
        return;
    }
    FBWIN.focus();
}



window.onFocus = raiseFbWindow;
 
 
 
 
 
function scoPageUnloaded(){
    if (FBWIN == null) 
        return;
    FBWIN.close();
}

function notifyFeedbackWindowClosed(){
    FBWIN = null;
}

 
var FBWIN; 

function showFeedbackBasic(){
    window.onFocus = raiseFbWindow;
    if (!(FBWIN == null)) {
    
        FBWIN.close();
        
    }
    var msg = getEvaluativeFeedback();
    if (msg == "") 
        return;
    
    
    var wname = 'feedback';
    var url = '#';
    var feats = 'dependent=yes,toolbar=no,status=no,menubar=no,scrollbars=auto,' +
    'width=372,height=134,resizable=no';
    
    
    var shPath = '../../../../../shared/';
    var ptype = pageType();
    if (ptype == 'HtmlExercise') {
        shPath = "../shared/";
    }
    
    var url = shPath + "html/fback.htm";
    var feats = features();
    
    if (document.all) {
        if (window.popup) {
        
            window.popup.close();
            
        }
        var w = 372;
        var h = 174;
        var x = Math.round((screen.width - w) / 2);
        var y = Math.round((screen.height - h) / 2) - 55;
        feats = "dialogLeft:" + x +
        "px;dialogTop:" +
        y +
        "px;dialogHeight:" +
        h +
        "px;dialogWidth:" +
        w +
        "px;dependent:yes;directories:no;menubar:no;personalbar:no;status:no;toolbar:no;resizable:yes;scroll:no;";
        
        
        FBWIN = window.showModelessDialog(url, window, feats);
        return;
    }
    
    FBWIN = window.open(url, wname, feats);
    
    
    FBWIN.focus();
    
}

 
function features(){
    var height = 134;
    var myleft = window.screenLeft;
    var mytop = window.screenTop - height;
    h = 134;
    w = 372;
    myleft = Math.round((screen.width - w) / 2);
    mytop = Math.round((screen.height - h) / 2);
    
    var feats = 'alwaysRaised=yes,dependent=yes,toolbar=no,resizable=no,menubar=no' +
    ',left=' +
    myleft +
    ',screenX=' +
    myleft +
    ',top=' +
    mytop +
    ',screenY=' +
    mytop +
    ',scrollbars=no,width=385,height=' +
    height;
    if (!isIE) {
        var feats = 'alwaysRaised=yes,dependent=yes,toolbar=no,resizable=no,menubar=no' +
        ',screenX=' +
        myleft +
        ',screenY=' +
        mytop +
        ',scrollbars=no,width=385,height=' +
        height;
    }
    
    return feats
}

 
function openScormPopup(aString){
    var wname = aString.replace(/\./g, '_');
    var url = '../../../../' + aString;
    var w = 430, h = 700;
    var features = 'toolbar=no,menubar=no,scrollbars=yes,width=430,height=600';
    window.open(url, wname, features);
}
var m_lessonTitle;
var m_exerciseTitle;
 
function getTitle(arg){
     var etitle = getExerciseTitle(arg);
     var lno = getLessonNumber();
     var realTitle = etitle + " " + lno;
     
     if (m_exerciseTitle) 
         return m_exerciseTitle;
     if (document.m_lessonTitle) 
         return m_lessonTitle;
     if (m_lessonTitle) 
         return m_lessonTitle;
     return 'Exercise'
 }
 
function getLessonNumber(){
     var pno = getExercisePageNumber();
     var parts = pno.toString().split(/\./);
     var lno = parts[0];
     
     return lno;
 }
 
var isFBmodeVAR = true;
 
function isEvaluativeFeedbackMode(){
     return isFBmodeVAR;
}

function init(){

    if (!document.all) {
        if (document.answer_a &&
			document.answer_a.document &&
			document.answer_a.document.forms &&
			document.answer_a.document.forms.answer_a) 
				document.forms.answer_a = document.answer_a.document.forms.answer_a;
        if (document.answer_b &&
			document.answer_b.document &&
			document.answer_b.document.forms &&
			document.answer_b.document.forms.answer_b) 
				document.forms.answer_b = document.answer_b.document.forms.answer_b;
        if (document.answer_c &&
			document.answer_c.document &&
			document.answer_c.document.forms &&
			document.answer_c.document.forms.answer_c) 
				document.forms.answer_c = document.answer_c.document.forms.answer_c;
        if (document.answer_d &&
			document.answer_d.document &&
			document.answer_d.document.forms &&
			document.answer_d.document.forms.answer_d) 
				document.forms.answer_d = document.answer_d.document.forms.answer_d;
        if (document.answer_e &&
			document.answer_e.document &&
			document.answer_e.document.forms &&
			document.answer_e.document.forms.answer_e) 
				document.forms.answer_e = document.answer_e.document.forms.answer_e;
        if (document.answer_f &&
			document.answer_f.document &&
			document.answer_f.document.forms &&
			document.answer_f.document.forms.answer_f) 
				document.forms.answer_f = document.answer_f.document.forms.answer_f;
        if (document.answer_g &&
			document.answer_g.document &&
			document.answer_g.document.forms &&
			document.answer_g.document.forms.answer_g) 
				document.forms.answer_g = document.answer_g.document.forms.answer_g;
        if (document.answer_h &&
			document.answer_h.document &&
			document.answer_h.document.forms &&
			document.answer_h.document.forms.answer_h) 
				document.forms.answer_h = document.answer_h.document.forms.answer_h;
    }
}

function showCorrectInstance(){
    if (!hasNumericAnswer()) 
        return;
    var cid = currentInstanceId();
    if (cid == null) {
        getNewInstance();
        return
    }
    
    var m = document.location.toString().match(cid);
    if (m == null) 
        getSpecificInstance(cid);
}

function currentInstanceId(){
    if (!hasNumericAnswer()) 
        return null;
    var id = pageId();
    var insid;
    if (ixd()) {
        insid = ixd()[id];
        return insid
    }
    return null;
}

function isFirstInstance(){
    if (!hasNumericAnswer()) 
        return false;
    var id = pageId();
    var insid;
    if (ixd()) {
        insid = ixd()[id];
        
        
        if (insid == null) {
            return true;
        }
        else {
            return false;
        }
    }
    return false;
}

function showFeedback(){
    if (hasNumericAnswer()) {
        return;
    }
    if (isEvaluativeFeedbackMode()) {
        showFeedbackBasic();
    }
}

function alertReview(){
    alert("You have already submitted" +
    "\nthe answers to this test." +
    "\n\nChanging the answers after" +
    "\na test was submitted is not" +
    "\nallowed.\n" +
    "\nYou may be able to retake" +
    "\nthe test however, and give" +
    "\na new set of answers.");
}

function uad(){
    if (parent.uaDict) 
        return parent.uaDict();
    return null;
}

function cad(){
    if (parent.caDict) 
        return parent.caDict();
    return null;
}

function ixd(){
    if (parent.ixDict) 
        return parent.ixDict();
    return null;
}

function localPageNoToIndexMap(){
    if (parent['pageNoToIndexMap']) {
        return parent.pageNoToIndexMap();
    }
    return new Object();
}

function cacheInstanceIndex(aString){
    var id = pageId();
    
    if (ixd()) {
        var pno = pageNumb();
        var pindex = localPageNoToIndexMap()[pno];
        
        
        ixd()[pindex] = aString;
        var scoi = parent.scoInfo();
        var oldUrl = scoi[pindex];
        var ipart = oldUrl.match(/i0\S*\//);
        var newUrl = oldUrl.replace(/\/i\S*\//, "/i" + aString + "/");
        
        parent.scoInfo()[pindex] = newUrl;
    }
}

function currentPindex(){
    //	alert( " ::: inside currentPindex ::: " );
    if (parent == this) 
        return 1;
    var pno = pageNumb();
    // alert ( " pno ::: " + pno );
    var pindex = localPageNoToIndexMap()[pno];
    // alert ( " pindex ::: " + pindex );
    return pindex;
}

var debugUA = null;

function localuAnswersAtPut(keyString, aString){
    if (parent == this) {
        debugUA = aString;
        return
    }
    if (parent.uAnswersAtPut) 
        parent.uAnswersAtPut(keyString, aString);
}

function localUAnswersAt(aString){
    if (parent == this) {
        return debugUA;
    }
    if (parent.uAnswersAt) {
        return parent.uAnswersAt(aString);
    }
    return new Object();
}

function retrievePageAnswer(){
    var pindex = currentPindex();
    var result = localUAnswersAt(pindex);
    if (result == 'LESSON') 
        return null;
    if (result == 'EXERCISE') 
        return null;
    return result
}

 
function setUserAnswerBASIC(value){

    NAF_SetValue(value);
}

function localIsReviewMode(){
    if (parent == this) 
        return false;
    if (parent.isReviewMode) 
        return (parent.isReviewMode());
    return false
}

function setUserAnswer(value){

    if (localIsReviewMode()) {
        restoreUserInputs();
        correctAnswer();
        alertReview();
        return;
    }
    NAF_SetValue(value);
    cacheUserAnswer(value);
}

function getUserAnswer(){
    var result = NAF_GetValue();
    if (!result) 
        result = retrievePageAnswer();
    return result;
    
}

function storeCurrentAnswer(){
    var awr = getUserAnswer();
    
    cacheUserAnswer(awr);
}

function finishPageDisplay(){
    if (parent.updateSessionTime) {
    
    
    }
    showAnswerButtons();
    mathPaneInit();
    restoreUserInputs();
}

function mathPaneInit(){
    if (isIE) {
        var thing = 'try {initPage()} catch (e) { }'
        eval(thing);
    }
    
    else {
        if (parent.frames['contentFrame'].initPage) {
            initPage();
        }
    }
}



RANEXERCISE = false;
RANDOMANDRADIO = false;


function getAnswerType(uaString){
    answerTypes = ["mc", "mc2", "dnd", "acc", "ran", "number", "numberRadio"];
    if (RANDOMANDRADIO) 
        return "numberRadio";
    if (RANEXERCISE) 
        return "number";
    if (!uaString.indexOf) 
        return "mc";
    if (uaString.length == 1) 
        return answerTypes[0];
    if (uaString.indexOf("|") != -1) {
        if (uaString.indexOf("t") != -1) 
            return answerTypes[2];
        else 
            if (uaString.indexOf("&") != -1) 
                return answerTypes[4];
            else 
                return answerTypes[3];
    }
    else 
        return answerTypes[0];
    
    return "";
}

function restoreDNDUserInput(uaString){
    setUserAnswer(uaString);
}

function restoreACCUserInput(uaString){
    setUserAnswer(uaString);
}

function restoreRANUserInput(uaString){
    setRANUserAnswer(uaString);
}

function restoreRANRADIOUserInput(uaString){
    setRANRADIOUserAnswer(uaString);
}

function restoreUserInputs(){
    var ua = retrievePageAnswer();
    if (!ua) 
        ua = "";
    
    var uaType = getAnswerType(ua);
    switch (uaType) {
        case "dnd":
            restoreDNDUserInput(ua);
            return;            break;
        case "acc":
            restoreACCUserInput(ua);
            return;            break;
            
        case "ran":
            restoreRANUserInput(String(ua));
            return;            break;
            
        case "number":
            restoreRANUserInput(String(ua));
            return;            break;
            
        case "numberRadio":
            restoreRANRADIOUserInput(String(ua));
            return;            break;
        default:
            
    }
    
    /////////////////////////////////////////
    // If the parent does not tell us it is  
    // NOT in review-mode, hide the spans.
    
    if (parent.isReviewMode) {
        if (!parent.isReviewMode()) // PORTAL FIX	
        {
            getst("correctMsg").visibility = "hidden";
            getst("incorrectMsg").visibility = "hidden";
            getst("missingMsg").visibility = "hidden";
        }
    }
    else {
        getst("correctMsg").visibility = "hidden";
        getst("incorrectMsg").visibility = "hidden";
        getst("missingMsg").visibility = "hidden";
    }
    
    if (hasNumericAnswer()) {
        if (!ua) {
            return;
        }
        setUserAnswerBASIC(ua);
        return;
    }
    
    var rbs = rbuttons();
    for (key in rbs) {
        rbs[key].checked = false;
    }
    
    
    if (ua == null) 
        return;
    
    if (ua) {
        if (rbs[ua]) {
            rbs[ua].checked = true;
        }
        
        else {
            var i = 0;
            if (rbs == null) 
                return;
            
            
            
            var ua2 = ua + "";
            
            if (rbs['a']) 
                rbs['a'].checked = (ua2.charAt(i++) == "1");
            if (rbs['b']) 
                rbs['b'].checked = (ua2.charAt(i++) == "1");
            if (rbs['c']) 
                rbs['c'].checked = (ua2.charAt(i++) == "1");
            if (rbs['d']) 
                rbs['d'].checked = (ua2.charAt(i++) == "1");
            if (rbs['e']) 
                rbs['e'].checked = (ua2.charAt(i++) == "1");
            if (rbs['f']) 
                rbs['f'].checked = (ua2.charAt(i++) == "1");
            if (rbs['g']) 
                rbs['g'].checked = (ua2.charAt(i++) == "1");
            
            
            isFBmodeVAR = false;
        }
    }
    
}


function getExerciseNumber(arg){






    var pno = pageNumb();
    
    if (!pno) {
        pno = document.m_exerciseNumber
        
    };
    if (!pno) {
        pno = m_exerciseNumber;
        
    };
    if (!pno) {
        pno = getExerciseTitle();
        var words = pno.toString().split(/\ /);
        pno = words[1];
        
    }
    
    
    var numberArr = pno.toString().split(/\./);
    var number = numberArr[numberArr.length - 1];
    
    return (number + '.');
}

function getExercisePageNumber(arg){



    var pno = pageNumb();
    
    if (!pno) {
        pno = document.m_exerciseNumber
        
    };
    if (!pno) {
        pno = m_exerciseNumber;
        
    };
    if (!pno) {
        pno = getExerciseTitle();
        
        
    }
    
    
    return pno
}

 
var lastRind;

function rind(){
    var maxi = 10;
    if (parent == this) {
        maxi = 10
    }
    else {
        if (parent.noOfInstancesMax) // PORTAL FIX
        {
            maxi = parent.noOfInstancesMax();
        }
    }
    
    var rfloat = (Math.random()) * maxi;
    var rindex = Math.round(rfloat + 0.5);
    var pad = rindex.toString() + "";
    var res = "";
    if (pad.length == 3) {
        res = '0' + pad;
    }
    if (pad.length == 2) {
        res = '00' + pad;
    }
    if (pad.length == 1) {
        res = '000' + pad;
    }
    if (res == lastRind) {
        return (rind());
    }
    lastRind = res;
    cacheInstanceIndex(res);
    return res;
}

function getNewInstance(){
    setUserAnswer(null);
    var newloc = '../i' + rind() + '/exercise.htm';
    document.location = newloc;
}

function getSpecificInstance(idString){
    setUserAnswer(null);
    var newloc = '../i' + idString + '/exercise.htm';
    document.location = newloc;
    finishPageDisplay();
}

function isAnswerButtonsVisible(){
    return true
}

function getEvaluativeFeedback(){
    return '';
}

function showAnswerButtons(){
    var abstyle = getst("AnswerButtons");
    var abstyle2 = getst("AnswerButtons2");
    var abstyle3 = getst("AnswerButtons3");
    if (!abstyle) 
        return;
    
    abstyle.visibility = "visible";
    abstyle2.visibility = "visible";
    if (!abstyle3) 
        return;
    abstyle3.visibility = "visible";
}

function rbuttons(){
	
    if (document.getElementById) {
        return rbuttonsMS();
    }
    
    if (isIE) {
        return rbuttonsMS();
    }
    else {
        return rbuttonsNS();
    }
}

function rbuttonsNS(){
    var rbs = new Object();
    if (getsp("answer_a")) 
        rbs['a'] = getsp("answer_a").document.forms[0].elements[0];
    if (getsp("answer_b")) 
        rbs['b'] = getsp("answer_b").document.forms[0].elements[0];
    if (getsp("answer_c")) 
        rbs['c'] = getsp("answer_c").document.forms[0].elements[0];
    if (getsp("answer_d")) 
        rbs['d'] = getsp("answer_d").document.forms[0].elements[0];
    if (getsp("answer_e")) 
        rbs['e'] = getsp("answer_e").document.forms[0].elements[0];
    if (getsp("answer_f")) 
        rbs['f'] = getsp("answer_f").document.forms[0].elements[0];
    if (getsp("answer_g")) 
        rbs['g'] = getsp("answer_g").document.forms[0].elements[0];
    return rbs;
}

function rbuttonsMS(){
    if (document.forms[0] == null) {
        isFBmodeVAR = false;
        return new Object();
    }
    var rbs = new Object();
    rbs['a'] = document.forms[0].elements[0];
    if (!document.forms[1]) 
        return rbs;
    rbs['b'] = document.forms[1].elements[0];
    if (!document.forms[2]) 
        return rbs;
    rbs['c'] = document.forms[2].elements[0];
    if (!document.forms[3]) 
        return rbs;
    rbs['d'] = document.forms[3].elements[0];
    if (!document.forms[4]) 
        return rbs;
    rbs['e'] = document.forms[4].elements[0];
    if (!document.forms[5]) 
        return rbs;
    rbs['f'] = document.forms[5].elements[0];
    if (!document.forms[6]) 
        return rbs;
    rbs['g'] = document.forms[6].elements[0];
    return rbs;
}

function showsVisualFeedBack(){

}

//function compareRoundAnswer() {
//	errormargin = 1;
//	args = compareRoundAnswer.arguments;
//	uanswer = Number(args[0]);
//	tried =  Number(args[1]);
//	
//	delta = Math.abs(tried - uanswer);
//
//	if (delta<=errormargin)
//		return true;
//	else
//		return false;
//
//}

function correctAnswer(arg){
    var ua = getUserAnswer();
    showsVisualFeedBack();

//	alert("correctAnswer ::: "+ ua);

    if (ua == null) {
        getst("missingMsg").visibility = 'visible';
        getst("missingMsg").display = 'block';
        showProgressLocal();
        return
    }
    if (ua == trie()) {
        getst("correctMsg").visibility = 'visible';
        getst("correctMsg").display = 'block';
        showProgressLocal()
        return;
    }
    
    getst("incorrectMsg").visibility = 'visible';
    getst("incorrectMsg").display = 'block';
    showFeedback();
    showProgressLocal();
}
 
function hasNumericAnswer(){
    dsp = getsp("digitSpan");
    if (dsp) 
        return true;
    else 
        return false;
    
}

function localIsCommitted(){
    if (parent.isCommitted) 
        return parent.isCommitted();
    return false
}

function userAnswerChanged(){

    if (parent.isTestStatsReview) {
        return;
    }
    var ua;
    
    
    if (!localIsReviewMode()) {
    
        getst("correctMsg").visibility = "hidden";
        getst("incorrectMsg").visibility = "hidden";
        getst("missingMsg").visibility = "hidden";
    }
    
    if (hasNumericAnswer()) {
        storeCurrentAnswer();
        return;
    }
    var rbs = rbuttons();
    ua = getUserAnswer();
    
    if (!ua) {
        return;
    }
    for (key in rbs) {
        rbs[key].checked = false;
    }
    
    if (rbs[ua]) {
        rbs[ua].checked = true;
    }
    else {
        var i = 0;
        if (rbs['a']) 
            rbs['a'].checked = (ua.charAt(i++) == "1");
        if (rbs['b']) 
            rbs['b'].checked = (ua.charAt(i++) == "1");
        if (rbs['c']) 
            rbs['c'].checked = (ua.charAt(i++) == "1");
        if (rbs['d']) 
            rbs['d'].checked = (ua.charAt(i++) == "1");
        if (rbs['e']) 
            rbs['e'].checked = (ua.charAt(i++) == "1");
        if (rbs['f']) 
            rbs['f'].checked = (ua.charAt(i++) == "1");
        if (rbs['g']) 
            rbs['g'].checked = (ua.charAt(i++) == "1");
        
        isFBmodeVAR = false;
        
        NAF_SetValue(ua);
        cacheUserAnswer(ua);
    }
    
}
 
 
function getDocument(){
    if (document.all) 
        return document;
    else 
        if (document.digitSpan) 
            return document.digitSpan.document;
        else 
            return document;
}

function disableAnswerWidgets(){
    NAF_Disable();
}

function displayNumericAnswerField(mask){
    NAF_SetMask(mask);
    NAF_OnChange('userAnswerChanged()');
    NAF_Display();
}


var NAF_digits = 0;
var NAF_result = 0;
var NAF_mask = '1,111.11';
var NAF_value = '';
var NAF_isOpen = false;
var NAF_isDisabled = false;
var NAF_onChange = null;


function NAF_GetValue(){
    return (NAF_value == '' ? null : NAF_value);
}

function NAF_SetValue(newValue){
    NAF_value = (newValue == null ? '' : newValue);
    if (NAF_isOpen) 
        NAF_ShowValue();
    
}

function NAF_SetMask(newMask){
    NAF_mask = newMask;
}

function NAF_OnChange(str){
    NAF_onChange = str;
}

function NAF_Disable(){
    NAF_isDisabled = true;
    if (NAF_isOpen) 
        NAF_DisableDigits();
}

function NAF_Display() {

	var text = '';
		text += '<table id="answerWidget" name="answerWidget" border="0" cellpadding="0" cellspacing="0">';
		text += '<tr>';
		text += '<td nowrap>';
		text += '<font face="Arial"><b>';
		text += 'Answer =&nbsp;';
		text += '</b></font>';
		text += '</td>';
	var decimalPart = false;
	for (var i = 0; i < NAF_mask.length; i++) {
		var ch = NAF_mask.substr(i, 1);
		switch (ch) {
			case '9':
			case '1':
				text += '<td nowrap>';
				text += '<input type="text"';
//				if (document.all) 
					text += ' class="NEFinput"';
//				else
//					text += ' class="numericInputFieldNN"';
				text += ' name="NAF_Digit' + NAF_digits + '" id="NAF_Digit' + NAF_digits + '"';
				text += ' autocomplete="off" maxlength="1" size="1"';
				text += ' onfocus="select()"';
				if (isIE) 
					text += ' onkeydown="NAF_KeyDownIE(' + NAF_digits + ')"';
				text += '>';
				text += '</td>';
				NAF_digits++;
				break;
			case ' ':
				text += '<td nowrap>';
				text += '<font face="Arial"><b>&nbsp;';
				text += '</b></font>';
				text += '</td>';
				break;
			default:
				text += '<td nowrap>';
				text += '<font face="Arial"><b>';
				text += ch;
				text += '</b></font>';
				text += '</td>';
				break;
		}
	}
	
	text += '<td nowrap>&nbsp;&nbsp;</td>';
	text += '<td nowrap>';
	text += '<span id="AnswerButtons" name="AnswerButtons" class="answerButtonsSpan">';
	
	
 
	if (isAnswerButtonsVisible()) {
		
		text += '<a href="javascript:correctAnswer()" >';
		text += '<img name="AnswerButtonsImage" '
		
	 	+ ' src="../../../../../shared/images/correctanswer.gif" border="0">';
		
		
		
		text += '</a>';
	}
	text += '</span>';
	text += '</td>';
	text += '<td nowrap>&nbsp;</td>';
	text += '<td nowrap>';
	text += '<span id="AnswerButtons2" name="AnswerButtons2" class="answerButtonsSpan">';
	if (isAnswerButtonsVisible()) {
		text += '<a href="javascript:showAnswer()" >';
		text += '<img name="AnswerButtons2Image" ' 
		
		+
	 ' src="../../../../../shared/images/showanswer.gif" border="0">';
		
		
		text += '</a>';
	}
	text += '</span>';
	text += '</td>';
	text += '<td nowrap>&nbsp;</td>';
	text += '<td nowrap>';
	text += '<span name="AnswerButtons3" id="AnswerButtons3" class="answerButtonsSpan">';
	if (! localIsCommitted()) { 
		if (isAnswerButtonsVisible()) {
			text += '<a href="javascript:getNewInstance()" >';
			text += '<img name="AnswerButtons3Image" ' + ' src="../../../../../shared/images/tryagainup.gif" border="0">';
			text += '</a>';
		}
	}
	text += '</span>';
	text += '</td>';
	text += '</tr>';
	text += '</table>';
 
	text += '</span>'; 
	getDocument().write(text);
	setTimeout('NAF_PostOpen()', 1);
}

function NAF_PostOpen(){

    if (!document.all) {
        for (var i = 0; i < NAF_digits; i++) 
            NAF_GetDigit(i).onkeydown = NAF_KeyDownNN;
    }
    NAF_isOpen = true;
    NAF_ShowValue();
    if (NAF_isDisabled) 
        NAF_DisableDigits();
    
    
}

function NAF_GetDigit(index){
    return eval('getDocument().forms.digitForm.NAF_Digit' + index);
}

function NAF_CalculateValue(){
    NAF_value = '';
    var allEmpty = true;
    
    for (var i = 0; i < NAF_digits; i++) {
        var v = NAF_GetDigit(i).value;
        if (v == '') {
            NAF_value += '0';
        }
        else {
            allEmpty = false;
            NAF_value += v;
        }
    }
    
    if (allEmpty) {
        NAF_value = '';
        return;
    }
    
    
    
    
}

function NAF_ShowValue(){
    if (NAF_value == '') {
        for (var i = NAF_digits - 1; i >= 0; i--) 
            NAF_GetDigit(i).value = '';
    }
    else {
        for (var i = NAF_digits - 1, j = NAF_value.length - 1; i >= 0 && j >= 0; i--, j--) 
            NAF_GetDigit(i).value = NAF_value.substr(j, 1);
        for (var i = NAF_digits - NAF_value.length - 1; i >= 0; i--) 
            NAF_GetDigit(i).value = '0';
    }
}

function NAF_KeyDownIE(index){
    var keyCode = window.event.keyCode;
    var result = NAF_PrivateKeyDown(keyCode, index);
    if (result == false) 
        window.event.returnValue = false;
    return result;
}

function NAF_KeyDownNN(e){
    if (e.target.name.substr(0, 9) != 'NAF_Digit') 
        return false;
    var index = parseInt(e.target.name.substr(9), 10);
    if (index == NaN) 
        return false;
    var keyCode = e.which;
    return NAF_PrivateKeyDown(keyCode, index);
}

function NAF_PrivateKeyDown(keyCode, index){

    if (keyCode >= 48 && keyCode <= 57 || keyCode >= 96 && keyCode <= 105) {
        setTimeout('NAF_ValueChanged()', 1);
        var next = index + 1;
        NAF_SetFocusTo(next);
        return true;
    }
    
    if (keyCode == 8) {
        setTimeout('NAF_ValueChanged()', 1);
        NAF_SetFocusTo(index - 1);
        return true;
    }
    
    if (keyCode == 37) {
        NAF_SetFocusTo(index - 1);
        return true;
    }
    
    if (keyCode == 39) {
        NAF_SetFocusTo(index + 1);
        return true;
    }
    
    if (keyCode == 36) {
        NAF_SetFocusTo(0);
        return true;
    }
    
    if (keyCode == 35) {
        NAF_SetFocusTo(NAF_digits - 1);
        return true;
    }
    
    if (keyCode == 46) {
        setTimeout('NAF_ValueChanged()', 1);
        return true;
    }
    
    if (keyCode == 9 || keyCode == 13) 
        return true;
    return false;
}

function NAF_ValueChanged(){
    NAF_CalculateValue();
    if (NAF_onChange != '') 
        eval(NAF_onChange);
}

function NAF_DisableDigits(){
    for (var i = 0; i < NAF_digits; i++) 
        NAF_GetDigit(i).disabled = true;
}

function NAF_SetFocusTo(index){
    setTimeout('NAF_PrivateSetFocusTo(' + index + ')', 1);
}

function NAF_PrivateSetFocusTo(index){
    if (!NAF_isDisabled && 0 <= index && index < NAF_digits) 
        NAF_GetDigit(index).focus();
}




// ============= 2010/2 Protect your content by Panu 2 fixed sekection  =====
//document.onkeyup           = key2;
//document.onkeypress        = key2; 
// document.onselectstart     = function() {return false;}   

 document.oncontextmenu     = apuForSafari;
 document.onkeydown         = key2;
 
function key2(arg) 
  {
   var evt  = (window.event) ? event  : arg;
   var kid  = evt.keyCode ;
   
   if (evt.ctrlKey) {return false;}
     
 // alert(evt.modifiers);
  
   if (! document.all)
       { if (kid  == 17)  // ctrl 
	     {  return false;
	     }
       }
	  else
	  { if (evt.ctrlKey)
	    { return false; 
         }
      }
   return true;   	 
  } 
  
function apuForSafari(){return false; } 

