function formatInt(n) {
    var s = (n<0)?'-':'';var d = Math.abs( Math.floor(n) ).toString(); var o ="";
    while (d) {o=d.substring(d.length-3)+((o)?",":"")+o;d=d.substring(0,d.length-3);}
    return s+o;
}

// new functions --- 
function formatDouble(n) {
	var i = n.toString().indexOf(".");
	return formatInt(n) + ( (i!=-1)?n.toString().substring(i):"" );
}

// to be used with the OnKeyPress Event 
// ex. onKeyPress='return NumericEntryField(this,event,true,8);'
// defaults:  acceptDecimal = false, maxIntLength = 12
function NumericEntryField(fld,e,acceptDecimal,maxIntLength) {
    // INITIALIZE ---------------------------------------
    var strCheck = (acceptDecimal)?'-0123456789.':'-0123456789';
    var key = '';
	var formatFunction = (acceptDecimal)?formatDouble:formatInt;

    if (!maxIntLength) maxIntLength = 12;
    if (fld.readOnly == "true") return false;
    if (normalize(fld.value,strCheck).length>=maxIntLength) return false;  // enforce max length
    
    var whichCode = (window.Event) ? e.which : e.keyCode;
	
    key = String.fromCharCode(whichCode);  // Get key value from key code
    if (strCheck.indexOf(key) == -1)  { // check for valid keys
        if (whichCode == 8) { // check for backspace
            fld.value = formatFunction(normalize(fld.value, strCheck));
            if(typeof(OnFieldChange)=="function") OnFieldChange();
            return true;  // 
        }
        else {
            return false;
       }
    }
	// Check for sign: user press the - on the keyboard. 
	// fixed on 12/19-ea
	if (key == "-") { 
		fld.sign = (fld.sign)?null:1;
		key = "";
	}
	
	// check for decimal numbers...	
	fld.value = formatFunction((fld.sign?-1:1)*Math.abs(normalize(fld.value +""+key,strCheck))) + ((key == "." && fld.value.indexOf(".")==-1)?".":"");
    return false;
}


function normalize(n,permittedChars) {
    n = (n)?n+"":""; var o = "", strCheck = permittedChars, s;
    var i = n.length;
    while (i-->0) o=((permittedChars.indexOf(s=n.charAt(i))!=-1)?s:"")+o;
    return (o==""||o=="-")?0:o;
}


// Damon's original function plus some additions by Carlos

function formatString(aNumber,roundToPlaces,commas,symbol) {

	aNumber = (aNumber)?aNumber:this;
	roundToPlaces = (roundToPlaces>0)?roundToPlaces:0;
	commas = (commas=='undefined' || commas==false)?false:true;
	symbol = (symbol)?symbol:"$";
	
	theInput = Number(aNumber); // Cast the input to a number datatype.

	// Handle ticks
	if (symbol == "ticks") {
	
		theInput = theInput * 100; // Convert from percent
	
		aTick = 1/32; // Define a tick
		aHalfTick = 1/64; // Define a half-tick
		
		// Split ticks from integer part
		integerPart = Math.floor(theInput);
		tickPart = theInput - integerPart;
		
		theTicks = Math.round(tickPart / aTick) + "";
		
		// Figure-out if a half-tick is necessary
		
		theRemainder = tickPart - (theTicks * aTick);
		
		if ( theRemainder == aHalfTick ) {
			isHalfTick = "+";
		} else {
			isHalfTick = "";
		}
		
		
		// Pad zeros onto the front of the ticks part
		if ( theTicks.length == 1 ) {
			theTicks_display = "0" + theTicks;
		} else {
			theTicks_display = theTicks + "";
		}
		
		// Re-assemble tick string
		tick_string = integerPart + " - " + theTicks_display + isHalfTick;
	
		return tick_string;
	}

	// Modify inputs and commas if format is 'percent'.
	if (symbol == "%") {
		commas = false;
		theInput = theInput*100;
	}

	// Round to roundToPlaces
	multiplier = Math.pow(10,roundToPlaces);
	scaled2 = Math.round(multiplier*theInput);
	roundedNumber = (scaled2/multiplier) + ""; // Putting + "" forces Javascript to cast this var as a string.
	end = roundedNumber.indexOf(".");

	// Is the number negative? If so, compensate by removing the minus sign.
	if (roundedNumber<0) { 
		roundedNumber = roundedNumber * -1;
		isNegative = "-";
	} else {
		isNegative = "";
	}
	
	// Add commas
	displaynumber = roundedNumber + "";
	index = 1;
	revString = "";
	ltz_part = "";
	
	decimalIndex = displaynumber.indexOf(".");
	
	if (decimalIndex==-1) {
		end = displaynumber.length-1;
	} else {
		end = displaynumber.indexOf(".") - 1;
		ltz_part = displaynumber.substring(end+1,displaynumber.length);
	}
	
	if (commas==true) {
		for (i=end;i>-1;i--) {
			if (index==3) {
				revString = revString + displaynumber.substr(i,1) + ",";
				index = 1;
			} else {
				revString = revString + displaynumber.substr(i,1);
				index++;
			}
		}
	
		// Reverse the string
		fwdString = "";
		for (i=revString.length ; i>-1 ; i-- ) {
			fwdString = fwdString + revString.substr(i,1);
		}
	
		fwdString = fwdString + ltz_part;
	
		if (fwdString.substr(0,1)==",") {
			fwdString = fwdString.substr(1,fwdString.length);
		}
		
	} else {
		fwdString = displaynumber + "";
	}
	
	displaynumber = fwdString + "";
	
	// Find how many zeros need to be padded to the end of the number
	end = displaynumber.length;
	decimalIndex = displaynumber.indexOf(".");
	
	if (decimalIndex==-1) {
		zerosNeeded = roundToPlaces;
	} else {
		zerosNeeded = roundToPlaces-(end-decimalIndex)+1;
	}
	
	// Pad zeros to the end of the string
	padding = "";
	if ((zerosNeeded == roundToPlaces) && (roundToPlaces != 0)) {
		displaynumber = displaynumber + ".";
	}
	
	for (i=1; i<=zerosNeeded; i++) {
		padding = padding + "0";
	}
	
	displaynumber = displaynumber + padding;
	
	// Add currency or "%" sign
	if (symbol == "%") {
		displaynumber = isNegative + displaynumber + "%"; //&#037; \u00E0
	} else {
		displaynumber = isNegative + symbol + displaynumber;
	}
	
	// Add zero to beginning if necessary
	theIndexOfDot = displaynumber.indexOf(".");
	
	if (theIndexOfDot == 0) {
		displaynumber = "0" + displaynumber;
	}
	
	if (displaynumber.substr(0,2) == "-.") {
		displaynumber = "-0." + displaynumber.substr(2);
	}

	if (displaynumber.substr(0,2) == "$.") {
		displaynumber = "$0." + displaynumber.substr(2);
	}

	return displaynumber;
}

function setTextOfLayer_utils(objName,x,newText) { //v4.01
  if ((obj=findObj_utils(objName))!=null) with (obj)
    if (document.layers) {document.write(unescape(newText)); document.close();}
    else innerHTML = unescape(newText);
}

function findObj_utils(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}