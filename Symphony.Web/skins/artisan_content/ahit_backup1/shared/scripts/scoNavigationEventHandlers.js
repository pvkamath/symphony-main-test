// **************************************************
// File name : scoNavigationEventHandlers.js  
//
// These functions are called by the event handlers
// in the the Lesson navigation frame
// Implementation of these function is by Panu
// 
// **************************************************

function scoGuide(){
    openGuideBasic()
}

function scoRefresh(){
    parent.contentFrame.document.location = parent.contentFrame.document.location;
}

function scoPreferences(){

    //parent.preferencesButtonClicked ();
    openPreferencesBasic();
}



function scoExit(){
    parent.exitButtonClicked()
    
}

///////////////////////////////////////


function openPreferencesBasic(){
    // Copied from openScormGlossaryDef in scoPage.js
    // 
    var aPopupId = 'Preferences';
    var w = 600;
    var h = 700;
    var x = Math.round((screen.width - w) / 2);
    var y = Math.round((screen.height - h) / 2);
    
    h = 268;
    w = 358;
    
    var wname = 'Preferences';
    var url = '../html/preferences.htm';
    var features = "width=390,height=208,dependent=yes,directories=no,menubar=no,personalbar=no,status=no,toolbar=no,resizable=no,scrollbars=auto";
    
    
    if (window.popup && !window.popup.closed) 
        window.popup.close();
    
    if (document.all) {
        var features = "dialogLeft:" + x +
        "px;dialogTop:" +
        y +
        "px;dialogHeight:" +
        h +
        "px;dialogWidth:" +
        w +
        "px;dependent:yes;directories:no;menubar:no;personalbar:no;status:no;toolbar:no;resizable:yes;scroll:no;";
        
        window.showModalDialog(url, "", features);
        return;
    }
    else {
    }
    
    window.open(url, wname, features);
}




function openGuideBasic(){
    // Copied from openScormGlossaryDef in scoPage.js
    // 
    var aPopupId = 'Preferences';
    var w = 600;
    var h = 700;
    var x = Math.round((screen.width - w) / 2);
    var y = Math.round((screen.height - h) / 2);
    
    h = 268;
    w = 358;
    
    var wname = 'Guide';
    var url = '../html/lessonGuide.htm';
    var features = "width=480,height=520,scrollbars=yes";
    
    // 'toolbar=no,menubar=no,scrollbars=yes,width=650,height=400' ;
    
    window.open(url, wname, features);
}
