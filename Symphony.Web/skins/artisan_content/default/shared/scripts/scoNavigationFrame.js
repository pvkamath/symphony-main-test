/***********************************************************\
 **
 ** FILE: 		sco_00/scripts/mavigationFrame.js
 **
 ** WHAT:		Functions or wrappers called by
 **              navigationFrame.htm to implement
 **              WITHIN-SCO navigation.
 **
 ** WHY:			This is part of the Zoologics template
 **				for creating an SCO.
 **
 \********************************************************* */
window.offscreenBuffering = true;

function scoTOC(){
    // panu's redcefinition which for now shows
    // the classic toc since the hyperlinks on the
    // dynamic toc dont work yet.
    
    gotoTOC();
    
}


function gotoTOC(){
    if (!(GTW == null)) {
        GTW.close();
        GTW = null;
        setTimeout('gotoTOC()', 44);
        return;
    }
    var turl = parent.pageUrlAt(1);
    var winloc = parent.location.toString();
    var locparts = winloc.split('/');
    var fileName = locparts[locparts.length - 1];
    
    //	var dirUrl 	= winloc.replace(fileName, '');    
    var dirUrl = winloc.replace(fileName, 'toc.htm'); //  sco387:
    //	var newloc =  dirUrl + turl ;  
    var newloc = dirUrl; //  sco387: open the file toc.htm -panu
    var noOfLessons = parent.lessonUrlAt(0);
    var height = (noOfLessons * 22) + 55;
    var width = 484;
    
    var scrolls = "no";
    if (noOfLessons > 19) // else the toc won't fit the screen
    {
        height = 500;
        width = 499;
        scrolls = "yes";
    }
    
    // total screen dimensions
    var winW = (screen.width);
    var winH = (screen.height);
    
    //in IE
	if (isIE) {
        xOrigin = window.screenLeft;
        yOrigin = parent.frames['contentFrame'].self.screenTop;
        
        totalWidth = document.body.offsetWidth;
        totalHeight = parent.frames['contentFrame'].document.body.clientHeight;
        
    }
    else {
        // in NN
        xOrigin = window.screenX;
        yOrigin = window.screenY;
        
        totalWidth = window.outerWidth;
        totalHeight = window.outerHeight;
    }
    
    myleft = xOrigin + Math.round((totalWidth - width) / 2);
    mytop = yOrigin + Math.round((totalHeight - height) / 2);
    
    //		alert(	' xOrigin ::: ' + xOrigin + '\n' +
    //				' yOrigin ::: ' + yOrigin + '\n' +
    //				' totalWidth ::: ' + totalWidth + '\n' +
    //				' totalHeight ::: ' + totalHeight + '\n' +
    //				' width ::: ' + width + '\n' +
    //				' height ::: ' + height + '\n' +
    //				' myleft ::: ' + myleft + '\n' +
    //				' mytop ::: ' + mytop + '\n' 
    //				)
    
    // ************************************************************    
    // var myleft = window.screenLeft; 
    // var mytop  = window.screenTop - height - 70  ; // titlebar height 
    
    var features = 'dependent=yes,toolbar=no,resizable=no,menubar=no' +
    ',left=' +
    myleft +
    ',screenX=' +
    myleft +
    ',top=' +
    mytop +
    ',screenY=' +
    mytop +
    ',scrollbars=' +
    scrolls +
    ',' +
    'width=' +
    width +
    ',height=' +
    height;
    
    GTW = window.open(newloc, "gtw", features); // Old
    ontopIntervalHandle = GTW.setInterval("window.focus();", 50);
    GTW.document.onload = function(){
        window.setInterval("window.focus();", 50);
    };
    // *********************************************************************************************************

}

///////////////////////////////// sco 150: ///////

var GTW = null;
function notifyTocClosed(){
    GTW = null;
}

function navigationFrameLoaded(){ // gotoTOC();   sco160  now in scoState.js
}


function navigationFrameUnloaded(){
    if (!(GTW == null)) {
        if (!GTW.closed) {
            GTW.closeTOCbasic = null;
            GTW.close();
            GTW = null;
        }
    }
    
    parent.scoUnloaded(); // sco160.
}



////////////////////////////////////////////////////

function showCourseTitle(htmlString){
    // called by the frameset when it has loaded
    
    // oleg
    if (true) 
        return;
    
    
    var pattern = /index.htm/;
    var gurl = parent.document.location.toString().replace(pattern, 'images/coursetitle.gif');
    
    var html = '<img  height=24 src="' + gurl + '">';
    
    
    var st = getsp("courseTitleSpan");
    
    if (st.innerHTML) {
        st.innerHTML = html;
        
    }
    else {
    
        st.document.writeln(html);
        st.document.close();
    }
    
}




var hideStackVAR = new Array();
hideStackVAR[0] = 0; // top of stack;
function hideStack(){
    return hideStackVAR;
}


function hideStackIsEmpty(aString){
    var hs = hideStack();
    var toppen = hs[0];
    
    
    if (toppen <= 0) {
        return true;
    }
    
    return false;
}

function pushToHideStack(aString){
    var hs = hideStack();
    var toppen = hs[0];
    toppen = toppen + 1;
    hs[0] = toppen;
    hs[toppen] = aString;
    
    
}


function popHideStack(aString){
    var hs = hideStack();
    var toppen = hs[0];
    if (toppen == 0) 
        return;
    
    toppen = toppen - 1;
    hs[0] = toppen;
    if (toppen <= 0) {
        toggleNextAndPrevious();
    }
    
}


function outOfSequenceHyperlink(aString){

    if (hideStackIsEmpty()) {
    
        toggleNextAndPrevious();
        
    }
    else {
    };
    
    pushToHideStack(aString);
    
    //  setTimeout('hideNextAndPrevious ()' , 10000);

}

function go_back(){
    var aSco = parent.getSco();
    if (aSco == null) {
    }
    popHideStack();
    
    
    return aSco.goBack();
}


function toggleNextAndPrevious(){

    reverseVisibility('OnLayer'); //
    reverseVisibility('OffLayer'); // this lines had replaced all the code below to make it work with nn and ie
    //if(document.all) {
    //  if(  document.all.OnLayer.style.display == "none")
    //    {	document.all.OnLayer.style.display   = "inline";
    //		document.all.OffLayer.style.display  = "none";
    //	 
    //	}
    //	else 
    //    {	document.all.OnLayer.style.display   ="none";
    //		document.all.OffLayer.style.display ="inline";
    // 
    //	}
    //} else {
    //	// netscape implementation of switching visibility - carlos
    //	reverseVisibility('OnLayer');
    //	reverseVisibility('OffLayer');
    //}

}


function refresh(){
    parent.location = parent.location;
    
}



function gotoPageEntered(){
    checkboxClicked();
}

function checkboxClicked(){
    var pf;
    if (document.all) 
        pf = document.all.pageNoField;
    if (!pf) 
        pf = document.getElementById('mainForm').pageNoField;
    if (!pf) 
        pf = getsp('OnLayer').document.forms[0].pageNoField; // added the getsp('OnLayer') to make this work with netscape - carlos
    var targetPno = pf.value - 0;
    // if (targetPno < 0) return   ; 
    // if (targetPno ==  0) gotoTOC()
    
    if (targetPno <= 0) {
        alert("Invalid page-number: " + targetPno); // sco162.
        return;
    }
    
    
    targetPno = targetPno + 1; // #sco150. 
    loadAPage(targetPno);
}




/*************************************************\
 **
 ** FUNCTION(s): 	loadNextPage()   & loadPreviousPage()
 ** Called when the button is pressed.
 ** Forwards the call to the parent (i.e. the frameset).
 **
 \***/
function loadAPage(nurl){
    var aSco = parent.getSco();
    if (aSco == null) { // afer refresh
        return;
    }
    
    
    return aSco.loadThePage(nurl);
}



function loadNextPage(){
    var aSco = parent.getSco();
    if (aSco == null) { // afer refresh
        return;
    }
    return aSco.loadYourNextPage();
}


function loadPreviousPage(){

    // sco166:  The function newPageNo is missing from
    // carlos created pages like end-of-lesson
    // therefore tetst
    if (parent.contentFrame.newPageNo) {
        if (parent.contentFrame.newPageNo() <= 1) {
            alert('This is the first page!');
            return; //Carlos fix for not showing below the 1 page
        }
    }
    
    
    var aSco = parent.getSco();
    if (aSco == null) { // afer refresh
        return;
    }
    
    return aSco.loadYourPreviousPage();
}
