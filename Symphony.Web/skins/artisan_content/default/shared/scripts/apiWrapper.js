 
var		mAPIAdapter			= null;
var		kNotInitialized		= "301";

 
function LMSInitializeBasic(arg)        
//       ============= 
{ 
	alreadyInitializeVAR = true;

	var		retVal = "false";
	mAPIAdapter = getAPIAdapter();

//   may not be loaded yet
if(mAPIAdapter) 
{ retVal = mAPIAdapter.LMSInitialize("");
 }
 return retVal;
}



function isAPIAdapterValid()
{
 if ( mAPIAdapter == null)
  {   LMSInitializeBasic ()   // fixe3d
   } ; 

 return mAPIAdapter != null;
}

var alreadyInitializeVAR = false; 






function LMSInitialize(arg)
//       =============
{ 
	if (alreadyInitializeVAR) return;
	alreadyInitializeVAR = true;

	var		retVal = "false";
	mAPIAdapter = getAPIAdapter();
	if (mAPIAdapter)  // isAPIAdapterValid())
		{ retVal = mAPIAdapter.LMSInitialize("");
		} 
	return retVal;
}




function LMSFinish(arg)
//       =========
{
	var		retVal = "false";
	if (isAPIAdapterValid())
	{  
		retVal = mAPIAdapter.LMSFinish("");
 	}
	return retVal;
}


function LMSCommit()
//       ============
{
	var retVal = "false";
	if (isAPIAdapterValid())
		retVal = mAPIAdapter.LMSCommit("");
	return retVal;
}



function LMSGetDiagnostic(errorCode)
{
	var	retVal = "";
	if (isAPIAdapterValid())
		retVal = mAPIAdapter.LMSGetDiagnostic(errorCode);
		
	return retVal;
}


function LMSGetErrorString(errorCode)
{
	var		retVal = "";
	if (isAPIAdapterValid())
		retVal = mAPIAdapter.LMSGetErrorString(errorCode);
	return retVal;
}


function LMSGetLastError()
{	
	var		retVal = kNotInitialized;
 	 if (isAPIAdapterValid())
	 { retVal = mAPIAdapter.LMSGetLastError();
	   }
	return retVal;
}




function LMSGetValue(elementName)
//       ========================
{
	var		retVal = "";
	if (isAPIAdapterValid())
		retVal = mAPIAdapter.LMSGetValue(elementName);
	return retVal;
}


       
function LMSSetValue (elementName, elementValue)
//       ===========
{
	var		retVal = "false";
	if (isAPIAdapterValid())
	{	mAPIAdapter = getAPIAdapter();
	 	retVal = mAPIAdapter.LMSSetValue(elementName, elementValue);

// DEBUG
	//	alert("apiWrapper.js::LMSSetValue \n" +  	elementName  
	//	        + " -> \n"      +  elementValue 
	//			+ "\n\nAPI = "  +  mAPIAdapter );


	}
	return retVal;
} 

  



function findAPIAdapter(win)
{
	var		retVal = win.API;
	if (retVal == null)
	{
		var		parentWindow = win.parent;
		if (parentWindow == null || win == parentWindow)
			retVal = null;
		else
			retVal = findAPIAdapter(parentWindow);
	}
	return retVal;   // may be null
}


function getAPIAdapter()
{
	var		retVal = mAPIAdapter;
	
	if (retVal == null)
	{
		var		onErrorEvent = window.onerror;
		
		window.onerror = handleError;
		retVal = findAPIAdapter(window);
		
		if (retVal == null)
		{
			var		openerWindow = window.opener;
			if (openerWindow != null)
				retVal = findAPIAdapter(openerWindow);
		}
		 
		window.onerror = onErrorEvent;
	}
	
	return retVal;
}





 

function handleError(message, url, line)
{
	var		contentDomain = url.split("/");
	var		errorMessage = "Error " + LMSGetLastError() + "\n\n";
	
	errorMessage += "Unable to access the API Adapter.\n";
	errorMessage += "Tracking is unavailable.";
	
	alert(errorMessage);
	return true;
}


