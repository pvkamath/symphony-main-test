// **************************************************************
// file scoDynTOC.js
// includes all the code to create the TOC
// v 0.1
// Sept 23/2002


// page Object Constructor
function PageObject(pageName, pageURL, noExercises){
    var pageName = pageName || 'lesson page';
    var pageURL = pageURL || 'javascript:alert("no valid URL on page " + pageName)';
    var noExercise = noExercises || 0;
    var exerciseURL = exerciseLinkClicked('4')
}


//var navigationData = new Object();
// 

function GP_AdvOpenWindow(theURL, winName, features, popWidth, popHeight, winAlign, ignorelink, alwaysOnTop, autoCloseTime, borderless){ //v2.0
    var leftPos = 0, topPos = 0, autoCloseTimeoutHandle, ontopIntervalHandle, w = 480, h = 340;
    if (popWidth > 0) 
        features += (features.length > 0 ? ',' : '') + 'width=' + popWidth;
    if (popHeight > 0) 
        features += (features.length > 0 ? ',' : '') + 'height=' + popHeight;
    if (winAlign && winAlign != "" && popWidth > 0 && popHeight > 0) {
        if (document.all || document.layers || document.getElementById) {
            w = screen.availWidth;
            h = screen.availHeight;
        }
        if (winAlign.indexOf("center") != -1) {
            topPos = (h - popHeight) / 2;
            leftPos = (w - popWidth) / 2;
        }
        if (winAlign.indexOf("bottom") != -1) 
            topPos = h - popHeight;
        if (winAlign.indexOf("right") != -1) 
            leftPos = w - popWidth;
        if (winAlign.indexOf("left") != -1) 
            leftPos = 0;
        if (winAlign.indexOf("top") != -1) 
            topPos = 0;
        features += (features.length > 0 ? ',' : '') + 'top=' + topPos + ',left=' + leftPos;
    }
    if (document.all && borderless && borderless != "" && features.indexOf("fullscreen") != -1) 
        features += ",fullscreen=1";
    if (window["popupWindow"] == null) 
        window["popupWindow"] = new Array();
    var wp = popupWindow.length;
    popupWindow[wp] = window.open(theURL, winName, features);
    if (popupWindow[wp].opener == null) 
        popupWindow[wp].opener = self;
    if (document.all || document.layers || document.getElementById) {
        if (borderless && borderless != "") {
            popupWindow[wp].resizeTo(popWidth, popHeight);
            popupWindow[wp].moveTo(leftPos, topPos);
        }
        if (alwaysOnTop && alwaysOnTop != "") {
            ontopIntervalHandle = popupWindow[wp].setInterval("window.focus();", 50);
            popupWindow[wp].document.body.onload = function(){
                window.setInterval("window.focus();", 50);
            };
        }
        if (autoCloseTime && autoCloseTime > 0) {
            popupWindow[wp].document.body.onbeforeunload = function(){
                if (autoCloseTimeoutHandle) 
                    window.clearInterval(autoCloseTimeoutHandle);
                window.onbeforeunload = null;
            }
            autoCloseTimeoutHandle = window.setTimeout("popupWindow[" + wp + "].close()", autoCloseTime * 1000);
        }
        window.onbeforeunload = function(){
            for (var i = 0; i < popupWindow.length; i++) 
                popupWindow[i].close();
        };
    }
    document.MM_returnValue = (ignorelink && ignorelink != "") ? false : true;
}



function scoTOC(){
    //alert('showTOC() called!!!');
    GP_AdvOpenWindow('newTOC.htm', 'tocWindow', 'fullscreen=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no', 480, 400, 'center', 'ignoreLink', 'alwaysOnTop', 0, '');
    //openTOCPopUp();
    return document.MM_returnValue;
    
}


// scoInfo returns an array of the relative url of the page
// pageNoToIndexMap returns the index of the page to navigate to a certain page based on the
// page.exercise format




function scoTOC(){
    // panu's redcefinition which for now shows
    // the classic toc since the hyperlinks on the
    // dynamic toc dont work yet.
    
    gotoTOC();
    
}
