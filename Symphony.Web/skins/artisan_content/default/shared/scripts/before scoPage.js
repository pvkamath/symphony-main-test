/*************************************************\

 ISSUE for popups parent isnot the frameset.

** File:   shared/scripts/scoPage.js
** ------------------------------
** Functions to include on every (SCORM)
** CONTENT page:
  
**  1. Things needed by most pages.
**       1.1 Session data cache
**       1.2 Display-triggers like updating the
**            pagenumber field.
**       1.3  Hyperlink handlers. (  these more properly could
**             go to scoLesson.js, but dyks neede them too) 
**           
**  2. Semantically named wrappers that
**     use Scorm functions in the parent frameset
**     to communicate with the LMS. 
**      => Content Developers need not know 
**          about Scorm runtime-meta-data vocabulary.
**
**      POINT: ContentDevelopers should NEVER call LMSSetValue
**              directly.
**
\************************************************/ 
var pageTypeVAR;

function scoPageLoadedCOMMON
 (	idString		, 
	classString		,
	windowObject	,
	pageNoString
 )
{  

   
 pageTypeVAR = classString;

	pageIdIs			(idString)			;
 

	pageNoIs			(pageNoString)		;
 

	setPageNoField(pageNoString)			;
 
	finishPageDisplay	()					;
 
 if (parent.updateSessionTime)
    { parent.updateSessionTime ( ); 
    }
}

function pageType ()
{
return  pageTypeVAR;
}




var pageId_VAR; 
var pageNo_VAR;

function pageNumb ()			
{	return pageNo_VAR	;
}
function pageNoIs (aString) 
 {	 pageNo_VAR = aString	;
 }



function pageId ()			
{  
var pno = pageNumb();
var pindex = parent.pageNoToIndexMap() [ pno];

//	return pageId_VAR	;
// art ("pageId()   = " + pindex );

return pindex ;	
}




function pageIdIs (aString) 
 {
 	pageId_VAR = aString	;
 }



   
function zooCacheValue  ( keyString, valueString )
{ 
 parent.store(keyString, valueString);   
}
  
function zooCachedValue  ( keyString)
{ 
    result =  parent.load(keyString);
	     return result;
}



function setPageNoField(aString0)
{
 // points: 
 // 1. in netscape even though the function
 //    is in another frame, it executes in the 
 //     current context, so the span was not found.
 // 2. document.close must be called.
 // 3. The span needs an id to how up as a layer.

 if (! parent.navigationFrame ) 
   return;

// #sco74

var aString = aString0;
if (aString == '0') aString = 'n/a';

var pageField ;

 if (isms())
 { pageField =  parent.navigationFrame.document.all.pageNoField;
   pageField.value  = aString.toString() ; 
} 
 else
 {  
 pageField = parent.navigationFrame.document.forms[0].pageNoField ; 
    parent.navigationFrame.document.pageNoField ;
    pageField.value  = aString.toString() ; 
}
} 

function openHyperlink (aString)
{ 
	openScormLesson (aString);
}




function showGlossaryPopup (aString)
{ // called from within FLash
 openScormGlossaryDef (aString)
}


function openScormDYK (aString)
{ 
// #sco74

var wname	 =  aString.replace (/\./g , '_' );
var url		 = aString;
var features = 
     'toolbar=no,menubar=no,scrollbars=yes,width=650,height=400' ;
	 
 // window.open (url, "_self", features);
 window.open (url, wname, features);
}

function openScormGlossaryDef (aString)
{
var wname	 =  aString.toString().replace (/\./g , '_' );
var url		 = '../shared/glossary/' +  aString + '.htm' ;
var features = 
     'toolbar=no,menubar=no,scrollbars=yes,width=350,height=180,resizable=yes' ;
    
 window.open (url, wname, features);
}
 
   


function openScormLesson (aString)
{ 
if (true)
 { // #sco74
    openScormLessonInSameWindow (aString);
	return;
  }

var wname	 =  aString.replace (/\./g , '_' );
var url		 = aString;
var features = 
     'toolbar=no,menubar=no,scrollbars=yes,width=650,height=400' ;
	 
 // window.open (url, "_self", features);
 window.open (url, wname, features);
}
     


function openScormLessonInSameWindow (aString)
{ // #sco74
 var url		 = aString;
 parent.getSco().loadUrlInSameDir(url);

}
   


///////////////////////////////////////////////// 
//////  Wrappers for 6 of 8 SCorm functions: ////
///////////////////////////////////////////////// 

function  
LMSGetValue  (keyString)
//======================
{	var result =  parent.zooLMSGetValue( keyString);
	return result;
}


function  
LMSSetValue (keyString, valueString)
//===================================
{  parent.zooLMSSetValue(keyString, valueString); 
}


function  
LMSCommit ()
//==========
{   var result =  parent.zooLMSCommit() ;
	return result; 
}


function  
LMSGetLastError ()
{  var result = parent.zooLMSGetLastError();
   return result; 
}


function  
LMSGetErrorString ()
//===================
{  var result = parent.zooLMSGetErrorString();
   return result; 
}


function  
LMSGetDiagnostic ()
//=================
{  var result =  parent.zooLMSGetDiagnostic();
   return result; 
}


function  
LMSGetLastError ()
//================
{  var result = parent.zooLMSGetLastError();
   return result; 
}

