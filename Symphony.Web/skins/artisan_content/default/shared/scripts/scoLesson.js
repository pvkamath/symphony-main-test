

function scoPageLoaded(idString, classString, windowObject, pageNoString, pageTitle){
    scoPageLoadedCOMMON(idString, classString, windowObject, pageNoString, pageTitle);
    if (parent.markLessonVisited) {
        parent.markLessonVisited(idString);
    }
    
    if (parent.topNavigationFrame) {
        showProgressLocal();
    }
    
}


function showProgressLocal(){
    if (parent.contentFrame) {
        if (parent.contentFrame.doNotShowProgressFLAG) {
            parent.topNavigationFrame.hideProgress();
            return;
        }
    }
    
    if (parent.topNavigationFrame.showProgress) {
        var stats = parent.calculateScoreStats();
        var corrects = stats[0];
        var wrongs = stats[1];
        var noAnswers = stats[2];
        var visitedPages = parent.visitedPages();
        var exercises = corrects + wrongs + noAnswers;
        
        ///////////////////////////////////////////////
        var score;
        if (exercises == 0) { // NO LONGER:  return;
            score = -1;
        }
        else {
            score = corrects / (corrects + wrongs + noAnswers);
        }
        /////////////////////////////////////////////////////
        
        
        parent.topNavigationFrame.showProgress(score, visitedPages);
    }
}

function finishPageDisplay(){
    setTimeout('mathPaneInit()', 45);
}

function mathPaneInit(){
    if (isIE) {
        var thing = 'try {initPage()} catch (e) { }'
        eval(thing);
    }
    
    else {
        if (parent.frames[1].initPage) {
            initPage();
        }
    }
}
