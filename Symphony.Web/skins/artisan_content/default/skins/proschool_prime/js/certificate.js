﻿(function(){
    var currentTime = new Date();
    window.config = {
	    score: "",
	    name: "",
	    course: "",
	    type: "",
	    month: currentTime.getMonth() + 1,
	    day: currentTime.getDate(),
	    year: currentTime.getFullYear()
    };
    if (window.location.search.indexOf("?") > -1) { 
	    //addr_str = document.URL.substring(is_input+1, document.URL.length);
	    var params = window.location.search.substring(1).split("&");
	    for(var i = 0; i< params.length; i++){
		    param = params[i];
		    var pair = param.split("=");
		    var key = pair[0];
		    var value = pair[1];
		    if(key == "score"){
			    //special handling for the score
			    value = (parseInt(Math.round(parseFloat(value))) || -1);
		        if (value == -1) {
		            value = pair[1];
		        } else {
		            value = value + '%';
		        }
		    }
		    config[key] = unescape(value);
	    };
    }
    if(!config.date){
	    config.date = config.month + "/" + config.day + "/" + config.year;
    }
})();