
function markLessonVisited(pid0){
    pid = currentPindex();
    
    if (uAnswersAt(pid) == 'LESSON') {
        return
    }
    
    if (isTest()) { 
		// fix for losing one answer in test-review
        // when looking at a correctly answered question
        // and then going to the review-mode
        return;
    }
    
    
    
    uAnswersAtPut(pid, 'LESSON');
    uploadData();
    
    return true;
}


// FIXED for KPMG use so that
// an url-parameter can now be a URL
// without breaking the parsing of it.

var isReviewMode_VAR = false;
var THESCO;
var theWINDOW;
var sco_VAR;
var answeredQuestionsVAR = 0;
var tempRandomSeedVAR;



var hist_VAR = new Array();
hist_VAR[0] = 0;
var absUrl_VAR = "";

function scoHomeUrl(){
    var cfWindow = contentFrame;
    var winloc1 = cfWindow.parent.location.toString();
    var winloc = (winloc1.split('?'))[0];
    var result1 = filenameFromURL(winloc);
    var result2 = winloc.replace("/" + result1, "");
    return result2;
}

function contentFrameHomeUrl(){
    var cfWindow = contentFrame;
    var winloc = cfWindow.location.toString();
    var result1 = filenameFromURL(winloc);
    var result2 = winloc.replace("/" + result1, "");
    return result2;
}

function openRelativeLink(nurl){




    addToHistory();
    var newloc = nurl;
    var scoHome = contentFrameHomeUrl();
    newloc = scoHome + "/" + nurl;
    contentFrame.location = newloc;
    
}

function loadCF(nurl){
//	alert("loadCF");
    var newloc = nurl;
    var scoHome = scoHomeUrl();
    if (nurl.match(/\/\//)) {
    }
    else {
        newloc = scoHome + "/" + nurl;
    }
    
    if (nurl.match("/scoOver.htm")) {
        contentFrame.parent.location = newloc;
    }
    else {
        contentFrame.location = newloc;
    }
    
    
    var isTestVAR = this.isTest();
    if (isTestVAR) 
        return newloc;
    
    
    
    var fileName = filenameFromURL(nurl);
    var uar = getSco().getUrlArray();
    
    
    
    for (i = 1; i < uar.length; i++) {
    
        var bool = escape(uar[i]).match(escape(fileName));
        
        if (bool) {
            pageIndex_VAR = i;
            currentPageIndexIs(i);
            return newloc;
        }
    }
    return newloc;
}

function loadCFH(nurl){
    addToHistory();
    return loadCF(nurl);
}

function loadUrlInWindowNOH(nurl, targetWindow){
    return loadCF(nurl);
}

function loadUrlInSameDir(nurl){
    return loadCFH(nurl);
}

function loadUrlInSameDirNoHISTORY(nurl){
    return loadCF(nurl);
}

function loadUrlInWindow(nurl, targetWindow){
    return loadCFH(nurl);
}



function addToHistory(nurl2){
    var nurl = contentFrame.location.toString();
    if (nurl.match(/\/shared\//i)) {
        return;
    }
    var h = hist_VAR;
    var oldStackPointer = h[0];
    var newStackPointer = oldStackPointer + 1;
    var oldTopUrl = h[oldStackPointer];
    var newTopUrl = nurl;
    
    if (oldTopUrl == newTopUrl) {
    }
    else {
        h[0] = newStackPointer;
        h[newStackPointer] = nurl;
    }
}


function filenameFromURL(winloc){

    // KPMG first take the args out completely
    // because they might now be a URL.
    
    var locparts0 = winloc.split('?');
    var locparts1 = locparts0[0];
    // var locparts = winloc.split('/'); 
    var locparts = locparts1.split('/');
    
    var fileName = locparts[locparts.length - 1];
    
    if (fileName.indexOf('?') >= 0) {
        var parts = fileName.split('\?');
        return parts[0];
    }
    return fileName;
}


function visitedPages(){
    var visitedPages = 0;
    var allPages = 0;
    var uas = uAnswersPRIVATE();
    for (k in uas) {
        allPages++;
        var givenAnswer = uas[k];
        if (givenAnswer == "") {
        }
        else {
            visitedPages++
        }
    }
    allPages--;
    return [visitedPages, allPages];
}



function calculateScoreStats(){
    var visitedLessonPages = 0;
    var rights;
    var score = 0;
    var answers = 0;
    var uas = uAnswersPRIVATE();
    var cas = caDict();
    aqsReset();
    
    var noqs = getNumberOfQuestions();
    var wrongAnswers = 0;
    var correctAnswers = 0;
    for (k in uas) {
        var givenAnswer = uas[k];
        var hasAnswer = true;
        
        if (givenAnswer == 'LESSON') {
            hasAnswer = false;
            visitedLessonPages++;
        }
        
        if (givenAnswer == 'EXERCISE') 
            hasAnswer = false;
        if (givenAnswer == '') 
            hasAnswer = false;
        if (hasAnswer) {
            aqsInc();
            var correctAnswer = cas[k];
            
            if (givenAnswer == correctAnswer) {
                if (!givenAnswer == "") {
                    score++;
                    correctAnswers++;
                }
                else {
                    wrongAnswers++
                }
            }
            else {
                wrongAnswers++
            }
        }
    }
    var noAnswers = noqs - correctAnswers - wrongAnswers;
    return [correctAnswers, wrongAnswers, noAnswers, visitedLessonPages];
}




function lastPageVisited(){
    var def = '1';
    def = '2';
    
    def = '2';
    var arg = null;
    
    var docloc = document.location.toString().split('?');
    if (docloc.length > 1) {
        arg = docloc[1];
        var str = (arg.match(/pindex=.*/)).toString();
        var args = str.split("&");
        var str2 = args[0];
        var strs3 = str2.split("=");
        def = strs3[1];
        return def - 2 + 2 + 1;
        
    }
    var val = zooLMSGetValue('cmi.core.lesson_location');
    if (val == null) 
        return def;
    if (val == "") 
        return def;
    return val;
}

function currentPindex(){
    if (currentPageIndexVAR) 
        return currentPageIndexVAR;
    var loc = contentFrame.location.toString();
    var splits = loc.split(/\//);
    var pid = splits[splits.length - 1];
    var scoi = scoInfo();
    for (p in scoi) {
        var val = scoi[p];
        if (val == "") 
            val = null;
        if (val) {
            if (pid == val) {
                return p;
            }
        }
    }
    return currentPageIndexVAR;
}


function closeUnlessIntranet(){
    if (true) 
        return false;
    
    var loc = "";
    if (window.top.opener) {
        loc = window.top.opener.location.toString();
        if (loc.match('\/x\/launch_main_')) {
            return false;
        }
    }
    setTimeout("window.top.close()", 1000);
}


function exitSCOzBasic(anSCO){
    var fset = theWINDOW.parent;
    toppen()["emergencyExit"] = false;
    
    /* *********************************
     To fix NT problem take out all this:
     
     if (window.opener)
     { if (window.opener.top != window.top)
     { closeUnlessIntranet();
     
     }
     }
     if (window.top.opener)
     { if (window.top.opener != window.top)
     { closeUnlessIntranet();
     }
     }
     if (window.parent.opener)
     { var matc =
     window.parent.location.toString().match('/spido/');
     
     if ((window.parent.opener.top != window.top)
     && (matc == null))
     { closeUnlessIntranet();
     }
     }
     ************************************* */
    var oldloc = window.location.toString();
    var oldFileName = anSCO.filenameFromURL(oldloc);
    var nurli = "../shared/html/scoOver.htm";
    var newloc = oldloc.replace(oldFileName, nurli);
    
    window.location = newloc;
}




function closeit(time){
    if (time == null) 
        time = 1100;
    if (window.opener) {
    
    
        if (false) {
            setTimeout("window.top.close()", time);
            return true;
        }
    }
    else {
        if (window.parent.opener) {
            var matc = window.parent.location.toString().match('/spido/main.html');
            
            
            
            if (false) {
                setTimeout("window.top.close()", time);
                return true;
            }
        }
    }
    return false;
}




function toppen(){
    var toppen = window.top;
    if (window.opener) 
        toppen = window.opener.top;
    return toppen;
}

function isEmergencyExit(){
    return toppen()["emergencyExit"];
}

function scoLoaded(aWindow){
    toppen()["emergencyExit"] = true;
    
    theWINDOW = aWindow;
    if (THESCO == null) {
        THESCO = newSco(aWindow, scoInfo());
    }
    
    var msg = scoCustomInitialize();
    if (msg) {
        if (msg == "") 
            return;
        alert(msg);
        return
    }
    zooLMSInitialize();
    var lastPageIndex = lastPageVisited();
    
    var aa = getAPIAdapter();
    if (aa) {
        if (aa.zooPageIndex123) {
            lastPageIndex = aa.zooPageIndex123;
        }
    }
    
    THESCO.loadThePageAtIndex(lastPageIndex);
    displayCourseTitle();
}





function getSco(){
    if (THESCO == null) {
        if (this.loadFirstPage) 
            this.loadFirstPage();
    }
    return THESCO;
}








var isFirstPageCPI = true;
function currentPageIndexIs(anInteger){
    currentPageIndexVAR = anInteger - 1 + 1;
    if (isFirstPageCPI) {
        isFirstPageCPI = false
    }
    else {
    
    
    }
}

function scoUnloaded(){
    if (FINISHED) {
        return;
    }
    FINISHED = true;
    zooUpdateSessionTimeFINISH();
    if (isTest()) {
        zooLMSFinish();
        return;
    }
    scoSubmitExit();
    zooLMSFinish();
}


function exitSCOz(anSCO){
    var error = scoCustomFinish();
    if (error) {
        if (error == "") 
            return;
        alert(error);
        return;
    }
    var score = calculateScore();
    if (!confirmExit(anSCO, score)) 
        return false;
    exitSCOzBasic(anSCO);
}

function noOfInstancesMax(){
    return 10;
}

var exercisesOfLesson_CACHE = new Object();
function exercisesOfLesson(pnoString){
    var cached = exercisesOfLesson_CACHE[pnoString];
    if (cached) {
        return cached;
    }
    var pages = pageNoToIndexMap();
    var lessonIndex = pages[pnoString];
    if (lessonIndex == null) 
        return null;
    var result = new Object();
    
    for (i = 1; i < 11; i++) {
        var epno = pnoString + '.' + i;
        var eindex = pages[epno];
        if (eindex == null) {
            exercisesOfLesson_CACHE[pnoString] = result;
            return result
        }
        result[eindex] = epno;
    }
    exercisesOfLesson_CACHE[pnoString] = result;
    return result;
}



var FINISHED = false;
function cleanupTestExit(){
    if (FINISHED == true) 
        return;
    scoUnloaded();
}

function currentPageIndexIsBASIC(anInteger){
    currentPageIndexVAR = anInteger - 1 + 1;
    
}

var currentPageIndexVAR = 0;
function setPageNoToState(){
}


function exitButtonClicked(){
    var theSco = getSco();
    exitSCOz(theSco)
}

function confirmExit(anSCO, aNumber){
    return true
}

function confirmExitThorough(anSCO, aNumber){

    var qs = ' questions.'
    if (aqs() == 1) 
        qs = ' question.'
    var msg1 = "You have answered " +
    aqs() +
    qs +
    "\n\nDo you want to commit your" +
    "\nanswers and exit the \'" +
    anSCO +
    "\' ?";
    if (isCommitted()) 
        msg1 = "You answered and committed " +
        aqs() +
        "\nquestions " +
        "and got " +
        aNumber +
        " right!" +
        "\n\nDo you want to exit ?";
    var msg2 = "You got " + aNumber +
    " questions right!";
    if (!confirm(msg1)) {
        return false;
    }
    if (!isCommitted()) 
        alert(msg2);
    return true;
}

function isTest(){
    return false;
}

function alertFirstPage(titleString){
}

function setLmsRandomSeed(aFloat){
    tempRandomSeedVAR = aFloat;
}

function persistRandomSeed(){
    var rsFloat = tempRandomSeedVAR;
    scoPropertySet('#randomSeed', rsFloat);
}

function getLmsRandomSeed(){
    var rsFloat = scoPropertyGet('#randomSeed');
    setLmsRandomSeed(rsFloat);
    return rsFloat;
}

function scoSubmit(){
//	alert("scoSubmit");
    persistRandomSeed();
    commitAnswers();
}

function scoSubmitExit(){


    commitAnswers();
}

function scoPropertySet(key, value){
    var pd = propsDict();
    pd[key] = value
}

function scoPropertyGet(key){
    var pd = propsDict();
    var value = pd[key];
    return value
}

function currentPageIndex(){
    return currentPageIndexVAR
}

function getCourseTitle(){
    return courseTitle()
}

function commitAnswers(){
    var nzedScore = calculateNormalizedScore();
    var status = calculateStatus();
    sendDataToLMS(nzedScore, status);
}


function displayCourseTitle(){
    var ct = getCourseTitle();
    ct = '<font size="-1" face="tahoma"><b> ' + ct + '</b></font>';
    navigationFrame.showCourseTitle(ct);
}

function hasExercisesBeenVisited(pageNumber){

    var exes = exercisesOfLesson(pageNumber);
    if (exes == null) 
        return false;
    var some = false;
    for (eIndex in exes) {
        if (uAnswersAt(eIndex) == null) 
            return some;
        if (uAnswersAt(eIndex) == "") 
            return some;
        some = true;
    }
    return null;
}

function hasPageBeenVisited(pindex){
    if (pindex == null) 
        return false;
    var mark = uAnswersAt(pindex.toString());
    
    if (mark == null) {
        return false;
    }
    if (mark == "") {
        return false;
    }
    return true;
}

function hasEveryPageBeenVisited(){
    var lastPageIndex = scoInfo().length - 1;
    for (pin = 2; pin <= lastPageIndex; pin++) {
        var val = uAnswersAt(pin);
        if (val == null) 
            return false;
        if (val == "") 
            return false;
    }
    return true;
}

function hasEveryExerciseBeenAnswered(){


    var lastPageIndex = scoInfo().length - 1;
    for (pin = 1; pin <= lastPageIndex; pin++) {
        if (uAnswersAt(pin) == 'EXERCISE') 
            return false;
    }
    return true;
}

function calculateStatus(){
    if (contentFrame.customState) {
        var custom = contentFrame.customState();
        if (custom) 
            return custom;
    }
    var completed = hasEveryPageBeenVisited();
    var c2 = hasEveryExerciseBeenAnswered();
    var sc = calculateNormalizedScore();
    var rsc = requiredScore()
    if (rsc == 0) {
        if (completed) 
            return 'completed';
        return 'incomplete'
    }
    
    if (completed) {
        return 'completed'
    }
    else {
        return 'incomplete'
    }
    if (sc >= rsc) 
        return 'passed';
    return 'failed';
}

function calculateNormalizedScore(){
    var sc = calculateScore();
    var msc = maxScore();
    
    var nsc = Math.round(sc / msc * 100);
    return nsc;
}



function testPropsDict(){
    if (true) 
        return;
    var pd = propsDict();
    var nam = prompt("Your name this time around?", pd['nameThisTime']);
    pd['nameThisTime'] = nam;
    pd['nameThisTime2'] = nam;
    pd['dated'] = (new Date()).toString();
}

function restoreInstanceIds(){
    var dicts = restoreDicts();
    restoreInstanceIdsBASIC();
}

function restoreInstanceIdsBASIC(){
    var pts = noOfInstances();
    for (i = 1; i < scoInfo().length; i++) {
        if (pts[i] > 1) {
            var already = ixDict()[i];
            
            if (already) {
            
            
            
                prind();
            }
            else {
                ixDict()[i] = prind();
                
            }
            
        }
    }
    for (k in ixDict()) {
        modifyInstanceId(ixDict()[k], k)
    }
}

var lastpRind;


function prind(){
    var rfloat = (Math.random()) * 100;
    
    var maxi = noOfInstancesMax();
    rfloat = (Math.random()) * maxi;
    var rindex = Math.round(rfloat + 0.5);
    var pad = rindex.toString() + "";
    var res = "";
    if (pad.length == 3) {
        res = '0' + pad;
    }
    if (pad.length == 2) {
        res = '00' + pad;
    }
    if (pad.length == 1) {
        res = '000' + pad;
    }
    if (res == lastpRind) {
        return (rind());
    }
    
    return res;
}

function modifyInstanceId(insidString, pidString){
    if (insidString == "") 
        return;
    if (insidString == null) 
        return;
    var id = pidString;
    var pindex = id;
    var scoi = scoInfo();
    var oldUrl = scoi[pindex];
    if (!oldUrl) 
        return;
    var ipart = oldUrl.match(/i0\S*\//);
    var newUrl = oldUrl.replace(/\/i\S*\//, "/i" + insidString + "/");
    
    scoInfo()[pindex] = newUrl;
}

function isTest(){
    var typen = 'lesson';
    if (scoType) {
        typen = scoType();
    }
    if (typen == 'TestSco') 
        return true;
    if (typen == 'test') 
        return true;
    
    return false;
}

function isCommitted(){
    if (!isTest()) {
        return false;
    }
    var status = scoStatus_persistence();
    if (status == "completed") {
        return true;
    }
    return false;
}

function isReviewMode(){
    return isReviewMode_VAR;
}

function setReviewMode(){
    isReviewMode_VAR = true;
    
}

function markExerciseVisited(pid){



    var oldValue = uAnswersAt(pid);
    if (oldValue) {
        return false;
    }
    uAnswersAtPut(pid, 'EXERCISE');
    uploadData();
    
    return true;
}


function pindexOfPid(pid){

    var scoi = scoInfo();
    for (p in scoi) {
        if (!scoi[p] == null) {
            var val = scoi[p].replace('.htm', '');
            ;
            
            if (pid.match(val)) {
                return p;
            }
        }
    }
    
    return null;
}

function calculateScore(){
    var rights;
    var score = 0;
    var answers = 0;
    var uas = uAnswersPRIVATE();
    var cas = caDict();
    aqsReset();
    for (k in uas) {
        var givenAnswer = uas[k];
        var hasAnswer = true;
        
        if (givenAnswer == 'LESSON') 
            hasAnswer = false;
        if (givenAnswer == 'EXERCISE') 
            hasAnswer = false;
        if (givenAnswer == '') 
            hasAnswer = false;
        if (hasAnswer) {
            aqsInc();
            var correctAnswer = cas[k];
            
            if (givenAnswer == correctAnswer) {
                if (!givenAnswer == "") {
                    score++;
                }
            }
        }
    }
    return score;
}

function uploadData(anSCO){
}

function exitScoWithoutConfirmation(anSCO){




}

function exitSCOzBasic2(anSCO){
    var fset = theWINDOW.parent;
    var nurli = "../shared/html/scoOver.htm";
}


function newSco(aWindowARG, urlArray){
    var sco = new Sco_class(aWindowARG, urlArray, urlArray[0]);
    setSco(sco);
    return sco;
}

function setSco(anSco){
    THESCO = anSco;
}

function isLearned(arg){
    return false;
}






function Sco_class(aWindowARG, urlArray, titleString){
    var theWindow = aWindowARG;
    var urlArray_VAR = urlArray;
    var title_VAR = titleString;
    var pageIndex_VAR = 0;
    var isStatic = false;
    
    
    
    
    function hPos(posInt){
        var h = hist_VAR;
        var toppen = h[0];
        if (posInt == null) 
            return toppen;
        h[0] = posInt;
        return posInt;
    }
    function nextUrl(){
        var pix = currentPageIndex();
        if (isStatic) 
            return nextUrlX();
        var noOfElements = this.getUrlArray().length - 1;
        if (pix == noOfElements) {
            return null;
        }
        
        currentPageIndexIs(pix + 1);
        pageIndex_VAR = currentPageIndex();
        
        var nurl = this.getUrlArray()[currentPageIndex()];
        if (isLearned(nurl)) {
            return this.nextUrl();
        }
        
        return nurl
    }
    function previousUrl(){
        var pix = currentPageIndex();
        if (isStatic) 
            return previousUrlX();
        var limit = 2;
        if (isTest()) 
            limit = 1;
        var cid = currentPageIndex();
        if (cid <= limit) {
            return null;
        }
        if (pix <= limit) {
            if (!(currentPageIndex() == pageIndex_VAR)) {
                alert("This is the first page you haven't learned yet." +
                "\n\nIf you want to see a page you already learned," +
                "\nenter its page-number in the page-number -field " +
                "\non the bottom right corner of the window!");
                pageIndex_VAR = currentPageIndex();
            }
            return null;
        }
        
        
        pageIndex_VAR--;
        currentPageIndexIs(pix - 1);
        pageIndex_VAR = currentPageIndex();
        
        var nurl = this.getUrlArray()[currentPageIndex()];
        if (isLearned(nurl)) {
            return this.previousUrl();
        }
        return nurl;
    }
    function nextUrlX(){
        var noOfElements = this.getUrlArray().length - 1;
        if (pageIndex_VAR == noOfElements) {
            return null;
        }
        pageIndex_VAR++;
        return this.getUrlArray()[pageIndex_VAR];
    }
    function previousUrlX(){
        var limit = 2;
        if (isTest()) 
            limit = 1;
        var cid = currentPageIndex();
        if (cid <= limit) {
            return null;
        }
        return this.getUrlArray()[cid - 1];
    }
    
    
    function getUrlArray(){
        return (scoInfo());
    }
    
    
    
    
    function convert(aString){
        if (aString == null) 
            return 0;
        
        var parts = aString.toString().split('.');
        var whole = parts[0];
        var dec = parts[1];
        if (dec == null) 
            return (whole - 1);
        return (whole - 1) + "." + dec;
    }
    
    function loadThePage(pnoString){
        var index = pageNoToIndexMap()[pnoString];
        var nurl = scoInfo()[index];
        if (!nurl) {
            alert("Invalid page-number: " +
            convert(pnoString));
            return
        }
        pageIndex_VAR = index;
        currentPageIndexIs(index);
        this.loadUrlInSameDir(nurl)
    }
    var endOfLessonIndex = 'not yet nothing';
    
    
    
    function myTitle(){
        return title_VAR;
    }
    
    
    function toString(){
        return ("" + title_VAR);
    }
    
    function currentUrl(){
        var index = this.getUrlArray()[0];
        var noOfElements = this.getUrlArray().length - 1;
        return this.getUrlArray()[index];
    }
    function firstUrl(){
        pageIndex_VAR = 1;
        return this.getUrlArray()[pageIndex_VAR];
    }
    
    
    
    function filenameFromWindow(aWindow){
        var winloc = aWindow.location.toString();
        return filenameFromURL(winloc);
    }
    function loadFirstPage(){
        var nurl = this.firstUrl();
        var result = this.loadUrlInSameDir(nurl);
        
        return result;
    }
    
    
    function loadYourNextPage(){
    
        addToHistory();
        
        if (currentPageIndex() == endOfLessonIndex) 
            return;
        var nurl = this.nextUrl();
        if (nurl == null) {
            this.loadUrlInSameDir("../shared/html/scoEndLesson.htm");
            currentPageIndexIsBASIC(currentPageIndex() + 1);
            
            endOfLessonIndex = currentPageIndex();
            addToHistory("../shared/html/scoEndLesson.htm");
            if (contentFrame.setPageNoFieldBasic) 
                contentFrame.setPageNoFieldBasic("");
            return;
        }
        
        currentPageIndexIsBASIC(currentPageIndex() + 1);
        var result = this.loadUrlInSameDirNoHISTORY(nurl);
        this.markUrlAsCurrent(nurl);
    }
    
    
    
    function loadYourPreviousPage(){
        addToHistory();
        var nurl = this.previousUrl();
        if (nurl == null) {
            alertFirstPage();
            return;
        }
        currentPageIndexIsBASIC(currentPageIndex() - 1);
        this.loadUrlInSameDirNoHISTORY(nurl);
        this.markUrlAsCurrent(nurl);
    }
    
    
    
    
    function goBack(){
        if (isTest()) {
            goBackInTest();
            return;
        };
        var h = hist_VAR;
        var toppen = h[0];
        if (toppen < 1) {
            alert("End of history-list!");
            return;
        }
        var goBackUrl = h[toppen];
        h[0] = h[0] - 1;
        
        if (contentFrame.location.toString() ==
        goBackUrl) {
            return this.goBack();
        }
        else {
        }
        
        this.loadUrlInSameDirNoHISTORY(goBackUrl);
    }
    
    
    
    
    function markUrlAsCurrent(relativeURL){
        var uar = this.getUrlArray();
        for (i = 1; i < uar.length; i++) {
            if (uar[i] == relativeURL) {
                pageIndex_VAR = i;
                currentPageIndexIs(i);
                return;
            }
        }
    }
    
    
    function goBackBASIC(){
        var h = hist_VAR;
        var toppen = h[0];
        var goBackUrl = h[toppen - 1];
        h[0] = h[0] - 1;
        loadUrlInSameDirNoHISTORY(goBackUrl);
        
        var uar = this.getUrlArray();
        for (i = 1; i < uar.length; i++) {
            if (uar[i] == goBackUrl) {
                pageIndex_VAR = i;
                return
            }
        }
    }
    
    
    
    
    function loadThePageAtIndex(anInteger){
        var index = anInteger;
        
        if (index >= scoInfo().length) 
            index = scoInfo().length - 1;
        
        
        var nurl = scoInfo()[index];
        if (!nurl) {
            alert("loadThePageAtIndex::Invalid page-index: " + anInteger);
            return
        }
        this.loadUrlInSameDir(nurl)
    }
    this.hPos = hPos;
    this.hist_VAR = hist_VAR;
    this.loadThePageAtIndex = loadThePageAtIndex;
    this.markUrlAsCurrent = markUrlAsCurrent;
    this.loadUrlInWindow = loadUrlInWindow;
    this.goBack = goBack;
    this.loadThePage = loadThePage;
    this.loadFirstPage = loadFirstPage;
    this.loadYourNextPage = loadYourNextPage;
    this.loadYourPreviousPage = loadYourPreviousPage;
    this.loadUrlInSameDir = loadUrlInSameDir;
    this.loadUrlInSameDirNoHISTORY = loadUrlInSameDirNoHISTORY;
    this.getUrlArray = getUrlArray;
    this.urlArray_VAR = urlArray;
    this.title_VAR = titleString;
    this.myTitle = myTitle;
    this.toString = toString;
    this.currentUrl = currentUrl;
    this.nextUrl = nextUrl;
    this.previousUrl = previousUrl;
    this.firstUrl = firstUrl;
    this.filenameFromURL = filenameFromURL;
    
    this.scoHomeUrl = scoHomeUrl;
    this.loadCF = loadCF;
    this.loadCFH = loadCFH;
    this.openRelativeLink = openRelativeLink;
    
}

function aqs(){
    return answeredQuestionsVAR;
}

function aqsReset(){
    answeredQuestionsVAR = 0;
}

function aqsInc(){
    answeredQuestionsVAR++;
    
}
