


/************************************
 ** FILE: zooAPIadapter.js
 **
 ** Defines the function getZooAPI ()
 ** that returns an object that
 ** understands SCORM API functions.
 **
 ** It is your responsibility to call
 ** that function and store its result
 ** into the global variable API in the
 ** topmost frame.
 **
 */
function getZooAPI(){
    if (zapiAdapter == null) {
        zapiAdapter = new ZooApiAdapter_class();
    }
    
    
    return zapiAdapter;
}



//////////////////////////////////////////////////////////
///////////////// PRIVATE PART: ////////////////////////// 
//////////////////////////////////////////////////////////

var zapiAdapter;
var ZGlobalData = new Object(); 

// stores in session memory
/********************************************
 **
 **  CLASS:  ZooApiAdapter_class (anLMS)
 **  The initialization parameter is
 **  lexically remembered.
 */

function ZooApiAdapter_class(){




    function LMSInitialize(emptyString){ 
		// art ("zooAPI: LMSInitialize()");
    }
    
    
    function LMSFinish(emptyString){
        //art ("zooAPIAdapter.js:: LMSFinish ( )\n");
    }
    
    
    
    function LMSSetValue(keyString, valueString){ 
		//art ("zooAPI: LMSSetValue ("
        //       + keyString + " ,  " + valueString + ")" );
        
        ZGlobalData[keyString] = valueString;
    }
    
    
    
    function LMSGetValue(keyString){
    
    
        if (keyString == 'cmi.core.student_name') 
            //   return 'N/A';
            return '';
        
        
        var result = ZGlobalData[keyString]; 
		/*!<variable name="result" type="Global" />!*/
        if (!result) 
            result = ""; // must always return a string
        return result;
    }
    
    
    function LMSCommit(emptyString){
        return ("false");
    }
    
    
    
    function LMSGetLastError(){
		// art ("zooAPI: LMSGetLastError ()");
        return (0);
    }
    
    
    
    function LMSGetErrorString(eCodeString){
        // art ("zooAPI: LMSGetErrorString ("
        //            + eCodeString + ")"
        //	    ); 
        
        return "No error"
    }
    
    
    
    function LMSGetDiagnostic(eCodeString){
        //  art ("zooAPI: LMSGetDiagnostic ("
        //             + eCodeString + ")"
        //	    );
        
        return "No error"
        
    }
    
    ////////////////////////////////////////
    
    
    
    var tn = (new Date()).getTime().toString();
    function toString(){
        return ("ZooApiAdapter-" + tn);
    }
    
    ///////////////////////////////////////
    /// SETUP  PUBLIC INTERFACE:
    ///
    this.LMSFinish = LMSFinish;
    this.LMSInitialize = LMSInitialize;
    this.LMSGetValue = LMSGetValue;
    this.LMSSetValue = LMSSetValue;
    this.LMSCommit = LMSCommit;
    this.LMSGetLastError = LMSGetLastError;
    this.LMSGetErrorString = LMSGetErrorString;
    this.LMSGetDiagnostic = LMSGetDiagnostic;
    this.LMSFinish = LMSFinish;
    this.toString = toString;
    
} // end class ZooApiAdapter
