


function currentLessonNo(){
    var pid = it().currentPageIndex();
    var pageNo
    if (it().contentFrame.newPageNo) {
        pageNo = it().contentFrame.newPageNo();
    }
    else {
        pageNo = 1;
    }
    var lessonNo = pageNo.toString().split('.')[0];
    return lessonNo
}



///////////  NOT READY, Carlos do your thing : ////////////////////////////////

// *********************************************************************************************************************************
// *********************************************************************************************************************************
// functions for the ellipses

function ellipseTitle(titleString){

    var maxedOutString = '';
    var stringMaxLength = 41;
    var titleLength = titleString.length;
    
    if (titleLength > stringMaxLength) {
        maxedOutString = titleString.slice(0, stringMaxLength - 3) + "...";
    }
    else {
        maxedOutString = titleString;
    }
    
    return maxedOutString;
    
}

function showTooltip(evt, oi, linkText){

    //if(!isms()) return false;
    MM_setTextOfLayer('t1', '', linkText);
    popUp(evt, oi);
    return true;
}

var DH = 0;
var an = 0;
var al = 0;
var ai = 0;
if (document.getElementById) {
    ai = 1;
    DH = 1;
}
else {
    if (document.all) {
        al = 1;
        DH = 1;
    }
    else {
        browserVersion = parseInt(navigator.appVersion);
        if ((navigator.appName.indexOf('Netscape') != -1) && (browserVersion == 4)) {
            an = 1;
            DH = 1;
        }
    }
}

function fd(oi, ws){
    if (ws == 1) {
        if (ai) {
            return (document.getElementById(oi).style);
        }
        else {
            if (al) {
                return (document.all[oi].style);
            }
            else {
                if (an) {
                    return (document.layers[oi]);
                }
            };
                    }
    }
    else {
        if (ai) {
            return (document.getElementById(oi));
        }
        else {
            if (al) {
                return (document.all[oi]);
            }
            else {
                if (an) {
                    return (document.layers[oi]);
                }
            };
                    }
    }
}

function pw(){
    if (window.innerWidth != null) 
        return window.innerWidth;
    if (document.body.clientWidth != null) 
        return document.body.clientWidth;
    return (null);
}

function popUp(evt, oi){
    if (DH) {
        var wp = pw();
        ds = fd(oi, 1);
        dm = fd(oi, 0);
        st = ds.visibility;
        if (dm.offsetWidth) 
            ew = dm.offsetWidth;
        else 
            if (dm.clip.width) 
                ew = dm.clip.width;
        if (st == "visible" || st == "show") {
            ds.visibility = "hidden";
        }
        else {
            if (evt.y || evt.pageY) {
                if (evt.pageY) {
                    tv = evt.pageY + 10;
                    lv = evt.pageX - (ew / 4) + 50;
                }
                else {
                    tv = evt.y + 10 + document.body.scrollTop;
                    lv = evt.x - (ew / 4) + document.body.scrollLeft + 50;
                }
                if (lv < 2) 
                    lv = 2;
                else 
                    if (lv + ew > wp) 
                        lv -= ew / 2;
                ds.left = lv;
                ds.top = tv;
            }
            ds.visibility = "visible";
        }
    }
}

// *********************************************************************************************************************************
// *********************************************************************************************************************************

function createTOCRow(lessonNumber){
    var currentPid = currentLessonNo(); // Carlos, use this somehow
    var lessonTitleTT = getLessonTitle(lessonNumber);
    var lessonTitle = ellipseTitle(lessonTitleTT);
    var eLinkName = getExercisesLinkName(lessonNumber);
    var lstateGraphic = getStateImageLesson(lessonNumber);
    var estateGraphic = getStateImageExercise(lessonNumber);
    
    var toolTipAction = (lessonTitleTT == lessonTitle) ? "" : "showTooltip(event,'t1','" + lessonTitleTT + "')";
    
    var imagesFolder = 'images/';
    
    if (lessonNumber == currentLessonNo()) {
        alternateSwitch = 3;
        lessonMarkerImg = imagesFolder + 'redArrowMarker.gif';
    }
    else {
        alternateSwitch = (lessonNumber % 2) ? '1' : '2';
        lessonMarkerImg = lstateGraphic;
    }
    
    var columnWidths = [6, 11, 14, 254, 10, 71, 15, 77, 9, 6];
    // Note that 220 appears magically in the table def below
    // If you change it in either plsce, must change in both
    
    
    var openRowGraphic = imagesFolder + 'openhl' + alternateSwitch + '.gif';
    var closeRowGraphic = imagesFolder + 'closehl' + alternateSwitch + '.gif';
    var tableBackGraphic = imagesFolder + 'highlight' + alternateSwitch + '.gif';
    //var lessonMarkerGraphic = lessonMarkerImg;
    
    var lessonLink = 'javascript:scoGetLessonFromLessonNumber(' + lessonNumber + ');';
    var questionLink = 'javascript:scoGetExerciseFromLessonNumber(' + lessonNumber + ');';
    
    // HTML
    
    // Heading Row
    var openTableHTML = '<table height="20" border="0" cellpadding="0" cellspacing="0" background="' + tableBackGraphic + '">';
    var leadingRowHTML = '<tr>';
    
    for (i = 0; i < columnWidths.length; i++) {
        leadingRowHTML += '    <td width="' + columnWidths[i] + '" bgcolor="#FFFFFF"><img src="images/whitespacer.gif" width="' + columnWidths[i] + '" height="2" border="0" alt=""></td>';
    }
    leadingRowHTML += '</tr>';
    
    // Content ROW
    contentRowHTML = '<tr>';
    // open left graphic
    contentRowHTML += '<td><img name="slice_" src="' + openRowGraphic + '" width="6" height="20" border="0" alt=""></td>';
    // current lesson marker or checkmark
    contentRowHTML += '<td><img name="slice_" src="' + lessonMarkerImg + '" width="7" height="9" border="0" alt=""></td>';
    // page icon
    contentRowHTML += '<td><img name="slice_" src="images/pageicon.gif" width="7" height="9" border="0" alt=""></td>';
    
    // lesson text and link
    // onMouseOut="popUp(event,'t1')" onMouseOver="popUp(event,'t1')"
    contentRowHTML += '<td><a href="' + lessonLink + '" class="toclink" onMouseOut="' + toolTipAction + '" onMouseOver="' + toolTipAction + '">' + lessonTitle + '</a></td>';
    
    // spacer
    contentRowHTML += '<td><img name="slice_" src="images/spacer.gif" width="5" height="20" border="0" alt=""></td>';
    
    // page #
    contentRowHTML += '<td class="WVCTfontText">page ' + lessonNumber + '</td>';
    
    if (eLinkName != "") {
        //exercise icon
        contentRowHTML += '<td><img name="slice_" src="images/exicon.gif" width="10" height="10" border="0" alt=""></td>';
        // exercise link and text
        contentRowHTML += '<td><a class="toclink" href="' + questionLink + '" class="toclink">' + eLinkName + ' </a></td>';
        // exercise state
        contentRowHTML += '<td><img name="slice_" src="' + estateGraphic + '" width="7" height="9" border="0" alt=""></td>';
    }
    else {
        //exercise icon
        contentRowHTML += '<td><img name="slice_" src="images/spacer.gif" width="10" height="10" border="0" alt=""></td>';
        // exercise link and text
        contentRowHTML += '<td><img name="slice_" src="images/spacer.gif" width="7" height="9" border="0"></td>';
        // exercise state
        contentRowHTML += '<td><img name="slice_" src="images/spacer.gif" width="7" height="9" border="0" alt=""></td>';
    }
    
    contentRowHTML += '<td><img name="slice_" src="' + closeRowGraphic + '" width="6" height="20" border="0" alt=""></td>';
    contentRowHTML += '</tr>';
    
    // Closing Tags
    var closeTableHTML = '</table>';
    
    // closeButton Table
    var HTML = openTableHTML + leadingRowHTML + contentRowHTML + closeTableHTML;
    return HTML;
}



/////////////////// NEW FUNCTIONS: ///////////////////////////



function closeTOC(){
    if (closeTOCbasic) 
        closeTOCbasic()
}

function closeTOCbasic(){
    if (!window.opener) {
        return;
    }
    
    // problem is there is the opener, but
    // is is already closed
    
    if (window.opener.notifyTocClosed) {
        window.opener.notifyTocClosed();
    }
}


function it(){
    if (window.opener == null) 
        result = window.parent
    else 
        result = window.opener.parent;
    return result;
}

function scoGetExerciseFromLessonNumber(qNumber){
    var sco = it().getSco();
    var pno = (qNumber + 1) + ".1";
    sco.loadThePage(pno);
    
    window.close();
    //window.location = window.location;
}



function scoGetLessonFromLessonNumber(lNumber){
    var sco = it().getSco();
    sco.loadThePage(lNumber + 1);
    
    window.close();
    //window.location = window.location;
}




function getTocTitle(){
    return (it().courseTitle() + ':');
}

function getNoOfLessonPages(){
    var result = it().lessonUrlAt(0); // gives the size
    return result;
    
}



function hasBeenVisitedL(pnoNew){
    var result = false;
    var pno = pnoNew + 1;
    var pindex = it().pageNoToIndexMap()[pno];
    
    if (it().hasPageBeenVisited) {
        result = it().hasPageBeenVisited(pindex)
    }
    return result;
}



function hasBeenVisitedE(pnoNew){
    var result = false;
    var pno = (pnoNew + 1);
    var pindex = it().pageNoToIndexMap()[pno];
    
    
    if (it().hasExercisesBeenVisited) {
        result = it().hasExercisesBeenVisited(pno)
    }
    
    return result;
}




function getStateImageLesson(lessonNumber){
    var isChecked = hasBeenVisitedL(lessonNumber);
    result = getCheckMarkImage(isChecked);
    
    
    return result
    
}


function getStateImageExercise(lessonNumber){ //#ISSUE. is it ok to lesson number to get the checkmark for exercise?
    // should be
    
    var eNumber = lessonNumber;
    var isChecked = hasBeenVisitedE(lessonNumber);
    return getCheckMarkImageE(isChecked);
}



function getCheckMarkImageE(checkedBoolean){
    if (checkedBoolean == null) { // sco150b
        return 'images/WV_Check_MarkEall.gif';
    }
    
    
    if (checkedBoolean) 
        return 'images/WV_Check_MarkE.gif';
    
    return 'images/WV_Check_Mark_EMPTY.gif';
    
}

function getCheckMarkImage(checkedBoolean){
    if (checkedBoolean) 
        return 'images/WV_Check_Mark.gif';
    
    return 'images/WV_Check_Mark_EMPTY.gif';
    
}





///////////////// MODIFIED FUNCTIONS:







function getStateImage(qNumber){
    // STATES: 
    // notchanged	= 1 (square)
    // changed		= 2 (square - red dot)
    // correct		= 3 (check)
    // incorrect	= 4 (x)
    // noanswer		= 5 (question mark)
    //
    //
    qStateImageArray = ["WV_Check_Mark_EMPTY.gif", "WV_Check_Mark.gif", "WV_Check_MarkE.gif"];
    state = getQuestionState(qNumber);
    switch (state) {
        case 3:
            return 'images/' + qStateImageArray[0];
            break;
        case 4:
            return 'images/' + qStateImageArray[1];
            break;
        default:
            return 'images/' + qStateImageArray[2];
    }
}





function createTOC(){
    var no = getNoOfLessonPages();
    var tocHTML = '';
    for (j = 1; j <= no; j++) {
        tocHTML += createTOCRow(j);
    }
    
    var buttonTableHTML = '<table width="461" height="30" border="0" cellpadding="0" cellspacing="0">';
    buttonTableHTML += '<tr><td align="center" valign="bottom"><a onMouseOver="document.images[\'close\'].src=\'images/closebutton_over.gif\';"';
    buttonTableHTML += 'onMouseOut="document.images[\'close\'].src=\'images/closebutton.gif\';"';
    buttonTableHTML += 'href="javascript:window.close();"><img name="close" src="images/closebutton.gif" width="65" height="18" border="0"></a></td>';
    buttonTableHTML += '</tr></table>';
    
    tocHTML += buttonTableHTML;
    return tocHTML;
    
}


function refreshPage(){//      ===========
    var tocHTML = createTOC();
    MM_setTextOfLayer_original('toc', '', tocHTML);
    setPageNoFieldBasic('0'); // #sco150.
}



function getLessonTitle(qNumber)//       ===============
// really: title of the lesson page
//
{
    return it().lessonTitleAt(qNumber);
    
}



function getExercisesLinkName(lessonNumber){
    var noOfE = it().noOfExercises(lessonNumber);
    if (noOfE == 0) 
        return "";
    
    var s = 's';
    if (noOfE == 1) 
        s = '';
    var linkName = noOfE + " exercise" + s;
    
    return linkName;
}

