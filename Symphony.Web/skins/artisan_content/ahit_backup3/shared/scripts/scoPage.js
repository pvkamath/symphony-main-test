

////////////  BELOW added Dec 29 2009 by Panu //////////
// by copying Carlos' code in pageFooter.js.
// By including it here in scoPage.js it will be 
// automatically available for every page, which
// fixes multiple script-errors for pages, especially
// test-questions which failed to include pageFooter.js
/////////////////////////////////////////////////////////

// Auto updating page footer

function off_renderPageFooter() {
	//do nothing!!!
	
}
 
function renderPageFooter() {
	
	var dateObject=new Date();
	var year=dateObject.getYear();
	if (year < 1000) year+=1900;
//	var day=dateObject.getDay();
//	var month=dateObject.getMonth();
//	var daymonth=dateObject.getDate();
//	if (daymonth<10) daymonth="0"+daymonth;
//	var dayarray=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
//	var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
	
footerHTML = '<table width="600" border="0" cellpadding="0" cellspacing="0" class="tableFooter">';
footerHTML+= '<tr bgcolor="#000000">';
footerHTML+= '<td height="1"><img src="images/spacer.gif" width="1" height="1"></td>';
footerHTML+= '</tr>';
footerHTML+= '<tr bgcolor="#FFFFFF">';
footerHTML+= '<td height="5"><img src="images/spacer.gif" width="1" height="1"></td>';
footerHTML+= '</tr>';
footerHTML+= '<tr>';
footerHTML+= '<td width="16%"><div align="center"><span class="pageFooter">Copyright &copy; '+year+' SS&amp;C Technologies, Inc. All rights reserved.</span></div></td>';
footerHTML+= '</tr>';
footerHTML+= '<tr bgcolor="#FFFFFF"><td><img src="images/spacer.gif" width="1" height="15"></td>';
footerHTML+= '</tr>';
footerHTML+= '</table>';

	document.write(footerHTML);

}

function renderPageFooter_simple() {
	
	var dateObject=new Date();
	var year=dateObject.getYear();
	if (year < 1000) year+=1900;
//	var day=dateObject.getDay();
//	var month=dateObject.getMonth();
//	var daymonth=dateObject.getDate();
//	if (daymonth<10) daymonth="0"+daymonth;
//	var dayarray=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
//	var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
	
footerHTML= '<div align="center"><span class="pageFooter">Copyright &copy; '+year+' SS&amp;C Technologies, Inc. All rights reserved.</span></div>';

	document.write(footerHTML);

}
////////////  ABOVE added Dec 29 2009 by Panu //////////



function indexOf(absUrl){
    var absu2 = absUrl.replace(/\/i000\d.\//, 'i0001');
    for (i = 0; i < this.length; i++) {
        if (this[i] == absu2) 
            return i;
        if (absu2.match(this[i])) 
            return i;
    }
    return null;
}


function setPageNoFieldBasic(aString0){
    if (!nf()) {
        return;
    }
    var sinf = parent.scoInfo();
    sinf.indexOf = indexOf;
    
    var splits = document.location.toString().split('/');
    var filename = splits[splits.length - 1];
    
    //////////////////////////////////////////////
    //var inlist	= sinf.indexOf(filename); 
    var absurl = document.location.toString();
    var inlist = sinf.indexOf(absurl);
    if (inlist) {
        parent.currentPageIndexIs(inlist);
    }
    ///////////////////////////////////////////////// 
    
    if (inlist == null) {
        if (pageType == 'HtmlLesson') {
            aString0 = '0';
            
        }
        if (pageType == 'auxiliary') {
            aString0 = '0';
        }
    }
    
    var aString = aString0;
    if (aString == '0') {
        aString = ' - ';
    }
    var pageField;
    if (isIE) {
        pageField = nf().document.all.pageNoField;
        if (pageField == null) 
            return;
        pageField.value = aString.toString();
    }
    else {
        var formm;
        if (isMoz) {
            var onLay = parent.navigationFrame.document.getElementById('OnLayer');
            formm = parent.navigationFrame.document.forms[0];
        }
        else {
            formm = parent.navigationFrame.document.layers.OnLayer.document.forms[0];
        }
        if (formm == null) 
            return;
        pageField = formm.pageNoField;
        
        if (pageField != null) {
            pageField.value = aString.toString()
        };
            }
    return true;
}






//////////////////////////////////////

function glossaryBase(){
    return '../lib/glossary/'
}

function openScormGlossaryDef(aString){
    if (top.frames.ServerResponse != null) {
        openJITGlossaryDef(aString);
        return;
    }
    var aPopupId = aString;
    var w = 600;
    var h = 700;
    h = 238;
    w = 358;
    
    h = 250;
    h = 230;
    if (window.event) {
        x = window.event.screenX - 40 + 30;
        y = window.event.screenY + 15;
    }
    else {
        "FLASH "
        x = Math.round((screen.width - w) / 2);
        y = Math.round((screen.height - h) / 2);
        h = 208;
        h = 230
        if (window.popup && !window.popup.closed) {
            x = window.popup.screenX;
            y = window.popup.screenY;
            w = window.popup.innerWidth;
            h = window.popup.innerHeight;
        }
    }
    
    var wname = aString.toString().replace(/\./g, '_');
    var url = glossaryBase() + aString + '.htm';
    var features = "left=" + x + ",top=" + y + "," +
    "width=" +
    w +
    ",height=" +
    h +
    ",dependent=yes,directories=no,menubar=no,personalbar=no,status=no,toolbar=no,resizable=yes,scrollbars=auto";
    
    if (window.popup && !window.popup.closed) {
        window.popup.close();
    }
    
    if (isIE) {
        var features = "dialogLeft:" + x +
        "px;dialogTop:" +
        y +
        "px;dialogHeight:" +
        h +
        "px;dialogWidth:" +
        w +
        "px;dependent:yes;directories:no;menubar:no;personalbar:no;status:no;toolbar:no;resizable:yes;scroll:no;";
        
        window.popup = window.showModelessDialog(url, "", features);
        return;
    };
    window.popup = window.open(url, wname, features);
}


function scoPageLoadedCOMMON(idString, classString, windowObject, pageNoString, pageTitle){


    if (parent == this) {
        finishPageDisplay();
        return
    }
    pageTypeVAR = classString;
    pageIdIs(idString);
    pageNoIs(pageNoString);
    setPageNoField(pageNoString);
    finishPageDisplay();
    
    if (parent.updateSessionTime) {
        if (!parent.isReviewMode()) {
            parent.updateSessionTime();
        }
    }
    
    if (parent.navigationFrame) {
        if (parent.navigationFrame.indicateHelp) 
            parent.navigationFrame.indicateHelp();
    }
    
    
}

var pageTypeVAR;
function raisePopup(){
    if (window.popup && (!window.popup.closed)) 
        window.popup.focus();
    
    if (parent.popupPopup) {
        if (!parent.popupPopup.closed) 
        
            parent.popupPopup.focus()
    }
    
}

window.onFocus = raisePopup;


function scoPageUnloaded(){
    if (window.popup) 
        window.popup.close()
    
    
}

function newPageNo(){
    var aString = convert(pageNumb());
    return aString;
    
}

function nf(){
    var nf = parent.navigationFrame;
    if (nf == null) 
        nf = document.navigationFrame;
    if (nf == null) 
        nf = this.navigationFrame;
    // alert ( "nf ::: " + nf );
    return nf;
}


function notifyTop(){
    if (top.opener) {
        if (top.opener.notifyZooPageLoaded) 
            top.opener.notifyZooPageLoaded(pageTitle, convert(pageNoString))
    }
    else {
        if (top.notifyZooPageLoaded) 
            top.notifyZooPageLoaded(pageTitle, convert(pageNoString))
    }
}

function pageType(){
    return pageTypeVAR;
}

var pageId_VAR;
var pageNo_VAR;
function pageNumb(){
    //	alert ( " ::: inside pageNumb :::: " );
    return pageNo_VAR;
}

function pageNoIs(aString){
    pageNo_VAR = aString;
}

function pageId(){
    var pno = pageNumb();
    if (!parent.pageNoToIndexMap) // PORTAL FIX
    {
        return 2;
    }
    var pindex = parent.pageNoToIndexMap()[pno];
    return pindex;
}



function pageIdIs(aString){
    pageId_VAR = aString;
}

function zooCacheValue(keyString, valueString){
    parent.store(keyString, valueString);
}

function zooCachedValue(keyString){
    result = parent.load(keyString);
    return result;
}





function isInSequence(aString){
    if (parent.scoInfo == null) 
        return false;
    var sinf = parent.scoInfo();
    sinf.indexOf = indexOf;
    var splits = aString.split('/');
    var filename = splits[splits.length - 1];
    var inlist = sinf.indexOf(filename);
    if (inlist == null) 
        return false;
    return true;
}

function convert(aString){
    if (aString == null) 
        return 0;
    
    var parts = aString.toString().split('.');
    var whole = parts[0];
    var dec = parts[1];
    if (dec == null) 
        return (whole - 1);
    return (whole - 1) + "." + dec;
}

function setPageNoField(aString0){
    var aString = convert(aString0);
    setPageNoFieldBasic(aString);
}


function openHyperlink(aString){
    openScormLesson(aString);
}

function showGlossaryPopup(aString){

    openScormGlossaryDef(aString)
}

function openScormPopup(aString){
    var aPopupId = aString;
    var w = 670;
    var h = 600;
    
    if (window.event) {
        x = window.event.screenX - 40 + 30;
        y = window.event.screenY + 15;
        
    }
    else {
        if (parent.popupPopup) {
            if (!parent.popupPopup.closed) 
            
                parent.popupPopup.focus()
        }
        x = Math.round((screen.width - w) / 2);
        y = Math.round((screen.height - h) / 2);
        h = 508;
        if (window.popup && !window.popup.closed) {
            x = window.popup.screenX;
            y = window.popup.screenY;
            w = window.popup.innerWidth;
            h = window.popup.innerHeight;
        }
    }
    
    var wname = aString.toString().replace(/\./g, '_');
    var url = aString;
    var features = "left=" + x + ",top=" + y + "," +
    "width=" +
    w +
    ",height=" +
    h +
    ",dependent=yes,directories=no,menubar=yes,personalbar=no,status=no,toolbar=no,resizable=yes,scrollbars=yes";
    
    if (isIE) {
        var features = "dialogLeft:" + x +
        "px;dialogTop:" +
        y +
        "px;dialogHeight:" +
        h +
        "px;dialogWidth:" +
        w +
        "px;dependent:yes;directories:no;menubar:yes;personalbar:no;status:no;toolbar:no;resizable:yes;scroll:yes;";
        
        window.popup = window.showModalDialog(url, "", features);
        return;
    };
    
    parent.popupPopup = window.open(url, wname, features);
}



function showPopupPage(aString){
    var url = 'popup_' + aString + '.htm';
    if (top.frames.ServerResponse != null) {
        openJITPopup(url);
        return;
    }
    openScormPopup(url);
}

function openScormDYK(aString){
    //	 alert("openScormDYK ::: " + aString);
    if (top.frames.ServerResponse != null) {
        alert("ServerResponse");
        openJITDYK(aString);
        return;
    }
    if (true) {
        //			 alert("openScormLessonWithoutNextAndPrevious");
        openScormLessonWithoutNextAndPrevious(aString);
        return;
    }
    //	alert("after openScormDYK");
    var wname = aString.replace(/\./g, '_');
    var url = aString;
    var features = 'toolbar=no,menubar=no,scrollbars=yes,width=650,height=400';
    
    
    window.open(url, wname, features);
}

var X = 1;
var Y = 1;
function clickz(arg){
    X = arg.x;
    Y = arg.y;
}

function openScormLesson(aString){

    // alert ( " ::: openScormLesson ::: " );
    if (top.frames.ServerResponse != null) {
        openJITLesson(aString);
        return;
    }
    
    if (true) {
        openScormLessonWithoutNextAndPrevious(aString);
        return;
    }
    if (!(aString.indexOf('dyk_') == 0)) {
    
        openScormLessonInSameWindow(aString);
        return;
    }
    
    var wname = aString.replace(/\./g, '_');
    var url = aString;
    var features = 'toolbar=no,menubar=no,scrollbars=yes,width=650,height=400';
    
    
    window.open(url, wname, features);
}

function openScormLessonWithoutNextAndPrevious(aString){
    //	alert( "openScormLessonWithoutNextAndPrevious ::: " + aString )
    if (nf()) 
        nf().outOfSequenceHyperlink(aString);
    openScormLessonInSameWindow(aString);
    if (true) {
    
        return;
    }
    var wname = aString.replace(/\./g, '_');
    var url = aString;
    var features = 'toolbar=no,menubar=no,scrollbars=yes,width=650,height=400';
    
    
    window.open(url, wname, features);
}

function openScormLessonInSameWindow(aString){
    //	alert( "openScormLessonInSameWindow ::: " + aString )
    var url = aString;
    if (!(parent == this)) {
        if (parent.getSco) {
            parent.getSco().openRelativeLink(url);
        }
        else {
            window.open(url, "_blank", "toobar=no,resizable=yes,scrollbars =yes");
        }
    }
    else {
        document.location = url;
    }
}


function LMSGetValue(keyString){
    var result = parent.zooLMSGetValue(keyString);
    return result;
}

function LMSSetValue(keyString, valueString){
    parent.zooLMSSetValue(keyString, valueString);
}

function LMSCommit(){
    var result = parent.zooLMSCommit();
    return result;
}

function LMSGetLastError(){
    var result = parent.zooLMSGetLastError();
    return result;
}

function LMSGetErrorString(){
    var result = parent.zooLMSGetErrorString();
    return result;
}

function LMSGetDiagnostic(){
    var result = parent.zooLMSGetDiagnostic();
    return result;
}

function LMSGetLastError(){
    var result = parent.zooLMSGetLastError();
    return result;
}


function openJITDYK(aString){

    var htmlString = '.html';
    var index = aString.indexOf(htmlString);
    if (index != -1) {
        aString = aString.substring(0, index);
    }
    var htmString = '.htm';
    var index = aString.indexOf(htmString);
    if (index != -1) {
        aString = aString.substring(0, index);
    }
    var index = aString.indexOf('_');
    if (index == 3) {
        aString = aString.substring(index + 1);
    }
    
    top.frames.ServerResponse.document.location = window.top.serverUrl + '/viewer?request=get_page&id=' + aString;
}

function openJITGlossaryDef(aString){

    var aPopupId = aString;
    var w = 600;
    var h = 700;
    h = 238;
    w = 358;
    
    h = 250;
    h = 230;
    if (window.event) {
        x = window.event.screenX - 40 + 30;
        y = window.event.screenY + 15;
    }
    else {
        "FLASH "
        x = Math.round((screen.width - w) / 2);
        y = Math.round((screen.height - h) / 2);
        h = 208;
        h = 230
        if (window.popup && !window.popup.closed) {
            x = window.popup.screenX;
            y = window.popup.screenY;
            w = window.popup.innerWidth;
            h = window.popup.innerHeight;
        }
    }
    
    var wname = aString.toString().replace(/\./g, '_');
    var url = 'shared/glossary/' + aString + '.htm';
    var features = "left=" + x + ",top=" + y + "," +
    "width=" +
    w +
    ",height=" +
    h +
    ",dependent=yes,directories=no,menubar=no,personalbar=no,status=no,toolbar=no,resizable=yes,scrollbars=auto";
    
    
    if (window.popup && !window.popup.closed) {
        window.popup.close();
    }
    
    if (isIE) {
        var features = "dialogLeft:" + x +
        "px;dialogTop:" +
        y +
        "px;dialogHeight:" +
        h +
        "px;dialogWidth:" +
        w +
        "px;dependent:yes;directories:no;menubar:no;personalbar:no;status:no;toolbar:no;resizable:yes;scroll:no;";
        
        window.popup = window.showModelessDialog(url, "", features);
        return;
    };
    window.popup = window.open(url, wname, features);
}

function openJITPopup(aString){
    var aPopupId = aString;
    var w = 600;
    var h = 700;
    h = 508;
    w = 560;
    
    if (window.event) {
        x = window.event.screenX - 40 + 30;
        y = window.event.screenY + 15;
        
    }
    else {
        if (parent.popupPopup) {
            if (!parent.popupPopup.closed) 
            
                parent.popupPopup.focus()
        }
        x = Math.round((screen.width - w) / 2);
        y = Math.round((screen.height - h) / 2);
        h = 508;
        if (window.popup && !window.popup.closed) {
            x = window.popup.screenX;
            y = window.popup.screenY;
            w = window.popup.innerWidth;
            h = window.popup.innerHeight;
        }
    }
    
    var wname = aString.toString().replace(/\./g, '_');
    var url = aString;
    url = window.location.href;
    var index = url.lastIndexOf("/");
    url = url.substring(0, index + 1);
    url = url + aString;
    var features = "left=" + x + ",top=" + y + "," +
    "width=" +
    w +
    ",height=" +
    h +
    ",dependent=yes,directories=no,menubar=yes,personalbar=no,status=no,toolbar=no,resizable=yes,scrollbars=yes";
    
    if (isIE) {
        var features = "dialogLeft:" + x +
        "px;dialogTop:" +
        y +
        "px;dialogHeight:" +
        h +
        "px;dialogWidth:" +
        w +
        "px;dependent:yes;directories:no;menubar:yes;personalbar:no;status:no;toolbar:no;resizable:yes;scroll:yes;";
        
        window.popup = window.showModalDialog(url, "", features);
        return;
    };
    
    parent.popupPopup = window.open(url, wname, features);
}

function openJITLesson(aString){

    var htmlString = '.html';
    var index = aString.indexOf(htmlString);
    if (index != -1) {
    
        aString = aString.substring(0, index);
    }
    var htmString = '.htm';
    var index = aString.indexOf(htmString);
    if (index != -1) {
    
        aString = aString.substring(0, index);
    }
    var index = aString.indexOf('_');
    if (index == 2) {
        aString = aString.substring(index + 1);
        
    }
    else {
        aString = aString + "_" + zoologicCourseID();
        
    }
    
    top.frames.ServerResponse.document.location = window.top.serverUrl + '/viewer?request=get_page&id=' + aString;
    
}
