<?xml version="1.0"?>
<!-- The above must be on the FIRST line for Netscape -->

<xsl:stylesheet 
    xmlns:imsmd="http://www.imsglobal.org/xsd/imsmd_v1p2" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
 xmlns:zoo="http://www.zoologic.com/"
xmlns:adlcp =  "http://www.adlnet.org/xsd/adlcp_rootv1p2" 	

   version="1.0"  >

<xsl:output method="html" encoding="UTF-8" />



<xsl:template match="/">
	<html><head><title> imsmanifest.xml </title></head>
	<body bgcolor="#fffbf0">
		<xsl:apply-templates/> 
	</body>
	</html>
</xsl:template> 
 

 
 
<xsl:template match="metadata">
<b>
<xsl:value-of select = "schema" /> 
<xsl:value-of select = "schemaversion" /> _
<xsl:value-of select = "adlcp:location" />
 </b>	
 <br />
  <xsl:apply-templates select="organizations"/>
</xsl:template>  



 
<xsl:template match="organizations">
 <br/>Organization:
<xsl:value-of select = "@default" />
<br/>
<xsl:apply-templates select="organization"/>
</xsl:template>  
  
 
 
 
<xsl:template match="resources">
 <br/>Resources:
<xsl:value-of select = "@default" />
<br/>
<xsl:apply-templates select="resource"/>
</xsl:template>  
  

 
<xsl:template match="resource">
 <h3>Resource: </h3>
  <xsl:value-of select = "@identifier" />
<xsl:apply-templates select="file"/>
</xsl:template>  
  

 
<xsl:template match="file">
 <br/>
<xsl:variable name="href"  select="@href"/>
 <a href="{@href}">    
	<xsl:value-of select = "$href" />  
 </a>

     
<br/>
</xsl:template>  


</xsl:stylesheet>
