// JavaScript Document


				/*$	$( '.owlbear li' ).hide();
					
					
					( '.owlbear .selected' )
						.show()
						.before('<span id="clawaft" style="text-decoration:underline;">previous slides</span>')
						.after('<span id="clawfore" style="text-decoration:underline;">next slides</span>')
						;
						
						$( '.owlbear .selected' ).click( function( e ) {
						$( '.owlbear li' ).not('.selected').toggle();
						});
						$( '.owlbear #clawaft' ).click( function( e ) {
							$(this).toggle;
							$( '.owlbear .selected' ).prevAll().toggle();
						});
						$( '.owlbear #clawfore' ).click( function( e ) {
							$(this).toggle;
							$( '.owlbear .selected' ).nextAll().toggle();
						});
						
						*/

(function ( $ ) {
 
    $.fn.owlbear = function() {
        


										
										
					$( '.owlbear li' ).not('.selected').hide();
					
					$('.owlbear .selected a').css('cursor','default');
					
					$('.owlbear .first')
						.before('<div id="clawaft"><span class="handle-all">all</span></div>');
					
					$('.owlbear .last')
						.after('<div id="clawfore"><span class="handle-next">further reading</span></div>');
					
					$( '.owlbear #clawaft' ).click( function( e ) {
						//open+close previous only $( '.owlbear .selected' ).prevAll().not('#clawaft').toggle().addClass('prev-slide');
						if($('.owlbear #clawfore:hidden')) {										
							$( '.next-slide' ).removeClass('.next-slide');
							$( '.owlbear li, #clawfore' ).not('.selected').toggle();	
//							$( '.owlbear #clawfore' ).show();
							$( '#clawfore .handle-next' ).removeClass('minus');
							//close all but selected
							//show bottom button
							//remove minus						
						} else {
							$( '.next-slide' ).hide();
							$( '.owlbear li, #clawfore' ).not('.selected').toggle();
						}
					});										
					
					$( '.owlbear #clawfore' ).click( function( e ) {
						//open+close further only $( '.owlbear .selected' ).nextAll().not('#clawfore').toggle().toggleClass('next-slide');
						$( '.owlbear .selected' ).nextAll().not('#clawfore').toggle().toggleClass('next-slide');
						$('#clawfore .handle-next').toggleClass('minus');
					});
					
					if ( $('.selected').hasClass('last') ) {
					  $('.handle-next').hide().parent().css('cursor','default');
					};
	
	return this;
	};
 
}( jQuery ));