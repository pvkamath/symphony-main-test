(function () {
    var LMS = parent.objLMS;

    // load up the contents of the course
    var state = { course: {}, sco: null, learningObject: null, page: null, gameTotal: 0, reviewQuestionIndex: 0, isPreviousPageReview: false, progress: { } };

    var videos = [];
    var counter = 0;
    var maxLongAnswerLength = 7799;

    Artisan.newScoreMustBeHigher = true;
    // document.getElementById('ScormContent').contentWindow.frames[0].Artisan
    Artisan.App = {
        scormFailure: false,
        reconnectAttempts: 0,
        course: null,
        state: state,
        autoLogoutTimeout: null,
        maximumQuestionTimeout: null,
        maximumTestTimeout: null,
        maximumTestTimeSectionId: null,
        minimumPageTimeout: null,
        minimumPageTimeCountdown: null,

        minCourseTimer: new Artisan.Models.Timer(),
        minLoTimer: new Artisan.Models.Timer('lo'),
        minScoTimer: new Artisan.Models.Timer('sco'),


        /* the following methods are for use with the JS hook *only*, they won't have any effect while the course is running */
        setPosttestType: function (type) {
            var json = Artisan.CourseJson;
            for (var i = 0; i < json.sections.length; i++) {
                if (json.sections[i].sectionType == ArtisanSectionType.Posttest) {
                    json.sections[i].testType = type;
                }
            }
        },
        setPassingScore: function (score) {
            Artisan.CourseJson.passingScore = score;
        },
        hasPosttest: function () {
            var json = Artisan.CourseJson;
            for (var i = 0; i < json.sections.length; i++) {
                if (json.sections[i].sectionType == ArtisanSectionType.Posttest) {
                    return true;
                }
            }
            return false;
        },
        removePosttest: function () {
            Artisan.App.removeSpecial(ArtisanSectionType.Posttest);
        },
        setPretestType: function (type) {
            var json = Artisan.CourseJson;
            for (var i = 0; i < json.sections.length; i++) {
                if (json.sections[i].sectionType == ArtisanSectionType.Pretest) {
                    json.sections[i].testType = type;
                }
            }
        },
        removePretest: function () {
            Artisan.App.removeSpecial(ArtisanSectionType.Pretest);
        },
        removeObjectives: function () {
            Artisan.App.removeSpecial(ArtisanSectionType.Objectives);
        },
        removeSpecial: function (type) {
            var json = Artisan.CourseJson;
            for (var i = 0; i < json.sections.length; i++) {
                if (json.sections[i].sectionType == type) {
                    json.sections.splice(i, 1);
                }
            }
        },
        setNavigationMethod: function (method) {
            Artisan.CourseJson.navigationMethod = method;
        },
        setPretestTestOut: function (testOut) {
            var json = Artisan.CourseJson;
            for (var i = 0; i < json.sections.length; i++) {
                if (json.sections[i].sectionType == ArtisanSectionType.Pretest) {
                    json.sections[i].testOut = testOut;
                }
            }
        },
        setSkin: function (cssPath) {
            if (document.getElementById('artisan_skin')) {
                document.getElementById('artisan_skin').setAttribute('href', cssPath);
            }
        },
        setIsAllowRetryAssignment: function (isAllowRetryAssignment) {
            Artisan.App.isAllowRetryAssignment = isAllowRetryAssignment;
        },
        setAssignmentResults: function (results) {
            Artisan.App.assignmentResults = results;
        },
        reinit: function () {
            Artisan.App.updateMenu();
            Artisan.App.resetState();
            Artisan.App.startCourse();
        },
        /* end JS hook methods */

        init: function (options) {

            if (window.top.Control && window.top.Control.Comm) {
                window.top.Control.Comm.HandleCallFailed = Artisan.App.handleScormFailure;
                window.top.Control.Comm.ClearCallFailed = Artisan.App.handleScormReconnect;
            }

            if (Artisan.IsScormDisabled && !Artisan.IsCourseUnlocked) {
                alert("Your session has expired and you must contact customer service to enroll in a new session. You may preview the course now but no work you perform will count towards your completion requirements.");
                LMS = new parent.LMSStandardAPI('NONE');
            } else if (Artisan.IsScormDisabled && Artisan.IsCourseUnlocked) {
                alert("You have met your completion requirements and this content is now available for review only. Your score will not be affected.");
                LMS = new parent.LMSStandardAPI('NONE');
            }

            var json = JSON.parse(JSON.stringify(window.Artisan.CourseJson));
            Artisan.App.course = new Artisan.Models.Course(json, LMS.GetDataChunk());

            this.showVersion();

            if (this.loaded) {
                Artisan.App.reinit();
                return;
            }

            this.loaded = true;

            this.options = Artisan.Utilities.extend({
                scoMenuClass: '',
                loMenuClass: ''
            }, options || {});

            // create a list for the menu
            var menu = [];
            Artisan.App.buildMenu(Artisan.App.course.getScos(), menu);
            $('#artisan-menu').html(menu.join('\r\n'));

            // display the previous/next buttons
            //$('.artisan-navigation').show();

            $('.artisan-next').live('click', function (e) {
                e.stopPropagation();
                e.preventDefault();
                if ($(this).hasClass('disabled')) return;

                // if needed, dispose of gamewheel
                if (state.learningObject.config.testType == ArtisanTestType.GameWheel) {
                    swfobject.removeSWF("artisan-gamewheel-control");
                }
                if ($(this).hasClass('exit')) {
                    window.top.close();
                }
                else {
                    Artisan.App.moveNext();
                }
            });

            $('.artisan-previous').live('click', function (e) {
                e.stopPropagation();
                e.preventDefault();
                if ($(this).hasClass('disabled')) return;
                Artisan.App.movePrevious();
            });

            $('.artisan-review-previous').live('click', function (e) {
                e.stopPropagation();
                e.preventDefault();
                if ($(this).hasClass('disabled')) return;
                Artisan.App.movePreviousReview();
            });

            // Fixing an accidental bug in index.html, this is why we are hooking this function
            // to two classes. The artisan-redfsafdview-next was likely a test change that
            // inadvertantly got committed. It's easier to replace all the js files
            // than the index.html file. The index.html gets modified on deploy with some
            // artisan data for the theme. 
            $('.artisan-review-next, .artisan-redfsafdview-next').live('click', function (e) {
                e.stopPropagation();
                e.preventDefault();
                if ($(this).hasClass('disabled')) return;
                Artisan.App.moveNextReview();
            });

            $('.artisan-quiz-retry').live('click', function (e) {
                e.stopPropagation();
                e.preventDefault();
                if ($(this).hasClass('disabled')) return;
                Artisan.App.retryLearningObject();
            });

            /* hook up the various UI elements */
            $('.artisan-sco').live('click', function (e) {
                e.stopPropagation();
                e.preventDefault();

                var scoId = parseInt($(this).attr('scoId'));

                // if the item is disabled, bail out
                if ($(this).hasClass('disabled')) {
                    return;
                }

                // Don't do anything if "Course Menu" is clicked
                if ($(this).hasClass('topitem')) {
                    return;
                }

                Artisan.App.selectSco(Artisan.App.course.getSco(scoId));
            });

            $('.artisan-learningobject').live('click', function (e) {
                e.stopPropagation();
                e.preventDefault();

                var learningObjectId = parseInt($(this).attr('learningObjectId'));

                // don't allow locked learning objects
                if ($(this).hasClass('disabled')) {
                    return;
                }

                Artisan.App.selectLearningObject(Artisan.App.course.getLearningObject(learningObjectId));
            });

            $('#artisan-survey-submit').live('click', function (e) {
                e.stopPropagation();
                e.preventDefault();

                Artisan.App.processSurvey(state.page);
            });

            $('.artisan-answer.text-wrapper textarea').live('keyup', function (e) {
                return Artisan.App.handleMaxTextLength($(this));
            });

           

            $('#artisan-question-submit').live('click', function (e) {
                e.stopPropagation();
                e.preventDefault();

                var allowEmptyAnswer = e.allowEmptyAnswer ? e.allowEmptyAnswer : false,
                    pageContext = state.page,
                    pageId = parseInt($(this).attr('data-pageid'), 10),
                    pageQuestionContext = $(this).parent('.artisan-question');

                if (pageId && state.learningObject.config.isSinglePage) {
                    for (var i = 0; i < state.learningObject.config.combinedPages.length; i++) {
                        var p = state.learningObject.config.combinedPages[i];

                        if (p.id === pageId) {
                            pageContext = new Artisan.Models.Page(state.learningObject, p);
                        }
                    }
                }

                var userAnswers = Artisan.App.gradePage(pageContext, allowEmptyAnswer, pageQuestionContext);
                if (!userAnswers && !allowEmptyAnswer) {
                    alert('The question could not be submitted because an answer was not entered.');
                    return;
                }
                if (state.page.config.questionType == ArtisanQuestionType.LongAnswer) {
                    var answer = userAnswers[0];
                    if (!Artisan.App.isValidLongAnswer(answer)) {
                        alert('The question could not be submitted because the response is too long.');
                        Artisan.App.handleMaxTextLength($('textarea', pageQuestionContext));
                        return;
                    }
                }

                var isCorrect = Artisan.App.course.record.recordInteraction(state, userAnswers, pageContext);

                var isGameBoard = (state.learningObject.config.testType == ArtisanTestType.GameBoard);
                var isGameWheel = (state.learningObject.config.testType == ArtisanTestType.GameWheel);

                // Game Board/Wheel updates
                if (isGameBoard || isGameWheel) {
                    if (isCorrect) {
                        if (pageContext.amount == 'double_current_total')
                            state.gameTotal *= 2;
                        else
                            state.gameTotal += state.page.amount;
                    }
                    else {
                        if (pageContext.amount != 'double_current_total') {
                            state.gameTotal -= state.page.amount;
                        }
                        if (state.gameTotal < 0)
                            state.gameTotal = 0;
                    }
                    if (isGameBoard) {
                        var finished = Artisan.App.renderGameBoard();
                        if (finished) {
                            Artisan.App.selectPage(state.learningObject.getLastPage()); // go to review page
                            return; // we don't set bookmark on review pages
                        }
                    }
                    else if (isGameWheel) {
                        Artisan.App.updateGameWheelTotal();

                        // if the next page is the last page (review page), then you are finished
                        var finished = (state.learningObject.getNextPage(pageContext.getId()).getId() == state.learningObject.getLastPage().getId());
                        if (finished) {
                            // show next button
                            $('.artisan-previous').addClass('disabled');
                            $('.artisan-next').removeClass('disabled');
                        }
                        else {
                            $('.artisan-gamewheel-button').removeClass('disabled');
                        }
                    }
                }

                // Don't want to save a bookmark if we are in a post test when force restart is enabled
                if ((!state.learningObject.isTestForceRestart() || pageContext.isIntroduction())) {
                    // setting the bookmark saves the datachunk and ensures game totals get stored
                    Artisan.App.setBookmark();
                }

                // If we are in a forceTestRestart mode then we need to calculate and save
                // the score for each question answered
                if (state.learningObject.isTestForceRestart() && !pageContext.isIntroduction()) {
                    Artisan.App.course.record.calculateInProgressPostTestScore(state.learningObject.getId());
                }
            });

            // monitor click events on an image question
            $('.artisan-question-imagequestion img.artisan-answer').live('click', function (e) {
                var questionContext = $(this).parents('.artisan-question');

                if ($('#artisan-question-submit', questionContext).attr('disabled')) {
                    // no clicking if the submit is disabled
                    return;
                }

                var x = (e.pageX - $(this).offset().left);
                var y = (e.pageY - $(this).offset().top);
                Artisan.App.addImagePointer(this, x, y, questionContext);
            });

            $('.artisan-question-imagequestion #artisan-question-reset').live('click', function (e) {
                Artisan.App.removeImagePointers();
            });

            // monitor click events on gameboard buttons
            $('.game_button').live('click', function (e) {
                var pageIndex = parseInt($(this).attr('pageIndex'));
                var amount = parseInt($(this).attr('amount'));
                if (pageIndex >= 0) {
                    var page = state.learningObject.getPageAt(pageIndex);
                    page.amount = amount;
                    Artisan.App.selectPage(page);
                }
                e.stopPropagation();
                e.preventDefault();
            });

            // TODO: add glossary support

            // apply student name and disclaimer
            //$('.artisan-student-name').html(Artisan.App.settings.studentName);

            if (Artisan.App.course.config.disclaimer) {
                $('.artisan-disclaimer').html(Artisan.App.course.config.disclaimer);
            }
            if (Artisan.App.course.config.contactEmail) {
                $('.artisan-contact-email').html('<a href="mailto:' + Artisan.App.course.config.contactEmail + '">' + Artisan.App.course.config.contactEmail + '</a>');
            }

            document.title = Artisan.App.course.getName();

            $('.artisan-course-title .artisan-content').html(Artisan.App.course.getName());

            if (Artisan.App.course.config.headerImageUrl) {
                $('.artisan-course-image')[0].src = Artisan.App.course.config.headerImageUrl;
            }
            Artisan.App.startCourse();

            window.setTimeout(function () {
                Artisan.App.hideLoading();
            }, 1000);

            // Clear auto logout on any mouse movement
            // Tracking last mouse since other apps can 
            // affect when the mousemove event is fired.
            // Assuming mouse position may be different
            // from the iframe tracking method so we need
            // a separate tracking var for that. 
            var lastMouse = {
                x: 0,
                y: 0
            }
            $('body').mousemove(function (e) {
                if (lastMouse.x != e.pageX &&
                    lastMouse.y != e.pageY) {
                    Artisan.App.updateAutoLogout();
                }
                lastMouse.x = e.pageX;
                lastMouse.y = e.pageY;
            });

            // From https://github.com/aroder/IframeActivityMonitor
            // Listens for mouseover events on iframes, as soon as
            // the mouse enters an iframe a timeout starts to create
            // a div in the same position of the iframe every 30 seconds
            // This will dispatch a mouseover event on the div that
            // we can track. As soon as the div is created it is removed
            // so it should not affect input. 
            var trackingTimer;
            var oldPosition = {
                x: 0,
                y: 0
            };
            $('iframe').live('mouseover', function (e) {
                trackingTimer = setTimeout(function tracker() {
                    var frame = e.target;
                    var div = document.createElement('div');
                    div.style.height = frame.clientHeight + 'px';
                    div.style.width = frame.clientWidth + 'px';

                    div.style.left = frame.offsetLeft + 'px';
                    div.style.top = frame.offsetTop + 'px';
                    div.style.position = 'absolute';
                    div.innerHTML = '&nbsp;';
                    document.body.appendChild(div);

                    div.addEventListener('mouseover', function tracker2(mouseEvent) {
                        var position = {
                            x: mouseEvent.clientX,
                            y: mouseEvent.clientY
                        };

                        // if the position is different than the old position, 
                        // mouse moved, so update the timeout
                        if (position.x != oldPosition.x || position.y != oldPosition.y) {
                            Artisan.App.updateAutoLogout();
                        }
                        oldPosition = position;
                        div.parentNode.removeChild(div);
                    }, false);

                }, 30000);
            });

            $('iframe').live('mouseout', function (e) {
                clearTimeout(trackingTimer);
            });
        },
        resetState: function () {
            state = { course: {}, sco: null, learningObject: null, page: null, gameTotal: 0, reviewQuestionIndex: 0 };
        },
        startCourse: function () {
            var loId, pageId, loSelected;

            // avoid double-prompting on a re-init
            if (!Artisan.App.prompted) {
                Artisan.App.prompted = true;

                // Check to see if a previous bookmark was set
                if (Artisan.App.getBookmark()) {
                    this.bookmark = Artisan.App.getBookmark();

                    // make sure we only ask it once, so if the reinit() happens, we know their response
                    if (this.fromHash) {
                        // no prompt if the bookmark was hash-generated, that overrides everything
                        Artisan.App.returnToLast = true;
                    } else {
                        if (!confirm('You have previously been in this course.\r\nWould you like to return to the last visited location in the lesson?')) {
                            Artisan.App.returnToLast = false;
                        } else {
                            Artisan.App.returnToLast = true;
                        }
                    }
                }
            }

            // we do this via a flag set globally, so if we re-init, we don't prompt the user multiple times
            if (!Artisan.App.returnToLast) {
                this.bookmark = null;
            }

            if (this.bookmark) {
                var parsedBookmark = Artisan.App.parseBookmark(this.bookmark),
                    scoId = parsedBookmark.scoId,
                    loId = parsedBookmark.loId,
                    pageId = parsedBookmark.pageId;

                state.gameTotal = parsedBookmark.gameTotal ? parsedBookmark.gameTotal : state.gameTotal;

                if (scoId) {
                    var sco = Artisan.App.course.getSco(scoId);
                    if (sco) {
                        this.selectSco(sco);
                        if (loId) {
                            var lo = Artisan.App.course.getLearningObject(loId);
                            if (lo) {
                                this.selectLearningObject(lo);
                                loSelected = true;
                                if (pageId) {
                                    var page = lo.getPage(pageId);
                                    if (page && !lo.config.isQuiz) {
                                        this.selectPage(page);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!loSelected) {
                this.selectSco(Artisan.App.course.getFirstScoWithContent());
            }

            this.startTimer(Artisan.App.course);
        },
        removeImagePointers: function () {
            $('.artisan-image-pointer').remove();
            $('input.artisan-answer').val('');
        },
        addImagePointer: function (imageEl, x, y, questionContext) {
            var pointerId = 'artisan-image-question-pointer';
            var parent = $(imageEl).parent();
            parent.css({ position: 'relative' });
            //$('#' + pointerId).remove();
            var img = $('<img/>', {
                id: pointerId,
                src: 'cursor.png'
            }).addClass('artisan-image-pointer').css({
                position: 'absolute',
                left: x + 'px',
                top: y + 'px'
            }).appendTo(parent);

            var results = [];
            $('.artisan-image-pointer', questionContext).each(function () {
                var o1 = $(this).parent().offset();
                var o2 = $(this).offset();
                results.push({
                    x: (o2.left - o1.left),
                    y: (o2.top - o1.top)
                });
            });
            $('input.artisan-answer', questionContext).val(JSON.stringify(results));
        },
        applyPageState: function (page, learningObject, useCache) {
            var pageArray = [];

            if (learningObject.config.isSinglePage) {
                for (var i = 0; i < learningObject.config.combinedPages.length; i++) {
                    var p = learningObject.config.combinedPages[i];
                    pageArray.push(new Artisan.Models.Page(learningObject, p));
                }
            } else {
                pageArray.push(page);
            }

            for (var i = 0; i < pageArray.length; i++) {
                page = pageArray[i];

                var questionContext = $('.artisan-question-pageid-' + page.config.id);
                questionContext = questionContext.length ? questionContext : $(document);

                this.initMatchingQuestions(page, questionContext);

                var answeredQuestions = (useCache ? Artisan.App.course.record._answeredQuestionsCache : Artisan.App.course.record.answeredQuestions);
                var pastAnswer = $.grep(answeredQuestions, function (q) {
                    return (q.id == page.getId() && q.learningObjectId == learningObject.getId())
                })[0];

                if (pastAnswer) {
                    Artisan.App.applyResponse(page, pastAnswer.response, questionContext);
                    Artisan.App.gradePage(page, false, questionContext, pastAnswer);
                }
            }
        },
        applyResponse: function (page, response, questionContext) {

            var inputFields;
            if (page.isAllThatApply()) {
                inputFields = $('.artisan-answers input[type=checkbox]', questionContext);
                // adapt in case we encounter an old data chunk (w/ Short & Long)
                if (response.length > 0 && response[0].Short) {
                    response = $.map(response, function (r) { return r.Short });
                }

                var indicies = page.getAnswerIndexesFromSortIndexes(Artisan.Utilities.charsToIndicies(response));

                for (var i = 0; i < indicies.length; i++) {
                    if (inputFields[indicies[i]]) {
                        $(inputFields[indicies[i]]).attr('checked', 'checked'); // select the corresponding checkboxes
                    }
                }
            }
            else if (page.isMultipleChoice()) {
                inputFields = $('.artisan-answers input[type=radio]', questionContext);
                var index = page.getAnswerIndexFromSortIndex(Artisan.Utilities.charToIndex(response));
                if (inputFields[index]) {
                    $(inputFields[index]).attr('checked', 'checked'); // select the corresponding radio button
                }
            }
            else if (page.isTrueFalse()) {
                inputFields = $('.artisan-answers input[type=radio]', questionContext);
                var responseText = (response ? 'True' : 'False');
                var answeredInput = $.grep(inputFields, function (inpt) {
                    var labelEl = $(inpt).siblings('label')[0];
                    return ((labelEl.textContent || labelEl.innerText) == responseText);
                })[0];
                $(answeredInput).attr('checked', 'checked'); // select the corresponding radio button
            }
            else if (page.isImageQuestion()) {
                var image = $('.artisan-question-imagequestion img.artisan-answer', questionContext);
                for (var i = 0; i < response.length; i++) {
                    var point = response[i];
                    Artisan.App.addImagePointer(image, point.x, point.y);
                }
            }
            else if (page.isFillInTheBlank()) {
                inputFields = $('.artisan-answers input[type=text]', questionContext);
                for (var i = 0; i < response.length; i++) {
                    $(inputFields[i]).attr('value', response[i]);
                }
            }
            else if (page.isMatching()) {
                $.each(response, function (answerId, matchId) {
                    var match = $('#artisan-answer-left-' + matchId, questionContext);
                    var dropzone = $('#artisan-matching-dropzone-' + answerId, questionContext);
                    match.detach().appendTo(dropzone).css('left', 10).css('top', 10);
                });
            }
            else if (page.isCategoryMatching()) {
                $.each(response, function (answerId, matches) {
                    $.each(matches, function (i, matchId) {
                        var match = $('.artisan-category-match', questionContext).filter(function () {
                            return matchId == $(this).attr('id').replace('artisan-category-match-', '');
                        });
                        var dropzone = $('#artisan-category-dropzone-' + answerId, questionContext);

                        // Dummy container to maintain consistant order if answer randomization is on
                        var placeholder = $('<div>').attr('id', 'artisan-category-match-placeholder-' + matchId).addClass('placeholder artisan-category-match').css('display', 'none');

                        match.before(placeholder).detach().appendTo(dropzone).css('left', 5).css('top', 5);
                    });
                });
            }
            else if (page.isLongAnswer()) {
                inputFields = $('.artisan-answers textarea', questionContext);
                for (var i = 0; i < response.length; i++) {
                    $(inputFields[i]).val(response[i]);
                }
            }
        },
        gradePage: function (page, allowEmptyAnswer, questionContext, prevAnswer) {
            var userAnswers = [];
            var inputFields;
            var missingAnswer = false;

            if (!questionContext || !questionContext.hasClass('artisan-question')) {
                questionContext = $('.artisan-question');
            }

            questionContext.addClass('graded');

            if (page.isAllThatApply()) {
                inputFields = $('.artisan-answers input[type=checkbox]', questionContext);
                var selected = $('.artisan-answers input[type=checkbox]:checked', questionContext);
                for (var i = 0; i < selected.length; i++) {
                    userAnswers.push($(selected[i]).attr('value'));
                }
            } else if (page.isMultipleChoice() || page.isTrueFalse()) {
                inputFields = $('.artisan-answers input[type=radio]', questionContext);
                var selected = $('.artisan-answers input[type=radio]:checked', questionContext);
                for (var i = 0; i < selected.length; i++) {
                    userAnswers.push($(selected[i]).attr('value'));
                }
            } else if (page.isImageQuestion()) {
                inputFields = $('img.artisan-answer', questionContext);  // use the img element so it can be given the 'disabled' styling
                var answer = $('input.artisan-answer', questionContext).val();
                if (!answer) missingAnswer = true;

                if (answer.length) {
                    userAnswers = JSON.parse(answer);
                }

            } else if (page.isFillInTheBlank()) {
                inputFields = $('.artisan-answers input[type=text]', questionContext);
                for (var i = 0; i < inputFields.length; i++) {
                    var answer = $(inputFields[i]).attr('value');
                    if (!answer) missingAnswer = true;
                    userAnswers.push(answer);
                }
            } else if (page.isMatching()) {
                inputFields = $('.artisan-matching-dropzone', questionContext);
                for (var i = 0; i < inputFields.length; i++) {
                    var dropId = $(inputFields[i]).attr('id').replace('artisan-matching-dropzone-', '');
                    var dragId = 0;
                    var draggables = $(inputFields[i]).children('.artisan-matching-text-left');
                    if (draggables && draggables.length > 0) {
                        dragId = $(draggables[0]).attr('id').replace('artisan-answer-left-', '');
                    }
                    userAnswers.push({ answerId: dropId, userAnswer: dragId });
                }
            } else if (page.isCategoryMatching()) {
                inputFields = $('.artisan-category-match', questionContext);
                for (var i = 0; i < inputFields.length; i++) {
                    var dragId = $(inputFields[i]).attr('id').replace('artisan-category-match-', '');
                    var dropId = 0;
                    var dropzone = $(inputFields[i]).parent('.artisan-category-dropzone');
                    if (dropzone && dropzone.length > 0) {
                        dropId = $(dropzone[0]).attr('id').replace('artisan-category-dropzone-', '');
                    }
                    userAnswers.push({ matchId: dragId, userAnswer: dropId });
                }
            } else if (page.isLongAnswer()) {
                inputFields = $('.artisan-answer textarea', questionContext);
                var answer = $(inputFields[0]).attr('value');
                if (!answer) missingAnswer = true;

                userAnswers.push(answer);

                if (!Artisan.App.isValidLongAnswer(answer)) {
                    return userAnswers;
                }
                
            }

            // check the answers
            var results = page.checkAnswers(userAnswers);
            var instructorFeedback = (prevAnswer && prevAnswer.instructorFeedback) ? prevAnswer.instructorFeedback : '';

            if (prevAnswer && prevAnswer.isMarked && page.isLongAnswer()) {
                // special case, always allow re-entry
                if (!prevAnswer.isCorrect) {
                    $('.artisan-question-feedback', questionContext).show().addClass('incorrect').html(instructorFeedback);
                } else {
                    $('.artisan-question-feedback', questionContext).show().addClass('correct').html(instructorFeedback);
                    $('#artisan-question-submit', questionContext).attr('disabled', 'disabled').addClass('disabled');
                    $('#artisan-question-reset', questionContext).attr('disabled', 'disabled').addClass('disabled');
                    if (inputFields) {
                        inputFields.attr('disabled', 'disabled').addClass('disabled');
                    }
                    $('.artisan-next').removeClass('disabled');
                }
                return false;
            }

            // do not continue if the user hasn't entered an answer
            // EXCEPT - When AllowEmptyAnswer is True, or IsReviewPage is True
            if ((!userAnswers.length || missingAnswer) && !allowEmptyAnswer && !state.page.isReviewPage())
                return false;

            // Disable the submit button and input fields
            $('#artisan-question-submit', questionContext).attr('disabled', 'disabled').addClass('disabled');
            $('#artisan-question-reset', questionContext).attr('disabled', 'disabled').addClass('disabled');
            if (inputFields)
                inputFields.attr('disabled', 'disabled').addClass('disabled');

            if (!(state.learningObject.config.testType == ArtisanTestType.GameBoard) &&
                !(state.learningObject.config.testType == ArtisanTestType.GameWheel) &&
                (page.config.isGradable || !Artisan.App.minimumPageTimeout) &&
                this.isPageComplete()) {
                // Enable the Next button
                $('.artisan-next').removeClass('disabled');
            }


            var markingMethod = page.parent.config.markingMethod ? page.parent.config.markingMethod : Artisan.App.course.config.markingMethod;

            // Only display feedback if we are not using the hide all feedback mode. Hide all feedback will show nothing to the student.
            if (markingMethod !== ArtisanCourseMarkingMethod.HideAllFeedback) {

                // if we have any incorrect, show the incorrect feedback
                if (markingMethod !== ArtisanCourseMarkingMethod.HideAnswersAndFeedback) {
                    if (results.incorrect.length > 0) {
                        $('.artisan-question-feedback', questionContext).show().addClass('incorrect').html(page.getIncorrectFeedback(userAnswers));
                    } else {
                        $('.artisan-question-feedback', questionContext).show().addClass('correct').html(page.getCorrectFeedback(userAnswers));
                    }
                }

                // for each *incorrect* answer show the indicator as to right/wrong
                for (var i = 0; i < results.incorrect.length; i++) {
                    var clsToAdd = 'incorrect';
                    if (results.incorrect[i].isCorrect && (page.isAllThatApply() || page.isMultipleChoice() || page.isTrueFalse())) {
                        clsToAdd = 'correct';
                    }


                    // If its correct, and we have set HideAnswers to true, always ignore it
                    if (clsToAdd === 'correct' && (markingMethod === ArtisanCourseMarkingMethod.HideAnswers ||
                                                   markingMethod === ArtisanCourseMarkingMethod.HideAnswersAndFeedback)) {
                        continue;
                    }

                    // Otherwise, bail out on any non-review pages that where we don't have any answers specified. This would look
                    // ugly since we would see the incorrect result, and only happy green checkmarks on the screen
                    if (clsToAdd === 'correct' && !userAnswers.length && !state.page.isReviewPage()) {
                        continue;
                    }

                    // Should be safe to display whatever at this point
                    $('#artisan-answer-' + results.incorrect[i].id + '-feedback', questionContext).removeClass('incorrect correct').addClass(clsToAdd).show();
                }

                // for each *correct* answer the user did *not* select, show the indicator as to right/wrong
                for (var i = 0; i < results.correct.length; i++) {
                    if (results.correct[i].isCorrect) {
                        $('#artisan-answer-' + results.correct[i].id + '-feedback', questionContext).removeClass('incorrect correct').addClass('correct').show();
                    } else {
                        $('#artisan-answer-' + results.correct[i].id + '-feedback', questionContext).removeClass('incorrect correct').hide();
                    }
                }
            }

            Artisan.App.stopMaxiumumQuestionTimeout(page);

            // for image questions, highlight the correct regions on the image

            return userAnswers;
        },
        isPageComplete: function () {
            return $('.artisan-question').length === $('.artisan-question.graded').length;
        },
        processSurvey: function (page) {
            var userAnswers = [];
            var missingAnswer = false;

            var surveyItemIds = $('.artisan-survey-item').map(function () {
                return this.id.replace('artisan-survey-item-', '');
            }).get();

            var selected = $('.artisan-survey-item-value input[type=radio]:checked');
            for (var i = 0; i < surveyItemIds.length; i++) {
                var userAnswer = null;
                var selection = $.grep(selected, function (el, index) {
                    return el.name.replace('survey-', '') == surveyItemIds[i]
                });
                if (selection.length > 0) {
                    userAnswer = selection[0].value;
                }
                else {
                    missingAnswer = true;
                }
                userAnswers.push({ itemId: surveyItemIds[i], userAnswer: userAnswer });
            }

            // do not continue if the user hasn't entered all of the items
            if (!userAnswers.length || missingAnswer) {
                alert('The survey could not be submitted because an item was not answered.');
                return;
            }

            LMS.RecordFillInInteraction(page.getId(), JSON.stringify(userAnswers), true, null, ArtisanSectionType.Survey, ArtisanSectionType.Survey, ArtisanSectionType.Survey, null);

            var comments = $('#survey-comments').val();
            if (comments == "") { comments = "(none)"; }
            LMS.WriteComment(comments);

            // Disable the submit button and input fields
            $('#artisan-survey-submit').attr('disabled', 'disabled').addClass('disabled');
            var inputFields = $('.artisan-survey-item-value input[type=radio], textarea');
            if (inputFields)
                inputFields.attr('disabled', 'disabled').addClass('disabled');

            $('.artisan-next').removeClass('disabled');

            if (Artisan.App.course.isNavigationMethod()) {
                LMS.SetScore(100, 100, 100);
                LMS.SetPassed();
            }

            return userAnswers;
        },
        selectSco: function (sco, end) {
            if (Artisan.App.getState().sco && Artisan.App.getState().sco.getId() == sco.getId()) {
                return;
            }
            var learningObject = end ? sco.getLastSectionWithPages() : sco.getFirstSectionWithPages();

            // shouldn't ever happen...
            if (!learningObject) {
                return;
            }

            this.startTimer(sco, 'sco');

            // update the state to contain the new sco and learning object
            Artisan.App.updateState({
                sco: sco
            });
            this.selectLearningObject(learningObject, end);

            $('.artisan-sco-title .artisan-content').html(sco.getName());

            Artisan.App.course.record.addVisitedSco(sco.getId());
            $('#sco-' + sco.getId()).removeClass('disabled');
        },
        selectLearningObject: function (learningObject, end) {
            // ignore clicks on the same learning object and null los
            if (!learningObject || (Artisan.App.getState().learningObject && Artisan.App.getState().learningObject.getId() == learningObject.getId())) {
                return;
            }

            this.startTimer(learningObject, 'lo');

            Artisan.App.updateState({
                learningObject: learningObject,
                // if end is true, move to the last page; used when navigating backwards through a course
                page: end ? learningObject.getLastPage() : learningObject.getFirstPage()
            });

            $('.artisan-learningobject-title .artisan-content').html(learningObject.getName());
            if (learningObject.getName() == learningObject.getParent().getName()) {
                $('.artisan-learningobject-title').hide();
            } else {
                $('.artisan-learningobject-title').show();
            }

            Artisan.App.course.record.addVisitedLo(learningObject.getId());
            $('#learningobject-' + learningObject.getId()).removeClass('disabled');
        },
        retryLearningObject: function () {
            var learningObject = state.learningObject;

            // Clear out the randomized question order as we want to 
            // rerandomize the order when we retry the quiz. 
            for (var i = 0; i < learningObject.pages.length; i++) {
                if (learningObject.pages[i].config.randomizedOrder) {
                    delete learningObject.pages[i].config.randomizedOrder;
                }
            }

            Artisan.App.course.randomizePages(learningObject.config);
            learningObject = new Artisan.Models.Container(learningObject.config, learningObject.parent);
            
            Artisan.App.updateState({
                learningObject: learningObject,
                page: learningObject.getFirstPage()
            });

            $('.artisan-learningobject-title .artisan-content').html(learningObject.getName());
            if (learningObject.getName() == learningObject.getParent().getName()) {
                $('.artisan-learningobject-title').hide();
            } else {
                $('.artisan-learningobject-title').show();
            }
        },
        selectPage: function (page) {
            this.updateState({ page: page });
        },
        moveNext: function () {
            var nextPage = (state.page != null ? state.learningObject.getNextPage(state.page.getId()) : null);
            var nextLO = Artisan.App.course.getNextLearningObject(state.learningObject.getId());
            var nextSCO = Artisan.App.course.getNextSco(state.sco.getId());

            // Only do this when next is clicked. We only should record the page as 
            // visited once the pass the page using the next button.
            // Otherwise, the page would be marked as visited when moving backwards
            // thus allowing the user to move past by moving backwards then forwards.
            if (state.page != null) {
                Artisan.App.course.record.addVisitedPage(state.page.getId());
            }

            if (nextPage) {
                if (((state.learningObject.config.isQuiz && state.page.isReview && nextPage.isReview) || (!state.learningObject.config.isQuiz && nextPage.isReview)) && this.minLoTimer.isRunning()) {
                    alert("You have not met the minimum time requirement for this learning object. You have " + this.minLoTimer.getTimeRemaining() + " remaining.");
                    return;
                }
                if (((state.learningObject.config.isQuiz && state.page.isReview && nextPage.isReview) || (!state.learningObject.config.isQuiz && nextPage.isReview)) && this.minScoTimer.isRunning()) {
                    alert("You have not met the minimum time requirement for this SCO. You have " + this.minScoTimer.getTimeRemaining() + " remaining.");
                    return;
                }
                if (((state.learningObject.config.isQuiz && state.page.isReview && nextPage.isReview) || (!state.learningObject.config.isQuiz && nextPage.isReview)) && this.minCourseTimer.isRunning()) {
                    alert("You have not met the minimum time requirement for this content. You have " + this.minCourseTimer.getTimeRemaining() + " remaining.");
                    return;
                }
                this.selectPage(nextPage);
            }
            else if (nextLO) {
                if (this.minLoTimer.isRunning()) {
                    alert("You have not met the minimum time requirement for this learning object. You have " + this.minLoTimer.getTimeRemaining() + " remaining.");
                    return;
                }
                this.selectLearningObject(nextLO, false);
            }
            else if (nextSCO) {
                if (this.minScoTimer.isRunning()) {
                    alert("You have not met the minimum time requirement for this SCO. You have " + this.minScoTimer.getTimeRemaining() + " remaining.");
                    return;
                }
                if (this.minLoTimer.isRunning()) {
                    alert("You have not met the minimum time requirement for this learning object. You have " + this.minLoTimer.getTimeRemaining() + " remaining.");
                    return;
                }
                this.selectSco(nextSCO, false);
            }
            else if (Artisan.App.course.hasSurvey()) {
                this.selectSco(Artisan.App.course.getSurveySection());
            }
        },
        movePrevious: function () {
            var previousPage = state.learningObject.getPreviousPage(state.page.getId());
            var previousLO = Artisan.App.course.getPreviousLearningObject(state.learningObject.getId());
            var previousSCO = Artisan.App.course.getPreviousSco(state.sco.getId());
            if (previousPage) {
                this.selectPage(previousPage);
            } else if (previousLO) {
                this.selectLearningObject(previousLO, true);
            } else if (previousSCO) {
                this.selectSco(previousSCO, true);
            }
        },
        moveNextReview: function () {
            var questions = Artisan.App.course.record.getFailedQuestions();
            state.reviewQuestionIndex++;
            if (state.reviewQuestionIndex >= questions.length) {
                state.reviewQuestionIndex = questions.length - 1;
            }
            this.loadReviewPageQuestion(state.reviewQuestionIndex);
        },
        movePreviousReview: function () {
            var questions = Artisan.App.course.record.getFailedQuestions();
            state.reviewQuestionIndex--;
            if (state.reviewQuestionIndex < 0) {
                state.reviewQuestionIndex = 0;
            }
            this.loadReviewPageQuestion(state.reviewQuestionIndex);
        },
        loadReviewPageQuestion: function (questionIndex) {
            // only include questions they got wrong
            var questions = Artisan.App.course.record.getFailedQuestions();
            if (questions.length > 0) {

                var q = questions[state.reviewQuestionIndex];
                var learningObject = Artisan.App.course.getLearningObject(q.learningObjectId);
                var page = learningObject.getPage(q.id);

                // show the question
                $('#artisan-review-page-question').html(page.getRawHtml());

                // show the answer they picked
                // disable the answers
                Artisan.App.applyPageState(page, learningObject, true);

                // Randomize the answers if enabled
                Artisan.App.randomizeAnswers(page);

                // hide the submit button and feedback
                $('#artisan-question-submit').hide();
                $('.artisan-question-feedback').hide();

                // show the reference page
                var referencePage = Artisan.App.course.getPage(page.getReferencePageId());
                if (referencePage) {
                    $('#artisan-review-page-reference').html(referencePage.getHtmlContent());
                } else {
                    $('#artisan-review-page-reference').html('No reference page available.');
                }
            } else {
                $('#artisan-review-page-reference').hide();
                $('#artisan-review-page-question').hide();
            }

            // handle prev/next buttons
            $('.artisan-review-previous').removeClass('disabled');
            $('.artisan-review-next').removeClass('disabled');
            if (questionIndex == 0) {
                $('.artisan-review-previous').addClass('disabled');
            }
            if (questionIndex == questions.length - 1 || questions.length <= 1) {
                $('.artisan-review-next').addClass('disabled');
            }
        },
        onFirstPage: function () {
            // if there is no previous page, no previous LO, and no previous SCO, you are on the first page
            return !state.learningObject.getPreviousPage(state.page.getId())            // no previous page
		            && !Artisan.App.course.getPreviousLearningObject(state.learningObject.getId())  // no previous learning object
		            && !Artisan.App.course.getPreviousSco(state.sco.getId());                       // no previous sco
        },
        onLastPage: function () {
            if (Artisan.App.course.hasSurvey()) {
                return this.onSurveyPage();
            }

            // if there is no next page, no next LO, and no next SCO, you are on the last page
            return !state.learningObject.getNextPage(state.page.getId())            // no next page
		            && !Artisan.App.course.getNextLearningObject(state.learningObject.getId())  // no next learning object
		            && !Artisan.App.course.getNextSco(state.sco.getId());                       // no next sco

        },
        onSurveyPage: function () {
            return (state.page && state.page.config.pageType && state.page.config.pageType == ArtisanPageType.Survey)
        },
        getRecord: function () {
            return Artisan.App.course.record;
        },
        getState: function () {
            return state;
        },
        updateAutoLogout: function () {
            var me = this;

            // Set a timeout to logout the user after no activity
            if (Artisan.App.autoLogoutTimeout) {
                clearTimeout(Artisan.App.autoLogoutTimeout);
            }

            if (Artisan.App.course.config.inactivityTimeout > 0 && Artisan.App.course.config.timeoutMethod != ArtisanCourseTimeoutMethod.None) {
                Artisan.App.autoLogoutTimeout = setTimeout(function () {
                    var videos = $('video'),
                        cancelLogout = false;

                    if (Artisan.IsCourseTimingDisabled) {
                        return;
                    }

                    // Check if any videos are playing, if they are, then don't
                    // log them out yet as the user may be watching them and just
                    // not moving the mouse.
                    $.each(videos, function (i, v) {
                        if (!v.paused && !v.ended && v.currentTime < v.duration) {
                            cancelLogout = true;
                        }
                    });

                    if (cancelLogout) {
                        Artisan.App.updateAutoLogout();
                        return;
                    }

                    me.minCourseTimer.autoLogout(Artisan.App.course.config.inactivityTimeout);
                    me.minLoTimer.autoLogout(Artisan.App.course.config.inactivityTimeout);
                    me.minScoTimer.autoLogout(Artisan.App.course.config.inactivityTimeout);

                    if (Artisan.App.course.config.timeoutMethod == ArtisanCourseTimeoutMethod.ShowAlert) {
                        window.alert("You are now being logged out due to inactivity.");
                    }
                    top.close();



                }, Artisan.App.course.config.inactivityTimeout * 1000);
            }
        },
        updateMaxTestTimeout: function () {
            var currentSectionId = state.learningObject ? state.learningObject.getId() : null;

            if (currentSectionId != Artisan.App.maximumTestTimeSectionId || (state.page && state.page.isReview)) {

                // Clear the timeout as soon as the section changes
                if (Artisan.App.maximumTestTimeout) {
                    clearTimeout(Artisan.App.maximumTestTimeout);
                }

                // Restart the timeout as soon as the first question page is hit
                if (state.page && state.page.config.pageType == ArtisanPageType.Question) {
                    Artisan.App.maximumTestTimeSectionId = currentSectionId;

                    var isTest = state.learningObject ? state.learningObject.isTestSection() || state.learningObject.config.isQuiz : false;

                    var maxTime = state.learningObject.config.maxTime ?
                                        state.learningObject.config.maxTime :
                                        Artisan.App.course.config.maxTime;

                    if (isTest && maxTime > 0) {
                        Artisan.App.maximumTestTimeout = setTimeout(function () {
                            if (Artisan.IsCourseTimingDisabled) {
                                return;
                            }
                            top.close();
                        }, maxTime * 1000);
                    }
                }
            }
        },
        updateState: function (o) {
            var pastStateHadNoPage = !state.page;
            var pastStatePageId = (pastStateHadNoPage ? null : state.page.getId());
            var pastStateWasContentPage = (state.page && state.page.isContentPage());

            Artisan.App.state.isPreviousPageReview = state.page ? state.page.isReviewPage() : false;

            // handle specific scos, such as pre/post tests, objectives, and surveys
            Artisan.Utilities.extend(state, o);

            // Reset any timeouts waiting to expire for automatic logout after inactivity 
            Artisan.App.updateAutoLogout();

            if ((pastStateHadNoPage && o.page) || (o.page && o.page.getId() != pastStatePageId)) {
                var applyInserts = function () {
                    $('.artisan-page-insert').each(function () {
                        var $this = $(this);
                        if ($this.data('el')) {
                            $this.data('el').remove();
                        }
                        var $contents = $(Artisan.App.transformMarkup($this.html()));
                        $this.data('el', $contents);
                        if ($this.attr('position') == 'top') {
                            $($this.attr('applyTo')).prepend($contents);
                        } else {
                            $($this.attr('applyTo')).append($contents);
                        }
                    });
                };

                var isGameBoard = (state.learningObject.config.testType == ArtisanTestType.GameBoard);
                var isGameWheel = (state.learningObject.config.testType == ArtisanTestType.GameWheel);
                var isGamePage = (isGameBoard || isGameWheel) && !o.page.isIntroduction() && !o.page.isReviewPage();

                for (var i = 0; i < videos.length; i++) {
                    _V_(videos[i]).destroy();
                }
                videos = [];
                // a page was selected
                // if the "page" placeholder exists, throw all the content in it
                // if the "page" placeholder does *not* exist, look for the "page-content" placeholder, and
                // just put the content in it
                if (o.page.isContentPage() || o.page.isReviewPage() || $('.ContentWrapper').length == 0) {

                    $('#artisan-page').html(o.page.getHtml());

                    applyInserts();

                    // If flash is not supported, show any alternate assets
                    if (!swfobject.hasFlashPlayerVersion("1")) {
                        $('.artisan-asset-primary').hide();
                        $('.artisan-asset-alternate').show();
                    }

                    $('video').each(function () {
                        var vid = this, id = vid.id; // cache the id, the _V_ call can change it
                        _V_(vid.id, {}, function () {
                            this.play(); //autostart it
                        });

                        videos.push(id);
                    });

                    if (o.page.isReviewPage()) {
                        if (o.page.isPassed() || (Artisan.IsScormDisabled || Artisan.IsCourseUnlocked)) {
                            $('#artisan-page .failed').remove();
                            if (Artisan.App.hasAssignment(Artisan.App.course)) {
                                $('#artisan-page .passed.no-assignment').remove();
                            } else {
                                $('#artisan-page .passed.assignment').remove();
                            }
                        } else {
                            $('#artisan-page .passed').remove();
                        }
                        // load the first review page question
                        this.loadReviewPageQuestion(0);
                        state.reviewQuestionIndex = 0;

                        if (Artisan.App.course.config.reviewMethod == ArtisanCourseReviewMethod.HideReview) {
                            $('#artisan-review-page-question').remove();
                            $('.artisan-review-missed-questions-header').remove();
                            $('#artisan-review-page-reference').remove();
                            $('.artisan-review-inner').css('width', '100%');
                        }
                    }
                }
                else {
                    // Reset wrapper divs style because we are about to reuse them
                    if (pastStateWasContentPage) {
                        $('div.Main, div.MainWrapper, div.Content, div.ContentWrapper').removeAttr('style');
                    }

                    if (isGamePage) {
                        var $questionContainer = $('#game-question');
                        if ($questionContainer.length) {
                            $('#game-question').html(o.page.getRawHtml());
                        } else {
                            $('.ContentWrapper').html('<div id="game-question">' + o.page.getRawHtml() + '</div>');
                        }
                    } else if (o.page.config.isGroup) {
                        // Avoiding multiple content wrappers and scrollbars on each grouped content page.
                        var tempContent = $('<div>').html(o.page.getHtml());
                        var innerContent = tempContent.find('.ContentWrapper').html();
                        $('.ContentWrapper').html(innerContent);
                    } else {
                        $('.ContentWrapper').html(o.page.getRawHtml());
                    }

                    applyInserts();
                }

                $('div.ContentWrapper').append(Artisan.App.course.getPopupMenu());

                if ($.fancybox) {
                    $('#artisan-page .artisan-popup')
                        .fancybox({
                            openEffect: 'none',
                            closeEffect: 'none',
                            helpers: {
                                media: {}
                            }
                        });
                }

                if (isGamePage) {
                    if (o.page.isGameStart()) {
                        state.gameTotal = 0;
                        if (isGameWheel) {
                            Artisan.App.course.record.reset(); // reset the bookmark, clear the answered questions, etc; this ensures that no matter what, wheels always reset
                        }
                    }

                    if (isGameBoard) {
                        Artisan.App.renderGameBoard();
                    } else if (isGameWheel) {
                        Artisan.App.renderGameWheel();
                    }

                    $('.artisan-previous').addClass('disabled');
                    $('.artisan-next').addClass('disabled');
                }
                else {
                    // reset buttons
                    $('.artisan-previous').removeClass('disabled');
                    $('.artisan-next').removeClass('disabled').removeClass('exit').text('Next');

                    // disable the Previous button if on the first page
                    if (this.onFirstPage()) {
                        $('.artisan-previous').addClass('disabled');
                    }

                    Artisan.App.startMinimumPageTimeout(o);

                    // disable the Next button if on a question page or survey page
                    var isQuestion = o.page.config.questionType;
                    if (o.page.config.isGroup) {
                        for (var i = 0; i < o.page.parent.config.combinedPages.length; i++) {
                            if (o.page.parent.config.combinedPages[i].questionType) {
                                isQuestion = true;
                            }
                        }
                    }
                    if (isQuestion || o.page.config.pageType == ArtisanPageType.Survey) {
                        $('.artisan-next').addClass('disabled');
                    }

                    // Check for maximum question time
                    Artisan.App.startMaximumQuestionTimeout(o);

                    // on the last page turn it into an Exit button
                    if (this.onLastPage()) {
                        $('.artisan-next').addClass('exit').text('Exit');
                    }

                    // disable the Previous button for all review pages
                    if (o.page.isReviewPage()) {
                        $('.artisan-previous').addClass('disabled');
                        if (state.learningObject.config.isQuiz) {
                            if (o.page.isPassed()) {
                                $('.artisan-next').removeClass('disabled');
                            } else {
                                $('.artisan-next').addClass('disabled');
                            }
                        }
                    }

                    if (o.page.isSurveyPage()) {
                        // If survey has already been completed, disable it
                        var pastComments = LMS.GetComments();
                        if (pastComments != "") {
                            $('.artisan-survey-text').prepend('<p><strong>This survey has already been completed</strong></p><br />');
                            $('#artisan-survey-submit').attr('disabled', 'disabled').addClass('disabled');
                            var inputFields = $('.artisan-survey-item-value input[type=radio], textarea');
                            if (inputFields) {
                                inputFields.attr('disabled', 'disabled').addClass('disabled');
                            }

                            $('.artisan-next').removeClass('disabled');
                        }
                    }
                }
                // if previously answered, apply the past answer to the page
                Artisan.App.applyPageState(o.page, state.learningObject, false);

                // Handle randomized answers if enabled
                Artisan.App.randomizeAnswers(o.page);

                // always save the bookmark unless this is a gamewheel question or a review page or 
                // gamewheel state can only be bookmarked after a question is answered because the flash wheel can't resume state
                if ((!o.page.parent.isTestForceRestart() || o.page.isIntroduction()) && !(state.learningObject.config.testType == ArtisanTestType.GameWheel && o.page.isQuestion() || o.page.isReviewPage())) {
                    Artisan.App.setBookmark();
                }
            }

            Artisan.App.updateMenu(); // always rebuild the main menu


            if (state.learningObject) {
                // if the LO is the only LO in the SCO, and the names match, don't update it
                if (state.learningObject.getParent().getSections().length == 1 && state.learningObject.getName() == state.learningObject.getParent().getName()) {
                    $('#artisan-submenu').hide();
                    $('.SubNav').hide();
                }
                else {
                    Artisan.App.updateSubMenu(state.learningObject);
                    $('#artisan-submenu').show();
                    $('.SubNav').show();
                }

                // Show page numbers unless on a Gameboard or Gamewheel
                if (state.learningObject.config.testType == ArtisanTestType.GameBoard || state.learningObject.config.testType == ArtisanTestType.GameWheel) {
                    Artisan.App.removePageNumbers();
                }
            }
            else {
                Artisan.App.removePageNumbers();
            }

            if (o.page && o.page.parent.config.isSinglePage) {
                $('#Content').addClass('SinglePage');
            }


            if (Artisan.IsScormDisabled || Artisan.IsCourseUnlocked) {
                $('.artisan-next').removeClass('disabled');
            }

            if (o.page && o.page.parent && o.page.parent.isQuizComplete && o.page.parent.isQuizComplete()) {
                var lastPage = o.page.parent.getLastPage();
                if (lastPage.getId() != o.page.getId()) {
                    var skipQuizBtn = $('<a>').addClass('button artisan-skip-quiz').attr('href', '#').html('Skip Quiz').on('click', function () {
                        Artisan.App.selectPage(lastPage)
                    });

                    $('.artisan-next').after(skipQuizBtn);
                }
            }

            this.updateMaxTestTimeout();
        },
        initMatchingQuestions: function (page, questionContext) {
            if (!questionContext) {
                questionContext = '.artisan-question';
            }

            if (page.isMatching()) {
                $('.artisan-matching-text-left', questionContext).draggable({
                    revert: 'invalid',
                    cursor: 'move'
                });
                $('.artisan-matching-dropzone', questionContext).droppable({
                    accept: '.artisan-matching-text-left',
                    activeClass: 'droppable-active',
                    hoverClass: 'droppable-hover',
                    drop: function (event, ui) {
                        var dragId = $(ui.draggable).attr('id').replace('artisan-answer-left-', '');
                        var dropId = $(this).attr('id').replace('artisan-matching-dropzone-', '');
                        //alert("Drag ID: "+dragId+", Drop ID:"+dropId);
                        ui.draggable.detach().appendTo($(this)).css('left', 10).css('top', 10);
                    }
                });
            }
            else if (page.isCategoryMatching()) {

                $('.artisan-category-match', questionContext).draggable({
                    revert: 'invalid',
                    cursor: 'move'
                });
                $('.artisan-category-dropzone', questionContext).droppable({
                    accept: '.artisan-category-match',
                    activeClass: 'droppable-active',
                    hoverClass: 'droppable-hover',
                    drop: function (event, ui) {
                        ui.draggable.detach().prependTo($(this)).css('left', 5).css('top', 5);
                    }
                });
            }

        },
        getBookmark: function () {
            var bookmark = Artisan.App.course.record.getBookmark();
            if (window.top && window.top.location && window.top.location.hash) {
                /*
					potential bookmark override
				    hash must be in this format:
				        (/assign)?/sco/{id}/lo/{id}/page/{id}
				 */
                var isMatch = false;
                var re = /(assign\/)?sco\/(-?\d+)\/lo\/(-?\d+)\/page\/(-?\d+)/; // negative ids allowed, assignment toggle

                var converted = {};
                if (bookmark) {
                    converted = Artisan.App.course.record.parseBookmark(bookmark);
                }

                // "/sco/1/lo/2/page/3".match(/(assign\/)?sco\/(-?\d+)\/lo\/(-?\d+)\/page\/(-?\d+)/)         => ["sco/1/lo/2/page/3", undefined, "1", "2", "3"]
                // "/assign/sco/1/lo/2/page/3".match(/(assign\/)?sco\/(-?\d+)\/lo\/(-?\d+)\/page\/(-?\d+)/)  => ["/assign/sco/1/lo/2/page/3", "assign/", "1", "2", "3"]
                var match = window.top.location.hash.match(re);
                if (match.length == 5) {
                    var isAssignmentLink = typeof (match[1]) !== 'undefined';
                    var scoId = match[2];
                    var loId = match[3];
                    var pageId = match[4];

                    if (isAssignmentLink) {
                        // With assignments, we have either been given a page id, or a section id
                        if (pageId > 0) {
                            // Look up the learning object and session id
                            var page = Artisan.App.course.getPage(pageId);
                            loId = page.parent && page.parent.getId ? page.parent.getId() : -1;
                            scoId = page.parent && page.parent.parent && page.parent.parent.getId() ? page.parent.parent.getId() : -1;
                        } else if (loId > 0) {
                            // look up the section id and first assignment page id
                            var section = Artisan.App.course.getSection(loId);
                            scoId = section.parent && section.parent.getId ? section.parent.getId() : -1;
                            
                            for (var i = 0; i < section.pages.length && pageId == -1; i++) { // just grab first long answer page
                                if (section.pages[i].config.questionType == ArtisanQuestionType.LongAnswer) {
                                    pageId = section.pages[i].getId();
                                }
                            }
                        }
                    }

                    // make sure the bookmark hash is valid.
                    // if it isn't, ignore it and return original bookmark
                    if (scoId <= 0 || loId <= 0 || pageId <= 0) {
                        this.fromHash = false;
                        return bookmark;
                    }

                    converted.scoId = scoId;
                    converted.loId = loId;
                    converted.pageId = pageId;
                    converted.gameTotal = bookmark.gameTotal || 0;
                    converted.percent = bookmark.percent || 0;
                    this.fromHash = true;

                    bookmark = Artisan.App.course.record.serializeBookmark(converted);
                }

            }
            return bookmark;
        },
        setBookmark: function () {
            Artisan.App.course.record.setBookmark(state);
        },
        parseBookmark: function(bookmark) {
            return Artisan.App.course.record.parseBookmark(bookmark);
        },
        updateGameWheelTotal: function (value) {
            if (value >= 0) {
                state.gameTotal = value;
            }
            $('.game_total_value').html(state.gameTotal);
        },
        getCourseProgress: function() {
            var currentPage;
            var learningObject = this.getState().learningObject;
            var sco = this.getState().sco;
            var page = this.getState().page;
            var totalPages = learningObject.pages.length;

            for (var i = 0; i < totalPages; i++) {
                if (learningObject.pages[i].getId() == page.getId()) {
                    currentPage = i + 1; //translate from 0-based array index
                    break;
                }
            }

            var courseIndex = Artisan.App.course.getTotalPageIndex(learningObject.getId(), page.getId(), learningObject);
            var scoIndex = sco.getTotalPageIndex(learningObject.getId(), page.getId(), learningObject); 

            var progress = {
                coursePercent: Math.ceil((((courseIndex + 1) / Artisan.App.course.getTotalPageCount()) * 100)),
                scoPercent: Math.ceil((((scoIndex + 1) / sco.getTotalPageCount()) * 100)),
                loPercent: Math.ceil((currentPage / totalPages) * 100),
                currentPage: currentPage,
                totalPages: totalPages
            };

            this.getState().progress = progress;
            
            return progress;
        },
        transformMarkup: function (html) {
            var progress = this.getCourseProgress();

            html = Artisan.Utilities.replaceAll(html, '{!course_progress}', progress.coursePercent);
            html = Artisan.Utilities.replaceAll(html, '{!sco_progress}', progress.scoPercent);
            html = Artisan.Utilities.replaceAll(html, '{!learning_object_progress}', progress.loPercent);
            html = Artisan.Utilities.replaceAll(html, '{!current_page}', progress.currentPage);
            html = Artisan.Utilities.replaceAll(html, '{!total_pages}', progress.totalPages);

            return html;
        },
        renderGameWheel: function () {
            // if wheel is already rendered just update the total
            if ($('.artisan-game').length) {
                this.updateGameWheelTotal();
                return;
            }

            var swfDivId = "artisan-gamewheel-container";
            var swfId = "artisan-gamewheel-control";
            var path = "./flash/gamewheel.swf";
            var width = 320;
            var height = 320;

            var html = '<div class="artisan-game">';
            html += '\
                        <div style="text-align:center">\
                            <div id="GameWheelIndicator"></div>\
                            <div id="' + swfDivId + '">\
                                <p>Game Wheel</p>\
                            </div>\
                            <br/>\
                            <a class="button artisan-gamewheel-button">Spin the Wheel</a>\
                        </div>\
                    ';

            html += '<div class="game_total">Total: $<span class="game_total_value">' + state.gameTotal + '</span></div>';
            html += '</div>';

            $(html).prependTo('.ContentWrapper');

            var flashvars = {};
            var params = {
                quality: 'high',
                bgcolor: '#FFFFFF',
                wmode: 'transparent',
                allowScriptAccess: 'sameDomain'
            };
            var attributes = {
                id: swfId,
                name: swfId
            };
            swfobject.embedSWF(path, swfDivId, width, height, "10", "./flash/expressInstall.swf", flashvars, params, attributes);

            $('.artisan-gamewheel-button').live('click', function (e) {
                if ($(this).hasClass('disabled')) return;

                var movie = swfobject.getObjectById(swfId);

                // BE TODO: Flash API: rename sendToActionScript to something descriptive.
                // BE TODO: Flash API: why do we need to pass a var?
                movie.sendToActionScript(""); // this spins the wheel apparently

                $(this).addClass('disabled');
            });
        },
        setGameWheelAmount: function (value) {
            if (value == 'lose_it_all') {
                Artisan.App.updateGameWheelTotal(0);
                $('.artisan-gamewheel-button').removeClass('disabled');
            }
            else {
                // set the amount and display the question
                var page = state.learningObject.getNextPage(state.page.getId());
                page.amount = value;
                this.selectPage(page);
            }
        },
        renderGameBoard: function () {
            $('.artisan-game').remove();
            var finished = true;
            var rows = 5;
            var cols = 4;
            var html = '<div class="artisan-game">';
            html += '<table>';
            for (var i = 0; i < rows; i++) {
                html += '<tr>';
                for (var j = 0; j < cols; j++) {
                    var answered = false;
                    var index = (i * (rows - 1)) + j;
                    var quesCount = state.learningObject.pages.length - 3; // (p.0 = intro, p.1 = gameboard start, p.n = review)
                    var pageIndex = (index < quesCount ? index + 2 : -1);  // -1 if no page 
                    if (pageIndex >= 0) {
                        var page = state.learningObject.getPageAt(pageIndex);
                        var pastAnswer = $.grep(Artisan.App.course.record.answeredQuestions, function (q) {
                            return (q.id == page.getId() && q.learningObjectId == state.learningObject.getId())
                        })[0];
                        if (pastAnswer)
                            answered = true;
                        else // an unanswered question remains, so we aren't finished
                            finished = false;
                    }
                    var amount = (i + 1) * 100;
                    html += '<td><a id="game_button_' + index + '" pageIndex="' + pageIndex + '" amount="' + amount + '" class="game_button ' + (pageIndex < 0 ? 'off_button ' : '') + (answered ? 'answered ' : '') + 'game_button_col_' + (j + 1) + '">$' + amount + '</a></td>';
                }
                html += '</tr>';
            }
            html += '</table>';
            html += '<div class="game_total">Total: $<span class="game_total_value">' + state.gameTotal + '</span></div>';
            html += '</div>';

            $(html).prependTo('.ContentWrapper');

            return finished;
        },
        updateMenu: function () {
            var menu = [];
            Artisan.App.buildMenu(Artisan.App.course.getScos(), menu);
            $('#artisan-menu').html(menu.join('\r\n'));
            $('#artisan-menu').trigger('loaded'); // custom event handled by the theme in site.js
        },
        updateSubMenu: function (learningObject) {
            var submenu = [];
            Artisan.App.buildSubMenu(learningObject, submenu);
            var html = submenu.join('');
            $('#artisan-submenu').html(html);
            $('#artisan-submenu').trigger('loaded'); // custom event handled by the theme in site.js

        },
        buildMenu: function (scos, list, subItem) {
            for (var i = 0; i < scos.length; i++) {
                list.push('<ul class="' + (subItem ? '' : this.options.scoMenuClass) + '">');
                list.push('<li class="artisan-sco');
                if (Artisan.App.getState().sco && Artisan.App.getState().sco.getId() == scos[i].getId()) {
                    list.push(' selected');
                }
                if (Artisan.App.course.isSequentialNavigation()) {
                    // sequential navigation; enable only scos that have been started already through navigation
                    if (Artisan.App.course.record.hasVisitedSco(scos[i].getId()) || (i == 0 && !subItem)) {
                        // enabled
                        list.push(' enabled');
                    } else {
                        // disabled
                        list.push(' disabled');
                    }
                }
                var topItem = false;
                if (i == 0 && !subItem) {
                    topItem = true;
                    list.push(' topitem');
                }
                list.push('" scoId="' + scos[i].getId() + '" id="sco-' + scos[i].getId() + '">');
                if (topItem) {
                    list.push('<a href="#">' + 'Course Menu' + '</a>');
                } else {
                    list.push('<a href="#">' + scos[i].getName() + '</a>');
                }
                if (scos[i].scos) {
                    var subscos = scos[i].scos;

                    list.push('<ul>');
                    for (var j = 0; j < subscos.length; j++) {
                        var item = subscos[j];
                        list.push('<li class="artisan-sco');
                        if (Artisan.App.getState().sco && Artisan.App.getState().sco.getId() == item.getId()) {
                            list.push(' selected');
                        }
                        if (Artisan.App.course.isSequentialNavigation()) {
                            // sequential navigation; enable only scos that have been started already through navigation
                            if (Artisan.App.course.record.hasVisitedSco(item.getId()) || (j == 0 && !subItem)) {
                                // enabled
                                list.push(' enabled');
                            } else {
                                // disabled
                                list.push(' disabled');
                            }
                        }
                        list.push('" scoId="' + item.getId() + '" id="sco-' + item.getId() + '"><a href="#">' + item.getName() + '</a>');
                        if (item.scos) {
                            Artisan.App.buildMenu(item.scos, list, true);
                        }
                        Artisan.App.addSubMenu(item.getSections(), list);

                        list.push('</li>');
                    }
                    list.push('</ul>');
                }
                list.push('</li>');
                list.push('</ul>');
            }
        },
        addSubMenu: function (learningObjects, list) {
            // does nothing by default, some themes use this to build out a single-level menu system
	    //     // BC 8/15/2015 Added SubMenu option for full left menu
	    //     // var learningObjects = learningObject.getParent().getSections();
            list.push('<ul>');
            for (i = 0; i < learningObjects.length; i++) {
                var item = learningObjects[i];
                var index = (i == 0) ? 'first' : (i == learningObjects.length - 1) ? 'last' : '';
                list.push('<li class="artisan-learningobject');

                if (Artisan.App.course.isFullSequentialNavigation()) {
                    // full sequential navigation; enable only los that have been started already through navigation
                    if (Artisan.App.course.record.hasVisitedLo(learningObjects[i].getId()) || (i == 0)) {
                        // enabled
                        list.push(' enabled');
                    } else {
                        // disabled
                        list.push(' disabled');
                    }
                }

                if (Artisan.App.getState().learningObject && Artisan.App.getState().learningObject.getId() == item.getId()) {
                    list.push(' selected');
                }
                list.push(' ' + index + '" learningObjectId="' + item.getId() + '" id="learningobject-' + item.getId() + '"><a href="#">' + item.getName() + '</a></li>');
            }
            list.push('</ul>');
	    // End BC added SubMenu
        },
        /* builds the menu for a given LO; this includes all the other LOs at the same level */
        buildSubMenu: function (learningObject, list) {
            var learningObjects = learningObject.getParent().getSections();
            list.push('<ul class="' + this.options.loMenuClass + '">');
            for (i = 0; i < learningObjects.length; i++) {
                var item = learningObjects[i];
                var index = (i == 0) ? 'first' : (i == learningObjects.length - 1) ? 'last' : '';
                list.push('<li class="artisan-learningobject');

                if (Artisan.App.course.isFullSequentialNavigation()) {
                    // full sequential navigation; enable only los that have been started already through navigation
                    if (Artisan.App.course.record.hasVisitedLo(learningObjects[i].getId()) || (i == 0)) {
                        // enabled
                        list.push(' enabled');
                    } else {
                        // disabled
                        list.push(' disabled');
                    }
                }

                if (Artisan.App.getState().learningObject && Artisan.App.getState().learningObject.getId() == item.getId()) {
                    list.push(' selected');
                }
                list.push(' ' + index + '" learningObjectId="' + item.getId() + '" id="learningobject-' + item.getId() + '"><a href="#">' + item.getName() + '</a></li>');
            }
            list.push('</ul>');
        },
        removePageNumbers: function () {
            $('.artisan-page-numbers').html('');
        },
        showLoading: function () {
            $('#artisan_loading').show();
        },
        hideLoading: function () {
            $('#artisan_loading').hide();
        },
        randomizeAnswers: function (page) {
            // Ensure it's not a review page
            // Ensure randomizeAnswers is enabled
            // Ensure one of AllThatApply, MultiChoice, Matching, or CategoryMatching
            if (page.isReview || !(page.parent.config.isRandomizeAnswers && (page.isAllThatApply() || page.isMultipleChoice() || page.isMatching() || page.isCategoryMatching()))) {
                return;
            }

            // The first time we encounter this page, we randomize the order of the responses,
            // this order gets saved to o.page.config.
            // Next time we encounter the page, the saved order will be applied as opposed to a new order
            // this keeps the order consistent when moving back and forth, and in the review.
            // Previous resposnse first in a non-randomized state since if answer D is correct, and moved to the 
            // first position, the answer saved will be "D". This means the next time the page is loaded
            // the fourth position will be selected. If it is already randomized, then that 4th
            // position might not be D.

            var answerContainerSelector = '#Content ol.artisan-answers';
            var answerSelector = 'li';

            if (page.isMatching()) {
                answerContainerSelector = '#Content ul.artisan-answers:first-child';
                answerSelector = 'li.artisan-matching-answer.left';
            } else if (page.isCategoryMatching()) {
                answerContainerSelector = '#Content div.artisan-category-matches';
                answerSelector = 'div.artisan-category-match';
            }

            var answerList = $(answerContainerSelector);
            var answers = answerList.children(answerSelector);

            var currentIndex = answers.length;
            var randomizedOrder = new Array(answers.length);
            var currentAnswer, swapToIndex;

            if (answers.length) {
                if (page.config.randomizedOrder) {
                    for (var i = page.config.randomizedOrder.length - 1; i >= 0; i--) {
                        swapToIndex = page.config.randomizedOrder[i];
                        currentIndex = i;

                        currentAnswer = answers[currentIndex];
                        answers[currentIndex] = answers[swapToIndex];
                        answers[swapToIndex] = currentAnswer;
                    }
                } else {
                    while (currentIndex > 0) {
                        swapToIndex = Math.floor(Math.random() * currentIndex);
                        currentIndex--;

                        currentAnswer = answers[currentIndex];

                        answers[currentIndex] = answers[swapToIndex];
                        answers[swapToIndex] = currentAnswer;

                        randomizedOrder[currentIndex] = swapToIndex;
                    }
                    page.config.randomizedOrder = randomizedOrder;
                }

                answers.detach().prependTo(answerList);
            }
        },
        stopMinimumPageTimeout: function () {
            if (Artisan.App.minimumPageTimeout) {
                clearTimeout(Artisan.App.minimumPageTimeout);

                Artisan.App.minimumPageTimeout = null;

                $(".minimumpagetimeout-counter").remove();
                $(".artisan-next").show();
            }
        },
        showMinimumPageCountdown: function (remainingTime) {
            if (Artisan.App.minimumPageTimeout && remainingTime >= 0) {
                if (!$(".minimumpagetimeout-counter").length) {
                    // Make it look just like the existing disabled next button
                    $('.artisan-next').hide();
                    $('.artisan-next').after('<a class="button artisan-next disabled minimumpagetimeout-counter"></a>');
                }
                var minutes = Math.floor(remainingTime / 60),
                    seconds = remainingTime % 60,
                    display = minutes + ":" + (seconds < 10 ? "0" : "") + seconds;

                $(".minimumpagetimeout-counter").html(display);
                remainingTime--;

                setTimeout(function () {
                    Artisan.App.showMinimumPageCountdown(remainingTime);
                }, 1000);
            }
        },
        startMinimumPageTimeout: function (o) {
            Artisan.App.stopMinimumPageTimeout();

            if (Artisan.IsCourseTimingDisabled) {
                return;
            }

            if (
                !Artisan.App.course.record.hasVisitedPage(o.page.getId()) &&
                !o.page.config.isGradable &&
                (Artisan.App.course.config.minimumPageTime > 0 || o.page.parent.config.minimumPageTime > 0 || o.page.config.minimumPageTime > 0) &&
                (o.page.config.pageType == ArtisanPageType.Content || o.page.config.pageType == ArtisanPageType.Question) &&
                !o.page.isReviewPage()
            ) {
                $('.artisan-next').addClass('disabled');

                // Take first minimumPage time > 0. Order: page -> learning object -> course (course may have been overridden by artisan overrides)
                var timeoutSeconds =
                    o.page.config.minimumPageTime > 0 ?
                    o.page.config.minimumPageTime :
                        o.page.parent.config.minimumPageTime > 0 ?
                            o.page.parent.config.minimumPageTime :
                            Artisan.App.course.config.minimumPageTime;

                Artisan.App.minimumPageTimeout = setTimeout(function () {
                    if (o.page.config.pageType == ArtisanPageType.Question) {
                        if (Artisan.App.isPageComplete()) {
                            $('.artisan-next').removeClass('disabled');
                        }
                    } else {
                        $('.artisan-next').removeClass('disabled');
                    }

                    Artisan.App.stopMinimumPageTimeout();
                }, timeoutSeconds * 1000);

                Artisan.App.showMinimumPageCountdown(timeoutSeconds);
            }
        },
        stopMaxiumumQuestionTimeout: function () {
            if (Artisan.App.maximumQuestionTimeout) {
                clearTimeout(Artisan.App.maximumQuestionTimeout);
                Artisan.App.maximumQuestionTimeout = null;

                $(".maximumquestiontimeout-counter").remove();

            }
        },
        showMaxiumuQuestionCountdown: function (remainingTime) {
            if (Artisan.App.maximumQuestionTimeout && remainingTime >= 0) {
                if (!$(".maximumquestiontimeout-counter").length) {
                    $('#artisan-question-submit').after('<span class="maximumquestiontimeout-counter"></span>');
                }
                var minutes = Math.floor(remainingTime / 60),
                    seconds = remainingTime % 60,
                    display = "Time remaining: " + minutes + ":" + (seconds < 10 ? "0" : "") + seconds;

                $(".maximumquestiontimeout-counter").html(display);
                remainingTime--;

                setTimeout(function () {
                    Artisan.App.showMaxiumuQuestionCountdown(remainingTime);
                }, 1000);
            }
        },
        startMaximumQuestionTimeout: function (o) {

            Artisan.App.stopMaxiumumQuestionTimeout();

            // Training program override to disable all course timing
            if (Artisan.IsCourseTimingDisabled) {
                return;
            }

            if (o.page.config.questionType) {
                if (Artisan.App.course.config.maximumQuestionTime || o.page.parent.config.maximumQuestionTime) {

                    var timeoutSeconds = o.page.parent.config.maximumQuestionTime ?
                                                o.page.parent.config.maximumQuestionTime :
                                                Artisan.App.course.config.maximumQuestionTime

                    Artisan.App.maximumQuestionTimeout = setTimeout(function () {
                        if (Artisan.IsCourseTimingDisabled) {
                            Artisan.App.stopMaximumQuestionTimeout();
                            return;
                        }

                        $('.artisan-answer').each(function (i, o) {
                            $(o).prop('disabled', true);
                        });

                        var event = $.Event("click");
                        event.allowEmptyAnswer = true;
                        $('#artisan-question-submit').trigger(event);

                        Artisan.App.stopMaxiumumQuestionTimeout();

                    }, timeoutSeconds * 1000);

                    Artisan.App.showMaxiumuQuestionCountdown(timeoutSeconds);
                }
            }
        },
        hasAssignment: function (section) {
            if (section.pages) {
                for (var p = 0; p < section.pages.length; p++) {
                    if (section.pages[p].isLongAnswer()) {
                        return true;
                    }
                }
            }

            if (section.config.combinedPages) {
                for (var cp = 0; cp < section.config.combinedPages.length; cp++) {
                    if (section.config.combinedPages[cp].questionType == ArtisanQuestionType.LongAnswer) {
                        return true;
                    }
                }
            }

            if (section.sections) {
                for (var i = 0; i < section.sections.length; i++) {
                    var isAssignment = Artisan.App.hasAssignment(section.sections[i]);
                    if (isAssignment) {
                        return true;
                    }
                }
            }

            return false;
        },
        startTimer: function (o, type, timeoutFunction, intervalFunction) {
            if (Artisan.IsCourseTimingDisabled) {
                return;
            }

            if (type) {
                type = type.toLowerCase();
            }

            if (type != 'lo' && type != 'sco') {
                type = '';
            } else {
                type = type.charAt(0).toUpperCase() + type.slice(1);
            }

            var configValue = 'min' + type + 'Time',
                timer = 'min' + (type ? type : 'Course') + 'Timer',
                duration = o.config[configValue] ?
                            o.config[configValue] :
                            Artisan.App.course.config[configValue] ?
                            Artisan.App.course.config[configValue] :
                            0;

            if (duration) {
                this[timer].start(o.getId(), timeoutFunction, duration, intervalFunction);
            }
        },
        isValidLongAnswer: function(answer) {
            return escape(answer).length <= maxLongAnswerLength;
        },
        handleMaxTextLength: function (textbox) {
            var maxLength = maxLongAnswerLength,
                value = textbox.val(),
                encoded = escape(value), // Realize it's depricated, but SCORM is doing this somewhere to our
                                         // data before checking the maxLength. To prevent errors we need
                                         // to check max length on escaped data.
                currentLength,
                context = textbox.parent(),
                remaining,
                characterRemainingDiv = $('<div>');

            currentLength = encoded.length;


            remaining = maxLength - currentLength;
            
            context.find('.character-remaining').remove();

            characterRemainingDiv.addClass('character-remaining')
                                .html(remaining + ' Char. Remaining');
            

            if (currentLength > maxLength) {
                characterRemainingDiv.addClass('error');
            } else {
                characterRemainingDiv.removeClass('error');
            }

            textbox.after(characterRemainingDiv);
        },
        handleScormReconnect: function() {
            if (Artisan.App.scormFailure) {
                Artisan.App.scormFailure = false;
                $.fancybox.close();
                $.fancybox({
                    'content': 'Successfully Reconnected! You may now continue.'
                });
            }
        },
        handleScormFailure: function () {
            if (!Artisan.App.scormFailure) {
                Artisan.App.reconnectAttempts = 1;

                $.fancybox({
                    //'orig'			: $(this),
                    'content': '<div class="error" style="width: 200"><p>Your connection to the learning management system has been lost.<br/>Please check your internet connection and wait until the connection can be restablished or reload this course.</p><p class="attempts"></p><a href="#" class="cancel">Cancel</a></div>',
                    'padding': 10,
                    'title': 'Reconnecting...',
                    'showCloseButton': false,
                    'enableEscapeButton': false,
                    'transitionIn': 'elastic',
                    'transitionOut': 'elastic',
                    'titlePosition': 'inside',
                    'modal': true,
                    'width': 200,
                    'height': 200,
                    'titleShow': false,
                    'hideOnOverlayClick': false,
                    'hideOnContentClick': false
                });


                $('a.cancel').click(function () {
                    window.top.close();
                });

                Artisan.App.scormFailure = true;
            }

            if (!Artisan.App.nextCallTimeout) {
                $('p.attempts').html('Reconnecting: Attempt ' + Artisan.App.reconnectAttempts);
                Artisan.App.reconnectAttempts++;

                Artisan.App.nextCallTimeout = setTimeout(function () {
                    Artisan.App.nextCallTimeout = null;
                    window.top.API.SetDirtyData();
                    window.top.Control.Comm.SaveData();
                }, 10000);
            }

            
        },
        // Displays the version of the artisan course
        // at the bottom of the screen
        showVersion: function () {
            $('.version-info').remove();

            var vSymphony = Artisan.App.SymphonyVersion ? Artisan.App.SymphonyVersion : '0.0.0.0';
            var deliveredCoreVersion = Artisan.App.DeliveredCourse ? Artisan.App.DeliveredCourse.CoreVersion : '0.0.0.0';
            var currentCoreVersion = Artisan.App.CurrentCourse ? Artisan.App.CurrentCourse.CoreVersion : '0.0.0.0';
            var deliveredCourseVersion = Artisan.App.DeliveredCourse ? Artisan.App.DeliveredCourse.OnlineCourseVersion : 0;
            var currentCourseVersion = Artisan.App.CurrentCourse ? Artisan.App.CurrentCourse.OnlineCourseVersion : 0;

            var template = '<span class="{0}"><span class="label">{1}:</span><span class="value">{2}</span></span>';

            var versionInfo =
                Artisan.Utilities.format(template, "CourseVerstion", "Version", "v" + deliveredCourseVersion) +
                Artisan.Utilities.format(template, "LatestCourseVersion", "Latest Version", "v" + currentCourseVersion) +
                Artisan.Utilities.format(template, "SymphonyBuild", "Symphony Build", vSymphony) +
                Artisan.Utilities.format(template, "DeliveredBuild", "Course Build", deliveredCoreVersion) +
                Artisan.Utilities.format(template, "LatestBuild", "Latest Build", currentCoreVersion);
                

            $('<div>').addClass('version-info').html(versionInfo).appendTo($('body'));

        }
    };
}());


////////////////////////////////////////////////////////////////////////////
// Callback from flash.
////////////////////////////////////////////////////////////////////////////
function sendToJavaScript(value) {
    // BE TODO: rename to something descriptive.
    // BE TODO: the callback should be passed to the flash to decouple.

    // BE FIX: Normalize symbols.
    if (value === "Busto") {
        value = "lose_it_all";
    } else if (value === "Double Money") {
        value = "double_current_total";
    } else {
        value = parseInt(value.replace(/^\$/, ''));
        if (isNaN(value)) {
            value = 0;
        }
    }

    Artisan.App.setGameWheelAmount(value);
}
