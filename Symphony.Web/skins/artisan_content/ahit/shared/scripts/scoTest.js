/*  =================================
 AUTO-CLOSE ON SCO-EXIT IF SCO IS
 SHOWN IN A SEPARATE WINDOW is ENABLED
 in this custom version 'autoClose'
 ================================== */
function exitBasic(aSCO) {

	//       =========
	// Called when you exit from the pink-page
	
//	alert("exitBasic ::: " + aSCO );
	
    if (!confirmExit()) 
        return;
    toppen()["emergencyExit"] = false; 
	
	// Mark that a proper exit is happening
    /*  =================================
     AUTO-CLOSE IN SEPARATE WINDOW ENABLED:
     ================================= */
	
    if (closeit(500)) 
        return;
    
    
    
    var fset = theWINDOW.parent;
    var nurli = "../shared/html/scoOver.htm?cancel";
    aSCO.loadUrlInWindow(nurli, fset);
}





function exitVeryBasic(aSCO){
	
//	alert("exitVeryBasic ::: " + aSCO );
	
	// called when you SUBMIT a TEST.
    toppen()["emergencyExit"] = false; // a proper exit is happening
    if (closeit()) 
        return;
    var fset = theWINDOW.parent;
    var uru = document.location.toString();
    top['latestScoURL'] = uru;
    var nurli = "../shared/html/scoOver.htm?submit";
    
    aSCO.loadUrlInWindow(nurli, fset);
}


function toppen(){
    var toppen = window.top;
    if (window.opener) 
        toppen = window.opener.top;
    return toppen;
}


function scoLoaded(aWindow){
    theWINDOW = aWindow;
    toppen()["emergencyExit"] = true; // remains true if you close the window from 'x'
    if (THESCO == null) {
        THESCO = newSco(aWindow, scoInfo());
    }
    var msg = scoCustomInitialize();
    if (msg) {
        if (msg == "") 
            return;
        alert(msg);
        return
    }
    zooLMSInitializeBASIC();
    shufflePages1();
    
    if (!isFirstTime()) {
        contentFrame.location = "../shared/html/scoTestWelcomeBack.htm";
        navigationFrame.location = "../shared/html/scoBlank.htm";
    }
    else {
        contentFrame.location = "../shared/html/scoTestStart.htm";
        navigationFrame.location = "../shared/html/scoBlank.htm";
    }
}

/////////////////////////////////////////////////////////////////////////

function calculateScoreStats(){
    var rights;
    var score = 0;
    var answers = 0;
    var uas = uAnswersPRIVATE();
    var cas = caDict();
    aqsReset();
    
    var noqs = getNumberOfQuestions();
    var wrongAnswers = 0;
    var correctAnswers = 0;
    for (k in uas) {
        var givenAnswer = uas[k];
        var hasAnswer = true;
        
        if (givenAnswer == 'LESSON') 
            hasAnswer = false;
        if (givenAnswer == 'EXERCISE') 
            hasAnswer = false;
        if (givenAnswer == '') 
            hasAnswer = false;
        if (hasAnswer) {
            aqsInc();
            var correctAnswer = cas[k];
            
            if (givenAnswer == correctAnswer) {
                if (!givenAnswer == "") {
                    score++;
                    correctAnswers++;
                }
                else {
                    wrongAnswers++
                }
            }
            else {
                wrongAnswers++
            }
        }
    }
    var noAnswers = noqs - correctAnswers - wrongAnswers;
    return [correctAnswers, wrongAnswers, noAnswers];
}


function tdStyle(){

    var result = ' style="margin-left:12;font-family:arial;" '
    return result;
}

function scoGetQuestionResults(){
    var resultsArray = calculateScoreStats();
    var HTML = ' <br>Here are your results: <p>';
    
    HTML += '<table cellpadding=0 border=0 ' + tdStyle() + '>';
    HTML += '<tr><td>Correct answers:</td><td align="right">' + resultsArray[0] + " </td></tr>";
    HTML += '<tr><td>Incorrect answers:&nbsp;&nbsp;&nbsp;&nbsp; </td><td align="right">' + resultsArray[1] + "</td></tr>";
    HTML += '<tr><td>Unanswered: </td><td align="right">' + resultsArray[2] + "</td></tr>";
    HTML += '<tr><td>SCORE: </td><td align="right">' + scoGetScore() + '</td><td>%</td></tr>';
    HTML += '</table>\n';
    return HTML;
}


function alertWasSubmitted(){

    var runningOnSpido = parent.location.toString().match(/\/spido\//);
    if (runningOnSpido) 
        return;
    
    // panumod: for running the sco in a separate window.
    runningOnSpido = parent.location.toString().match(/\/lms-courses\//);
    if (runningOnSpido) 
        return;
    
    var str = " Thank you for submitting the test '" +    courseTitle2() +  ".' ";
    str += scoGetQuestionResults();
    str += '<p>To see the corrected version of the test and \
			 review your answers, go back to the course and \
			 select the test you just took.  \
			 Then click "Review Test."';
    str += "<hr width='99%'><div style='text-align:left'><tt>" + (new Date).toString() + "</tt></div>";

	openNotif(str);

}

function openNotif(str){
	
    var features = "left=4,top=4,width=322,height=306,scrollbars=no,dependent=yes,directories=no,menubar=no,status=no,location=no,toolbar=no,resizable=yes";
    var url = "";
    var win = window.open(url, "Nfier", features);
    
    var headHTML = '<html><head><title>' + courseTitle2() + '</title>	\n\
				<STYLE TYPE="text/css">	\n\
				 BODY { font-family:arial; background-color: white}	\n\
				</STYLE>\n</head>\n';
    
    var bodyHTML = '<body>' + str + "\n</body></html>\n";
	
	if (win) {
		win.document.writeln(headHTML + bodyHTML);
		win.document.close();
		win.focus();
	}
    // win.defaultStatus = "SCORE: " + scoGetScore() + "% &nbsp;&nbsp;"; 
}



function confirmSubmit(anSCO, aNumber){
    var qs = ' questions.'
    if (aqs() == 1) 
        qs = ' question.'
    var msg1 = "You answered " +
    aqs() +
    qs +
    "\n\nDo you want to submit the test?";
    if (!confirm(msg1)) {
        return false;
    }
    return true;
}





function noOfInstancesMax(){
    return 10;
}

function scoGetSessionTime(){


    var pd = propsDict();
    var sessionTime = pd['sessionTime'];
    return sessionTime;
}

function markSessionTime(){
    var pd = propsDict();
    var dat = getSessionTimeTotalSeconds();
    pd['sessionTime'] = dat;
}

function markSubmitTime(){
//	alert("markSubmitTime");
    var pd = propsDict();
    var dat = (new Date()).toString();
    pd['submitTime'] = dat;
    markSessionTime();
}

function scoGetDate(){
    var pd = propsDict();
    var submitTime = pd['submitTime'];
    return submitTime;
}

function currentPageIndexIs(anInteger){
    currentPageIndexVAR = anInteger - 1 + 1;
    
    
    
    
}

function isTest(){
    return true;
}



function confirmExitWithoutSaving(){
    var ok = confirm("Do you want exit without submitting the test?");
    return ok;
}

function confirmReview(){
    var wantsReview = confirm("Welcome back.\n\n" +
    "Click 'OK' to review previously completed test.\n" +
    "Click 'Cancel' to take this test again.");
    return wantsReview
}

function alertFirstPage(){
    alert("This is the first question of the test");
}

function confirmExit(){
    return true
    
}

function alertAlreadySubmitted(){
    alert("You may not change your answers while reviewing a submitted test.");
}


function scoGetUserName(){
    var nam = zooLMSGetValue('cmi.core.student_name');
    return nam;
}

function scoGetScore(){
    var nzedScore = calculateNormalizedScore();
    return nzedScore;
}

var hist_VAR = new Array();
hist_VAR[0] = 0;

function nf(){
    var nf = parent.navigationFrame;
    if (nf == null) 
        nf = navigationFrame;
    if (nf == null) 
        nf = document.navigationFrame;
    return nf;
}

function sendDataToLMS(scoreString, statusString){
    zooSendScoreToLMS(scoreString);
    zooSendStatusToLMS(statusString);
    var encString = encodeDicts();
    zooLMSSetValue("cmi.suspend_data", encString);
}

function commitAnswers(){
    var nzedScore = calculateNormalizedScore();
    var status = 'completed';
    sendDataToLMS(nzedScore, status);
}

function submitButtonClicked(){
//	alert("submitButtonClicked");
    var theSco = getSco();
    var score = calculateScore();
    if (!confirmSubmit(theSco, score)) 
        return false;
    markSubmitTime();
    scoSubmit();
    var code = zooLMSGetLastError();
    if (!(code == 0)) {
        alert("Your test could not be submitted," +
        "\nperhaps due to a timeout! \n\nError Code = " +
        code);
        exitVeryBasic(theSco);
        
        return;
    }
    alertWasSubmitted();
    exitVeryBasic(theSco);
}

var wasInResultsModeVAR = false;
function wasInResultsMode(){
    return wasInResultsModeVAR;
}

function rememberNonResultsMode(){
    wasInResultsModeVAR = false;
}

function rememberResultsMode(){
    wasInResultsModeVAR = true;
}

function goBackInTest(){
    if (wasInResultsMode()) {
        goToResultsMode();
        return;
    }
    var navframe = nf();
    var qn = navframe.getQuestionNumber();
    goToReviewMode();
    navframe.scoGoToQuestion(qn);
}

function goToResultsMode(){
//	alert("goToResultsMode");
    var navframe = nf();
    goToReviewMode();
    
    navframe.setState(3);
    setState('results');
    var winloc = document.location.toString();
    var locparts = winloc.split('/');
    var fileName = locparts[locparts.length - 1];
    var dirUrl = winloc.replace(fileName, '');
    var newloc = dirUrl + "../shared/html/scoTestTOC.htm";
    cf().location = newloc;
    
    
    rememberResultsMode();
}

function cf(){
    if (this.contentFrame) 
        return this.contentFrame;
    return parent.contentFrame
}

function goToRemediationMode(){
    var navframe = nf();
    var qn = navframe.getQuestionNumber();
    var idex = 'i' + ixDict()[qn];
    var qurl = scoInfo()[qn];
    
    var key = qurl.replace(/\/i\w*\//, '/i0001/');
    var lurl = lessonMap()[key];
    
    
    var winloc = document.location.toString();
    var locparts = winloc.split('/');
    var fileName = locparts[locparts.length - 1];
    var dirUrl = winloc.replace(fileName, '');
    var newloc = dirUrl + lurl;
    cf().location = newloc;
    
    navframe.setState(4);
    setState('remediation');
    
    navframe.refreshNavigationBar();
    rememberNonResultsMode();
    
}

var fsetStateVAR;
function setState(aString){

    fsetStateVAR = aString;
}

function getState(aString){
    return fsetStateVAR;
}

function isReviewMode(){
    var result = (getState() == 'review');
    
    return result;
}

function goToReviewMode(){
    var navframe = nf();
    var cas = caDict();
    var nqs = getNumberOfQuestions();
    for (i = 1; i <= nqs; i++) {
        var ua = uAnswersAt(i);
        var ca = cas[i];
        var state = 1;
        if (ca == ua) {
            state = 3
        }
        else {
            state = 4
        };
        if (ua == "") 
            state = 5;
        if (ua == "EXERCISE") 
            state = 5;
        if (ua == null) 
            state = 5;
        navframe.setPageNoToState(i, state);
    }
    setState('review');
    navframe.setState(2);
    navframe.refreshNavigationBar();
    cf().document.location = cf().document.location;
}

function setPageNoToState(pindex, stateInteger){
    var nframe = nf();
    var oldState = nframe.getPageNoState(pindex);
    if (stateInteger == oldState) {
        return;
    }
    nframe.setPageNoToState(pindex, stateInteger);
}

function exitButtonClicked(){
    var theSco = getSco();
    exitBasic(theSco)
}

function exitWithoutSaving(){
//	alert("exitWithoutSaving");
    if (!confirmExitWithoutSaving()) 
        return;
    var theSco = getSco();
    var fset = theWINDOW.parent;
    var nurli = "../shared/html/scoOver.htm";
    theSco.loadUrlInWindow(nurli, fset);
}

function deleteButtonClicked(){
    exitWithoutSaving();
}

function resultsButtonClicked(){
    goToResultsMode();
}

function firstAidButtonClicked(){
    goToRemediationMode();
}

function resfreshButtonClicked(){
    cf().document.location = cf().document.location;
}

function setXPrevious(anInteger){
    xPrevious = anInteger;
}

var xPrevious = 5;
function getXPrevious(){
    return xPrevious
}

var Modulus = 2147483647;
function pageId(){
    var pno = pageNumb();
    var pindex = parent.pageNoToIndexMap()[pno];
    return pindex;
}

function pageIdIs(aString){
    pageId_VAR = aString;
}

function prind(){
    var maxi = noOfInstancesMax();
    var rindex = rand(maxi) + 1;
    var pad = rindex.toString() + "";
    var res = "";
    if (pad.length == 3) {
        res = '0' + pad;
    }
    if (pad.length == 2) {
        res = '00' + pad;
    }
    if (pad.length == 1) {
        res = '000' + pad;
    }
    return res;
}


var seedFloatVAR = null;
var firstTimeVAR = false;
function getRandomSeed(){
    if (seedFloatVAR) {
        return seedFloatVAR;
    }
    seedFloatVAR = getLmsRandomSeed();
    if (seedFloatVAR) {
        firstTimeVAR = false;
        initRand(seedFloatVAR);
        setLmsRandomSeed(seedFloatVAR);
        return seedFloatVAR;
    }
    
    setFirstTime();
    seedFloatVAR = Math.random();
    setLmsRandomSeed(seedFloatVAR);
    initRand(seedFloatVAR);
    return null;
}

function isFirstTime(){
    var result = isFirstTime.value;
    if (result == null) {
        return false;
    }
    {
        return result;
    }
}

function setFirstTime(){
    isFirstTime.value = true;
    
}


function initRand(seedFloat){
    if (seedFloat == null) {
        seedFloat = Math.random();
    }
    seedFloatVAR = seedFloat;
    setXPrevious(Math.round(Modulus * seedFloat));
}

function randomPermutation(size){
    var bins = size;
    var dones = new Object();
    var str = "";
    var arr = new Array();
    var arri = 1;
    arr[0] = size;
    for (i = 1; i <= 999; i++) {
        rint = rand(size) + 1;
        if (dones[rint] == rint) {
        }
        else {
            arr[arri] = rint;
            arri++;
            dones[rint] = rint;
            if (arri == size + 1) {
                return arr;
            }
        }
    }
    return arr;
}

function rand(noOfBins){
    getRandomSeed();
    var Multiplier = 1277;
    var Increment = 0;
    if (noOfBins == null) 
        noOfBins = 10;
    var xNext0 = Multiplier * getXPrevious() + Increment;
    var xNext = xNext0 % Modulus;
    setXPrevious(xNext);
    var rfloat = xNext / (Modulus - 1);
    var rint = Math.round(rfloat * noOfBins - 0.5);
    return rint;
}


function doRetake(){
    var pd = propsDict();
    for (k in pd) {
        pd[k] = null; // panulos
    }
    
    navigationFrame.location = "scoTestNavigationFrame.htm";
    
    setState('test');
    var ixd = ixDict();
    for (i in ixd) {
        ixd[i] = null;
    }
    var olds = uAnswersPRIVATE();
    for (j in olds) {
        olds[j] = "";
        
    }
    seedFloatVAR = Math.random();
    setLmsRandomSeed(seedFloatVAR);
    initRand(seedFloatVAR);
    restoreInstanceIdsBASIC();
    shufflePages2();
    getSco().loadYourNextPage();
}

function doReview(){

    setNavbarLocation(2);
    if (!isNavbarLoaded()) {
        setTimeout("doReview2()", 400);
        return;
    }
    
    restoreInstanceIds();
    goToReviewMode();
    getSco().loadYourNextPage();
    
}

function doReview2(){
    if (!isNavbarLoaded()) {
        setTimeout("doReview2()", 400);
        return;
    }
    restoreInstanceIds();
    shufflePages2();
    goToResultsMode();
}


function doReviewBasic(){
    restoreInstanceIds();
    goToReviewMode();
    getSco().loadYourNextPage();
}

function doNotFirstTime(){
    if (true) 
        return;
    
    
    var doReviewVAR = false;
    doReviewVAR = confirmReview();
    var retake = !doReviewVAR;
    if (retake) {
        doRetake()
    }
    else {
        doReview()
    }
}

function doFirstTime(){

}

function doContinue(){
    setNavbarLocation(1);
    seedFloatVAR = Math.random();
    setLmsRandomSeed(seedFloatVAR);
    initRand(seedFloatVAR);
    
    doContinue2();
}


function doContinue2(){
    if (!isNavbarLoaded()) {
    
    
    }
    restoreInstanceIds();
    var olds = uAnswersPRIVATE();
    for (k in olds) {
        olds[k] = "";
    }
    shufflePages2();
    getSco().loadYourNextPage();
}

function navbarLoaded(){



    navbarLoadingVAR = false;
    
}

var navbarLoadingVAR = false;
var navbarType = 1;
function setNavbarLocation(type){



    navbarLoadingVAR = true;
    navbarType = type;
    
    if (navbarType == 1) {
        navigationFrame.location = "scoTestNavigationFrame.htm";
    }
    else 
        if (navbarType == 2) {
            navigationFrame.location = "scoTestReviewNavigationFrame.htm";
        }
}

function isNavbarLoaded(){
    if (navbarLoadingVAR) 
        return false;
    var loc = navigationFrame.location.toString();
    
    if (navbarType == 1) {
        var m = loc.match("scoTestNavigationFrame.htm");
    }
    else 
        if (navbarType == 2) {
            var m = loc.match("scoTestReviewNavigationFrame.htm");
        }
    
    if (m == null) 
        return false;
    return true;
}

function shufflePages1(){
    var doReview = false;
    var seed = getRandomSeed();
    
    
    
}

function shufflePages2(){

    if (!isFirstTime()) {
        doNotFirstTime()
    }
    else {
        doFirstTime()
    };
    shuffleBasic();
    
}

function shuffleBasic(){
    var noOfVisibleQuestions = getNumberOfQuestions();
    var qlimitVAR = qlimit();
    var str = "";
    var permutation;
    
    
    
    
    var urls = new Array();
    urls[0] = courseTitle();
    var permutation = randomPermutation(qlimit());
    for (i = 1; i <= qlimitVAR; i++) {
        urls[i] = pageUrlAt(permutation[i]);
        str += urls[i] + "\n"
    }
    
    
    var noOfPages = scoInfo().length - 1;
    permutation = randomPermutation(noOfPages - qlimitVAR);
    str = "";
    for (i = 1; i <= noOfPages; i++) {
        rindex = permutation[i];
        urls[i + qlimitVAR] = pageUrlAt(rindex + qlimitVAR);
        str += urls[i] + "\n"
    }
    
    var urls2 = new Array();
    urls2[0] = courseTitle();
    var permutation = randomPermutation(noOfVisibleQuestions);
    str = "";
    for (i = 1; i <= noOfVisibleQuestions; i++) {
        rindex = permutation[i];
        urls2[i] = urls[rindex];
        str += urls2[i] + "\n"
    }
    
    relativeUrlPathVAR = urls2;
}
