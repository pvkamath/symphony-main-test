// scoTestTOC.js
// all the functions needed for the creation of the TOC
// in exercises

// ***************************************************************************************
// Test TOC HTML generator
// 
// ***************************************************************************************


function getLessonTitle(qNumber){
    var qURL = parent.scoInfo()[qNumber];
    var key = qURL.replace(/\/i\w*\//, '/i0001/');
    var lURL = parent.lessonMap()[key];
    
    if (lURL == null) {
        key = key.replace(/N_/, '');
        lURL = parent.lessonMap()[key];
    }
    var lTitle = parent.lessonTitleMap()[lURL];
    return lTitle;
}

function scoGetLessonFromQuestionNumber(qNumber){
    var navframe = parent.nf();
    
    var qn = qNumber;
    var idex = 'i' + parent.ixDict()[qn];
    var qurl = parent.scoInfo()[qn];
    
    parent.navigationFrame.setState(4);
    parent.navigationFrame.refreshNavigationBar();
    
    var key = qurl.replace(/\/i\w*\//, '/i0001/');
    var lurl = parent.lessonMap()[key];
    
    if (lurl == null) {
        key = key.replace(/N_/, '');
        lurl = parent.lessonMap()[key];
    }
    
    var winloc = parent.document.location.toString();
    var locparts = winloc.split('/');
    var fileName = locparts[locparts.length - 1];
    var dirUrl = winloc.replace(fileName, ''); // #sco144
    var newloc = dirUrl + lurl;
    parent.cf().location = newloc;
}

///////////////// end sco444 , making test-toc show ok on Netscape ////////////////////////////



// *********************************************************************************************************************************
// *********************************************************************************************************************************
// functions for the ellipses

function ellipseTitle(titleString){
    if (!titleString) 
        return titleString;
    var maxedOutString = '';
    var stringMaxLength = 48;
    var titleLength = titleString.length;
    
    if (titleLength > stringMaxLength) {
        maxedOutString = titleString.slice(0, stringMaxLength - 3) + "...";
    }
    else {
        maxedOutString = titleString;
    }
    
    return maxedOutString;
    
}

function showTooltip(evt, oi, linkText){

    if (!isIE) 
        return false;
    MM_setTextOfLayer('t1', '', linkText);
    popUp(evt, oi);
    return true;
}

var DH = 0;
var an = 0;
var al = 0;
var ai = 0;
if (document.getElementById) {
    ai = 1;
    DH = 1;
}
else {
    if (document.all) {
        al = 1;
        DH = 1;
    }
    else {
        browserVersion = parseInt(navigator.appVersion);
        if ((navigator.appName.indexOf('Netscape') != -1) && (browserVersion == 4)) {
            an = 1;
            DH = 1;
        }
    }
}

function fd(oi, ws){
    if (ws == 1) {
        if (ai) {
            return (document.getElementById(oi).style);
        }
        else {
            if (al) {
                return (document.all[oi].style);
            }
            else {
                if (an) {
                    return (document.layers[oi]);
                }
            };
                    }
    }
    else {
        if (ai) {
            return (document.getElementById(oi));
        }
        else {
            if (al) {
                return (document.all[oi]);
            }
            else {
                if (an) {
                    return (document.layers[oi]);
                }
            };
                    }
    }
}

function pw(){
    if (window.innerWidth != null) 
        return window.innerWidth;
    if (document.body.clientWidth != null) 
        return document.body.clientWidth;
    return (null);
}

function popUp(evt, oi){
    if (DH) {
        var wp = pw();
        ds = fd(oi, 1);
        dm = fd(oi, 0);
        st = ds.visibility;
        if (dm.offsetWidth) 
            ew = dm.offsetWidth;
        else 
            if (dm.clip.width) 
                ew = dm.clip.width;
        if (st == "visible" || st == "show") {
            ds.visibility = "hidden";
        }
        else {
            if (evt.y || evt.pageY) {
                if (evt.pageY) {
                    tv = evt.pageY + 10;
                    lv = evt.pageX - (ew / 4) + 50;
                }
                else {
                    tv = evt.y + 10 + document.body.scrollTop;
                    lv = evt.x - (ew / 4) + document.body.scrollLeft + 50;
                }
                if (lv < 2) 
                    lv = 2;
                else 
                    if (lv + ew > wp) 
                        lv -= ew / 2;
                ds.left = lv;
                ds.top = tv;
            }
            ds.visibility = "visible";
        }
    }
}

// *********************************************************************************************************************************
// *********************************************************************************************************************************


function getStateImage(qNumber){
    // STATES: 
    // notchanged = 1 (square)
    // changed = 2 (square - red dot)
    // correct = 3 (check)
    // incorrect = 4 (x)
    // noanswer = 5 (question mark)
    qStateImageArray = ["correctTOC.gif", "incorrectTOC.gif", "noanswerTOC.gif"];
    state = getQuestionState(qNumber);
    switch (state) {
        case 3:
            return 'images/' + qStateImageArray[0];
            break;
        case 4:
            return 'images/' + qStateImageArray[1];
            break;
        default:
            return 'images/' + qStateImageArray[2];
    }
}

function unQuote(text){
    // &#39; quote code
    return text.replace(/'/g, "\\'");
}


function createTOCRow(qNumber){
    // question data
    // status,noQuestion,qURL,pageTitle,pURL
    // status :
    //          1 : correct
    //          2 : incorrect
    //          3 : unanswered
    //alert("createTOC called!!!");
    //if (!quesionData) return 'no data available to create the toc in this page';
    
    var imagesFolder = 'images/';
    //var alternateSwitch =  (quesionData.noQuestion % 2)?'1':'2';
    var alternateSwitch = (qNumber % 2) ? '1' : '2';
    var columnWidths = [6, 4, 22, 2, 5, 120, 2, 5, 300, 6];
    var openRowGraphic = imagesFolder + 'openhl' + alternateSwitch + '.gif';
    var closeRowGraphic = imagesFolder + 'closehl' + alternateSwitch + '.gif';
    var tableBackGraphic = imagesFolder + 'highlight' + alternateSwitch + '.gif';
    var sGraphic = getStateImage(qNumber);
    var lessonLink = 'javascript:scoGetLessonFromQuestionNumber(' + qNumber + ');';
    var questionLink = 'javascript:scoTestGoToQuestion(' + qNumber + ');';
    
    var lessonTitleComplete = getLessonTitle(qNumber);
    var lessonTitle = ellipseTitle(lessonTitleComplete);
    
    toolTipCompleteText = "\'" + unQuote(lessonTitleComplete) + "\'";
    var toolTipAction = (lessonTitleComplete == lessonTitle) ? "" : 'showTooltip(event,\'t1\',' + toolTipCompleteText + ")";
    
    // HTML
    
    // Heading Row
    var openTableHTML = '<table height="20" border="0" cellpadding="0" cellspacing="0" background="' + tableBackGraphic + '">';
    var leadingRowHTML = '<tr>';
    
    for (i = 0; i < columnWidths.length; i++) {
        leadingRowHTML += '    <td width="' + columnWidths[i] + '"><img name="slice_" src="images/whitespacer.gif" width="' + columnWidths[i] + '" height="2" border="0" alt=""></td>';
    }
    leadingRowHTML += '</tr>';
    
    // Content ROW
    contentRowHTML = '<tr>';
    contentRowHTML += '<td><img name="slice_" src="' + openRowGraphic + '" width="6" height="20" border="0" alt=""></td>';
    contentRowHTML += '<td><img name="slice_" src="images/spacer.gif" width="4" height="20" border="0" alt=""></td>';
    contentRowHTML += '<td><img name="slice_" src="' + sGraphic + '" width="10" height="15" border="0" alt=""></td>';
    contentRowHTML += '<td><img name="slice_" src="images/whitespacer.gif" width="1" height="20" border="0" alt=""></td>';
    contentRowHTML += '<td><img name="slice_" src="images/spacer.gif" width="5" height="20" border="0" alt=""></td>';
    contentRowHTML += '<td><a href="' + questionLink + '" class="toclink">Question ' + qNumber + ' </a></td>';
    contentRowHTML += '<td><img name="slice_" src="images/whitespacer.gif" width="1" height="20" border="0" alt=""></td>';
    contentRowHTML += '<td><img name="slice_" src="images/spacer.gif" width="5" height="20" border="0" alt=""></td>';
    
    // contentRowHTML+= '<td><a href="' + lessonLink + '" class="toclink">' + lessonTitle + '</a></td>'; // New Carlos Line for a tooltip
    
    contentRowHTML += '<td><a href="' + lessonLink + '" class="toclink" onMouseOut="' + toolTipAction + '" onMouseOver="' + toolTipAction + '">' + lessonTitle + '</a></td>';
    
    contentRowHTML += '<td><img name="slice_" src="' + closeRowGraphic + '" width="6" height="20" border="0" alt=""></td>';
    contentRowHTML += '</tr>';
    
    
    // Closeing Tags
    var closeTableHTML = '</table>';
    
    // Putting all together
    var HTML = openTableHTML + leadingRowHTML + contentRowHTML + closeTableHTML;
    
    return HTML;
    
}

function createReport(){
    /* html template
     <p>Name : Oleg Arsky<br>
     Date: Jul 26, 2002<br>
     Score: 10%<br>
     Level: Advanced<br>
     </p>
     */
    //function scoGetUserName() {
    //function scoGetDate() {
    //function scoGetScore() {
    //function scoGetLevel() {
    
    //	if (!testData) return 'no data for this test';
    
    //	html = '<table border="0" cellpadding="0" cellspacing="0">';
    //	html+= '<tr><td class="scoTocText" width="53">Name</td><td class="scoTocText" width="13">:</td><td class="scoTocText" width="427">' + escape(scoGetUserName()) + '</td></tr>';
    //	html+= '<tr><td class="scoTocText" width="53">Date</td><td class="scoTocText" width="13">:</td><td class="scoTocText" width="427">' + escape(scoGetDate()) + '</td></tr>';
    //	html+= '<tr><td class="scoTocText" width="53">Course</td><td class="scoTocText" width="13">:</td><td class="scoTocText" width="427">' + escape(scoGetCourseName()) + '</td></tr>';
    //	html+= '<tr><td class="scoTocText" width="53">Level</td><td class="scoTocText" width="13">:</td><td class="scoTocText" width="427">' + escape(scoGetLevel()) + '</td></tr>';
    //	html+= '<tr><td class="scoTocText" width="53">Score</td><td class="scoTocText" width="13">:</td><td class="scoTocText" width="427">' + escape(scoGetScore() + "&#37; ") + '</td></tr>';
    //	html+= '</table>';
    html = '<B>Name : </B>' + escape(scoGetUserName()) + '<br>';
    html += '<B>Date : </B>' + escape(scoGetDate()) + '<br>';
    html += '<B>Course : </B>' + escape(scoGetCourseName()) + '<br>';
    html += '<B>Level : </B>' + escape(scoGetLevel()) + '<br>';
    html += '<B>Score : </B>' + escape(scoGetScore() + "&#37; ") + '<br>';
    
    return unescape(html);
}

function refreshPage(){

    var reportHTML = createReport();
    var tocHTML = createTOC();
    
    parent.navigationFrame.setState(3);
    parent.navigationFrame.refreshNavigationBar();
    
    MM_setTextOfLayer_original('userData', '', reportHTML);
    MM_setTextOfLayer_original('toc', '', tocHTML);
    
}


function createTOC(){
    var no = parent.getNumberOfQuestions();
    var tocHTML = '';
    
    for (j = 1; j <= no; j++) {
        tocHTML += createTOCRow(j);
        //		alert(j);
        //		alert(tocHTML);
    }
    return tocHTML;
    
}

// ***************************************************************************************
// Test Navigation Functions
// 
// ***************************************************************************************




function scoTestGoToQuestion(qNumber){

    parent.navigationFrame.setState(2);
    //	parent.navigationFrame.refreshCurrentQuestionMarker(qNumber); // Carlos... so it only updates the image for the current marker
    // parent.navigationFrame.refreshNavigationBar();
    parent.navigationFrame.scoGoToQuestion(qNumber);
}
