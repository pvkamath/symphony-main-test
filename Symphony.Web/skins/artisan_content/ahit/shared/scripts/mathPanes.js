// hopefully this wont change :-)

// netscape style definitions

//NTSstyle_standard = 'style="font-family: Arial, Helvetica, sans-serif; font-size:25px; font-weight: normal;" ';
//NTSstyle_paneVariable = 'style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #000000; text-decoration: none;" ';
//NTSstyle_variable = 'style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; font-weight: bold; color: #000000; text-decoration: none;" ';

//BROWSER CHECKER
// **************************************************
//function isNetscape() {
//	return isNetscape;
//}

function isFirefox() {
	return isNetscape();
}

//function isie() {
//	return isIE;
//}

function showInputBox(variableNumber, varFormat, varRange){

    // updates or generates (IE & NTS ) the html for the dialogue box
    generateWidgetHTML(variableNumber, varFormat, varRange);
    
    //sets the variables for the "jumping" between input fields
    setWidgetDigits(getDigits(varFormat));
    setWidgetDecimals(getDecimals(varFormat));
    
    //ataches the event handlers to the input fields
    startUp();
    
    //shows the layers
    //	layerName = isNetscape() ? 'NTS_InputDialogue' : 'inputDialogue'; // OLD NETSCAPE
    layerName = 'inputDialogue'; // THIS FIXES FIREFOX
    MM_showHideLayers(layerName, '', 'show');
    
    // sets the focus to the first input field
    setFocusToFirstCell();
    setFocusToFirstCell();
    
}

function setWidgetDigits(aNumber) {
	widgetDigits=aNumber;
}

function setWidgetDecimals(aNumber) {
	widgetDecimals=aNumber;
}

function generateWidgetHTML(){

    args = generateWidgetHTML.arguments;
    variableNumber = args[0];
    varFormat = args[1];
    varRange = args[2];
    /*
     if (isFirefox()) {
     alert("this is firefox!!");
     updatedHTML = generateNTSWidgetHTML(variableNumber , varFormat , varRange);
     MM_setTextOfLayer_original("inputDialogue",null,updatedHTML);
     //			ff_InputDialogue = document.getElementById("inputDialogue");
     //			alert(ff_InputDialogue);
     //			ff_InputDialogue.write(updatedHTML);
     //			ff_InputDialogue.close();
     return;
     };
     if (isNetscape()) {
     alert("this is netscape!!");
     updatedHTML = generateNTSWidgetHTML(variableNumber , varFormat , varRange);
     Netscape_InputDialogue = MM_findObj('NTS_InputDialogue');
     Netscape_InputDialogue.document.write(updatedHTML);
     Netscape_InputDialogue.document.close();
     return;
     }
     */
    updatedHTML = generateIEWidgetHTML(varFormat);
    WidgetSpanObject = MM_findObj('allTheInputFields');
    WidgetSpanObject.innerHTML = updatedHTML;
    promptTextObject = MM_findObj('promptMessage');
    promptTextObject.innerHTML = promptArray[variableNumber];
    formObject = MM_findObj('digitForm');
    formObject.variableNumber.value = variableNumber;
    formObject.bottom.value = varRange[0];
    formObject.top.value = varRange[1];
    
}

function generateNTSWidgetHTML () {

	args=generateNTSWidgetHTML.arguments;
	variableNumber = args[0];
	varFormat = args[1];
	varRange = args[2];
	
	var WidgetHTML = '';
	var promptMessage = promptArray[variableNumber];
	var bottomValue = varRange[0];
	var topValue = varRange[1];
	
	var headerHTML1 = '<table width="100%" border="2" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><tr><td>';
	var headerHTML2 = '<table width="100%" border="0" cellspacing="0" cellpadding="0" background="../shared/images/titleBar.gif" bgcolor="#000099">';
	var headerHTML3	= '<tr><td width="83%" height="27"><img src="../shared/images/title.gif" width="240" height="18" border="0" onMouseDown="MM_dragLayer('+ "'NTS_InputDialogue','',0,0,0,0,true,false,-1,-1,-1,-1,false,false,0,'',false,'')" + '"></td>';
	var headerHTML4 = '<td width="17%" height="27" valign="top"><div align="center"><img name="cancelButton1" border="0" src="../shared/images/cancel_off.gif" width="20" height="20" onMouseDown="MM_swapImage(' + "'cancelButton1','','../shared/images/cancel_on.gif',1);MM_showHideLayers('inputDialogue','','hide');" + '"></div>';
	var headerHTML5 = '</td></tr></table>';
	var headerHTML6 = '<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%"><tr><td height="40" valign="bottom" width="8%">&nbsp;</td><td height="40" valign="bottom" ><td height="40" ><span id="promptMessage" style="font-family: Verdana; font-size: 9pt; font-weight: normal; color: #4A657B;"><div>' + promptMessage + '</div></span></td><td height="40" valign="bottom" width="2%">&nbsp;</td></tr></table>';
	var headerHTML7 = '<span name="digitSpan" id="digitSpan">';
	var headerHTML8 = '<form name="digitForm" method="post" action="" id="digitForm">';
	var headerHTML9 = '<div align="center">';
	var headerHTML10 = '<table border="0" cellspacing="5" cellpadding="0"><tr><td>';
	var headerHTML11 = '<span name="allTheInputFields" id="allTheInputFields">'
	
	var AllHeaders = headerHTML1 + headerHTML2 + headerHTML3 + headerHTML4 + headerHTML5 + headerHTML6 + headerHTML7 + headerHTML8 + headerHTML9 + headerHTML10 + headerHTML11;
	
	var newInputFields = generateIEWidgetHTML(varFormat);
	
	var footerHTML1 = '</span></td><td valign="middle">';
	var footerHTML2 = '<span name="dataSpan" id="dataSpan">';
	var footerHTML3 = '<img name="checkButton1" border="0" src="../shared/images/check_off.gif" width="40" height="20" onMouseUp="checkInterval(this)" onMouseOut="MM_swapImgRestore()" onMouseDown="MM_swapImage(\'checkButton1\',\'\',\'../shared/images/check_on.gif\',1)">';
	var footerHTML4 = '<input type="hidden" id="variableNumber" name="variableNumber" value="' + variableNumber + '"><input type="hidden" id="top" name="top" value="' + topValue +'"><input type="hidden"id="bottom" name="bottom" value="' + bottomValue + '">';
	var footerHTML5 = '</span></td></tr>';
	var footerHTML6 = '</table>';
	var footerHTML7 = '</div>';
	var footerHTML8 = '</form>';
	var footerHTML9 = '</span>';
	var footerHTML10 = '</td></tr></table>';
	
	var AllFooters = footerHTML1 + footerHTML2 + footerHTML3 + footerHTML4 + footerHTML5 + footerHTML6 + footerHTML7 + footerHTML8 + footerHTML9 + footerHTML10;
	
	WidgetHTML = AllHeaders + newInputFields + AllFooters;

	return WidgetHTML;
}

function generateIEWidgetHTML(varFormat){

    var length = varFormat.length;
    var currentDigit = 0;
    var WidgetHTML = '';
    var tableHeaderHTML = '<table border="0" cellspacing="0" cellpadding="0"><tr>';
    var tableFooterHTML = '</tr></table>';
    var eachDigitHeaderHTML = '<td>';
    var eachDigitFooterHTML = '</td>';
    
    // open table tag
    WidgetHTML += tableHeaderHTML;
    
    // add each cell of the table and fill it with the proper content(text field or format)
    for (i = 0; i < length; i++) {
        currentCharacter = varFormat.substr(i, 1);
        currentCellHTML = '';
        if (currentCharacter == '1') {
            currentDigit++;
        }
        
        switch (currentCharacter) {
            case "1":
                currentCellHTML += eachDigitHeaderHTML;
                currentCellHTML += '<input onFocus="mouseDownOnField(this)" name="digit_' + currentDigit + '" tabindex="' + currentDigit + '" maxlength="1"  size="1" type="text"  value="" class="input" >';
                currentCellHTML += eachDigitFooterHTML;
                break;
            case ".":
            case ",":
            case "'":
            case "$":
            case "%":
            case "-":
            case " ":
                currentCellHTML += '<td><span class="inputText">&nbsp;' + currentCharacter + '&nbsp;</span></td>';
                break;
            default:
                currentCellHTML += '<td><span class="inputText">' + currentCharacter + '</span></td>';
        }
        
        WidgetHTML += currentCellHTML;
        
    }
    
    // close table
    WidgetHTML += tableFooterHTML;
    
    
    // return value
    return WidgetHTML;
}


function getDigits(aString){
    var length = aString.length;
    var digits = 0;
    for (i = 0; i < length; i++) {
        if (aString.substr(i, 1) == "1") {
            digits++
        };
            }
    return digits;
    //	alert("digits: "+digits);
}

function getDecimals(aString){
    var length = aString.length;
    var stringArray = aString.split('.');
    var decimalPlace = 0;
    //	alert ( 'stringArray :\n' + stringArray + '\n' + stringArray.length );
    if (stringArray.length == 1) {
        return 0;
    }
    else {
        decimalPlaces = getDigits(stringArray[1]);
        return decimalPlaces;
    }
}

// *********************************************************************************
// **                   Checks if the user entry value is aceptable
// **
// *********************************************************************************


function checkInterval(theField){
//    netscapeBrowser = isNetscape();
//    firefoxBrowser = isFirefox();
    userEntry = Number(answerDigits());
    
    formObject = MM_findObj('digitForm');
    
    bottomValue = formObject.bottom.value;
    topValue = formObject.top.value;
    variableNumber = formObject.variableNumber.value;
    
    if ((userEntry < bottomValue) || (userEntry > topValue)) {
        alert(errorArray[variableNumber]);
        setFocusToFirstCell();
    }
    else {
        variablesValue[variableNumber] = userEntry;
        setVar();
        //	if (netscapeBrowser) {
        //	MM_showHideLayers('NTS_InputDialogue','','hide');
        //	}
        //	else {
        MM_showHideLayers('inputDialogue', '', 'hide');
        //	}
    }
}

// *********************************************************************************
// **                         Lesson Link Translator
// **
// *********************************************************************************

function linkCode(){

    args = linkCode.arguments;
    linkHTML = args[0];
    linkText = args[1];
    
    translatedCode = (isNetscape()) ? linkText : linkHTML;
    
    return translatedCode;
    
}

// *********************************************************************************
// **                           DAMON'S FORMATING FUNCTION
// **
// *********************************************************************************

function formatString(aNumber, roundToPlaces, commas, symbol){

    theInput = aNumber * 1.0; // Cast the input to a number datatype.
    // Handle ticks
    if (symbol == "ticks") {
    
        theInput = theInput * 100; // Convert from percent
        aTick = 1 / 32; // Define a tick
        aHalfTick = 1 / 64; // Define a half-tick
        // Split ticks from integer part
        integerPart = Math.floor(theInput);
        tickPart = theInput - integerPart;
        
        theTicks = Math.round(tickPart / aTick) + "";
        
        if (theTicks == 32) { //cm fix for bug 3087
            theTicks = "0";
            integerPart++;
        }
        
        // Figure-out if a half-tick is necessary
        
        theRemainder = tickPart - (theTicks * aTick);
        
        if (theRemainder == aHalfTick) {
            isHalfTick = "+";
        }
        else {
            isHalfTick = "";
        }
        
        
        // Pad zeros onto the front of the ticks part
        if (theTicks.length == 1) {
            theTicks_display = "0" + theTicks;
        }
        else {
            theTicks_display = theTicks + "";
        }
        
        // Re-assemble tick string
        tick_string = integerPart + " - " + theTicks_display + isHalfTick;
        
        return tick_string;
    }
    
    // Modify inputs and commas if format is 'percent'.
    if (symbol == "%") {
        commas = false;
        theInput = theInput * 100;
    }
    
    // Round to roundToPlaces
    multiplier = Math.pow(10, roundToPlaces);
    scaled2 = Math.round(multiplier * theInput);
    roundedNumber = (scaled2 / multiplier) + ""; // Putting + "" forces Javascript to cast this var as a string.
    end = roundedNumber.indexOf(".");
    
    // Is the number negative? If so, compensate by removing the minus sign.
    if (roundedNumber < 0) {
        roundedNumber = roundedNumber * -1;
        isNegative = "-";
    }
    else {
        isNegative = "";
    }
    
    // Add commas
    displaynumber = roundedNumber + "";
    index = 1;
    revString = "";
    ltz_part = "";
    
    decimalIndex = displaynumber.indexOf(".");
    
    if (decimalIndex == -1) {
        end = displaynumber.length - 1;
    }
    else {
        end = displaynumber.indexOf(".") - 1;
        ltz_part = displaynumber.substring(end + 1, displaynumber.length);
    }
    
    if (commas == true) {
        for (i = end; i > -1; i--) {
            if (index == 3) {
                revString = revString + displaynumber.substr(i, 1) + ",";
                index = 1;
            }
            else {
                revString = revString + displaynumber.substr(i, 1);
                index++;
            }
        }
        
        // Reverse the string
        fwdString = "";
        for (i = revString.length; i > -1; i--) {
            fwdString = fwdString + revString.substr(i, 1);
        }
        
        fwdString = fwdString + ltz_part;
        
        if (fwdString.substr(0, 1) == ",") {
            fwdString = fwdString.substr(1, fwdString.length);
        }
        
    }
    else {
        fwdString = displaynumber + "";
    }
    
    displaynumber = fwdString + "";
    
    // Find how many zeros need to be padded to the end of the number
    end = displaynumber.length;
    decimalIndex = displaynumber.indexOf(".");
    
    if (decimalIndex == -1) {
        zerosNeeded = roundToPlaces;
    }
    else {
        zerosNeeded = roundToPlaces - (end - decimalIndex) + 1;
    }
    
    // Pad zeros to the end of the string
    padding = "";
    if ((zerosNeeded == roundToPlaces) && (roundToPlaces != 0)) {
        displaynumber = displaynumber + ".";
    }
    
    for (i = 1; i <= zerosNeeded; i++) {
        padding = padding + "0";
    }
    
    displaynumber = displaynumber + padding;
    
    // Add currency or "%" sign
    if (symbol == "%") {
        displaynumber = isNegative + displaynumber + "%"; //&#037; \u00E0
    }
    else {
        displaynumber = isNegative + symbol + displaynumber;
    }
    
    // Add zero to beginning if necessary
    theIndexOfDot = displaynumber.indexOf(".");
    
    if (theIndexOfDot == 0) {
        displaynumber = "0" + displaynumber;
    }
    
    if (displaynumber.substr(0, 2) == "-.") {
        displaynumber = "-0." + displaynumber.substr(2);
    }
    
    if (displaynumber.substr(0, 2) == "$.") {
        displaynumber = "$0." + displaynumber.substr(2);
    }
    
    return displaynumber;
}


// *********************************************************************************
// **                    DAMON'S ALIGNMENT CORRECTION FUNCTIONS
// **
// *********************************************************************************

function alignTagBegin(alignment){
    if (document.all) {
        theTag = "<div align=\'" + alignment + "\'>";
    }
    else {
        theTag = "<span class='total'>";
    }
    return theTag;
}

function alignTagEnd(){
    if (document.all) {
        theTag = "</div>";
    }
    else {
        theTag = "</span>";
    }
    return theTag;
}











