

 function pageTypes()
 {
 var dict = new Object();
 dict [1]    = 0 ;
 dict [2]    = 0 ;
 dict [3]    = 0 ;
 dict [4]    = 0 ;
 dict [5]    = 'randomExercise' ;
  
 return dict
  }


 function scoInfo ()
 { return titleAndPagesVAR
  }

 var titleAndPagesVAR =
   [  "Lesson 1"
      , "toc.htm"
      , "02_51600.htm"
      , "03_51603.htm"
      , "exercises/exercise_03_1/current/i0001/exercise.htm"
      , "exercises/exercise_03_2/current/i0001/exercise.htm"
   ];
  
 
function pageNoToIndexMap  ()
{
 var dict = new Object();
 dict ['1']    = 1 ;
 dict ['2']    = 2 ;
 dict ['3']    = 3 ;
 dict ['3.1']    = 4 ;
 dict ['3.2']    = 5 ;

return dict
} 


function scoType ()
//==============
 { return "lesson";
 }



function maxScore ()
//==================
// Return the maximum NON-normalized score,
// i.e. the number of questions
{
 return  2 ;
}


function scoCustomStatus  ()
//       ===================
{
  return null;

 // Null return value means: let the caller
 // decide the logic for determining the
 // completion status. Else use any logic
 // you desire and return one of "failed"
 // "passed" , "complete" or "incomplete".

}


function scoCustomInitialize  ()
//       ===================
{
  return null;   //null means: OK TO START

// If you return a string, the SCO
// will not be started, and perhaps
// the return string gets logged as
// an error.  For instance:
// return "SORRY, need more money";
}


function scoCustomFinish  ()
//       ===============
// If you return non-null, the course
// will not be exited.
//
{  return null;   //null means: OK TO EXIT
}

function requiredScore ()
//       =============
// Return the NORMALIZED score the student
// must get to pass the course. The return
// value is between 0 and 100 incusive.
//
// Returning a non-zero value means the SCO
// status can be either INCOMPLETE, PASSED or FAILED.
//
// Returning 0 means the status can only be
// INCOMPLETE or  COMPLETE. (I.e. if there are
// no questions, it does not make sense to rate
// a course as passed or failed BECAUSE in SCORM
// the status can NOT be  "complete AND passed"
// nor "complete AND failed" nor "incomplete and passed" etc.
//

{ return 50;
}


function   customCourseTitle ()
{

}




