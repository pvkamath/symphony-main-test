/*************************************************\
 ** File:   shared/scripts/scoDyk.js
 ** ------------------------------
 ** Functions needed  within THE GLOSSARY POPUP-WINDOW
 **
 \************************************************/
function presentGDef(termString, defString){
    var content = '<HTML><HEAD><TITLE>risk</TITLE>                            \
					<META http-equiv="Content-Type"                             \
					      content="text/html; charset=iso-8859-1">              \
					<link rel="stylesheet"                                      \
					      href="../styles/WebSite.css" type="text/css">         \
					</HEAD>                                                     \
					<BODY    bgcolor="#FFFFFF" text="#000000"                   \
					         leftmargin="44" rightmargin="42" topmargin="0"		\
					         bottomMargin=8 marginwidth="44" marginheight="0">	\
					<p class="term">' + termString + '</p>						\
					<p class="definition">' +
					    defString +
					    '</p>					\
					</body></html>\n';
    
    var fr = midFrame.document;
    fr.writeln(content);
    fr.close();
    
}


function glossaryDefLoaded(){
    if (document.links[0]) 
        document.links[0].blur();
    
}


function closeGlossaryDialog(){
    window.returnValue = false;
    window.close();
};



function glossaryUnloaded(){

}




function presentFrameset(){
    var content = '<frameset											\
				    rows="44,*,40"										\
				    onLoad   = "presentGDef(term , definition);"		\
				    frameborder="NO" border="0" framespacing="0"		\
				 ><frame												\
				        name                = "topFrame"				\
				        src                 = "../html/gtopFrame.htm"	\
				        FRAMEBORDER         = "no"						\
						border              = "0"						\
				        NORESIZE										\
						SCROLLING           ="no"						\
				><frame													\
				        name                ="midFrame"					\
				        src                 ="../html/scoBlank.htm"		\
				        FRAMEBORDER         = "no"						\
				        border              = "0"						\
				        NORESIZE										\
				        SCROLLING           ="yes"						\
				><frame													\
				        name                ="botFrame"					\
				         src                ="../html/gbotFrame.htm"    \
				        FRAMEBORDER         = "no"						\
				        border              = "0"						\
				        NORESIZE										\
				        SCROLLING           ="no"						\
				     >													\
				</frameset>												\
				</HTML>													\
				';
    document.writeln(content);
    
}


presentFrameset()

