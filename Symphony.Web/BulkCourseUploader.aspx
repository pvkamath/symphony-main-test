﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Symphony.Web.BulkCourseUploader" ValidateRequest="false" Codebehind="BulkCourseUploader.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Artisan Bulk Asset Importer</title>
	<link rel="stylesheet" type="text/css" href="/Admin/css/default.css" />
	<script type="text/javascript" src="/scripts/swfupload/swfupload.js"></script>
	<script type="text/javascript" src="/scripts/extjs3/adapter/ext/ext-base.js"></script>
	<script type="text/javascript" src="/scripts/extjs3/ext-all.js"></script>
		
	<script type="text/javascript">
		var swfu;
		window.onload = function() {
		    var maxsize = 200;
		    swfu = new SWFUpload({
		        // Backend Settings
		        upload_url: "/Uploaders/BulkCourseFileUploader.ashx?customer=<%= new Symphony.Core.Controllers.SymphonyController().ApplicationName %>",
		        post_params: {
		            'ASP.NET_SessionId': '<%= Session.SessionID %>',
		            '.ASPXAUTH': '<%= HttpContext.Current.Request.Cookies[System.Web.Security.FormsAuthentication.FormsCookieName].Value %>'
		        },

		        // File Upload Settings
		        file_size_limit: maxsize + " MB",
		        file_types: "*.zip",
		        file_types_description: "Zip Files",
		        file_upload_limit: "0",    // Zero means unlimited
		        file_dialog_complete_handler: function(){
		            document.getElementById('divFileProgressContainer').innerHTML = '';
		            this.startUpload();
		        },
		        upload_progress_handler: function(item, completed, total){
                    var percent = parseInt(completed * 1.0 / total * 1.0 * 100, 10);
                    var c = document.getElementById('divFileProgressContainer');
                    c.style.width = percent + '%';
                    c.style.display = 'block';
                    if(percent == 100){
                        c.innerHTML = 'Upload complete, importing...';
                    }
                },
		        //upload_error_handler: uploadError,
		        upload_success_handler: function(file, results, successful){
		            if(results.error){
		                // global failure, like directory permissions, etc
		                alert(results.error);
		            }else{
		                var resultsDiv = document.getElementById('results');
		                resultsDiv.style.display = 'block';
		                var resultsTable = document.getElementById('resultsTable');
		                // we got to the import stage, so check each import attempt
		                var tpl = "<td>{filename}</td><td>{success}</td><td>{version}</td>";
		                results = Ext.decode(results);
		                
		                for(var i = 0; i < results.length; i++){
		                    var data = results[i].result;
		                    var row = document.createElement('tr');
		                    if(!results[i].success){
		                        row.innerHTML = tpl.replace("{filename}",data.name)
		                                           .replace("{success}",'No')
		                                           .replace("{version}", '-');
		                    }else{
		                        var td = document.createElement('td');
		                        td.innerHTML = data.name;
		                        row.appendChild(td);
		                        
		                        var td = document.createElement('td');
		                        td.innerHTML = 'Yes';
		                        row.appendChild(td);
		                        
		                        var td = document.createElement('td');
		                        td.innerHTML = data.version;
		                        row.appendChild(td);
		                    }
		                    resultsTable.appendChild(row);
		                }
		            }
		            document.getElementById('divFileProgressContainer').innerHTML = 'Processing complete.';
		        },
		        //upload_complete_handler: uploadComplete,

		        // Button settings
		        button_image_url: "images/XPButtonNoText_160x22.png",
		        button_placeholder_id: "spanButtonPlaceholder",
		        button_width: 240,
		        button_height: 22,
		        button_text: '<span class="button" style="border:1px solid red">Click here to select a file &nbsp;<span class="buttonSmall">(' + maxsize + ' MB Max)</span></span>',
		        button_text_style: '.button { font-family: Helvetica, Arial, sans-serif; font-size: 14pt; background-color:red; } .buttonSmall { font-size: 10pt; }',
		        button_text_top_padding: 1,
		        button_text_left_padding: 5,

		        // Flash Settings
		        flash_url: "/scripts/swfupload/flash/swfupload.swf", // Relative to this file

		        custom_settings: {
		            upload_target: "divFileProgressContainer"
		        },

		        // Debug Settings
		        debug: false
		    });

		    // once a minute, we reload this iframe, which will ensure that we keep the session alive
		    window.setInterval(function() {
		        document.getElementById("keepalive").src = "blank.html?cb=" + (new Date().getTime());
		    }, 60000);
		}
	</script>
	<style type="text/css">
	    table{
	        width:100%;
	    }
	</style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin-left:auto;margin-right:auto;width:900px;text-align:center;">
        <h2>Symphony Bulk Course Importer</h2>
        <span>This process will import the uploaded courses into the Symphony database.</span><br /><br />
        <span>The import file must be a zip file containing the other zipped courses.</span><br /><br />
        <span>Courses will be given names according to their filename, and will be *version* updated if a corresponding name already exists.</span><br /><br />
        <hr />
        
        <div id="content">
	        <div id="swfu_container" style="margin: 0px 10px;">
		        <div style="height:1.2em;padding:5px;">
				    <span id="spanButtonPlaceholder"></span>
		        </div>
		        <div id="divFileProgressContainer" style="text-align:center;height:1.2em;padding:5px;width:0%;background-color:AliceBlue;border:1px solid #333;display:none;"></div>
	        </div>
		</div>
	    
        <hr />               
        <div id="results" style="position:relative;width:100%;display:none;">
            <h2 id="resultsTitle" runat="server">Results:</h2>
            <table>
                <tbody id="resultsTable">
                    <tr>
                        <th>File/Course Name</th><th>Success</th><th>Version</th>
                    </tr>
                </tbody>
            </table>
        </div>
        
        <iframe id="keepalive" src="blank.html" style="display:none"></iframe>
    </div>
    </form>
</body>
</html>
