function janrainDefaultSettings() {
    var defaultSettings = {
        actionText: ' ',
        appUrl: 'https://login.oncourselearning.com',
        tokenAction: 'event',
        tokenUrl: 'http://localhost:58599/handlers/janrainhandler.ashx',
        borderColor: '#ffffff',
        fontFamily: 'Helvetica, Lucida Grande, Verdana, sans-serif',
        packages: ['login', 'capture'],
        providersPerPage: 4,
        width: 300,
        capture: {
            appId: 'bbfeudad3fd3y298mnq4jgaafu',
            captureServer: 'https://oncourselearning.janraincapture.com',
            clientId: '7yu42un9xuhfcfbura2efatz6cqr5tyj',
            flowVersion: 'HEAD',
            recaptchaPublicKey: '6LeVKb4SAAAAAGv-hg5i6gtiOV4XrLuCDsJOnYoP',
            keepProfileCookieAfterLogout: true,
            modalCloseHtml: '<span class="janrain-icon-16 janrain-icon-ex2"></span>',
            redirectUri: 'http://localhost/',
            responseType: 'token',
            setProfileCookie: true,
            transactionTimeout: 10000,
            noModalBorderInlineCss: true,
            returnExperienceUserData: ['displayName'],
            federate: true,
            federateServer: 'https://oncourselearning.janrainsso.com',
            federateXdReceiver: 'http://symphony.ocltraining.com/scripts/janrain/xdreceiver.html',
            federateLogoutUri: 'http://symphony.ocltraining.com/scripts/janrain/logout.html',
            federateEnableSafari: true
        }
    };
    if (typeof window.janrain !== 'object') window.janrain = {};
    if (typeof window.janrain.settings !== 'object') window.janrain.settings = {};
    window.janrain.settings = janrainMergeObjects(defaultSettings, window.janrain.settings);
};

function janrainReturnExperience() {
    var span = document.getElementById('traditionalWelcomeName');
    var name = janrain.capture.ui.getReturnExperienceData("displayName");
    if (span && name) {
        span.innerHTML = "Welcome back, " + name + "!";
    }
}

function janrainInitLoad() {
    function isReady() { janrain.ready = true; };
    if (document.addEventListener) {
        document.addEventListener("DOMContentLoaded", isReady, false);
    } else {
        window.attachEvent('onload', isReady);
    }

    var httpsLoadUrl = "https://rpxnow.com/load/login.oncourselearning.com";
    var httpLoadUrl = "http://widgets-cdn.rpxnow.com/load/login.oncourselearning.com";
    var e = document.createElement('script');
    e.type = 'text/javascript';
    e.id = 'janrainAuthWidget';
    if (document.location.protocol === 'https:') {
        e.src = httpsLoadUrl;
    } else {
        e.src = httpLoadUrl;
    }
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(e, s);
}
// Will show appropriate Janrain login window if enabled. 
function janrainInitLoginUi() {

    if (window.janrainLogin) {

        switch (window.ssoLoginUiType) {
            case Symphony.SsoLoginUiType.standard:
                // don't show the login-sso
                $('.sso-login').hide();
                break;
            case Symphony.SsoLoginUiType.mixed:
                // show the mixed ui to allow both methods of sign in
                $('#loginDiv, .x-window, .x-shadow').hide();

                break;
            case Symphony.SsoLoginUiType.forced:

                // Show only sso login mode
                $('#loginDiv, .x-window, .x-shadow').hide();
                $('#login-symphony').hide();
                $('.sso-login').addClass('forced');
                break;
        }
    }

    $("#login-symphony")
        .click(function (e) {
            // Format the lovely login window here
            $('#loginDiv, .x-window').show();
            $('#capture_signin_link, #login-symphony, .sso-login').hide();
            $('.x-window, .x-window-body').width(300);

        });
};


function janrainInjectCaptureWidget(targetId, widgetHtml) {
    var s = document.getElementById(targetId);
    var e = document.createElement('div');
    e.id = 'janrainCaptureWidget';
    e.innerHTML = widgetHtml;
    s.parentNode.insertBefore(e, s);

}

// merge 2 objects: properties in the 2nd object overwrite properties in the 1st
function janrainMergeObjects(obj1, obj2) {
    for (var item in obj2) {
        if (obj2[item].constructor === Object && item in obj1 && obj1[item].constructor === Object) {
            obj1[item] = janrainMergeObjects(obj1[item], obj2[item]);
        }
        else {
            obj1[item] = obj2[item];
        }
    }
    return obj1;
}


(function () {
    return;

    janrainDefaultSettings();
    janrain.settings.capture.registerFlow = 'socialRegistration';

    janrainInitLoad();

    janrainInjectCaptureWidget('janrainCaptureDevScript', '<div style="display:none;" id="signIn">\n' +
'    <div class="capture_header">\n' +
'        <h1>Sign In</h1>\n' +
'    </div>\n' +
'    <div class="capture_backgroundColor">\n' +
'        <div class="capture_signin">\n' +
'                {* #userInformationForm *}\n' +
'                    {* traditionalSignIn_emailAddress *}\n' +
'                    {* traditionalSignIn_password *}\n' +
'                    <div class="capture_form_item">\n' +
'                        <a href="#" data-capturescreen="forgotPassword">Forgot your password?</a>\n' +
'                    </div>\n' +
'                    <div class="capture_rightText">\n' +
'                        {* traditionalSignIn_signInButton *}\n' +
'                    </div>\n' +
'                {* /userInformationForm *}\n' +
'        </div>\n' +
'    </div>\n' +
'</div>\n' +
'<div style="display:none;" id="returnSocial">\n' +
'    <div class="capture_header">\n' +
'        <h1>Sign In</h1>\n' +
'    </div>\n' +
'    <div class="capture_signin">\n' +
'        <h2>Welcome back, {* welcomeName *}!</h2>\n' +
'        {* loginWidget *}\n' +
'        <div class="capture_centerText switchLink"><a href="#" data-cancelcapturereturnexperience="true">Use another account</a></div>\n' +
'    </div>\n' +
'</div>\n' +
'<div style="display:none;" id="returnTraditional">\n' +
'    <div class="capture_header">\n' +
'        <h1>Sign In</h1>\n' +
'    </div>\n' +
'    <h2 class="capture_centerText"><span id="traditionalWelcomeName">Welcome back!</span></h2>\n' +
'            <div class="capture_backgroundColor">\n' +
'                {* #userInformationForm *}\n' +
'                    {* traditionalSignIn_emailAddress *}\n' +
'                    {* traditionalSignIn_password *}\n' +
'                    <div class="capture_form_item capture_rightText">\n' +
'                        {* traditionalSignIn_signInButton *}\n' +
'                    </div>\n' +
'                {* /userInformationForm *}\n' +
'        <div class="capture_centerText switchLink"><a href="#" data-cancelcapturereturnexperience="true">Use another account</a></div>\n' +
'    </div>\n' +
'</div>\n' +
'<div style="display:none;" id="forgotPassword">\n' +
'    <div class="capture_header">\n' +
'        <h1>Create a new password</h1>\n' +
'    </div>\n' +
'    <h2>We\'ll send you a link to create a new password.</h2>\n' +
'    {* #forgotPasswordForm *}\n' +
'        {* traditionalSignIn_emailAddress *}\n' +
'        <div class="capture_footer">\n' +
'            <div class="capture_left">\n' +
'                {* backButton *}\n' +
'            </div>\n' +
'            <div class="capture_right">\n' +
'                {* forgotPassword_sendButton *}\n' +
'            </div>\n' +
'        </div>\n' +
'    {* /forgotPasswordForm *}\n' +
'</div>\n' +
'<div style="display:none;" id="forgotPasswordSuccess">\n' +
'    <div class="capture_header">\n' +
'        <h1>Create a new password</h1>\n' +
'    </div>\n' +
'      <p>We\'ve sent an email with instructions to create a new password. Your existing password has not been changed.</p>\n' +
'    <div class="capture_footer">\n' +
'        <a href="#" onclick="janrain.capture.ui.modal.close()" class="capture_btn capture_primary">Close</a>\n' +
'    </div>\n' +
'</div>\n' +
'<div style="display:none;" id="mergeAccounts">\n' +
'    {* mergeAccounts *}\n' +
'</div>\n' +
'<div style="display:none;" id="traditionalAuthenticateMerge">\n' +
'    <div class="capture_header">\n' +
'        <h1>Sign in to complete account merge</h1>\n' +
'    </div>\n' +
'    <div class="capture_signin">\n' +
'    {* #tradAuthenticateMergeForm *}\n' +
'        {* traditionalSignIn_emailAddress *}\n' +
'        {* mergePassword *}\n' +
'        <div class="capture_footer">\n' +
'            <div class="capture_left">\n' +
'                {* backButton *}\n' +
'            </div>\n' +
'            <div class="capture_right">\n' +
'                {* traditionalSignIn_signInButton *}\n' +
'            </div>\n' +
'        </div>\n' +
'     {* /tradAuthenticateMergeForm *}\n' +
'    </div>\n' +
'</div>');
})();
