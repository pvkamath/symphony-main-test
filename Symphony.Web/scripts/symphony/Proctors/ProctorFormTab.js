﻿/**
 * ProctorTab is a panel that wraps a grid of proctors and a sub tab panel
 * that displays proctor forms.
 */
Ext.define('Proctors.ProctorFormTab', {
    extend: 'Ext.Panel',
    xtype: 'proctors.proctorformtab',
    requires: [
        'ProctorForm',
        'Proctors.ProctorFormGrid',
        'Ext.tab.Panel'
    ],
    uses: 'Proctors.ProctorFormPanel',
    mixins: ['Symphony.mixins.FunctionOwnership'],

    layout: 'border',

    items: [{
        xtype: 'proctors.proctorformgrid',
        itemId: 'grid',
        region: 'west',

        border: false,
        split: true,
        width: 290,

        listeners: {
            addgriditem: function(model) {
                var me = this,
                    root = me.up('[xtype=proctors.proctorformtab]');

                root.addPanel(model);
            },

            opengriditem: function(model) {
                var me = this,
                    root = me.up('[xtype=proctors.proctorformtab]');

                root.addPanel(model);
            }
        }
    }, {
        xtype: 'tabpanel',
        itemId: 'tab-panel',
        region: 'center',
        border: false
    }],

    initComponent: function() {
        var me = this;

        me.callParent();

        me.on('activate', function() {
            var grid = me.queryById('grid');

            Log.debug('Proctor form tab activated, refreshing grid.');
            grid.refresh();
        });
    },

    addPanel: function(proctorForm) {
        var me = this,
            grid = me.queryById('grid'),
            tabPanel = me.queryById('tab-panel'),
            editor;

        Log.info('Opening a new proctor form editor.');

        editor = tabPanel.items.findBy(function(el, key) {
            if (el.model && !el.model.phantom) {
                return el.model.getId() == proctorForm.getId();
            }

            return false;
        });

        if (!editor) {
            Log.debug('Editor is not open, opening an editor for the model.', proctorForm);

            editor = tabPanel.add(new Proctors.ProctorFormPanel({
                model: proctorForm,
                closable: true
            }));
            editor.on('change', function() {
                grid.refresh();
            });
        } else {
            Log.debug('Editor is already open for the model, switching focus.', editor);
        }

        tabPanel.activate(editor);
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});