﻿Ext.define('Proctors.ProctorFormGrid', {
    extend: 'symphony.searchablegrid',
    xtype: 'proctors.proctorformgrid',
    require: [
        'ProctorForm',
        'Ext.button.Button'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],

    assumeOwnership: ['add-button'],
    model: 'ProctorForm',
    url: '/services/proctor.svc/proctorForms',
    deferLoad: true,

    columns: [{
        text: 'Name',
        dataIndex: 'name',
        flex: 1
    }, {
        text: 'Description',
        dataIndex: 'description',
        flex: 2
    }],

    tbar: {
        items: [{
            text: 'Add',
            itemId: 'add-button',
            iconCls: 'x-button-add',
            handler: function() {
                var me = this,
                    root = me.up('[xtype=proctors.proctorformgrid]');

                Log.debug('Clicked button to add a new proctor form.');

                root.fireEvent('addgriditem', new ProctorForm({
                    name: 'New Proctor Form'
                }));
            }
        }]
    },

    initComponent: function() {
        var me = this;

        me.callParent();

        Log.watchStore(me, me.getStore());

        me.on('itemclick', function(view, model) {
            Log.debug('Clicked on an grid item to open a new editor.');

            me.fireEvent('opengriditem', model);
        });
    },

    getSelected: function () {
        return this.getSelectionModel().selections;
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});