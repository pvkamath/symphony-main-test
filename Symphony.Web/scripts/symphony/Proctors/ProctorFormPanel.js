﻿Ext.define('Proctors.ProctorFormPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'proctors.proctorformpanel',
    requires: [
        'Symphony.SaveCancelBar',
        'Symphony.ComponentBuilderField'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],

    autoScroll: true,
    model: null,
    frame: true,
    border: false,
    header: false,
    margin: 0,
    padding: 0,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    assumeOwnership: ['save-bar', 'delete-button', 'component-field'],

    dockedItems: [{
        xtype: 'symphony.savecancelbar',
        itemId: 'save-bar',
        dock: 'top',
        showManually: true,
        listeners: {
            save: function() {
                var me = this.up('[xtype=proctors.proctorformpanel]');

                Log.debug('Clicked button to save the proctor form.');

                me.save();
            },
            cancel: function() {
                var me = this.up('[xtype=proctors.proctorformpanel]');

                Log.debug('Clicked button to cancel changes to the proctor form.');

                me.close();
            }
        },

        items: ['->', {
            xtype: 'button',
            itemId: 'delete-button',
            iconCls: 'x-button-cross',
            text: 'Delete',

            handler: function() {
                var me = this.up('[xtype=proctors.proctorformpanel]');

                Log.debug('Clicked button to delete the proctor form');

                me.deleteProctor();
            }
        }]
    }],

    items: [{
        xtype: 'fieldset', 
        title: 'General',
        margin: 5,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [{
            xtype: 'textfield',
            name: 'name',
            fieldLabel: 'Name',
            allowOnlyWhitespace: false,
            maxLength: 512
        }, {
            xtype: 'textfield',
            name: 'description',
            fieldLabel: 'Description',
            maxLength: 512
        }, {
            xtype: 'symphony.componentbuilderfield',
            itemId: 'component-field',
            name: 'proctorJSON',

            fieldItemFilter: [
                Symphony.ComponentBuilderFieldType.LABEL,
                Symphony.ComponentBuilderFieldType.TEXT_FIELD,
                Symphony.ComponentBuilderFieldType.DATE_FIELD,
                Symphony.ComponentBuilderFieldType.CONFIRMATION_BUTTON,
                Symphony.ComponentBuilderFieldType.CHECKBOX_FIELD,
                Symphony.ComponentBuilderFieldType.PROCTOR_CODE_FIELD
            ],

            preview: function() {
                var me = this.up('[xtype=proctors.proctorformpanel]');

                if (!this.isValid()) {
                    Log.info('Preview aborted due to invalid state.');
                    return;
                }

                Log.info('Rendering preview.');
       
                me.save(Ext.Function.createOwned(me, function(success) {
                    if (!success) {
                        Log.error('Aborting preview, save was unsuccessful.');
                        return;
                    }

                    Symphony.Portal.ProctorPanel.showProctorWindow(me.model.getId(), 'preview', 0, 0, Ext.emptyFn);
                }));
            }
        }]
    }],

    initComponent: function() {
        var me = this;

        me.callParent();

        if (!me.model) {
            Log.error('Model property is undefined.');
            return;
        }

        Log.watchModel(me, me.model);

        me.setTitle(me.model.get('name'));
        if (!me.model.phantom) {
            me.getForm().setValues(me.model.getData());
        } else {
            me.queryById('delete-button').hide();
        }
    },

    save: function(callback) {
        var me = this,
            saveBar = me.queryById('save-bar'),
            form = me.getForm(),
            model,
            data;

        saveBar.showSaving();
        Log.info('Saving proctor form.');

        if (!form.isValid()) {
            saveBar.hideSaving();
            saveBar.showNotSaving();

            Log.error('Cannot submit because form is in an invalid state.');
            if (callback) {
                callback(false);
            }
            return;
        }

        data = me.form.getValues();
        me.model.set(data);

        Log.debug('Updating model data to save.', me.model);

        me.model.save({
            success: Ext.Function.createOwned(me, function() {
                Log.info('Model was successfully saved.');

                me.queryById('delete-button').show();
                me.fireEvent('change', me.model);

                if (callback) {
                    callback(true);
                }
            }),
            failure: Ext.Function.createOwned(me, function(model, operation) {
                Log.error('Failed to save model.');
                saveBar.showError(Ext.String.format('Save failed: {0}  {1}', operation.error.status, operation.error.statusText));

                if (callback) {
                    callback(false);
                }
            })
        });
    },

    deleteProctor: function () {
        var me = this,
            model,
            data;

        Log.debug('Asking user for confirmation to delete proctor.');

        Ext.Msg.confirm('Confirm deletion', 'Are you sure you want to delete this proctor? This action cannot be undone.', Ext.Function.createOwned(me, function(result) {
            if (result != 'yes') {
                Log.debug('User canceled delete operation.');
                return;
            }

            Log.info('Deleting proctor.');

            me.model.destroy({
                success: Ext.Function.createOwned(me, function() {
                    Log.info('Model was successfully destroyed.');

                    me.fireEvent('change', me.model);
                    me.close();
                }),
                failure: Ext.Function.createOwned(me, function(model, operation) {
                    Log.error('Failed to destroy model.');
                    saveBar.showError(Ext.String.format('Delete failed: {0}  {1}', operation.error.status, operation.error.statusText));
                })
            });
        }));
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});