﻿Symphony.Customer.CustomerAdministrationTab = Ext.define('customer.customeradministrationtab', {
    alias: 'widget.customer.customeradministrationtab',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            layout: 'border',
            items: [{
                region: 'west',
                width: 300,
                split: true,
                collapsible: true,
                xtype: 'customer.customeradministrationgrid',
                listeners: {
                    itemclick: function (grid, record, item, index, e, opts) {
                        me.addPanel(record);
                    },
                    addCustomer: function () {
                        var newCustomer = Ext.create('customer', {
                            id: 0,
                            salesChannelId: 0,
                            name: 'New Customer'
                        });

                        me.addPanel(newCustomer);
                    }
                }
            }, {
                region: 'center',
                xtype: 'tabpanel',
                name: 'customerEditorContainer'
            }],
            listeners: {
                activate: {
                    fn: function (panel) {
                        panel.query('[xtype=customer.customeradministrationgrid]')[0].refresh();
                        panel.query('[xtype=customer.saleschanneltreepicker]')[0].store.load();
                    },
                    single: true
                }
            }
        });
        this.callParent(arguments);
    },
    addPanel: function (record) {
        var tabpanel = this.query('tabpanel')[0];
        var panel = tabpanel.queryBy(function (p) {
            return p.tabId == 'customer' + record.get('id');
        })[0];

        if (!panel) {
            panel = new Symphony.Customer.CustomerAdministrationEditor({
                customerId: record.get('id'),
                customerName: record.get('name'),
                salesChannelId: record.get('salesChannelId'),
                tabId: 'customer' + record.get('id')
            });

            tabpanel.add(panel);
            tabpanel.doLayout();
        }

        tabpanel.layout.setActiveItem(panel);
    }
});