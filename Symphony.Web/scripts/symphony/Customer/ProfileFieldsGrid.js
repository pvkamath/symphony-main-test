﻿Symphony.Customer.CustomerForm = Ext.define('customer.profilefieldsgrid', {
    alias: 'widget.customer.profilefieldsgrid',
    extend: 'symphony.searchablegrid',
    /*formDefaults: {
        xtype: 'textfield',
        anchor: '96%',
        labelWidth: 145
    },
    */
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            url: '/services/customer.svc/user/datafields/0',
            model: 'userDataField',
            columns: [{
                flex: 0.1,
                header: '',
                dataIndex: 'isGlobal',
                renderer: function (value) {
                    return Symphony.globalRenderer(value, "Global profile field - Applies to all customers.");
                },
                sortable: false
            }, {
                header: 'Name',
                dataIndex: 'displayName',
                flex: 1,
                sortable: false
            }, {
                header: 'Category',
                dataIndex: 'categoryDisplayName',
                renderer: function (value) {
                    if (!value) {
                        return 'Other';
                    }
                    return value;
                },
                flex: 0.5,
                sortable: false
            }, { 
                header: 'Xtype',
                dataIndex: 'xtype',
                renderer: function (value) {
                    if (!value) {
                        return 'textfield';
                    }
                    return value;
                },
                flex: 0.5,
                sortable: false
            }]
        });

        this.callParent(arguments);
    }
});
