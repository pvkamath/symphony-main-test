﻿Symphony.Customer.CustomerForm = Ext.define('customer.profilefieldform', {
    alias: 'widget.customer.profilefieldform',
    extend: 'Ext.form.Panel',
    userDataFieldId: 0,
    customerId: 0,
    record: null,
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            bodyPadding: 10,
            defaults: {
                anchor: '96%',
                xtype: 'textfield',
                labelWidth: 145
            },
            tbar: {
                xtype: 'symphony.savecancelbar',
                items: ['->', {
                    xtype: 'button',
                    text: 'Delete',
                    iconCls: 'x-button-delete',
                    handler: function () {
                        if (me.userDataFieldId > 0) {
                            Ext.Msg.confirm('Confirm Delete', 'Are you sure you want to delete this data field?', function () {
                                Symphony.Ajax.request({
                                    method: 'DELETE',
                                    url: '/services/customer.svc/user/datafields/' + me.userDataFieldId,
                                    success: function (result) {
                                        me.fireEvent('refresh');
                                        me.destroy();
                                    }
                                });
                            });
                        } else {
                            me.destroy();
                        }
                    }
                }],
                listeners: {
                    save: function () {
                        var data = me.getValues();

                        if (data.categoryCodeName && data.categoryCodeName.length) {
                            var record = data.categoryCodeName[0];

                            if (record.data) {
                                var categoryCodeName = record.get('categoryCodeName');
                                var categoryDisplayName = record.get('categoryDisplayName');

                                data.categoryCodeName = categoryCodeName;
                                data.categoryDisplayName = categoryDisplayName;
                            }
                        }

                        if (data.xtype && data.xtype.length) {
                            data.xtype = data.xtype[0];
                        }

                        if (data.isGlobal) {
                            data.customerId = 0;
                        } else {
                            data.customerId = me.customerId;
                        }

                        Symphony.Ajax.request({
                            method: 'POST',
                            url: '/services/customer.svc/user/datafields/' + me.userDataFieldId,
                            jsonData: data,
                            success: function (result) {
                                if (result.data) {
                                    me.setTitle(data.displayName);
                                    me.userDataFieldId = result.data.userDataFieldId;
                                    me.fireEvent('refresh');
                                }
                            }
                        });

                    },
                    cancel: function () {
                        me.destroy();
                    }
                }
            },
            items: [{
                name: 'displayName',
                fieldLabel: 'Name'
            }, {
                name: 'codeName',
                fieldLabel: 'Code Name'
            }, {
                name: 'xtype',
                xtype: 'boxselect',
                fieldLabel: 'Xtype',
                emptyText: 'textfield',
                multiSelect: true,
                forceSelection: false,
                allowAddNewData: true,
                createNewOnEnter: true,
                queryMode: 'local',
                triggerAction: 'all',
                queryDelay: 0,
                minChars: 1,
                store: new Ext.data.Store({
                    model: 'simple',
                    proxy: {
                        type: 'memory'
                    },
                    data: [
                        { id: 'textfield', text: 'textfield' },
                        { id: 'checkbox', text: 'checkbox' },
                        { id: 'combobox', text: 'combobox' },
                        { id: 'textarea', text: 'textarea' },
                        { id: 'datefield', text: 'datefield' },
                        { id: 'numberfield', text: 'numberfield' }
                    ]
                }),
                listeners: {
                    change: function(bs, newValue, lastValue) {
                        var currentValues = newValue.split(',');
                        if (currentValues.length > 1) {
                            for (var i = 0; i < currentValues.length - 1; i++) {
                                bs.removeValue(currentValues[i]);
                            }
                        }
                    }
                }
            }, {
                name: 'categoryCodeName',
                fieldLabel: 'Category',
                emptyText: 'Other',
                xtype: 'boxselect',
                multiSelect: true,
                forceSelection: false,
                allowAddNewData: true,
                createNewOnEnter: true,
                queryMode: 'local',
                triggerAction: 'all',
                queryDelay: 0,
                minChars: 1,
                displayField: 'categoryDisplayName',
                valueField: 'categoryCodeName',
                listeners: {
                    change: function(bs, newValue, lastValue) {
                        var currentValues = newValue.split(',');
                        if (currentValues.length > 1) {
                            for (var i = 0; i < currentValues.length - 1; i++) {
                                bs.removeValue(currentValues[i]);
                            }
                        }
                    }
                },
                getValue: function() {
                    return this.getValueRecords();
                },
                store: new Ext.data.Store({
                    model: 'userDataField',
                    proxy: {
                        type: 'ajax',
                        url: '/services/customer.svc/user/datafields/0',
                        reader: {
                            type: 'json',
                            root: 'data',
                            idProperty: 'categoryCodeName'
                        }
                    },
                    autoLoad: true
                })
            }, {
                name: 'config',
                fieldLabel: 'Config',
                xtype: 'textarea',
                help: {
                    title: 'Config',
                    text: 'This configuration will be applied to the field when used in the user profile. This must be valid Javascript.'
                }
            }, {
                name: 'validator',
                fieldLabel: 'Validator',
                xtype: 'textarea',
                help: {
                    title: 'Validator',
                    text: 'A valid regular expression used to validate this field when the user saves their profile.'
                }
            }, {
                name: 'isGlobal',
                fieldLabel: 'Global',
                xtype: 'checkbox',
                boxLabel: 'Apply this field to all customers.'
            }],
            listeners: {
                afterrender: function () {
                    if (me.record) {
                        if (me.record.get('customerId') == 0) {
                            me.record.set('isGlobal', true);
                        }
                        me.bindValues(me.record.data);
                    }
                }
            }
        });

        this.callParent(arguments);
    }
});
