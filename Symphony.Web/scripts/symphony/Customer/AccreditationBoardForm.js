﻿Ext.define('Artisan.AccreditationBoardForm', {
    extend: 'Ext.form.Panel',
    xtype: 'artisan.accreditationboardform',
    requires: [
        'Symphony.SaveCancelBar',
        'AccreditationBoardProfessionsStore'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],

    model: null,
    frame: true,
    border: false,
    header: false,
    margin: 0,
    padding: 0,
    layout: 'fit',
    assumeOwnership: ['save-bar'],

    dockedItems: [{
        xtype: 'symphony.savecancelbar',
        itemId: 'save-bar',
        dock: 'top',
        showManually: true,
        listeners: {
            save: function() {
                var me = this,
                    root = me.up('[xtype=artisan.accreditationboardform]');

                Log.debug('Clicked button to save the accredition board.');

                root.save();
            },
            cancel: function() {
                var me = this,
                    root = me.up('[xtype=artisan.accreditationboardform]');

                Log.debug('Clicked button to cancel changes to the accreditation board.');

                root.close();
            }
        }
    }],

    defaults: {
        margin: 5
    },
    items: [{
        xtype: 'tabpanel',
        itemId: 'tabPanel',
        border: false,
        bodyPadding: 12,
        autoScroll: true,

        defaults: {
            frame: true,
            bodyPadding: 5,
            autoScroll: true,
            layout: {
                type: 'vbox',
                align: 'stretch',
                scrollOffset: 40
            }
        },
        items: [{
            title: 'General',
            itemId: 'generalTab',

            items: [{
                xtype: 'fieldset', 
                itemId: 'detailsFieldSet',
                title: 'Details',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },

                defaults: {
                    labelWidth: 130
                },
                items: [{
                    xtype: 'textfield',
                    name: 'name',
                    fieldLabel: 'Name',
                    allowOnlyWhitespace: false,
                    maxLength: 128
                }, {
                    xtype: 'textarea',
                    name: 'description',
                    fieldLabel: 'Description',
                    maxlength: 32768
                }, {
                    xtype: 'textfield',
                    name: 'primaryContact',
                    fieldLabel: 'Primary Contact',
                    maxLength: 64
                }, {
                    xtype: 'textfield',
                    name: 'phone',
                    fieldLabel: 'Phone',
                    maxLength: 32
                }, {
                    xtype: 'associatedimagefield',
                    name: 'logo',
                    fieldLabel: 'Logo'
                }, {
                    xtype: 'symphony.spellcheckarea',
                    itemId: 'disclaimerField',
                    name: 'disclaimer',
                    fieldLabel: 'Disclaimer'
                }, {
                    xtype: 'fieldcontainer', 
                    fieldLabel: 'Professions',

                    items: [{
                        xtype: 'grid',
                        itemId: 'professionsGrid',
                        flex: 1,
                        height: 140,
                        hideHeaders: true,
                        store: {
                            type: 'accreditationBoardProfessions',
                            autoLoad: false
                        },

                        listeners: {
                            itemclick: function(grid, record) {
                                record.set('isAssigned', !record.get('isAssigned'));
                            }
                        },

                        columns: [{
                            xtype: 'checkcolumn',
                            dataIndex: 'isAssigned',
                            width: 32
                        }, {
                            flex: 1,
                            dataIndex: 'name'
                        }]
                    }]
                }]
            }]
        }, {
            title: 'Surveys',
            itemId: 'surveysTab',
            frame: false,
            border: false,
            height: '100%',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },

            defaults: {
                height: '100%'
            },
            items: [{
                xtype: 'grid',
                itemId: 'availableGrid',
                flex: 1,
                store: {
                    type: 'accreditationBoardSurveys',
                    remoteSort: true
                },
                viewConfig: {
                    markDirty: false
                },

                listeners: {
                    itemclick: function(grid, record) {
                        record.set('isChecked', !record.get('isChecked'));
                    }
                },

                columns: {
                    defaults: {
                        hideable: false,
                        draggable: false
                    },

                    items: [{
                        xtype: 'checkcolumn',
                        width: 32,
                        dataIndex: 'isChecked',
                        sortable: false
                    }, {
                        text: 'Name',
                        flex: 3,
                        dataIndex: 'name'
                    }, {
                        text: 'Category',
                        flex: 1,
                        dataIndex: 'category'
                    }]
                },

                dockedItems: {
                    xtype: 'pagingtoolbar',
                    dock: 'bottom'
                }
            }, {
                xtype: 'container',
                flex: 0,
                width: 70,
                padding: 10,
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    pack: 'center'
                },

                defaults: {
                    height: 32
                },
                items: [{
                    xtype: 'button', 
                    text: '>>',
                    margin: {
                        bottom: 8
                    },

                    handler: function() {
                        var me = this.up('[xtype=artisan.accreditationboardform]');

                        me.assign();
                    }
                }, {
                    xtype: 'button',
                    text: '<<',

                    handler: function() {
                        var me = this.up('[xtype=artisan.accreditationboardform]');

                        me.unassign();
                    }
                }]
            }, {
                xtype: 'grid',
                itemId: 'assignedGrid',
                flex: 1,
                store: {
                    type: 'accreditationBoardSurveys'
                },
                viewConfig: {
                    markDirty: false,
                    plugins: {
                        ptype: 'gridviewdragdrop',
                        dragText: 'Drag and drop to change the order of the surveys'
                    }
                },

                listeners: {
                    itemclick: function(grid, record) {
                        record.set('isChecked', !record.get('isChecked'));
                    }
                },

                columns: {
                    defaults: {
                        hideable: false,
                        draggable: false,
                        sortable: false
                    },

                    items: [{
                        xtype: 'checkcolumn',
                        width: 32,
                        dataIndex: 'isChecked'
                    }, {
                        text: 'Name',
                        flex: 3,
                        dataIndex: 'name'
                    }, {
                        text: 'Category',
                        flex: 1,
                        dataIndex: 'category'
                    }]
                }
            }]
        }, {
            title: 'View Accreditations',
            itemId: 'viewTab',

            items: [{
                xtype: 'fieldset',
                itemId: 'accreditationsFieldSet',
                title: 'Accreditations',

                defaults: {
                    labelWidth: 130
                },
                items: [{
                    xtype: 'fieldcontainer',
                    itemId: 'courseContainer',
                    fieldLabel: 'Courses',

                    items: [{
                        xtype: 'artisan.accreditationgrid',
                        itemId: 'courseAccreditationGrid',
                        fieldLabel: 'Accreditation',
                        minHeight: 192,
                        gridType: 'boardCourse',
                        urlFormat: '/services/Accreditation.svc/accreditationBoards/{0}/courseAccreditations'
                    }]
                }, {
                    xtype: 'fieldcontainer',
                    itemId: 'trainingProgramContainer',
                    fieldLabel: 'Training Programs',

                    items: [{
                        xtype: 'artisan.accreditationgrid',
                        itemId: 'trainingProgramAccreditationGrid',
                        fieldLabel: 'Accreditation',
                        minHeight: 192,
                        gridType: 'boardTrainingProgram',
                        urlFormat: '/services/Accreditation.svc/accreditationBoards/{0}/trainingProgramAccreditations'
                    }]
                }]
            }]
        }]
    }],

    initComponent: function() {
        var me = this,
            grid1Cfg = Symphony.Object.findByItemId(me, 'tabPanel.viewTab.accreditationsFieldSet.courseContainer.courseAccreditationGrid'),
            grid2Cfg = Symphony.Object.findByItemId(me, 'tabPanel.viewTab.accreditationsFieldSet.trainingProgramContainer.trainingProgramAccreditationGrid'),
            availableGrid,
            assignedGrid,
            professions,
            prof,
            i;

        grid1Cfg.accreditationBoardId = me.model.getId();
        grid2Cfg.accreditationBoardId = me.model.getId();

        me.callParent();

        if (me.model.get('surveys')) {
            assignedGrid = me.queryById('assignedGrid');
            assignedGrid.getStore().add(me.model.get('surveys'));
        }

        availableGrid = me.queryById('availableGrid');
        availableGrid.query('[xtype=pagingtoolbar]')[0].bindStore(availableGrid.getStore());

        me.refreshGridStatus();
        me.setAssignedProxy(me.model.getId());
        me.refreshAssignedProxyExcludes();

        // Disclaimer isn't being written because setValue is called before the rich text editor is
        // ready.
        me.queryById('disclaimerField').on('boxready', function() {
            this.setValue(me.model.get('disclaimer'));
        });

        if (!me.model) {
            Log.error('Model property is undefined.');
            return;
        }

        me.setTitle(me.model.get('name'));
        if (!me.model.phantom) {
            me.getForm().setValues(me.model.getData());
        }

        me.setProfessionData(me.model.data.professions);
    },

    setAssignedProxy: function(id) {
        var me = this,
            grid = me.queryById('availableGrid'),
            store = grid.getStore(); //Symphony.Object.findByItemId(me, 'tabPanel.surveysTab.availableGrid');

        store.setProxy({
            type: 'rest',
            url: '/services/accreditation.svc/accreditationBoards/' + id + '/surveys',
            simpleSortMode: true,

            reader: {
                type: 'json',
                root: 'data'
            }
        });
    },

    refreshAssignedProxyExcludes: function() {
        var me = this,
            availableGrid = me.queryById('availableGrid'),
            assignedGrid = me.queryById('assignedGrid'),
            availableStore = availableGrid.getStore(),
            assignedStore = assignedGrid.getStore(),
            ids = [];

        assignedStore.each(function(record) {
            ids.push(record.getId());
        });

        if (ids.length > 0) {
            availableStore.getProxy().setExtraParam('exclude', ids.join('.'));
        } else {
            delete availableStore.getProxy().extraParams.exclude;
        }

        availableStore.load();
    },

    assign: function() {
        var me = this,
            availableGrid = me.queryById('availableGrid'),
            assignedGrid = me.queryById('assignedGrid'),
            availableStore = availableGrid.getStore(),
            assignedStore = assignedGrid.getStore(),
            assigned = [];

        availableStore.each(function(record) {
            if (record.get('isChecked')) {
                record.set('isChecked', false);
                assigned.push(record);
            }
        });

        if (assigned.length == 0) {
            return;
        }

        assignedStore.add(assigned);
        me.refreshAssignedProxyExcludes();
    },

    unassign: function() {
        var me = this,
            availableGrid = me.queryById('availableGrid'),
            assignedGrid = me.queryById('assignedGrid'),
            availableStore = availableGrid.getStore(),
            assignedStore = assignedGrid.getStore(),
            available = [];

        assignedStore.each(function(record) {
            if (record.get('isChecked')) {
                record.set('isChecked', false);
                available.push(record);
            }
        });

        if (available.length == 0) {
            return;
        }

        assignedStore.remove(available);
        me.refreshAssignedProxyExcludes();
    },

    save: function() {
        var me = this,
            saveBar = me.queryById('save-bar'),
            professionsGrid = me.queryById('professionsGrid'),
            form = me.getForm(),
            model,
            data;

        saveBar.showSaving();
        Log.info('Saving accreditation board.');

        if (!form.isValid()) {
            saveBar.hideSaving();
            saveBar.showNotSaving();

            Log.error('Cannot submit because form is in an invalid state.');
            return;
        }

        data = me.form.getValues();
        me.model.set(data);

        me.model.data.professions = me.getProfessionData();
        me.model.data.surveys = me.getSurveyData();

        Log.debug('Updating model data to save.', me.model);
        Log.watchModel(me, me.model);

        me.model.save({
            success: Ext.Function.createOwned(me, function() {
                Log.info('Model was successfully saved.');

                me.fireEvent('change', me.model);

                me.refreshGridStatus(me.model.getId());
                me.setAssignedProxy(me.model.getId());
                me.refreshAssignedProxyExcludes();

                // remove dirty indicator
                professionsGrid.getStore().commitChanges();

                me.setTitle(me.model.get('name'));
            }),
            failure: Ext.Function.createOwned(me, function(model, operation) {
                Log.error('Failed to save model.');
                saveBar.showError(Ext.String.format('Save failed: {0}  {1}', operation.error.status, operation.error.statusText));
            })
        });
    },

    getSurveyData: function() {
        var me = this,
            assignedGrid = me.queryById('assignedGrid'),
            data = [],
            i = 1;

        assignedGrid.getStore().each(function(model) {
            model.set('rank', i);
            i++;

            data.push(model.data);
        });

        return data;
    },

    getProfessionData: function() {
        var me = this,
            professionsGrid = me.queryById('professionsGrid'),
            data = [],
            models;

        models = professionsGrid.getStore().getRange();
        Ext.Array.each(models, function(model) {
            data.push(model.data);
        });

        return data;
    },

    setProfessionData: function(data) {
        var me = this,
            professions,
            prof,
            i;

        professions = [];

        if (data) {
            // Professions returned from the server are from a view, so they don't all have Ids. The
            // null ids are coereced to 0s, which messes with the grid bc it assumes they are unique.
            // 
            // The specific id is not important, since we destroy and recreate profession associations
            // on save, so we null out the id before adding the profession so the grid works properly,
            // (it assumes they are all new records).
            for (i = 0; i < data.length; i++) {
                delete data[i].id;

                prof = new accreditationBoardProfession(data[i]);
                professions.push(prof);
            }

            me.queryById('professionsGrid').getStore().loadData(professions);
        } else {
            var store = new ProfessionsStore({});
            store.load(function() {
                store.each(function(record) {
                    prof = new accreditationBoardProfession({
                        professionId: record.getId(),
                        name: record.get('name')
                    });

                    professions.push(prof);

                    me.queryById('professionsGrid').getStore().loadData(professions);
                });
            });
        }
    },

    refreshGridStatus: function(newId) {
        var me = this,
            fieldset = me.queryById('accreditationsFieldSet'),
            grid1 = me.queryById('courseAccreditationGrid'),
            grid2 = me.queryById('trainingProgramAccreditationGrid');

        if (!me.model.phantom) {
            if (newId) {
                grid1.updateUrlId(newId);
                grid2.updateUrlId(newId);
            }

            fieldset.show();
            grid1.getStore().load();
            grid2.getStore().load();
        } else {
            fieldset.hide();
        }
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});