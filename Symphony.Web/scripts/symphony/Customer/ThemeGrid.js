﻿Ext.define('Customer.ThemeGrid', {
    extend: 'symphony.searchablegrid',
    xtype: 'customer.themegrid',
    require: [
        'Theme',
        'Ext.button.Button'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],

    assumeOwnership: ['add-button'],
    model: 'Theme',
    url: '/services/Customer.svc/themes/',
    deferLoad: true,

    columns: [{
        text: 'Name',
        dataIndex: 'name',
        flex: 1
    }, {
        text: 'Code Name',
        dataIndex: 'codeName',
        flex: 1
    }],

    tbar: {
        items: [{
            text: 'Add',
            itemId: 'add-button',
            iconCls: 'x-button-add',
            handler: function() {
                var me = this,
                    root = me.up('[xtype=customer.themegrid]');

                Log.debug('Clicked button to add a new theme.');

                root.fireEvent('addgriditem', new theme({
                    name: 'New Theme'
                }));
            }
        }]
    },

    initComponent: function() {
        var me = this;

        me.callParent();

        Log.watchStore(me, me.getStore());

        me.on('itemclick', function(view, model) {
            Log.debug('Clicked on an grid item to open a new editor.');

            me.fireEvent('opengriditem', model);
        });
    },

    getSelected: function () {
        return this.getSelectionModel().selections;
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});