﻿Symphony.Customer.UserAdministrationGrid = Ext.define('customer.useradministrationgrid', {
    alias: 'widget.customer.useradministrationgrid',
    extend: 'symphony.searchablegrid',
    initComponent: function () {
        var me = this;

        me.salesChannelFilter = null;
        me.customerFilter = null;

        Ext.apply(this, {
            title: 'Users',
            dockedItems: [{
                xtype: 'toolbar',
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    pack: 'start',
                    defaultMargins: 5
                },
                items: [{
                    xtype: 'customer.saleschanneltreepicker',
                    fieldLabel: 'Sales Channel',
                    multiSelect: true,
                    listeners: {
                        salesChannelChange: function (records) {
                            var customerCombo = me.query('[name=customerSearch]')[0],
                                store = customerCombo.getStore(),
                                ids = [];

                            Ext.each(records, function(r) {
                                ids.push(r.getId());
                            });
                            store.filters.clear();

                            if (ids.length > 0) {
                                me.salesChannelFilter = {
                                    property: '[customer].[salesChannelId]',
                                    value: ids.join()
                                }

                                store.addFilter(me.salesChannelFilter, false);
                            } else {
                                me.salesChannelFilter = null;
                            }

                            store.load();

                            me.applyUserFilters();
                        }
                    }
                }, {
                    xtype: 'symphony.pagedcombobox',
                    name: 'customerSearch',
                    fieldLabel: 'Customer',
                    displayField: 'name',
                    valueField: 'id',
                    url: '/services/customer.svc/customers/',
                    model: 'customer',
                    matchFieldWidth: false,
                    clearable: true,
                    queryMode: 'remote',
                    listeners: {
                        change: function (cmb, newVal, oldVal, opts) {
                            if (newVal > 0) {
                                me.customerFilter = {
                                    property: 'customerId',
                                    value: newVal
                                }
                            } else {
                                me.customerFilter = null;
                            }

                            me.applyUserFilters();
                        }
                    }
                }]
            }],
            tbar: {
                items: [{
                    xtype: 'button',
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function() {
                        me.fireEvent('addUser');
                    }
                }]
            },
            columns: {
                items: [{
                    header: 'Username',
                    dataIndex: 'username'
                }, {
                    header: 'Last Name',
                    dataIndex: 'lastName'   
                }, {
                    header: 'First Name',
                    dataIndex: 'firstName'
                }, {
                    header: 'Email',
                    dataIndex: 'email'
                }, {
                    header: 'Customer',
                    dataIndex: 'customerName',
                    getSortParam: function () {
                        return 'customer.name';
                    }
                }, {
                    header: 'SalesChannel',
                    dataIndex: 'salesChannelName',
                    getSortParam: function() {
                        return 'saleschannel.name';
                    }
                }, {
                    header: 'ID',
                    dataIndex: 'id'
                }]
            },
            model: 'user',
            url: '/services/customer.svc/users/',
            stateful: true,
            stateId: 'useradmingrid',
            remoteFilter: true,
            deferLoad: true
        });
        this.callParent(arguments);
    },
    applyUserFilters: function () {
        this.store.filters.clear();

        if (this.salesChannelFilter) {

            this.store.addFilter(this.salesChannelFilter, false);
        }
        if (this.customerFilter) {
            this.store.addFilter(this.customerFilter, false);
        }

        this.store.load();
    }
});