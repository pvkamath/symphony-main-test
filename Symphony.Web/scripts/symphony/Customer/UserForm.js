﻿Symphony.Customer.UserForm = Ext.define('customer.userform', {
    alias: 'widget.customer.userform',
    extend: 'Ext.Panel',
    userId: 0,
    customerId: 0,
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            tbar: {
                xtype: 'symphony.savecancelbar',
                listeners: {
                    save: function () {
                        var form = me.find('xtype', 'form')[0];
                        var salesforceForm = me.find('name', 'salesforce')[0];
                        var securityForm = me.find('name', 'security')[0];
                        var redirectForm = me.find('name', 'redirectForm')[0];

                        var userpermlocationstree = me.find('name', 'userhierarchytreelocations')[0];
                        var userpermjobrolestree = me.find('name', 'userhierarchytreejobroles')[0];
                        var userpermaudiencetree = me.find('name', 'userhierarchytreeaudience')[0];

                        var userRepPermFlag = false;
                        var userHierarchyPermissions = me.find('xtype', 'customer.userhierarchypermissions');
                        if (userHierarchyPermissions.length && userHierarchyPermissions[0].rendered) {
                            userRepPermFlag = true;
                        }

                        if (!form.isValid()) {
                            return false;
                        }
                        var user = form.getValues();
  
                        var userProfile = me.find('name', 'profile')[0];
                        if (userProfile) {
                            Ext.apply(user, userProfile.getData());
                        }

                        var salesforce = salesforceForm.getValues();
                        var security = securityForm.getValues();
                        var redirect = redirectForm.getForm().getValues();

                        if (userRepPermFlag) {

                            var userReportingPermissions = [];
                            if (userpermlocationstree) {
                                var userpermlocationstreeSelected = userpermlocationstree.getChecked();
                                if (userpermlocationstreeSelected.length > 0) {
                                    for (var i = 0; i < userpermlocationstreeSelected.length; i++) {
                                        var newperm = { userId: me.userId, objectId: userpermlocationstreeSelected[i].id, permissionType: 'Location' };
                                        userReportingPermissions.push(newperm);
                                    }
                                }
                            }

                            if (userpermjobrolestree) {
                                var userpermjobrolestreeSelected = userpermjobrolestree.getChecked();
                                if (userpermjobrolestreeSelected.length > 0) {
                                    for (var i = 0; i < userpermjobrolestreeSelected.length; i++) {
                                        var newperm = { userId: me.userId, objectId: userpermjobrolestreeSelected[i].id, permissionType: 'JobRole' };
                                        userReportingPermissions.push(newperm);
                                    }
                                }
                            }

                            if (userpermaudiencetree) {
                                var userpermaudiencetreeSelected = userpermaudiencetree.getChecked();
                                if (userpermaudiencetreeSelected.length > 0) {
                                    for (var i = 0; i < userpermaudiencetreeSelected.length; i++) {
                                        var newperm = { userId: me.userId, objectId: userpermaudiencetreeSelected[i].id, permissionType: 'Audience' };
                                        userReportingPermissions.push(newperm);
                                    }
                                }
                            }
                        }

                        user.userReportingPermissions = userReportingPermissions;

                        Ext.apply(user, salesforce);
                        Ext.apply(user, security);
                        Ext.apply(user, redirect);

                        delete user.failedAttempts;
                        delete user.loginCounter;

                        if (!user.jobRoleId) {
                            user.jobRoleId = 0;
                        }
                        if (!user.locationId) {
                            user.locationId = 0;
                        }
                        if (!user.supervisorId) {
                            user.supervisorId = 0;
                        }
                        if (!user.secondarySupervisorId) {
                            user.secondarySupervisorId = 0;
                        }
                        if (!user.reportingSupervisorId) {
                            user.reportingSupervisorId = 0;
                        }
                        if (!user.statusId) {
                            user.statusId = 1;
                        }
                        if (!user.hireDate) {
                            delete user.hireDate;
                        }

                        if (user.socialSecurityNumber) {
                            user.ssn = user.socialSecurityNumber.replace(/[^0-9]/g, "");
                        }
                        
                        if (user.dateOfBirth) {
                            user.dob = Symphony.parseDateLong(user.dateOfBirth).formatSymphony('m/d/y');
						}

                        user.applicationPermissions = [];

                        Ext.each(me.find('xtype', 'fieldset'), function (fieldset) {
                            fieldset.items.each(function (field) {
                                if (field.getValue && field.getValue() && field.role) {
                                    user.applicationPermissions.push(field.role);
                                }
                            });
                        });

                        var customerId = (me.customerId > 0 ? me.customerId : user.customerId);
                        if (!customerId) {
                            // fall back to the current user, server enforces anyway
                            customerId = Symphony.User.customerId;
                        }
                        user.customerId = customerId;

                        Symphony.Ajax.request({
                            url: '/services/customer.svc/users/' + me.userId + '/customer/' + customerId,
                            jsonData: user,
                            success: function (result) {
                                if (result.notice) {
                                    Ext.Msg.alert("Notice", result.notice);
                                }
                                me.fireEvent('save', result.data);

                                me.customerId = user.customerId;
                                me.userId = result.data.id;

                                if (me.customerId > 0) {
                                    var idField = me.find('name', 'id')[0];
                                    if (idField) {
                                        idField.setValue(result.data.id);
                                        idField.show();
                                    }

                                    var customerPicker = me.query('[name=customerId]');
                                    if (customerPicker && customerPicker.length) {
                                        customerPicker[0].setDisabled(true);
                                    }
                                }
                            }
                        });

                    },
                    cancel: function () {
                        me.fireEvent('cancel');
                    }
                },
                items: [{
                    text: 'Reset Login Counter',
                    iconCls: 'x-button-unlock',
                    handler: function () {
                        Symphony.Ajax.request({
                            url: '/services/customer.svc/users/' + me.userId + '/customer/' + me.customerId + '/reset/',
                            success: function (args) {
                                Ext.Msg.alert('Reset Complete', 'The user\'s account has been successfully unlocked.');
                            }
                        });
                    }
                }].concat(
                    (Symphony.User.isSalesChannelAdmin ?
                    [{
                        text: 'View Transcript',
                        iconCls: 'x-button-transcript',
                        disabled: !(this.userId > 0),
                        handler: function() {
                            Symphony.Portal.showTranscriptWindow(me.userFullName, me.userId);
                        }
                    }] : []))
            },
            items: [{
                xtype: 'tabpanel',
                activeItem: 0,
                border: false,
                cls: 'x-panel-transparent',
                defaults: {
                    border: false,
                    frame: false,
                    autoScroll: true,
                    cls: 'x-panel-transparent',
                    bodyPadding: 5
                },
                items: [{
                    xtype: 'form',
                    title: 'Basic Information',

                    labelWidth: 125,
                    defaults: {
                        anchor: '96%',
                        xtype: 'textfield'
                    },
                    items: [{
                        hidden: !me.isCustomerAdministration,
                        xtype: 'symphony.pagedcombobox',
                        allowBlank: false,
                        disabled: me.customerId > 0 || !me.isCustomerAdministration,
                        name: 'customerId',
                        fieldLabel: 'Customer',
                        displayField: 'name',
                        valueField: 'id',
                        url: '/services/customer.svc/customers/',
                        model: 'customer',
                        matchFieldWidth: false,
                        clearable: true,
                        remoteFilter: true,
                        queryMode: 'remote',
                        bindingName: 'customerName'
                    }, {
                        name: 'id',
                        fieldLabel: 'ID',
                        disabled: true
                    },{
                        name: 'username',
                        // https://code.google.com/p/chromium/issues/detail?id=468153
                        fieldLabel: Symphony.Aliases.username,
                        allowBlank: false
                    }, {
                        name: 'password',
                        fieldLabel: 'Password',
                        inputType: Symphony.User.hasPasswordMask ? 'password' : 'text',
                        allowBlank: true,
                        help: {
                            text: 'Passwords must be at least 7 characters long and must contain at least 1 digit and 1 letter.'
                        }
                    }, {
                        name: 'employeeNumber',
                        fieldLabel: 'Employee Number',
                        allowBlank: true
                    }, {
                        name: 'firstName',
                        fieldLabel: 'First Name',
                        allowBlank: false
                    }, {
                        name: 'middleName',
                        fieldLabel: 'Middle Name',
                        allowBlank: true
                    }, {
                        name: 'lastName',
                        fieldLabel: 'Last Name',
                        allowBlank: false
                    }, {
                        name: 'hireDate',
                        fieldLabel: 'Hire Date',
                        allowBlank: true,
                        xtype: 'datefield',
                        dateOnly: true
                    }, {
                        name: 'newHireIndicator',
                        fieldLabel: 'New Hire',
                        xtype: 'checkbox',
                        listeners: {
                            check: function (field, checked) {
                                // when this changes, require the new hire end date to be set?
                                var endDate = field.ownerCt.find('name', 'newHireEndDate')[0];
                                if (checked) {
                                    endDate.setValue('');
                                    endDate.disable();
                                } else {
                                    if (!endDate.getValue()) {
                                        endDate.setValue(new Date());
                                    }
                                    endDate.enable();
                                }
                            }
                        }
                    }, {
                        name: 'newHireEndDate',
                        fieldLabel: 'New Hire End Date',
                        allowBlank: true,
                        xtype: 'datefield',
                        dateOnly: true
                    }, {
                        name: 'email',
                        fieldLabel: 'Email',
                        allowBlank: true
                    }, {
                        name: 'nmlsNumber',
                        fieldLabel: 'NMLS Number',
                        allowBlank: true
                    }, {
                        name: 'supervisorId',
                        bindingName: 'supervisor',

                        allowBlank: true,
                        displayField: 'fullName',
                        valueField: 'id',

                        fieldLabel: 'Classroom Supervisor',
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/supervisors/customer/' + me.customerId,
                        model: 'user',

                        emptyText: 'Select a Supervisor'
                    }, {
                        name: 'secondarySupervisorId',
                        bindingName: 'secondarySupervisor',

                        allowBlank: true,
                        displayField: 'fullName',
                        valueField: 'id',

                        fieldLabel: 'Secondary Supervisor',
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/supervisors/customer/' + me.customerId,
                        model: 'user',

                        emptyText: 'Select a Secondary Supervisor'
                    }, {
                        name: 'reportingSupervisorId',
                        bindingName: 'reportingSupervisor',

                        allowBlank: true,
                        displayField: 'fullName',
                        valueField: 'id',

                        fieldLabel: 'Reporting Supervisor',
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/reportingsupervisors/customer/' + me.customerId,
                        model: 'user',

                        emptyText: 'Select a Reporting Supervisor'
                    }, {
                        name: 'statusId',
                        fieldLabel: 'Status',
                        allowBlank: false,

                        bindingName: 'status',
                        displayField: 'description',
                        valueField: 'id',
                        value: 1,
                        valueNotFoundText: 'Active',
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/userstatuses/',
                        model: 'userStatus',

                        emptyText: 'Select a Status'
                    }, {
                        name: 'locationId',
                        bindingName: 'location',
                        displayField: 'name',
                        valueField: 'id',

                        fieldLabel: Symphony.Aliases.location,
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/locations/customer/' + me.customerId,
                        model: 'locationx',

                        emptyText: 'Select a ' + Symphony.Aliases.location + '...'
                    }, {
                        name: 'jobRoleId',
                        bindingName: 'jobRole',
                        displayField: 'name',
                        valueField: 'id',

                        fieldLabel: Symphony.Aliases.jobRole,
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/jobroles/customer/' + me.customerId,
                        model: 'jobRole',

                        emptyText: 'Select a ' + Symphony.Aliases.jobRole + '...'
                    }, {
                        name: 'timeZone',
                        fieldLabel: 'Time Zone',
                        allowBlank: true,
                        xtype: 'symphony.nettimezonecombo',
                        emptyText: 'Select a time zone...'
                    }, {
                        name: 'notes',
                        fieldLabel: 'Notes',
                        xtype: 'textarea'
                    }, {
                        name: 'isLockedOut',
                        fieldLabel: 'Locked Out',
                        disabled: true,
                        xtype: 'checkbox'
                    }, {
                        name: 'failedAttempts',
                        fieldLabel: 'Failed Attempts',
                        disabled: true,
                        allowBlank: false
                    }, {
                        name: 'isAuthor',
                        xtype: 'checkbox',
                        fieldLabel: 'Is Author',
                        inputValue: true,
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Books)
                    }, {
                        name: 'speciality',
                        xtype: 'textarea',
                        fieldLabel: 'Speciality',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Books)
                    }, {
                        name: 'associatedImageData',
                        fieldLabel: 'Picture',
                        xtype: 'associatedimagefield',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Libraries)
                    }]
                }, {
                    xtype: 'form',
                    title: 'Landing Page Override',
                    name: 'redirectForm',
                    defaults: {
                        anchor: '96%',
                        xtype: 'textfield'
                    },
                    items: [{
                        xtype: 'fieldset',
                        title: 'Landing Page',
                        defaults: { anchor: '100%', xtype: 'textfield' },
                        items: [{
                            xtype: 'radio',
                            name: 'redirectType',
                            fieldLabel: 'Default',
                            inputValue: Symphony.RedirectType.symphony,
                            boxLabel: 'Use customer configuration for this user.',
                            handler: function (chk, checked) {
                                if (checked) {
                                    me.query("[name=loginRedirect]")[0].setDisabled(true);
                                }
                            }
                        }, {
                            xtype: 'radio',
                            name: 'redirectType',
                            fieldLabel: 'Simple Redirect',
                            inputValue: Symphony.RedirectType.simple,
                            boxLabel: 'Redirect this user to the Alternate Landing Page URL entered below.',
                            handler: function (chk, checked) {
                                if (checked) {
                                    me.query("[name=loginRedirect]")[0].setDisabled(false);
                                }
                            }
                        }, {
                            xtype: 'radio',
                            name: 'redirectType',
                            fieldLabel: 'Opt-In Redirect',
                            inputValue: Symphony.RedirectType.optIn,
                            boxLabel: 'Present this user with the option to redirect to the Alternate Landing Page URL',
                            handler: function (chk, checked) {
                                if (checked) {
                                    me.query("[name=loginRedirect]")[0].setDisabled(false);
                                }
                            }
                        }, {
                            xtype: 'radio',
                            name: 'redirectType',
                            fieldLabel: 'Force Redirect',
                            inputValue: Symphony.RedirectType.force,
                            boxLabel: 'Force this user to the Alternate Landing Page URL',
                            handler: function (chk, checked) {
                                if (checked) {
                                    me.query("[name=loginRedirect]")[0].setDisabled(false);
                                }
                            }
                        }, {
                            name: 'loginRedirect',
                            fieldLabel: 'Alternate Landing Page URL',
                            xtype: 'textfield',
                            help: {
                                title: 'Landing Page Redirect URL',
                                text: 'URL to redirect to after the user has logged in. The behavior will be defined by the option selected above.'
                            },
                            labelStyle: {
                                marginTop: '10px'
                            }
                        }]
                    }]
                }, {
                    xtype: 'customer.profiletab',
                    name: 'profile'
                }, {
                    xtype: 'form',
                    title: 'Security',
                    labelWidth: 145,
                    name: 'security',
                    cls: 'x-panel-transparent ' + Symphony.Modules.getModuleClass(Symphony.Modules.Validation),
                    hidden: !Symphony.Modules.hasModule(Symphony.Modules.Validation),
                    
                    defaults: {
                        anchor: '96%',
                        xtype: 'textfield'
                    },
                    items: [{
                        name: 'socialSecurityNumber',
                        fieldLabel: 'SSN',
                        disabled: false,
                        allowBlank: true,
                        help: {
                            text: 'Social Security Number is stored encrypted.'
                        }
                    }, {
                        name: 'dateOfBirth',
                        fieldLabel: 'Date of Birth',
                        allowBlank: true,
                        xtype: 'datefield',
                        dateOnly: true,
						isUseParseDateLong: true,
                        help: {
                            text: 'Date of birth is used to validate users while taking an online course.'
                        }
                    }]
                }, {
                    xtype: 'form',
                    title: 'Salesforce',
                    labelWidth: 145,
                    name: 'salesforce',
                    cls: 'x-panel-transparent ' + Symphony.Modules.getModuleClass(Symphony.Modules.Salesforce),
                    hidden: !Symphony.Modules.hasModule(Symphony.Modules.Salesforce),
                    defaults: {
                        anchor: '96%',
                        xtype: 'textfield'
                    },
                    items: [{
                        name: 'telephoneNumber',
                        fieldLabel: 'Phone',
                        allowBlank: true,
                        help: {
                            text: 'The phone number provided for this user if created by Salesforce.'
                        }
                    }, {
                        name: 'mobile',
                        fieldLabel: 'Mobile',
                        allowBlank: true,
                        help: {
                            text: 'The mobile number provided for this user if created by Salesforce.'
                        }
                    }, {
                        name: 'fax',
                        fieldLabel: 'Fax',
                        allowBlank: true,
                        help: {
                            text: 'The fax number provided for this user if created by Salesforce.'
                        }
                    }, {
                        name: 'janrainUserUuid',
                        fieldLabel: 'Janrain Uuid',
                        allowBlank: true,
                        help: {
                            text: 'The user id for this user\'s Janrain account if created by Salesforce.'
                        }
                    }, {
                        name: 'salesforceAccountId',
                        fieldLabel: 'Salesforce Account Id',
                        allowBlank: true,
                        help: {
                            text: 'The id of the Salesforce account this user is part of.'
                        }
                    }, {
                        name: 'salesforceAccountName',
                        fieldLabel: 'Salesforce Account Name',
                        allowBlank: true,
                        help: {
                            text: 'The name of the Salesforce account this user is part of.'
                        }
                    }, {
                        name: 'salesforceContactId',
                        fieldLabel: 'Salesforce Contact Id',
                        allowBlank: true,
                        help: {
                            text: 'The Salesforce contact id for this user.'
                        }
                    }]
                }, {
                    xtype: 'form',
                    title: 'Permissions' + (Symphony.User.id == me.userId ? ' (Read Only)' : ''),
                    labelWidth: 330,
                    defaults: {
                        xtype: 'fieldset',
                        anchor: '96%'
                    },
                    items: [{
                        title: 'Classroom Training',
                        defaults: { xtype: 'checkbox', disabled: (Symphony.User.id == me.userId) },
                        items: [{
                            role: 'Classroom - ILT Manager',
                            fieldLabel: 'CT Manager',
                            help: { text: Symphony.PermissionText['Classroom - ILT Manager'], isCheckbox: true }
                        }, {
                            role: 'Classroom - Instructor',
                            fieldLabel: 'Instructor',
                            help: { text: Symphony.PermissionText['Classroom - Instructor'], isCheckbox: true }
                        }, {
                            role: 'Classroom - Resource Manager',
                            fieldLabel: 'Resource Manager',
                            help: { text: Symphony.PermissionText['Classroom - Resource Manager'], isCheckbox: true }
                        }, {
                            role: 'Classroom - Supervisor',
                            fieldLabel: 'Supervisor',
                            help: { text: Symphony.PermissionText['Classroom - Supervisor'], isCheckbox: true }
                        }]
                    }, {
                        title: 'Student Management',
                        defaults: { xtype: 'checkbox', disabled: (Symphony.User.id == me.userId) },
                        items: [{
                            role: 'Customer - Administrator',
                            fieldLabel: 'Administrator',
                            help: { text: Symphony.PermissionText['Customer - Administrator'], isCheckbox: true }
                        }, {
                            role: 'Customer - Manager',
                            name: 'customerManager',
                            fieldLabel: 'Manager',
                            help: { text: Symphony.PermissionText['Customer - Manager'], isCheckbox: true },
                            listeners: {
                                check: function (checkbox, checked) {
                                    if (checked) {
                                        me.find('name', 'customerJobRoleManager')[0].setValue(false);
                                        me.find('name', 'customerLocationManager')[0].setValue(false);
                                    }
                                }
                            }
                        }, {
                            role: 'Customer - Job Role Manager',
                            name: 'customerJobRoleManager',
                            fieldLabel: 'Job Role Manager',
                            help: { text: Symphony.PermissionText['Customer - Job Role Manager'], isCheckbox: true },
                            listeners: {
                                check: function (checkbox, checked) {
                                    if (checked) {
                                        me.find('name', 'customerManager')[0].setValue(false);
                                    }
                                }
                            }
                        }, {
                            role: 'Customer - Location Manager',
                            name: 'customerLocationManager',
                            fieldLabel: 'Location Manager',
                            help: { text: Symphony.PermissionText['Customer - Location Manager'], isCheckbox: true },
                            listeners: {
                                check: function (checkbox, checked) {
                                    if (checked) {
                                        me.find('name', 'customerManager')[0].setValue(false);
                                    }
                                }
                            }
                        }, {
                            role: 'Customer - User',
                            fieldLabel: 'User',
                            checked: me.userId ? null : true, // default to true
                            help: { text: Symphony.PermissionText['Customer - User'], isCheckbox: true }
                        }]
                    }, {
                        title: 'Artisan',
                        defaults: { xtype: 'checkbox', disabled: (Symphony.User.id == me.userId) },
                        items: [{
                            role: 'Artisan - Author',
                            fieldLabel: 'Author',
                            help: { text: Symphony.PermissionText['Artisan - Author'], isCheckbox: true }
                        }, {
                            role: 'Artisan - Packager',
                            fieldLabel: 'Packager',
                            help: { text: Symphony.PermissionText['Artisan - Packager'], isCheckbox: true }
                        }, {
                            role: 'Artisan - Viewer',
                            fieldLabel: 'Viewer',
                            help: { text: Symphony.PermissionText['Artisan - Viewer'], isCheckbox: true }
                        }]
                    }, {
                        title: 'Course Management (<a href="#" onclick="Symphony.Customer.clearPermission()">clear</a>)',
                        defaults: { xtype: 'radio', disabled: (Symphony.User.id == me.userId) },
                        items: [{
                            name: 'courseAssignmentRole',
                            role: 'CourseAssignment - Training Administrator',
                            fieldLabel: 'Training Administrator',
                            help: { text: Symphony.PermissionText['CourseAssignment - Training Administrator'], isCheckbox: true }
                        }, {
                            name: 'courseAssignmentRole',
                            role: 'CourseAssignment - Training Manager',
                            fieldLabel: 'Training Manager',
                            help: { text: Symphony.PermissionText['CourseAssignment - Training Manager'], isCheckbox: true }
                        }]
                    }, {
                        title: 'Reporting',
                        defaults: { xtype: 'checkbox', disabled: (Symphony.User.id == me.userId) },
                        items: [{
                            role: 'Reporting - Analyst',
                            fieldLabel: 'Analyst',
                            name: 'reportingAnalyst',
                            help: { text: Symphony.PermissionText['Reporting - Analyst'], isCheckbox: true },
                            listeners: {
                                check: function (checkbox, checked) {
                                    if (checked) {
                                        me.find('name', 'reportingJobRoleAnalyst')[0].setValue(false);
                                        me.find('name', 'reportingLocationAnalyst')[0].setValue(false);
                                        me.find('name', 'reportingAudienceAnalyst')[0].setValue(false);
                                    }
                                }
                            }
                        }, {
                            role: 'Reporting - Job Role Analyst',
                            fieldLabel: 'Job Role Analyst',
                            name: 'reportingJobRoleAnalyst',
                            help: { text: Symphony.PermissionText['Reporting - Job Role Analyst'], isCheckbox: true },
                            listeners: {
                                check: function (checkbox, checked) {
                                    if (checked) {
                                        me.find('name', 'reportingAnalyst')[0].setValue(false);
                                    }
                                }
                            }
                        }, {
                            role: 'Reporting - Location Analyst',
                            fieldLabel: 'Location Analyst',
                            name: 'reportingLocationAnalyst',
                            help: { text: Symphony.PermissionText['Reporting - Location Analyst'], isCheckbox: true },
                            listeners: {
                                check: function (checkbox, checked) {
                                    if (checked) {
                                        me.find('name', 'reportingAnalyst')[0].setValue(false);
                                    }
                                }
                            }
                        }, {
                            role: 'Reporting - Audience Analyst',
                            fieldLabel: 'Audience Analyst',
                            name: 'reportingAudienceAnalyst',
                            help: { text: Symphony.PermissionText['Reporting - Audience Analyst'], isCheckbox: true },
                            listeners: {
                                check: function (checkbox, checked) {
                                    if (checked) {
                                        me.find('name', 'reportingAnalyst')[0].setValue(false);
                                    }
                                }
                            }
                        }, {
                            role: 'Reporting - Report Developer',
                            fieldLabel: 'Report Developer',
                            help: { text: Symphony.PermissionText['Reporting - Report Developer'], isCheckbox: true }
                        }, {
                            role: 'Reporting - Supervisor',
                            fieldLabel: 'Reporting Supervisor',
                            help: { text: Symphony.PermissionText['Reporting - Supervisor'], isCheckbox: true }
                        }]
                    }, {
                        title: 'Collaboration',
                        defaults: { xtype: 'checkbox', disabled: (Symphony.User.id == me.userId) },
                        items: [{
                            role: 'Collaboration - GTM Organizer',
                            fieldLabel: 'GTM Organizer',
                            help: { text: Symphony.PermissionText['Collaboration - GTM Organizer'], isCheckbox: true }
                        }, {
                            role: 'Collaboration - GTW Organizer',
                            fieldLabel: 'GTW Organizer',
                            help: { text: Symphony.PermissionText['Collaboration - GTW Organizer'], isCheckbox: true }
                        }]
                    }]
                }].concat(Symphony.User.isSalesChannelAdmin ? [{
                    xtype: 'customer.usertileform',
                    userId: me.userId,
                    title: 'Tiles',
                    bodyPadding: 0
                }, {
                    xtype: 'customer.userhierarchypermissions',
                    userId: me.userId,
                    customerId: me.customerId,
                    title: 'Reporting Permissions'
                }, {
                    title: 'Licenses',
                    xtype: 'license.userassignmenttab',
                    userId: me.userId,
                    url: '/services/license.svc/reports/user/' + me.userId
                }] : []).concat(Symphony.User.isTrainingAdministrator ? [{
                    xtype: 'courseassignment.trainingprogramstab',
                    userId: me.userId,
                    isIncludeSalesforce: true,
                    title: 'Training Programs',
                    bodyPadding: 0,
                    width: me.width
                }] : [])
            }]
        });
        this.callParent(arguments);
    },
    load: function () {
        var me = this;
        var idField = me.find('name', 'id')[0];
        me.fireEvent('beforeload');

        if (this.userId) {
            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/customer.svc/users/' + this.userId + '/customer/' + this.customerId,
                success: function (result) {

                    if (result.data.loginRedirect && !(result.data.redirectType >= 1)) {
                        result.data.redirectType = Symphony.RedirectType.simple;
                    } else if (!result.data.loginRedirect && !result.data.redirectType) {
                        result.data.redirectType = Symphony.RedirectType.symphony;
                    }

				    Ext.each(me.find('xtype', 'form'), function (form) {
                		form.bindValues(result.data);
                    });
                    Ext.each(me.find('xtype', 'fieldset'), function (fieldset) {
                        fieldset.items.each(function (field) {
                            if (result.data.applicationPermissions && result.data.applicationPermissions.indexOf(field.role) > -1) {
                                field.setValue(true);
                            }
                        });
                    });

                    if (result.data.fullName) {
                        me.userFullName = result.data.fullName;
                    }

                    me.loadProfile(result.data);

                    me.fireEvent('loaded', result.data);
                }
            });
            idField.show();
        } else {
            // require passwords for new users
            me.find('name', 'password')[0].allowBlank = false;
            // default status to active
            me.find('name', 'statusId')[0].setValue(1, true);
            // default user id to 0 and hide
            
            idField.setValue(0);
            idField.hide();

            me.fireEvent('loaded', null);

            me.loadProfile();
        }
    },
    loadProfile: function(data) {
        var profileTab = this.query('[xtype=customer.profiletab]')[0];
        if (profileTab) {
            profileTab.load(data);
        }
    },
    onRender: function () {
        Symphony.Customer.UserForm.superclass.onRender.apply(this, arguments);
        window.setTimeout(Ext.bind(this.load, this));
    }
});



Symphony.Customer.clearPermission = function () {
    Ext.each(Ext.getCmp('usereditor').find('xtype', 'radio'), function (item) {
        item.setValue(false);
    });
};
    