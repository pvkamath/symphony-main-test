﻿Ext.define('Customer.ThemeTab', {
    extend: 'Ext.Panel',
    xtype: 'customer.themetab',
    requires: [
        'Theme',
        'Ext.tab.Panel'
    ],
    uses: 'Customer.ThemeForm',
    mixins: ['Symphony.mixins.FunctionOwnership'],

    layout: 'border',

    items: [{
        xtype: 'customer.themegrid',
        itemId: 'grid',
        region: 'west',

        border: false,
        split: true,
        width: 290,

        listeners: {
            addgriditem: function(model) {
                var me = this,
                    root = me.up('[xtype=customer.themetab]');

                root.addPanel(model);
            },

            opengriditem: function(model) {
                var me = this,
                    root = me.up('[xtype=customer.themetab]');

                root.addPanel(model);
            }
        }
    }, {
        xtype: 'tabpanel',
        itemId: 'tab-panel',
        region: 'center',
        border: false
    }],

    initComponent: function() {
        var me = this;

        me.callParent();

        me.on('activate', function() {
            var grid = me.queryById('grid');

            Log.debug('Theme tab activated, refreshing grid.');
            grid.refresh();
        });
    },

    addPanel: function(model) {
        var me = this,
            grid = me.queryById('grid'),
            tabPanel = me.queryById('tab-panel'),
            editor;

        Log.info('Opening a new theme editor.');

        editor = tabPanel.items.findBy(function(el, key) {
            if (el.model && !el.model.phantom) {
                return el.model.getId() == model.getId();
            }

            return false;
        });

        if (!editor) {
            Log.debug('Editor is not open, opening an editor for the model.', model);

            if (model.getId() > 0) {
                me.setLoading(true);
                Theme.load(model.getId(), {
                    success: function(record) {
                        editor = tabPanel.add(new Customer.ThemeForm({
                            model: record,
                            closable: true
                        }));

                        editor.on('change', function() {
                            grid.refresh();
                        });

                        tabPanel.activate(editor);
                    },
                    callback: function() {
                        me.setLoading(false);
                    }
                });
            } else {
                editor = tabPanel.add(new Customer.ThemeForm({
                    model: model,
                    closable: true
                }));

                editor.on('change', function() {
                    grid.refresh();
                });

                tabPanel.activate(editor);
            }
        } else {
            Log.debug('Editor is already open for the model, switching focus.', editor);
        }

        tabPanel.activate(editor);
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});