﻿Symphony.Customer.CustomerForm = Ext.define('customer.profilefieldstab', {
    alias: 'widget.customer.profilefieldstab',
    extend: 'Ext.Panel',
    /*formDefaults: {
        xtype: 'textfield',
        anchor: '96%',
        labelWidth: 145
    },
    */
    customerId: 0,
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            name: 'profile',
            border: false,
            frame: false,
            layout: {
                type: 'border'
            },
            items: [{
                xtype: 'customer.profilefieldsgrid',
                region: 'west',
                width: 300,
                tbar: {
                    items: [{
                        xtype: 'button',
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.addPanel();
                        }
                    }]
                },
                listeners: {
                    itemclick: function (grid, record, item, index, e) {
                        me.addPanel(record);
                    }
                }
            }, {
                xtype: 'tabpanel',
                region: 'center'
            }]
        });

        this.callParent(arguments);
    },
    addPanel: function (record) {
        var me = this;

        var tabPanel = me.query('tabpanel')[0];
        var found;

        if (record && record.get('userDataFieldId')) {
            found = tabPanel.queryBy(function (element) {
                if (element && element.userDataFieldId == record.get('userDataFieldId')) {
                    return true;
                }
                return false;
            });
        }

        if (found && found.length > 0) {
            tabPanel.activate(found[0]);
        } else {
            var formPanel = Ext.create('customer.profilefieldform', {
                xtype: 'customer.profilefieldform',
                userDataFieldId: record && record.get('userDataFieldId') ? record.get('userDataFieldId') : 0,
                record: record,
                title: record && record.get('displayName') ? record.get('displayName') : 'New Field',
                customerId: me.customerId,
                closable: true,
                listeners: {
                    refresh: function () {
                        me.query('[xtype=customer.profilefieldsgrid]')[0].refresh();
                    }
                }
            });

            tabPanel.add(formPanel);
            tabPanel.activate(formPanel);
        }

        tabPanel.doLayout();
    }
});
