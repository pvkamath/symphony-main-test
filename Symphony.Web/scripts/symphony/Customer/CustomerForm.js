﻿Symphony.Customer.CustomerForm = Ext.define('customer.customerform', {
    alias: 'widget.customer.customerform',
    extend: 'Ext.Panel',
    customerId: 0,
    salesChannelId: 0,
    initComponent: function () {
        var me = this;

        var modules = [];
        for (var prop in Symphony.Modules) {
            if (Symphony.Modules.hasOwnProperty(prop) && Ext.isString(Symphony.Modules[prop])) {
                var label = prop.replace(/([a-z])([A-Z])/g, '$1 $2');
                modules.push({
                    name: Symphony.Modules[prop],
                    fieldLabel: label,
                    xtype: 'checkbox'
                });
            }
        }

        Ext.apply(this, {
            tbar: {
                xtype: 'symphony.savecancelbar',
                listeners: {
                    save: function () {
                        var form = me.find('xtype', 'form')[0];
                        
                        if (!form.isValid()) { return false; }

                        var customer = form.getValues();
                        if (!customer.evaluationIndicator) {
                            delete customer.evaluationEndDate;
                        }
                        if (me.salesChannelId) {
                            customer.salesChannelId = me.salesChannelId;
                        }

                        // get the modules, convert to string format
                        var modules = me.find('name', 'modules')[0];
                        customer.modules = Symphony.keys(modules.getValues(), function (p, v) { return v == true; }).join(',');

                        // get the advanced fields
                        var advanced = me.find('name', 'advanced')[0];
                        Symphony.merge(customer, advanced.getForm().getValues());

                        // get the contact fields
                        var contacts = me.find('name', 'contacts')[0];
                        Symphony.merge(customer, contacts.getValues());

                        // get the external system fields
                        var externalSystems = me.find('name', 'externalSystemLogin')[0];
                        Symphony.merge(customer, externalSystems.getValues());

                        // external systems selected
                        var externalSystemGrid = me.find('xtype', 'customer.externalsystemsgrid')[0];
                        customer.externalSystems = externalSystemGrid.getSelected();

                        if (customer.maxTestAttempts) {
                            customer.retries = customer.maxTestAttempts - 1;
                        } else {
                            customer.retries = null;
                        }

                        if (!customer.enforcePasswordDays) {
                            customer.enforcePasswordDays = 30;
                        }

                        if (!customer.locationId) {
                            customer.locationId = 0;
                        }

                        if (!customer.maxCourseWork) {
                            delete customer.maxCourseWork;
                        }

                        if (!customer.accountExecutive) {
                            delete customer.accountExecutive;
                        }
                        if (!customer.customerCareRep) {
                            delete customer.customerCareRep;
                        }
                        if (!customer.ssoType) {
                            delete customer.ssoType;
                        }

                        if (!customer.ssoLoginUiType) {
                            customer.ssoLoginUiType = 1;
                        }
                        if (customer.ssoEnabled == "on" || customer.ssoEnabled === true) {
                            customer.ssoEnabled = true;
                        } else {
                            customer.ssoEnabled = false;
                        }
                        var portalConfigPanel = me.find('xtype', 'portal.portalpanel')[0];
                        if (portalConfigPanel.rendered) {
                            var portalColumns = portalConfigPanel.query('[xtype=portalcolumn]'),
                                custPortalSettings = new Array();
                            
                            for (var i = 0; i < portalColumns.length; i++) {
                                var portlets = portalColumns[i].query('[xtype=customer.portalconfigurationportlet]');

                                for (var j = 0; j < portlets.length; j++) {
                                    var portlet = portlets[j],
                                        position = (i + 1) + (j * 2);

                                    custPortalSettings.push({
                                        widgetOrderNumber: position,
                                        widgetName: portlet.title,
                                        widgetIsVisible: portlet.getChecked()
                                    });
                                }
                            }
                            
                            customer.portalSettings = custPortalSettings;
                        }

                        Symphony.Ajax.request({
                            url: '/services/customer.svc/customers/' + me.customerId,
                            jsonData: customer,
                            success: function (result) {
                                me.fireEvent('save', result.data);
                                me.customerId = result.data.id;
                                if (me.customerId > 0) {
                                    var salesChannelPicker = me.query('[name=salesChannelId]');
                                    if (salesChannelPicker && salesChannelPicker.length) {
                                        salesChannelPicker[0].setDisabled(true);
                                    }
                                }
                            }
                        });
                    },
                    cancel: function () { me.fireEvent('cancel'); }
                }
            },
            items: [{
                border: false,
                bodyStyle: 'padding: 15px',
                xtype: 'tabpanel',
                activeTab: 0,
                items: [{
                    border: false,
                    title: 'Basic Settings',
                    xtype: 'form',
                    frame: true,
                    defaults: { anchor: '96%', xtype: 'textfield' },
                    labelWidth: 155,
                    autoScroll: true,
                    items: [{
                        hidden: !me.isCustomerAdministration,
                        name: 'salesChannelId',
                        fieldLabel: 'Sales Channel',
                        xtype: 'customer.saleschanneltreepicker',
                        multiSelect: false,
                        allowBlank: false,
                        disabled: me.salesChannelId > 0 || !me.isCustomerAdministration
                    }, {
                        name: 'name',
                        fieldLabel: 'Name',
                        allowBlank: false
                    }, {
                        name: 'selfRegistrationIndicator',
                        fieldLabel: 'Forgot Password Link',
                        xtype: 'checkbox'
                    }, {
                        name: 'canUsersChangePassword',
                        fieldLabel: 'Password Change',
                        xtype: 'checkbox'
                    }, {
                        name: 'hasPasswordMask',
                        fieldLabel: 'Password Mask',
                        xtype: 'checkbox'
                    }, {
                        name: 'enforcePasswordReset',
                        fieldLabel: 'Enforce Password Reset',
                        xtype: 'checkbox'
                    }, {
                        name: 'allowSelfAccountCreation',
                        fieldLabel: 'Allow Self Account Creation',
                        xtype: 'checkbox'
                    }, {
                        name: 'quickQueryIndicator',
                        fieldLabel: 'Quick Query',
                        xtype: 'checkbox'
                    }, {
                        name: 'reportBuilderIndicator',
                        fieldLabel: 'Report Builder',
                        xtype: 'checkbox'
                    }, {
                        name: 'suspendedIndicator',
                        fieldLabel: 'Is Suspended',
                        xtype: 'checkbox'
                    }, {
                        name: 'evaluationIndicator',
                        fieldLabel: 'Is Evalutation',
                        xtype: 'checkbox',
                        listeners: {
                            check: function (box, value) {
                                var field = me.ownerCt.find('name', 'evaluationEndDate')[0];
                                if (value) {
                                    field.enable();
                                    field.allowBlank = false;
                                } else {
                                    field.disable();
                                    field.allowBlank = true;
                                }
                            }
                        }
                    }, {
                        name: 'evaluationEndDate',
                        fieldLabel: 'Evaluation End Date',
                        xtype: 'datefield',
                        disabled: true,
                        allowBlank: true
                    }, {
                        name: 'userLimit',
                        fieldLabel: 'User Limit',
                        xtype: 'numberfield',
                        allowNegative: false,
                        allowDecimals: false,
                        allowBlank: false
                    }, {
                        name: 'enforcePasswordDays',
                        fieldLabel: 'Password Duration (days)',
                        xtype: 'numberfield',
                        allowNegative: false,
                        allowDecimals: false,
                        help: {
                            title: 'Password Duration in Days',
                            text: 'The amount of days a password is allowed to work. After the days have passed, users will be forced to reset their password. If you want to enforce this feature, make sure you check the "Enforce Password Reset" checkbox above'
                        }
                    }, {
                        name: 'usernameAlias',
                        fieldLabel: 'Username Alias'
                    }, {
                        name: 'maxSignInAttempts',
                        fieldLabel: 'Max Sign In Attempts',
                        xtype: 'numberfield'
                    }, {
                        name: 'lockedOutMessage',
                        fieldLabel: 'User Locked Out Text'
                    }, {
                        name: 'lockedOutPeriodInMinutes',
                        fieldLabel: 'Locked Out Period in Min. (use 0 to require Admin)',
                        xtype: 'numberfield'
                    }, {
                        name: 'locationAlias',
                        fieldLabel: 'Location Alias'
                    }, {
                        name: 'jobRoleAlias',
                        fieldLabel: 'Job Role Alias'
                    }, {
                        name: 'audienceAlias',
                        fieldLabel: 'Audience Alias'
                    }, {
                        name: 'courseAlias',
                        fieldLabel: 'Course Alias'
                    }, {
                        name: 'newHireDuration',
                        fieldLabel: 'New Hire Duration (days)',
                        allowBlank: false,
                        allowNegative: false,
                        allowDecimals: false
                    }, {
                        name: 'newHireTransitionPeriod',
                        fieldLabel: 'New Hire Transition Period',
                        help: {
                            title: 'New Hire Transition Period',
                            text: 'The minumum number of days a Training Program must be available to a user in order to be seen by that user after a transition from "New Hire" to "Normal" status.<br/><br/>' +
                            'NOTE: If left blank, this defaults to 0 days.'
                        },
                        allowBlank: false,
                        allowNegative: true,
                        allowDecimals: false,
                        value: 0
                    }, {
                        name: 'subdomain',
                        fieldLabel: 'Sub-Domain',
                        allowBlank: false
                    }, {
                        name: 'locationId',
                        bindingName: 'customerLocation',
                        displayField: 'name',
                        valueField: 'id',

                        fieldLabel: Symphony.Aliases.location,
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/locations/customer/' + me.customerId,
                        model: 'location',

                        emptyText: 'Select a ' + Symphony.Aliases.location + '...'
                    }, {
                        name: 'organizerSeats',
                        fieldLabel: 'Organizer Seats',
                        allowBlank: false,
                        value: 0
                    }, {
                        name: 'timeZone',
                        fieldLabel: 'Time Zone',
                        allowBlank: false,
                        xtype: 'symphony.nettimezonecombo'
                    }, {
                        name: 'maxTestAttempts',
                        fieldLabel: 'Max Online Test Attempts',
                        help: {
                            title: 'Max Online Test Attempts',
                            text: 'The maximum number of tests attempts to allow, by default, for an online course. If left blank, allows unlimited or follows the course setting.'
                        },
                        allowBlank: true,
                        xtype: 'numberfield',
                        minValue: 1
                    }, {
                        name: 'maxCourseWork',
                        fieldLabel: 'Max Daily Course Work',
                        help: {
                            title: 'Max Daily Course Work',
                            text: 'The maximum number of hours allowed per day for online course work. If left blank, allows unlimited time.'
                        },
                        allowBlank: true,
                        xtype: 'numberfield',
                        minValue: 1
                    }, {
                        name: 'helpUrl',
                        fieldLabel: 'Help URL',
                        vtype: 'url',
                        vtypeText: 'Please enter a valid URL',
                        help: {
                            title: 'Help URL',
                            text: 'If entered the help button will display at the top toolbar. When clicked this url will open in a new window.'
                        },
                        allowBlank: true
                    }, {
                        name: 'description',
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        help: {
                            title: 'Description',
                            text: 'Company description, used for generic details about the company.'
                        },
                        allowBlank: true
                    }, {
                        name: 'associatedImageData',
                        fieldLabel: 'Logo',
                        xtype: 'associatedimagefield',
                        noPreviewResize: true
                    }, {
                        xtype: 'combobox',
                        name: 'themeId',
                        fieldLabel: 'Theme',
                        displayField: 'name',
                        valueField: 'id',
                        lastQuery: '',
                        forceSelection: true,
                        queryParam: 'searchText',
                        pageSize: 10,
                        emptyText: 'Select a theme',
                        store: {
                            autoLoad: true,
                            type: 'themes',
                            listeners: {
                                load: function() {
                                    this.insert(0, new Theme({
                                        id: null,
                                        name: 'None'
                                    }));
                                }
                            }
                        }
                    }]
                }, {
                    title: 'Modules',
                    xtype: 'form',
                    name: 'modules',
                    frame: true,
                    border: false,
                    labelWidth: 145,
                    autoScroll: true,
                    items: modules
                }, {
                    title: 'Reps',
                    xtype: 'form',
                    name: 'contacts',
                    frame: true,
                    defaults: { anchor: '100%' },
                    labelWidth: 155,
                    items: [{
                        name: 'accountExecutive',
                        bindingName: 'accountExecutiveFullName',

                        allowBlank: true,
                        displayField: 'fullName',
                        valueField: 'id',
                        fieldLabel: 'Account Executive',
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/accountexecutives/customer/' + me.customerId,
                        model: 'user',

                        emptyText: 'Select an Account Executive'
                    }, {
                        name: 'customerCareRep',
                        bindingName: 'customerCareRepFullName',

                        allowBlank: true,
                        displayField: 'fullName',
                        valueField: 'id',
                        fieldLabel: 'Customer Care',
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/customercarereps/customer/' + me.customerId,
                        model: 'user',

                        emptyText: 'Select a Customer Care Rep'
                    }]
                }, {
                    title: 'Advanced',
                    xtype: 'form',
                    name: 'advanced',
                    frame: true,
                    border: false,
                    labelWidth: 200,
                    autoScroll: true,
                    items: [{
                        xtype: 'fieldset',
                        title: 'Single Sign On',
                        defaults: { anchor: '100%', xtype: 'textfield' },
                        items: [{
                            name: 'ssoEnabled',
                            fieldLabel: 'Enable Single Sign-On',
                            xtype: 'checkbox'
                        }, {
                            xtype: 'combo',
                            fieldLabel: 'Single Sign-On Type',
                            triggerAction: 'all',
                            queryMode: 'local',
                            store: Symphony.SsoTypeStore,
                            valueField: 'id',
                            displayField: 'text',
                            name: 'ssoType'
                        }, {
                            xtype: 'combo',
                            fieldLabel: 'Single Sign-On Login UI',
                            triggerAction: 'all',
                            queryMode: 'local',
                            store: Symphony.SsoLoginUiTypeStore,
                            valueField: 'id',
                            displayField: 'text',
                            name: 'ssoLoginUiType',
                            value: 1,
                            help: {
                                title: 'Single Sign-On Login UI',
                                text: '<br/><b>Standard</b><br/>' +
                                      'Only displays the standard Symphony login screen, but allows automic login through the SSO provider.' +
                                      '<br/><br/><b>Mixed</b><br/>' +
                                      'Displays options on the login screen to use the standard Symphony login, or to login using an account from the SSO provider.' +
                                      '<br/><br/><b>Forced</b><br/>' +
                                      'Only allows login through the SSO provider. Users will not be able to use the standard Symphony login.'
                            }
                        }, {
                            name: 'ssoTimeoutRedirect',
                            fieldLabel: 'Single Sign-On Timeout Redirect',
                            xtype: 'textfield',
                            help: {
                                title: 'Single Sign-On Timeout Redirect',
                                text: 'The URL to which the user should be redirected upon timeout. If left unset, the user will be redirected to the default "SsoTimeoutLanding.aspx" page built into Symphony.'
                            },
                            labelStyle: {
                                marginTop: '10px'
                            }
                        }, {
                            name: 'ssoTimeoutMessage',
                            fieldLabel: 'Single Sign-On Timeout Message',
                            xtype: 'textfield',
                            help: {
                                title: 'Single Sign-On Timeout Message',
                                text: 'If the redirect URL is not specified, this message will be shown to the user when they hit the "SsoTimeoutLanding.aspx" page.'
                            },
                            labelStyle: {
                                marginTop: '10px'
                            }
                        }, {
                            name: 'identityProviderUrl',
                            fieldLabel: 'Identity Provider URL',
                            xtype: 'textfield',
                            help: {
                                title: 'Identity Provider URL',
                                text: 'The URL of the company authenticating. This is the property that must be sent by the 3rd party when authenticating to Symphony as this company.'
                            },
                            labelStyle: {
                                marginTop: '10px'
                            }
                        }, {
                            xtype: 'fieldcontainer',
                            fieldLabel: 'Verification Certificate:',
                            layout: {
                                type: 'hbox',
                                pack: 'start'
                            },
                            items: [{
                                name: 'certificateFileName',
                                xtype: 'textfield',
                                readOnly: true,
                                style: {
                                    color: '#666'
                                },
                                width: 350,
                                help: {
                                    title: 'Public Key Certificate',
                                    text: 'The certificate file must contain the public key for Symphony to verify sign-in requests.'
                                }
                            }, {
                                text: 'Upload Certificate',
                                xtype: 'swfuploadbutton',
                                uploadUrl: '/Uploaders/CustomerCertificateUploader.ashx',
                                iconCls: 'x-button-cert',
                                fileTypes: '*.*',
                                fileTypesDescription: 'All Files',
                                //fileQueueLimit: 1,
                                listeners: {
                                    'queue': function (file) {
                                        this.startUpload();
                                    },
                                    'queueerror': function () {
                                        Ext.Msg.alert('Error', 'The file could not be queued');
                                    },
                                    'uploadstart': function (item) {
                                        this.addParameter('customerId', me.customerId);

                                        Ext.Msg.progress('Please wait...', 'Please wait while your certificate is uploaded...');
                                    },
                                    'uploadprogress': function (item, completed, total) {
                                        var ratio = parseFloat(completed / total);
                                        Ext.Msg.updateProgress(ratio, parseInt(ratio * 100, 10) + "% complete");
                                    },
                                    'uploaderror': function () {
                                        this.error = true;
                                        Ext.Msg.hide();
                                        Ext.Msg.alert('Upload failed', 'The upload failed. Please make sure it is within the proper file size limitations.');
                                    },
                                    'uploadsuccess': function (item, response, hasData) {
                                        var result = Ext.decode(response);
                                        if (result.success) {
                                            Ext.Msg.hide();
                                            me.find('name', 'advanced')[0].findField('certificateFileName').setValue(result.value);
                                        } else {
                                            Ext.Msg.hide();
                                            Ext.Msg.alert('Upload failed', result.error);
                                        }
                                    }
                                }
                            }]
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Landing Page',
                        defaults: { anchor: '100%', xtype: 'textfield' },
                        items: [{
                            xtype: 'radio',
                            name: 'redirectType',
                            fieldLabel: 'Symphony',
                            inputValue: Symphony.RedirectType.symphony,
                            boxLabel: 'Do not redirect any users. Use the default Symphony UI for all users.',
                            handler: function (chk, checked) {
                                if (checked) {
                                    me.query("[name=loginRedirect]")[0].setDisabled(true);
                                }
                            }
                        }, {
                            xtype: 'radio',
                            name: 'redirectType',
                            fieldLabel: 'Simple Redirect',
                            inputValue: Symphony.RedirectType.simple,
                            boxLabel: 'Redirect all standard users (Users with 1 role that are not admins) to the Alternate Landing Page URL entered below.',
                            handler: function (chk, checked) {
                                if (checked) {
                                    me.query("[name=loginRedirect]")[0].setDisabled(false);
                                }
                            }
                        }, {
                            xtype: 'radio',
                            name: 'redirectType',
                            fieldLabel: 'Opt-In Redirect',
                            inputValue: Symphony.RedirectType.optIn,
                            boxLabel: 'Users will be presented with the option to redirect to the Alternate Landing Page URL',
                            handler: function (chk, checked) {
                                if (checked) {
                                    me.query("[name=loginRedirect]")[0].setDisabled(false);
                                }
                            }
                        }, {
                            xtype: 'radio',
                            name: 'redirectType',
                            fieldLabel: 'Force Redirect',
                            inputValue: Symphony.RedirectType.force,
                            boxLabel: 'All users will be redirected to the Alternate Landing Page URL',
                            handler: function (chk, checked) {
                                if (checked) {
                                    me.query("[name=loginRedirect]")[0].setDisabled(false);
                                }
                            }
                        }, {
                            name: 'loginRedirect',
                            fieldLabel: 'Alternate Landing Page URL',
                            xtype: 'textfield',
                            help: {
                                title: 'Landing Page Redirect URL',
                                text: 'URL to redirect to after the user has logged in. The behavior will be defined by the option selected above.'
                            },
                            labelStyle: {
                                marginTop: '10px'
                            }
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Opt-In Message Configuration',
                        defaults: {
                            anchor: '100%'
                        },
                        items: [{
                            name: 'optInTitle',
                            fieldLabel: 'Title',
                            xtype: 'textfield'
                        }, {
                            name: 'optInMessage',
                            fieldLabel: 'Message',
                            xtype: 'htmleditor',
                            listeners: {
                                afterlayout: function () {
                                    if (me.customerData) {
                                        this.setValue(me.customerData.optInMessage);
                                    }
                                },
                                change: function () {
                                    if (me.customerData) {
                                        me.customerData.optInMessage = this.getValue();
                                    }
                                }
                            }
                        }, {
                            name: 'optInConfirmText',
                            fieldLabel: 'Confirm Button Text',
                            xtype: 'textfield'
                        }, {
                            name: 'optInPreviewText',
                            fieldLabel: 'Preview Button Text',
                            xtype: 'textfield'
                        }, {
                            name: 'optInCancelText',
                            fieldLabel: 'Cancel Button Text',
                            xtype: 'textfield'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Mail and DNS',
                        defaults: {
                            anchor: '100%'
                        },
                        items: [{
                            fieldLabel: 'Email Sender Address',
                            name: 'email',
                            xtype: 'textfield',
                            allowBlank: true,
                            help: {
                                title: 'Email Sender Address',
                                text: 'The email address from which notifications will be sent. MUST match a valid mail domain in the email sender system (e.g., Mandrill using @bankersedge.com)'
                            }
                        }, {
                            fieldLabel: 'Default HTTP Protocol',
                            name: 'defaultProtocol',
                            xtype: 'textfield',
                            allowBlank: true,
                            regex: new RegExp(/^https?$/),
                            help: {
                                text: 'The protocol (http/https) that will be used if the current context can\'t be determined, for example from scheduled notifications',
                                title: 'HTTP Protocol'
                            }
                        }, {
                            fieldLabel: 'Default HTTP Authority',
                            name: 'defaultAuthority',
                            xtype: 'textfield',
                            allowBlank: true,
                            help: {
                                title: 'Default HTTP Authority',
                                text: 'The authority (base domain) that will be used when we can\'t figure it out automatically'
                            }
                        }]
                    }]
                }, {
                    title: 'External System Login',
                    xtype: 'form',
                    name: 'externalSystemLogin',
                    frame: true,
                    border: false,
                    defaults: { anchor: '100%', xtype: 'textfield' },
                    labelWidth: 200,
                    items: [{
                        name: 'isExternalSystemLoginEnabled',
                        fieldLabel: 'Enable External System Login',
                        xtype: 'checkbox',
                        listeners: {
                            check: function (chbox, checked) {
                                var externalSystemsGrid = me.find('xtype', 'customer.externalsystemsgrid')[0];

                                if (checked) {
                                    externalSystemsGrid.setDisabled(false);
                                } else {
                                    externalSystemsGrid.setDisabled(true);
                                }
                            }
                        }
                    }, {
                        xtype: 'panel',
                        name: 'extSystemPanel',
                        height: 470,
                        layout: 'fit',
                        items: [{
                            name: 'externalSystems',
                            xtype: 'customer.externalsystemsgrid',
                            title: 'External Systems',
                            layout: 'fit',
                            listeners: {
                                render: function (grid) {
                                    var externalCheckbox = me.find('name', 'isExternalSystemLoginEnabled')[0];

                                    if (externalCheckbox.checked) {
                                        grid.setDisabled(false);
                                    } else {
                                        grid.setDisabled(true);
                                    }
                                }
                            }
                        }]
                    }]
                }, {
                    title: 'Portal Layout',
                    xtype: 'portal.portalpanel',
                    stateId: 'portalCustomerConfig',
                    id: 'portalCustomerConfig',
                    currentState: {},
                    title: 'Portal Layout',
                    width: 380,
                    height: 380,
                    border: false,
                    frame: false,
                    getState: function () {
                        return Ext.getCmp('portalCustomerConfig').currentState;
                    },
                    applyState: function (e) {
                        var portal = Ext.getCmp('portalCustomerConfig');

                        for (var id in e) {
                            var portlet = portal.query("[itemId=" + id + "]");
                            portal.moveItem(portlet, e[id].row, e[id].column);
                        }

                        portal.currentState = e;
                    },
                    listeners: {
                        // update the state information every time an item in the portal is moved
                        drop: function (e) {
                            var index = e.columnIndex;
                            var position = e.position;
                            var id = e.panel.id;
                            Ext.getCmp('portalCustomerConfig').currentState[id] = { row: position, column: index };
                        }
                    },
                    items: [{
                        columnWidth: 0.5,
                        style: 'padding-right: 10px',
                        defaults: {
                            xtype: 'customer.portalconfigurationportlet'
                        },
                        items: [{
                            itemId: 'portal.trainingprograms',
                            title: 'Training Programs'
                        }, {
                            itemId: 'portal.publiccourses',
                            title: 'Public Courses'
                        }, {
                            itemId: 'portal.libraries',
                            title: 'Libraries'
                        }]
                    }, {
                        columnWidth: 0.5,
                        defaults: {
                            xtype: 'customer.portalconfigurationportlet'
                        },
                        items: [{
                            itemId: 'portal.upcomingevents',
                            title: 'Upcoming Events'
                        }, {
                            itemId: 'portal.publicdocuments',
                            title: 'Public Documents'
                        }, {
                            itemId: 'portal.librarybrowser',
                            title: 'Library Browser',
                            height: 310
                        }]
                    }]
                }, {
                    xtype: 'customer.profilefieldstab',
                    title: 'Profile Fields',
                    customerId: me.customerId
                }]
            }],
            listeners: {
                afterrender: function () {
                    me.loadData();
                }
            }
        });
        this.callParent(arguments);
    },
    loadData: function () {
        if (this.customerId) {
            var me = this;
            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/customer.svc/customers/' + this.customerId,
                success: function (result) {
                    if (result.data.retries || result.data.retries === 0) {
                        result.data.maxTestAttempts = result.data.retries + 1;
                    }

                    if (result.data.loginRedirect && !(result.data.redirectType >= 1)) {
                        result.data.redirectType = Symphony.RedirectType.simple;
                    } else if (!result.data.loginRedirect && !result.data.redirectType) {
                        result.data.redirectType = Symphony.RedirectType.symphony;
                    }

                    var form = me.find('xtype', 'form')[0];
                    
                    result.data.optInMessage = result.data.optInMessage || Symphony.Portal.optInDefaults.optInMessage;
                    result.data.optInTitle = result.data.optInTitle || Symphony.Portal.optInDefaults.optInTitle;
                    result.data.optInConfirmText = result.data.optInConfirmText || Symphony.Portal.optInDefaults.optInConfirmText;
                    result.data.optInPreviewText = result.data.optInPreviewText || Symphony.Portal.optInDefaults.optInPreviewText;
                    result.data.optInCancelText = result.data.optInCancelText || Symphony.Portal.optInDefaults.optInCancelText;

                    form.bindValues(result.data);
                    
                    form.findField('subdomain').disable();

                    var modules = me.find('name', 'modules')[0];
                    var available = result.data.modules.split(',');

                    modules.items.each(function (field) {
                        field.setValue(available.indexOf(field.name) > -1);
                    });

                    var advanced = me.find('name', 'advanced')[0];
                    advanced.bindValues(result.data);
                                      

                    if (result.data.certificateFileName == null || result.data.certificateFileName == '') {
                        advanced.findField('certificateFileName').setValue("No certificate uploaded");
                    }

                    if (result.data.ssoLoginUiType == null || result.data.ssoLoginUiType == '') {
                        advanced.findField('ssoLoginUiType').setValue(1);
                        advanced.findField('ssoLoginUiType').setRawValue('Standard');
                    }

                    var contacts = me.find('name', 'contacts')[0];
                    contacts.bindValues(result.data);

                    var externalSystems = me.find('name', 'externalSystemLogin')[0];
                    externalSystems.bindValues(result.data);

                    var externalSystemsGrid = me.find('xtype', 'customer.externalsystemsgrid')[0];
                    externalSystemsGrid.setSelections(result.data.externalSystems);
                    var portalPanel = me.query('[xtype=portal.portalpanel]')[0];
                    portalPanel.applyConfiguration(result.data.portalSettings);

                    me.customerData = result.data;
                }
            });
        } else {
            var salesChannelField = this.query('[name=salesChannelId]')[0];
            salesChannelField.getStore().load();
        }
    }
});
