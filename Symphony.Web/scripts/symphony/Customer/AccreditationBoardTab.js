﻿/**
 * AccreditationBoardTab is a panel that wraps a grid of accreditation boards
 * and a sub tab panel that displays accreditation board forms.
 */
Ext.define('Artisan.AccreditationBoardTab', {
    extend: 'Ext.Panel',
    xtype: 'artisan.accreditationboardtab',
    requires: [
        'Artisan.AccreditationBoardGrid',
        'Ext.tab.Panel'
    ],
    uses: 'Artisan.AccreditationBoardForm',
    mixins: ['Symphony.mixins.FunctionOwnership'],

    layout: 'border',

    items: [{
        xtype: 'artisan.accreditationboardgrid',
        itemId: 'grid',
        region: 'west',

        border: false,
        split: true,
        width: 290,

        listeners: {
            addgriditem: function(model) {
                var me = this,
                    root = me.up('[xtype=artisan.accreditationboardtab]');

                root.addPanel(model);
            },

            opengriditem: function(model) {
                var me = this,
                    root = me.up('[xtype=artisan.accreditationboardtab]');

                root.addPanel(model);
            }
        }
    }, {
        xtype: 'tabpanel',
        itemId: 'tab-panel',
        region: 'center',
        border: false
    }],

    initComponent: function() {
        var me = this;

        me.callParent();

        me.on('activate', function() {
            var grid = me.queryById('grid');

            Log.debug('Accreditation board tab activated, refreshing grid.');
            grid.refresh();
        });
    },

    addPanel: function(board) {
        var me = this,
            grid = me.queryById('grid'),
            tabPanel = me.queryById('tab-panel'),
            editor;

        Log.info('Opening a new accredition board editor.');

        editor = tabPanel.items.findBy(function(el, key) {
            if (el.model && !el.model.phantom) {
                return el.model.getId() == board.getId();
            }

            return false;
        });

        if (!editor) {
            Log.debug('Editor is not open, opening an editor for the model.', board);

            if (board.getId() != 0) {
                me.setLoading(true);
                AccreditationBoard.load(board.getId(), {
                    success: function(record) {
                        editor = tabPanel.add(new Artisan.AccreditationBoardForm({
                            model: record,
                            closable: true
                        }));

                        editor.on('change', function() {
                            grid.refresh();
                        });

                        tabPanel.activate(editor);
                    },
                    callback: function() {
                        me.setLoading(false);
                    }
                });
            } else {
                editor = tabPanel.add(new Artisan.AccreditationBoardForm({
                    model: board,
                    closable: true
                }));

                editor.on('change', function() {
                    grid.refresh();
                });

                tabPanel.activate(editor);
            }
        } else {
            Log.debug('Editor is already open for the model, switching focus.', editor);
        }

        tabPanel.activate(editor);
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});