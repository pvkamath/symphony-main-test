﻿/**
 * Grid that displays a list of accreditation boards.
 */
Ext.define('Artisan.AccreditationBoardGrid', {
    extend: 'symphony.searchablegrid',
    xtype: 'artisan.accreditationboardgrid',
    require: [
        'AccreditationBoard',
        'Ext.button.Button'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],

    assumeOwnership: ['add-button', 'delete-button'],
    model: 'AccreditationBoard',
    url: '/services/Accreditation.svc/accreditationBoards/',
    deferLoad: true,

    columns: [{
        text: 'Name',
        dataIndex: 'name',
        flex: 1
    }, {
        text: 'Primary Contact',
        dataIndex: 'primaryContact',
        flex: 1
    }, {
        text: 'Phone',
        dataIndex: 'phone',
        flex: 1
    }],

    tbar: {
        items: [{
            text: 'Add',
            itemId: 'add-button',
            iconCls: 'x-button-add',
            handler: function() {
                var me = this,
                    root = me.up('[xtype=artisan.accreditationboardgrid]');

                Log.debug('Clicked button to add a new accredition board.');

                root.fireEvent('addgriditem', new AccreditationBoard({
                    name: 'New Accreditation Board'
                }));
            }
        }, {
            text: 'Delete',
            itemId: 'delete-button',
            iconCls: 'x-button-delete',
            handler: function () {
                var root = this.up('[xtype=artisan.accreditationboardgrid]');
                var node = root.getSelectionModel().getSelection()[0];
                var id = node.get('id');
                var me = this;

                Ext.Msg.show({
                    title: 'Delete?',
                    msg: 'Are you sure you want to delete this license?',
                    buttons: Ext.Msg.YESNO,
                    animEl: 'elId',
                    icon: Ext.MessageBox.QUESTION,
                    fn: function () {
                        console.log("DELETE AB clicked: ", id);
                        Symphony.Ajax.request({
                            url: '/services/accreditation.svc/accreditations/' + id,
                            method: 'DELETE',
                            jsonData: { id: id },
                            success: function (result) {
                                root.fireEvent('delete', node);
                                root.refresh();
                            }
                        });
                    }
                });
            }
        }]
    },

    initComponent: function() {
        var me = this;

        me.callParent();

        Log.watchStore(me, me.getStore());

        me.on('itemclick', function(view, model) {
            Log.debug('Clicked on an grid item to open a new editor.');

            me.fireEvent('opengriditem', model);
        });
    },

    getSelected: function () {
        return this.getSelectionModel().selections;
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});