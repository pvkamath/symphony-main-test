﻿Symphony.Customer.UserAdministrationTab = Ext.define('customer.useradministrationtab', {
    alias: 'widget.customer.useradministrationtab',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            layout: 'border',
            items: [{
                region: 'west',
                width: 300,
                split: true,
                collapsible: true,
                xtype: 'customer.useradministrationgrid',
                listeners: {
                    itemclick: function (grid, record, item, index, e, opts) {
                        me.addPanel(record);
                    },
                    addUser: function () {
                        var user = Ext.create('user', {
                            id: 0,
                            customerId: 0,
                            salesChannelId: 0
                        });

                        me.addPanel(user);
                    }
                }
            }, {
                region: 'center',
                xtype: 'tabpanel',
                name: 'userEditorContainer'
            }],
            listeners: {
                activate: {
                    fn: function (panel) {
                        panel.query('[xtype=customer.useradministrationgrid]')[0].refresh();
                        panel.query('[xtype=customer.saleschanneltreepicker]')[0].store.load();
                    },
                    single: true
                }
            }
        });
        this.callParent(arguments);
    },
    addPanel: function (record) {
        var tabpanel = this.query('tabpanel')[0];
        var panel = tabpanel.queryBy(function (p) {
            return p.tabId == 'user' + record.get('id');
        })[0];

        if (!panel) {
            panel = new Symphony.Customer.UserAdministrationEditor({
                userId: record.get('id'),
                customerId: record.get('customerId'),
                salesChannelId: record.get('salesChannelId'),
                tabId: 'user' + record.get('id')
            });

            tabpanel.add(panel);
            tabpanel.doLayout();
        }

        tabpanel.layout.setActiveItem(panel);
    }
});