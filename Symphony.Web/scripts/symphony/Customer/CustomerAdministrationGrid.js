﻿Symphony.Customer.CustomerAdministrationGrid = Ext.define('customer.customeradministrationgrid', {
    alias: 'widget.customer.customeradministrationgrid',
    extend: 'symphony.searchablegrid',
    initComponent: function () {
        var me = this;

        me.salesChannelFilter = null;
        
        Ext.apply(this, {
            title: 'Customers',
            dockedItems: [{
                xtype: 'toolbar',
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    pack: 'start',
                    defaultMargins: 5
                },
                items: [{
                    xtype: 'customer.saleschanneltreepicker',
                    fieldLabel: 'Sales Channel',
                    multiSelect: true,
                    listeners: {
                        salesChannelChange: function (records) {
                            var ids = [];

                            Ext.each(records, function(r) {
                                ids.push(r.getId());
                            });
                            
                            if (ids.length > 0) {
                                me.salesChannelFilter = {
                                    property: '[customer].[salesChannelId]',
                                    value: ids.join()
                                }
                            } else {
                                me.salesChannelFilter = null;
                            }

                            me.applyCustomerFilters();
                        }
                    }
                }]
            }],
            tbar: {
                items: [{
                    xtype: 'button',
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function() {
                        me.fireEvent('addCustomer');
                    }
                }]
            },
            columns: {
                items: [{
                    header: 'Name',
                    dataIndex: 'name'   
                }, {
                    header: 'Sub Domain',
                    dataIndex: 'subdomain'
                }, {
                    header: 'SalesChannel',
                    dataIndex: 'salesChannelName',
                    getSortParam: function() {
                        return 'saleschannel.name';
                    }
                }]
            },
            model: 'customer',
            url: '/services/customer.svc/customers/',
            deferLoad: true
        });
        this.callParent(arguments);
    },
    applyCustomerFilters: function () {
        this.store.filters.clear();

        if (this.salesChannelFilter) {

            this.store.addFilter(this.salesChannelFilter, false);
        }

        this.store.load();
    }
});