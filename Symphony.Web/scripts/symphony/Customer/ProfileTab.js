﻿Symphony.Customer.ProfileTab = Ext.define('customer.profiletab', {
    alias: 'widget.customer.profiletab',
    extend: 'Ext.Panel',
    formDefaults: {
        xtype: 'textfield',
        anchor: '96%',
        labelWidth: 145
    },
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            title: 'Profile',
            name: 'profile',
            border: false,
            frame: false,
            layout: {
                type: 'accordion'
            },
            defaults: {
                xtype: 'form',
                frame: false,
                border: false,
                autoScroll: true,
                bodyPadding: 10
            },
            items: [{
                title: 'Contact Information',
                defaults: me.formDefaults,
                items: [{
                    name: 'homePhone',
                    fieldLabel: 'Home phone',
                    allowBlank: true
                }, {
                    name: 'workPhone',
                    fieldLabel: 'Work phone',
                    allowBlank: true
                }, {
                    name: 'cellPhone',
                    fieldLabel: 'Cell phone',
                    allowBlank: true
                }, {
                    name: 'alternateEmail1',
                    fieldLabel: 'Alternate Email 1',
                    allowBlank: true
                }, {
                    name: 'alternateEmail2',
                    fieldLabel: 'Alternate Email 2',
                    allowBlank: true
                }]
            }, {
                title: 'Address',
                defaults:  me.formDefaults,
                items: [{
                    name: 'addressLine1',
                    fieldLabel: 'Address Line 1',
                    allowBlank: true
                }, {
                    name: 'addressLine2',
                    fieldLabel: 'Address Line 2',
                    allowBlank: true
                }, {
                    name: 'city',
                    fieldLabel: 'City',
                    allowBlank: true
                }, {
                    name: 'state',
                    fieldLabel: 'State',
                    allowBlank: true
                }, {
                    name: 'zipCode',
                    fieldLabel: 'Zip Code',
                    allowBlank: true
                }]
            }, {
                title: 'Profession',
                defaults:  me.formDefaults,
                items: [{
                    name: 'isAuthor',
                    xtype: 'checkbox',
                    fieldLabel: 'Is Author',
                    inputValue: true
                }, {
                    name: 'speciality',
                    xtype: 'textarea',
                    fieldLabel: 'Speciality'
                }, {
                    name: 'primaryProfessionId',
                    bindingName: 'primaryProfessionName',
                    xtype: 'symphony.pagedcombobox',
                    fieldLabel: 'Primary Profession',
                    allowBlank: true,
                    url: '/services/license.svc/profession/',
                    //recordDef: Ext.data.Record.create(Symphony.Definitions.profession),
                    model: 'profession',
                    forceSelection: false,
                    emptyText: 'Enter or select a primary profession',
                    anchor: '100%',
                    displayField: 'name',
                    valueField: 'id'
                }, {
                    name: 'secondaryProfessionId',
                    bindingName: 'secondaryProfessionName',
                    xtype: 'symphony.pagedcombobox',
                    fieldLabel: 'Secondary Profession',
                    allowBlank: true,
                    url: '/services/license.svc/profession/',
                    //recordDef: Ext.data.Record.create(Symphony.Definitions.profession),
                    model: 'profession',
                    forceSelection: false,
                    emptyText: 'Enter or select a secondary profession',
                    anchor: '100%',
                    displayField: 'name',
                    valueField: 'id'
                }]
            }]
        });

        this.callParent(arguments);
    },
    getData: function() {
        var forms = this.query('form');
        var profile = {
            userDataFields: []
        };

        for (var i = 0; i < forms.length; i++) {
            var form = forms[i].getForm();

            if (!forms[i].isDynamic) {
                var formData = form.getValues();
                Ext.apply(profile, formData);
            } else {
                var formFields = form.getFields();
                for (var j = 0; j < formFields.items.length; j++) {
                    var field = formFields.items[j];
                    if (field) {
                        profile.userDataFields.push({
                            userValue: field.getValue(),
                            userDataFieldUserMapId: field.userDataFieldUserMapId,
                            userDataFieldId: field.userDataFieldId
                        });
                    }
                }
            }
        }

        if (profile.primaryProfessionId && !Ext.isNumber(profile.primaryProfessionId)) {
            profile.primaryProfessionName = profile.primaryProfessionId;
            profile.primaryProfessionId = 0;
        } else if (!profile.primaryProfessionId) {
            profile.primaryProfessionId = 0;
        }
        if (profile.secondaryProfessionId && !Ext.isNumber(profile.secondaryProfessionId)) {
            profile.secondaryProfessionName = profile.secondaryProfessionId;
            profile.secondaryProfessionId = 0;
        } else if (!profile.secondaryProfessionId) {
            profile.secondaryProfessionId = 0;
        }

        return profile;
    },
    load: function (data) {
        var me = this;

        if (!data || !data.userDataFields) {
            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/customer.svc/user/datafields/' + (data && data.id ? data.id : 0),
                success: function (result) {
                    me.buildForm(result.data);
                }
            });
        } else {
            me.buildForm(data.userDataFields);
        }
    },
    buildForm: function (fields) {
        var configuration = {};

        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];

            if (!field.categoryCodeName) {
                field.categoryCodeName = 'other';
            }
            if (!field.categoryDisplayName) {
                field.categoryDisplayName = 'Other';
            }

            if (!configuration[field.categoryCodeName]) {
                configuration[field.categoryCodeName] = {
                    xtype: 'form',
                    isDynamic: true,
                    name: field.categoryCodeName,
                    title: field.categoryDisplayName,
                    defaults: this.formDefaults,
                    items: []
                }
            }

            if (field.config) {
                try {
                    var parsedConfig = JSON.parse(field.config);
                    field.config = parsedConfig;
                } catch (e) {
                    field.config = null;
                    console.error(e);
                }
            }

            var fieldConfig = {
                name: field.codeName,
                xtype: field.xtype ? field.xtype : 'textfield',
                fieldLabel: field.displayName,
                userDataFieldUserMapId: field.userDataFieldUserMapId,
                userDataFieldId: field.userDataFieldId,
                value: field.userValue ?
                    field.userValue :
                    field.config && field.config['default'] ?
                        field.config['default'] :
                        ''
            }

            if (fieldConfig.xtype === 'checkbox') {
                if (fieldConfig.value === 'on' || fieldConfig.value === 1 || fieldConfig.value === true || fieldConfig.value === 'true') {
                    fieldConfig.checked = true;
                    fieldConfig.value = null;
                    fieldConfig.uncheckedValue = false;
                }
            }

            if (field.config) {
                Ext.apply(fieldConfig, field.config);
            }

            

            configuration[field.categoryCodeName].items.push(fieldConfig);
        }

        for (var codeName in configuration) {
            if (configuration.hasOwnProperty(codeName)) {
                var panel = Ext.create('Ext.form.Panel', configuration[codeName]);
                this.add(panel);
            }
        }

        this.doLayout();
    }
});
