﻿Symphony.Customer.SalesChannelTreePicker = Ext.define('customer.saleschanneltreepicker', {
    alias: 'widget.customer.saleschanneltreepicker',
    extend: 'Ext.ux.TreePicker',
    initComponent: function () {
        var me = this;

        // Default to multiselect
        if (me.multiSelect !== false) {
            me.multSelect = true;
        }

        // To override the normal tree picker with our regular saleschannel tree
        // This will give us our cascade checks and search functionality
        var salesChannelTree = Ext.create('customer.saleschanneltree', {
            shrinkWrapDock: 2,
            floating: true,
            minHeight: this.minPickerHeight,
            maxHeight: this.maxPickerHeight,
            width: 400,
            manageHeight: false,
            shadow: false,
            rootVisible: false,
            editable: false,
            isHideSearch: true,
            listeners: {
                scope: this,
                itemclick: this.onItemClick,
                searchComplete: function () {
                    me.focus();
                }
            },
            viewConfig: {
                listeners: {
                    scope: this,
                    itemclick: function (tree, record, item, index, e, opts) {
                        if (!me.multSelect) {
                            me.setValue(record);
                            tree.getSelectionModel().select(record);
                        }
                    },
                    render: this.onViewRender
                }
            }
        });

        if (me.multiSelect) {
            // Handle adding multiselect functionality

            var checkChangeFn = null;
            if (salesChannelTree.hasListeners.checkchange) {
                checkChangeFn = salesChannelTree.events.checkchange.listeners[0].fn;
            }

            // Override the normal checkchange function to handle cascade checks
            // and to pass the selection to the BoxSelect component. This will
            // list the top level selected sales channels (ignores implied leaf selections)
            salesChannelTree.on('checkchange', function (node, checked, opts) {
                if (Ext.isFunction(checkChangeFn)) {
                    checkChangeFn(node, checked, opts);
                }

                if (checked) {
                    me.addValue(node);
                } else {
                    me.removeValue(node.getId());
                }

            });

            // Make sure we show the checkboxes on this tree by default
            salesChannelTree.store.on('load', function (store) {
                var root = store.getRootNode();
                root.cascadeBy(function (n) {
                    n.set('checked', false);
                });
            });
        }

        Ext.apply(this, {
            store: salesChannelTree.store,
            matchFieldWidth: false,
            valueField: 'id',
            displayField: 'name',
            createPicker: function () {
                // Overrides the usual TreePicker create picker function
                // Just returns the salesChannelTree that we have already created
                // Leaving in the Ext IE9 view fix from the original function

                if (Ext.isIE9 && Ext.isStrict) {
                    // In IE9 strict mode, the tree view grows by the height of the horizontal scroll bar when the items are highlighted or unhighlighted.
                    // Also when items are collapsed or expanded the height of the view is off. Forcing a repaint fixes the problem.
                    view.on({
                        scope: me,
                        highlightitem: me.repaintPickerView,
                        unhighlightitem: me.repaintPickerView,
                        afteritemexpand: me.repaintPickerView,
                        afteritemcollapse: me.repaintPickerView
                    });
                }
                return salesChannelTree;
            },
            // Overriding doQuery so any queries will be passed directly
            // to the saleschannel tree performSearch function. This adds
            // additional search behavior (Search is applied to customers
            // within each saleschannel as well)
            doQuery: function (txt) {
                me.expand();
                salesChannelTree.performSearch(txt);
                me.focus();
            },
            listeners: {
                // Make sure the selections in the combo field
                // always match what appears in the picker. 
                change: function () {
                    var root = me.store.getRootNode();
                    if (me.multiSelect) {
                        root.cascadeBy(function (n) {
                            if (n.get('cls') == 'x-disabled') {
                                // ignore these nodes since they will be 
                                // dealt with via the parent.
                                return;
                            }

                            if (Ext.Array.indexOf(me.getValue(), n.getId()) >= 0) {
                                if (Ext.isFunction(checkChangeFn)) {
                                    checkChangeFn(n, true);
                                }
                            } else {
                                if (Ext.isFunction(checkChangeFn)) {
                                    checkChangeFn(n, false);
                                }
                            }
                        });
                    } else {
                        me.collapse();
                    }

                    me.fireEvent('salesChannelChange', me.getCheckedRecords());
                }
            }
        });

        if (me.multiSelect) {
            // Override the select item function so the 
            // picker tree does not close on item click. 
            // Checkchange is responsible for passing the set value
            // up to the combo box. 
            me.selectItem = function (record) {}
        }

        this.callParent(arguments);
    },
    getCheckedRecords: function () {
        var root = this.store.getRootNode(),
            records = [];

        root.cascadeBy(function (r) {
            if (r.get('checked')) {
                records.push(r);
            }
        });

        return records;
    }
});
