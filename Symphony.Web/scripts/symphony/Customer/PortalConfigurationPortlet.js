﻿Symphony.Customer.CustomerForm = Ext.define('customer.portalconfigurationportlet', {
    alias: 'widget.customer.portalconfigurationportlet',
    extend: 'portal.portlet',
    
    initComponent: function () {
        var me = this;

        me.checked = false;

        Ext.apply(this, {
            collapsible: false,
            closable: false,
            bodyPadding: 10,
            frame: true,
            border: false,
            width: this.width ? this.width : 246,
            height: this.height ? this.height : 150,
            tools: [{
                xtype: 'checkbox',
                fieldLabel: 'Enabled',
                name: 'isVisible'
            }],
            listeners: {
                afterrender: function () {
                    me.setChecked(me.checked);
                }
            }
        });

        this.callParent(arguments);
    },

    setChecked: function (checked) {
        this.checked = checked;
        if (this.header) {
            this.header.query('[name=isVisible]')[0].setValue(checked);
        }
    },
    getChecked: function () {
        if (this.header) {
            return this.header.query('[name=isVisible]')[0].getValue();
        }
        return this.checked;
    }
});
