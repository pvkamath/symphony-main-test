﻿Symphony.Customer.UserAdministrationEditor = Ext.define('customer.useradministrationeditor', {
    alias: 'widget.customer.useradministrationeditor',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;
        var loadMask = new Ext.LoadMask(this, { msg: "Loading user, please wait..." });
        Ext.apply(this, {
            layout: 'fit',
            bodyCls: 'x-panel-default-framed',
            bodyStyle: 'padding: 0px',
            frame: false,
            border: false,
            closable: true,
            items: [{
                flex: 1,
                xtype: 'customer.userform',
                userId: me.userId,
                customerId: me.customerId,
                salesChannelId: me.salesChannelId,
                isCustomerAdministration: true,
                layout: 'fit',
                border: false,
                cls: 'x-panel-transparent',
                listeners: {
                    beforeload: function () {
                        loadMask.show();
                    },
                    loaded: function (data) {
                        if (data) {
                            me.setTitle(data.fullName);
                        } else {
                            me.setTitle('New User');
                        }

                        loadMask.hide();
                    },
                    save: function (data) {
                        if (data) {
                            me.setTitle(data.fullName);
                            me.tabId = 'user' + data.id;
                        }
                    }
                }
            }]
        });

        this.callParent(arguments);
    }
});