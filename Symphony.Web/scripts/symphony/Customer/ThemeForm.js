﻿Ext.define('Customer.ThemeForm', {
    xtype: 'customer.themeform',
    extend: 'Ext.form.Panel',
    requires: [
        'Theme',
        'Symphony.SaveCancelBar',
        'Ext.ux.ColorPicker'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],

    model: null,
    frame: true,
    header: false,
    autoScroll: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    assumeOwnership: ['save-bar', 'delete-button'],

    dockedItems: [{
        xtype: 'symphony.savecancelbar',
        itemId: 'save-bar',
        dock: 'top',
        showManually: true,
        listeners: {
            save: function() {
                var me = this,
                    root = me.up('[xtype=customer.themeform]');

                Log.debug('Clicked button to save the theme.');

                root.save();
            },
            cancel: function() {
                var me = this,
                    root = me.up('[xtype=customer.themeform]');

                Log.debug('Clicked button to cancel changes to the theme.');

                root.close();
            }
        },


        items: ['->', {
            xtype: 'button',
            itemId: 'delete-button',
            iconCls: 'x-button-cross',
            text: 'Delete',

            handler: function() {
                var me = this.up('[xtype=customer.themeform]');

                Log.debug('Clicked button to delete the theme form');

                me.deleteTheme();
            }
        }]
    }],

    defaults: {
        labelWidth: 140
    },
    items: [{
        xtype: 'fieldset',
        title: 'General',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        defaults: {
            labelWidth: 140
        },
        items: [{
            xtype: 'textfield',
            itemId: 'name-field',
            name: 'name',
            fieldLabel: 'Name',
            allowOnlyWhitespace: false
        }, {
            xtype: 'textfield',
            itemId: 'code-name-field',
            name: 'codeName',
            fieldLabel: 'Code Name',
            allowOnlyWhitespace: false
        }, {
            xtype: 'textarea',
            itemId: 'description-field',
            name: 'description',
            fieldLabel: 'Description'
        }, {
            xtype: 'colorpickerfield',
            itemId: 'primary-color-field',
            name: 'primaryColor',
            fieldLabel: 'Primary Color',
            allowOnlyWhitespace: false
        }, {
            xtype: 'colorpickerfield',
            itemId: 'secondary-color-field',
            name: 'secondaryColor',
            fieldLabel: 'Secondary Color',
            allowOnlyWhitespace: false
        }, {
            xtype: 'colorpickerfield',
            itemId: 'tertiary-color-field',
            name: 'tertiaryColor',
            fieldLabel: 'Tertiary Color',
            allowOnlyWhitespace: false
        }, {
            xtype: 'colorpickerfield',
            itemId: 'quaternary-color-field',
            name: 'quaternary',
            fieldLabel: 'Quaternary Color',
            allowOnlyWhitespace: false
        }, {
            xtype: 'associatedimagefield',
            itemId: 'primary-image-field',
            name: 'primaryImage',
            fieldLabel: 'Primary Image',
            editable: true
        }, {
            xtype: 'associatedimagefield',
            itemId: 'secondary-image-field',
            name: 'secondaryImage',
            fieldLabel: 'Secondary Image',
            editable: true
        }]
    }],

    initComponent: function() {
        var me = this;

        me.callParent(arguments);

        if (!me.model) {
            Log.error('Model property is undefined.');
            return;
        }

        me.setTitle(me.model.get('name'));
        if (!me.model.phantom) {
            me.getForm().setValues(me.model.getData());
        } else {
            me.queryById('delete-button').hide();
        }
    },

    save: function() {
        var me = this,
            saveBar = me.queryById('save-bar'),
            form = me.getForm(),
            model,
            data;

        saveBar.showSaving();
        Log.info('Saving theme.');

        if (!form.isValid()) {
            saveBar.hideSaving();
            saveBar.showNotSaving();

            Log.error('Cannot submit because form is in an invalid state.');
            return;
        }

        data = me.form.getValues();
        //console.log(data);
        me.model.set(data);
        
        Log.debug('Updating theme data to save.', me.model);
        Log.watchModel(me, me.model);

        me.model.save({
            success: Ext.Function.createOwned(me, function() {
                Log.info('Theme was successfully saved.');

                me.fireEvent('change', me.model);

                me.setTitle(me.model.get('name'));
                me.queryById('delete-button').show();
            }),
            failure: Ext.Function.createOwned(me, function(model, operation) {
                Log.error('Failed to save theme.');
                saveBar.showError(Ext.String.format('Save failed: {0}  {1}', operation.error.status, operation.error.statusText));
            })
        });
    },

    deleteTheme: function () {
        var me = this,
            saveBar = me.queryById('save-bar'),
            model,
            data;

        Log.debug('Asking user for confirmation to delete theme.');

        Ext.Msg.confirm('Confirm deletion', 'Are you sure you want to delete this theme? This action cannot be undone.', Ext.Function.createOwned(me, function(result) {
            if (result != 'yes') {
                Log.debug('User canceled delete operation.');
                return;
            }

            Log.info('Deleting theme.');

            me.model.destroy({
                success: Ext.Function.createOwned(me, function() {
                    Log.info('Theme was successfully deleted.');

                    me.fireEvent('change', me.model);
                    me.close();
                }),
                failure: Ext.Function.createOwned(me, function(model, operation) {
                    Log.error('Failed to delete theme.');

                    if (operation.error) {
                        saveBar.showError(Ext.String.format('Delete failed: {0}  {1}', operation.error.status, operation.error.statusText));
                    } else {
                        saveBar.showError('Deleted failed: <<success status code>>');
                    }
                })
            });
        }));
    }
});
