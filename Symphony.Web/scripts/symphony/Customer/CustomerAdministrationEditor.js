﻿Symphony.Customer.CustomerAdministrationEditor = Ext.define('customer.customeradministrationeditor', {
    alias: 'widget.customer.customeradministrationeditor',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;
        var loadMask = new Ext.LoadMask(this, { msg: "Loading customer details, please wait..." });
        Ext.apply(this, {
            bodyCls: 'x-panel-default-framed',
            bodyStyle: 'padding: 0px',
            title: me.customerName,
            frame: false,
            border: false,
            closable: true,
            layout: 'fit',
            items: [{
                xtype: 'customer.customereditor',
                customerId: me.customerId,
                customerName: me.customerName,
                salesChannelId: me.salesChannelId,
                isCustomerAdministration: true,
                layout: 'fit',
                border: false,
                cls: 'x-panel-transparent',
                listeners: {
                    save: function (data) {
                        if (data) {
                            me.setTitle(data.name);
                            me.tabId = 'customer' + data.id;
                        }
                    }
                }
            }]
        });

        this.callParent(arguments);
    }
});