﻿(function () {
	Symphony.Customer.ExternalSystemsGrid = Ext.define('customer.externalsystemsgrid', {
	    alias: 'widget.customer.externalsystemsgrid',
	    extend: 'symphony.searchablegrid',
        selectedValues: null,
	    initComponent: function () {
			var me = this;
			var url = '/services/customer.svc/externalsystems/';

			var colModel = new Ext.grid.ColumnModel({
				columns: [
                    {
                        header: 'Id',
                        dataIndex: 'id',
                        hidden: true
                    },
				    {
					    /*id: 'systemName',*/
					    header: 'Name',
					    dataIndex: 'systemName',
					    align: 'left'
				    },{
					    /*id: 'systemCodeName',*/
					    header: 'Code Name',
					    dataIndex: 'systemCodeName',
					    align: 'left'
				    },{
				        /*id: 'loginUrl',*/
				        header: 'Login Url',
				        dataIndex: 'loginUrl',
				        flex: 1
				    }
				]
			});

			Ext.apply(this, {
				idProperty: 'id',
				url: url,
				colModel: colModel,
				model: 'externalSystem',
				selType: 'checkboxmodel',
				plugins: [{ ptype: 'pagingselectpersist' }]
			});

			this.callParent(arguments);

	    },
	    getSelections: function () {
	        var selectedIds = this.getPlugin('pagingSelectionPersistence').getPersistedSelection(), results = [];
	        for (var i = 0; i < selectedIds.length; i++) {
	            if (selectedIds[i] && selectedIds[i].get) {
	                results.push({ id: selectedIds[i].get('id'), systemCodeName: selectedIds[i].get('systemCodeName') });
	            }
	        }
	        return results;
	    },
	    setSelections: function (values) {
	        var selection = [];
	        for (var i = 0; i < values.length; i++) {
	            var externalSystem = Ext.create('externalSystem', values[i]);
	            selection.push(externalSystem);
	        }
	        this.getPlugin('pagingSelectionPersistence').setSelections(selection);
	    },
	    clearSelections: function () {
	        this.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
	    },
	    getSelected: function () {
	        return this.getSelections();
	    }
	});

})();