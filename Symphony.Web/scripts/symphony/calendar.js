﻿Symphony.Calendar = {
    str: function(val) { return val + ''; },
    num: function(val) { return val * 1;  },
    dayNames: ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'],
    leapYear: function(year) {
        return (year % 4 == 0 && year % 100 != 0 || year % 400 == 0);
    },
    daysInMonth: function(year, month) {
        if (month == 1 || month == 3 || month == 5 || month == 7
            || month == 8 || month == 10 || month == 12) {
            return 31;
        }
        if (month == 4 || month == 6 || month == 9 || month == 11) {
            return 30;
        }
        if (month == 2) {
            return (this.leapYear(year) ? 29 : 28);
        }
        throw 'Invalid month.';
    },
    // 0 = Sunday
    // 1 = Monday
    // 2 = Tuesday
    // 3 = Wednesday
    // 4 = Thursday
    // 5 = Friday
    // 6 = Saturday
    dayOfWeek: function(year, month, day) {
        var q = this.num(day);
        var m = this.num(month);
        var Y = this.num(year);
        if (m == 1 || m == 2) {
            m += 12;
            Y--;
        }
        return (q + Math.floor(((m+1)*26)/10) + Y + Math.floor(Y/4) + (6*Math.floor(Y/100)) + Math.floor(Y/400) - 1) % 7;
    },
    weeks: function(year, month) {
        var daysInMonth = this.daysInMonth(year, month);
        
        var monthStartDOW = this.dayOfWeek(year, month, 1);
        var monthEndDOW   = this.dayOfWeek(year, month, daysInMonth);
        
        var weeks = [];
        var week = {};
        var dow = monthStartDOW;
        for (var i = 1; i <= daysInMonth; i++) {
            week[this.dayNames[dow]] = {
                year: year,
                month: month,
                day: i
            };
            dow++;
            if (dow == 7) {
                weeks.push(week);
                week = {};
                dow = 0;
            }
        }
        if (monthEndDOW != 6) {
            weeks.push(week);
        }
        return weeks;
    }
};