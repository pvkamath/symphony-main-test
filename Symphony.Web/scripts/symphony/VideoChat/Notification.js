﻿(function() {
    //AMTODO - Set Flag for when video chat is in progress - this should auto decline incoming invites for chat as "I'm Busy"
    // need to clear the flag once the user has quit the chat.
    Symphony.VideoChat.Notification = {
        url: '/websync.ashx',
        baseChannel: '/notification/',
        videoChannel: '/videochat/',
        presenceChannel: '/presence/',
        client: null,
        init: function(onSuccess) {
            if (Symphony.VideoChat.Notification.client) {
                if (typeof onSuccess == 'function') {
                    onSuccess();
                }
                return;
            }

            var client = new fm.websync.client(Symphony.VideoChat.Notification.url);

            Symphony.VideoChat.Notification.client = client;

            client.setAutoDisconnect({
                synchronous: true
            });

            client.connect({
                onSuccess: function(e) {
                    client.join({
                        channel: Symphony.VideoChat.Notification.getPresenceChannel(),
                        userId: Symphony.VideoChat.UserIdPrefix + Symphony.User.id,
                        userNickname: Symphony.VideoChat.FirstName(),
                        onSuccess: function(e) {
                            var users = e.getUsers();
                            for (var i = 0; i < users.length; i++) {
                                Symphony.VideoChat.SetUserOnlineStatus(users[i].getUserId(), users[i].getUserNickname(), Symphony.VideoChat.Status.online);
                            }
                            if (typeof onSuccess == 'function') {
                                onSuccess();
                            }
                        },
                        onFailure: function(e) {
                            console.log("Could not join.");
                        },
                        onReceive: function(e) {
                            var data = e.getData();
                            Symphony.VideoChat.SetUserOnlineStatus(Symphony.VideoChat.UserIdPrefix + data.userId, data.name, data.status);
                        },
                        onUserJoin: function(e) {
                            var joinedUser = e.getJoinedUser();
                            Symphony.VideoChat.SetUserOnlineStatus(joinedUser.getUserId(), joinedUser.getUserNickname(), Symphony.VideoChat.Status.online);
                        },
                        onUserLeave: function(e) {
                            var leftUser = e.getLeftUser();
                            Symphony.VideoChat.SetUserOnlineStatus(leftUser.getUserId(), leftUser.getUserNickname(), Symphony.VideoChat.Status.offline);
                        }
                    });


                    client.subscribe({
                        channel: Symphony.VideoChat.Notification.baseChannel + Symphony.User.id,
                        onSuccess: function(e) {
                            //console.log("Subscribed to channel " + Symphony.VideoChat.Notification.baseChannel + Symphony.User.id);
                        },
                        onFailure: function(e) {
                            //console.log("Could not subscribe to channel" + Symphony.VideoChat.Notification.baseChannel + Symphony.User.id);
                        },
                        onReceive: function(e) {
                            var data = e.getData(),
                                userId = data.from,
                                name = data.name;

                            switch (data.type) {
                                case Symphony.VideoChat.NotificationTypes.Invite:

                                    if (Symphony.App) {
                                        var notification = new Symphony.VideoChat.NotificationWindow({
                                            fromId: userId,
                                            fromName: name,
                                            channel: data.channel
                                        }).show();
                                    }

                                    break;
                                case Symphony.VideoChat.NotificationTypes.Answer:

                                    break;
                                case Symphony.VideoChat.NotificationTypes.Decline:
                                    Symphony.VideoChat.Notification.handleDecline(data.msg);
                                    break;
                            }
                        }
                    });
                },
                onFailure: function(e) {
                    console.log("Could not connect to websync " + e.getException().message);
                },
                onStreamFailure: function(e) {
                    console.log("Stream failure");
                }
            });

            client.bind({
                record: {
                    key: 'name',
                    value: Symphony.VideoChat.FirstName()
                }
            });
        },
        getPresenceChannel: function() {
            return Symphony.VideoChat.Notification.presenceChannel + Symphony.User.customerId;
        },
        getUserChannel: function(userId) {
            return Symphony.VideoChat.Notification.baseChannel + userId;
        },
        getVideoChannel: function(userId) {
            return Symphony.VideoChat.Notification.videoChannel + userId;
        },
        sendStatus: function(status) {
            if (!Symphony.VideoChat.Notification.client) {
                return;
            }

            Symphony.VideoChat.Notification.client.publish({
                channel: Symphony.VideoChat.Notification.getPresenceChannel(),
                data: {
                    userId: Symphony.User.id,
                    name: Symphony.VideoChat.FirstName(),
                    type: Symphony.VideoChat.NotificationTypes.Status,
                    status: status
                }
            });
        },
        send: function(userId, data, success, failure) {
            if (!Symphony.VideoChat.Notification.client) {
                return;
            }

            Symphony.VideoChat.Notification.client.publish({
                channel: Symphony.VideoChat.Notification.getUserChannel(userId),
                data: data,
                onSuccess: function(e) {
                    if (typeof (success) === 'function') {
                        success();
                    }
                    console.log("Sent data to channel " + Symphony.VideoChat.Notification.getUserChannel(userId));
                },
                onFailure: function(e) {
                    if (typeof (failure) === 'function') {
                        failure();
                    }
                    console.log("Could not send invite " + e.getException().message);
                }
            });
        },
        invite: function(userId, suppressChatWindow) {
            if (!Symphony.VideoChat.Notification.client) {
                return;
            }

            if (userId == Symphony.User.id) {
                return;
            }

            var userInfo = Symphony.VideoChat.UsersOnline[Symphony.VideoChat.UserIdPrefix + userId];
            var status = userInfo ? userInfo.status : Symphony.VideoChat.Status.offline;

            switch (status) {
                case Symphony.VideoChat.Status.online:
                case Symphony.VideoChat.Status.away:
                    var channel = Symphony.VideoChat.Notification.getVideoChannel(userId);
                    Symphony.VideoChat.Notification.startChat(channel, function () {
                        Symphony.VideoChat.Notification.send(userId, {
                            to: userId,
                            from: Symphony.User.id,
                            name: Symphony.User.fullName,
                            type: Symphony.VideoChat.NotificationTypes.Invite,
                            channel: channel
                        });
                    }, suppressChatWindow);

                    return channel;
                    break;
                case Symphony.VideoChat.Status.busy:
                    Ext.MessageBox.alert('User Busy', 'This user is busy and is not accepting any video chat invites.');
                    return false;
                    break;
                case Symphony.VideoChat.Status.offline:
                    Ext.MessageBox.alert('User Offline', 'This user is offline and cannot accept video chat invites.');
                    return false;
                    break;
            }
        },
        accept: function(userId, channel) {
            if (!Symphony.VideoChat.Notification.client) {
                return;
            }

            Symphony.VideoChat.Notification.startChat(channel);
        },
        decline: function(userId, reason) {
            if (!Symphony.VideoChat.Notification.client) {
                return;
            }

            Symphony.VideoChat.Notification.send(userId, {
                to: userId,
                from: Symphony.User.id,
                name: Symphony.User.fullName,
                type: Symphony.VideoChat.NotificationTypes.Decline,
                msg: reason
            });
        },
        handleDecline: function(msg) {
            if (!Symphony.VideoChat.Notification.client) {
                return;
            }

            var chatwindow = Ext.getCmp('chatWindow');
            if (chatwindow) {
                var statusLabel = chatwindow.find('name', 'statusLabel')[0],
                    reasonLabel = chatwindow.find('name', 'reasonLabel')[0];

                if (!msg) {
                    msg = '';
                } else {
                    msg = '"' + msg + '"';
                }

                statusLabel.setText("User Unavailable");
                statusLabel.addClass("declined");
                reasonLabel.setText(msg);
                reasonLabel.show();
            }
        },
        joinConference: function(conference, channel) {
            if (!Symphony.VideoChat.Notification.client) {
                return;
            }

            var client = Symphony.VideoChat.Notification.client;
            client.joinConference(
            {
                conferenceChannel: channel,
                conference: conference,
                onFailure: function(e) {
                    alert('Could not join conference ' + e.getConferenceChannel() + '. ' + e.getException().message);
                }
            });
        },
        startChat: function(channel, callback, suppressChatWindow) {
            if (!Symphony.VideoChat.Notification.client) {
                return;
            }
            var chatWindow;

            if (!suppressChatWindow) {
                chatWindow = window.open("/VideoChat.aspx?channel=" + channel, "_blank", "height=660,width=445,menubar=no,toolbar=no,location=no").onload = function () {
                    chatWindow = this;
                    if (typeof (callback) === "function") {
                        callback();
                    }
                };
            } else {
                callback();
            }

            if (chatWindow) {
                var status = Symphony.VideoChat.UsersOnline[Symphony.VideoChat.UserIdPrefix + Symphony.User.id].status;

                var activeChatInterval = setInterval(function () {
                    if (chatWindow.closed) {
                        clearInterval(activeChatInterval);
                        Symphony.VideoChat.Status.setStatus(status);
                    }
                }, 1000);
            }

            Symphony.VideoChat.Status.setStatus(Symphony.VideoChat.Status.busy);
        },
        disconnect: function() {
            if (!Symphony.VideoChat.Notification.client) {
                return;
            }

            Symphony.VideoChat.Notification.client.leave({
                channel: Symphony.VideoChat.Notification.getPresenceChannel()
            });

            Symphony.VideoChat.Notification.client.disconnect();
        }
    };

})();
