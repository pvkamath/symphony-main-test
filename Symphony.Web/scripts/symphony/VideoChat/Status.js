﻿(function() {
    Symphony.VideoChat.Status = {
        online: 1,
        busy: 2,
        away: 3,
        offline: 4,
        statusClasses: {},
        inactivityDuration: 300000,
        inactivityTimeout: null,
        resetInProgress: false,
        init: function() {
            Symphony.VideoChat.Status.statusClasses[Symphony.VideoChat.Status.online] = 'online';
            Symphony.VideoChat.Status.statusClasses[Symphony.VideoChat.Status.busy] = 'busy';
            Symphony.VideoChat.Status.statusClasses[Symphony.VideoChat.Status.away] = 'away';
            Symphony.VideoChat.Status.statusClasses[Symphony.VideoChat.Status.offline] = 'offline';
        },
        resetStatus: function(status) {
            if (!Symphony.VideoChat.Status.resetInProgress) {
                Symphony.VideoChat.Status.resetInProgress = true;
                if (status > 0) {
                    Symphony.VideoChat.Status.setStatus(status);
                    Symphony.VideoChat.Status.resetInProgress = false;
                } else {
                    Symphony.Ajax.request({
                        url: '/services/customer.svc/user/' + Symphony.User.id + '/onlinestatus',
                        method: 'get',
                        success: function (args) {
                            Symphony.VideoChat.Status.setStatus(args.data);
                            Symphony.VideoChat.Status.resetInProgress = false;
                        },
                        failure: function (args) {
                            Symphony.VideoChat.Status.setStatus(Symphony.VideoChat.Status.offline);
                            Symphony.VideoChat.Status.resetInProgress = false;
                        }
                    });
                }
            }
        },
        resetInactivityTimeout: function() {
            if (Symphony.VideoChat.Status.inactivityTimeout) {
                clearTimeout(Symphony.VideoChat.Status.inactivityTimeout);
            }

            if (Symphony.VideoChat.GetOnlineStatus(Symphony.User.id) == Symphony.VideoChat.Status.away) {
                Symphony.VideoChat.Status.resetStatus()
            }

            Symphony.VideoChat.Status.inactivityTimeout = setTimeout(function() {
                var status = Symphony.VideoChat.GetOnlineStatus(Symphony.User.id);
                if (status == Symphony.VideoChat.Status.online) {
                    Symphony.VideoChat.Status.setStatus(Symphony.VideoChat.Status.away);
                }
            }, Symphony.VideoChat.Status.inactivityDuration);
        },
        getStatusMenu: function() {
            return Symphony.Portal.hasPermission(Symphony.Modules.VideoChat) ?
            {
                text: 'Online Status',
                xtype: 'button',
                name: 'onlineStatus',
                id: 'onlineStatusBtn',
                scale: 'medium',
                iconCls: 'x-button-offline',
                menu: {
                    items: [{
                        text: 'Online',
                        checked: false,
                        group: 'onlineStatus',
                        status: Symphony.VideoChat.Status.online,
                        checkHandler: Symphony.VideoChat.Status.statusHandler
                    }, {
                        text: 'Busy',
                        checked: false,
                        group: 'onlineStatus',
                        status: Symphony.VideoChat.Status.busy,
                        checkHandler: Symphony.VideoChat.Status.statusHandler
                    }, {
                        text: 'Away',
                        checked: false,
                        group: 'onlineStatus',
                        status: Symphony.VideoChat.Status.away,
                        checkHandler: Symphony.VideoChat.Status.statusHandler
                    }, {
                        text: 'Offline',
                        checked: false,
                        group: 'onlineStatus',
                        status: Symphony.VideoChat.Status.offline,
                        checkHandler: Symphony.VideoChat.Status.statusHandler
}]
                    }
} : '';
                },
                statusHandler: function(item, checked) {
                    if (checked) {
                        Symphony.VideoChat.Status.setStatus(item.status, true);
                    }
                },
                setStatus: function (statusId, isUpdate) {
                    var status = Symphony.VideoChat.Status.statusClasses.hasOwnProperty(statusId) ? statusId : Symphony.VideoChat.Status.offline;
                    
                    // If we have symphony ui to update
                    if (Symphony && Symphony.App && Symphony.App.find) {
                        var buttons = Symphony.App.find('id', 'onlineStatusBtn');
                        if (buttons.length) {
                            var statusBtn = buttons[0];
                            statusBtn.setIconCls('x-button-' + Symphony.VideoChat.Status.statusClasses[status]);
                            statusBtn.setText('Status: ' + Symphony.VideoChat.Status.statusClasses[status].charAt(0).toUpperCase() + Symphony.VideoChat.Status.statusClasses[status].slice(1));
                        }
                    
                        if (isUpdate) {
                            Symphony.Ajax.request({
                                url: '/services/customer.svc/user/' + Symphony.User.id + '/onlinestatus/',
                                method: 'post',
                                jsonData: status
                            });
                        }

                        if (status != Symphony.VideoChat.Status.offline) {
                            Symphony.VideoChat.Notification.init(function () {
                                Symphony.VideoChat.Notification.sendStatus(status);
                            });
                        } else {
                            Symphony.VideoChat.Notification.sendStatus(status);
                        }
                    } else {
                        console.warn('Symphony.App is not defined.');
                    }
                }
            }
            Symphony.VideoChat.Status.init();
        })();