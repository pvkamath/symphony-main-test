﻿(function () {
    Symphony.VideoChat = {
        init: function (status) {
            Symphony.VideoChat.Status.resetStatus(status);
        },
        NotificationTypes: {
            Invite: 1,
            Accept: 2,
            Decline: 3,
            Status: 4
        },
        NotificationTimeout: 30000,
        UsersOnline: {},
        UserIdPrefix: 'uid',
        GetOnlineStatus: function(userId) {
            return  Symphony.VideoChat.UsersOnline.hasOwnProperty(Symphony.VideoChat.UserIdPrefix + userId) ?
            Symphony.VideoChat.UsersOnline[Symphony.VideoChat.UserIdPrefix + userId].status :
            Symphony.VideoChat.Status.offline;
        },
        GetOnlineStatusClass: function (userId) {
           return  Symphony.VideoChat.UsersOnline.hasOwnProperty(Symphony.VideoChat.UserIdPrefix + userId) ?
            Symphony.VideoChat.Status.statusClasses[Symphony.VideoChat.UsersOnline[Symphony.VideoChat.UserIdPrefix + userId].status] :
            Symphony.VideoChat.Status.statusClasses[Symphony.VideoChat.Status.offline];
        },
        SetUserOnlineStatus: function (prefixedUserId, nickname, status) {
            var webcamLinks = Ext.query(".webcam." + prefixedUserId);

            status = Symphony.VideoChat.Status.statusClasses.hasOwnProperty(status) ? status : Symphony.VideoChat.Status.offline;

            if (status == Symphony.VideoChat.Status.offline) {
                delete Symphony.VideoChat.UsersOnline[prefixedUserId];
            } else {
                Symphony.VideoChat.UsersOnline[prefixedUserId] = { name: nickname, status: status };
            }

            Ext.each(webcamLinks, function (item, index, allItems) {
                var el = new Ext.Element(item);
                el.replaceClass(Symphony.VideoChat.Status.statusClasses[Symphony.VideoChat.Status.offline], '');
                el.replaceClass(Symphony.VideoChat.Status.statusClasses[Symphony.VideoChat.Status.online], '');
                el.replaceClass(Symphony.VideoChat.Status.statusClasses[Symphony.VideoChat.Status.away], '');
                el.replaceClass(Symphony.VideoChat.Status.statusClasses[Symphony.VideoChat.Status.busy], '');
                el.addClass(Symphony.VideoChat.Status.statusClasses[status]);
            });
        },
        FirstName: function () {
            var names = Symphony.User.fullName.split(',');
            return names[1].trim();
        }
    }
})();