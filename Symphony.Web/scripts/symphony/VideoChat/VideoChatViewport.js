﻿(function () {
    Symphony.VideoChat.VideoChatViewport = Ext.define('videochat.viewport', {
        alias: 'widget.videochat.viewport',
        extend: 'Ext.Viewport',
        layoutManager: null,
        conference: null,
        dataChannelInfo: null,
        initComponent: function () {
            var me = this;
            
            Ext.apply(this, {
                renderTo: 'VideoChat',
                cls: 'VideoChat',
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    pack: 'start'
                },
                
                items: [{
                    flex: 0.65,
                    xtype: 'panel',
                    padding: '10px 10px 0px 10px',
                    border: false,
                    layout: 'card',
                    activeItem: 0,
                    id: 'chatWindow',
                    items: [{
                        xtype: 'panel',
                        name: 'status',
                        cls: 'statusPanel',
                        layout: 'vbox',
                        layoutConfig: {
                            pack: 'center',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'panel',
                            border: false,
                            cls: 'statusLabels',

                            items: [{
                                xtype: 'label',
                                text: 'Waiting for user',
                                name: 'statusLabel',
                                cls: 'status'

                            }, {
                                xtype: 'label',
                                text: 'Reason',
                                name: 'reasonLabel',
                                cls: 'reason',
                                hidden: true
                            }]
                        }]
                    }, {
                        xtype: 'panel',
                        name: 'video',
                        cls: 'video-chat-panel',
                        listeners: {
                            render: function() {
                                Symphony.VideoChat.IceLink.init(me, me.getVideoElement(), conferenceChannel, me.chatHandler);
                            },
                            resize: function (panel, adjWidth, adjHeight, rawWidth, rawHeight) {
                                if (me.layoutManager) {
                                    me.layoutManager.doLayout();
                                }
                            }
                        }
                    }]
                }, {
                    flex: 0.35,
                    xtype: 'panel',
                    border: false,
                    padding: '10px 10px 10px 10px',
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    items: [{
                        flex: 0.75,
                        xtype: 'panel',
                        autoScroll: true,
                        padding: '5px 5px 5px 5px',
                        name: 'chatWindow'
                    }, {
                        xtype: 'panel',
                        layout: 'hbox',
                        padding: '10px 0px 0px 0px',
                        border: false,
                        items: [{
                            flex: 1,
                            xtype: 'textfield',
                            enableKeyEvents: true,
                            emptyText: 'Send a message',
                            listeners: {
                                keydown: function (field, e) {
                                    if (e.getKey() === Ext.EventObject.ENTER) {
                                        if (me.conference) {
                                            var message = field.getValue();

                                            me.conference.sendData({
                                                channelInfo: me.dataChannelInfo,
                                                data: message
                                            });

                                            me.chatHandler(Symphony.VideoChat.FirstName(), message);

                                            field.setValue('');
                                        }

                                        e.preventDefault();
                                    }
                                }
                            }
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        getVideoElement: function () {
            return this.find('name', 'video')[0].body.dom;
        },
        setLayoutManager: function (layoutManager) {
            this.layoutManager = layoutManager;
        },
        chatHandler: function (name, message) {
            var msg = document.createElement('div');
            var from = document.createElement('div');
            var text = document.createElement('div');
            var cls = 'msg';

            if (!message) {
                message = '';
                cls = cls + ' empty';
            }

            msg.className = cls;

            from.innerHTML = name + ' - <span class="date">' + (Symphony.dateTimeRenderer(new Date().formatSymphony('microsoft'))) + '</span>';
            text.innerHTML = message;

            from.className = 'from';
            text.className = 'text';

            msg.appendChild(from);

            if (message) {
                msg.appendChild(text);
            }
            var chatWindow = this.find('name', 'chatWindow')[0];

            chatWindow.body.dom.appendChild(msg);
            chatWindow.body.scroll("b", chatWindow.body.dom.scrollHeight, true);
        }
    });

})();