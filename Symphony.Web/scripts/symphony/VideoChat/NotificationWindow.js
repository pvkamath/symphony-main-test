﻿(function () {
    Symphony.VideoChat.NotificationWindow = Ext.define('videochat.notificationwindow', {
        alias: 'widget.videochat.notificationwindow',
        extend: 'Ext.Window',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                width: 380,
                height: 250,
                frame: true,
                draggable: false,
                resizable: false,
                closable: false,
                cls: 'notification',
                title: '',
                listeners: {
                    render: function () {
                        setTimeout(function () {
                            me.close();
                        }, Symphony.VideoChat.NotificationTimeout);
                    }
                },
                margins: {
                    top: 5,
                    right: 5,
                    left: 5,
                    bottom: 5
                },
                layout: {
                    type: 'vbox',
                    align: 'center',
                    pack: 'start'
                },
                items: [{
                    xtype: 'label',
                    text: 'Incoming Video Call',
                    cls: 'incoming',
                    margins: {
                        top: 15,
                        right: 0,
                        left: 0,
                        bottom: 0
                    },
                    flex: .35
                }, {
                    xtype: 'label',
                    text: 'From: ' + me.fromName,
                    cls: 'from',
                    flex: .25
                }, {
                    xtype: 'panel',
                    flex: .4,
                    width: 350,
                    unstyled: true,
                    ref: 'answerPanel',
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    items: [{
                        xtype: 'button',
                        text: 'Decline',
                        flex: 1,
                        margins: {
                            top: 5,
                            left: 0,
                            right: 5,
                            bottom: 10
                        },
                        handler: function() {
                            me.answerPanel.hide();
                            me.declinePanel.show();

                            me.doLayout();
                        }
                    }, {
                        xtype: 'button',
                        text: 'Answer',
                        flex: 1,
                        margins: {
                            top: 5,
                            left: 5,
                            right: 0,
                            bottom: 10
                        },
                        handler: function() {
                            Symphony.VideoChat.Notification.accept(me.fromId, me.channel);
                            me.close();
                        }
                    }]
                }, {
                    xtype: 'panel',
                    flex: .5,
                    width: 360,
                    unstyled: true,
                    hidden: true,
                    ref: 'declinePanel',
                    layout: {
                        type: 'vbox',
                        align: 'center',
                        pack: 'start',
                        defaultMargins: {
                            top: 5,
                            left: 5,
                            right: 5,
                            bottom: 5
                        }
                    },
                    items: [{
                        xtype: 'label',
                        text: 'Decline call:',
                        flex: 0.025
                    }, {
                        xtype: 'panel',
                        flex: 0.45,
                        width: 350,
                        unstyled: true,
                        layout: {
                            type: 'hbox',
                            align: 'stretch',
                            pack: 'start',
                            defaultMargins: {
                                top: 5,
                                left: 5,
                                right: 5,
                                bottom: 5
                            }
                        },
                        items: [{
                            xtype: 'button',
                            text: 'I\'m Busy',
                            flex: 0.95,
                            handler: function() {
                                me.decline(this.text);
                            }
                        }, {
                            xtype: 'button',
                            text: 'I\'ll Call You Back',
                            flex: 0.95,
                            handler: function () {
                                me.decline(this.text);
                            }
                        }, {
                            xtype: 'button',
                            text: 'Dont Answer',
                            flex: 0.95,
                            handler: function () {
                                me.close();
                            }
                        }]
                    }, {
                        xtype: 'label',
                        text: 'Or',
                        flex: 0.03
                    }, {
                        xtype: 'panel',
                        flex: 0.40,
                        width: 350,
                        border: false,
                        frame: false,
                        bodyStyle: 'background: transparent',
                        margins: {
                            top: 5,
                            left: 5,
                            right: 5,
                            bottom: 10
                        },
                        layout: {
                            type: 'hbox',
                            align: 'center',
                            pack: 'start'
                        },
                        items: [{
                            xtype: 'textfield',
                            flex: 0.8,
                            name: 'declineResponse',
                            emptyText: 'Enter a custom reason'
                        }, {
                            xtype: 'button',
                            text: 'Send',
                            flex: 0.2,
                            handler: function () {
                                var response = me.find('name', 'declineResponse')[0];
                                me.decline(response.getValue());
                            }
                        }]
                    }]
                }]
            });

            this.callParent(arguments);
        },
        decline: function (reason) {
            Symphony.VideoChat.Notification.decline(this.fromId, reason);
            this.close();
        }
    });

})();