﻿(function () {
    if (!Symphony.VideoChat) {
        Symphony.VideoChat = {};
    }

    Symphony.VideoChat.IceLink = {

        serverAddressIceLink: window.location.host,
        serverPortIceLink: 3478,
        useWebSyncExtension: true,
        layoutManager: null,
        conference: null,
        dataChannelInfo: null,
        
        getPeerName: function (e) {
            var peerBindings = e.getPeerClient().getBoundRecords();
            return peerBindings["name"].getValue();
        },

        init: function (videoViewport, videoElement, conferenceChannel, chatHandler) {

            if (!Symphony.Modules.hasModule(Symphony.Modules.VideoChat)) {
                return;
            }

            var me = this;

            fm.icelink.webrtc.setApplet({
                path: 'scripts/icelink/fm.icelink.webrtc.applet.jar',
                name: 'IceLink WebRTC for JavaScript'
            });

            fm.util.addOnLoad(function () {
                setTimeout(function () {
                    Symphony.VideoChat.Notification.handleDecline();
                }, Symphony.VideoChat.NotificationTimeout);

                fm.icelink.webrtc.userMedia.getMedia({
                    defaultVideoPreviewScale: fm.icelink.webrtc.layoutScale.Cover,
                    defaultVideoScale: fm.icelink.webrtc.layoutScale.Cover,
                    audio: true,        
                    video: true,
                    onFailure: function (e) {
                        alert('Could not get media. ' + e.getException().message);
                    },
                    onSuccess: function (e) {

                        videoElement.style.display = 'block';
                        if (videoElement.parentNode) {
                            videoElement.parentNode.style.display = 'block';
                        }

                        var localMedia = e.getLocalStream();
                        var localVideoControl = e.getLocalVideoControl();
                        var layoutManager = new fm.icelink.webrtc.layoutManager(videoElement);

                        layoutManager.setLocalVideoControl(localVideoControl);

                        var audioStream = new fm.icelink.webrtc.audioStream(localMedia);

                        var videoStream = new fm.icelink.webrtc.videoStream(localMedia);

                        videoStream.addOnLinkInit(function (e) {
                            // Hide the loading indicator.
                            var chatWindow = Ext.getCmp('chatWindow');
                            chatWindow.layout.setActiveItem(1);

                            var remoteVideoControl = e.getLink().getRemoteVideoControl();
                            layoutManager.addRemoteVideoControl(e.getPeerId(), remoteVideoControl);
                        });
                        videoStream.addOnLinkDown(function (e) {
                            layoutManager.removeRemoteVideoControl(e.getPeerId());
                        });

                        var dataChannelInfo = new fm.icelink.webrtc.dataChannelInfo({
                            label: 'datachannel',
                            onReceive: function (e) {
                                chatHandler.apply(videoViewport, [
                                    me.getPeerName(e),
                                    e.getData()
                                ]);
                            }
                        });

                        var dataChannelStream = new fm.icelink.webrtc.dataChannelStream([dataChannelInfo], false);

                        var conference = new fm.icelink.conference(me.serverAddressIceLink, me.serverPortIceLink, [audioStream, videoStream, dataChannelStream]);

                        conference.setRelayUsername('test');
                        conference.setRelayPassword('pa55w0rd!');

                        conference.addOnLinkInit(function (e) {
                            chatHandler.apply(videoViewport, [
                                    "Initializing connection with " + me.getPeerName(e)
                            ]);
                        });
                        conference.addOnLinkUp(function (e) {
                            chatHandler.apply(videoViewport, [
                                    "Connected with " + me.getPeerName(e)
                            ]);
                        });
                        conference.addOnLinkDown(function (e) {
                            chatHandler.apply(videoViewport, [
                                    "Lost connection to " + me.getPeerName(e)
                            ]);
                        });
                        videoViewport.conference = conference;
                        videoViewport.dataChannelInfo = dataChannelInfo;
                        videoViewport.layoutManager = layoutManager;

                        Symphony.VideoChat.Notification.joinConference(conference, conferenceChannel);

                    }
                });
            });
        }
    }
    
})();
