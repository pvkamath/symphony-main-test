﻿(function () {
    var oldDateFormat = Date.prototype.format;
    Date.prototype.formatSymphony = function (format) {
        if (format == 'microsoft') {
            // format to MS style
            return '/Date(' + this.getTime() + ')/';
        }
        return oldDateFormat.apply(this, arguments);
    };
    var _id = 0;
    var dateRegex = /\/Date\(-?(\d+[\-|\+]?\d{0,4})\)\//;
    Symphony.quoteRegex = /\"/g;
    Symphony.singleQuoteRegex = /\'/g;

    Symphony.htmlEscape = function (str) {
        return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    };

    Symphony.escapeQuotes = function (str) {
        return str.replace(Symphony.quoteRegex, '\\"').replace(Symphony.singleQuoteRegex, "\\'");
    };

    Symphony.map = function (array, fn, scope) {
        var result = [];
        for (var i = 0; i < array.length; i++) {
            result.push(fn.call(scope || this, array[i]));
        }
        return result;
    }

    Symphony.getIndexById = function (arr, id) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].id === id) {
                return i;
            }
        }
        return null;
    };

    Symphony.getIndexByProperty = function (arr, prop, val) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][prop] === val) {
                return i;
            }
        }
        return null;
    };

    // does a full merge of two objects, including merging sub-objects and sub-arrays
    Symphony.merge = function (dest, src, useArrayIds) {
        useArrayIds = useArrayIds || false;
        for (var prop in src) {
            if (Ext.isObject(src[prop])) {
                if (!dest[prop]) {
                    dest[prop] = {};
                }
                Symphony.merge(dest[prop], src[prop], useArrayIds);
            } else if (Ext.isArray(src[prop])) {
                if (!Ext.isArray(dest[prop])) {
                    dest[prop] = [];
                }
                for (var i = 0; i < src[prop].length; i++) {
                    if (useArrayIds && (prop == "sections" || prop == "pages")) {
                        var index = i;
                        var mergeId = src[prop][i].clientId || src[prop][i].id;
                        if (mergeId) {
                            index = Symphony.getIndexById(dest[prop], mergeId);
                            if (index == null) {
                                continue;
                                //index = i; // if it fails, we assume it's because this is a new item, with negative values
                            }
                        }
                        Symphony.merge(dest[prop][index], src[prop][i], useArrayIds);

                        // after the merge, we don't need the temp client id anymore
                        if (src[prop][i].clientId) {
                            delete src[prop][i].clientId;
                        }
                    }
                    else {
                        Symphony.merge(dest[prop][i], src[prop][i], useArrayIds);
                    }
                }
            } else {
                dest[prop] = src[prop];
            }
        }
        return dest;
    };

    // Move position of an array element
    // http://stackoverflow.com/questions/5306680/move-an-array-element-from-one-array-position-to-another
    // Allows negative indexes too (to reference from the end of the array)
    Array.prototype.move = function (old_index, new_index) {
        while (old_index < 0) {
            old_index += this.length;
        }
        while (new_index < 0) {
            new_index += this.length;
        }
        if (new_index >= this.length) {
            var k = new_index - this.length;
            while ((k--) + 1) {
                this.push(undefined);
            }
        }
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    };


    var spellChecker;

    String.prototype.format = function () {
        var string = this;
        for (var i = 0; i < arguments.length; i++) {
            string = string.replace("{" + i + "}", arguments[i]);
        }
        return string;
    };

    String.prototype.replaceAll = function (token, newToken, ignoreCase) {
        var str, i = -1, _token;
        // ensure we're working w/ strings
        newToken = newToken + '';
        token = token + '';
        if ((str = this.toString()) && typeof token === "string") {
            _token = ignoreCase === true ? token.toLowerCase() : undefined;
            while ((i = (
            _token !== undefined ?
                str.toLowerCase().indexOf(
                            _token,
                            i >= 0 ? i + newToken.length : 0
                ) : str.indexOf(
                            token,
                            i >= 0 ? i + newToken.length : 0
                )
        )) !== -1) {
                str = str.substring(0, i)
                    .concat(newToken)
                    .concat(str.substring(i + token.length));
            }
        }
        return str;
    };

    Ext.override(Ext.Panel, {
        setTabTip: function (tip) {
            Ext.get(this.tabEl).child('span.x-tab-strip-text', true).qtip = tip;
        }
    });

    // Enable state management for all "state aware" components
    Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
        expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 180)) // 180 days
    }));

    Ext.QuickTips.init();
    if (!window.Symphony) {
        window.Symphony = {};
    }

    Symphony = Ext.apply(Symphony, {
        // the year the parser comes back with when it reads a non-null column that has a default
        // value; the year in the db is actually 1/1/1900, but since the JS parser doesn't handle them
        // propertly, it ends up with 2039.
        defaultYear: 2039,
        getCustomer: function () {
            return Symphony.User.customerDomain;
        },
        getUserTimezoneKey: function () {
            var zone = (new Date()).formatSymphony('T');
            // GTM only deals with standard time, not daylight savings
            if (zone.charAt(1) == 'D') {
                zone = zone.charAt(0) + 'S' + zone.charAt(2);
            }
            for (var i = 0; i < Symphony.TimeZones.length; i++) {
                if (Symphony.TimeZones[i].zone == zone) {
                    return Symphony.TimeZones[i].gtmId;
                }
            }
        },
        clone: function (o) {
            if (!o || 'object' !== typeof o) {
                return o;
            }
            if ('function' === typeof o.clone) {
                return o.clone();
            }
            var c = '[object Array]' === Object.prototype.toString.call(o) ? [] : {};
            var p, v;
            for (p in o) {
                if (o.hasOwnProperty(p)) {
                    v = o[p];
                    if (v && 'object' === typeof v) {
                        c[p] = Symphony.clone(v);
                    }
                    else {
                        c[p] = v;
                    }
                }
            }
            return c;
        },
        keys: function (o, f) {
            var keys = [];
            for (var p in o) {
                if (o.hasOwnProperty(p)) {
                    if (f && f(p, o[p], o)) {
                        keys.push(p);
                    } else if (!f) {
                        keys.push(p);
                    }
                }
            }
            return keys;
        },
        Customer: {},
        Classroom: {},
        CourseAssignment: {},
        Date: {},
        Settings: Ext.apply({
            defaultPageSize: 20,
            defaultComboPageSize: 10
        }, Symphony.Settings || {}),
        PermissionText: {
            "Artisan - Author": "Authors online courses.  Access to Artisan Assets, Learning Objects, Assemble Courses and Review tabs.",
            "Artisan - Packager": "Packages online courses.  Access to Artisan Configure and Deploy tab.",
            "Artisan - Viewer": "Reviews previously packaged courses.  Access to Artisan Review tab.",

            "Customer - Administrator": "All functions under the Student Management and Reporting tabs.",
            "Customer - Manager": "View only for Student Management and Reporting functions. Can reset user passwords.",
            "Customer - Limited Administrator": "All functions under the Student Management and Reporting tabs with dataview limited as specified.",
            "Customer - Limited Manager": "View only for Student Management and Reporting functions. Can reset user passwords with dataview limited as specified.",
            "Customer - User": "May take online courses, and register for classroom and virtual courses.",
            "Customer - Job Role Manager": "View only Student Management functions for users in the active user's job role.",
            "Customer - Location Manager": "View only Student Management functions for users in the active user's location.",

            "Classroom - ILT Manager": "All functions under Classroom Training tab, including approvals of student requests.",
            "Classroom - Instructor": "Eligible to be assigned as an instructor for classroom courses.  Enters student attendance and scores.",
            "Classroom - Resource Manager": "Eligible to be assigned as a resource manager for a given venue. Manages resources for classes within that venue.",
            "Classroom - Supervisor": "Eligible to be assigned as a student’s supervisor.  Receives and approves student training requests.",
            "Classroom - User": "The XX role provides full access to all features of the XX.", /*removed*/

            "CourseAssignment - Training Administrator": "All functions under the Course Management tab.",
            "CourseAssignment - Training Manager": "All functions under the Course Management tab, limited to editing their own Training Programs.",
            "CourseAssignment - Help Desk": "The XX role provides full access to all features of the XX.", /*removed*/

            "Collaboration - GTM Organizer": "Can initiate online meetings and invite internal and external users.",
            "Collaboration - GTW Organizer": "Can host and instruct virtual classes.",

            "Reporting - Analyst": "Can view reports.",
            "Reporting - Report Developer": "Can develop reports.",
            "Reporting - Job Role Analyst": "Can view reports for users in the active user's job role.",
            "Reporting - Location Analyst": "Can view reports for users in the active user's location.",
            "Reporting - Supervisor": "" /* TODO: This text needs to be written */
        },
        TimeZones: [
            { zone: 'AST', gtmId: 59, displayName: 'Atlantic Standard Time' },
            { zone: 'EST', gtmId: 61, displayName: 'Eastern Standard Time' },
            { zone: 'CST', gtmId: 64, displayName: 'Central Standard Time' },
            { zone: 'MST', gtmId: 65, displayName: 'Mountain Standard Time' },
            { zone: 'PST', gtmId: 67, displayName: 'Pacific Standard Time' },
            { zone: 'AKST', gtmId: 68, displayName: 'Alaska Standard Time' },
            { zone: 'HST', gtmId: 69, displayName: 'Hawaii Standard Time' }
        ],
        TimeStore: new Ext.data.ArrayStore({
            fields: [
                'hour',
                'minute',
                'text'
            ],
            data: [
                [0, 0, '12:00 AM'],
                [0, 15, '12:15 AM'],
                [0, 30, '12:30 AM'],
                [0, 45, '12:45 AM'],
                [1, 0, '1:00 AM'],
                [1, 15, '1:15 AM'],
                [1, 30, '1:30 AM'],
                [1, 45, '1:45 AM'],
                [2, 0, '2:00 AM'],
                [2, 15, '2:15 AM'],
                [2, 30, '2:30 AM'],
                [2, 45, '2:45 AM'],
                [3, 0, '3:00 AM'],
                [3, 15, '3:15 AM'],
                [3, 30, '3:30 AM'],
                [3, 45, '3:45 AM'],
                [4, 0, '4:00 AM'],
                [4, 15, '4:15 AM'],
                [4, 30, '4:30 AM'],
                [4, 45, '4:45 AM'],
                [5, 0, '5:00 AM'],
                [5, 15, '5:15 AM'],
                [5, 30, '5:30 AM'],
                [5, 45, '5:45 AM'],
                [6, 0, '6:00 AM'],
                [6, 15, '6:15 AM'],
                [6, 30, '6:30 AM'],
                [6, 45, '6:45 AM'],
                [7, 0, '7:00 AM'],
                [7, 15, '7:15 AM'],
                [7, 30, '7:30 AM'],
                [7, 45, '7:45 AM'],
                [8, 0, '8:00 AM'],
                [8, 15, '8:15 AM'],
                [8, 30, '8:30 AM'],
                [8, 45, '8:45 AM'],
                [9, 0, '9:00 AM'],
                [9, 15, '9:15 AM'],
                [9, 30, '9:30 AM'],
                [9, 45, '9:45 AM'],
                [10, 0, '10:00 AM'],
                [10, 15, '10:15 AM'],
                [10, 30, '10:30 AM'],
                [10, 45, '10:45 AM'],
                [11, 0, '11:00 AM'],
                [11, 30, '11:15 AM'],
                [11, 30, '11:30 AM'],
                [11, 45, '11:45 AM'],

                [12, 0, '12:00 PM'],
                [12, 15, '12:15 PM'],
                [12, 30, '12:30 PM'],
                [12, 45, '12:45 PM'],
                [13, 0, '1:00 PM'],
                [13, 15, '1:15 PM'],
                [13, 30, '1:30 PM'],
                [13, 45, '1:45 PM'],
                [14, 0, '2:00 PM'],
                [14, 15, '2:15 PM'],
                [14, 30, '2:30 PM'],
                [14, 45, '2:45 PM'],
                [15, 0, '3:00 PM'],
                [15, 15, '3:15 PM'],
                [15, 30, '3:30 PM'],
                [15, 45, '3:45 PM'],
                [16, 0, '4:00 PM'],
                [16, 15, '4:15 PM'],
                [16, 30, '4:30 PM'],
                [16, 45, '4:45 PM'],
                [17, 0, '5:00 PM'],
                [17, 15, '5:15 PM'],
                [17, 30, '5:30 PM'],
                [17, 45, '5:45 PM'],
                [18, 0, '6:00 PM'],
                [18, 15, '6:15 PM'],
                [18, 30, '6:30 PM'],
                [18, 45, '6:45 PM'],
                [19, 0, '7:00 PM'],
                [19, 15, '7:15 PM'],
                [19, 30, '7:30 PM'],
                [19, 45, '7:45 PM'],
                [20, 0, '8:00 PM'],
                [20, 15, '8:15 PM'],
                [20, 30, '8:30 PM'],
                [20, 45, '8:45 PM'],
                [21, 0, '9:00 PM'],
                [21, 15, '9:15 PM'],
                [21, 30, '9:30 PM'],
                [21, 45, '9:45 PM'],
                [22, 0, '10:00 PM'],
                [22, 15, '10:15 PM'],
                [22, 30, '10:30 PM'],
                [22, 45, '10:45 PM'],
                [23, 0, '11:00 PM'],
                [23, 30, '11:15 PM'],
                [23, 30, '11:30 PM'],
                [23, 45, '11:45 PM']
            ]
        }),
        Modules: {
            Artisan: 'ARTISAN',
            GoToMeeting: 'GTM',
            GoToWebinar: 'GTW',
            Portal: 'PORTAL',
            Reporting: 'REPORTING',
            Classroom: 'CLASSROOM',
            Customer: 'CUSTOMER',
            CourseAssignment: 'COURSEASSIGNMENT',
            hasModule: function (m) {
                return (Symphony.Settings.modules.indexOf(m) > -1);
            }
        },
        Definitions: {
            artisanAsset: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'path' },
                { name: 'assetTypeId' },
                { name: 'template' },
                { name: 'previewTemplate' },
                { name: 'keywords' },
                { name: 'isPublic' },
                { name: 'width' },
                { name: 'height' },
                { name: 'alternateHtml' },
                { name: 'hasAlternateHtml' }
            ],
            artisanAssetType: [
                { name: 'id' },
                { name: 'name' },
                { name: 'extensions' },
                { name: 'template' },
                { name: 'hasAlternateHtml' }
            ],
            artisanTheme: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'cssPath' },
                { name: 'skinCssPath' }
            ],
            publicDocument: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'uploaded' },
                { name: 'categoryName' }
            ],
            category: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' }
            ],
            trainingProgram: [
                { name: 'id' },
                { name: 'name' },
                { name: 'categoryName' },
                { name: 'dueDate' },
                { name: 'startDate' },
                { name: 'endDate' },
                { name: 'isNewHire' },
                { name: 'isLive' },
                { name: 'ownerId' },
                { name: 'internalCode' },
                { name: 'description' },
                { name: 'cost' },
                { name: 'courseCount' },
                { name: 'completed' },
                { name: 'requiredCourses' },
                { name: 'electiveCourses' },
                { name: 'dateCompleted' },
                { name: 'categoryId' },
                { name: 'disableScheduled' }
            ],
            user: [
                { name: 'id' },
                { name: 'customerId' },
                { name: 'firstName' },
                { name: 'lastName' },
                { name: 'fullName' },
                { name: 'supervisor' },
                { name: 'reportingSupervisor' },
                { name: 'username' },
                { name: 'status' },
                { name: 'location' },
                { name: 'jobRole' },
                { name: 'audiences' },
                { name: 'employeeNumber' },
                { name: 'telephoneNumber' },
                { name: 'isAccountExec' },
                { name: 'isCustomerCare' }
            ],
            hierarchy: [
                { name: 'id' },
                { name: 'name' },
                { name: 'parentId' }
            ],
            course: [
                { name: 'id' },
                { name: 'name' },
                { name: 'courseTypeId' },
                { name: 'internalCode' },
                { name: 'description' },
                { name: 'courseObjective' },
                { name: 'score' },
                { name: 'startDate' },
                { name: 'endDate' },
                { name: 'dueDate' },
                { name: 'activeAfterDate' },
                { name: 'hasStarted' },
                { name: 'hasEnded' },
                { name: 'classId' },
                { name: 'passed' },
                { name: 'registrationId' },
                { name: 'registrationStatusId' },
                { name: 'attendedIndicator' },
                { name: 'publicIndicator' },
                { name: 'credit' },
                { name: 'perStudentFee' },
                { name: 'courseCompletionTypeId' },
                { name: 'courseCompletionTypeDescription' },
                { name: 'onlineCourseId' },
                { name: 'onlineCourseName' },
                { name: 'presentationTypeId' },
                { name: 'presentationTypeDescription' },
                { name: 'publicIndicator' },
                { name: 'createdOn' },
                { name: 'modifiedOn' },
                { name: 'version' },
                { name: 'webinarKey' },
                { name: 'launchUrl' },
                { name: 'isStartingSoon' },
                { name: 'categoryId' },
                { name: 'categoryName' },
                { name: 'numberOfDays' },
                { name: 'dailyDuration' },
                { name: 'artisanCourseId' }
            ],
            artisanCourse: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'keywords' },
                { name: 'createdOn' }
            ],
            artisanTemplate: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'html' },
                { name: 'thumbPath' },
                { name: 'pageType' },
                { name: 'questionType' },
                { name: 'cssText' }
            ],
            artisanQuestionType: [
                { name: 'id' },
                { name: 'name' },
                { name: 'displayName' }
            ],
            artisanAnswer: [
                { name: 'id' },
                { name: 'pageId' },
                { name: 'text' },
                { name: 'sort' },
                { name: 'isCorrect' }
            ],
            artisanDeployment: [
                { name: 'id' },
                { name: 'courseIds' },
                { name: 'customerIds' },
                { name: 'name' },
                { name: 'notes' },
                { name: 'isUpdate' },
                { name: 'isUpdateOnly' },
                { name: 'isAutoUpdate' },
                { name: 'toArtisan' },
                { name: 'toSymphony' },
                { name: 'toMars' },
                { name: 'createdOn' }
            ],
            upcomingEvent: [
                { name: 'eventId' },
                { name: 'eventName' },
                { name: 'groupId' },
                { name: 'eventType' },
                { name: 'startDate' },
                { name: 'endDate' },
                { name: 'webinarRegistrationId' },
                { name: 'webinarKey' },
                { name: 'webinarLaunchUrl' },
                { name: 'joinUrl' },
                { name: 'userId' }
            ],
            presentationType: [
                { name: 'id' },
                { name: 'description' }
            ],
            courseCompletionType: [
                { name: 'id' },
                { name: 'description' },
                { name: 'codeName' }
            ],
            category: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'categoryTypeId' }
            ],
            transcriptEntry: [
                { name: 'courseId' },
                { name: 'courseName' },
                { name: 'courseTypeId' },
                { name: 'classId' },
                { name: 'registrationId' },
                { name: 'registrationStatusId' },
                { name: 'passOrFail' },
                { name: 'passed' },
                { name: 'completeOrIncomplete' },
                { name: 'completed' },
                { name: 'onlineTestId' },
                { name: 'surveyId' },
                { name: 'isPublic' },
                { name: 'score' },
                { name: 'attemptDate' },
                { name: 'attemptTime' },
                { name: 'attemptCount' },
                { name: 'startDate' },
                { name: 'endDate' }
            ],
            klass: [
                { name: 'classId' },
                { name: 'courseId' },
                { name: 'className' },
                { name: 'classDescription' },
                { name: 'classRoomName' },
                { name: 'venueName' },
                { name: 'dates' },
                { name: 'startDate' },
                { name: 'numberOfDays' },
                { name: 'dailyDuration' },
                { name: 'registrationClosed' },
                { name: 'webinarKey' }
            ],
            classroomClass: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'internalCode' },
                { name: 'courseId' },
                { name: 'courseName' },
                { name: 'timeZoneId' },
                { name: 'timeZoneDescription' },
                { name: 'capacityOverride' },
                { name: 'lockedIndicator' },
                { name: 'additionalInstructions' },
                { name: 'notes' },
                { name: 'objective' },
                { name: 'isVirtual' },
                { name: 'surveyId' },
                { name: 'venueId' },
                { name: 'venueName' },
                { name: 'venueCity' },
                { name: 'roomId' },
                { name: 'roomName' },
                { name: 'location' },
                { name: 'minClassDate' },
                { name: 'classDates' },
                { name: 'classInstructors' },
                { name: 'numberOfDays' },
                { name: 'dailyDuration' },
                { name: 'approvalRequired' }
            ],
            room: [
                { name: 'id' },
                { name: 'name' },
                { name: 'internalCode' },
                { name: 'roomCost' },
                { name: 'capacity' },
                { name: 'networkAccessIndicator' },
                { name: 'lockedIndicator' },
                { name: 'venueId' }
            ],
            venue: [
                { name: 'id' },
                { name: 'name' },
                { name: 'internalCode' },
                { name: 'city' },
                { name: 'stateId' },
                { name: 'stateName' },
                { name: 'stateDescription' },
                { name: 'timeZoneId' },
                { name: 'timeZoneDescription' }
            ],
            message: [
                { name: 'id' },
                { name: 'senderId' },
                { name: 'sender' },
                { name: 'recipientId' },
                { name: 'recipient' },
                { name: 'subject' },
                { name: 'body' },
                { name: 'isRead' },
                { name: 'createdOn' },
                { name: 'priority' }
            ],
            notificationTemplate: [
                { name: 'id' },
                { name: 'subject' },
                { name: 'body' },
                { name: 'displayName' },
                { name: 'description' },
                { name: 'isScheduled' },
                { name: 'enabled' },
                { name: 'scheduleParameterId' }
            ],
            meeting: [
                { name: 'id' },
                { name: 'startDateTime' },
                { name: 'endDateTime' },
                { name: 'gtmMeetingId' },
                { name: 'gtmOrganizerId' },
                { name: 'meetingId' },
                { name: 'subject' },
                { name: 'joinUrl' },
                { name: 'meetingType' },
                { name: 'maxParticipants' },
                { name: 'timeZoneKey' },
                { name: 'passwordRequired' },
                { name: 'conferenceCallInfo' },
                { name: 'internalInvitees' },
                { name: 'externalInvitees' },
                { name: 'isEditable' },
                { name: 'isOrganizer' }
            ],
            trainingProgramFile: [
                { name: 'id' },
                { name: 'filename' },
                { name: 'createdOn' },
                { name: 'title' },
                { name: 'description' },
                { name: 'percentUploaded' }
            ],
            courseFile: [
                { name: 'id' },
                { name: 'filename' },
                { name: 'createdOn' },
                { name: 'title' },
                { name: 'description' },
                { name: 'percentUploaded' }
            ],
            state: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' }
            ],
            timeZone: [
                { name: 'id' },
                { name: 'description' }
            ],
            classInstructor: [
                { name: 'id' },
                { name: 'classId' },
                { name: 'instructorId' },
                { name: 'firstName' },
                { name: 'middleName' },
                { name: 'lastName' },
                { name: 'fullName' },
                { name: 'gtmOrganizerStatus' },
            /*{name: 'canHostVirtual' },*/
                {name: 'collaborator' }
            ],
            registration: [
                { name: 'id' },
                { name: 'classId' },
                { name: 'registrantId' },
                { name: 'registrationTypeId' },
                { name: 'registrationStatusId' },
                { name: 'classCapacity' },
                { name: 'firstName' },
                { name: 'middleName' },
                { name: 'lastName' },
                { name: 'fullName' },
                { name: 'score' },
                { name: 'attendedIndicator' },
                { name: 'modifiedOn' }
            ],
            classResource: [
                { name: 'id' },
                { name: 'classId' },
                { name: 'resourceId' },
                { name: 'name' },
                { name: 'cost' },
                { name: 'venueId' },
                { name: 'venueName' },
                { name: 'roomId' },
                { name: 'roomName' },
                { name: 'location' }
            ],
            classDate: [
                { name: 'id' },
                { name: 'classId' },
                { name: 'startDateTime' },
                { name: 'duration' }
            ],
            venueResourceManager: [
                { name: 'id' },
                { name: 'resourceManagerId' },
                { name: 'venueId' },
                { name: 'firstName' },
                { name: 'middleName' },
                { name: 'lastName' },
                { name: 'fullName' }
            ],
            resource: [
                { name: 'id' },
                { name: 'name' },
                { name: 'cost' },
                { name: 'venueId' },
                { name: 'roomId' }
            ],
            location: [
                { name: 'id' },
                { name: 'parentId' },
                { name: 'name' },
                { name: 'levelText' },
                { name: 'levelIndentText' }
            ],
            jobRole: [
                { name: 'id' },
                { name: 'parentId' },
                { name: 'name' },
                { name: 'levelText' },
                { name: 'levelIndentText' }
            ],
            audience: [
                { name: 'id' },
                { name: 'parentId' },
                { name: 'name' },
                { name: 'levelText' },
                { name: 'levelIndentText' }
            ],
            userStatus: [
                { name: 'id' },
                { name: 'description' }
            ],
            salesChannel: [
                { name: 'id' },
                { name: 'parentId' },
                { name: 'name' }
            ],
            customer: [
                { name: 'id' },
                { name: 'salesChannelId' },
                { name: 'parentId' },
                { name: 'name' },
                { name: 'evaluationIndicator' },
                { name: 'evaluationEndDate' },
                { name: 'suspendedIndicator' },
                { name: 'userLimit' },
                { name: 'selfRegistrationIndicator' },
                { name: 'selfRegistrationPassword' },
                { name: 'lockoutCount' },
                { name: 'subdomain' },
                { name: 'quickQueryIndicator' },
                { name: 'reportBuilderIndicator' },
                { name: 'customerCareRep' },
                { name: 'accountExecutive' }
            ],
            importHistory: [
                { name: 'id' },
                { name: 'startDate' },
                { name: 'description' },
                { name: 'endDate' },
                { name: 'status' },
                { name: 'statusCode' },
                { name: 'lastUpdated' },
                { name: 'errored' }
            ],
            gtmOrganizer: [
                { name: 'id' },
                { name: 'organizerKey' },
                { name: 'email' },
                { name: 'password' }
            ],
            pendingRegistration: [
                { name: 'classId' },
                { name: 'registrationId' },
                { name: 'name' },
                { name: 'studentName' },
                { name: 'startDate' },
                { name: 'location' },
                { name: 'status' }
            ],
            onlineCourseRollup: [
                { name: 'id' },
                { name: 'userId' },
                { name: 'courseId' },
                { name: 'trainingProgramId' },
                { name: 'score' },
                { name: 'attemptCount' },
                { name: 'username' }
            ],
            reportType: [
                { name: 'id' },
                { name: 'name' },
                { name: 'userNotes' },
                { name: 'isEnabled' },
                { name: 'codeName' }
            ],
            reportJob: [
                { name: 'id' },
                { name: 'name' },
                { name: 'isScheduled' },
                { name: 'userNotes' },
                { name: 'group' },
                { name: 'description' },
                { name: 'jobType' }
            ],
            reportAudience: [
                { name: 'audiencekey' },
                { name: 'name' },
                { name: 'customerkey' },
                { name: 'audienceLevelDetail' },
                { name: 'audienceId' },
                { name: 'parentId' }
            ],
            reportLocation: [
                { name: 'locationkey' },
                { name: 'Name' },
                { name: 'customerkey' },
                { name: 'locationLevelDetail' },
                { name: 'locationId' },
                { name: 'parentLocationId' },
                { name: 'customerId' }
            ],
            reportJobRole: [
                { name: 'jobRolekey' },
                { name: 'Name' },
                { name: 'customerkey' },
                { name: 'jobRoleLevelDetail' },
                { name: 'jobRoleId' },
                { name: 'parentJobRoleId' },
                { name: 'customerId' }
            ],
            reportTrainingProgram: [
                { name: 'trainingProgramkey' },
                { name: 'name' },
                { name: 'customerkey' },
                { name: 'trainingProgramId' },
                { name: 'isCurrentIndicator' },
                { name: 'customerId' }
            ],
            reportCourse: [
                { name: 'coursekey' },
                { name: 'coursekeyType' },
                { name: 'name' },
                { name: 'customerkey' },
                { name: 'thirdPartyIndicator' },
                { name: 'courseId' },
                { name: 'customerId' }
            ],
            reportUser: [
                { name: 'userkey' },
                { name: 'firstName' },
                { name: 'lastName' },
                { name: 'middleName' },
                { name: 'customerkey' },
                { name: 'userId' },
                { name: 'isImported' },
                { name: 'customerId' },
                { name: 'fullName' }
            ],
            report: [
                { name: 'id' },
                { name: 'ownerId' },
                { name: 'customerId' },
                { name: 'name' },
                { name: 'reportTypeId' },
                { name: 'notifyWhenReady' },
                { name: 'downloadCSV' },
                { name: 'downloadXLS' },
                { name: 'downloadPDF' },
                { name: 'parameters' },
                { name: 'scheduleType' },
                { name: 'scheduleHour' },
                { name: 'scheduleDaysOfWeek' },
                { name: 'scheduleDayOfMonth' },
                { name: 'scheduleMonth' },
                { name: 'isFavorite' },
                { name: 'ownerName' },
                { name: 'reportTypeCode' },
                { name: 'nextRunTime' }
            ],
            reportQueue: [
                { name: 'id' },
                { name: 'queueCommand' },
                { name: 'parameters' },
                { name: 'reportType' },
                { name: 'isEnabled' },
                { name: 'jobId' }
            ],
            reportQueueEntry: [
                { name: 'id' },
                { name: 'reportId' },
                { name: 'queueId' },
                { name: 'status' },
                { name: 'statusId' },
                { name: 'startedOn' },
                { name: 'finishedOn' },
                { name: 'reportName' },
                { name: 'downloadCSV' },
                { name: 'downloadXLS' },
                { name: 'downloadPDF' },
                { name: 'nextRunTime' }
            ]
        }
    });

    Symphony.newId = function () {
        // create a unique identifier
        return "symphony_" + _id++;
    };

    //TODO: load this from the database so we use the appropriate customer-specific values
    Symphony.HierarchyTypeStoreData = [
        [Symphony.HierarchyType.location, Symphony.Aliases.location],
        [Symphony.HierarchyType.jobRole, Symphony.Aliases.jobRole],
        [Symphony.HierarchyType.audience, Symphony.Aliases.audience],
        [Symphony.HierarchyType.user, 'User']
    ];
    Symphony.ImportTypeStore = new Ext.data.ArrayStore({
        fields: [
            'id', 'text'
        ],
        data: [
            [Symphony.ImportType.user, 'Users'],
            [Symphony.ImportType.jobRole, Symphony.Aliases.jobRoles],
            [Symphony.ImportType.location, Symphony.Aliases.locations],
            [Symphony.ImportType.audience, Symphony.Aliases.audiences]
        ]
    });
    Symphony.RegistrationStatusTypeStore = new Ext.data.ArrayStore({
        fields: [
            'id', 'text'
        ],
        data: [
            [Symphony.RegistrationStatusType.awaitingApproval, 'Awaiting Approval'],
            [Symphony.RegistrationStatusType.waitList, 'Wait List'],
            [Symphony.RegistrationStatusType.denied, 'Denied'],
            [Symphony.RegistrationStatusType.registered, 'Registered']
        ]
    });
    Symphony.LimitedCourseCompletionTypeStore = new Ext.data.ArrayStore({
        fields: [
            'id', 'text'
        ],
        data: [
            [Symphony.CourseCompletionType.attendance, 'Attendance'],
            [Symphony.CourseCompletionType.numeric, 'Numeric'],
            [Symphony.CourseCompletionType.letter, 'Letter']
        ]
    });
    Symphony.PresentationType = {
        'Class': true,
        'Lecture': true,
        'Seminar': true,
        'Vendor Training': true,
        'Third Party Training': true,
        'Conference': true,
        'Forum': true,
        'Meeting': true,
        'Webinar': true
    };

    /* a couple generic renderers */
    Symphony.parseDate = function (value, addLocalOffset) {
        if (!value) { return value; }
        var ticksAndZone = value.match(dateRegex)[1];
        var delim = ticksAndZone.indexOf('-') > -1 ? '-' : '+';
        var parts = ticksAndZone.split(delim);
        var ticks = parseInt(parts[0], 10);
        var dt = new Date(ticks);
        if (addLocalOffset) {
            var offset = dt.getTimezoneOffset();
            dt = dt.add(Date.MINUTE, offset);
        }
        return dt;
    };
    Symphony.dateRenderer = function (value) {
        if (!value) {
            return '';
        }
        var dt = null;
        if (Ext.isString(value)) {
            dt = Symphony.parseDate(value);
        } else {
            dt = value;
        }
        var dateString = dt.formatSymphony('m/d/Y');
        return Symphony.qtipRenderer(dateString);
    };
    Symphony.dateTimeRenderer = function (value) {
        if (!value) {
            return '';
        }
        var dateString = Symphony.parseDate(value).formatSymphony('m/d/Y g:i a T');
        return Symphony.qtipRenderer(dateString);
    };
    Symphony.naturalTimeRendererTZ = function (value) {
        if (!value) {
            return '';
        }
        var dateString = Symphony.parseDate(value).formatSymphony('g:i a T');
        return Symphony.qtipRenderer(dateString);
    };
    Symphony.naturalDateRenderer = function (value) {
        if (!value) {
            return '';
        }
        var dateString = Symphony.parseDate(value).formatSymphony('M j, Y');
        return Symphony.qtipRenderer(dateString);
    };
    Symphony.longDateRenderer = function (value) {
        if (!value) {
            return '';
        }
        var dateString = Symphony.parseDate(value).formatSymphony('D, F j, Y g:i a');
        return Symphony.qtipRenderer(dateString);
    };

    Symphony.shortDateRenderer = function (value) {
        if (!value) {
            return '';
        }
        var dateString = Symphony.parseDate(value).formatSymphony('m/d/y');
        return Symphony.qtipRenderer(dateString);
    };
    Symphony.naturalTimeRenderer = function (value, hideTip) {
        if (!value) {
            return '';
        }
        var timeString = Symphony.parseDate(value).formatSymphony('g:i a');
        if (hideTip) { return timeString; }
        return Symphony.qtipRenderer(timeString);
    };
    Symphony.naturalTimeRendererTZ = function (value, hideTip) {
        if (!value) {
            return '';
        }
        var timeString = Symphony.parseDate(value).formatSymphony('g:i a T');
        if (hideTip) { return timeString; }
        return Symphony.qtipRenderer(timeString);
    };
    Symphony.shortTimeRenderer = function (value, hideTip) {
        if (!value) {
            return '';
        }
        var timeString = Symphony.parseDate(value).formatSymphony('g:ia');
        if (hideTip) { return timeString; }
        return Symphony.qtipRenderer(timeString);
    };
    Symphony.timeRenderer = function (value, hideTip) {
        if (!value) {
            return '';
        }
        var timeString = Symphony.parseDate(value).formatSymphony('g:i:s A T');
        if (hideTip) { return timeString; }
        return Symphony.qtipRenderer(timeString);
    };
    Symphony.checkRenderer = function (value) {
        if (!value) {
            return '<img src="/images/bullet_black.png" />';
        }
        return '<img src="/images/tick.png" />';
    };
    Symphony.starRenderer = function (value) {
        if (!value) {
            return '<img src="/images/bullet_white.png" />';
        }
        return '<img src="/images/star.png" />';
    };
    Symphony.qtipRenderer = function (title, qtip) {
        if (arguments.length == 1) {
            qtip = title;
        }
        if (qtip && qtip.replace) {
            // escape quotes
            qtip = qtip.replace(Symphony.quoteRegex, '&quot;');
        }
        return '<span ext:qtip="' + qtip + '">' + title + '</span>';
    };

    Symphony.quickStore = function (items) {
        var result = [];
        for (var i = 0; i < items.length; i++) {
            var inner = [];
            if (!Ext.isObject(items[i])) {
                inner.push(items[i]);
                inner.push(items[i]);
            } else {
                for (var prop in items[i]) {
                    if (items[i].hasOwnProperty(prop)) {
                        inner.push(prop);
                        inner.push(items[i][prop]);
                    }
                }
            }
            result.push(inner);
        }
        return new Ext.data.SimpleStore({ fields: ['id', 'name'], data: result });
    };

    Symphony.PagedComboBox = Ext.extend(Ext.form.ComboBox, {
        initComponent: function () {
            var proxy = this.proxy || new Ext.data.HttpProxy({
                method: 'GET',
                url: this.url
            });
            this.pageSize = this.pageSize || Symphony.Settings.defaultComboPageSize;

            var store = this.store || new Ext.data.Store({
                remoteSort: true,
                proxy: proxy,
                baseParams: { limit: this.pageSize },
                reader: new Ext.data.JsonReader({
                    idProperty: this.idProperty || null,
                    totalProperty: 'totalSize',
                    root: 'data'
                }, this.recordDef)
            });
            if (this.blankValueTitle) {
                var me = this;
                store.on('load', function () {
                    var obj = {};
                    obj[me.displayField] = me.blankValueTitle;
                    obj[me.valueField] = 0;
                    store.insert(0, new me.recordDef(obj));
                }, { single: true });
            }
            Ext.apply(this, {
                queryParam: 'searchText',
                triggerAction: 'all',
                loadMask: true,
                forceSelection: true,
                minChars: 0,
                mode: 'remote',
                displayField: this.displayField || 'name',
                valueField: this.valueField || 'id',
                store: store
            });

            Symphony.PagedComboBox.superclass.initComponent.apply(this, arguments);
        },
        setUrl: function (url) {
            this.url = url;
            if (this.store) {
                this.store.proxy = new Ext.data.HttpProxy({
                    method: 'GET',
                    url: url
                });
                this.store.reload();
            }
        }
    });
    Ext.reg('symphony.pagedcombobox', 'Symphony.PagedComboBox');

    Symphony.TimeZoneCombo = Ext.extend(Ext.form.ComboBox, {
        initComponent: function () {
            Ext.apply(this, {
                triggerAction: 'all',
                mode: 'local',
                displayField: 'displayName',
                valueField: 'id',
                store: this.generateStore()
            });

            Symphony.TimeZoneCombo.superclass.initComponent.apply(this, arguments);
        },
        generateStore: function () {
            var zones = [];
            Ext.each(Symphony.TimeZones, function (tz) {
                zones.push([tz.displayName, tz.zone, tz.gtmId]);
            });
            var store = new Ext.data.ArrayStore({
                fields: ['displayName', 'zone', 'id'],
                data: zones
            });
            return store;
        }
    });
    Ext.reg('symphony.timezonecombo', 'Symphony.TimeZoneCombo');

    // the timezone combobox above uses the GoToMeeting zones, etc, this one uses the .NET ones
    Symphony.NetTimeZoneCombo = Ext.extend(Ext.form.ComboBox, {
        initComponent: function () {
            Ext.apply(this, {
                triggerAction: 'all',
                mode: 'local',
                displayField: 'displayName',
                valueField: 'zoneId',
                store: this.generateStore()
            });

            Symphony.NetTimeZoneCombo.superclass.initComponent.apply(this, arguments);
        },
        generateStore: function () {
            var zones = [];
            Ext.each(Symphony.NetTimeZones, function (tz) {
                zones.push([tz.displayName, tz.zoneId]);
            });
            var store = new Ext.data.ArrayStore({
                fields: ['displayName', 'zoneId'],
                data: zones
            });
            return store;
        }
    });
    Ext.reg('symphony.nettimezonecombo', 'Symphony.NetTimeZoneCombo');


    Symphony.getCombo = function (name, label, store, options) {
        return Ext.apply({
            xtype: 'combo',
            name: name,
            allowBlank: true,
            fieldLabel: label,
            anchor: '100%',
            triggerAction: 'all',
            mode: 'local',
            forceSelection: false,
            editable: false,
            store: store,
            valueField: 'id',
            displayField: 'text'
        }, options);
    };

    Symphony.getYesNoInheritStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            fields: ['id', 'text'],
            data: [
                [null, '- Default -'],
                [0, 'No'],
                [1, 'Yes']
            ]
        });
    };

    Symphony.getArtisanTestTypeStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            fields: ['id', 'text'],
            data: [
                [null, '- Default -'],
                [Symphony.ArtisanTestType.gameBoard, 'Game Board'],
                [Symphony.ArtisanTestType.gameWheel, 'Game Wheel'],
                [Symphony.ArtisanTestType.sequential, 'Sequential'],
                [Symphony.ArtisanTestType.random, 'Random']
            ]
        });
    };

    Symphony.getRetestModeStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            fields: ['id', 'text'],
            data: [
                [Symphony.RetestMode.alwaysAllow, 'Always Allow'],
                [Symphony.RetestMode.onlyOnFailure, 'Only Allow on Failure']
            ]
        });
    };

    Symphony.getArtisanNavigationMethodStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            fields: ['id', 'text'],
            data: [
                [null, '- Default -'],
                [Symphony.ArtisanCourseNavigationMethod.sequential, 'Sequential'],
                [Symphony.ArtisanCourseNavigationMethod.freeForm, 'Free Form']
            ]
        });
    };

    /* basic grids */
    Symphony.LocalGrid = Ext.extend(Ext.grid.GridPanel, {
        initComponent: function () {
            if (!this.recordDef) {
                var names = [];
                for (var i = 0; i < this.colModel.config.length; i++) {
                    var dataIndex = this.colModel.config[i].dataIndex;
                    if (dataIndex) {
                        names.push({ name: dataIndex });
                    }
                }
                this.recordDef = Ext.data.Record.create(names);
            }
            var store = new Ext.data.Store({
                remoteSort: false,
                reader: new Ext.data.JsonReader({
                    idProperty: this.idProperty || null
                }, this.recordDef),
                sortInfo: this.sortInfo
            });

            Ext.apply(this, {
                loadMask: true,
                store: store,
                colModel: this.colModel
            });
            Symphony.LocalGrid.superclass.initComponent.apply(this, arguments);
        },
        refresh: function () {
            this.store.loadData(this.data || []);
        },
        onRender: function () {
            this.setData(this.data);
            Symphony.LocalGrid.superclass.onRender.apply(this, arguments);
        },
        setData: function (data) {
            this.data = data;
            this.store.loadData(data || []);
        },
        remove: function (name, value) {
            // if only one value is specified, use the 'id' field
            if (arguments.length == 1) {
                value = name;
                name = 'id';
            }
            var idx = this.store.find(name, value);
            this.store.removeAt(idx);
        },
        disable: function () {
            if (!this.rendered) {
                this.on('afterrender', function () {
                    this.disable();
                }, this, { single: true });
            } else {
                if (!this.disabledDiv) {
                    this.disabledDiv = div = document.createElement('div');
                    div.style.width = '100%';
                    div.style.height = '100%';
                    div.style.position = 'absolute';
                    div.style.left = 0;
                    div.style.top = 0;
                    div.style.backgroundColor = '#eee';
                    Ext.get(div).setOpacity(0.4);
                    this.body.dom.appendChild(div);
                }
            }
        },
        enable: function () {
            if (this.rendered) {
                this.body.dom.removeChild(this.disabledDiv);
            }
            delete this.disabledDiv;
        }
    });
    Ext.reg('symphony.localgrid', 'Symphony.LocalGrid');

    Symphony.UnpagedGrid = Ext.extend(Ext.grid.GridPanel, {
        initComponent: function () {
            var proxy = this.proxy || new Ext.data.HttpProxy({
                method: 'GET',
                url: this.url
            });

            if (!this.recordDef) {
                var names = [];
                for (var i = 0; i < this.colModel.config.length; i++) {
                    var dataIndex = this.colModel.config[i].dataIndex;
                    if (dataIndex) {
                        names.push({ name: dataIndex });
                    }
                }
                this.recordDef = Ext.data.Record.create(names);
            }

            var store = this.store || new Ext.data.Store({
                remoteSort: false,
                proxy: proxy,

                reader: new Ext.data.JsonReader({
                    idProperty: this.idProperty || null,
                    root: 'data'
                }, this.recordDef)
            });

            Ext.apply(this, {
                loadMask: this.hasOwnProperty('loadMask') ? this.loadMask : true,
                store: store,
                colModel: this.colModel
            });
            Symphony.UnpagedGrid.superclass.initComponent.apply(this, arguments);
        },
        refresh: function () {
            this.store.reload();
        },
        onRender: function () {
            this.store.on('exception', function (proxy, type, action, options, response, arg) {
                if (response.status == 200) {
                    var result = Ext.decode(response.responseText);
                    Ext.Msg.alert('Error', result.error);
                } else {
                    //TODO: add option to report/add details/etc
                    Ext.Msg.alert('Error', 'The store failed to load properly; the server responded with error code ' + response.status);
                }

            });
            Symphony.UnpagedGrid.superclass.onRender.apply(this, arguments);

            window.setTimeout(function () {
                if (!this.deferLoad) {
                    this.store.load();
                }
            } .createDelegate(this), 1);
        },
        updateStore: function (url) {
            this.url = url;
            if (this.store) {
                this.store.proxy = new Ext.data.HttpProxy({
                    method: 'GET',
                    url: url
                });
                this.store.reload();
            }
        }
    });

    Symphony.EditableUnpagedGrid = Ext.extend(Ext.grid.EditorGridPanel, {
        initComponent: function () {
            var proxy = this.proxy || new Ext.data.HttpProxy({
                method: 'GET',
                url: this.url
            });

            if (!this.recordDef) {
                var names = [];
                for (var i = 0; i < this.colModel.config.length; i++) {
                    var dataIndex = this.colModel.config[i].dataIndex;
                    if (dataIndex) {
                        names.push({ name: dataIndex });
                    }
                }
                this.recordDef = Ext.data.Record.create(names);
            }

            var store = this.store || new Ext.data.Store({
                remoteSort: false,
                proxy: proxy,

                reader: new Ext.data.JsonReader({
                    idProperty: this.idProperty || null,
                    root: 'data'
                }, this.recordDef)
            });

            Ext.apply(this, {
                loadMask: true,
                store: store,
                colModel: this.colModel
            });
            Symphony.EditableUnpagedGrid.superclass.initComponent.apply(this, arguments);
        },
        refresh: function () {
            this.store.reload();
        },
        onRender: function () {
            this.store.on('exception', function (proxy, type, action, options, response, arg) {
                if (response.status == 200) {
                    var result = Ext.decode(response.responseText);
                    Ext.Msg.alert('Error', result.error);
                } else {
                    //TODO: add option to report/add details/etc
                    Ext.Msg.alert('Error', 'The store failed to load properly; the server responded with error code ' + response.status);
                }

            });
            Symphony.EditableUnpagedGrid.superclass.onRender.apply(this, arguments);

            window.setTimeout(function () {
                if (!this.deferLoad) {
                    this.store.load();
                }
            } .createDelegate(this), 1);
        },
        disable: function () {
            if (!this.rendered) {
                this.on('afterrender', function () {
                    this.disable();
                }, this, { single: true });
            } else {
                if (!this.disabledDiv) {
                    this.disabledDiv = div = document.createElement('div');
                    div.style.width = '100%';
                    div.style.height = '100%';
                    div.style.position = 'absolute';
                    div.style.left = 0;
                    div.style.top = 0;
                    div.style.backgroundColor = '#eee';
                    Ext.get(div).setOpacity(0.4);
                    this.body.dom.appendChild(div);
                }
            }
        },
        enable: function () {
            if (this.rendered) {
                this.body.dom.removeChild(this.disabledDiv);
            }
            delete this.disabledDiv;
        }
    });
    Ext.reg('symphony.editableunpagedgrid', 'Symphony.EditableUnpagedGrid');

    Symphony.SimpleGrid = Ext.extend(Ext.grid.GridPanel, {
        initComponent: function () {
            var proxy = this.proxy || new Ext.data.HttpProxy({
                method: 'GET',
                url: this.url
            });
            this.pageSize = this.pageSize || Symphony.Settings.defaultPageSize;

            if (!this.recordDef) {
                var names = [];
                for (var i = 0; i < this.colModel.config.length; i++) {
                    var dataIndex = this.colModel.config[i].dataIndex;
                    if (dataIndex) {
                        names.push({ name: dataIndex });
                    }
                }
                this.recordDef = Ext.data.Record.create(names);
            }

            var store = this.store || new Ext.data.Store({
                remoteSort: true,
                proxy: proxy,
                baseParams: { limit: this.pageSize },
                reader: new Ext.data.JsonReader({
                    idProperty: this.idProperty || null,
                    totalProperty: 'totalSize',
                    root: 'data'
                }, this.recordDef)
            });


            var pager = this.pager || new Ext.PagingToolbar({
                pageSize: this.pageSize,
                displayInfo: (this.displayPagingInfo === false ? false : true),
                displayMsg: '{0} - {1} of {2}',
                emptyMsg: 'No results',
                store: store
            });

            Ext.apply(this, {
                loadMask: true,
                store: store,
                bbar: pager,
                colModel: this.colModel
            });

            Symphony.SimpleGrid.superclass.initComponent.apply(this, arguments);

            var me = this;
            this.getSelectionModel().on('rowselect', function (sm, rowIndex, record) {
                me.fireEvent('rowselect', me, rowIndex, record);
            });
        },
        refresh: function () {
            this.store.reload();
        },
        onRender: function () {
            Symphony.SimpleGrid.superclass.onRender.apply(this, arguments);
            window.setTimeout(function () {
                if (!this.deferLoad) {
                    this.store.load({ params: { start: 0} });
                }
                this.store.on('exception', function (proxy, type, action, options, response, arg) {
                    if (response.status == 200) {
                        var result = Ext.decode(response.responseText);
                        Ext.Msg.alert('Error', result.error);
                    } else {
                        //TODO: add option to report/add details/etc
                        Ext.Msg.alert('Error', 'The store failed to load properly; the server responded with error code ' + response.status);
                    }

                });
            } .createDelegate(this), 1);
        },
        disable: function () {
            if (!this.rendered) {
                this.on('afterrender', function () {
                    this.disable();
                }, this, { single: true });
            } else {
                if (!this.disabledDiv) {
                    this.disabledDiv = div = document.createElement('div');
                    div.style.width = '100%';
                    div.style.height = '100%';
                    div.style.position = 'absolute';
                    div.style.left = 0;
                    div.style.top = 0;
                    div.style.backgroundColor = '#eee';
                    Ext.get(div).setOpacity(0.4);
                    this.body.dom.appendChild(div);
                }
            }
        },
        enable: function () {
            if (this.rendered && this.disabledDiv) {
                this.body.dom.removeChild(this.disabledDiv);
            }
            delete this.disabledDiv;
        }
    });

    Symphony.SearchableGrid = Ext.extend(Symphony.SimpleGrid, {
        performSearch: function (searchText) {
            // set it as a "base" parameter so it applies to pagination and refreshes too
            this.store.setBaseParam('searchText', searchText);
            this.store.load();
        },
        initComponent: function () {
            var grid = this;
            var currentItems = this.tbar ? this.tbar.items || [] : [];
            Ext.apply(this, {
                tbar: {
                    hideBorders: true,
                    items: currentItems.concat([
                        '->',
                        {
                            xtype: 'textfield',
                            name: 'searchText',
                            emptyText: 'Enter search text',
                            listeners: {
                                specialkey: function (txt, e) {
                                    if (e.getKey() == e.ENTER) {
                                        grid.performSearch(txt.getValue());
                                    }
                                }
                            }
                        },
                        {
                            xtype: grid.searchMenu ? 'splitbutton' : 'button',
                            ref: '../searchButton',
                            iconCls: 'x-button-search',
                            menu: grid.searchMenu,
                            handler: function (b, e) {
                                var txt = b.findParentByType('toolbar').find('name', 'searchText')[0];
                                grid.performSearch(txt.getValue());
                            }
                        }
                    ])
                }
            });
            Symphony.SearchableGrid.superclass.initComponent.apply(this, arguments);
        },
        addSearchMenuItems: function (items) {
            if (this.searchButton && this.searchButton.menu) {
                this.searchButton.menu.add(items);
            }
        },
        searchOptionClicked: function (item, checked) {
            var me = this;
            if (me._filter) {
                me.store.proxy.url = me.url;
                me.store.un('beforeload', me._filter);
            }

            if (checked) {
                if (item.filter) {
                    me._filter = Ext.bind(me.filter, me, [item.filter]);
                    me.store.on('beforeload', me._filter);
                }
                me.refresh();
            }
        },
        checkSearchOptionClicked: function (item, checked) {
            var me = this;

            if (me._filter) {
                me.store.proxy.url = me.url;
                me.store.un('beforeload', me._filter);
            }

            var filter = [];
            for (var i = 0; i < me.searchButton.menu.items.length; i++) {
                var menuItem = me.searchButton.menu.items.items[i];
                if (menuItem.checked) {
                    filter.push(menuItem.filter);
                }
            }
            if (filter.length > 0) {
                me._filter = Ext.bind(me.filter, me, [filter]);
                me.store.on('beforeload', me._filter);
            }
            me.refresh();
        },
        filter: function (filter) {
            var f = Ext.util.JSON.encode(filter);
            this.store.proxy.url = this.url + '?filter=' + f;
        },
        updateStore: function (url) {
            this.url = url;
            if (this.store) {
                this.store.proxy = new Ext.data.HttpProxy({
                    method: 'GET',
                    url: url
                });
                this.store.reload();
            }
        }
    });

    Ext.reg('symphony.searchablegrid', 'Symphony.SearchableGrid');

    Symphony.SearchableLocalGrid = Ext.extend(Symphony.LocalGrid, {
        performSearch: function (searchText) {
            // set it as a "base" parameter so it applies to pagination and refreshes too
            if (!this.store) { return; }
            if (searchText) {
                var filter = [];
                for (var i = 0; i < this.searchColumns.length; i++) {
                    filter.push({
                        property: this.searchColumns[i],
                        value: searchText
                    });
                }
                this.store.filter(filter);
            } else {
                this.store.clearFilter();
            }
        },
        initComponent: function () {
            var grid = this;
            var currentItems = this.tbar ? this.tbar.items || [] : [];
            Ext.apply(this, {
                tbar: {
                    hideBorders: true,
                    items: currentItems.concat([
                        '->',
                        {
                            xtype: 'textfield',
                            name: 'searchText',
                            emptyText: 'Enter search text',
                            listeners: {
                                specialkey: function (txt, e) {
                                    if (e.getKey() == e.ENTER) {
                                        grid.performSearch(txt.getValue());
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-button-search',
                            handler: function (b, e) {
                                var txt = b.findParentByType('toolbar').find('name', 'searchText')[0];
                                grid.performSearch(txt.getValue());
                            }
                        }
                    ])
                }
            });
            Symphony.SearchableLocalGrid.superclass.initComponent.apply(this, arguments);
        }
    });
    Ext.reg('symphony.searchablelocalgrid', 'Symphony.SearchableLocalGrid');

    /* end basic grids */

    // A simple editor for name/description pairs
    // it assumes the "save" url is the original url with a "save/" tacked on the end
    // and that the original url can be used without any parameters to get the complete list
    // of items to be edited
    Symphony.EditNameDescriptionPairs = function (title, url, callback, options) {
        options = options || {};

        var w = new Ext.Window({
            title: 'Manage Categories',
            modal: true,
            width: 500,
            height: 500,
            layout: 'fit',
            items: [{
                tbar: [{
                    xtype: 'button',
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function () {
                        var Category = Ext.data.Record.create(Symphony.Definitions.category);
                        var category = new Category({ name: 'New Category', description: 'New Category' });
                        var grid = w.grid;
                        grid.store.add(category);
                        grid.startEditing(grid.store.getCount() - 1, 0);
                    }
                }],
                border: false,
                xtype: 'symphony.editableunpagedgrid',
                url: url,
                layout: 'fit',
                ref: 'grid',
                model: 'category',
                autoExpandColumn: 'description',
                clicksToEdit: 1,
                colModel: new Ext.grid.ColumnModel({
                    defaults: {
                        sortable: true,
                        renderer: Symphony.Portal.valueRenderer
                    },
                    columns: [
                        { /*id: 'name',*/ header: 'Name', dataIndex: 'name', editor: new Ext.form.TextField({ allowBlank: false }) },
                        { /*id: 'description',*/ header: 'Description', dataIndex: 'description', editor: new Ext.form.TextField({ allowBlank: false }) }
                    ]
                })
            }],
            buttons: [{
                text: 'Save',
                iconCls: 'x-button-save',
                handler: function () {
                    w.grid.stopEditing();
                    var records = w.grid.store.getModifiedRecords();
                    var results = [];
                    for (var i = 0; i < records.length; i++) {
                        var len = results.push(records[i].data);
                        if (options.categoryTypeId) {
                            results[len - 1].categoryTypeId = options.categoryTypeId;
                        }
                    }
                    Symphony.Ajax.request({
                        url: url + 'save/',
                        jsonData: results,
                        success: function (result) {
                            w.close();
                            if (callback) {
                                callback(result);
                            }
                        }
                    });
                }
            }, {
                text: 'Cancel',
                iconCls: 'x-button-cancel',
                handler: function () {
                    w.close();
                }
            }]
        }).show();
    };

    /* start tree */
    Symphony.SearchableTree = Ext.extend(Ext.tree.TreePanel, {
        idColumn: 'id',
        parentColumn: 'parentId',
        displayColumn: 'name',
        autoScroll: true,
        containerScroll: true,
        animate: true,
        cascadeChecks: true,
        rootVisible: false,
        rootText: 'Root',
        initComponent: function () {
            var currentItems = this.tbar ? this.tbar.items || [] : [];
            var tree = this;

            Ext.apply(this, {
                hiddenNodes: [],
                root: this.root || new Ext.tree.TreeNode({
                    text: this.rootText,
                    children: this.data || []
                }),
                rootVisible: this.rootVisible,
                tbar: {
                    hideBorders: true,
                    items: currentItems.concat([
                        '->',
                        {
                            xtype: 'textfield',
                            name: 'searchText',
                            emptyText: 'Enter search text',
                            listeners: {
                                specialkey: function (txt, e) {
                                    if (e.getKey() == e.ENTER) {
                                        tree.performSearch(txt.getValue());
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-button-search',
                            handler: function (b, e) {
                                var txt = b.findParentByType('toolbar').find('name', 'searchText')[0];
                                tree.performSearch(txt.getValue());
                            }
                        }
                    ])
                },
                listeners: Ext.apply({
                    checkchange: function (node, checked) {
                        if (!tree.cascadeChecks) { return; }
                        tree.cascadeChecks = false;
                        node.cascade(function (subnode) {
                            subnode.ui.toggleCheck(checked);
                            if (checked) {
                                subnode.disable();
                            } else {
                                subnode.enable();
                            }
                        });
                        node.enable();
                        tree.cascadeChecks = true;
                    }
                }, this.listeners || {})
            });
            Symphony.SearchableTree.superclass.initComponent.apply(this, arguments);
        },
        disableSubnodes: function () {
            if (!this.cascadeChecks) { return; }
            this.cascadeChecks = false;

            var cascadeNode = function (node) {
                for (var i = 0; i < node.childNodes.length; i++) {
                    var thisNode = node.childNodes[i];
                    if (thisNode.parentNode.ui.isChecked()) {
                        // node is checked, check and disable subnodes
                        thisNode.cascade(function (subnode) {
                            subnode.ui.toggleCheck(true);
                            subnode.disable();
                        });
                    } else {
                        cascadeNode(thisNode);
                    }
                }
            };
            cascadeNode(this.root);

            this.cascadeChecks = true;
        },
        performSearch: function (text) {
            Ext.each(this.hiddenNodes, function (n) {
                n.ui.show();
            });
            if (!text) {
                this.filter.clear();
                return;
            }
            this.expandAll();

            this.filter.filterBy(function (n) {
                return this.leafFilter(text, n) || n.hasChildNodes();
            }, this);

            // hide empty nodes that weren't filtered
            this.hiddenNodes = [];
            this.root.cascade(function (n) {
                if (this.checkHidden(text, n)) {
                    n.ui.hide();
                    this.hiddenNodes.push(n);
                }
            }, this);
        },
        checkHidden: function (text, node) {
            return node.ui.ctNode.offsetHeight < 3 && node.hasChildNodes() && this.parentFilter(text, node);
        },
        parentFilter: function (text, node) {
            var re = new RegExp('^' + Ext.escapeRe(text), 'i');
            return !re.test(node.text);
        },
        leafFilter: function (text, node) {
            var re = new RegExp('^' + Ext.escapeRe(text), 'i');
            return re.test(node.text);
        },
        onRender: function () {
            this.filter = new Ext.tree.TreeFilter(this, {
                clearBlank: true,
                autoClear: true
            });

            Symphony.SearchableTree.superclass.onRender.apply(this, arguments);
        }
    });

    Ext.reg('symphony.searchabletree', 'Symphony.SearchableTree');

    Symphony.TableTree = Ext.extend(Symphony.SearchableTree, {
        initComponent: function () {
            var proxy = new Ext.data.HttpProxy({
                method: 'GET',
                url: this.url
            });

            if (!this.recordDef) {
                var names = [];
                for (var i = 0; i < this.colModel.config.length; i++) {
                    var dataIndex = this.colModel.config[i].dataIndex;
                    if (dataIndex) {
                        names.push({ name: dataIndex });
                    }
                }
                this.recordDef = Ext.data.Record.create(names);
            }

            var store = new Ext.data.Store({
                remoteSort: true,
                proxy: proxy,
                reader: new Ext.data.JsonReader({
                    idProperty: this.idProperty || null,
                    root: 'data'
                }, this.recordDef)
            });

            var tree = this;


            // when the store loads, build the tree nodes
            store.on('load', function () {
                if (store.working) { return; }

                store.working = true;

                // get the selected node, and store its id
                var node = tree.getSelectionModel().getSelectedNode();
                var selectedId = 0;
                if (node) {
                    selectedId = node.attributes.value;
                }

                // kill all existing nodes
                tree.root.removeAll(true);

                var addNode = function (record) {
                    // for this record, find out it's parent id
                    var parentId = record.get(tree.parentColumn);
                    // for this record, find the parent node
                    var parentNode = tree.root.findChild('value', parentId, true) || tree.root;
                    // create a new node under that child
                    var tn = new Ext.tree.TreeNode({
                        expanded: true,
                        value: record.get(tree.idColumn),
                        parentId: parentId,
                        text: record.get(tree.displayColumn),
                        checked: tree.checkboxes ? tree.isChecked(record) : null,
                        record: record,
                        iconCls: tree.iconCls
                    });
                    parentNode.suspendEvents();
                    parentNode.appendChild(tn);
                    parentNode.resumeEvents();
                };

                // make sure we're sorted by parent then display name before we start
                tree.root.beginUpdate();
                store.each(function () {
                    addNode(this);
                });
                tree.root.endUpdate();

                tree.disableSubnodes();

                if (selectedId) {
                    node = tree.root.findChildBy(function (n) {
                        return n.attributes.value == selectedId;
                    }, tree, true);
                    if (node) {
                        tree.getSelectionModel().select(node);
                    } else {
                        // there was a node selected...but we couldn't find it after a refresh, so nothing's selected anymore
                        tree.fireEvent('selectioncleared');
                    }
                }
                tree.root.expand();

                store.working = false;
            });

            Ext.apply(this, {
                data: null,
                store: store
            });
            Symphony.TableTree.superclass.initComponent.apply(this, arguments);
        },
        onRender: function () {
            Symphony.TableTree.superclass.onRender.apply(this, arguments);

            // load the data 1ms later to give the load mask time to show
            window.setTimeout(function () {
                this.loadMask = new Ext.LoadMask(this.bwrap);

                this.store.on('beforeload', Ext.bind(this.loadMask.show, this.loadMask));
                this.store.on('load', Ext.bind(this.loadMask.hide, this.loadMask));
                this.store.on('exception', Ext.bind(this.loadMask.hide, this.loadMask));

                if (!this.deferLoad) {
                    this.store.load();
                }
            } .createDelegate(this), 1);
        },
        refresh: function () {
            this.store.load();
        }
    });

    Symphony.SpellCheckArea = Ext.extend(Ext.form.TextArea, {
        initComponent: function () {
            Symphony.SpellCheckArea.superclass.initComponent.apply(this, arguments);
            return;
            // for now, ignore; causes issues with paste
            if (!spellChecker) {
                spellChecker = new LiveSpellInstance();
                spellChecker.Fields = 'ALL'; //fields;
                spellChecker.ServerModel = 'aspx';
                spellChecker.ActivateAsYouType();
            }
        }
    });
    Ext.reg('symphony.spellcheckarea', 'Symphony.SpellCheckArea');

    Symphony.SpellCheckField = Ext.extend(Ext.form.TextField, {
        //height: 20,
        initComponent: function () {
            Symphony.SpellCheckField.superclass.initComponent.apply(this, arguments);
        }
    });
    Ext.reg('symphony.spellcheckfield', 'Symphony.SpellCheckField');

    // Very simple combobox to act like html select box
    Symphony.DropDownBox = Ext.extend(Ext.form.ComboBox, {
        allowBlank: false,
        editable: false,
        triggerAction: 'all',
        typeAhead: false,
        initComponent: function () {
            //            Ext.apply(this, { 
            //            });
            Symphony.DropDownBox.superclass.initComponent.apply(this, arguments);
//        },
//        setValue : function(value){
//            Symphony.DropDownBox.superclass.setValue.call(this, value);
//            if (this.)
//            return this;
        }
    });
    Ext.reg('symphony.dropdownbox', 'Symphony.DropDownBox');


    Symphony.Ajax = {
        request: function (options) {
            options = Ext.apply({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            }, options || {});

            var defaultFailure = true;
            if (options.failure) {
                defaultFailure = false;
            }

            options.failure = options.failure || function (args) {
                var msg = 'The request failed.';
                if (args.status != 200) {
                    msg += ' The error was: ' + args.status + ': ' + args.statusText;
                }
                Ext.Msg.alert('Error', msg);
            };

            var success = options.success || function () { };
            options.success = function (args) {
                var result = Ext.decode(args.responseText);
                if (!result.success) {
                    if (!defaultFailure) {
                        options.failure(result);
                    } else {
                        Ext.Msg.alert('Error', result.error);
                    }
                } else {
                    success(result);
                }
            };
            Ext.Ajax.request(options);
        }
    };

    Ext.override(Ext.form.DateField, {
        getValue: function (overrideDateOnly) {
            var date = this.parseDate(Ext.form.DateField.superclass.getValue.call(this)) || "";
            if (date && this.dateOnly && !overrideDateOnly) {
                // TODO: Make this a util function
                // subtract the timezone offset from the date to effectively zero out the time once it's converted to UTC
                date = date.add(Date.MINUTE, date.getTimezoneOffset());
            }
            return date;

        },
        setValue: function (date) {
            if (date && this.dateOnly) {
                date = date.add(Date.MINUTE, date.getTimezoneOffset());
            }
            return Ext.form.DateField.superclass.setValue.call(this, this.formatDate(this.parseDate(date)));
        }
    });

    // fix for checkboxes not rendering under certain conditions in IE
    Ext.override(Ext.form.Checkbox, {
        onRender: function (ct, position) {
            Ext.form.Checkbox.superclass.onRender.call(this, ct, position);
            if (this.inputValue !== undefined) {
                this.el.dom.value = this.inputValue;
            }
            this.wrap = this.el.wrap({ cls: 'x-form-check-wrap' });
            if (this.boxLabel) {
                this.wrap.createChild({ tag: 'label', htmlFor: this.el.id, cls: 'x-form-cb-label', html: this.boxLabel });
            }
            if (this.checked) {
                this.setValue(true);
            } else {
                this.checked = this.el.dom.checked;
            }
            if (Ext.isIE && !Ext.isStrict) {
                this.wrap.repaint();
            }
            this.resizeEl = this.positionEl = this.wrap;
            if (this.afterText) {
                var container = this.el.parent().dom;
                var span = document.createElement('span');
                span.innerHTML = this.afterText;
                container.appendChild(span);
            }
        }
    });

    Symphony.parseQueryString = function (queryString) {
        var parts = {};
        var pairs = queryString.split('&');
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split('=');
            parts[pair[0]] = pair[1];
        }
        return parts;
    }

    Ext.override(Ext.ux.form.SuperBoxSelect, {
        isSuperBoxSelect: true
    });

})();