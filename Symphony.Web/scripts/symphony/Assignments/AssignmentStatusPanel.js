﻿Symphony.Assignments.AssignmentStatusPanel = Ext.define('assignments.assignmentstatuspanel', {
    alias: 'widget.assignments.assignmentstatuspanel',
    extend: 'Ext.Panel',
    userId: null,
    courseId: null,
    trainingProgramId: null,
    template: new Ext.Template([
            '<div class="assignmentStatus">',
                '<div class="total">',
                    '<h1>Total Assignments</h1>',
                    '<p>{total}</p>',
                '</div>',
                '<div class="passed">',
                    '<h1>Passed</h1>',
                    '<p>{passed}</p>',
                '</div>',
                '<div class="failed"> ',
                    '<h1>Failed</h1>',
                    '<p>{failed}</p>',
                '</div>',
                '<div class="unmarked">',
                    '<h1>Unmarked</h1>',
                    '<p>{unmarked}</p>',
                '</div>',
                '<div class="unattempted">',
                    '<h1>Unattempted</h1>',
                    '<p>{unattempted}</p>',
                '</div>',
            '</div>'
    ]),
    initComponent: function () {
        var me = this;
        
        me.assignmentStatus = {
            total: '',
            passed: '',
            failed: '',
            unmarked: '',
            attempted: '',
            unattempted: ''
        };

        Ext.apply(this, {
            html: me.template.apply(me.assignmentStatus)         
        });

        this.callParent(arguments);
    },
    updateStatus: function () {
        var url = '/services/assignment.svc/assignment/status/{0}/{1}/{2}/'.format(this.trainingProgramId, this.courseId, this.userId);
        var me = this;

        var loadMask = new Ext.LoadMask(me.getEl(), { msg: "Please wait..." });
        loadMask.show();

        Symphony.Ajax.request({
            method: 'get',
            url: url,
            success: function (result) {
                me.assignmentStatus.total = result.data.assignmentsInCourse;
                me.assignmentStatus.passed = result.data.assignmentsCompleted;
                me.assignmentStatus.failed = result.data.assignmentsMarked - result.data.assignmentsCompleted;
                me.assignmentStatus.attempted = result.data.assignmentsAttempted;
                me.assignmentStatus.unmarked = result.data.assignmentsAttempted - result.data.assignmentsMarked;
                me.assignmentStatus.unattempted = result.data.assignmentsInCourse - result.data.assignmentsAttempted;

                me.update(me.template.apply(me.assignmentStatus));

                loadMask.hide();
            }
        });
    }
});



