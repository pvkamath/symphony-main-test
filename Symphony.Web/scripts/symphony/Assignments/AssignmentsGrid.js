﻿Symphony.Assignments.AssignmentsGrid = Ext.define('assignments.assignmentsgrid', {
    alias: 'widget.assignments.assignmentsgrid',
    extend: 'symphony.searchablegrid',
    courseRecord: null,
    initComponent: function () {
        var me = this;
        var url = '/services/assignment.svc/assignments/' + me.trainingProgramId + '/' + me.courseId + '/' + me.userId;
        
        var proxy = new Ext.data.HttpProxy({
            method: 'GET',
            url: url,
            reader: new Ext.data.JsonReader({
                idProperty: this.idProperty || null,
                totalProperty: 'totalSize',
                root: 'data'
            })
        });

        var store = new Ext.data.Store({
            simpleSortMode: true,
            proxy: proxy,
            baseParams: { limit: 20 },
            model: 'assignment',
            groupField: 'groupId',
            sorters: [{ field: 'groupId', direction: "ASC" }]
        });
        var columns;

        if (_ui(this)) {
            columns = [
            { width: 20, header: '', dataIndex: 'displayAssignmentStatus', renderer: Symphony.Renderer.displayIconRenderer },
            { header: 'Assignment', dataIndex: 'name', width: 100, align: 'left', flex: 1, getSortParam: function () { return 'AssignmentSummary.Name' } },
            {
                 header: 'Score', dataIndex: 'score', width: 75, renderer: function (value, meta, record) {
                    return value + '% (' + record.get('numCorrect') + '/' + record.get('totalQuestions') + ')';
                }
            },
            { header: 'Submit Date', dataIndex: 'displaySubmitDate', renderer: Symphony.Renderer.displayDateRenderer, getSortParam: function () { return 'AssignmentSummaryAttempts.SubmittedOn' } }
            ];
            _log("Cleanup - Symphony.Assignments.AssignmentsGrid - only use these columns");
        } else {
            columns = [
            {
                hidden: true, dataIndex: 'courseId', width: 45, groupRenderer: function (value, meta, record, rowIndex, colIndex) {
                    return String.format("<span class='assignment-group'>" +
                        "<span class='assignment-course-name'>Course: {0}</span>" +
                        "<span class='assignment-attempt'>Attempt: {1}</span>" +
                        "<span class='assignment-status'>Status: {2}</span>" +
                    "</span>", record.get('courseName'), record.get('attempt') + 1, record.get('isMarked') ? 'Marked' : 'Unmarked');
                }
            },
            {
                width: 20, header: '', dataIndex: 'isMarked', renderer: function (value, meta, record) {
                    var qTip;
                    var icon;
                    var msg = "";

                    if (record.get("isOldArtisanCourse")) {
                        msg = "<b>This assignment uses an out of date version of this {0}.</b><br/>".format(Symphony.Aliases.course.toLowerCase());
                    }

                    if (record.get('isOld')) {
                        msg += "This assignment has been re-submitted.";
                        icon = Symphony.undoRenderer(msg);
                    } else if (record.get('numQuestions') < record.get('totalQuestions')) {
                        msg += "This asssignment has not been fully completed.";
                        icon = Symphony.errorRenderer(msg)
                    } else if (!value) {
                        msg += "This assignment has not yet been marked.";
                        icon = Symphony.newRenderer(!value, msg)
                    } else {
                        if (record.get('isPassed')) {
                            msg += "This assignment has successfully been completed.";
                        } else {
                            msg += "This assignment has not been completed.";
                        }

                        icon = Symphony.passFailRenderer(record.get('isPassed'), msg);
                    }

                    return icon;
                }
            },
            {
                /*id: 'name1',*/ header: 'Assignment', dataIndex: 'name', width: 100, align: 'left',
                flex: 1, getSortParam: function() { return 'AssignmentSummary.Name'}
            },
            {
                /*id: 'score1',*/ header: 'Score', dataIndex: 'score', width: 75, renderer: function (value, meta, record) {
                    return value + '% (' + record.get('numCorrect') + '/' + record.get('totalQuestions') + ')';
                }
            },
            {
                /*id: 'date1',*/ header: 'Submit Date', dataIndex: 'date', renderer: function (value) {
                    return Symphony.dateTimeRenderer(value);
                },
                getSortParam: function () { return 'AssignmentSummaryAttempts.SubmittedOn' }
            }
        ];
        }

        if (me.courseRecord && me.courseRecord.get('isDisplayAssignmentLaunchLink')) {
            columns = columns.concat({
                /*id: 'launchLink',*/ header: 'Launch', dataIndex: 'isDisplayAssignmentLaunchLink', renderer: function (value, meta, record) {
                    if (!record.get('isOldArtisanCourse')) {
                        return Symphony.Renderer.displayLinkRenderer(Symphony.Assignments.getAssignmentLaunchLink(me.courseRecord));
                    }
                }
            });
        }

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: columns
        });

        Ext.apply(this, {
            colModel: colModel,
            model: 'assignment',
            store: store,
            cls: 'assignment-grid',
            tbar: {
                items: ['<span style="font-style:italic">' +  me.instructions + '</span>']
            },
            features: Ext.create('Ext.grid.feature.Grouping', {
                collapsible: false,
                forceFit: true,
                showGroupName: false,
                enableGroupingMenu: false,
                enableNoGroups: false,
                groupHeaderTpl: [
                    '{children:this.renderHeader}',
                    {
                        renderHeader: function (children) {
                            if (children.length) {
                                var record = children[0];
                                return String.format("<span class='assignment-group'>" +
                                    "<span class='assignment-course-name'>Course: {0}</span>" +
                                    "<span class='assignment-attempt'>Attempt: {1}</span>" +
                                    "<span class='assignment-status'>Status: {2}</span>" +
                                "</span>", record.get('courseName'), record.get('attempt') + 1, record.get('isMarked') ? 'Marked' : 'Unmarked');
                            }
                        }
                    }
                ]
            })
        });
        this.callParent(arguments);

        this.getView().getRowClass = function (record, rowIndex, rp, ds) {
            if (record.get("isOldArtisanCourse")) {
                return "old-artisan-course";
            }
            if (record.get("isOld")) {
                return "old-assignment";
            }
            if (record.get('numQuestions') < record.get('totalQuestions')) {
                return "in-progress";
            }

        }
    }
});
