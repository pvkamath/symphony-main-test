﻿Symphony.Assignments.AssignmentViewPanel = Ext.define('assignments.assignmentviewpanel', {
    alias: 'widget.assignments.assignmentviewpanel',
    extend: 'Ext.Panel',
    courseRecord: null,
    initComponent: function () {
        var me = this;

        var assignmentView = [{
            border: false,
            region: 'center',
            xtype: 'assignments.assignmentsgrid',
            userId: me.userId,
            trainingProgramId: me.trainingProgramId,
            courseId: me.courseId,
            title: me.singleUser ? '' : 'Assignments',
            instructions: me.singleUser ? 'Double click to view your assignment.' : 'Double click to grade assignment.',
            courseRecord: me.courseRecord,
            listeners: {
                itemdblclick: function (grid, record, item, index, e) {
                    me.addPanel(record.get('trainingProgramId'),
                                record.get('courseId'),
                                record.get('sectionId'),
                                record.get('attempt'),
                                me.userId, 
                                record);
                },
                cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                    var assignmentRecord = grid.getStore().getAt(rowIndex);
                    var fieldName = grid.getColumnModel().getDataIndex(columnIndex);

                    if (fieldName == 'isDisplayAssignmentLaunchLink') {
                        var launchLink = Symphony.Assignments.getAssignmentLaunchLink(me.courseRecord, -1, assignmentRecord.get('sectionId'), -1);
                        Symphony.LinkHandlers.click(grid, e, launchLink, me.courseRecord);
                    }
                }
            }
        }]

        if (me.courseId) {
            assignmentView = [{
                border: false,
                region: 'north',
                height: 80,
                xtype: 'assignments.assignmentstatuspanel',
                userId: me.userId,
                courseId: me.courseId,
                trainingProgramId: me.trainingProgramId,
                ref: '../status'
            }].concat(assignmentView);
        }


        Ext.apply(this, {
            border: false,
            enableTabScroll: true,
            layout: 'card',
            activeItem: 0,
            bubbleEvents: ['beforeComponentRemove'],
            listeners: {
                beforeremove: function (container, component) {
                    if (!component.confirmClose) {
                        me.fireEvent('beforeComponentRemove', container, component);
                        return false;
                    }
                    me.layout.setActiveItem(0);
                },
                remove: function (component) {
                    var assignmentGridContainer = me.query('[itemId=assignmentGridContainer]')[0];
                    var assignmentGrid = assignmentGridContainer.query('[xtype=assignments.assignmentsgrid]')[0];

                    me.layout.setActiveItem(assignmentGridContainer);
                    assignmentGrid.refresh();
                },
                render: function () {
                    var assignmentsGrid = me.find('xtype', 'assignments.assignmentsgrid')[0];
                    var assignmentsStatusGrid = me.find('xtype', 'assignments.assignmentstatuspanel')[0];

                    if (assignmentsGrid && assignmentsStatusGrid) {
                        assignmentsGrid.store.on('load', function() {
                            assignmentsStatusGrid.updateStatus();
                        });
                    }
                }
            },
            items: [{
                border: false,
                xtype: 'panel',
                layout: 'border',
                ref: 'main',
                itemId: 'assignmentGridContainer',
                listeners: {
                    activate: function () {
                        if (me.courseId) {
                            me.status.updateStatus();
                        }
                    }
                },
                items: assignmentView
            }]
        });

        this.callParent(arguments);
    },
    addPanel: function (trainingProgramId, courseId, sectionId, attempt, userId, record) {
        var me = this;
        var args = { userId: userId };
        this.fireEvent('beforeadd', args);

        var panel = Ext.create('assignments.assignmentresponsegrid', {
            border: false,
            userId: userId,
            trainingProgramId: trainingProgramId,
            courseId: courseId,
            sectionId: sectionId,
            attempt: attempt,
            title: record.data.courseName + ': ' + record.data.name,
            record: record,
            singleUser: me.singleUser,
            bubbleEvents: ['beforeComponentRemove', 'savedAssignments'],
            courseRecord: me.courseRecord,
            listeners: {
                showAssignments: function (suppressAlert) {
                    if (suppressAlert) {
                        panel.confirmClose = true;
                    }

                    var removed = me.remove(panel);
                }
            }
        });

        me.add(panel);
        me.doLayout();
        panel.show();

        me.layout.setActiveItem(panel.id);

        this.fireEvent('add', args);

    }
});



