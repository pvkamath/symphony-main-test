﻿Symphony.Settings.hidePortal = true;

(function () {
    Ext.onReady(function () {
        
        var courseRecord = Ext.create('course', course);

        Symphony.App = new Ext.Viewport({
            layout: 'fit',
            items: [{
                xtype: 'assignments.assignmentstabpanel',
                trainingProgramId: trainingProgram.id,
                courseId: course.id,
                userId: Symphony.User.id,
                border: false,
                courseRecord: courseRecord,
                listeners: {
                    beforeComponentRemove: function (container, component) {
                        if (!component.confirmClose) {
                            Symphony.Assignments.confirmClose(
                                'Any unsaved changes will be lost. Are you sure you want to close this tab?',
                                function () {
                                    component.confirmClose = true;
                                    container.remove(component);
                                }
                           );
                        } else {
                            component.confirmClose = true;
                            container.remove(component);
                        }
                    }
                }
            }]
        });
    });
})();