﻿Symphony.Assignments.AssignmentsTabPanel = Ext.define('assignments.assignmentstabpanel', {
    alias: 'widget.assignments.assignmentstabpanel',
    extend: 'Ext.Panel',
    courseRecord: null,
    initComponent: function () {
        var me = this,
            items = [],
            centerPanel = {
                border: false,
                region: 'center',
                region: 'center',
                enableTabScroll: true,
                ref: 'mainpanel',
                xtype: 'tabpanel',
                listeners: {
                    afterrender: function () {
                        if (me.userId) {
                            me.addPanel(me.userId, Symphony.User.fullName, true)
                        }

                    },
                    beforeremove: function (container, component) {
                        if (!component.confirmClose) {
                            me.fireEvent('beforeComponentRemove', container, component);
                            return false;
                        }
                    },
                    savedAssignments: function () {
                        if (me.assignmentusersgrid) {
                            me.assignmentusersgrid.refresh();
                        }
                    }
                }
            };

        if (!me.userId) {
            items.push({
                border: false,
                xtype: 'assignments.assignmentsusersgrid',
                trainingProgramId: me.trainingProgramId,
                courseId: me.courseId,
                region: 'west',
                width: 400,
                title: 'Users',
                split: true,
                ref: 'assignmentusersgrid',
                listeners: {
                    rowclick: function (grid, rowIndex, e) {
                        var record = grid.getStore().getAt(rowIndex);
                        me.addPanel(record.get('UserID'), record.get('fullName'), false);
                    }
                }
            });
        } else {
            centerPanel.xtype = 'panel';
            centerPanel.layout = 'card';
        }

        items.push(centerPanel);

        if (me.userId) {
            items[0].xtype = 'panel';
            items[0].layout = 'card';
        }

        Ext.apply(this, {
            layout: 'border',
            items: items
        });

        this.callParent(arguments);
    },
    addPanel: function (userId, name, singleUser) {
        var me = this;

        var found = me.mainpanel.findBy(function (component, container) {
            if (component.userId) {
                if (component.userId == userId) {
                    return true;
                }
            }
            return false;
        });

        if (found.length) {
            me.mainpanel.layout.setActiveItem(found[0].id);
            me.mainpanel.doLayout();
            found[0].show();
        } else {
            var args = { userId: userId };
            this.fireEvent('beforeadd', args);

            var panel = Ext.create('assignments.assignmentviewpanel', {
                userId: userId,
                trainingProgramId: me.trainingProgramId,
                courseId: me.courseId,
                title: singleUser ? '' : name,
                closable: me.userId ? false : true,
                singleUser: singleUser,
                courseRecord: me.courseRecord
            });
            me.mainpanel.add(panel);
            me.mainpanel.doLayout();
            panel.show();

            me.mainpanel.layout.setActiveItem(panel.id);

            this.fireEvent('add', args);
        }
    },
    hasAssignmentsOpen: function () {
        return this.mainpanel.items.length > 0;
    }
});



