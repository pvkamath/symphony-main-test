﻿Symphony.Assignments.AssignmentResponseGrid = Ext.define('assignments.assignmentresponsegrid', {
    alias: 'widget.assignments.assignmentresponsegrid',
    extend: 'symphony.simplegrid',
    initComponent: function () {
        var me = this;

        me.url = '/services/assignment.svc/assignment/responses/{0}/{1}/{2}/{3}/{4}'.format(
                me.trainingProgramId, me.courseId, me.sectionId, me.attempt, me.userId
            );

        me.responseTogglePanels = {};

        var columns = [
            {
                /*id: 'question',*/ flex: 1, sortable: false, header: 'Assignment Responses', dataIndex: 'question', width: 100, align: 'left', renderer: function (value, meta, record) {
                    var id = Ext.id();
                    var responseId = Ext.id();
                    var instructorFeedbackId = Ext.id();

                    var responseClass = 'not-marked';

                    if (record.get('responseStatus') == Symphony.AssignmentResponseStatus.incorrect) {
                        responseClass = 'incorrect';
                    } else if (record.get('responseStatus') == Symphony.AssignmentResponseStatus.correct) {
                        responseClass = 'correct';
                    }

                    var template = '<div id="{3}" class="assignment response ' + (me.singleUser ? 'single-user' : '') + ' {4}">' +
                                        '<div id="{2}"></div>' +
                                        '<h3>Assignment:</h3>{7}' +
                                        '<div class="question reset">{0}</div>' +
                                        '<h3 class="user">' + (me.singleUser ? 'Your' : 'Student') + ' Response: </h3>' +
                                        '<p class="userResponse">{1}</p>' +
                                        '<h3>Instructor Feedback:</h3>' +
                                        '<div class="instructorfeedback" id="{6}"></div>' +
                                        '<h3 class="correctResponseHeader">Correct Response:</h3>' +
                                        '<p class="correctResponse">{5}</p>' +
                                   '</div>';

                        setTimeout(function () {
                            var toggleGroup = id + "-status";

                            var instructorFeedbackForm = new Ext.form.FormPanel({
                                renderTo: instructorFeedbackId,
                                border: false,
                                hideLabels: true,
                                labelWidth: 0,
                                items: [{
                                    xtype: 'textarea',
                                    name: 'instructorFeedback',
                                    value: record.get('instructorFeedback'),
                                    allowBlank: true,
                                    readOnly: me.singleUser,
                                    anchor: '100%',
                                    grow: true,
                                    preventScrollbars: true
                                }]
                            });
                            
                            instructorFeedbackForm.find('xtype', 'textarea')[0].setValue(record.get('instructorFeedback'));

                            var panel = new Ext.Toolbar({
                                renderTo: id,
                                responseId: responseId,
                                border: false,
                                width: 200,
                                height: 80,
                                cls: 'response-status',
                                items: [{
                                    xtype: 'button',
                                    name: 'btn-correct',
                                    iconCls: 'x-button-finish',
                                    text: 'Correct',
                                    enableToggle: true,
                                    border: true,
                                    toggleGroup: toggleGroup,
                                    width: 90,
                                    handler: function (btn) {
                                        panel.setStatus(Symphony.AssignmentResponseStatus.correct);
                                    }
                                }, {
                                    xtype: 'button',
                                    name: 'btn-incorrect',
                                    iconCls: 'x-button-incorrect',
                                    text: 'Incorrect',
                                    enableToggle: true,
                                    border: true,
                                    toggleGroup: toggleGroup,
                                    width: 90,
                                    handler: function (btn) {
                                        panel.setStatus(Symphony.AssignmentResponseStatus.incorrect);
                                    }
                                }],
                                listeners: {
                                    render: function (p) {
                                        var responseCell = Ext.get(responseId);

                                        if (record.get('responseStatus') == Symphony.AssignmentResponseStatus.correct) {
                                            p.find('name', 'btn-correct')[0].toggle(true);

                                            responseCell.addCls("correct");
                                            responseCell.removeCls("incorrect");
                                            responseCell.removeCls("not-marked");
                                            
                                            
                                            if(me.singleUser){
                                                p.find('name', 'btn-correct')[0].disable();
                                                p.find('name', 'btn-incorrect')[0].hide();
                                            }

                                        } else if (record.get('responseStatus') == Symphony.AssignmentResponseStatus.incorrect) {
                                            p.find('name', 'btn-incorrect')[0].toggle(true);

                                            responseCell.removeCls("correct");
                                            responseCell.addCls("incorrect");
                                            responseCell.removeCls("not-marked");

                                            if (me.singleUser) {
                                                p.find('name', 'btn-incorrect')[0].disable();
                                                p.find('name', 'btn-correct')[0].hide();
                                            }
                                        } else {
                                            p.find('name', 'btn-incorrect')[0].toggle(true);
                                            p.find('name', 'btn-incorrect')[0].toggle(false);

                                            if (me.singleUser) {
                                                p.find('name', 'btn-correct')[0].hide();
                                                p.find('name', 'btn-incorrect')[0].hide();
                                            }

                                        }
                                    }
                                },
                                getInstructorFeedback: function() {
                                    return instructorFeedbackForm.find('xtype', 'textarea')[0].getValue();
                                },
                                setStatus: function (status) {
                                    
                                    var btnName = (status === Symphony.AssignmentResponseStatus.correct ?
                                                        'btn-correct' : 'btn-incorrect');

                                    var cls = (status === Symphony.AssignmentResponseStatus.correct ?
                                                        'correct' : 'incorrect');

                                    var btn = panel.find('name', btnName)[0];

                                    if (!btn.pressed) {
                                        btn.toggle(true);
                                    }
                                    
                                    var responseCell = Ext.get(panel.responseId);

                                    responseCell.removeCls('correct');
                                    responseCell.removeCls('incorrect');
                                    responseCell.removeCls('not-marked');
                                    responseCell.addCls(cls);

                                    var feedback = instructorFeedbackForm.find('xtype', 'textarea')[0].getValue();
                                    
                                    if (feedback) { // Otherwise we loose this when updating the status
                                        record.data.getInstructorFeedback = feedback;
                                    }
                                    record.data.responseStatus = status;
                                }
                            });

                            me.responseTogglePanels['responseTogglePanel' + record.get('id')] = panel;

                        }, 10);
                    
                    var resp = record.get('response');
                    try{
                        resp = decodeURI(record.get('response'));
                    }catch(e){
                    }

                    var courseLaunchLinkHtml = "";
                    if (me.courseRecord && me.courseRecord.get('isDisplayAssignmentLaunchLink') && !me.record.get('isOldArtisanCourse')) {
                        courseLaunchLinkHtml = Symphony.Renderer.displayLinkRenderer(Symphony.Assignments.getAssignmentLaunchLink(me.courseRecord));
                    }

                    return String.format(template,
                                        record.get('question'),
                                        resp.replace(/\r?\n/g, '<br />'),
                                        id,
                                        responseId,
                                        responseClass,
                                        (me.singleUser && record.get('responseStatus') != Symphony.AssignmentResponseStatus.correct ? '' : record.get('correctResponse').replace(/\r?\n/g, '<br />')),
                                        instructorFeedbackId,
                                        courseLaunchLinkHtml);

                }}
        ];

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: columns
        });

        Ext.apply(this, {
            colModel: colModel,
            model: 'assignmentResponse',
            disableSelection: true,
            trackMouseOver: false,
            cls: 'assignment-response-grid',
            bubbleEvents: ['savedAssignments'],
            listeners: {
                resize: function (grid, adjWidth, adjHeight, rawWidth, rawHeight) {
                    var element = grid.getEl();

                    var textareas = element.select('textarea');
                    
                    var width = adjWidth * 0.95;
                    
                    textareas.setWidth(width);

                },
                cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                    if (e.target.className.indexOf("method_launch") >= 0) {
                        var launchLink = Symphony.Assignments.getAssignmentLaunchLink(me.courseRecord, -1, -1, record.get('pageId'));
                        Symphony.LinkHandlers.click(grid, e, launchLink, me.courseRecord);
                    }
                }
            },
            tbar: {
                items: [{
                    xtype: 'button',
                    iconCls: me.singleUser ? 'x-button-previous' : 'x-button-save',
                    text: me.singleUser ? 'Back to Assignments' : 'Save and return to Assignments',
                    listeners: {
                        click: function (btn, e) {
                            if (!me.singleUser) {
                                me.save(true);
                            } else {
                                me.fireEvent('showAssignments', true); // Single user tab, so we can't save anything anyway, don't show alert. 
                            }
                        }
                    }
                }].concat(!me.singleUser ? 
                [{
                    xtype: 'button',
                    iconCls: 'x-button-save-disk',
                    text: 'Save',
                    listeners: {
                        click: function (btn, e) {
                            me.save(false);
                        }
                    }
                }, {
                    xtype: 'button',
                    iconCls: 'x-button-finish',
                    text: 'Mark all correct',
                    listeners: {
                        click: function (btn, e) {
                            me.setBulkStatus(Symphony.AssignmentResponseStatus.correct);
                        }
                    }
                }, {
                    xtype: 'button',
                    iconCls: 'x-button-incorrect',
                    text: 'Mark all incorrect',
                    listeners: {
                        click: function (btn, e) {
                            me.setBulkStatus(Symphony.AssignmentResponseStatus.incorrect);
                        }
                    }
                },
                '->',
                {
                    xtype: 'button',
                    iconCls: 'x-button-cancel',
                    text: 'Cancel',
                    listeners: {
                        click: function (btn, e) {
                            me.fireEvent('showAssignments');
                        }
                    }
                }] : [])
            }
        });

        this.callParent(arguments);
    },
    save: function (isReturn) {
        var me = this;
        var records = me.store.getRange();
        var assignmentData = [];

        Ext.Msg.wait('Please wait while the assignment is saved...', 'Please wait...');

        for (var i = 0; i < records.length; i++) {
            records[i].set('instructorFeedback', me.getResponseTogglePanel(records[i].get('id')).getInstructorFeedback());
            assignmentData.push(records[i].data);
        }

        Symphony.Ajax.request({
            method: 'POST',
            url: me.url,
            jsonData: assignmentData,
            success: function (args) {
                Ext.Msg.hide();
                me.fireEvent('savedAssignments');
                if (isReturn) {
                    me.fireEvent('showAssignments', true); // Confirm the panel can close due to just saving.
                } else {
                    me.refresh();
                }
            }
        });
    },
    getResponseTogglePanel: function(id) {
        return this.responseTogglePanels['responseTogglePanel' + id];
    },
    setResponseTogglePanel: function(id, panel) {
        this.responseTogglePanels['responseTogglePanel' + id];
    },
    setStatus: function (id, status) {
        var panel = this.getResponseTogglePanel(id);

        if (panel && panel.setStatus) {
            panel.setStatus(status);
        }
    },
    setBulkStatus: function (status) {
        var me = this;

        me.store.each(function (record) {
            me.setStatus(record.get('id'), status);
        });
    }
});
