﻿Symphony.Assignments.confirmClose = function (msg, callback) {
    Ext.Msg.show({
        title: 'Are you sure?',
        msg: msg,
        fn: function (btn) {
            if (btn == 'yes') {
                if (typeof (callback) == 'function') {
                    callback();
                }
            }
        },
        cls: 'bringToFront',
        buttons: Ext.Msg.YESNO
    });
}

Symphony.Assignments.getAssignmentLaunchLink = function (courseRecord, scoId, loId, pageId) {
    var displayLink = courseRecord.get('displayCourseLink');

    if (scoId || loId || pageId) {
        displayLink.bookmark = '/assign/sco/{0}/lo/{1}/page/{2}'.format(scoId, loId, pageId)
    }

    displayLink.title = courseRecord.get('displayAssignmentLaunchText').messages[0];

    return displayLink;
}

Symphony.Assignments.AssignmentsWindow = Ext.define('assignments.assignmentswindow', {


    alias: 'widget.assignments.assignmentswindow',


    extend: 'Ext.Window',
    userId: 0,
    courseRecord: null,
    initComponent: function () {
        var me = this;

        me.width = me.width || 950;
        me.height = me.height || 650;

        Ext.apply(this, {
            title: 'Assignments',
            stateId: 'assignmentswindow',
            stateful: true,
            width: me.width || 850,
            height: me.height || 650,
            layout: 'fit',
            modal: true,
            //minimizable: true,
            listeners: {
                minimize: function (window, opts) {
                    me.width = window.getWidth();
                    window.collapse(false);
                    window.setWidth(200);
                    window.alignTo(Ext.getBody(), 'bl-bl');
                },
                beforeclose: function () {
                    assignmentTabPanel = me.find('xtype', 'assignments.assignmentstabpanel')[0];
                    if (assignmentTabPanel.hasAssignmentsOpen()) {
                        if (!me.confirmedClose && me.userId == 0) {
                            Symphony.Assignments.confirmClose(
                                'Any unsaved changes will be lost. Are you sure you want to close this window?',
                                function () {
                                    me.confirmedClose = true;
                                    me.close();
                                });
                            return false;
                        }
                    }
                }
            },
            tools: [{
                id: 'restore',
                handler: function (evt, toolEl, owner, tool) {
                    if (me.collapsed) {
                        me.expand(false);
                        me.setWidth(me.width);
                        me.center();
                    }
                }
            }],
            items: [{
                xtype: 'assignments.assignmentstabpanel',
                trainingProgramId: me.trainingProgramId,
                courseId: me.courseId,
                userId: me.userId,
                border: false,
                courseRecord: me.courseRecord,
                listeners: {
                    beforeComponentRemove: function (container, component) {
                        if (!component.confirmClose) {
                            Symphony.Assignments.confirmClose(
                                'Any unsaved changes will be lost. Are you sure you want to close this tab?',
                                function () {
                                    component.confirmClose = true;
                                    container.remove(component);
                                }
                           );
                            return false;
                        } else {
                            component.confirmClose = true;
                            return true;
                        }
                        return true;
                    }
                }
            }]
        });

        this.callParent(arguments);
    }
});



