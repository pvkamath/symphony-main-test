﻿Symphony.Assignments.AssignmentsUsersGrid = Ext.define('assignments.assignmentsusersgrid', {
    alias: 'widget.assignments.assignmentsusersgrid',
    extend: 'symphony.searchablegrid',
    initComponent: function () {
        var me = this;
        var url = '/services/assignment.svc/assignments/users/' + me.trainingProgramId + '/' + me.courseId;


        var proxy = new Ext.data.HttpProxy({
            method: 'GET',
            url: url,
            reader: new Ext.data.JsonReader({
                idProperty: this.idProperty || null,
                totalProperty: 'totalSize',
                root: 'data'
            })
        });

        var store = new Ext.data.Store({
            simpleSort: true,
            proxy: proxy,
            baseParams: { limit: 20 },
            model: 'userAssignment',
            remoteSort: true,
            groupField: 'ClassID'
        });


        var columns = [
            {
                /*id: 'isNew2',*/ header: '', dataIndex: 'IsNew', width: 20, align: 'center', renderer: function (value, meta, record) {
                    return Symphony.newRenderer(value, value ? 'This student has recently submitted assignments to mark.' : '');
                }
            },
            {
                /*id: 'name2',*/ header: 'Name', dataIndex: 'fullName', width: 100, align: 'left',
                flex: 1
            },
            {
                /*id: 'maxSubmitDate2',*/ header: 'Last Submitted', dataIndex: 'MaxSubmitDate', width: 150, align: 'left', renderer: function (value, meta, record) {
                    return Symphony.dateTimeRenderer(value);
                }},
            {
                /*id: 'marked3',*/ header: 'Marked', dataIndex: 'AssignmentsMarked', width: 60, align: 'center', renderer: function (value, meta, record) {
                   
                    return Symphony.checkRenderer(value == record.get('AssignmentsInCourse') && !record.get('IsNew'), (value == record.get('AssignmentsInCourse') && !record.get('IsNew')) ? 'All assignments have been marked for this student.' : 'There are assignments waiting to be marked, or the student has not yet completed all assignments.');
            }}
        ];

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: columns
        });

        Ext.apply(this, {
            idProperty: 'userId',
            colModel: colModel,
            stateId: 'assignmentsusersgrid',
            stateful: true,
            model: 'userAssignment',
            store: store,
            //proxy: proxy,
            features: Ext.create('Ext.grid.feature.Grouping', {
                forceFit: true,
                showGroupName: false,
                groupHeaderTpl: [
                    '{children:this.renderHeader}',
                    {
                        renderHeader: function (children) {
                            if (children.length) {
                                var record = children[0];
                                if (record.get('ClassID') > 0) {
                                    var longDate = Symphony.parseDate(record.get('ClassStartTime')).formatSymphony('D, F j, Y g:i a');
                                    var shortDate = Symphony.parseDate(record.get('ClassStartTime')).formatSymphony('m/d/Y');
                                    var title = String.format('{0} - {1}', record.get('ClassName'), shortDate)
                                    var qTip = Symphony.qtipRenderer(title, longDate);

                                    return String.format('<span data-classid="{0}">{1}</span>',
                                        record.get('ClassID'),
                                        qTip);
                                }

                                return 'No Session';
                            }
                        }
                    }
                ]
            }),
            url: url
        });
        this.callParent(arguments);
    }
});
