﻿Symphony.Students.ClassroomTranscriptPanel = Ext.define('students.classroomtranscriptpanel', {
    alias: 'widget.students.classroomtranscriptpanel',
    extend: 'Ext.Panel',
    trainingProgram: null,
    trainingProgramId: 0,
    launchCourseId: 0,
    launchCallback: null,
    isLaunchTest: false,
    klass: null,
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            height: 50,
            frame: true,
            items: [{
                height: 50,
                flex: 0.83,
                name: 'classroomTranscriptPanel',
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    defaultMargins: {
                        top: 0,
                        right: 5,
                        left: 5,
                        bottom: 0
                    }
                },

                items: [{
                    xtype: 'panel',
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        defaultMargins: {
                            top: 5,
                            right: 5,
                            left: 5,
                            bottom: 0
                        }
                    },
                    height: 20,
                    flex: 1,
                    defaults: {
                        border: false,
                        xtype: 'label',
                        flex: 0.25,
                        style: 'text-align: center; font-weight: bold;'
                    },
                    items: [{
                        text: 'Status:'
                    }, {
                        text: 'Score:'
                    }, {
                        text: 'Attended:'
                    }, {
                        text: 'Passed:'
                    }]
                }, {
                    xtype: 'panel',
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        defaultMargins: {
                            top: 0,
                            right: 5,
                            left: 5,
                            bottom: 5
                        }
                    },
                    height: 20,
                    name: 'transcriptPanel',
                    flex: 1,
                    defaults: {
                        border: false,
                        xtype: 'label',
                        flex: 0.25,
                        style: 'text-align: center'
                    },
                    items: [{
                        text: '',
                        name: 'completeOrIncomplete'
                    }, {
                        text: '',
                        name: 'score'
                    }, {
                        text: '',
                        name: 'attended'
                    }, {
                        text: '',
                        name: 'passOrFail'
                    }]
                }]
            }, {
                xtype: 'panel',
                height: 40,
                flex: 0.17,
                layout: {
                    type: 'hbox',
                    align: 'center',
                    pack: 'center'
                },
                padding: '5px 5px 5px 5px',
                items: [{
                    xtype: 'button',
                    iconCls: 'x-button-play',
                    text: 'Launch ' + Symphony.Aliases.course,
                    name: 'launchCourseBtn',
                    height: 27,
                    disabled: true,
                    handler: function () {
                        if (me.launchCourseId > 0) {
                            if (typeof (me.launchCallback) === 'function') {
                                Symphony.Portal.onCourseCompletion(me.launchCallback);
                            }

                            if (!me.isLaunchTest) {
                                // nothing special needed for surveys
                                Symphony.Portal.launchOnlineCourse(me.launchCourseId, me.trainingProgramId);
                            } else {
                                var course = null;
                                if (me.trainingProgram) {
                                    var tpcourses = me.trainingProgram.requiredCourses.concat(
                                            me.trainingProgram.electiveCourses,
                                            me.trainingProgram.optionalCourses,
                                            me.trainingProgram.finalAssessments
                                        );

                                        
                                    for (var i = 0; i < tpcourses.length; i++) {
                                        if (tpcourses[i].id == me.klass.courseId) {
                                            course = tpcourses[i];
                                            break;
                                        }
                                    }

                                }
                                var affidavitId = course ? course.affidavitId : 0;
                                var proctorRequiredIndicator = course ? course.proctorRequiredIndicator : me.klass.proctorRequiredIndicator;
                                var isCourseWorkAllowed = course ? course.isCourseWorkAllowed : true;
                                var isUserValidationAllowed = course ? course.isUserValidationAllowed : me.klass.isUserValidationAllowed;

                                Symphony.Portal.launchOnlineCourse(me.launchCourseId, me.trainingProgramId, affidavitId, proctorRequiredIndicator, isCourseWorkAllowed, isUserValidationAllowed);
                            }
                        }
                    }
                }]
            }]
        });
        this.callParent(arguments);
    },
    setCourse: function (courseId, trainingProgramId, transcriptRecord, course, trainingProgram, launchCallback) {
        this.launchCourseId = 0;
        var launchButton = this.find('name', 'launchCourseBtn')[0];
        if (transcriptRecord.hasStarted && transcriptRecord.hasEnded) {
            if (!transcriptRecord.passed) {
                if (transcriptRecord.onlineTestId > 0) {
                    if (transcriptRecord.attendedIndicator) { // only show online test if they have attended the class
                        this.launchCourseId = transcriptRecord.onlineTestId;
                        this.isLaunchTest = true;
                        launchButton.setText("Take Test");
                    }
                }
                else if (transcriptRecord.surveyId > 0 && !transcriptRecord.surveyTaken) {
                    this.launchCourseId = transcriptRecord.surveyId;
                    launchButton.setText("Take Survey");
                }
            } else {
                if (transcriptRecord.surveyId > 0 && !transcriptRecord.surveyTaken) {
                    this.launchCourseId = transcriptRecord.surveyId;
                    launchButton.setText("Take Survey");
                }
            }
        }
        
        if (this.launchCourseId === 0) {
            launchButton.setVisible(false);
        } else {
            launchButton.setDisabled(false);
        }

        this.trainingProgramId = trainingProgramId;
        this.launchCallback = launchCallback;
        this.trainingProgram = trainingProgram;
        this.klass = course;

    }
});
