﻿Symphony.Students.EnrolledClassesGrid = Ext.define('students.enrolledclassesgrid', {
    alias: 'widget.students.enrolledclassesgrid',
    extend: 'symphony.searchablegrid',
    readOnly: false,
    initComponent: function () {
        var me = this;
        var url = '/services/classroom.svc/enrolledClasses/';

        var proxy = new Ext.data.HttpProxy({
            method: 'GET',
            url: url,
            reader: new Ext.data.JsonReader({
                idProperty: 'none',
                totalProperty: 'totalSize',
                root: 'data'
            })
        });

        var store = new Ext.data.Store({
            simpleSort: true,
            proxy: proxy,
            baseParams: { limit: 20 },
            model: 'instructedClass',
            sorters: { field: 'trainingProgramId', direction: "ASC" },
            groupField: 'trainingProgramId'
        });

        var columns = [
            { header: 'Locked', dataIndex: 'lockedIndicator', renderer: Symphony.checkRenderer, width: 45, fixed: true },
            { header: ' ', dataIndex: 'courseTypeId', width: 30, fixed: true, align: 'center', renderer: Symphony.Portal.courseTypeRenderer }, // course type icons
            {
                /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 140, align: 'left',
                flex: 1
            },
            {
                /*id: 'startDate',*/ header: 'Start Date', dataIndex: 'minClassDate', width: 65, align: 'left', renderer: function (value, meta, record) {
                    if (value && Symphony.parseDate(value).getFullYear() != Symphony.defaultYear) {
                        return Symphony.shortDateRenderer(value);
                    } else {
                        return '';
                    }
                }
            },
            { /*id: 'messageBoard',*/ header: '', dataIndex: 'messageBoardId', width: 30, fixed: true, align: 'right', renderer: Symphony.Portal.MessageBoardRenderer }
        ]

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: columns
        });
        
        Ext.apply(this, {
            cls: 'groupedCourseGrid',
            border: false,
            deferLoad: true,
            url: url,
            colModel: colModel,
            model: 'instructedClass',
            store: store,
            minColumnWidth: 30,
            features: Ext.create('Ext.grid.feature.Grouping', {
                forceFit: true,
                showGroupName: false,
                groupHeaderTpl: [
                    '{children:this.renderHeader}',
                    {
                        renderHeader: function (children) {
                            if (children.length) {
                                var record = children[0];
                                if (record.data.trainingProgramName) {
                                    return String.format(
                                        '<span class="instructed-class-group"><div class="icons">{0}</div><div class="header">{1}</div></span>',
                                        Symphony.Portal.MessageBoardRenderer(record.get('groupByTrainingProgramId'), {}, { data: { messageBoardId: record.data.trainingProgramMessageBoardId } }),
                                        Symphony.qtipRenderer(record.data.trainingProgramName)
                                    );
                                } else {
                                    return 'My Classes'
                                }
                            }
                        }
                    }
                ]
            })
            /*view: new Ext.grid.GroupingView({
                forceFit: true,
                showGroupName: false
            })*/
        });
        this.callParent(arguments);
    }
});
