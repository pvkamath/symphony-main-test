﻿Symphony.Students.OnlineTranscriptPanel = Ext.define('students.onlinetranscriptpanel', {
    alias: 'widget.students.onlinetranscriptpanel',
    extend: 'Ext.Panel',
    courseId: 0,
    trainingProgramId: 0,
    trainingProgram: null,
    launchCallback: null,
    launchLinkHtml: null,
    course: null,
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            layout: { type: 'hbox', align: 'stretch' },
            height: 50,
            frame: true,
            items: [{
                name: 'onlineTranscriptPanel',
                height: 50,
                flex: 0.83,
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    defaultMargins: {
                        top: 0,
                        right: 5,
                        left: 5,
                        bottom: 0
                    }
                },

                items: [{
                    xtype: 'panel',
                    height: 20,
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        defaultMargins: {
                            top: 5,
                            right: 5,
                            left: 5,
                            bottom: 0
                        }
                    },
                    defaults: {
                        border: false,
                        xtype: 'label',
                        flex: 0.25,
                        style: 'text-align: center; font-weight: bold;'
                    },
                    items: [{
                        text: 'Attempts:'
                    }, {
                        text: 'Last Attempt Date:'
                    }, {
                        text: 'Time in Course:'
                    }, {
                        text: 'Score:'
                    }, {
                        text: 'Passed:'
                    }]
                }, {
                    xtype: 'panel',
                    height: 20,
                    name: 'transcriptPanel',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        defaultMargins: {
                            top: 0,
                            right: 5,
                            left: 5,
                            bottom: 5
                        }
                    },
                    defaults: {
                        border: false,
                        xtype: 'label',
                        flex: 0.25,
                        style: 'text-align: center'
                    },
                    items: [{
                        text: '',
                        name: 'attemptCount'
                    }, {
                        text: '',
                        name: 'attemptDate'
                    }, {
                        text: '',
                        name: 'attemptTime'
                    }, {
                        text: '',
                        name: 'score'
                    }, {
                        text: '',
                        name: 'passOrFail'
                    }]
                }]
            }, {
                xtype: 'panel',
                height: 40,
                flex: 0.17,
                layout: {
                    type: 'hbox',
                    pack: 'center',
                    align: 'center'
                },
                padding: '5px 5px 5px 5px',
                items: [{
                    xtype: 'button',
                    iconCls: 'x-button-play',
                    text: 'Launch ' + Symphony.Aliases.course,
                    name: 'launchCourseBtn',
                    height: 27,
                    disabled: true,
                    handler: function () {
                        if (me.courseId > 0 && me.trainingProgramId > 0) {
                            if (typeof (me.launchCallback) === 'function') {
                                Symphony.Portal.onCourseCompletion(me.launchCallback);
                            }

                            // Just get the parameters from the lanuchlink html
                            var temp = document.createElement('div');
                            temp.innerHTML = me.launchLinkHtml;
                            var params = temp.firstChild.getAttribute('symphony:parameter');
                            Symphony.Portal.launchOnlineCourse.apply(this, params.split(','));
                        }
                    }
                }]
            }]
        });
        this.callParent(arguments);
    },
    setCourse: function (courseId, trainingProgramId, transcriptRecord, course, trainingProgram, launchCallback) {
        if (courseId > 0) {
            var disableLaunch = false;
            var msg = "";
            var courseRecord = null;

            if (trainingProgramId > 0) {
                var courseSyllabusType = null;
                var courseTypeId = 0;

                var allCourses = trainingProgram.requiredCourses.concat(
                                    trainingProgram.electiveCourses,
                                    trainingProgram.optionalCourses,
                                    trainingProgram.finalAssessments
                                );

                for (var i = 0; i < allCourses.length; i++) {
                    var course = allCourses[i];
                    if (course.id === courseId) {
                        courseSyllabusType = course.syllabusTypeId;
                        courseTypeId = course.courseTypeId;

                        var CourseRecordDefinition = Ext.data.Record.create(Symphony.Definitions.course)
                        courseRecord = new CourseRecordDefinition(course);
                        courseRecord.raw = course;
                        break;
                    }
                }

                switch (courseSyllabusType) {
                    case Symphony.SyllabusType.required:

                        if (trainingProgram.enforceRequiredOrder && Symphony.Portal.hasPreviousRequiredCourse(allCourses, courseId, courseTypeId)) {
                            disableLaunch = true;
                            msg = "This " + Symphony.Aliases.course.toLowerCase() + " will not be available until all previous required " + Symphony.Aliases.courses.toLowerCase() + " are complete.";
                        }

                        if (trainingProgram.enforceCanMoveForwardIndicator && Symphony.Portal.hasPreviousCourseRequiringApproval(allCourses, courseId, courseTypeId)) {
                            disableLaunch = true;
                            msg = "This " + Symphony.Aliases.course.toLowerCase() + " will not be available until you have permission to move foward from a previous " + Symphony.Aliases.course.toLowerCase() + ".";
                        }

                        break;
                    case Symphony.SyllabusType.elective:
                        if (trainingProgram.enforceRequiredOrder && !Symphony.Portal.requiredComplete(trainingProgram)) {
                            disableLaunch = true;
                            msg = "This " + Symphony.Aliases.course.toLowerCase() + " will not be available until all required " + Symphony.Aliases.courses.toLowerCase() + " are complete";
                        }
                        break;
                    case Symphony.SyllabusType.optional:
                        if (trainingProgram.enforceRequiredOrder && (!Symphony.Portal.requiredComplete(trainingProgram) || !Symphony.Portal.electiveComplete(trainingProgram))) {
                            disableLaunch = true;
                            msg = "This " + Symphony.Aliases.course.toLowerCase() + " will not be available until all required " + Symphony.Aliases.courses.toLowerCase() + " are complete and minimum electives are met.";
                        }
                        break;
                    case Symphony.SyllabusType['final']:
                        var disabledMessage = Symphony.Portal.getFinalAssessmentDisabledMessage(trainingProgram);
                        var isEnabled = Symphony.Portal.isFinalEnabled(trainingProgram);

                        if (!isEnabled) {
                            disableLaunch = true;
                            msg = disabledMessage;
                        }
                        break;
                }

            }
            

            
            var unavailableReasons = [];

            var launchLink = Symphony.Portal.courseLinkRenderer("", "", courseRecord, trainingProgram, null, null, unavailableReasons);

            if (unavailableReasons.length == 0 && launchLink.toLowerCase().indexOf('launch') < 0) {
                unavailableReasons.push(launchLink);
            }

            if (!disableLaunch) {
                if (unavailableReasons.length) {
                    msg = unavailableReasons.join('\r\n\r\n');
                    disableLaunch = true;
                }
            }

            if (disableLaunch) {
                Ext.Msg.alert(Symphony.Aliases.course + " Unavailable", msg);
            }

            this.find('name', 'launchCourseBtn')[0].setDisabled(disableLaunch);
            
            this.launchLinkHtml = launchLink;
            this.courseId = courseId;
            this.trainingProgramId = trainingProgramId;
            this.launchCallback = launchCallback;
            this.course = course;
            this.trainingProgram = trainingProgram;
        }
    }
});
