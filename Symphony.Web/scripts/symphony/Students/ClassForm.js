﻿(function () {
    Symphony.Students.ClassForm = Ext.define('students.classform', {
        alias: 'widget.students.classform',
        extend: 'Ext.Panel',
        classId: 0,
        classTypeId: 0,
        trainingProgramId: 0,

        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                border: false,
                defaults: {
                    border: false
                },
                listeners: {
                    beforeadd: function (panel, cmp, i) {
                        var type = cmp.getXType(),
                            hiddenComponents = [];

                        hiddenComponents[Symphony.CourseType.online] = [
                            'classroom.attendancegrid',
                            'classroom.registrationtrees',
                            'classroom.coursedocumentsgrid',
                            'classroom.classschedulegrid',
                            'classroom.classresourcesgrid',
                            'students.classroomtranscriptpanel'
                        ];

                        hiddenComponents[Symphony.CourseType.classroom] = [
                            'instructors.onlinecoursestudentgrid',
                            'instructors.trainingprogramdetailspanel',
                            'students.onlinetranscriptpanel'
                        ];

                        if (hiddenComponents[me.classTypeId].indexOf(type) > -1) {
                            return false;
                        }

                        if (type === 'messageboard.app' && !Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards)) {
                            return false;
                        }

                        return true;
                    }
                },
                items: [{
                    region: 'center',
                    layout: 'fit',
                    ref: 'main',
                    defaults: {
                        border: false
                    },
                    items: [{
                        xtype: 'tabpanel',
                        border: false,
                        activeTab: 0,
                        ref: '../classTabs',
                        bubbleEvents: ['beforeadd'], // because we only want one place to decide which controls should be shown
                        listeners: {
                            beforeadd: function (panel, cmp, i) {
                                if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                    return false;
                                }
                            }
                        },
                        items: [{
                            title: 'General',
                            name: 'general',
                            xtype: 'panel',
                            autoScroll: true,
                            border: false,
                            bodyCssClass: 'x-panel-mc',
                            layout: {
                                type: 'vbox',
                                align: 'stretch',
                                defaultMargins: {
                                    top: 5,
                                    right: 5,
                                    left: 5,
                                    bottom: 5
                                }
                            },
                            padding: 0,
                            margins: {
                                left: 0,
                                right: 0,
                                top: 0,
                                bottom: 0
                            },
                            listeners: {
                                render: function () {
                                    me.load();
                                },
                                beforeadd: function (panel, cmp, i) {
                                    if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                        return false;
                                    }
                                }
                            },
                            tbar: {
                                xtype: 'classroom.classcontactbar',
                                border: false,
                                instructorsGridType: me.classTypeId === Symphony.CourseType.online ? 'courseassignment.leadersgrid' : 'classroom.classinstructorsgrid',
                                listeners: {
                                    afterlayout: function (tbar) {
                                        var removeButtons = tbar.findBy(function (cmp) {
                                            if (cmp.getText && cmp.getText().indexOf('Instructor') < 0) {
                                                return true;
                                            }
                                            return false;
                                        });

                                        for (var i = 0; i < removeButtons.length; i++) {
                                            tbar.remove(removeButtons[i]);
                                        }
                                    },
                                    beforeadd: function (panel, cmp, i) {
                                        if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                            return false;
                                        }
                                    }
                                }
                            },
                            defaults: {
                                border: false,
                                xtype: 'panel',
                                layout: 'fit',
                                flex: 1
                            },
                            bubbleEvents: ['beforeadd'], // Because we only want one place to manage which controls should and shouldn't be shown
                            items: [{
                                xtype: 'students.classroomtranscriptpanel',
                                listeners: {
                                    beforeadd: function (panel, cmp, i) {
                                        if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                            return false;
                                        }
                                    }
                                }
                            }, {
                                xtype: 'students.onlinetranscriptpanel',
                                listeners: {
                                    beforeadd: function (panel, cmp, i) {
                                        if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                            return false;
                                        }
                                    }
                                }
                            }, {
                                xtype: 'panel',
                                layout: {
                                    type: 'hbox',
                                    defaultMargins: {
                                        top: 5,
                                        right: 5,
                                        left: 5,
                                        bottom: 5
                                    },
                                    align: 'stretch'
                                },
                                flex: 0.9,
                                defaults: {
                                    border: false,
                                    xtype: 'panel',
                                    layout: 'fit',
                                    flex: 1
                                },
                                bubbleEvents: ['beforeadd'], // because we only want one place to decide which controls should be shown
                                listeners: {
                                    beforeadd: function (panel, cmp, i) {
                                        if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                            return false;
                                        }
                                    }
                                },
                                items: [{
                                    xtype: me.classTypeId === Symphony.CourseType.online ? 'courseassignment.leadersgrid' : 'classroom.classinstructorsgrid',
                                    name: 'instructorsgrid',
                                    flex: 1,
                                    autoheight: false,
                                    allowDelete: false,
                                    title: me.classTypeId === Symphony.CourseType.online ? 'Leaders' : 'Instructors',
                                    showVideoChatColumn: true,
                                    listeners: {
                                        added: function (grid, component, index) {
                                            grid.getTopToolbar().hide();
                                        },
                                            beforeadd: function (panel, cmp, i) {
                                                if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                                    return false;
                                                }
                                            }
                                        
                                    },
                                    style: "overflow-x: hidden"
                                }, {
                                    xtype: 'classroom.classschedulegrid',
                                    flex: 1,
                                    autoheight: false,
                                    allowDelete: false,
                                    title: 'Schedule',
                                    listeners: {
                                        added: function (grid, component, index) {
                                            grid.getTopToolbar().hide();
                                        },
                                            beforeadd: function (panel, cmp, i) {
                                                if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                                    return false;
                                                }
                                            }
                                        
                                    }
                                }, {
                                    xtype: 'instructors.trainingprogramdetailspanel',
                                    flex: 1,
                                    courseId: me.classId, // When it's an online course, the course id of the online course will be in the classId field.
                                    trainingProgramId: me.trainingProgramId,
                                        beforeadd: function (panel, cmp, i) {
                                            if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                                return false;
                                            }
                                        }
                                    
                                }]
                            }]
                        }, {
                            title: 'Documents',
                            name: 'files',
                            ref: '../../documents',
                            xtype: me.classTypeId === Symphony.CourseType.online ? 'courseassignment.trainingprogramdocumentsgrid' : 'classroom.coursedocumentsgrid',
                            trainingProgramId: me.trainingProgramId,
                            listeners: {
                                render: function (documentsGrid) {
                                    documentsGrid.getTopToolbar().hide();
                                },
                                activate: {
                                    fn: function (documentsGrid) {
                                        if (documentsGrid.setCourseId) {
                                            documentsGrid.setCourseId(me.klass.courseId);
                                        }
                                    },
                                    single: true
                                },
                                    beforeadd: function (panel, cmp, i) {
                                        if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                            return false;
                                        }
                                    }
                                
                            }
                        }, {
                            title: 'Message Board',
                            name: 'messageboard',
                            xtype: 'messageboard.app',
                            ref: '../../messageBoardTab',
                            hidden: true,
                            disabled: true,
                            showForumGrid: false,
                            listeners: {
                                activate: function (panel) {
                                    if (!panel.forumLoaded) {
                                        var messageBoardType = me.classTypeId === Symphony.CourseType.classroom ?
                                                                Symphony.MessageBoardType.classroom :
                                                                Symphony.MessageBoardType.online;

                                        panel.loadMessageBoardByClassId(messageBoardType, me.classId, me.klass.name, me.trainingProgramId, null, false);
                                    }
                                },
                                    beforeadd: function (panel, cmp, i) {
                                        if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                            return false;
                                        }
                                    }
                                
                            }
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        load: function () {
            var me = this;
          
            me.loadMask = new Ext.LoadMask(me.main.getEl(), {msg: "Please wait..."});
            me.loadMask.show();

            if (me.classId && me.classTypeId === Symphony.CourseType.classroom) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/classroom.svc/classes/' + me.classId,
                    success: function (result) {
                        if (me.trainingProgramId > 0) {
                            Symphony.Ajax.request({
                                method: 'GET',
                                url: '/services/portal.svc/trainingprogramdetails/' + me.trainingProgramId,
                                success: function (tpresult) {
                                    me.loadData(result.data, tpresult.data);
                                }
                            });
                        } else {
                            me.loadData(result.data);
                        }
                    }
                });
            } else if (me.classId && me.classTypeId === Symphony.CourseType.online) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/courseassignment.svc/courses/' + me.classId,
                    success: function (result) {
                        Symphony.Ajax.request({
                            method: 'GET',
                            url: '/services/portal.svc/trainingprogramdetails/' + me.trainingProgramId,
                            success: function (tpresult) {
                                me.loadData(result.data, tpresult.data);
                            }
                        });
                    }
                });
            }

            
        },
        loadData: function (data, tpData) {
            // load up the appropriate panels with the results
            var me = this;
            me.klass = data;
            me.trainingProgram = tpData;

            if (me.messageBoardTab) {
                me.messageBoardTab.setDisabled(false);
            }

            var instructorsGrid = me.find('name', 'instructorsgrid')[0]

            // first, bind the basic information
            if (me.trainingProgramId > 0 && me.classTypeId === Symphony.CourseType.online) {
                instructorsGrid.setData(tpData.leaders);
                me.find('xtype', 'instructors.trainingprogramdetailspanel')[0].setData(tpData);
                
            } else {
                instructorsGrid.setData(data.classInstructorList);
                if (me.klass.id > 0) {
                    me.find('xtype', 'classroom.classschedulegrid')[0].setClass(data);
                }
            }

            // Get the transcript details
            me.loadTranscriptDetails();


            // Show a tab if requested
            if (me.showTab) {
                switch (me.showTab) {
                    case 'documents':
                        me.classTabs.activate(me.documents);
                    break;
                }
            }
        },
        loadTranscriptDetails: function () {
            var me = this;

            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/portal.svc/transcript/',
                success: function (transcriptResult) {
                    var transcriptRecord = {
                        attemptCount: 'None',
                        attemptTime: 0,
                        score: 'Not scored',
                        passOrFail: 'Unknown',
                        completeOrIncomplete: 'Not taken',
                        attendedIndicator: false
                    };

                    for (var i = 0; i < transcriptResult.data.length; i++) {
                        var record = transcriptResult.data[i];

                        if (me.classTypeId === Symphony.CourseType.online &&
                            me.classId === record.courseId &&
                            record.courseTypeId === Symphony.CourseType.online) {
                            transcriptRecord = record;
                            break;
                        } else if (me.classTypeId === Symphony.CourseType.classroom &&
                                   record.courseTypeId === Symphony.CourseType.classroom &&
                                   record.classId === me.classId) {
                            transcriptRecord = record;
                            break;
                        }

                    }

                    var transcriptPanel = me.find('name', 'transcriptPanel')[0];
                    var transcriptLabels = transcriptPanel.find('xtype', 'label');

                    transcriptRecord.attended = transcriptRecord.attendedIndicator ? 'Yes' : 'No';
                    transcriptRecord.attemptTime = parseInt(Math.ceil(transcriptRecord.attemptTime / 60)) + ' min';

                    if (transcriptRecord.attemptDate) {
                        transcriptRecord.attemptDate = Symphony.dateRenderer(transcriptRecord.attemptDate);
                    } else {
                        transcriptRecord.attemptDate = 'Not yet attempted';
                    }
                    for (var i = 0; i < transcriptLabels.length; i++) {
                        var label = transcriptLabels[i];
                        var template = label.text;
                        var name = label.name;

                        if (name) {
                            label.update(transcriptRecord[name] ? transcriptRecord[name] : 0);
                            label.setVisible(true);
                        }
                    }
                    if (me.classTypeId === Symphony.CourseType.online) {
                        me.find('xtype', 'students.onlinetranscriptpanel')[0].setCourse(me.classId, me.trainingProgramId, transcriptRecord, me.klass, me.trainingProgram, function () {
                            me.loadTranscriptDetails();
                        });
                    } else if (me.classTypeId === Symphony.CourseType.classroom) {
                        me.find('xtype', 'students.classroomtranscriptpanel')[0].setCourse(me.classId, me.trainingProgramId, transcriptRecord, me.klass, me.trainingProgram, function () {
                            me.loadTranscriptDetails();
                        });
                    }

                    transcriptPanel.doLayout();

                    me.loadMask.hide();
                }
            });
        },
        clearButtons: function (grid) {
            var toolbar = grid.getTopToolbar();
            var buttons = toolbar.findByType('button');
            
            for (var i = 0; i < buttons.length; i++) {
                toolbar.remove(buttons[i]);
            }
        }
    });

})();