﻿(function () {
    if (!Symphony.Students) {
        Symphony.Students = {};
        Symphony.License = {};
    }

    Ext.override(Ext.grid.GroupingView, {
        interceptMouse: function (e) {
            if (e.target.getAttribute('symphony:method')) {
                return;
            }
            var hd = e.getTarget('.x-grid-group-hd', this.mainBody);
            if (hd) {
                e.stopEvent();
                this.toggleGroup(hd.parentNode);
            }
        }
    });

    Symphony.Students.App = Ext.define('students.app', {
        alias: 'widget.students.app',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                border: false,
                items: [{
                    split: true,
                    collapsible: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    title: 'My ' + Symphony.Aliases.courses,
                    xtype: 'students.enrolledclassesgrid',
                    readOnly: true,
                    ref: 'mycoursesgrid',
                    listeners: {
                        rowclick: function (grid, rowIndex, e) {
                            if (e.target.getAttribute('symphony:method')) {
                                Symphony.Portal.linkHandler(grid, rowIndex, e);
                            } else {
                                var record = grid.store.getAt(rowIndex);
                                var panelId = record.data.courseTypeId + "_" + record.data.id + "_" + record.data.trainingProgramId;

                                me.addPanel(null, null, null, record);
                            }
                        },
                        groupclick: function (grid, groupField, groupValue, e) {
                            if (e.target.getAttribute('symphony:method')) {
                                Symphony.Portal.linkHandler(grid, 0, e);
                            }
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    itemId: 'students.classcontainer'
                }]
            });

            this.callParent(arguments);

            this.on('activate', function (panel) {
                panel.find('xtype', 'students.enrolledclassesgrid')[0].refresh();
            }, this, { single: true });
        },
        addPanel: function (trainingProgramId, classId, courseTypeId, record, tab) {
            var me = this;

            // Get the data for the id if the record is not provided
            if (!record) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/classroom.svc/enrolledClasses/' + trainingProgramId + '/' + classId + '/' + courseTypeId,
                    success: function (args) {
                        var recordDef = Ext.data.Record.create(Symphony.Definitions.instructedClass);
                        var record = new recordDef(args.data);

                        me.addCoursePanel(record.get('id'), record.get('name'), record, tab);
                    }
                });
            } else {
                me.addCoursePanel(record.get('id'), record.get('name'), record, tab);
            }

        },
        addCoursePanel: function (id, name, record, tab) {
            var me = this;
            var tabPanel = this.query('[itemId=students.classcontainer]')[0];
            //get all the classforms
            var classForm = tabPanel.query('[itemId=' + id + ']'); // ('xtype', 'classroom.classform');
            if (classForm.length) {
                tabPanel.activate(classForm[0]);
            } else {
                var panel = tabPanel.add({
                    xtype: 'students.classform',
                    closable: true,
                    activate: true,
                    itemId: id,
                    title: name,
                    tabTip: name,
                    classId: record.data.id,
                    classTypeId: record.data.courseTypeId,
                    trainingProgramId: record.data.trainingProgramId,
                    showTab: tab
                });
                panel.show();
            }
            tabPanel.doLayout();
        }
    });

})();