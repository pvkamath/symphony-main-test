﻿(function () {
    Symphony.Book.BookGrid = Ext.define('book.bookgrid', {
        alias: 'widget.book.bookgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {

            var me = this;
            var url = '/services/book.svc/books/';

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'name',*/
                    header: 'Name',
                    dataIndex: 'name',
                    align: 'left',
                    flex: 1
                }, {
                    /*id: 'author',*/
                    dataIndex: 'author',
                    header: 'Author',
                    align: 'left',
                    width: 60
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                colModel: colModel,
                url: url,
                model: 'book',
                viewConfig: {
                    forceFit: true
                },
                tbar: {
                    items: [{
                        xtype: 'button',
                        iconCls: 'x-button-add',
                        text: 'Add Book',
                        handler: function () {
                            me.fireEvent('addBook');
                        }
                    }]
                }
            });

            this.callParent(arguments);
        }
    });




}());