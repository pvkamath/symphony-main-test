﻿(function () {
    Symphony.Book.BookPanel = Ext.define('book.bookpanel', {
        alias: 'widget.book.bookpanel',
        extend: 'Ext.Panel',
        bookId: 0,
        record: null,
        typeFormatBook: new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [['electronic','Electronic'],['paperback','Paperback']]
        }),
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                border: false,
                layout: 'fit',
                defaults: {
                    border: false
                },
                bodyStyle: 'padding: 15px',
                tbar: [{
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'x-button-save',
                    handler: function () {
                        me.save();
                    }
                }, {
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        me.fireEvent('cancel');
                    }
                }],
                items: [{
                    name: 'bookform',
                    xtype: 'form',
                    bodyStyle: 'padding: 0 5px',
                    frame: true,
                    border: false,
                    items: [{
                        xtype: 'fieldset',
                        title: 'Book Info',
                        defaults: { anchor: '90%', xtype: 'textfield' },
                        items: [{
                            name: 'name',
                            fieldLabel: 'Name',
                            allowBlank: false
                        }, {
                            name: 'author',
                            fieldLabel: 'Author',
                            allowBlank: true
                        }, {
                            name: 'publisher',
                            fieldLabel: 'Publisher',
                            allowBlank: true
                        }, {
                            name: 'isbn',
                            fieldLabel: 'ISBN',
                            allowBlank: true
                        },
                        Symphony.getCombo('typeFormat', 'Book Format', me.typeFormatBook, {
                            anchor: '90%',
                            valueField: 'id',
                            displayField: 'text'
                        })
                        ]
                    }]
                }],
                listeners: {
                    afterRender: function () {
                        me.loadData(me.record);
                    }
                }
            })
            this.callParent(arguments);
        },
        save: function () {
            var me = this;

            var form = me.find('name', 'bookform')[0];
            var book = form.getValues();
            Symphony.Ajax.request({
                url: '/services/book.svc/book/' + me.bookId,
                jsonData: book,
                success: function (result) {
                    var recordType = Ext.data.Record.create(Symphony.Definitions.book);
                    me.record = new recordType(result.data);

                    me.loadData(me.record.data);

                    me.bookId = me.record.get('id');
                    me.setTitle(result.data.name);

                    me.fireEvent('save');
                }
            });
        },
        loadData: function (record) {
            var me = this;

            if (record && record.data) {
                this.find('name', 'bookform')[0].bindValues(record.data);
                this.bookId = record.get('id');
            }
        }
    })

}(window));