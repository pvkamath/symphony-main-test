(function () {

    Symphony.Book.TrainingProgramBookPanel = Ext.define('book.trainingprogrambookpanel', {
        alias: 'widget.book.trainingprogrambookpanel',
        extend: 'Ext.Panel',
        trainingProgramId: 0,
        isLockBySharedTrainingFlag: false,
        initComponent: function () {
            var me = this;
            me.bookData = new Array();

            Ext.apply(this, {
                layout: 'fit',
                border: false,
                frame: false,
                bookData: me.bookData,
                items: [{
                    xtype: 'book.tpbookgrid',
                    border: false,
                    disabled: this.isLockBySharedTrainingFlag,
                    isLockBySharedTrainingFlag: me.isLockBySharedTrainingFlag,
                    trainingProgramId: me.trainingProgramId,
                    ref: 'bookAssignmentGrid'
                }]
            });

            this.callParent(arguments);
        },
        getData: function () {
            if (this.bookAssignmentGrid.rendered) {
                var results = [];
                var items = this.bookAssignmentGrid.store.data.items;

                for (var i = 0; i < items.length; i++) {
                    results.push(items[i].data);
                }

                return results;
            } else {
                return this.bookData;
            }
        },
        setData: function (data) {
            this.bookData = data;
        }
    });



    Symphony.Book.TPBookGrid = Ext.define('book.tpbookgrid', {
        alias: 'widget.book.tpbookgrid',
        extend: 'symphony.searchablegrid',
        trainingProgramId: 0,
        isLockBySharedTrainingFlag: false,
        initComponent: function () {

            var me = this;
            var url = '/services/book.svc/tpbooks/?tpid=' + me.trainingProgramId;

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns:
                [
                    { /*id: 'id',*/ hidden: true },
                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name', align: 'left',
                        flex: 1
                    },
                    { /*id: 'author',*/ dataIndex: 'author', header: 'Author', align: 'left', width: 260 },
                    { itemId: 'deleteBook', header: ' ', renderer: Symphony.deleteRenderer, width: 28 }
                ]
            });

            Ext.apply(this, {
                idProperty: 'id',
                disabled: me.isLockBySharedTrainingFlag,
                border: true,
                colModel: colModel,
                url: url,
                model: 'book',
                viewConfig: {
                    forceFit: false
                },
                tbar: {
                    items: [{
                        xtype: 'button',
                        disabled: me.isLockBySharedTrainingFlag,
                        iconCls: 'x-button-add',
                        text: 'Add',
                        handler: function () {
                            me.fireEvent('addTrainingProgramBook');
                        }
                    }]
                },
                listeners: {
                    addTrainingProgramBook: function () {
                        var win = new Ext.Window({
                            items: [{
                                name: 'addbookpanelcontainer',
                                xtype: 'panel',
                                frame: false,
                                border: false,
                                layout: 'border',
                                items: [{
                                    name: 'bookridselector',
                                    region: 'center',
                                    xtype: 'book.bookgridselector',
                                    frame: false,
                                    border: false,
                                    layout: 'fit',
                                    listeners: {
                                        addSelectedBook: function (selectedBooks) {
                                            me.addBooksToTrainingProgram(selectedBooks);
                                            win.destroy();
                                        },
                                        cancelBookSelection: function () {
                                            win.destroy();
                                        }
                                    }
                                }]
                            }],
                            modal: true,
                            resizable: false,
                            title: 'Add Book',
                            height: 600,
                            width: 600,
                            layout: 'fit'
                        }).show();
                    },
                    cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.getStore().getAt(rowIndex);
                        var columnId = grid.getColumnModel().getColumnAt(columnIndex).itemId;
                        if (columnId == 'deleteBook') {
                            grid.getStore().remove(record);
                        }
                    }
                }
            });

            this.callParent(arguments);
        },
        addBooksToTrainingProgram: function (selectedBooks) {
            me = this;
            var recordDef = Ext.data.Record.create(Symphony.Definitions.book);
            for (var i = 0; i < selectedBooks.length; i++) {
                var actRec = new recordDef(selectedBooks[i].data);
                me.getStore().add(actRec);
            }
        }
    });


    Symphony.Book.BookGridSelector = Ext.define('book.bookgridselector', {
        alias: 'widget.book.bookgridselector',
        extend: 'symphony.searchablegrid',
        initComponent: function () {

            var me = this;
            var url = '/services/book.svc/books/';
            var sm = new Ext.selection.CheckboxModel({
                checkOnly: false,
                sortable: true,
                /*id: 'checker',*/
                singleSelect: false,
                header: ' '
            });

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
 {
                        /*id: 'id',*/
                        dataIndex: 'id',
                        hidden: true
                    },
                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name', align: 'left',
                        flex: 1
                    },
                    { /*id: 'author',*/ dataIndex: 'author', header: 'Author', align: 'left', width: 60 }
                    
                ]
            });

            Ext.apply(this, {
                selModel: sm,
                idProperty: 'id',
                colModel: colModel,
                url: url,
                model: 'book',
                viewConfig: {
                    forceFit: true
                },
                tbar: {
                    items: [{
                        xtype: 'button',
                        iconCls: 'x-button-add',
                        text: 'Add Selected Book(s)',
                        handler: function () {
                            var locset = me.getSelectionModel().getSelections();
                            var selectedBooks = new Array();
                            for (var i = 0; i < locset.length; i++) {
                                selectedBooks.push(locset[i]);
                            };
                            me.fireEvent('addSelectedBook', selectedBooks);
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        iconCls: 'x-button-cancel',
                        handler: function () {
                            me.fireEvent('cancelBookSelection');
                        }
                    }]
                }
            });

            this.callParent(arguments);
        }
    });


}());