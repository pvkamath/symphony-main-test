﻿(function () {
    Symphony.CourseAssignment.BooksTab = Ext.define('courseassignment.bookstab', {
        alias: 'widget.courseassignment.bookstab',
        extend: 'Ext.Panel',
        allcustomers: [],
        initComponent: function () {
            var me = this;
            Ext.apply(me, {
                layout: 'border',
                border: false,
                items: [{
                    split: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    xtype: 'book.bookgrid',
                    listeners: {
                        rowclick: function (grid, rowIndex, e) {
                            var record = grid.getStore().getAt(rowIndex);
                            me.addPanel(record.get('id'), record.get('name'), record);
                        },
                        addBook: function () {
                            me.addPanel(0, 'New Book');
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    enableTabScroll: true,
                    xtype: 'tabpanel',
                    ref: 'tabPanel'
                }],
                addPanel: function (bookId, bookName, record) {
                    var me = this;

                    var tabPanel = me.tabPanel;
                    var found = tabPanel.find('bookId', bookId);

                    if (!found.length) {
                        var panel = tabPanel.add({
                            xtype: 'book.bookpanel',
                            closable: true,
                            activate: true,
                            title: bookName,
                            record: record,
                            border: false,
                            listeners: {
                                save: function () {
                                    me.find('xtype', 'book.bookgrid')[0].refresh();
                                },
                                cancel: function () {
                                    tabPanel.remove(panel);
                                }
                            }
                        });
                        panel.show();
                    } else {
                        me.tabPanel.setActiveTab(found[0].id);
                    }
                    tabPanel.doLayout();
                }
            });
            Symphony.CourseAssignment.BooksTab.superclass.initComponent.apply(me, arguments);
        }
    });

}());
