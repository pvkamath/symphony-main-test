﻿(function () {
    var aliases = [
        { key: '{!Audience}', val: Symphony.Aliases.audience || "Audience" },
        { key: '{!Audiences}', val: Symphony.Aliases.audiences || "Audience" },
        { key: '{!Course}', val: Symphony.Aliases.course || "Audience" },
        { key: '{!Courses}', val: Symphony.Aliases.courses || "Audience" },
        { key: '{!JobRole}', val: Symphony.Aliases.jobRole || "Audience" },
        { key: '{!JobRoles}', val: Symphony.Aliases.jobRoles || "Audience" },
        { key: '{!Location}', val: Symphony.Aliases.location || "Audience" },
        { key: '{!Locations}', val: Symphony.Aliases.locations || "Audience" },
        { key: '{!Username}', val: Symphony.Aliases.username || "Audience" },
        { key: '{!audience}', val: (Symphony.Aliases.audience || "Audience").toLowerCase() },
        { key: '{!audiences}', val: (Symphony.Aliases.audiences || "Audiences").toLowerCase() },
        { key: '{!course}', val: (Symphony.Aliases.course || "Course").toLowerCase() },
        { key: '{!courses}', val: (Symphony.Aliases.courses || "Courses").toLowerCase() },
        { key: '{!jobrole}', val: (Symphony.Aliases.jobRole || "Job Role").toLowerCase() },
        { key: '{!jobroles}', val: (Symphony.Aliases.jobRoles || "Job Roles").toLowerCase() },
        { key: '{!location}', val: (Symphony.Aliases.location || "Location").toLowerCase() },
        { key: '{!locations}', val: (Symphony.Aliases.locations || "Locations").toLowerCase() },
        { key: '{!username}', val: (Symphony.Aliases.username || "Username").toLowerCase() }
    ];

    var _dv = function (displayValue) {
        // For getting the value with any value
        // that has an associated code.
        //
        // This will allow us to do any last minute overrides
        // based on the code or any settings. In most cases
        // the value should be exactly what we want from 
        // the server, but since we have the option
        // why not.
        var result = "-";

        if (displayValue && displayValue.value) {
            result = displayValue.value;
        }
        if (typeof (displayValue) == 'string') {
            result = displayValue;
        }

        for (var i = 0; i < aliases.length; i++) {
            result = result.replace(new RegExp(aliases[i].key, "g"), aliases[i].val);
        }

        if (displayValue && displayValue.messageDate) {
            result = result.replace("{!date}", Symphony.Renderer.displayDateRenderer(displayValue.messageDate));

            var minutes = Math.ceil(displayValue.messageDate.relativeMilliseconds / 1000 / 60);
            result = result.replace("{!date_relative}", Symphony.Classroom.durationRenderer(minutes));
        }

        return result;
    }

    Symphony.Renderer = {

        getMessageTooltip: function (displayMessages) {
            var tooltipText = "";
            var messageArray = [];

            if (typeof (displayMessages) == 'string') {
                messageArray.push(displayMessages);
            } else if (displayMessages.length) {
                for (var i = 0; i < displayMessages.length; i++) {
                    messageArray.push(_dv(displayMessages[i]));
                }

                if (messageArray.length > 0) {
                    tooltipText = messageArray.length > 1 ?
                        "<ul><li>" + messageArray.join("</li><li>") + "</li></ul>" :
                        messageArray[0];
                }
            }

            return tooltipText;
        },
        displayTextRenderer: function (display) {
            if (!display) {
                return '';
            }

            var messages = Symphony.Renderer.displayMessageRenderer(display);

            if (messages !== false) {
                return messages;
            }

            return _dv(display);
        },
        displayIconRenderer: function (display) {
            if (!display) {
                return '';
            }

            var tooltip = Symphony.Renderer.getMessageTooltip(display.messages);
            var icon = !display.isEmpty ? '<span data-qtip="{0}"><img src="{1}"/></span>'.format(tooltip, _dv(display.icon)) : "";

            if (display.isEmpty && display.messages.length > 0) {
                var displayMessage = display.messages.shift();
                tooltip = display.messages.length > 0 ? Symphony.Renderer.getMessageTooltip(display.messages) : "";
                return '<span data-qtip="{0}">{1}</span>'.format(tooltip, _dv(displayMessage));
            }

            if (display.link && !display.link.isEmpty) {
                display.link.title = icon;
                icon = Symphony.Renderer.displayLinkRenderer(display.link);
            }

            return icon;
        },
        displayDateRenderer: function (display) {
            if (!display) {
                return '';
            }

            var messages = Symphony.Renderer.displayMessageRenderer(display);

            if (messages !== false) {
                return messages;
            }

            var dt = Symphony.parseDate(display.date, display.addLocalOffset);

            return dt.formatSymphony(_dv(display.format));
        },
        displayMessageRenderer: function (display) {
            if (!display) {
                return '';
            }

            if (display.messages && display.messages.length > 0) {
                if (!display.messageValues.length) {
                    return false;
                }

                var text = display.title ? _dv(display.title) : _dv(display.messages[0]);

                if (text == _dv(display.messages[0])) {
                    display.messages.shift();
                }

                var helpText = Symphony.Renderer.getMessageTooltip(display.messages);

                if (text && !helpText) {
                    return text;
                }

                return Symphony.qtipRenderer(text, helpText, 'help-icon');
            }

            return false;
        },
        displayLinkRenderer: function (display) {
            if (!display) {
                return '';
            }

            var messages = Symphony.Renderer.displayMessageRenderer(display);

            if (messages !== false && display.isEmpty) {
                return messages;
            }

            // extract query params
            var params = {};
            if (display.queryParams) {
                for (var i = 0; i < display.queryParams.length; i++) {
                    params[display.queryParams[i].key] = display.queryParams[i].value;
                }
            }

            // render the link
            var link = new Ext.Element(document.createElement("a"))
                .set({
                    id: display.domId,
                    "class": display.domClass,
                    href: display.fullUrl
                })
                .update(messages !== false ? messages : _dv(display.title))

            if (display.isNewWindow) {
                link.set({ target: '_blank' });
            }

            return link.dom.outerHTML;
        },
        displayModelRenderer: function (display) {
            if (!display || !display.messages) {
                return "";
            }

            var message = "";
            for (var i = 0; i < display.messages.length; i++) {
                message += _dv(display.messages[i]);
            }

            return message;
        }

    }
})();