﻿(function () {
    Symphony.Reporting.AvailableReportEntitiesGrid = Ext.define('reporting.availablereportentitiesgrid', {
        alias: 'widget.reporting.availablereportentitiesgrid',
        extend: 'symphony.unpagedgrid',
        initComponent: function () {
            var me = this;
            var url = '/services/reporting.svc/reportentities/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false,
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                        /*id: 'name',*/
                        header: 'Label',
                        dataIndex: 'name',
                        width: 110
                    },
                    {
                        /*id: 'description',*/
                        header: 'Description',
                        dataIndex: 'description',
                        flex: 1
                    }
                ]
            });

            Ext.apply(this, {
                idProperty: 'id',
                colModel: colModel,
                model: 'reportEntity',
                url: url,
                tbar: {
                    items: [{
                        xtype: 'button',
                        name: 'addEntity',
                        text: 'Add selected parameters to report',
                        iconCls: 'x-button-add',
                        handler: function () {
                            
                            var selected = me.getSelectionModel().getSelections();
                            if (!selected.length) {
                                Ext.Msg.alert("Select a parameter", "Please select a parameter from the list to add to your report.");
                                return;
                            }

                            me.fireEvent('addEntities', selected);
                        }
                    }]
                },
                listeners: {
                    itemdblclick: function (grid, record, item, rowIndex, e) {
                        var entity = [record]
                        me.fireEvent('addEntities', entity);
                    }
                }
            });

            this.callParent(arguments);
        }
    });

})();