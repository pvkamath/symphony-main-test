﻿(function () {
    Symphony.Reporting.WeekDayPicker = Ext.define('reporting.weekdaypicker', {
        alias: 'widget.reporting.weekdaypicker',
        extend: 'Ext.Panel',
        name: 'weekDay',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                border: false,
                cls: 'x-panel-transparent',
                items: [{
                    fieldLabel: '',
                    //style: 'margin-left:105px',
                    border: false,
                    layout: 'hbox',
                    cls: 'x-panel-transparent',
                    defaults: {
                        xtype: 'checkbox',
                        style: 'margin-left: 20px'
                    },
                    items: [{
                        boxLabel: 'Sun',
                        ref: '../sunday',
                        style: 'margin-left: 0'
                    }, {
                        boxLabel: 'Mon',
                        ref: '../monday'
                    }, {
                        boxLabel: 'Tues',
                        ref: '../tuesday'
                    }, {
                        boxLabel: 'Wed',
                        ref: '../wednesday'
                    }, {
                        boxLabel: 'Thurs',
                        ref: '../thursday'
                    }, {
                        boxLabel: 'Fri',
                        ref: '../friday'
                    }, {
                        boxLabel: 'Sat',
                        ref: '../saturday'
                    }]
                }]
            });

            this.callParent(arguments);
        },
        // Need to have all 4 (getValue, setValue, markInvalid, clearInvalid)
        // for this to be detected as a form field
        getValue: function () {
            var me = this;
            var values = [];
            if (me.sunday.getValue()) { values.push(0); }
            if (me.monday.getValue()) { values.push(1); }
            if (me.tuesday.getValue()) { values.push(2); }
            if (me.wednesday.getValue()) { values.push(3); }
            if (me.thursday.getValue()) { values.push(4); }
            if (me.friday.getValue()) { values.push(5); }
            if (me.saturday.getValue()) { values.push(6); }
            return values;
        },
        setValue: function (values) {
            var me = this;

            me.sunday.setValue(false);
            me.monday.setValue(false);
            me.tuesday.setValue(false);
            me.wednesday.setValue(false);
            me.thursday.setValue(false);
            me.friday.setValue(false);
            me.saturday.setValue(false);

            if (values && values.length) {
                for (var i = 0; i < values.length; i++) {
                    if (values[i] == 0) { me.sunday.setValue(true); }
                    if (values[i] == 1) { me.monday.setValue(true); }
                    if (values[i] == 2) { me.tuesday.setValue(true); }
                    if (values[i] == 3) { me.wednesday.setValue(true); }
                    if (values[i] == 4) { me.thursday.setValue(true); }
                    if (values[i] == 5) { me.friday.setValue(true); }
                    if (values[i] == 6) { me.saturday.setValue(true); }
                }
            }
        },
        markInvalid: function () {

        },
        clearInvalid: function () {

        }
    });

})();