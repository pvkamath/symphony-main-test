﻿
(function () {
    Symphony.Reporting.ReportViewer = Ext.define('reporting.reportviewer', {
        alias: 'widget.reporting.reportviewer',
        extend: 'Ext.Panel',
        reportJson: null,
        queueEntry: null,
        pageSize: 40,
        initComponent: function () {
            var me = this;
            var items = [];

            // Each table in the dataset gets a tab
            for (var tableName in me.reportJson) {
                var table = me.reportJson[tableName];

                var fields = [];
                var columns = [];
                for (var fieldName in table[0]) {
                    fields.push(fieldName);
                    columns.push({ sortable: true, header: fieldName, dataIndex: fieldName, renderer: function (value) { return Ext.util.Format.htmlEncode(value) } });
                }

                var store = new Ext.data.JsonStore({
                    fields: fields,
                    pageSize: me.pageSize,
                    data: table,
                    lastOptions: { params: { start: 0, limit: me.pageSize } }
                });

                items.push({
                    title: tableName,
                    xtype: 'grid',
                    store: store,
                    columns: columns,
                    bbar: new Ext.PagingToolbar({
                        displayInfo: true,
                        displayMsg: '{0} - {1} of {2}',
                        emptyMsg: 'No results',
                        store: store
                    })
                    //                    view: new Ext.ux.grid.BufferView({
                    //                        // custom row height
                    //                        rowHeight: 34,
                    //                        // render rows as they come into viewable area.
                    //                        scrollDelay: false
                    //                    })
                });
            }

            Ext.apply(this, {
                title: 'Report Results',
                border: false,
                defaults: {
                    border: false
                },
                layout: 'fit',
                items: [{
                    xtype: 'tabpanel',
                    activeItem: 0,
                    items: items
                }]
            });
            this.callParent(arguments);
        }
    });


})();