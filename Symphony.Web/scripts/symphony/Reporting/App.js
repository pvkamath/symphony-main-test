﻿(function () {
    if (!Symphony.Reporting) {
        Symphony.Reporting = {};
    }
    Symphony.Reporting.App = Ext.define('reporting.app', {
        alias: 'widget.reporting.app',
        extend: 'Ext.Panel',
        selectTab: function (xtype) {
            this.find('xtype', 'tabpanel')[0].activate(this.find('xtype', xtype)[0]);
            return true;
        },
        initComponent: function () {
            var items = [];
            items.push({
                title: 'Report Queue',
                //disabled: true,
                tabTip: 'Queue of all reports that are running or finished running.',
                xtype: 'reporting.reportqueuetab',
                listeners: {
                    activate: function (queueTab) {
                        queueTab.reportQueueGrid.refresh();
                        queueTab.reportQueueGrid.startReloadTask();
                    },
                    deactivate: function (queueTab) {
                        queueTab.reportQueueGrid.stopReloadTask();
                    }
                }
            });
            items.push({
                title: 'My Reports',
                tabTip: 'Create new reports from templates',
                xtype: 'reporting.reportbuildertab'
            });
            if (Symphony.User.isSalesChannelAdmin) {
                items.push({
                    title: 'Report Templates',
                    tabTip: 'Manage Report Templates',
                    xtype: 'reporting.reporttemplatestab'
                });
            }

            Ext.apply(this, {
                requiresActivation: true,
                // activates the first panel on load of the tab
                border: false,
                defaults: {
                    border: false
                },
                layout: 'fit',
                items: [{
                    xtype: 'tabpanel',
                    //activeItem: 0,
                    items: items
                }]
            });
            this.callParent(arguments);
        },
        // manual activate
        activate: function () {
            this.selectTab('reporting.reportqueuetab');
        }
    });


    Symphony.Reporting.autoSaveInterval = 2000; // in milliseconds, set null to disable
    Symphony.Reporting.enabledReportTypes = [
        'coursesummary',
        'studentdetail',
        'studenttranscript',
        'coursecompletion',
        'studentsummary',
        'studenttestanalysis',
        'studenttestattempt',
        'survey',
        'coursesbystudent',
        'audiencelisting',
        'coursesbyaudience',
        'coursesbycourse',
        'coursetotals',
        'onlinecourseschedules',
        'studentlisting',
        'studenttestreview',
        'classroomroster',
        'classroomtraining'
    ];
    Symphony.Reporting.isReportEnabled = function (reportTypeName) {
        return (Symphony.Reporting.enabledReportTypes.indexOf(reportTypeName) >= 0);
    };
})();