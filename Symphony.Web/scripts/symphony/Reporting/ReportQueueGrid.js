﻿(function () {
    Symphony.Reporting.ReportQueueGrid = Ext.define('reporting.reportqueuegrid', {
        alias: 'widget.reporting.reportqueuegrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/reporting.svc/queueEntries/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'status',*/
                    header: 'Status',
                    dataIndex: 'statusId',
                    align: 'left',
                    renderer: function (value, meta, record) {
                        var statusText = record.get('status');
                        var color = 'black';
                        switch (value) {
                            case 0: color = 'black'; break;
                            case 1: color = '#ed0'; break;
                            case 2: color = 'red'; break;
                            case 3: color = 'green'; break;
                        }
                        
                        if (value == 0) {
                            statusText = "Queued";
                        }
                        var res = '<span style="color:' + color + ';font-weight:bold;">' + statusText  + '</span>'
                        return res;
                    }
                }, {
                    /*id: 'download',*/
                    header: 'Download',
                    dataIndex: 'download',
                    align: 'left',
                    renderer: function (value, meta, record) {

                        var statusId = record.get('statusId');

                        var hasCSV = record.get('downloadCSV');
                        var hasXLS = record.get('downloadXLS');
                        var hasPDF = record.get('downloadPDF');
                        var queueId = record.get('id');

                        var res = '<a target="_blank" href="/services/reporting.svc/reports/CSV/' + queueId + '" title="Download CSV" class="queue-download-link"><img src="/images/' + (hasCSV ? 'page_white_database.png' : 'bullet_white.png') + '" alt="Download CSV" /></a>' +
                               '<a ' + (hasXLS ? 'target="_blank" href="/services/reporting.svc/reports/XLS/' + queueId + '"' : 'href="#"') + ' title="Download XLS" class="queue-download-link"><img src="/images/' + (hasXLS ? 'page_white_excel.png' : 'bullet_white.png') + '" alt="Download XLS" /></a>';
                        if (statusId != 3) {
                            res = '<span><img src="/images/bullet_white.png" alt="Download CSV" /></span>' +
                               '<span><img src="/images/bullet_white.png" alt="Download XLS" /></span>';
                        }
                        return res;
                    }
                }, {
                    /*id: 'reportName',*/
                    header: 'Report Name',
                    dataIndex: 'reportName',
                    align: 'left',
                    width: 300,
                    flex: 1
                }, {
                    /*id: 'startedOn',*/
                    header: 'Run Date',
                    dataIndex: 'startedOn',
                    align: 'left',
                    width: 150,
                    renderer: Symphony.dateTimeRenderer
                    //                }, {
                    //                    /*id: 'nextRunTime',*/
                    //                    header: 'Next Run Date',
                    //                    dataIndex: 'nextRunTime',
                    //                    align: 'left',
                    //                    renderer: Symphony.dateRenderer
                }, {
                    /*id: 'view',*/
                    header: ' ',
                    dataIndex: 'status',
                    align: 'left',
                    renderer: function (value, meta, record) {
                        var statusId = record.get('statusId');
                        var statusDataLenth = record.get('statusDataLength');
                        if (statusDataLenth > 0) {
                            res = '<span><img src="/images/bullet_white.png" alt="View Results" /></span>';
                            return res;
                        }
                        var res = '<a href="/ReportViewer.aspx?queueId=' + record.get('id') + '" target="_blank">View Results</a>';
                        if (statusId != 3) {
                            res = '<span><img src="/images/bullet_white.png" alt="No Results" /></span>';
                            if (statusId == 2) { // failed
                                return Symphony.qtipRenderer(record.get('errorMessage'));
                            }
                        }
                        return res;
                    }
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                deferLoad: true,
                url: url,
                sortInfo: { field: "startedOn", direction: "DESC" },
                colModel: colModel,
                model: 'reportQueue'
            });

            this.callParent(arguments);
        },
        getSelected: function () {
            return this.getSelectionModel().selections;
        },
        reloadTask: null,
        startReloadTask: function (refreshSeconds) {
            var me = this;
            refreshSeconds = refreshSeconds || 5;
            me.reloadTask = Ext.TaskMgr.start({
                run: function () {
                    me.loadMask.disable();
                    me.getStore().load();
                },
                interval: refreshSeconds * 1000
            });
        },
        stopReloadTask: function () {
            var me = this;
            Ext.TaskMgr.stop(me.reloadTask);
        }
    });

})();