﻿(function () {
    Symphony.Reporting.ReportEditor = Ext.define('reporting.reporteditor', {
        alias: 'widget.reporting.reporteditor',
        extend: 'Ext.Panel',
        report: null,
        initComponent: function () {
            var me = this;
            var isQlik = me.report.reportTemplate.reportSystem == Symphony.ReportSystem.qlik ? true : false;
            var isDefault = me.report.reportTemplate.reportSystem == Symphony.ReportSystem['default'] ? true : false;

            me.baseReportEditorConfig = {
                xtype: 'reporting.basereporteditor',
                ref: '../../../reportParametersForm',
                frame: false,
                border: true,
                cls: 'x-panel-transparent',
                report: me.report,
                defaults: {
                    anchor: '100%'
                },
                listeners: {
                    formready: function (form) {
                        if (me.report.parameters) {
                            var params = JSON.parse(me.report.parameters);
                            form.bindValues(params);
                        }
                        me.fireEvent('formready', form);
                    }
                }
            }

            Ext.apply(this, {
                layout: 'border',
                items: [{
                    region: 'north',
                    height: 27,
                    xtype: 'symphony.savecancelbar',
                    items: ['-', {
                        text: 'Make Favorite',
                        iconCls: 'x-button-favorite',
                        enableToggle: true,
                        pressed: me.report.isFavorite,
                        toggleHandler: Ext.bind(function (btn, pressed) {
                            this.saveReport(false, pressed);
                        }, this)
                    }, {
                        text: 'Run',
                        hidden: !isDefault,
                        iconCls: 'x-button-run',
                        handler: function () {
                            this.saveReport(true);
                        }.createDelegate(this)
                    }, {
                        text: 'View',
                        hidden: !isQlik,
                        iconCls: 'x-button-chart',
                        handler: function () {
                            this.viewQlikReport();
                        }.createDelegate(this)
                    }],
                    listeners: {
                        save: Ext.bind(function () {
                            this.saveReport();
                        }, this),
                        cancel: Ext.bind(function () {
                            this.ownerCt.remove(this);
                        }, this)
                    }
                }, {
                    region: 'center',
                    border: false,
                    name: 'main',
                    bodyStyle: 'padding: 15px',
                    xtype: 'tabpanel',
                    activeItem: 0,
                    deferredRender: false,
                    items: [{
                        title: 'General',
                        frame: true,
                        overflowY: 'scroll',
                        overflowX: 'hidden',

                        items: [{
                            xtype: 'form',
                            border: false,
                            frame: false,
                            cls: 'x-panel-transparent',
                            ref: '../../reportForm',
                            defaults: {
                                itemCls: 'report-editor-field'
                            },
                            labelWidth: 120,
                            items: [{
                                xtype: 'textfield',
                                fieldLabel: 'Name',
                                name: 'name',
                                width: 250
                            }, {
                                xtype: 'checkbox',
                                fieldLabel: 'Notify when ready',
                                name: 'notifyWhenReady',
                                hidden: !isDefault
                            }, {
                                hidden: true, // jerod, 11/05, hidden for now since the backend doesn't support it anyway !isDefault,
                                layout: 'hbox',
                                fieldLabel: 'Download Options',
                                cls: 'options',
                                defaults: {
                                    xtype: 'checkbox',
                                    style: 'margin-left:20px;font-size:11px;'
                                },
                                items: [{
                                    boxLabel: '<img src="/images/page_white_database.png" class="option-icon" /> CSV',
                                    name: 'downloadCSV',
                                    style: 'margin-left: 0'
                                }, {
                                    boxLabel: '<img src="/images/page_white_excel.png" class="option-icon" /> Excel',
                                    name: 'downloadXLS'
                                }]
                            }, {
                                // xtype: 'checkbox',// andrew, 03/05/16 - not allowing a toggle between symphony ids and the internal reporting keys
                                                     // anymore, so this is defaulting to Symphony IDs all the time. Leaving the functionality
                                                     // in place in case we ever need to be able to switch back. 
                                xtype: 'hiddenfield', 
                                value: 1,
                                fieldLabel: 'Use Symphony IDs',
                                name: 'isSymphonyIdMode',
                                boxLabel: 'Enable to use IDs from the Symphony database instead of the Symphony Reporting database.',
                                listeners: {
                                    change: function (chk, newValue, oldValue, opts) {
                                        Ext.Msg.wait("Rebuilding form...", "Please Wait");

                                        // Hate doing this, but it seems that the message wait needs some time
                                        // to complete in order to render. Even when trying to hook into the 
                                        // after render event, the messagebox would not display at all.
                                        setTimeout(function () {
                                            var baseReportEditor = me.query('[xtype=reporting.basereporteditor]')[0];
                                            var reportEditorContainer = me.query('[name=reporteditorcontainer]')[0];

                                            reportEditorContainer.remove(baseReportEditor);
                                            me.baseReportEditorConfig.isSymphonyIdMode = newValue;
                                            reportEditorContainer.add(Ext.create('reporting.basereporteditor', me.baseReportEditorConfig));

                                            reportEditorContainer.doLayout();

                                            Ext.Msg.hide();
                                        }, 1);
                                    }
                                }
                            }]
                        }, {
                            xtype: 'tabpanel',
                            activeItem: 0,
                            border: false,
                            frame: false,
                            cls: 'x-panel-transparent',
                            width: 700,
                            items: [{
                                title: 'Report Parameters',
                                name: 'reporteditorcontainer',
                                border: false,
                                frame: false,
                                cls: 'x-panel-transparent',
                                items: [me.baseReportEditorConfig]
                            }, {
                                xtype: 'reporting.scheduleeditor',
                                ref: '../../../reportScheduleForm',
                                frame: false,
                                border: true,
                                cls: 'x-panel-transparent',
                                defaults: {
                                    anchor: '100%'
                                }
                            }]
                        }]
                    }].concat(isQlik ? [{
                        title: 'Results',
                        frame: true,
                        name: 'results',
                        layout: 'fit',
                        html: '<iframe width="100%" width="100%" style="height:100%;width:100%;padding:0;border:0" frameborder="0" src="about:blank"></iframe>'
                        
                    }] : [])
                }],
                listeners: {
                    afterrender: function () {
                        if (me.report) {
                            setTimeout(function () {
                                me.reportForm.bindValues(me.report);
                                me.reportScheduleForm.bindValues(me.report);
                            }, 1);
                        }
                    }
                }
            });

            this.callParent(arguments);
        },
        viewQlikReport: function () {
            this.saveReport();
            this.on('save', function (report) {
                
                var self = this;

                //validate the qlik user                
                var qlikInterface = Ext.create('reporting.qlikinterface');
                
                qlikInterface.validateUser().then(function (success) {
                    if (success) {

                        // take the converted params + the template url and generate a url for the report
                        var url = report.reportTemplate.reportSystemParameters + "&symrpt=" + self.report.id;
                        var params = $.parseXML(report.absoluteXmlParameters);

                        //var query = [];
                        //$(params).find('report').children().each(function () {
                        //    //query.push('select=' + encodeURIComponent($(self).prop('tagName')) + ',' + encodeURIComponent($(self).text()));
                        //    query.push('select=' + encodeURIComponent(this.tagName)) + ',' + encodeURIComponent(this.textContent)));
                        //});
                        //if (url.indexOf('?') == -1) {
                        //    url += '?';
                        //} else {
                        //    url += '&';
                        //}
                        //url += query.join('&');

                        var query = [];
                        var tName;

                        $(params).find('report').children().each(function () {
                            tName = encodeURIComponent(this.tagName)

                            if (!query[tName]) {
                                query[tName] = [];
                            }
                            query[tName].push(this.textContent);
                        });

                        for (var property in query) {
                            if (query.hasOwnProperty(property)) {
                                url += "&select=" + property + ",";
                                $(query[property]).each(function () {
                                    url += this + ",";
                                });
                                url = url.substring(0, url.length - 1); //remove trailing comma
                            }
                        }

                        // set the frame url
                        var resultsTab = self.find('name', 'results')[0];
                        var frame = $(resultsTab.el.dom).find('iframe');
                        frame.attr('src', url);

                        var mask = resultsTab.el.mask('Loading, please wait...');
                        frame.one('load', function () {
                            resultsTab.el.unmask();
                        });

                        var container = self.find('name', 'main')[0];
                        container.setActiveTab(resultsTab);
                    } else {
                        //TODO show pretty errror
                        var resultsTab = self.find('name', 'results')[0];
                        var frame = $(resultsTab.el.dom).find('iframe');
                        frame.attr('src', '');

                        resultsTab.el.mask('Qlik error -- could not verify user -- Please close your browser and try again.');

                        var container = self.find('name', 'main')[0];
                        container.setActiveTab(resultsTab);
                    }
                });

            }, this, { single: true });
        },
        saveReport: function (runImmediately, isFavorite) {
            var me = this;
            var values = me.reportForm.getValues();
            var paramValues = me.query('[xtype=reporting.basereporteditor]')[0].getValues();
            var scheduleValues = me.reportScheduleForm.getValues();

            values.parameters = JSON.stringify(paramValues);
            values.reportTypeId = me.report.reportTemplate.id;
            values.reportTemplateId = me.report.reportTemplate.id;

            if (isFavorite != null) {
                values.isFavorite = isFavorite;
            }
            Ext.apply(values, scheduleValues);

            if (values.scheduleType == 2 && values.scheduleDaysOfWeek.length < 1) {
                Ext.Msg.alert('Validation Error', 'You must select at least one day of the week for a weekly schedule.');
                return;
            }

            Ext.Msg.wait('Please wait while this report is saved.', 'Saving...');

            Symphony.Ajax.request({
                timeout: 240 * 1000,
                url: '/services/reporting.svc/reports/' + (me.report && me.report.id ? me.report.id : '0') + (runImmediately ? '?run=true' : ''),
                jsonData: values,
                success: function (result) {
                    Ext.apply(me.report, result.data);

                    me.fireEvent('save', me.report);
                    Ext.Msg.hide();
                },
                failure: function (result) {
                    me.fireEvent('saveFailed');
                    Ext.Msg.hide();
                    var errorMsg = result.error || 'An error occurred while saving this report.';
                    Ext.Msg.alert('Error saving report', errorMsg);
                }
            });
        }
    });

})();