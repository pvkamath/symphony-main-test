﻿(function () {
    Symphony.Reporting.BaseReportEditor = Ext.define('reporting.basereporteditor', {
        alias: 'widget.reporting.basereporteditor',
        extend: 'Ext.form.FormPanel',
        frame: true,
        border: false,
        autoHeight: true,
        autoScroll: true,
        labelWidth: 125,
        bodyStyle: 'padding: 15px',
        report: null,
        defaults: {
            itemCls: 'report-editor-field'
        },
        isSymphonyIdMode: false,
        buildForm: function () {
            var me = this;

            for (var i = 0; i < me.report.reportTemplate.reportEntities.length; i++) {
                var entity = me.report.reportTemplate.reportEntities[i];
                var elementConfig = {
                    xtype: entity.xtype,
                    fieldLabel: entity.name,
                    name: entity.tagName
                }

                if (entity.description) {
                    elementConfig.help = {
                        title: entity.name,
                        text: entity.description
                    }
                }

                if (entity.config) {
                    try {
                        var config = JSON.parse(entity.config);
                        Ext.apply(elementConfig, config);
                    } catch (e) {
                        // Invalid json, just ignore it. 
                    }
                }

                elementConfig.isSymphonyIdMode = me.isSymphonyIdMode;

                var element = Ext.create(entity.xtype, elementConfig);
                var reportingKey = 'key';
                var symphonyKey = 'Id';

                if (me.isSymphonyIdMode && element.valueField) {
                    var keyPosition = element.valueField.indexOf(reportingKey, element.valueField.length - reportingKey.length);
                    if (keyPosition > 0) {
                        element.valueField = element.valueField.substr(0, keyPosition) + symphonyKey;
                    }
                }

                me.add(element);
            }

            me.on('afterlayout', function (panel, layout, opts) {
                this.fireEvent('formready', this);
            }, me, { single: true });

            me.doLayout();
        },
        initComponent: function () {
            var me = this;
            
            Ext.apply(this, {
                listeners: {
                    customerChanged: function (customerIds) {
                        var fields = me.items.items;

                        for (var i = 0; i < fields.length; i++) {
                            var field = fields[i];

                            if (field.setCustomers) {
                                field.setCustomers(customerIds);
                            }
                        }
                    },
                    afterrender: function () {
                        me.buildForm();
                    }
                }
            });

            this.callParent(arguments);
        }
    });

})();