﻿(function () {
    Symphony.Reporting.ReportTemplatePicker = Ext.define('reporting.reporttemplatepicker', {
        alias: 'widget.reporting.reporttemplatepicker',
        extend: 'Ext.Window',
        selected: null,
        pageSize: 20,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                title: 'Select a Report Template',
                height: 565,
                width: 450,
                defaults: {
                    border: false
                },
                buttons: [{
                    text: 'OK',
                    itemId: 'okButton',
                    disabled: true,
                    handler: function () {
                        me.fireEvent('templateselect', me.selected);
                        
                        me.close();
                    }
                }, {
                    text: 'Cancel',
                    handler: function () {
                        me.close();
                    }
                }]
            });
            this.items = [{
                xtype: 'reporting.reporttemplatesgrid',
                pageSize: this.pageSize,
                type: this.type,
                height: 500,
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.store.getAt(rowIndex);
                        var fbar = me.getDockedItems('[dock=bottom]')[0];
                        me.selected = record ? record.data : null;
                        fbar.getComponent('okButton').setDisabled(!me.selected);
                    },
                    celldblclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.store.getAt(rowIndex);
                        var fbar = me.getDockedItems('[dock=bottom]')[0];
                        me.selected = record ? record.data : null;
                        fbar.getComponent('okButton').setDisabled(!me.selected);

                        me.fireEvent('templateselect', me.selected);
                        me.close();
                    },
                    afterlayout: {
                        fn: function (grid) {
                           grid.refresh();
                        },
                        single: true
                    }
                }
            }];
            this.callParent(arguments);
        }
    });

})();