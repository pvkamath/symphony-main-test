﻿(function () {
    Symphony.Reporting.ReportTemplatesTab = Ext.define('reporting.reporttemplatestab', {
        alias: 'widget.reporting.reporttemplatestab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                items: [{
                    split: true,
                    collapsible: true,
                    region: 'west',
                    border: false,
                    width: 400,
                    title: 'Report Templates',
                    xtype: 'reporting.reporttemplatesgrid',
                    ref: 'reportTemplatesGrid',
                    listeners: {
                        cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            var record = grid.store.getAt(rowIndex);
                            if (grid.headerCt.getGridColumns()[columnIndex].id != 'delete') {
                                me.addPanel(record.get('id'), record.get('name'));
                            }
                        },
                        addclick: function () {
                            me.addPanel(0, 'New Template');
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    name: 'tabpanelreporttemplateeditor',
                    enableTabScroll: true,
                    itemId: 'reporting.reporttemplateeditorcontainer'
                }],
                listeners: {
                    // delayed load
                    activate: {
                        fn: function (panel) {
                            panel.find('xtype', 'reporting.reporttemplatesgrid')[0].refresh();
                        },
                        single: true
                    }
                }
            });
            this.callParent(arguments);
        },
        activate: function () {
            this.find('xtype', 'reporting.reporttemplatesgrid')[0].refresh();
        },
        addPanel: function (id, name) {
            var me = this;
            //var tabPanel = this.getComponent('reporting.reporttemplateeditorcontainer');
            var tabPanel = me.find('name', 'tabpanelreporttemplateeditor')[0];
            
            var found = id && tabPanel.queryBy(function (element) {
                return element.templateId == id;
            });

            if (found.length > 0) {
                tabPanel.activate(found[0]);
            } else {
                if (id) {
                    Ext.Msg.wait('Please wait while this report template is loaded...', 'Loading...');
                    Symphony.Ajax.request({
                        method: 'GET',
                        url: '/services/reporting.svc/reporttemplate/' + id,
                        success: function (result) {
                            me._addPanel(result.data, name, id);

                            Ext.Msg.hide();
                        }
                    });
                } else {
                    me._addPanel({
                        name: name,
                        id: id
                    }, name);
                }
            }
            tabPanel.doLayout();
        },
        _addPanel: function (reporttemplate, name, templateId) {
            var me = this;
            var tabPanel = this.getComponent('reporting.reporttemplateeditorcontainer');
            var panel = tabPanel.add({
                xtype: 'reporting.reporttemplateeditor',
                closable: true,
                activate: true,
                templateId: templateId,
                title: name,
                reporttemplate: reporttemplate,
                listeners: {
                    save: function (reporttemplate) {
                        me.find('xtype', 'reporting.reporttemplatesgrid')[0].refresh();
                        panel.setTitle(reporttemplate.name);
                        panel.reporttemplate = reporttemplate;
                    },
                    saveFailed: function () {
                        me.find('xtype', 'reporting.reporttemplatesgrid')[0].refresh();
                    },
                    duplicate: function (reporttemplate) {
                        me.find('xtype', 'reporting.reporttemplatesgrid')[0].refresh();
                        me._addPanel(report, report.name);
                    }
                }
            });
            panel.show();
        }
    });

})();