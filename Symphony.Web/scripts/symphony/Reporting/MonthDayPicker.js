﻿(function () {
    Symphony.Reporting.MonthDayPicker = Ext.define('reporting.monthdaypicker', {
        alias: 'widget.reporting.monthdaypicker',
        extend: 'symphony.dropdownbox',
        name: 'monthDay',
        _fieldLabel: 'Day',
        forceSelection: true,
        value: 1, // default value
        initComponent: function () {
            var me = this;

            var data = [];
            for (var i = 1; i <= 31; i++) {
                data.push([i, 'Day ' + i + ' of month']);
            }
            data.push([-1, 'Last day of the month']);


            Ext.apply(this, {
                fieldLabel: '', //this._fieldLabel,
                queryMode: 'local',
                store: new Ext.data.ArrayStore({
                    fields: ['id', 'text'],
                    data: data
                }),
                valueField: 'id',
                displayField: 'text'
            });

            this.callParent(arguments);
        },
        setVisible: function (visible) {
            Symphony.Reporting.MonthDayPicker.superclass.setVisible.call(this, visible);
            //this.fieldLabel = (visible ? this._fieldLabel : '');
        }
    });

})();