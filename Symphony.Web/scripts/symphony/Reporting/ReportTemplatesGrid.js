﻿(function () {
    Symphony.Reporting.ReportTemplatesGrid = Ext.define('reporting.reporttemplatesgrid', {
        alias: 'widget.reporting.reporttemplatesgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/reporting.svc/reporttemplates/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'name',*/
                    header: 'Name',
                    dataIndex: 'name',
                    align: 'left',
                    flex: 1
                }, {
                    //id: 'reportSystem',
                    header: 'Report Type',
                    dataIndex: 'reportSystem',
                    align: 'center',
                    renderer: function (value, meta, record) {
                        return value == Symphony.ReportSystem.qlik ? 'Enhanced' : 'Standard';
                    }
                }]
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }]
                },
                idProperty: 'id',
                deferLoad: true,
                url: url,
                colModel: colModel,
                model: 'reportTemplate'
            });

            this.callParent(arguments);
        },
        getSelected: function () {
            return this.getSelectionModel().selections;
        }
    });

})();