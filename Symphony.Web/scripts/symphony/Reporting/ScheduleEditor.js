﻿(function () {
    Symphony.Reporting.ScheduleEditor = Ext.define('reporting.scheduleeditor', {
        alias: 'widget.reporting.scheduleeditor',
        extend: 'Ext.form.FormPanel',
        title: 'Schedule',
        frame: true,
        border: false,
        height: 500,
        autoScroll: true,
        bodyStyle: 'padding: 15px',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                defaults: {
                    itemCls: 'report-editor-field'
                },
                items: [{
                    //                    xtype: 'checkbox',
                    //                    fieldLabel: 'Enabled'
                    //                }, {
                    xtype: 'symphony.dropdownbox',
                    value: 0,
                    forceSelection: true,
                    fieldLabel: 'Schedule Type',
                    name: 'scheduleType',
                    queryMode: 'local',
                    store: new Ext.data.ArrayStore({
                        fields: ['id', 'text'],
                        data: [[0, 'No Schedule'], [1, 'Daily'], [2, 'Weekly'], [3, 'Monthly']/*, [4, 'Yearly']*/]
                    }),
                    valueField: 'id',
                    displayField: 'text',
                    listeners: {
                        'select': function (combo, record, index) {
                            var selected = combo.getValue();
                            me.hourpicker.setVisible(selected == 1 || selected == 2 || selected == 3);
                            me.weekdaypicker.setVisible(selected == 2);
                            me.monthdaypicker.setVisible(selected == 3);
                        },
                        'afterrender': function () {
                            var combo = this;
                            setTimeout(function () {
                                var selected = combo.getValue();
                                me.hourpicker.setVisible(selected == 1 || selected == 2 || selected == 3);
                                me.weekdaypicker.setVisible(selected == 2);
                                me.monthdaypicker.setVisible(selected == 3);
                            }, 1);
                        }
                    }
                }, {
                    xtype: 'reporting.hourpicker',
                    name: 'scheduleHour',
                    ref: 'hourpicker',
                    hideEmptyLabel: false
                }, {
                    xtype: 'reporting.weekdaypicker',
                    name: 'scheduleDaysOfWeek',
                    ref: 'weekdaypicker',
                    style: 'margin-left:105px',
                    hideEmptyLabel: false
                }, {
                    xtype: 'reporting.monthdaypicker',
                    name: 'scheduleDayOfMonth',
                    ref: 'monthdaypicker',
                    hideEmptyLabel: false
                }]
            });
            this.callParent(arguments);
        }
    });

})();