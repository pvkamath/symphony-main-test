﻿(function () {
    Symphony.Reporting.HourPicker = Ext.define('reporting.hourpicker', {
        alias: 'widget.reporting.hourpicker',
        extend: 'symphony.dropdownbox',
        value: 0,       // default value
        name: 'hour',
        _fieldLabel: 'At',
        forceSelection: true,
        isMilitaryTime: false,
        initComponent: function () {
            var me = this;

            var data = [];
            for (var i = 0; i < 24; i++) {
                var displayText = (me.isMilitaryTime ? i + ':00' : me.convertToStandardTime(i));
                data.push([i, displayText]);
            }

            Ext.apply(this, {
                fieldLabel: '', //this._fieldLabel,
                queryMode: 'local',
                store: new Ext.data.ArrayStore({
                    fields: ['id', 'text'],
                    data: data
                }),
                valueField: 'id',
                displayField: 'text'
            });

            this.callParent(arguments);
        },
        convertToStandardTime: function (militaryHour) {
            if (militaryHour == 0) {
                return '12:00 pm';
            }
            else if (militaryHour > 12) {
                return (militaryHour - 12) + ':00 pm';
            }
            else {
                return militaryHour + ':00 am';
            }
        },
        setVisible: function (visible) {
            Symphony.Reporting.HourPicker.superclass.setVisible.call(this, visible);
            //this.fieldLabel = (visible ? this._fieldLabel : '');
        }
    });

})();