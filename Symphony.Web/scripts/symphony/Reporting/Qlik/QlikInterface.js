﻿Ext.define('reporting.qlikinterface', {
    constructor: function () { },
    getQlikInstanceInfo: function () {
        //get the qlik instance info
        var qlikInfoURL = 'https://oclqlikapi.azurewebsites.net/api/qlik/' + Symphony.Qlik.environment + '/' + Symphony.Qlik.instance;

        //console.log("Qlik Info URL: ", qlikInfoURL);
        return $.ajax({
            url: qlikInfoURL
        });
    },
    loginUser: function () {
        if (window.console) {
            console.log("Log in Current User To Qlik");
        }

        this.getQlikInstanceInfo().then(function (qlikInfo) {
            //log the user into qlik by requesting the HUB
            //console.log("REQUEST HUB: ", qlikInfo);
            var hubUrl = qlikInfo.BaseURL + qlikInfo.Proxy + '/hub/&callback=?';
            //console.log("HUB URL: ", hubUrl);
            return $.ajax({
                url: hubUrl,
                method: 'GET',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true
            }).then(function (data) {
                //console.log("USER LOGGED IN", data);
            });
        });
    },
    getQlikUserInfo: function (qlikInstanceInfo) {
        var qlikUserInfoURL = qlikInstanceInfo.BaseURL;
        if (qlikUserInfoURL.slice(-1) !== '/') {
            qlikUserInfoURL += '/';
        }

        if (qlikInstanceInfo.Proxy.length > 0) {
            qlikUserInfoURL += qlikInstanceInfo.Proxy.toLowerCase() + '/';
        }
        qlikUserInfoURL += 'qps/user';

        //console.log("QLIK USER INFO URL", qlikUserInfoURL);

        return $.ajax({
            url: qlikUserInfoURL,
            method: 'GET',
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true
        }).then(function (qlikUserInfo) {
            return qlikUserInfo;
        });
    },
    validateUser: function () {
        //console.log("Validating user");

        //TODO -- set constants
        //console.log("QlikInfo", Symphony);

        //var qlikEnvironment = 'prod';
        //var symphonyInstance = 'localhost';
        var self = this;

        //get the qlik instance info
        //var qlikInfoURL = 'https://oclqlikapi.azurewebsites.net/api/qlik/' + Symphony.Qlik.environment + '/' + Symphony.Qlik.instance;

        //return $.ajax({
        //    url: qlikInfoURL
        //})

        return this.getQlikInstanceInfo().then(function (qlikInstanceInfo) {
            //console.log("QLIK INSTANCE INFO", qlikInstanceInfo);
            self.qlikInstanceInfo = qlikInstanceInfo;
            return qlikInstanceInfo;

        }).then(this.getQlikUserInfo)
        .then(function (qlikUserInfo) {
            //console.log("QLIK USER INFO", qlikUserInfo);

            if (qlikUserInfo && qlikUserInfo.session) {
                //console.log("Qlik session is inactive -- no user is logged in");
                return true;
            }

            //console.log("SYM USER", Symphony.User);

            //compare users
            var idIsEqual = Symphony.User.id == qlikUserInfo.userId;
            var proxyIsEqual = self.qlikInstanceInfo.Proxy.toUpperCase() === qlikUserInfo.userDirectory.toUpperCase();

            if (!idIsEqual || !proxyIsEqual) {
                //console.log("MUST LOG OUT USER");

                //console.log('Logout qlik user UserDir=' + qlikUserInfo.userDirectory + '  UserID=' + qlikUserInfo.userId);

                var requestURL = self.qlikInstanceInfo.BaseURL + qlikUserInfo.userDirectory.toLowerCase() + '/qps/user';

                //console.log('DELETE REQUEST URL', requestURL);

                return $.ajax({
                    url: requestURL,
                    method: 'DELETE',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
                }).then(function () {
                    return true;
                });

            } else {
                //console.log("User information sucessfully verified");
                return true;
            }
        });
    }


});