﻿(function () {
    Symphony.Reporting.allowedHierarchies = null;
    Symphony.Reporting.ReportBuilderTab = Ext.define('reporting.reportbuildertab', {
        alias: 'widget.reporting.reportbuildertab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                items: [{
                    split: true,
                    collapsible: true,
                    region: 'west',
                    border: false,
                    width: 400,
                    title: 'My Reports',
                    xtype: 'reporting.reportsgrid',
                    ref: 'reportsGrid',
                    listeners: {
                        cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            if (grid.headerCt.getGridColumns()[columnIndex].name != 'delete') {
                                me.getReport(record.get('id'), record.get('name'), record.data);
                            }
                        },
                        addclick: function () {
                            new Symphony.Reporting.ReportTemplatePicker({
                                modal: true,
                                listeners: {
                                    templateselect: Ext.bind(me.onTemplateSelect, me)
                                }
                            }).show();
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    itemId: 'reporting.reporteditorcontainer'
                }],
                listeners: {
                    // delayed load
                    activate: {
                        fn: function (panel) {
                            panel.find('xtype', 'reporting.reportsgrid')[0].refresh();

                            Symphony.Ajax.request({
                                method: 'GET',
                                url: '/services/reporting.svc/hierarchies/' + Symphony.User.id,
                                success: function (args) {
                                    var allowedHierarchies = {};

                                    for (var i = 0; i < args.data.length; i++) {
                                        var codeName = args.data[i].Key;
                                        var ids = args.data[i].Value;

                                        allowedHierarchies[codeName] = {};

                                        for (var j = 0; j < ids.length; j++) {
                                            allowedHierarchies[codeName][ids[j]] = true;
                                        }

                                        Symphony.Reporting.allowedHierarchies = allowedHierarchies;

                                        Symphony.fire("hierarchies_loaded", allowedHierarchies);
                                    }
                                }
                            });
                        },
                        single: true
                    }
                }
            });
            this.callParent(arguments);
        },
        activate: function () {
            this.find('xtype', 'reporting.reportsgrid')[0].refresh();
        },
        onTemplateSelect: function (reportTemplate) {
            var me = this;
            var report = {
                name: 'New ' + reportTemplate.name,
                id: 0,
                reportTemplateId: reportTemplate.id
            }
            me.getReport(report.id, report.name, report);
        },
        getReport: function (id, name, report) {
            var me = this;

            Ext.Msg.wait('Please wait while the report template is loaded...', 'Loading...');

            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/reporting.svc/reporttemplate/' + report.reportTemplateId,
                success: function (result) {
                    me.addPanel(id, name, result.data);

                }
            });
        },
        addPanel: function (id, name, reportTemplate) {
            var me = this;
            var tabPanel = this.getComponent('reporting.reporteditorcontainer');
            var found = id && tabPanel.queryBy(function (element) {
                if (element.report && element.report.id == id) {
                    return true;
                }
                return false;
            });

            

            if (found.length > 0) {
                tabPanel.activate(found[0]);
                Ext.Msg.hide();
            } else {
                if (id) {
                    Ext.Msg.wait('Please wait while the report is loaded...', 'Loading...');

                    Symphony.Ajax.request({
                        method: 'GET',
                        url: '/services/reporting.svc/reports/' + id,
                        success: function (result) {
                            result.data.reportTemplate = reportTemplate;
                            me._addPanel(result.data, name);
                        }
                    });
                } else {
                    me._addPanel({
                        name: name,
                        id: id,
                        reportTemplate: reportTemplate,
                        scheduleType: 0,
                        scheduleHour: 0,
                        scheduleDayOfMonth: 1,
                        isSymphonyIdMode: true
                    }, name);
                }
            }
            tabPanel.doLayout();
        },
        _addPanel: function (report, name) {
            var me = this;
            var tabPanel = this.getComponent('reporting.reporteditorcontainer');
                var panel = tabPanel.add({
                    xtype: 'reporting.reporteditor',
                    popout: true,
                    closable: true,
                    activate: true,
                    title: name,
                    report: report,
                    listeners: {
                        save: function (report) {
                            me.find('xtype', 'reporting.reportsgrid')[0].refresh();
                            panel.setTitle(report.name);
                            panel.course = report;
                        },
                        saveFailed: function () {
                            me.find('xtype', 'reporting.reportsgrid')[0].refresh();
                        },
                        duplicate: function (report) {
                            me.find('xtype', 'reporting.reportsgrid')[0].refresh();
                            me._addPanel(report, report.name);
                        },
                        formready: function (form) {
                            Ext.Msg.hide();
                        }
                    }
                });
                panel.show();
        }
    });

})();