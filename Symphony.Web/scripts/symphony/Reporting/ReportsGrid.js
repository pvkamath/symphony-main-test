﻿(function () {
    Symphony.Reporting.ReportsGrid = Ext.define('reporting.reportsgrid', {
        alias: 'widget.reporting.reportsgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/reporting.svc/reports/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'isFavorite',*/
                    header: 'Fav.',
                    dataIndex: 'isFavorite',
                    width: 40,
                    renderer: Symphony.starRenderer
                }, {
                    /*id: 'name',*/
                    header: 'Name',
                    dataIndex: 'name',
                    align: 'left',
                    flex: 1
                }, {
                    /*id: 'nextRunTime',*/
                    header: 'Next Run',
                    dataIndex: 'nextRunTime',
                    width: 140
                }]
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }]
                },
                idProperty: 'id',
                deferLoad: true,
                url: url,
                colModel: colModel,
                model: 'report'
            });

            this.callParent(arguments);
        },
        getSelected: function () {
            return this.getSelectionModel().selections;
        }
    });

})();