﻿(function () {
    Symphony.Reporting.TrainingProgramPicker = Ext.define('reporting.trainingprogrampicker', {
        alias: 'widget.reporting.trainingprogrampicker',
        extend: 'reporting.customerfilterablesuperboxselect',
        fieldLabel: 'Training Program',
        name: 'trainingProgram',
        valueField: 'trainingProgramkey',
        displayField: 'name',
        customerField: 'customerId',
        idField: 'trainingProgramId',
        allowBlank: true,
        emptyText: 'Select a Training Program',
        valueNotFoundText: 'Select a Training Program',
		initComponent: function () {
		    var me = this;
		    Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
                    idProperty: 'trainingProgramkey',
		            root: 'data',
		            fields: Symphony.Definitions.reportTrainingProgram,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/TrainingPrograms/',
                        reader: new Ext.data.JsonReader({
                            idProperty: this.idProperty || null,
                            totalProperty: 'totalSize',
                            root: 'data'
                        })
                    })
		        })
		    });
		    this.callParent(arguments);
		}
	});

})();