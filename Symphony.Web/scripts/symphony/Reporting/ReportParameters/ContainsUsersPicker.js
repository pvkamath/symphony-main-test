﻿(function () {
    Symphony.Reporting.ContainsUsersPicker = Ext.define('reporting.containsuserspicker', {
        alias: 'widget.reporting.containsuserspicker',
        extend: 'symphony.dropdownbox',
        value: 0,                           // default value
        fieldLabel: 'Contains Users',       // default label
        name: 'containsUsers',
		initComponent: function () {
		    var me = this;

		    Ext.apply(this, {
		        queryMode: 'local',
		        store: new Ext.data.ArrayStore({
		            fields: ['id','text'],
		            data: [[0, 'All'], [1, 'Contains Users'], [2, 'Does Not Contain Users']]
		        }),
		        valueField: 'id',
		        displayField: 'text'
		    });

		    this.callParent(arguments);
		}
	});

})();