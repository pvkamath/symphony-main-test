﻿(function () {
    Symphony.Reporting.CourseFilterPicker = Ext.define('reporting.coursefilterpicker', {
        alias: 'widget.reporting.coursefilterpicker',
        extend: 'Ext.Panel',
        fieldLabel: 'Course Filter',
        name: 'courseFilter',
        initComponent: function () {
            var me = this;

            var key = me.isSymphonyIdMode ? 'Id' : 'key';

            Ext.apply(this, {
                border: false,
                frame: false,
                cls: 'x-panel-transparent',
                layout: 'form',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'symphony.dropdownbox',
                    queryMode: 'local',
                    name: me.name,
                    fieldLabel: me.fieldLabel,
                    value: 0,   // default value
                    store: new Ext.data.ArrayStore({
                        fields: ['id', 'text'],
                        data: [[0, 'All'], [1, 'Public'], [2, 'Training Program Only']]
                    }),
                    valueField: 'id',
                    displayField: 'text',
                    listeners: {
                        'select': function (combo, records, opts) {
                            var tpCombo = me.query('[name=' + me.name + 'TrainingProgram]')[0]
                            tpCombo.setVisible(records[0].get('id') == 2);
                        },
                        'afterrender': function () {
                            var tpCombo = me.query('[name=' + me.name + 'TrainingProgram]')[0]
                            tpCombo.setVisible(this.getValue() == 2);
                        }
                    },
                    style: 'margin-bottom:5px;'
                }, {
                    xtype: 'reporting.trainingprogrampicker',
                    fieldLabel: '',
                    hideEmptyLabel: false,
                    name: me.name + 'TrainingProgram',
                    valueField: 'trainingProgram' + key
                }]
            });

            this.callParent(arguments);
        }
    });

})();