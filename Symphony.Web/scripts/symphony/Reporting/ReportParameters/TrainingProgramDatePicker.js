﻿(function () {
    Symphony.Reporting.TrainingProgramDatePicker = Ext.define('reporting.trainingprogramdatepicker', {
        alias: 'widget.reporting.trainingprogramdatepicker',
        extend: 'Ext.ux.form.DateRangeField',
        fieldLabel: 'Training Program Date',
        name: 'trainingProgramDate',
        width: 500
	});

})();