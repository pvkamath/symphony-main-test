﻿(function () {
    Symphony.Reporting.AudiencePicker = Ext.define('reporting.audiencepicker', {
        alias: 'widget.reporting.audiencepicker',
        extend: 'reporting.customerfilterablesuperboxselect',
        fieldLabel: 'Audience',
        name: 'audience',
        valueField: 'audiencekey',
        displayField: 'audienceLevelDetail',
        customerField: 'customerId',
        idField: 'audienceId',
        allowBlank: true,
        emptyText: 'Select an Audience',
        valueNotFoundText: 'Select an Audience',
        isHierarchy: true,
        hierarchyCodeName: 'audience',
        width: 500,
		initComponent: function () {
		    var me = this;
		    Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
                    idProperty: 'audiencekey',
		            root: 'data',
		            fields: Symphony.Definitions.reportAudience,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/Audiences/',
                        reader: new Ext.data.JsonReader({
                            idProperty: this.idProperty || null,
                            totalProperty: 'totalSize',
                            root: 'data'
                        })
                    })
		        })
		    });
		    this.callParent(arguments);
		}
	});

})();