﻿(function () {
    Symphony.Reporting.JobRolePicker = Ext.define('reporting.jobrolepicker', {
        alias: 'widget.reporting.jobrolepicker',
        extend: 'reporting.customerfilterablesuperboxselect',
        fieldLabel: 'Job Role',
        name: 'jobRole',
        valueField: 'jobRolekey',
        displayField: 'jobRoleLevelDetail',
        customerField: 'customerId',
        idField: 'jobRoleId',
        allowBlank: true,
        emptyText: 'Select a Job Role',
        valueNotFoundText: 'Select a Job Role',
        width: 500,
        isHierarchy: true,
        hierarchyCodeName: 'jobrole',
		initComponent: function () {
		    var me = this;
		    Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
                    idProperty: 'jobRolekey',
		            root: 'data',
		            fields: Symphony.Definitions.reportJobRole,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/JobRoles/',
                        reader: new Ext.data.JsonReader({
                            idProperty: this.idProperty || null,
                            totalProperty: 'totalSize',
                            root: 'data'
                        })
                    })
		        })
		    });
		    this.callParent(arguments);
		}
	});

})();