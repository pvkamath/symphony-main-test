﻿(function () {
    Symphony.Reporting.Search = Ext.define('reporting.search', {
        alias: 'widget.reporting.search',
        extend: 'Ext.form.TextField',
        fieldLabel: 'Search',
        name: 'search',
        width: 500
	});

})();