﻿(function () {
    Symphony.Reporting.SupervisorPicker = Ext.define('reporting.supervisorpicker', {
        alias: 'widget.reporting.supervisorpicker',
        extend: 'reporting.customerfilterablesuperboxselect',
        fieldLabel: 'Supervisor',
        name: 'supervisor',
        valueField: 'userkey',
        displayField: 'fullName',
        displayFieldTpl: '{lastName}, {firstName} {middleName}',
        allowBlank: true,
        emptyText: 'Select a Supervisor',
        valueNotFoundText: 'Select a Supervisor',
        width: 500,
		initComponent: function () {
		    var me = this;
		    Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
                    idProperty: 'userkey',
		            root: 'data',
		            fields: Symphony.Definitions.reportUser,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/Users/', //'/services/reporting.svc/Supervisors/'
                        reader: new Ext.data.JsonReader({
                            idProperty: this.idProperty || null,
                            totalProperty: 'totalSize',
                            root: 'data'
                        })
                    })
		        })
		    });
		    this.callParent(arguments);
		}
	});

})();