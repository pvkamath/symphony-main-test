﻿(function () {
    Symphony.Reporting.LocationPicker = Ext.define('reporting.locationpicker', {
        alias: 'widget.reporting.locationpicker',
        extend: 'reporting.customerfilterablesuperboxselect',
        fieldLabel: 'Location',
        name: 'location',
        valueField: 'locationkey',
        displayField: 'locationLevelDetail',
        customerField: 'customerId',
        idField: 'locationId',
        allowBlank: true,
        emptyText: 'Select a Location',
        valueNotFoundText: 'Select a Location',
        isHierarchy: true,
        hierarchyCodeName: 'location',
		initComponent: function () {
		    var me = this;
		    Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
                    idProperty: 'locationkey',
		            root: 'data',
		            fields: Symphony.Definitions.reportLocation,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/Locations/',
                        reader: new Ext.data.JsonReader({
                            idProperty: this.idProperty || null,
                            totalProperty: 'totalSize',
                            root: 'data'
                        })
                    })
		        })
		    });
		    this.callParent(arguments);
		}
	});

})();