﻿(function () {
    Symphony.Reporting.CoursePicker = Ext.define('reporting.coursepicker', {
        alias: 'widget.reporting.coursepicker',
        extend: 'reporting.customerfilterablesuperboxselect',
        fieldLabel: 'Course',
        name: 'course',
        valueField: 'coursekey',
        displayField: 'name',
        allowBlank: true,
        emptyText: 'Select a Course',
        valueNotFoundText: 'Select a Course',
        width: 500,
		initComponent: function () {
		    var me = this;
		    Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
                    idProperty: 'coursekey',
                    root: 'data',
		            fields: Symphony.Definitions.reportCourse,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/Courses/'
                    })
		        })
		    });
		    this.callParent(arguments);
		}
	});

})();