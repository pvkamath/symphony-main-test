﻿(function () {
    Symphony.Reporting.SupervisorStatusPicker = Ext.define('reporting.supervisorstatuspicker', {
        alias: 'widget.reporting.supervisorstatuspicker',
        extend: 'symphony.dropdownbox',
        value: 0,                           // default value
        fieldLabel: 'Supervisor Status',    // default label
        name: 'supervisorStatus',
		initComponent: function () {
		    var me = this;

		    Ext.apply(this, {
		        queryMode: 'local',
		        store: new Ext.data.ArrayStore({
		            fields: ['id','text'],
		            data: [[0, 'All'], [1, 'Yes'], [2, 'No']]
		        }),
		        valueField: 'id',
		        displayField: 'text'
		    });

		    this.callParent(arguments);
		}
	});

})();