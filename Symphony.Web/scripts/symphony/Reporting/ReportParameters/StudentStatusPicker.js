﻿(function () {
    Symphony.Reporting.StudentStatusPicker = Ext.define('reporting.studentstatuspicker', {
        alias: 'widget.reporting.studentstatuspicker',
        extend: 'symphony.dropdownbox',
        value: 0,                           // default value
        fieldLabel: 'Student Status Type',  // default label
        name: 'studentStatusType',
		initComponent: function () {
		    var me = this;

		    Ext.apply(this, {
		        queryMode: 'local',
		        store: new Ext.data.ArrayStore({
		            fields: ['id','text'],
		            data: [[0, 'All'], [1, 'Active'], [2, 'Inactive'], [3, 'Leave of Absence'], [4, 'Deleted'], [5, 'Approval Pending']]
		        }),
		        valueField: 'id',
		        displayField: 'text'
		    });

		    this.callParent(arguments);
		}
	});

})();