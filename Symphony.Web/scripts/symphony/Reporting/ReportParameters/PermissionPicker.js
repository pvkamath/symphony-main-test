﻿(function () {
    Symphony.Reporting.PermissionPicker = Ext.define('reporting.permissionpicker', {
        alias: 'widget.reporting.permissionpicker',
        extend: 'Ext.ux.form.field.BoxSelect',
        fieldLabel: 'Permission',
        name: 'permission',
        valueField: 'permissionkey',
        displayField: 'permissionLevelDetail',
        allowBlank: true,
        emptyText: 'Select a Permission',
        valueNotFoundText: 'Select a Permission',
        width: 500,
		initComponent: function () {
		    var me = this;
		    /*Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
		            idProperty: 'permissionkey',
		            root: 'data',
		            fields: Symphony.Definitions.reportPermission,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/Permissions/'
                    })
		        })
		    });
            this.callParent(arguments);*/
		}
	});

})();