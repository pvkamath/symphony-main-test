﻿(function () {
    Symphony.Reporting.StudentTestResultsPicker = Ext.define('reporting.studenttestresultspicker', {
        alias: 'widget.reporting.studenttestresultspicker',
        extend: 'Ext.ux.form.field.BoxSelect',
        fieldLabel: 'Test',
        name: 'studentTestResults',
        valueField: 'testkey',
        displayField: 'testLevelDetail',
        allowBlank: true,
        emptyText: 'Select a Test',
        valueNotFoundText: 'Select a Test',
        width: 500,
		initComponent: function () {
		    var me = this;
		    Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
		            idProperty: 'testkey',
		            root: 'data',
		            fields: Symphony.Definitions.reportTest,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/Tests/',
                        reader: new Ext.data.JsonReader({
                            idProperty: this.idProperty || null,
                            totalProperty: 'totalSize',
                            root: 'data'
                        })
                    })
		        })
		    });
            this.callParent(arguments);
		}
	});

})();