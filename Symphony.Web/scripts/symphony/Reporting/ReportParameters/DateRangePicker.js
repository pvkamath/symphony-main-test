﻿(function () {
    Symphony.Reporting.DateRangePicker = Ext.define('reporting.daterangepicker', {
        alias: 'widget.reporting.daterangepicker',
        extend: 'Ext.form.CompositeField',
        fieldLabel: 'Date Range',
        initComponent: function () {
            var me = this;

            me.startDate = new Ext.form.DateField({
                xtype: 'datefield', // needed for form processing
                fieldLabel: 'Start',
                name: me.name + 'Start'
            });

            me.endDate = new Ext.form.DateField({
                xtype: 'datefield', // needed for form processing
                fieldLabel: 'End',
                name: me.name + 'End'
            });

            Ext.apply(this, {
                items: [
                    { html: '<span class="subfield-label">Start</span>' },
                    me.startDate,
                    { html: '<span class="subfield-label" style="margin-left:20px;">End</span>' },
                    me.endDate
                ]
            });

            this.callParent(arguments);
        }
    });

})();