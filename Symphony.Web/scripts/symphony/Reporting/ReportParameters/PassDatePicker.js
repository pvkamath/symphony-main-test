﻿(function () {
    Symphony.Reporting.PassDatePicker = Ext.define('reporting.passdatepicker', {
        alias: 'widget.reporting.passdatepicker',
        extend: 'Ext.ux.form.DateRangeField',
        fieldLabel: 'Pass Date',
        name: 'passDate',
        width: 500
	});

})();