﻿(function () {
    Symphony.Reporting.CourseDueDatePicker = Ext.define('reporting.courseduedatepicker', {
        alias: 'widget.reporting.courseduedatepicker',
        extend: 'Ext.ux.form.DateRangeField',
        fieldLabel: 'Course Due Date',
        name: 'courseDueDate',
        width: 500
	});

})();