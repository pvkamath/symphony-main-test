﻿(function () {
    Symphony.Reporting.TestStatusPicker = Ext.define('reporting.teststatuspicker', {
        alias: 'widget.reporting.teststatuspicker',
        extend: 'symphony.dropdownbox',
        value: 0,                    // default value
        fieldLabel: 'Test Status',   // default label
        name: 'testStatus',
        width: 500,
		initComponent: function () {
		    var me = this;

		    Ext.apply(this, {
		        queryMode: 'local',
		        store: new Ext.data.ArrayStore({
		            fields: ['id','text'],
		            data: [
                        [0, 'All'],
                        [1, 'Passed'],
                        [2, 'Not Passed'],
                        [3, 'Passed before Training Program Due Date'],
                        [4, 'Did Not Pass Before Training Program Due Date'],
                        [5, 'Passed Within 30 Days of Hire'],
                        [6, 'Passed Within 45 Days of Hire'],
                        [7, 'Passed Within 60 Days of Hire'],
                        [8, 'Passed Within 90 Days of Hire'],
                        [9, 'Not Passed Within 30 Days of Hire'],
                        [10, 'Not Passed Within 45 Days of Hire'],
                        [11, 'Not Passed Within 60 Days of Hire'],
                        [12, 'Not Passed Within 90 Days of Hire'],
                        [13, 'Passed Before Course Due Date'],
                        [14, 'Did Not Pass Before Course Due Date']
                    ]
		        }),
		        valueField: 'id',
		        displayField: 'text'
		    });

		    this.callParent(arguments);
		}
	});

})();