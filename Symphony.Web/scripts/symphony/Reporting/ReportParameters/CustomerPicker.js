﻿(function () {
    Symphony.Reporting.CustomerPicker = Ext.define('reporting.customerpicker', {
        alias: 'widget.reporting.customerpicker',
        extend: 'Ext.ux.form.field.BoxSelect',
        fieldLabel: 'Customers',
        name: 'customer',
        valueField: 'id',
        displayField: 'name',
        allowBlank: true,
        emptyText: 'Select a Customer',
        valueNotFoundText: 'Select a Customer',
        width: 500,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                queryMode: 'remote',
                triggerAction: 'all',
                queryDelay: 0,
                minChars: 1,
                bubbleEvents: ['customerChanged'],
                store: new Ext.data.JsonStore({
                    idProperty: 'id',
                    root: 'data',
                    fields: Symphony.Definitions.customer,
                    proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/customer.svc/customers/' + Symphony.User.customerId + '/networked/reporting',
                        reader: new Ext.data.JsonReader({
                            idProperty: this.idProperty || null,
                            totalProperty: 'totalSize',
                            root: 'data'
                        })
                    }),
                    listeners: {
                        load: function () {
                            if (me.store.getCount() <= 1) {
                                me.setVisible(false);
                            }
                        }
                    }
                }),
                listeners: {
                    additem: function () {
                        me.fireEvent('customerChanged', me.getCustomers());
                    },
                    newitem: function () {
                        me.fireEvent('customerChanged', me.getCustomers());
                    },
                    removeitem: function () {
                        me.fireEvent('customerChanged', me.getCustomers());
                    },
                    clear: function () {
                        me.fireEvent('customerChanged', me.getCustomers());
                    },
                    render: function () {
                        me.store.load();
                    }
                }
            });
            this.callParent(arguments);
        },
        getCustomers: function () {
            var customers = this.getValueEx();
            var customerIds = [];

            for (var i = 0; i < customers.length; i++) {
                customerIds.push(customers[i].id);
            }

            return customerIds;
        }
    });

})();