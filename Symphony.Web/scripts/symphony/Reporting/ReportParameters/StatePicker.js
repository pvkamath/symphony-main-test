﻿(function () {
    Symphony.Reporting.StatePicker = Ext.define('reporting.statepicker', {
        alias: 'widget.reporting.statepicker',
        extend: 'Symphony.Reporting.CustomerFilterableSuperBoxSelect',
        fieldLabel: 'State',
        name: 'state',
        valueField: 'name',
        displayField: 'description',
        customerField: 'customerId',
        idField: 'name',
        allowBlank: true,
        emptyText: 'Select a State',
        valueNotFoundText: 'Select a State',
        width: 500,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                mode: 'remote',
                triggerAction: 'all',
                queryDelay: 0,
                minChars: 1,
                store: new Ext.data.JsonStore({
                    idProperty: 'state',
                    root: 'data',
                    fields: Symphony.Definitions.state,
                    pageSize: 999999,
                    proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/classroom.svc/states/',
                        reader: new Ext.data.JsonReader({
                            idProperty: this.idProperty || null,
                            totalProperty: 'totalSize',
                            root: 'data'
                        })
                    }),
                    listeners: {
                        load: function () {
                            Ext.apply(me, { mode: 'local' });
                            me.doQuery(me.getRawValue());
                        }
                    }
                })
            });
            this.callParent(arguments);
        }
    });

})();