﻿(function () {
    Symphony.Reporting.ViewDatePicker = Ext.define('reporting.viewdatepicker', {
        alias: 'widget.reporting.viewdatepicker',
        extend: 'Ext.ux.form.DateRangeField',
        fieldLabel: 'View Date',
        name: 'viewDate',
        width: 500
	});

})();