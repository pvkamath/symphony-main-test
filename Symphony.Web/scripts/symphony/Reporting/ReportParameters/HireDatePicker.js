﻿(function () {
    Symphony.Reporting.HireDatePicker = Ext.define('reporting.hiredatepicker', {
        alias: 'widget.reporting.hiredatepicker',
        extend: 'Ext.ux.form.DateRangeField',
        fieldLabel: 'Hire Date',
        name: 'hireDate',
        width: 500
	});

})();