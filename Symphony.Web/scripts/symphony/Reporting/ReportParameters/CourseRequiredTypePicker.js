﻿(function () {
    Symphony.Reporting.CourseRequiredTypePicker = Ext.define('reporting.courserequiredtypepicker', {
        alias: 'widget.reporting.courserequiredtypepicker',
        extend: 'reporting.coursetypepicker',
        fieldLabel: 'Course Required Type',
        name: 'courseRequiredType'
	});

})();