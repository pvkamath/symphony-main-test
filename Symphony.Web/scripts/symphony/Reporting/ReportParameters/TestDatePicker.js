﻿(function () {
    Symphony.Reporting.TestDatePicker = Ext.define('reporting.testdatepicker', {
        alias: 'widget.reporting.testdatepicker',
        extend: 'Ext.ux.form.DateRangeField',
        fieldLabel: 'Test Date',
        name: 'testDate',
        width: 500
	});

})();