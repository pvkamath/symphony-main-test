﻿(function () {
    Symphony.Reporting.CustomerFilterableSuperBoxSelect = Ext.define('reporting.customerfilterablesuperboxselect', {
        alias: 'widget.reporting.customerfilterablesuperboxselect',
        extend: 'Ext.ux.form.field.BoxSelect',
        customerIds: [],
        isHierarchy: false,
        hierarchyCodeName: "",
        loaded: false,
        initComponent: function () {
            var me = this;
            
            Ext.apply(this, {
                pageSize: this.store.pageSize || Symphony.Settings.defaultPageSize,
                queryParam: 'searchText'
            });

            this.callParent(arguments);

            me.store.on('load', function () {
                me.loaded = true;
                me.mode = 'local';
                me.store.filter(me.displayField, 'al');
                me.filterByCustomerAndHierarchy();
            });

            Symphony.on("hierarchies_loaded", function () {
                if (me.loaded) {
                    me.filterByCustomerAndHierarchy();
                }
            });
        },

        filterByCustomerAndHierarchy: function () {
            var me = this;
            me.store.clearFilter();

            me.store.filterBy(
                function (record, id) {
                    var isValid = true; // assume valid
                    if (me.isHierarchy && me.hierarchyCodeName) {
                        if (!Symphony.Reporting.allowedHierarchies[me.hierarchyCodeName][record.get(me.idField)]) {
                            isValid = false;
                        }
                    }

                    var customerId = record.get(me.customerField);
                        
                    if (customerId && me.customerIds.length > 0) {
                        if (!isValid || me.customerIds.indexOf(customerId) < 0) {
                            isValid = false;
                        }
                    }
                        
                    return isValid;
                }
            );
            
        },
        setCustomers: function (customers) {
            this.customerIds = customers;
            if (this.loaded) {
                this.filterByCustomerAndHierarchy();
            }
        },
        getValue: function() {
            var values = this.getValueRecords();

            if (values && Ext.isArray(values)) {
                for (var i = 0; i < values.length; i++) {
                    var val = values[i];
                    if (val && Ext.isObject(val) && val.isModel) {
                        values[i] = val.get(this.valueField);
                    }
                }
            }

            return values;
        },
        setValue: function (values) {
            if (values && Ext.isArray(values)) {
                for (var i = 0; i < values.length; i++) {
                    if (values[i] && Ext.isObject(values[i]) && !values[i].isModel) {
                        var data = {};

                        data[this.valueField] = values[i].id;
                        data[this.displayField] = values[i].name;

                        values[i] = Ext.create(this.store.model.$className, data);
                    }
                }
        
                values = [values, true, true];
            }
            
            this.callParent(values);
        }
	});

})();