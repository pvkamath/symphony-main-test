﻿(function () {
    Symphony.Reporting.ClassStatusPicker = Ext.define('reporting.classstatuspicker', {
        alias: 'widget.reporting.classstatuspicker',
        extend: 'symphony.dropdownbox',
        value: 0,                           // default value
        fieldLabel: 'Class Status',         // default label
        name: 'classStatus',
		initComponent: function () {
		    var me = this;

		    Ext.apply(this, {
		        queryMode: 'local',
		        store: new Ext.data.ArrayStore({
		            fields: ['id','text'],
		            data: [[0, 'All'], [1, 'Held'], [2, 'Not Held']]
		        }),
		        valueField: 'id',
		        displayField: 'text'
		    });

		    this.callParent(arguments);
		}
	});

})();