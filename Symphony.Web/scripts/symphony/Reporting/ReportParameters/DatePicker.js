﻿(function () {
    Symphony.Reporting.DatePicker = Ext.define('reporting.datepicker', {
        alias: 'widget.reporting.datepicker',
        extend: 'Ext.ux.form.DateRangeField',
        fieldLabel: 'Date',
        name: 'date',
        width: 500
	});

})();