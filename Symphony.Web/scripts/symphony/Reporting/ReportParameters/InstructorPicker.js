﻿(function () {
    Symphony.Reporting.InstructorPicker = Ext.define('reporting.instructorpicker', {
        alias: 'widget.reporting.instructorpicker',
        extend: 'reporting.customerfilterablesuperboxselect',
        fieldLabel: 'Instructor',
        name: 'instructor',
        valueField: 'instructorkey',
        displayField: 'instructorLevelDetail',
        allowBlank: true,
        emptyText: 'Select an Instructor',
        valueNotFoundText: 'Select an Instructor',
        width: 500,
		initComponent: function () {
		    var me = this;
		    Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
		            idProperty: 'instructorkey',
		            root: 'data',
		            fields: Symphony.Definitions.user,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/Instructors/',
                        reader: new Ext.data.JsonReader({
                            idProperty: this.idProperty || null,
                            totalProperty: 'totalSize',
                            root: 'data'
                        })
                    })
		        })
		    });
            this.callParent(arguments);
		}
	});

})();