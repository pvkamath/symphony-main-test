﻿(function () {
    Symphony.Reporting.CourseTypePicker = Ext.define('reporting.coursetypepicker', {
        alias: 'widget.reporting.coursetypepicker',
        extend: 'symphony.dropdownbox',
        value: 0,                   // default value
        fieldLabel: 'Course Type',  // default label
        name: 'courseType',
		initComponent: function () {
		    var me = this;

		    Ext.apply(this, {
		        queryMode: 'local',
		        store: new Ext.data.ArrayStore({
		            fields: ['id','text'],
		            data: [[0, 'All'], [1, 'Classroom'], [2, 'Online']]
		        }),
		        valueField: 'id',
		        displayField: 'text'
		    });

			this.callParent(arguments);
		}
	});

})();