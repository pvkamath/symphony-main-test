﻿(function () {
    Symphony.Reporting.CoursePassTypePicker = Ext.define('reporting.coursepasstypepicker', {

        alias: 'widget.reporting.coursepasstypepicker',

        extend: 'symphony.dropdownbox',
        value: 0,                       // default value
        fieldLabel: 'Course Pass Type', // default label
        name: 'coursePassType',

		initComponent: function () {
		    var me = this;

		    Ext.apply(this, {
		        queryMode: 'local',
		        store: new Ext.data.ArrayStore({
		            fields: ['id','text'],
		            data: [
                        [0, 'All'],
                        [1, 'Passed'],
                        [2, 'Failed']
                    ]
		        }),
		        valueField: 'id',
		        displayField: 'text'
		    });

		    this.callParent(arguments);
		}
	});

})();