﻿(function () {
    Symphony.Reporting.ClassLocationPicker = Ext.define('reporting.classlocationpicker', {
        alias: 'widget.reporting.classlocationpicker',
        extend: 'reporting.customerfilterablesuperboxselect',
        fieldLabel: 'Class Location',
        name: 'classLocation',
        valueField: 'classlocationkey',
        displayField: 'classLocationLevelDetail',
        allowBlank: true,
        emptyText: 'Select a Class Location',
        valueNotFoundText: 'Select a Class Location',
        width: 500,
		initComponent: function () {
		    var me = this;
		    /*Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
		            idProperty: 'classlocationkey',
		            root: 'data',
		            fields: Symphony.Definitions.reportClassLocation,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/ClassLocations/'
                    })
		        })
		    });
            this.callParent(arguments);*/
		}
	});

})();