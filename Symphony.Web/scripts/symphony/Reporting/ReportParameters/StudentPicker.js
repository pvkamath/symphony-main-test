﻿(function () {
    Symphony.Reporting.StudentPicker = Ext.define('reporting.studentpicker', {
        alias: 'widget.reporting.studentpicker',
        extend: 'reporting.customerfilterablesuperboxselect',
        fieldLabel: 'Student',
        name: 'student',
        valueField: 'userkey',
        displayField: 'fullName',
        displayFieldTpl: '{lastName}, {firstName} {middleName}',
        allowBlank: true,
        emptyText: 'Select a Student',
        valueNotFoundText: 'Select a Student',
        width: 500,
		initComponent: function () {
		    var me = this;
		    Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
                    idProperty: 'userkey',
		            root: 'data',
		            fields: Symphony.Definitions.reportUser,
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/Users/',
                        reader: new Ext.data.JsonReader({
                            idProperty: this.idProperty || null,
                            totalProperty: 'totalSize',
                            root: 'data'
                        })
                    })
		        })
		    });
		    this.callParent(arguments);
		}
	});

})();