﻿(function () {
    Symphony.Reporting.GroupBySelectionPicker = Ext.define('reporting.groupbyselectionpicker', {
        alias: 'widget.reporting.groupbyselectionpicker',
        extend: 'Ext.Panel',
        fieldLabel: 'Group By',
        name: 'groupBy',
        selectionBox: null,

        initComponent: function () {
            var me = this;

            var groupByConfig = {
                setValue: function (value) {
                    me.setSelection(value);
                },
                style: 'margin-bottom:5px;',
                xtype: 'reporting.groupbypicker',
                name: me.name
            }

            if (me.help) {
               groupByConfig.help = me.help;
            }

            me.groupByBox = new Symphony.Reporting.GroupByPicker(groupByConfig);

            var key = me.isSymphonyIdMode ? 'Id' : 'key';

            me.locationBox = new Symphony.Reporting.LocationPicker({ hidden: true, name: me.name + 'Location', valueField: 'location' + key});
            me.jobRoleBox = new Symphony.Reporting.JobRolePicker({ hidden: true, name: me.name + 'JobRole', valueField: 'jobrole' + key });
            me.audienceBox = new Symphony.Reporting.AudiencePicker({ hidden: true, name: me.name + 'Audience', valueField: 'audience' + key });

            Ext.apply(this, {
                border: false,
                frame: false,
                cls: 'x-panel-transparent',
                layout: 'form',
                defaults: {
                    anchor: '100%'
                },
                items: [
                    me.groupByBox,
                    me.locationBox,
                    me.jobRoleBox,
                    me.audienceBox
                ]
            });

            this.callParent(arguments);
        },
        setSelection: function (groupId) {
            var me = this;
            if (me.locationBox) {
                me.locationBox.setVisible(groupId == 1);
            }

            if (me.jobRoleBox) {
                me.jobRoleBox.setVisible(groupId == 2);
            }

            if (me.audienceBox) {
                me.audienceBox.setVisible(groupId == 3);
            }

        }
    });

})();