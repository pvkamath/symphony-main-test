﻿(function () {
    Symphony.Reporting.ClassDatePicker = Ext.define('reporting.classdatepicker', {
        alias: 'widget.reporting.classdatepicker',
        extend: 'Ext.ux.form.field.BoxSelect',
        fieldLabel: 'Class Date',
        name: 'classDate',
        valueField: 'classdatekey',
        displayField: 'classDateLevelDetail',
        allowBlank: true,
        emptyText: 'Select a Class Date',
        valueNotFoundText: 'Select a Class Date',
        width: 500,
		initComponent: function () {
		    var me = this;
		    /*Ext.apply(this, {
		        queryMode: 'remote',
		        triggerAction: 'all',
		        queryDelay: 0,
                minChars: 1,
		        store: new Ext.data.JsonStore({
		            idProperty: 'classdatekey',
		            root: 'data',
		            fields: Symphony.Definitions.reportClassDates,
                    model: 
		            proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/reporting.svc/ClassDates/'
                    })
		        })
		    });
            this.callParent(arguments);*/
		}
	});

})();