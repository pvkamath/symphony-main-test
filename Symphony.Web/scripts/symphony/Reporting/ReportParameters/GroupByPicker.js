﻿(function () {
    Symphony.Reporting.GroupByPicker = Ext.define('reporting.groupbypicker', {
        alias: 'widget.reporting.groupbypicker',
        extend: 'symphony.dropdownbox',
        value: 1,                           // default value
        fieldLabel: 'Group By',             // default label
        name: 'groupBy',
		initComponent: function () {
		    var me = this;

		    Ext.apply(this, {
		        queryMode: 'local',
		        store: new Ext.data.ArrayStore({
		            fields: ['id','text'],
		            data: [
		                //[0, 'None'],
                        [1, Symphony.Aliases.location],
                        [2, Symphony.Aliases.jobRole], 
                        [3, Symphony.Aliases.audience]]
		        }),
		        valueField: 'id',
		        displayField: 'text'
		    });

		    this.callParent(arguments);
		}
	});

})();