﻿(function () {
    Symphony.Reporting.ReportTemplateEditor = Ext.define('reporting.reporttemplateeditor', {
        alias: 'widget.reporting.reporttemplateeditor',
        extend: 'Ext.Panel',
        report: null,
        initComponent: function () {
            var me = this;
            var reportSystemData = [];
            for (var prop in Symphony.ReportSystem) {
                if (Symphony.ReportSystem.hasOwnProperty(prop)) {
                    var val = Symphony.ReportSystem[prop];
                    reportSystemData.push([val, prop.charAt(0).toUpperCase() + prop.substring(1)]);
                }
            }

            var processCombo = function (cbo) {
                if (me.rendered) {
                    var proc = me.query("[name=codeName]")[0];
                    var url = me.query("[name=reportSystemParameters]")[0];
    
                    if (cbo.getValue() == Symphony.ReportSystem.qlik) {
                        proc.hide();
                        url.show();
                    } else {
                        proc.show();
                        url.hide();
                    } 
                }
            };

            Ext.apply(this, {
                layout: 'border',
                items: [{
                    region: 'north',
                    height: 27,
                    xtype: 'symphony.savecancelbar',
                    listeners: {
                        save: Ext.bind(function () {
                            this.saveReportTemplate();
                        }, this),
                        cancel: Ext.bind(function () {
                            this.ownerCt.remove(this);
                        }, this)
                    }
                }, {
                    bodyStyle: 'padding: 15px',
                    xtype: 'tabpanel',
                    border: false,
                    region: 'center',
                    activeTab: 0,
                    items: [{
                        xtype: 'panel',
                        border: false,
                        title: 'Basic Info',
                        //region: 'center',
                        frame: true,
                        //autoHeight: true,
                        autoScroll: true,
                        layout: {
                            type: 'vbox',
                            align: 'stretch',
                            pack: 'start'
                        },
                        items: [{
                            height: 150,
                            xtype: 'form',
                            name: 'reporttemplateformcontainer',
                            ref: '../reportTemplateForm',
                            labelWidth: 120,
                            border: false,
                            frame: false,
                            cls: 'x-panel-transparent',
                            items: [{
                                xtype: 'textfield',
                                fieldLabel: 'Name',
                                name: 'name',
                                width: 350
                            }, {
                                xtype: 'textarea',
                                fieldLabel: 'Description',
                                name: 'description',
                                width: 350
                            }, {
                                xtype: 'symphony.dropdownbox',
                                mode: 'local',
                                fieldLabel: 'Report System',
                                name: 'reportSystem',
                                store: new Ext.data.ArrayStore({
                                    fields: ['id', 'text'],
                                    data: reportSystemData
                                }),
                                width: 350,
                                valueField: 'id',
                                displayField: 'text',
                                listeners: {
                                    set: processCombo,
                                    select: processCombo
                                }
                            }, {
                                name: 'codeName',
                                fieldLabel: 'Report Procedure',
                                xtype: 'symphony.pagedcombobox',
                                url: '/services/reporting.svc/reportprocedures/',
                                model: 'reportProcedure',
                                bindingName: 'codeName',
                                valueField: 'codeName',
                                displayField: 'codeName',
                                allowBlank: false,
                                emptyText: 'Select a report procedure...',
                                valueNotFoundText: 'Select a report procedure...',
                                width: 350,
                                pageSize: 999
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'Target Url',
                                hidden: true,
                                allowBlank: false,
                                name: 'reportSystemParameters', // for qlik this just a URL...left as a generic field so we can swap it out later
                                width: 350
                            }]
                        }, {
                            xtype: 'panel',
                            flex: 1,
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },border: false,
                            frame: false,
                            cls: 'x-panel-transparent',
                            items: [{
                                xtype: 'reporting.reportentitiesgrid',
                                title: 'Report Parameters',
                                region: 'center',
                                ref: '../../../reportEntitiesGrid',
                                reportEntities: me.reporttemplate ? me.reporttemplate.reportEntities : null,
                                templateId: me.reporttemplate ? me.reporttemplate.id : 0,
                                flex: 1,
                                margins: { top: 0, right: 5, bottom: 0, left: 0 }
                            }, {
                                xtype: 'reporting.availablereportentitiesgrid',
                                title: 'Available Report Parameters',
                                region: 'east',
                                ref: '../../../availableReportEntitiesGrid',
                                flex: 1,
                                margins: { top: 0, right: 0, bottom: 0, left: 5 },
                                listeners: {
                                    addEntities: function (entities) {
                                        for (var i = 0; i < entities.length; i++) {
                                            me.reportEntitiesGrid.addEntity(entities[i]);
                                        }
                                    }
                                }
                            }]
                        }]
                    }, {
                        xtype: 'reporting.reporttemplatepermissionseditor',
                        title: 'Permissions',
                        reporttemplate: me.reporttemplate
                    }]
                }],
                listeners: {
                    afterlayout: function () {
                        if (me.reporttemplate) {
                            var reportform = me.find('name', 'reporttemplateformcontainer')[0];
                            reportform.bindValues(me.reporttemplate);
                        }
                    }
                }
            });

            this.callParent(arguments);
        },
        saveReportTemplate: function () {
            var me = this;
            var reportform = me.find('name', 'reporttemplateformcontainer')[0];
            var permsaleschannel = me.find('xtype', 'customer.saleschanneltree')[0];
            var permcustomers = me.find('xtype', 'artisan.customersgrid')[0];
            
            var values = reportform.getValues();

            if (permsaleschannel.rendered) {
                var permsaleschannelSelected = permsaleschannel.getView().getChecked();
                var selectedSalesChannels = [];
                for (var i = 0; i < permsaleschannelSelected.length; i++) {
                    selectedSalesChannels.push({ id: permsaleschannelSelected[i].get('id'), name: permsaleschannelSelected[i].get('name') });
                };
                values.reportPermittedSalesChannels = selectedSalesChannels;

                var permcustomersSelected = permcustomers.getSelectionModel().getSelection();
                var selectedCustomers = [];
                for (var i = 0; i < permcustomersSelected.length; i++) {
                    selectedCustomers.push({ id: permcustomersSelected[i].data.id, name: permcustomersSelected[i].data.name });
                }
                values.reportPermittedCustomers = selectedCustomers;
            }

            var reportformgrid = me.find('xtype', 'reporting.reportentitiesgrid')[0];
            values.reportEntities = reportformgrid.getEntityData();

            if (values.reportEntities === false) {
                Ext.Msg.alert("Error", "Cannot save when parameters have duplicate tag names.");
                return;
            }

            var hasDeprecatedEntities = false;

            for (var i = 0; i < values.reportEntities.length; i++) {
                if (values.reportEntities[i].isDeprecated) {
                    hasDeprecatedEntities = true;
                }
            }

            if (hasDeprecatedEntities) {
                Ext.Msg.confirm(
                    "Save Deprecated Parameters?",
                    "This report is using some deprecated parameters. It is recommended you restructure your report to avoid using these parameters.<br/><br/>Would you like to save your report anyway?",
                    function (buttonId) {
                        if (buttonId === 'yes') {
                            me.completeSave(me, values, reportformgrid);
                        }
                    }
                );
            } else {
                me.completeSave(me, values, reportformgrid);
            }
        },
        completeSave: function (me, values, reportformgrid) {
            Ext.Msg.wait('Please wait while this report template is saved.', 'Saving...');

            Symphony.Ajax.request({
                timeout: 240 * 1000,
                url: '/services/reporting.svc/reporttemplate/' + (me.reporttemplate && me.reporttemplate.id ? me.reporttemplate.id : '0'),
                jsonData: values,
                success: function (result) {
                    me.reporttemplate = result.data;
                    me.fireEvent('save', me.reporttemplate);
                    reportformgrid.clearDirty();
                    Ext.Msg.hide();
                },
                failure: function (result) {
                    me.fireEvent('saveFailed');
                    Ext.Msg.hide();
                    var errorMsg = result.error || 'An error occurred while saving this report template.';
                    Ext.Msg.alert('Error saving report', errorMsg);
                }
            });
        }
    });

})();

(function () {
    Symphony.Reporting.ReportTemplatePermissionsEditor = Ext.define('reporting.reporttemplatepermissionseditor', {
        alias: 'widget.reporting.reporttemplatepermissionseditor',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                frame: true,
                title: 'Permissions',
                layout: 'fit',
                selected: [],
                border: false,
                items: [{
                    xtype: 'panel',
                    cls: 'x-panel-transparent',
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        defaultMargins: 5
                    },
                    border: false,
                    frame: false,
                    items: [{
                        xtype: 'panel',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        border: false,
                        flex: 0.5,
                        items: [{
                            xtype: 'panel',
                            layout: 'fit',
                            frame: false,
                            border: false,
                            flex: 1,
                            items: [{
                                xtype: 'customer.saleschanneltree',
                                deferLoad: false,
                                checkboxes: true,
                                editable: false,
                                layout: 'fit',
                                title: 'Sales Channels',
                                listeners: {
                                    treedataloaded: function () {
                                        if (!me.reporttemplate.reportPermittedSalesChannels) return;
                                        var selected = me.reporttemplate.reportPermittedSalesChannels;
                                        var selectedIdNodes = [];
                                        for (var i = 0; i < selected.length; i++) {
                                            selectedIdNodes.push(selected[i].id);
                                        }
                                        var saleschanneltree = me.find('xtype', 'customer.saleschanneltree')[0];
                                        saleschanneltree.getRootNode().cascadeBy(function (n) {
                                            if (!n.isRoot) {
                                                var id = n.attributes.record.get('id');
                                                if (selectedIdNodes.indexOf(id) > -1) {
                                                    n.ui.toggleCheck(true);
                                                }
                                            }
                                        });
                                    }
                                }
                            }]
                        }]
                    }, {
                        xtype: 'panel',
                        border: false,
                        flex: 0.5,
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'panel',
                            layout: 'fit',
                            frame: false,
                            border: false,
                            flex: 1,
                            items: [{
                                xtype: 'artisan.customersgrid',
                                layout: 'fit',
                                pageSize: -1,
                                displayPagingInfo: false,
                                title: 'Customers',
                                listeners: {
                                    datagridloaded: function () {
                                        if (!me.reporttemplate.reportPermittedCustomers) return;
                                        this.getSelectionModel().clearSelections();
                                        var selected = me.reporttemplate.reportPermittedCustomers;
                                        for (var i = 0; i < selected.length; i++) {
                                            var recId = this.store.findExact('id', selected[i].id);
                                            if (recId > -1) {
                                                this.getSelectionModel().selectRow(recId, true);
                                            }
                                        };
                                        Symphony.Ajax.request({
                                            method: 'GET',
                                            url: '/services/reporting.svc/reporttemplatecustbysc/' + me.reporttemplate.id,
                                            success: function (result) {
                                                var selectedCustomers = result.data;
                                                var grid = me.find('xtype', 'artisan.customersgrid')[0];
                                                for (var i = 0; i < selectedCustomers.length; i++) {
                                                    var recId = grid.store.findExact('id', selectedCustomers[i].id);
                                                    if (recId > -1) {
                                                        //var record = grid.getView().getRecord(recId);
                                                        grid.getSelectionModel().selectRow(recId, true, true);
                                                        //.style.backgroundColor = '#EFEFEF';
                                                        //record.setChecked(true);
                                                        //var el = Ext.fly(grid.getView().getNode(recId)).query('.x-grid-cell-row-checker')[0];
                                                        //el.style.backgroundImage = "url(/scripts/extjs3/resources/images/default/grid/row-check-sprite-chk-dis.png)";
                                                    }
                                                };
                                            }
                                        })
                                    }
                                }
                            }]
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        }
    });


})();