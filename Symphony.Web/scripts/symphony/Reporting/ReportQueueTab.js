﻿(function () {
    Symphony.Reporting.ReportQueueTab = Ext.define('reporting.reportqueuetab', {
        alias: 'widget.reporting.reportqueuetab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'fit',
                border: false,
                items: [{
                    title: 'Reports',
                    xtype: 'reporting.reportqueuegrid',
                    ref: 'reportQueueGrid'
                }]
            });
            this.callParent(arguments);
        }
    });

})();