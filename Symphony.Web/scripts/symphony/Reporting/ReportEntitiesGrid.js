﻿(function () {
    Symphony.Reporting.ReportEntitiesGrid = Ext.define('reporting.reportentitiesgrid', {
        alias: 'widget.reporting.reportentitiesgrid',
        //extend: 'symphony.editableunpagedgrid',
        extend: 'Ext.grid.Panel',
        initComponent: function () {
            var me = this;


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false,
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        /*id: 'tagName',*/
                        header: 'Tag Name',
                        dataIndex: 'tagName',
                        width: 110,
                        editor: new Ext.form.TextField({
                            allowBlank: false
                        })
                    },
                    {
                        /*id: 'name',*/
                        header: 'Label',
                        dataIndex: 'name',
                        width: 110,
                        editor: new Ext.form.TextField({
                            allowBlank: false
                        })
                    },
                    {
                        /*id: 'description',*/
                        header: 'Tooltip',
                        dataIndex: 'description',
                        editor: new Ext.form.TextField({
                            allowBlank: false
                        }),
                        flex: 1
                    }
                ]
            });

            var store = new Ext.data.Store({
                remoteSort: false,
                model: 'reportEntity',
                reader: new Ext.data.JsonReader({
                    idProperty: "fdhsajkds"
                })
            });

            Ext.apply(this, {
                colModel: colModel,
                model: 'reportEntity',
                pageSize: 9999,
                ddGroup: 'reportEntities',
                enableDragDrop: true,
                plugins: [{
                    ptype: 'cellediting',
                    clicksToEdit: 2,
                    listeners: {
                        //beforeedit: Ext.bind(this.onTreeBeforeEdit, this),
                        //canceledit: Ext.bind(this.onTreeCancelEdit, this),
                        validateedit: function (editor, e) {
                            if (e.field == 'tagName') {
                                var v = e.value;
                                me.store.each(function (r) {
                                    if (r.get('tagName') == v && r.get('id') != e.record.get('id')) {
                                        Ext.Msg.alert("Error", "Cannot have duplicate tag names.");
                                        e.cancel = true;
                                    }
                                });
                            }
                        },
                        edit: function (editor, e) {
                            
                        }
                    }
                }],
                viewConfig: {
                    plugins: 'gridviewdragdrop'
                },
                store: store,
                deferLoad: true,
                forceValidation: true,
                
                tbar: {
                    items: [{
                        xtype: 'button',
                        text: 'Remove selected parameters from report',
                        iconCls: 'x-button-delete',
                        handler: function () {
                            me.removeEntities(me.getSelectionModel().getSelections());
                        }
                    },
                    '->',
                        '(drag/drop to re-order, double click to edit)'
                    ]
                },
                listeners: {
                    /*afterrender: {
                        single: true,
                        fn: function (grid) {
                            var ddrow = new Ext.dd.DropTarget(grid.container, {
                                ddGroup: 'reportEntities',
                                copy:false,
                                notifyDrop : function(dd, e, data){
                                    var ds = grid.store;

                                    var sm = grid.getSelectionModel();
                                    var rows = sm.getSelections();
                                    if(dd.getDragData(e)) {
                                        var cindex=dd.getDragData(e).rowIndex;
                                        if(typeof(cindex) != "undefined") {
                                            for(i = 0; i <  rows.length; i++) {
                                                ds.remove(ds.getById(rows[i].id));
                                            }
                                            ds.insert(cindex,data.selections);
                                            sm.clearSelections();
                                        }
                                    }
                                }
                            }) 
                        }
                    },*/
                    beforerender: function () {
                        if (me.reportEntities) {
                            me.load(me.reportEntities);
                        }
                    },
                    afteredit: function (e) {
                        
                    }
                }
            });

            this.callParent(arguments);

            this.getView().getRowClass = function (record, rowIndex, rp, ds) {
                if (record.get("duplicated")) {
                    return "duplicated-entity";
                }
                if (record.get("isDeprecated")) {
                    return "deprecated-entity";
                }

            }
        },
        load: function(entities) {
            for (var i = 0; i < entities.length; i++) {
                this.addEntity(entities[i]);
            }
        },
        getEntityRecords: function () {
            return this.store.getRange();
        },
        getEntityData: function () {
            var records = this.getEntityRecords();
            var data = [];
            var invalid = false;
            var tagNames = {};
            

            for (var i = 0; i < records.length; i++) {
                records[i].set('duplicated', false);

                var order = this.store.indexOf(records[i]);
                var item = records[i].data;

                item.order = order;

                data.push(item);

                if (tagNames[item.tagName]) {
                    invalid = true;
                    records[i].set('duplicated', true);
                }

                tagNames[item.tagName] = true;
            }

            this.view.refresh();

            if (invalid) {
                return false;
            }

            return data;
        },
        addEntity: function (entity) {
            
            if (entity.data) {
                entity = entity.data;
            }

            var tagName = entity.tagName;
            var index = 0;
            
            while (this.store.queryBy(function (record, id) {
                if (record.get('tagName') == tagName + (index > 0 ? index : "")) {
                    return true;
                }
                return false;
            }).length > 0) {
                index++;
            }

            this.store.add({
                name: entity.name,
                description: entity.description,
                tagName: tagName + (index > 0 ? index : ""),
                id: entity.id,
                isDeprecated: entity.isDeprecated
            });

        },
        removeEntities: function (entities) {
            for (var i = 0; i < entities.length; i++) {
                this.store.remove(this.store.getById(entities[i].id));
            }
        },
        clearDirty: function () {
            var entities = this.getEntityRecords();
            for (var i = 0; i < entities.length; i++) {
                entities[i].commit();
            }
        },
        onEditComplete: function(ed, value, startValue)
        {
            this.editing = false;
            this.lastActiveEditor = this.activeEditor;
            this.activeEditor = null;

            var r = ed.record,
                field = this.colModel.getDataIndex(ed.col);
            value = this.postEditValue(value, startValue, r, field);
            if(this.forceValidation === true || String(value) !== String(startValue)){
                var e = {
                    grid: this,
                    record: r,
                    field: field,
                    originalValue: startValue,
                    value: value,
                    row: ed.row,
                    column: ed.col,
                    cancel:false
                };
                if(this.fireEvent("validateedit", e) !== false && !e.cancel /* && String(value) !== String(startValue) */){
                    r.set(field, e.value);
                    delete e.cancel;
                    this.fireEvent("afteredit", e);
                }
            }
            this.view.focusCell(ed.row, ed.col);
        }
    });

})();


