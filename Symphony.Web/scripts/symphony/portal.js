﻿Ext.Loader.setConfig({
    enabled: false
});

Ext.onReady(function () {
    var activeCourses = [];
    var activeCourseChecker = null;

    Symphony.getViewport = function () {
        var viewportwidth;
        var viewportheight;

        // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
        if (typeof window.innerWidth != 'undefined') {
            viewportwidth = window.innerWidth;
            viewportheight = window.innerHeight;
        }
            // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
        else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth) {
            viewportwidth = document.documentElement.clientWidth;
            viewportheight = document.documentElement.clientHeight;
        }
            // older versions of IE
        else {
            viewportwidth = document.getElementsByTagName('body')[0].clientWidth;
            viewportheight = document.getElementsByTagName('body')[0].clientHeight;
        }

        return [viewportwidth, viewportheight];
    };

    var connections = {};
    
    Symphony.on = function (channel, fn) {
        if (!connections[channel]) {
            connections[channel] = [];
        }
        var cnxs = connections[channel];
        cnxs.push(fn);
    };

    Symphony.un = function (channel, fn) {
        var cnxs = connections[channel];
        if (cnxs) {
            for (var i = 0; i < cnxs.length; i++) {
                if (cnxs[i] == fn) {
                    cnxs = cnxs.splice(i, 1);
                    if (cnxs.length == 0) {
                        delete connections[channel];
                    }
                    break;
                }
            }
        }
    };

    Symphony.fire = function (channel, args) {
        var cnxs = connections[channel];
        if (cnxs) {
            for (var i = 0; i < cnxs.length; i++) {
                cnxs[i](args);
            }
        }
    };
    Symphony.Portal.showCertificate = function (o, callback) {

        var w = (790),
            h = (564),
            t = ((screen.height - h) / 2),
            l = ((screen.width - w) / 2),
            domain = window.location.host,
            path = '/Handlers/CertificateHandler.ashx?',
            params = [];

        for (var prop in o) {
            if (o.hasOwnProperty(prop)) {
                params.push(prop + '=' + o[prop]);
            }
        }

        var url = window.location.protocol + '//' + domain + path + params.join('&');

        window.open(url, 'CertificateWindow', 'height=' + h + ',width=' + w + ',top=' + t + ',left=' + l + ',toolbar=no,menubar=no,status=no,location=no,scrollbars=yes,resizable=yes');

        if (typeof (callback) == 'function') {
            callback();
        }
    }
    Symphony.Portal.popup = function (url, params, args) {
        // the whole thing goes in a timeout to ensure we're checking JS popups
        window.setTimeout(function () {
            var w = window.open(url, '_blank', params);
            args = args || {};
            args.failure = args.failure || function () { };
            args.success = args.success || function () { };

            try {
                if (typeof w == 'undefined') {
                    // Safari with popup blocker... leaves the popup window handle undefined
                    args.failure(w);
                }
                else if (w && w.closed) {
                    // This happens if the user opens and closes the client window...
                    // Confusing because the handle is still available, but it's in a "closed" state.
                    // We're not saying that the window is not being blocked, we're just saying
                    // that the window has been closed before the test could be run.
                    args.failure(w);
                }
                else if (w) {
                    // This is the actual test. The client window should be fine.
                    if (navigator.userAgent.toLowerCase().search(/chrome\//) != -1) {
                        w.onload = function () {
                            window.setTimeout(function () {
                                if (w.screenX === 0) {
                                    args.failure(w);
                                } else {
                                    args.success(w);
                                }
                            }, 100);
                        };
                    } else {
                        args.success(w);
                    }
                }
                else {
                    // Else we'll assume the window is not OK
                    args.failure(w);
                }

            } catch (err) {
                // assume failure
                args.failure(w);
            }
        }, 100);
    };

    //    Symphony.Portal.showCourseCertificate = function (o) {
    //        var w = (790);
    //        var h = (564);
    //        var t = ((screen.height - h) / 2);
    //        var l = ((screen.width - w) / 2);
    //        var domain = window.location.host;
    //        var path = '/scripts/course_certificate.html?';

    //        var params = "score=" + o.score + "&name=" + o.name + "&course=" + o.course + "&type=" + o.courseType + " Course Completed&month=" + o.month + "&day=" + o.day + "&year=" + o.year + "&customer=" + Symphony.User.customerDomain;
    //        var url = window.location.protocol + '//' + domain + path + params;
    //        window.open(url, 'CourseCertificateWindow', 'height=' + h + ',width=' + w + ',top=' + t + ',left=' + l + ',toolbar=no,menubar=no,status=no,location=no,scrollbars=yes,resizable=yes');
    //    };

    //    Symphony.Portal.showTrainingProgramCertificate = function (o) {
    //        var w = (790);
    //        var h = (564);
    //        var t = ((screen.height - h) / 2);
    //        var l = ((screen.width - w) / 2);
    //        var domain = window.location.host;
    //        var path = '/scripts/trainingprogram_certificate.html?';
    //        var params = "name=" + o.name + "&trainingProgram=" + o.trainingProgram + "&month=" + o.month + "&day=" + o.day + "&year=" + o.year + "&customer=" + Symphony.User.customerDomain;
    //        var url = window.location.protocol + '//' + domain + path + params;
    //        window.open(url, 'TrainingProgramCertificateWindow', 'height=' + h + ',width=' + w + ',top=' + t + ',left=' + l + ',toolbar=no,menubar=no,status=no,location=no,scrollbars=yes,resizable=yes');
    //    };


    Symphony.Portal.launchGTM = function (id, url) {
        if (Symphony.User.isGtmOrganizer) {
            // show the login info
            Ext.Msg.wait('Locating meeting details...', 'Please wait...');
            Symphony.Ajax.request({
                url: '/services/portal.svc/gtmlogin/' + id,
                method: 'GET',
                success: function (result) {
                    Ext.Msg.hide();
                    if (result.success) {
                        var d = result.data;
                        Ext.Msg.show({
                            title: 'Login Information',
                            msg: 'Please use the following credentials to start your meeting:<br/><br/>Email: {0}<br/>Password: {1}'.format(d.email, d.password),
                            buttons: Ext.Msg.OK
                        });
                        window.setTimeout(function () {
                            window.open(url);
                        }, 1500);
                    } else {
                        // either an exception, or more likely they're just not the organizer
                        // for this meeting; in either case, just launch the freakin url
                        window.open(url);
                    }
                }
            });
        } else {
            window.open(url);
        }
    };

    Symphony.Portal.gtmRenderer = function (id, subject, joinUrl) {
        var value = '<a href="#" onclick="Symphony.Portal.launchGTM({0},\'{1}\')">{2}</a>'.format(id, joinUrl, subject);
        return Symphony.qtipRenderer(value, subject);
    };

    /* Portal grid renderers */
    Symphony.Portal.upcomingEventTypeRenderer = function (value) {
        if (!value) { return value; }
        switch (value) {
            default:
                value = value.charAt(0).toUpperCase() + value.substring(1);
                break;
        }

        return Symphony.qtipRenderer(value, 'This event is a <b>' + value + '</b>');
    };
    Symphony.Portal.upcomingEventRenderer = function (value, meta, record, row, col, store, disableQtip) {
        // Deprecated
        _deprecate(this);

        if (!value) { return value; }
        var initialValue = value;

        if (record.get('webinarKey') && Symphony.Modules.hasModule(Symphony.Modules.GoToWebinar)) {
            var url = record.get('webinarLaunchUrl');
            // for students, this is either 1) the link for them because they've successfully been registered or
            // 2) the link for them to register if something failed
            // for instructors, link them to the GTW login page
            if (record.get('webinarRegistrationId') == '[NA]') {
                url = 'https://www2.gotomeeting.com/en_US/island/organizers/webinar/myWebinars.tmpl';
            }
            value = '<a href="' + url + '" target="_blank">' + value + '</a>';
        } else if (record.get('eventType') == 'meeting') {
            return Symphony.Portal.gtmRenderer(record.get('eventId'), value, record.get('joinUrl'));
        } else if (Symphony.PresentationType[record.get('eventType')]) {
            var id = record.get('eventId');
            var userId = record.get('userId');
            if (userId) {
                value = '<a href="#" onclick="Symphony.Portal.viewClassDetails({0});return false;">'.format(id) + value + '</a>';
            }
        } else if (record.raw && record.raw.eventTypeId == Symphony.EventType.trainingProgramClass) {
            var id = record.get('eventId');
            // TODO: if they're registered, hyperlink to the class details
            var registrationId = record.raw.details.registrationId;
            if (registrationId) {
                value = '<a href="#" onclick="Symphony.Portal.viewClassDetails({0});return false;">'.format(id) + value + '</a>';
            }
        }
        return disableQtip ? value : Symphony.qtipRenderer(value, initialValue);
    };
    Symphony.Portal.showTranscriptWindowForCurrentUser = function () {
        Symphony.Portal.showTranscriptWindow(Symphony.User.fullName)
    };
    Symphony.Portal.showTranscriptWindow = function (userFullName, userId) {
        var w = new Ext.Window({
            title: 'Transcript for ' + userFullName,
            height: 500,
            width: 600,
            modal: true,
            layout: 'fit',
            items: [{
                border: false,
                userId: userId,
                id: 'portal.transcriptgrid',
                xtype: 'portal.transcriptgrid'
            }],
            buttons: [{
                text: 'Print',
                handler: function () {

                    var tsLoaded = false, dataLoaded = false, data, ts;
                    var checkReady = function () {
                        if (dataLoaded && ts.setTranscript) {
                            ts.setTranscript(userFullName, data);
                        } else {
                            window.setTimeout(checkReady, 10);
                        }
                    };

                    ts = window.open('/scripts/transcript.html?cb=' + (new Date().getTime()));
                    Symphony.Ajax.request({
                        url: '/services/portal.svc/transcript/courses/' + (userId > 0 ? userId : ''),
                        method: 'GET',
                        success: function (result) {
                            data = result.data;
                            dataLoaded = true;
                            checkReady();
                        }
                    });
                }
            }]
        });
        w.show();
    }

    Symphony.Portal.upcomingEventOptionsRenderer = function (value, meta, record, row, col, store) {
        // Deprecated
        _deprecate(this);
        
        if (record.raw.details) {
            var details = record.raw.details;
            switch (record.raw.eventTypeId) {
                case Symphony.EventType.publicClass:
                case Symphony.EventType.registeredClass:
                    // details is a class
                    var recordDef = Ext.data.Record.create(Symphony.Definitions.course);
                    var minDate = Symphony.parseDate(details.startDate);
                    var maxDate = Symphony.parseDate(details.endDate);
                    var courseRecord = new recordDef(record.raw.details);
                    courseRecord.raw = record.raw.details;
                    courseRecord.cls = record.raw;
                    return Symphony.Portal.courseLinkRenderer(value, meta, courseRecord, null, minDate, maxDate);
                case Symphony.EventType.trainingProgramClass:
                    var recordDef = Ext.data.Record.create(Symphony.Definitions.course);
                    var tp = record.raw.extendedDetails;
                    //Symphony.Portal.trainingProgramCourseOptionsRenderer = function (value, meta, record, row, col, store, courses, trainingProgram, required) {
                    // determine which of the 4 lists the course belongs to; that list, and whether or not that is the required list, are necessary
                    var lists = ['requiredCourses', 'electiveCourses', 'optionalCourses', 'finalAssessments'];
                    var list = null;
                    var course = null;
                    var required = false;
                    for (var i = 0; i < lists.length; i++) {
                        var idx = Symphony.getIndexById(tp[lists[i]], record.raw.groupId);
                        if (idx !== null) {
                            list = lists[i];
                            course = tp[lists[i]][idx];
                            required = (i == 0);
                            break;
                        }
                    }
                    var courseRecord = new recordDef(course);
                    courseRecord.raw = course;
                    courseRecord.cls = record.raw;
                    return Symphony.Portal.trainingProgramCourseOptionsRenderer(value, meta, courseRecord, row, col, store, tp[list], tp, required);

                case Symphony.EventType.meeting:
                    return Symphony.Portal.gtmRenderer(record.get('eventId'), 'Join', record.get('joinUrl'));
                case Symphony.EventType.instructedClass:
                    return '-'; // nothing, they can't undo from here
            }
        }
        return '-';
    };
    Symphony.Portal.courseTypeRenderer = function (value, meta, record) {
        return record.get('courseTypeId') == Symphony.CourseType.classroom ?
            '<div class="grid-classroom" ext:qtip="Classroom ' + Symphony.Aliases.course + '"></div>' :
            '<div class="grid-online" ext:qtip="Online ' + Symphony.Aliases.course + '"></div>';
    };
    Symphony.Portal.trainingProgramFileRenderer = function (value, meta, record) {
        return Symphony.qtipRenderer('<a href="#">' + value + '</a>', 'Click to download this file.');
    };

    Symphony.Portal.MessageBoardRenderer = function (value, meta, record) {
        var domain = Symphony.User.customerDomain.toLowerCase();
        return record.data.messageBoardId && Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards) ?
                '<a href="#" symphony:method="launchMessageBoard" symphony:parameter="' + record.data.messageBoardId + ',' + (record.data.trainingProgramId ? record.data.trainingProgramId : 0) + ', ' + (record.data.courseTypeId ? record.data.courseTypeId : 0) + '" class="grid-messageboard" ext:qtip="Go to Message Board"></a>' :
                '';
    };

    Symphony.Portal.LaunchAssignmentsRenderer = function (value, meta, record) {
        var trainingProgramId = record.data.trainingProgramId ? record.data.trainingProgramId : 0,
            courseId = record.data.courseId ? record.data.courseId : 0,
            userId = record.data.userId ? record.data.userId : 0;

        return '<a href="#" symphony:method="launchAssignments" symphony:parameter="' + trainingProgramId + ',' + courseId + ',' + userId + '" class="grid-assignments" ext:qtip="' + (record.data.qTip ? record.data.qTip : 'View Assignments') + '"></a>';
    }

    Symphony.Portal.requiredComplete = function (trainingProgram) {
        for (var i = 0; i < trainingProgram.requiredCourses.length; i++) {
            var course = trainingProgram.requiredCourses[i];
            if (!course.completed) {
                return false;
            }
        }
        return true;
    };

    Symphony.Portal.requiredEnforceCanMoveForward = function (trainingProgram) {
        if (trainingProgram.requiredCourses.length == 0) {
            return false;
        }
        if (trainingProgram.finalAssessments.length > 0) {
            // based on the last course in the required courses list
            var finalCourse = trainingProgram.requiredCourses[trainingProgram.requiredCourses.length - 1];
            return finalCourse.enforceCanMoveForwardIndicator;
        }
        return false;
    };

    Symphony.Portal.requiredCanMoveForward = function (trainingProgram) {
        // Deprecated
        _deprecate(this);

        if (trainingProgram.requiredCourses.length == 0) {
            return true;
        }
        if (trainingProgram.finalAssessments.length > 0) {
            // based on the last course in the required courses list
            var finalCourse = trainingProgram.requiredCourses[trainingProgram.requiredCourses.length - 1];
            if (finalCourse.enforceCanMoveForwardIndicator || trainingProgram.enforceCanMoveForwardIndicator) {
                return finalCourse.canMoveForwardIndicator;
            }
        }
        return true;
    };

    Symphony.Portal.electiveComplete = function (trainingProgram) {
        var completeCount = 0;
        for (var i = 0; i < trainingProgram.electiveCourses.length; i++) {
            var course = trainingProgram.electiveCourses[i];
            if (course.completed) {
                completeCount++;
            }
        }
        return (completeCount >= trainingProgram.minimumElectives);
    };

    Symphony.Portal.assignmentTotals = function (trainingProgram) {
        var assignmentCount = {
            totalAssignments: 0,
            totalAssignmentsComplete: 0,
            totalAssignmentsFailed: 0,
            totalAssignmentsUnmarked: 0
        };

        for (var i = 0; i < trainingProgram.requiredCourses.length; i++) {
            var course = trainingProgram.requiredCourses[i];
            assignmentCount.totalAssignments += course.assignmentsInCourse;
            assignmentCount.totalAssignmentsComplete += course.assignmentsCompleted;
            assignmentCount.totalAssignmentsUnmarked += (course.assignmentsInCourse - course.assignmentsMarked);
            assignmentCount.totalAssignmentsFailed += (course.assignmentsMarked - course.assignmentsCompleted);
        }

        return assignmentCount;
    };

    Symphony.Portal.isFinalEnabled = function (tp) {
        return Symphony.Portal.requiredComplete(tp) && Symphony.Portal.electiveComplete(tp) && Symphony.Portal.requiredCanMoveForward(tp) && tp.isMinCourseWorkComplete;
    };

    Symphony.Portal.isAccreditationSurveyEnabled = function (tp) {
        for (var i = 0; i < tp.finalAssessments.length; i++) {
            var course = tp.finalAssessments[i];
            if (!course.completed) {
                return false;
            }
        }
        return true;
    };

    Symphony.Portal.getAccreditationSurveyDisabledMessage = function (tp) {
        return 'Surveys are disabled until final assessments are complete.';
    }

    Symphony.Portal.getFinalAssessmentDisabledMessage = function (tp) {
        // Deprecated
        _deprecate(this);
        var msg = 'The final assessment is disabled until ',
            assignmentTotals = Symphony.Portal.assignmentTotals(tp),
            assignmentsComplete = assignmentTotals.totalAssignments == assignmentTotals.totalAssignmentsComplete,
            reasons = '',
            response = '';

        if (!Symphony.Portal.requiredComplete(tp)) {
            reasons += 'all required ' + Symphony.Aliases.courses.toLowerCase() + ' are complete';
        }

        if (!tp.isMinCourseWorkComplete) {
            if (reasons != '') {
                reasons += Symphony.Portal.requiredCanMoveForward(tp) && Symphony.Portal.electiveComplete(tp) ? ', and ' : ', ';
            }
            reasons += 'you spend at least ' + Symphony.Classroom.durationRenderer(tp.minCourseWork) + ' in your required ' + Symphony.Aliases.courses.toLowerCase();
        }

        if (!Symphony.Portal.electiveComplete(tp)) {
            if (reasons != '') {
                reasons += Symphony.Portal.requiredCanMoveForward(tp) ? ', and ' : ', ';
            }
            reasons += 'minimum elective requirements are met';
        }

        if (!Symphony.Portal.requiredCanMoveForward(tp)) {
            if (reasons != '') {
                reasons += ', and ';
            }
            reasons += 'your instructor has enabled you to move forward';
        }

        response = msg + reasons + '.';

        if (!assignmentsComplete) {
            var assignmentMessage = '';
            if (assignmentTotals.totalAssignmentsFailed > 0) {
                assignmentMessage = 'you retry and pass ' + assignmentTotals.totalAssignmentsFailed + ' failed assignment' + (assignmentTotals.totalAssignmentsFailed == 1 ? '' : 's');
            }
            if (assignmentTotals.totalAssignmentsUnmarked > 0) {
                if (assignmentMessage) {
                    assignmentMessage += ', and ';
                }
                assignmentMessage += 'your instructor marks ' + assignmentTotals.totalAssignmentsUnmarked + ' unmarked assignment' + (assignmentTotals.totalAssignmentsUnmarked == 1 ? '' : 's');
            }
            if (!reasons) { // shouldn't happen, but just in case.
                response = msg + assignmentMessage + '.';
            } else {
                response += '<br/>Your required ' + Symphony.Aliases.courses.toLowerCase() + ' will not be counted as complete until ' + assignmentMessage + '.';
            }
        }

        if (!reasons && assignmentsComplete) {
            return 'The final assessment is disabled.';
        }

        return response;
    }


    Symphony.Portal.getPreviousClassroomCourse = function (courses, courseId, courseTypeId) {
        // Deprecated
        _deprecate(this);

        // if we are at the first course nothign before it so return null
        if (courses.length > 0 && courses[0].id == courseId && courses[0].courseTypeId == courseTypeId) {
            return null;
        }

        var lastCourse = null;

        for (var i = 0; i < courses.length; i++) {
            var course = courses[i];

            // if we hit the course we're looking for, return the previous one, if we found a previous one
            if (course.id == courseId && course.courseTypeId == courseTypeId) {
                if (lastCourse) {
                    return lastCourse;
                } else { // Must have been just online courses prior to this course, so return null
                    return null;
                }
            }

            // Store the last course if it matches the type
            if (course.courseTypeId == courseTypeId) {
                lastCourse = course;
            }
        }
    };
    Symphony.Portal.getNextClassroomCourse = function (courses, courseId, courseTypeId) {
        // Deprecated
        _deprecate(this);

        // if we are at the last course, nothing after it so return null
        if (courses.length > 0 && courses[courses.length - 1].id == courseId && courses[courses.length - 1].courseTypeId == courseTypeId) {
            return null;
        }

        var lastCourse = null;

        for (var i = courses.length - 1; i >= 0; i--) {
            var course = courses[i];

            // if we hit the course we're looking for, return the next one
            if (course.id == courseId && course.courseTypeId == courseTypeId) {
                if (lastCourse) {
                    return lastCourse;
                } else { // Must have been just online courses after this course, so return null
                    return null;
                }
            }

            // Store the last course if it matches the type
            if (course.courseTypeId == courseTypeId) {
                lastCourse = course;
            }
        }
    };

    Symphony.Portal.hasPreviousRequiredCourse = function (courses, courseId, courseTypeId) {
        // Deprecated
        _deprecate(this);

        // first, check if it's the first course in the list
        if (courses.length > 0 && courses[0].id == courseId && courses[0].courseTypeId == courseTypeId) {
            return false;
        }
        var found = false;
        for (var i = 0; i < courses.length; i++) {
            var course = courses[i];

            // if we hit the course we're looking for, break out
            if (course.id == courseId && course.courseTypeId == courseTypeId) {
                return false;
            }

            // if the course isn't complete, we have a course that needs to be done first
            if (!course.passed) {
                return true;
            }
        }
    };
    Symphony.Portal.hasPreviousCourseRequiringApproval = function (courses, courseId, courseTypeId, tpEnforceCanMoveForwardIndicator) {
        // Deprecated
        _deprecate(this);

        // first, check if it's the first course in the list
        if (courses.length > 0 && courses[0].id == courseId && courses[0].courseTypeId == courseTypeId) {
            return false;
        }

        for (var i = 0; i < courses.length; i++) {
            var course = courses[i];

            // if we hit the course we're looking for, break out
            if (course.id == courseId && course.courseTypeId == courseTypeId) {
                return false;
            }

            // if the course hasn't been flagged as able to move forward, that needs to be done first.
            // But only if the training program enforces the flag OR the course itself enforces it
            if (!course.canMoveForwardIndicator && (tpEnforceCanMoveForwardIndicator || course.enforceCanMoveForwardIndicator)) {
                return true;
            }
        }
    }

    Symphony.Portal.CourseDocumentsGrid = Ext.define('portal.coursedocumentsgrid', {
        alias: 'widget.portal.coursedocumentsgrid',
        extend: 'symphony.localgrid',
        initComponent: function () {
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        /*id: 'name',*/
                        header: 'File Name',
                        dataIndex: 'filename',
                        renderer: function (value, meta, record) {
                            return '<a href="#" onclick="Symphony.Classroom.downloadCourseFile({0})">{1}</a>'.format(record.get('id'), value);
                        }
                    },
                    { /*id: 'title',*/ header: 'Title', dataIndex: 'title', editor: new Ext.form.TextField({ allowBlank: false }) },
                    {
                        /*id: 'description',*/ header: 'Description', dataIndex: 'description', editor: new Ext.form.TextField({ allowBlank: false }),
                        flex: 1
                    }
                ]
            });

            Ext.apply(this, {
                idProperty: 'id',
                colModel: colModel,
                model: 'courseFile'
            });

            this.callParent(arguments);
        }
    });


    Ext.util.Format.datetime = function (v) {
        return Symphony.parseDate(v).formatSymphony('m/d/Y g:i a T');
    };

    Symphony.Portal.viewClassDetails = function (classId, downloads, callback) {
        var tpl = new Ext.XTemplate(
            '<div class="portal-class-details">',
            '<p><span class="name">Name:</span> <span class="value">{name}</span></p>',
            '<p><span class="name">Location:</span> <span class="value">{location}</span></p>',
            '<p><span class="name">Date(s): </span>',
                '<span class="value">',
                '<tpl for="dateList">',
                    '{startDateTime:datetime}{[xindex!=xcount ? "<br/>" : ""]}',
                 '</tpl>',
                '</span>',
            '</p>',
            '<p><span class="name">Instructor(s):</span> <span class="value">',
                '<tpl for="classInstructorList">',
                    '{firstName} {lastName}{[xindex!=xcount ? ", " : ""]}',
                '</tpl>',
            '</span></p>',
            '<p><span class="name">Description:</span> <span class="value description">{description}</span></p>',
            '</div>'
        );

        var win = null;

        var items = [{
            region: 'north',
            border: false,
            tpl: tpl,
            name: 'classdetail',
            height: 160,
            autoScroll: true
        }];

        if (downloads === false) {
            items.push({
                region: 'center',
                border: false,
                html: ''
            });
        } else {
            items.push({
                region: 'center',
                id: 'portal.coursedocumentsgrid',
                xtype: 'portal.coursedocumentsgrid',
                name: 'classfiles',
                border: false
            });
        }

        if (callback) {
            items.push({
                region: 'center',
                border: false,
                layout: 'hbox',
                layoutConfig: {
                    pack: 'center'
                },
                items: [{
                    xtype: 'button',
                    text: 'Confirm',
                    handler: function () {
                        callback('yes');
                        win.close();
                    },
                    margins: '0 15'
                }, {
                    xtype: 'button',
                    text: 'Cancel',
                    handler: function () {
                        callback('no');
                        win.close();
                    },
                    margins: '0 15'
                }]
            });
        }

        win = new Ext.Window({
            items: items,
            layout: 'border',
            modal: true,
            resizable: false,
            title: 'Class Details',
            height: downloads === false ? 230 : 405,
            width: 400,
            listeners: {
                afterrender: function (w) {
                    w.center();
                }
            }
        });

        win.show();


        Symphony.Ajax.request({
            url: '/services/portal.svc/classdetails/' + classId,
            method: 'GET',
            success: function (result) {
                win.find('name', 'classdetail')[0].update(result.data);
                var files = win.find('name', 'classfiles')[0];
                if (files) {
                    files.store.loadData(result.data.files);
                }
            }
        });
    };

    Symphony.Portal.courseNameRenderer = function (value, meta, record) {
        if (record.get('registrationId') != 0 && record.get('courseTypeId') == Symphony.CourseType.classroom) {
            // if they're registered, and its a classroom class, link the class to the details view
            return Symphony.qtipRenderer('<a class="course-link" href="#" onclick="Symphony.Portal.viewClassDetails(' + record.get('classId') + ')">' + value + '</a>', 'Click to view details for the class <b>' + value + '</b>.');
        } else {
            return Symphony.qtipRenderer(value, record.get('description') || value);
        }
    };

    Symphony.Portal.trainingProgramCourseOptionsRenderer = function (value, meta, record, row, col, store, courses, trainingProgram, required) {
        // Deprecated
        _deprecate(this);

        var endDate = trainingProgram.dueDate || trainingProgram.endDate;
        var startDate = trainingProgram.startDate;
        var unavailableReasons = [];

        if (record.get('courseTypeId') == Symphony.CourseType.online) {
            // online courses are the simplest
            if (required && (trainingProgram.enforceRequiredOrder || trainingProgram.enforceCanMoveForwardIndicator)) {

                // If this is a required course and ordering is enforced or can move forward indicators are enforced
                // and there are previous courses that are either incomplete, or require instructor approval to advance
                // then we must disabled this course.

                var previousRequiredCourse = Symphony.Portal.hasPreviousRequiredCourse(courses, record.get('id'), record.get('courseTypeId'));
                var previousCourseRequiringApproval = Symphony.Portal.hasPreviousCourseRequiringApproval(courses, record.get('id'), record.get('courseTypeId'), trainingProgram.enforceCanMoveForwardIndicator);

                // The previous courses are all complete, but there is a course that still needs instructor approval
                if (previousCourseRequiringApproval) {
                    unavailableReasons.push('You need instructor approval from a previous ' + Symphony.Aliases.course.toLowerCase() + ' in order to advance.');
                }

                // There is a previous course that is incomplete. There could be a previous course requiring approval as well
                // but we only care about that if its the last one
                if (previousRequiredCourse && unavailableReasons.length == 0) {
                    unavailableReasons.push('This ' + Symphony.Aliases.course.toLowerCase() + ' is unavailable until all prior required ' + Symphony.Aliases.courses.toLowerCase() + ' are complete.');
                }
            }
        } else {
            if (required && (trainingProgram.enforceRequiredOrder || trainingProgram.enforceCanMoveForwardIndicator)) {
                var previous = Symphony.Portal.getPreviousClassroomCourse(courses, record.get('id'), record.get('courseTypeId'));
                var next = Symphony.Portal.getNextClassroomCourse(courses, record.get('id'), record.get('courseTypeId'));
                // if the previous course is registered, they may register for the next course
                if (previous && previous.registrationId) {
                    endDate = next ? next.startDate : endDate;
                    return Symphony.Portal.courseLinkRenderer(value, meta, record, trainingProgram, Symphony.parseDate(previous.endDate), Symphony.parseDate(endDate));
                } else if (!previous && next && next.registrationId) {
                    endDate = next.startDate;
                    return Symphony.Portal.courseLinkRenderer(value, meta, record, trainingProgram, Symphony.parseDate(startDate), Symphony.parseDate(endDate));
                } else if (previous && !previous.registrationId) {
                    unavailableReasons.push('This ' + Symphony.Aliases.course.toLowerCase() + ' is unavailable until all prior required ' + Symphony.Aliases.courses.toLowerCase() + ' are registered.');
                }
            }
        }
        return Symphony.Portal.courseLinkRenderer(value, meta, record, trainingProgram, Symphony.parseDate(startDate), Symphony.parseDate(endDate), unavailableReasons);
    };
    Symphony.Portal.DynamicTimeIntervals = {};
    Symphony.Portal.displayDynamicTime = function (unit, timeRemaining, template) {
        var displayTimeRemaining = timeRemaining;

        if (unit != "minutes" && timeRemaining >= 60) {
            displayTimeRemaining = Math.ceil(timeRemaining / 60) * 60;
        }

        displayTimeRemaining = Symphony.Classroom.durationRenderer(displayTimeRemaining)

        return template.replace('[remaining]', displayTimeRemaining);
    },
    Symphony.Portal.dynamicTimeRenderer = function (type, id, timeRemaining, totalTime, template, active, callback) {
        var unit = Symphony.Classroom.getDurationUnits(totalTime),
            elementId = Ext.id(),
            elementTemplate = "<span id='{0}'>{1}</span>",
            displayTimeRemaining = timeRemaining,
            intervalId = type + '-' + id,
            updateInterval = 60000,
            display;


        if (timeRemaining <= 0 && typeof callback === 'function') {
            return callback();
        }

        if (Symphony.Portal.DynamicTimeIntervals[intervalId]) {
            clearInterval(Symphony.Portal.DynamicTimeIntervals[intervalId]);
        }

        if (active) {
            // Only update if the course or training program has been actually started.
            Symphony.Portal.DynamicTimeIntervals[intervalId] = setInterval(function () {
                var minutesToSubtract = (updateInterval / 1000) / 60,
                    elementText;

                timeRemaining = timeRemaining - minutesToSubtract;

                if (timeRemaining <= 0 && typeof callback === 'function') {
                    elementText = callback();
                    clearInterval(Symphony.Portal.DynamicTimeIntervals[intervalId]);
                } else {
                    elementText = Symphony.Portal.displayDynamicTime(unit, timeRemaining, template);
                }
                var element = Ext.get(elementId);

                if (element) {
                    element.update(elementText);
                } else {
                    // Clean up the interval since the display no longer exists.
                    clearInterval(Symphony.Portal.DynamicTimeIntervals[intervalId]);
                    delete (Symphony.Portal.DynamicTimeIntervals[intervalId]);
                }

            }, updateInterval);
        }

        return elementTemplate.format(elementId, Symphony.Portal.displayDynamicTime(unit, timeRemaining, template));

    };
    Symphony.Portal.courseLinkRenderer = function (value, meta, record, trainingProgram, minDate, maxDate, unavailableReasons) {
        // Deprecated
        _deprecate(this);

        var trainingProgramId = trainingProgram && trainingProgram.id ? trainingProgram.id : 0;
        var isOnline = record.get('courseTypeId') == Symphony.CourseType.online;
        var courseId = record.get('id');
        var classId = 0;
        if (record.cls) {
            classId = record.cls.eventId;
        }
        var minTicks = (minDate && minDate.getTime ? minDate : new Date()).getTime();
        var maxTicks = (maxDate && maxDate.getTime ? maxDate : new Date(100000 * 86400000)).getTime(); // huge date in the future

        var activeAfterDate = Symphony.parseDate(record.get('activeAfterDate'), !record.get('isUsingSession'));
        var dueDate = Symphony.parseDate(record.get('dueDate'), !record.get('isUsingSession'));
        var currentDate = new Date();
        var activeAfterMinutes = record.get('activeAfterMinutes');
        var expiresAfterMinutes = record.get('expiresAfterMinutes');

        if (!unavailableReasons) {
            unavailableReasons = [];
        }


        if (activeAfterDate && (activeAfterDate.getFullYear() >= Symphony.defaultYear)) {
            activeAfterDate = minDate;
        };

        if (dueDate && (dueDate.getFullYear() >= Symphony.defaultYear)) {
            dueDate = maxDate;
        }


        trainingProgramId = trainingProgramId || 0;
        // Online courses have several parameters that we need to deal with before launching
        // For now they are all available through GetTrainingProgramDetails.
        // We can pass them all in on this link here. 
        // Some of these parameters may apply to other online courses
        // (Courses launched through the student tab - not heavily used at the moment. Or courses launched as public tab)
        // I'm assuming we don't need to worry about public stuff. That probably wouldn't be locked down much anyway
        // if these parameters exist it's *probably* in a training program. 

        var parameters = [
                isOnline ? courseId : record.get("onlineCourseId"), // If online, use id, if class, onlineCourseId maps to the test
                trainingProgramId,
                record.get("affidavitId"),
                record.get("proctorRequiredIndicator"),
                record.get("isCourseWorkAllowed"),
                record.get("isUserValidationAllowed"),
                trainingProgram && trainingProgram.isUsingSessions && !trainingProgram.isSessionActive, // Inactive session
                trainingProgram && trainingProgram.courseUnlockModeId == Symphony.CourseUnlockMode.afterCourseCompletion && record.get('passed') && !record.get('isAssignmentRequired'), // completed course
                record.get("syllabusTypeId"),
                undefined, // bookmark, was prev undefined but needed to add padding here to add the proctorFormId
                trainingProgram ? trainingProgram.proctorFormId : 0
        ].join(',');

        if (isOnline) {
            var launchLink = '<a id="launch-link-' + trainingProgramId + "-" + courseId + '" href="#" class="course-launch-link" symphony:method="launchOnlineCourse" symphony:parameter="' + parameters + '">Launch</a>';

            record.raw.attemptCount;
            // retestMode, retries
            //if (record.json.retries != null && record.json.attemptCount >= record.json.retries) {
            //    return Symphony.qtipRenderer('Unavailable', 'Max attempts reached ({0}/{1}).'.format(record.json.attemptCount, record.json.retries));
            //}
            if (record.raw.passed && record.raw.retestMode == Symphony.RetestMode.onlyOnFailure && unavailableReasons.length == 0) {
                return Symphony.qtipRenderer('Completed', 'You have passed this ' + Symphony.Aliases.course.toLowerCase() + ', and it doesn\'t allow retries.');
            }

            if (record.get('isRelativeDueDate')) {
                if (record.get('minutesUntilExpired') <= 0 && Symphony.defaultYear && record.get("denyAccessAfterDueDateIndicator")) {
                    unavailableReasons.push('The due date for this ' + Symphony.Aliases.course.toLowerCase() + ' has passed. Contact your training program leader for more time if needed.');
                }
            }

            if (record.get('isRelativeActiveAfter')) {

                var isActive = trainingProgramId > 0 ? record.get('hasTrainingProgramStarted') : true;
                var returnLink = unavailableReasons.length == 0 ? launchLink : '';

                var unavailableReason = Symphony.Portal.dynamicTimeRenderer('course', record.get('id'), record.get('minutesUntilActive'), record.get('activeAfterMinutes'), "Available in [remaining]", isActive, function () {
                    return returnLink;
                });

                if (unavailableReasons.length == 0) {
                    return unavailableReason;
                }

                unavailableReasons.push(unavailableReason);

            } else if (activeAfterDate && activeAfterDate.getTime() > currentDate.getTime() && activeAfterDate.getFullYear() < Symphony.defaultYear) {
                var availableDate = "Available " + Symphony.dateRenderer(activeAfterDate);
                var returnLink = unavailableReasons.length == 0 ? launchLink : '';
                var unavailableReason = availableDate;

                if (record.get('isUsingSession')) {
                    availableDate += activeAfterDate.formatSymphony(' g:i a');
                    var minutesUntilActive = Math.ceil((activeAfterDate - currentDate) / 60000);

                    unavailableReason = Symphony.Portal.dynamicTimeRenderer('course', record.get('id'), minutesUntilActive, minutesUntilActive, availableDate, true, function () {
                        return returnLink;
                    });
                }

                if (unavailableReasons.length == 0) {
                    return unavailableReason;
                }
                unavailableReasons.push(unavailableReason);

            } else if (dueDate && dueDate.getTime() < currentDate.getTime() && dueDate.getFullYear() < Symphony.defaultYear && record.get("denyAccessAfterDueDateIndicator")) {
                unavailableReasons.push('The due date for this ' + Symphony.Aliases.course.toLowerCase() + ' has passed. Contact your training program leader for more time if needed.');
            } else {
                if (unavailableReasons.length == 0) {
                    return launchLink;
                }
            }


            // Override the unavailable reasons in the case of the
            // user not having a session registered as there
            // is no point in displaying any other details
            // since we don't know them. 
            if (trainingProgram && trainingProgram.id && !trainingProgram.isSessionRegistered && trainingProgram.sessionCourseId > 0) {
                unavailableReasons.splice(0, unavailableReasons.length);

                unavailableReasons.push(
                    'You are not registered in a session for this training program. Contact your administrator for assistance.'
                );
            }

            // Pulling this out of the else statement 
            // since we should always want to return if we have no unavailable reasons
            // This will make sure we catch the session registration message
            // in the event we do not have a registration for the session
            // and we have a simple course (No relative due/active after or other settings)
            if (unavailableReasons.length == 0) {
                return launchLink;
            }

            var id = Ext.id();

            setTimeout(function () {
                var target = document.getElementById(id);
                var message = "";

                for (var i = 0; i < unavailableReasons.length; i++) {
                    if (unavailableReasons[i]) {
                        message += "<li>" + unavailableReasons[i] + "</li>";
                    }
                }

                message = "<ul>" + message + "</ul>";

                Ext.QuickTips.register({
                    target: target,
                    autoHide: true,
                    text: message,
                    width: 160,
                    cls: 'unavailable-reasons'
                });

            }, 10);

            return '<span class="help-icon" id="' + id + '">Unavailable</span>';

        } else { // is a classroom course
            var hasStarted = record.get('hasStarted');
            var hasEnded = record.get('hasEnded');

            if (record.cls && record.cls.startDate) {
                hasStarted = Symphony.parseDate(record.cls.startDate) < (new Date());
            }
            if (record.cls && record.cls.endDate) {
                hasEnded = Symphony.parseDate(record.cls.endDate) < (new Date());
            }
            if (!record.get('registrationStatusId')) { // have not registered at all
                return '<a href="#" class="course-launch-link" symphony:method="register" symphony:parameter="' + courseId + ',' + classId + ',' + minTicks + ',' + maxTicks + '">Register</a>';
            }
            if (record.get('registrationStatusId') == Symphony.RegistrationStatusType.denied) {
                return '<a href="#" class="course-launch-link" symphony:method="register" symphony:parameter="' + courseId + ',' + classId + ',' + minTicks + ',' + maxTicks + '" ext:qtip="Your previous registration has been denied.">Register</a>';
            }
            if (record.get('registrationStatusId') == Symphony.RegistrationStatusType.waitList) {
                return 'Wait List';
            }
            if (record.get('registrationStatusId') == Symphony.RegistrationStatusType.awaitingApproval) {
                return '<span class="awaiting-approval">Awaiting Approval <a class="course-launch-link" href="#"  symphony:method="unregister" symphony:parameter="' + record.get('registrationId') + '"></a><span>';
            }
            if (!hasStarted) {
                // if it hasn't started according to the date, but they have a score somehow anyway,
                // show the unregister link. in reality, this doesn't happen, but during testing, the testers
                // think something's broken since they can unregister after a score is assigned.
                if (record.get('score')) {
                    return '-';
                } else {
                    if (record.cls && record.cls.isAdminRegistration) {
                        return '-';
                    }
                    return '<a class="course-launch-link" href="#" symphony:method="unregister" symphony:parameter="' + record.get('registrationId') + '">Unregister</a>';
                }
            }

            if (record.get('webinarKey') && (record.get('isStartingSoon') || hasStarted) && !hasEnded) {
                return '<a class="course-launch-link" href="' + record.get('webinarLaunchUrl') + '" target="_blank">Launch Webinar</a>';
            }
            if (hasStarted && !hasEnded) {
                return 'In Progress';
            }
            if (hasStarted && hasEnded) {
                // class is over...let them retake if they've failed, or send them along to the online test portion
                if (!record.get('passed')) {
                    // if their status is unknown, let them take the online test or survey
                    if (record.get('onlineCourseId') > 0) {
                        if (record.get('attendedIndicator')) { // only show online test if they have attended the class
                            // Use the online course parameters for the test
                            return '<a class="course-launch-link" href="#" symphony:method="launchOnlineCourse" symphony:parameter="' + parameters + '">Take Test</a>';
                        }
                    }
                    else if (record.get('surveyId') > 0 && !record.get('surveyTaken')) {
                        return '<a class="course-launch-link" href="#" symphony:method="launchOnlineCourse" symphony:parameter="' + record.get('surveyId') + ', ' + trainingProgramId + '">Take Survey</a>';
                    }
                    else {
                        return '<a class="course-launch-link" href="#" symphony:method="register" symphony:parameter="' + courseId + ',' + classId + ',' + minTicks + ',' + maxTicks + '" ext:qtip="A passing score has not yet been assigned to you." >Register</a>';
                    }
                } else {
                    if (record.get('surveyId') > 0 && !record.get('surveyTaken')) {
                        return '<a class="course-launch-link" href="#" symphony:method="launchOnlineCourse" symphony:parameter="' + record.get('surveyId') + ', ' + trainingProgramId + '">Take Survey</a>';
                    }
                }
                return '-'; // no action to do here
            }
            return '?'; // this should never show up
        }
    };
    Symphony.Portal.publicCourseNameRenderer = function (value, meta, record) {
        return Symphony.qtipRenderer(value, record.get('description'));
    };
    Symphony.Portal.courseCountRenderer = function (value, meta, record) {
        return Symphony.qtipRenderer('<a href="#">' + value + '</a>', '<b>' + value + '</b> ' + (value == 1 ? Symphony.Aliases.course.toLowerCase() + ' is' : Symphony.Aliases.courses.toLowerCase() + ' are') + ' in this training program');
    };
    Symphony.Portal.completedRenderer = function (value, meta, record) {
        // Deprecated
        _deprecate(this);

        var tpSurveyComplete = record.get('surveyTaken') && record.get('surveyId') > 0 && record.get('isSurveyMandatory') || !record.get('isSurveyMandatory') || !record.get('surveyId');
        var courseSurveysComplete = !record.get('surveysRequired');
        var assignmentsComplete = !record.get('assignmentsRequired');

        if (record.get('requiredCourses').length || record.get('electiveCourses').length) {
            if (value) {
                var id = "training_program_completed_" + Ext.id(),
                    certificateTypeId = Symphony.CertificateType.trainingProgram,
                    courseOrClassId = 0,
                    trainingProgramId = record.get('id'),
                    userId = Symphony.User.id,
                    canMoveForward = true;

                if (record.get('finalAssessments').length > 0) {
                    // TP cert is only available if the last required course is allowed to be passed
                    canMoveForward = Symphony.Portal.requiredCanMoveForward(record.raw);
                }

                if (tpSurveyComplete && courseSurveysComplete && assignmentsComplete && canMoveForward) {
                    var cert = Symphony.Portal.getCertificateBadge(id, certificateTypeId, trainingProgramId, courseOrClassId);
                    return cert;
                } else if (!assignmentsComplete) {
                    return '<img ext:qtip="Some ' + Symphony.Aliases.courses.toLowerCase() + ' require assignments yet to be completed or reviewed by an instructor. Please ensure all your assignments are complete." src="/images/bullet_white.png" />';
                } else {
                    return '<img ext:qtip="Your certificate will be available after you complete all required surveys." src="/images/bullet_white.png" />';
                }
            }
            else {
                return '<img ext:qtip="This training program is incomplete." src="/images/bullet_white.png" />';
            }
        }
        else return ''; // no courses required = no certificate
    };
    Symphony.Portal.trainingProgramRenderer = function (value, meta, record) {
        return Symphony.qtipRenderer('<a href="#" id="trainingprogram_' + record.get('id') + '">' + value + '</a>', 'Click to view the details for the <b>' + value + '</b> training program');
    };
    Symphony.Portal.valueRenderer = function (value, meta, record) {
        if (!value) {
            return value;
        }
        return Symphony.qtipRenderer(value);
    };
    Symphony.Portal.scoreRenderer = function (value, meta, record, source, trainingProgramId, userId) {
        // Deprecated
        _deprecate(this);

        if (record.get('isSurvey') && record.get('passed')) {
            return '<a class="" href="/"><span data-qtip="You have completed this survey."><img src="/images/tick.png"></span></a>';
        }

        if (!value) {
            return '-';
        }

        // don't show the score if attended is false on a classroom course with an online test
        if ((record.get('courseTypeId') == Symphony.CourseType.classroom) &&
            (record.get('onlineCourseId') > 0) &&
            !record.get('attendedIndicator')) {
            return '-';
        }

        if (record.get('passed')) {
            if (record.get('displayPrint')) {
                return Symphony.Portal.certificateRenderer(record, source, trainingProgramId, userId); // TODO: + Symphony.Portal.getPrintBadge(record, source, trainingProgramId, userId);
            } else {
                return Symphony.Portal.certificateRenderer(record, source, trainingProgramId, userId);
            }
        }
        return value;
    };

    Symphony.Portal.getPrintBadge = function (record, source, trainingProgramId, userId) {
        var artisanCourse = record.get('artisanCourseId');
        return '<img src="/images/printer.png" ext:qtip="Print" alt="Print" title="Print" />';
    };

    Symphony.Portal.getCertificateBadge = function (id, certificateTypeId, trainingProgramId, courseOrClassId, userId) {
        // Deprecated
        _deprecate(this);

        userId = userId ? userId : Symphony.User.id;

        var params = {
            certificateTypeId: certificateTypeId,
            trainingProgramId: trainingProgramId,
            courseOrClassId: courseOrClassId,
            userId: userId
        };
        var s = Ext.encode(params);
        var certificate = '<a onclick=\'Symphony.Portal.showCertificate(' + s + ')\' href="#" id="' + id + '"><img src="/images/medal_gold_3.png" ext:qtip="View Certificate" alt="View Certificate" title="View Certificate" /></a>';

        return certificate;
    },
    Symphony.Portal.assignmentRenderer = function (value, meta, record) {
        // Deprecated
        _deprecate(this);

        var assignmentTooltip = '<div class="assignmentsTooltip">' +
            '<p class="message">{0}</p><br/>' +
            '<table><tr><th>Total Assignments: </th><th>{1}</th></tr>' +
            '<tr class="info"><td><img src="/images/tick.png"/>Passed:</td><td>{2}</td></tr>' +
            '<tr class="info"><td><img src="/images/cross.png"/>Failed:</td><td>{3}</td></tr>' +
            '<tr class="info"><td><img src="/images/bullet_black.png"/>Unmarked:</td><td>{4}</td></tr>'
        '</div>';
        if (record.get('hasAssignments')) {
            var message = "",
                iconSrc = "",
                id = Ext.id(),
                tooltip;

            if (record.get('isAssignmentRequired') && record.get('isAssignmentMarked')) {
                // The assignment is marked, and the assignment is still required
                // This means the student has not scored high enough on the assignment
                iconSrc = '/images/cross.png';
                message = 'You have not successfully passed the assignments for this ' + Symphony.Aliases.course.toLowerCase() + '.';
            } else if (record.get('isAssignmentRequired') && !record.get('isAssignmentMarked') && record.get('assignmentsInCourse') != null) {
                // Assignment is required, and it is not marked. The instructor has not yet viewed the assignment
                iconSrc = '/images/bullet_black.png';
                message = 'Your assignment has not yet been marked by an instructor.';
            } else if (record.get('score') === null || record.get('assignmentsInCourse') == null) {
                // Course has yet to be launched
                iconSrc = '/images/bullet_white.png';
                message = 'Assignments have not yet been attempted for this ' + Symphony.Aliases.course.toLowerCase() + '.';
            } else {
                // No assignment required, and course has assignments, and course has been attempted. This must mean the assignment is complete
                iconSrc = '/images/tick.png';
                message = 'Your assignments have been successfully completed!';
            }

            tooltip = assignmentTooltip.format(
                message,
                record.get('assignmentsInCourse'),
                record.get('assignmentsCompleted'),
                record.get('assignmentsMarked') - record.get('assignmentsCompleted'),
                record.get('assignmentsInCourse') - record.get('assignmentsMarked'));

            record.data.generateQTip = function () {
                Ext.QuickTips.register({
                    target: id,
                    autoHide: true,
                    text: tooltip,
                    width: 160,
                    cls: 'assignmentsTooltip'
                });
            };

            return '<img id="{0}" src="{1}"/>'.format(id, iconSrc);
        } else {
            return 'N/A';
        }

    },
    Symphony.Portal.certificateRenderer = function (record, source, trainingProgramId, userId) {
        // Deprecated
        _deprecate(this);

        var o = {
            id: source + '-' + record.get('id') + '-' + record.get('courseTypeId'),
            score: record.get('score')
        };

        var cert;

        if (record.data.hasOwnProperty('certificateEnabled') && !record.get('certificateEnabled')) {
            cert = ''; // Hide the certificate if we have a record that allows toggling on or off and it is off.
        } else if (record.get('isAssignmentRequired')) {

            if (record.get('isAssignmentMarked')) {
                cert = '<img ext:qtip="You have not succesfully passed the assignments for this ' + Symphony.Aliases.course.toLowerCase() + '. " src="/images/bullet_black.png" />'
            } else {
                cert = '<img ext:qtip="Your assignment has not yet been marked by an instructor." src="/images/bullet_black.png" />';
            }

        } else if (record.get('surveyId') && record.get('isSurveyMandatory') && !record.get('surveyTaken')) {
            cert = '<img ext:qtip="Certificate will be available after survey is complete." src="/images/bullet_black.png" />';
        } else {
            var courseTypeId = record.get('courseTypeId'),
                certificateTypeId,
                courseOrClassId;

            if (courseTypeId == Symphony.CourseType.classroom) {
                certificateTypeId = Symphony.CertificateType.classroom;
                courseOrClassId = record.get('classId');
            } else if (courseTypeId == Symphony.CourseType.online) {
                certificateTypeId = Symphony.CertificateType.online;
                courseOrClassId = record.get('id');
            } else {
                if (trainingProgramId > 0) {
                    certificateTypeId = Symphony.CertificateType.trainingProgram;
                    courseOrClassId = 0;
                }
            }

            cert = Symphony.Portal.getCertificateBadge(o.id, certificateTypeId, trainingProgramId ? trainingProgramId : 0, courseOrClassId, userId ? userId : Symphony.User.id);

        }

        var t = new Ext.Template([
            '<span class="score">{score}</span>',
            cert
        ]);

        return t.apply(o);
    };
    /* end grid renderers */

    /* determine if a user has permission to a specific area in the app */
    Symphony.Portal.hasPermission = function (module) {
        // if the customer doesn't have permission, then the user doesn't have permission
        if (!Symphony.Modules.hasModule(module)) {
            return false;
        }
        // shortcut
        var u = Symphony.User;
        switch (module) {
            // everyone gets the portal                                                                                                                                                                                                                         
            case Symphony.Modules.Portal:
                return true;
            case Symphony.Modules.GoToMeeting:
                return u.isGtmOrganizer;
            case Symphony.Modules.GoToWebinar:
                return u.isGtwOrganizer;
            case Symphony.Modules.Artisan:
                return (u.isArtisanAuthor || u.isArtisanPackager || u.isArtisanViewer);
            case Symphony.Modules.Reporting:
                return (u.isReportingAnalyst || u.isReportingDeveloper || u.isReportingLocationAnalyst || u.isReportingJobRoleAnalyst)
            case Symphony.Modules.Classroom:
                return (u.isClassroomInstructor || u.isClassroomManager || u.isClassroomResourceManager || u.isClassroomSupervisor);
            case Symphony.Modules.Customer:
                return (u.isCustomerAdministrator || u.isCustomerManager || u.isCustomerJobRoleManager || u.isCustomerLocationManager);
            case Symphony.Modules.CourseAssignment:
                return (u.isTrainingAdministrator || u.isTrainingManager);
            case Symphony.Modules.VideoChat:
                return true; // If video chat assigned, everyone needs to be able to connect
            case Symphony.Modules.InstructorPortal:
                return (u.isClassroomInstructor || u.isClassroomManager || u.isClassroomResourceManager || u.isClassroomSupervisor ||
                        u.isTrainingAdministrator || u.isTrainingManager);
            case Symphony.Modules.StudentPortal:
                return true; // Anyone can take a course - consider hiding it if they have no courses
            case Symphony.Modules.MessageBoards:
                return true; // Message boards should be seen for anyone in a course, since anyone can take a course, always allow
            case Symphony.Modules.Licenses:
                return (u.isSalesChannelAdmin || u.isTrainingAdministrator);
            default:
                return (Symphony.Modules.hasModule(module));
        }
        return false;
    };

    Symphony.Portal.hasAnySettingsPermission = function () {
        //var classroomTraining = Symphony.Portal.hasPermission(Symphony.Modules.Classroom);
        //var studentMgmt = Symphony.Portal.hasPermission(Symphony.Modules.Customer);
        var courseMgmt = Symphony.Portal.hasPermission(Symphony.Modules.CourseAssignment);
        //var artisan = Symphony.Portal.hasPermission(Symphony.Modules.Artisan);
        //var reporting = Symphony.Portal.hasPermission(Symphony.Modules.Reporting);

        // right now we only have online course settings
        return (courseMgmt);
    };

    Symphony.Portal.isUserSpoofing = (Symphony.User.id != Symphony.ActualUser.id) || (Symphony.User.customerId != Symphony.ActualUser.customerId);

    /* the main portal */
    Symphony.Portal.MainBar = function (cfg) {
        var setActivePanel = function (index) {
            // get the center component of the viewport
            var main = Ext.getCmp('symphonymain');
            // get the layout
            var layout = main.getLayout();

            // set the active item as specified
            if (layout.setActiveItem) {
                layout.setActiveItem(index);
            }

            // we can have deferred activation so not every sub-tab attempts to load its content right away
            if (layout.activeItem && layout.activeItem.requiresActivation && !layout.activeItem.activated) {
                if (layout.activeItem.setActiveTab) {
                    layout.activeItem.setActiveTab(0);
                } else if (layout.activeItem.setActive) {
                    layout.activeItem.setActive(0);
                } else {
                    layout.activeItem.activate(0);
                }

                layout.activeItem.activated = true;
            }
        };
        var u = Symphony.User;
        var domain = u.customerDomain.toLowerCase();
        // determine permissions
        var classroomTraining = Symphony.Portal.hasPermission(Symphony.Modules.Classroom);
        var studentMgmt = Symphony.Portal.hasPermission(Symphony.Modules.Customer);
        var courseMgmt = Symphony.Portal.hasPermission(Symphony.Modules.CourseAssignment);
        var artisan = Symphony.Portal.hasPermission(Symphony.Modules.Artisan);
        var reporting = Symphony.Portal.hasPermission(Symphony.Modules.Reporting);
        var instructors = Symphony.Portal.hasPermission(Symphony.Modules.InstructorPortal);
        var students = Symphony.Portal.hasPermission(Symphony.Modules.StudentPortal);
        var messageBoards = Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards);
        var licenses = Symphony.Portal.hasPermission(Symphony.Modules.Licenses);


        //Saleschannel edit fails
        var i = 0;
        var buttons = [
            { name: 'portal', text: '&nbsp;Portal', enableToggle: true, toggleGroup: 'mainbar', xtype: 'button', scale: 'medium', iconCls: 'x-button-home', handler: Ext.bind(setActivePanel, this, [0]) },
            '-'
        ];

        if (classroomTraining) {
            buttons = buttons.concat([
                { name: 'classroom', text: 'Classroom<br/>Training', enableToggle: true, toggleGroup: 'mainbar', xtype: 'button', scale: 'medium', iconCls: 'x-button-pencil', handler: Ext.bind(setActivePanel, this, [1]) },
                ' '
            ]);
        }
        if (studentMgmt) {
            buttons = buttons.concat([
                { name: 'customer', text: 'Student<br/>Management', enableToggle: true, toggleGroup: 'mainbar', xtype: 'button', scale: 'medium', iconCls: 'x-button-user', handler: Ext.bind(setActivePanel, this, [2]) },
                ' '
            ]);
        }
        if (courseMgmt) {
            buttons = buttons.concat([
                { name: 'courseassignment', text: 'Course<br/>Management', enableToggle: true, toggleGroup: 'mainbar', xtype: 'button', scale: 'medium', iconCls: 'x-button-openbook', handler: Ext.bind(setActivePanel, this, [3]) },
                ' '
            ]);
        }
        if (artisan) {

            buttons = buttons.concat([
                { name: 'artisan', text: 'Artisan', enableToggle: true, toggleGroup: 'mainbar', xtype: 'button', scale: 'medium', iconCls: 'x-button-palette', handler: Ext.bind(setActivePanel, this, [4]) },
                ' '
            ]);

            /*if (window.artisanv1companies[domain] === true) {
            buttons = buttons.concat([
            {
            text: 'Artisan (v1)',
            xtype: 'button',
            cls: 'symphonybar',
            toggleGroup: 'mainbar',
            iconCls: 'x-button-palette',
            handler: function () {
            if (!Symphony.artisanWindow || Symphony.artisanWindow.closed) {
            var params = 'width=' + screen.width;
            params += ', height=' + screen.height;
            params += ', top=0,left=0,channelmode=1,directories=0,location=0,menubar=0,resizable=1,toolbar=0';
            Symphony.artisanWindow = window.open(Symphony.Portal.artisanUrl + '?customer=' + Symphony.User.customerDomain, 'Artisan', params);
            } else {
            Symphony.artisanWindow.focus();
            }
            }
            },
            ' '
            ]);
            }*/
        }
        if (licenses) {
            buttons = buttons.concat([
                { name: 'license', text: 'Licenses', enableToggle: true, toggleGroup: 'mainbar', xtype: 'button', scale: 'medium', iconCls: 'x-button-license', handler: setActivePanel.createDelegate(this, [9]) }
            ]);
        }

        if (reporting) {
            // default v1 reporting to false
            var showReportingV1 = window.location.href.indexOf('betraining.com') > -1 ? true : false;
            // default v2 reporting to true
            var showReportingV2 = window.location.href.indexOf('ocltraining.com') > -1 ? true : false;

            if (showReportingV1) {
                buttons = buttons.concat([{
                    name: 'reporting',
                    text: 'Reporting',
                    xtype: 'button',
                    scale: 'medium',
                    enableToggle: true,
                    toggleGroup: 'mainbar',
                    iconCls: 'x-button-reporting',
                    handler: function () {
                        var dims = Symphony.getViewport();
                        Symphony.reportingWindow = window.open('/CizerRedirect.aspx', 'reporting', 'height=' + dims[1] + ',width=' + (dims[0] - 15) + ',toolbar=no,menubar=no,status=no,location=no,scrollbars=yes,resizable=yes,top=0,left=0');
                    }//Ext.bind(setActivePanel, this, [5])
                }]);
            }

            if (showReportingV2) {
                buttons = buttons.concat([{
                    name: 'reporting',
                    text: 'Reporting' + (showReportingV1 ? ' (New)' : ''),
                    xtype: 'button',
                    scale: 'medium',
                    enableToggle: true,
                    toggleGroup: 'mainbar',
                    iconCls: 'x-button-reporting',
                    handler: Ext.bind(setActivePanel, this, [6])
                }]);

                // force an initial login to qlik
                try {
                    var qlikInterface = Ext.create('reporting.qlikinterface');
                    qlikInterface.loginUser();
                } catch (ex) {

                }
            }
        }

        if (instructors) {
            buttons = buttons.concat([
                { name: 'instructors', text: 'Instructors', enableToggle: true, toggleGroup: 'mainbar', xtype: 'button', scale: 'medium', iconCls: 'x-button-instructors', handler: Ext.bind(setActivePanel, this, [7]) },
                ' '
            ]);
        }

        if (students) {
            buttons = buttons.concat([
                    { name: 'students', text: 'My ' + Symphony.Aliases.courses, enableToggle: true, toggleGroup: 'mainbar', xtype: 'button', scale: 'medium', iconCls: 'x-button-students', handler: Ext.bind(setActivePanel, this, [8]) },
                    ' '
            ]);
        }

        buttons = buttons.concat(['->']);

        var transcriptButtonHandler = function () {
            Symphony.Portal.showTranscriptWindowForCurrentUser();
        };

        //        console.debug("User: %O", Symphony.User);
        //        console.debug("Actual User: %O", Symphony.ActualUser);
        //        console.debug("Is user spoofing: %s", Symphony.Portal.isUserSpoofing);
        if (Symphony.Settings.isShowRedirectButton) {
            buttons = buttons.concat([{
                text: 'Portal Settings',
                xtype: 'button',
                iconCls: 'x-button-change-portal',
                scale: 'medium',
                handler: function() {
                    Symphony.Portal.redirectPrompt();
                }
            }]);
        }


        if (Symphony.ActualUser.isSalesChannelAdmin) {
            buttons = buttons.concat([
                {
                    text: 'Transcript', //Symphony.User.fullName,
                    id: 'portal.adminbutton',
                    xtype: 'button',
                    scale: 'medium',
                    iconCls: 'x-button-user',
                    menu: {
                        items: [
                            {
                                text: 'Transcript for ' + Symphony.User.fullName,
                                iconCls: 'x-menu-page',
                                handler: transcriptButtonHandler
                            },
                            (Symphony.Portal.isUserSpoofing ?
                                {
                                    text: 'Exit User',
                                    iconCls: 'x-menu-exit',
                                    handler: function () {
                                        Symphony.Ajax.request({
                                            url: '/services/portal.svc/exituser',
                                            jsonData: {},
                                            success: function (result) {
                                                Symphony.loggingOut = true; // bypass the warning about leaving the page
                                                window.location = '/home/' + Symphony.ActualUser.customerDomain;
                                            },
                                            failure: function (result) {
                                                Ext.Msg.alert('Error', result.error);
                                            }
                                        });
                                    }
                                } :
                                {
                                    text: 'Switch User',
                                    iconCls: 'x-menu-switch-user',
                                    handler: function () {
                                        if (!Symphony.Portal.switchUserWindow) {
                                            Symphony.Portal.switchUserWindow = new Symphony.Portal.SwitchUserWindow();
                                        }
                                        Symphony.Portal.switchUserWindow.show();
                                    }
                                }
                            )
                        ]
                    }
                }
            ]);
        }
        else {
            buttons = buttons.concat([
                {
                    text: 'Transcript for<br/>' + Symphony.User.fullName,
                    id: 'portal.transcriptbutton',
                    xtype: 'button',
                    scale: 'medium',
                    iconCls: 'x-button-transcript',
                    handler: transcriptButtonHandler
                }
            ]);
        }


        buttons = buttons.concat([
            {
                text: 'Inbox',
                id: 'portal.inboxbutton',
                xtype: 'button',
                scale: 'medium',
                iconCls: 'x-button-email',
                handler: function () {
                    Symphony.Portal.showInbox();
                }
            }, ' ', {
                text: 'Meetings',
                xtype: 'button',
                scale: 'medium',
                iconCls: 'x-button-meeting',
                handler: function () {
                    if (!Symphony.Portal.meetingManager) {
                        Symphony.Portal.meetingManager = new Symphony.Portal.MeetingsWindow({
                            listeners: {
                                hide: function () {
                                    Symphony.App.query('[itemId=portal.upcomingeventsgrid]')[0].refresh();
                                }
                            }
                        });
                    }
                    Symphony.Portal.meetingManager.show();
                }
            }, ' ', {
                text: 'Help',
                xtype: 'button',
                scale: 'medium',
                iconCls: 'x-button-help',
                hidden: !Symphony.Portal.helpUrl,
                handler: function () {
                    window.open(Symphony.Portal.helpUrl, 'symphonyHelp');
                }
            }, ' ',
                Symphony.VideoChat.Status.getStatusMenu(),
            {
                text: 'Logout',
                xtype: Symphony.User.canChangePassword || Symphony.Portal.hasAnySettingsPermission() ? 'splitbutton' : 'button',
                scale: 'medium',
                iconCls: 'x-button-logout',
                handler: function () {
                    Symphony.logout();
                },
                menu: Symphony.User.canChangePassword || Symphony.Portal.hasAnySettingsPermission() ? new Ext.menu.Menu({
                    items: []
                        .concat(Symphony.User.canChangePassword ? [{
                            text: 'Change Password',
                            iconCls: 'x-menu-password',
                            handler: function () {
                                if (!Symphony.Portal.changePassword) {
                                    Symphony.Portal.changePassword = new Symphony.Portal.ChangePasswordWindow();
                                }
                                Symphony.Portal.changePassword.show();
                            }
                        }] : [])
                        .concat(Symphony.Portal.hasAnySettingsPermission() ? [{
                            text: 'Company Settings',
                            iconCls: 'x-menu-gear',
                            handler: function () {
                                //if (!Symphony.Portal.customerSettingsWindow) {
                                Symphony.Portal.customerSettingsWindow = new Symphony.Portal.CustomerSettingsWindow();
                                //}
                                Symphony.Portal.customerSettingsWindow.show();
                            }
                        }] : [])

                }) : null
            }
        ]);
        Ext.apply(this, {
            cls: 'symphonymainbar',
            
            items: buttons
        });

        var checkUnreadCount = function () {
            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/portal.svc/messages/unread/',
                failure: function () {
                    // ignored on purpose; this is just an unread message count, doesn't really matter
                    // since it's executing every 30 seconds, if there's a problem, it'll pop up a ton
                    // of messages, which is really annoying
                },
                success: function (result) {
                    Ext.getCmp('portal.inboxbutton').setText('Inbox (' + result.data + ')');
                }
            });
        };

        // for some unknown reason, in IE, the reporting iframe doesn't refresh nicely all the time
        // however, if we change *any* dom element in the page, it nicely updates
        // thus, we permanently put in this little refresh line that doesn nothing but set the text
        // back to the current value
        var _inboxButton = null;
        window.setInterval(function () {
            if (!_inboxButton) {
                _inboxButton = Ext.getCmp('portal.inboxbutton');
                if (!_inboxButton) { return; }
            }
            _inboxButton.setText(_inboxButton.getText());
        }, 1000);

        // start monitoring unread count
        window.setInterval(checkUnreadCount, 30000);
        // and go ahead and check now
        checkUnreadCount();

        Symphony.Portal.MainBar.superclass.constructor.apply(this, arguments);
    };

    Ext.define('Symphony.Portal.MainBar', {
        extend: 'Ext.Toolbar',
        alias: 'widget.symphonybar',
        constructor: Symphony.Portal.MainBar,
        initComponent: function () {
            this.hidden = Symphony.getQueryParam('toolbar') == 'false' ? true : false;
            Symphony.Portal.MainBar.superclass.initComponent.apply(this, arguments);
        }
    });

    Symphony.Portal.register = function (callback, courseId, classId, minDateTicks, maxDateTicks) {
        var minDate = minDateTicks ? new Date(parseInt(minDateTicks, 10)) : new Date();
        var maxDate = maxDateTicks ? new Date(parseInt(maxDateTicks, 10)) : new Date();
        classId = parseInt(classId, 10);
        if (classId && !isNaN(classId)) {
            Symphony.Portal.registerClass(classId, callback);
        } else {
            var window = new Symphony.Portal.ClassRegistrationWindow({
                minDate: minDate,
                maxDate: maxDate,
                courseId: courseId,
                listeners: {
                    'close': callback
                }
            });
            window.show();
        }
    };

    Symphony.Portal.unregister = function (callback, registrationId) {
        var target = '/services/portal.svc/classes/unregister/' + registrationId;
        Symphony.Ajax.request({
            url: target,
            success: function (result) {
                callback();
            }
        });
    };

    Symphony.Portal.launchMessageBoard = function (messageBoardId, trainingProgramId, courseTypeId) {
        var messageBoardWindow = new Symphony.MessageBoard.ViewWindow();

        var showCanMoveForwardButtons = this.xtype == 'instructors.instructedclassesgrid';

        messageBoardWindow.loadMessageBoardById(messageBoardId, trainingProgramId, showCanMoveForwardButtons, courseTypeId);
        messageBoardWindow.show();
    }

    Symphony.Portal.launchAssignments = function (trainingProgramId, courseId, userId, courseRecord) {
        // Find the course launch link for this course
        var launchLink = Ext.get('launch-link-{0}-{1}'.format(trainingProgramId, courseId));


        var assignmentsWindow = new Symphony.Assignments.AssignmentsWindow({
            trainingProgramId: parseInt(trainingProgramId, 10),
            courseId: parseInt(courseId, 10),
            userId: parseInt(userId, 10),
            courseRecord: courseRecord
        });

        assignmentsWindow.show();
        assignmentsWindow.center();
    }

    Symphony.Portal.TrainingProgramWindow = Ext.define('Portal.TrainingProgramWindow', {
        alias: 'widget.portal.trainingprogramwindow',
        extend: 'Ext.Window',
        trainingProgramId: 0,
        initComponent: function () {
            var me = this;

            this.width = Ext.getBody().getWidth() - 50;
            this.height = Ext.getBody().getHeight() - 50;


            Ext.apply(this, {
                title: 'Training Program Details',
                width: me.width,
                height: me.height,
                items: [{
                    xtype: 'portal.trainingprogramdetailspanel',
                    ref: 'TrainingProgramDetailsPanel',
                    trainingProgramId: me.trainingProgramId,
                    width: me.width,
                    height: me.height,
                    isShowBackButton: me.isShowBackButton,
                    returnToLibrary: function () {
                        me.hide();
                    }
                }],
                border: false,
                layout: 'fit',
                modal: true,
                listeners: {
                    show: function (w) {
                        Symphony.App.addListener('resize', w.resizeTrainingProgramWindow, w);
                    },
                    close: function (w) {
                        Symphony.App.removeListener('resize', w.resizeTrainingProgramWindow, w);
                        var trainingProgramGrid = Ext.getCmp('portal.trainingprogramsgrid');
                        if (trainingProgramGrid) {
                            trainingProgramGrid.refresh();
                        }

                        if (typeof (me.callback) == 'function') {
                            me.callback();
                        }
                    }
                }
            });

            this.callParent(arguments);
        },

        resizeTrainingProgramWindow: function () {
            var width = Ext.getBody().getWidth() - 50,
                height = Ext.getBody().getHeight() - 50;

            this.setWidth(width);
            this.setHeight(height);
        }

    });

    Symphony.Portal.showTrainingProgram = function (trainingProgramId, callback, isShowBackButton) {
        var window = new Symphony.Portal.TrainingProgramWindow({
            trainingProgramId: trainingProgramId,
            callback: callback,
            isShowBackButton: isShowBackButton
        });
        window.show();
    };

    Symphony.Portal.showInbox = function () {
        if (!Symphony.Portal.inbox) {
            Symphony.Portal.inbox = new Symphony.Portal.MessagesWindow();
        }
        Symphony.Portal.inbox.show();
    };

    Symphony.Portal.launchOnlineCourse_NewUI = function (launchLink, bookmark) {
        var launchUrl = launchLink.fullUrl;

        if (Symphony.Portal.isUserSpoofing) {
            var regExTp = /\!TrainingProgramId\|[0-9]+/;
            var regExUser = /=UserId\|[0-9]+\!/;
            launchUrl = launchUrl.replace(regExTp, "!TrainingProgramID|0");
            launchUrl = launchUrl.replace(regExUser, "=UserId|" + Symphony.ActualUser.id + "!");
        }

        if (bookmark) {
            launchUrl += '#' + bookmark;
        }

        var params = [
            'height=' + (screen.height - 90), // a little padding to make the chrome fit onscreen
            'width=' + (screen.width - 18),
            'top=0',
            'left=0',
            'toolbar=no',
            'menubar=no',
            'status=no',
            'location=no',
            'scrollbars=yes',
            'resizable=yes'
        //'fullscreen=yes'
        ].join(',');

        Ext.getBody().mask('Your ' + Symphony.Aliases.course.toLowerCase() + ' is currently in progress. Exit your ' + Symphony.Aliases.course + ' to re-activate the portal.').dom.style.zIndex = '99999';

        var w = window.open(launchUrl, 'course_window' + Math.round(Math.random() * 10000000), params);

        var activeCourseInterval = window.setInterval(function () {
            if (w.closed) {
                window.clearInterval(activeCourseInterval);

                Ext.getBody().unmask();

                Symphony.checkAuthStatus(function () {
                    // run any registered callbacks
                    // if we are still logged in. 
                    while (window.courseCompletionCallbacks && courseCompletionCallbacks.length > 0) {
                        callback = courseCompletionCallbacks.pop();
                        callback();
                    }
                });
            }
            // if courses are active, no timeouts get hit
            Symphony.resetTimeout();
        }, 1000);
    };

    Symphony.Portal.launchOnlineCourse = function (courseId, trainingProgramId, affidavitId, proctorRequiredIndicator, isCourseWorkAllowed, isUserValidationAllowed, isSessionExpired, isCompleted, syllabusTypeId, bookmark, proctorFormId) {
        _deprecate(this);
        if (_ui(this)) {
            _log("Calling LaunchOnlineCourse_NewUi from LaunchOnlineCourse. Replace the old function with the new.");
            // Launch link will be the first and only parameter
            var launchLink = courseId;
            Symphony.Portal.launchOnlineCourse_NewUI(launchLink);
            return;
        }
        // Set default values - everything that gets passed from a link handler will be in string format
        // so we need defaults if they don't exist, and need to convert the strings to proper values
        // 
        // also needs to work if values are directly passed in.

        affidavitId = typeof (affidavitId) === 'undefined' ? 0 : parseInt(affidavitId, 10);
        syllabusTypeId = typeof (syllabusTypeId) === 'undefined' ? 0 : parseInt(syllabusTypeId, 10);

        if (typeof (proctorRequiredIndicator) === 'undefined') {
            proctorRequiredIndicator = false;
        } else if (proctorRequiredIndicator !== true && proctorRequiredIndicator !== false) { // string passed in
            proctorRequiredIndicator = proctorRequiredIndicator.toLowerCase() === 'true' ? true : false;
        }

        if (typeof (isCourseWorkAllowed) === 'undefined') {
            isCourseWorkAllowed = true;
        } else if (isCourseWorkAllowed !== true && isCourseWorkAllowed !== false) { // string passed in
            isCourseWorkAllowed = isCourseWorkAllowed.toLowerCase() === 'true' ? true : false;
        }

        if (typeof (isUserValidationAllowed) === 'undefined') {
            isUserValidationAllowed = true;
        } else if (isUserValidationAllowed !== true && isUserValidationAllowed !== false) { // string passed in
            isUserValidationAllowed = isUserValidationAllowed.toLowerCase() === 'true' ? true : false;
        }

        if (typeof (isSessionExpired) === 'undefined') {
            isSessionExpired = false;
        } else if (isSessionExpired !== true && isSessionExpired !== false) { // string passed in
            isSessionExpired = isSessionExpired.toLowerCase() === 'true' ? true : false;
        }

        if (typeof (isCompleted) === 'undefined') {
            isCompleted = false;
        } else if (isCompleted !== true && isCompleted !== false) { // string passed in
            isCompleted = isCompleted.toLowerCase() === 'true' ? true : false;
        }


        // This starts the online course launch
        // Grabs details about the course and training program first
        // to ensure the course can be launched
        var userId = Symphony.User.id;

        if (!trainingProgramId) {
            trainingProgramId = 0;
        }

        var launchWizard = new Symphony.Portal.CourseLaunchWizard({
            userId: Symphony.User.id,
            customerId: Symphony.User.customerId,
            onlineCourseId: courseId,
            trainingProgramId: trainingProgramId,
            affidavitId: affidavitId,
            isUserValidationAllowed: isUserValidationAllowed,
            isCourseWorkAllowed: isCourseWorkAllowed,
            proctorRequiredIndicator: proctorRequiredIndicator,
            isReviewMode: isSessionExpired || isCompleted,
            syllabusTypeId: syllabusTypeId,
            bookmark: bookmark,
            proctorFormId: proctorFormId
        }).show();

    };

    Symphony.Portal.launchOnlineCourseStart = function (userId, courseId, trainingProgramId, bookmark) {

        var activeCourseChecker = null;

        if (Symphony.Portal.isUserSpoofing) {
            trainingProgramId = 0;
            userId = Symphony.ActualUser.id;
        }

        // Update the start time for the training program if not started yet
        var updateUrl = trainingProgramId > 0 ?
                    '/services/courseassignment.svc/trainingprograms/userstarttime/' + trainingProgramId + '/' + courseId + '/' :
                    '/services/courseassignment.svc/courses/userstarttime/' + courseId + '/';

        Symphony.Ajax.request({
            url: updateUrl,
            method: 'POST'
        });

        var urlTemplate = Symphony.Portal.launchPageBase + "&registration=UserId|{UserID}!CourseId|{CourseID}!TrainingProgramId|{TrainingProgramID}";
        var url = urlTemplate
                    .replace("{UserID}", userId)
                    .replace("{CourseID}", courseId)
                    .replace("{TrainingProgramID}", trainingProgramId || 0);

        if (bookmark) {
            url += '#' + bookmark;
        }

        var params = [
            'height=' + (screen.height - 90), // a little padding to make the chrome fit onscreen
            'width=' + (screen.width - 18),
            'top=0',
            'left=0',
            'toolbar=no',
            'menubar=no',
            'status=no',
            'location=no',
            'scrollbars=yes',
            'resizable=yes'
        //'fullscreen=yes'
        ].join(',');




        Ext.getBody().mask('Your ' + Symphony.Aliases.course.toLowerCase() + ' is currently in progress. Exit your ' + Symphony.Aliases.course + ' to re-activate the portal.').dom.style.zIndex = '99999';

        var w = window.open(url, 'course_window' + Math.round(Math.random() * 10000000), params);

        var activeCourseInterval = window.setInterval(function () {

            if (w.closed) {
                window.clearInterval(activeCourseInterval);
                
                Ext.getBody().unmask();
                Symphony.checkAuthStatus(function () {
                    // run any registered callbacks
                    // if we are still logged in. 
                    if (courseCompletionCallbacks && courseCompletionCallbacks.length) {
                        while (courseCompletionCallbacks.length > 0) {
                            callback = courseCompletionCallbacks.pop();
                            callback();
                        }
                    }
                });
            }
            // if courses are active, no timeouts get hit
            Symphony.resetTimeout();
        }, 1000);
    };

    var courseCompletionCallbacks = [];
    Symphony.Portal.onCourseCompletion = function (fn) {
        courseCompletionCallbacks.push(fn);
    };


    Symphony.Portal.TrainingProgramSummary = Ext.define('Portal.TrainingProgramSummary', {
        extend: 'Ext.Panel',
        alias: 'widget.portal.trainingprogramsummary',
        targetUserId: null,
        initComponent: function () {
            var tpSurveyComplete = this.data.surveyTaken && this.data.surveyId > 0 && this.data.isSurveyMandatory || !this.data.isSurveyMandatory || !this.data.surveyId,
                courseSurveysComplete = !this.data.surveysRequired,
                assignmentsComplete = !this.data.assignmentsRequired,
                id = "training_program_cert_" + this.data.id,
                certificateTypeId = Symphony.CertificateType.trainingProgram,
                trainingProgramId = this.data.id,
                courseOrClassId = 0,
                userId = this.targetUserId ? this.targetUserId : Symphony.User.id,
                canMoveForward = Symphony.Portal.requiredCanMoveForward(this.data),
                certificate = '',
                survey = '';

            // Using certificate generated serverside
            if (this.data.displayCompleted && this.data.displayCompleted.link) {
                this.data.displayCompleted.link.domId = 'certificate_link_' + Ext.id();
            }
            certificate = Symphony.Renderer.displayIconRenderer(this.data.displayCompleted);

            survey = this.data.surveyId > 0 ?
                    (
                        this.data.completed ?
                        '<a href="#" id="survey_link_' + this.data.id + '" symphony:method="launchOnlineCourse" symphony:parameter="' + this.data.surveyId + ', ' + this.data.id + '">Take Survey</a>' :
                        '<img ext:qtip="Complete the training program before taking the survey." src="/images/bullet_white.png" />'
                    ) : '<img ext:qtip="This training program does not have a survey." src="/images/bullet_white.png" />'


            if (_ui(this)) {
                survey = Symphony.Renderer.displayIconRenderer(this.data.displaySurvey);

                _log("Cleanup - Symphony.Portal.TrainingProgramSummary - Replacing certificate and survey with server side logic. Remove old method of determining these states.");
            }

            var templateInfo = {
                requiredCount: this.data.requiredCourses.length,
                minimumElectives: this.data.minimumElectives,
                electiveCount: this.data.electiveCourses.length,
                optionalCount: this.data.optionalCourses.length,
                assessmentSet: this.data.finalAssessments.length > 0 ? 'Set' : 'Not Set',
                certificate: certificate,
                survey: survey
            };

            var template = new Ext.Template([
                '<div class="summary">',
                    '<h2>Summary</h2>',
                    '<div class="group required">',
                        '<h3>Required</h3>',
                        '<p><span id="requiredCount" class="count">{requiredCount}</span></p>',
                        '</div>',
                    '<div class="group elective">',
                        '<h3>Elective</h3>',
                        '<p><span>Take</span> <span id="electiveMin">{minimumElectives}</span> <span>of</span> <span class="count">{electiveCount}</span></p>',
                    '</div>',
                    '<div class="group optional">',
                        '<h3>Optional</h3>',
                        '<p><span class="count">{optionalCount}</span></p>',
                    '</div>',
                    '<div class="group assessment">',
                        '<h3>Assessment</h3>',
                        '<p><span class="set">{assessmentSet}</span></p>',
                    '</div>',
                    '<div id="certContainer" class="group certificate">',
                        '<h3>Certificate</h3>',
                        '<p><span class="cert">{certificate}</span></p>',
                    '</div>',
                    '<div id="surveyContainer" class="group survey">',
                        '<h3>Survey</h3>',
                        '<p><span class="survey">{survey}</span></p>',
                    '</div>',
                '</div>'
            ]);
            Ext.apply(this, {
                html: template.apply(templateInfo)
            });

            this.callParent(arguments);
        },
        onRender: function () {
            Symphony.Portal.TrainingProgramSummary.superclass.onRender.apply(this, arguments);
            var me = this;

            var surveyLinkId = 'survey_link_' + this.data.id;
            window.setTimeout(Ext.bind(function () {
                var link = Ext.get(surveyLinkId);
                if (link) {
                    link.on('click', Ext.bind(function () {
                        Symphony.Portal.onCourseCompletion(function () {
                            me.fireEvent('surveytaken');
                        })
                        Symphony.Portal.launchOnlineCourse(this.data.surveyId, this.data.id);
                    }, this));
                }

                if (this.data.displayCompleted && this.data.displayCompleted.link) {
                    var certificateLink = Ext.get(me.data.displayCompleted.link.domId);
                    if (certificateLink) {
                        certificateLink.on('click', Ext.bind(function (e) {
                            Symphony.LinkHandlers.click(this, e, this.data.displayCompleted, this.data);
                        }, this));
                    }
                }

            }, this), 1);
        }
    });


    Symphony.Portal.ClassDetailsGrid = Ext.define('portal.classdetailsgrid', {
        alias: 'widget.portal.classdetailsgrid',
        extend: 'symphony.simplegrid',
        initComponent: function () {

            var url = _ui(this) ? this.url : '/services/portal.svc/classes/?courseId=' + this.courseId + '&minDate=' + this.minDate.formatSymphony('m/d/Y') + '&maxDate=' + this.maxDate.formatSymphony('m/d/Y') + '&userId=' + Symphony.User.id;
            if (_ui(this)) {
                _log("Cleanup - Symphony.Portal.ClassDetailsGrid - Remove conditional on url. Set serverside now.");
            }

            var columns;
            if (_ui(this)) {
                columns = [
                    {
                        /*id: 'name',*/ header: 'Class Name', dataIndex: 'className', width: 140, align: 'left',
                        flex: 1
                    },
                    {  header: 'Start Date', dataIndex: 'displayStartDate', width: 100, sortable: false, renderer: Symphony.Renderer.displayDateRenderer },
                    {  header: 'Location', dataIndex: 'displayLocation', width: 100, renderer: Symphony.Renderer.displayMessageRenderer },
                    {  header: 'Register', dataIndex: 'displayRegisterLink', renderer: Symphony.Renderer.displayLinkRenderer }
                ];
                _log("Cleanup - Symphony.Portal.ClassDetailsGrid - Remove conditional on columns shown. Always use new ui columns.");
            } else {
                columns = [
                    {  header: 'Class Name', dataIndex: 'className', width: 140, align: 'left' },
                    { header: 'Start Date', dataIndex: 'startDate', width: 100, sortable: false, renderer: Symphony.dateRenderer },
                    { header: 'Time', dataIndex: 'startDate', width: 80, sortable: false, align: 'center', renderer: Symphony.naturalTimeRendererTZ },
                    {
                        header: 'Location', dataIndex: 'location', width: 100, renderer:
                          function (value, meta, record) {
                              if (record.get('webinarKey')) {
                                  return 'Online';
                              }
                              var venue = record.get('venueName') || '', room = record.get('classRoomName') || '';

                              var location = venue + ', ' + room;
                              if (location.length > 2) {
                                  return location;
                              }
                              return 'Not specified';
                          }
                    },
                    {
                        /*id: 'register',*/
                        header: 'Register',
                        name: 'register',
                        dataIndex: 'displayRegisterLink',
                        renderer: function (value, meta, record) {
                            if (record.get('isAdminRegistration') || record.get('registrationClosed')) {
                                return 'Closed'
                            }

                            var dt = Symphony.parseDate(record.get('startDate'));
                            var diff = (new Date()).getTime() - dt.getTime();
                            if (diff > 1000 * 60 * 60 * 24 * 7 /* 7 days */) {
                                return '-';
                            }

                            return '<a href="#">Register</a>';
                        }
                    }
                ]
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    width: 65,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: columns
            });

            Ext.apply(this, {
                url: url,
                colModel: colModel,
                model: 'klass'
            });
            this.callParent(arguments);
        }
    });

    Symphony.Portal.TranscriptAccordion = Ext.define('portal.transcriptaccordion', {
        alias: 'widget.portal.transcriptaccordion',
        extend: 'Ext.panel.Panel',
        initComponent: function () {
            var me = this;

            var url = '/services/portal.svc/trainingprograms/list/';

            var proxy = new Ext.data.HttpProxy({
                method: 'GET',
                url: url,
                reader: new Ext.data.JsonReader({
                    idProperty: 'none',
                    totalProperty: 'totalSize',
                    root: 'data'
                })
            });

            var store = new Ext.data.GroupingStore({
                proxy: proxy,
                model: 'trainingProgram',
                listeners: {
                    load: function (store) {
                        me.loadTranscript(store);
                    }
                }
            });

            Ext.apply(this, {
                items: [],
                layout: 'accordion',
                listeners: {
                    render: function () {
                        store.load();
                    }
                }
            });

            this.callParent();
        },
        loadTranscript: function (store) {
            var me = this;
            
            me.suspendLayout = true;

            store.each(function (record, i) {
                var panel = Ext.create('Ext.panel.Panel', {
                    title: record.get('name'),
                    items: []
                });

                me.add(panel);
            });

            me.suspendLayout = false;

            me.doLayout();

        }
    });

    Symphony.Portal.TranscriptGrid = Ext.define('portal.transcriptgrid', {
        alias: 'widget.portal.transcriptgrid',
        extend: 'Ext.grid.Panel',
        /*plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl: Ext.XTemplate('<div id="training-program-{id}"></div>')
        }],*/
        rowCollapsedCls: Ext.baseCSSPrefix + 'grid-row-collapsed',
        initComponent: function () {
            var me = this,
                userId = me.userId > 0 ? me.userId : Symphony.User.id,
                url = '/services/portal.svc/trainingprograms/list/all/' + userId + '/';

            var proxy = new Ext.data.HttpProxy({
                method: 'GET',
                url: url,
                reader: new Ext.data.JsonReader({
                    idProperty: 'courseKey',
                    totalProperty: 'totalSize',
                    root: 'data'
                })
            });

            var store = new Ext.data.Store({
                proxy: proxy,
                model: 'transcriptEntryCourse',
                autoLoad: true,
                remoteSort: true,
                listeners: {
                    load: function (store) {
                        var publicEntry = Ext.create('transcriptEntryCourse', {
                            name: 'Public ' + Symphony.Aliases.courses,
                            id: 0
                        });

                        store.insert(0, publicEntry);
                    }
                }
            });

            var columns = [{
                width: 24,
                lockable: false,
                sortable: false,
                resizable: false,
                draggable: false,
                hideable: false,
                menuDisabled: true,
                renderer: function (value, metadata, record) {
                    if (me.isCourseRecord(record)) {
                        return '';
                    }

                    metadata.tdCls = metadata.tdCls + ' ' + Ext.baseCSSPrefix + 'grid-cell-special';
                    
                    return '<div class="' + Ext.baseCSSPrefix + 'grid-row-expander" role="presentation"></div>';
                },
                processEvent: function (type, view, cell, rowIndex, cellIndex, e, record) {
                    if (me.isCourseRecord(record)) {
                        return;
                    }

                    if (type == "mousedown" && e.getTarget('.' + Ext.baseCSSPrefix + 'grid-row-expander')) {
                        me.toggleRow(rowIndex, record);
                    }
                }
            }, {
                header: Symphony.Aliases.course,
                dataIndex: 'courseName',
                align: 'left',
                flex: 1,
                tdCls: 'name',
                renderer: function (value, metadata, record) {
                    if (me.isCourseRecord(record)) {
                        return value;
                    }
                    
                    return '<div class="' + Ext.baseCSSPrefix + 'grid-group-title">' + record.data.name + '</div>';
                }
            }, {
                header: 'Passed',
                dataIndex: 'passed',
                renderer: function (value) {
                    if (value) { return '<img src="/images/tick.png" />'; }
                    return '';
                },
                width: 45
            }, {
                header: 'Attempt Date',
                dataIndex: 'attemptDate',
                width: 80,
                renderer: function (value, meta, record) {
                    if (!me.isCourseRecord(record)) {
                        return '';
                    }
                    if (value) {
                        return Symphony.dateRenderer(value);
                    } else if (record.get('startDate')) {
                        return Symphony.dateRenderer(record.get('startDate'));
                    }
                }
            }, {
                header: 'Total Time',
                dataIndex: 'attemptTime',
                renderer: function (value, meta, record) {
                    if (!me.isCourseRecord(record)) {
                        return '';
                    }
                    return parseInt(Math.ceil(value / 60)) + ' min';
                },
                width: 80
            }, {
                header: 'Score',
                dataIndex: 'score',
                width: 40
            }, {
                header: 'Certificate',
                dataIndex: 'displayCertificate',
                renderer: function (value, meta, record) {
                    if (me.isCourseRecord(record)) {
                        cert = value;
                    } else {
                        cert = record.raw.displayCompleted;
                    }

                    return Symphony.Renderer.displayIconRenderer(cert);
                },
                align: 'right',
                width: 80
            }];

            Ext.apply(this, {
                cls: 'transcript',
                columns: columns,
                model: 'transcriptEntryCourse',
                store: store
            });
            this.callParent();

            this.getView().getRowClass = function (record, rowIndex, rowParams, store) {
                var expandedStateCls = record.isExpanded ? '' : me.rowCollapsedCls;
                var rowTypeCls = record.get('courseTypeId') > 0 ? 'symphony-course' : 'symphony-training-program';
                var courseTpCls = record.get('courseTypeId') > 0 ? 'symphony-course-tp-' + record.get('trainingProgramId') : '';

                return rowTypeCls + ' ' + courseTpCls + ' ' + expandedStateCls;
            }

        },
        isCourseRecord: function(record) {
            return record.get('courseTypeId') > 0;
        },
        toggleRow: function (rowIdx, record) {
            var me = this,
                view = me.view,
                rowNode = view.getNode(rowIdx),
                row = Ext.fly(rowNode, '_rowExpander'),
                isCollapsed = row.hasCls(me.rowCollapsedCls),
                addOrRemoveCls = isCollapsed ? 'removeCls' : 'addCls';

            record.isExpanded = isCollapsed;

            row[addOrRemoveCls](me.rowCollapsedCls);

            var id = record.get('id');

            if (!record.hasExpanded) {
                var userId = me.userId > 0 ? me.userId : Symphony.User.id,
                    url = '/services/portal.svc/transcript/courses/' + userId + '/' + id + '/',
                    loadMask = new Ext.LoadMask(me.getEl(), { msg: "Loading..." });

                loadMask.show();

                Symphony.Ajax.request({
                    url: url,
                    method: 'GET',
                    success: function (result) {
                        var courses = [];
                        for (var i = 0; i < result.data.length; i++) {
                            var courseData = result.data[i];
                            delete (result.data[i].id);
                            courses.push(Ext.create('transcriptEntryCourse', courseData));
                        }
                        me.store.insert(rowIdx + 1, courses);
                    },
                    complete: function () {
                        loadMask.hide();
                    }
                });
            } else {
                var courseRowElements = Ext.DomQuery.select('.symphony-course-tp-' + id);

                for (var i = 0; i < courseRowElements.length; i++) {
                    if (record.isExpanded) {
                        Ext.fly(courseRowElements[i]).removeCls('hidden');
                    } else {
                        Ext.fly(courseRowElements[i]).addCls('hidden');
                    }
                }
            }

            record.hasExpanded = true;
        }
    });


    Symphony.Portal.MessagesGrid = Ext.define('portal.messagesgrid', {
        alias: 'widget.portal.messagesgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var url = '/services/portal.svc/messages/';


            var renderer = function (value, meta, record) {
                return record.get('isRead') ? Symphony.Portal.valueRenderer(value) : Symphony.Portal.valueRenderer('<b>' + value + '</b>');
            };

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: renderer
                },
                columns: [
                    { header: 'Subject', dataIndex: 'subject' },
                    {
                        /*id: 'body',*/ header: 'Body', dataIndex: 'body', width: 140,
                        flex: 1
                    },
                    {
                        header: 'Date',
                        dataIndex: 'createdOn',
                        renderer: function (value, meta, record) {
                            return record.get('isRead') ? Symphony.dateRenderer(value) : '<b>' + Symphony.dateRenderer(value) + '</b>';
                        },
                        align: 'center'
                    },
                    { /*id: 'sender',*/ header: 'Sender', dataIndex: 'sender' }
                ]
            });

            var me = this;
            Ext.apply(this, {
                url: url,
                colModel: colModel,
                model: 'message',
                tbar: {
                    items: [{
                        id: 'portal.deletemessagebutton',
                        xtype: 'button',
                        disabled: true,
                        iconCls: 'x-button-message-delete',
                        text: 'Delete Message',
                        handler: function () {
                            //var message = me.getSelectionModel().getSelected().data;
                            var record = me.getSelectionModel().getSelection()[0];
                            if (record) {
                                var id = record.get('id');
                                Ext.Msg.confirm('Are you sure?', 'You are about to delete the selected message. Are you sure?', function (btn) {
                                    if (btn == 'yes') {
                                        Symphony.Ajax.request({
                                            url: '/services/portal.svc/messages/delete/' + (id || 0),
                                            success: function (result) {
                                                me.refresh();
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }, {
                        id: 'portal.deleteallmessagesbutton',
                        xtype: 'button',
                        disabled: false,
                        iconCls: 'x-button-message-deleteall',
                        text: 'Delete All Messages',
                        handler: function () {
                            Ext.Msg.confirm('Are you sure?', 'This will completely clear your inbox. Are you sure?', function (btn) {
                                if (btn == 'yes') {
                                    Symphony.Ajax.request({
                                        url: '/services/portal.svc/messages/delete/',
                                        success: function (result) {
                                            me.refresh();
                                        }
                                    });
                                }
                            });
                        }
                    }, '-', '<span style="font-style:italic">Double click a message to view</span>']
                },
                viewConfig: {
                    listeners: {
                        'refresh': function () {
                            Ext.getCmp('portal.deletemessagebutton').disable();
                        }
                    }
                },
                listeners: {
                    rowselect: function (grid, rowIndex, e) {
                        Ext.getCmp('portal.deletemessagebutton').enable();
                    },
                    itemdblclick: function (grid, record, item, rowIndex, e) {
                        var viewer = new Symphony.Portal.MessageViewer({
                            modal: true,
                            message: record.data
                        });
                        viewer.show();

                        // mark the message as read
                        var target = '/services/portal.svc/messages/read/' + record.get('id');
                        Symphony.Ajax.request({
                            url: target
                        });
                        me.refresh();
                    }
                }
            });
            this.callParent(arguments);
        }
    });


    Symphony.Portal.GTMUserList = Ext.define('portal.gtmuserlist', {
        alias: 'widget.portal.gtmuserlist',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var url = '/services/portal.svc/users/';

            var sm = new Ext.selection.CheckboxModel({
                checkOnly: true,
                sortable: true,
                header: ' ' // kills the select-all checkbox
            });
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [

                    { header: 'First Name', dataIndex: 'firstName' },
                    { header: 'Last Name', dataIndex: 'lastName' },
                    { header: 'Email', dataIndex: 'email' },
                    { header: 'Supervisor', dataIndex: 'supervisor' }
                ]
            });
            this.selectionPaging = new Ext.ux.grid.RowSelectionPaging();
            this.selectionPaging.setSelections(this.value);
            Ext.apply(this, {
                idProperty: 'id',
                tbar: {
                    items: [
                        'Internal Invitees'
                    ]
                },
                url: url,
                colModel: colModel,
                model: 'user',
                selModel: sm,
                plugins: [this.selectionPaging]
            });
            this.callParent(arguments);
        }
    });


    Symphony.Portal.ExternalGTMUserList = Ext.define('portal.externalgtmuserlist', {
        alias: 'widget.portal.externalgtmuserlist',
        extend: 'Ext.Panel',
        initComponent: function () {
            Ext.apply(this, {
                tbar: {
                    items: ['External Invitees']
                },
                items: [{
                    layout: 'fit',
                    style: 'border:0',
                    xtype: 'textarea',
                    value: this.value,
                    emptyText: 'Enter email addresses, separated by commas, e.g. john.smith@gmail.com, jane.doe@hotmail.com'
                }]
            });
            this.callParent(arguments);
        },
        getSelections: function () {
            var text = this.find('xtype', 'textarea')[0].getValue().replace(/\s/g, '');
            if (text) {
                return text.split(',');
            }
            return [];
        }
    });


    Symphony.Portal.MeetingsGrid = Ext.define('portal.meetingsgrid', {
        alias: 'widget.portal.meetingsgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var url = '/services/portal.svc/meetings/';
            var me = this;


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { header: 'Subject', dataIndex: 'subject', width: 140, align: 'left' },
                    { header: 'Start Date', dataIndex: 'startDateTime', width: 80, renderer: Symphony.dateRenderer },
                    { header: 'Start Time', dataIndex: 'startDateTime', renderer: Symphony.timeRenderer },
                    { header: 'Call Information', dataIndex: 'conferenceCallInfo', width: 110 },
                    {
                        header: 'Options',
                        width: 50,
                        renderer: function (value, meta, record) {
                            return Symphony.Portal.gtmRenderer(record.get('id'), 'Join', record.get('joinUrl'));
                        }
                    }
                ]
            });

            var toolbarItems = [];

            if (Symphony.User.isGtmOrganizer) {
                toolbarItems.push({
                    xtype: 'button',
                    iconCls: 'x-button-meeting-add',
                    text: 'New Meeting',
                    handler: function () {
                        me.editMeeting();
                    }
                });
                toolbarItems.push({
                    id: 'portal.editmeetingbutton',
                    xtype: 'button',
                    disabled: true,
                    iconCls: 'x-button-meeting-edit',
                    text: 'Edit Meeting',
                    handler: function () {
                        me.editMeeting(me.getSelectionModel().getSelected().data);
                    }
                });
                toolbarItems.push({
                    id: 'portal.deletemeetingbutton',
                    xtype: 'button',
                    disabled: true,
                    iconCls: 'x-button-meeting-delete',
                    text: 'Delete Meeting',
                    handler: function () {
                        var meeting = me.getSelectionModel().getSelected().data;
                        Ext.Msg.confirm('Are you sure?', 'You are about to delete the selected meeting. Are you sure?', function (btn) {
                            if (btn == 'yes') {
                                Symphony.Ajax.request({
                                    url: '/services/portal.svc/meetings/delete/' + (meeting.id || 0),
                                    success: function () {
                                        me.refresh();
                                    }
                                });
                            }
                        });
                    }
                });
            }

            Ext.apply(this, {
                url: url,
                //sm: new Ext.grid.RowSelectionModel({ singleSelect: true }),
                selType: 'rowmodel',
                colModel: colModel,
                model: 'meeting',
                tbar: {
                    items: toolbarItems
                },
                viewConfig: {
                    listeners: {
                        'refresh': function () {
                            var btnEdit = Ext.getCmp('portal.editmeetingbutton');
                            var btnDelete = Ext.getCmp('portal.deletemeetingbutton');
                            if (btnEdit) { btnEdit.disable(); }
                            if (btnDelete) { btnDelete.disable(); }
                        }
                    }
                },
                listeners: {
                    itemdblclick: function (grid, record, item, rowIndex, e) {
                        if (Symphony.User.isGtmOrganizer && record.data.isOrganizer && record.data.isEditable) {
                            me.editMeeting(record.data);
                        }
                    },
                    rowselect: function (grid) {
                        var btnEdit = Ext.getCmp('portal.editmeetingbutton');
                        var btnDelete = Ext.getCmp('portal.deletemeetingbutton');
                        if (btnEdit) {
                            var selected = grid.getSelectionModel().getSelected();
                            if (Symphony.User.isGtmOrganizer && selected && selected.data.isOrganizer) {
                                if (selected.data.isEditable) {
                                    btnEdit.enable();
                                }
                                btnDelete.enable();
                            } else {
                                btnEdit.disable();
                                btnDelete.disable();
                            }
                        }
                    }
                }
            });
            this.callParent(arguments);
        },
        destroy: function () {
            window.clearInterval(this.checker);
        },
        editMeeting: function (meeting) {
            var me = this;
            var meetingEditor = new Symphony.Portal.MeetingEditor({
                meeting: meeting,
                listeners: {
                    destroy: function () {
                        me.store.reload();
                    }
                }
            });
            meetingEditor.show();
        }
    });


    Symphony.Portal.MeetingEditor = Ext.define('Symphony.Portal.MeetingEditor', {
        extend: 'Ext.Window',
        meeting: null,
        initComponent: function () {
            this.meeting = this.meeting || {};
            var current = Ext.Date.add(new Date(), Date.HOUR, 1);
            var next = Ext.Date.add(new Date(), Date.HOUR, 2);

            Ext.apply(this, {
                modal: true,
                title: this.meeting.id > 0 ? 'Edit a Meeting' : 'Create a Meeting',
                height: 575,
                width: 600,
                layout: 'border',
                items: [{
                    region: 'north',
                    xtype: 'toolbar',
                    autoHeight: true,
                    items: [{
                        xtype: 'button',
                        text: 'Save',
                        iconCls: 'x-button-save',
                        handler: Ext.bind(this.save, this)
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        iconCls: 'x-button-cancel',
                        handler: Ext.bind(this.cancel, this)
                    }]
                }, {
                    id: 'portal.meetingform',
                    region: 'center',
                    xtype: 'form',
                    height: 230,
                    border: false,
                    bodyStyle: 'padding:10px',
                    defaults: {
                        xtype: 'textfield',
                        allowBlank: false,
                        width: 200
                    },
                    items: [{
                        name: 'subject',
                        value: this.meeting.subject,
                        fieldLabel: 'Subject'
                    }, {
                        xtype: 'checkbox',
                        name: 'meetNow',
                        value: this.meeting.meetNow,
                        fieldLabel: 'Meet Now',
                        listeners: {
                            check: function (chkbox, checked) {
                                Ext.each(['startDate', 'startTime', 'endDate', 'endTime'], function (n) {
                                    var el = Ext.getCmp('portal.meetingform').find('name', n)[0];
                                    el.setDisabled(checked);
                                    el.allowBlank = checked;
                                    el.validate();
                                });
                            }
                        }
                    }, {
                        xtype: 'datefield',
                        name: 'startDate',
                        value: this.meeting.startDateTime ? Symphony.parseDate(this.meeting.startDateTime) : '',
                        fieldLabel: 'Start Date'
                    }, {
                        xtype: 'timefield',
                        name: 'startTime',
                        forceSelection: true,
                        value: this.meeting.startDateTime ? Symphony.parseDate(this.meeting.startDateTime) : current.formatSymphony('g:00A'),
                        fieldLabel: 'Start Time'
                    }, {
                        xtype: 'datefield',
                        name: 'endDate',
                        value: this.meeting.endDateTime ? Symphony.parseDate(this.meeting.endDateTime) : '',
                        fieldLabel: 'End Date'
                    }, {
                        xtype: 'timefield',
                        name: 'endTime',
                        forceSelection: true,
                        value: this.meeting.endDateTime ? Symphony.parseDate(this.meeting.endDateTime) : next.formatSymphony('g:00A'),
                        fieldLabel: 'End Time'
                    }/*,{
                        xtype: 'symphony.timezonecombo',
                        name: 'timeZone',
                        value: this.meeting.timeZoneKey,
                        fieldLabel: 'Timezone'
                    }*/]
                }, {
                    region: 'south',
                    layout: 'hbox',
                    height: 335,
                    border: false,
                    items: [{
                        id: 'portal.meetinginternallist',
                        layout: 'fit',
                        xtype: 'portal.gtmuserlist',
                        height: 330,
                        margins: '0 5 5 5',
                        flex: 1,
                        displayPagingInfo: false,
                        value: this.meeting.internalInvitees || []
                    }, {
                        id: 'portal.meetingexternallist',
                        layout: 'fit',
                        height: 330,
                        xtype: 'portal.externalgtmuserlist',
                        margins: '0 5 5 5',
                        flex: 1,
                        value: this.meeting.externalInvitees ? this.meeting.externalInvitees.join(', ') : ''
                    }]
                }]
            });
            this.callParent(arguments);
        },
        timeStringToObject: function (s) {
            var time = {
                hours: parseInt(s.split(':')[0], 10),
                minutes: parseInt(s.split(':')[1], 10)
            };
            if (s.charAt(s.length - 2) == 'P') {
                if (time.hours != 12) {
                    time.hours += 12;
                }
            } else {
                if (time.hours == 12) {
                    time.hours = 0;
                }
            }
            return time;
        },
        addTime: function (date, time) {
            time = this.timeStringToObject(time);
            date = Symphony.parseDate(date);
            var dt = new Date(date.setHours(time.hours));
            dt = new Date(dt.setMinutes(time.minutes));
            return dt;
        },
        save: function () {
            var me = this;
            var form = Ext.getCmp('portal.meetingform');
            if (form.isValid()) {
                // these are ids
                var internalList = Ext.getCmp('portal.meetinginternallist');
                var internalSelections = internalList.selectionPaging.getSelections();

                // these are strings
                var externalList = Ext.getCmp('portal.meetingexternallist');
                var externalSelections = externalList.getSelections();

                var values = form.getValues();

                var meeting = {
                    conferenceCallInfo: this.meeting.conferenceCallInfo || '',
                    endDateTime: values.meetNow ? (new Date()).add('h', 1).formatSymphony('microsoft') : this.addTime(values.endDate, values.endTime).formatSymphony('microsoft'),
                    externalInvitees: externalSelections,
                    gtmOrganizerId: this.meeting.gtmOrganizerId || 0,
                    id: this.meeting.id || 0,
                    internalInvitees: internalSelections, // we'll add these in a sec
                    joinUrl: this.meeting.joinUrl || '',
                    maxParticipants: this.meeting.maxParticipants || 0,
                    meetingId: this.meeting.meetingId || 0,
                    meetingType: values.meetNow ? 'immediate' : 'scheduled',
                    passwordRequired: this.meeting.passwordRequired || false,
                    startDateTime: values.meetNow ? (new Date()).formatSymphony('microsoft') : this.addTime(values.startDate, values.startTime).formatSymphony('microsoft'),
                    subject: values.subject,
                    timeZoneKey: Symphony.getUserTimezoneKey(),
                    uniqueMeetingId: this.meeting.uniqueMeetingId || ''
                };

                var tb = this.find('xtype', 'toolbar')[0];
                this.savingText = new Ext.Toolbar.TextItem({ text: '<span class="notification">Saving...</span>' });
                tb.add(this.savingText);
                tb.doLayout();

                var target = '/services/portal.svc/meetings/' + (this.meeting.id || 0);
                Symphony.Ajax.request({
                    url: target,
                    jsonData: meeting,
                    success: function (result) {
                        me.close();
                    },
                    failure: function (result) {
                        Ext.Msg.alert('Error', result.error);
                        tb.remove(me.savingText);
                    }
                });
            }
        },
        cancel: function () {
            this.close();
        }
    });

    Symphony.Portal.MessageViewer = Ext.define('Portal.MessageViewer', {
        extend: 'Ext.Window',

        message: null,
        initComponent: function () {
            this.message.date = Symphony.parseDate(this.message.createdOn);
            var template = new Ext.Template([
                '<div class="message">',
                    '<p class="from"><span class="label">From:</span> <span>{sender}</span></p>',
                    '<p class="sent"><span class="label">Sent:</span> <span>{date}</span></p>',
                    '<p class="subject"><span class="label">Subject:</span> <span>{subject}</span></p>',
                    '<p class="body"><span class="label">Message:</span> <span>{body}</span></p>',
                '</div>'
            ]);
            Ext.apply(this, {
                layout: 'fit',
                items: [{
                    border: false,
                    html: template.apply(this.message)
                }],
                title: 'Message from ' + this.message.sender,
                height: 300,
                width: 400
            });
            this.callParent(arguments);
        }
    });

    Symphony.Portal.PublicCoursesGrid = Ext.define('Portal.PublicCoursesGrid', {
        extend: 'symphony.searchablegrid',
        alias: 'widget.portal.publiccoursesgrid',
        initComponent: function () {
            var me = this;
            var url = '/services/portal.svc/publiccourses/';

            if (_ui(this)) {
                var columns = [
                    { header: ' ', dataIndex: 'courseTypeId', renderer: Symphony.Portal.courseTypeRenderer, width: 35 },
                        { header: 'Category', dataIndex: 'categoryName', align: 'left' },
                        { header: Symphony.Aliases.course + ' Name', dataIndex: 'name', width: 140, align: 'left', renderer: Symphony.Portal.courseNameRenderer },
                        { header: 'Options', dataIndex: 'displayCourseLink', sortable: false, renderer: Symphony.Renderer.displayLinkRenderer },
                        {
                            header: 'Score', dataIndex: 'score', sortable: false, renderer: function (value, meta, record) {
                                return Symphony.Renderer.displayModelRenderer(record.get('displayScore')) +
                                        Symphony.Renderer.displayIconRenderer(record.get('displayCertificate'))
                            }
                        }
                ];
                _log("Cleanup - Symphony.Portal.PublicCoursesGrid - remove conditional on columns used. Should always use new columns");
            } else {
                columns = [
                    { header: ' ', dataIndex: 'courseTypeId', renderer: Symphony.Portal.courseTypeRenderer, width: 35 },
                    { header: 'Category', dataIndex: 'categoryName', align: 'left' },
                    { header: Symphony.Aliases.course + ' Name', dataIndex: 'name', flex: 1, align: 'left', renderer: Symphony.Portal.courseNameRenderer },
                    {
                        header: 'Options', sortable: false, renderer: function (value, meta, record) {
                            return Symphony.Portal.courseLinkRenderer(value, meta, record, null);
                        }
                    },
                    {
                        header: 'Score', dataIndex: 'score', sortable: false, renderer: function (value, meta, record) {
                            return Symphony.Portal.scoreRenderer(value, meta, record, 'p');
                        }
                    }
                ];
            }

            if (Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards)) {
                columns = columns.concat([{ header: ' ', dataIndex: 'messageBoardId', sortable: false, renderer: Symphony.Portal.MessageBoardRenderer, width: 35 }]);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    width: 65,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: columns
            });

            Ext.apply(this, {
                url: url,
                colModel: colModel,
                model: 'course',
                viewConfig: {
                    forceFit: true
                },
                listeners: {
                    itemclick: !_ui(this) ? Symphony.Portal.linkHandler4 : _getDeprecatedHandler(this),
                    cellclick: _ui(this) ? Symphony.LinkHandlers.simpleClick : _nullHandler
                }
            });
            this.callParent(arguments);
        }
    });

    Symphony.Portal.linkHandler4 = function (grid, record, item, rowIndex, e) {
        Symphony.Portal.linkHandler(grid, rowIndex, e);
    };

    Symphony.Portal.linkHandler = function (scope, rowIndex, e) {
        var method = e.target.getAttribute('symphony:method');
        var param = e.target.getAttribute('symphony:parameter');
        scope = scope || window;
        
        // the method is "launchOnline", "register", "unregister", or "launchMessageBoard"
        if (Symphony.Portal[method]) {

            if (method == 'register' || method == 'unregister') {
                // register/unregister = refresh both upcoming events and public courses
                var callback = function () {
                    Symphony.App.query('[itemId=portal.publiccoursesgrid]')[0].refresh();
                    Symphony.App.query('[itemId=portal.upcomingeventsgrid]')[0].refresh();
                };

                Symphony.Portal[method].apply(scope, [callback].concat(param.split(',')));
            } else if (method == 'launchOnline') {
                var callback = function () {
                    scope.store.reload();
                };
                Symphony.Portal[method].apply(scope, param.split(','));
                Symphony.Portal.onCourseCompletion(callback);
            } else {
                Symphony.Portal[method].apply(scope, param.split(','));
            }

        }
    };



    Symphony.Portal.TrainingProgramsGrid = Ext.define('portal.trainingprogramsgrid', {
        alias: 'widget.portal.trainingprogramsgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var url = '/services/portal.svc/trainingprograms/list/';

            var columns = [{
                header: 'Name', 
                dataIndex: 'name', 
                align: 'left', 
                renderer: Symphony.Portal.trainingProgramRenderer,
                flex: 1,
                minWidth: 100
            }, { 
                header: 'Bundle', 
                dataIndex: 'purchasedFromBundleName', 
                align: 'left' 
            }, {
                header: 'Due Date',
                dataIndex: 'displayDueDate',
                width: 80,
                renderer: Symphony.Renderer.displayDateRenderer,
                getSortParam: function () {
                    return "dueDate"
                }
            }, { 
                header: '# ' + Symphony.Aliases.courses, 
                dataIndex: 'courseCount', 
                width: 75, 
                renderer: Symphony.Portal.courseCountRenderer 
            }, { 
                header: 'Category', 
                dataIndex: 'categoryName', 
                width: 100, 
                renderer: Symphony.Portal.valueRenderer 
            }, { 
                header: 'Completed', 
                dataIndex: 'displayCompleted', 
                id: 'completed', 
                width: 60, 
                renderer: Symphony.Renderer.displayIconRenderer,
                getSortParam: function () {
                    return "completed"
                }
            }, {
                header: 'Percent Completed', 
                dataIndex: 'percentComplete', 
                id: 'percentComplete', 
                width: 60, 
                renderer: function (value) {
                    return value + '%';
                }
            }];

            if (Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards)) {
                columns = columns.concat([{ header: ' ', dataIndex: 'messageBoardId', sortable: false, renderer: Symphony.Portal.MessageBoardRenderer, width: 35 }]);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    align: 'center',
                    sortable: true
                },
                columns: columns
            });

            Ext.apply(this, {
                url: url,
                colModel: colModel,
                model: 'trainingProgram',
                cls: 'portal-trainingprograms-grid',
                viewConfig: {
                    forceFit: true,
                    emptyText: "<div class='no-trainingprograms'>There are no training programs currently assigned to you.<br/><span class='transcript-info'>You may still <a href='#' class='view-transcript'>view your transcript</a> containing any previous records.</span></div>",
                    listeners: {
                        containerclick: function (view, e, opts) {
                            if (e.target.tagName.toLowerCase() == 'a' && e.target.className == 'view-transcript') {
                                e.stopEvent();
                                Symphony.Portal.showTranscriptWindowForCurrentUser();
                            }
                        }
                    }
                },
                listeners: {
                    cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var column = grid.headerCt.getGridColumns()[columnIndex].dataIndex;
                        var data = record.get(column);
                        
                        if (column === 'messageBoardId') {    
                            Symphony.Portal.linkHandler(grid, rowIndex, e);
                        } else {
                            if (grid.headerCt.getGridColumns()[columnIndex].dataIndex != 'completed' &&
                                grid.headerCt.getGridColumns()[columnIndex].dataIndex != 'displayCompleted') {
                                Symphony.Portal.showTrainingProgram(record.get('id'));
                            } else {
                                Symphony.LinkHandlers.click(grid, e, data, record);
                            }
                        }
                    }
                }
            });
            this.callParent(arguments);

            var grid = this;
            var store = grid.store;
            store.on('load', function () {
                var columns = grid.columns;
                var bundleColumnIndex;
                var isNmlsNumberRequired = false;
                var isBundleColumnRequired = false;

                for (var i = 0; i < columns.length; i++) {
                    if (columns[i].dataIndex === 'purchasedFromBundleName') {
                        bundleColumnIndex = i;
                        break;
                    }
                }

                for (var i = 0; i < store.data.items.length; i++) {
                    var tp = store.data.items[i];

                    if (tp.get('purchasedFromBundleName')) {
                        isBundleColumnRequired = true;
                    }

                    if (tp.get('isNmls') && !Symphony.User.hasNmlsNumber) {
                        isNmlsNumberRequired = true;
                    }
                }

                if (isNmlsNumberRequired) {
                    Symphony.Portal.requestNmlsNumber();
                }

                if (!isBundleColumnRequired) {
                    columns[bundleColumnIndex].hide();
                }
            });
        }
    });


    Symphony.Portal.TrainingProgramFilesGrid = Ext.define('portal.trainingprogramfilesgrid', {
        alias: 'widget.portal.trainingprogramfilesgrid',
        extend: 'symphony.localgrid',
        initComponent: function () {

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    align: 'center',
                    sortable: true
                },
                columns: [
                    { header: 'File Name', dataIndex: 'filename', align: 'left', width: 120, renderer: Symphony.Portal.trainingProgramFileRenderer },
                    { header: 'Created On', dataIndex: 'createdOn', width: 80, renderer: Symphony.dateRenderer },
                    { header: 'Description', dataIndex: 'description', width: 80, renderer: Symphony.Portal.valueRenderer, align: 'left' }
                ]
            });

            Ext.apply(this, {
                colModel: colModel,
                model: 'trainingProgramFile',
                listeners: {
                    itemclick: function (grid, record, e) {
                        Symphony.Portal.downloadTrainingProgramFile(record.get('id'));
                    }
                }
            });
            this.callParent(arguments);
        }
    });


    Symphony.Portal.TrainingProgramCoursesGrid = Ext.define('portal.trainingprogramcoursesgrid', {
        alias: 'widget.portal.trainingprogramcoursesgrid',
        extend: 'symphony.localgrid',
        showMessageBoardColumn: false,
        targetUserId: null,
        initComponent: function () {
            var me = this;

            var messageBoardColumn = [{ header: ' ', dataIndex: 'messageBoardId', sortable: false, renderer: Symphony.Portal.MessageBoardRenderer, width: 35 }];
            var scoreProgressColumn = [{
                header: 'Progress', dataIndex: 'navigationPercentageDisplay', width: 25
            }, {
                header: 'Score', dataIndex: _ui(this) ? 'displayCertificate' : 'score', width: 45, renderer: function (value, meta, record) {
                    if (_ui(this)) {
                        _log("Cleanup - Symphony.Portal.TrainingProgramCoursesGrid - Score column should always return new ui. No need for toggle.");
                        if (record.get('isSurvey')) {
                            return Symphony.Renderer.displayIconRenderer(record.get('displayCertificate'));
                        } else {
                            return Symphony.Renderer.displayTextRenderer(record.get('displayScore')) +
                                Symphony.Renderer.displayIconRenderer(record.get('displayCertificate'));
                        }
                    } else {
                    return Symphony.Portal.scoreRenderer(value, meta, record, 't', me.trainingProgram.id, me.targetUserId);
                }
                }
            }];

            var hasAssignmentColumn = [{
                header: ' ', dataIndex: 'displayAssignmentsIcon', align: 'left', width: 35, renderer: Symphony.Renderer.displayIconRenderer
            }];

            var assignmentColumn;
            if (_ui(this)) {
                assignmentColumn = [{
                    header: 'Assignments', dataIndex: 'displayAssignmentStatus', width: 25, renderer: Symphony.Renderer.displayIconRenderer
                }];
                _log("Cleanup - Symphony.Portal.TrainingProgramCoursesGrid - No need for toggle on assignment status columnd");
            } else {
                assignmentColumn = [{
                header: 'Assignments', dataIndex: 'assignments', width: 25, renderer: Symphony.Portal.assignmentRenderer
            }];
            }

            var columns;

            if (_ui(this)) {
                columns = [
                    { header: ' ', dataIndex: 'courseTypeId', align: 'left', width: 35, renderer: Symphony.Portal.courseTypeRenderer, menuDisabled: true },
                    {
                        header: Symphony.Aliases.course + ' Name', dataIndex: 'name', width: 120, renderer: function (value, meta, record) {
                            var courseLink = Symphony.Portal.courseNameRenderer(value, meta, record);

                            return courseLink;
                        }, align: 'left'
                    },
                    {
                        header: 'Options', width: 100, dataIndex: 'displayCourseLink', renderer: function (value, meta, record, row, col, store) {
                            var printLink = Symphony.Renderer.displayLinkRenderer(record.get('displayPrintIcon').link);
                            if (printLink && printLink != '') {
                                printLink = " | " + printLink;
                            }

                            return Symphony.Renderer.displayLinkRenderer(value) + printLink;
                        }
                    },
                    {
                        header: 'Due Date',
                        dataIndex: 'displayDueDate',
                        width: 65,
                        renderer: function (value, meta, record) {
                            return Symphony.Renderer.displayDateRenderer(value);
                        }
                    }
                ];
                _log("Cleanup - Symphony.Portal.TrainingProgramCoursesGrid - Toggle on columns for Options and Due Date - Not required, huge amount of code can be removed!");
            } else {
                columns = [
                    { header: ' ', dataIndex: 'courseTypeId', align: 'left', width: 35, renderer: Symphony.Portal.courseTypeRenderer, menuDisabled: true },
                    {
                        header: Symphony.Aliases.course + ' Name', dataIndex: 'name', width: 120, renderer: function (value, meta, record) {
                            var courseLink = Symphony.Portal.courseNameRenderer(value, meta, record);

                            return courseLink;
                        }, align: 'left'
                    },
                    {
                        header: 'Options', width: 100, renderer: function (value, meta, record, row, col, store) {
                            //return " The options ? ";
                            var link = "";
                            if (me.enabled !== false) {
                                var courseOptionsLink = Symphony.Portal.trainingProgramCourseOptionsRenderer(value, meta, record, row, col, store, me.data, me.trainingProgram, me.required);
                                link = courseOptionsLink;
                            } else {
                                link = '-';
                            }
                            
                            var printLink = Symphony.Renderer.displayLinkRenderer(record.get('displayPrintIcon').link);
                            if (printLink && printLink != '') {
                                printLink = " | " + printLink;
                            }
                            return link + printLink
                        }
                    },
                    {
                        header: 'Due Date',
                        dataIndex: 'dueDate',
                        width: 65,
                        renderer: function (value, meta, record) {
                            /*if (record.get('courseTypeId') == Symphony.CourseType.classroom) {
                                return Symphony.parseDate(value);
                            }*/
                            var dt = Symphony.parseDate(value, !record.get('isUsingSession'));

                            // text to show when the class is about to expire
                            // note that this is overwritten if the course is set to deny access after the due date
                            var overdue = '<span class="help-icon" ext:qtip="You may progress in the course but you are behind schedule. You must complete the course by the session end time.">Due Date Passed</span>';

                            if (record.get('isRelativeDueDate')) {
                                var displayTemplate;

                                if (record.get('hasLaunched')) {
                                    displayTemplate = 'In [remaining]';
                                } else {
                                    displayTemplate = '[remaining] after launch';
                                }

                                return Symphony.Portal.dynamicTimeRenderer('course-due',
                                        record.get('id'),
                                        record.get('minutesUntilExpired'),
                                        record.get('expiresAfterMinutes'),
                                        displayTemplate,
                                        record.get('hasLaunched'),
                                function () {
                                    if (record.get('denyAccessAfterDueDateIndicator')) {
                                        var launchLink = Ext.get("launch-link-" + me.trainingProgram.id + "-" + record.get('id'));

                                        var id = Ext.id();

                                        if (launchLink) {
                                            launchLink.parent().update('<span class="help-icon" id="' + id + '">Unavailable</span>');
                                            setTimeout(function () {
                                                var target = document.getElementById(id);
                                                Ext.QuickTips.register({
                                                    target: target,
                                                    autoHide: true,
                                                    text: 'The due date for this ' + Symphony.Aliases.course.toLowerCase() + ' has passed. Contact your training program leader for more time if needed.',
                                                    width: 160,
                                                    cls: 'unavailable-reasons'
                                                });
                                            }, 10);
                                        }
                                    }
                                    return overdue;
                                });
                            }

                            if (dt && (dt.getFullYear() >= Symphony.defaultYear)) {

                                // if no date is set and this isn't a classroom course, inherit the due date of the Training Program
                                if (me.trainingProgram && (me.trainingProgram.dueDate || me.trainingProgram.endDate) && record.get('courseTypeId') != Symphony.CourseType.classroom) {
                                    var tpDueDate = Symphony.parseDate(me.trainingProgram.dueDate, true);
                                    if (tpDueDate.getFullYear() >= Symphony.defaultYear) {
                                        tpDueDate = Symphony.parseDate(me.trainingProgram.endDate, true);
                                    }

                                    if (!tpDueDate || (tpDueDate.getFullYear() >= Symphony.defaultYear)) {
                                        return '';
                                    }

                                    return Symphony.dateRenderer(tpDueDate);
                                }
                                else {
                                    return '';
                                }
                            }

                            if (record.get('isUsingSession')) {
                                // User is not registered in a session
                                // no point in showing a date or starting a countdown
                                if (!me.trainingProgram.isSessionRegistered) {
                                    return '-';
                                }

                                var expiresAfterMinutes = Math.ceil((dt - (new Date())) / 60000);


                                if (expiresAfterMinutes > 0) {
                                    return Symphony.Portal.dynamicTimeRenderer('course-due',
                                        record.get('id'),
                                        expiresAfterMinutes,
                                        expiresAfterMinutes,
                                        Symphony.dateRenderer(dt) + dt.formatSymphony(' g:i a'),
                                        true,
                                        function () {
                                            if (record.get('denyAccessAfterDueDateIndicator')) {
                                                var launchLink = Ext.get("launch-link-" + me.trainingProgram.id + "-" + record.get('id'));

                                                var id = Ext.id();

                                                if (launchLink) {
                                                    launchLink.parent().update('<span class="help-icon" id="' + id + '">Unavailable</span>');
                                                    setTimeout(function () {
                                                        var target = document.getElementById(id);
                                                        Ext.QuickTips.register({
                                                            target: target,
                                                            autoHide: true,
                                                            text: 'The due date for this ' + Symphony.Aliases.course.toLowerCase() + ' has passed. Contact your training program leader for more time if needed.',
                                                            width: 160,
                                                            cls: 'unavailable-reasons'
                                                        });
                                                    }, 10);
                                                }
                                            }
                                            return overdue;
                                        })
                                } else {
                                    return overdue;
                                }
                            }
                            return Symphony.dateRenderer(dt);
                        }
                    }
            ];
            }

            if (Symphony.Modules.hasModule(Symphony.Modules.InstructorPortal)) {
                columns = hasAssignmentColumn.concat(columns).concat(assignmentColumn);
            }

            columns = columns.concat(scoreProgressColumn);

            if (me.showMessageBoardColumn && Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards)) {
                columns = messageBoardColumn.concat(columns);
            }

            if (me.targetUserId > 0) {
                columns = [
                    {
                        header: ' ', dataIndex: 'adminMenu', align: 'left', width: 35, renderer: function (value, meta, record) {
                            return '<a href="#" symphony:method="show-training-program-context-menu"  class="grid-gear" ext:qtip="Admin Menu"></a>';
                        }
                    }
                ].concat(columns);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    align: 'center',
                    sortable: false,
                    menuDisabled: true
                },
                columns: columns
            });
            var me = this;
            Ext.apply(this, {
                colModel: colModel,
                model: 'course',
                stateId: me.targetUserId > 0 ? 'has-admin-menu' : null,
                listeners: {
                    viewready: function (grid) {
                        grid.getStore().each(function (r) {
                            var generateQTip = r.get('generateQTip');
                            if (typeof generateQTip == 'function') {
                                generateQTip();
                            }
                        });
                    },
                    cellclick: function (gridView, td, columnIndex, record, tr, rowIndex, e, opts) {
                        var fieldName = gridView.getColumnModel().getDataIndex(columnIndex);
                        var data = record.get(fieldName);
                        var method = e.target.getAttribute('symphony:method');
                        var param = e.target.getAttribute('symphony:parameter');

                        if (method == 'show-training-program-context-menu' && me.targetUserId > 0) {
                            var row = Ext.fly(gridView.getNode(rowIndex));

                            new Symphony.CourseAssignment.TrainingProgramAdminMenu({
                                trainingProgram: me.trainingProgram,
                                courseRecord: record,
                                userId: me.targetUserId,
                                grid: gridView,
                                callback: function () {
                                    me.fireEvent('registrationchanged');
                                },
                                listeners: {
                                    beforeUpdate: function (type, msg) {
                                        var maskKey = "mask" + type;
                                        me[maskKey] = new Ext.LoadMask(me.getEl(), { msg: msg });
                                        me[maskKey].show();
                                    },
                                    afterUpdate: function (type) {
                                        var maskKey = "mask" + type;
                                        me[maskKey].hide();
                                    }
                                }
                            }).showAt(row.getX(), row.getY());
                        } else if (Symphony.Portal[method]) {
                            var callback = function () {
                                me.fireEvent('registrationchanged');
                            };
                            if (method == 'register' || method == 'unregister') {
                                Symphony.Portal[method].apply(me, [callback].concat(param.split(',')));
                            } else {
                                Symphony.Portal[method].apply(me, param.split(','));
                                Symphony.Portal.onCourseCompletion(callback);
                            }
                        } else if (data) {
                            Symphony.LinkHandlers.click(me, e, data, record);
                        }
                    }
                }
            });
            _log("Cleanup - Symphony.Portal.TrainingProgramCoursesGrid - Remove event toggles - only use cellclick not old rowclick one");
            this.addEvents('registrationchanged');

            this.callParent(arguments);
        },
        initEvents: function () {
            Symphony.Portal.TrainingProgramCoursesGrid.superclass.initEvents.call(this);
            this.disabledMask = new Ext.LoadMask(this.getView(), { cls: this.cls + ' trainingprogramcoursesgrid-mask', msg: this.disabledMessage, msgCls: 'disabled' });

            var allowMask = true;

            if (this.hideMaskWhenEmpty) {
                allowMask = this.data.length > 0;
            }

            if (this.enabled === false && allowMask) {
                this.disable();
            }
        },
        destroy: function () {
            this.disabledMask.destroy();
        },
        disable: function () {
            this.disabledMask.show();
        },
        enable: function () {
            this.disabledMask.hide();
        }
    });


    Symphony.Portal.UpcomingEventsGrid = Ext.define('portal.upcomingeventsgrid', {
        alias: 'widget.portal.upcomingeventsgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var url = '/services/portal.svc/upcomingevents/false';

            var columns;

            if (_ui(this)) {
                columns = [
                    { id: 'name', header: 'Event', dataIndex: 'displayEventLink', renderer: Symphony.Renderer.displayLinkRenderer, width: 130, align: 'left' },
                    { header: 'Options', dataIndex: 'displayCourseLink', renderer: Symphony.Renderer.displayLinkRenderer, width: 120 },
                    { id: 'id', header: 'Event ID', dataIndex: 'eventId', width: 50 },
                    { header: 'Date', dataIndex: 'displayStartDate', width: 155, renderer: Symphony.Renderer.displayDateRenderer },
                    { header: 'Event Type', dataIndex: 'displayEventType', width: 80, renderer: Symphony.Renderer.displayTextRenderer }
                ];
                _log("Cleanup - Symphony.Portal.UpcomingEventsGrid - Remove the toggle on the column definition");
            } else {
                columns = [
                    {
                        /*id: 'name',*/ header: 'Event', dataIndex: 'eventName', renderer: Symphony.Portal.upcomingEventRenderer, width: 130, align: 'left',
                        flex: 1
                    },
                    { header: 'Options', renderer: Symphony.Portal.upcomingEventOptionsRenderer, width: 120 },
                    { /*id: 'id',*/ header: 'Event ID', dataIndex: 'eventId', width: 50 },
                    { header: 'Date', dataIndex: 'startDate', width: 155, renderer: Symphony.dateTimeRenderer },
                    { header: 'Event Type', dataIndex: 'eventType', width: 80, renderer: Symphony.Portal.upcomingEventTypeRenderer }
            ];
            }

            if (Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards)) {
                columns = columns.concat([{ header: ' ', dataIndex: 'messageBoardId', sortable: false, renderer: Symphony.Portal.MessageBoardRenderer, width: 35 }]);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center'
                },
                columns: columns
            });

            Ext.apply(this, {
                url: url,
                colModel: colModel,
                model: 'upcomingEvent',
                viewConfig: {
                    forceFit: true
                },
                listeners: {
                    itemclick: !_ui(this) ? Symphony.Portal.linkHandler4 : _getDeprecatedHandler(this),
                    cellclick: _ui(this) ? Symphony.LinkHandlers.simpleClick : _nullHandler
                }
            });
            _log("Cleanup - Symphony.Portal.UpcomingEventsGrid - Remove handler toggle - only use cell click");
            this.callParent(arguments);
        },
        setShowAll: function (showAll) {
            this.showAll = showAll;
            this.updateUrl();
        },
        updateUrl: function () {
            this.url = '/services/portal.svc/upcomingevents/' + (this.showAll ? false : true);
            if (this.updateStore) {
                this.updateStore(this.url);
            }
        }
    });


    Symphony.Portal.PublicDocumentsGrid = Ext.define('portal.publicdocumentsgrid', {
        alias: 'widget.portal.publicdocumentsgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var url = '/services/portal.svc/publicdocuments/';

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { /*id: 'name',*/ header: 'Category', dataIndex: 'categoryName', width: 80 },
                    {
                        header: 'Name', dataIndex: 'name', width: 105, renderer:
                          function (value, meta, record) {
                              var id = record.get('id');
                              return '<a href="/services/courseassignment.svc/publicdocuments/binary/{0}" target="_blank">{1}</a>'.format(id, value);
                          }
                    },
                    {
                        /*id: 'description',*/ header: 'Description', dataIndex: 'description', width: 80,
                        flex: 1
                    }
                ]
            });

            Ext.apply(this, {
                url: url,
                colModel: colModel,
                model: 'publicDocument',
                deferLoad: true
            });
            this.callParent(arguments);
        }
    });


    Ext.onReady(function () {
        // Add classes to hide module specific fields
        Symphony.Modules.hideDisabledModules();

        var grid = [{
            // add the portal grids here
            columnWidth: 0.5,
            style: 'padding:10px',
            items: [
                {
                    xtype: 'portal.portlet',
                    title: 'Training Programs',
                    itemId: 'portal.trainingprograms',
                    closable: false,
                    hidden: true,
                    initStore: function () {
                        this.query('[xtype=portal.trainingprogramsgrid]')[0].getStore().load();
                    },
                    items: [
                        {
                            itemId: 'portal.trainingprogramsgrid',
                            header: false,
                            border: false,
                            pageSize: 6,
                            height: 225,
                            xtype: 'portal.trainingprogramsgrid',
                            deferLoad: true
                        }
                    ]
                },
                {
                    xtype: 'portal.portlet',
                    title: 'Public ' + Symphony.Aliases.courses,
                    itemId: 'portal.publiccourses',
                    closable: false,
                    hidden: true,
                    initStore: function () {
                        this.query('[xtype=portal.publiccoursesgrid]')[0].getStore().load();
                    },
                    items: [
                        {
                            itemId: 'portal.publiccoursesgrid',
                            header: false,
                            border: false,
                            pageSize: 6,
                            height: 225,
                            xtype: 'portal.publiccoursesgrid',
                            deferLoad: true
                        }
                    ]
                },
                {
                    xtype: 'portal.portlet',
                    title: 'My Libraries',
                    itemId: 'portal.libraries',
                    closable: false,
                    hidden: true,
                    cls: Symphony.Modules.getModuleClass(Symphony.Modules.Libraries),
                    layout: 'fit',
                    initStore: function () {
                        this.query('[xtype=libraries.portallibrarygrid]')[0].getStore().load();
                    },
                    items: [{
                        itemId: 'portal.portallibrariesgrid',
                        header: false,
                        border: false,
                        pageSize: 7,
                        height: 225,
                        xtype: 'libraries.portallibrarygrid',
                        deferLoad: true
                    }]
                }
            ]
        }, {
            columnWidth: 0.5,
            style: 'padding:10px',
            items: [
                {
                    xtype: 'portal.portlet',
                    title: 'Library Browser',
                    itemId: 'portal.librarybrowser',
                    hidden: true,
                    closable: false,
                    cls: Symphony.Modules.getModuleClass(Symphony.Modules.Libraries),
                    initStore: function () {
                        var filterTree = this.query('[xtype=libraries.portallibraryfiltertree]')[0];
                        filterTree.store.load();
                        filterTree.fireEvent('filterchanged');
                    },
                    items: [{
                        itemId: 'portal.portallibrarybrowser',
                        header: false,
                        border: false,
                        pageSize: 7,
                        height: 495,
                        xtype: 'libraries.portallibrarybrowser',
                        deferLoad: true
                    }]
                }, {
                    xtype: 'portal.portlet',
                    title: 'Upcoming Events',
                    itemId: 'portal.upcomingevents',
                    hidden: true,
                    closable: false,
                    initStore: function () {
                        this.query('[xtype=portal.upcomingeventsgrid]')[0].getStore().load();
                    },
                    items: [{
                        itemId: 'portal.upcomingeventsgrid',
                        header: false,
                        border: false,
                        pageSize: 7,
                        height: 225,
                        xtype: 'portal.upcomingeventsgrid',
                        deferLoad: true,
                        tbar: {
                            items: [{
                                text: 'Calendar',
                                iconCls: 'x-button-calendar',
                                handler: function () {
                                    var w = new Ext.Window({
                                        height: 550,
                                        width: 950,
                                        modal: true,
                                        layout: 'fit',
                                        title: 'Upcoming Events',
                                        items: [{
                                            border: false,
                                            id: 'portal.upcomingeventscalendar',
                                            xtype: 'portal.upcomingeventscalendar',
                                            tbarItems: [{
                                                xtype: 'button',
                                                enableToggle: true,
                                                stateId: 'upcomingevents-calendar-toggle',
                                                stateful: true,
                                                getState: function () {
                                                    return { pressed: this.pressed };
                                                },
                                                applyState: function (state) {
                                                    this.toggle(state.pressed);
                                                    var upcomingEventsCalendar = Ext.getCmp('portal.upcomingeventscalendar');

                                                    if (upcomingEventsCalendar) {
                                                        upcomingEventsCalendar.setShowAll(state.pressed);
                                                    }
                                                },
                                                stateEvents: ['click'],
                                                text: 'Show All',
                                                iconCls: 'x-button-all',
                                                handler: function (btn, state) {
                                                    var upcomingEventsCalendar = Ext.getCmp('portal.upcomingeventscalendar');

                                                    if (upcomingEventsCalendar) {
                                                        upcomingEventsCalendar.setShowAll(btn.pressed);
                                                    }
                                                }
                                            }]
                                        }],
                                        listeners: {
                                            close: function () {
                                                Symphony.App.query('[itemId=portal.publiccoursesgrid]')[0].refresh();
                                                Symphony.App.query('[itemId=portal.upcomingeventsgrid]')[0].refresh();
                                            }
                                        }
                                    });
                                    w.show();
                                }
                            }, {
                                xtype: 'button',
                                enableToggle: true,
                                stateId: 'upcomingevents-grid-toggle',
                                stateful: true,
                                getState: function () {
                                    return { pressed: this.pressed };
                                },
                                applyState: function (state) {
                                    this.toggle(state.pressed);
                                    if (Symphony.App) {
                                        var upcomingEvents = Symphony.App.query('[itemId=portal.upcomingeventsgrid]')[0];

                                        if (upcomingEvents) {
                                            Symphony.App.query('[itemId=portal.upcomingeventsgrid]')[0].setShowAll(state.pressed)
                                        }
                                    }
                                },
                                stateEvents: ['click'],
                                text: 'Show All',
                                iconCls: 'x-button-all',
                                handler: function (btn) {
                                    var upcomingEvents = Symphony.App.query('[itemId=portal.upcomingeventsgrid]')[0];

                                    if (upcomingEvents) {
                                        Symphony.App.query('[itemId=portal.upcomingeventsgrid]')[0].setShowAll(btn.pressed)
                                    }
                                }
                            }]
                        }
                    }]
                },
                {
                    xtype: 'portal.portlet',
                    title: 'Public Documents',
                    itemId: 'portal.publicdocuments',
                    deferLoad: true,
                    closable: false,
                    hidden: true,
                    initStore: function () {
                        this.query('[xtype=portal.publicdocumentsgrid]')[0].getStore().load();
                    },
                    items: [{
                        itemId: 'portal.publicdocumentsgrid',
                        header: false,
                        border: false,
                        pageSize: 7,
                        height: 225,
                        xtype: 'portal.publicdocumentsgrid'
                    }]
                }
            ]
        }];

        var _lastRefresh = null;
        Symphony.Portal.refresh = function () {
            if (Symphony.App) {
                if (_lastRefresh != null && (new Date()).getTime() - _lastRefresh < 5000) {
                    // ensure we don't hit this more than 1x every 5 seconds
                    // this is particularly useful for the history token, which can trigger multiple times
                    return;
                }
                var grids = ['portal.trainingprogramsgrid',
                    'portal.publiccoursesgrid',
                    'portal.upcomingeventsgrid',
                    'portal.publicdocumentsgrid',
                    'portal.portallibrariesgrid'];

                for (var i = 0; i < grids.length; i++) {
                    var grid = Symphony.App.query('[itemId=' + grids[i] + ']')[0];
                    if (!grid.hidden) {
                        grid.refresh();
                    }
                }
                _lastRefresh = (new Date()).getTime();
            }
        };

        if (Symphony.Settings.hidePortal) {
            return;
        }

        var getModule = function (panel) {
            return (panel.name && panel.name.indexOf('.app') > -1 ? panel.name : panel.xtype).replace('.app', '');
        };

        var handleModuleTabChange = function (tabpanel, tab) {
            var module = getModule(tabpanel);
            var section = (tab.name && tab.name.indexOf('tab') > -1 ? tab.name : tab.xtype).replace('tab', '').replace(module + '.', '');
            if (Symphony.App) {
                Symphony.App.applyHistoryToken(module + '/' + section);
            }
        };

        var handleModuleChange = function (panel) {
            var module = getModule(panel);
            if (Symphony.App) {
                Symphony.App.applyHistoryToken(module);
            }
        };

        Symphony.App = Ext.create('Ext.container.Viewport', {
            initHistory: function () {
                var me = this;

                var cookieHash = Ext.util.Cookies.get('history_token');
                Ext.util.Cookies.clear('history_token');
                
                Ext.History.init();
                Ext.History.on('change', function (token) {
                    var parts, length, i;
                    if (token) {
                        me.applyHistoryToken(token);
                    }
                });

                // from the hash in the url, then the hash in the cookie, then the default for the company
                this.applyHistoryToken(Ext.History.getToken() || cookieHash || ''); // TODO: set the default token based on the web config, then based on the company config
            },
            applyHistoryToken: function (token) {
                if (!token) {
                    return;
                }
                this.selectTab(token);
                Ext.History.add(token);
            },
            selectTab: function (url) {
                if (this._selecting) {
                    return;
                }

                var parts = url.split('/');
                var module = parts.length > 0 ? parts[0] : '';
                //## Training Program Assignment popup hack
                //## And message board popup hack
                // TODO: Fix this hack for Training Program Assignments and Message boards to be something more generic
                // We'd need to indicate in the url we are pointing to a popup, and do something to standardize
                // where the ids end up in the url. We would need to ensure the popups all have a standard
                // method to launching them as well
                if (module === 'trainingprograms' && parts.length > 1) {

                    // Assignments: /trainingprograms/{trainingProgramid}/{courseId}/assignments
                    var trainingProgramId = parts[1];


                    if (parts.length > 3) {
                        var courseId = parts[2];
                        var type = parts[3];

                        if (courseId > 0 && type === 'assignments') {
                            Symphony.Portal.launchAssignments(trainingProgramId, courseId, Symphony.User.id);
                        }
                    } else {
                        Symphony.Portal.showTrainingProgram(trainingProgramId);
                    }

                    return;
                } else if (module === 'messageboards' && parts.length > 1) {
                    // Message Boards: /messageboards/{messageBoardId}
                    var messageBoardId = parts[1];
                    if (Symphony.Modules.hasOwnProperty('MessageBoards')) {
                        if (!Symphony.Portal.hasPermission(Symphony.Modules['MessageBoards'])) {
                            return;
                        }
                    }
                    Symphony.Portal.launchMessageBoard(messageBoardId, 0, false);

                    return;
                } else if (module == 'inbox') {
                    Symphony.Portal.showInbox();
                    return;
                }

                var tab = parts.length > 1 ? parts[1] : '';
                var id = parts.length > 2 ? parts[2] : '';

                id = parseInt(id, 10);

                // Fix some naming issues here
                // (Keeps the old urls active)
                switch (module) {
                    case 'classroom':
                    case 'classroomtraining':
                        module = 'Classroom';
                        break;
                    case 'courseassignment':
                    case 'coursemanagement':
                        module = 'CourseAssignment';
                        break;
                    case 'customer':
                    case 'studentmanagement':
                        module = 'Customer';
                        break;
                    case 'reportingv1':
                        module = 'Reporting';
                        tab = 'reportingv1';
                        break;
                }
               
                if (Symphony.Modules.hasOwnProperty(module)) {
                    if (!Symphony.Portal.hasPermission(Symphony.Modules[module])) {
                        return;
                    }
                }

                var main = Ext.getCmp('symphonymain');
                var selectedItem = null;

                for (var i = 0; i < main.items.items.length; i++) {
                    var item = main.items.items[i];

                    if (item.xtype == module.toLowerCase() + '.app' ||
                        item.name == module.toLowerCase() + '.app' ||
                        (tab == 'reportingv1' && item.name == 'reporting')) {
                        selectedItem = item;
                        break;
                    }
                }

                if (!selectedItem) {
                    return;
                }

                var symphonybar = Ext.getCmp('symphonybar');

                this._selecting = true;

                for (var b = 0; b < symphonybar.items.items.length; b++) {
                    var btn = symphonybar.items.items[b];

                    if (btn.name == module.toLowerCase() && btn.toggle && !btn.pressed) {
                        btn.toggle();
                    }
                }

               
                main.getLayout().setActiveItem(selectedItem);

                var selectedTab = null;

                if (tab) {
                    if (selectedItem.xtype == module.toLowerCase() + '.' + tab.toLowerCase()) {
                        // In this case the module level item we selected is the tab we want
                        // and has the addTab function we need to call for the entity
                        // The url example would be /students/app
                        // which would map to Students.App
                        // which then has the addPanel function to load a course
                        selectedTab = selectedItem
                    } else {
                        for (var t = 0; t < selectedItem.items.items.length; t++) {
                            var tabItem = selectedItem.items.items[t];
                            // something like "classroom.registrationstab"
                            if (tabItem.xtype == module.toLowerCase() + '.' + tab.toLowerCase() + 'tab' ||
                                tabItem.name == module.toLowerCase() + '.' + tab.toLowerCase() + 'tab') {
                                selectedTab = tabItem;
                            }
                        }
                    }

                    if (selectedTab) {
                        // Only do this if the selected item is a tab panel
                        if (typeof selectedItem.setActiveTab === 'function' && selectedItem !== selectedTab) {
                            selectedItem.setActiveTab(selectedTab);
                        } else if (selectedItem.getLayout().setActiveItem) {
                            selectedItem.getLayout().setActiveItem(selectedTab);
                        } else if (selectedItem.activate) {
                            selectedItem.activate(selectedTab);
                        }

                        // If an id is passed in, the tab must have an addPanel function
                        // The addPanel function needs to work by just passing in an id
                        // Meaning it must not rely on receiving the record from the list.
                        if (id > 0 && (typeof selectedTab.addPanel == 'function')) {
                            if (parts.length > 4) {
                                // A hack for loading the course documents
                                var courseId = parts[3];
                                var courseTypeId = parts[4];

                                selectedTab.addPanel(id, courseId, courseTypeId, null, "documents");
                            } else {
                                selectedTab.addPanel(id);
                            }
                        }
                    }
                }

                if (selectedItem.name == 'portal.app') {
                    Symphony.Portal.refresh();
                }

                this._selecting = false;
            },
            loadPortalConfig: function () {
                if (!Symphony.App.isLoadedPortalConfig) {
                    Symphony.App.isLoadedPortalConfig = true
                    var portal = Ext.getCmp('portal');
                    portal.applyConfiguration(Symphony.Portal.portalSettings);
                }
            },
            isLoadedPortalConfig: false,
            layout: 'border',
            cls: 'portal',
            listeners: {
                afterrender: function (viewport) {
                    // Url structure looks like /[area]/[company]?[params]#[module]/[tab]/[id]
                    // Cut out the first two items
                    /*
                    var urlParts = window.location.hash.substring(1).split('?')[0].split('/');
                    if (urlParts.length) {
                        this.selectTab(urlParts.join('/'));
                    }*/
                    this.initHistory();
                }
            },
            items: [{
                region: 'north',
                /*autoHeight: true,*/
                border: false,
                xtype: 'symphonybar',
                id: 'symphonybar',
                margins: '0 0 5 0'//,
                //height: 42
            }, {
                region: 'center',
                id: 'symphonymain',
                cls: 'center',
                layout: 'card',
                activeItem: 0,
                items: [{
                    //xtype: 'portal',
                    xtype: 'portal.portalpanel',
                    name: 'portal.app',
                    html: '<div class="symphony-logo"></div>',
                    border: false,
                    id: 'portal',
                    stateId: 'portal',
                    stateEvents: ['drop'],
                    currentState: {},
                    getState: function () {
                        var portalState = Ext.getCmp('portal').currentState;
                        return portalState;
                    },
                    applyState: function (e) {
                        var portal = Ext.getCmp('portal');

                        for (var id in e) {
                            var portlet = portal.query("[itemId=" + id + "]")[0];
                            portal.moveItem(portlet, e[id].row, e[id].column);
                        }
                        portal.currentState = e;
                    },
                    listeners: {
                        activate: handleModuleChange,
                        // update the state information every time an item in the portal is moved
                        afterlayout: function () {
                            if (Symphony.App) {
                                Symphony.App.loadPortalConfig();
                            }
                        },
                        drop: function (e) {
                            var index = e.columnIndex;
                            var position = e.position;
                            var id = e.panel.id;
                            Ext.getCmp('portal').currentState[id] = { row: position, column: index };
                        }
                    },
                    items: grid
                },
                // note that these types must match the indexes used in the 'symphonybar' item
                {
                    xtype: 'classroom.app',
                    listeners: {
                        activate: handleModuleChange,
                        tabchange: handleModuleTabChange
                    }
                }, {
                    xtype: 'customer.app',
                    listeners: {
                        activate: handleModuleChange,
                        tabchange: handleModuleTabChange
                    }
                }, {
                    xtype: 'courseassignment.app',
                    listeners: {
                        activate: handleModuleChange,
                        tabchange: handleModuleTabChange
                    }
                }, {
                    xtype: 'artisan.app',
                    listeners: {
                        activate: handleModuleChange
                    }
                }, {
                    xtype: 'panel',
                    name: 'reporting',
                    listeners: {
                        activate: handleModuleChange,
                        afterrender: function () {
                            // create an iframe
                            var iframe = document.createElement('iframe');
                            if (Symphony.User.cnrToken) {
                                iframe.src = Symphony.Portal.reportingLoginUrl + '?CNRServiceID=' + Symphony.User.cnrToken;
                            } else {
                                iframe.src = Symphony.Portal.reportingDefaultUrl;
                            }
                            iframe.id = 'reporting_frame';
                            iframe.width = '100%';
                            iframe.height = '100%';
                            iframe.style.border = 0;

                            this.getLayout().getRenderTarget().dom.appendChild(iframe);
                            Ext.get(iframe.id).on('load', function () {
                                iframe.src = Symphony.Portal.reportingUrl;
                            }, this, { single: true });
                        }
                    }
                }, {
                    xtype: 'reporting.app',
                    listeners: {
                        activate: handleModuleChange
                    }
                }, {
                    xtype: 'instructors.app',
                    listeners: {
                        activate: handleModuleChange
                    }
                }, {
                    xtype: 'students.app',
                    listeners: {
                        activate: handleModuleChange
                    }
                }, {
                    xtype: 'licenses.app',
                    listeners: {
                        activate: handleModuleChange
                    }
                }]
            }]
        });
    });
    Symphony.Portal.ClassRegistrationWindow = Ext.define('portal.classregistrationwindow', {
        alias: 'widget.portal.classregistrationwindow',
        extend: 'Ext.Window',
        initComponent: function () {
            var me = this;
            var items = [{
                layout: 'fit',
                items: [{
                    region: 'center',
                    id: 'portal.classdetailsgrid',
                    xtype: 'portal.classdetailsgrid',
                    url: me.url,
                    border: false,
                    courseId: this.initialConfig.courseId,
                    minDate: this.initialConfig.minDate,
                    maxDate: this.initialConfig.maxDate,
                    listeners: {
                        cellclick: _ui(this) ? function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
                            
                            var fieldName = grid.getColumnModel().getDataIndex(cellIndex);
                            var data = record.get(fieldName);

                            if (data) {
                                Symphony.LinkHandlers.click(me, e, data, record, function () {
                                    me.close();
                                });
                    }
                        } : this.save.createDelegate(this)
                    }
                }]
            }];
            Ext.apply(this, {
                title: 'Register for a Class',
                width: 550,
                height: 350,
                items: items,
                border: false,
                layout: 'fit',
                modal: true
            });
            this.callParent(arguments);
        },
        save: function (grid, td, cellIndex, record, tr, rowIndex, e) {
            _deprecate(this);
            var fieldName = grid.getColumnModel().getDataIndex(cellIndex);
            if (fieldName != 'displayRegisterLink') {
                return;
            }
            var me = this;
            if (!record) {
                return;
            } else {
                if (record.get('registrationClosed')) {
                    return; // do not proceed with registration if registration is closed
                }
                var classId = record.get('classId');
                Symphony.Portal.registerClass(classId, function () {
                    me.close();
                });
            }
        },
        cancel: function () {
            this.close();
        }
    });

    Symphony.Portal.registerClass = function (classId, callback) {
        Symphony.Portal.viewClassDetails(classId, false, function (result) {
            if (result == 'yes') {
                var target = '/services/portal.svc/classes/register/' + classId;
                Symphony.Ajax.request({
                    url: target,
                    success: function (result) {
                        if (result.notice) {
                            Ext.Msg.show({
                                title: "Warning",
                                buttons: Ext.MessageBox.OK,
                                msg: result.notice,
                                icon: Ext.MessageBox.WARNING,
                                closable: true
                            });

                        }
                        if (callback) {
                            callback();
                        }
                    }
                });
            }
        });
    };
    Symphony.Portal.MessagesWindow = Ext.define('Portal.MessagesWindow', {
        extend: 'Ext.Window',

        initComponent: function () {
            Ext.apply(this, {
                modal: true,
                loadedOnce: false,
                title: 'Inbox',
                closeAction: 'hide',
                height: 600,
                width: 700,
                layout: 'fit',
                items: {
                    id: 'portal.messagesgrid',
                    xtype: 'portal.messagesgrid',
                    border: false
                }
            });
            this.callParent(arguments);
        },
        show: function () {
            // after the window has loaded once, reload the grid if its re-shown
            if (this.loadedOnce) {
                Ext.getCmp('portal.messagesgrid').refresh();
            }
            this.loadedOnce = true;

            // monitor for new messages while the window is up
            this.checker = window.setInterval(function () {
                Ext.getCmp('portal.messagesgrid').refresh();
            }, 30000);

            Symphony.Portal.MessagesWindow.superclass.show.apply(this, arguments);
        },
        hide: function () {
            window.clearInterval(this.checker);
            Symphony.Portal.MessagesWindow.superclass.hide.apply(this, arguments);
        },
        destroy: function () {
            window.clearInterval(this.checker);
            Symphony.Portal.MessagesWindow.superclass.destroy.apply(this, arguments);
        }
    });

    Symphony.Portal.MeetingsWindow = Ext.define('Portal.MeetingsWindow', {
        extend: 'Ext.Window',

        initComponent: function () {
            Ext.apply(this, {
                loadedOnce: false,
                modal: true,
                title: 'Meetings',
                closeAction: 'hide',
                height: 500,
                width: 500,
                layout: 'fit',
                items: {
                    id: 'meetingsgrid',
                    xtype: 'portal.meetingsgrid',
                    pageSize: 15,
                    border: false
                }
            });
            this.callParent(arguments);
        },
        show: function () {
            if (this.loadedOnce) {
                Ext.getCmp('meetingsgrid').refresh();
            }
            this.loadedOnce = true;
            Symphony.Portal.MeetingsWindow.superclass.show.apply(this, arguments);
        }
    });

    //    Symphony.Portal.ClassDetailsWindow = Ext.define('Portal.ClassDetailsWindow', {
    //        extend: 'Ext.Window',

    //        classId: 0,
    //        initComponent: function(){
    //            Ext.apply(this, {
    //                title: 'Class Details',
    //                items: [{
    //                    name: 'content',
    //                    border: false,
    //                    padding: 15,
    //                    html: ''
    //                }]
    //            });
    //            this.callParent(arguments);
    //        },
    //        onRender: function(){
    //            Symphony.Portal.TrainingProgramWindow.superclass.onRender.apply(this, arguments);
    //            this.load();
    //        },
    //        load: function(){
    //            var me = this;
    //            
    //            var mask = new Ext.LoadMask(this.body, {msg:"Please wait..."});
    //            mask.show();
    //            
    //            Symphony.Ajax.request({
    //                method: 'GET',
    //                url: '/services/classroom.svc/classes/' + this.classId,
    //                callback: function(){
    //                    mask.hide();
    //                },
    //                success: function(result){
    //                    var template = new Ext.Template([
    //                        '<div class="class-details-view">',
    //                            '<div class="course"><span>Course: </sapn>{courseName}</div>',
    //                            '<div class="name"><span>Class: </span>{name}</div>',
    //                            '<div class="description"><span>Description: </span>{description}</div>',
    //                            '<div class="location"><span>Location: </span>{location}</div>',
    //                            '<div class="instructors"><span>Instructor(s): </span>{instructors}</div>',
    //                            '<div class="dates"><span>Date(s): </span>{dates}</div>',
    //                        '</div>'
    //                    ]);
    //                    result.data.instructors = Ext.pluck(result.data.classInstructorList, 'fullName').join(', ');
    //                    var dates = Ext.pluck(result.data.dateList, 'startDateTime');
    //                    for(var i = 0; i < dates.length; i++){
    //                        dates[i] = Symphony.dateTimeRenderer(dates[i]);
    //                    }
    //                    result.data.dates = dates.join(', ');
    //                    template.overwrite(me.find('name','content')[0].body, result.data);
    //                }
    //            });
    //        }
    //    });



    Symphony.Portal.ChangePasswordWindow = Ext.define('Portal.ChangePasswordWindow', {
        extend: 'Ext.Window',
        alias: 'widget.portal.changepasswordwindow',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                modal: true,
                title: 'Change Password',
                iconCls: 'x-menu-password',
                closeAction: 'hide',
                border: false,
                bodyBorder: false,
                width: 290,
                height: 175,
                items: {
                    xtype: 'form',
                    frame: true,
                    ref: 'passwordForm',
                    padding: 10,
                    defaultType: 'textfield',
                    items: [{
                        xtype: 'label',
                        id: 'enforcePassword',
                        cls: 'x-form-item',
                        text: 'Your password has expired - please create a new one.',
                        hidden: true
                    }, {
                        name: 'oldPassword',
                        fieldLabel: 'Old Password',
                        inputType: Symphony.User.hasPasswordMask ? 'password' : 'text'
                    }, {
                        name: 'newPassword',
                        fieldLabel: 'New Password',
                        inputType: Symphony.User.hasPasswordMask ? 'password' : 'text'
                    }],
                    buttons: [{
                        xtype: 'button',
                        text: 'Submit',
                        handler: function () {
                            var data = me.passwordForm.getForm().getFieldValues();

                            if (data.oldPassword == data.newPassword) {
                                Ext.Msg.alert('Error', 'You cannot reuse the same password.');
                                return;
                            }

                            Symphony.Ajax.request({
                                url: '/services/portal.svc/passwordchange',
                                jsonData: data,
                                success: function (result) {
                                    me.hide();
                                    Ext.Msg.alert('Password Changed', 'Password successfully changed.');
                                },
                                failure: function (result) {
                                    Ext.Msg.alert('Error', result.error);
                                }
                            });
                        },
                        margins: '0 15'
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        id: 'changePasswordCancelButton',
                        handler: function () {
                            me.hide();
                        },
                        margins: '0 15'
                    }]
                }
            });
            this.callParent(arguments);
        }
    });
    
    Symphony.Portal.SwitchUserWindow = Ext.define('portal.switchuserwindow', {
        alias: 'widget.portal.switchuserwindow',
        extend: 'Ext.Window',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                modal: true,
                title: 'Switch User',
                closeAction: 'hide',
                border: false,
                bodyBorder: false,
                layout: 'border',
                width: 1000,
                height: 600,
                items: [{
                    region: 'west',
                    width: 300,
                    ref: 'customerTree',
                    id: 'portal.saleschanneltree',
                    xtype: 'customer.saleschanneltree',
                    editable: false,
                    enableDD: false,
                    deferLoad: false,
                    listeners: {
                        saleschannelclick: function (parentId, salesChannelId, salesChannelName) {
                            me.customersGrid.setSalesChannel(salesChannelId);
                        }
                    }
                }, {
                    ref: 'customersGrid',
                    region: 'center',
                    layout: 'fit',
                    xtype: 'customer.basiccustomersgrid',
                    listeners: {
                        customerselect: function (customerId, salesChannelId, customerName) {
                            me.usersGrid.setCustomer(customerId);
                        }
                    }
                }, {
                    ref: 'usersGrid',
                    region: 'east',
                    width: 400,
                    xtype: 'customer.basicusersgrid',
                    listeners: {
                        userselect: function (userId) {
                            Symphony.Ajax.request({
                                url: '/services/portal.svc/switchuser/' + userId,
                                jsonData: {},
                                success: function (result) {
                                    me.hide();
                                    Symphony.loggingOut = true; // bypass the warning about leaving the page
                                    var subdomain = result.data;
                                    window.location = '/home/' + subdomain;
                                },
                                failure: function (result) {
                                    Ext.Msg.alert('Error', result.error);
                                }
                            });
                        }
                    }
                }]
            });
            this.callParent(arguments);
        }
    });


    Symphony.Portal.downloadTrainingProgramFile = function (fileId) {
        window.open("/services/portal.svc/trainingprogramfiles/download/" + fileId);
    };


    Symphony.Portal.requestNmlsNumber = function () {
        Symphony.Ajax.request({
            method: 'GET',
            url: '/services/customer.svc/users/' + Symphony.User.id + '/customer/' + Symphony.User.customerId,
            success: function (result) {
                var w = new Ext.Window({
                    width: 300,
                    height: 140,
                    title: 'Please enter your NMLS number',
                    modal: true,
                    closeable: false,

                    border: false,
                    items: [{
                        xtype: 'form',
                        border: false,
                        frame: true,
                        items: [{
                            xtype: 'label',
                            cls: 'x-form-item',
                            text: 'One or more training programs assigned to you require your NMLS number for your certificate.',
                            height: 35
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'NMLS Number',
                            name: 'nmlsNumber',
                            allowEmpty: false
                        }]
                    }],
                    buttons: [{
                        xtype: 'button',
                        text: 'Submit',
                        handler: function () {

                            var form = w.find('xtype', 'form')[0];

                            if (!form.isValid()) {
                                return false;
                            }

                            result.data.nmlsNumber = form.getValues().nmlsNumber;

                            Symphony.Ajax.request({
                                method: 'POST',
                                url: '/services/customer.svc/users/' + Symphony.User.id + '/customer/' + Symphony.User.customerId,
                                jsonData: result.data,
                                success: function () {
                                    Symphony.User.hasNmlsNumber = true;
                                    w.close();
                                }
                            });
                        }
                    }]
                });
                w.show();
            }
        });
    };

    Symphony.Portal.optInDefaults = {
        optInMessage: 'Your account has been granted access to a new student portal. Would you like use the new student portal as your default portal?<br/><br/>You can change your response by clicking the "Portal Settings" button.',
        optInTitle: 'Use new student portal?',
        optInConfirmText: 'Preview the new portal',
        optInCancelText: 'No, use current portal',
        optInPreviewText: 'Preview the new portal'
    };

    Symphony.Portal.redirectPrompt = function () {
        var w = new Ext.Window({
            border: false,
            width: 450,
            modal: true,
            resizable: false,
            title: Symphony.CustomerDetails.optInTitle || Symphony.Portal.optInDefaults.optInTitle,
            layout: 'fit',
            iconCls: 'question-window',
            listeners: {
                afterrender: {
                    fn: function (win) {
                        var f = win.down('#contentContainer');
                        f.doLayout();
                        var h = f.body.dom.scrollHeight;
                        if (f.getHeight() > h) {
                            h = f.getHeight();
                        }
                        if (h > 400) {
                            h = 400;
                        }
                        win.setHeight(h + 61);
                        win.center();
                    },
                    single: true
                }
            },
            items: [{
                xtype: 'panel',
                border: false,
                frame: false,
                bodyStyle: 'padding: 10px',
                cls: 'x-panel-transparent',
                itemId: 'contentContainer',
                autoScroll: true,
                items: [{
                    border: false,
                    frame: false,
                    xtype: 'panel',
                    cls: 'x-panel-transparent',
                    html: Symphony.CustomerDetails.optInMessage || Symphony.Portal.optInDefaults.optInMessage
                }]
            }],
            buttons: [{
                text: Symphony.CustomerDetails.optInConfirmText || Symphony.Portal.optInDefaults.optInConfirmText,
                iconCls: 'x-button-save',
                handler: function () {
                    w.setRedirect(true);
                }
            }, {
                text: Symphony.CustomerDetails.optInPreviewText || Symphony.Portal.optInDefaults.optInPreviewText,
                iconCls: 'x-button-advanced-search',
                handler: function () {
                    w.redirect();
                }
            }, {
                text: Symphony.CustomerDetails.optInCancelText || Symphony.Portal.optInDefaults.optInCancelText,
                iconCls: 'x-button-cancel',
                handler: function () {
                    w.setRedirect(false);
                }
            }],
            redirect: function () {
                Symphony.redirecting = true;
                window.location = Symphony.Settings.redirect;
            },
            setRedirect: function (isRedirect) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/customer.svc/users/' + Symphony.User.id + '/customer/' + Symphony.User.customerId,
                    success: function (result) {
                        result.data.isRedirectOptIn = isRedirect;
                        Symphony.Ajax.request({
                            method: 'POST',
                            url: '/services/customer.svc/users/' + Symphony.User.id + '/customer/' + Symphony.User.customerId,
                            jsonData: result.data,
                            success: function () {
                                if (isRedirect) {
                                    w.redirect();
                                }
                                w.close();
                            }
                        });
                    }
                })
            }
        });

        w.show();
    };

    var logoutWarningTimeout = null;
    var logoutTimeout = null;
    var warnThreshold = 1; //minute
    var MINUTES_TO_MILLISECONDS = 1000 * 60;
    Symphony.logoutWarning = function () {
        if ((new Date()).getTime() - lastActive.getTime() < (Symphony.Settings.timeout * MINUTES_TO_MILLISECONDS) / 5) {
            // we hit a warning within 20% of the timeout value; bail out
            // because this is due to some modal dialog causing timing issues with our setTimeout() calls
            return;
        }
        logoutTimeout = window.setTimeout(Symphony.logout, warnThreshold * MINUTES_TO_MILLISECONDS);
        Ext.Msg.show({
            title: 'Logout Warning',
            closable: false,
            msg: 'You are about to be logged out due to inactivity.',
            buttonText: {
                ok: 'Stay Logged In',
                cancel: 'Logout'
            },
            fn: function (btn) {
                if (btn == 'ok') {
                    window.clearTimeout(logoutTimeout);
                } else {
                    Symphony.logout();
                }
            },
            icon: Ext.MessageBox.WARNING
        });
    };

    Symphony.logout = function () {
        Ext.Ajax.request({
            url: '/Logout.aspx',
            success: function (result) {
                Symphony.handleLogout();
            },
            failure: function (args) {
                Ext.Msg.alert('Logout Failed', 'The logout attempt failed. Please close your browser to end your session.');
            }
        });
    };

    Symphony.checkAuthStatus = function (callback) {
        var message = "You have been logged out of Symphony.";

        Ext.Ajax.request({
            url: '/services/auth.svc/status/',
            success: function (result) {
                try {
                    var loggedIn = JSON.parse(result.responseText.toLowerCase());
                    if (loggedIn !== true) {
                        Symphony.handleLogoutMessage(message);
                    } else {
                        // We are logged in, so we can run any callbacks
                        if (typeof (callback) === 'function') {
                            // Wrap the callback in an inner catch as we don't want
                            // any errors in the callback to force a logout
                            try {
                                callback();
                            } catch (e) {
                                if (console && typeof (console.error) === 'function') {
                                    console.error(e);
                                }
                            }
                        }
                    }
                } catch (e) {
                    // Something went wrong with the parsing of the response, assume logged out
                    Symphony.handleLogoutMessage(message);
                }
            },
            failure: function (args) {
                // Will return a 401 when logged out, so kick the user out
                Symphony.handleLogoutMessage(message);
            }
        });
    };

    Symphony.handleLogoutMessage = function (message) {
        Ext.Msg.show({
            title: 'Logged Out',
            closable: false,
            msg: message,
            buttons: Ext.Msg.OK,
            fn: function (btn) {
                Symphony.handleLogout();
            },
            icon: Ext.MessageBox.INFO
        });
    }

    Symphony.handleLogout = function () {
        // bypass the warning about leaving the page
        Symphony.loggingOut = true;

        if (Symphony.reportingWindow) {
            Symphony.reportingWindow.close();
        }

        Symphony.VideoChat.Notification.disconnect();

        if (Symphony.Portal.ssoEnabled) {
            if (Symphony.Portal.ssoTimeoutRedirect.indexOf('betraining.com') > -1) {
                window.location = Symphony.Portal.ssoTimeoutRedirect;
            } else {
                window.location = Symphony.Portal.ssoTimeoutRedirect + '?customer=' + Symphony.getCustomer();
            }
        } else {
            window.location = '/login/' + Symphony.getCustomer();
        }
    }

    // start a timer to warn us about timeouts 1 minute before the timeout happens
    // the settings timeout is in minutes, but the setTimeout() call needs milliseconds
    var lastActive = new Date();
    Symphony.resetTimeout = function () {
        window.clearTimeout(logoutWarningTimeout);
        logoutWarningTimeout = window.setTimeout(Symphony.logoutWarning, (Symphony.Settings.timeout - warnThreshold) * MINUTES_TO_MILLISECONDS);
        lastActive = new Date();
        Symphony.VideoChat.Status.resetInactivityTimeout();
    };

    // when the mouse moves, consider it activity and reset the timeout
    Symphony.monitorActivity = function () {
        Ext.onReady(function () {
            Ext.getDoc().on('mousemove', Symphony.resetTimeout);
        });
        Symphony.resetTimeout();
    };

    
    Ext.onReady(function () {
        // Check if this user needs to reset his password
        if (Symphony.User.enforcePasswordReset && Symphony.User.isPasswordResetRequired) {
            if (!Symphony.Portal.enforcePassword) {
                Symphony.Portal.enforcePassword = new Symphony.Portal.ChangePasswordWindow();
            }
            Symphony.Portal.enforcePassword.show();
            // Hide stuff we dont want the users to see
            Ext.getCmp('enforcePassword').setVisible(true);
            Ext.getCmp('changePasswordCancelButton').setVisible(false);
            Ext.removeNode(Ext.select('.x-window-noborder .x-tool.x-tool-close').elements[0])
        }

        // Init Video chat
        if (Symphony.Portal.hasPermission(Symphony.Modules.VideoChat)) {
            /*if (!Symphony.VideoChat.Notification.client) {
                Symphony.VideoChat.Notification.init();
            }*/
            var status = Symphony.User.onlineStatus > 0 ? Symphony.User.onlineStatus : Symphony.VideoChat.Status.offline;
            Symphony.VideoChat.init(status);
        } else {
            delete Symphony.VideoChat.Notification.init;
        }
    });

    // start monitoring
    Symphony.monitorActivity();

    // monitor for page unload
    window.onbeforeunload = function (e) {
        // if we set this flag, the user is explicitly logging out or timed out; in either case, ignore the warning
        if (!Symphony.loggingOut && !Symphony.redirecting) {
            // user hits F5, backspace, bad link, etc
            Symphony.resetTimeout();
            return "Any unsaved content will be lost if you leave this page.";
        }
    };

    

    if (Symphony.Settings.isShowRedirectPopup) {
        Symphony.Portal.redirectPrompt();
    }

});