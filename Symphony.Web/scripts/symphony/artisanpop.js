﻿(function(){
    Ext.onReady(function(){
        var href = window.location.href;
        var id = href.substring(href.lastIndexOf('/')+1);
        
        Symphony.Ajax.request({
            method: 'GET',
            url: '/services/artisan.svc/courses/' + id,
            success: function(result){
                Symphony.App = new Ext.Viewport({
                    layout: 'fit',
                    items: [{
                        xtype: 'artisan.courseeditor',
                        cancelButton: false,
                        course: result.data
                    }]
                });
            }
        });
        
    });
})();
