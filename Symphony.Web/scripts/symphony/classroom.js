﻿(function () {
    Symphony.Classroom.App = Ext.define('classroom.app', {
        alias: 'widget.classroom.app',
        extend: Symphony.getQueryParam('tabs') == 'false' ? 'Ext.Panel' : 'Ext.TabPanel',
        layout: 'card',
        select: function (xtype) {
            var tabs = this.find('xtype', xtype);
            if (tabs.length > 0) {
                this.setActiveTab(tabs[0]);
                return true;
            }
            return false;
        },
        initComponent: function () {
            var items = [];
            var u = Symphony.User;

            if (u.isClassroomManager) {
                items.push({
                    title: 'Courses',
                    tabTip: 'Create and manage courses.',
                    xtype: 'classroom.coursestab'
                });
            }
            if (u.isClassroomManager || u.isClassroomSupervisor || u.isClassroomResourceManager || u.isClassroomInstructor) {
                items.push({
                    title: 'Classes',
                    tabTip: 'Create and manage classes.',
                    xtype: 'classroom.classestab'
                });
            }
            if (u.isClassroomManager || u.isClassroomResourceManager) {
                items.push({
                    title: 'Venues/Rooms',
                    tabTip: 'Create and manage venues and their rooms.',
                    xtype: 'classroom.venuestab'
                });
            }
            items.push({
                title: 'Training Calendar',
                tabTip: 'View and manage classes in month or list form.',
                xtype: 'classroom.trainingcalendartab',
                listeners: {
                    activate: function (panel) {
                        panel.getLayout().setActiveItem(0);
                    }
                }
            });
            if (u.isClassroomManager || u.isClassroomInstructor || u.isClassroomSupervisor) {
                items.push({
                    title: 'Registrations',
                    tabTip: 'View and manage registrations for all classes.',
                    xtype: 'classroom.registrationstab',
                    listeners: {
                        activate: function (panel) {
                            panel.refresh();
                        }
                    }
                });
            }
            Ext.apply(this, {
                requiresActivation: true, // activates the first panel on load of the tab
                border: false,
                defaults: {
                    border: false
                },
                items: items
            });
            this.callParent(arguments);
        }
    });


    Symphony.Classroom.DurationField = Ext.define('classroom.durationfield', {
        alias: 'widget.classroom.durationfield',
        extend: 'Ext.form.CompositeField',
        duration: 0,
        defaults: {
            flex: 1
        },
        isSimpleComposite: true,
        getValue: function () {
            return Symphony.Classroom.durationToMinutes(this.items.items[0].getValue(), this.items.items[1].getValue());
        },
        setValue: function (minutes) {
            var items;

            // If a form calls bindValues before this field is rendered
            // setValue will be called on this field, but the sub fields
            // will not have the setValue function yet. The structure will
            // also be different.
            //
            // Before render: this.items = [{ duration value config }, {duration units config }]
            // After render: this.items = { ... items: [{ duration value }, { duration units }] }

            if (this.items.length > 0 && !this.items.items) {
                items = this.items; // config (before render)
            } else if (this.items.items) {
                items = this.items.items; // ext form item (after rendered)
            }

            items[0].setValue ? items[0].setValue(Symphony.Classroom.getDurationValue(minutes, this.allowDays)) // ext form item (after render)
                              : items[0].value = Symphony.Classroom.getDurationValue(minutes, this.allowDays);  // config (before render)

            items[1].setValue ? items[1].setValue(Symphony.Classroom.getDurationUnits(minutes, false, this.allowDays)) // ext form item ( after render)
                              : items[1].value = Symphony.Classroom.getDurationUnits(minutes, false, this.allowDays);  // config ( before render)
        },
        initComponent: function () {
            var me = this,
                unitStore = ['minutes', 'hours'];

            if (me.allowDays) {
                unitStore = unitStore.concat(['days']);
            }

            Ext.apply(this, {
                items: [{
                    name: 'coursedurationvalue',
                    xtype: 'numberfield',
                    emptyText: 'Duration',
                    decimalPrecision: 2,
                    minValue: typeof(this.minValue == 'number') ? this.minValue : 1,
                    allowNegative: false,
                    hideLabel: true,
                    width: this.durationWidth ? this.durationWidth : 33,
                    anchor: '100%',
                    enableKeyEvents: this.enableKeyEvents,
                    value: Symphony.Classroom.getDurationValue(this.duration), // must translate to hours/minutes from minutes
                    listeners: {
                        change: function (field, newValue, oldValue) {
                            var desiredUnits = Symphony.Classroom.getDurationUnits(newValue);
                            if (field.nextSibling().value != desiredUnits) {
                                if (field.nextSibling().value == 'hours') {
                                    newValue = newValue * 60;
                                } else if (field.nextSibling().value == 'days' && me.allowDays) {
                                    newValue = newValue * 1440;
                                }
                                me.setValue(newValue);
                                units = 'minutes';
                            } else {
                                units = field.nextSibling().value;
                            }
                            me.fireEvent('change', { duration: Symphony.Classroom.durationToMinutes(newValue, units) });
                        },
                        keyUp: function (field, e) {
                            field.fireEvent('change', field, field.getValue(), field.getValue());
                        }
                    }
                },
                {
                    name: 'coursedurationunits',
                    xtype: 'combo',
                    allowBlank: false,
                    editable: false,
                    store: Symphony.quickStore(unitStore),
                    displayField: 'name',
                    valueField: 'id',
                    cls: 'x-timereference',
                    listConfig: {
                        cls: 'x-menu x-timereference-list'
                    },
                    help: this.help,
                    queryMode: 'local',
                    triggerAction: 'all',
                    hideLabel: true,
                    anchor: '100%',
                    //width: 70,
                    value: Symphony.Classroom.getDurationUnits(this.duration, this.allowDays),
                    listeners: {
                        change: function (field, newValue, oldValue) {
                            var durationValue = field.previousSibling().value;
                            me.fireEvent('change', { duration: Symphony.Classroom.durationToMinutes(durationValue, newValue) });
                        }
                    }
                }]
            });
            this.callParent(arguments);
        }
    });


    Symphony.Classroom.getDurationValue = function (minutes, allowDays) {
        if ((minutes % 1440) == 0 && allowDays) {
            return minutes / 1440;
        } else if ((minutes % 60) == 0) {
            return minutes / 60;
        } else {
            return minutes;
        }
    };

    Symphony.Classroom.getDurationUnits = function (minutes, allowSingular, allowDays) {
        if ((minutes % 1440) == 0 && allowDays) {
            return (minutes / 1440) == 1 && allowSingular ? 'day' : 'days';
        } if ((minutes % 60) == 0) {
            return (minutes / 60) == 1 && allowSingular ? 'hour' : 'hours';
        } else {
            return minutes == 1 && allowSingular ? 'minute' : 'minutes';
        }
    };

    Symphony.Classroom.durationToMinutes = function (value, units) {
        if (units == 'hours') {
            return value * 60;
        } else if (units == 'days') {
            return value * 1440;
        } else { // 'minutes'
            return value;
        }
    };

    Symphony.Classroom.durationRendererAllowDays = function (minutes, meta, record) {
        return Symphony.Classroom.getDurationValue(minutes, true) + ' ' + Symphony.Classroom.getDurationUnits(minutes, true, true);
    }

    Symphony.Classroom.durationRenderer = function (minutes, meta, record) {
        return Symphony.Classroom.getDurationValue(minutes) + ' ' + Symphony.Classroom.getDurationUnits(minutes, true);
    };

    Symphony.Classroom.deleteCourseRenderer = function (value, meta, record) {
        return '<a href="#" onclick="Symphony.Classroom.deleteCourse({0});"><img src="/images/bin.png" /></a>'.format(record.get('id'));
    };

    Symphony.Classroom.deleteClassRenderer = function (value, meta, record) {
        return '<a href="#" onclick="Symphony.Classroom.deleteClass({0});"><img src="/images/bin.png" /></a>'.format(record.get('id'));
    };

    Symphony.Classroom.deleteVenueRenderer = function (value, meta, record) {
        return '<a href="#" onclick="Symphony.Classroom.deleteVenue({0});"><img src="/images/bin.png" /></a>'.format(record.get('id'));
    };

    Symphony.deleteRenderer = function (value, meta, record) {
        return '<a href="#"><img src="/images/bin.png" ext:qtip="Delete" /></a>';
    };

    Symphony.Classroom.messageClassInstructorRenderer = function (value, meta, record) {
        return '<a href="#" onclick="Symphony.Classroom.messageClassInstructor({0});"><img src="/images/email.png" /></a>'.format(value);
    };

    Symphony.Classroom.messageStudentRenderer = function (value, meta, record) {
        return '<a href="#" onclick="Symphony.Classroom.messageStudent({0});"><img src="/images/email.png"/></a>'.format(value);
    }

    Symphony.Classroom.videoChatRenderer = function (value, meta, record) {
        var cls = Symphony.VideoChat.GetOnlineStatusClass(value),
            linkTemplate = '<a href="#" class="webcam {0} {1}{2}" onclick="Symphony.VideoChat.Notification.invite({3});"><div></div></a>';

        return Symphony.Portal.hasPermission(Symphony.Modules.VideoChat) ? 
                linkTemplate.format(cls, Symphony.VideoChat.UserIdPrefix, value, value) :
                '';
    }

    Symphony.Classroom.messageVenueResourceManagerRenderer = function (value, meta, record) {
        return '<a href="#" onclick="Symphony.Classroom.messageVenueResourceManager({0});"><img src="/images/email.png" /></a>'.format(record.get('resourceManagerId'));
    };

    Symphony.Classroom.deleteCourse = function (courseId) {
        Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this course?', function (btn) {
            if (btn == 'yes') {
                Symphony.Ajax.request({
                    url: '/services/classroom.svc/courses/delete/' + courseId,
                    success: function () {
                        var app = Symphony.App.find('xtype', 'classroom.app')[0];
                        var tab = app.find('xtype', 'classroom.coursestab')[0];
                        // refresh the grid
                        tab.find('xtype', 'classroom.coursesgrid')[0].refresh();
                        // remove the tab, if it's open
                        Ext.each(tab.find('xtype', 'classroom.courseform'), function (form) {
                            if (form.courseId == courseId) {
                                form.ownerCt.remove(form);
                            }
                        });
                    }
                });
            }
        });
    };

    Symphony.Classroom.deleteClass = function (classId) {
        Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this class?', function (btn) {
            if (btn == 'yes') {
                Symphony.Ajax.request({
                    url: '/services/classroom.svc/classes/delete/' + classId,
                    success: function () {
                        var app = Symphony.App.find('xtype', 'classroom.app')[0];
                        var tab = app.find('xtype', 'classroom.classestab')[0];
                        // refresh the grid
                        tab.find('xtype', 'classroom.classesgrid')[0].refresh();
                        // remove the tab, if it's open
                        Ext.each(tab.find('xtype', 'classroom.classform'), function (form) {
                            if (form.classId == classId) {
                                form.ownerCt.remove(form);
                            }
                        });
                    }
                });
            }
        });
    };

    Symphony.Classroom.deleteVenue = function (venueId) {
        Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this venue?', function (btn) {
            if (btn == 'yes') {
                Symphony.Ajax.request({
                    url: '/services/classroom.svc/venues/delete/' + venueId,
                    success: function () {
                        var app = Symphony.App.find('xtype', 'classroom.app')[0];
                        var tab = app.find('xtype', 'classroom.venuestab')[0];
                        // refresh the grid
                        tab.find('xtype', 'classroom.venuesgrid')[0].refresh();
                        // remove the tab, if it's open
                        Ext.each(tab.find('xtype', 'classroom.venueform'), function (form) {
                            if (form.venueId == venueId) {
                                form.ownerCt.remove(form);
                            }
                        });
                    }
                });
            }
        });
    };

    Symphony.Classroom.messageClassInstructor = function (classInstructorId) {
        var w = new Symphony.Classroom.MessageWindow({
            title: 'Message to Class Instructor',
            userIds: [classInstructorId]
        });
        w.show();
    };

    Symphony.Classroom.messageStudent = function (studentId) {
        var w = new Symphony.Classroom.MessageWindow({
            title: 'Message to Student',
            userIds: [studentId]
        });
        w.show();
    };

    Symphony.Classroom.messageVenueResourceManager = function (venueResourceManagerId) {
        var w = new Symphony.Classroom.MessageWindow({
            title: 'Message to Venue Resource Manager',
            userIds: [venueResourceManagerId]
        });
        w.show();
    };

    Symphony.Classroom.MessageWindow = Ext.define('classroom.messagewindow', {
        extend: 'Ext.Window',
        alias: 'widget.classroom.messagewindow',
        userIds: [], // normally bad, but we only ever have 1 of these at a time
        initComponent: function () {
            var me = this;
            var word = this.userIds.length == 1 ? 'person' : 'people';
            this.title = this.title + ' (' + this.userIds.length + ' ' + word + ')';
            Ext.apply(this, {
                modal: true,
                border: false,
                height: 400,
                width: 400,
                items: [{
                    bodyStyle: 'padding: 15px',
                    xtype: 'form',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [{
                        name: 'subject',
                        xtype: 'textfield',
                        fieldLabel: 'Subject'
                    }, {
                        name: 'body',
                        xtype: 'textarea',
                        anchor: '100%',
                        height: 275,
                        fieldLabel: 'Body'
                    }]
                }],
                buttons: [{
                    text: 'Send',
                    handler: function () {
                        var data = [];
                        var form = me.find('xtype', 'form')[0];

                        var body = form.findField('body').getValue();
                        body += '<br/><br/>Message sent from {!name} ({!username} - {!email})';

                        for (var i = 0; i < me.userIds.length; i++) {
                            data.push({
                                senderId: Symphony.User.id,
                                recipientId: me.userIds[i],
                                subject: form.findField('subject').getValue(),
                                body: body
                            });
                        }
                        Symphony.Ajax.request({
                            url: '/services/classroom.svc/messages/send/',
                            jsonData: data,
                            success: function () {
                                me.destroy();
                            }
                        });
                    }
                }, {
                    text: 'Cancel',
                    handler: function () {
                        me.destroy();
                    }
                }]
            });
            this.callParent(arguments);
        }
    });

    Symphony.Classroom.CoursesTab = Ext.define('Classroom.CoursesTab', {
        extend: 'Ext.Panel',
        alias: 'widget.classroom.coursestab',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                items: [{
                    split: true,
                    region: 'west',
                    border: false,
                    width: 340,
                    stateId: 'classroom.coursesgrid',
                    xtype: 'classroom.coursesgrid',
                    listeners: {
                        cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            // delete column gets ignored
                            if (columnIndex === 0) { return; }
                            // everything else opens the editor window
                            var record = grid.store.getAt(rowIndex);
                            me.addPanel(record.get('id'), record.get('name'), record.get('description'));
                        },
                        addclick: function () {
                            me.addPanel(0, 'New Course', 'New Course');
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    ref: 'tabpanel'
                    //id: 'classroom.courseeditorcontainer'
                }],
                listeners: {
                    // delayed load
                    activate: {
                        fn: function (panel) {
                            panel.find('xtype', 'classroom.coursesgrid')[0].refresh();
                        },
                        single: true
                    }
                }
            });
            this.callParent(arguments);
        },
        addPanel: function (id, name, description) {
            var me = this;

            var tabPanel = me.tabpanel; //Ext.getCmp('classroom.courseeditorcontainer');
            var found = tabPanel.query('> panel[courseId=' + id + ']')[0]; // tabPanel.getComponent(id);
            if (found && id > 0) {
                tabPanel.setActiveTab(found);
            } else {
                var panel = tabPanel.add({
                    xtype: 'classroom.courseform',
                    closable: true,
                    activate: true,
                    border: false,
                    //id: id,
                    title: name,
                    tabTip: description,
                    courseId: id,
                    listeners: {
                        save: function () {
                            me.find('xtype', 'classroom.coursesgrid')[0].refresh();
                        }
                    }
                });
                panel.show();
            }
            tabPanel.doLayout();
        }
    });


    Symphony.Classroom.ClassesTab = Ext.define('classroom.classestab', {
        alias: 'widget.classroom.classestab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                items: [{
                    split: true,
                    region: 'west',
                    border: false,
                    width: 340,
                    stateId: 'classroom.classesgrid',
                    xtype: 'classroom.classesgrid',
                    listeners: {
                        cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            // delete column gets ignored
                            if (columnIndex === 0) { return; }
                            // everything else opens the editor window
                            var record = grid.store.getAt(rowIndex);

                            me.addPanel(record.get('id'), record.get('name'), record.get('description'));
                        },
                        addclick: function () {
                            me.addPanel(0, 'New Class', 'New Class');
                        },
                        quickaddclick: function () {
                            var win = new Ext.Window({
                                items: [{
                                    xtype: 'classroom.quickaddpanel',
                                    border: false,
                                    listeners: {
                                        save: function (args) {
                                            Symphony.Ajax.request({
                                                url: '/services/classroom.svc/classes/quickadd/',
                                                jsonData: args,
                                                success: function (result) {
                                                    if (result) {
                                                        me.find('xtype', 'classroom.classesgrid')[0].refresh();
                                                        me.addPanel(result.data.id, result.data.name, result.data.description);
                                                    }
                                                }
                                            });
                                            win.destroy();
                                        },
                                        cancel: function () {
                                            win.destroy();
                                        }
                                    }
                                }],
                                modal: true,
                                resizable: false,
                                title: 'Create a Class',
                                height: 230,
                                width: 400
                            }).show();
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    ref: 'tabpanel'
                    //id: 'classroom.classeditorcontainer'
                }],
                listeners: {
                    // delayed load
                    activate: {
                        fn: function (panel) {
                            panel.find('xtype', 'classroom.classesgrid')[0].refresh();
                        },
                        single: true
                    }
                }
            });
            this.callParent(arguments);
        },
        addPanel: function (id, name, description, klass) {
            var me = this;
            var tabPanel = me.tabpanel; //Ext.getCmp('classroom.classeditorcontainer');
            
            //get all the classforms
            var found = tabPanel.query('> panel[classId=' + id + ']')[0];
            //var classForm = tabPanel.getComponent(id); // ('xtype', 'classroom.classform');
            
            if (found && id > 0) {
                tabPanel.setActiveTab(found);
            } else {
                var panel = tabPanel.add({
                    xtype: 'classroom.classform',
                    closable: true,
                    activate: true,
                    border: false,
                    //id: id,
                    title: name,
                    tabTip: description,
                    classId: id,
                    klass: klass,
                    listeners: {
                        duplicate: function (klass) {
                            klass.id = 0;
                            klass.surveyId = 0;
                            me.addPanel(0, klass.name, klass.description, klass);
                            Ext.Msg.alert('Class Duplicated', 'Your class has been duplicated.  Please remember to save your changes.');
                        },
                        save: function () {
                            me.find('xtype', 'classroom.classesgrid')[0].refresh();
                        }
                    }
                });
                panel.show();
            }
            tabPanel.doLayout();
        }
    });


    Symphony.Classroom.VenuesTab = Ext.define('classroom.venuestab', {
        alias: 'widget.classroom.venuestab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                items: [{
                    split: true,
                    region: 'west',
                    border: false,
                    width: 340,
                    stateId: 'classroom.venuesgrid',
                    xtype: 'classroom.venuesgrid',
                    listeners: {
                        cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            // delete column gets ignored
                            if (columnIndex === 0) { return; }
                            // everything else opens the editor window
                            var record = grid.store.getAt(rowIndex);
                            me.addPanel(record.get('id'), record.get('name'), record.get('description'));
                        },
                        addclick: function () {
                            me.addPanel(0, 'New Venue', 'New Venue');
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    ref: 'tabpanel'
                    //id: 'classroom.venueeditorcontainer'
                }],
                listeners: {
                    // delayed load
                    activate: {
                        fn: function (panel) {
                            panel.find('xtype', 'classroom.venuesgrid')[0].refresh();
                        },
                        single: true
                    }
                }
            });
            this.callParent(arguments);
        },
        addPanel: function (id, name, description) {
            var me = this;

            var tabPanel = me.tabpanel; //Ext.getCmp('classroom.venueeditorcontainer');
            var found = tabPanel.query('> panel[venueId=' + id + ']')[0];
            //var found = tabPanel.getComponent(id);
            if (found) {
                tabPanel.setActiveTab(found);
            } else {
                var panel = tabPanel.add({
                    xtype: 'classroom.venueform',
                    closable: true,
                    activate: true,
                    border: false,
                    //id: id,
                    title: name,
                    tabTip: description,
                    venueId: id,
                    listeners: {
                        save: function () {
                            me.find('xtype', 'classroom.venuesgrid')[0].refresh();
                        }
                    }
                });
                panel.show();
            }
            tabPanel.doLayout();
        }
    });


    Symphony.Classroom.TrainingCalendarTab = Ext.define('classroom.trainingcalendartab', {
        alias: 'widget.classroom.trainingcalendartab',
        extend: 'Ext.TabPanel',
        initComponent: function () {
            Ext.apply(this, {
                requiresActivation: true,
                border: false,
                defaults: {
                    border: false
                },
                items: [{
                    title: 'Month',
                    tabTip: 'Training calendar displayed in month format.',
                    stateId: 'classroom.trainingcalendarmonthgrid',
                    xtype: 'classroom.trainingcalendarmonthgrid'
                }, {
                    title: 'List',
                    tabTip: 'Training calendar displayed in list format.',
                    stateId: 'classroom.trainingcalendarlistgrid',
                    xtype: 'classroom.trainingcalendarlistgrid'
                }]
            });
            this.callParent(arguments);
        }
    });


    Symphony.Classroom.RegistrationsTab = Ext.define('classroom.registrationstab', {
        alias: 'widget.classroom.registrationstab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                items: [
                    {
                        xtype: 'symphony.savecancelbar',
                        region: 'north',
                        height: 27,
                        listeners: {
                            save: function () {
                                var grid = me.grid;

                                var records = grid.store.getModifiedRecords();

                                var result = [];

                                for (var i = 0; i < records.length; i++) {
                                    var record = records[i];
                                    result.push({
                                        registrationId: record.get('registrationId'),
                                        status: record.get('status')
                                    });
                                }

                                Symphony.Ajax.request({
                                    url: '/services/classroom.svc/pendingregistrations/update/',
                                    jsonData: result,
                                    success: function (result) {
                                        if (result.notice) {
                                            Ext.Msg.show({
                                                title: "Class Capacity Reached",
                                                msg: result.notice,
                                                width: 550
                                            });
                                        }
                                        grid.refresh();
                                    }
                                });
                            },
                            cancel: function () {
                                me.grid.refresh();
                            }
                        }
                    },
                    {
                        stateId: 'classroom.registrationsgrid',
                        xtype: 'classroom.registrationsgrid',
                        region: 'center',
                        listeners: {
                            render: function (cmp) {
                                me.grid = cmp;
                            }
                        }
                    }
                ]
            });

            this.callParent(arguments);
        },
        refresh: function () {
            this.grid.refresh();
        }
    });


    Symphony.Classroom.RegistrationsGrid = Ext.define('classroom.registrationsgrid', {
        alias: 'widget.classroom.registrationsgrid',
        extend: 'symphony.editableunpagedgrid',
        initComponent: function () {
            var me = this;
            var url = '/services/classroom.svc/pendingRegistrations/';


            var cache = {};

            for (var prop in Symphony.RegistrationStatusType) {
                if (Symphony.RegistrationStatusType.hasOwnProperty(prop)) {
                    var id = Symphony.RegistrationStatusType[prop]; //gets id based on status type text
                    var index = Symphony.RegistrationStatusTypeStore.find('id', id); //gets index of id from store
                    var record = Symphony.RegistrationStatusTypeStore.getAt(index);  //gets record from store
                    cache[id] = record.get('text');
                }
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left'
                },
                columns: [
                    { /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 175 },
                    { /*id: 'studentName',*/ header: 'Student Name', dataIndex: 'studentName', width: 175 },
                    { /*id: 'startDate',*/ header: 'Start Date', dataIndex: 'startDate', renderer: Symphony.dateTimeRenderer, width: 150 },
                    {
                        /*id: 'location',*/ header: 'Location', dataIndex: 'location',
                        flex: 1
                    },
                    { /*id: 'status',*/ header: 'Status', dataIndex: 'status', width: 150,
                        renderer: function (value, meta, record) {
                            return cache[value];
                        },
                        editor: new Ext.form.ComboBox({
                            triggerAction: 'all',
                            queryMode: 'local',
                            store: Symphony.RegistrationStatusTypeStore,
                            valueField: 'id',
                            displayField: 'text',
                            allowBlank: false
                        })
                    }
                ]
            });

            Ext.apply(this, {
                url: url,
                colModel: colModel,
                model: 'pendingRegistration',
                clicksToEdit: 1,
                stripeRows: true
            });

            this.callParent(arguments);
        }
    });




    Symphony.Classroom.CoursesGrid = Ext.define('classroom.coursesgrid', {
        alias: 'widget.classroom.coursesgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/classroom.svc/courses/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { header: ' ', hideable: false, renderer: Symphony.Classroom.deleteCourseRenderer, width: 28 },
                    { header: 'Public', dataIndex: 'publicIndicator', renderer: Symphony.checkRenderer, width: 40 },
                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 140, align: 'left',
                        flex: 1
                    },
                    { /*id: 'description',*/ header: 'Description', dataIndex: 'description', width: 150, align: 'left' }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }]
                },
                idProperty: 'id',
                deferLoad: true,
                url: url,
                colModel: colModel,
                model: 'course'
            });
            this.callParent(arguments);
        }
    });


    Symphony.Classroom.ClassesGrid = Ext.define('classroom.classesgrid', {
        alias: 'widget.classroom.classesgrid',
        extend: 'symphony.searchablegrid',
        readOnly: false,
        
        initComponent: function () {
            var me = this;
            var url = '/services/classroom.svc/classes/';

            var deleteColumn = [{ header: ' ', hideable: false, renderer: Symphony.Classroom.deleteClassRenderer, width: 28 }];

            var columns = [
                { header: 'Locked', dataIndex: 'lockedIndicator', renderer: Symphony.checkRenderer, width: 45 },
                {
                    /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 140, align: 'left',
                    flex: 1
                },
                { /*id: 'location',*/ header: 'Location', dataIndex: 'location', width: 85, align: 'left', renderer: this.locationRenderer },
                { /*id: 'startDate',*/ header: 'Start Date', dataIndex: 'minClassDate', width: 65, align: 'left', renderer: Symphony.shortDateRenderer },
                { /*id: 'endDate',*/ header: 'End Date', dataIndex: 'maxClassDate', width: 65, align: 'left', renderer: Symphony.shortDateRenderer }
            ]

            Ext.apply(this, {
                searchMenu: {
                    items: [{
                        text: 'Active',
                        group: 'searchGroup',
                        filter: { archived: false },
                        checked: false,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    },
                    {
                        text: 'Archived',
                        group: 'searchGroup',
                        filter: { archived: true },
                        checked: false,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }, {
                        text: 'All',
                        group: 'searchGroup',
                        checked: true,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }]
                },
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }]
                },
                idProperty: 'id',
                deferLoad: true,
                url: url,
                colModel: colModel,
                model: 'classroomClass'
            });

            if (!this.readOnly && (Symphony.User.isSalesChannelAdmin || Symphony.User.isClassroomManager)) {
                columns = deleteColumn.concat(columns);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: columns
            });

            var tbar = {
                items: [{
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function () {
                        me.fireEvent('addclick');
                    }
                }, {
                    text: 'Quick Add',
                    hidden: !Symphony.User.isClassroomManager,
                    iconCls: 'x-button-add',
                    handler: function () {
                        me.fireEvent('quickaddclick');
                    }
                }]
            };

            console.warn("CHECK EXT.APPLY BELOW - THERE ARE TWO EXT.APPLY IN THIS FUNCTION - REGEX GONE BAD?");
            Ext.apply(this, {
                tbar: !this.readOnly ? tbar : null,
                idProperty: 'id',
                deferLoad: true,
                url: url,
                colModel: colModel
                //recordDef: recordDef
            });
            this.callParent(arguments);
        },
        locationRenderer: function (value, metaData, record) {
            if (record.data.isVirtual) {
                return 'Virtual';
            }
            if (value) {
                return value;
            }
            return '';
        }
    });


    Symphony.Classroom.VenuesGrid = Ext.define('classroom.venuesgrid', {
        alias: 'widget.classroom.venuesgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/classroom.svc/venues/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { header: ' ', hideable: false, renderer: Symphony.Classroom.deleteVenueRenderer, width: 28 },
                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 140, align: 'left',
                        flex: 1
                    },
                    { /*id: 'city',*/ header: 'City', dataIndex: 'city', width: 100, align: 'left' },
                    { /*id: 'state',*/ header: 'State', dataIndex: 'stateName', width: 75, align: 'left' }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }]
                },
                idProperty: 'id',
                deferLoad: true,
                url: url,
                colModel: colModel,
                model: 'venue'
            });
            this.callParent(arguments);
        }
    });


    Symphony.Classroom.CourseForm = Ext.define('classroom.courseform', {
        alias: 'widget.classroom.courseform',
        extend: 'Ext.Panel',
        courseId: 0,
        initComponent: function () {
            var me = this;

            var categoriesUrl = '/services/classroom.svc/categories/';

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 27,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        items: [],
                        listeners: {
                            save: function () {
                                var form = me.find('xtype', 'form')[0],
                                    metaDataPanel = me.find('xtype', 'certificates.metadatapanel')[0];

                                if (!form.isValid()) { return false; }

                                var course = form.getValues();

                                if (!course.presentationTypeId) {
                                    course.presentationTypeId = 0;
                                }
                                if (!course.courseCompletionTypeId) {
                                    course.courseCompletionTypeId = 0;
                                }
                                if (!course.onlineCourseId) {
                                    course.onlineCourseId = 0;
                                }
                                if (!course.credit) {
                                    course.credit = 0;
                                }
                                if (!course.perStudentFee) {
                                    course.perStudentFee = 0;
                                }
                                if (!course.surveyId) {
                                    course.surveyId = 0;
                                }

                                course.certificateTemplateId = course.certificateTemplateId || 0;

                                course.metaDataJson = metaDataPanel.getData();
                                
                                Symphony.Ajax.request({
                                    url: '/services/classroom.svc/courses/' + me.courseId,
                                    jsonData: course,
                                    success: function (result) {

                                        // save any documents
                                        var documents = me.find('xtype', 'classroom.coursedocumentsgrid')[0];

                                        if (documents.store.getCount() > 0) {
                                            
                                            Ext.Msg.wait('Please wait while your documents are uploaded...', 'Please wait...');
                                            
                                            // For some reason, the documnet upload queue fails if it's not visible?
                                            // Make it visible, save the files, switch back to the current panel
                                            var tabpanel = me.find('xtype', 'tabpanel')[0],
                                                activePanel = tabpanel.getActiveTab();

                                            tabpanel.setActiveTab(0);
                                            setTimeout(function () {
                                                // upload new files
                                                documents.upload({
                                                    courseId: result.data.id,
                                                    onComplete: function () {
                                                        Ext.Msg.hide();
                                                        documents.setCourseId(result.data.id);
                                                        me.fireEvent('save');
                                                        tabpanel.setActiveTab(activePanel);
                                                    }
                                                });
                                            }, 100);
                                        }
                                        else {
                                            me.fireEvent('save');
                                        }

                                        metaDataPanel.setData(result.data);

                                        me.courseId = result.data.id;
                                        me.setTitle(result.data.name);
                                    }
                                });
                            },
                            cancel: Ext.bind(function () { this.ownerCt.remove(this); }, this)
                        }
                    }]
                }, {
                    region: 'center',
                    layout: 'fit',
                    defaults: {
                        border: false
                    },
                    items: [{
                        xtype: 'tabpanel',
                        bodyStyle: 'padding: 15px',
                        activeTab: 0,
                        items: [{
                            border: false,
                            layout: {
                                type: 'vbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            title: 'General',
                            frame: true,
                            autoScroll: true,
                            items: [{
                                name: 'general',
                                xtype: 'form',
                                height: 280,
                                border: false,
                                frame: false,
                                cls: 'x-panel-transparent',
                                items: [{
                                    xtype: 'fieldset',
                                    title: 'Details',
                                    items: [{
                                        layout: 'column',
                                        border: false,
                                        cls: 'x-panel-transparent',
                                        // column defaults
                                        defaults: {
                                            columnWidth: 0.5,
                                            border: false,
                                            xtype: 'panel',
                                            layout: 'anchor',
                                            cls: 'x-panel-transparent'
                                        },
                                        items: [{
                                            //left column
                                            defaults: { anchor: '100%', xtype: 'textfield' },
                                            items: [{
                                                name: 'publicIndicator',
                                                fieldLabel: 'Public',
                                                xtype: 'checkbox'
                                            }, {
                                                name: 'name',
                                                fieldLabel: 'Name',
                                                allowBlank: false
                                            }, {
                                                name: 'internalCode',
                                                fieldLabel: 'Internal Code',
                                                allowBlank: true
                                            }, {
                                                name: 'courseObjective',
                                                fieldLabel: 'Course Objective',
                                                xtype: 'textarea'
                                            }, {
                                                name: 'description',
                                                fieldLabel: 'Description'
                                            }, {
                                                xtype: 'hidden',
                                                name: 'categoryId'
                                            }, {
                                                xtype: 'compositefield',
                                                anchor: '100%',
                                                fieldLabel: 'Category',
                                                name: 'categoryCompositeField',
                                                items: [{
                                                    xtype: 'textfield',
                                                    valueField: 'id',
                                                    displayField: 'name',
                                                    allowBlank: false,
                                                    flex: 1,
                                                    emptyText: 'Select a category...',
                                                    valueNotFoundText: 'Select a category...',
                                                    readOnly: true
                                                }, {
                                                    xtype: 'button',
                                                    text: '...',
                                                    handler: function (btn) {
                                                        var w = Symphony.Categories.getCategoryEditorWindow('classroom', function (node) {
                                                            var categoryText = node.get('levelIndentText');
                                                            var categoryId = node.get('id');
                                                            me.find('name', 'categoryCompositeField')[0].items.items[0].setValue(categoryText);
                                                            me.find('name', 'categoryId')[0].setValue(categoryId);
                                                            w.close();
                                                        }, me.find('name', 'categoryId')[0].getValue()).show();
                                                    }
                                                }]
                                            }, {
                                                xtype: 'hidden',
                                                name: 'certificatePath'
                                            }, {
                                                xtype: 'hidden',
                                                name: 'certificateTemplateId'
                                            }, {
                                                xtype: 'compositefield',
                                                anchor: '100%',
                                                fieldLabel: 'Certificate',
                                                name: 'certificateCompositeField',
                                                formItemCls: Symphony.Modules.getModuleClass(Symphony.Modules.Certificates),
                                                items: [{
                                                    xtype: 'textfield',
                                                    valueField: 'id',
                                                    displayField: 'name',
                                                    allowBlank: true,
                                                    flex: 1,
                                                    emptyText: 'Select a certificate...',
                                                    valueNotFoundText: 'Select a certificate...',
                                                    readOnly: true
                                                }, {
                                                    xtype: 'button',
                                                    text: '...',
                                                    handler: function (btn) {
                                                        var w = Symphony.Certificates.getCertificateEditorWindow(function (node) {
                                                            var certificateText = node.attributes.record.get('levelIndentText');
                                                            me.find('name', 'certificateCompositeField')[0].items.items[0].setValue(certificateText);

                                                            var certificateTemplateId = node.attributes.record.get('certificateTemplateId');
                                                            if (certificateTemplateId) {
                                                                me.find('name', 'certificateTemplateId')[0].setValue(certificateTemplateId);
                                                                // in the event there was a OLDE cert type, blow away the path value
                                                                me.find('name', 'certificatePath')[0].setValue("");
                                                            }
                                                            else {
                                                                var certificatePath = node.attributes.record.get('path');
                                                                me.find('name', 'certificatePath')[0].setValue(certificatePath);
                                                                // in the event there was a new cert type, blow away the id
                                                                me.find('name', 'certificateTemplateId')[0].setValue(null);
                                                            }
                                                            w.close();
                                                        }, me.find('name', 'certificatePath')[0].getValue()).show();
                                                    }
                                                }]
                                            }, {
                                                name: 'certificateEnabled',
                                                boxLabel: 'If checked, a certificate will be provided upon completion.',
                                                hideEmptyLabel: false,
                                                xtype: 'checkboxfield',
                                                checked: true,
                                                cls: Symphony.Modules.getModuleClass(Symphony.Modules.Certificates)
                                            }]
                                        }, {
                                            // right column
                                            defaults: { anchor: '100%', xtype: 'textfield', labelWidth: 170 },
                                            items: [{
                                                name: 'numberOfDays',
                                                fieldLabel: '&nbsp;&nbsp;Number of Days',
                                                labelStyle: 'width: 170px',
                                                xtype: 'numberfield',
                                                allowBlank: false
                                            }, {
                                                name: 'dailyDuration',
                                                fieldLabel: '&nbsp;&nbsp;Daily Duration',
                                                labelStyle: 'width: 170px',
                                                xtype: 'classroom.durationfield'
                                            }, {
                                                name: 'credit',
                                                fieldLabel: '&nbsp;&nbsp;Credit',
                                                labelStyle: 'width: 170px',
                                                xtype: 'numberfield',
                                                allowBlank: true
                                            }, {
                                                name: 'perStudentFee',
                                                fieldLabel: '&nbsp;&nbsp;Per Student Fee',
                                                labelStyle: 'width: 170px',
                                                xtype: 'moneyfield',
                                                allowBlank: true
                                            }, {
                                                name: 'presentationTypeId',
                                                fieldLabel: '&nbsp;&nbsp;Presentation Type',
                                                labelStyle: 'width: 170px',
                                                xtype: 'symphony.pagedcombobox',
                                                url: '/services/classroom.svc/presentationTypes/',
                                                model: 'presentationType',
                                                bindingName: 'presentationTypeDescription',
                                                valueField: 'id',
                                                displayField: 'description',
                                                allowBlank: false,
                                                emptyText: 'Select a Presentation Type...',
                                                valueNotFoundText: 'Select a Presentation Type...'
                                            }, {
                                                name: 'courseCompletionTypeId',
                                                fieldLabel: '&nbsp;&nbsp;Course Completion Type',
                                                labelStyle: 'width: 170px',
                                                xtype: 'symphony.pagedcombobox',
                                                url: '/services/classroom.svc/courseCompletionTypes/',
                                                model: 'courseCompletionType',
                                                bindingName: 'courseCompletionTypeDescription',
                                                valueField: 'id',
                                                displayField: 'description',
                                                allowBlank: false,
                                                emptyText: 'Select a Course Completion Type...',
                                                valueNotFoundText: 'Select a Course Completion Type...',
                                                listeners: {
                                                    select: function (combo, records, index) 
                                                    {
                                                        if (records.length == 0) {
                                                            return;
                                                        }
                                                        
                                                        var record = records[0];
                                                        var courseCbo = this.ownerCt.find('name', 'onlineCourseId')[0];

                                                        if (record.get('id') == Symphony.CourseCompletionType.onlineCourse) {
                                                            courseCbo.enable();
                                                        } else {
                                                            courseCbo.disable();
                                                        }
                                                    }
                                                }
                                            }, {
                                                name: 'onlineCourseId',
                                                fieldLabel: '&nbsp;&nbsp;Online Test',
                                                labelStyle: 'width: 170px',
                                                xtype: 'symphony.pagedcombobox',
                                                url: '/services/classroom.svc/onlineCourses/',
                                                model: 'course',
                                                bindingName: 'onlineCourseName',
                                                valueField: 'id',
                                                displayField: 'name',
                                                emptyText: 'Select an Online Test...',
                                                valueNotFoundText: 'Select an Online Test...'
                                            }, {
                                                name: 'surveyId',
                                                fieldLabel: '&nbsp;&nbsp;Course Survey',
                                                labelStyle: 'width: 170px',
                                                xtype: 'symphony.pagedcombobox',
                                                url: '/services/classroom.svc/onlineSurveys/',
                                                model: 'course',
                                                bindingName: 'surveyName',
                                                valueField: 'id',
                                                displayField: 'name',
                                                emptyText: 'Select a Survey...',
                                                valueNotFoundText: 'Select a Survey...',
                                                cls: Symphony.Modules.getModuleClass(Symphony.Modules.Surveys),
                                                listeners: {
                                                    blur: function(combo) {
                                                        var mandatorySurvey = me.find('name', 'isSurveyMandatory')[0];
                                                        if (!combo.getValue()) {
                                                            mandatorySurvey.setDisabled(true);
                                                        } else {
                                                            mandatorySurvey.setDisabled(false);
                                                        }
                                                    },
                                                    select: function (combo, record, index) {
                                                        if (record) {
                                                            me.find('name', 'isSurveyMandatory')[0].setDisabled(false);
                                                        }
                                                    }
                                                }
                                            }, {
                                                name: 'isSurveyMandatory',
                                                labelStyle: 'width: 170px',
                                                fieldLabel: '&nbsp;&nbsp;Survey Mandatory',
                                                help: {
                                                    title: '',
                                                    text: 'If checked, the survey will be required before certificate is available'
                                                },
                                                xtype: 'checkbox',
                                                disabled: true,
                                                cls: Symphony.Modules.getModuleClass(Symphony.Modules.Surveys)
                                            }]
                                        }]
                                    }]
                                }]
                            }, {
                                flex: 1,
                                minHeight: 200,
                                stateId: 'classroom.coursedocumentsgrid',
                                xtype: 'classroom.coursedocumentsgrid',
                                anchor: '100% 50%'
                            }]
                        }, {
                            xtype: 'certificates.metadatapanel',
                            cls: Symphony.Modules.getModuleClass(Symphony.Modules.Certificates),
                            hidden: !Symphony.Modules.hasModule(Symphony.Modules.Certificates)
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        onRender: function () {
            Symphony.Classroom.CourseForm.superclass.onRender.apply(this, arguments);
            var me = this;
            window.setTimeout(function () {
                me.load();
            }, 1);
        },
        load: function () {
            var me = this;
            if (this.courseId) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/classroom.svc/courses/' + this.courseId,
                    success: function (result) {
                        // load up the course details
                        me.find('name', 'general')[0].bindValues(result.data);

                        me.find('xtype', 'certificates.metadatapanel')[0].setData(result.data);

                        // new cert trumps old: 
                        if (result.data.certificateTemplateId) {
                            // get the certificate details: 
                            Symphony.Ajax.request({
                                url: '/services/certificate.svc/certificateTemplates/' + result.data.certificateTemplateId,
                                method: "GET",
                                success: function (res) {
                                    console.log(res.data);
                                    me.find('name', 'certificateCompositeField')[0].items.items[0].setValue(res.data.name);
                                },
                                failure: function () {
                                }
                            });
                        } else if (result.data.certificatePath) {
                            me.find('name', 'certificateCompositeField')[0].items.items[0].setValue(Symphony.Certificates.pathToLevelIndentText(result.data.certificatePath));
                        }

                        if (result.data.courseCompletionTypeId != Symphony.CourseCompletionType.onlineCourse) {
                            me.find('name', 'onlineCourseId')[0].disable();
                        }

                        if (result.data.categoryId) {
                            me.find('name', 'categoryCompositeField')[0].items.items[0].setValue(result.data.levelIndentText);
                        }

                        if (result.data.surveyId) {
                            me.find('name', 'isSurveyMandatory')[0].setDisabled(false);
                        }

                        var documents = me.find('xtype', 'classroom.coursedocumentsgrid')[0];
                        documents.setCourseId(result.data.id);

                        if (result.data.hasClasses) {
                            var fld = me.find('name', 'courseCompletionTypeId')[0];
                            fld.disable();
                        }
                    }
                });
            } else {
                //TODO: do we need a replacement for the next line with numberOfDays or dailyDuration?
                //me.find('name','courseDurationUnitCount')[0].setValue(1);
                
                me.find('name', 'categoryCompositeField')[0].items.items[0].setValue('');
            }
        }
    });

    Ext.define('RegistrationModeModel', {
        extend: 'Ext.data.Model',
        fields: [
            'id',
            'approvalRequired',
            'isAdminRegistration',
            'text'
        ]
    });

    Symphony.Classroom.ClassForm = Ext.define('classroom.classform', {
        alias: 'widget.classroom.classform',
        extend: 'Ext.Panel',
        classId: 0,
        courseCompletionTypeId: 0,
        klass: null,
        registrationModeStore: new Ext.data.ArrayStore({
            id: 0,
            model: 'RegistrationModeModel',
            data: [
                [0, false, false, 'Self registration allowed'],
                [1, true, false, 'Approval required for self registration'],
                [2, true, true, 'Disable self registration']
            ]
        }),
        initComponent: function () {
            var me = this;
            var isNew = false;
            var disabled = !Symphony.User.isClassroomManager;

            if (!this.klass) {
                this.klass = {
                    name: 'Unsaved Class',
                    roomName: 'Unknown Room',
                    timeZoneDescription: 'Unknown Timezone',
                    numberOfDays: 0,
                    dailyDuration: 0,
                    registrationModeId: 0
                };
                isNew = true;
            }

            var virtualFields = [{
                name: 'isVirtual',
                fieldLabel: 'Virtual',
                xtype: 'checkbox',
                disabled: disabled,
                listeners: {
                    check: function (check, checked) {
                        var venue = me.find('name', 'venueId')[0];
                        var room = me.find('name', 'roomId')[0];
                        if (checked) {
                            Ext.each([venue, room], function (cmp) {
                                cmp.disable();
                                cmp.__oldValue = cmp.getValue();
                                cmp.clearValue();
                            });

                        } else {
                            Ext.each([venue, room], function (cmp) {
                                cmp.enable();
                                if (cmp.__oldValue) {
                                    cmp.setValue(cmp.__oldValue);
                                }
                            });
                        }
                        me.validateClass();

                        var key = me.find('name', 'webinarKey')[0];
                        if (key) {
                            if (checked && Symphony.Modules.hasModule(Symphony.Modules.GoToWebinar)) {
                                // enable the gotowebinar textbox
                                key.enable();
                            } else {
                                key.disable();
                            }
                        }
                    }
                }
            }];

            // if they have the webinar feature, all virtual classes
            // use gotowebinar, not gotomeeting
            if (Symphony.Modules.hasModule(Symphony.Modules.GoToWebinar)) {
                virtualFields.push({
                    xtype: 'textfield',
                    fieldLabel: 'Webinar Key',
                    disabled: true,
                    name: 'webinarKey',
                    help: {
                        title: 'What is my Webinar Key?',
                        text: '<br/>The Webinar Key can be found in the email sent to you by GoToWebinar when you created the webinar.<br/><br/>' +
                        'The Webinar Key is the last set of digits in the link in your email. <br/><br/>' +
                        'For example, in the url:<br/><br/> https://www2.gotomeeting.com/register/123456789<br/><br/>your Webinar Key is "123456789".'

                    },
                    allowBlank: true
                });
            }

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 27,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        items: [{
                            ref: '../../duplicateButton',
                            text: 'Duplicate',
                            disabled: true,
                            iconCls: 'x-button-duplicate',
                            handler: function () {
                                return me.save(true);
                            },
                            listeners: {
                                afterrender: function (cmp) {
                                    me.duplicateButton = cmp;
                                }
                            }
                        }],
                        listeners: {
                            save: Ext.bind(function() { this.save(false) }, this),
                            cancel: Ext.bind(function () { this.ownerCt.remove(this); }, this)
                        }
                    }]
                }, {
                    region: 'center',
                    layout: 'fit',
                    defaults: {
                        border: false
                    },
                    items: [{
                        xtype: 'tabpanel',
                        bodyStyle: 'padding: 15px',
                        border: false,
                        activeTab: 0,
                        listeners: {
                            beforeadd: function (panel, cmp, i) {
                                var type = cmp.getXType();

                                if (!Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards) && type === 'messageboard.app') {
                                    return false;
                                }

                                return true;
                            }
                        },
                        items: [{
                            title: 'General',
                            name: 'general',
                            xtype: 'form',
                            autoScroll: true,
                            border: false,
                            frame: true,
                            items: [{
                                xtype: 'fieldset',
                                title: 'Details',
                                name: 'details',
                                layout: 'column',
                                layoutConfig: {
                                    scrollOffset: 18
                                },
                                // column defaults
                                defaults: {
                                    border: false,
                                    frame: false,
                                    xtype: 'panel',
                                    layout: 'form',
                                    columnWidth: 0.5,
                                    cls: 'x-panel-transparent'
                                },
                                items: [{
                                    //left column
                                    defaults: { anchor: '100%', xtype: 'textfield' },
                                    items: [{
                                        name: 'courseId',
                                        fieldLabel: 'Course',
                                        xtype: 'symphony.pagedcombobox',
                                        url: '/services/classroom.svc/courses/',
                                        model: 'course',
                                        bindingName: 'courseName',
                                        valueField: 'id',
                                        displayField: 'name',
                                        allowBlank: false,
                                        disabled: disabled,
                                        emptyText: 'Select a Course...',
                                        valueNotFoundText: 'Select a Course...',
                                        listeners: {
                                            select: function (cbo, records, index) {
                                                if (records.length == 0) {
                                                    return;
                                                }

                                                var record = records[0];

                                                Ext.each(['name', 'description', 'internalCode'], function (n) {
                                                    var el = this.ownerCt.find('name', n)[0];
                                                    if (!el.getValue()) {
                                                        el.setValue(record.get(n));
                                                    }
                                                }, this);
                                                var el = this.ownerCt.find('name', 'objective')[0];
                                                if (!el.getValue()) {
                                                    el.setValue(record.get('courseObjective'));
                                                }
                                                me.klass.numberOfDays = record.get('numberOfDays');
                                                me.klass.dailyDuration = record.get('dailyDuration');

                                                // set the course completion type so the grid can be displayed properly
                                                me.find('xtype', 'classroom.attendancegrid')[0].setCourseCompletionType(record.get('courseCompletionTypeId'));
                                            }
                                        }
                                    }, {
                                        name: 'name',
                                        fieldLabel: 'Name',
                                        allowBlank: false,
                                        disabled: disabled
                                    }, {
                                        name: 'internalCode',
                                        fieldLabel: 'Internal Code',
                                        allowBlank: true,
                                        disabled: disabled
                                    }, {
                                        name: 'description',
                                        fieldLabel: 'Description',
                                        xtype: 'textarea',
                                        allowBlank: false,
                                        disabled: disabled
                                    }, {
                                        name: 'objective',
                                        fieldLabel: 'Objective',
                                        xtype: 'textarea',
                                        disabled: disabled
                                    }].concat(virtualFields).concat([
                                    {
                                        name: 'lockedIndicator',
                                        fieldLabel: 'Locked',
                                        xtype: 'checkbox',
                                        disabled: disabled
                                    },
                                    Symphony.getCombo('registrationModeId', 'Registration Mode', me.registrationModeStore, { value: 1, allowBlank: false }),
                                    {
                                        name: 'venueId',
                                        fieldLabel: 'Venue',
                                        xtype: 'symphony.pagedcombobox',
                                        url: '/services/classroom.svc/venues/',
                                        model: 'venue',
                                        bindingName: 'venueName',
                                        valueField: 'id',
                                        displayField: 'name',
                                        emptyText: 'Select a Venue...',
                                        valueNotFoundText: 'Select a Venue...',
                                        allowBlank: false,
                                        disabled: disabled,
                                        listeners: {
                                            select: function (combo, records, index) {
                                                var room = me.find('name', 'roomId')[0];
                                                room.setUrl('/services/classroom.svc/venues/' + combo.value + '/rooms/');
                                                room.clearValue();
                                                room.enable();
                                                me.klass.timeZoneDescription = records[0].get('timeZoneDescription');
                                            },
                                            disable: function () {
                                                var room = me.find('name', 'roomId')[0];
                                                room.disable();
                                            },
                                            enable: function () {
                                                if (this.getValue()) {
                                                    var room = me.find('name', 'roomId')[0];
                                                    room.enable();
                                                }
                                            }
                                        }
                                    }, {
                                        name: 'roomId',
                                        fieldLabel: 'Room',
                                        xtype: 'symphony.pagedcombobox',
                                        url: '/services/classroom.svc/rooms/',
                                        model: 'room',
                                        bindingName: 'roomName',
                                        valueField: 'id',
                                        displayField: 'name',
                                        emptyText: 'Select a Room...',
                                        valueNotFoundText: 'Select a Room...',
                                        disabled: true,
                                        allowBlank: false,
                                        listeners: {
                                            select: function (cbo, records) {
                                                me.find('name', 'capacityOverride')[0].setValue(records[0].get('capacity'));
                                                me.find('name', 'classroom.registrationtrees')[0].setCapacity(records[0].get('capacity'));
                                                me.klass.roomName = records[0].get('name');
                                            }
                                        }
                                    }, {
                                        name: 'capacityOverride',
                                        fieldLabel: 'Capacity',
                                        xtype: 'numberfield',
                                        allowBlank: false,
                                        disabled: disabled,
                                        enableKeyEvents: true,
                                        listeners: {
                                            keyup: function (field) {
                                                me.find('name', 'classroom.registrationtrees')[0].setCapacity(field.getRawValue() || 0);
                                            }
                                        }
                                    }, {
                                        name: 'additionalInstructions',
                                        fieldLabel: 'Additional Comments',
                                        disabled: disabled,
                                        xtype: 'textarea'
                                    }])
                                }, {
                                    defaults: { anchor: '100%', xtype: 'textfield', height: 131 },
                                    layout: {
                                        type: 'vbox',
                                        pack: 'start',
                                        align: 'stretch'
                                    },
                                    items: [{
                                        xtype: 'classroom.classcontactbar',
                                        style: 'margin-left: 15px',
                                        height: 22
                                    }, {
                                        stateId: 'classroom.classinstructorsgrid',
                                        xtype: 'classroom.classinstructorsgrid',
                                        classId: me.classId,
                                        disabled: disabled,
                                        style: 'margin-left: 15px; margin-top: 15px'
                                    }, {
                                        stateId: 'classroom.classresourcesgrid',
                                        xtype: 'classroom.classresourcesgrid',
                                        classId: me.classId,
                                        disabled: disabled,
                                        style: 'margin-left: 15px; margin-top: 15px'
                                    }, {
                                        stateId: 'classroom.classschedulegrid',
                                        xtype: 'classroom.classschedulegrid',
                                        classId: me.classId,
                                        disabled: disabled,
                                        style: 'margin-left: 15px; margin-top: 15px',
                                        listeners: {
                                            manageschedule: function () {
                                                var klass = me.getClass();

                                                if (!klass.courseId) {
                                                    Ext.Msg.alert('Select a Course', 'Please select a course first. Courses define the number of days the class will run.');
                                                    return false;
                                                }

                                                if (klass.name == '') { delete klass.name; }
                                                klass = Ext.apply(me.klass, klass);
                                                this.setClass(klass);
                                            },
                                            setstartdateclick: function () {
                                                var klass = me.getClass();

                                                var grid = this;
                                                if (!klass.courseId) {
                                                    Ext.Msg.alert('Select a Course', 'Please select a course first. Courses define the number of days the class will run.');
                                                    return false;
                                                }

                                                if (klass.name == '') { delete klass.name; }
                                                klass = Ext.apply(me.klass, klass);
                                                var cm = new Symphony.Classroom.ClassScheduleManager({
                                                    klass: klass,
                                                    classId: klass.id,
                                                    listeners: {
                                                        schedulechanged: function (dateList) {
                                                            klass.dateList = dateList;
                                                            grid.setClass(klass);
                                                        }
                                                    }
                                                });
                                                cm.showReScheduler('Set Start Date/Time');
                                            }
                                        }
                                    }, {
                                        style: 'margin-left: 15px; margin-top: 15px',
                                        xtype: 'messageboard.create',
                                        name: 'createMessageBoard',
                                        hidden: me.classId > 0 || !Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards)
                                    }]
                                }]
                            }]
                        }, {
                            title: 'Registrations and Waiting List',
                            name: 'classroom.registrationtrees',
                            classId: me.classId,
                            xtype: 'classroom.registrationtrees',
                            listeners: {
                                registrationadded: function (registration) {
                                    me.find('name', 'classroom.attendancegrid')[0].addRegistration(registration);
                                },
                                registrationupdated: function (registration) {
                                    me.find('name', 'classroom.attendancegrid')[0].updateRegistration(registration);
                                },
                                registrationremoved: function (registration) {
                                    me.find('name', 'classroom.attendancegrid')[0].removeRegistration(registration);
                                }
                            }
                        }, {
                            title: 'Attendance and Scores',
                            name: 'classroom.attendancegrid',
                            disabled: disabled,
                            classId: me.classId,
                            courseCompletionTypeId: me.courseCompletionTypeId,
                            attendanceGridId: 'classroom.attendancegrid.' + me.classId,
                            xtype: 'classroom.attendancegrid'
                        }, {
                            title: 'Message Board',
                            name: 'messageboard',
                            xtype: 'messageboard.app',
                            disabled: true,
                            showForumGrid: false,
                            border: true,
                            listeners: {
                                activate: {
                                    fn: function (panel) {
                                        var classroom = me.find('name', 'general')[0].getValues();
                                        panel.loadMessageBoardByClassId(Symphony.MessageBoardType.classroom, me.classId, classroom.name, 0, null, true);
                                    },
                                    single: true
                                }
                            }
                        }, {
                            xtype: 'certificates.metadatapanel',
                            cls: Symphony.Modules.getModuleClass(Symphony.Modules.Certificates),
                            hidden: !Symphony.Modules.hasModule(Symphony.Modules.Certificates)
                        }]
                    }]
                }]
            });
            this.callParent(arguments);

            if (!isNew && this.klass) {
                // there was class data passed into the instatiation of this object
                this.loadData(this.klass);
            }
        },
        onRender: function () {
            Symphony.Classroom.ClassForm.superclass.onRender.apply(this, arguments);
            this.load();
        },
        load: function () {
            var me = this;
            
            if (this.classId) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/classroom.svc/classes/' + this.classId,
                    success: function (result) {
                        me.loadData(result.data);
                    }
                });
            }
        },
        loadData: function (data) {
            // load up the appropriate panels with the results
            var me = this;
            me.klass = data;

            if (me.klass.id > 0) {
                me.duplicateButton.enable();
            }

            var messageBoard = me.find('xtype', 'messageboard.app')[0];
            if (messageBoard) {
                messageBoard.setDisabled(false);
            }

            if (!me.title) {
                me.setTitle(me.klass.name);
            }

            var registrationModeId = this.registrationModeStore.findBy(function (record, id) {
                if (record.get('approvalRequired') === data.approvalRequired && record.get('isAdminRegistration') == data.isAdminRegistration ||
                    data.isAdminRegistration === true && record.get('isAdminRegistration') === true) {
                        return true;
                    }
                return false;
            });

            if (registrationModeId > -1) {
                data.registrationModeId = registrationModeId
            } else {
                data.registrationModeId = 0;
            }

            var venue = me.find('name', 'venueId')[0];
            var room = me.find('name', 'roomId')[0];
            if (data.isVirtual) {
                venue.valueNotFoundText = 'Select a Venue...';
                room.valueNotFoundText = 'Select a Room...';
            }

            // first, bind the basic information
            me.find('name', 'general')[0].bindValues(data);
            if (data.isThirdParty) {
                me.find('name', 'details')[0].setTitle('Details (Note: This is a Quick Add class, and is not available for public registrations or training program assignment)');
            }
            me.klass.numberOfDays = data.numberOfDays;
            me.klass.dailyDuration = data.dailyDuration;

            var venueId = venue.value;
            if (venueId && !venue.disabled) {
                room.setUrl('/services/classroom.svc/venues/' + venueId + '/rooms/');
                room.enable();
            }

            // then details
            me.find('xtype', 'classroom.classinstructorsgrid')[0].setData(data.classInstructorList);
            me.find('xtype', 'classroom.classresourcesgrid')[0].setData(data.resourceList);

            if (me.klass.id > 0) {
                me.find('xtype', 'classroom.classschedulegrid')[0].setClass(data);

                // add the registrations and waiting list
                me.find('name', 'classroom.registrationtrees')[0].setData(data.registrationList);

                // add the attendance and scores
                var attendanceGrid = me.find('name', 'classroom.attendancegrid')[0];
                attendanceGrid.setData(data.registrationList);
                attendanceGrid.setClassId(me.klass.id);
            }

            // set the capacity based on the room
            me.find('name', 'classroom.registrationtrees')[0].setCapacity(data.capacityOverride);

            // set the completion type based on the course
            me.find('name', 'classroom.attendancegrid')[0].setCourseCompletionType(data.courseCompletionTypeId);

            // Set meta
            me.find('xtype', 'certificates.metadatapanel')[0].setData(data);
        },
        save: function (duplicate) {
            var me = this;
            if (!me.validateClass()) {
                return false;
            }

            var klass = me.getClass();

            if (duplicate) {
                if (klass) {
                    for (var i = 0; i < klass.classInstructorList.length; i++) {
                        klass.classInstructorList[i].id = klass.classInstructorList[i].instructorId;
                    }
                    for (i = 0; i < klass.resourceList.length; i++) {
                        klass.resourceList[i].id = klass.resourceList[i].resourceId;
                    }
                }
                me.fireEvent('duplicate', klass);
                return false;
            }

            // if it doesn't exist (because they don't have the GTW permission) make sure we set to blank so it still saves
            if (!klass.webinarKey) {
                klass.webinarKey = '';
            }

            for (var i = 0; i < klass.classInstructorList.length; i++) {
                klass.classInstructorList[i].id = 0;
            }
            for (var i = 0; i < klass.resourceList.length; i++) {
                klass.resourceList[i].id = 0;
            }
            for (var i = 0; i < klass.registrationList.length; i++) {
                klass.registrationList[i].id = 0;
            }
            for (var i = 0; i < klass.dateList.length; i++) {
                klass.dateList[i].id = 0;
            }

            var registeredCount = 0;
            for (var i = 0; i < klass.registrationList.length; i++) {
                var registration = klass.registrationList[i];
                if (registration.registrationStatusId == Symphony.RegistrationStatusType.registered) {
                    registeredCount++;
                } else {
                    // if they're not registered, they can't have a score
                    // this prevents issues with the user setting a score that's invalid (like 'foo'),
                    // then moving the user to "waitlist" so the score isn't visible anymore, and having the
                    // server crap out because it can't convert 'foo' to a valid score
                    delete registration.score;
                }
            }

            if (registeredCount > klass.capacityOverride) {
                Ext.Msg.alert('Registrations Over Capacity', 'The number of registered users exceeds this class\'s capacity. Please move some registered users to another status (i.e. wait list, denied) or delete them.');
                return false;
            }

            if (klass.dateList.length == 0) {
                Ext.Msg.alert('Start Date/time Required', 'The start date and time are required.');
                return false;
            }

            klass.metaDataJson = me.find('xtype', 'certificates.metadatapanel')[0].getData();


            var registrationModeCombo = me.find('name', 'registrationModeId')[0];
            var registrationModeId = registrationModeCombo.getValue();

            if (!registrationModeId) {
                registrationModeId = 0;
            }

            var registrationMode = registrationModeCombo.getStore().getAt(registrationModeId);

            klass.approvalRequired = registrationMode.get('approvalRequired');
            klass.isAdminRegistration = registrationMode.get('isAdminRegistration');

            if (klass.roomId == null) {
                delete klass.roomId;
            }
            if (klass.venueId == null) {
                delete klass.venueId;
            }

            Symphony.Ajax.request({
                url: '/services/classroom.svc/classes/' + me.classId,
                jsonData: klass,
                success: function (result) {
                    me.classId = result.data.id;
                    me.setTitle(result.data.name);
                    me.klass = result.data;
                    me.loadData(me.klass);
                    me.fireEvent('save');

                    me.duplicateButton.enable();

                    var messageBoardCreate = me.find('xtype', 'messageboard.create')[0];
                    if (messageBoardCreate) {
                        messageBoardCreate.setVisible(false);
                    }

                    var completeSave = function () {
                        if (Symphony.Modules.hasModule(Symphony.Modules.GoToWebinar) && result.data.isVirtual && result.data.webinarKey) {
                            // for virtual classes with GoToWebinar, once the class is saved,
                            // we need to register the users in GoToWebinar;
                            // we fire off a second call for this one so we can put up a status bar
                            Ext.Msg.wait('Please wait while your GoToWebinar registrations are processed...', 'Processing Registrations...');
                            Symphony.Ajax.request({
                                url: '/services/classroom.svc/webinarregistration/' + me.classId,
                                success: function (result) {
                                    // all registrations completed, or at least attempted;
                                    // display a list of results to the user
                                    Ext.Msg.hide();

                                    if (result.data.length > 0) {
                                        // create the data store
                                        var store = new Ext.data.ArrayStore({
                                            fields: [
                                           { name: 'email' },
                                           { name: 'success' },
                                           { name: 'error' }
                                        ]
                                        });
                                        var records = [], allSuccessful = true;
                                        for (var i = 0; i < result.data.length; i++) {
                                            var datum = result.data[i];
                                            records.push([datum.email, datum.success, datum.error]);
                                            if (!datum.success) {
                                                allSuccessful = false;
                                            }
                                        }

                                        if (!allSuccessful) {
                                            store.loadData(records);

                                            new Ext.Window({
                                                title: 'Completed Webinar Registrations with Errors',
                                                frame: true,
                                                border: false,
                                                modal: true,
                                                height: 400,
                                                width: 400,
                                                layout: 'fit',
                                                items: [{
                                                    xtype: 'grid',
                                                    stateId: 'classroom.webinarregerrorgrid',
                                                    layout: 'fit',
                                                    store: store,
                                                    columns: [
                                                        { /*id: 'email',*/ header: 'Email', width: 120, sortable: true, dataIndex: 'email' },
                                                        { header: 'Success', width: 75, align: 'center', sortable: true, renderer: Symphony.checkRenderer, dataIndex: 'success' },
                                                        {
                                                            /*id: 'error',*/ header: 'Error', width: 75, sortable: true, dataIndex: 'error',
                                                            flex: 1
                                                        }
                                                    ]
                                                }]
                                            }).show();
                                        } else {
                                            Ext.Msg.alert('Save Complete', 'Save complete, all registered users have been successfully imported into GoToWebinar.');
                                        }
                                    } else {
                                        Ext.Msg.alert('Save Complete', 'Save complete, all registered users have already been successfully imported into GoToWebinar.');
                                    }
                                }
                            });
                        }
                    };

                    if (result.notice) {
                        Ext.Msg.show({
                            title: "Warning",
                            buttons: Ext.MessageBox.OK,
                            msg: result.notice,
                            icon: Ext.MessageBox.WARNING,
                            closable: true,
                            //width: 550,
                            fn: completeSave
                        });
                    } else {
                        completeSave();
                    }
                }
            });
        },
        validateClass: function () {
            var form = this.find('xtype', 'form')[0];

            if (form.findField('isVirtual').getValue()) {
                form.findField('venueId').allowBlank = true;
                form.findField('roomId').allowBlank = true;
            } else {
                form.findField('venueId').allowBlank = false;
                form.findField('roomId').allowBlank = false;
            }

            return form.isValid();
        },
        getClass: function () {
            var form = this.find('xtype', 'form')[0];

            var klass = {};
            Ext.apply(klass, this.klass);
            Ext.apply(klass, form.getValues());

            var courseCombo = form.find('name', 'courseId')[0];
            klass.courseName = courseCombo.lastSelectionText;

            var venueCombo = form.find('name', 'venueId')[0];
            klass.venueName = venueCombo.lastSelectionText;

            var roomCombo = form.find('name', 'roomId')[0];
            klass.roomName = roomCombo.lastSelectionText;

            if (klass.courseId == '') {
                klass.courseId = 0;
            }
            if (klass.venueId == '') {
                klass.venueId = 0;
                klass.venueName = '';
            }
            if (klass.roomId == '') {
                klass.roomId = 0;
                klass.roomName = '';
            }

            klass.classInstructorList = this.find('xtype', 'classroom.classinstructorsgrid')[0].getData();
            klass.resourceList = this.find('xtype', 'classroom.classresourcesgrid')[0].getData();
            klass.registrationList = this.find('xtype', 'classroom.registrationtrees')[0].getData();
            klass.dateList = this.find('xtype', 'classroom.classschedulegrid')[0].getData();

            //klass.numberOfDays = this.find('xtype','classroom.classschedulegrid')[0].numberOfDays;
            //klass.dailyDuration = this.find('xtype','classroom.classschedulegrid')[0].dailyDuration;

            return klass;
        }
    });


    Symphony.Classroom.RegistrationTrees = Ext.define('classroom.registrationtrees', {
        alias: 'widget.classroom.registrationtrees',
        extend: 'Ext.Panel',
        capacity: 0,
        readOnly: false,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                registrations: [],
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 27,
                    xtype: 'toolbar',
                    items: [{
                        text: 'Add Registrant',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addregistrant');
                        }
                    }]
                }, {
                    region: 'center',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [{
                        flex: 1,
                        xtype: 'symphony.searchabletree',
                        rootVisible: true,
                        autoScroll: true,
                        enableDD: !me.readOnly,
                        ddGroup: 'classroom-registrations-dd',
                        name: 'awaitingApprovalTree',
                        model: 'classroomRegistration',
                        viewConfig: {
                            plugins: {
                                ptype: 'treeviewdragdrop'
                            },
                            listeners: {
                                beforedrop: function (node, data, overModel, dropPosition, dropHandlers) {
                                    return me.permissionValidator({
                                        dropNode: node
                                    });
                                },
                                drop: function (node, data, overModel, dropPosition) {
                                    me.changeRegistration(data.records[0].get('value'), Symphony.RegistrationStatusType.awaitingApproval);
                                    data.records[0].set('registrationStatusTypeId', Symphony.RegistrationStatusType.awaitingApproval);
                                }
                            }
                        },
                        registrationStatusTypeId: Symphony.RegistrationStatusType.awaitingApproval,
                        root: {
                            text: 'Awaiting Approval',
                            allowDrag: false,
                            allowDrop: true,
                            expanded: true,
                            leaf: false,
                            iconCls: 'x-treenode-m-uaa'
                        }
                    }, {
                        flex: 1,
                        xtype: 'symphony.searchabletree',
                        autoScroll: true,
                        rootVisible: true,
                        enableDD: !me.readOnly,
                        ddGroup: 'classroom-registrations-dd',
                        name: 'registeredTree',
                        model: 'classroomRegistration',
                        viewConfig: {
                            plugins: {
                                ptype: 'treeviewdragdrop'
                            },
                            listeners: {
                                beforedrop: function (node, data, overModel, dropPosition, dropHandlers) {
                                    if (me.atCapacity()) {
                                        return false;
                                    }
                                    return me.permissionValidator({
                                        dropNode: node
                                    });
                                },
                                drop: function (node, data, overModel, dropPosition) {
                                    me.changeRegistration(data.records[0].get('value'), Symphony.RegistrationStatusType.registered);
                                    data.records[0].set('registrationStatusTypeId', Symphony.RegistrationStatusType.registered);
                                }
                            }
                        },
                        registrationStatusTypeId: Symphony.RegistrationStatusType.registered,
                        root: {
                            text: 'Registered',
                            allowDrag: false,
                            allowDrop: true,
                            expanded: true,
                            leaf: false,
                            iconCls: 'x-treenode-m-ur'
                        }
                    }, {
                        flex: 1,
                        xtype: 'symphony.searchabletree',
                        autoScroll: true,
                        rootVisible: true,
                        enableDD: !me.readOnly,
                        ddGroup: 'classroom-registrations-dd',
                        name: 'waitlistTree',
                        model: 'classroomRegistration',
                        viewConfig: {
                            plugins: {
                                ptype: 'treeviewdragdrop'
                            },
                            listeners: {
                                beforedrop: function (node, data, overModel, dropPosition, dropHandlers) {
                                    return me.permissionValidator({
                                        dropNode: node
                                    });
                                },
                                drop: function (node, data, overModel, dropPosition) {
                                    me.changeRegistration(data.records[0].get('value'), Symphony.RegistrationStatusType.waitList);
                                    data.records[0].set('registrationStatusTypeId', Symphony.RegistrationStatusType.waitList);
                                }
                            }
                        },
                        registrationStatusTypeId: Symphony.RegistrationStatusType.waitList,
                        root: {
                            text: 'Wait List',
                            allowDrag: false,
                            allowDrop: true,
                            expanded: true,
                            leaf: false,
                            iconCls: 'x-treenode-m-uwl'
                        }
                    }, {
                        flex: 1,
                        xtype: 'symphony.searchabletree',
                        autoScroll: true,
                        rootVisible: true,
                        enableDD: !me.readOnly,
                        ddGroup: 'classroom-registrations-dd',
                        name: 'deniedTree',
                        model: 'classroomRegistration',
                        viewConfig: {
                            plugins: {
                                ptype: 'treeviewdragdrop'
                            },
                            listeners: {
                                beforedrop: function (node, data, overModel, dropPosition, dropHandlers) {
                                    return me.permissionValidator({
                                        dropNode: node
                                    });
                                },
                                drop: function (node, data, overModel, dropPosition) {
                                    me.changeRegistration(data.records[0].get('value'), Symphony.RegistrationStatusType.denied);
                                    data.records[0].set('registrationStatusTypeId', Symphony.RegistrationStatusType.denied);
                                }
                            }
                        },
                        registrationStatusTypeId: Symphony.RegistrationStatusType.denied,
                        root: {
                            text: 'Denied',
                            allowDrag: false,
                            allowDrop: true,
                            expanded: true,
                            leaf: false,
                            iconCls: 'x-treenode-m-ud'
                        }
                    }]
                }],
                listeners: {
                    addregistrant: function () {
                        var win = new Ext.Window({
                            items: [{
                                id: 'classroom.userassignmentpanel',
                                xtype: 'classroom.userassignmentpanel', //'classroom.registrantselectgrid',
                                border: false,
                                filter: me.getData(),
                                listeners: {
                                    save: function (items) {
                                        if (items.length > 0) {
                                            Ext.Msg.wait('Loading...', 'Adding registrants...');
                                            var typeId = items[0].hierarchyTypeId;
                                            var nodeIds = [];
                                            for (var i = 0; i < items.length; i++) {
                                                nodeIds.push(items[i].hierarchyNodeId);
                                            }

                                            // get user information back from the server
                                            Symphony.Ajax.request({
                                                url: '/services/classroom.svc/classes/getregistrants/' + typeId,
                                                jsonData: nodeIds,
                                                success: function (result) {
                                                    Ext.Msg.hide();
                                                    var users = result.data;

                                                    var result = [];
                                                    for (var i = 0; i < users.length; i++) {
                                                        var user = users[i];

                                                        var isDuplicate = false;
                                                        // check for a duplicate user
                                                        for (prop in me.registrations) {
                                                            var r = me.registrations[prop];
                                                            if (user.id == r.registrantId) {
                                                                isDuplicate = true;
                                                                break;
                                                            }
                                                        }
                                                        if (!isDuplicate) {
                                                            var registration = {
                                                                fullName: user.fullName,
                                                                registrantId: user.id,
                                                                registrationStatusId: Symphony.RegistrationStatusType.registered
                                                            };
                                                            if (me.currentCapacity() + result.length >= me.capacity) {
                                                                registration.registrationStatusId = Symphony.RegistrationStatusType.waitList;
                                                            }
                                                            me.addData(registration, true);
                                                            result.push(registration);
                                                        }
                                                    }
                                                    me.buildTrees();
                                                    me.fireEvent('registrationadded', result);
                                                }
                                            });
                                        }
                                        win.destroy();
                                    },
                                    cancel: function () {
                                        win.destroy();
                                    }
                                }
                            }],
                            layout: 'fit',
                            modal: true,
                            resizable: false,
                            title: 'Select a Registrant',
                            height: 465,
                            width: 400
                        }).show();
                    }
                }
            });
            this.callParent(arguments);
        },
        changeRegistration: function (id, statusId) {
            var registration = this.registrations[id];
            registration.registrationStatusId = statusId;
            this.fireEvent('registrationupdated', registration);
            this.updateRegisteredCount();
        },
        permissionValidator: function (e) {
            if (this.readOnly) {
                return false;
            }
            if (!(Symphony.User.isClassroomSupervisor || Symphony.User.isClassroomManager)) {
                // only these 2 roles are allowed to edit
                return false;
            } else if (!Symphony.User.isClassroomManager && Symphony.User.isClassroomSupervisor) {
                // if they're only a supervisor, they can only manage their supervised users
                if (e.dropNode.attributes.item.supervisorId != Symphony.User.id &&
                    e.dropNode.attributes.item.secondarySupervisorId != Symphony.User.id) {
                        Ext.Msg.alert('Permission Denied', 'You can only change the status of users whom you supervise.');
                        return false;
                }
            }
            return true;
        },
        setCapacity: function (capacity) {
            this.capacity = capacity;
            this.updateRegisteredCount();
        },
        atCapacity: function () {
            var root = this.query('[name=registeredTree]')[0].store.getRootNode();
            return (root.childNodes.length >= this.capacity);
        },
        currentCapacity: function () {
            var root = this.query('[name=registeredTree]')[0].store.getRootNode();
            return (root.childNodes.length);
        },
        updateRegisteredCount: function () {
            var root = this.query('[name=registeredTree]')[0].store.getRootNode();
            root.set('text', 'Registered - ' + root.childNodes.length + ' / ' + this.capacity);
            root.commit();
        },
        setData: function (data) {
            var hbox = this;
            var me = this;

            this.registrations = {};
            for (var i = 0; i < data.length; i++) {
                this.addData(data[i], true);
            }
            this.buildTrees();
        },
        getData: function (status) {
            var results = [];
            for (var prop in this.registrations) {
                if (this.registrations.hasOwnProperty(prop)) {
                    var registration = this.registrations[prop];
                    if (status && status != registration.registrationStatusId) {
                        continue;
                    }
                    results.push({
                        id: 0,
                        attendedIndicator: registration.attendedIndicator,
                        score: registration.score || '',
                        registrationStatusId: registration.registrationStatusId,
                        registrantId: registration.registrantId,
                        classId: this.classId // can be zero, will be set on the server
                    });
                }
            }
            return results;
        },
        addData: function (registration, deferBuild) {
            if (!registration.id) {
                if (!this.idCount) {
                    this.idCount = -1;
                }
                registration.id = this.idCount--;
            }

            this.registrations[registration.id] = registration;
            if (!deferBuild) {
                this.buildTrees();
            }
        },
        removeData: function (registrationId, deferBuild) {
            delete this.registrations[registrationId];
            if (!deferBuild) {
                this.buildTrees();
            }
        },
        nodeContextMenu: function (view, record, node, index, e) {
            var me = this;
            e.stopEvent();
            if (!me.permissionValidator({ dropNode: node })) {
                return false;
            }
            var target = e.getTarget();
            var menu = new Ext.menu.Menu({
                items: []
            });

            var status = record.get('registrationStatusTypeId'); //node.attributes.registrationStatusTypeId;
            console.warn('after converting to a full ext model, this needs to be "id" instead of "value"');
            var id = record.get('value');

            menu.add({
                disabled: status == Symphony.RegistrationStatusType.awaitingApproval,
                text: 'Move to Awaiting Approval',
                iconCls: 'x-treenode-m-uaa',
                handler: function () {
                    me.changeRegistration(id, Symphony.RegistrationStatusType.awaitingApproval);
                    record.set('registrationStatusTypeId', Symphony.RegistrationStatusType.awaitingApproval);
                    me.buildTrees();
                }
            });
            menu.add({
                disabled: status == Symphony.RegistrationStatusType.registered,
                text: 'Move to Registered',
                iconCls: 'x-treenode-m-ur',
                handler: function () {
                    me.changeRegistration(id, Symphony.RegistrationStatusType.registered);
                    record.set('registrationStatusTypeId', Symphony.RegistrationStatusType.registered);
                    me.buildTrees();
                }
            });
            menu.add({
                disabled: status == Symphony.RegistrationStatusType.denied,
                text: 'Move to Denied',
                iconCls: 'x-treenode-m-ud',
                handler: function () {
                    me.changeRegistration(id, Symphony.RegistrationStatusType.denied);
                    record.set('registrationStatusTypeId', Symphony.RegistrationStatusType.denied);
                    me.buildTrees();
                }
            });
            menu.add({
                disabled: status == Symphony.RegistrationStatusType.waitList,
                text: 'Move to Wait List',
                iconCls: 'x-treenode-m-uwl',
                handler: function () {
                    me.changeRegistration(id, Symphony.RegistrationStatusType.waitList);
                    record.set('registrationStatusTypeId', Symphony.RegistrationStatusType.waitList);
                    me.buildTrees();
                }
            });
            menu.add('-');
            menu.add({
                text: 'Delete',
                iconCls: 'x-menu-delete',
                handler: function () {
                    var registration = me.registrations[node.attributes.value];
                    me.removeData(id);
                    me.fireEvent('registrationremoved', record);
                    me.updateRegisteredCount();
                }
            });

            menu.target = target;
            menu.showAt(e.getXY());
        },
        buildTrees: function () {
            if (this.rendered) {
                var me = this;
                var trees = this.find('xtype', 'symphony.searchabletree');
                var aaTree = trees[0];
                var rTree = trees[1];
                var wlTree = trees[2];
                var dTree = trees[3];

                Ext.suspendLayouts();

                Ext.each(trees, function (tree) {
                    // remove all the nodes
                    tree.getRootNode().removeAll(true);
                    tree.on('itemcontextmenu', function(view, rec, node, index, e){
                        me.nodeContextMenu(view, rec, node, index, e);
                    });
                });
                

                // add the new registrations to the appropriate trees
                for (var prop in this.registrations) {
                    if (this.registrations.hasOwnProperty(prop)) {
                        var registration = this.registrations[prop];
                        var tree = null;
                        switch (registration.registrationStatusId) {
                            case Symphony.RegistrationStatusType.awaitingApproval:
                                tree = aaTree;
                                break;
                            case Symphony.RegistrationStatusType.waitList: // wait list
                                tree = wlTree;
                                break;
                            case Symphony.RegistrationStatusType.denied: // denied
                                tree = dTree;
                                break;
                            case Symphony.RegistrationStatusType.registered: // registered
                                tree = rTree;
                                break;
                        }

                        if (tree == null) { continue; }
                       
                        // USER NAME x2
                        tree.getRootNode().appendChild(new Ext.tree.TreeNode({
                            text: registration.fullName,
                            value: registration.id,
                            item: registration,
                            leaf: true,
                            allowDrop: false,
                            isTarget: false,
                            draggable: true,
                            iconCls: 'x-treenode-user',
                            qtip: registration.fullName,
                            registrationStatusTypeId: tree.registrationStatusTypeId
                        }));
                    }
                }

                // resume layouts
                Ext.resumeLayouts(true)

                this.updateRegisteredCount();
            } else {
                this.on('render', this.buildTrees, this, { single: true });
            }
        }
    });


    Symphony.Classroom.ScoreToolTip = null;
    Symphony.Classroom.SetScore = function (classId, registrantId, courseCompletionTypeId, input, score) {
        //console.log("Setting the score");
        var original = score;
        var errorText = '';
        if (courseCompletionTypeId == Symphony.CourseCompletionType.letter) {
            score = (score + '').replace(/[^ABCDFabcdf]/g, '');
            score = score.toUpperCase().substring(0, 1);
            errorText = 'A letter from A to F is required.';
        } else if (courseCompletionTypeId == Symphony.CourseCompletionType.numeric) {
            score = (score + '').replace(/[\D]/g, '');
            score = score.substring(0, 3);
            if (parseInt(score) > 100) {
                score = '100';
            }
            errorText = 'A number from 0 to 100 is required.';
        }

        // show a tooltip if they screw up
        if (Symphony.Classroom.ScoreToolTip) {
            Symphony.Classroom.ScoreToolTip.destroy();
        }
        if (original.toLowerCase() != score.toLowerCase()) {
            Symphony.Classroom.ScoreToolTip = new Ext.QuickTip({
                target: input,
                title: errorText,
                autoShow: true,
                width: 220,
                dismissDelay: 6000 // Hide after 6 seconds
            });
            var $i = Ext.get(input);
            var $xy = $i.getXY();
            Symphony.Classroom.ScoreToolTip.showAt([$xy[0], $xy[1] + $i.getHeight()]);
        }
        var data = Ext.ComponentQuery.query('[attendanceGridId=classroom.attendancegrid.' + classId + ']')[0].data;
        for (var i = 0; i < data.length; i++) {
            if (data[i].registrantId == registrantId) {
                data[i].score = score;
            }
        }
        //console.log("settings the input score value");
        input.value = score;
    };

    Symphony.Classroom.AttendanceGrid = Ext.define('classroom.attendancegrid', {
        alias: 'widget.classroom.attendancegrid',
        extend: 'symphony.searchablelocalgrid',
        classId: 0,
        showMessageColumn: false,
        showVideoChatColumn: false,
        showCanMoveForwardColumn: false,
        showScores: true,
        initComponent: function () {
            var me = this;
            this.data = this.data || [];
            
            var inputRenderer = function (value, meta, record) {
                if (me.isScoreEditable()) {
                    var cleanValue = (value + '').replace('"', '\\"');
                    return '<div class="editor-unfocused">' + cleanValue + '</div>';
                } else if (me.courseCompletionTypeId == Symphony.CourseCompletionType.attendance) {
                    return '';
                } else if (me.courseCompletionTypeId == Symphony.CourseCompletionType.onlineCourse) {
                    if (record.get('attendedIndicator') == true)
                        return value;
                    else
                        return '-';
                } else {
                    return value;
                }
            };

            var columns = [{
                /*id: 'registrant',*/ header: 'Registrant', dataIndex: 'fullName', align: 'left',
                flex: 1
            }];

            var scores = [{
                type: 'checkbox', /*id: 'attended',*/ header: 'Attended', dataIndex: 'attendedIndicator', width: 100, renderer: function (value) {
                    if (!value) {
                        return '<input type="checkbox"></input>';
                    }
                    return '<input type="checkbox" checked="checked"></input>';
                }
            },
                { /*id: 'score',*/ header: 'Score', dataIndex: 'score', align: 'left', width: 180, renderer: inputRenderer, editor: {
                    xtype: 'textfield',
                    fieldCls: 'editor-overlay',
                    width: '98%'
                }}
            ];

            var messageColumn = [{ header: ' ', dataIndex: 'registrantId', renderer: Symphony.Classroom.messageStudentRenderer, width: 28 }];
            var videoChatColumn = [{ header: ' ', dataIndex: 'registrantId', renderer: Symphony.Classroom.videoChatRenderer, width: 28 }];

            var canMoveForwardColumn = [{
                type: 'checkbox', /*id: 'canMoveForward',*/ header: 'Can Move Forward', dataIndex: 'canMoveForwardIndicator', width: 120, renderer: function (value) {
                    if (!value) {
                        return '<input type="checkbox"></input>';
                    }
                    return '<input type="checkbox" checked="checked"></input>';
                }
            }];

            if (this.showScores) {
                columns = columns.concat(scores);
            }

            if (this.showMessageColumn) {
                columns = messageColumn.concat(columns);
            }

            if (this.showVideoChatColumn && Symphony.Portal.hasPermission(Symphony.Modules.VideoChat)) {
                columns = videoChatColumn.concat(columns);
            }

            if (this.showCanMoveForwardColumn) {
                columns = columns.concat(canMoveForwardColumn);
            }


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: columns
            });

            //this.titleText = new Ext.Toolbar.TextItem(' ');

            Ext.apply(this, {
                tbar: {
                    items: ['']
                },
                plugins: [{
                    pluginId: 'cellediting',
                    ptype: 'cellediting',
                    clicksToEdit: 1,
                    disabled: true
                }],
                searchColumns: ['fullName'],
                //autoHeight: true,
                idProperty: 'id',
                //url: url,
                colModel: colModel,
                model: 'registration',
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var column = grid.getColumnModel().getColumnAt(columnIndex);
                        if (column.type && column.type === 'checkbox' && e.target.tagName.toLowerCase() === 'input') {
                            var record = grid.getStore().getAt(rowIndex);
                            for (var i = 0; i < this.data.length; i++) {
                                if (this.data[i].registrantId === record.get('registrantId')) {
                                    this.data[i][column.dataIndex] = e.target.checked;
                                }
                            }
                        }
                    },
                    validateedit: function (editor, e) {
                        var original = e.value;
                        var score = e.value;

                        var errorText = '';
                        if (me.courseCompletionTypeId == Symphony.CourseCompletionType.letter) {
                            score = (score + '').replace(/[^ABCDFabcdf]/g, '');
                            score = score.toUpperCase().substring(0, 1);
                            errorText = 'A letter from A to F is required.';
                        } else if (me.courseCompletionTypeId == Symphony.CourseCompletionType.numeric) {
                            score = (score + '').replace(/[\D]/g, '');
                            score = score.substring(0, 3);
                            if (parseInt(score) > 100) {
                                score = '100';
                            }
                            errorText = 'A number from 0 to 100 is required.';
                        }

                        if (original.toLowerCase() != score.toLowerCase()) {
                            var column = editor.grid.view.getCell(e.record, e.column);

                            if (Symphony.Classroom.ScoreToolTip) {
                                Symphony.Classroom.ScoreToolTip.destroy();
                            }
                            if (original.toLowerCase() != score.toLowerCase()) {
                                Symphony.Classroom.ScoreToolTip = new Ext.QuickTip({
                                    target: column,
                                    title: errorText,
                                    autoShow: true,
                                    width: 220,
                                    dismissDelay: 6000 // Hide after 6 seconds
                                });
                                var $i = Ext.get(column);
                                var $xy = $i.getXY();
                                Symphony.Classroom.ScoreToolTip.showAt([$xy[0], $xy[1] + $i.getHeight()]);
                            }

                            e.cancel = true;
                        } else {
                            e.record.set(e.field, e.value);
                            var registration = Ext.Array.findBy(me.data, function (item) {
                                return item.registrantId == e.record.get('registrantId');
                            });
                            registration.score = e.value;
                        }
                    }
                }
            });
            this.callParent(arguments);
            this.applyFilter();
        },
        getData: function () {
            return this.data;
        },
        setData: function () {
            Symphony.Classroom.AttendanceGrid.superclass.setData.apply(this, arguments);
            this.applyFilter();
        },
        setClassId: function(id) {
            this.classId = id;
            Ext.Array.each(this.data, function (item) {
                item.classId = id;
            });
            this.attendanceGridId = 'classroom.attendancegrid.' + id;
        },
        addRegistration: function (registration) {
            var classId = this.classId;

            if (Ext.isArray(registration)) {
                Ext.Array.each(registration, function (item) {
                    item.classId = classId
                });
                this.data = this.data.concat(registration);
            } else {
                registration.classId = classId;
                this.data.push(registration);
            }
            this.refresh();
            this.applyFilter();
        },
        updateRegistration: function (registration) {
            this.refresh();
            this.applyFilter();
        },
        removeRegistration: function (registration) {
            for (var i = 0; i < this.data.length; i++) {
                if (this.data[i].registrantId == registration.registrantId) {
                    this.data.splice(i, 1);
                }
            }
            this.refresh();
            this.applyFilter();
        },
        setCourseCompletionType: function (value) {
            this.courseCompletionTypeId = value;
            this.refresh();
            this.applyFilter();
            this.updateCompletionText();
            if (this.isScoreEditable()) {
                this.getPlugin('cellediting').enable();
            } else {
                this.getPlugin('cellediting').disable();
            }
        },
        isScoreEditable: function() {
            return Symphony.User.isClassroomManager && (this.courseCompletionTypeId == Symphony.CourseCompletionType.letter || this.courseCompletionTypeId == Symphony.CourseCompletionType.numeric);
        },
        updateCompletionText: function () {
            var completionType = 'Attendance';
            if (this.courseCompletionTypeId == Symphony.CourseCompletionType.letter) {
                completionType = 'Letter';
            } else if (this.courseCompletionTypeId == Symphony.CourseCompletionType.numeric) {
                completionType = 'Numeric';
            }
            //this.titleText.setText('Course Completion: ' + completionType);
        },
        applyFilter: function () {
            this.store.filter({
                property: 'registrationStatusId',
                value: Symphony.RegistrationStatusType.registered
            });
        }
    });


    Symphony.Classroom.VenueForm = Ext.define('classroom.venueform', {
        alias: 'widget.classroom.venueform',
        extend: 'Ext.Panel',
        venueId: 0,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 27,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        listeners: {
                            save: function () {
                                var form = me.find('xtype', 'form')[0];

                                if (!form.isValid()) { return false; }

                                var venue = form.getValues();

                                venue.resourceManagers = me.find('xtype', 'classroom.venueresourcemanagersgrid')[0].getData();
                                venue.rooms = me.find('xtype', 'classroom.roomsgrid')[0].getData();
                                venue.resources = me.find('xtype', 'classroom.resourcesgrid')[0].getData();

                                if (venue.stateId == '') {
                                    venue.stateId = 0;
                                }
                                if (venue.timeZoneId == '') {
                                    venue.timeZoneId = 0;
                                }

                                Symphony.Ajax.request({
                                    url: '/services/classroom.svc/venues/' + me.venueId,
                                    jsonData: venue,
                                    success: function (result) {
                                        me.venueId = result.data.id;
                                        me.setTitle(result.data.name);
                                        me.fireEvent('save');

                                        me.bindData(result.data);
                                    }
                                });
                            },
                            cancel: Ext.bind(function () { this.ownerCt.remove(this); }, this)
                        }
                    }]
                }, {
                    region: 'center',
                    autoScroll: true,
                    bodyStyle: 'padding: 15px',
                    items: [{
                        border: false,
                        items: [{
                            name: 'general',
                            xtype: 'form',
                            border: false,
                            frame: true,
                            items: [{
                                xtype: 'fieldset',
                                title: 'Details',
                                items: [{
                                    layout: 'column',
                                    cls: 'x-panel-transparent',
                                    border: false,
                                    // column defaults
                                    defaults: {
                                        columnWidth: 0.5,
                                        border: false,
                                        xtype: 'panel',
                                        layout: 'form',
                                        cls: 'x-panel-transparent'
                                    },
                                    items: [{
                                        //left column
                                        defaults: { anchor: '100%', xtype: 'textfield' },
                                        items: [{
                                            name: 'name',
                                            fieldLabel: 'Name',
                                            allowBlank: false
                                        }, {
                                            name: 'internalCode',
                                            fieldLabel: 'Internal Code',
                                            allowBlank: true
                                        }, {
                                            name: 'city',
                                            fieldLabel: 'City',
                                            allowBlank: false
                                        }, {
                                            name: 'stateId',
                                            fieldLabel: 'State',
                                            xtype: 'symphony.pagedcombobox',
                                            url: '/services/classroom.svc/states/',
                                            model: 'state',
                                            bindingName: 'stateDescription',
                                            valueField: 'id',
                                            displayField: 'description',
                                            allowBlank: false,
                                            emptyText: 'Select a State...',
                                            valueNotFoundText: 'Select a State...'
                                        }, {
                                            name: 'timeZoneId',
                                            fieldLabel: 'Time Zone',
                                            xtype: 'symphony.pagedcombobox',
                                            url: '/services/classroom.svc/timeZones/',
                                            model: 'timeZone',
                                            bindingName: 'timeZoneDescription',
                                            valueField: 'id',
                                            displayField: 'description',
                                            allowBlank: false,
                                            emptyText: 'Select a Time Zone...',
                                            valueNotFoundText: 'Select a Time Zone...'
                                        }]
                                    }, {
                                        defaults: { anchor: '100%' },
                                        items: [{
                                            stateId: 'classroom.venueresourcemanagersgrid',
                                            xtype: 'classroom.venueresourcemanagersgrid',
                                            style: 'margin-left: 15px',
                                            venueId: this.venueId
                                        }, {
                                            stateId: 'classroom.resourcesgrid',
                                            xtype: 'classroom.resourcesgrid',
                                            style: 'margin-left: 15px; margin-top: 15px',
                                            venueId: this.venueId
                                        }]
                                    }]
                                }, {
                                    stateId: 'classroom.roomsgrid',
                                    xtype: 'classroom.roomsgrid',
                                    venueId: this.venueId,
                                    style: 'margin-top: 15px; margin-bottom: 15px',
                                    anchor: '100% 50%'
                                }]
                            }]
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        onRender: function () {
            Symphony.Classroom.VenueForm.superclass.onRender.apply(this, arguments);
            this.load();
        },
        load: function () {
            var me = this;
            if (this.venueId) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/classroom.svc/venues/' + this.venueId,
                    success: function (result) {
                        me.bindData(result.data);
                    }
                });
            }
        },
        bindData: function (data) {
            this.find('name', 'general')[0].bindValues(data);

            var state = this.find('name', 'stateId')[0];
            var timeZone = this.find('name', 'timeZoneId')[0];

            if (state.value == 0) {
                state.clearValue();
            }
            if (timeZone.value == 0) {
                timeZone.clearValue();
            }

            this.find('xtype', 'classroom.venueresourcemanagersgrid')[0].setData(data.resourceManagers);
            this.find('xtype', 'classroom.roomsgrid')[0].setData(data.rooms);
            this.find('xtype', 'classroom.resourcesgrid')[0].setData(data.resources);
        }
    });


    Symphony.Classroom.ClassContactBar = Ext.define('classroom.classcontactbar', {
        alias: 'widget.classroom.classcontactbar',
        extend: 'Ext.Toolbar',
        instructorsGridType: 'classroom.classinstructorsgrid',
        studentsGridType: 'classroom.registrationtrees',
        initComponent: function () {
            Ext.apply(this, {
                border: false,
                items: [' ', ' ', {
                    xtype: 'label',
                    text: 'Contact'
                }, ' ', ' ', ' ', ' ', {
                    xtype: 'button',
                    text: 'CT Manager(s)',
                    iconCls: 'x-button-email',
                    handler: Ext.bind(this.messageManagers, this)
                }, {
                    xtype: 'button',
                    text: 'Instructor(s)',
                    iconCls: 'x-button-email',
                    handler: Ext.bind(this.messageInstructors, this)
                }, {
                    xtype: 'button',
                    text: 'Class',
                    iconCls: 'x-button-email',
                    handler: Ext.bind(this.messageClass, this)
                }].concat(this.items || [])
            });

            this.callParent(arguments);
        },
        messageManagers: function () {
            Ext.Msg.wait('Loading...', 'Finding CT Managers...');
            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/classroom.svc/ctmanagers/',
                success: function (result) {
                    Ext.Msg.hide();
                    if (result.data.length == 0) {
                        Ext.Msg.alert('No Managers Found', 'No CT Managers were found.');
                        return;
                    }
                    var w = new Symphony.Classroom.MessageWindow({
                        title: 'Message to CT Managers',
                        userIds: Ext.pluck(result.data, 'id')
                    });
                    w.show();
                }
            });
        },
        messageInstructors: function () {
            var instructors = this.ownerCt.find('xtype', this.instructorsGridType)[0].getData();

            var userIds = [];
            for (var i = 0; i < instructors.length; i++) {
                if (instructors[i].instructorId > 0) {
                    userIds.push(instructors[i].instructorId);
                } else if (instructors[i].userId > 0) {
                    userIds.push(instructors[i].userId);
                }
            }

            if (userIds.length == 0) {
                Ext.Msg.alert('No Instructors Found', 'This class currently has no instructors. Please assign some.');
                return;
            }
            var w = new Symphony.Classroom.MessageWindow({
                title: 'Message to Class Instructors',
                userIds: userIds
            });
            w.show();
        },
        messageClass: function () {
            debugger;
            var owner = this.ownerCt;
            while (!owner.find('name', this.studentsGridType)[0]) {
                owner = owner.ownerCt;
            }
            var registrants = owner.find('name', this.studentsGridType)[0].getData(Symphony.RegistrationStatusType.registered);

            var userIds = [];
            for (var i = 0; i < registrants.length; i++) {
                if (registrants[i].registrantId > 0) {
                    userIds.push(registrants[i].registrantId);
                } else if (registrants[i].get('userId') > 0) {
                    userIds.push(registrants[i].get('userId'));
                }
            }

            if (userIds.length == 0) {
                Ext.Msg.alert('No Registrants Found', 'This class currently has no registrations. Please assign some users.');
                return;
            }

            var w = new Symphony.Classroom.MessageWindow({
                title: 'Message to Class',
                userIds: userIds
            });
            w.show();
        }
    });


    Symphony.Classroom.ClassInstructorsGrid = Ext.define('classroom.classinstructorsgrid', {
        alias: 'widget.classroom.classinstructorsgrid',
        extend: 'symphony.localgrid',
        classId: 0,
        disabled: false,
        allowDelete: true,
        showVideoChatColumn: false,
        initComponent: function () {
            var me = this;
            //var url = '/services/classroom.svc/classes/' + this.classId + '/classInstructors';


            var columns = [
                { header: ' ', dataIndex: 'id', renderer: Symphony.Classroom.messageClassInstructorRenderer, width: 28 },
                {
                    /*id: 'name',*/ header: 'Name', dataIndex: 'fullName', width: 100, align: 'left',
                    flex: 1
                },
                { /*id: 'canHostVirtual',*/ header: 'Can Host Virtual', dataIndex: 'collaborator', width: 100, renderer: Symphony.checkRenderer }
            ];

            if (this.showVideoChatColumn && Symphony.Portal.hasPermission(Symphony.Modules.VideoChat)) {
                columns = [{ header: ' ', dataIndex: 'id', renderer: Symphony.Classroom.videoChatRenderer, width: 28 }].concat(columns);
            }

            if (this.allowDelete) {
                columns = [{ header: ' ', hideable: false, hideable: false, renderer: Symphony.deleteRenderer, width: 28 }].concat(columns);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: columns
            });

            var tbar = this.tbar || {
                items: [' ', ' ', {
                    xtype: 'label',
                    text: 'Instructors'
                }, '->', {
                    text: 'Add Instructor',
                    iconCls: 'x-button-add',
                    disabled: this.disabled,
                    handler: function () {
                        var win = new Ext.Window({
                            items: [{
                                stateId: 'classroom.classinstructorselectgrid',
                                xtype: 'classroom.classinstructorselectgrid',
                                border: false,
                                filter: me.getData(),
                                listeners: {
                                    select: function (grid, item) {
                                        me.store.loadData([item], true);
                                        win.destroy();
                                    }
                                }
                            }],
                            layout: 'fit',
                            modal: true,
                            resizable: false,
                            title: 'Select an Instructor',
                            height: 400,
                            width: 400
                        }).show();
                    }
                }]
            };

            var autoheight = typeof (this.autoheight) !== 'undefined' ?
                                this.autoheight :
                                true;
            Ext.apply(this, {
                tbar: tbar,
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.getStore().getAt(rowIndex);
                        if (columnIndex === 0 && me.allowDelete) {
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this class instructor?', function (btn) {
                                if (btn == 'yes') {
                                    grid.getStore().remove(record);
                                }
                            });
                        }
                    }
                },
                autoHeight: autoheight,
                idProperty: 'id',
                //url: url,
                colModel: colModel,
                model: 'classInstructor'
            });
            this.callParent(arguments);
        },
        getData: function () {
            var results = [];
            var me = this;

            this.store.each(function (record) {
                // records are users, convert them to "class instructor" records
                var item = {
                    id: 0,
                    instructorId: record.data.id,
                    fullName: record.data.fullName,
                    collaborator: record.data.collaborator,
                    classId: me.classId // can be zero, will be set on the server
                };
                results.push(item);
            });
            return results;
        }
    });


    Symphony.Classroom.ClassResourcesGrid = Ext.define('classroom.classresourcesgrid', {
        alias: 'widget.classroom.classresourcesgrid',
        extend: 'symphony.localgrid',
        classId: 0,
        disabled: false,
        allowDelete: true,
        initComponent: function () {
            var me = this;
            //var url = '/services/classroom.svc/classes/' + this.classId + '/classResources';


            var columns = [
                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 100, align: 'left',
                        flex: 1
                    },
                    { /*id: 'cost',*/ header: 'Cost', dataIndex: 'cost', width: 100, align: 'left', type: 'float', renderer: Ext.util.Format.usMoney }
            ];
            
            if (this.allowDelete) {
                columns = [{ header: ' ', hideable: false, renderer: Symphony.deleteRenderer, width: 28 }].concat(columns);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: columns
            });

            var autoheight = typeof (this.autoheight) !== 'undefined' ?
                                this.autoheight :
                                true;

            Ext.apply(this, {
                tbar: {
                    items: [' ', ' ', {
                        xtype: 'label',
                        text: 'Resources'
                    }, '->', {
                        text: 'Request Resource',
                        disabled: this.disabled,
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }]
                },
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        if (columnIndex == 0) {
                            var record = grid.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to remove this resource?', function (btn) {
                                if (btn == 'yes') {
                                    grid.getStore().remove(record);
                                }
                            });
                        }
                    },
                    addclick: function () {
                        var venueId = me.ownerCt.ownerCt.find('name', 'venueId')[0].getValue();
                        if (venueId) {
                            var win = new Ext.Window({
                                items: [{
                                    stateId: 'classroom.classresourceselectgrid',
                                    xtype: 'classroom.classresourceselectgrid',
                                    autoSize: false,
                                    border: false,
                                    venueId: venueId,
                                    filter: me.getData(),
                                    listeners: {
                                        select: function (grid, item) {
                                            me.store.loadData([item], true);
                                            win.destroy();
                                        }
                                    }
                                }],
                                layout: 'fit',
                                modal: true,
                                resizable: false,
                                title: 'Select a Resource',
                                height: 400,
                                width: 400
                            }).show();
                        } else {
                            Ext.Msg.alert('Invalid Venue', 'You must select a venue before you can request resources.');
                        }
                    }
                },
                autoHeight: autoheight,
                idProperty: 'id',
                //url: url,
                colModel: colModel,
                model: 'classResource'
            });
            this.callParent(arguments);
        },
        getData: function () {
            var results = [];
            var me = this;
            this.store.each(function (record) {
                // records are users, convert them to "venuue resource manager" records
                var item = {
                    id: 0,
                    resourceId: record.data.id,
                    name: record.data.name,
                    cost: record.data.cost,
                    classId: me.classId // can be zero, will be set on the server
                };
                results.push(item);
            });
            return results;
        }
    });


    Symphony.Classroom.ClassScheduleGrid = Ext.define('classroom.classschedulegrid', {
        alias: 'widget.classroom.classschedulegrid',
        extend: 'symphony.localgrid',
        classId: 0,
        disabled: false,
        showHeader: true,
        showContextMenu: false,
        showChangeDuration: true,
        bubbles: ['setstartdateclick'],
        initComponent: function () {
            var me = this;
            //var url = '/services/classroom.svc/classes/' + this.classId + '/classDates/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        /*id: 'date',*/ header: 'Date', dataIndex: 'startDateTime', width: 100, align: 'left', renderer: Symphony.naturalDateRenderer,
                        flex: 1
                    },
                    { /*id: 'time',*/ header: 'Time', dataIndex: 'startDateTime', width: 80, align: 'left', renderer: Symphony.naturalTimeRendererTZ },
                    { /*id: 'duration',*/ header: 'Duration', dataIndex: 'duration', width: 75, align: 'left', renderer: Symphony.Classroom.durationRenderer }
                ]
            });

            var autoheight = typeof (this.autoheight) !== 'undefined' ?
                    this.autoheight :
                    true;

            if (me.showHeader) {
                Ext.apply(this, {
                    tbar: {
                        items: [' ', ' ', {
                            xtype: 'label',
                            text: 'Schedule'
                        }, '->', {
                            text: 'Set Start Date/Time',
                            disabled: this.disabled,
                            iconCls: 'x-button-calendar',
                            handler: function () {
                                me.fireEvent('setstartdateclick');
                            }
                        }, {
                            text: 'Manage Schedule',
                            disabled: this.disabled,
                            iconCls: 'x-button-calendar-edit',
                            handler: function () {
                                me.fireEvent('manageclick');
                            }
                        }]
                    }
                });
            }

            Ext.apply(this.listeners, {
                manageclick: function () {
                    if (!me.fireEvent('manageschedule')) {
                        return;
                    }

                    var win = new Ext.Window({
                        items: [{
                            border: false,
                            xtype: 'classroom.classschedulemanager',
                            classId: me.classId,
                            klass: me.klass,
                            listeners: {
                                schedulechanged: function (dateList) {
                                    me.fireEvent('schedulechanged', dateList);
                                }
                            }
                        }],
                        layout: 'fit',
                        modal: true,
                        resizable: false,
                        title: 'Class Schedule',
                        height: 370,
                        width: 1050
                    }).show();
                },
                schedulechanged: function (dateList) {
                    me.setData(dateList);
                },
                cellcontextmenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
                    if (!me.showContextMenu) {
                        return;
                    }
                    var target = e.getTarget();
                    var original = record.get('startDateTime');

                    var items = [{
                        text: 'Change Start Date',
                        iconCls: 'x-menu-change-date',
                        hideOnClick: false,
                        menu: new Ext.menu.DatePicker({
                            handler: function (datePicker, d) {
                                var data = me.getData();
                                // only do any of this if we have dates set
                                if (data && data.length > 0) {
                                    // date time variables are always in utc, so we don't have to do anything
                                    // to the hours. simply find the date we're changing in the list,
                                    // and update it to have the new date value
                                    var targetDateString = Symphony.parseDate(original).formatSymphony('m/d/Y');
                                    for (var i = 0; i < data.length; i++) {
                                        var dt = Symphony.parseDate(data[i].startDateTime);
                                        if (targetDateString == dt.formatSymphony('m/d/Y')) {
                                            // set the new date's hours
                                            d = d.add(Date.HOUR, dt.getHours()).add(Date.MINUTE, dt.getMinutes());
                                            me.klass.dateList[i].startDateTime = d.formatSymphony('microsoft');
                                            break;
                                        }
                                    }
                                    var dateListSort = function (a, b) {
                                        return a.startDateTime - b.startDateTime;
                                    };

                                    me.klass.dateList = me.klass.dateList.sort(dateListSort);
                                    me.fireEvent('reschedule', me.klass.dateList);

                                }
                            }
                        })
                    }, {
                        text: 'Change Start Time',
                        iconCls: 'x-menu-change-time',
                        hideOnClick: false,
                        menu: new Ext.menu.Menu({
                            items: [{
                                hideOnClick: false,
                                xtype: 'combo',
                                queryMode: 'local',
                                typeAhead: true,
                                displayField: 'text',
                                triggerAction: 'all',
                                listeners: {
                                    select: function (combo, records, index) {
                                        var data = me.getData();
                                        if (data && data.length > 0 && records.length > 0) {
                                            // date time variables are always in utc, so we don't have to do anything
                                            // to the hours. simply find the date we're changing in the list,
                                            // and update it to have the new date value
                                            var targetDateString = Symphony.parseDate(original).formatSymphony('m/d/Y');
                                            for (var i = 0; i < data.length; i++) {
                                                var dt = Symphony.parseDate(data[i].startDateTime);
                                                if (targetDateString == dt.formatSymphony('m/d/Y')) {
                                                    // set the new date's hours
                                                    var originalDate = Symphony.parseDate(original);
                                                    originalDate.setHours(records[0].get('hour'));
                                                    originalDate.setMinutes(records[0].get('minute'));

                                                    me.klass.dateList[i].startDateTime = originalDate.formatSymphony('microsoft');
                                                    break;
                                                }
                                            }
                                            var dateListSort = function (a, b) {
                                                return a.startDateTime - b.startDateTime;
                                            };

                                            me.klass.dateList = me.klass.dateList.sort(dateListSort);
                                            me.fireEvent('reschedule', me.klass.dateList);
                                        }
                                    }
                                },
                                store: Symphony.TimeStore
                            }]
                        })
                    }];
                    if (me.showChangeDuration) {
                        items.push({
                            text: 'Change Duration',
                            iconCls: 'x-menu-change-duration',
                            hideOnClick: false,
                            menu: new Ext.menu.Menu({
                                items: {
                                    xtype: 'container',
                                    width: 160,
                                    layout: 'fit',
                                    padding: 3,
                                    items: [{
                                        xtype: 'classroom.durationfield',
                                        duration: record.get('duration'),
                                        listeners: {
                                            change: function (args) {
                                                var data = me.getData();
                                                if (data && data.length > 0) {
                                                    // date time variables are always in utc, so we don't have to do anything
                                                    // to the hours. simply find the date we're changing in the list,
                                                    // and update it to have the new date value
                                                    var targetDateString = Symphony.parseDate(original).formatSymphony('m/d/Y');
                                                    for (var i = 0; i < data.length; i++) {
                                                        var dt = Symphony.parseDate(data[i].startDateTime);
                                                        if (targetDateString == dt.formatSymphony('m/d/Y')) {
                                                            me.klass.dateList[i].duration = args.duration;
                                                            break;
                                                        }
                                                    }
                                                    me.fireEvent('reschedule', me.klass.dateList);
                                                }
                                            }
                                        }
                                    }]
                                }
                            })
                        });
                    }
                    var contextmenu = new Ext.menu.Menu({
                        items: items
                    });

                    e.stopEvent();
                    contextmenu.target = target;
                    contextmenu.showAt(e.getXY());
                }
            });

            Ext.apply(this, {
                autoHeight: autoheight,
                idProperty: 'id',
                sortInfo: {
                    field: 'startDateTime',
                    direction: 'ASC'
                },
                //url: url,
                colModel: colModel,
                model: 'classDate'
            });
            this.callParent(arguments);
        },
        setData: function (data) {
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    if (!item.id) {
                        if (!this.idCount) {
                            this.idCount = -1;
                        }
                        item.id = this.idCount--;
                    }
                }
            }
            this.data = data;
            this.store.loadData(data || []);
        },
        getData: function () {
            var results = [];
            this.store.each(function (record) {
                results.push({
                    duration: record.data.duration,
                    startDateTime: record.data.startDateTime
                });
            });
            return results;
        },
        setClass: function (klass) {
            this.klass = klass;
            this.setData(klass.dateList);
        }
    });


    Symphony.Classroom.VenueResourceManagersGrid = Ext.define('classroom.venueresourcemanagersgrid', {
        alias: 'widget.classroom.venueresourcemanagersgrid',
        extend: 'symphony.localgrid',
        initComponent: function () {
            var me = this;


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { header: ' ', hideable: false, renderer: Symphony.deleteRenderer, width: 28 },
                    { header: ' ', renderer: Symphony.Classroom.messageVenueResourceManagerRenderer, width: 28 },
                    {
                        /*id: 'fullName',*/ header: 'Name', dataIndex: 'fullName', width: 100, align: 'left',
                        flex: 1
                    }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [' ', ' ', {
                        xtype: 'label',
                        text: 'Resource Managers'
                    }, '->', {
                        text: 'Add Resource Manager',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }]
                },
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        if (columnIndex == 0) {
                            var record = grid.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to remove this resource manager?', function (btn) {
                                if (btn == 'yes') {
                                    grid.getStore().remove(record);
                                }
                            });
                        }
                    },
                    addclick: function () {
                        var win = new Ext.Window({
                            items: [{
                                stateId: 'classroom.venueresourcemanagerselectgrid',
                                xtype: 'classroom.venueresourcemanagerselectgrid',
                                layout: 'fit',
                                border: false,
                                filter: me.getData(), // gets the current list of users as a filter
                                listeners: {
                                    managerselected: function (item) {
                                        delete item.id; // If an id property is set, the row will always end up with that id applied, regardless of the id property set on the proxy
                                                        // Adding 1 resource with the ID 0 will mean no more resources with the id 0 can be added. Removing the id makes ext
                                                        // handle the ids properly.
                                                        // *****NOTE!!! This allows the item id to be 0 when retrieved from the grid! This is what we want in this case!!!!
                                                        // If the item id is not 0 for a new resource manager, the server will assume it is an existing manager and 
                                                        // try to update.
                                                        //
                                                        // After save, we must load the proper ids, otherwise we will get key constraint issues!
                                                        //
                                        me.store.loadData([item], true);
                                        win.destroy();
                                    }
                                }
                            }],
                            layout: 'fit',
                            modal: true,
                            resizable: false,
                            title: 'Select a Resource Manager',
                            height: 400,
                            width: 400
                        }).show();
                    }
                },
                autoHeight: true,
                idProperty: 'resourceManagerID',
                //url: url,
                colModel: colModel,
                model: 'venueResourceManager'
            });
            this.callParent(arguments);
        },
        getData: function () {
            var results = [];
            var me = this;
            this.store.each(function (record) {
                var item = {
                    id: record.data.id || 0,
                    resourceManagerId: record.data.resourceManagerId,
                    venueId: me.venueId
                };
                results.push(item);
            });
            return results;
        }
    });


    Symphony.Classroom.ResourcesGrid = Ext.define('classroom.resourcesgrid', {
        alias: 'widget.classroom.resourcesgrid',
        extend: 'symphony.localgrid',
        venueId: 0,
        initComponent: function () {
            var me = this;


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { header: ' ', hideable: false, renderer: Symphony.deleteRenderer, width: 28 },
                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 100, align: 'left',
                        flex: 1
                    },
                    { /*id: 'cost',*/ header: 'Cost', dataIndex: 'cost', width: 70, align: 'left', type: 'float', renderer: Ext.util.Format.usMoney }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [' ', ' ', {
                        xtype: 'label',
                        text: 'Resources'
                    }, '->', {
                        text: 'Add Resource',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }]
                },
                listeners: {
                    cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        if (columnIndex == 0) {
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this resource?', function (btn) {
                                if (btn == 'yes') {
                                    grid.store.remove(record);
                                }
                            });
                        }
                    },
                    celldblclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        if (columnIndex != 0) {
                            me.editResource({ id: record.get('id'), name: record.get('name'), cost: record.get('cost') });
                        }
                    },
                    addclick: function () {
                        me.editResource();
                    }
                },
                autoHeight: true,
                colModel: colModel,
                model: 'resource'
            });
            this.callParent(arguments);
        },
        editResource: function (resource) {
            var me = this;
            var win = new Ext.Window({
                items: [{
                    xtype: 'classroom.resourceform',
                    border: false,
                    venueId: me.venueId,
                    resource: resource,
                    listeners: {
                        save: function (item) {
                            delete item.id; // If an id property is set, the row will always end up with that id applied, regardless of the id property set on the proxy
                                            // Adding 1 resource with the ID 0 will mean no more resources with the id 0 can be added. Removing the id makes ext
                                            // handle the ids properly
                            me.store.loadData([item], true);

                            win.destroy();
                        },
                        cancel: function () {
                            win.destroy();
                        }
                    }
                }],
                layout: 'fit',
                modal: true,
                resizable: false,
                title: (resource ? 'Edit' : 'Add') + ' Resource',
                height: 205,
                width: 400
            }).show();
        },
        getData: function () {
            var results = [];
            var me = this;
            this.store.each(function (r) {
                if (!r.data.id) {
                    r.data.id = 0;
                }
                if (!r.data.roomId) {
                    r.data.roomId = 0;
                }
                r.data.venueId = me.venueId;
                results.push(r.data);
            });
            return results;
        }
    });


    Symphony.Classroom.RoomsGrid = Ext.define('classroom.roomsgrid', {
        alias: 'widget.classroom.roomsgrid',
        extend: 'symphony.localgrid',
        venueId: 0,
        initComponent: function () {
            var me = this;


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { header: ' ', hideable: false, renderer: Symphony.deleteRenderer, width: 28 },
                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 100, align: 'left',
                        flex: 1
                    },
                    { /*id: 'internalCode',*/ header: 'Internal Code', dataIndex: 'internalCode', width: 80 },
                    { /*id: 'roomCost',*/ header: 'Room Cost', dataIndex: 'roomCost', width: 70, renderer: function (value) { return "$" + value; } },
                    { /*id: 'capacity',*/ header: 'Capacity', dataIndex: 'capacity', width: 60 },
                    { /*id: 'networkAccessIndicator',*/ header: 'Network Access', dataIndex: 'networkAccessIndicator', width: 100, renderer: Symphony.checkRenderer },
                    { /*id: 'lockedIndicator',*/ header: 'Locked', dataIndex: 'lockedIndicator', width: 60, renderer: Symphony.checkRenderer }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [' ', ' ', {
                        xtype: 'label',
                        text: 'Rooms'
                    }, '-', '<span style="font-style:italic">Double click to edit</span>', '->', {
                        text: 'Add Room',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }]
                },
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        if (columnIndex == 0) {
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to remove this room?', function (btn) {
                                if (btn == 'yes') {
                                    grid.store.remove(record);
                                }
                            });
                        }
                    },
                    addclick: function () {
                        me.edit(0);
                    },
                    itemdblclick: function (grid, record, item, rowIndex, e) {
                        me.edit(record.data);
                    }
                },
                autoHeight: true,
                idProperty: 'id',
                colModel: colModel,
                model: 'room'
            });
            this.callParent(arguments);
        },
        getData: function () {
            var results = [];
            var me = this;
            this.store.each(function (r) {
                if (!r.data.id) {
                    r.data.id = 0;
                }
                r.data.venueId = me.venueId;
                results.push(r.data);
            });
            return results;
        },
        edit: function (room) {
            var me = this;
            var win = new Ext.Window({
                items: [{
                    border: false,
                    xtype: 'classroom.roomform',
                    cls: 'x-panel-transparent',
                    venueId: me.venueId,
                    room: room,
                    listeners: {
                        save: function (item) {
                            if (!item.id) {
                                // give it an id so if they edit it, we can find it back
                                if (!me.roomCount) {
                                    me.roomCount = 0;
                                }
                                item.id = --me.roomCount;
                            }
                            // workaround for a bug in ExtJS loadData where it doesn't find the record properly
                            var record = me.store.getAt(me.store.find('id', item.id));
                            if (!record) {
                                me.store.loadData([item], true);
                            } else {
                                for (var prop in item) {
                                    if (item.hasOwnProperty(prop)) {
                                        record.set(prop, item[prop]);
                                    }
                                }
                                record.commit();
                            }
                            win.destroy();
                        },
                        cancel: function () {
                            win.destroy();
                        }
                    }
                }],
                layout: 'fit',
                modal: true,
                resizable: false,
                title: (room ? 'Edit' : 'Add') + ' a Room',
                height: 305,
                width: 400
            }).show();
        }
    });


    Symphony.Classroom.ClassInstructorSelectGrid = Ext.define('classroom.classinstructorselectgrid', {
        alias: 'widget.classroom.classinstructorselectgrid',
        extend: 'symphony.searchablegrid',
        classId: 0,
        initComponent: function () {
            var me = this;
            var url = '/services/classroom.svc/classes/instructors/select/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        /*id: 'fullName',*/ header: 'Name', dataIndex: 'fullName', width: 100, align: 'left',
                        flex: 1
                    },
                    { /*id: 'canHostVirtual',*/ header: 'Can Host Virtual', dataIndex: 'collaborator', width: 100, renderer: Symphony.checkRenderer }
                ]
            });

            Ext.apply(this, {
                listeners: {
                    rowclick: function (grid) {
                        me.fireEvent('select', grid.getSelectionModel().getSelected().data);
                    }
                },
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'classInstructor',
                method: 'POST'
            });
            this.callParent(arguments);

            this.store.on('beforeload', function (store, options) {
                // set the filter
                //store.proxy.method = "POST";
                var proxy = Ext.clone(store.getProxy());
                proxy.jsonData = this.filter || [];
                store.setProxy(proxy);
            }, this);
        }
    });


    Symphony.Classroom.ClassResourceSelectGrid = Ext.define('classroom.classresourceselectgrid', {
        alias: 'widget.classroom.classresourceselectgrid',
        extend: 'symphony.searchablegrid',
        venueId: 0,
        initComponent: function () {
            var me = this;
            var url = '/services/classroom.svc/classes/resources/select/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 100, align: 'left',
                        flex: 1
                    },
                    { /*id: 'cost',*/ header: 'Cost', dataIndex: 'cost', width: 100, align: 'left', type: 'float', renderer: Ext.util.Format.usMoney }
                ]
            });

            Ext.apply(this, {
                listeners: {
                    rowclick: function (grid) {
                        me.fireEvent('select', grid.getSelectionModel().getSelected().data);
                    }
                },
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'classResource',
                method: 'POST'
            });
            this.callParent(arguments);

            this.store.on('beforeload', function (store, options) {
                // set the filter
                var proxy = Ext.clone(store.getProxy());
                proxy.overrideUrl = url + this.venueId + '/';
                proxy.jsonData = this.filter || [];
                store.setProxy(proxy);
            }, this);
        }
    });


    Symphony.Classroom.RegistrantSelectGrid = Ext.define('classroom.registrantselectgrid', {
        alias: 'widget.classroom.registrantselectgrid',
        extend: 'symphony.searchablegrid',
        classId: 0,
        initComponent: function () {
            var me = this;
            var url = '/services/classroom.svc/classes/registrants/select/';


            var sm = new Ext.selection.CheckboxModel({
                checkOnly: true,
                sortable: true,
                header: ' ' // kills the select-all checkbox
            });

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [

                    {
                        /*id: 'fullName',*/ header: 'Name', dataIndex: 'fullName', width: 100, align: 'left',
                        flex: 1
                    }
                ]
            });

            this.selectionPaging = new Ext.ux.grid.RowSelectionPaging();
            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Save',
                        iconCls: 'x-button-save',
                        handler: function () {
                            me.fireEvent('save', me.selectionPaging.getSelectionsData());
                        }
                    }, {
                        text: 'Cancel',
                        iconCls: 'x-button-cancel',
                        handler: function () {
                            me.fireEvent('cancel');
                        }
                    }]
                },
                selModel: sm,
                pageSize: 15,
                plugins: [{ ptype: 'pagingselectpersist' }],
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'registration',
                method: 'POST'
            });
            this.callParent(arguments);

            this.store.on('beforeload', function (store, options) {
                // set the filter
                var proxy = Ext.clone(store.getProxy());
                proxy.jsonData = this.filter || [];
                store.setProxy(proxy);
            }, this);
        }
    });


    Symphony.Classroom.ClassScheduleManager = Ext.define('classroom.classschedulemanager', {
        alias: 'widget.classroom.classschedulemanager',
        extend: 'Ext.Panel',
        classId: 0,
        showChangeDuration: true,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 27,
                    xtype: 'toolbar',
                    items: [{
                        id: 'classroom.classschedulemanager.name',
                        style: 'font-weight: bold',
                        xtype: 'tbtext'
                    }, '->', '-', {
                        text: 'Room:',
                        style: 'font-weight: bold',
                        xtype: 'tbtext'
                    }, {
                        id: 'classroom.classschedulemanager.roomname',
                        xtype: 'tbtext'
                    }, '-', {
                        text: 'Timezone:',
                        style: 'font-weight: bold',
                        xtype: 'tbtext'
                    }, {
                        id: 'classroom.classschedulemanager.timezone',
                        xtype: 'tbtext'
                    }, '-', {
                        text: 'Day(s):',
                        style: 'font-weight: bold',
                        xtype: 'tbtext'
                    }, {
                        id: 'classroom.classschedulemanager.days',
                        xtype: 'tbtext'
                    }, '-', {
                        xtype: 'button',
                        text: 'Reschedule Dates',
                        iconCls: 'x-button-calendar-edit',
                        handler: function () {
                            me.fireEvent('rescheduleclick');
                        }
                    }]
                }, {
                    split: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    stateId: 'classroom.classschedulegrid',
                    xtype: 'classroom.classschedulegrid',
                    classId: me.classId,
                    klass: me.klass,
                    showHeader: false,
                    showContextMenu: true,
                    showChangeDuration: me.showChangeDuration,
                    listeners: {
                        reschedule: function (startDateTime) {
                            me.fireEvent('reschedule', startDateTime);
                        },
                        changeduration: function (duration) {
                            me.fireEvent('changeduration', duration);
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    stateId: 'classroom.trainingcalendarmonthminigrid',
                    xtype: 'classroom.trainingcalendarmonthminigrid',
                    classId: me.classId,
                    klass: me.klass,
                    listeners: {
                        reschedule: function (startDateTime) {
                            me.fireEvent('reschedule', startDateTime);
                        }
                    }
                }],
                listeners: {
                    rescheduleclick: function () {
                        me.showReScheduler();
                    },
                    changeduration: function (duration) {
                        for (var i = 0; i < me.klass.dateList.length; i++) {
                            me.klass.dateList[i].duration = duration;
                        }
                        me.refresh();
                    },
                    reschedule: function (startDateTime) {
                        me.reschedule(startDateTime);
                    }
                }
            });
            this.callParent(arguments);
        },
        onRender: function () {
            Symphony.Classroom.ClassScheduleManager.superclass.onRender.apply(this, arguments);
            this.load();
        },
        load: function () {
            // load up the details
            var me = this;
            window.setTimeout(function () {
                Ext.getCmp('classroom.classschedulemanager.name').setText(me.klass.name);
                Ext.getCmp('classroom.classschedulemanager.roomname').setText(me.klass.roomName);
                Ext.getCmp('classroom.classschedulemanager.timezone').setText(me.klass.timeZoneDescription);
                Ext.getCmp('classroom.classschedulemanager.days').setText(me.klass.numberOfDays);

                me.find('xtype', 'classroom.classschedulegrid')[0].setClass(me.klass);
                me.find('xtype', 'classroom.trainingcalendarmonthminigrid')[0].setClass(me.klass);
            }, 1);
        },
        refresh: function () {
            this.find('xtype', 'classroom.classschedulegrid')[0].setClass(this.klass);
            this.find('xtype', 'classroom.trainingcalendarmonthminigrid')[0].setClass(this.klass);

            this.fireEvent('schedulechanged', this.klass.dateList);
        },
        showReScheduler: function (title) {
            var me = this;
            var win = new Ext.Window({
                items: [{
                    xtype: 'classroom.rescheduledatesform',
                    classId: me.classId,
                    border: false,
                    listeners: {
                        reschedule: function (startDateTime) {
                            //me.fireEvent('reschedule', startDateTime);
                            me.reschedule(startDateTime);
                            win.destroy();
                        },
                        destroy: function () {
                            win.destroy();
                        }
                    }
                }],
                layout: 'fit',
                modal: true,
                resizable: false,
                title: title || 'Reschedule Dates',
                height: 185,
                width: 400
            }).show();
        },
        reschedule: function (startDateTime) {
            var me = this;
            if (Ext.isArray(startDateTime)) {
                me.klass.dateList = startDateTime;
            } else {
                var oldDateList = me.klass.dateList.slice(0);
                me.klass.dateList.splice(0, me.klass.dateList.length);
                var dateTime = startDateTime;
                var idCount = -1;
                if (me.klass.numberOfDays > 1) {
                    for (var i = 0; i < me.klass.numberOfDays; i++) {
                        var id = idCount--;
                        var duration = me.klass.dailyDuration;
                        if (oldDateList.length > i) {
                            if (oldDateList[i].id) { // try to keep the id if we can
                                id = oldDateList[i].id;
                            }
                            if (oldDateList[i].duration) { // try to keep the duration if we can
                                duration = oldDateList[i].duration;
                            }
                        }
                        me.klass.dateList.push({
                            id: id,
                            classId: me.klass.id || 0,
                            duration: duration,
                            startDateTime: dateTime.formatSymphony('microsoft')
                        });
                        dateTime = dateTime.add(Date.DAY, 1);
                    }
                } else {
                    me.klass.dateList.push({
                        id: id,
                        classId: me.klass.id || 0,
                        duration: me.klass.dailyDuration,
                        startDateTime: dateTime.formatSymphony('microsoft')
                    });
                }
            }
            me.refresh();
        }
    });


    Symphony.Classroom.RescheduleDatesForm = Ext.define('classroom.rescheduledatesform', {
        alias: 'widget.classroom.rescheduledatesform',
        extend: 'Ext.Panel',
        classId: 0,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 27,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        items: [],
                        listeners: {
                            save: function () {
                                var form = me.find('xtype', 'form')[0];

                                if (!form.isValid()) { return false; }

                                var classDate = form.getValues();

                                var hour = classDate.startTime.getHours();
                                var minute = classDate.startTime.getMinutes();
                                
                                // just the day portion
                                var day = Symphony.parseDate(classDate.startDate);

                                // set the hour portion
                                var startDateTime = day.add(Date.HOUR, hour).add(Date.MINUTE, minute);

                                me.fireEvent('reschedule', startDateTime);
                            },
                            cancel: Ext.bind(function () { this.ownerCt.remove(this); }, this)
                        }
                    }]
                }, {
                    region: 'center',
                    layout: 'fit',
                    defaults: {
                        border: false
                    },
                    items: [{
                        border: false,
                        bodyStyle: 'padding: 15px',
                        items: [{
                            name: 'general',
                            xtype: 'form',
                            border: false,
                            frame: true,
                            items: [{
                                xtype: 'fieldset',
                                title: 'Basic Information',
                                defaults: {
                                    border: false,
                                    xtype: 'panel',
                                    layout: 'form',
                                    cls: 'x-panel-transparent'
                                },
                                items: [{
                                    defaults: { anchor: '100%', xtype: 'textfield' },
                                    items: [{
                                        name: 'startDate',
                                        fieldLabel: 'Start Date',
                                        xtype: 'datefield',
                                        allowBlank: false
                                    }, {
                                        name: 'startTime',
                                        fieldLabel: 'Start Time',
                                        xtype: 'timefield',
                                        allowBlank: false
                                    }]
                                }]
                            }]
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        }
    });

    Symphony.Classroom.VenueResourceManagerSelectGrid = Ext.define('classroom.venueresourcemanagerselectgrid', {
        alias: 'widget.classroom.venueresourcemanagerselectgrid',
        extend: 'symphony.simplegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/classroom.svc/venues/resourcemanagers/select/';

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        /*id: 'fullName',*/ header: 'Name', dataIndex: 'fullName', width: 100, align: 'left',
                        flex: 1
                    }
                ]
            });

            Ext.apply(this.listeners, {
                itemclick: function (grid, record) {
                    var data = record.data;
                    me.fireEvent('managerselected', {
                        firstName: data.firstName,
                        lastName: data.lastName,
                        middleName: data.middleName,
                        fullName: data.fullName,
                        resourceManagerId: data.id,
                        venueId: data.venueId || 0
                    });
                }
            });
            Ext.apply(this, {
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'venueResourceManager',
                method: 'POST'
            });

            this.callParent(arguments);
            

            this.store.on('beforeload', function (store, options) {
                // set the filter
                var proxy = Ext.clone(store.getProxy());
                proxy.jsonData = this.filter || [];
                store.setProxy(proxy);
            }, this);
        }
    });


    Symphony.Classroom.ResourceForm = Ext.define('classroom.resourceform', {
        alias: 'widget.classroom.resourceform',
        extend: 'Ext.Panel',
        resourceId: 0,
        venueId: 0,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    //height: 27,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        items: [],
                        listeners: {
                            save: function () {
                                var frm = me.find('xtype', 'form')[0];
                                if (!frm.isValid()) { return false; }

                                var resource = frm.getValues();
                                
                                resource.id = me.resource ? me.resource.id : 0;
                                //me.resourceId = result.data.id;
                                //me.setTitle(result.data.name);
                                me.fireEvent('save', resource);

                            },
                            cancel: function () {
                                me.fireEvent('cancel');
                            }
                        }
                    }]
                }, {
                    region: 'center',
                    layout: 'fit',
                    defaults: {
                        border: false
                    },
                    items: [{
                        border: false,
                        bodyStyle: 'padding: 15px',
                        items: [{
                            name: 'general',
                            xtype: 'form',
                            border: false,
                            frame: true,
                            items: [{
                                xtype: 'fieldset',
                                title: 'Basic Information',
                                defaults: {
                                    border: false,
                                    xtype: 'panel',
                                    layout: 'form',
                                    cls: 'x-panel-transparent'
                                },
                                items: [{
                                    defaults: { anchor: '100%', xtype: 'textfield' },
                                    items: [{
                                        name: 'name',
                                        fieldLabel: 'Name',
                                        allowBlank: false
                                    }, {
                                        name: 'cost',
                                        fieldLabel: 'Cost',
                                        allowBlank: false,
                                        xtype: 'moneyfield'
                                    }]
                                }]
                            }]
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        onRender: function () {
            Symphony.Classroom.ResourceForm.superclass.onRender.apply(this, arguments);
            this.load();
        },
        load: function () {
            var me = this;
            if (this.resource) {
                window.setTimeout(function () {
                    me.find('name', 'general')[0].bindValues(me.resource);
                }, 1);
            } else if (this.resourceId) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/classroom.svc/resources/' + this.resourceId,
                    success: function (result) {
                        // load up the details
                        me.find('name', 'general')[0].bindValues(result.data);
                    }
                });
            }
        }
    });


    Symphony.Classroom.RoomForm = Ext.define('classroom.roomform', {
        alias: 'widget.classroom.roomform',
        extend: 'Ext.Panel',
        roomId: 0,
        venueId: 0,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 27,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        items: [],
                        listeners: {
                            save: function () {
                                var form = me.find('xtype', 'form')[0];

                                if (!form.isValid()) { return false; }

                                var room = form.getValues();

                                room.venueId = me.venueId;

                                if (room.roomCost == '') {
                                    room.roomCost = 0;
                                }
                                room.id = me.room ? me.room.id : 0;

                                if (!room.lockedIndicator) {
                                    room.lockedIndicator = false;
                                }
                                if (!room.networkAccessIndicator) {
                                    room.networkAccessIndicator = false;
                                }

                                me.fireEvent('save', room);
                            },
                            cancel: function () { me.fireEvent('cancel'); }
                        }
                    }]
                }, {
                    region: 'center',
                    layout: 'fit',
                    defaults: {
                        border: false
                    },
                    items: [{
                        border: false,
                        bodyStyle: 'padding: 15px',
                        items: [{
                            name: 'general',
                            xtype: 'form',
                            border: false,
                            frame: true,
                            items: [{
                                xtype: 'fieldset',
                                title: 'Details',
                                defaults: {
                                    border: false,
                                    xtype: 'panel',
                                    layout: 'form',
                                    cls: 'x-panel-transparent'
                                },
                                items: [{
                                    defaults: { anchor: '100%', xtype: 'textfield' },
                                    items: [{
                                        name: 'name',
                                        fieldLabel: 'Name',
                                        allowBlank: false
                                    }, {
                                        name: 'internalCode',
                                        fieldLabel: 'Internal Code',
                                        allowBlank: true
                                    }, {
                                        name: 'roomCost',
                                        fieldLabel: 'Room Cost',
                                        allowBlank: true,
                                        xtype: 'moneyfield'
                                    }, {
                                        name: 'capacity',
                                        fieldLabel: 'Capacity',
                                        allowBlank: false,
                                        xtype: 'numberfield'
                                    }, {
                                        name: 'networkAccessIndicator',
                                        fieldLabel: 'Network Access',
                                        xtype: 'checkbox'
                                    }, {
                                        name: 'lockedIndicator',
                                        fieldLabel: 'Locked',
                                        xtype: 'checkbox'
                                    }]
                                }]
                            }]
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        onRender: function () {
            Symphony.Classroom.RoomForm.superclass.onRender.apply(this, arguments);
            this.load();
        },
        load: function () {
            if (this.room) {
                var me = this;
                window.setTimeout(function () {
                    me.find('name', 'general')[0].bindValues(me.room);
                });
            }
        }
    });


    Symphony.Classroom.TrainingCalendarMonthMiniGrid = Ext.define('classroom.trainingcalendarmonthminigrid', {
        alias: 'widget.classroom.trainingcalendarmonthminigrid',
        extend: 'symphony.localgrid',
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        id: '',
        classId: 0,
        initComponent: function () {
            var me = this;

            var dateRenderer = function (value, metadata, record) {
                if (!value) { return ''; }
                var style = ['height: 40px'];
                var klassDivs = [];
                if (value.classes) {
                    for (var i = 0; i < value.classes.length; i++) {
                        var klass = value.classes[i];
                        var id = klass.classId + '-' + klass.id;
                        klassDivs.push('<div id="classroom-trainingcalendar-{1}" class="classroom-trainingcalendar-dd-class" style="border: solid 1px #fff;" onmouseover="this.style.border=\'solid 1px #ccc\'" onmouseout="this.style.border=\'solid 1px #fff\'">{0}</div>'.format(Symphony.shortTimeRenderer(klass.startDateTime, true), id));
                    }
                }

                var id = value.year + '-' + value.month + '-' + value.day;

                var numStyle = 'font-weight: bold;';
                var now = new Date();
                if (now.getFullYear() == value.year
                    && now.getMonth() + 1 == value.month
                    && now.getDate() == value.day) {
                    numStyle += ' color: maroon;';
                }

                return '<div id="{3}" style="{0}"><span style="{4}">{1}</span><div>{2}</div></div>'.format(style.join('; '), value.day, klassDivs.join(''), id, numStyle);
            };

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    align: 'left',
                    editable: false,
                    sortable: false,
                    renderer: dateRenderer
                },
                columns: [
                    { /*id: 'sunday',*/ header: 'Sun', dataIndex: 'sunday' },
                    { /*id: 'monday',*/ header: 'Mon', dataIndex: 'monday' },
                    { /*id: 'tuesday',*/ header: 'Tue', dataIndex: 'tuesday' },
                    { /*id: 'wednesday',*/ header: 'Wed', dataIndex: 'wednesday' },
                    { /*id: 'thursday',*/ header: 'Thu', dataIndex: 'thursday' },
                    { /*id: 'friday',*/ header: 'Fri', dataIndex: 'friday' },
                    { /*id: 'saturday',*/ header: 'Sat', dataIndex: 'saturday' }
                ]
            });

            var tbarItems = [{
                iconCls: 'x-button-previous',
                handler: function () {
                    me.fireEvent('previousclick');
                }
            }, {
                xtype: 'label',
                style: 'display: block; width: 100px; text-align: center'
            }, {
                iconCls: 'x-button-next',
                handler: function () {
                    me.fireEvent('nextclick');
                }
            }, {
                text: 'Today',
                handler: function () {
                    me.fireEvent('todayclick');
                }
            }];

            Ext.apply(this, {
                tbar: {
                    items: tbarItems
                },
                disableSelection: true,
                enableDragDrop: true,
                ddGroup: 'classroom-trainingcalendar-dd' + me.id,
                ddText: 'Select a new starting date for this class.',
                enableColumnHide: false,
                enableColumnMove: false,
                enableColumnResize: false,
                enableHdMenu: false,
                columnLines: true,
                trackMouseOver: false,
                idProperty: 'id',
                colModel: colModel,
                model: 'DaysOfTheWeekModel',
                listeners: {
                    previousclick: function () {
                        this.month--;
                        if (this.month == 0) {
                            this.month = 12;
                            this.year--;
                        }
                        this.update();
                    },
                    nextclick: function () {
                        this.month++;
                        if (this.month == 13) {
                            this.month = 1;
                            this.year++;
                        }
                        this.update();
                    },
                    todayclick: function () {
                        this.month = new Date().getMonth() + 1;
                        this.year = new Date().getFullYear();
                        this.update();
                    },
                    contextmenu: function (e) {
                        var target = e.getTarget();
                        if (target.className == 'classroom-trainingcalendar-dd-class') {
                            if (!this.contextmenu) {
                                this.contextmenu = new Ext.menu.Menu({
                                    stateId: 'classroom.trainingcalendarmonthgrid.contextmenu',
                                    items: [{
                                        text: 'Move to Date',
                                        iconCls: 'x-menu-date-move',
                                        menu: new Ext.menu.DateMenu({
                                            handler: function (datePicker, d) {
                                                var oldDate = me.contextmenu.target.parentNode.parentNode.id.split('-');
                                                me.setClassDate(new Date(oldDate[0], oldDate[1] - 1, oldDate[2], 0, 0, 0, 0), d);
                                            }
                                        })
                                    }]
                                });
                            }
                            e.stopEvent();
                            this.contextmenu.target = target;
                            this.contextmenu.showAt(e.getXY());
                        }
                    },
                    afterRender: function () {
                        //debugger
                        this.view.dragZone = new Ext.dd.DragZone(this.view.getEl(), {

                            //      On receipt of a mousedown event, see if it is within a DataView node.
                            //      Return a drag data object if so.
                            getDragData: function (e) {
                                var target = e.getTarget();
                                //debugger
                                if (target.className == 'classroom-trainingcalendar-dd-class') {
                                    var id = target.id.substring('classroom-trainingcalendar-'.length).split('-');
                                    //this.grid.ddText = target.innerHTML;
                                    d = target.cloneNode(true);
                                    return {
                                        ddel: d,
                                        sourceEl: target,
                                        repairXY: Ext.fly(target).getXY(),
                                        sourceStore: null,
                                        draggedRecord: null //v.getRecord(target)
                                    };
                                }
                            },

                            //      Provide coordinates for the proxy to slide back to on failed drag.
                            //      This is the original XY coordinates of the draggable element captured
                            //      in the getDragData method.
                            getRepairXY: function () {
                                return this.dragData.repairXY;
                            }
                        });
                        /*this.view.dragZone.getDragData = function (e) {
                            var target = e.getTarget();
                            if (target.className == 'classroom-trainingcalendar-dd-class') {
                                var id = target.id.substring('classroom-trainingcalendar-'.length).split('-');
                                this.grid.ddText = target.innerHTML;
                                return {
                                    proxy: this,
                                    div: target,
                                    classId: id[0] * 1,
                                    id: id[1] * 1
                                };
                            }
                        };*/

                        this.view.dropZone = new Ext.dd.DropZone(this.view.getEl(), {
                            getTargetFromEvent: function (e) {
                                return e.getTarget(me.getView().cellSelector);
                            },
                            onNodeEnter: function (target, dd, e, data) {
                                Ext.fly(target).addCls('calendar-row-highlight');
                            },

                            // On exit from a target node, unhighlight that node.
                            onNodeOut: function (target, dd, e, data) {
                                Ext.fly(target).removeCls('calendar-row-highlight');
                            },
                            //ddGroup: 'classroom-trainingcalendar-dd' + me.id,
                            onNodeDrop: function (target, dd, e, data) {
                                //var target = e.getTarget(data.proxy.view.cellSelector);
                                var newDate = target.firstChild.firstChild.id.split('-');
                                var oldDate = dd.dragData.sourceEl.parentNode.parentNode.id.split('-');
                                me.setClassDate(new Date(oldDate[0], oldDate[1] - 1, oldDate[2], 0, 0, 0, 0), new Date(newDate[0], newDate[1] - 1, newDate[2], 0, 0, 0, 0));
                            }
                        });
                    },
                    schedulechanged: function () {
                        me.refresh();
                    }
                },
                loadMask: false,
                viewConfig: {
                    forceFit: true
                }
            });
            this.callParent(arguments);
        },
        setClass: function (klass) {
            this.klass = klass;
            this.update();
        },
        setClassDate: function (oldDate, newDate) {
            // find the matching date in the schedule and change it
            // for matchin, we only care about the day/month/year, the time isn't changing
            var newDateString = oldDate.formatSymphony('m/d/Y');
            for (var i = 0; i < this.klass.dateList.length; i++) {
                var dt = Symphony.parseDate(this.klass.dateList[i].startDateTime);
                if (newDateString == dt.formatSymphony('m/d/Y')) {
                    // set the new date's hours
                    newDate = newDate.add(Date.HOUR, dt.getHours()).add(Date.MINUTE, dt.getMinutes());
                    this.klass.dateList[i].startDateTime = newDate.formatSymphony('microsoft');
                    break;
                }
            }
            var dateListSort = function (a, b) {
                return a.startDateTime - b.startDateTime;
            };
            this.klass.dateList = this.klass.dateList.sort(dateListSort);
            this.fireEvent('reschedule', this.klass.dateList);
        },
        update: function () {
            var month = this.getDockedItems('toolbar[dock="top"]')[0].items.items[1];
            var date = new Date(this.year, this.month, 0, 0, 0, 0, 0);
            month.setText(date.formatSymphony('F Y'));

            var weeks = Symphony.Calendar.weeks(this.year, this.month);
            if (this.klass) {
                for (var i = 0; i < this.klass.dateList.length; i++) {
                    var classDate = this.klass.dateList[i];
                    var startDateTime = Symphony.parseDate(classDate.startDateTime);
                    if ((startDateTime.getMonth() + 1) == this.month
                        && startDateTime.getFullYear() == this.year) {
                        for (var j = 0; j < weeks.length; j++) {
                            var week = weeks[j];
                            for (var day in week) {
                                if (week.hasOwnProperty(day)) {
                                    if (week[day].day == startDateTime.getDate()) {
                                        week[day].classes = [classDate];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.setData(weeks);
        }
    });


    Symphony.Classroom.TrainingCalendarMonthGrid = Ext.define('classroom.trainingcalendarmonthgrid', {
        alias: 'widget.classroom.trainingcalendarmonthgrid',
        extend: 'symphony.unpagedgrid',
        filters: ['m', 'm-ur', 'm-uwl', 'm-uaa', 'm-ud', 'm-ac', 'rm', 'rm-rr', 'rm-nrr', 'i', 's', 's-ur', 's-uwl', 's-uaa', 's-ud'],
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        id: '',
        classId: 0,
        showFilters: true,
        enableDragDrop: true,
        initComponent: function () {
            var me = this;
            this.updateUrl();

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    align: 'left',
                    editable: false,
                    sortable: false
                },
                columns: [
                    { /*id: 'sunday',*/ header: 'Sunday', dataIndex: 'sunday', renderer: Ext.bind(this.cellRenderer, this) },
                    { /*id: 'monday',*/ header: 'Monday', dataIndex: 'monday', renderer: Ext.bind(this.cellRenderer, this) },
                    { /*id: 'tuesday',*/ header: 'Tuesday', dataIndex: 'tuesday', renderer: Ext.bind(this.cellRenderer, this) },
                    { /*id: 'wednesday',*/ header: 'Wednesday', dataIndex: 'wednesday', renderer: Ext.bind(this.cellRenderer, this) },
                    { /*id: 'thursday',*/ header: 'Thursday', dataIndex: 'thursday', renderer: Ext.bind(this.cellRenderer, this) },
                    { /*id: 'friday',*/ header: 'Friday', dataIndex: 'friday', renderer: Ext.bind(this.cellRenderer, this) },
                    { /*id: 'saturday',*/ header: 'Saturday', dataIndex: 'saturday', renderer: Ext.bind(this.cellRenderer, this) }
                ]
            });

            var tbarItems = [{
                iconCls: 'x-button-previous',
                handler: function () {
                    me.fireEvent('previousclick');
                }
            }, {
                xtype: 'label',
                style: 'display: block; width: 100px; text-align: center'
            }, {
                iconCls: 'x-button-next',
                handler: function () {
                    me.fireEvent('nextclick');
                }
            }, {
                iconCls: 'x-button-refresh',
                handler: function () {
                    me.fireEvent('refreshclick');
                }
            }, {
                text: 'Today',
                handler: function () {
                    me.fireEvent('todayclick');
                }
            }];
            if (this.tbarItems) {
                tbarItems = tbarItems.concat(this.tbarItems);
            }
            tbarItems.push('->');

            if (this.showFilters) {
                tbarItems.push({
                    text: 'Filters',
                    iconCls: 'x-button-user',
                    handler: function () {
                        me.fireEvent('filtersclick');
                    }
                });
            }

            Ext.applyIf(this, {
                tbar: {
                    items: tbarItems,
                    itemId: 'tbar'
                },
                disableSelection: true,
                enableDragDrop: true,
                ddGroup: 'classroom-trainingcalendar-dd' + me.id,
                ddText: 'Select a new starting date for this class.',
                enableColumnHide: false,
                enableColumnMove: false,
                enableColumnResize: false,
                enableHdMenu: false,
                columnLines: true,
                trackMouseOver: false,
                idProperty: 'id',
                colModel: colModel,
                model: 'DaysOfTheWeekModel',
                viewConfig: {
                    plugins: 'celldragdrop'
                },
                listeners: {
                    previousclick: function () {
                        this.month--;
                        if (this.month == 0) {
                            this.month = 12;
                            this.year--;
                        }
                        this.updateMonth();
                        this.updateUrl();
                    },
                    nextclick: function () {
                        this.month++;
                        if (this.month == 13) {
                            this.month = 1;
                            this.year++;
                        }
                        this.updateMonth();
                        this.updateUrl();
                    },
                    refreshclick: function () {
                        me.refresh();
                    },
                    todayclick: function () {
                        this.month = new Date().getMonth() + 1;
                        this.year = new Date().getFullYear();
                        this.updateMonth();
                        this.updateUrl();
                    },
                    filtersclick: function () {
                        var win = new Ext.Window({
                            items: [{
                                xtype: 'classroom.trainingcalendarmonthfilterstree',
                                border: false,
                                filters: me.filters,
                                listeners: {
                                    applyclick: function () {
                                        me.filters = this.filters;
                                        me.updateUrl();
                                        me.refresh();
                                        win.destroy();
                                    },
                                    destroy: function () {
                                        win.destroy();
                                    }
                                }
                            }],
                            layout: 'fit',
                            modal: true,
                            resizable: false,
                            title: 'Filters',
                            height: 350,
                            width: 250
                        }).show();
                    },
                    contextmenu: function (e) {
                        if (!Symphony.User.isClassroomManager || !me.enableDragDrop) { return; }
                        var target = e.getTarget();
                        if (target.className == 'classroom-trainingcalendar-dd-class') {
                            if (!this.contextmenu) {
                                this.contextmenu = new Ext.menu.Menu({
                                    stateId: 'classroom.trainingcalendarmonthgrid.contextmenu',
                                    items: [{
                                        text: 'Move to Date',
                                        iconCls: 'x-menu-date-move',
                                        menu: new Ext.menu.DateMenu({
                                            handler: function (datePicker, d) {
                                                var id = me.contextmenu.target.id.substring('classroom-trainingcalendar-'.length).split('-');

                                                var time = me.contextmenu.target.childNodes[0].data.split(' ')[0].split(':');
                                                var hour = time[0] * 1;
                                                var minute = time[1].substring(0, 2) * 1;
                                                var merid = time[1].substring(2, 4).toLowerCase();
                                                if (merid == 'am') {
                                                    if (hour == 12) {
                                                        hour = 0;
                                                    }
                                                } else if (merid == 'pm') {
                                                    if (hour != 12) {
                                                        hour += 12;
                                                    }
                                                }
                                                d = d.add(Date.HOUR, hour);
                                                d = d.add(Date.MINUTE, minute);
                                                me.setClassDate(id[0] * 1, id[1] * 1, d);
                                            }
                                        })
                                    }, {
                                        text: 'Class Schedule',
                                        iconCls: 'x-menu-calendar',
                                        handler: function () {
                                            var id = me.contextmenu.target.id.substring('classroom-trainingcalendar-'.length).split('-');

                                            Symphony.Ajax.request({
                                                method: 'GET',
                                                url: '/services/classroom.svc/classes/' + id[0] * 1,
                                                success: function (result) {
                                                    var klass = result.data;
                                                    var win = new Ext.Window({
                                                        items: [{
                                                            xtype: 'classroom.classschedulemanager',
                                                            klass: klass,
                                                            showChangeDuration: true,
                                                            listeners: {
                                                                schedulechanged: function (dateList) {
                                                                    var date = Symphony.parseDateToUTC(dateList[0].startDateTime);
                                                                    me.setClassDate(this.klass.id, this.klass.dateList[0].id, date);
                                                                }
                                                            }
                                                        }],
                                                        layout: 'fit',
                                                        modal: true,
                                                        resizable: false,
                                                        title: 'Class Schedule',
                                                        height: 360,
                                                        width: 800
                                                    }).show();
                                                }
                                            });
                                        }
                                    }, {
                                        text: 'View Class',
                                        iconCls: 'x-menu-view-class',
                                        handler: function () {
                                            var clsParts = me.contextmenu.target.id.substring('classroom-trainingcalendar-'.length).split('-'),
                                                id = parseInt(clsParts[0], 10);

                                            Symphony.App.selectTab('Classroom/classes/' + id);

                                        }
                                    }]
                                });
                            }
                            e.stopEvent();
                            this.contextmenu.target = target;
                            this.contextmenu.showAt(e.getXY());
                        }
                    },
                    afterRender: function (grid) {
                        if (!this.enableDragDrop) {
                            return;
                        }
                        
                        this.view.dragZone = new Ext.dd.DragZone(this.view.getEl(), {
                            ddGroup: 'classroom-trainingcalendar-dd' + me.id,
                            getDragData: function (e) {
                                var target = e.getTarget();
                                
                                if (target.className == 'classroom-trainingcalendar-dd-class') {
                                    var id = target.id.substring('classroom-trainingcalendar-'.length).split('-');
                                    var d = target.cloneNode(true);
                                    d.id = Ext.id();

                                    grid.ddText = target.innerHTML;

                                    return {
                                        ddel: d,
                                        sourceEl: target,
                                        repairXY: Ext.fly(target).getXY(),
                                        sourceStore: grid.store,
                                        proxy: this,
                                        div: target,
                                        classId: id[0] * 1,
                                        classDateId: id[1] * 1
                                    };
                                }
                            },
                            getRepairXY: function() {
                                return this.dragData.repairXY;
                            }
                        });

                        new Ext.dd.DropTarget(this.view.getEl(), {
                            ddGroup: 'classroom-trainingcalendar-dd' + me.id,
                            notifyDrop: function (dd, e, data) {
                                var target = e.getTarget(grid.view.getCellSelector());
                                var date = target.firstChild.firstChild.id.split('-');
                                var d = new Date(date[0], date[1] - 1, date[2], 0, 0, 0, 0);
                                var time = dd.dragData.div.childNodes[0].data.split(' ')[0].split(':');
                                var hour = time[0] * 1;
                                var minute = time[1].substring(0, 2) * 1;
                                var merid = time[1].substring(2, 4).toLowerCase();
                                if (merid == 'am') {
                                    if (hour == 12) {
                                        hour = 0;
                                    }
                                } else if (merid == 'pm') {
                                    if (hour != 12) {
                                        hour += 12;
                                    }
                                }
                                d = d.add(Date.HOUR, hour);
                                d = d.add(Date.MINUTE, minute);
                                me.setClassDate(data.classId, data.classDateId, d);
                            }
                        });
                    },
                    schedulechanged: function () {
                        me.refresh();
                    }
                },
                loadMask: false,
                viewConfig: {
                    forceFit: true
                }
            });
            this.callParent(arguments);

            this.updateMonth();
        },
        cellRenderer: function (value, metadata, record) {
            if (!value) { return ''; }
            var style = ['height: 80px'];
            if (!value.inCurrentMonth) {
                style.push('color: #999');
            }

            var content = this.renderDate(value);
            var id = value.year + '-' + value.month + '-' + value.day;

            var numStyle = 'font-weight: bold;';
            var now = new Date();
            if (now.getFullYear() == value.year
                && now.getMonth() + 1 == value.month
                && now.getDate() == value.day) {
                numStyle += ' color: maroon;';
            }

            return '<div id="{3}" style="{0}"><span style="{4}">{1}</span><div>{2}</div></div>'.format(style.join('; '), value.day, content, id, numStyle);
        },
        renderDate: function (value) {
            var divs = [];
            if (value.classes) {
                for (var i = 0; i < value.classes.length; i++) {
                    var klass = value.classes[i];
                    divs.push(this.renderClass(klass));
                }
            }
            return divs.join('');
        },
        renderClass: function (klass) {
            var id = klass.classId + '-' + klass.classDateId;
            var classStyle = (klass.classId == this.classId ? 'color: blue; ' : '');
            var qtip = '&lt;span style=\'font-weight:bold\'&gt;' + klass.name + '&lt;/span&gt;';
            if (klass.managerUsersRegistered) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/group_go.png\' /&gt; CT Manager - Users Registered';
            }
            if (klass.managerUsersWaitListed) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/group_link.png\' /&gt; CT Manager - Users on Wait List';
            }
            if (klass.managerUsersAwaitingApproval) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/group_edit.png\' /&gt; CT Manager - Users Awaiting Approval';
            }
            if (klass.managerUsersDenied) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/group_delete.png\' /&gt; CT Manager - Users Denied';
            }
            if (klass.managerAllClasses) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/group.png\' /&gt; CT Manager - All Classes';
            }
            if (klass.resourceManagerRequiredResources) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/cog.png\' /&gt; Resource Manager - Required Resources';
            }
            if (klass.resourceManagerNoRequiredResources) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/cog_delete.png\' /&gt; Resource Manager - No Required Resources';
            }
            if (klass.instructors) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/book_edit.png\' /&gt; Instructor';
            }
            if (klass.supervisorUsersRegistered) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/world_go.png\' /&gt; Supervisor - Users Registered';
            }
            if (klass.supervisorUsersWaitListed) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/world_link.png\' /&gt; Supervisor - Users on Wait List>';
            }
            if (klass.supervisorUsersAwaitingApproval) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/world_edit.png\' /&gt; Supervisor - Users Awaiting Approval';
            }
            if (klass.supervisorUsersDenied) {
                qtip += '&lt;br /&gt;&lt;img src=\'/images/world_delete.png\' /&gt; Supervisor - Users Denied';
            }
            return this.renderEventContent(Symphony.shortTimeRenderer(klass.startDateTime, true), klass.name, qtip, id, classStyle);
        },
        renderEventContent: function (date, name, qtip, id, style) {
            return '<div id="classroom-trainingcalendar-{3}" class="classroom-trainingcalendar-dd-class" style="{4}border: solid 1px #fff; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; o-text-overflow: ellipsis; -moz-binding: url(\'/xml/ellipsis.xml#ellipsis\');" ext:qtip="{2}" onmouseover="this.style.border=\'solid 1px #ccc\'" onmouseout="this.style.border=\'solid 1px #fff\'">{0} {1}</div>'.format(date, name, qtip, id, style);
        },
        setClassDate: function (classId, classDateId, date) {
            var me = this;
            me.month = date.getMonth() + 1;
            me.year = date.getFullYear();
            Symphony.Ajax.request({
                url: '/services/classroom.svc/calendar/',
                jsonData: {
                    classId: classId,
                    classDateId: classDateId,
                    startDateTime: date.formatSymphony('microsoft')
                },
                success: function (result) {
                    me.updateMonth();
                    me.updateUrl();
                }
            });
        },
        updateMonth: function () {
            var toolbar = this.getDockedComponent('tbar');
            var month = toolbar.items.items[1];
            var date = new Date(this.year, this.month, 0, 0, 0, 0, 0);
            month.setText(date.formatSymphony('F Y'));
        },
        updateUrl: function () {
            var url = '/services/classroom.svc/calendar/' + this.year + '/' + this.month + '/' + (this.filters.join('_') || '_') + '/';
            this.url = url;
            if (this.updateStore) {
                this.updateStore(url);
            }
        }
    });


    Symphony.Classroom.TrainingCalendarListGrid = Ext.define('classroom.trainingcalendarlistgrid', {
        alias: 'widget.classroom.trainingcalendarlistgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/classroom.svc/classes/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        /*id: 'name',*/ header: 'Class Name', dataIndex: 'name', width: 140, align: 'left',
                        flex: 1
                    },
                    { /*id: 'roomName',*/ header: 'Room Name', dataIndex: 'roomName', width: 200, align: 'left' },
                    { /*id: 'minClassDate',*/ header: 'Start Date/Time', dataIndex: 'minClassDate', width: 250, align: 'left', renderer: Symphony.longDateRenderer },
                    { /*id: 'classInstructors',*/ header: 'Instructors', dataIndex: 'classInstructors', width: 300, align: 'left' }
                ]
            });

            Ext.apply(this, {
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'classroomClass'
            });
            this.callParent(arguments);
        }
    });


    Symphony.Classroom.TrainingCalendarMonthFiltersTree = Ext.define('classroom.trainingcalendarmonthfilterstree', {
        alias: 'widget.classroom.trainingcalendarmonthfilterstree',
        extend: 'Ext.tree.Panel',
        initComponent: function () {
            var me = this;

            var root = {
                expanded: true,
                text: "",
                children: [
                    Ext.apply(me.getNodeConfig('m', 'CT Manager', !Symphony.User.isClassroomManager), {
                        children: [
                            me.getNodeConfig('m-ur', 'Users Registered', !Symphony.User.isClassroomManager),
                            me.getNodeConfig('m-uwl', 'Users on Wait List', !Symphony.User.isClassroomManager),
                            me.getNodeConfig('m-uaa', 'Users Awaiting Approval', !Symphony.User.isClassroomManager),
                            me.getNodeConfig('m-ud', 'Users Denied', !Symphony.User.isClassroomManager),
                            me.getNodeConfig('m-ac', 'All Classes', !Symphony.User.isClassroomManager)
                        ],
                        leaf: false,
                        expanded: true
                    }),
                    Ext.apply(me.getNodeConfig('rm', 'Resource Manager', !Symphony.User.isClassroomResourceManager), {
                        children: [
                            me.getNodeConfig('rm-rr', 'Resource(s) Required', !Symphony.User.isClassroomResourceManager),
                            me.getNodeConfig('rm-nrr', 'No Resource(s) Required', !Symphony.User.isClassroomResourceManager)
                        ],
                        leaf: false,
                        expanded: true
                    }),
                    Ext.apply(me.getNodeConfig('i', 'Instructor', !Symphony.User.isClassroomInstructor), {
                        children: [

                        ],
                        leaf: false,
                        expanded: true
                    }),
                    Ext.apply(me.getNodeConfig('s', 'Supervisor', !Symphony.User.isClassroomSupervisor), {
                        children: [
                            me.getNodeConfig('s-ur', 'Users Registered', !Symphony.User.isClassroomSupervisor),
                            me.getNodeConfig('s-uwl', 'Users on Wait List', !Symphony.User.isClassroomSupervisor),
                            me.getNodeConfig('s-uaa', 'Users Awaiting Approval', !Symphony.User.isClassroomSupervisor),
                            me.getNodeConfig('s-ud', 'Users Denied', !Symphony.User.isClassroomSupervisor)
                        ],
                        leaf: false,
                        expanded: true
                    })
                ]
            };

            var store = Ext.create('Ext.data.TreeStore', {
                model: 'scheduledClassFilter',
                proxy: {
                    type: 'memory'
                },
                root: root
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Apply',
                        handler: function () {
                            var checked = me.view.getChecked();
                            me.filters = [];
                            for (var i = 0; i < checked.length; i++) {
                                me.filters.push(checked[i].get('code'));
                            }
                            me.fireEvent('applyclick');
                        }
                    }]
                },
                lines: false,
                rootVisible: false,
                store: store
            });

            Ext.apply(this.listeners, {
                checkchange: function (node, checked, eOpts) {
                    if (node.hasChildNodes()) {
                        node.cascadeBy(function (node) {
                            node.set('checked', checked);
                        });
                    }
                }
            });
               
            this.callParent(arguments);

        },
        getNodeConfig: function(id, text, disabled) {
            return {
                allowDrag: false,
                expanded: true,
                disabled: disabled,
                text: text,
                code: id,
                leaf: true,
                iconCls: (id ? 'x-treenode-' + id : null),
                checked: this.filters.indexOf(id) >= 0
            }
        }
    });


    Symphony.Classroom.CourseDocumentsGrid = Ext.define('classroom.coursedocumentsgrid', {
        alias: 'widget.classroom.coursedocumentsgrid',
        extend: 'symphony.editableunpagedgrid',
        courseId: 0,
        initComponent: function () {
            var url = '/services/classroom.svc/coursefiles/';
            var me = this;
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        /*id: 'name',*/
                        header: 'File Name',
                        dataIndex: 'filename',
                        renderer: function (value, meta, record) {
                            return '<a href="#" onclick="Symphony.Classroom.downloadCourseFile({0})">{1}</a>'.format(record.get('id'), value);
                        }
                    },
                    { /*id: 'title',*/ header: 'Title', dataIndex: 'title', editor: new Ext.form.TextField({ allowBlank: false }) },
                    {
                        /*id: 'description',*/ header: 'Description', dataIndex: 'description', editor: new Ext.form.TextField({ allowBlank: false }),
                        flex: 1
                    },
                    {
                        /*id: 'uploaded',*/
                        header: 'Upload Date',
                        dataIndex: 'id',
                        width: 75,
                        align: 'center',
                        renderer: function (value, meta, record) {
                            return value ? Symphony.dateRenderer(record.get('createdOn')) : '-';
                        }
                    },
                    {
                        header: 'Upload Progress',
                        renderer: function (value, meta, record) {
                            if (record.get('id') > 0) {
                                return '<div style="text-align:center;width:100%;height:100%;">-</div>';
                            } else {
                                var percent = record.get('percentUploaded') * 100;
                                return '<div style="width:100%;height:100%;"><div style="height:100%;width:' + percent + '%;background-color:#D2E0F1;">&nbsp;</div></div>';
                            }
                        }
                    }
                ]
            });

            Ext.apply(this, {
                idProperty: 'id',
                deferLoad: true,
                baseUrl: url,
                url: url + (this.courseId || 0),
                colModel: colModel,
                model: 'courseFile',
                clicksToEdit: 1,
                listeners: {
                    'beforeedit': function (editor, e, eOpts) {
                        // at least for now, you can't edit a file after it's uploaded
                        if (e.record.get('id') > 0) {
                            e.cancel = true;
                        }
                    }
                },
                tbar: {
                    items: [{
                        xtype: 'swfuploadbutton',
                        uploadUrl: '/Uploaders/CourseFileUploader.ashx',
                        name: 'uploader',
                        text: 'Add File',
                        iconCls: 'x-button-upload',
                        listeners: {
                            'queue': function (file) {
                                var store = me.store;
                                var index = store.getCount();
                                store.add({
                                    filename: file.name,
                                    title: file.name,
                                    description: file.name,
                                    swfFileId: file.id,
                                    percentUploaded: 0
                                });

                                me.startEditing(index, 2);
                            },
                            'uploadstart': function (item) {
                                var record = me.store.getAt(me.store.find('swfFileId', item.id));
                                this.addFileParameter(item.id, 'title', record.get('title'));
                                this.addFileParameter(item.id, 'description', record.get('description'));
                            },
                            'uploadprogress': function (item, completed, total) {
                                var record = me.store.getAt(me.store.find('swfFileId', item.id));
                                record.set('percentUploaded', parseInt(completed / total, 10));
                                record.commit();
                            },
                            'allcomplete': function () {
                                var lastOptions = me.store.lastOptions;
                                me.refresh();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Remove File',
                        iconCls: 'x-button-upload-delete',
                        handler: function () {
                            var record = me.getSelectionModel().getSelection()[0];
                            if (!record) { return; }

                            var file = record.data;
                            // two options here:
                            // 1) if it's just queued, remove it immediately
                            // 2) if it's already uploaded, prompt and delete
                            if (file.swfFileId) {
                                var swf = this.ownerCt.find('xtype', 'swfuploadbutton')[0];
                                swf.removeFile(file.swfFileId);
                                me.store.remove(record);
                            } else {
                                Ext.Msg.confirm('Are you sure?', 'You are about to delete the selected file. This action cannot be undone. Are you sure?', function (btn) {
                                    if (btn == 'yes') {
                                        Symphony.Ajax.request({
                                            url: '/services/classroom.svc/coursefiles/delete/' + (file.id || 0),
                                            success: function (result) {
                                                me.store.remove(record);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }, '->',
                      '<span style="font-style:italic">Files will be uploaded when you save the Course; click the title or description to edit.</span>'
                    ]
                }
            });

            this.callParent(arguments);

            // make sure the url is always set correctly
            this.store.on('beforeload', function (store, options) {
                var proxy = Ext.clone(store.getProxy());
                proxy.api.read = me.baseUrl + (me.courseId || 0);
                proxy.jsonData = me.filter || [];
                store.setProxy(proxy);
            }, this);
        },
        upload: function (config) {
            var swf = this.getTopToolbar().find('name', 'uploader')[0];

            if (swf && swf.rendered) {
                swf.addParameter('courseId', config.courseId);
                swf.addParameter('customerId', Symphony.User.customerId);
                swf.on('allcomplete', config.onComplete);
                swf.uploadAll();
            } else {
                config.onComplete();
            }
        },
        setCourseId: function (courseId) {
            this.courseId = courseId;
            this.refresh();
        }
    });



    Symphony.Classroom.downloadCourseFile = function (fileId) {
        window.open("/services/classroom.svc/coursefiles/download/" + fileId);
    };

    Symphony.Classroom.QuickAddPanel = Ext.define('classroom.quickaddpanel', {
        alias: 'widget.classroom.quickaddpanel',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Save',
                        iconCls: 'x-button-save',
                        handler: function () {
                            var values = me.getValues();
                            if (values) {
                                me.fireEvent('save', values);
                            }
                        }
                    }, {
                        text: 'Cancel',
                        iconCls: 'x-button-cancel',
                        handler: function () {
                            me.fireEvent('cancel');
                        }
                    }]
                },
                items: [{
                    xtype: 'form',
                    ref: 'form',
                    border: false,
                    bodyStyle: 'padding:15px;',
                    title: 'Note: Quick Add Classes are not available for public registrations or training program assignment.',
                    //frame: true,
                    defaults: {
                        anchor: '100%',
                        allowBlank: false
                    },
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: 'Name',
                        name: 'name'
                    }, {
                        xtype: 'datefield',
                        fieldLabel: 'Date',
                        name: 'date'
                    }, {
                        xtype: 'timefield',
                        fieldLabel: 'Time',
                        name: 'time'
                    }, {
                        xtype: 'combo',
                        fieldLabel: 'Completion',
                        triggerAction: 'all',
                        queryMode: 'local',
                        store: Symphony.LimitedCourseCompletionTypeStore,
                        valueField: 'id',
                        displayField: 'text',
                        name: 'courseCompletionTypeId'
                    }]
                }]
            });

            this.callParent(arguments);
        },
        getValues: function () {
            var me = this;

            if (!me.form.isValid()) {
                return;
            }

            var result = me.form.getValues();
            var dt = Symphony.parseDate(result.date);
            var hours = me.form.getValues().time.getHours();
            var minutes = me.form.getValues().time.getMinutes();
            /*var hours = result.time.split(':')[0];
            var minutes = result.time.split(':')[1].split(' ')[0];
            var amPm = result.time.split(':')[1].split(' ')[1];
            if (amPm == 'PM' && hours != '12') {
                hours = hours + 12;
            }
            else if (amPm == 'AM' && hours == '12') {
                hours = 0;
            }*/
            dt = dt.add(Date.HOUR, hours);
            dt = dt.add(Date.MINUTE, minutes);

            result.date = dt.formatSymphony('microsoft');
            return result;
        }
    });


    Symphony.Classroom.UserAssignmentPanel = Ext.define('classroom.userassignmentpanel', {
        alias: 'widget.classroom.userassignmentpanel',
        extend: 'Ext.Panel',
        hideToolbar: false,
        initComponent: function () {
            var me = this;

            if (!me.hideToolbar) {
                Ext.apply(this, {
                    tbar: {
                        items: [{
                            text: 'Save',
                            iconCls: 'x-button-save',
                            handler: function () {
                                me.fireEvent('save', me.getSelections());
                            }
                        }, {
                            text: 'Cancel',
                            iconCls: 'x-button-cancel',
                            handler: function () {
                                me.fireEvent('cancel');
                            }
                        }]
                    }
                });
            }

            Ext.apply(this, {
                //bodyStyle: 'padding-right:5px',
                layout: 'anchor',
                items: [{
                    xtype: 'panel',
                    border: false,
                    title: this.title,
                    anchor: '100% 100%',
                    layout: 'border',
                    height: 40,
                    //frame: true,
                    items: [{
                        // assignment panel goes here
                        xtype: 'panel',
                        border: false,
                        region: 'center',
                        layout: 'fit',
                        name: 'assignmentpanel'
                    }, {
                        region: 'north',
                        height: 22,
                        border: false,
                        layout: 'fit',
                        items: [{
                            xtype: 'combo',
                            disabled: this.disabled,
                            name: 'usergroup',
                            triggerAction: 'all',
                            editable: false,
                            queryMode: 'local',
                            store: new Ext.data.ArrayStore({
                                //editable: false,
                                fields: ['id', 'displayName'],
                                data: Symphony.HierarchyTypeStoreData
                            }),
                            valueField: 'id',
                            displayField: 'displayName',
                            emptyText: 'Select a Group',
                            listeners: {
                                beforeselect: function (combo, record, index) {
                                    var id = record.get('id');
                                    if (combo.getValue() > 0) {
                                        // if they have a selection already...
                                        Ext.Msg.confirm('Are you sure?', 'Changing the group will clear your current selections. Are you sure?', function (btn) {
                                            if (btn == 'yes') {
                                                combo.setValue(id);
                                                me.loadHierarchy(id);
                                                me.fireEvent('groupchange', id, false);
                                            }
                                        });
                                        combo.collapse();
                                        // cancel so we can confirm the action
                                        return false;
                                    } else {
                                        me.loadHierarchy(id);
                                        me.fireEvent('groupchange', id, false);
                                    }
                                }
                            }
                        }]
                    }]
                }]
            });
            // reset the title after we're done, we don't want this set by the sub-elements
            this.title = '';
            this.callParent(arguments);
        },
        clearSelections: function () {
            if (this.currentPanel) {
                this.currentPanel.clearSelections();
            }
        },
        setSelections: function (hierarchyTypeId, hierarchyNodeIds) {
            if (hierarchyTypeId > 0) {
                this.find('name', 'usergroup')[0].setValue(hierarchyTypeId);
                this.loadHierarchy(hierarchyTypeId, hierarchyNodeIds);
            }
            this.hierarchyTypeId = hierarchyTypeId;
        },
        getSelections: function () {
            return this.currentPanel ? this.currentPanel.getSelections() : [];
        },
        loadHierarchy: function (hierarchyTypeId, hierarchyNodeIds) {
            var panel = null, height = 345, urlFragment = '', iconCls = '',
                container = this.find('name', 'assignmentpanel')[0];
            switch (hierarchyTypeId) {
                case Symphony.HierarchyType.location:
                    urlFragment = 'locations/';
                    iconCls = 'x-treenode-location';
                    break;
                case Symphony.HierarchyType.jobRole:
                    urlFragment = 'jobroles/';
                    iconCls = 'x-treenode-jobrole';
                    break;
                case Symphony.HierarchyType.audience:
                    urlFragment = 'audiences/';
                    iconCls = 'x-treenode-audience';
                    break;
                case Symphony.HierarchyType.user:
                    panel = new Symphony.CourseAssignment.TrainingProgramUserList({
                        border: false,
                        pageSize: 13, // fits into 1024x768
                        displayPagingInfo: false,
                        value: hierarchyNodeIds || [] //preselections
                    });
                    break;
            }
            if (!panel) {
                panel = new Symphony.CourseAssignment.TrainingProgramHierarchyList({
                    border: false,
                    urlFragment: urlFragment,
                    iconCls: iconCls,
                    bodyStyle: 'background-color:white',
                    hierarchyTypeId: hierarchyTypeId,
                    value: hierarchyNodeIds || [] // pre-selections
                });
            }

            container.removeAll();
            container.add(panel);
            container.doLayout();

            this.currentPanel = panel;
            this.hierarchyTypeId = hierarchyTypeId;
        },
        enable: function () {
            this.find('name', 'usergroup')[0].enable();
            this.find('name', 'clear')[0].enable();
            this.find('name', 'reset')[0].enable();
            if (this.currentPanel && this.currentPanel.enable) {
                this.currentPanel.enable();
            }
            this.disabled = false;
        },
        disable: function () {
            this.find('name', 'usergroup')[0].disable();
            this.find('name', 'clear')[0].disable();
            this.find('name', 'reset')[0].disable();
            if (this.currentPanel && this.currentPanel.disable) {
                this.currentPanel.disable();
            }
            this.disabled = true;
        },
        removeOption: function (id) {
            var cbo = this.find('name', 'usergroup')[0];
            var record = cbo.store.getAt(cbo.store.find('id', id));
            if (this.removed) {
                cbo.store.add(this.removed);
            }
            cbo.store.remove(record);
            this.removed = record;
        },
        reset: function () {
            var cbo = this.find('name', 'usergroup')[0];
            // if there is a removed record, put it back
            if (this.removed) {
                cbo.store.add(this.removed);
            }
            cbo.clearValue();
            this.removed = null;
            this.find('name', 'assignmentpanel')[0].removeAll();
        }
    });

})();
