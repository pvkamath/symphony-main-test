﻿/*
* Permissions for training programs:
* 1) You can only delete training programs you own
* 2) You can only edit training programs you own
* 3) If you change the owner of a TP, you get a warning and upon acceptange, the TP will be closed so you cannot edit it further
*/
Ext.layout.FormLayout.prototype.trackLabels = true;
(function () {
    Symphony.CourseAssignment.deleteTrainingProgram = function (id) {
        var msg = 'Are you sure you want to delete this training program?<br/><br/>'
        msg += 'Deleting a training program will cause it to be unavailable for reporting purposes.';
        Ext.Msg.show({
            title: 'Are you sure?',
            closable: false,
            msg: msg,
            buttonText: {
                ok: 'Delete',
                cancel: 'Cancel'
            },
            fn: function (btn) {
                if (btn == 'ok') {
                    Symphony.Ajax.request({
                        url: '/services/courseassignment.svc/trainingprograms/delete/' + id,
                        success: function () {
                            var app = Symphony.App.find('xtype', 'courseassignment.app')[0];
                            var trainingProgramsTab = app.find('xtype', 'courseassignment.trainingprogramstab')[0];
                            // refresh the grid
                            trainingProgramsTab.find('xtype', 'courseassignment.trainingprogramsgrid')[0].refresh();
                            // remove the tab, if it's open
                            Ext.each(trainingProgramsTab.find('xtype', 'courseassignment.trainingprogramform'), function (form) {
                                if (form.trainingProgramId == id) {
                                    form.confirmedRemove = true;
                                    form.ownerCt.remove(form);
                                }
                            });
                        }
                    });
                }
            },
            icon: Ext.MessageBox.WARNING
        });
    };

    Symphony.CourseAssignment.unassignTrainingProgram = function (id, userId, isBundle) {
        var msgSettings = {
            closeable: false,
            icon: Ext.MessageBox.WARNING,
            fn: function (btn) {
                switch (btn) {
                    case 'yes':
                        url = '/services/courseassignment.svc/trainingprograms/unassign/' + id + '/' + userId + '/bundle';
                        break;
                    case 'ok':
                    case 'no': // Because the no button when confirming to remove a bundle is used to delete just the single training program
                        url = '/services/courseassignment.svc/trainingprograms/unassign/' + id + '/' + userId;
                        break;
                    default:
                        return;
                }

                Symphony.Ajax.request({
                    url: url,
                    success: function () {
                        var app = Symphony.App.find('xtype', 'courseassignment.app')[0];
                        var trainingProgramsTab = app.find('xtype', 'courseassignment.trainingprogramstab')[0];
                        // refresh the grid
                        trainingProgramsTab.find('xtype', 'courseassignment.trainingprogramsgrid')[0].refresh();
                        // remove the tab, if it's open
                        Ext.each(trainingProgramsTab.find('xtype', 'courseassignment.trainingprogramform'), function (form) {
                            if (form.trainingProgramId == id) {
                                form.confirmedRemove = true;
                                form.ownerCt.remove(form);
                            }
                        });
                    }
                });
            }
        }

        if (isBundle) {
            Ext.apply(msgSettings, {
                title: 'Unassign Training Program Bundle?',
                msg: 'You are unassigning a training program from this user that is part of a bundle. Would you like to remove the entire bundle?',
                buttonText: {
                    yes: 'Unassign bundle',
                    no: 'Unassign only this training program.',
                    cancel: 'Cancel'
                }
            });
        } else {
            Ext.apply(msgSettings, {
                title: 'Unassign Training Program?',
                msg: 'Are you sure you want to unassign this training program from the user?',
                buttonText: {
                    ok: 'Unassign training program.',
                    cancel: 'Cancel'
                }
            });
        }

        Ext.Msg.show(msgSettings);
    }

    Symphony.CourseAssignment.deleteNotificationTemplate = function (id) {
        Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this message?', function (btn) {
            if (btn == 'yes') {
                Symphony.Ajax.request({
                    url: '/services/courseassignment.svc/notificationtemplates/delete/' + id,
                    success: function () {
                        var app = Symphony.App.find('xtype', 'courseassignment.app')[0];
                        var templatesTab = app.find('xtype', 'courseassignment.notificationtemplatestab')[0];
                        // refresh the grid
                        templatesTab.find('xtype', 'courseassignment.notificationtemplatesgrid')[0].refresh();
                        // remove the tab, if it's open
                        Ext.each(templatesTab.find('xtype', 'courseassignment.notificationtemplateform'), function (form) {
                            if (form.notificationTemplateId == id) {
                                form.confirmedRemove = true;
                                form.ownerCt.remove(form);
                            }
                        });
                    }
                });
            }
        });
    };

    Symphony.CourseAssignment.deletePublicDocument = function (id) {
        Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this public document?', function (btn) {
            if (btn == 'yes') {
                Symphony.Ajax.request({
                    url: '/services/courseassignment.svc/publicdocuments/delete/' + id,
                    success: function () {
                        var app = Symphony.App.find('xtype', 'courseassignment.app')[0];
                        var templatesTab = app.find('xtype', 'courseassignment.publicdocumentstab')[0];
                        // refresh the grid
                        templatesTab.find('xtype', 'courseassignment.publicdocumentsgrid')[0].refresh();
                        // remove the tab, if it's open
                        Ext.each(templatesTab.find('xtype', 'courseassignment.publicdocumentform'), function (form) {
                            if (form.publicDocumentId == id) {
                                form.confirmedRemove = true;
                                form.ownerCt.remove(form);
                            }
                        });
                    }
                });
            }
        });
    };

    Symphony.CourseAssignment.deleteTrainingProgramRenderer = function (value, meta, record) {
        if (record.get('ownerId') != Symphony.User.id) {
            return '<img src="/images/bullet_white.png" />';
        }
        return '<a href="#" onclick="Symphony.CourseAssignment.deleteTrainingProgram({0});"><img src="/images/bin.png" /></a>'.format(record.get('id'));
    };

    Symphony.CourseAssignment.unassignTrainingProgramRenderer = function (value, meta, record, userId) {
        var showUnassign = false;

        var isBundle = record.get('purchasedFromBundleName') ? true : false;

        if (userId > 0 && record.get('isSalesforce')) {
            showUnassign = true;
        }

        if (!showUnassign) {
            return '<img src="/images/bullet_white.png" />';
        }

        return '<a href="#" onclick="Symphony.CourseAssignment.unassignTrainingProgram({0}, {1}, {2});"><img src="/images/bin.png" /></a>'.format(record.get('id'), userId, isBundle);
    };

    Symphony.CourseAssignment.deleteNotificationTemplateRenderer = function (value, meta, record) {
        if (record.get('isScheduled') || record.get('isUserCreated')) {
            return '<a href="#" onclick="Symphony.CourseAssignment.deleteNotificationTemplate({0});"><img src="/images/bin.png" /></a>'.format(record.get('id'));
        } else {
            return '<img src="/images/bullet_white.png" />';
        }
    };

    Symphony.CourseAssignment.deletePublicDocumentRenderer = function (value, meta, record) {
        return '<a href="#" onclick="Symphony.CourseAssignment.deletePublicDocument({0});"><img src="/images/bin.png" /></a>'.format(record.get('id'));
    };

    Symphony.CourseAssignment.App = Ext.define('courseassignment.app', {
        alias: 'widget.courseassignment.app',
        extend: Symphony.getQueryParam('tabs') == 'false' ? 'Ext.Panel' : 'Ext.TabPanel',
        layout: 'card',
        select: function (xtype) {
            var tabs = this.find('xtype', xtype);
            if (tabs.length > 0) {
                this.setActiveTab(tabs[0]);
                return true;
            }
            return false;
        },
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                requiresActivation: true,
                border: false,
                defaults: {
                    border: false
                },
                listeners: {
                    /*
                    tabchange: function (tabpanel, tab) {
                        // Make sure it's a visible tab
                        // check if the tab element is visible for this item
                        // if not, just find the first one that is visible
                        if (!tab.isVisible()) {
                            for (var i = 0; i < tabpanel.items.length; i++) {
                                var tabEl = tabpanel.items.itemAt(i).tabEl;
                                var element = Ext.get(tabEl);
                                if (element.isVisible()) {
                                    tabpanel.getLayout().setActiveItem(i);
                                    return;
                                }
                            }
                        }
                    }, */
                    afterlayout: function (tabpanel) {
                        var foundTab = false;
                        tabpanel.items.each(function (panel) {
                            var selPanel = tabpanel.getActiveTab();
                            foundTab = foundTab || panel.id == selPanel.id;
                            if (!panel.tab.hidden && foundTab) {
                                tabpanel.setActiveTab(panel)
                                return false;
                            } else {
                                return true;
                            }
                        });
                    }
                },
                items: [{
                    title: 'Libraries',
                    tabTip: 'Create libraries and manage library registrations',
                    xtype: 'libraries.librarytab',
                    name: 'courseassignment.librariestab',
                    cls: Symphony.Modules.getModuleClass(Symphony.Modules.Libraries),
                    hidden: !Symphony.Modules.hasModule(Symphony.Modules.Libraries)
                }, {
                    title: 'Training Programs',
                    tabTip: 'Create training programs and manage training program assignments',
                    xtype: 'courseassignment.trainingprogramstab'
                }, {
                    title: 'Curriculum',
                    tabTip: 'Groups of training programs for Salesforce',
                    xtype: 'courseassignment.trainingprogrambundletab',
                    cls: Symphony.Modules.getModuleClass(Symphony.Modules.Salesforce),
                    hidden: !Symphony.Modules.hasModule(Symphony.Modules.Salesforce)
                }, {
                    title: 'Online Courses',
                    tabTip: 'Upload and update online courses',
                    xtype: 'courseassignment.coursestab'
                }, {
                    title: 'Books',
                    tabTip: 'Books information',
                    xtype: 'courseassignment.bookstab',
                    cls: Symphony.Modules.getModuleClass(Symphony.Modules.Books),
                    hidden: !Symphony.Modules.hasModule(Symphony.Modules.Books)
                }, {
                    title: 'Messaging',
                    tabTip: 'Enable and disable system notifications, and set up scheduled messages sent through Symphony',
                    xtype: 'courseassignment.notificationtemplatestab'
                }, {
                    title: 'Public Documents',
                    tabTip: 'Upload and manage public documents',
                    xtype: 'courseassignment.publicdocumentstab'
                }, {
                    title: 'Locked Users',
                    tabTip: 'Managed users that have been locked out of courses.',
                    xtype: 'courseassignment.lockeduserstab'
                }, {
                    title: 'Bulk Online Course Download',
                    tabTip: 'Configure and download online courses in bulk.',
                    xtype: 'courseassignment.bulkdownloadtab',
                    cls: Symphony.Modules.getModuleClass(Symphony.Modules.BulkDownload),
                    hidden: !Symphony.Modules.hasModule(Symphony.Modules.BulkDownload)
                }]
            });
            this.callParent(arguments);
        }
    });



    Symphony.CourseAssignment.TrainingProgramsTab = Ext.define('courseassignment.trainingprogramstab', {
        alias: 'widget.courseassignment.trainingprogramstab',
        extend: 'Ext.Panel',
        isIncludeSalesforce: false,
        userId: null,
        initComponent: function () {
            var me = this;

            me.trainingProgramGridWidth = 300;

            Ext.apply(this, {
                layout: 'border',
                items: [{
                    split: true,
                    region: 'west',
                    border: false,
                    width: me.trainingProgramGridWidth,
                    stateId: 'courseassignment.trainingprogramsgrid',
                    xtype: 'courseassignment.trainingprogramsgrid',
                    userId: me.userId ? me.userId : null,
                    tbarItems: me.userId ? [] : null,
                    isIncludeSalesforce: me.isIncludeSalesforce,
                    collapsible: true,
                    listeners: {
                        cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            // delete column gets ignored
                            if (columnIndex === 0) { return; }
                            // everything else opens the editor window
                            var record = grid.store.getAt(rowIndex);
                            me.addPanel(record.get('id'), record.get('name'), record.get('description'), record.get('isSalesforce'), record.get('purchasedFromBundleName') ? true : false, record.get('sharedFlag'));
                        },
                        addclick: function () {
                            me.addPanel(0, 'New Training Program', 'New Training Program', false);
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    itemId: 'courseassignment.trainingprogrameditorcontainer',
                    ref: 'trainingprogrameditor',
                    listeners: {
                        beforeremove: function (container, component) {
                            if (!component.confirmedRemove) {
                                Ext.Msg.show({
                                    title: 'Are you sure?',
                                    msg: 'Any unsaved changes will be lost. Are you sure?',
                                    fn: function (btn) {
                                        if (btn == 'yes') {
                                            component.confirmedRemove = true;
                                            container.remove(component);
                                        }
                                    },
                                    cls: 'bringToFront',
                                    buttons: Ext.Msg.YESNO
                                });
                                return false;
                            }
                        },
                        remove: function (container, component) {
                            Ext.each(container.items.items, function (item) {
                                if (item && item.studentProgress) {
                                    item.studentProgress.renderTrainingProgram();
                                }
                            });
                        }
                    }
                }],
                listeners: {
                    // delayed load
                    activate: {
                        fn: function (panel) {
                            panel.find('xtype', 'courseassignment.trainingprogramsgrid')[0].refresh();
                        },
                        single: true
                    }
                }
            });
            this.callParent(arguments);
        },
        addPanel: function (id, name, description, isSalesforce, isBundle, isSharedFlag) {
            var me = this;
            var tabPanel = me.trainingprogrameditor;
            var found = tabPanel.query('> panel[trainingProgramId=' + id + ']')[0];
            if (found && id > 0) {
                tabPanel.activate(found);
            } else {
                var panel = tabPanel.add({
                    xtype: 'courseassignment.trainingprogramform',
                    closable: true,
                    activate: true,
                    width: me.width - me.trainingProgramGridWidth,
                    itemId: id,
                    title: name,
                    tabTip: description,
                    trainingProgramId: id,
                    isShared: isSharedFlag,
                    isDisabled: me.userId ? true : false,
                    userId: me.userId ? me.userId : null,
                    isSalesforce: isSalesforce,
                    isBundle: isBundle,
                    listeners: {
                        save: function () {
                            me.find('xtype', 'courseassignment.trainingprogramsgrid')[0].refresh();
                        },
                        duplicate: function (id, name, description) {
                            me.addPanel(id, name, description);
                            me.find('xtype', 'courseassignment.trainingprogramsgrid')[0].refresh();
                        }
                    }
                });
                panel.show();
            }
            tabPanel.doLayout();
        }
    });


    Symphony.CourseAssignment.NotificationTemplatesTab = Ext.define('courseassignment.notificationtemplatestab', {
        alias: 'widget.courseassignment.notificationtemplatestab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    split: true,
                    region: 'west',
                    width: 300,
                    stateId: 'courseassignment.notificationtemplatesgrid',
                    xtype: 'courseassignment.notificationtemplatesgrid',
                    listeners: {
                        cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            // delete column gets ignored
                            if (columnIndex === 0) { return; }
                            // everything else opens the editor window
                            var record = grid.store.getAt(rowIndex);
                            me.addPanel(record.get('id'), record.get('displayName'), record.get('description'));
                        },
                        addclick: function (data) {
                            me.addPanel(data && data.id > 0 ? data.id : 0, 'New Message', 'New Message', data);
                        },
                        quicksendclick: function () {
                            var win = new Ext.Window({
                                modal: true,
                                //resizable: false,
                                title: 'Send a quick message',
                                height: 500,
                                width: 850,
                                layout: 'fit',
                                items: [{
                                    xtype: 'courseassignment.quicksendpanel',
                                    border: false,
                                    listeners: {
                                        send: function (data) {
                                            Symphony.Ajax.request({
                                                url: '/services/courseassignment.svc/messages/',
                                                jsonData: data,
                                                success: function (result) {
                                                    if (result) {
                                                        Ext.Msg.alert("Message sent", "Message sent successfully.");
                                                    }
                                                }
                                            });
                                            win.destroy();
                                        },
                                        cancel: function () {
                                            win.destroy();
                                        }
                                    }
                                }]
                            }).show();
                        }
                    }
                    // grid
                }, {
                    enableTabScroll: true,
                    region: 'center',
                    xtype: 'tabpanel',
                    itemId: 'courseassignment.notificationtemplateeditorcontainer'
                    // forms
                }]
            });
            this.callParent(arguments);
        },
        addPanel: function (id, name, description, data) {
            var me = this;
            var tabPanel = me.query('tabpanel')[0];
            var found = tabPanel.queryBy(function (p) { return p.notificationTemplateId == id; })[0];
            if (found) {
                tabPanel.activate(found);
            } else {
                var panel = tabPanel.add({
                    xtype: 'courseassignment.notificationtemplateform',
                    closable: true,
                    activate: true,
                    itemId: id,
                    title: name,
                    tabTip: description,
                    notificationTemplateId: id,
                    templateData: data,
                    listeners: {
                        save: function () {
                            me.find('xtype', 'courseassignment.notificationtemplatesgrid')[0].refresh();
                        }
                    }
                });
                panel.show();
            }
            tabPanel.doLayout();
        }
    });


    Symphony.CourseAssignment.QuickSendPanel = Ext.define('courseassignment.quicksendpanel', {
        alias: 'widget.courseassignment.quicksendpanel',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Send',
                        iconCls: 'x-button-email',
                        handler: function () {
                            var values = me.getValues();
                            if (values) {
                                me.fireEvent('send', values);
                            }
                        }
                    }, {
                        text: 'Cancel',
                        iconCls: 'x-button-cancel',
                        handler: function () {
                            me.fireEvent('cancel');
                        }
                    }]
                },
                layout: 'border',
                border: false,
                items: [{
                    xtype: 'classroom.userassignmentpanel',
                    layout: 'fit',
                    title: 'Recipients',
                    ref: 'users',
                    hideToolbar: true,
                    region: 'west',
                    //flex: 0.4
                    width: 280
                }, {
                    xtype: 'form',
                    ref: 'form',
                    border: false,
                    labelAlign: 'top',
                    //flex: 0.6,
                    region: 'center',
                    bodyStyle: 'padding:15px;',
                    frame: true,
                    defaults: {
                        anchor: '100%',
                        allowBlank: false
                    },
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: 'Subject',
                        name: 'subject'
                    }, {
                        xtype: 'htmleditor',
                        enableLists: false,
                        fieldLabel: 'Body',
                        name: 'body',
                        height: 330
                    }]
                }]
            });

            this.callParent(arguments);
        },
        getValues: function () {
            var me = this;

            if (!me.form.isValid()) {
                return;
            }
            var data = me.form.getValues();
            data.recipients = me.users.getSelections();

            return data;
        }
    });


    Symphony.CourseAssignment.CoursesTab = Ext.define('courseassignment.coursestab', {
        alias: 'widget.courseassignment.coursestab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    split: true,
                    region: 'west',
                    stateId: 'courseassignment.coursesgrid',
                    xtype: 'courseassignment.coursesgrid',
                    width: 300,
                    listeners: {
                        rowclick: function (grid, rowIndex, e) {
                            var record = grid.store.getAt(rowIndex);
                            me.addPanel(record.get('id'), record.get('name'), record.get('description'));
                        },
                        uploadcomplete: function (courseId) {
                            this.refresh();
                            me.addPanel(courseId, 'New Course', 'New Course');
                        }
                    }
                }, {
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    ref: 'tabpanel'
                    //id: 'courseassignment.courseeditorcontainer'
                    // forms
                }]
            });
            this.callParent(arguments);
        },
        addPanel: function (id, name, description) {
            var me = this;
            //var tabPanel = me.down('name', 'tabpanel')[0]; // Ext.getCmp('courseassignment.courseeditorcontainer');
            var found = me.tabpanel.queryBy(function (p) { return p.courseId == id; })[0];
            if (found) {
                me.tabpanel.activate(found);
            } else {
                var panel = me.tabpanel.add({
                    xtype: 'courseassignment.courseform',
                    border: false,
                    layout: 'fit',
                    closable: true,
                    activate: true,
                    title: name,
                    tabTip: description,
                    //id: id,
                    courseId: id,
                    listeners: {
                        save: function () {
                            me.find('xtype', 'courseassignment.coursesgrid')[0].refresh();
                        }
                    }
                });
                panel.show();
                me.tabpanel.doLayout();
            }
        }
    });


    Symphony.CourseAssignment.PublicDocumentsTab = Ext.define('courseassignment.publicdocumentstab', {
        alias: 'widget.courseassignment.publicdocumentstab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    split: true,
                    region: 'west',
                    ref: 'publicDocumentsGrid',
                    stateId: 'courseassignment.publicdocumentsgrid',
                    xtype: 'courseassignment.publicdocumentsgrid',
                    width: 300,
                    listeners: {
                        cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            if (columnIndex == 0) { return; } // ignore the delete column
                            var record = grid.store.getAt(rowIndex);
                            me.addPanel(record.get('id'), record.get('name'), record.get('description'));
                        },
                        uploadcomplete: function (publicDocumentId) {
                            this.refresh();
                            me.addPanel(publicDocumentId, 'New Document', 'New Document');
                        }
                    }
                }, {
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    ref: 'publicDocumentsContainer'
                    // forms
                }]
            });
            this.callParent(arguments);
        },
        addPanel: function (id, name, description) {
            var me = this;
            var tabPanel = this.publicDocumentsContainer;
            var found = tabPanel.queryBy(function (p) { return p.publicDocumentId == id; })[0];

            if (found) {
                tabPanel.activate(found);
            } else {
                var panel = tabPanel.add({
                    xtype: 'courseassignment.publicdocumentform',
                    layout: 'fit',
                    border: false,
                    closable: true,
                    activate: true,
                    title: name,
                    tabTip: description,
                    //id: id,
                    publicDocumentId: id,
                    listeners: {
                        save: function () {
                            me.publicDocumentsGrid.refresh();
                        }
                    }
                });
                panel.show();
                tabPanel.doLayout();
            }
        }
    });


    Symphony.CourseAssignment.TrainingProgramsGrid = Ext.define('courseassignment.trainingprogramsgrid', {
        alias: 'widget.courseassignment.trainingprogramsgrid',
        extend: 'symphony.searchablegrid',
        isHideDelete: false,
        userId: null,
        stateful: true,
        stateId: 'trainingprogramsgrid',
        isIncludeSalesforce: false,
        initComponent: function () {
            var me = this;
            var url = '/services/courseassignment.svc/trainingprograms/';

            // If a user has been passed in then this grid will be scoped to the 
            // training programs assigned to the current user. 
            if (me.userId) {
                url = '/services/portal.svc/trainingprograms/' + me.userId;
            }


            var columns = [
                    {
                        /*id: 'name',*/ header: 'Program Name', dataIndex: 'name', minWidth: 140, align: 'left',
                        flex: 1
                    },
                    { header: 'New Hire', dataIndex: 'isNewHire', renderer: Symphony.checkRenderer, width: 64 },
                    { header: 'Enabled', dataIndex: 'isLive', renderer: Symphony.checkRenderer, width: 64 },
                    { header: 'End Date', dataIndex: 'endDate', renderer: Symphony.pastDefaultDateRenderer, width: 64 },
                    { header: 'Sku', dataIndex: 'sku', width: 64 },
					{ header: 'Shared', dataIndex: 'sharedFlag', renderer: Symphony.checkRenderer, width: 64 }
            ]

            if (me.isIncludeSalesforce) {
                columns = columns.concat(
                    [
                        { header: 'Bundle', hideable: false, dataIndex: 'purchasedFromBundleName' },
                        { header: 'Is Salesforce', hideable: false, dataIndex: 'isSalesforce', renderer: Symphony.checkRenderer, width: 50 }
                    ]);
            }

            if (me.userId > 0) {
                columns.unshift({
                    header: 'Active',
                    dataIndex: 'isActive',
                    renderer: function (value) {
                        return Symphony.checkRenderer(value, value ? "Active training program" : "Inactive training program");
                    }
                });
            }

            if (!me.isHideDelete) {
                columns.unshift({
                    header: ' ', hideable: false, renderer: function (value, meta, record) {
                        if (me.userId) {
                            return Symphony.CourseAssignment.unassignTrainingProgramRenderer(value, meta, record, me.userId);
                        } else {
                            return Symphony.CourseAssignment.deleteTrainingProgramRenderer(value, meta, record);
                        }
                    }, width: 35
                });
            }



            if (me.sm) {
                columns.unshift(me.sm);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: columns
            });



            Ext.apply(this, {
                searchMenu: {
                    items: [{
                        text: 'Enabled',
                        group: 'searchGroup',
                        filter: { enabled: true },
                        checked: false,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }, {
                        text: 'Disabled',
                        group: 'searchGroup',
                        filter: { disabled: true },
                        checked: false,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }, {
                        text: 'Active',
                        group: 'searchGroup',
                        filter: { active: true },
                        checked: false,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }, {
                        text: 'Archived',
                        group: 'searchGroup',
                        filter: { archived: true },
                        checked: false,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }, {
                        text: 'All',
                        group: 'searchGroup',
                        filter: {},
                        checked: true,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }]
                },
                tbar: {
                    items: me.tbarItems ? me.tbarItems : [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }]
                }, /*
                listeners: Ext.apply(this.listeners || {}, {
                    rowcontextmenu: function (grid, rowIndex, e) {
                        var record = grid.store.getAt(rowIndex);
                        var isNewHire = record.get('isNewHire'), isLive = record.get('isLive');
                        var menu = new Ext.menu.Menu({
                            items: [
                                {
                                    text: 'Make this a ' + (isNewHire ? 'non new hire' : 'new hire') + ' training program',
                                    handler: function () {
                                        Symphony.Ajax.request({
                                            url: '/services/courseassignment.svc/trainingprograms/' + record.get('id') + '/isnewhire/' + (!isNewHire),
                                            success: Ext.bind(me.refresh, me)
                                        });
                                    }
                                },
                                {
                                    text: (isLive ? 'Disable' : 'Enable') + ' this training program',
                                    handler: function () {
                                        Symphony.Ajax.request({
                                            url: '/services/courseassignment.svc/trainingprograms/' + record.get('id') + '/islive/' + (!isLive),
                                            success: Ext.bind(me.refresh, me)
                                        });
                                    }
                                }
                            ]
                        });
                        menu.showAt(e.getXY());
                        e.stopEvent();
                    }
                }),*/
                idProperty: 'id',
                deferLoad: true,
                url: url,
                colModel: colModel,
                model: 'trainingProgram'
            });
            this.callParent(arguments);
        }
    });


    Symphony.CourseAssignment.CoursesGrid = Ext.define('courseassignment.coursesgrid', {
        alias: 'widget.courseassignment.coursesgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/courseassignment.svc/courses/';


            var columns = [
                {
                    /*id: 'name',*/ header: 'Course Name', dataIndex: 'name', width: 140, align: 'left',
                    flex: 1
                },
                { header: 'Version', dataIndex: 'version' },
                { header: 'Created', dataIndex: 'createdOn', renderer: Symphony.shortDateRenderer },
                { header: 'Modified', dataIndex: 'modifiedOn', renderer: Symphony.shortDateRenderer },
                { header: 'Public', dataIndex: 'publicIndicator', renderer: Symphony.checkRenderer }
            ]

            if (this.sm) {
                columns = [this.sm].concat(columns);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer,
                    width: 50
                },
                columns: columns
            });

            Ext.apply(this, {
                tbar: this.tbar ? this.tbar : {
                    items: [{
                        // creates a new online course
                        text: 'Upload New',
                        xtype: 'swfuploadbutton',
                        uploadUrl: '/Uploaders/TrainingProgramCourseUploader.ashx',
                        iconCls: 'x-button-add',
                        fileTypes: '*.zip',
                        fileTypesDescription: 'Zip Files',
                        fileQueueLimit: 1,
                        listeners: {
                            'queue': function (file) {
                                this.startUpload();
                            },
                            'queueerror': function () {
                                Ext.Msg.alert('Error', 'The file could not be queued');
                            },
                            'uploadstart': function (item) {
                                this.addParameter('courseId', 0);
                                this.addParameter('customerId', Symphony.User.customerId);
                                this.addParameter('userId', Symphony.User.id);

                                Ext.Msg.progress('Please wait...', 'Please wait while your file is uploaded...');
                            },
                            'uploadprogress': function (item, completed, total) {
                                var ratio = parseFloat(completed / total);
                                Ext.Msg.updateProgress(ratio, parseInt(ratio * 100, 10) + "% complete");
                            },
                            'uploaderror': function () {
                                this.error = true;
                                Ext.Msg.hide();
                                Ext.Msg.alert('Upload failed', 'The upload failed. Please make sure it is within the proper file size limitations.');
                            },
                            'uploadsuccess': function (item, response, hasData) {
                                var result = Ext.decode(response);
                                if (result.success) {
                                    Ext.Msg.hide();
                                    me.fireEvent('uploadcomplete', result.id);
                                } else {
                                    Ext.Msg.hide();
                                    Ext.Msg.alert('Upload failed', result.error);
                                }
                            }
                        }
                    }]
                },
                idProperty: 'id',
                //displayPagingInfo: false,
                url: url,
                colModel: colModel,
                model: 'course'
            });

            this.callParent(arguments);
        }
    });


    Symphony.CourseAssignment.PublicDocumentsGrid = Ext.define('courseassignment.publicdocumentsgrid', {
        alias: 'widget.courseassignment.publicdocumentsgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/courseassignment.svc/publicdocuments/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer,
                    width: 80
                },
                columns: [
                    { header: ' ', hideable: false, renderer: Symphony.CourseAssignment.deletePublicDocumentRenderer, width: 35 },
                    { /*id: 'categoryName',*/ header: 'Category', dataIndex: 'categoryName' },
                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name',
                        flex: 1
                    },
                    { header: 'Created', dataIndex: 'uploaded', renderer: Symphony.dateTimeRenderer, width: 130 }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        // creates a new online course
                        text: 'Upload New',
                        xtype: 'swfuploadbutton',
                        uploadUrl: '/Uploaders/PublicDocumentUploader.ashx',
                        iconCls: 'x-button-add',
                        fileTypes: '*.*',
                        fileTypesDescription: 'All Files',
                        /*fileQueueLimit: 1,*/
                        listeners: {
                            'queue': function (file) {
                                this.startUpload();
                            },
                            'queueerror': function () {
                                Ext.Msg.alert('Error', 'The file could not be queued');
                            },
                            'uploadstart': function (item) {
                                this.addParameter('publicDocumentId', 0);
                                this.addParameter('customerId', Symphony.User.customerId);
                                this.addParameter('userId', Symphony.User.id);

                                Ext.Msg.progress('Please wait...', 'Please wait while your file is uploaded...');
                            },
                            'uploadprogress': function (item, completed, total) {
                                var ratio = parseFloat(completed / total);
                                Ext.Msg.updateProgress(ratio, parseInt(ratio * 100, 10) + "% complete");
                            },
                            'uploaderror': function () {
                                this.error = true;
                                Ext.Msg.hide();
                                Ext.Msg.alert('Upload failed', 'The upload failed. Please make sure it is within the proper file size limitations.');
                            },
                            'uploadsuccess': function (item, response, hasData) {
                                var result = Ext.decode(response);
                                if (result.success) {
                                    Ext.Msg.hide();
                                    me.fireEvent('uploadcomplete', result.id);
                                } else {
                                    Ext.Msg.hide();
                                    Ext.Msg.alert('Upload failed', result.error);
                                }
                            }
                        }
                    }]
                },
                idProperty: 'id',
                //displayPagingInfo: false,
                url: url,
                colModel: colModel,
                model: 'publicDocument'
            });
            this.callParent(arguments);
        }
    });


    Symphony.CourseAssignment.NotificationTemplatesGrid = Ext.define('courseassignment.notificationtemplatesgrid', {
        alias: 'widget.courseassignment.notificationtemplatesgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/courseassignment.svc/notificationtemplates/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        header: ' ',
                        hideable: false,
                        renderer: Symphony.CourseAssignment.deleteNotificationTemplateRenderer,
                        width: 35
                    }, {
                        /*id: 'displayName',*/
                        header: 'Display Name',
                        dataIndex: 'displayName',
                        width: 70,
                        renderer: function (value, meta, record) {
                            // remove newlines from the value, but still use them in the hover text
                            return Symphony.qtipRenderer(value, record.get('body'));
                        },
                        flex: 1
                    },
                    { dataIndex: 'isScheduled', header: 'System', renderer: function (value) { return Symphony.checkRenderer(!value); }, width: 50 },
                    { dataIndex: 'enabled', header: 'Enabled', renderer: Symphony.checkRenderer, width: 50 }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        menu: {
                            items: [
                                '<strong class="x-menu-title">Scheduled</strong>'
                            , {
                                text: 'Scheduled Notification',
                                listeners: {
                                    click: Ext.bind(me.addMenuClick, me)
                                }
                            }]
                        },
                        loader: {
                            url: '/services/courseassignment.svc/notificationtemplates/system/',
                            autoLoad: true,
                            loadMask: true,
                            renderer: function (loader, response, active) {
                                var result = Ext.decode(response.responseText);
                                var templates = result.data;
                                var btn = loader.target;

                                btn.menu.add('<strong class="x-menu-title">System Notifications</strong>');
                                for (var i = 0; i < templates.length; i++) {
                                    btn.menu.add({
                                        text: templates[i].displayName,
                                        template: templates[i],
                                        listeners: {
                                            click: Ext.bind(me.addMenuClick, me)
                                        }
                                    });
                                }

                            }
                        }
                    }, {
                        text: 'Quick Send',
                        iconCls: 'x-button-email',
                        handler: function () {
                            me.fireEvent('quicksendclick');
                        }
                    }]
                },
                idProperty: 'id',
                //displayPagingInfo: false,
                url: url,
                colModel: colModel,
                model: 'notificationTemplate'
            });
            this.callParent(arguments);
        },
        addMenuClick: function (item) {
            this.fireEvent('addclick', item.template);
        }
    });

    Ext.define('Symphony.SaveCancelBar', {
        extend: 'Ext.Toolbar',
        alias: 'widget.symphony.savecancelbar',
        cancelButton: true,
        hideSaveCancel: false,
        disableSaveCancel: false,
        showManually: false,
        initComponent: function () {
            var items = [{
                xtype: 'button',
                text: 'Save',
                name: 'save',
                hidden: this.hideSaveCancel,
                disabled: this.disableSaveCancel,
                iconCls: 'x-button-save',
                handler: Ext.bind(this.save, this)
            }];
            if (this.cancelButton) {
                items.push({
                    xtype: 'button',
                    text: 'Cancel',
                    name: 'cancel',
                    disabled: this.disableSaveCancel,
                    hidden: this.hideSaveCancel,
                    iconCls: 'x-button-cancel',
                    handler: Ext.bind(this.cancel, this)
                });
            }
            Ext.apply(this, {
                items: items.concat(this.items || [])
            });

            Symphony.SaveCancelBar.superclass.initComponent.apply(this, arguments);
            this.addEvents('save', 'cancel');
        },
        save: function () {
            if (this.fireEvent('save') !== false) {
                if (!this.showManually) {
                    this.showSaving();
                }
            } else {
                this.showNotSaving();
            }
        },
        cancel: function () {
            this.fireEvent('cancel');
        },
        showSaving: function () {
            var savingText = this.savingText = new Ext.Toolbar.TextItem({ text: '<span class="notification">Saving...</span>' });

            this.add(this.savingText);
            this.doLayout();

            // register with the completion of the next ajax request
            Ext.Ajax.on('requestcomplete', this.hideSaving, this);
            Ext.Ajax.on('requestexception', this.hideSaving, this);

            window.setTimeout(Ext.bind(function () {
                this.remove(savingText);
                this.doLayout();
            }, this), 5000); // fallback
        },
        showNotSaving: function () {
            this.notSavingText = new Ext.Toolbar.TextItem({ text: '<span class="notification">Some values are invalid. Please check your input.</span>' });
            this.add(this.notSavingText);
            this.doLayout();

            window.setTimeout(Ext.bind(function () {
                this.remove(this.notSavingText);
                this.doLayout();
            }, this), 3000);
        },
        showError: function(error) {
            this.errorText = new Ext.Toolbar.TextItem({ text: '<span class="notification">' + error + '</span>' });
            this.add(this.errorText);
            this.doLayout();

            window.setTimeout(Ext.bind(function () {
                this.remove(this.errorText);
                this.doLayout();
            }, this), 5000);
        },
        hideSaving: function () {
            this.remove(this.savingText);
            this.doLayout();

            // unregister
            Ext.Ajax.un('requestcomplete', this.hideSaving, this);
            Ext.Ajax.un('requestexception', this.hideSaving, this);
        }
    });

    Symphony.CourseAssignment.TrainingProgramSummary = Ext.define('courseassignment.trainingprogramsummary', {
        alias: 'widget.courseassignment.trainingprogramsummary',
        extend: 'Ext.Panel',
        canMoveForwardIndicatorCheckbox: null,
        finalAffidavitSelector: null,
        isShared: false,
        tempData: null,
        unlockModeStore: new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [
                [0, 'Never'],
                [1, 'On Completion']
            ]
        }),
        initComponent: function () {
            var me = this;

            me.enforceCanMoveForwardCheckboxId = Ext.id();
            me.finalAffidavitId = Ext.id();
            me.courseUnlockModeId = Ext.id();

            var template = new Ext.Template([
                '<div class="summary">',
                    '<h2>Summary</h2>',
                    '<div class="group required">',
                        '<h3>Required</h3>',
                        '<p><span class="count">{requiredCount}</span></p>',
                    '</div>',
                    '<div class="group elective">',
                        '<h3>Elective</h3>',
                        '<p><span>Take</span> <span><input class="minimum" style="width:25px" value="{minimumElectives}" {lockElectiveOnSharedTraining} onchange="this.value=Math.max(Math.min(this.value, {electiveCount}),0)" /></span> <span>of</span> <span class="count">{electiveCount}</span></p>',
                    '</div>',
                    '<div class="group optional">',
                        '<h3>Optional</h3>',
                        '<p><span class="count">{optionalCount}</span></p>',
                    '</div>',
                    '<div class="group assessment">',
                        '<h3>Assessment</h3>',
                        '<p><span class="set">{assessmentSet}</span></p>',
                    '</div>',
                '</div>'
            ]);
            Ext.apply(this, {
                layout: 'border',
                items: [{
                    region: 'center',
                    border: false,
                    cls: 'settings',
                    layout: {
                        type: 'vbox',
                        align: 'center',
                        pack: 'start'
                    },
                    items: [{
                        xtype: 'panel',
                        border: false,
                        html: '<h2>Settings</h2>'
                    }, {
                        xtype: 'label',
                        text: 'Enforce Order'
                    }, {
                        xtype: 'checkbox',
                        fieldLabel: '',
                        boxLabel: '',
                        name: 'enforceRequiredOrder',
                        fieldCls: 'enforce',
                        checked: me.tempData && me.tempData.enforceRequiredOrder ? true : false,
                        disabled: me.tempData && me.tempData.enforceCanMoveForwardIndicator ? true : (this.isShared ? true : false)
                    }, {
                        xtype: 'label',
                        text: 'Require Instructor Approval'
                    }, {
                        xtype: 'checkbox',
                        fieldCls: 'enforceCanMoveForwardIndicator group',
                        name: 'enforceCanMoveForwardIndicator',
                        fieldLabel: '',
                        boxLabel: '',
                        checked: me.tempData && me.tempData.enforceCanMoveForwardIndicator ? true : false,
                        disabled: this.isShared,
                        handler: function (chkbox, checked) {
                            var enforceOrderInput = me.query('checkbox[name=enforceRequiredOrder]')[0];

                            if (checked) {
                                enforceOrderInput.setValue(true);
                                enforceOrderInput.setDisabled(true);
                            } else {
                                enforceOrderInput.setDisabled(false);
                            }
                        }
                    }, {
                        xtype: 'label',
                        text: 'Unlock Courses'
                    },
                    Symphony.getCombo('courseUnlockModeId', 'Unlock Courses', me.unlockModeStore, {
                        width: 110,
                        value: 0,
                        disabled: this.isShared,
                        hideLabel: true
                    }),
                    {
                        xtype: 'label',
                        text: 'Training Program Affidavit'
                    }, {
                        name: 'trainingProgramAffidavitSelector',
                        border: false,
                        layout: {
                            type: 'hbox',
                            pack: 'center',
                            align: 'center'
                        },
                        width: '100%',
                        listeners: {
                            render: function (panel) {
                                Ext.EventManager.onWindowResize(panel.doLayout, panel);
                            }
                        },
                        items: [{
                            name: 'trainingProgramAffidavitId',
                            fieldLabel: '',
                            labelWidth: 0,
                            width: 110,
                            listWidth: 250,
                            deferLoad: true,
                            xtype: 'symphony.pagedcombobox',
                            url: '/services/affidavit.svc/affidavits/',
                            model: 'AffidavitFinalExam',
                            bindingName: 'trainingProgramAffidavitName',
                            valueField: 'id',
                            displayField: 'name',
                            listeners: {
                                afterrender: function (combo) {
                                    combo.store.on('load', function (store) {
                                        var emptyRecord = new AffidavitFinalExam({
                                            id: 0,
                                            name: 'None',
                                            affidavitJSON: '{}'
                                        });

                                        combo.store.insert(0, emptyRecord);

                                        combo.setValue(me.tempData.trainingProgramAffidavitId);
                                    }, combo);

                                    combo.store.load();
                                }
                            }
                        }, {
                            xtype: 'button',
                            text: '',
                            iconCls: 'x-button-preview',
                            name: 'preview',
                            tooltip: 'Preview affidavit',
                            width: 20,
                            handler: function () {
                                var affidavitId = me.getTrainingProgramAffidavitId();

                                Symphony.CourseAssignment.AffidavitPanel.showAffidavitWindow(affidavitId, 'preview');
                            }
                        }]
                    },
                    /*{ xtype: 'spacer', height: 20 },*/
                    {
                        xtype: 'label',
                        text: 'Final Affidavit'
                    },
                    {
                        name: 'affidavitSelector',
                        border: false,
                        layout: {
                            type: 'hbox',
                            pack: 'center',
                            align: 'center'
                        },
                        width: '100%',
                        listeners: {
                            render: function (panel) {
                                Ext.EventManager.onWindowResize(panel.doLayout, panel);
                            }
                        },
                        items: [{
                            name: 'affidavitId',
                            fieldLabel: '',
                            labelWidth: 0,
                            width: 110,
                            listWidth: 250,
                            deferLoad: true,
                            xtype: 'symphony.pagedcombobox',
                            url: '/services/affidavit.svc/affidavits/',
                            model: 'AffidavitFinalExam',
                            bindingName: 'affidavitName',
                            valueField: 'id',
                            displayField: 'name',
                            listeners: {
                                afterrender: function (combo) {
                                    combo.store.on('load', function (store) {
                                        var emptyRecord = new AffidavitFinalExam({
                                            id: 0,
                                            name: 'None',
                                            affidavitJSON: '{}'
                                        });

                                        combo.store.insert(0, emptyRecord);

                                        combo.setValue(me.tempData.affidavitId);
                                    }, combo);

                                    combo.store.load();
                                }
                            }
                        }, {
                            xtype: 'button',
                            text: '',
                            iconCls: 'x-button-preview',
                            name: 'preview',
                            tooltip: 'Preview affidavit',
                            width: 20,
                            handler: function () {
                                var affidavitId = me.getAffidavitId();

                                Symphony.CourseAssignment.AffidavitPanel.showAffidavitWindow(affidavitId, 'preview');
                            }
                        }]
                    }, {
                        xtype: 'label',
                        text: 'Proctor Form'
                    }, {
                        name: 'proctorFormSelector',
                        border: false,
                        width: '100%',
                        layout: {
                            type: 'hbox',
                            pack: 'center',
                            align: 'center'
                        },
                        listeners: {
                            render: function (panel) {
                                Ext.EventManager.onWindowResize(panel.doLayout, panel);
                            }
                        },
                        items: [{
                            name: 'proctorFormId',
                            fieldLabel: '',
                            labelWidth: 0,
                            width: 110,
                            listWidth: 250,
                            deferLoad: true,
                            xtype: 'symphony.pagedcombobox',
                            url: '/services/proctor.svc/proctorForms/',
                            model: 'ProctorForm',
                            bindingName: 'proctorFormName',
                            valueField: 'id',
                            displayField: 'name',
                            listeners: {
                                afterrender: function (combo) {
                                    combo.store.on('load', function (store) {
                                        combo.setValue(me.tempData.proctorFormId);
                                    }, combo);

                                    combo.store.load();
                                }
                            }
                        }, {
                            xtype: 'button',
                            text: '',
                            iconCls: 'x-button-preview',
                            name: 'preview',
                            tooltip: 'Preview proctor form',
                            width: 20,
                            handler: function () {
                                var proctorFormId = me.getProctorFormId();

                                Symphony.Portal.ProctorPanel.showProctorWindow(proctorFormId, 'preview');
                            }
                        }]
                    }]
                }, {
                    xtype: 'panel',
                    ref: 'summary',
                    tpl: template,
                    region: 'north',
                    height: 245,
                    border: false,
                    listeners: {
                        afterrender: function (panel) {
                            panel.update(me.tempData);
                        }
                    }
                }],
                listeners: {
                    afterlayout: function () {
                        if (me.tempData) {
                            me.setData(me.tempData);
                        }
                    }
                }
            });
            this.callParent(arguments);
        },
        setData: function (data) {
            var templateInfo = {
                requiredCount: data.requiredCount,
                electiveCount: data.electiveCount,
                optionalCount: data.optionalCount,
                minimumElectives: data.minimumElectives,
                enforceRequiredOrder: data.enforceRequiredOrder || data.enforeCanMoveFowardIndicator ? 'checked="checked"' : '',
                isEnforceLockedByTrainingShared: this.isShared ? 'disabled="disabled"' : '',
                lockElectiveOnSharedTraining: this.isShared ? 'disabled' : '',
                assessmentSet: data.assessmentSet ? 'Set' : 'Not Set',
                enforceCanMoveForwardIndicator: data.enforceCanMoveForwardIndicator ? 'checked="checked"' : '',
                isEnforceOrderDisabled: data.enforceCanMoveForwardIndicator ? 'disabled' : '',
                enforceCanMoveForwardId: this.enforceCanMoveForwardCheckboxId,
                courseUnlockModeId: this.courseUnlockModeId,
                finalAffidavitId: this.finalAffidavitId,
                trainingProgramAffidavitId: this.trainingProgramAffidavitId,
                proctorFormId: this.proctorFormId
            };

            if (this.rendered) {
                this.summary.update(templateInfo);

                this.find('name', 'enforceRequiredOrder')[0].setValue(data.enforceRequiredOrder || data.enforceCanMoveForwardIndicator);
                this.find('name', 'enforceCanMoveForwardIndicator')[0].setValue(data.enforceCanMoveForwardIndicator);

                this.find('name', 'courseUnlockModeId')[0].setValue(data.courseUnlockModeId);

                this.tempData = data;
            } else {
                this.data = templateInfo;
                this.tempData = data;
            }
        },
        getAffidavitId: function () {
            var affidavitSelector = this.find('name', 'affidavitId')[0];
            if (affidavitSelector) {
                return affidavitSelector.getValue();
            }
            return 0;
        },
        getTrainingProgramAffidavitId: function () {
            var affidavitSelector = this.find('name', 'trainingProgramAffidavitId')[0];
            if (affidavitSelector) {
                return affidavitSelector.getValue();
            }
            return 0;
        },
        getProctorFormId: function () {
            var proctorFormSelector = this.find('name', 'proctorFormId')[0];
            if (proctorFormSelector) {
                return proctorFormSelector.getValue();
            }
            return 0;
        },
        getCourseUnlockModeId: function () {
            var courseUnlockSelector = this.find('name', 'courseUnlockModeId')[0];
            if (courseUnlockSelector) {
                return courseUnlockSelector.getValue()
            }
            return 0;
        }
    });


    Symphony.CourseAssignment.CourseForm = Ext.define('courseassignment.courseform', {
        alias: 'widget.courseassignment.courseform',
        extend: 'Ext.Panel',
        courseId: 0,
        courseModeData: [
            [0, false, false, 'Online course'],
            [1, false, true, 'Classroom Test']
        ],
        courseModeStore: null,
        showValidationAlert: false,
        initComponent: function () {
            var me = this;

            if (Symphony.Modules.hasModule(Symphony.Modules.Surveys)) {
                me.courseModeData.push([2, true, false, 'Course Survey']);
            }

            me.courseModeStore = new Ext.data.ArrayStore({
                id: 0,
                fields: ['id', 'isSurvey', 'isTest', 'text'],
                data: me.courseModeData
            });

            var categoriesUrl = '/services/courseassignment.svc/categories/';

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 30,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        items: [{
                            xtype: 'swfuploadbutton',
                            uploadUrl: '/Uploaders/TrainingProgramCourseUploader.ashx',
                            name: 'uploader',
                            text: 'Update',
                            iconCls: 'x-button-upload',
                            fileTypes: '*.zip',
                            fileTypesDescription: 'Zip Files',
                            fileQueueLimit: 1,
                            listeners: {
                                'queue': function (file) {
                                    if (!this.textAdded) {
                                        this.addParameter('courseId', me.courseId);
                                        this.addParameter('userId', Symphony.User.id);
                                        this.addParameter('customerId', Symphony.User.customerId);
                                        this.queueNotification = [
                                            new Ext.toolbar.Separator(),
                                            new Ext.toolbar.TextItem('<span style="font-style:italic">The file has been queued and will be uploaded when you save this course.</span>')
                                        ];
                                        this.ownerCt.add(this.queueNotification);
                                        this.ownerCt.doLayout();
                                        this.textAdded = true;
                                    }
                                },
                                'queueerror': function () {
                                    Ext.Msg.alert('Error', 'The file could not be queued');
                                },
                                'uploadstart': function (item) {
                                    Ext.Msg.progress('Please wait...', 'Please wait while your file is uploaded...');
                                },
                                'uploadprogress': function (item, completed, total) {
                                    var ratio = parseFloat(completed / total);
                                    Ext.Msg.updateProgress(ratio, parseInt(ratio * 100, 10) + "% complete");
                                },
                                'uploaderror': function () {
                                    this.error = true;
                                    Ext.Msg.hide();
                                    Ext.Msg.alert('Upload failed', 'The upload failed. Please make sure it is within the proper file size limitations.');
                                },
                                'allcomplete': function () {
                                    if (!this.error) {
                                        Ext.Msg.hide();
                                    }
                                    this.error = false;

                                    // always remove the queue notification
                                    if (this.queueNotification) {
                                        for (var i = 0; i < this.queueNotification.length; i++) {
                                            this.ownerCt.remove(this.queueNotification[i]);
                                        }
                                    }
                                    this.ownerCt.doLayout();
                                }
                            }
                        }, {
                            xtype: 'button',
                            text: 'Launch',
                            iconCls: 'x-button-play',
                            tooltip: 'Launch as a public course',
                            listeners: {
                                click: function () {
                                    Symphony.Portal.launchOnlineCourseStart(Symphony.User.id, me.courseId, 0);
                                }
                            }
                        }],
                        listeners: {
                            save: function () {
                                var form = me.find('xtype', 'form')[0];

                                if (!form.isValid()) { return false; }

                                var course = form.getValues();

                                Ext.apply(course, me.find('xtype', 'courseassignment.courseoverridepanel')[0].getValues());

                                if (!course.credit) {
                                    course.credit = 0;
                                }
                                if (!course.cost) {
                                    course.cost = 0;
                                }

                                var courseModeCombo = me.find('name', 'courseModeId')[0];
                                var courseModeId = courseModeCombo.getValue();

                                if (!courseModeId) {
                                    courseModeId = 0;
                                }

                                var courseMode = courseModeCombo.getStore().getAt(courseModeId);

                                course.metaDataJson = me.find('xtype', 'certificates.metadatapanel')[0].getData();

                                course.isSurvey = courseMode.get('isSurvey');
                                course.isTest = courseMode.get('isTest');

                                if (!course.artisanCourseId) {
                                    course.artisanCourseId = null;
                                } else {
                                    var panel = me.find('name', 'parameters')[0];
                                    if (panel) {
                                        course.onlineCourseParameterOverrides = panel.getParameterOverrides();
                                    } else {
                                        course.onlineCourseParameterOverrides = [];
                                    }
                                }

                                if (!course.validationInterval) {
                                    delete course.validationInterval;
                                }

                                course.duration = me.find('name', 'duration')[0].getValue();

                                if (!course.certificateTemplateId) {
                                    delete course.certificateTemplateId;
                                }

                                Symphony.Ajax.request({
                                    url: '/services/courseassignment.svc/courses/' + me.courseId,
                                    jsonData: course,
                                    success: function (result) {
                                        me.courseId = result.data.id;

                                        me.find('xtype', 'swfuploadbutton')[0].uploadAll(function () {
                                            me.load();

                                            me.fireEvent('save');
                                        });
                                    }
                                });
                            },
                            cancel: Ext.bind(function () { this.ownerCt.remove(this); }, this)
                        }
                    }]
                }, {
                    region: 'center',
                    //layout: 'fit',
                    defaults: {
                        border: false
                    },
                    xtype: 'tabpanel',
                    activeTab: 0,
                    listeners: {
                        beforeadd: function (panel, cmp, i) {
                            var type = cmp.getXType();

                            if (!Symphony.Artisan.canOverrideParameters() &&
                                type === 'courseassignment.onlinecourseparameterpanel') {
                                return false;
                            }

                            if (!Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards) && type === 'messageboard.app') {
                                return false;
                            }
                        }
                    },
                    items: [{
                        border: false,
                        bodyStyle: 'padding: 15px',
                        autoScroll: true,
                        title: 'General',
                        items: [{
                            name: 'general',
                            xtype: 'form',
                            border: false,
                            frame: true,
                            items: [{
                                xtype: 'fieldset',
                                title: 'Basic Information',
                                items: [{
                                    layout: 'column',
                                    border: false,
                                    cls: 'x-panel-transparent',
                                    // column defaults
                                    defaults: {
                                        columnWidth: 0.5,
                                        border: false,
                                        xtype: 'panel',
                                        layout: 'form',
                                        cls: 'x-panel-transparent'
                                    },
                                    items: [{
                                        //left column
                                        defaults: { anchor: '100%', xtype: 'textfield' },
                                        items: [{
                                            name: 'proctorRequiredIndicator',
                                            fieldLabel: 'Require Proctor',
                                            xtype: 'checkbox',
                                            hidden: true, // currently this can only be set on a final within the TP management. 
                                            cls: Symphony.Modules.getModuleClass(Symphony.Modules.Proctor)
                                        }, {
                                            name: 'publicIndicator',
                                            fieldLabel: 'Public',
                                            xtype: 'checkbox'
                                        }, {
                                            name: 'name',
                                            fieldLabel: 'Name',
                                            allowBlank: false
                                        }, {
                                            name: 'internalCode',
                                            fieldLabel: 'Internal Code'
                                        }, {
                                            xtype: 'hidden',
                                            name: 'certificatePath'
                                        }, {
                                            xtype: 'hidden',
                                            name: 'certificateTemplateId'
                                        }, {
                                            xtype: 'compositefield',
                                            anchor: '100%',
                                            fieldLabel: 'Certificate',
                                            name: 'certificateCompositeField',
                                            formItemCls: Symphony.Modules.getModuleClass(Symphony.Modules.Certificates),
                                            items: [{
                                                xtype: 'textfield',
                                                valueField: 'id',
                                                displayField: 'name',
                                                allowBlank: true,
                                                flex: 1,
                                                emptyText: 'Select a certificate...',
                                                valueNotFoundText: 'Select a certificate...',
                                                readOnly: true
                                            }, {
                                                xtype: 'button',
                                                text: '...',
                                                handler: function (btn) {
                                                    var w = Symphony.Certificates.getCertificateEditorWindow(function (node) {
                                                        var certificateText = node.attributes.record.get('levelIndentText');
                                                        me.find('name', 'certificateCompositeField')[0].items.items[0].setValue(certificateText);

                                                        var certificateTemplateId = node.attributes.record.get('certificateTemplateId');
                                                        if (certificateTemplateId) {
                                                            me.find('name', 'certificateTemplateId')[0].setValue(certificateTemplateId);
                                                            // in the event there was a OLDE cert type, blow away the path value
                                                            me.find('name', 'certificatePath')[0].setValue("");
                                                        }
                                                        else {
                                                            var certificatePath = node.attributes.record.get('path');
                                                            me.find('name', 'certificatePath')[0].setValue(certificatePath);
                                                            // in the event there was a new cert type, blow away the id
                                                            me.find('name', 'certificateTemplateId')[0].setValue(null);
                                                        }
                                                        w.close();
                                                    }, me.find('name', 'certificatePath')[0].getValue()).show();
                                                }
                                            }]
                                        }, {
                                            xtype: 'compositefield',
                                            anchor: '100%',
                                            fieldLabel: 'Category',
                                            name: 'categoryCompositeField',
                                            items: [{
                                                xtype: 'textfield',
                                                valueField: 'id',
                                                displayField: 'name',
                                                allowBlank: false,
                                                flex: 1,
                                                emptyText: 'Select a category...',
                                                valueNotFoundText: 'Select a category...',
                                                readOnly: true
                                            }, {
                                                xtype: 'button',
                                                text: '...',
                                                handler: function (btn) {
                                                    var w = Symphony.Categories.getCategoryEditorWindow('online', function (node) {
                                                        var categoryText = node.attributes.record.get('levelIndentText');
                                                        var categoryId = node.attributes.record.get('id');
                                                        me.find('name', 'categoryCompositeField')[0].items.items[0].setValue(categoryText);
                                                        me.find('name', 'categoryId')[0].setValue(categoryId);
                                                        w.close();
                                                    }, me.find('name', 'categoryId')[0].getValue()).show();
                                                }
                                            }]
                                        }, {
                                            xtype: 'fieldcontainer',
                                            fieldLabel: 'Secondary Categories',
                                            cls: Symphony.Modules.getModuleClass(Symphony.Modules.Libraries),
                                            items: [{
                                                name: 'secondaryCategoryList',
                                                xtype: 'courseassignment.secondarycategorieslist',
                                                clicksToEdit: 1,
                                                data: {},
                                                height: 105
                                            }]
                                        }] //, Symphony.getCombo('retestMode', 'Re-Test Mode', Symphony.getRetestModeStore())]
                                    }, {
                                        defaults: { anchor: '100%', xtype: 'textfield' },
                                        items: [{
                                            name: 'artisanCourseId',
                                            fieldLabel: '&nbsp;&nbsp;Artisan Course',
                                            xtype: 'displayfield',
                                            anchor: '0'
                                        },
                                            Symphony.getCombo('courseModeId', '&nbsp;&nbsp;Course Mode', me.courseModeStore),
                                        {
                                            name: 'cost',
                                            fieldLabel: '&nbsp;&nbsp;Cost',
                                            xtype: 'moneyfield'
                                        }, {
                                            name: 'credit',
                                            fieldLabel: '&nbsp;&nbsp;Credit',
                                            xtype: 'numberfield',
                                            decimalPrecision: 1,
                                            minValue: 0
                                        }, {
                                            name: 'isUseAutomaticDuration',
                                            xtype: 'checkbox',
                                            fieldLabel: '&nbsp;&nbsp;Duration',
                                            boxLabel: 'Use duration automatically calculated for this course by Artisan.',
                                            help: {
                                                title: 'Duration',
                                                text: 'The expected minimum amount of time it will take a student to complete the course. This is used when calculating how far a student has progressed through a training program. If the duration is automatically calculated, the minimum timers set on the course are used.'
                                            },
                                            listeners: {
                                                check: function (chkbox, value) {
                                                    var durationField = me.find('name', 'duration')[0];
                                                    var automaticDurationField = me.find('name', 'automaticDuration')[0];

                                                    if (value) {
                                                        durationField.setVisible(false);
                                                        automaticDurationField.setVisible(true);
                                                    } else {
                                                        durationField.setVisible(true);
                                                        automaticDurationField.setVisible(false);
                                                    }
                                                }
                                            }
                                        }, {
                                            name: 'duration',
                                            xtype: 'classroom.durationfield',
                                            fieldLabel: ' ',
                                            enableKeyEvents: true,
                                            labelConnector: '',
                                            labelSeparator: ''
                                            //itemCls: 'hide-label'
                                        }, {
                                            name: 'automaticDuration',
                                            xtype: 'classroom.durationfield',
                                            fieldLabel: ' ',
                                            enableKeyEvents: true,
                                            labelConnector: '',
                                            labelSeparator: '',
                                            //itemCls: 'hide-label',
                                            hidden: true,
                                            disabled: true
                                        }, {
                                            name: 'associatedImageData',
                                            fieldLabel: '&nbsp;&nbsp;Image',
                                            xtype: 'associatedimagefield'
                                        }, {
                                            xtype: 'fieldcontainer',
                                            fieldLabel: '&nbsp;&nbsp;Authors',
                                            cls: Symphony.Modules.getModuleClass(Symphony.Modules.Books),
                                            items: {
                                                xtype: 'courseassignment.authorslist',
                                                name: 'authorList',
                                                data: {},
                                                height: 105
                                            }
                                        }]
                                    }]
                                }, {
                                    xtype: 'hidden',
                                    name: 'categoryId'
                                }, {
                                    name: 'description',
                                    fieldLabel: 'Description',
                                    xtype: 'textarea',
                                    anchor: '0',
                                    allowBlank: false
                                }, {
                                    name: 'keywords',
                                    fieldLabel: 'Keywords',
                                    xtype: 'textarea',
                                    anchor: '0'
                                }, {
                                    layout: 'hbox',
                                    xtype: 'panel',
                                    border: false,
                                    cls: 'x-panel-transparent ' + Symphony.Modules.getModuleClass(Symphony.Modules.Validation),
                                    defaults: {
                                        cls: 'x-panel-transparent'
                                    },
                                    items: [{
                                        xtype: 'panel',
                                        layout: 'form',
                                        border: false,
                                        flex: 1,
                                        items: [{
                                            xtype: 'checkbox',
                                            fieldLabel: 'Validation',
                                            name: 'isValidationEnabled',
                                            boxLabel: 'If enabled, security questions will be asked  at the interval specified (in seconds).',
                                            listeners: {
                                                check: function (cbo, checked) {
                                                    if (!checked) {
                                                        me.find('name', 'validationInterval')[0].setDisabled(true);
                                                    } else {
                                                        me.find('name', 'validationInterval')[0].setDisabled(false);
                                                        if (me.showValidationAlert) {
                                                            Ext.Msg.alert('User Validation', 'Enabling user validation requires that each user has a date of birth, and a social security number set as these will be used to identifiy the current user taking the course.');
                                                        }
                                                    }
                                                }
                                            }
                                        }]
                                    }, {
                                        xtype: 'panel',
                                        layout: 'form',
                                        border: false,
                                        flex: 1,
                                        items: [{
                                            xtype: 'numberfield',
                                            fieldLabel: '&nbsp;&nbsp;Interval',
                                            name: 'validationInterval',
                                            anchor: '100%'
                                        }]
                                    }]
                                }, {
                                    xtype: 'courseassignment.accreditationlist',
                                    itemId: 'accreditation-list'
                                }]
                            }]
                        }]
                    }, {
                        bodyStyle: 'padding: 15px',
                        xtype: 'courseassignment.courseoverridepanel'
                    }, {
                        title: 'Parameters',
                        name: 'parameters',
                        layout: 'fit',
                        border: false,
                        bodyStyle: 'padding: 15px',
                        xtype: 'courseassignment.onlinecourseparameterpanel',
                        isShared: this.isShared,
                        disabled: true,
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Parameters),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.Parameters)
                    }, {
                        title: 'Message Board',
                        name: 'messageboard',
                        xtype: 'messageboard.app',
                        disabled: true,
                        isLockBySharedTrainingFlag: this.isShared,
                        showForumGrid: false,
                        border: true,
                        listeners: {
                            activate: {
                                fn: function (panel) {
                                    var onlineCourse = me.find('name', 'general')[0].getValues();
                                    panel.loadMessageBoardByClassId(Symphony.MessageBoardType.online, me.courseId, onlineCourse.name, 0, null, true);
                                },
                                single: true
                            }
                        }
                    }, {
                        xtype: 'certificates.metadatapanel',
                        isLockBySharedTrainingFlag: this.isShared,
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Certificates),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.Certificates)
                    }]
                }]
            });
            this.callParent(arguments);
        },
        afterRender: function () {
            Symphony.CourseAssignment.CourseForm.superclass.afterRender.apply(this, arguments);
            this.load();

            // any time the settings window is closed, we need to reload
            var me = this;
            Symphony.on('settings.closed', function () {
                me.load();
            });
        },
        load: function () {
            //TODO: load the data optionally
            var me = this;
            if (this.courseId) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/courseassignment.svc/courses/' + this.courseId,
                    success: function (result) {
                        me.bind(result.data);

                        if (result.data.isUseAutomaticDuration) {
                            me.find('name', 'duration')[0].setVisible(false);
                        } else {
                            me.find('name', 'automaticDuration')[0].setVisible(false);
                        }

                        me.showValidationAlert = !result.data.isValidationEnabled;
                        me.find('name', 'validationInterval')[0].setDisabled(!result.data.isValidationEnabled);
                        me.find('xtype', 'courseassignment.courseoverridepanel')[0].loadData(result.data);
                        me.find('xtype', 'certificates.metadatapanel')[0].setData(result.data);
                        me.find('name', 'secondaryCategoryList')[0].loadData(result.data.secondaryCategories);
                        me.find('name', 'authorList')[0].loadData(result.data.authors);

                        var messageBoard = me.find('xtype', 'messageboard.app')[0];
                        if (messageBoard) {
                            messageBoard.setDisabled(false);
                        }

                        if (result.data.categoryId) {
                            me.find('name', 'categoryCompositeField')[0].items.items[0].setValue(result.data.levelIndentText);
                        }

                        // new cert trumps old: 
                        if (result.data.certificateTemplateId) {
                            // get the certificate details: 
                            Symphony.Ajax.request({
                                url: '/services/certificate.svc/certificateTemplates/' + result.data.certificateTemplateId,
                                method: "GET",
                                success: function (res) {
                                    console.log(res.data);
                                    me.find('name', 'certificateCompositeField')[0].items.items[0].setValue(res.data.name);
                                },
                                failure: function () {
                                }
                            });
                        } else if (result.data.certificatePath) {
                            me.find('name', 'certificateCompositeField')[0].items.items[0].setValue(Symphony.Certificates.pathToLevelIndentText(result.data.certificatePath));
                        }

                        var parameterTab = me.find('name', 'parameters')[0];
                        if (result.data.artisanCourseId && result.data.hasCourseParameters) {
                            if (parameterTab) {
                                parameterTab.artisanCourseId = result.data.artisanCourseId;
                                parameterTab.onlineCourseOverrides = result.data.onlineCourseParameterOverrides;
                                parameterTab.setDisabled(false);
                            }
                        } else {
                            parameterTab.setDisabled(true);
                        }

                        me.queryById('accreditation-list').setAccreditations(result.data.accreditations);
                    }
                });


            }
        },
        bind: function (data) {
            this.courseId = data.id;

            if (!data.isSurvey) {
                data.isSurvey = false;
            }

            if (!data.isTest) {
                data.isTest = false;
            }

            var courseModeId = this.courseModeStore.findBy(function (record, id) {
                if (record.get('isSurvey') === data.isSurvey && record.get('isTest') === data.isTest) {
                    return true;
                }
                return false;
            });

            if (courseModeId > -1) {
                data.courseModeId = courseModeId
            } else {
                data.courseModeId = 0;
            }

            // load up the course details
            this.find('name', 'general')[0].bindValues(data);

            this.setTitle(data.name);
            this.setTabTip(data.description);
        }
    });


    Symphony.CourseAssignment.PublicDocumentForm = Ext.define('courseassignment.publicdocumentform', {
        alias: 'widget.courseassignment.publicdocumentform',
        extend: 'Ext.Panel',
        publicDocumentId: 0,
        initComponent: function () {
            var me = this;
            var categoriesUrl = '/services/courseassignment.svc/publicdocumentcategories/';
            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 30,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        items: [{
                            xtype: 'swfuploadbutton',
                            uploadUrl: '/Uploaders/PublicDocumentUploader.ashx',
                            name: 'uploader',
                            text: 'Update',
                            iconCls: 'x-button-upload',
                            fileTypes: '*.*',
                            fileTypesDescription: 'All Files',
                            fileQueueLimit: 1,
                            listeners: {
                                'queue': function (file) {
                                    if (!this.textAdded) {
                                        this.addParameter('publicDocumentId', me.publicDocumentId);
                                        this.addParameter('userId', Symphony.User.id);
                                        this.addParameter('customerId', Symphony.User.customerId);
                                        this.queueNotification = [
                                            new Ext.Toolbar.Separator(),
                                            new Ext.Toolbar.TextItem('<span style="font-style:italic">The file has been queued and will be uploaded when you save this document.</span>')
                                        ];
                                        this.ownerCt.add(this.queueNotification);
                                        this.ownerCt.doLayout();
                                        this.textAdded = true;
                                    }
                                },
                                'queueerror': function () {
                                    Ext.Msg.alert('Error', 'The file could not be queued');
                                },
                                'uploadstart': function (item) {
                                    Ext.Msg.progress('Please wait...', 'Please wait while your file is uploaded...');
                                },
                                'uploadprogress': function (item, completed, total) {
                                    var ratio = parseFloat(completed / total);
                                    Ext.Msg.updateProgress(ratio, parseInt(ratio * 100, 10) + "% complete");
                                },
                                'uploaderror': function () {
                                    this.error = true;
                                    Ext.Msg.hide();
                                    Ext.Msg.alert('Upload failed', 'The upload failed. Please make sure it is within the proper file size limitations.');
                                },
                                'allcomplete': function () {
                                    if (!this.error) {
                                        Ext.Msg.hide();
                                    }
                                    this.error = false;

                                    // always remove the queue notification
                                    if (this.queueNotification) {
                                        for (var i = 0; i < this.queueNotification.length; i++) {
                                            this.ownerCt.remove(this.queueNotification[i]);
                                        }
                                    }
                                    this.ownerCt.doLayout();
                                }
                            }
                        }, {
                            xtype: 'button',
                            text: 'Download',
                            iconCls: 'x-button-download',
                            handler: function () {
                                window.open("/services/courseassignment.svc/publicdocuments/binary/{0}".format(me.publicDocumentId));
                            }
                        }],
                        listeners: {
                            save: function () {
                                var form = me.find('xtype', 'form')[0];

                                if (!form.isValid()) { return false; }

                                var publicDocument = form.getValues();

                                Symphony.Ajax.request({
                                    url: '/services/courseassignment.svc/publicdocuments/' + me.courseId,
                                    jsonData: publicDocument,
                                    success: function (result) {
                                        me.courseId = result.data.id;

                                        me.find('xtype', 'swfuploadbutton')[0].uploadAll(function () {
                                            me.bind(result.data);

                                            me.fireEvent('save');
                                        });
                                    }
                                });

                            },
                            cancel: Ext.bind(function () { this.ownerCt.remove(this); }, this)
                        }
                    }]
                }, {
                    region: 'center',
                    layout: 'fit',
                    defaults: {
                        border: false
                    },
                    items: [{
                        border: false,
                        bodyStyle: 'padding: 15px',
                        items: [{
                            name: 'general',
                            xtype: 'form',
                            border: false,
                            frame: true,
                            items: [{
                                xtype: 'compositefield',
                                anchor: '100%',
                                fieldLabel: 'Category',
                                name: 'categoryCompositeField',
                                items: [{
                                    name: 'categoryId',
                                    xtype: 'symphony.pagedcombobox',
                                    url: categoriesUrl,
                                    model: 'category',
                                    bindingName: 'categoryName',
                                    ref: 'combo',
                                    valueField: 'id',
                                    displayField: 'name',
                                    allowBlank: false,
                                    flex: 1,
                                    emptyText: 'Select a category...',
                                    valueNotFoundText: 'Select a category...'
                                }, {
                                    xtype: 'button',
                                    text: '...',
                                    handler: function (btn) {
                                        Symphony.EditNameDescriptionPairs("Manage Public Document Categories", categoriesUrl, function (result) {
                                            var composite = me.find('name', 'categoryCompositeField')[0];
                                            var combo = composite.items.itemAt(0);
                                            // refresh the list if it's already been loaded
                                            combo.store.load();
                                            combo.store.on('load', function () {
                                                // reset the combobox
                                                for (var i = 0; i < result.data.length; i++) {
                                                    if (result.data[i].id == combo.getValue()) {
                                                        combo.valueNotFoundText = result.data[i].name;
                                                        combo.setValue(0);
                                                        combo.setValue(result.data[i].id);
                                                    }
                                                }
                                            }, this, { single: true });
                                        });
                                    }
                                }]
                            }, {
                                name: 'name',
                                fieldLabel: 'Name',
                                xtype: 'textfield',
                                anchor: '0',
                                allowBlank: false
                            }, {
                                name: 'description',
                                fieldLabel: 'Description',
                                xtype: 'textarea',
                                anchor: '0',
                                allowBlank: false
                            }]
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        onRender: function () {
            Symphony.CourseAssignment.PublicDocumentForm.superclass.onRender.apply(this, arguments);
            this.load();
        },
        load: function () {
            //TODO: load the data optionally
            var me = this;
            if (this.publicDocumentId) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/courseassignment.svc/publicdocuments/' + this.publicDocumentId,
                    success: function (result) {
                        me.bind(result.data);

                        if (!result.data.categoryId) {
                            var cf = me.find('name', 'categoryCompositeField')[0];
                            cf.items.itemAt(0).setValue(-1, true, true);
                        }
                    }
                });
            }
        },
        bind: function (data, ignoreDirty) {
            this.courseId = data.id;
            // load up the course details
            this.find('name', 'general')[0].bindValues(data, ignoreDirty);
            this.setTitle(data.name);
            this.setTabTip(data.description);
        }
    });




    Symphony.CourseAssignment.NotificationTemplateForm = Ext.define('courseassignment.notificationtemplateform', {
        alias: 'widget.courseassignment.notificationtemplateform',
        extend: 'Ext.Panel',
        //notificationTemplateId: 0,
        MINS_PER_DAY: 60 * 24,
        MINS_PER_HOUR: 60,
        scheduleParamOptionCls: "schedule-param-option",
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 30,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        listeners: {
                            save: function () {
                                // first, trigger a previou, which will force the text to parse correctly
                                me.preview(function () {
                                    return me.save(false);
                                });
                            },
                            cancel: Ext.bind(function () { this.ownerCt.remove(this); }, this)
                        },
                        items: [{
                            text: 'History',
                            iconCls: 'x-button-history',
                            handler: function () {
                                if (!this.win) {
                                    this.win = new Ext.Window({
                                        title: 'Template History',
                                        height: 520,
                                        width: 700,
                                        modal: true,
                                        layout: 'fit',
                                        closeAction: 'hide',
                                        border: false,
                                        autoScroll: true,
                                        items: [{
                                            xtype: 'courseassignment.templatehistorygrid',
                                            notificationTemplateId: me.notificationTemplateId
                                        }]
                                    });
                                }
                                this.win.show();
                            }
                        }, {
                            text: 'Preview',
                            iconCls: 'x-button-preview',
                            handler: function () {
                                me.preview();
                            }
                        }, {
                            text: 'Template Help',
                            iconCls: 'x-button-help',
                            handler: function () {
                                if (!this.win) {
                                    this.win = new Ext.Window({
                                        cls: 'pure-text',
                                        title: 'Template Help',
                                        height: 520,
                                        width: 610,
                                        modal: false,
                                        autoScroll: true,
                                        closeAction: 'hide',
                                        autoLoad: '/scripts/template_help.html'
                                    });
                                }
                                this.win.show();
                            }
                        }]
                    }]
                }, {
                    region: 'center',
                    bodyStyle: 'padding: 15px',
                    layout: 'fit',
                    items: [{
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        onResize: function () {
                            Ext.Panel.prototype.onResize.apply(this, arguments);
                            this.items.each(function (i) {
                                i.setHeight(this.body.getHeight(true));
                            }, this);
                        },
                        frame: true,
                        border: false,
                        // column defaults
                        defaults: {
                            border: false,
                            xtype: 'form',
                            name: 'general'
                        },
                        items: [{
                            xtype: 'form',
                            // left column
                            flex: 0.65,
                            autoScroll: true,
                            bodyStyle: 'padding-right:20px;',
                            cls: 'x-panel-transparent',
                            defaults: {
                                cls: 'x-panel-transparent'
                            },
                            items: [{
                                defaults: { anchor: '100%', xtype: 'textfield' },
                                xtype: 'fieldset',
                                title: 'Basic Information',
                                items: [{
                                    name: 'displayName',
                                    fieldLabel: 'Name',
                                    allowBlank: false
                                }, {
                                    name: 'description',
                                    fieldLabel: 'Description'
                                }, {
                                    name: 'enabled',
                                    fieldLabel: 'Enabled',
                                    xtype: 'checkbox',
                                    // Only enabled if this message can be assigned to a training program
                                    trainingProgramBoxLabel: 'Check to enable this message for all training programs with custom messages disabled. Leave unchecked if this message applies to specific training programs only.'
                                }, {
                                    name: 'includeCalendarInvite',
                                    fieldLabel: 'Calendar Invite',
                                    xtype: 'checkbox'
                                }]
                            }, {
                                defaults: { anchor: '100%', xtype: 'textfield' },
                                xtype: 'fieldset',
                                title: 'Message Template',
                                items: [{
                                    xtype: 'compositefield',
                                    fieldLabel: 'Subject',
                                    allowBlank: false,
                                    items: [{
                                        xtype: 'textfield',
                                        name: 'subject',
                                        flex: 1
                                        /*listeners: {
                                            render: Ext.bind(this.monitorDrag, this)
                                        }*/
                                    }, {
                                        xtype: 'button',
                                        text: '<< Add',
                                        handler: function () {
                                            me.addTemplateParameter(this.ownerCt.find('name', 'subject')[0]);
                                        }
                                    }]
                                }, {
                                    xtype: 'compositefield',
                                    fieldLabel: 'Body',
                                    allowBlank: false,
                                    items: [{
                                        xtype: 'htmleditor',
                                        itemId: 'bodyEditor',
                                        name: 'body',
                                        height: 100,
                                        flex: 1,
                                        listeners: {
                                            //render: Ext.bind(this.monitorDrag, this)
                                        },

                                        // hack to fix the flex layout for html editor
                                        // http://www.sencha.com/forum/showthread.php?105861-OPEN-1169-Flex-options-is-ignored-for-HtmlEditor-in-vbox-layout
                                        onRender: function () {
                                            Ext.form.HtmlEditor.prototype.onRender.apply(this, arguments);
                                            delete this.height;
                                            delete this.width;
                                        }
                                    }, {
                                        xtype: 'button',
                                        text: '<< Add',
                                        handler: function () {
                                            me.addTemplateParameter(this.ownerCt.find('name', 'body')[0]);
                                        }
                                    }]
                                }, {
                                    xtype: 'fieldcontainer',
                                    fieldLabel: 'Recipient Params',
                                    defaults: {
                                        xtype: 'button',
                                        iconCls: 'x-button-add'
                                    },
                                    items: [{
                                        text: 'First Name',
                                        handler: function () {
                                            me.addTemplateParameter(me.query('[itemId=bodyEditor]')[0], '##FirstName##');
                                        }
                                    }, {
                                        text: 'Middle Name',
                                        handler: function () {
                                            me.addTemplateParameter(me.query('[itemId=bodyEditor]')[0], '##MiddleName##');
                                        }
                                    }, {
                                        text: 'Last Name',
                                        handler: function () {
                                            me.addTemplateParameter(me.query('[itemId=bodyEditor]')[0], '##LastName##');
                                        }
                                    }, {
                                        text: 'Username',
                                        handler: function () {
                                            me.addTemplateParameter(me.query('[itemId=bodyEditor]')[0], '##Username##');
                                        }
                                    }]
                                }]
                            }, {
                                defaults: { anchor: '100%', xtype: 'checkbox' },
                                xtype: 'fieldset',
                                name: 'recipientgroups',
                                title: 'The following users will receive these messages:',
                                items: [] // these are added dynamically
                            }, {
                                xtype: 'fieldset',
                                name: 'ccgroups',
                                title: 'The following users will be CCed:',
                                layout: {
                                    type: 'hbox'
                                },
                                items: [{
                                    xtype: 'checkbox',
                                    name: 'ccClassroomSupervisors',
                                    boxLabel: "All Classroom Supervisors"
                                }, {
                                    xtype: 'checkbox',
                                    name: 'ccReportingSupervisors',
                                    boxLabel: "All Reporting Supervisors",
                                    style: {
                                        marginLeft: '8px'
                                    }
                                }]
                            }, {
                                xtype: 'fieldset',
                                name: 'scheduleowner',
                                title: 'The message will be sent by:',
                                items: [{
                                    anchor: '100%',
                                    name: 'ownerId',
                                    bindingName: 'ownerName',
                                    displayField: 'fullName',
                                    valueField: 'id',
                                    fieldLabel: 'Owner',
                                    xtype: 'symphony.pagedcombobox',
                                    blankValueTitle: '[None]',
                                    emptyText: '[None]',
                                    model: 'user',
                                    clearable: true,
                                    //url: '/services/portal.svc/users/',
                                    url: '/Services/customer.svc/users/customer/{0}'.format(Symphony.User.customerId),
                                    allowBlank: true
                                }]
                            }, {
                                defaults: { anchor: '100%' },
                                xtype: 'fieldset',
                                name: 'scheduleparameters',
                                title: 'The message will be sent:',
                                hidden: false,
                                items: [] // these are added dynamically
                            }, {
                                xtype: 'fieldset',
                                name: 'trainingprograms',
                                title: 'Assigned to Training Programs',
                                padding: 7,
                                help: {
                                    title: 'Assigned to Training Programs',
                                    text: 'You can assign this message to a training program using the Notifications tab when editing a training program. Checking the enabled field above will enable this message for all training programs.'
                                },
                                items: [{
                                    xtype: 'courseassignment.templateassignedtrainingprogramsgrid',
                                    templateId: me.notificationTemplateId
                                }]
                            }]
                        }, {
                            // right column
                            flex: 0.35,
                            style: 'margin-left: 10px;padding:5px',
                            defaults: { anchor: '100% 100%' },
                            xtype: 'fieldset',
                            cls: 'template-parameters',
                            name: 'templateparameters',
                            border: true,
                            title: 'Template Parameters',
                            items: [{
                                xtype: 'symphony.templateparametertree',
                                itemId: 'templateparametertree'
                            }]
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        preview: function (callback) {
            var me = this;
            var editor = me.find('name', 'general')[0];
            var scheduleParameter = me.find('name', 'scheduleparameter')[0];
            var scheduleParameterId = 0;
            if (scheduleParameter) {
                scheduleParameterId = scheduleParameter.getValue();
            }
            var subject = editor.findField('subject').getValue();
            var body = editor.findField('body').getValue();
            var text = '<b>Subject:</b> ' + subject + '<br/><br/>' + '<b>Body:</b> ' + body;
            Symphony.Ajax.request({
                url: '/services/courseassignment.svc/previewnotificationtemplate/' + me.notificationTemplateId + '/' + scheduleParameterId,
                jsonData: Ext.encode(text),
                success: function (result) {
                    if (callback) {
                        callback();
                    } else {
                        var w = Ext.create('Ext.Window', {
                            width: 500,
                            height: 400,
                            layout: 'fit',
                            title: 'Preview',
                            modal: true,
                            items: {
                                xtype: 'panel',
                                bodyPadding: 10,
                                html: result.data,
                                border: false,
                                autoScroll: true
                            }
                        });
                        w.show();
                    }
                }
            });
        },
        /*monitorDrag: function (field) {
            var me = this;
            var dropTarget = new Ext.dd.DropTarget(field.el, {
                ddGroup: 'templateparameter',
                copy: false,
                overClass: 'over',
                notifyDrop: function (dragSource, event, data) {
                    me.addTemplateParameter(field);
                    event.preventDefault();
                }
            });
        },*/
        afterRender: function () {
            Symphony.CourseAssignment.NotificationTemplateForm.superclass.afterRender.apply(this, arguments);
            this.load();
        },
        load: function () {

            var me = this;
            if (this.notificationTemplateId) {
                if (!this.templateData) {
                    Ext.Msg.wait('Please wait...', 'Loading...');
                    Symphony.Ajax.request({
                        method: 'GET',
                        url: '/services/courseassignment.svc/notificationtemplates/' + this.notificationTemplateId,
                        success: function (result) {
                            me.processData(result.data);
                            Ext.Msg.hide();
                        }
                    });
                } else {
                    me.templateData.Enabled = false;
                    me.templateData.isUserCreated = true;
                    me.templateData.id = 0;
                    me.templateData.displayName += " (User Created)";

                    me.notificationTemplateId = 0;

                    me.processData(me.templateData);
                }
            } else {
                me.loadScheduleParameters(function () {
                    // add the schedule parameters
                    var p = Symphony.CourseAssignment.scheduleParameters;
                    me.displayScheduleParameters({ isScheduled: true, scheduleParameterId: p[0].id });

                    me.displayTemplateParameters(p[0].validTemplateParameters);
                    me.displayRecipientGroups({}, p[0].possibleRecipients, []);
                });
            }
        },
        processData: function(data) {
            // cache the result
            this.template = data;

            // load up the appropriate panels with the results
            this.notificationTemplateId = data.id;

            // set the basic information
            this.bindForm(data);

            // add the template parameters
            this.displayTemplateParameters(data.validTemplateParameters);

            // add the recipient groups
            this.displayRecipientGroups(data, data.possibleRecipients, data.selectedRecipients);

            // add the schedule parameters
            this.displayScheduleParameters(data);

            // disable the display name and description if this is a system message
            var form = this.find('name', 'general')[0];
            if (data.isScheduled) {
                form.findField('includeCalendarInvite').hide();
            } else {
                form.findField('displayName').disable();
                form.findField('description').disable();
            }

            if (data.isUserCreated && !data.isScheduled) {
                this.find('name', 'enabled')[0].hide();
                form.findField('displayName').setDisabled(false);
            }

            this.updateTrainingProgramGrid(data);
        },
        bindForm: function (data) {
            // first, bind the basic information
            this.find('name', 'general')[0].bindValues(data);

        },
        save: function () {
            var me = this;

            var form = me.find('name', 'general')[0];

            if (!form.isValid()) { return false; }

            // basic info
            var template = form.getValues();

            // New items can be schedule or system notifications now
            template.isScheduled = me.template ? me.template.isScheduled : true;

            if (!me.notificationTemplateId && !template.isScheduled && me.template.isUserCreated) {
                template.codeName = me.template.codeName;
            }

            if (!template.ownerId) {
                template.ownerId = 0;
            }

            // recipients
            var checks = Ext.query('#' + this.body.dom.id + ' .recipient_group');
            template.selectedRecipients = [];
            Ext.each(checks, function (check) {
                if (check.checked) {
                    var idParts = check.id.split('_');
                    template.selectedRecipients.push({ id: idParts[idParts.length - 1] });
                }
            });

            // scheduling
            if (template.isScheduled) {
                var minutes = 0;
                var duration = me.find('name', 'scheduleduration')[0].getValue();
                var interval = me.find('name', 'scheduleinterval')[0].getValue();
                switch (interval) {
                    case 'minutes':
                        minutes = duration;
                        break;
                    case 'hours':
                        minutes = duration * me.MINS_PER_HOUR;
                        break;
                    case 'days':
                        minutes = duration * me.MINS_PER_DAY;
                        break;
                }

                var scheduleParameterCombo = me.find('name', 'scheduleparameter')[0];
                var scheduleParameterRecord = scheduleParameterCombo.store.getById(scheduleParameterCombo.getValue());
                var reference = me.find('name', 'schedulereference')[0].getValue();

                if (scheduleParameterRecord.get('scheduleParameterOption') == Symphony.ScheduleParameterOptions.afterOnly) {
                    reference = 'after';
                } else if (scheduleParameterRecord.get('scheduleParameterOption') == Symphony.ScheduleParameterOptions.percentage) {
                    minutes = 0;
                }

                if (reference == 'before') {
                    minutes = -minutes;
                }


                template.relativeScheduledMinutes = minutes;
                template.scheduleParameterId = scheduleParameterCombo.getValue();
                template.percentage = me.find('name', 'percentage')[0].getValue();

                if (!template.percentage) {
                    delete template.percentage;
                }
            }

            // save and process files
            Symphony.Ajax.request({
                url: '/services/courseassignment.svc/notificationtemplates/' + (me.notificationTemplateId || 0),
                jsonData: template,
                success: function (result) {
                    // cache the result
                    me.template = result.data;
                    me.notificationTemplateId = result.data.id;
                    me.setTitle(result.data.displayName);

                    // re-bind the form values, in case the name was modified by a duplication
                    me.bindForm(result.data);

                    // then display scheduling
                    me.displayScheduleParameters(result.data);

                    me.updateTrainingProgramGrid(result.data);

                    me.fireEvent('save');
                }
            });
        },
        updateTrainingProgramGrid: function(data) {
            var trainingProgramGrid = this.query('[xtype=courseassignment.templateassignedtrainingprogramsgrid]')[0];
            var enabledChkbox = this.query('[name=enabled]')[0];

            if (data.isConfigurableForTrainingProgram || (data.scheduleParameter && data.scheduleParameter.isConfigurableForTrainingProgram)) {
                trainingProgramGrid.up().show();
                if (data.id > 0 && data.id != this.notificationTemplateId) {
                    trainingProgramGrid.setTemplateId(data.id);
                }
                enabledChkbox.setBoxLabel(enabledChkbox.trainingProgramBoxLabel);
            } else {
                trainingProgramGrid.up().hide();
                enabledChkbox.setBoxLabel('');
            }
        },
        displayRecipientGroups: function (template, possibleRecipients, selectedRecipients) {
            var me = this;
            var txt = '<span class="recipient_group_wrap" style="white-space:nowrap;line-height:20px" ext:qtip="{qtip}"><input class="recipient_group"  id="messages_group_{templateId}_{id}" type="checkbox" {checked}></input>&nbsp;<label for="messages_group_{templateId}_{id}">{name}</label></span> ';
            var isSelected = function (recipientGroup) {
                for (var i = 0; i < selectedRecipients.length; i++) {
                    if (selectedRecipients[i].id == recipientGroup.id) {
                        return true;
                    }
                }
                return false;
            };

            var exclude = {};
            /*if(template.codeName == 'NewClassScheduled'){
            // ugh. for this particular notification, it doesn't make
            // sense that we should send to the Registrants
            // (it's a new class...therefore there *are* no Registrants)
            // so make sure we don't include them. technically this applies to
            // the instructors too, but the verdict from above is leave those alone.
            exclude = { 'Registrants': true };
            }*/

            var results = [];
            Ext.each(possibleRecipients, function (recipientGroup) {
                if (!(recipientGroup.displayName in exclude)) {
                    results.push(txt
                        .replaceAll("{id}", recipientGroup.id)
                        .replaceAll("{templateId}", template.id)
                        .replace("{checked}", isSelected(recipientGroup) ? 'checked="checked"' : '')
                        .replace("{qtip}", me.getQtipForReceipientGroup(recipientGroup.displayName))
                        .replace("{name}", recipientGroup.displayName));
                }
            });
            this.find('name', 'recipientgroups')[0].update(results.join('&nbsp;&nbsp;'));
        },
        getQtipForReceipientGroup: function (recipientGroup) {
            switch (recipientGroup) {
                case 'All Registrants':
                    return 'The currently registered or registering users for the event.';
                case 'All Registrants Classroom Supervisors':
                    return 'The classroom supervisors for the currently registered or registering users for the event.';
                case 'All Registrants Reporting Supervisors':
                    return 'The reporting supervisors for the currently registered or registering users for the event.';
                case 'Registrant':
                    return 'The currently registered or registering user.';
                case 'Instructors':
                    return 'The instructors currently assigned to the class.';
                case 'All Registrants':
                    return '<b>All</b> the currently registered users.';
                case 'Resource Managers':
                    return 'The resources managers currently assigned to the class.';
                case 'CT Managers':
                    return 'All classroom training managers.';
                case 'Assigned Users':
                    return 'All users assigned to the training program.';
                case 'Invitee':
                    return 'The invited user.';
                case 'Classroom Supervisor':
                    return 'The classroom supervisor of the user referenced in the message.';
                case 'Reporting Supervisor':
                    return 'The reporting supervisor of the user referenced in the message.';
                case 'Customer Managers':
                    return 'All customer managers.';
                case 'User':
                    return 'The user the message is about.';
            }
            return '';
        },
        updateScheduleParameterOptions: function (record) {
            var scheduleParameterPanel = this.query('[name=scheduleparameters]')[0];

            var currentParameterOptionCls = this.scheduleParamOptionCls + '-' + record.get('scheduleParameterOption');
            var allOptions = scheduleParameterPanel.query('[cls~=' + this.scheduleParamOptionCls + ']');
            var currentParameterOptions = scheduleParameterPanel.query('[cls~=' + currentParameterOptionCls + ']');

            for (var i = 0; i < allOptions.length; i++) {
                allOptions[i].hide();
                allOptions[i].setDisabled(true);
            }
            for (var i = 0; i < currentParameterOptions.length; i++) {
                currentParameterOptions[i].show();
                currentParameterOptions[i].setDisabled(false);
            }

            this.updateTrainingProgramGrid({
                scheduleParameter: record.data
            });
        },
        displayScheduleParameters: function (template) {
            var me = this;
            var panel = me.find('name', 'scheduleparameters')[0];
            var owner = me.find('name', 'scheduleowner')[0];


            if (!template.isScheduled) {
                panel.hide();
                return;
            }

            var processScheduleParameters = function (result) {
                // looks like this in the UI:
                // [x (number)] [minutes/hours/days] [before/after] [schedule parameter]


                // we do a simple calc to figure out it we're talking mins/days/hours
                // basically, just see which one divides evenly
                var interval = 'minutes';
                var duration = (Math.abs(template.relativeScheduledMinutes) || 0);

                
                var beforeOrAfter = me.scheduleParamOptionCls + "-" + Symphony.ScheduleParameterOptions.beforeOrAfter;
                var percentage = me.scheduleParamOptionCls + "-" + Symphony.ScheduleParameterOptions.percentage;
                var afterOnly = me.scheduleParamOptionCls + "-" + Symphony.ScheduleParameterOptions.afterOnly;

                if (duration % me.MINS_PER_DAY == 0) {
                    interval = 'days';
                    duration = duration / me.MINS_PER_DAY;
                } else if (duration % me.MINS_PER_HOUR == 0) {
                    interval = 'hours';
                    duration = duration / me.MINS_PER_HOUR;
                }



                var durationField = panel.find('name', 'scheduleduration')[0] || new Ext.form.NumberField({
                    name: 'scheduleduration',
                    emptyText: 'Duration',
                    decimalPrecision: 0,
                    allowNegative: false,
                    cls: ['x-timespan', me.scheduleParamOptionCls,beforeOrAfter, afterOnly].join(' '),
                    hideLabel: true,
                    width: 60,
                    value: duration
                });
                durationField.setValue(duration);



                var intervalCombo = panel.find('name', 'scheduleinterval')[0] || new Ext.form.ComboBox({
                    name: 'scheduleinterval',
                    allowBlank: false,
                    editable: false,
                    cls: ['x-timeinterval', me.scheduleParamOptionCls, beforeOrAfter, afterOnly].join(' '),
                    listClass: 'x-menu x-timeinterval-list',
                    store: Symphony.quickStore(['minutes', 'hours', 'days']),
                    displayField: 'name',
                    valueField: 'id',
                    queryMode: 'local',
                    triggerAction: 'all',
                    hideLabel: true,
                    width: 70,
                    value: interval
                });
                intervalCombo.setValue(interval);

                var referenceCombo = panel.find('name', 'schedulereference')[0] || new Ext.form.ComboBox({
                    name: 'schedulereference',
                    allowBlank: false,
                    editable: false,
                    cls: ['x-timereference', me.scheduleParamOptionCls, beforeOrAfter].join(' '),
                    listClass: 'x-menu x-timereference-list',
                    store: Symphony.quickStore(['before', 'after']),
                    displayField: 'name',
                    valueField: 'id',
                    queryMode: 'local',
                    triggerAction: 'all',
                    hideLabel: true,
                    width: 70,
                    value: template.relativeScheduledMinutes < 0 ? 'before' : 'after'
                });
                referenceCombo.setValue(template.relativeScheduledMinutes < 0 ? 'before' : 'after');

                var percentCompleteTextField = panel.find('name', 'percentage')[0] || new Ext.form.NumberField({
                    name: 'percentage',
                    emptyText: '%',
                    decimalPrecision: 0,
                    allowNegative: false,
                    allowBlank: false,
                    cls: ['x-timespan', me.scheduleParamOptionCls, percentage].join(' '),
                    hideLabel: true,
                    width: 60,
                    value: template.percentage,
                    minValue: 1,
                    maxValue: 100
                });

                var afterField = panel.find('name', 'afterlabel')[0] || new Ext.form.Label({
                    name: 'after',
                    text: 'after',
                    width: 40,
                    padding: '4 0 0 8',
                    cls: ['after-label', me.scheduleParamOptionCls, afterOnly].join(' ')
                });

                // ok, the template is scheduled, so it gets the parameter options;
                // display the appropriate comboboxes, etc
                var scheduleArray = [];
                var canFilterComplete = false;
                Ext.each(result, function (parameter) {
                    scheduleArray.push([parameter.displayName, parameter.id, parameter.canFilterComplete, parameter.scheduleParameterOption, parameter.isConfigurableForTrainingProgram]);
                    if (parameter.id == template.scheduleParameterId) {
                        canFilterComplete = parameter.canFilterComplete;
                    }
                });
                var store = new Ext.data.ArrayStore({
                    fields: ['displayName', 'id', 'canFilterComplete', 'scheduleParameterOption', 'isConfigurableForTrainingProgram'],
                    data: scheduleArray
                });

                var parameterCombo = panel.find('name', 'scheduleparameter')[0] || new Ext.form.ComboBox({
                    name: 'scheduleparameter',
                    allowBlank: false,
                    editable: false,
                    cls: 'x-scheduleparameter',
                    listClass: 'x-menu x-scheduleparameter-list',
                    store: store,
                    displayField: 'displayName',
                    valueField: 'id',
                    queryMode: 'local',
                    triggerAction: 'all',
                    hideLabel: true,
                    flex: 1,
                    value: template.scheduleParameterId,
                    listeners: {
                        select: function (combo, records, index) {
                            var field = this.ownerCt.ownerCt.find('name', 'filterComplete')[0];
                            var record = records[0];

                            me.updateScheduleParameterOptions(record);

                            if (record.get('canFilterComplete')) {
                                field.enable();
                                field.setValue(field.previousValue);
                            } else {
                                field.disable();
                                field.previousValue = field.getValue();
                                field.setValue(false);
                            }

                            // based on this selection, we also want to change
                            // the available template parameters and the available users
                            Ext.each(Symphony.CourseAssignment.scheduleParameters, function (parameter) {
                                if (parameter.id == record.get('id')) {
                                    me.displayTemplateParameters(parameter.validTemplateParameters);
                                    me.displayRecipientGroups(me.template || {}, parameter.possibleRecipients, me.template ? me.template.selectedRecipients : []);
                                }
                            });
                        }
                    }
                });

                parameterCombo.setValue(template.scheduleParameterId);
                

                if (panel.items.length == 0) {
                    panel.add({
                        xtype: 'panel',
                        cls: 'x-panel-transparent scheduleparameters',
                        border: false,
                        layout: 'hbox',
                        items: [
                            durationField,
                            intervalCombo,
                            referenceCombo,
                            percentCompleteTextField,
                            afterField,
                            parameterCombo
                        ]
                    });
                    panel.add({
                        xtype: 'panel',
                        cls: 'x-panel-transparent',
                        border: false,
                        items: [{
                            xtype: 'checkbox',
                            name: 'filterComplete',
                            disabled: !canFilterComplete,
                            boxLabel: 'Only send to users who have not completed the assignment'
                        }]
                    });
                }
                panel.find('name', 'filterComplete')[0].setValue(template.filterComplete);
                panel.show();
                panel.doLayout();

                me.updateScheduleParameterOptions(parameterCombo.store.getById(template.scheduleParameterId));
            };

            me.loadScheduleParameters(processScheduleParameters);
        },
        loadScheduleParameters: function (callback) {
            // load the parameters if they haven't been loaded yet, then cache them
            if (!Symphony.CourseAssignment.scheduleParameters) {
                Symphony.Ajax.request({
                    url: '/services/courseassignment.svc/scheduleparameters/',
                    method: 'GET',
                    success: function (result) {
                        // cache since they don't change
                        Symphony.CourseAssignment.scheduleParameters = result.data;

                        // display the scheduling info
                        callback(Symphony.CourseAssignment.scheduleParameters);
                    }
                });
            } else {
                window.setTimeout(function () {
                    callback(Symphony.CourseAssignment.scheduleParameters);
                }, 1);
            }
        },
        displayTemplateParameters: function (data) {
            var templateParameters = this.queryById("templateparametertree");
            templateParameters.displayTemplateParameters(data);
        },
        addTemplateParameter: function (target, text) {
            var templateParameters = this.queryById("templateparametertree");
            templateParameters.addTemplateParameter(target, text);
        }
    });


    Symphony.CourseAssignment.TrainingProgramUsers = Ext.define('courseassignment.trainingprogramusers', {
        alias: 'widget.courseassignment.trainingprogramusers',
        extend: 'Ext.Panel',
        isDisabled: false,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                border: false,
                items: [{
                    xtype: 'courseassignment.userassignmentpanel',
                    name: 'primary',
                    border: false,
                    title: 'Primary Assignment',
                    listeners: {
                        groupchange: function (id, reset) {
                            me.find('name', 'secondary')[0].removeOption(id);
                            if (id != 0) {
                                me.find('name', 'secondary')[0].enable();
                            } else {
                                var secondary = me.find('name', 'secondary')[0];
                                if (reset) {
                                    secondary.reset();
                                    secondary.disable();
                                } else {
                                    secondary.disable();
                                }
                            }
                        }
                    },
                    flex: 0.45
                }, {
                    xtype: 'courseassignment.userassignmentpanel',
                    name: 'secondary',
                    border: false,
                    title: 'Secondary Assignment',
                    disabled: true,
                    listeners: {
                        groupchange: function (id, reset) {
                            me.find('name', 'primary')[0].removeOption(id);
                        }
                    },
                    flex: 0.45
                }]
            });
            this.callParent(arguments);
        },
        // sets the specified values for the assignments
        setAssignments: function (primary, secondary) {
            var pEl = this.find('name', 'primary')[0], sEl = this.find('name', 'secondary')[0];
            if (primary.length > 0) {
                var ptype = primary[0].hierarchyTypeId;
                var pids = Ext.pluck(primary, 'hierarchyNodeId');

                pEl.setSelections(ptype, pids);

                // remove the primary selection from the secondary selection options
                sEl.removeOption(ptype);
            }
            if (secondary.length > 0) {
                var stype = secondary[0].hierarchyTypeId;
                var sids = Ext.pluck(secondary, 'hierarchyNodeId');

                sEl.setSelections(stype, sids);
            }
            this.primaryAssignments = primary;
            this.secondaryAssignments = secondary;

            // don't allow secondary selection until a primary selection is made
            if (primary.length == 0) {
                sEl.disable();
            } else if (!this.isDisabled) {
                sEl.enable();
            }
        },
        getAssignments: function () {
            return {
                primaryAssignment: this.rendered ? this.find('name', 'primary')[0].getSelections() : this.primaryAssignments,
                secondaryAssignment: this.rendered ? this.find('name', 'secondary')[0].getSelections() : this.secondaryAssignments
            };
        },
        disable: function () {
            var pEl = this.find('name', 'primary')[0], sEl = this.find('name', 'secondary')[0];
            pEl.disable();
            sEl.disable();
            this.isDisabled = true;

        }
    });


    Symphony.CourseAssignment.UserAssignmentPanel = Ext.define('courseassignment.userassignmentpanel', {
        alias: 'widget.courseassignment.userassignmentpanel',
        extend: 'Ext.Panel',
        singleSelect: false,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                bodyStyle: me.bodyStyleOverride ? me.bodyStyleOverride : 'padding-right:5px',
                layout: 'anchor',
                items: [{
                    xtype: 'panel',
                    title: this.title,
                    anchor: '100% 100%',
                    layout: 'border',
                    height: 40,
                    bodyPadding: 5,
                    frame: me.frame === false ? false : true,
                    items: [{
                        region: 'north',
                        //xtype: 'compositefield',
                        layout: {
                            type: 'hbox',
                            defaultMargins: {
                                top: 0,
                                right: 5,
                                bottom: 0,
                                left: 0
                            }
                        },
                        cls: 'x-panel-transparent',
                        border: false,
                        items: [{
                            xtype: 'combo',
                            width: 225,
                            disabled: this.disabled,
                            name: 'usergroup',
                            triggerAction: 'all',
                            editable: false,
                            queryMode: 'local',
                            store: new Ext.data.ArrayStore({
                                editable: false,
                                fields: ['id', 'displayName'],
                                data: Symphony.HierarchyTypeStoreData
                            }),
                            valueField: 'id',
                            displayField: 'displayName',
                            emptyText: 'Select a Group',
                            listeners: {
                                beforeselect: function (combo, record, index) {
                                    var id = record.get('id');
                                    if (combo.getValue() > 0) {
                                        // if they have a selction already...
                                        Ext.Msg.confirm('Are you sure?', 'Changing the group will clear your current selections. Are you sure?', function (btn) {
                                            if (btn == 'yes') {
                                                combo.setValue(id);
                                                me.loadHierarchy(id);
                                                me.fireEvent('groupchange', id, false);
                                            }
                                        });
                                        combo.collapse();
                                        // cancel so we can confirm the action
                                        return false;
                                    } else {
                                        me.loadHierarchy(id);
                                        me.fireEvent('groupchange', id, false);
                                    }
                                }
                            }
                        }, {
                            xtype: 'button',
                            name: 'clear',
                            disabled: this.disabled,
                            text: 'Clear Selections',
                            handler: function () {
                                Ext.Msg.confirm('Are you sure?', 'This will clear your current selections. Are you sure?', function (btn) {
                                    if (btn == 'yes') {
                                        // reset all the selections for this grid/combobox
                                        me.clearSelections();
                                    }
                                });
                            }
                        }, {
                            xtype: 'button',
                            name: 'reset',
                            disabled: this.disabled,
                            text: 'Reset All',
                            handler: function () {
                                Ext.Msg.confirm('Are you sure?', 'This will reset all your selections, both primary and secondary. Are you sure?', function (btn) {
                                    if (btn == 'yes') {
                                        // reset all the selections for this grid/combobox
                                        me.reset();
                                        me.fireEvent('groupchange', 0, true);
                                    }
                                });
                            }
                        }]
                    }, {
                        // assignment panel goes here
                        xtype: 'panel',
                        region: 'center',
                        layout: 'fit',
                        name: 'assignmentpanel',
                        cls: 'x-panel-transparent',
                        border: false
                    }]
                }]
            });
            // reset the title after we're done, we don't want this set by the sub-elements
            this.title = '';
            this.callParent(arguments);
        },
        clearSelections: function () {
            if (this.currentPanel) {
                this.currentPanel.clearSelections();
            }
        },
        setSelections: function (hierarchyTypeId, hierarchyNodeIds) {
            if (hierarchyTypeId > 0) {
                this.find('name', 'usergroup')[0].setValue(hierarchyTypeId);
                this.loadHierarchy(hierarchyTypeId, hierarchyNodeIds);
            }
            this.hierarchyTypeId = hierarchyTypeId;
        },
        getSelections: function () {
            return this.currentPanel ? this.currentPanel.getSelections() : [];
        },
        loadHierarchy: function (hierarchyTypeId, hierarchyNodeIds) {
            var panel = null, height = 345, urlFragment = '', iconCls = '',
                container = this.find('name', 'assignmentpanel')[0],
                me = this;

            switch (hierarchyTypeId) {
                case Symphony.HierarchyType.location:
                    urlFragment = 'locations/';
                    iconCls = 'x-treenode-location';
                    break;
                case Symphony.HierarchyType.jobRole:
                    urlFragment = 'jobroles/';
                    iconCls = 'x-treenode-jobrole';
                    break;
                case Symphony.HierarchyType.audience:
                    urlFragment = 'audiences/';
                    iconCls = 'x-treenode-audience';
                    break;
                case Symphony.HierarchyType.user:
                    panel = new Symphony.CourseAssignment.TrainingProgramUserList({
                        border: false,
                        pageSize: 13, // fits into 1024x768
                        displayPagingInfo: false,
                        value: hierarchyNodeIds || [], //preselections
                        singleSelect: this.singleSelect,
                        listeners: {
                            rowclick: function (grid, rowIndex, e) {
                                me.fireEvent('rowclick', grid, rowIndex, e);
                            },
                            itemdblclick: function (grid, record, item, rowIndex, e) {
                                me.fireEvent('itemdblclick', grid, rowIndex, e, record);
                            }
                        }
                    });
                    break;
            }
            if (!panel) {
                panel = new Symphony.CourseAssignment.TrainingProgramHierarchyList({
                    border: false,
                    urlFragment: urlFragment,
                    iconCls: iconCls,
                    bodyStyle: 'background-color:white',
                    hierarchyTypeId: hierarchyTypeId,
                    value: hierarchyNodeIds || [] // pre-selections
                });
            }

            container.removeAll();
            container.add(panel);
            container.doLayout();

            this.currentPanel = panel;
            this.hierarchyTypeId = hierarchyTypeId;
        },
        enable: function () {
            this.find('name', 'usergroup')[0].enable();
            this.find('name', 'clear')[0].enable();
            this.find('name', 'reset')[0].enable();
            if (this.currentPanel && this.currentPanel.enable) {
                this.currentPanel.enable();
            }
            this.disabled = false;
        },
        disable: function () {
            this.find('name', 'usergroup')[0].disable();
            this.find('name', 'clear')[0].disable();
            this.find('name', 'reset')[0].disable();
            if (this.currentPanel && this.currentPanel.disable) {
                this.currentPanel.disable();
            }
            this.disabled = true;
        },
        removeOption: function (id) {
            var cbo = this.find('name', 'usergroup')[0];
            var record = cbo.store.getAt(cbo.store.find('id', id));

            if (record) {   // in case record doesn't exist (Happens if this function is fired twice with the same ID)
                // so this.removed would actually be the record we expected to end up in record. So in this 
                // situation, do nothing. 
                if (this.removed) {
                    cbo.store.add(this.removed);
                }

                cbo.store.remove(record);
                this.removed = record;
            }
        },
        reset: function () {
            var cbo = this.find('name', 'usergroup')[0];
            // if there is a removed record, put it back
            if (this.removed) {
                cbo.store.add(this.removed);
            }
            cbo.clearValue();
            this.removed = null;
            this.find('name', 'assignmentpanel')[0].removeAll();
        }
    });


    Symphony.CourseAssignment.TrainingProgramUserList = Ext.define('courseassignment.trainingprogramuserlist', {
        alias: 'widget.courseassignment.trainingprogramuserlist',
        extend: 'symphony.searchablegrid',
        singleSelect: false,
        initComponent: function () {
            var url = '/services/portal.svc/users/';
            var me = this;

            var sm = new Ext.selection.CheckboxModel({
                checkOnly: !me.singleSelect,
                sortable: true,
                header: ' ', // kills the select-all checkbox
                mode: me.singleSelect ? 'SINGLE' : 'MULTI'
            });
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { header: 'First Name', dataIndex: 'firstName' },
                    {
                        /*id: 'lastName',*/ header: 'Last Name', dataIndex: 'lastName',
                        flex: 1
                    }
                ]
            });

            Ext.apply(this, {
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'user',
                selModel: sm,
                plugins: [{ ptype: 'pagingselectpersist' }],
                method: 'POST',
                tbar: {
                    items: [{
                        xtype: 'checkbox',
                        boxLabel: 'Display Only Selected Users',
                        hideLabel: true,
                        hidden: me.singleSelect,
                        listeners: {
                            check: function (checkbox, checked) {
                                if (checked) {
                                    me.store.on('beforeload', me.selectedUserFilter, me);
                                } else {
                                    me.store.proxy.api.read = me.url;
                                    me.store.proxy.method = 'GET';
                                    me.store.proxy.jsonData = null;
                                    me.store.un('beforeload', me.selectedUserFilter, me);
                                }
                                me.refresh();
                            }
                        }
                    }]
                }
            });
            this.callParent(arguments);

            this.store.proxy.method = 'GET';
        },
        selectedUserFilter: function (store, options) {
            store.proxy.api.read = '/services/courseassignment.svc/specificusers/';
            store.proxy.method = 'POST';

            var ids = [];
            var selections = Ext.Array.clean(this.getPlugin('pagingSelectionPersistence').getPersistedSelection());
            for (var i = 0; i < selections.length; i++) {
                if (selections[i].get) {
                    ids.push(selections[i].get('id'));
                }
            }

            store.proxy.jsonData = ids;
        },
        getSelections: function () {
            var selectedIds = this.getPlugin('pagingSelectionPersistence').getPersistedSelection(), results = [];
            for (var i = 0; i < selectedIds.length; i++) {
                if (selectedIds[i] && selectedIds[i].get) {
                    results.push({ hierarchyTypeId: Symphony.HierarchyType.user, hierarchyNodeId: selectedIds[i].get('id') });
                }
            }
            return results;
        },
        setSelections: function (value) {
            this.getPlugin('pagingSelectionPersistence').setSelections(value);
        },
        clearSelections: function () {
            this.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
        },
        onRender: function () {
            Symphony.CourseAssignment.TrainingProgramUserList.superclass.onRender.apply(this, arguments);

            // after the load
            this.setSelections(this.value);
        }
    });


    Symphony.CourseAssignment.TrainingProgramHierarchyList = Ext.define('courseassignment.trainingprogramhierarchylist', {
        alias: 'widget.courseassignment.trainingprogramhierarchylist',
        extend: 'symphony.tabletree',
        urlFragment: '',
        hierarchyTypeId: 0,
        initComponent: function () {
            var me = this;
            var url = '/services/courseassignment.svc/' + this.urlFragment;
            if (me.urlsource) {
                url = me.urlsource + this.urlFragment;
            }

            Ext.apply(this, {
                url: url,
                model: 'hierarchy',
                checkboxes: true,
                columns: [{
                    xtype: 'treecolumn',
                    dataIndex: 'name',
                    flex: 1
                }]
            });
            this.callParent(arguments);


            this.store.on('load', function () {
                if (me.value && me.value.length) {
                    for (var i = 0; i < me.value.length; i++) {
                        var node = me.store.getNodeById(me.value[i]);
                        if (node) {
                            me.fireEvent('checkchange', node, true);
                            node.set('checked', true);
                        }
                    }
                }
            });
        },
        isChecked: function (record) {
            // if the item is in the list of checked items, check it
            if (this.value.indexOf(record.get('id')) > -1) {
                return true;
            }
            return false;
        },
        getSelections: function () {
            var nodes = this.getChecked(), results = [];
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].cls.indexOf('x-disabled') < 0) {
                    results.push({ hierarchyTypeId: this.hierarchyTypeId, hierarchyNodeId: nodes[i].id });
                }
            }
            return results;
        },
        setStateCheckedOnNode: function (nodeId) {
            var me = this;
            this.getRootNode().cascade(function (n) {
                if (!n.isRoot) {
                    if (n.get('id') == nodeId) {
                        n.set('checked', true);
                    }
                }
            });
        },
        clearSelections: function () {
            this.getRootNode().cascade(function (node) {
                node.set('checked', false);
            });
        }
    });


    Symphony.CourseAssignment.TrainingProgramCourseList = Ext.define('courseassignment.trainingprogramcourselist', {
        alias: 'widget.courseassignment.trainingprogramcourselist',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var url = '/services/courseassignment.svc/trainingprograms/courses/';



            var sm = new Ext.selection.CheckboxModel({
                checkOnly: true,
                sortable: true,
                header: ' ' // kills the select-all checkbox
            });
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
 // checkboxes
                    { header: ' ', dataIndex: 'courseTypeId', width: 30, align: 'center', renderer: Symphony.Portal.courseTypeRenderer }, // icons
                    {
                        /*id: 'name',*/ header: 'Course Name', dataIndex: 'name',
                        flex: 1
                    },
                    { /*id: 'levelIndentText',*/ header: 'Category', dataIndex: 'levelIndentText' }
                ]
            });

            Ext.apply(this, {
                //pageSize: 14,
                //displayPagingInfo: false,
                method: 'POST',
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'course',
                selModel: sm
            });

            this.callParent(arguments);
        },
        removeExclusions: function (exclude) {
            var data = [], me = this;
            for (var i = 0; i < exclude.length; i++) {
                data.push({ courseTypeId: exclude[i].get('courseTypeId'), id: exclude[i].get('id') });
            }
            var toRemove = [];
            // run through all the items currently excluded
            Ext.each(this.jsonData || [], function (item) {
                // compare them to each item to be excluded
                for (var i = 0; i < exclude.length; i++) {
                    if (item.courseTypeId == exclude[i].get('courseTypeId') && item.id == exclude[i].get('id')) {
                        // match found; store it for removal
                        toRemove.push(item);
                    }
                }
            });
            Ext.each(toRemove, function (item) {
                me.jsonData = me.jsonData.remove(item);
            });

            this.store.proxy.jsonData = this.jsonData;

            this.store.reload();
        },
        addExclusions: function (exclude) {
            var data = [];
            // add the new items
            for (var i = 0; i < exclude.length; i++) {
                data.push({ courseTypeId: exclude[i].get('courseTypeId'), id: exclude[i].get('id') });
            }
            this.jsonData = (this.jsonData || []).concat(data);
            this.store.proxy.jsonData = this.jsonData;

            this.store.reload();
        },
        setExclusions: function (trainingProgram) {
            var data = [];
            if (trainingProgram) {
                if (trainingProgram.requiredCourses) {
                    for (var i = 0; i < trainingProgram.requiredCourses.length; i++) {
                        data.push({ courseTypeId: trainingProgram.requiredCourses[i].courseTypeId, id: trainingProgram.requiredCourses[i].id });
                    }
                }
                if (trainingProgram.electiveCourses) {
                    for (var i = 0; i < trainingProgram.electiveCourses.length; i++) {
                        data.push({ courseTypeId: trainingProgram.electiveCourses[i].courseTypeId, id: trainingProgram.electiveCourses[i].id });
                    }
                }
                if (trainingProgram.optionalCourses) {
                    for (var i = 0; i < trainingProgram.optionalCourses.length; i++) {
                        data.push({ courseTypeId: trainingProgram.optionalCourses[i].courseTypeId, id: trainingProgram.optionalCourses[i].id });
                    }
                }
                if (trainingProgram.finalAssessments) {
                    for (var i = 0; i < trainingProgram.finalAssessments.length; i++) {
                        data.push({ courseTypeId: trainingProgram.finalAssessments[i].courseTypeId, id: trainingProgram.finalAssessments[i].id });
                    }
                }
            }
            this.jsonData = data;

            this.store.proxy.jsonData = this.jsonData;
        }
    });


    Symphony.CourseAssignment.TrainingProgramAssignedCourseList = Ext.define('courseassignment.trainingprogramassignedcourselist', {
        alias: 'widget.courseassignment.trainingprogramassignedcourselist',
        extend: 'symphony.searchablegrid',
        trainingProgramId: 0,
        initComponent: function () {
            var url = '/services/courseassignment.svc/trainingprograms/' + this.trainingProgramId + '/courses/';

            var sm = new Ext.selection.CheckboxModel({
                checkOnly: true,
                sortable: true,
                header: ' ' // kills the select-all checkbox
            });
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
 // checkboxes
                    { header: ' ', dataIndex: 'courseTypeId', width: 30, align: 'center', renderer: Symphony.Portal.courseTypeRenderer }, // icons
                    {
                        /*id: 'name',*/ header: 'Course Name', dataIndex: 'name',
                        flex: 1
                    }
                ]
            });

            Ext.apply(this, {
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'course',
                sm: sm
            });
            this.callParent(arguments);
        }
    });


    Symphony.CourseAssignment.TrainingProgramCoursePanel = Ext.define('courseassignment.trainingprogramcoursepanel', {
        alias: 'widget.courseassignment.trainingprogramcoursepanel',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: {
                    type: 'hbox',
                    align: 'stretch',
                    defaultMargins: { top: 0, right: 5, bottom: 0, left: 5 }
                },
                border: false,
                items: [{
                    xtype: 'courseassignment.trainingprogramcourselist',
                    name: 'available',
                    disabled: this.isShared,
                    flex: 0.38
                }, {
                    xtype: 'panel',
                    width: 50,
                    layout: {
                        type: 'vbox',
                        pack: 'center',
                        align: 'center',
                        defaultMargins: 2
                    },
                    disabled: this.isShared,
                    border: false,
                    items: [{
                        xtype: 'button',
                        text: '&nbsp;&nbsp;>>&nbsp;&nbsp;',
                        handler: function () {
                            var finalPanel = me.find('name', 'final')[0];

                            // get the selected available records
                            var records = me.find('name', 'available')[0].getSelectionModel().getSelections();

                            if (records.length === 0) {
                                // just bail if there is no selection
                                return;
                            }

                            // add them to the active list
                            me.activePanel.addRecords(records);
                            // remove them from the available list (set the exclusion, refresh the grid)
                            me.find('name', 'available')[0].addExclusions(records);

                            // update the summary
                            me.updateSummary();
                        }
                    }, {
                        xtype: 'button',
                        text: '&nbsp;&nbsp;<<&nbsp;&nbsp;',
                        handler: function () {
                            // get the selected assigned records from the active list
                            var records = me.activePanel.getSelectionModel().getSelections();

                            if (records.length === 0) {
                                // just bail if there is no selection
                                return;
                            }

                            // remove them from the list of assigned courses
                            me.activePanel.store.remove(records);
                            // and make them available again
                            me.find('name', 'available')[0].removeExclusions(records);

                            // update the summary
                            me.updateSummary();
                        }
                    }]
                }, {
                    flex: 0.44,
                    border: true,
                    layout: {
                        type: 'accordion',
                        titleCollapse: true,
                        animate: true
                    },
                    items: [{
                        title: 'Required Courses (drag/drop to re-order)',
                        name: 'required',
                        disabled: this.isShared,
                        syllabusTypeId: Symphony.SyllabusType.required,

                        viewConfig: {
                            plugins: 'gridviewdragdrop'
                        },
                        border: true,
                        stateId: 'courseassignment.trainingprogramrequiredcoursesgrid',
                        xtype: 'courseassignment.trainingprogramcoursesgrid',
                        cls: 'accordion-panel-first', /* fixes the top border of the accordion */
                        listeners: {
                            expand: function (panel) { me.activePanel = panel; },
                            manageuserattempts: function (onlineCourseId, record, grid, e) {
                                me.manageUserAttempts(onlineCourseId);
                            }
                        }
                    }, {
                        title: 'Elective Courses',
                        name: 'elective',
                        disabled: this.isShared,
                        syllabusTypeId: Symphony.SyllabusType.elective,
                        stateId: 'courseassignment.trainingprogramelectivecoursesgrid',
                        xtype: 'courseassignment.trainingprogramcoursesgrid',
                        listeners: {
                            expand: function (panel) { me.activePanel = panel; },
                            manageuserattempts: function (onlineCourseId, record, grid, e) {
                                me.manageUserAttempts(onlineCourseId);
                            }
                        }
                    }, {
                        title: 'Optional Courses',
                        name: 'optional',
                        disabled: this.isShared,
                        syllabusTypeId: Symphony.SyllabusType.optional,
                        stateId: 'courseassignment.trainingprogramoptionalcoursesgrid',
                        xtype: 'courseassignment.trainingprogramcoursesgrid',
                        listeners: {
                            expand: function (panel) { me.activePanel = panel; },
                            manageuserattempts: function (onlineCourseId, record, grid, e) {
                                me.manageUserAttempts(onlineCourseId);
                            }
                        }
                    }, {
                        title: 'Final Assessment (drag/drop to re-order)',
                        name: 'final',
                        disabled: this.isShared,
                        syllabusTypeId: Symphony.SyllabusType['final'],
                        stateId: 'courseassignment.trainingprogramfinalcoursesgrid',
                        xtype: 'courseassignment.trainingprogramcoursesgrid',
                        listeners: {
                            expand: function (panel) { me.activePanel = panel; },
                            manageuserattempts: function (onlineCourseId, record, grid, e) {
                                me.manageUserAttempts(onlineCourseId);
                            }
                        },
                        cls: 'accordion-panel-last', /* fixes the bottom border of the accordion */
                        viewConfig: {
                            plugins: 'gridviewdragdrop'
                        },
                    }]
                }, {
                    xtype: 'courseassignment.trainingprogramsummary',
                    name: 'summary',
                    isShared: this.isShared,
                    cls: 'courseassignment-summary',
                    width: 170
                }]
            });
            this.callParent(arguments);

            this.activePanel = this.query('[stateId=courseassignment.trainingprogramrequiredcoursesgrid]')[0];

            //TODO: ordering, plus allow setting of the various parameters

        },
        setNewHire: function (newHire) {
            var grids = this.findByType('courseassignment.trainingprogramcoursesgrid');
            Ext.each(grids, function (grid) {
                grid.setNewHire(newHire);
            });
        },
        updateSummary: function (tp) {
            // update the summary
            var summary = this.find('name', 'summary')[0];
            summary.setData({
                requiredCount: tp ? tp.requiredCourses.length : this.find('name', 'required')[0].store.getCount(),
                electiveCount: tp ? tp.electiveCourses.length : this.find('name', 'elective')[0].store.getCount(),
                optionalCount: tp ? tp.optionalCourses.length : this.find('name', 'optional')[0].store.getCount(),
                minimumElectives: tp ? tp.minimumElectives : (summary.rendered ? summary.getEl().query('input.minimum')[0].value : 0),
                enforceRequiredOrder: tp ? tp.enforceRequiredOrder : (summary.rendered ? summary.query('checkbox[name=enforceRequiredOrder]')[0].checked : false),
                assessmentSet: tp ? (tp.finalAssessments.length > 0) : this.find('name', 'final')[0].store.getCount() > 0 ? true : false,
                enforceCanMoveForwardIndicator: tp ? tp.enforceCanMoveForwardIndicator : (summary.rendered ? summary.query('checkbox[name=enforceCanMoveForwardIndicator]')[0].checked : false),
                affidavitId: tp ? tp.affidavitId : (summary.rendered ? summary.getAffidavitId() : 0),
                trainingProgramAffidavitId: tp ? tp.trainingProgramAffidavitId : (summary.rendered ? summary.getTrainingProgramAffidavitId() : 0),
                proctorFormId: tp ? tp.proctorFormId : (summary.rendered ? summary.getProctorFormId() : 0),
                courseUnlockModeId: tp ? tp.courseUnlockModeId : (summary.rendered ? summary.getCourseUnlockModeId() : 0)
            });
        },
        setTrainingProgram: function (trainingProgram) {
            this.trainingProgram = trainingProgram;
            if (this.trainingProgram) {
                this.find('name', 'required')[0].setCourses(trainingProgram.requiredCourses || [], trainingProgram);
                this.find('name', 'elective')[0].setCourses(trainingProgram.electiveCourses || [], trainingProgram);
                this.find('name', 'optional')[0].setCourses(trainingProgram.optionalCourses || [], trainingProgram);
                this.find('name', 'final')[0].setCourses(trainingProgram.finalAssessments || [], trainingProgram);
            }

            this.find('name', 'available')[0].setExclusions(trainingProgram);

            this.updateSummary(trainingProgram);
        },
        manageUserAttempts: function (onlineCourseId) {
            if (this.trainingProgram == null || this.trainingProgram.id == null || this.trainingProgram.id < 1) {
                Ext.Msg.alert('Notice', 'This feature is not available until the Training Prgram has been saved.');
                return;
            }

            var userAttemptsWindow = new Symphony.CourseAssignment.UserAttemptsWindow({
                trainingProgramId: this.trainingProgram.id,
                onlineCourseId: onlineCourseId
            });
            userAttemptsWindow.show();
        },
        getAssignments: function () {
            var summary = this.find('name', 'summary')[0];

            var tp = this.trainingProgram || {};
            return {
                requiredCourses: this.find('name', 'required')[0].getCourses(tp.requiredCourses),
                electiveCourses: this.find('name', 'elective')[0].getCourses(tp.electiveCourses),
                optionalCourses: this.find('name', 'optional')[0].getCourses(tp.optionalCourses),
                finalAssessments: this.find('name', 'final')[0].getCourses(tp.finalAssessments),
                minimumElectives: this.rendered ? parseInt(summary.getEl().query('input.minimum')[0].value || 0, 10) : (tp.minimumElectives || 0),
                enforceRequiredOrder: this.rendered ? summary.query('checkbox[name=enforceRequiredOrder]')[0].checked : (tp.enforceRequiredOrder || false),
                enforceCanMoveForwardIndicator: this.rendered ? summary.query('checkbox[name=enforceCanMoveForwardIndicator]')[0].checked : (tp.enforeCanMoveFowardIndicator || false),
                affidavitId: this.rendered ? summary.getAffidavitId() : (tp.affidavitId || 0),
                trainingProgramAffidavitId: this.rendered ? summary.getTrainingProgramAffidavitId() : (tp.trainingProgramAffidavitId || 0),
                proctorFormId: this.rendered ? summary.getProctorFormId() : (tp.proctorFormId || 0),
                courseUnlockModeId: this.rendered ? summary.getCourseUnlockModeId() : (tp.courseUnlockModeId || 0)
            };
        },
        isValid: function () {
            // if it's not rendered, just let them save it
            alert('');
            if (!this.rendered) { return true; }

            var elective = this.find('name', 'elective')[0], summary = this.find('name', 'summary')[0];
            if (elective.getCourses().length == 1) {
                Ext.Msg.alert('Invalid elective list', 'You cannot have only one elective course.');
                return false;
            }
            if (elective.getCourses().length > 0) {
                var min = parseInt(summary.getEl().query('input.minimum')[0].value || 0, 10);
                if (min == 0) {
                    Ext.Msg.alert('Invalid minimum electives', 'At least one of your elective courses must be required.<br/><br/>Use the "Summary" section to change this value.');
                    return false;
                }
            }
            return true;
        },
        disable: function () {
            if (this.rendered) {
                this.find('name', 'required')[0].disable();
                this.find('name', 'elective')[0].disable();
                this.find('name', 'optional')[0].disable();
                this.find('name', 'final')[0].disable();
                this.find('name', 'available')[0].disable();
                this.find('name', 'summary')[0].disable();
            } else {
                this.on('afterrender', function () {
                    this.disable();
                }, this, { single: true });
            }
        }
    });


    Symphony.CourseAssignment.TrainingProgramCoursesGrid = Ext.define('courseassignment.trainingprogramcoursesgrid', {
        alias: 'widget.courseassignment.trainingprogramcoursesgrid',
        extend: 'symphony.localgrid',
        viewConfig: {
            preserveScrollOnRefresh: true
        },
        initComponent: function () {
            var me = this;
            var sm = new Ext.selection.CheckboxModel({
                checkOnly: true,
                sortable: true,
                header: ' ' // kills the select-all checkbox
            });

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    align: 'center'
                },
                columns: [

                    { header: ' ', hideable: false, dataIndex: 'courseTypeId', align: 'left', width: 35, renderer: Symphony.Portal.courseTypeRenderer, menuDisabled: true },
                    {
                        /*id: 'name',*/ header: 'Course Name', dataIndex: 'name', renderer: Symphony.Portal.valueRenderer, align: 'left',
                        flex: 1
                    },
                    {
                        /*id: 'settings',*/
                        header: 'Settings',
                        width: 55,
                        dataIndex: 'settings',
                        renderer: function (value, meta, record) {
                            var id = Ext.id();

                            var parentForm = me.findParentBy(function (p) {
                                return p.xtype == 'courseassignment.trainingprogramform';
                            });

                            if (parentForm.find('name', 'isNewHire')[0].getValue() &&
                                !Symphony.Modules.hasModule(Symphony.Modules.AdvancedTrainingProgram) &&
                                !Symphony.Modules.hasModule(Symphony.Modules.Proctor)) {
                                return '-';
                            }


                            setTimeout(function () {
                                me.createSettingsQTip(id, record);
                            }, 10);

                            return '<a href="#" id="' + id + '">Settings</a>';
                        }
                    },
                    {
                        /*id: 'userAttempts',*/
                        header: 'Attempts',
                        width: 55,
                        dataIndex: 'attemptCount',
                        renderer: function (value, meta, record) {
                            //return ''; //disable this feature for now

                            if (record.get('courseTypeId') === Symphony.CourseType.online) {
                                return '<a href="#">Attempts</a>';
                            }
                        }
                    }
                ]
            });

            Ext.apply(this, {
                selModel: sm,
                colModel: colModel,
                model: 'course',
                bubbleEvents: ['showSettingsMenu'],
                listeners: {
                    cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex;

                        if (fieldName == 'settings') {
                            var record = grid.getStore().getAt(rowIndex);
                            var proctorRequiredIndicator = record.get('proctorRequiredIndicator');
                            var activeAfterDate = Symphony.parseDate(record.get('activeAfterDate'));
                            var dueDate = Symphony.parseDate(record.get('dueDate'));
                            var enforceCanMoveForwardIndicator = record.get('enforceCanMoveForwardIndicator');
                            var denyAccessAfterDueDateIndicator = record.get('denyAccessAfterDueDateIndicator');


                            if (activeAfterDate && activeAfterDate.getFullYear() == Symphony.defaultYear) {
                                activeAfterDate = null;
                            }

                            if (activeAfterDate) {
                                //we need to add the offset back, because we took it off on save
                                activeAfterDate = activeAfterDate.add(Date.MINUTE, activeAfterDate.getTimezoneOffset());
                            }

                            if (dueDate && dueDate.getFullYear() == Symphony.defaultYear) {
                                dueDate = null;
                            }

                            if (dueDate) {
                                //we need to add the offset back, because we took it off on save
                                dueDate = dueDate.add(Date.MINUTE, dueDate.getTimezoneOffset());
                            }

                            me.fireEvent('showSettingsMenu', proctorRequiredIndicator, activeAfterDate, dueDate, enforceCanMoveForwardIndicator, denyAccessAfterDueDateIndicator, record, grid, e, (me.name && me.name.toLowerCase() === 'final'));
                        }
                        else if (fieldName == 'attemptCount') {
                            var record = grid.getStore().getAt(rowIndex);

                            // only fire if valid course type
                            if (record.get('courseTypeId') === Symphony.CourseType.online) {
                                var onlineCourseId = record.get('id');
                                me.fireEvent('manageuserattempts', onlineCourseId, record, grid, e);
                            }
                        }
                    },
                    cellcontextmenu: function (grid, rowIndex, columnIndex, e) {
                        var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex;
                        if (fieldName == 'dueDate') {
                            var record = grid.store.getAt(rowIndex);
                            var dt = Symphony.parseDate(record.get('dueDate'));
                            if (dt && dt.getFullYear() != Symphony.defaultYear) {
                                var menu = new Ext.menu.Menu({
                                    items: [
                                        {
                                            text: 'Remove Date',
                                            iconCls: 'x-menu-date-remove',
                                            handler: function () {
                                                record.set('dueDate', null);
                                                record.commit();
                                                grid.getView().refresh();
                                            }
                                        }
                                    ]
                                });
                                menu.showAt(e.getXY());
                            }
                        }
                        e.stopEvent();
                    }
                }
            });

            this.callParent(arguments);
        },
        createSettingsQTip: function (id, record) {
            var dueDate = record.data.dueDate,
                activeAfterDate = record.data.activeAfterDate,
                proctorRequiredIndicator = record.data.proctorRequiredIndicator,
                enforceCanMoveForwardIndicator = record.data.enforceCanMoveForwardIndicator,
                activeAfterMinutes = record.data.activeAfterMinutes,
                expiresAfterMinutes = record.data.expiresAfterMinutes,
                qTipTpl = '<h1 class="{0}"><img src="/images/{1}"/>{2}</h1><p>{3}</p>',//'<h1><img src="/images/{0}"/>{1}</h1><p>{2}</p>',
                qTip = '',
                target = document.getElementById(id);

            if (activeAfterMinutes > 0) {
                qTip += String.format(qTipTpl, 'active-after', 'date_go.png', 'Active After:', Symphony.Classroom.durationRenderer(activeAfterMinutes) + ' ' + Symphony.ActiveAfterModesStore.getById(record.get('activeAfterMode')).get('text'));
            } else if (activeAfterDate && Symphony.parseDate(activeAfterDate).getFullYear() != Symphony.defaultYear) {
                activeAfterDate = Symphony.parseDate(activeAfterDate);
                activeAfterDate = activeAfterDate.add(Date.MINUTE, activeAfterDate.getTimezoneOffset()); // Convert UTC to local
                qTip += String.format(qTipTpl, 'active-after', 'date_go.png', 'Active After Date:', activeAfterDate.formatSymphony('m/d/Y'));
            }

            if (expiresAfterMinutes) {
                qTip += String.format(qTipTpl, 'due-date', 'date_next.png', 'Due:', Symphony.Classroom.durationRenderer(expiresAfterMinutes) + ' ' + Symphony.DueModesStore.getById(record.get('dueMode')).get('text'));
            } else if (dueDate && Symphony.parseDate(dueDate).getFullYear() != Symphony.defaultYear) {
                dueDate = Symphony.parseDate(dueDate);
                dueDate = dueDate.add(Date.MINUTE, dueDate.getTimezoneOffset()); // Convert UTC to local
                qTip += String.format(qTipTpl, 'due-date', 'date_next.png', 'Due Date:', dueDate.formatSymphony('m/d/Y'));
            }

            if (proctorRequiredIndicator) {
                qTip += String.format(qTipTpl, 'proctor', 'eye.png', 'Proctor Required', '');
            }

            if (enforceCanMoveForwardIndicator) {
                qTip += String.format(qTipTpl, 'approval', 'lock.png', 'Approval Required', '');
            }

            if (qTip.length > 0) {
                Ext.QuickTips.register({
                    target: id,
                    autoHide: true,
                    text: qTip,
                    width: 140,
                    cls: 'tp-course-settings-qtip'
                });
            }
        },
        setNewHire: function (newHire) {
            // The due date column is no longer visible in the grid
            // it is part of the settings popup so this column does 
            // not need to be hidden when setting new hire.
            /*
            var cl = this.getColumnModel();

            var index = cl.getIndexById('dueDate');

            if (index > -1) {
                cl.setHidden(cl.getIndexById('dueDate'), newHire);
                var me = this;
                window.setTimeout(function () {
                    //me.getView().refresh(true);
                    me.doLayout();
                });
            }*/
        },
        setCourses: function (data, tp) {
            // used automatically by the underlying grid component
            this.data = data;
            this.trainingProgram = tp;
        },
        getCourses: function (currentCourses) {
            var results = [], me = this, data = this.store.data.items;

            if (!this.rendered && Ext.isArray(currentCourses)) {
                data = [];
                for (var i = 0; i < currentCourses.length; i++) {
                    data.push(Ext.create(this.model, currentCourses[i]));
                }
            }

            Ext.Array.each(data, function (record) {
                var o = {
                    courseTypeId: record.get('courseTypeId'),
                    id: record.get('id'),
                    dueDate: record.get('dueDate'),
                    activeAfterDate: record.get('activeAfterDate'),
                    proctorRequiredIndicator: record.get('proctorRequiredIndicator'),
                    parameters: record.get('parameters'),
                    enforceCanMoveForwardIndicator: record.get('enforceCanMoveForwardIndicator'),
                    expiresAfterMinutes: record.get('expiresAfterMinutes'),
                    activeAfterMinutes: record.get('activeAfterMinutes'),
                    denyAccessAfterDueDateIndicator: record.get('denyAccessAfterDueDateIndicator') ? true : false,
                    activeAfterMode: record.get('activeAfterMode'),
                    dueMode: record.get('dueMode'),
                    duration: record.get('duration'),
                    isUseAutomaticDuration: record.get('isUseAutomaticDuration'),
                    automaticDuration: record.get('automaticDuration')
                };

                if (!o.dueDate) {
                    delete o.dueDate;
                }
                if (!o.activeAfterDate) {
                    delete o.activeAfterDate;
                }
                if (!o.expiresAfterMinutes) {
                    delete o.expiresAfterMinutes;
                }
                if (!o.activeAfterMinutes) {
                    delete o.activeAfterMinutes;
                }
                if (!o.activeAfterMode) {
                    delete o.activeAfterMode;
                }
                if (!o.dueMode) {
                    delete o.dueMode;
                }

                if (!o.duration) {
                    delete o.duration;
                }

                if (typeof (o.proctorRequiredIndicator) === 'undefined') {
                    delete o.proctorRequiredIndicator;
                }
                if (typeof (o.enforceCanMoveForwardIndicator) === 'undefined') {
                    delete o.enforceCanMoveForwardIndicator;
                }
                if (typeof (o.parameters) === 'undefined') {
                    delete o.parameters;
                }

                results.push(o);
            });
            return results;
        },
        addRecords: function (records) {
            this.store.add(records);
        },
        removeRecords: function (records) {
            this.store.remove(records);
        }
    });


    Symphony.CourseAssignment.TrainingProgramDocumentsGrid = Ext.define('courseassignment.trainingprogramdocumentsgrid', {
        alias: 'widget.courseassignment.trainingprogramdocumentsgrid',
        extend: 'symphony.editableunpagedgrid',
        isShared: false,
        initComponent: function () {
            var url = '/services/courseassignment.svc/trainingprogramfiles/' + (this.trainingProgramId || 0);
            var me = this;
            var isDisabled = false;
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        /*id: 'name',*/
                        header: 'File Name',
                        dataIndex: 'filename',
                        renderer: function (value, meta, record) {
                            return '<a href="#" onclick="Symphony.Portal.downloadTrainingProgramFile({0})">{1}</a>'.format(record.get('id'), value);
                        }
                    },
                    { /*id: 'title',*/ header: 'Title', dataIndex: 'title', editor: new Ext.form.TextField({ allowBlank: false }) },
                    {
                        /*id: 'description',*/ header: 'Description', dataIndex: 'description', editor: new Ext.form.TextField({ allowBlank: false }),
                        flex: 1
                    },
                    {
                        /*id: 'uploaded',*/
                        header: 'Upload Date',
                        dataIndex: 'id',
                        width: 75,
                        align: 'center',
                        renderer: function (value, meta, record) {
                            return value ? Symphony.dateRenderer(record.get('createdOn')) : '-';
                        }
                    },
                    {
                        header: 'Upload Progress',
                        renderer: function (value, meta, record) {
                            if (record.get('id') > 0) {
                                return '<div style="text-align:center;width:100%;height:100%;">-</div>';
                            } else {
                                var percent = record.get('percentUploaded') * 100;
                                return '<div style="width:100%;height:100%;"><div style="height:100%;width:' + percent + '%;background-color:#D2E0F1;">&nbsp;</div></div>';
                            }
                        }
                    }
                ]
            });

            Ext.apply(this, {
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'trainingProgramFile',
                clicksToEdit: 1,
                listeners: {
                    'beforeedit': function (editor, e, eOpts) {
                        // at least for now, you can't edit a file after it's uploaded
                        if (e.record.get('id') > 0) {
                            e.cancel = true;
                        }
                    },
                    edit: function (editor, e, eOpts) {
                        e.record.commit();
                    }
                },
                tbar: {
                    items: [{
                        xtype: 'swfuploadbutton',
                        disabled: this.isShared,
                        uploadUrl: '/Uploaders/TrainingProgramFileUploader.ashx',
                        name: 'uploader',
                        text: 'Add File',
                        iconCls: 'x-button-upload',
                        listeners: {

                            'queue': function (file) {
                                if (me.isDisabled() || me.isShared) {
                                    return;
                                }

                                var store = me.store;
                                var index = store.getCount();
                                store.add({
                                    filename: file.name,
                                    title: file.name,
                                    description: file.name,
                                    swfFileId: file.id,
                                    percentUploaded: 0
                                });
                                me.startEditing(index, 2);
                            },
                            'uploadstart': function (item) {
                                if (me.isDisabled() || me.isShared) {
                                    return;
                                }
                                var record = me.store.getAt(me.store.find('swfFileId', item.id));

                                this.addFileParameter(item.id, 'title', record.get('title'));
                                this.addFileParameter(item.id, 'description', record.get('description'));
                            },
                            'uploadprogress': function (item, completed, total) {
                                if (me.isDisabled() || me.isShared) {
                                    return;
                                }

                                var record = me.store.getAt(me.store.find('swfFileId', item.id));
                                record.set('percentUploaded', parseInt(completed / total, 10));
                                record.commit();
                            },
                            'allcomplete': function () {
                                if (me.isDisabled() || me.isShared) {
                                    return;
                                }

                                var lastOptions = me.store.lastOptions;
                                me.refresh();
                            }
                        }
                    }, {
                        xtype: 'button',
                        disabled: this.isShared,
                        text: 'Remove File',
                        iconCls: 'x-button-upload-delete',
                        handler: function () {
                            var record = me.getSelectionModel().getSelection()[0];
                            if (!record) { return; }

                            var file = record.data;
                            // two options here:
                            // 1) if it's just queued, remove it immediately
                            // 2) if it's already uploaded, prompt and delete
                            if (file.swfFileId) {
                                var swf = this.ownerCt.find('xtype', 'swfuploadbutton')[0];
                                swf.removeFile(file.swfFileId);
                                me.store.remove(record);
                            } else {
                                Ext.Msg.confirm('Are you sure?', 'You are about to delete the selected file. This action cannot be undone. Are you sure?', function (btn) {
                                    if (btn == 'yes') {
                                        Symphony.Ajax.request({
                                            url: '/services/courseassignment.svc/trainingprogramfiles/delete/' + (file.id || 0),
                                            success: function (result) {
                                                me.store.remove(record);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }, '->',
                    //'->', 
                      '<span style="font-style:italic">Files will be uploaded when you save the Training Program.</span>'
                    ]
                }
            });

            this.callParent(arguments);

            // make sure the url is always set correctly
            this.store.on('beforeload', function (store, options) {
                store.proxy.api.read = '/services/courseassignment.svc/trainingprogramfiles/' + (me.trainingProgramId || 0);
            });
        },
        upload: function (config) {
            var swf = this.getTopToolbar().find('name', 'uploader')[0];

            if (swf && swf.rendered) {
                swf.addParameter('trainingProgramId', config.trainingProgramId);
                swf.on('allcomplete', config.onComplete);
                swf.uploadAll();
            } else {
                config.onComplete();
            }
        },
        setTrainingProgram: function (tp) {
            this.trainingProgramId = tp.parentTrainingProgramId ? tp.parentTrainingProgramId : tp.id;
        },
        disable: function () {
            this.getTopToolbar().find('xtype', 'swfuploadbutton')[0].disable();
            this.getTopToolbar().find('xtype', 'button')[0].disable();
            this.isDisabled = true;
            Symphony.CourseAssignment.TrainingProgramDocumentsGrid.superclass.disable.apply(this, arguments);
        }
    });


    Symphony.CourseAssignment.SecondaryCategoriesList = Ext.define('courseassignment.secondarycategorieslist', {
        alias: 'widget.courseassignment.secondarycategorieslist',
        extend: 'symphony.localgrid',
        data: null,
        loadData: function (secondaryCategories) {
            this.store.loadData(secondaryCategories);
        },
        initComponent: function () {
            var me = this;

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left'
                },
                columns: [{
                    //id: 'id',
                    header: 'Id',
                    width: 100,
                    dataIndex: 'id',
                    align: 'left'
                }, {
                    //id: 'name',
                    dataIndex: 'name',
                    width: 300,
                    header: 'Name',
                    align: 'left',
                    editor: new Ext.form.TextField({
                        allowBlank: false
                    })
                }]
            });

            Ext.apply(this, {
                colModel: colModel,
                model: 'secondaryCategory'
            });

            this.callParent(arguments);
        }
    });


    /*    Authors List    */

    Symphony.CourseAssignment.AuthorsList = Ext.define('courseassignment.authorslist', {
        alias: 'widget.courseassignment.authorslist',
        extend: 'symphony.localgrid',
        data: null,
        loadData: function (authors) {
            this.store.loadData(authors);
        },
        initComponent: function () {
            var me = this;

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left'
                },
                columns: [
                {
                    //id: 'email',
                    header: 'Email',
                    width: 180,
                    dataIndex: 'email',
                    align: 'left'
                }, {
                    //id: 'firstName',
                    dataIndex: 'firstName',
                    header: 'First Name',
                    align: 'left'
                }, {
                    //id: 'lastName',
                    dataIndex: 'lastName',
                    header: 'Last Name',
                    align: 'left'
                }]
            });
            Ext.apply(this, {
                colModel: colModel,
                model: 'author'
            });
            this.callParent(arguments);
        }
    });



})();
