﻿using Symphony.Core.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Symphony.Web.scripts.symphony.Certificates
{
    /// <summary>
    /// Summary description for CertificateImagesJSON
    /// </summary>
    public class CertificateImagesJSON : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var jserial = new System.Web.Script.Serialization.JavaScriptSerializer();
            var fileList = (new CertificateController()).GetAssetListForCKEditor();

            var responseJson = jserial.Serialize(fileList);

            context.Response.ContentType = "application/json";
            context.Response.Write(responseJson);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}