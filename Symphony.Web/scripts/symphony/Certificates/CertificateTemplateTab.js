﻿Ext.define('Certificates.CertificateTemplateTab', {
    extend: 'Ext.tab.Panel',
    xtype: 'certificates.certificatetemplatetab',
    requires: [
        'CertificateTemplate',
        'Certificates.CertificateTemplateGrid',
        'Ext.tab.Panel'
    ],
    uses: 'Certificates.CertificateTemplatePanel',
    mixins: ['Symphony.mixins.FunctionOwnership'],

    layout: 'fit',

    defaults: {
        layout: 'border',
        header: false,
        border: false
    },
    items: [{
        xtype: 'panel',
        title: 'Templates',

        items: [{
            xtype: 'certificates.certificatetemplatetree',
            url: '/services/certificate.svc/certificateTemplates/',
            itemId: 'template-grid',
            region: 'west',

            border: false,
            split: true,
            width: 290,

            listeners: {
                addrecord: function (record) {
                    var root = this.up('[xtype=certificates.certificatetemplatetab]');
                    root.addTemplatePanel(record);
                },
                click: function (model) {
                    var root = this.up('[xtype=certificates.certificatetemplatetab]');
                    root.addTemplatePanel(model);
                }
            }
        }, {
            xtype: 'tabpanel',
            itemId: 'template-tab-panel',
            region: 'center',
            border: false
        }]
    }, {
        xtype: 'symphony.assetsgrid',
        title: 'Assets',
        model: '',
        assetsUploadUrl: '/Uploaders/CertificateAssetsUploader.ashx',    // handler to save the asset
        assetsUrl: '/services/certificate.svc/certificates/assets'
    }],

    initComponent: function() {
        var me = this;

        me.callParent();

        me.on('activate', function() {
            var templateGrid = me.queryById('template-grid');

            Log.debug('Certificate template tab activated, refreshing grids.');
            templateGrid.refresh();
        });
    },

    addTemplatePanel: function(template) {
        var me = this,
            grid = me.queryById('template-grid'),
            tabPanel = me.queryById('template-tab-panel'),
            editor;

        Log.info('Opening a new certificate template editor.');

        editor = tabPanel.items.findBy(function(el, key) {
            if (el.model && !el.model.phantom) {
                return el.model.getId() == template.getId();
            }

            return false;
        });

        if (!editor) {
            Log.debug('Editor is not open, opening an editor for the model.', template);

            editor = tabPanel.add(new Certificates.CertificateTemplatePanel({
                model: template,
                closable: true
            }));
            editor.on('change', function() {
                grid.refresh();
            });
        } else {
            Log.debug('Editor is already open for the model, switching focus.', editor);
        }

        tabPanel.activate(editor);
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});