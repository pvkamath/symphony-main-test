﻿(function () {
    if (!window.Symphony) {
        window.Symphony = {
            Certificates: {}
        }
    }

    Symphony.Certificates.initUserDataForm = function (submitCallback) {
        var form = Ext.get('dataForm');

        if (!form) {
            return false;
        }

        var certificateJson = form.getAttribute('data-content'),
            formUrl = form.getAttribute('data-action'),
            certificate = JSON.parse(certificateJson);

        if (Ext.isArray(certificate)) {

            for (var i = 0; i < certificate.length; i++) {
                if (certificate[i].xtype === 'datefield' && !certificate[i].altFormats) {
                    certificate[i].altFormats = 'Y-m-dTH:i:s'
                }

                if (certificate[i].xtype === 'checkbox') {
                    if (certificate[i].value === 'on' || certificate[i].value === 1 || certificate[i].value === true || certificate[i].value === 'true') {
                        certificate[i].checked = true;
                        certificate[i].value = null;
                        certificate[i].uncheckedValue = false;
                    }
                }

                if (certificate[i].config) {
                    try {
                        var parsedConfig = JSON.parse(certificate[i].config);
                        Ext.apply(certificate[i], parsedConfig);
                    } catch (e) {
                        console.error(e);
                    }
                }
            }

            if (certificate.length > 0) {
                var first = certificate[0];
                if (first.xtype != 'component') {
                    certificate = [{
                        xtype: 'component',
                        html: 'Please enter the following information in order to display it on your certificate:'
                    }].concat(certificate);
                }
            }

            Ext.create('Ext.form.Panel', {
                width: 420,
                renderTo: 'dataForm',
                border: false,
                url: formUrl,
                standardSubmit: true,
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    pack: 'start'
                },
                defaults: {
                    flex: 1,
                    labelWidth: 150,
                    cls: 'x-form-item',
                    style: 'padding-bottom: 10px'
                },
                items: certificate,
                buttons: [{
                    text: 'Get My Certificate',
                    formBind: true, //only enabled once the form is valid
                    disabled: true,
                    handler: function () {
                        var form = this.up('form').getForm(),
                            window = this.up('window'),
                            mask = new Ext.LoadMask(Ext.get('dataForm').parent().dom, { msg: "Loading Certificate Preview..." });

                        mask.show();

                        if (form.isValid()) {
                            if (typeof(submitCallback) == 'function') {
                                var formData = form.getFieldValues();

                                Ext.Ajax.request({
                                    url: formUrl,
                                    method: 'post',
                                    params: formData,
                                    success: function (args) {
                                        mask.hide();
                                        var html = args.responseText;
                                        submitCallback(html);
                                    }
                                });
                            } else {
                                form.submit();
                            }
                        }
                    }
                }]
            });
        }
        return true;
    }
})();
