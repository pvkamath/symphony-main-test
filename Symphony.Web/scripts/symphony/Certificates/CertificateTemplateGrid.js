﻿Ext.define('Certificates.CertificateTemplateGrid', {
    extend: 'symphony.searchablegrid',
    xtype: 'certificates.certificatetemplategrid',
    require: [
        'CertificateTemplate',
        'Ext.button.Button'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],

    assumeOwnership: ['add-button'],
    model: 'CertificateTemplate',
    url: '/services/certificate.svc/certificateTemplates/',
    deferLoad: true,

    columns: [{
        text: 'Name',
        dataIndex: 'name',
        flex: 1
    }, {
        text: 'Description',
        dataIndex: 'description',
        flex: 2
    }],

    tbar: {
        items: [{
            text: 'Add',
            itemId: 'add-button',
            iconCls: 'x-button-add',
            handler: function() {
                var root = this.up('[xtype=certificates.certificatetemplategrid]');
                root.fireEvent('addgriditem', new CertificateTemplate({
                    name: 'New Certificate Template'
                }));
            }
        }]
    },

    initComponent: function() {
        var me = this;
        me.callParent();

        Log.watchStore(me, me.getStore());

        this.on('itemclick', function(view, model) {
            Log.debug('Clicked on an grid item to open a new editor.');

            me.fireEvent('opengriditem', model);
        });
    },

    getSelected: function () {
        return this.getSelectionModel().selections;
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});