﻿(function () {
    Symphony.Certificates.CertificateEditorTree = Ext.define('certificates.certificateeditortree', {
        alias: 'widget.certificates.certificateeditortree',
        extend: 'symphony.tabletree',
        afterSelect: null,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                url: '/services/certificate.svc/certificates/',
                model: 'certificateTemplate',
                columns: [{
                    xtype: 'treecolumn',
                    dataIndex: 'name',
                    flex: 1
                }],
                rootVisible: true,
                rootText: 'Certificates'
            });
            this.callParent(arguments);
        }
    });

})();