﻿Symphony.Certificates.MetaDataPanel = Ext.define('certificates.metadatapanel', {
    alias: 'widget.certificates.metadatapanel',
    extend: 'Ext.Panel',
    isLockBySharedTrainingFlag: false,
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            frame: false,
            bodyCls: 'x-panel-mc',
            border: false,
            title: 'Meta Data',
            layout: 'fit',
            items: [{
                tbar: [{
                    xtype: 'button',
                    text: 'Add',
                    disabled: me.isLockBySharedTrainingFlag,
                    iconCls: 'x-button-add',
                    handler: function () {
                        var grid = me.find('xtype', 'symphony.editableunpagedgrid')[0];
                        var keyValuePair = Ext.create('keyValuePair', { key: 'New Key', value: 'New Value' });

                        grid.store.add(keyValuePair);
                        grid.startEditing(grid.store.indexOf(keyValuePair), 0);
                    }
                }, {
                    xtype: 'tbtext',
                    text: 'Data entered here can be used when generating the certificate for this training program.',
                    style: 'margin-top: -2px; margin-left: 5px; font-style: italic'
                }],
                frame: true,
                border: false,
                xtype: 'symphony.editableunpagedgrid',
                disabled: this.isLockBySharedTrainingFlag,
                layout: 'fit',
                model: 'keyValuePair',
                clicksToEdit: 1,
                autoLoad: false,
                deferLoad: true,
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.getStore().getAt(rowIndex);
                        if (columnIndex === 2) {
                            if (me.isLockBySharedTrainingFlag) {
                                Ext.Msg.alert('Title', 'This is shared training program. You can not remove items.', Ext.emptyFn);
                                return;
                            }
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete?', function (btn) {
                                if (btn == 'yes') {
                                    grid.getStore().remove(record);
                                }
                            });
                        }
                    }
                },
                colModel: new Ext.grid.ColumnModel({
                    defaults: {
                        sortable: true,
                        renderer: Symphony.Portal.valueRenderer
                    },
                    columns: [
                        { /*id: 'key',*/ header: 'Key', dataIndex: 'key', editor: new Ext.form.TextField({ allowBlank: false }) },
                        { /*id: 'value',*/ header: 'Value', dataIndex: 'value', editor: new Ext.form.TextField({ allowBlank: false }) },
                        { header: ' ', dataIndex: 'key', hideable: false, renderer: Symphony.deleteRenderer, width: 28 }
                    ]
                })
            }]
        });
        this.callParent(arguments);
    },
    setData: function (data) {
        var grid = this.find('xtype', 'symphony.editableunpagedgrid')[0];

        if (data.metaDataJson) {
            var metaData = JSON.parse(data.metaDataJson);
            grid.store.removeAll();
            for (var i = 0; i < metaData.length; i++) {
                grid.store.add({
                    key: metaData[i].key, value: metaData[i].value
                });
            }
        }
    },
    getData: function (data) {
        var grid = this.find('xtype', 'symphony.editableunpagedgrid')[0];

        var metaDataArray = [];
        grid.getStore().each(function (record) {
            metaDataArray.push({ key: record.get('key'), value: record.get('value') });
        });

        return JSON.stringify(metaDataArray);
    },
    disable: function () {
        this.items.each(function (item) {
            item.setDisabled(true);
            var toolbar = item.getTopToolbar();
            if (toolbar) {
                toolbar.setDisabled(true);
            }
        });
    }
});
