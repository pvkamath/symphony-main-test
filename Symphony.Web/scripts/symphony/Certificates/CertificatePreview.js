﻿Ext.define('certificates.certificatepreview', {
    extend: 'Ext.Window',
    alias: 'widgets.certificates.certificatepreview',
    width: 525,
    height: 400,
    layout: 'card',
    title: 'Select a Training Program',
    border: false,
    modal: true,
    initComponent: function() {
        var me = this;

        Ext.apply(this, {
            items: [{
                xtype: 'panel',
                layout: 'fit',
                items: [{
                    xtype: 'courseassignment.trainingprogramsgrid',
                    border: false,
                    frame: false,
                    isHideDelete: true,
                    tbarItems: [],
                    listeners: {
                        rowclick: function (grid, rowIndex, e) {
                            var record = grid.getStore().getAt(rowIndex);
                            me.trainingProgramId = record.get('id');
                        },
                        itemdblclick: function (grid, record, item, rowIndex, e) {
                            me.trainingProgramId = record.get('id');
                            me.trainingProgramSelected();
                        }
                    }
                }],
                listeners: {
                    activate: function () {
                        this.find('xtype', 'courseassignment.trainingprogramsgrid')[0].refresh();
                    }
                },
                buttons: [{
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        me.close();
                    }
                }, {
                    xtype: 'button',
                    text: 'Select Training Program',
                    iconCls: 'x-button-finish',
                    handler: function () {
                        if (me.trainingProgramId > 0) {
                            me.trainingProgramSelected()
                        } else {
                            Ext.Msg.alert('Please select a training program', 'Please select a training program to use when previewing this certificate.');
                        }
                    }
                }]
            }, {
                xtype: 'courseassignment.userassignmentpanel',
                bodyStyleOverride: 'padding:0px',
                border: false,
                frame: false,
                singleSelect: true,
                listeners: {
                    render: function (panel) {
                        panel.loadHierarchy(Symphony.HierarchyType.user);
                    },
                    rowclick: function (grid, rowIndex, e) {
                        var record = grid.getStore().getAt(rowIndex);
                        me.userId = record.get('id');
                    },
                    itemdblclick: function (grid, rowIndex, e, record) {
                        me.userId = record.get('id');
                        me.userSelected();
                    }
                },
                buttons: [{
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        me.close();
                    }
                }, {
                    xtype: 'button',
                    text: 'Select User',
                    iconCls: 'x-button-finish',
                    handler: function () {
                        if (me.userId > 0) {
                            me.userSelected()
                        } else {
                            Ext.Msg.alert('Please select a user', 'Please select a user to use when previewing this certificate.');
                        }
                    }
                }]
            }, {
                readOnly: true,
                xtype: 'license.trainingprogramassignmentpanel',
                border: false,
                listeners: {
                    rowclick: function (grid, rowIndex, e) {
                        var record = grid.getStore().getAt(rowIndex);
                        me.licenseId = record.get('id');
                    },
                    itemdblclick: function (grid, rowIndex, e, record) {
                        me.licenseId = record.get('id');
                        me.licenseSelected();
                    },
                    activate: function () {
                        if (me.licenseData) {
                            this.setData(licenseData);
                        } else {
                            var licensePanel = this;
                            Symphony.Ajax.request({
                                method: 'GET',
                                url: '/services/license.svc/license/',
                                success: function (result) {
                                    licensePanel.setData(result.data);
                                }
                            });
                        }
                    }
                },
                buttons: [{
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        me.close();
                    }
                }, {
                    xtype: 'button',
                    text: me.isPreview ? 'Preview' : 'Print',
                    iconCls: me.isPreview ? 'x-button-preview' : 'x-button-print',
                    handler: function () {
                        if (me.licenseId > 0) {
                            me.licenseSelected();
                        } else {
                            Ext.Msg.alert('Please select a license', 'Please select a license to use when previewing this certificate.');
                        }
                    }
                }]
            }]
        });

        this.callParent(arguments);

        if (me.trainingProgramId > 0) {
            me.trainingProgramSelected();
        }

        if (me.userId > 0) {
            me.userSelected();
        }
    },
    trainingProgramSelected: function() {
        this.layout.setActiveItem(1);
        this.setTitle('Select a User');
    },
    userSelected: function () {
        this.layout.setActiveItem(2);
        this.setTitle('Select a License');
    },
    licenseSelected: function () {
        
        var me = this,
            mask = new Ext.LoadMask(this.el.dom, { msg: "Loading Certificate Preview..." }),
            previewCertificateHandler = typeof (this.previewCertificate) == 'function' ? this.previewCertificate : Symphony.Portal.showCertificate;

        mask.show();

        previewCertificateHandler({
            certificateTypeId: Symphony.CertificateType.trainingProgram,
            trainingProgramId: this.trainingProgramId,
            courseOrClassId: 0,
            userId: this.userId,
            licenseOverrideId: this.licenseId,
            isCertificatePreview: this.isPreview,
            scoreOverride: this.isPreview ? 100 : null
        }, function () {
            mask.hide();
            me.close();
        });

    }
})