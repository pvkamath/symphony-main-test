﻿Symphony.Customer.HierarchyTree = Ext.define('certificates.certificatetemplatetree', {
    alias: 'widget.certificates.certificatetemplatetree',
    extend: 'symphony.tabletree',
    customerId: 0,
    title: 'Certificate Templates',
    plugins: { ptype: 'bufferedrenderer' },
    tbar: {
        items: [{
            text: 'Add',
            itemId: 'add-button',
            iconCls: 'x-button-add',
            handler: function () {
                var root = this.up('[xtype=certificates.certificatetemplatetree]');
                root.fireEvent('addrecord', new CertificateTemplate({
                    name: 'New Certificate Template'
                }));
            }
        }]
    },
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            viewConfig: Symphony.Customer.enableEditing ? {
                plugins: {
                    ptype: 'treeviewdragdrop'
                },
                listeners: {
                    drop: function (node, data, overModel, dropPosition, opts) {

                        var id = data.records[0].getId();
                        var parent = data.records[0].parentNode;
                        var parentId = 0;

                        if (parent && !parent.isRoot()) {
                            parentId = parent.getId();
                        }

                        var name = data.records[0].get('name');

                        Symphony.Ajax.request({
                            url: '/services/certificate.svc/template/' + id + '/reparent/' + parentId,
                            jsonData: { id: id, name: name, parentId: parentId },
                            success: function (result) {
                                // refresh to ensure we have the latest data
                                me.refresh();
                            },
                            failure: function (result) {
                                if (result.status != 200) {
                                    Ext.Msg.alert('Error', result.statusText);
                                } else {
                                    Ext.Msg.alert('Error', result.error);
                                }
                                // refresh to make everything go back the way it was
                                me.refresh();
                            }
                        });
                    }
                }
            } : null,
            //enableDD: Symphony.Customer.enableEditing,
            rootVisible: false,
            iconCls: this.iconCls,
            columns: [{
                xtype: 'treecolumn', //this is so we know which column will show the tree
                text: this.title,
                flex: 2,
                sortable: true,
                dataIndex: 'name'
            }],
            url: me.url,
            model: CertificateTemplate,
            deferLoad: true
        });
        this.callParent(arguments);
    }
});