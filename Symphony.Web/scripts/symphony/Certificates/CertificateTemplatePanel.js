﻿Ext.define('Certificates.CertificateTemplatePanel', {
    extend: 'Ext.form.Panel',
    xtype: 'certificates.certificatetemplatepanel',
    requires: [
        'Symphony.SaveCancelBar',
        'Symphony.SearchableTree'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],

    autoScroll: true,
    model: null,
    frame: true,
    border: false,
    header: false,
    margin: 0,
    padding: 0,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    //split: true,
    assumeOwnership: [
        'save-bar',
        'delete-button',
        'preview-button'
    ],
    initComponent: function() {
        var me = this;

        Ext.apply(this, {
            tbar: [{
                xtype: 'symphony.savecancelbar',
                itemId: 'save-bar',
                dock: 'top',
                showManually: true,
                listeners: {
                    save: function () {
                        var root = this.up('[xtype=certificates.certificatetemplatepanel]');
                        root.save();
                    },
                    cancel: function () {
                        var root = this.up('[xtype=certificates.certificatetemplatepanel]');
                        root.close();
                    }
                },

                items: [{
                    xtype: 'button',
                    itemId: 'preview-button',
                    iconCls: 'x-button-preview',
                    text: 'Preview',

                    handler: function () {
                        var target = me.find('name', 'content')[0],
                            height = me.find('name', 'height')[0].value,
                            width = me.find('name', 'width')[0].value,
                            w = Ext.create('certificates.certificatepreview', {
                                isPreview: true,
                                previewCertificate: function(options, callback) {
                                    var path = '/Handlers/CertificateHandler.ashx?',
                                        params = [];

                                    for (var prop in options) {
                                        if (options.hasOwnProperty(prop)) {
                                            params.push(prop + '=' + options[prop]);
                                        }
                                    }

                                    path = path + params.join('&');

                                    Ext.Ajax.request({
                                        url: path,
                                        method: 'post',
                                        params: {
                                            certificateTemplateOverride: encodeURIComponent(JSON.stringify({
                                                content: target.getData(),
                                                bgImagePath: me.find('name', 'bgImagePath')[0].value,
                                                height: height,
                                                width: width
                                            }))
                                        },
                                        success: function (args) {
                                            var html = args.responseText;
                                            var previewWin = new Ext.Window({
                                                border: false,
                                                width: width + 15,
                                                height: height + 33,
                                                layout: 'fit',
                                                
                                                items: [{
                                                    xtype: 'panel',
                                                    html: html,
                                                    autoScroll: true,
                                                    listeners: {
                                                        afterrender: function () {
                                                            var panel = this;

                                                            var submitCallback = function (certHtml) {
                                                                previewWin.setWidth(width + 15);
                                                                previewWin.setHeight(height + 33);
                                                                previewWin.center();
                                                                
                                                                panel.update(certHtml);
                                                            }

                                                            if (Symphony.Certificates.initUserDataForm(submitCallback)) {
                                                                // we have a user data form
                                                                previewWin.setWidth(800);
                                                                previewWin.setHeight(600);

                                                            } else {
                                                                previewWin.setWidth(width + 15);
                                                                previewWin.setHeight(height + 33);
                                                            }
                                                        }
                                                    }
                                                }],
                                                title: me.model.get('name'),
                                                modal: true,
                                                closable: true
                                            });

                                            if (typeof (callback) == 'function') {
                                                callback();
                                            }

                                            previewWin.show();
                                        }
                                    });
                                }
                            });

                        w.show();
                    }
                }, '->', {
                    xtype: 'button',
                    itemId: 'delete-button',
                    iconCls: 'x-button-cross',
                    text: 'Delete',

                    handler: function () {
                        var root = this.up('[xtype=certificates.certificatetemplatepanel]');
                        root.deleteCertificateTemplate();
                    }
                }]
            }],

            defaults: {
                margin: 5
            },
            items: [{
                xtype: 'fieldset',
                title: 'Certificate Information',
                flex: 75,
                region: 'west',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },

                items: [{
                    xtype: 'textfield',
                    name: 'name',
                    fieldLabel: 'Name',
                    allowOnlyWhitespace: false,
                    maxLength: 512
                }, {
                    xtype: 'combobox',
                    itemId: 'customer-field',
                    name: 'customerId',
                    fieldLabel: 'Customer',
                    displayField: 'name',
                    valueField: 'id',
                    queryParam: 'searchText',
                    allowOnlyWhitespace: false,
                    //forceSelection: true,
                    pageSize: 20,
                    store: {
                        type: 'customers',
                        autoLoad: true,
                        listeners: {
                            load: function () {
                                this.insert(0, new customer({
                                    id: 0,
                                    name: 'None'
                                }));
                            }
                        }
                    },
                    listeners: {
                        boxready: function () {
                            var me = this,
                                store = me.getStore(),
                                proxy = store.getProxy(),
                                value = me.getValue();

                            // If the customer is on page 2 of results, try to load them manually and add
                            // them to the store.
                            store.on('load', function () {
                                if (!value) {
                                    return;
                                }
                                if (me.isValid()) {
                                    return;
                                }

                                customer.load(value, {
                                    success: function (record) {
                                        store.insert(1, record);
                                        me.setValue(value);
                                    }
                                });
                            }, store, { single: true });

                            // Hack or the combo won't populate; forceSelection will clear it before the
                            // store has had a chance to load.
                            this.forceSelection = true;
                        }
                    }
                }, {
                    xtype: 'combobox',
                    name: 'certificateType',
                    fieldLabel: 'Type',
                    valueField: 'id',
                    allowOnlyWhitespace: false,
                    forceSelection: true,
                    editable: false,
                    queryMode: 'local',
                    store: {
                        type: 'certificateTypes'
                    }
                }, {
                    xtype: 'textfield',
                    name: 'description',
                    fieldLabel: 'Description',
                    maxLength: 512,

                    listeners: {
                        change: function () {
                            var root = this.up('[xtype=certificates.certificatetemplatepanel]');
                            root.refreshContentSize();
                        }
                    }
                }, {
                    xtype: 'numberfield',
                    itemId: 'width-field',
                    name: 'width',
                    fieldLabel: 'Width',
                    minValue: 100,
                    maxValue: 2000,
                    value: 670,
                    allowOnlyWhitespace: false,
                    allowDecimals: false,
                    allowExponential: false,

                    listeners: {
                        change: function () {
                            var me = this.up('[xtype=certificates.certificatetemplatepanel]');

                            me.refreshContentSize();
                        }
                    }
                }, {
                    xtype: 'numberfield',
                    itemId: 'height-field',
                    name: 'height',
                    fieldLabel: 'Height',
                    minValue: 100,
                    maxValue: 2000,
                    value: 910,
                    allowOnlyWhitespace: false,
                    allowDecimals: false,
                    allowExponential: false
                }, {
                    xtype: 'compositefield',
                    anchor: '100%',
                    fieldLabel: 'BG Image',
                    border: false,
                    //name: 'bgImage',
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: 'BG Image',
                        name: 'bgImage',
                        flex: 1
                    }, {
                        xtype: 'button',
                        text: '...',
                        handler: function () {
                            var selectedAsset;
                            var root = this.up('[xtype=certificates.certificatetemplatepanel]');

                            var w = new Ext.Window({
                                title: 'Assets',
                                tbar: {
                                    items: [{
                                        xtype: 'button',
                                        text: 'Set as BG',
                                        handler: function () {
                                            var bgNameField = root.find('name', 'bgImage')[0];
                                            bgNameField.setValue(selectedAsset.get('name'));

                                            var bgIdField = root.find('name', 'bgImageId')[0];
                                            bgIdField.setValue(selectedAsset.get('id'));

                                            var bgPathField = root.find('name', 'bgImagePath')[0];
                                            bgPathField.setValue(selectedAsset.get('path'));

                                            var ck = root.find('name', 'content')[0];
                                            if (ck.xtype === 'symphony.ckeditorfield') {
                                                debugger;
                                                ck.setBackgroundImage(selectedAsset.get('path'));
                                            }

                                            w.hide();
                                            w.destroy();
                                        }
                                    }]
                                },
                                modal: true,
                                width: 500,
                                height: 500,
                                layout: 'fit',
                                items: [{
                                    xtype: 'symphony.assetsgrid',
                                    title: 'Assets',
                                    model: '',
                                    allowUploads: false,
                                    assetsUploadUrl: '/Uploaders/CertificateAssetsUploader.ashx',    // handler to save the asset
                                    assetsUrl: '/services/certificate.svc/certificates/assets',
                                    dblClickHandler: function (e) {
                                        console.log("asset dbl click", e);
                                    },
                                    listeners: {
                                        assetselected: function (rec) {
                                            selectedAsset = rec;
                                            console.log("ASSET SELECTED: ", selectedAsset);
                                        }
                                    }
                                }]
                            });

                            w.show();
                        }
                    }, {
                        xtype: 'button',
                        text: 'Remove BG',
                        handler: function () {
                            var root = this.up('[xtype=certificates.certificatetemplatepanel]');

                            var bgNameField = root.find('name', 'bgImage')[0];
                            bgNameField.setValue(null);

                            var bgIdField = root.find('name', 'bgImageId')[0];
                            bgIdField.setValue(null);

                            var bgPathField = root.find('name', 'bgImagePath')[0];
                            bgPathField.setValue(null);

                            var ck = root.find('name', 'content')[0];
                            if (ck.xtype === 'symphony.ckeditorfield') {
                                ck.setBackgroundImage(null);
                            }
                        }
                    }]
                }, {
                    xtype: 'hidden',
                    name: 'bgImageId'
                }, {
                    xtype: 'hidden',
                    name: 'bgImagePath'
                }, {
                    xtype: 'symphony.ckeditorfield',
                    itemId: 'content-field',
                    certificateId: me.model.get('id'),
                    flex: 1,
                    name: 'content',
                    fieldLabel: 'Content'
                }]
            }, {
                xtype: 'panel',
                width: 50,
                cls: 'x-panel-transparent',
                layout: {
                    type: 'vbox',
                    pack: 'center',
                    align: 'center',
                    defaultMargins: 2
                },
                disabled: this.isShared,
                border: false,
                items: [{
                    xtype: 'button',
                    text: '&nbsp;&nbsp;<<&nbsp;&nbsp;',
                    handler: function () {
                        var target = me.find('name', 'content')[0];
                        var templateParameters = me.queryById("parameters-panel");
                        templateParameters.addTemplateParameter(target);
                    }
                }]
            }, {
                xtype: 'fieldset',
                title: 'Template Parameters',
                flex: 25,
                layout: 'fit',
                items: [{
                    xtype: 'symphony.templateparametertree',
                    itemId: 'parameters-panel'
                }]
            }]
        });

        me.callParent();

        if (me.model.get('customerId') == null) {
            me.model.set('customerId', 0);
        }

        me.setParameters();

        if (!me.model) {
            Log.error('Model property is undefined.');
            return;
        }

        Log.watchModel(me, me.model);

        me.setTitle(me.model.get('name'));
        if (!me.model.phantom) {
            me.getForm().setValues(me.model.getData());
        } else {
            me.queryById('delete-button').hide();
        }

        // ExtJS 4.0 bug, combo boxes send their "query" both in the "query/searchText" url
        // parameter and as a filter. Our server can't deal with null filter values, which is the
        // default before any query is typed in.
        //
        // Since this is a combo box, we aren't using any other filters, so we change the filter
        // param to something the server won't attempt to parse. I think this is fixed in 4.2.1.
        me.queryById('customer-field').getStore().proxy.filterParam ='_filter';
    },

    /**
     * Refreshes the size of the certificate's content area. The values are
     * determined from the values of the width and height fields.
     */
    refreshContentSize: function() {
        var me = this,
            widthField = me.queryById('width-field'),
            heightField = me.queryById('height-field'),
            contentField = me.queryById('content-field'),
            w = widthField.getValue(),
            h = heightField.getValue(),
            msg;

        if (!widthField.isValid()) {
            Log.info('Aborting resize, invalid value for width: ' + w);
            return;
        }

        if (!heightField.isValid()) {
            Log.info('Aborting resize, invalid value for height: ' + h);
            return;
        }

        Log.info(Ext.String.format('Resizing to ({0}px, {1}px).', w, h));

        contentField.setContentSize(w, h);
    },

    /**
     * Saves a certificate template to the server.
     * @param {Function} callback Callback to invoke after the operation is
     * complete.
     */
    save: function(callback) {
        var me = this,
            saveBar = me.queryById('save-bar'),
            contentField = me.queryById('content-field'),
            form = me.getForm(),
            model,
            data;

        saveBar.showSaving();
        Log.info('Saving certificate template.');

        if (!form.isValid()) {
            saveBar.hideSaving();
            saveBar.showNotSaving();

            Log.error('Cannot submit because form is in an invalid state.');
            if (callback) {
                callback(false);
            }
            return;
        }

        contentField.commit();

        data = me.form.getValues();
        me.model.set(data);
        if (me.model.get('customerId') == 0) {
            me.model.set('customerId', null);
        }

        Log.debug('Updating model data to save.', me.model);

        me.model.save({
            callback: function() {
                console.log(arguments);
            },
            success: Ext.Function.createOwned(me, function() {
                Log.info('Model was successfully saved.');

                me.queryById('delete-button').show();
                me.fireEvent('change', me.model);

                if (callback) {
                    callback(true);
                }
            }),
            failure: Ext.Function.createOwned(me, function(model, operation) {
                Log.error('Failed to save model.');

                if (operation.error) {
                    saveBar.showError(Ext.String.format('Save failed: {0}  {1}', operation.error.status, operation.error.statusText));
                } else {
                    saveBar.showError('Save failed: Unknown error (status code 200)');
                }

                if (callback) {
                    callback(false);
                }
            })
        });
    },

    /**
     * Deletes a certificate template from the server.
     */
    deleteCertificateTemplate: function() {
        var me = this,
            model,
            data;

        Log.debug('Asking user for confirmation to delete certificate template.');

        Ext.Msg.confirm('Confirm deletion', 'Are you sure you want to delete this certificate template? This action cannot be undone.', Ext.Function.createOwned(me, function(result) {
            if (result != 'yes') {
                Log.debug('User canceled delete operation.');
                return;
            }

            Log.info('Deleting certificate template.');

            me.model.destroy({
                success: Ext.Function.createOwned(me, function() {
                    Log.info('Model was successfully destroyed.');

                    me.fireEvent('change', me.model);
                    me.close();
                }),
                failure: Ext.Function.createOwned(me, function(model, operation) {
                    Log.error('Failed to destroy model.');

                    if (operation.error) {
                        saveBar.showError(Ext.String.format('Save failed: {0}  {1}', operation.error.status, operation.error.statusText));
                    } else {
                        saveBar.showError('Save failed: Unknown error (status code 200)');
                    }
                })
            });
        }));
    },

    setParameters: function() {
        var me = this,
            parametersPanel = me.queryById('parameters-panel');

        if (!Symphony.certificateParameters) {
            Ext.Ajax.request({
                url: '/services/certificate.svc/certificates/parameters',
                method: 'GET',
                callback: function(request, success, xhr) {
                    var data = Ext.decode(xhr.responseText);

                    Symphony.certificateParameters = data.data;

                    me.setParameters();
                }
            });

            return;
        }
        
        parametersPanel.displayTemplateParameters(Symphony.certificateParameters);
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});


