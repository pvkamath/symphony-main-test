﻿Symphony.Certificates.pathToLevelIndentText = function(path) {
    path = path.replace('\\Certificates\\', '')
                .replace(/\\/g, ' > ');

    return path;
}

Symphony.Certificates.getCertificateEditorWindow = function (afterSelect, certificateId) {
     return new Ext.Window({
         title: 'Select a Certificate',
         width: 250,
         height: 400,
         layout: 'border',
         border: false,
         modal: true,
         items: [{
             region: 'center',
             xtype: 'certificates.certificateeditorpanel',
             selectedCertificateId: certificateId,
             afterSelect: function (node) {
                 afterSelect(node);
             }
         }]
     });
}
Symphony.Certificates.CertificateEditorPanel = Ext.define('certificates.certificateeditorpanel', {
    alias: 'widget.certificates.certificateeditorpanel',
    extend: 'Ext.Panel',
    selectedNode: null,
    selectedCertificateId: null,
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            layout: 'fit',
            items: [{
                border: false,
                xtype: 'certificates.certificateeditortree',
                iconCls: this.iconCls,
                title: "Certificates",
                customerId: this.customerId,
                split: true,
                ref: 'certificatetree',
                listeners: {
                    itemdblclick: function (tree, record, item, index, e) {
                        if (record) {
                            if (typeof (me.afterSelect) === 'function') {
                                me.afterSelect(record);
                            }
                        }
                    },
                    itemclick: function (tree, record, item, index, e) {
                        if (record) {
                            me.selectedNode = record;
                            me.selectedCertificateId = record.get('certificateId');
                        }
                    }
                }
            }],
            bbar: {
                items: ['->', {
                    xtype: 'button',
                    iconCls: 'x-button-save',
                    text: 'Select',
                    ref: '../selectBtn',
                    handler: function () {
                        var node = me.selectedNode;
                        if (node) {
                            if (node.attributes.record.get('id') > 0) {
                                if (typeof (me.afterSelect) === 'function') {
                                    me.afterSelect(node);
                                }
                            }
                        } else {
                            Ext.Msg.alert("Select a Certificate", "Please select a certificate from the tree.");
                        }
                    }
                }]
            }
        });
        this.callParent(arguments);
    }
});
