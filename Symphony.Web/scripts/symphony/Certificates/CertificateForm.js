﻿Symphony.Certificates.CertificateForm = Ext.define('certificates.certificateform', {
    alias: 'widget.certificates.certificateform',
    extend: 'Ext.Panel',
    record: null,
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            frame: false,
            bodyCssClass: 'x-panel-mc',
            border: false,
            bodyStyle: 'padding: 10px',
            items: [{
                border: false
            }]
        });
        this.callParent(arguments);
    },
    onRender: function () {
        //Do something to show a demo certificate
        Symphony.Certificates.CertificateForm.superclass.onRender.apply(this, arguments);
    }
});
