﻿(function () {
    Symphony.Artisan.ImageQuestionForm = Ext.define('artisan.imagequestionform', {
        alias: 'widget.artisan.imagequestionform',
        extend: 'Ext.Panel',
        content: '',
        lastContent: '',
        reset: false,
        page: null,
        node: null,
        cropperId: 'crop_' + (new Date()).getTime(),
        initComponent: function () {
            var me = this;

            if (this.page.answers && this.page.answers.length > 0) {
                this.answer = Ext.decode(this.page.answers[0].text);
                if (!Ext.isArray(this.answer.dimensions)) {
                    // backwards compatibility
                    this.answer.dimensions = [this.answer.dimensions];
                }
            }
            if (!this.page.answers || this.page.answers.length == 0 || !this.answer.dimensions) {
                // give a default if no answers exist yet
                this.page.answers = this.page.lastAnswers = [{
                    id: -1,
                    text: Ext.encode({
                        imageUrl: '/images/artisan/image_question_placeholder.gif',
                        dimensions: [{
                            x: 10,
                            y: 10,
                            width: 175,
                            height: 175
                        }]
                    }),
                    isCorrect: true
                }];
                this.answer = Ext.decode(this.page.answers[0].text);
            }

            Ext.apply(this, {
                frame: true,
                labelAlign: 'top',
                border: false,
                items: [{
                    layout: 'column',
                    border: false,
                    cls: 'x-panel-transparent',
                    items: [{
                        columnWidth: 0.5,
                        layout: 'form',
                        labelAlign: 'top',
                        cls: 'x-panel-transparent',
                        border: false,
                        items: [{
                            xtype: 'label',
                            cls: 'x-form-item',
                            html: 'Directions: Enter the question text below, then adjust the cropper within the image to select the "correct" area.'
                        }, {
                            name: 'html',
                            xtype: 'textarea',
                            anchor: '100%',
                            height: 125,
                            enableKeyEvents: true,
                            fieldLabel: 'Question',
                            value: me.page.html,
                            ref: '../../questionEditor',
                            listeners: {
                                keyup: function (field, e) {
                                    me.fireEvent('contentchange', me.questionEditor.getValue(), me.getAnswers(), me.correctResponse.getValue(), me.incorrectResponse.getValue());
                                }
                            }
                        }, {
                            name: 'correctResponse',
                            xtype: 'textarea',
                            fieldLabel: 'Correct Response',
                            value: me.page.correctResponse || 'Correct.',
                            anchor: '100%',
                            ref: '../../correctResponse',
                            enableKeyEvents: true,
                            listeners: {
                                keyup: function (field, e) {
                                    me.fireEvent('contentchange', me.questionEditor.getValue(), me.getAnswers(), field.getValue(), me.incorrectResponse.getValue());
                                }
                            }
                        }, {
                            name: 'incorrectResponse',
                            xtype: 'textarea',
                            fieldLabel: 'Incorrect Response',
                            value: me.page.incorrectResponse || 'Incorrect.',
                            anchor: '100%',
                            ref: '../../incorrectResponse',
                            enableKeyEvents: true,
                            listeners: {
                                keyup: function (field, e) {
                                    me.fireEvent('contentchange', me.questionEditor.getValue(), me.getAnswers(), me.correctResponse.getValue(), field.getValue());
                                }
                            }
                        }]
                    }, {
                        columnWidth: 0.5,
                        bodyStyle: 'padding-left:15px',
                        cls: 'x-panel-transparent',
                        border: false,
                        items: [{
                            border: false,
                            items: [{
                                xtype: 'symphony.imagecropper',
                                height: 400,
                                width: 400,
                                preserveRatio: false,
                                // initial cropper size
                                cropData: this.answer.dimensions,
                                /*initialWidth: this.answer.dimensions.width,
                                initialHeight: this.answer.dimensions.height,
                                initialX: this.answer.dimensions.x,
                                initialY: this.answer.dimensions.y,*/
                                src: this.answer.imageUrl,
                                ref: '../../../cropper',
                                listeners: {
                                    change: function (box, dims) {
                                        me.answer.dimensions = dims;
                                        me.fireEvent('contentchange', me.questionEditor.getValue(), me.getAnswers(), me.correctResponse.getValue(), me.incorrectResponse.getValue());
                                    }
                                }
                            }],
                            tbar: {
                                items: [{
                                    xtype: 'button',
                                    text: 'Choose Image',
                                    handler: function () {
                                        Symphony.Artisan.AssetPicker.getInstance({
                                            filter: 'png|jpg|jpeg|gif'
                                        }, function (asset) {
                                            me.cropper.setImage(asset.path, asset.naturalWidth, asset.naturalHeight);
                                            me.answer.imageUrl = asset.path;
                                            me.fireEvent('contentchange', me.questionEditor.getValue(), me.getAnswers(), me.correctResponse.getValue(), me.incorrectResponse.getValue());
                                        }).show();
                                    }
                                }, {
                                    xtype: 'button',
                                    text: 'Add Region',
                                    handler: function () {
                                        me.cropper.addCropper();
                                    }
                                }, {
                                    xtype: 'button',
                                    text: 'Reset Regions',
                                    handler: function () {
                                        me.cropper.reset();
                                    }
                                }]
                            }
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        getAnswers: function () {
            return [{
                text: Ext.encode(this.answer),
                isCorrect: true,
                sort: 0
            }];
        },
        getValues: function () {
            return {
                question: this.questionEditor.getValue(),
                answers: this.getAnswers(),
                correctResponse: this.correctResponse.getValue(),
                incorrectResponse: this.incorrectResponse.getValue()
            };
        },
        setValue: function (question, answers) {
            this.questionEditor.setValue(question);
            if (answers.length && answers[0].text) {
                var ans = Ext.decode(answers[0].text);
                this.cropper.setCropData([ans.dimensions]);
                this.cropper.setImage(ans.imageUrl);
            }
        }
    });

})();