﻿(function () {
	Symphony.Artisan.ImporterType = Ext.define('artisan.importertype', {
	    alias: 'widget.artisan.importertype',
	    extend: 'Ext.form.FormPanel',
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				border: false,
				bodyStyle: 'padding:15px',
				title: 'Select Import Type...',
				items: [{
					xtype: 'fieldset',
					title: 'Import Type',
					labelWidth: 250,
					defaults: {
                        labelWidth: 250
					},
					items: [{
						xtype: 'radio',
						fieldLabel: 'Documents (PowerPoint, Word or PDF)',
						name: 'inputType',
						checked: true,
						ref: '../document',
						listeners: {
							check: function (box, checked) {
								if (checked) {
									me.fireEvent('typeSelected', 'document');
									me.options.enable();
								}
							}
						}
					}, {
						xtype: 'radio',
						fieldLabel: 'Existing Course',
						name: 'inputType',
						ref: '../existingCourse',
						listeners: {
							check: function (box, checked) {
								if (checked) {
									me.fireEvent('typeSelected', 'existingCourse');
									me.options.disable();
								}
							}
						}
					}]
				}, {
					xtype: 'fieldset',
					name: 'options',
					title: 'Options',
					ref: 'options',
					layout: {
					    type: 'hbox',
					    defaultMargins: {
					        top: 5,
					        right: 20,
					        bottom: 5,
					        left: 0
					    }
					},
					items: [{
						xtype: 'fieldset',
						name: 'dimensions',
						title: 'Dimensions',
						ref: '../dimensions',
                        labelWidth: 80,
						defaults: {
						    labelWidth: 80
						},
						items: [{
							xtype: 'numberfield',
							fieldLabel: 'Width',
							name: 'width',
							ref: '../../widthField'
						}, {
							xtype: 'numberfield',
							fieldLabel: 'Height',
							name: 'height',
							ref: '../../heightField'
						}]
					}, {
						xtype: 'fieldset',
						name: 'scaleType',
						title: 'Scale Type',
                        width: 150,
						items: [{
							xtype: 'radio',
							fieldLabel: 'Shrink To Fit',
							name: 'scaleType',
							ref: '../../shrinkToFit',
							listeners: {
								check: function (box, checked) {
									if (checked) {
										me.cropStart.disable();
									}
								}
							}
						}, {
							xtype: 'radio',
							fieldLabel: 'Crop',
							name: 'scaleType',
							ref: '../../crop',
							checked: true,
							listeners: {
								check: function (box, checked) {
									if (checked) {
										me.cropStart.enable();
									}
								}
							}
						}, {
							xtype: 'fieldset',
							title: 'Crop Start',
							ref: '../../cropStart',
							labelWidth: 20,
							defaults: {
							    labelWidth: 20,
							    width: 105
							},
							items: [{
								xtype: 'numberfield',
								fieldLabel: 'X',
								name: 'cropX',
								ref: '../../../cropX',
								value: 0
								
							}, {
								xtype: 'numberfield',
								fieldLabel: 'Y',
								name: 'cropY',
								ref: '../../../cropY',
								value: 0
							}]
						}]
					}]
				}]
			});

			this.callParent(arguments);
		},
		getChecked: function () {
			if (this.document.checked) {
				return 'document';
			} else if (this.existingCourse.checked) {
				return 'existingCourse';
			}
		},
		getWidth: function () {
			return this.widthField.getValue();
		},
		getHeight: function () {
			return this.heightField.getValue();
		},
		getScaleType: function () {
			if (this.crop.checked) {
				return 'crop';
			} else if (this.shrinkToFit.checked) {
				return 'shrinkToFit';
			}
		},
		getCropX: function () {
			return this.cropX.getValue();
		},
		getCropY: function () {
			return this.cropY.getValue();
		}
	});

})();