﻿(function () {
	Symphony.Artisan.MultipleChoiceForm = Ext.define('artisan.multiplechoiceform', {
	    alias: 'widget.artisan.multiplechoiceform',
	    extend: 'artisan.questionanswerform',
		singleCorrect: true
	});

})();