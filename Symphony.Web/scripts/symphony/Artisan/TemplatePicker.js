﻿(function () {
	Symphony.Artisan.TemplatePicker = Ext.define('artisan.templatepicker', {
	    alias: 'widget.artisan.templatepicker',
	    extend: 'Ext.Window',
		selected: null,
		type: 0,
		pageSize: 36,
		hidePreview: true,
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				layout: 'border',
				title: 'Select a Template',
				height: 540,
				width: (this.hidePreview ? 590 : 850),
				defaults: {
					border: false
				},
				buttons: [{
					text: 'OK',
					itemId: 'okButton',
					disabled: true,
					handler: function () {
						me.fireEvent('templateselect', me.selected);
						me.close();
					}
				}, {
					text: 'Cancel',
					handler: function () {
						me.close();
					}
				}]
			});
			this.items = [{
				id: 'artisan.templategrid',
                xtype: 'artisan.templategrid',
				pageSize: this.pageSize,
				type: this.type,
				listeners: {
				    templateSelected: function (selected, go) {
				        var fbar = me.getDockedItems('[dock=bottom]')[0];
                        me.selected = selected;
					    fbar.getComponent('okButton').setDisabled(!selected);
						me.updatePreview();
						if (go === true) {
							me.fireEvent('templateselect', me.selected);
							me.close();
						}
					}
				}
			}];
			if (this.hidePreview) {
				Ext.apply(this.items[0], {
					region: 'center'
				});
			} else {
				Ext.apply(this.items[0], {
					region: 'west',
					width: 350,
					split: true
				});
				this.items.push({
					region: 'center',
					bodyStyle: 'padding: 15px',
					itemId: 'preview',
					title: 'Preview'
				});
			}
			this.callParent(arguments);
		},
		updatePreview: function () {
			var preview = this.getComponent('preview');
			if (preview) {
				if (this.selected) {
					preview.update(this.selected.html);
				} else {
					preview.update('');
				}
			}
		}
	});

})();