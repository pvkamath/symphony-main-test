﻿(function () {

	Symphony.Artisan.CourseAccordian = Ext.define('artisan.courseaccordian', {
	    alias: 'widget.artisan.courseaccordian',
	    extend: 'Ext.Panel',
		course: null,
		initComponent: function () {
			var me = this;

			var bubble = function (evt) {
					return function () {
						me.fireEvent.apply(me, [evt].concat(Array.prototype.slice.call(arguments, 0)));
					};
				};
			Ext.apply(this, {
				title: 'Course Management',
				layout: 'fit',
				defaults: {
					border: false,
					collapsible: true,
					titleCollapse: false,
					floatable: false
				},
				items: [{
					region: 'center',
					ref: 'courseLayout',
					layout: 'fit',
					course: me.course,
					collapsible: false,
					xtype: 'artisan.courselayout',
					listeners: {
						addpage: bubble('addpage'),
						editpage: bubble('editpage'),
						editsection: bubble('editsection'),
						editcourse: bubble('editcourse'),
						nodeselected: bubble('nodeselected'),
						renamepage: bubble('renamepage'),
						addsection: bubble('addsection'),
						deletepage: bubble('deletepage'),
                        deletesection: bubble('deletesection')
					}
				}]
			});
			this.callParent(arguments);
		},
		setSaved: function () {
			this.courseLayout.setSaved();
		}
	});

})();