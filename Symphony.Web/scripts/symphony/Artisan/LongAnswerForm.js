﻿(function () {
	Symphony.Artisan.LongAnswerForm = Ext.define('artisan.longanswerform', {
	    alias: 'widget.artisan.longanswerform',
	    extend: 'Ext.form.FormPanel',
		content: '',
		lastContent: '',
		reset: false,
		page: null,
		node: null,
		initComponent: function () {
		    var me = this;

		    Ext.apply(this, {
				frame: true,
				labelAlign: 'top',
				items: [{
					xtype: 'label',
					cls: 'x-form-item',
					html: 'Directions: Enter the question text below. This is a free form answer that must be reviewed by an instructor to be marked as correct or incorrect. You can add an example of a correct response to aid marking.'
				}, {
					name: 'body',
					xtype: 'symphony.spellcheckarea',
					labelAlign: 'top',
					anchor: '100%',
					height: 95,
					enableKeyEvents: true,
					fieldLabel: 'Question',
					ref: 'questionEditor',
					value: me.page.html,
					listeners: {
					    keyup: function (field, e) {
					        var question = me.questionEditor.getValue();
					        var answer = me.getAnswer();
					        var correctResponse = me.correctResponse.getValue();

					        me.fireEvent('contentchange', question, answer, correctResponse, 'N/A');
						}
					}
				}, {
				    name: 'responseExample',
				    xtype: 'symphony.spellcheckarea',
				    labelAlign: 'top',
				    anchor: '100%',
				    height: 180,
				    enableKeyEvents: true,
				    fieldLabel: 'Correct Response Example',
				    ref: 'responseExample',
				    value: me.page.answers && me.page.answers.length > 0 ? me.page.answers[0].text : '',
				    listeners: {
				        keyup: function (field, e) {
				            var question = me.questionEditor.getValue();
				            var answer = me.getAnswer();
				            var correctResponse = me.correctResponse.getValue();

				            me.fireEvent('contentchange', question, answer, correctResponse, 'N/A');
				        }
				    }
				}, {
				    xtype: 'symphony.spellcheckarea',
				    fieldLabel: 'Response',
				    value: me.page.correctResponse || 'Your assignment has been received and an instructor will review your submission.  You may now continue with the course, however the Final Exam will remain locked until you receive an email from your instructor indicating your assignment has been accepted and approved.',
				    anchor: '100%',
				    ref: 'correctResponse',
				    enableKeyEvents: true,
				    listeners: {
				        keyup: function (field, e) {
				            var question = me.questionEditor.getValue();
				            var answer = me.getAnswer();
				            var correctResponse = me.correctResponse.getValue();

				            me.fireEvent('contentchange', question, answer, correctResponse, 'N/A');
				        }
				    }
				}]
			});
			this.callParent(arguments);
		},
		getAnswer: function() {
		    return [{
		        sort: 0,
		        isCorrect: true,
		        text: this.responseExample.getValue()
		    }]
		},
		getValues: function () {
			return {
			    question: this.questionEditor.getValue(),
			    answers: this.getAnswer(),
			    correctResponse: this.correctResponse.getValue(),
			    incorrectResponse: 'N/A'
			};
		},
		setValue: function (question, answers, correctResponse, incorrectResponse) {
		    this.questionEditor.setValue(question);
			this.fireEvent('contentchange', question, answers, correctResponse, incorrectResponse);
		}
	});

})();