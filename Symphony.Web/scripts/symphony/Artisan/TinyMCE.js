﻿(function () {
    tinyMCE.init({
        mode: 'none',
        theme: 'advanced',
        skin: 'o2k7',
        doctype: '',

        forced_root_block: '',
        // Needed for 3.x
        //doctype : '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
        plugins: 'lists,inlinepopups,spellchecker,searchreplace,paste,table,insertdatetime,print,preview',
        //,advimagescale',
        theme_advanced_buttons1: 'spchk,search,replace,|,copy,cut,paste,pasteword,|,undo,redo,|,link,unlink,|,justifyleft,justifycenter,justifyright,justifyfull,|,indent,outdent,blockquote',
        theme_advanced_buttons2: 'tablecontrols,blockquote,charmap,|,formatselect,fontselect,fontsizeselect',
        //,styleselect
        theme_advanced_buttons3: 'bold,italic,underline,strikethrough,|,cleanup,removeformat,forecolor,backcolor,|,numlist,bullist,|,sub,sup,|,hr,|,insertdate,inserttime,|,print,|,code,|,asset,preview2,parameter',
        theme_advanced_toolbar_location: 'top',
        theme_advanced_toolbar_align: 'left',
        theme_advanced_resizing: false,
        valid_children: "+body[style],+div[style]",
        extended_valid_elements: 'style[type],script[type],video[width|height|id|class|controls|preload|data-setup],source[src|type],iframe[src|width|height|name|align|style|border|frameBorder],object[width|height|classid|codebase],param[name|value],embed[src|type|width|height|flashvars|wmode],audio[width|height|id|class|controls|preload|data-setup]',
        paste_postprocess: function (pl, o) {
            if (o.node.innerHTML && o.node.innerHTML.indexOf('&nbsp;') == 0) {
                o.node.innerHTML = o.node.innerHTML.replace('&nbsp;', '');
            }
            Ext.getCmp(pl.editor.editorId).ownerCt.fireEvent('paste', this);
        },
        //spellchecker_rpc_url: 'https://www.google.com/tbproxy/spell?lang=en&hl=en',
        setup: function (ed) {
            ed.addButton('asset', {
                title: 'Insert Asset',
                image: '/images/picture_add.png',
                onclick: function () {
                    Ext.getCmp(ed.editorId).ownerCt.fireEvent('insertasset', this);
                }
            });

            ed.addButton('parameter', {
                title: 'Insert Parameter',
                image: '/images/script_code_red.png',
                onclick: function () {
                    Ext.getCmp(ed.editorId).ownerCt.fireEvent('insertparameter', this);
                }
            });

            ed.addButton('spchk', {
                title: 'Check',
                image: '/images/spellcheck.png',
                onclick: function () {
                    var html = ed.spellChecker.SpellButton();
                    var event = html.match(/onclick="(.+?)"/)[1];
                    eval(event);
                }
            });
            ed.onMouseUp.add(function (ed, e) {
                var node = ed.selection ? ed.selection.getNode() : ed.lastSelectedNode;
                if (!node || !node.className) {
                    return;
                }
                if (node.className.indexOf('element_media') > -1 || node.className.indexOf('element_text') > -1) {
                    ed.lastSelectedNode = node;
                }
            });

            /*var downTime = null;
            ed.onMouseDown.add(function(ed, e){
            if(downTime != null && (new Date()).getTime() - downTime < 200){
            // double-click
            // check the item selected; if it, or any of it's anscenstors, include the classes we're monitoring, find it, and select the first child
            var el = e.target;
            do{
            if(el.className && (el.className.indexOf('element_media') > -1 || el.className.indexOf('element_text') > -1)){
            ed.selection.select(el.firstChild);
            break;
            }
            el = el.parentNode;
            }while(el != null && el.nodeName != 'IFRAME')
            }
            downTime = (new Date()).getTime();
            });
            ed.onMouseUp.add(function(ed, e){
            var contents = e.target.innerHTML.toUpperCase().trim();
            var placeholders = ['HEADER','CONTENT','MEDIA'];
            for(var i = 0; i < placeholders.length; i++){
            if(contents == placeholders[i]){
            ed.selection.select(e.target);
            }else if(e.target.firstChild && e.target.firstChild.innerHTML && e.target.firstChild.innerHTML.toUpperCase().trim() == placeholders[i]){
            ed.selection.select(e.target.firstChild);
            }
            }
            });
            */

            //            ed.onMouseUp.add(function(ed, e){
            //                var contents = e.target.innerHTML.toUpperCase().trim();
            //                var placeholders = ['HEADER','CONTENT','MEDIA'];
            //                for(var i = 0; i < placeholders.length; i++){
            //                    if(contents == placeholders[i]){
            //                        ed.selection.select(e.target);
            //                    }else if(e.target.firstChild && e.target.firstChild.innerHTML && e.target.firstChild.innerHTML.toUpperCase().trim() == placeholders[i]){
            //                        ed.selection.select(e.target.firstChild);
            //                    }
            //                }
            //            });
            ed.addButton('preview2', {
                title: 'Preview',
                image: '/images/magnifier.png',
                onclick: function () {
                    Ext.getCmp(ed.editorId).ownerCt.fireEvent('preview2', this);
                }
            });

            ed.onPostProcess.add(function (ed, o) {
                // links to messageboards are absolute
                // TODO: any other deep links will need to be added here
                o.content = o.content.replace('../../messageboard', '/messageboard');
            });
        },
        init_instance_callback: function () {
            var owner = Ext.getCmp(this.editorId).ownerCt;
            owner.fireEvent('tinymceinit', this);

            var mceParameterBtn = Ext.query('.mce_parameter')[0];
            mceParameterBtn.className = mceParameterBtn.className + " " + Symphony.Modules.getModuleClass(Symphony.Modules.Parameters);

            // create the spell checker now that we've got the iframe instance
            this.spellChecker = new LiveSpellInstance();
            this.spellChecker.Fields = Ext.query('iframe', owner.id)[0].id; //'ext-comp-1421_ifr';//ed.editorId;//fields;
            this.spellChecker.ServerModel = 'aspx';
        }
    });

    Symphony.Artisan.TinyMCE = Ext.define('artisan.tinymce', {
        alias: 'widget.artisan.tinymce',
        extend: 'Ext.Panel',
        cssText: '',
        defaultThemeCssPath: '/skins/artisan_content/editor.css?2',
        artisanDefaultThemeCssPath: '/Handlers/ArtisanSkinHandler.ashx?href=' + window.location.protocol + '//' + window.location.host + '/css/artisan.default.css',
        // default values; adds borders, etc around elements
        themeCssPath: '',
        skinCssPath: '',
        // overrides for a specific theme
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                ref: 'wrap',
                items: [{
                    border: false,
                    listeners: {
                        render: function () {
                            //tinyMCE.idCounter=0;
                            tinyMCE.execCommand('mceAddControl', false, this.id);
                        },
                        beforedestroy: function () {
                            tinyMCE.execCommand('mceRemoveControl', false, this.id);
                        }
                    }
                }],
                resizeEditor: function (width, height) {
                    var layout = Ext.query('table.mceLayout', this.el.dom)[0];
                    if (layout) {
                        width = width > 0 ? width : 0;
                        height = height > 0 ? height : 0;

                        layout.style.width = width + 'px';
                        layout.style.height = height + 'px';
                        var iframe = Ext.query('.mceIframeContainer > iframe', layout)[0];
                        var toolbar = Ext.query('.mceToolbar', layout)[0];
                        if (iframe) {
                            try {
                                iframe.style.width = width + 'px';
                                iframe.style.height = (height - (toolbar ? toolbar.offsetHeight + 3 : 0)) + 'px';
                            } catch (e) { }
                        }
                    }
                },
                listeners: {
                    tinymceinit: function (editor) {
                        this.editor = editor;
                        this.resizeEditor(this.getWidth(), this.getHeight());
                        if (this.content) {
                            this.setContent(this.content);

                            if (this.cssText) {
                                this.setCssText(this.cssText);
                            }
                            if (this.reset) {
                                this.resetNodes();
                            }
                            this.attachLabels();
                            this.fireContentChange();
                        }
                        this.editor.onChange.add(Ext.bind(this.onContentChange, this));
                        this.editor.onKeyPress.add(Ext.bind(this.onContentChange, this));

                        window.setTimeout(Ext.bind(function () {
                            if (this.defaultThemeCssPath) {
                                this.loadCss(this.defaultThemeCssPath);
                            }

                            if (this.artisanDefaultThemeCssPath) {
                                this.loadCss(this.artisanDefaultThemeCssPath);
                            }

                            if (this.themeCssPath) {
                                this.loadCss(this.themeCssPath);
                            }

                            if (this.skinCssPath) {
                                this.loadCss(this.skinCssPath);
                            }
                        }, this));
                        //this.fixIE();

                    },
                    resize: function (container, adjWidth, adjHeight, rawWidth, rawHeight) {
                        this.resizeEditor(adjWidth, adjHeight);
                    },
                    insertparameter: function () {
                        // AMTODO Why does insert access use lastSelectedNode or getNode? Seems like getNode on it's own works better
                        var el = this.editor.selection.getNode();

                        var body = this.editor.dom.select('body')[0];
                        var span = this.editor.dom.doc.createElement('span');
                        body.appendChild(span);
                        this.editor.selection.select(span);
                        Symphony.Artisan.ParameterPicker.getInstance({}, function (parameter) {
                            try {
                                var html = new Ext.XTemplate('{code}').apply(parameter);
                                el.innerHTML = el.innerHTML + html;
                                // doesn't work in IE so we have to use the inner html method above
                                // fme.insertContent(html);
                                body.removeChild(span);
                                me.fireContentChange();
                            } catch (e) {
                                Ext.Msg.alert('Error', 'Your parameter cannot be inserted at the selected location. Please select a valid content area.');
                            }

                        }).show();
                    },
                    insertasset: function () {
                        var el = this.editor.lastSelectedNode || this.editor.selection.getNode();

                        // this creates a dummy element, which hides the last selection grab handles while the editor is visible
                        var body = this.editor.dom.select('body')[0];
                        var span = this.editor.dom.doc.createElement('span');
                        body.appendChild(span);
                        this.editor.selection.select(span);
                        Symphony.Artisan.AssetPicker.getInstance({}, function (asset) {
                            try {
                                // IE 7 bug; editor.select.getNode() returns the body of the entire document. If you try
                                // to write to innerHTML, it will wipe out the page. This makes the user select something
                                // before we try to set innerHTML.
                                if (Ext.isIE && el.tagName == 'BODY') {
                                    throw new Error('IE7: Select something first.');
                                }

                                var html = new Ext.XTemplate(asset.template).apply(asset);
                                el.innerHTML = html;
                                // doesn't work in IE so we have to use the inner html method above
                                // fme.insertContent(html);
                                body.removeChild(span);
                                me.fireContentChange();
                            } catch (e) {
                                Ext.Msg.alert('Error', 'Your asset cannot be inserted at the selected location. Please select a valid content area.');
                            }

                        }).show();
                    },
                    paste: function () { },
                    preview2: function () {
                        var ed = this.editor;
                        var t = ed.dom,
							d = t.doc,
							b = d.body,
							h = t.select('head')[0];
                        var win = new Ext.Window({
                            title: 'Preview (1024x600)',
                            width: 1024,
                            height: 600,
                            layout: 'fit',
                            items: [{
                                xtype: 'panel',
                                name: 'results',
                                layout: 'fit',
                                html: '',
                                border: false,
                                listeners: {
                                    afterrender: function (panel) {
                                        var iframe = document.createElement('iframe');
                                        iframe.onload = function () {
                                            var doc = iframe.contentDocument || iframe.contentWindow;
                                            var frameContents = Ext.fly(doc);
                                            var newHead = frameContents.select('head');

                                            for (var i = 0; i < h.childNodes.length; i++) {
                                                try {
                                                    var n = h.childNodes[i];
                                                    if (n.tagName.toUpperCase() == 'LINK') {
                                                        if (doc.createStyleSheet) {
                                                            doc.createStyleSheet(n.href);
                                                        } else {
                                                            var el = document.createElement(n.tagName);
                                                            el.href = n.href;
                                                            el.rel = n.rel;
                                                            newHead.appendChild(el);
                                                        }
                                                    } else if (n.tagName.toUpperCase() == 'STYLE') {
                                                        var el = document.createElement(n.tagName);
                                                        el.setAttribute("type", "text/css");
                                                        if (el.styleSheet) {
                                                            el.styleSheet.cssText = n.innerHTML;
                                                        } else {
                                                            el.innerHTML = n.innerHTML;
                                                        }
                                                        newHead.appendChild(el);
                                                    }
                                                } catch (e) {
                                                }
                                            }

                                            // add the content
                                            var newBody = frameContents.select('body');
                                            newBody.elements[0].innerHTML = b.innerHTML;
                                        }
                                        panel.body.dom.appendChild(iframe);
                                        iframe.className += 'artisan-preview';
                                        iframe.setAttribute('src', '/blank.html');
                                    }
                                }
                            }]
                        });
                        win.show();
                    }
                }
            });
            this.callParent(arguments);

            // start a timed monitor to check for content changes
            this.monitorContent();
        },
        destroy: function () {
            this.stopMonitoringContent();
            Symphony.Artisan.TinyMCE.superclass.destroy.apply(this, arguments);
        },
        attachLabels: function (selector, labelText) {
            //            if (!selector) {
            //                this.attachLabels('.element_text', 'Text');
            //                this.attachLabels('.element_media', 'Media');
            //                return;
            //            }
            //            
            //            Ext.each(Ext.query(selector, this.editor.dom.doc), function(element) {
            //                var label = element.ownerDocument.createElement('img');
            //                label.className = 'artisan_label mceNonEditable';
            //                label.style.display = 'none';
            //                label.innerHTML = labelText;
            //                element.appendChild(label);
            //            });
        },
        detachLabels: function () {
            /*Ext.each(Ext.query('.artisan_label', this.editor.dom.doc), function(element) {
            element.parentNode.removeChild(element);
            });*/
        },
        resetNodes: function (selector) {
            if (!selector) {
                this.resetNodes('.element_text');
                this.resetNodes('.element_media');
                return;
            }
            Ext.each(Ext.query(selector, this.editor.dom.doc), function (element) {
                element.innerHTML = '&nbsp;';
            });
        },
        insertContent: function (content) {
            this.editor.execCommand('mceInsertContent', false, content);
        },
        setContent: function (content) {
            this.editor.execCommand('mceSetContent', false, content);
        },
        unloadCss: function (url) {
            var t = this.editor.dom,
				d = t.doc,
				head, me = this;

            if (!url) {
                return;
            }

            head = t.select('head')[0];
            Ext.each(url.split(','), function (u) {

                var href = me.editor.documentBaseURI.toAbsolute(u);
                var link = t.select('link[href=' + href + ']')[0];
                delete t.files[href];

                head.removeChild(link);
            });
        },
        loadCss: function (url) {
            if (url) {
                this.editor.dom.loadCSS(this.editor.documentBaseURI.toAbsolute(url));
            }
        },
        fixIE: function (url) {
            var doc = this.editor.dom.doc;
            var jsNode = doc.createElement('script');
            jsNode.type = 'text/javascript';
            jsNode.src = '/scripts/ie/IE8.js';
            doc.getElementsByTagName('head')[0].appendChild(jsNode);
        },
        setCssText: function (css) {
            var doc = this.editor.dom.doc;
            var cssNode = doc.createElement('style');
            cssNode.type = 'text/css';
            cssNode.rel = 'stylesheet';
            cssNode.media = 'screen';
            if (cssNode.styleSheet) {
                cssNode.styleSheet.cssText = css;
            } else {
                cssNode.appendChild(document.createTextNode(css));
            }
            doc.getElementsByTagName('head')[0].appendChild(cssNode);
        },
        getContent: function () {
            if (this.editor && this.editor.dom && this.editor.dom.doc && this.editor.dom.doc.body) {
                this.detachLabels();

                var content;
                var currentInnerHtml = this.editor.dom.doc.body.innerHTML;
                if (!this.lastInnerHtml || !this.lastContent || currentInnerHtml != this.lastInnerHtml) {
                    content = this.editor.getContent();
                } else {
                    content = this.lastContent;
                }

                this.lastInnerHtml = currentInnerHtml;
                this.lastContent = content;

                this.attachLabels();
                return content;
            }
            return this.content;
        },
        onContentChange: function () {
            if (this.contentChangeTimeout) {
                clearTimeout(this.contentChangeTimeout);
            }

            this.contentChangeTimeout = setTimeout(Ext.bind(this.fireContentChange, this), 333);
        },
        fireContentChange: function () {
            this.fireEvent('contentchange', this.getContent());
        },
        monitorContent: function () {
            try {
                if (this._prevContent != null && this._prevContent != this.getContent()) {
                    this.fireContentChange();
                }
                this._prevContent = this.getContent();
            } catch (e) { }

            // every 2 seconds, check again
            this.monitorContentTimeout = setTimeout(Ext.bind(this.monitorContent, this), 2000);
        },
        stopMonitoringContent: function () {
            window.clearTimeout(Ext.bind(this.monitorContent, this));
        }
    });

})();