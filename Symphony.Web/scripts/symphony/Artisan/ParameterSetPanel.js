﻿(function () {
    Symphony.Artisan.ParameterSetPanel = Ext.define('Artisan.ParameterSetPanel', {
        extend: 'Ext.Panel',

        parameterSet: null,
        initComponent: function () {
            var me = this;

            me.baseUrl = '/services/artisan.svc/parametersets/';

            Ext.apply(this, {
                layout: 'border',
                closable: true,
                border: false,
                tbar: [{
                    xtype: 'button',
                    text: 'Save',
                    name: 'save',
                    iconCls: 'x-button-save',
                    handler: function () {
                        me.save();
                    }
                }],
                items: [{
                    region: 'north',
                    border: false,
                    split: true,
                    height: 135,
                    layout: 'fit',
                    items: [{
                        xtype: 'form',
                        frame: false,
                        bordeR: false,
                        cls: 'x-panel-mc',
                        padding: '5px',
                        items: [{
                            fieldLabel: 'Name',
                            xtype: 'textfield',
                            name: 'name',
                            anchor: '100%'
                        }, {
                            fieldLabel: 'Description',
                            xtype: 'textarea',
                            name: 'description',
                            anchor: '100%'
                        }]
                    }]
                }, {
                    xtype: 'artisan.parametersetoptionseditorgrid',
                    region: 'center',
                    border: false,
                    parameterSet: me.parameterSet
                }]
            });
            this.callParent(arguments);
        },
        load: function(parameterSet) {
            this.parameterSet = parameterSet;

            this.find('xtype', 'form')[0].bindValues(parameterSet);
            this.find('xtype', 'artisan.parametersetoptionseditorgrid')[0].setParameterSet(parameterSet);
            
            this.setTitle(parameterSet.name);
        },
        save: function () {
            var me = this;

            var parameterSet = me.find('xtype', 'form')[0].getForm().getValues();
            var options = me.find('xtype', 'artisan.parametersetoptionseditorgrid')[0].getStore().getRange(0);

            parameterSet.options = [];

            for (var i = 0; i < options.length; i++) {
                parameterSet.options.push(options[i].data);
            }

            Symphony.Ajax.request({
                method: 'POST',
                jsonData: parameterSet,
                url: me.baseUrl + (me.parameterSet ? me.parameterSet.id : 0),
                success: function (args) {
                    me.load(args.data);
                    me.fireEvent('save');
                }
            });
        }
    });
})();