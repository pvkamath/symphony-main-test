﻿(function () {
	Symphony.Artisan.DeploymentGrid = Ext.define('artisan.deploymentgrid', {
	    alias: 'widget.artisan.deploymentgrid',
	    extend: 'symphony.searchablegrid',
		initComponent: function () {
			var me = this;
			var url = '/services/artisan.svc/deployments/';


			var colModel = new Ext.grid.ColumnModel({
				defaults: {
					sortable: true,
					renderer: Symphony.Portal.valueRenderer
				},
				columns: [{
					/*id: 'name',*/
					header: 'Name',
					dataIndex: 'name',
					align: 'left',
					flex: 1
				}, {
					/*id: 'createdOn',*/
					header: 'Date',
					dataIndex: 'createdOn',
					align: 'left',
					renderer: Symphony.dateRenderer
				}]
			});

			Ext.apply(this, {
				tbar: {
					items: [{
						text: 'Add',
						iconCls: 'x-button-add',
						handler: function () {
							me.fireEvent('addclick');
						}
					}]
				},
				idProperty: 'id',
				deferLoad: true,
				url: url,
				colModel: colModel,
				model: 'artisanDeployment'
			});

			this.callParent(arguments);
		},
		getSelected: function () {
			return this.getSelectionModel().selections;
		}
	});

})();