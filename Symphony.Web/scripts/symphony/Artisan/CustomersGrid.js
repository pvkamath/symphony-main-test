﻿(function () {
	Symphony.Artisan.CustomersGrid = Ext.define('artisan.customersgrid', {
	    alias: 'widget.artisan.customersgrid',
	    extend: 'symphony.searchablegrid',
        height: 530,
        width: 320,
        initComponent: function(){
            var me = this;
            var url = '/services/customer.svc/customers/';
            
            var sm = new Ext.selection.CheckboxModel();
            var colModel = new Ext.grid.ColumnModel({
                columns: [
                    {
                        flex: 1, header: 'Name', dataIndex: 'name', align: 'left', width: 298, sortable: true,
                        renderer: Symphony.Portal.valueRenderer,
                        doSort: function (state) {
                            // weird bug i was seeing where the sort on the server failed due to ambiguous name...fixed here
                            // by explicitly sorting by customer name
                            var ds = this.up('tablepanel').store;
                            ds.sort({
                                property: 'customer.name',
                                direction: state
                            });
                        }
                    }
                ]
            });
            Ext.apply(this, {
                listeners: {
                    itemdblclick: function (grid, record, item, rowIndex, e) {
                        me.fireEvent('open', record.get('id'), me.salesChannelId, record.get('name'));
                    }
                },
                idProperty: 'id',
                url: url,
                selModel: sm,
                plugins: [{ ptype: 'pagingselectpersist' }],
                colModel: colModel,
                model: 'customer'
            });
            this.callParent(arguments);
        },
        getSelected: function () {
			return this.getSelectionModel().selections;
		}
    });

})();