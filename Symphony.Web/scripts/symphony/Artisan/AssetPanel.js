﻿(function () {
    Symphony.Artisan.AssetPanel = Ext.define('Artisan.AssetPanel', {
        extend: 'Ext.Panel',

        asset: null,
        autoPlay: false,
        initComponent: function () {
            var template = this.asset.template;
            var html = new Ext.XTemplate(template).apply(this.asset);
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                closable: true,
                border: false,
                items: [{
                    region: 'north',
                    height: 250,
                    border: false,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        listeners: {
                            save: function () {
                                var form = me.find('xtype', 'form')[0];

                                if (!form.isValid()) {
                                    return false;
                                }

                                var values = form.getValues();
                                if (!values.width) {
                                    delete values.width;
                                }
                                if (!values.height) {
                                    delete values.height;
                                }

                                if (me.alternateHtml && me.alternateHtml.hidden != true){
                                    values.alternateHtml = me.alternateHtml.getValue();
                                }

                                Symphony.Ajax.request({
                                    url: '/services/artisan.svc/assets/' + me.asset.id,
                                    jsonData: values,
                                    success: function (result) {
                                        me.fireEvent('save');
                                        var html = new Ext.XTemplate(template).apply(result.data);
                                        me.destroyVideo();
                                        me.preview.update(html);
                                        me.createVideo();
                                    }
                                });
                            },
                            cancel: Ext.bind(function () {
                                this.ownerCt.remove(this);
                            }, this)
                        }
                    }, {
                        xtype: 'form',
                        frame: true,
                        items: [{
                            xtype: 'fieldset',
                            title: 'General',
                            labelWidth: 120,
                            items: [{
                                fieldLabel: 'Public',
                                xtype: 'checkbox',
                                name: 'isPublic',
                                anchor: '100%',
                                hidden: !Symphony.User.isSuperUser,
                                hideLabel: !Symphony.User.isSuperUser,
                                checked: this.asset.isPublic
                            }, {
                                fieldLabel: 'Name',
                                xtype: 'textfield',
                                name: 'name',
                                anchor: '100%',
                                value: this.asset.name
                            }, {
                                fieldLabel: 'Description',
                                xtype: 'symphony.spellcheckfield',
                                name: 'description',
                                anchor: '100%',
                                value: this.asset.description
                            }, {
                                fieldLabel: 'Keywords',
                                xtype: 'textfield',
                                name: 'keywords',
                                anchor: '100%',
                                value: this.asset.keywords
                            }, {
                                fieldLabel: 'Url',
                                xtype: 'textfield',
                                name: 'url',
                                anchor: '100%',
                                disabled: this.asset.id > 0,
                                value: this.asset.path
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Dimensions (in pixels, w x h; blank = 100% x 100%)',
                            name: 'dimensions',
                            layout: 'hbox',
                            hidden: !(this.asset.assetTypeId == 1 || this.asset.assetTypeId == 7 || this.asset.assetTypeId == 8 || this.asset.width > 0 || this.asset.height > 0),
                            items: [{
                                xtype: 'textfield',
                                name: 'width',
                                value: this.asset.width
                            }, {
                                xtype: 'label',
                                style: 'padding:5px;',
                                text: ' x '
                            }, {
                                xtype: 'textfield',
                                name: 'height',
                                value: this.asset.height
                            }]
                        }]
                    }]
                }, {
                    xtype: 'tabpanel',
                    region: 'center',
                    activeItem: 0,
                    items: [{
                        title: 'Preview',
                        autoScroll: true,
                        border: false,
                        bodyStyle: 'padding:15px;border-top:1px;',
                        html: html,
                        ref: '../preview'
                    }].concat(this.asset.hasAlternateHtml ? [{
                        title: 'Alternate HTML',
                        xtype: 'htmleditor',
                        name: 'alternateHtml',
                        ref: '../alternateHtml',
                        value: this.asset.alternateHtml
                    }] : [])
                }]
            });
            this.callParent(arguments);

            // fix for html5 video, uses videojs
            window.setTimeout(function () {
                me.createVideo();
            }, 500);
        },
        // TODO: don't forget that when we add saving capabilities, we need to update this underlying object
        getAsset: function () {

            if (this.preview.el.dom.querySelector) {
                var img = this.preview.el.dom.querySelector('img');
            } else {
                var img = this.preview.el.query('img')[0];
            }

            if (img) {
                this.asset.naturalWidth = img.naturalWidth;
                this.asset.naturalHeight = img.naturalHeight;
            }
            
            return this.asset;
        },
        destroy: function () {
            this.destroyVideo();
            Symphony.Artisan.AssetPanel.superclass.destroy.apply(this, arguments);
        },
        createVideo: function () {
            var me = this;
            var vid = Ext.query('video', me.id)[0];
            if (vid) {
                _V_(vid.id, {}, function () {
                    if (me.autoPlay) {
                        this.play(); //autostart it
                    }
                });
            }
        },
        destroyVideo: function () {
            var me = this;
            var vid = Ext.query('video', me.id)[0];
            if (vid) {
                _V_(vid.id).destroy();
            }
        }
    });
})();