﻿(function () {
    Symphony.Artisan.AssetsGrid = Ext.define('artisan.assetsgrid', {
        alias: 'widget.artisan.assetsgrid',
        extend: 'symphony.searchablegrid',
        allowUploads: true,
        initComponent: function () {
            var me = this;
            var url = '/services/artisan.svc/assets/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'name',*/
                    header: 'Preview',
                    align: 'center',
                    width: 65,
                    renderer: function (value, meta, record) {
                        return new Ext.XTemplate(record.get('previewTemplate')).apply(record.raw);
                    }
                }, {
                    /*id: 'description',*/
                    header: 'Description',
                    renderer: function (value, meta, record) {
                        return new Ext.XTemplate('<b>{name}</b><br/>{description}').apply(record.raw);
                    },
                    flex: 1
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                deferLoad: true,
                hideHeaders: true,
                url: url,
                colModel: colModel,
                model: 'artisanAsset',
                searchMenu: {
                    items: []
                },
                tbar: {
                    items: this.allowUploads ? [{
                        xtype: 'artisan.assetuploadbutton',
                        listeners: {
                            'uploadcomplete': function (data) {
                                me.fireEvent('uploadcomplete', data);
                            }
                        }
                    }, {
                        xtype: 'artisan.assetlinkbutton',
                        listeners: {
                            'linkcomplete': function (data) {
                                me.fireEvent('uploadcomplete', data);
                            }
                        }
                    }] : []
                },
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.getStore().getAt(rowIndex); // Get the Record
                        me.fireEvent('assetselected', record);
                    }
                }
            });

            this.callParent(arguments);
        },
        onRender: function () {
            Symphony.Artisan.AssetsGrid.superclass.onRender.apply(this, arguments);

            var me = this;
            // defer this jazz so it doesn't affect main page load time
            window.setTimeout(function () {
                // get list of file types
                var searchItems = [];
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/artisan.svc/assetTypes/',
                    success: function (result) {
                        for (i = 0; i < result.data.length; i++) {
                            searchItems.push({
                                hideOnClick: false,
                                text: result.data[i].name,
                                checked: true,
                                filter: {
                                    assetTypeId: result.data[i].id
                                },
                                checkHandler: Ext.bind(me.checkSearchOptionClicked, me)
                            });
                        }
                        me.addSearchMenuItems(searchItems);
                        // pre-set the filter for the grid and refresh it
                        me.checkSearchOptionClicked();
                    }
                });
            }, 1);
        }
    });

})();