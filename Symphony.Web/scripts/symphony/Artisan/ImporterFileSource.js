﻿(function () {
	Symphony.Artisan.ImporterFileSource = Ext.define('artisan.importerfilesource', {
	    alias: 'widget.artisan.importerfilesource',
	    extend: 'Ext.form.FormPanel',
		sectionId: 0,
		courseId: 0,
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				title: 'Select File to Import',
				border: false,
				tbar: {
					items: [{
						xtype: 'swfuploadbutton',
						ref: '../uploadButton',
						text: 'Import and Upload',
						iconCls: 'x-button-upload',
						uploadUrl: '/Uploaders/ArtisanDocumentUploader.ashx',
						name: 'documentUploader',
						fileTypes: '*.doc;*.docx;*.ppt;*.pptx;*.pdf',
						fileTypesDescription: 'Document Files',
						fileQueueLimit: 1,
						listeners: {
							'queue': function (file) {
								this.startUpload();
							},
							'queueerror': function () {
								Ext.Msg.alert('Error', 'The file could not be queued');
							},
							'uploadstart': function (item) {
								this.addParameter('customerId', Symphony.User.customerId);
								this.addParameter('fileType', me.fileType);
								this.addParameter('sectionId', me.sectionId);
								this.addParameter('courseId', me.courseId);
								this.addParameter('width', me.imageWidth);
								this.addParameter('height', me.imageHeight);
								this.addParameter('scaleType', me.scaleType);
								this.addParameter('cropX', me.cropX);
								this.addParameter('cropY', me.cropY);

								Ext.Msg.progress('Please wait...', 'Please wait while your file is uploaded...');
							},
							'uploadprogress': function (item, completed, total) {
								var ratio = parseFloat(completed / total);
								Ext.Msg.updateProgress(ratio, parseInt(ratio * 100, 10) + '% complete');
								if (ratio == 1) {
									Ext.Msg.updateProgress(1, 'Upload complete, processing...');
								}
							},
							'uploaderror': function () {
								this.error = true;
								Ext.Msg.hide();
								Ext.Msg.alert('Upload failed', 'The upload failed. Please make sure it is within the proper file size limitations.');
							},
							'uploadsuccess': function (item, response, hasData) {
								var result = Ext.decode(response);
								if (result.success) {
									Ext.Msg.hide();
									me.fireEvent('uploadComplete', me.sectionId, result.data);
								} else {
									Ext.Msg.hide();
									Ext.Msg.alert('Upload failed', result.error);
								}
							}
						}
					}]
				}
			});

			this.callParent(arguments);
		},
		setSectionId: function (id) {
			this.sectionId = id;
		},
		setCourseId: function (id) {
			this.courseId = id;
		},
		setOptions: function (width, height, scaleType, cropX, cropY) {
			this.imageWidth = width;
			this.imageHeight = height;
			this.scaleType = scaleType;
			this.cropX = cropX;
			this.cropY = cropY;
		}
	});

})();