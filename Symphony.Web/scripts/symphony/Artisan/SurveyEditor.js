﻿(function () {
	Symphony.Artisan.SurveyEditor = Ext.define('artisan.surveyeditor', {
	    alias: 'widget.artisan.surveyeditor',
	    extend: 'Ext.Panel',
		content: '',
		lastContent: '',
		reset: false,
		page: null,
		node: null,
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				layout: 'border',
				cls: 'contentarea-layout',
				border: false,
				defaults: {
					border: false
				},
				listeners: {
					beforedestroy: function (comp) {
						if (me.timer) {
							window.clearInterval(me.timer);
						}
					}
				},
				items: [{
					region: 'north',
					height: 27,
					xtype: 'toolbar',
					items: [{
						text: 'Revert to Last Save',
						iconCls: 'x-button-revert',
						ref: '../revertButton',
						disabled: true,
						handler: Ext.bind(this.revert, this)
					}]
				}, {
					region: 'center',
					margins: '15',
					xtype: 'artisan.surveyform',
					ref: 'surveyContainer',
					page: me.page,
					listeners: {
						contentchange: Ext.bind(this.onContentChange, this)
					}
				}]
			});
			this.callParent(arguments);

			if (Symphony.Artisan.autoSaveInterval != null) {
				me.timer = window.setInterval(function () {
					var values = me.surveyContainer.getValues();
					me.onContentChange(values.question, values.answers, values.correctResponse, values.incorrectResponse);
				}, Symphony.Artisan.autoSaveInterval);
			}
		},
		revert: function () {
			this.surveyContainer.setValue(this.page.lastHtml, this.page.lastAnswers, this.page.lastCorrectResponse, this.page.lastIncorrectResponse);
		},
		onContentChange: function (content, answers, correctResponse, incorrectResponse) {
			var contentChanged = (content != this.page.lastHtml);

			// simple check first
			var answersChanged = (this.page.lastAnswers) ? (answers.length != this.page.lastAnswers.length) : true;

			// ok, they have the same number, let's make sure the content matches
			if (!answersChanged) {
				for (var i = 0; i < this.page.lastAnswers.length; i++) {
					var currentAnswer = this.page.lastAnswers[i];
					var incomingAnswer = answers[i];
					var fields = ['sort', 'text', 'isCorrect'];
					for (var j = 0; j < fields.length; j++) {
						var prop = fields[j];
						if (currentAnswer.hasOwnProperty(prop)) {
							if (incomingAnswer[prop] != currentAnswer[prop]) {
								answersChanged = true;
								break;
							}
						}
					}
					if (answersChanged) {
						break;
					}
				}
			}

			var correctResponseChanged = (correctResponse != this.page.lastCorrectResponse);
			var incorrectResponseChanged = (incorrectResponse != this.page.lastIncorrectResponse);
			this.setRevertDisabled(!contentChanged && !answersChanged && !correctResponseChanged && !incorrectResponseChanged);
			this.page.html = content;
			this.page.answers = answers;
			this.page.correctResponse = correctResponse;
			this.page.incorrectResponse = incorrectResponse;

			this.fireEvent('contentchange', content, answers, correctResponse, incorrectResponse);
		},
		setRevertDisabled: function (disable) {
			this.revertButton.setDisabled(disable);
			if (this.node && disable) {
				this.node.set('cls', this.node.get('cls').replace(' node-italic', ''));
			} else {
			    if (this.node.get('cls').indexOf('node-italic') == -1) {
			        this.node.set('cls', this.node.get('cls') + ' node-italic');
			    }
			}
		},
		setSaved: function () {
			this.setRevertDisabled(true);
		}
	});

})();