﻿(function () {
    Symphony.Artisan.ParameterPanel = Ext.define('Artisan.ParameterPanel', {
        extend: 'Ext.Panel',

        parameter: null,
        baseUrl: '/services/artisan.svc/parameters/',
        initComponent: function () {
            var me = this;

            var tbar = [{
                xtype: 'button',
                text: 'Save',
                name: 'save',
                iconCls: 'x-button-save',
                handler: function () {
                    me.save();
                }
            }];

            Ext.apply(this, {
                layout: 'border',
                closable: true,
                border: false,
                tbar: !me.readOnly ? tbar : {},
                items: [{
                    region: 'north',
                    border: false,
                    frame: false,
                    
                    split: true,
                    height: 240,
                    layout: 'fit',
                    items: [{
                        xtype: 'form',
                        cls: 'x-panel-mc',
                        frame: false,
                        border: false,
                        padding: '5px',
                        items: [{
                            fieldLabel: 'Name',
                            xtype: 'textfield',
                            name: 'name',
                            anchor: '100%',
                            readOnly: me.readOnly
                        }, {
                            fieldLabel: 'Code',
                            xtype: 'textfield',
                            name: 'code',
                            anchor: '100%',
                            emptyText: 'Format: {!code}',
                            allowEmpty: false,
                            readOnly: me.readOnly,
                            listeners: {
                                blur: function (txt) {
                                    var code = txt.getValue().toLowerCase();
                                    var regex = /[^a-z0-9\s]/g;

                                    code = code.replace(regex, '').replace(/\s+/g, '_');

                                    if (code !== '') {
                                        code = '{!' + code + '}';
                                    }

                                    txt.setValue(code);
                                }
                            }
                        }, {
                            fieldLabel: 'Description',
                            xtype: 'textarea', 
                            name: 'description',
                            anchor: '100%',
                            readOnly: me.readOnly
                        }, {
                            //AMTODO - Make parameter set load correctly after save
                            fieldLabel: 'Parameter Set',
                            xtype: 'symphony.pagedcombobox',
                            url: '/services/artisan.svc/parametersets/',

                            name: 'parameterSetId',
                            bindingName: 'parameterSetName',
                            displayField: 'name',
                            valueField: 'id',

                            model: 'parameterSet',

                            emptyText: 'Select a parameter set...',

                            anchor: '100%',
                            readOnly: me.readOnly,
                            hideTrigger: me.readOnly,

                            listeners: {
                                select: function (cbo, record, index) {
                                    var parameterValuesGrid = me.find('xtype', 'artisan.parametervaluesgrid')[0];
                                    parameterValuesGrid.setParameterSet(record.data);
                                },
                                render: function (cbo) {
                                    if (me.parameter) {
                                        var parameterValuesGrid = me.find('xtype', 'artisan.parametervaluesgrid')[0];
                                        parameterValuesGrid.setParameterSet({ name: me.parameter.parameterSetName, id: me.parameter.parameterSetId });
                                        cbo.setDisabled(true);
                                    }
                                }
                            }
                        }, {
                            fieldLabel: 'Default Value',
                            xtype: 'textarea',
                            name: 'defaultValue',
                            anchor: '100%',
                            readOnly: me.readOnly
                        }]
                    }]
                }, {
                    xtype: 'artisan.parametervaluesgrid',
                    region: 'center',
                    parameter: me.parameter,
                    readOnly: me.readOnly,
                    border: false,
                    listeners: {
                        addparameter: function (grid) {
                            me.find('name', 'parameterSetId')[0].setDisabled(true);
                        },
                        deleteparameter: function (grid) {
                            if (grid.getStore().getCount() === 0) {
                                me.find('name', 'parameterSetId')[0].setDisabled(false);
                            }
                        }
                    }
                }]
            });
            this.callParent(arguments);
        },
        load: function(parameter) {
            var me = this;
            var valuesGrid = me.find('xtype', 'artisan.parametervaluesgrid')[0];

            me.parameter = parameter;
            me.find('xtype', 'form')[0].bindValues(parameter);

            if (parameter && parameter.values && parameter.values.length > 0) {
                valuesGrid.setData({ data: parameter.values });
            } else if (parameter && parameter.id) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: me.baseUrl + parameter.id + '/values',
                    success: function (args) {
                        valuesGrid.setData(args);
                    }
                });
            }

            me.setTitle(parameter.name);
        },
        getParameter: function () {
            return this.parameter;
        },
        save: function () {
            var me = this;

            var parameter = me.find('xtype', 'form')[0].getForm().getValues();
            var combo = me.find('xtype', 'symphony.pagedcombobox')[0];

            // This field is disabled after adding a value so we need to grab it manually
            parameter.parameterSetId = combo.getValue();

            var values = me.find('xtype', 'artisan.parametervaluesgrid')[0].getStore().getRange(0);

            parameter.values = [];

            for (var i = 0; i < values.length; i++) {
                parameter.values.push(values[i].data);
            }

            if (me.parameter && (!parameter.parameterSetId || isNaN(parameter.parameterSetId))) {
                parameter.parameterSetId = me.parameter.parameterSetId;
            }
            if (parameter.values.length) {
                Symphony.Ajax.request({
                    method: 'POST',
                    jsonData: parameter,
                    url: me.baseUrl + (me.parameter ? me.parameter.id : 0),
                    success: function (args) {
                        me.load(args.data);
                        me.fireEvent('save');
                    }
                });
            } else {
                Ext.Msg.alert("Error", "Please add at least 1 value.");
            }
        }
    });
})();