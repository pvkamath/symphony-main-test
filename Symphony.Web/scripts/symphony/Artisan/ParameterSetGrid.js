﻿(function () {
    Symphony.Artisan.ParameterSetGrid = Ext.define('artisan.parametersetgrid', {
        alias: 'widget.artisan.parametersetgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/artisan.svc/parametersets/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'name',*/
                    dataIndex: 'name',
                    header: 'Name',
                    flex: 1
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                hideHeaders: true,
                url: url,
                colModel: colModel,
                model: 'parameterSet'
            });

            this.callParent(arguments);
        }
    });

})();