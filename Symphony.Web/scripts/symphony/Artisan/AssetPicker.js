﻿(function () {
	Symphony.Artisan.AssetPicker = Ext.define('artisan.assetpicker', {
	    alias: 'widget.artisan.assetpicker',
	    extend: 'Ext.Window',
		allowUpload: false,
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				layout: 'fit',
				defaults: {
					border: false
				},
				items: [{
					xtype: 'artisan.assetstab',
					ref: 'assetsTab',
					allowUpload: this.allowUpload,
					listeners: {
						assetavailable: function () {
							me.okBtn.enable();
						},
						assetunavailable: function () {
							me.okBtn.disable();
						},
						assetdoubleclicked: Ext.bind(this.onOkClick, this)
					}
				}],
				// buttons are placed in the footer's "items" array, so the ref has one more layer to go through
				buttons: [{
					text: 'OK',
					ref: '../okBtn',
					disabled: true,
					handler: Ext.bind(this.onOkClick, this)
				}, {
					text: 'Cancel',
					ref: '../cancelBtn',
					handler: Ext.bind(this.onCancelClick, this)
				}]
			});
			this.callParent(arguments);
		},
		onOkClick: function () {
			this.fireEvent('assetpicked', this.assetsTab.getActiveAsset());
			this.hide();
		},
		onCancelClick: function () {
			this.hide();
		}
	});


	Symphony.Artisan.AssetPicker.getInstance = function (options, callback) {
		Symphony.Artisan.AssetPicker._instanceCallback = callback;
		if (Symphony.Artisan.AssetPicker._instance == null) {
			Symphony.Artisan.AssetPicker._instance = new Symphony.Artisan.AssetPicker({
				title: 'Insert Asset',
				width: 900,
				height: 600,
				cls: 'maxindex',
				modal: true,
				closeAction: 'hide',
				listeners: {
					assetpicked: function (asset) {
						Symphony.Artisan.AssetPicker._instanceCallback(asset);
					}
				}
			});
		}
		Ext.apply(Symphony.Artisan.AssetPicker._instance, options || {});
		return Symphony.Artisan.AssetPicker._instance;
	};
})();