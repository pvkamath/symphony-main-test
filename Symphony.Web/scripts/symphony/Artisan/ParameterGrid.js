﻿(function () {
    Symphony.Artisan.ParameterGrid = Ext.define('artisan.parametergrid', {
        alias: 'widget.artisan.parametergrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/artisan.svc/parameters/';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'name',*/
                    dataIndex: 'name',
                    header: 'Name',
                    flex: 1
                }, {
                    /*id: 'code',*/
                    dataIndex: 'code',
                    header: 'Code'
                }, {
                    /*id: 'parameterSetName',*/
                    dataIndex: 'parameterSetName',
                    header: 'Parameter Set'
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'parameter'
            });

            this.callParent(arguments);
        }
    });

})();