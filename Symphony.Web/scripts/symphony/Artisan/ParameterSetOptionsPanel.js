﻿(function () {
    Symphony.Artisan.ParameterSetOptionsPanel = Ext.define('artisan.parametersetoptionspanel', {
        alias: 'widget.artisan.parametersetoptionspanel',
        extend: 'Ext.Panel',
        parameters: [],
        parametersFound: [],
        comboHeight: 35,
        defaultMaxDisplayedCombos: 4,
        parameterSetData: null,
        isTrainingProgram: false,
        isLockBySharedTrainingFlag: false,
        initComponent: function () {
            var me = this;

            var urlTemplate = '/services/artisan.svc/parametersets/{0}/options';

            var optionCombos = [];
            var parameterSetsUsed = {};
            var parametersUsedMap = {};
            
            for (var i = 0; i < me.parameters.length; i++) {
                var parameter = me.parameters[i];

                if (parameterSetsUsed[parameter.parameterSetId]) {
                    continue;
                }

                parameterSetsUsed[parameter.parameterSetId] = true;
                parametersUsedMap[parameter.code] = true;

                optionCombos.push({
                    xtype: 'symphony.pagedcombobox',
                    disabled: me.isLockBySharedTrainingFlag,
                    url: String.format(urlTemplate,parameter.parameterSetId),
                    fieldLabel: parameter.parameterSetName,
                    name: 'parameterSetText' + parameter.parameterSetId,
                    bindingName: 'parameterSetText' + parameter.parameterSetId,
                    model: 'parameterSetOption',
                    valueField: 'id',
                    displayField: 'value',
                    allowBlank: true,
                    emptyText: '- Default -',
                    anchor: '95%',
                    parameterSetId: parameter.parameterSetId,
                    parameterSetName: parameter.parameterSetName,
                    listeners: {
                        render: function (combo) {
                            if (me.parameterSetData && me.parameterSetData[combo.getName()]) {
                                combo.setValue(me.parameterSetData[combo.getName()].display);
                            }
                        },
                        select: function (combo, record, index) {
                            var parameterSetId = record.get('parameterSetId');
                            var updateParameterCodes = [];
                            var parameterMap = [];

                            for (var x = 0; x < me.parameters.length; x++) {
                                var param = me.parameters[x];

                                if (param.parameterSetId === parameterSetId) {
                                    updateParameterCodes.push(param.code);
                                    parameterMap[param.id] = x;
                                    // Clear value so we can use the default if no value found for the option
                                    param.value = '';
                                }
                            }

                            Symphony.Ajax.request({
                                method: 'GET',
                                url: String.format('/services/artisan.svc/parameterValues/{0}/?codes={1}',
                                           record.get('id'),
                                           updateParameterCodes.join('|')),
                                success: function(args) {
                                    for (var j = 0; j < args.data.length; j++) {
                                        var parameterValue = args.data[j];
                                        
                                        me.parameters[parameterMap[parameterValue.parameterId]].value = parameterValue.value;
                                    }
                                    me.find('xtype', 'artisan.parameterpreviewgrid')[0].setData(me.parameters);
                                }
                            });
                        }
                    }
                });
            }

            var missingParams = "";
            
            for (var i = 0; i < me.parametersFound.length; i++) {
                if (!parametersUsedMap[me.parametersFound[i]]) {
                   if (missingParams.indexOf(me.parametersFound[i]) === -1) {
                        missingParams += me.parametersFound[i] + "<br/>";
                   }
                }
            }

            Ext.apply(this, {
                layout: 'border',
                border: false,
                defaults: {
                    split: true
                },
                listeners: {
                    afterrender: function () {
                        if (missingParams) {
                            setTimeout(function () {
                                Ext.Msg.alert("Missing Parameters",
                                    String.format("Some parameters appear to be used in this {0} that are not available. " +
                                    "These will not have values replaced. The codes are: <br/><br/>{1}<br/>" +
                                    "Please review these codes and ensure they are spelled correctly, or created in the system.",
                                    me.isTrainingProgram ? 'training program' : 'course',
                                    missingParams));
                            }, 200);

                        }
                    }
                },
                items: [{
                    xtype: 'form',
                    region: 'north',
                    frame: true,
                    border: false,
                    height: Math.min(me.defaultMaxDisplayedCombos, optionCombos.length) * me.comboHeight,
                    autoScroll: true,
                    items: optionCombos
                }, {
                    region: 'center',
                    xtype: 'artisan.parameterpreviewgrid',
                    tbar: {
                        items: [
                            'Parameter Preview'
                        ]
                    },
                    listeners: {
                        render: function (grid) {
                            grid.setData(me.parameters);
                        }
                    }
                }]
            });

            this.callParent(arguments);
        },
        getData: function () {
            var records = this.find('xtype', 'artisan.parameterpreviewgrid')[0].getStore().getRange(0);
            var parameterDefaults = [];
            
            for (var i = 0; i < records.length; i++) {
                var parameter = records[i].data;
                var combo = this.find('parameterSetName', parameter.parameterSetName)[0];
                
                var parameterDefault = {
                    parameterId: parameter.id,
                    value: (typeof (parameter.value) === 'undefined' || parameter.value === '') ? parameter.defaultValue : parameter.value,
                    code: parameter.code,
                    parameterSetId: combo.parameterSetId,
                    parameterSetName: parameter.parameterSetName,
                    parameterSetOptionId: combo.getValue(),
                    parameterSetOptionValue: combo.getRawValue()
                };

                if (!parameterDefault.parameterSetOptionId) {
                    delete parameterDefault.parameterSetOptionId;
                }
                if (!parameterDefault.parameterSetOptionValue) {
                    delete parameterDefault.parameterSetOptionValue;
                }

                parameterDefaults.push(parameterDefault);
            }

            return parameterDefaults;
        },
        getParameterOverrides: function() {
            var combos = this.find('xtype', 'symphony.pagedcombobox');
            var data = [];

            for (var i = 0; i < combos.length; i++) {
                var combo = combos[i];
                var lastData = this.parameterSetData && this.parameterSetData[combo.getName()] ? this.parameterSetData[combo.getName()] : null;

                var value = combo.getValue();

                if (!value) {
                    value = 0;
                }

                var parameterSetOverride = {
                    parameterSetOptionId: value
                }

                if (lastData) {
                    if (isNaN(combo.getValue())) {
                        parameterSetOverride.parameterSetOptionId = lastData.value;
                    }
                }
                data.push(parameterSetOverride);
            }

            return data;
        },
        getParameterSetData: function () {
            var combos =  this.find('xtype', 'symphony.pagedcombobox');
            var data = {};
            for (var i = 0; i < combos.length; i++) {
                data[combos[i].getName()] = {
                    value: combos[i].getValue(),
                    display: combos[i].getRawValue()
                }
            }

            return data;
        }
    });

})();