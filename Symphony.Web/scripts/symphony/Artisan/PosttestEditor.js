﻿(function () {
    Symphony.Artisan.PosttestEditor = Ext.define('artisan.posttesteditor', {
        alias: 'widget.artisan.posttesteditor',
        extend: 'artisan.optionseditor',
        section: null,
        lastDescription: '',
        lastTestType: 0,
        lastIsRandomizeAnswers: false,
        lastIsRandomizeQuestions: false,
        lastMaxQuestions: 20,
        lastMarkingMethod: 0,
        lastMaxTime: '',
        lastFinalContentPage: '',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                listeners: {
                    beforedestroy: function (comp) {
                        if (me.timer) {
                            window.clearInterval(me.timer);
                        }
                    }
                },
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 27,
                    xtype: 'toolbar',
                    items: [{
                        text: 'Revert to Last Save',
                        iconCls: 'x-button-revert',
                        ref: '../revertButton',
                        disabled: true,
                        handler: Ext.bind(me.onRevertClick, this)
                    }]
                }, {
                    region: 'center',
                    xtype: 'panel',
                    layout: 'fit',
                    bodyStyle: 'padding: 15px;',
                    items: [{
                        layout: 'form',
                        frame: true,
                        autoScroll: true,
                        bodyStyle: 'padding-right:20px',
                        items: [{
                            xtype: 'fieldset',
                            title: 'General',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: 150
                            },
                            items: [{
                                name: 'description',
                                ref: '../../../description',
                                xtype: 'symphony.spellcheckarea',
                                fieldLabel: 'Description',
                                value: me.section.description,
                                allowBlank: true,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.section.description = field.getValue();
                                        me.onContentChanged();
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Test Type',
                            border: true,
                            labelWidth: 150,
                            defaults: {
                                border: false,
                                anchor: '100%'
                            },
                            items: [{
                                name: 'testType',
                                ref: '../../../../gameBoard',
                                xtype: 'radio',
                                fieldLabel: 'Game Board',
                                boxLabel: 'Places random questions on a game board where the user chooses which question to answer next.',
                                checked: me.section.testType == Symphony.ArtisanTestType.gameBoard ? true : false,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        me.setTestType(Symphony.ArtisanTestType.gameBoard, newValue);
                                    }
                                }
                            }, {
                                name: 'testType',
                                ref: '../../../../gameWheel',
                                xtype: 'radio',
                                fieldLabel: 'Game Wheel',
                                checked: me.section.testType == Symphony.ArtisanTestType.gameWheel ? true : false,
                                boxLabel: 'Uses a spinnable wheel to select questions at random.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        me.setTestType(Symphony.ArtisanTestType.gameWheel, newValue);
                                    }
                                }
                            }, {
                                name: 'testType',
                                ref: '../../../../sequential',
                                xtype: 'radio',
                                fieldLabel: 'Sequential Assessment',
                                boxLabel: 'Use all questions for this assessment in the order defined in Artisan.',
                                checked: me.section.testType == Symphony.ArtisanTestType.sequential ? true : false,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        me.setTestType(Symphony.ArtisanTestType.sequential, newValue);
                                    }
                                }
                            }, {
                                name: 'testType',
                                ref: '../../../../random',
                                xtype: 'radio',
                                fieldLabel: 'Random Assessment',
                                checked: me.section.testType == Symphony.ArtisanTestType.random ? true : false,
                                boxLabel: 'A random set of questions. If Max Questions is provided the number of questions will be limited.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        me.setTestType(Symphony.ArtisanTestType.random, newValue);
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Test Settings',
                            border: true,
                            labelWidth: 150,
                            defaults: {
                                anchor: '100%'
                            },
                            items: [{
                                xtype: 'checkbox',
                                fieldLabel: 'Randomize Answers',
                                boxLabel: 'Will randomize position of answers in each question the first time a question is accessed. Does not apply to Sequential tests.',
                                name: 'isRandomizeAnswers',
                                ref: '../../../isRandomizeAnswers',
                                checked: me.section.isRandomizeAnswers,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        me.setIsRandomizeAnswers(newValue);
                                    }
                                }
                            }, {
                                xtype: 'checkbox',
                                fieldLabel: 'Re-Randomize Order',
                                boxLabel: 'If checked, then each attempt will see a new random set of questions.',
                                name: 'isReRandomizeOrder',
                                ref: '../../../isRandomizeQuestions',
                                checked: me.section.isReRandomizeOrder,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        me.setIsReRandomizeOrder(newValue);
                                    }
                                }
                            }, {
                                name: 'maxQuestions',
                                ref: '../../../maxQuestions',
                                xtype: 'numberfield',
                                fieldLabel: 'Max Questions',
                                value: me.section.maxQuestions,
                                disabled: me.section.testType == Symphony.ArtisanTestType.sequential ? true : false,
                                minValue: 1,
                                allowBlank: false,
                                enableKeyEvents: true,
                                help: {
                                    title: 'Max Questions',
                                    text: 'The maximum number of questions to display for this test. This will not apply to Sequential tests.'
                                },
                                listeners: {
                                    keyup: function (field, event) {
                                        me.section.maxQuestions = field.getValue();
                                        if (!me.section.maxQuestions) {
                                            delete me.section.maxQuestions;
                                        }
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                name: 'maxTime',
                                ref: '../../../maxTime',
                                xtype: 'numberfield',
                                fieldLabel: 'Max Time (s)',
                                value: me.section.maxTime,
                                minValue: 0,
                                allowBlank: true,
                                enableKeyEvents: true,
                                help: {
                                    title: 'Max Time (Seconds)',
                                    text: 'The maximum amount of time in seconds the user has to complete this test before being logged out. Leave blank to allow unlimited time.'
                                },
                                listeners: {
                                    keyup: function (field, event) {
                                        me.section.maxTime = field.getValue();
                                        if (!me.section.maxTime) {
                                            delete me.section.maxTime;
                                        }
                                        me.onContentChanged();
                                    }
                                }
                            }]
                              
                        }, {
                            xtype: 'fieldset',
                            title: 'Marking Method',
                            border: true,
                            labelWidth: 150,
                            defaults: {
                                border: false,
                                anchor: '100%'
                            },
                            items: [{
                                name: 'markingMethod',
                                ref: '../../../defaultMarking',
                                xtype: 'radio',
                                fieldLabel: 'Default',
                                checked: me.section.markingMethod != Symphony.ArtisanCourseMarkingMethod.showAnswers &&
                                         me.section.markingMethod != Symphony.ArtisanCourseMarkingMethod.hideAnswers &&
                                         me.section.markingMethod != Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback,
                                afterText: 'Use global course setting.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.section.markingMethod = 0;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'markingMethod',
                                ref: '../../../showAnswers',
                                xtype: 'radio',
                                fieldLabel: 'Show Answers',
                                checked: this.section.markingMethod == Symphony.ArtisanCourseMarkingMethod.showAnswers ? true : false,
                                afterText: 'Show correct answers after questions are marked.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.section.markingMethod = Symphony.ArtisanCourseMarkingMethod.showAnswers;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'markingMethod',
                                ref: '../../../hideAnswers',
                                xtype: 'radio',
                                fieldLabel: 'Hide Answers',
                                checked: this.section.markingMethod == Symphony.ArtisanCourseMarkingMethod.hideAnswers ? true : false,
                                afterText: 'Hide correct answers after questions are marked.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.section.markingMethod = Symphony.ArtisanCourseMarkingMethod.hideAnswers;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'markingMethod',
                                ref: '../../../hideAnswersAndFeedback',
                                xtype: 'radio',
                                fieldLabel: 'Hide Answers and Feedback',
                                checked: this.section.markingMethod == Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback ? true : false,
                                afterText: 'Hide both correct answers and question feedback after questions are marked.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.section.markingMethod = Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'markingMethod',
                                ref: '../../../hideAllFeedback',
                                xtype: 'radio',
                                fieldLabel: 'Hide All Feedback',
                                checked: this.section.markingMethod == Symphony.ArtisanCourseMarkingMethod.hideAllFeedback ? true : false,
                                afterText: 'Student will not be aware if their answer is correct or incorrect.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.section.markingMethod = Symphony.ArtisanCourseMarkingMethod.hideAllFeedback;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Final Content Page',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%'
                            },
                            items: [{
                                xtype: 'artisan.tinymce',
                                border: false,
                                name: 'finalContentPage',
                                fieldLabel: 'Content',
                                content: function () {
                                    return me.section.finalContentPage;
                                }(),
                                cls: 'popup-menu-editor',
                                height: 245,
                                enableKeyEvents: true,
                                listeners: {
                                    contentchange: function (content) {
                                        me.section.finalContentPage = content;
                                        me.onContentChanged();
                                    }
                                }
                            }]
                        }]
                    }]
                }]
            });

            this.callParent(arguments);

            this.lastDescription = this.section.description;
            this.lastTestType = this.section.testType;
            this.lastIsRandomizeAnswers = this.section.isRandomizeAnswers;
            this.lastIsRandomizeQuestions = this.section.isRandomizeQuestions;
            this.lastMaxQuestions = this.section.maxQuestions;
            this.lastMarkingMethod = this.section.markingMethod;
            this.lastMaxTime = this.section.maxTime;
            this.lastFinalContentPage = this.section.finalContentPage;

            if (Symphony.Artisan.autoSaveInterval != null) {
                me.timer = window.setInterval(function () {
                    var newDescription = me.findField('description').getValue();
                    if (newDescription != me.section.description) {
                        me.section.description = newDescription;
                        me.onContentChanged();
                    }
                    //TODO: add TestType field check
                }, Symphony.Artisan.autoSaveInterval);
            }
        },
        setTestType: function (type, value) {
            if (value) {
                this.section.testType = type;

                if (type == Symphony.ArtisanTestType.sequential) {
                    this.maxQuestions.setDisabled(true);
                } else {
                    this.maxQuestions.setDisabled(false);
                }

                this.onContentChanged();
            }
        },
        setIsRandomizeAnswers: function (isRandomizeAnswers) {
            this.section.isRandomizeAnswers = isRandomizeAnswers;
            this.onContentChanged();
        },
        setIsReRandomizeOrder: function (isReRandomizeOrder) {
            this.section.isReRandomizeOrder = isReRandomizeOrder;
            this.onContentChanged();
        },
        onContentChanged: function () {
            this.setRevertDisabled(false);
        },
        onRevertClick: function () {
            this.description.setValue(this.lastDescription);
            this.isRandomizeAnswers.setValue(this.lastIsRandomizeAnswers);
            this.isRandomizeQuestions.setValue(this.lastIsRandomizeQuestions);
            this.maxQuestions.setValue(this.lastMaxQuestions);
            this.maxTime.setValue(this.lastMaxTime);

            switch (this.lastTestType) {
                case Symphony.ArtisanTestType.gameBoard:
                    this.gameBoard.setValue(true);
                    break;
                case Symphony.ArtisanTestType.gameWheel:
                    this.gameWheel.setValue(true);
                    break;
                case Symphony.ArtisanTestType.sequential:
                    this.sequential.setValue(true);
                    break;
                case Symphony.ArtisanTestType.random:
                    this.random.setValue(true);
                    break;
            }

            switch (this.lastMarkingMethod) {
                case Symphony.ArtisanCourseMarkingMethod.showAnswers:
                    this.showAnswers.setValue(true);
                    break;
                case Symphony.ArtisanCourseMarkingMethod.hideAnswers:
                    this.hideAnswers.setValue(true);
                    break;
                case Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback:
                    this.hideAnswersAndFeedback.setValue(true);
                    break;
                case Symphony.ArtisanCourseMarkingMethod.hideAllFeedback:
                    this.hideAllFeedback.setValue(true);
                default:
                    this.defaultMarking.setValue(true);
            }

            this.setRevertDisabled(true);
        },
        setRevertDisabled: function (disable) {
            this.revertButton.setDisabled(disable);
            if (this.node && disable) {
                this.node.set('cls', this.node.get('cls').replace(' node-italic', ''));
            } else {
                if (this.node.get('cls').indexOf('node-italic') == -1) {
                    this.node.set('cls', this.node.get('cls') + ' node-italic');
                }
            }
        },
        setSaved: function () {
            this.lastDescription = this.section.description;
            this.lastTestType = this.section.testType;
            this.lastIsRandomizeAnswers = this.section.isRandomizeAnswers;
            this.lastIsRandomizeQuestions = this.section.isRandomizeQuestions;
            this.lastMaxQuestions = this.section.maxQuestions;
            this.lastMarkingMethod = this.section.markingMethod;
            this.lastMaxTime = this.section.maxTime;
            this.lastFinalContentPage = this.section.finalContentPage;

            this.setRevertDisabled(true);
        }
    });

})();