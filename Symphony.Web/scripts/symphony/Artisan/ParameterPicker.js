﻿(function () {
	Symphony.Artisan.ParameterPicker = Ext.define('artisan.parameterpicker', {
	    alias: 'widget.artisan.parameterpicker',
	    extend: 'Ext.Window',
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				layout: 'fit',
				defaults: {
					border: false
				},
				items: [{
					xtype: 'artisan.parametertab',
					ref: 'parameterTab',
					readOnly: true,
					listeners: {
					    parameteravailable: function () {
					        me.okBtn.enable();
					    },
					    parameterunavailable: function () {
					        me.okBtn.disable();
					    },
					    parameterdoubleclicked: Ext.bind(this.onOkClick, this)
					}
				}],
				// buttons are placed in the footer's "items" array, so the ref has one more layer to go through
				buttons: [{
					text: 'OK',
					ref: '../okBtn',
					disabled: true,
					handler: Ext.bind(this.onOkClick, this)
				}, {
					text: 'Cancel',
					ref: '../cancelBtn',
					handler: Ext.bind(this.onCancelClick, this)
				}]
			});
			this.callParent(arguments);
		},
		onOkClick: function () {
			this.fireEvent('parmeterpicked', this.parameterTab.getActiveParameter());
			this.hide();
		},
		onCancelClick: function () {
			this.hide();
		}
	});


	Symphony.Artisan.ParameterPicker.getInstance = function (options, callback) {
	    Symphony.Artisan.ParameterPicker._instanceCallback = callback;
	    if (Symphony.Artisan.ParameterPicker._instance == null) {
	        Symphony.Artisan.ParameterPicker._instance = new Symphony.Artisan.ParameterPicker({
				title: 'Insert Parameter',
				width: 900,
				height: 600,
				cls: 'maxindex',
				modal: true,
				closeAction: 'hide',
				listeners: {
				    parmeterpicked: function (parameter) {
				        Symphony.Artisan.ParameterPicker._instanceCallback(parameter);
					}
				}
			});
		}
	    Ext.apply(Symphony.Artisan.ParameterPicker._instance, options || {});
	    return Symphony.Artisan.ParameterPicker._instance;
	};
})();