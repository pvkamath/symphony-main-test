﻿(function () {
	Symphony.Artisan.ImporterWizard = Ext.define('artisan.importerwizard', {
	    alias: 'widget.artisan.importerwizard',
	    extend: 'Ext.Window',
		course: null,
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				height: 550,
				width: 500,
				title: 'Import Wizard',
				layout: 'fit',
				items: [{
					xtype: 'panel',
					ref: 'mainPanel',
					border: false,
					defaults: {
						border: false
					},
					layout: 'card',
					activeItem: 0,
					items: [{
						//layout: 'fit',
						xtype: 'artisan.importertype',
						ref: '../inputPage',
						listeners: {
							typeSelected: function (type) {
								if (type == 'existingCourse') {
									me.targetPage.setTitle('Choose Target SCO or Learning Object');
								}
							}
						}
					}, {
						layout: 'fit',
						title: 'Choose Target Learning Object',
						xtype: 'artisan.importercoursetree',
						ref: '../targetPage',
						course: me.course,
                        listeners: {
							nodeSelected: function (node) {
								var allowed = true;
								var checked = me.inputPage.getChecked();

								if (checked == 'document') {
									allowed = node.section && node.section.sectionType == Symphony.ArtisanSectionType.learningObject;
								} else if (checked == 'existingCourse') {
									allowed = node.isRoot || (node.section && (node.section.sectionType == Symphony.ArtisanSectionType.learningObject || node.section.sectionType == Symphony.ArtisanSectionType.sco));

									if (node.section && node.section.sectionType == Symphony.ArtisanSectionType.learningObject) {
										me.courseTree.setTitle('Choose a Page to Import...');
									} else if ((node.section && node.section.sectionType == Symphony.ArtisanSectionType.sco) || node.isRoot) {
										me.courseTree.setTitle('Choose a Learning Object or Page to Import...');
									}
								}

								if (allowed) {
									me.nextButton.enable();
								} else {
									me.nextButton.disable();
								}
							}
						}
					}, {
						layout: 'fit',
						xtype: 'artisan.importerfilesource',
						ref: '../sourcePage',
						listeners: {
							uploadComplete: function (sectionId, pages) {
								me.fireEvent('importComplete', sectionId, pages);
								me.close();
							}
						}
					}, {
						title: 'Choose Existing Course to Import',
						id: 'artisan.importcoursesgrid',
                        xtype: 'artisan.coursesgrid',
						ref: '../coursesGrid',
						hideAddButton: true,
						listeners: {
							cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
								me.nextButton.enable();
								var record = grid.store.getAt(rowIndex);
								me.courseTree.setCourseId(record.get('id'));
							}
						}
					}, {
						layout: 'fit',
						title: 'Choose a Learning Object or Page to Import...',
						xtype: 'artisan.importercoursetree',
						ref: '../courseTree',
						showPages: true,
						listeners: {
						    nodeSelected: function (selectedNode) {
						        var targetNode = me.targetPage.getSelectedNode();
								var targetType = targetNode.section ? targetNode.section.sectionType : Symphony.ArtisanSectionType.sco;
								var loAllowed = false;

								if (targetType == Symphony.ArtisanSectionType.sco) {
									loAllowed = true;
								}

								// if the target node is a SCO, we can put:
								// 1) LOs if the SCO contains LOs
								// 2) SCOs if the SCO contains SCOs
								// 3) Either if there are no children at all
								// 4) No pages
								// if the target node is a LO, we can put:
								// 1) pages
								// 2) an entire LO (moving the pages only)
								var enable = false;
								if (targetType == Symphony.ArtisanSectionType.sco) {
									if (!selectedNode.section) {
										// no pages, per #4
										enable = false;
									} else if (targetNode.childNodes.length == 0) {
										// allow anything else if no children, per #3
										enable = true;
									} else {
										// #1 and #2
										enable = (selectedNode.section.sectionType == targetNode.childNodes[0].section.sectionType);
									}
								} else {
									// LO
									if (!selectedNode.section) {
										// it's a page, allow it, per #1
										enable = true;
									} else if (targetType == Symphony.ArtisanSectionType.learningObject && selectedNode.section && selectedNode.section.sectionType == Symphony.ArtisanSectionType.learningObject) {
										// both source and target are LOs, allow it per #2
										enable = true;
									}
								}
								me.nextButton.setDisabled(!enable);

/*if((selectedNode.section && ((selectedNode.section.sectionType == Symphony.ArtisanSectionType.learningObject && loAllowed)) || node.page)){
                                                                                                                                                                                                                                                                                                            me.nextButton.enable();
                                                                                                                                                                                                                                                                                                            } else{
                                                                                                                                                                                                                                                                                                            me.nextButton.disable();
                                                                                                                                                                                                                                                                                                            }*/
							}
						}
					}]
				}],
				bbar: {
					items: [{
						xtype: 'button',
						text: 'Previous',
						disabled: true,
						ref: '../previousButton',
						iconCls: 'x-button-previous',
						listeners: {
							click: Ext.bind(me.onPreviousClick, this)
						}
					}, '->',
					{
						xtype: 'button',
						text: 'Next',
						ref: '../nextButton',
						iconCls: 'x-button-next',
						listeners: {
							click: Ext.bind(me.onNextClick, this)
						}
					}]
				},
				listeners: {
					activate: function (window) {
						this.coursesGrid.refresh();
					}
				}
			});

			this.callParent(arguments);
		},
		importExisting: function () {
			var source = this.courseTree.getSelectedNode();
			var target = this.targetPage.getSelectedNode();

			this.clearIds(source);

			this.fireEvent('importExisting', target, source);
			this.close();
		},
		clearIds: function (node) {
			if (node.section) {
				node.section.id = 0;
				if (node.section.pages) {
					for (var i = 0; i < node.section.pages.length; i++) {
						node.section.pages[i].id = 0;
					}
				} else if (node.section.sections) {
					for (var i = 0; i < node.section.sections.length; i++) {
						this.clearIds(node.section.sections[i]);
					}
				}
			} else if (node.page) {
				node.page.id = 0;
			}
		},
		setFinishButton: function () {
			this.nextButton.setText('Finish');
			this.nextButton.setIconCls('x-button-finish');
		},
		setNextButton: function () {
			this.nextButton.setText('Next');
			this.nextButton.setIconCls('x-button-next');
		},
		onNextClick: function () {
		    var active = this.mainPanel.layout.activeItem;

			if (active == this.inputPage) {
				this.previousButton.enable();

				if (this.targetPage.getSelectedNode()) {
					this.nextButton.enable();
				} else {
					this.nextButton.disable();
				}

				active = this.targetPage;
			} else if (active == this.targetPage) {
				var input = this.inputPage.getChecked();
				if (input == 'document') {
					active = this.sourcePage;

					this.previousButton.enable();
					this.nextButton.hide();

					this.sourcePage.setCourseId(this.course.id);
					this.sourcePage.setSectionId(this.targetPage.getSelectedId());
					this.sourcePage.setOptions(this.inputPage.getWidth(), this.inputPage.getHeight(), this.inputPage.getScaleType(), this.inputPage.getCropX(), this.inputPage.getCropY());
				} else if (input == 'existingCourse') {
					active = this.coursesGrid;

					this.previousButton.enable();
					this.nextButton.show();

					if (this.coursesGrid.getSelected()) {
						this.nextButton.enable();
					} else {
						this.nextButton.disable();
					}
				}
			} else if (active == this.coursesGrid) {
				active = this.courseTree;

				this.previousButton.enable();
				this.nextButton.show();
				this.nextButton.disable();

				this.setFinishButton();
			} else if (active == this.courseTree) {
				this.importExisting();
				return;
			}

			this.mainPanel.layout.setActiveItem(active);
		},
		onPreviousClick: function () {
		    var active = this.mainPanel.layout.activeItem;

			if (active == this.targetPage) {
				active = this.inputPage;

				this.previousButton.disable();
				this.nextButton.enable();
			} else if (active == this.sourcePage || active == this.coursesGrid) {
				active = this.targetPage;

				this.previousButton.enable();
				this.nextButton.show();

				if (this.targetPage.getSelectedNode()) {
					this.nextButton.enable();
				} else {
					this.nextButton.disable();
				}
			} else if (active == this.courseTree) {
				active = this.coursesGrid;

				this.setNextButton();

				this.previousButton.enable();
				this.nextButton.show();

				if (this.coursesGrid.getSelected()) {
					this.nextButton.enable();
				} else {
					this.nextButton.disable();
				}
			}

			this.mainPanel.layout.setActiveItem(active);
		}
	});

})();