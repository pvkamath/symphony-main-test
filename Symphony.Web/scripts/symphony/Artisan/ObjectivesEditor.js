﻿(function () {
	Symphony.Artisan.ObjectivesEditor = Ext.define('artisan.objectiveseditor', {
	    alias: 'widget.artisan.objectiveseditor',
	    extend: 'artisan.optionseditor',
		course: null,
		section: null,
		bodyStyle: 'padding: 15px;',
		initComponent: function () {
			var me = this;

			var store = Ext.create('Ext.data.TreeStore', {
			    model: 'simple',
			    proxy: {
			        type: 'memory'
			    },
			    root: {
			        text: 'Course Root',
			        expanded: true,
			        expandable: true,
			        draggable: false,
			        iconCls: 'x-treenode-course',
			        children: []
			    }
			});

			Ext.apply(this, {
				items: [{
					frame: true,
					xtype: 'treepanel',
					autoScroll: true,
					split: true,
					border: false,
					ref: 'tree',
					rootVisible: false,
					loader: new Ext.tree.TreeLoader(),
					cls: 'multiline',
				    store: store
				}],
				listeners: {
					activate: function () {
						me.buildObjectives();
					} // activate
				}
			});
			this.callParent(arguments);
		},
		buildObjectives: function () {
			var root = this.tree.getRootNode();
			root.removeAll();
			root.appendChild(this.buildSectionObjectives(this.course, root));
		},
		buildSectionObjectives: function (parentSection) {
			if (parentSection != this.course && parentSection.sectionType != Symphony.ArtisanSectionType.learningObject && parentSection.sectionType != Symphony.ArtisanSectionType.sco) {

				return null;
			}
			var node = new Ext.tree.TreeNode({
				text: parentSection.name + '<br/><span class="content">' + (parentSection.objectives || '<i>No objectives specified</i>') + '</span>',
				expanded: true,
				expandable: true,
				iconCls: parentSection.sections ? 'x-menu-sco' : 'x-menu-learningobject',
				children: parentSection.sections ? [] : null,
				leaf: parentSection.sections ? false : true,
                loaded: true
			});

			if (parentSection.sections) {
				for (var i = 0; i < parentSection.sections.length; i++) {
					var section = parentSection.sections[i];
					var child = this.buildSectionObjectives(section, node);
					if (child) {
						node.appendChild(child);
					}
				}
			}
			return node;
		}

	});

})();