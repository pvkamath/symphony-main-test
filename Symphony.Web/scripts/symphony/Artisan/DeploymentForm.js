﻿(function () {
    Symphony.Artisan.DeploymentForm = Ext.define('artisan.deploymentform', {
        alias: 'widget.artisan.deploymentform',
        extend: 'Ext.Panel',
        selectLogBtnText: 'View Results',
        selectDeploymentBtnText: 'View Deployment',
        selectLogBtnCls: 'x-button-deployment-log',
        selectDeploymentBtnCls: 'x-button-deployment',
        activeItem: 0,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'card',
                activeItem: 0,
                listeners: {
                    afterlayout: function () {
                        if (me.deployment.id > 0 && me.deployment.running) {
                            this.selectLog();
                        }
                    }
                },
                tbar: {
                    items: [{
                        text: 'Apply Deployment',
                        iconCls: 'x-button-save',
                        name: 'applyBtn',
                        disabled: me.deployment && me.deployment.running ? true : false,
                        handler: function () {
                            var sto = me.symphonyTargetOption.getValue();
                            var btn = this;
                            var resultToggleBtn = me.getTopToolbar().find('name', 'resultsToggle')[0];
                            var currentResultDisabled = resultToggleBtn.disabled;

                            var deployment = {
                                courseIds: me.coursesGrid.getPlugin('pagingSelectionPersistence').getPersistedSelectionIds(),
                                customerIds: me.customersGrid.getPlugin('pagingSelectionPersistence').getPersistedSelectionIds(),
                                name: me.deploymentName.getValue(),
                                notes: me.deploymentNotes.getValue(),
                                isUpdate: (sto == 'updateAdd'),
                                isUpdateOnly: (sto == 'updateOnly'),
                                isAutoUpdate: (sto == 'autoUpdate'),
                                toArtisan: me.toArtisan.getValue(),
                                toSymphony: me.toSymphony.getValue(),
                                toMars: me.toMars.getValue()
                            };
                            btn.setDisabled(true);
                            resultToggleBtn.setDisabled(true);

                            Symphony.Ajax.request({
                                timeout: 240 * 1000,
                                url: '/services/artisan.svc/deployment',
                                jsonData: deployment,
                                method: 'POST',
                                success: function (result) {
                                    me.deployment = result.data;
                                    me.setTitle(result.data.name);
                                    me.fireEvent('applied');
                                    me.selectLog();
                                },
                                failure: function (result) {
                                    me.selectDeployment();
                                    Ext.Msg.alert('Error', result.error);
                                    btn.setDisabled(false);
                                    if (!currentResultDisabled) {
                                        resultToggleBtn.setDisabled(false);
                                    }
                                }
                            });
                        }
                    }, {
                        text: 'View Results',
                        name: 'resultsToggle',
                        iconCls: 'x-button-deployment-log',
                        disabled: me.deployment && me.deployment.id > 0 && !me.deployment.running ? false : true,
                        handler: function () {
                            if (me.activeItem == 0) {
                                me.selectLog();
                            } else {
                                me.selectDeployment();
                            }
                        }
                    }]
                },
                items: [{
                    xtype: 'panel',
                    name: 'deployment',
                    layout: 'fit',
                    border: false,
                    
                    items: [{
                        name: 'general',
                        xtype: 'form',
                        border: false,
                        frame: true,
                        labelWidth: 45,
                        labelPad: 5,
                        layout: 'hbox',
                        autoScroll: true,
                        layoutConfig: {
                            defaultMargins: {
                                top: 5,
                                left: 5,
                                right: 5,
                                bottom: 5
                            }
                        },
                        items: [{
                            xtype: 'fieldset',
                            flex: 0.2,
                            border: false,
                            style: 'padding: 0px;',
                            items: [{
                                xtype: 'fieldset',
                                title: 'General',
                                border: true,
                                labelAlign: 'top',
                                width: 'auto',
                                defaults: {
                                    anchor: '100%'
                                },
                                items: [{
                                    xtype: 'textfield',
                                    value: me.deployment.name,
                                    fieldLabel: 'Name',
                                    name: 'name',
                                    ref: '../../../../deploymentName',
                                    labelStyle: 'width:40px'
                                }, {
                                    xtype: 'textarea',
                                    value: me.deployment.notes,
                                    fieldLabel: 'Notes',
                                    name: 'notes',
                                    height: 308,
                                    ref: '../../../../deploymentNotes',
                                    labelStyle: 'width:40px'
                                }]
                            }, {
                                xtype: 'fieldset',
                                title: 'Target(s)',
                                border: true,
                                labelWidth: 20,
                                width: 'auto',
                                defaults: {
                                    labelSeparator: '',
                                    anchor: '100%'
                                },
                                items: [{
                                    xtype: 'checkbox',
                                    checked: me.deployment.toArtisan,
                                    fieldLabel: '<img src="/images/palette.png" />',
                                    boxLabel: 'To Artisan',
                                    name: 'toArtisan',
                                    ref: '../../../../toArtisan'
                                }, {
                                    xtype: 'checkbox',
                                    checked: me.deployment.toSymphony,
                                    fieldLabel: '<img src="/images/symphony.png" />',
                                    boxLabel: 'To Symphony',
                                    name: 'toSymphony',
                                    ref: '../../../../toSymphony'
                                }, {
                                    xtype: 'combo',
                                    ref: '../../../../symphonyTargetOption',
                                    valueField: 'id',
                                    displayField: 'name',
                                    queryMode: 'local',
                                    triggerAction: 'all',
                                    forceSelection: true,
                                    editable: false,
                                    typeAhead: false,
                                    store: new Ext.data.ArrayStore({
                                        fields: ['id', 'name'],
                                        data: [
                                            ['addOnly', 'Add only'],
                                            ['updateAdd', 'Update existing or add'],
                                            ['updateOnly', 'Update existing only'],
                                            ['autoUpdate', 'Automatic Update']
                                        ]
                                    }),
                                    value: (me.deployment.isAutoUpdate ? 'autoUpdate' :
                                             (me.deployment.isUpdateOnly ? 'updateOnly' :
                                                (me.deployment.isUpdate ? 'updateAdd' : 'addOnly')
                                             )
                                           ),
                                    listeners: {
                                        select: function (combo, record, index) {
                                            // always check the Symphony box if the dropdown is selected
                                            me.toSymphony.setValue(true);

                                            var isAutoUpdate = (record.get('id') == 'autoUpdate');
                                            me.updateFormState(isAutoUpdate);
                                        },
                                        render: function (combo) {
                                            var isAutoUpdate = (combo.getValue() == 'autoUpdate');
                                            me.updateFormState(isAutoUpdate);
                                        }
                                    }
                                }, {
                                    xtype: 'checkbox',
                                    checked: me.deployment.toMars,
                                    fieldLabel: '<img src="/images/monitor.png" />',
                                    boxLabel: 'To Mars',
                                    name: 'toMars',
                                    ref: '../../../../toMars'
                                }]
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Course(s)',
                            border: true,
                            flex: 0.4,
                            items: [{
                                xtype: 'artisan.publishedcoursesgrid',
                                ref: '../../../coursesGrid',
                                border: true,
                                width: 'auto',
                                listeners: {
                                    afterrender: function () {
                                        this.getPlugin('pagingSelectionPersistence').setSelections(me.deployment.courseIds);
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Customer(s)',
                            border: true,
                            flex: 0.4,
                            ref: '../../customersFieldset',
                            items: [{
                                xtype: 'artisan.customersgrid',
                                ref: '../../../customersGrid',
                                width: 'auto',
                                border: true,
                                listeners: {
                                    render: function () {
                                        this.getPlugin('pagingSelectionPersistence').setSelections(me.deployment.customerIds);
                                    }
                                }
                            }]
                        }]
                    }]
                }, {
                    xtype: 'panel',
                    name: 'results',
                    border: false,
                    layout: 'fit',
                    items: {
                        xtype: 'panel',
                        border: false,
                        frame: true,
                        layout: 'border',
                        items: [{
                            xtype: 'progress',
                            animate: true,
                            text: 'Initializing...',
                            height: 20,
                            region: 'north'
                        }, {
                            title: 'Deployment Log',
                            xtype: 'artisan.deploymentinfogrid',
                            deploymentId: me.deployment.id,
                            region: 'center',
                            listeners: {
                                deploymentUpdate: function (record) {
                                    var pbar = me.find('xtype', 'progress')[0],
                                        current = record.get('currentCount'),
                                        total = record.get('totalCount'),
                                        text = 'Processed ' + current + ' of ' + total;
                                    if (current < total) {
                                        if (record.get('running') || record.get('queued')) {

                                            var progressText = record.get('running') ? text + '...' : 'Deployment queued.';
                                            var percent = record.get('running') ? current / total : 0;

                                            pbar.updateProgress(percent, progressText, false);
                                            me.getTopToolbar().find('name', 'resultsToggle')[0].setDisabled(true);
                                            me.getTopToolbar().find('name', 'applyBtn')[0].setDisabled(true);

                                        } else if (record.get('queued')) {
                                            pbar.updateProgress(0, 'Deployment queued');
                                            me.getTopToolbar().find('name', 'resultsToggle')[0].setDisabled(true);
                                            me.getTopToolbar().find('name', 'applyBtn')[0].setDisabled(true);
                                        } else {
                                            pbar.updateProgress(0, 'Failed - (' + text + ')');
                                            me.getTopToolbar().find('name', 'resultsToggle')[0].setDisabled(false);
                                            me.getTopToolbar().find('name', 'applyBtn')[0].setDisabled(false);
                                            me.deployment.running = false;
                                        }
                                    } else {
                                        pbar.updateProgress(1, 'Complete - Processed ' + total + ' items', false);
                                        me.getTopToolbar().find('name', 'resultsToggle')[0].setDisabled(false);
                                        me.getTopToolbar().find('name', 'applyBtn')[0].setDisabled(false);
                                        me.deployment.running = false;
                                    }                                        
                                }
                            }
                        }]
                    }
                }]
            });
            this.callParent(arguments);
        },
        refreshLog: function() {
            var grid = this.find('xtype', 'artisan.deploymentinfogrid')[0],
                pbar = this.find('xtype', 'progress')[0];

            pbar.updateProgress(0, 'Initializing...', false);
            grid.startLog(this.deployment.id);
        },
        selectLog: function () {
            this.activeItem = 1;
            this.getLayout().setActiveItem(1);
            this.refreshLog();

            var toggleBtn = this.getTopToolbar().find('name', 'resultsToggle')[0];
            toggleBtn.setIconCls(this.selectDeploymentBtnCls).setText(this.selectDeploymentBtnText);
        },
        selectDeployment: function () {
            this.activeItem = 0;
            this.getLayout().setActiveItem(0);

            var toggleBtn = this.getTopToolbar().find('name', 'resultsToggle')[0];
            toggleBtn.setIconCls(this.selectLogBtnCls).setText(this.selectLogBtnText);
        },
        onRender: function () {
            Symphony.Artisan.DeploymentForm.superclass.onRender.apply(this, arguments);
            this.load();
        },
        load: function () {

        },
        updateFormState: function (isAutoUpdate) {
            var me = this;

            // clear out non-Symphony check boxes
            if (isAutoUpdate) {
                me.toArtisan.setValue(false);
                me.toMars.setValue(false);
            }
            me.customersFieldset.setDisabled(isAutoUpdate);
            me.toArtisan.setDisabled(isAutoUpdate);
            me.toMars.setDisabled(isAutoUpdate);
        }
    });

})();