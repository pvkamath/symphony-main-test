﻿(function () {
    Symphony.Artisan.AssetLinkButton = Ext.define('artisan.assetlinkbutton', {
        alias: 'widget.artisan.assetlinkbutton',
        extend: 'Ext.Button',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                text: 'Link',
                iconCls: 'x-button-link',
                xtype: 'button',
                listeners: {
                    click: function () {
                        Ext.Msg.prompt('Enter a URL', 'Please enter a valid asset URL:', function (btn, value) {
                            if (btn == 'ok') {
                                Ext.Msg.wait('Please wait while your asset is created...');
                                Symphony.Ajax.request({
                                    method: 'POST',
                                    url: '/services/artisan.svc/assets/',
                                    jsonData: {
                                        url: value
                                    },
                                    success: function (result) {
                                        me.fireEvent('linkcomplete', result.data);
                                        Ext.Msg.hide();
                                    }
                                });
                            }
                        });
                    }
                }
            });
            Symphony.Artisan.AssetLinkButton.superclass.initComponent.call(this);
        }
    });

})();