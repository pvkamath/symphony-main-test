﻿(function () {
    Symphony.Artisan.CourseOptionsEditor = Ext.define('artisan.courseoptionseditor', {
        alias: 'widget.artisan.courseoptionseditor',
        extend: 'artisan.optionseditor',
        course: null,
        lastCourse: null,
        labelWidth: 145,
        popupMenuTpl: '<div class="pdf-menu" data-title="{0}" {1}>{2}</div>',
        initComponent: function () {
            var me = this,
                id = this.course.id;

            if (me.course && me.course.theme && me.course.theme.isDynamic) {
                me.course.theme.cssPath = me.getThemeCssUrl(me.course.theme);
            }

            Ext.apply(this, {
                layout: 'border',
                listeners: {
                    beforedestroy: function (comp) {
                        if (me.timer) {
                            window.clearInterval(me.timer);
                        }
                    }
                },
                items: [{
                    region: 'north',
                    height: 27,
                    border: false,
                    xtype: 'toolbar',
                    items: [{
                        text: 'Revert to Last Save',
                        iconCls: 'x-button-revert',
                        ref: '../revertButton',
                        disabled: true,
                        xtype: 'button',
                        handler: Ext.bind(me.onRevertClick, this)
                    }]
                }, {
                    region: 'center',
                    xtype: 'panel',
                    layout: 'fit',
                    border: false,
                    bodyStyle: 'padding: 15px;',
                    items: [{
                        layout: 'form',
                        frame: true,
                        autoScroll: true,
                        bodyStyle: 'padding-right:20px',
                        items: [{
                            xtype: 'fieldset',
                            title: 'General',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'name',
                                ref: '../../../name',
                                fieldLabel: 'Name',
                                allowBlank: false,
                                xtype: 'textfield',
                                allowBlank: false,
                                value: this.course.name,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.lastName = me.course.name;
                                        me.course.name = field.getValue();
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                name: 'description',
                                ref: '../../../description',
                                fieldLabel: 'Description',
                                xtype: 'symphony.spellcheckarea',
                                allowBlank: true,
                                value: this.course.description,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.description = field.getValue();
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                name: 'associatedImageData',
                                fieldLabel: 'Image',
                                value: me.course.associatedImageData,
                                xtype: 'associatedimagefield',
                                cls: Symphony.Modules.getModuleClass(Symphony.Modules.Libraries),
                                listeners: {
                                    thumbnail: function (file, dataUrl) {
                                        me.course.associatedImageData = dataUrl;
                                    },
                                    afterrender: function () {
                                        // once the image data is loaded, trash it so we don't re-send it to the server
                                        // just saves on bandwidth
                                        delete me.course.associatedImageData;
                                    },
                                    deleted: function () {
                                        // sending -1 for the image will clear out the image data.
                                        // Need to do this since the above listener prevents any 
                                        // image data from sending if it has not changed. 
                                        me.course.associatedImageData = -1;
                                    }
                                }
                            }, {
                                name: 'keywords',
                                ref: '../../../keywords',
                                fieldLabel: 'Keywords',
                                xtype: 'textfield',
                                value: this.course.keywords,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.keywords = field.getValue();
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                name: 'accreditation',
                                xtype: 'symphony.spellcheckarea',
                                fieldLabel: 'Accreditation',
                                ref: '../../../accreditation',
                                allowBlank: true,
                                value: this.course.accreditation,
                                cls: Symphony.Modules.getModuleClass(Symphony.Modules.Libraries),
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.accreditation = field.getValue();
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                xtype: 'fieldcontainer',
                                fieldLabel: 'Accreditations',
                                items: [{
                                    xtype: 'artisan.accreditationgrid',
                                    itemId: 'accreditation-grid',
                                    fieldLabel: 'Accreditation',
                                    courseId: this.course.id,
                                    height: 192
                                }]
                            }, {
                                name: 'objectives',
                                ref: '../../../objectives',
                                fieldLabel: 'Objectives',
                                xtype: 'symphony.spellcheckarea',
                                value: this.course.objectives,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.objectives = field.getValue();
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                xtype: 'symphony.pagedcombobox',
                                url: '/services/artisan.svc/themes/', // Artisan theme folders - some may be dynamic
                                name: 'themeId',
                                fieldLabel: 'Theme',
                                bindingName: 'theme.name',
                                model: 'artisanTheme',
                                valueField: 'id',
                                displayField: 'name',
                                allowBlank: false,
                                emptyText: 'Select a Theme',
                                valueNotFoundText: 'Select a Theme',
                                listeners: {
                                    select: function (combo, records, eOpts) {
                                        // change the theme for all open pages
                                        var record = records[0];
                                        me.course.themeId = record.data.id;
                                        
                                        if (record.data.isDynamic) {
                                            record.data.cssPath = me.getThemeCssUrl(record.data)
                                        }

                                        me.onContentChanged();

                                        me.fireEvent('themechanged', record.data);
                                        var themeFlavorCombo = me.query('[name=themeFlavorId]')[0];
                                        if (themeFlavorCombo) {
                                            themeFlavorCombo.allowBlank = !record.get('isDynamic');
                                            themeFlavorCombo.setVisible(record.get('isDynamic'));
                                            themeFlavorCombo.validate();
                                        }
                                    }
                                }
                            }, {
                                xtype: 'symphony.pagedcombobox',
                                url: '/services/customer.svc/themes/', // Not really themes, but groups of settings for a dynamic theme.... A flavor!
                                name: 'themeFlavorId',
                                fieldLabel: 'Theme Flavor',
                                model: 'Theme',
                                valueField: 'id',
                                displayField: 'name',
                                allowBlank: true,
                                emptyText: 'Select a Flavor',
                                valueNotFoundText: 'Select a Flavor',
                                listeners: {
                                    select: function (combo, records, eOpts) {
                                        var record = records[0];
                                        me.course.themeFlavorId = record.data.id;

                                        if (me.course.theme.isDynamic) {
                                            me.course.theme.cssPath = me.getThemeCssUrl(me.course.theme);
                                        }

                                        me.onContentChanged();
                                        me.fireEvent('themechanged', me.course.theme);
                                    }
                                },
                                bindingName: 'themeFlavor.name',
                                hidden: (me.course && me.course.theme && !me.course.theme.isDynamic) || !me.course || !me.course.theme
                            }, {
                                hidden: true,
                                hideLabel: true,
                                name: 'headerImageUrl',
                                ref: '../../../headerImageUrl',
                                fieldLabel: 'Header Image',
                                xtype: 'textfield',
                                value: this.course.headerImageUrl,
                                emptyText: 'Enter image URL to override default header image',
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.headerImageUrl = field.getValue();
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                xtype: 'symphony.pagedcombobox',
                                url: '/services/artisan.svc/mediaplayers/',
                                name: 'mediaPlayer',
                                fieldLabel: 'Media Player',
                                bindingName: 'mediaPlayer',
                                model: 'MediaPlayer',
                                valueField: 'name',
                                displayField: 'name',
                                allowBlank: true,
                                emptyText: 'Default',
                                valueNotFoundText: 'Default',
                                pageSize: 999,
                                listeners: {
                                    afterrender: function (combo) {
                                        if (!me.course.mediaPlayer) {
                                            me.course.mediaPlayer = 'Default';
                                            combo.setValue(me.course.mediaPlayer);
                                        }
                                    },
                                    select: function (combo, records, eOpts) {
                                        var record = records[0];
                                        me.course.mediaPlayer = record.data.name;
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                xtype: 'compositefield',
                                anchor: '100%',
                                fieldLabel: 'Category',
                                name: 'categoryCompositeField',
                                items: [{
                                    xtype: 'textfield',
                                    valueField: 'id',
                                    name: 'levelIndentText',
                                    displayField: 'name',
                                    allowBlank: false,
                                    flex: 1,
                                    emptyText: 'Select a category...',
                                    valueNotFoundText: 'Select a category...',
                                    value: this.course.levelIndentText,
                                    readOnly: true
                                }, {
                                    xtype: 'button',
                                    text: '...',
                                    handler: function (btn) {
                                        var w = Symphony.Categories.getCategoryEditorWindow('artisan', function (node) {
                                            var categoryText = node.attributes.record.get('levelIndentText');
                                            me.find('name', 'categoryCompositeField')[0].items.items[0].setValue(categoryText);
                                            me.course.categoryId = node.attributes.record.get('id');
                                            me.onContentChanged();
                                            w.close();
                                        }, me.course.categoryId).show();
                                    }
                                }]
                            },{
                                xtype: 'fieldcontainer',
                                fieldLabel: 'Secondary Categories',
                                items: [{
                                    name: 'secondaryCategoryList',
                                    xtype: 'artisan.secondarycategorieslist',
                                    data: this.course.secondaryCategories ? this.course.secondaryCategories : [],
                                    height: 105
                                }],
                                cls: Symphony.Modules.getModuleClass(Symphony.Modules.Libraries)
                            }, {
                                xtype: 'fieldcontainer',
                                fieldLabel: 'Authors',
                                cls: Symphony.Modules.getModuleClass(Symphony.Modules.Books),
                                items: [{
                                    fieldLabel: 'Authors',
                                    name: 'authorslist',
                                    xtype: 'artisan.authorslist',
                                    data: this.course.authors ? this.course.authors : [],
                                    height: 124
                                }]
                            }, Symphony.getCombo('contentType', 'Content Type', Symphony.getArtisanContentTypeStore(), {
                                listeners: {
                                    select: function (cbo, records, opts) {
                                        me.course.contentType = records[0].get('id');
                                    }
                                }
                            }), {
                                name: 'artisanID',
                                xtype: 'displayfield',
                                fieldLabel: 'Artisan ID',
                                listeners: {
                                    bindvalue: function (field) {
                                        if (me.course.id < 1) {
                                            field.setValue('');
                                        } else {
                                            field.setValue(me.course.id);
                                        }
                                    }
                                }
                            }, {
                                name: 'externalId',
                                xtype: 'displayfield',
                                fieldLabel: 'External ID',
                                listeners: {
                                    bindvalue: function (field) {
                                        if (me.course.id < 1) {
                                            field.setValue('');
                                        } else {
                                            field.setValue(me.course.externalId);
                                        }
                                    }
                                }
                            }, {
                                name: 'lastPublishedOn',
                                xtype: 'displayfield',
                                fieldLabel: 'Last Published',
                                listeners: {
                                    bindvalue: function (field) {
                                        //var dateString = Symphony.dateRenderer(field.getValue());
                                        if (field.getValue()) {
                                            var dateString = Symphony.parseDate(field.getValue()).formatSymphony('m/d/Y');
                                            if (dateString == '12/31/3938') { dateString = 'n/a'; }
                                            field.setValue(dateString);
                                        } else {
                                            field.setValue('n/a');
                                        }
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Package Settings',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'contactEmail',
                                ref: '../../../contactEmail',
                                xtype: 'textfield',
                                fieldLabel: 'Contact email',
                                allowBlank: true,
                                value: this.course.contactEmail,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.contactEmail = field.getValue();
                                        me.onContentChanged();
                                    }
                                },
                                help: {
                                    title: 'What is the contact email?',
                                    text: 'The contact email is the email address of the main contact for the course.'
                                }
                            }, {
                                name: 'disclaimer',
                                ref: '../../../disclaimer',
                                xtype: 'symphony.spellcheckfield',
                                //'textfield',
                                fieldLabel: 'Disclaimer',
                                value: this.course.disclaimer,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.disclaimer = field.getValue();
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                name: 'passingScore',
                                ref: '../../../passingScore',
                                xtype: 'numberfield',
                                fieldLabel: 'Passing Score',
                                value: this.course.passingScore || 80,
                                minValue: 0,
                                maxValue: 100,
                                disabledForModes: 'mastery',
                                masteryDefault: 100,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.passingScore = field.getValue();
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                name: 'isUseAutomaticDuration',
                                ref: '../../../isUseAutomaticDuration',
                                xtype: 'checkbox',
                                fieldLabel: 'Duration',
                                checked: this.course.isUseAutomaticDuration,
                                boxLabel: 'Use duration automatically calculated for this course by Artisan.',
                                help: {
                                    title: 'Duration',
                                    text: 'The expected minimum amount of time it will take a student to complete the course. This is used when calculating how far a student has progressed through a training program. If the duration is automatically calculated, the minimum timers set on the course are used.'
                                },
                                listeners: {
                                    check: function (chkbox, value) {
                                        me.course.isUseAutomaticDuration = value;
                                        
                                        if (value) {
                                            me.course.oldDuration = me.duration.getValue();

                                            if (!me.course.automaticDuration) {
                                                Ext.Msg.alert("Automatic Duration", "Automatic duration will be calculated after saving the course.");
                                            }
                                            me.duration.setValue(me.course.automaticDuration);
                                            me.duration.setDisabled(true);

                                        } else {
                                            me.duration.setValue(me.course.oldDuration ? me.course.oldDuration : me.course.duration);
                                            me.duration.setDisabled(false);
                                        }

                                        
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                name: 'duration',
                                ref: '../../../duration',
                                xtype: 'classroom.durationfield',
                                fieldLabel: '',
                                layout: 'hbox',
                                disabled: this.course.isUseAutomaticDuration,
                                enableKeyEvents: true,
                                allowBlank: false,
                                hideEmptyLabel: false,
                                //labelConnector: '',
                                //labelSeparator: '',
                                //itemCls: 'hide-label',
                                listeners: {
                                    keyup: function (field, event) {
                                        if (!me.course.isUseAutomaticDuration) {
                                            me.course.duration = field.getValue();
                                            me.onContentChanged();
                                        }
                                    },
                                    change: function (args) {
                                        if (!me.course.isUseAutomaticDuration) {
                                            me.course.duration = args.duration;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Mastery (beta)',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'isMasteryEnabled',
                                checked: this.course.isMasteryEnabled,
                                xtype: 'checkbox',
                                fieldLabel: 'Enable Mastery',
                                boxLabel: 'Enabling mastery will use Artisan beta features. Not all existing features will be available when mastery is enabled.',
                                handler: function (chk, checked) {
                                    me.course.isMasteryEnabled = checked;
                                    me.fireEvent('toggleartisanmode', 'mastery', checked);
                                }
                            }, {
                                name: 'masteryLevel',
                                fieldLabel: 'Mastery Level',
                                boxLabel: '<b>Level One:</b> Students are forced to answer all questions correctly. For any questions they get incorrect, they will have to retry at the end of the course until they answer correctly.',
                                value: Symphony.ArtisanMasteryLevel.levelOne,
                                checked: this.course.masteryLevel == Symphony.ArtisanMasteryLevel.levelOne ? true : false,
                                xtype: 'radio',
                                enabledForModes: 'mastery',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.masteryLevel = Symphony.ArtisanMasteryLevel.levelOne;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'masteryLevel',
                                hideEmptyLabel: false,
                                boxLabel: '<b>Level Two:</b> If students answer any question incorrectly, they will go through the entire course again. At the end of the course they will be presented with all questions that were answered incorrectly. All questions must be answered correctly at least twice.',
                                value: Symphony.ArtisanMasteryLevel.levelTwo,
                                checked: this.course.masteryLevel == Symphony.ArtisanMasteryLevel.levelTwo ? true : false,
                                xtype: 'radio',
                                enabledForModes: 'mastery',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.masteryLevel = Symphony.ArtisanMasteryLevel.levelTwo;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'isUspap',
                                fieldLabel: 'Enable USPAP Mode',
                                boxLabel: 'When mastery level two is enabled, the second attempt will only show questions that were answered incorrectly in the first attempt.',
                                xtype: 'checkbox',
                                checked: this.course.isUspap,
                                enabledForModes: 'mastery',
                                listeners: {
                                    check: function (field, checked) {
                                        me.course.isUspap = checked;
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                name: 'isAntiGuessing',
                                fieldLabel: 'Enable Anti Guessing',
                                boxLabel: 'When anti guessing is enabled, the student much achieve at least 50% in the course before they can enter mastery mode.',
                                xtype: 'checkbox',
                                checked: this.course.isAntiGuessing,
                                enabledForModes: 'mastery',
                                listeners: {
                                    check: function (field, checked) {
                                        me.course.isAntiGuessing = checked;
                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                name: 'minimumMasteryQuestions',
                                fieldLabel: 'Min. Mastery Questions',
                                xtype: 'numberfield',
                                enabledForModes: 'mastery',
                                value: this.course.minimumMasteryQuestions,
                                help: {
                                    title: 'Minimum Mastery Questions',
                                    text: 'The minimum number of questions to present in the mastery section. If the number of incorrect questions is less than the minimum, a random selection of questions will be added to the mastery section. The student does not have to answer these correctly in order to move on.'
                                },
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.minimumMasteryQuestions = field.getValue();
                                        me.onContentChanged();
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Completion Method',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'completionMethod',
                                ref: '../../../test',
                                xtype: 'radio',
                                disabledForModes: 'mastery',
                                fieldLabel: 'Test',
                                checked: this.course.completionMethod == Symphony.ArtisanCourseCompletionMethod.test ? true : false,
                                afterText: 'Enables post and pre tests and disables the gradable functionality on question pages.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.completionMethod = Symphony.ArtisanCourseCompletionMethod.test;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'completionMethod',
                                ref: '../../../test',
                                xtype: 'radio',
                                fieldLabel: 'Test - Force Restart',
                                disabledForModes: 'mastery',
                                checked: this.course.completionMethod == Symphony.ArtisanCourseCompletionMethod.testForceRestart ? true : false,
                                afterText: 'Enables post and pre tests and disables the gradable functionality on question pages. Post test requires full restart if the course is exited prior to completion.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.completionMethod = Symphony.ArtisanCourseCompletionMethod.testForceRestart;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'completionMethod',
                                ref: '../../../navigation',
                                xtype: 'radio',
                                fieldLabel: 'Navigation',
                                disabledForModes: 'mastery',
                                checked: this.course.completionMethod == Symphony.ArtisanCourseCompletionMethod.navigation ? true : false,
                                afterText: 'Disables post and pre tests and disables the gradable functionality on question pages.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.completionMethod = Symphony.ArtisanCourseCompletionMethod.navigation;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'completionMethod',
                                ref: '../../../inlineQuestions',
                                xtype: 'radio',
                                fieldLabel: 'Inline Questions',
                                defaultForModes: 'mastery',
                                afterText: 'Disables post and pre tests and enables the gradable functionality on question pages.',
                                checked: this.course.completionMethod == Symphony.ArtisanCourseCompletionMethod.inlineQuestions ? true : false,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.completionMethod = Symphony.ArtisanCourseCompletionMethod.inlineQuestions;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'completionMethod',
                                ref: '../../../survey',
                                xtype: 'radio',
                                fieldLabel: 'Survey',
                                disabledForModes: 'mastery',
                                afterText: 'Allows blank answers to be entered for long answer questions. Disables post and pre tests and disables the gradable functionality on question pages.',
                                checked: this.course.completionMethod == Symphony.ArtisanCourseCompletionMethod.survey ? true : false,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.completionMethod = Symphony.ArtisanCourseCompletionMethod.survey;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Navigation Method',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'navigationMethod',
                                ref: '../../../freeForm',
                                xtype: 'radio',
                                fieldLabel: 'Free-Form',
                                afterText: 'Allows students to jump from section to section within a course.',
                                disabledForModes: 'mastery',
                                checked: this.course.navigationMethod == Symphony.ArtisanCourseNavigationMethod.freeForm ? true : false,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.navigationMethod = Symphony.ArtisanCourseNavigationMethod.freeForm;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            },{
                                name: 'navigationMethod',
                                ref: '../../../freeFormWithQuiz',
                                xtype: 'radio',
                                fieldLabel: 'Free Form with Quiz',
                                afterText: 'Requires students to pass a quiz before jumping to sections that follows this quiz',
                                disabledForModes: 'mastery',
                                checked: this.course.navigationMethod == Symphony.ArtisanCourseNavigationMethod.freeFormWithQuiz ? true : false,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.navigationMethod = Symphony.ArtisanCourseNavigationMethod.freeFormWithQuiz;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'navigationMethod',
                                ref: '../../../sequential',
                                xtype: 'radio',
                                fieldLabel: 'Sequential',
                                afterText: 'Requires students to navigate course sections in order.',
                                defaultForModes: 'mastery',
                                checked: this.course.navigationMethod == Symphony.ArtisanCourseNavigationMethod.sequential ? true : false,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.navigationMethod = Symphony.ArtisanCourseNavigationMethod.sequential;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'navigationMethod',
                                ref: '../../../fullSequential',
                                xtype: 'radio',
                                fieldLabel: 'Full Sequential',
                                afterText: 'Requires students to navigate both course sections and learning objects in order.',
                                checked: this.course.navigationMethod == Symphony.ArtisanCourseNavigationMethod.fullSequential ? true : false,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.navigationMethod = Symphony.ArtisanCourseNavigationMethod.fullSequential;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Bookmarking Method',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'bookmarkingMethod',
                                ref: '../../../standard',
                                xtype: 'radio',
                                fieldLabel: 'Standard',
                                afterText: 'Bookmarks the student at the last page they were on when they left the course, and allows students the option to clear the bookmark and start at the beginning of the course.',
                                checked: this.course.bookmarkingMethod == Symphony.ArtisanCourseBookmarkingMethod.standard || this.course.id < 0 ? true : false,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.bookmarkingMethod = Symphony.ArtisanCourseBookmarkingMethod.standard;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'bookmarkingMethod',
                                ref: '../../../automatic',
                                xtype: 'radio',
                                fieldLabel: 'Automatic Resume',
                                afterText: 'The furthest location the student reached in the course will be used will always be used as the starting location. The student is not given the option to resume at the beginning.',
                                checked: this.course.bookmarkingMethod == Symphony.ArtisanCourseBookmarkingMethod.automatic ? true : false,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.bookmarkingMethod = Symphony.ArtisanCourseBookmarkingMethod.automatic;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Certificate',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'isCertificateEnabled',
                                ref: '../../../certificateEnabled',
                                xtype: 'radio',
                                fieldLabel: 'Enabled',
                                afterText: 'Issues a certificate to the student after successfully completing the course.',
                                checked: this.course.certificateEnabled || this.course.id < 0,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.certificateEnabled = true;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'isCertificateEnabled',
                                ref: '../../../certificateDisabled',
                                xtype: 'radio',
                                fieldLabel: 'Disabled',
                                afterText: 'Will not issue a certificate after completing the course.',
                                checked: !this.course.certificateEnabled && this.course.id > 0,
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.certificateEnabled = false;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Course Timers (Seconds)',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'inactivityTimeout',
                                ref: '../../../inactivityTimeout',
                                xtype: 'textfield',
                                fieldLabel: 'Inactivity Timeout',
                                allowBlank: true,
                                value: this.course.inactivityTimeout,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.inactivityTimeout = field.getValue();
                                        me.onContentChanged();
                                    }
                                },
                                help: {
                                    title: 'What is the inactivity timeout?',
                                    text: 'The timespan in seconds of inactivity before the student is logged out.'
                                },
                                regex: /^\d+$/,
                                regexText: 'Please enter a value greater than or equal to 0.'
                            }, {
                                name: 'minimumPageTime',
                                ref: '../../../pageTime',
                                xtype: 'textfield',
                                fieldLabel: 'Min. Page Time',
                                allowBlank: true,
                                value: this.course.minimumPageTime,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.minimumPageTime = field.getValue();
                                        me.onContentChanged();
                                    }
                                },
                                help: {
                                    title: 'What is minimum page time?',
                                    text: 'This is the required amount of time in seconds that each student must spend on each page. To disable this timer, set this to 0.'
                                },
                                regex: /^\d*$/,
                                regexText: 'Please enter a value greater than or equal to 0.'
                            }, {
                                name: 'maximumQuestionTime',
                                ref: '../../../questionTime',
                                xtype: 'textfield',
                                fieldLabel: 'Max. Question Time',
                                allowBlank: true,
                                value: this.course.maximumQuestionTime,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.maximumQuestionTime = field.getValue();
                                        me.onContentChanged();
                                    }
                                },
                                help: {
                                    title: 'What is maximum question time?',
                                    text: 'This is the amount of time in seconds the student is allowed per question. To disable this timer, set this to 0.'
                                },
                                regex: /^\d*$/,
                                regexText: 'Please enter a value greater than or equal to 0.'
                            }, {
                                name: 'maxTime',
                                ref: '../../../maxTime',
                                xtype: 'numberfield',
                                fieldLabel: 'Max. Test Time',
                                value: this.course.maxTime,
                                minValue: 0,
                                allowBlank: true,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.maxTime = field.getValue();
                                        me.onContentChanged();
                                    }
                                },
                                help: {
                                    title: 'What is max test time?',
                                    text: 'The maximum amount of time in seconds the student has to complete a test or quiz before being logged out. Leave blank to allow unlimited time.'
                                }
                            }, {
                                name: 'minTime',
                                ref: '../../../minTime',
                                xtype: 'numberfield',
                                fieldLabel: 'Min. Course Time',
                                value: this.course.minTime,
                                minValue: 0,
                                allowBlank: true,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.minTime = field.getValue();
                                        me.onContentChanged();
                                    }
                                },
                                help: {
                                    title: 'What is minimum course time?',
                                    text: 'The minimum amount of time in seconds the student must spend in a course in order to receive credit. Leaving this blank will not enforce any minimum time.'
                                }
                            }, {
                                name: 'minScoTime',
                                ref: '../../../minScoTime',
                                xtype: 'numberfield',
                                fieldLabel: 'Min. SCO Time',
                                value: this.course.minScoTime,
                                minValue: 0,
                                allowBlank: true,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.minScoTime = field.getValue();
                                        me.onContentChanged();
                                    }
                                },
                                help: {
                                    title: 'What is minimum SCO time?',
                                    text: 'The minimum amount of time in seconds the student must spend in a SCO in order to advance. Leaving this blank will not enforce any minimum time.'
                                }
                            }, {
                                name: 'minLoTime',
                                ref: '../../../minLoTime',
                                xtype: 'numberfield',
                                fieldLabel: 'Min. Learning Object Time',
                                value: this.course.minLoTime,
                                minValue: 0,
                                allowBlank: true,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function (field, event) {
                                        me.course.minLoTime = field.getValue();
                                        me.onContentChanged();
                                    }
                                },
                                help: {
                                    title: 'What is minimum learning object time?',
                                    text: 'The minimum amount of time in seconds the student must spend in a learning object in order to advance. Leaving this blank will not enforce any minimum time.'
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Inactivity Method',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'timeoutMethod',
                                ref: '../../../none',
                                xtype: 'radio',
                                fieldLabel: 'None',
                                checked: this.course.timeoutMethod == Symphony.ArtisanCourseTimeoutMethod.none ? true : !this.course.timeoutMethod ? true : false,
                                afterText: 'Does not exit the course after inactivity.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.timeoutMethod = Symphony.ArtisanCourseTimeoutMethod.none;
                                            me.isForceLogout.setDisabled(true);
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'timeoutMethod',
                                ref: '../../../showAlert',
                                xtype: 'radio',
                                fieldLabel: 'Show Alert',
                                checked: this.course.timeoutMethod == Symphony.ArtisanCourseTimeoutMethod.showAlert ? true : false,
                                afterText: 'Shows an alert to the student prior to exiting the course after inactivity.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.timeoutMethod = Symphony.ArtisanCourseTimeoutMethod.showAlert;
                                            me.isForceLogout.setDisabled(false);
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'timeoutMethod',
                                ref: '../../../automatic',
                                xtype: 'radio',
                                fieldLabel: 'Automatic',
                                checked: this.course.timeoutMethod == Symphony.ArtisanCourseTimeoutMethod.automatic ? true : false,
                                afterText: 'Automatically exits the course without showing an alert after inactivity.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.timeoutMethod = Symphony.ArtisanCourseTimeoutMethod.automatic;
                                            me.isForceLogout.setDisabled(false);
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'isForceLogout',
                                ref: '../../../isForceLogout',
                                disabled: this.course.timeoutMethod == Symphony.ArtisanCourseTimeoutMethod.none,
                                xtype: 'checkbox',
                                fieldLabel: 'Force Logout',
                                checked: this.course.isForceLogout,
                                afterText: 'Log the student out of Symphony after inactivity.',
                                handler: function () {
                                    me.course.isForceLogout = this.checked;
                                    me.onContentChanged();
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Marking Method',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'markingMethod',
                                ref: '../../../showAnswers',
                                xtype: 'radio',
                                fieldLabel: 'Show Answers',
                                checked: this.course.markingMethod == Symphony.ArtisanCourseMarkingMethod.showAnswers ? true : !this.course.markingMethod ? true : false,
                                afterText: 'Show correct answers after questions are marked.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.markingMethod = Symphony.ArtisanCourseMarkingMethod.showAnswers;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'markingMethod',
                                ref: '../../../hideAnswers',
                                xtype: 'radio',
                                fieldLabel: 'Hide Answers',
                                checked: this.course.markingMethod == Symphony.ArtisanCourseMarkingMethod.hideAnswers ? true : false,
                                afterText: 'Hide correct answers after questions are marked.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.markingMethod = Symphony.ArtisanCourseMarkingMethod.hideAnswers;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'markingMethod',
                                ref: '../../../hideAnswersAndFeedback',
                                xtype: 'radio',
                                fieldLabel: 'Hide Answers & Feedback',
                                checked: this.course.markingMethod == Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback ? true : false,
                                afterText: 'Hide both correct answers and question feedback after questions are marked.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.markingMethod = Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'markingMethod',
                                ref: '../../../hideAllFeedback',
                                xtype: 'radio',
                                fieldLabel: 'Hide All Feedback',
                                checked: this.course.markingMethod == Symphony.ArtisanCourseMarkingMethod.hideAllFeedback ? true : false,
                                afterText: 'Student will not be aware if their answer is correct or incorrect.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.markingMethod = Symphony.ArtisanCourseMarkingMethod.hideAllFeedback;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Review Method',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                name: 'reviewMethod',
                                ref: '../../../showReview',
                                xtype: 'radio',
                                fieldLabel: 'Show Review',
                                checked: this.course.reviewMethod == Symphony.ArtisanCourseReviewMethod.showReview ? true : !this.course.reviewMethod ? true : false,
                                afterText: 'Show review at the end of the course indicating the correct answers for each question missed.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.reviewMethod = Symphony.ArtisanCourseReviewMethod.showReview;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }, {
                                name: 'reviewMethod',
                                ref: '../../../hideReview',
                                xtype: 'radio',
                                fieldLabel: 'Hide Review',
                                checked: this.course.reviewMethod == Symphony.ArtisanCourseReviewMethod.hideReview ? true : false,
                                afterText: 'Do not show review at the end of the course containing the correct answers to quiestions missed.',
                                listeners: {
                                    check: function (field, newValue, oldValue) {
                                        if (newValue) {
                                            me.course.reviewMethod = Symphony.ArtisanCourseReviewMethod.hideReview;
                                            me.onContentChanged();
                                        }
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Document Library',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                xtype: 'checkbox',
                                fieldLabel: 'Enable Document Library',
                                checked: me.course.isMenuEnabled,
                                afterText: 'Enables a custom menu on every page.',
                                name: 'isMenuEnabled',
                                ref: '../../../isMenuEnabled',
                                handler: function () {
                                    me.setPopupMenuDisabled(!this.checked);
                                    me.course.popupMenu = me.generatePopupMenu();

                                    me.onContentChanged();
                                }
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'Title',
                                name: 'popupTitle',
                                help: {
                                    title: 'Title',
                                    text: 'Text to use for the button that will toggle the pop up display.'
                                },
                                value: me.course.popupTitle ? me.course.popupTitle : '',
                                ref: '../../../popupTitle',
                                disabled: !me.course.isMenuEnabled,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: function () {
                                        me.course.popupTitle = this.getValue();
                                        me.course.popupMenu = me.generatePopupMenu();

                                        me.onContentChanged();
                                    }
                                }
                            }, {
                                xtype: 'artisan.tinymce',
                                border: false,
                                name: 'popupMenu',
                                content: function() {
                                    me.formatPopupMenu();
                                    return me.course.popupMenu;
                                }(),
                                themeCssPath: me.course.theme ? me.getThemeCssUrl(me.course.theme) : '',
                                skinCssPath: me.course.theme ? me.course.theme.skinCssPath : '',
                                cssText: me.course.cssText,
                                fieldLabel: 'Content',
                                cls: 'popup-menu-editor',
                                height: 245,
                                enableKeyEvents: true,
                                listeners: {
                                    contentchange: function (content) {
                                        me.course.popupMenu = content;
                                        me.course.popupMenu = me.generatePopupMenu();
                                        me.onContentChanged();
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Final Content Page',
                            border: true,
                            defaults: {
                                border: false,
                                anchor: '100%',
                                labelWidth: me.labelWidth
                            },
                            items: [{
                                xtype: 'symphony.spellcheckarea',
                                border: false,
                                name: 'finalContentPage',
                                fieldLabel: 'Content',
                                value: me.course.finalContentPage,
                                cls: 'popup-menu-editor',
                                height: 245,
                                enableKeyEvents: true,
                                listeners: {
                                    change: function (editor, content) {
                                        me.course.finalContentPage = content;
                                        me.onContentChanged();
                                    }
                                }
                            }]
                        }]
                    }]
                }]
            });

            this.callParent(arguments);

            this.formatPopupMenu();

            // passing score needs a good default
            this.course.passingScore = this.course.passingScore || 80;

            // Enable certs by default
            if (this.course.id <= 0) {
                this.course.certificateEnabled = true;
            }

            // create a copy of the course for reference for reverting
            this.lastCourse = Ext.apply({}, this.course);

            this.bindCourse();

            if (Symphony.Artisan.autoSaveInterval != null) {
                me.timer = window.setInterval(function () {
                    var newName = me.findField('name').getValue();
                    if (newName != me.course.name) {
                        me.lastName = me.course.name;
                        me.course.name = newName;
                        me.onContentChanged();
                    }

                    var newDescription = me.findField('description').getValue();
                    if (newDescription != me.course.description) {
                        me.course.description = newDescription;
                        me.onContentChanged();
                    }

                    /*
                    var newDuration = this.duration.getValue();
                    if (newDuration != me.course.duration)
                    {
                        me.course.duration = newDuration;
                        me.onContentChanged();
                    }
                    */

                    var newKeywords = me.findField('keywords').getValue();
                    if (newKeywords != me.course.keywords) {
                        me.course.keywords = newKeywords;
                        me.onContentChanged();
                    }

                    var newAccreditation = me.findField('accreditation').getValue();
                    if (newAccreditation != me.course.accreditation) {
                        me.course.accreditation = newAccreditation;
                        me.onContentChanged();
                    }

                    var newObjectives = me.findField('objectives').getValue();
                    if (newObjectives != me.course.objectives) {
                        me.course.objectives = newObjectives;
                        me.onContentChanged();
                    }

                    //TODO: add check for themeId
                    var newHeaderImageUrl = me.findField('headerImageUrl').getValue();
                    if (newHeaderImageUrl != me.course.headerImageUrl) {
                        me.course.headerImageUrl = newHeaderImageUrl;
                        me.onContentChanged();
                    }

                    var newContactEmail = me.findField('contactEmail').getValue();
                    if (newContactEmail != me.course.contactEmail) {
                        me.course.contactEmail = newContactEmail;
                        me.onContentChanged();
                    }

                    var newDisclaimer = me.findField('disclaimer').getValue();
                    if (newDisclaimer != me.course.disclaimer) {
                        me.course.disclaimer = newDisclaimer;
                        me.onContentChanged();
                    }

                    var newPassingScore = me.findField('passingScore').getValue();
                    if (newPassingScore != me.course.passingScore) {
                        me.course.passingScore = newPassingScore;
                        me.onContentChanged();
                    }

                    var newDuration = me.duration.getValue();
                    if (newDuration != me.course.duration) {
                        if (!me.course.isUseAutomaticDuration) {
                            me.course.duration = newDuration;
                            me.onContentChanged();
                        }
                    }

                    //TODO: add check for completionMethod
                }, Symphony.Artisan.autoSaveInterval);
            }

            me.setPopupMenuDisabled(!me.course.isMenuEnabled);

        },
        getThemeCssUrl: function (theme) {
            if (theme.isDynamic) {
                return '/skins/artisan_dynamic/' + theme.id + '/' + this.course.themeFlavorId + '/css/theme.css';
            }
            return theme.cssPath;
        },
        setPopupMenuDisabled: function (isDisabled) {
            var me = this;

            me.popupTitle.setDisabled(isDisabled);
            me.find('name', 'popupMenu')[0].setDisabled(isDisabled);

            me.course.isMenuEnabled = !isDisabled;
        },
        getPopupMenuContent: function(popupMenu) {
            var menuHtml = document.createElement('div');
            menuHtml.innerHTML = popupMenu;

            var menuContainer = menuHtml.children;

            if (menuContainer.length) {
                if (menuContainer[0].className.indexOf('pdf-menu') < 0) {
                    return popupMenu;
                } else {
                    return menuContainer[0].innerHTML;
                }
            }

            return popupMenu;

        },
        generatePopupMenu: function() {
            var title = this.course.popupTitle,
                isEnabled = this.course.isMenuEnabled,
                content = this.course.popupMenu;
            
            return String.format(this.popupMenuTpl,
                    title ? title : 'Document Library',
                    isEnabled ? '' : 'data-disabled="true"',
                    this.getPopupMenuContent(content ? content : ''));
        },
        formatPopupMenu: function (isEnabled) {
            if (typeof (isEnabled) == 'undefined') {
                isEnabled = true;
            }

            this.course.isMenuEnabled = isEnabled;
            // Deal with popup menu html
            if (this.course.popupMenu) {

                var menuHtml = document.createElement('div');
                menuHtml.innerHTML = this.course.popupMenu;

                var menuContainer = menuHtml.children;

                if (menuContainer.length) {
                    var titleAttribute = menuContainer[0].attributes['data-title'];
                    if (titleAttribute && titleAttribute.value) {
                        this.course.popupTitle = titleAttribute.value;
                    } else {
                        this.course.popupTitle = 'Document Library';
                    }

                    var menu = menuContainer[0].children;
                    /*
                        Display this.course.popupMenu as is if it contains div class="pdf-menu" as a begining of the record.
                    */
                    if (this.course.popupMenu.indexOf('<div class="pdf-menu"') != 0) {
                        if (menu.length) {
                            this.course.popupMenu = menu[0].innerHTML;
                        } else if (menuContainer[0].innerHTML) {
                            this.course.popupMenu = menuContainer[0].innerHTML;
                        } else {
                            this.course.isMenuEnabled = false;
                        }
                    }

                    var disabledAttribute = menuContainer[0].attributes['data-disabled'];
                    if (disabledAttribute && disabledAttribute.value) {
                        this.course.isMenuEnabled = false;
                    }
                }
            } else {
                this.course.isMenuEnabled = false;
            }

            this.course.popupMenu = this.generatePopupMenu();
        },
        getCourse: function () {
            return this.course;
        },
        setCourse: function (course) {
            this.course = course;
        },
        onContentChanged: function () {
            this.setRevertDisabled(false);
            this.fireEvent('contentchanged');
        },
        onRevertClick: function () {
            // set all the values back on the original object and re-bind
            Ext.apply(this.course, this.lastCourse);
            this.bindCourse();

            this.setRevertDisabled(true);

            // if we revert, the theme might have changed
            this.fireEvent('themechanged', this.course.theme);
        },
        setRevertDisabled: function (disable) {
            this.revertButton.setDisabled(disable);
            if (this.node && disable) {
                this.node.set('cls', this.node.get('cls').replace(' node-italic', ''));
            } else {
                if (this.node.get('cls').indexOf('node-italic') == -1) {
                    this.node.set('cls', this.node.get('cls') + ' node-italic');
                }
            }
        },
        setSaved: function () {
            this.lastCourse = this.course;
            this.setRevertDisabled(true);
            this.bindCourse();
        },
        bindCourse: function () {
            this.bindValues(this.course);
            if (this.course.isUseAutomaticDuration) {
                this.duration.setValue(this.course.automaticDuration);
            }
            
            var categoryPicker = this.query('[name=levelIndentText]')[0];
            if (!this.course.categoryId && categoryPicker) {
                categoryPicker.setRawValue('');
            }

            var authorsList = this.query('[xtype="artisan.authorslist"]')[0];
            if (authorsList && this.course.authors) {
                authorsList.getStore().loadData(this.course.authors);
            }

            var categoryList = this.query('[xtype="artisan.secondarycategorieslist"]')[0];
            if (categoryList && this.course.secondaryCategories) {
                categoryList.getStore().loadData(this.course.secondaryCategories);
            }

            var accreditationList = this.queryById('accreditation-grid');
            if (accreditationList && this.course.accreditations) {
                accreditationList.getStore().loadRawData(this.course.accreditations);
            }
        }
    });


    Symphony.Artisan.AuthorsList = Ext.define('artisan.authorslist', {
        alias: 'widget.artisan.authorslist',
        extend: 'symphony.localgrid',
        data: [],
        initComponent: function () {

            var me = this;

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left'
                },
                columns: [
                {
                    /*id: 'email',*/
                    header: 'Email',
                    width: 180,
                    dataIndex: 'email',
                    align: 'left'
                }, {
                    /*id: 'firstName',*/
                    dataIndex: 'firstName',
                    header: 'First Name',
                    align: 'left'
                }, {
                    /*id: 'lastName',*/
                    dataIndex: 'lastName',
                    header: 'Last Name',
                    align: 'left'
                }, {
                    /*id: 'deleteAuthor',*/
                    dataIndex: 'deleteAuthor',
                    renderer: Symphony.deleteRenderer,
                    header: '',
                    width: 30
                }]
            });
            Ext.apply(this, {
                colModel: colModel,
                model: 'author',
                data: this.data,
                border: false,
                bbar: {
                    xtype: 'toolbar',
                    items: [{
                        text: 'Add Author',
                        iconCls: 'x-button-add',
                        xtype: 'button',
                        handler: function (btn) {
                            me.fireEvent('addNewAuthor');
                        }
                    }]
                },
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.getStore().getAt(rowIndex);
                        if (columnIndex == 3) {
                            grid.getStore().remove(record);
                        }
                    },
                    addNewAuthor: function () {
                        var win = new Ext.Window({
                            layout: 'fit',
                            resizable: false,
                            title: 'Add new author',
                            height: 400,
                            width: 500,
                            items: [{
                                xtype: 'panel',
                                frame: false,
                                border: false,
                                layout: 'fit',
                                items: [{
                                    xtype: 'artisan.authorsgridselector',
                                    name: 'artisanAuthorGrid',
                                    frame: false,
                                    border: false,
                                    pageSize: -1,
                                    displayInfo: false,
                                    listeners: {
                                        cancel: function () {
                                            win.destroy();
                                        },
                                        addSelectedAuthors: function (selectedAuthors) {
                                            for (var i = 0; i < selectedAuthors.length; i++) {
                                                var newAuthorRecord = Ext.create('author', selectedAuthors[i].data);
                                                me.store.add(newAuthorRecord);
                                            }
                                            win.destroy();
                                        }
                                    }
                                }]
                            }]
                        }).show();
                    }
                }
            });
            this.callParent(arguments);
        }
    });


    Symphony.Artisan.SecondaryCategoriesList = Ext.define('artisan.secondarycategorieslist', {
        alias: 'widget.artisan.secondarycategorieslist',
        extend: 'symphony.localgrid',
        data: null,
        border: false,
        initComponent: function () {
            var me = this;

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left'
                },
                columns: [{
                    /*id: 'id',*/
                    header: 'Id',
                    width: 100,
                    dataIndex: 'id',
                    align: 'left'
                }, {
                    /*id: 'name',*/
                    dataIndex: 'name',
                    width: 300,
                    header: 'Name',
                    align: 'left',
                    editor: new Ext.form.TextField({
                        allowBlank: false
                    })
                }, {
                    /*id: 'deleteSecondaryCategory',*/
                    dataIndex: 'deleteSecondaryCategory',
                    renderer: Symphony.deleteRenderer,
                    header: '',
                    width: 30
                }]
            });

            Ext.apply(this, {
                colModel: colModel,
                model: 'secondaryCategory',
                data: this.data,
                plugins: [{
                    ptype: 'cellediting',
                    clicksToEdit: 1
                }],
                bbar: {
                    xtype: 'toolbar',
                    items: [{
                        text: 'Add Category',
                        iconCls: 'x-button-add',
                        xtype: 'button',
                        handler: function (btn) {
                            me.fireEvent('addNewRecord');
                        }
                    }]
                },
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.getStore().getAt(rowIndex);
                        if (columnIndex == 2) {
                            grid.getStore().remove(record);
                        }
                    },
                    addNewRecord: function () {
                        var emptyRecord = Ext.create('secondaryCategory', {
                            name: '- new category -',
                            id: 0
                        });
                        me.store.add(emptyRecord);
                    }
                }
            });

            this.callParent(arguments);
        }
    });

    Symphony.Artisan.SourceImportInfoGrid = Ext.define('artisan.sourceimportinfogrid', {
        alias: 'widget.artisan.sourceimportinfogrid',
        extend: 'symphony.searchablegrid',
        courseId: 0,
        initComponent: function () {
            var me = this;
            var url = '/services/artisan.svc/sourceimportinfo/?id=' + me.courseId;

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                        //id: 'id',
                        header: 'Id',
                        dataIndex: 'id'
                    }, {
                        //id: 'name',
                        dataIndex: 'name',
                        header: 'Name'
                    }, {
                        //id: 'value',
                        dataIndex: 'value',
                        header: 'Value'
                    }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                url: url,
                model: 'artisanSourceImportInfo',
                pager: {},
                pageSize: -1,
                colModel: colModel,
                viewConfig: {
                    forceFit: true
                }
            });
            this.callParent(arguments);
        }
    });

    Symphony.Artisan.AuthorsGridSelector = Ext.define('artisan.authorsgridselector', {
        alias: 'widget.artisan.authorsgridselector',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/customer.svc/authors/';

            var sm = new Ext.selection.CheckboxModel({
                checkOnly: false,
                sortable: true,
                id: 'checker',
                mode: 'MULTI',
                header: ' '
            });

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'firstName',*/
                    header: 'First Name',
                    dataIndex: 'firstName',
                    align: 'left'
                }, {
                    /*id: 'lastName',*/
                    dataIndex: 'lastName',
                    header: 'Last Name',
                    align: 'left'
                }, {
                    /*id: 'email',*/
                    dataIndex: 'email',
                    header: 'Email',
                    align: 'left'
                }]
            });

            Ext.apply(this, {
                selModel: sm,
                border: false,
                idProperty: 'id',
                url: url,
                model: 'author',
                colModel: colModel,
                viewConfig: {
                    forceFit: true
                },
                tbar: {
                    items: [{
                        xtype: 'button',
                        iconCls: 'x-button-add',
                        text: 'Add selected authors',
                        handler: function () {
                            var locSelection = me.getSelectionModel().getSelections();
                            me.fireEvent('addSelectedAuthors', locSelection);
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        iconCls: 'x-button-cancel',
                        handler: function () {
                            me.fireEvent('cancel');
                            //win.destroy();
                        }
                    }]
                }
            });
            this.callParent(arguments);
        }
    });


})();



