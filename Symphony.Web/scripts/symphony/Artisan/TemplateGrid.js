﻿(function () {
	Symphony.Artisan.TemplateGrid = Ext.define('artisan.templategrid', {
	    alias: 'widget.artisan.templategrid',
	    extend: 'symphony.searchablegrid',
		type: 0,
		initComponent: function () {
			var me = this;
			var url = '/services/artisan.svc/templates/?pageType=' + this.type;

			var columns = [{}, {}, {}, {}, {}, {}];

			var colModel = new Ext.grid.ColumnModel({
				hideHeaders: true,
				defaults: {
					align: 'left',
					hideable: false,
					menuDisabled: true,
					renderer: Ext.bind(Symphony.Artisan.templateRenderer, this, [columns.length], true),
					width: 90
				},
				columns: columns
			});

			Ext.apply(this, {
				idProperty: 'id',
				url: url,
				colModel: colModel,
				model: 'artisanTemplate',
				hideHeaders: true,
				selModel: new Ext.selection.CellModel({
					singleSelect: true,
					listeners: {
					    select: function (sm, record, row, column, eOpts) {
					        if (record) {
					            var index = column + (row * columns.length);
					            var actualRecord = me.store.getAt(index);
					            me.fireEvent('templateSelected', actualRecord ? actualRecord.data : null, false);
					        }
					    }
					}
				}),
				listeners: {
					celldblclick: Ext.bind(this.onCellDblClick, this)
				}
			});
			this.callParent(arguments);

			console.warn("For some reason adding getRowClass to the view config returns blank rows. However, adding the function afterwards appears to work. Look into this.");
			this.getView().getRowClass = function (record, rowIndex, rowParams, store) {
			    if ((rowIndex) > (store.getCount() * 1.0 / (colModel.length * 1.0))) {
			        return 'x-hidden';
			    }
			}
		},
		onCellDblClick: function (grid, td, column, record, tr, row, e) {
		    var count = this.columns.length;
			var index = column + (row * count);
			var record = this.store.getAt(index);
			this.fireEvent('templateSelected', record.data, true);
		}
	});

})();