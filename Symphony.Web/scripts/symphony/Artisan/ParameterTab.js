﻿(function () {
    Symphony.Artisan.ParameterTab = Ext.define('artisan.parametertab', {
        alias: 'widget.artisan.parametertab',
        extend: 'Ext.Panel',
        count: 0,
        initComponent: function () {
            var me = this;

            var tbar = {
                items: [{
                    xtype: 'button',
                    iconCls: 'x-button-add',
                    text: 'Add',
                    handler: function () {
                        me.addPanel(0, "New Parameter");
                    }
                }]
            };

            Ext.apply(this, {
                layout: 'border',
                border: false,
                items: [{
                    split: true,
                    collapsible: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    title: 'Parameters',
                    itemId: 'artisan.parametergrid',
                    xtype: 'artisan.parametergrid',
                    ref: 'parameterGrid',
                    tbar: !me.readOnly ? tbar : {},
                    listeners: {
                        cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            var record = grid.getStore().getAt(rowIndex);
                            me.addPanel(record.data.id, record.data.name, record);
                        },
                        celldblclick: function (grid, rowIndex, columnIndex, e) {
                            me.fireEvent('parameterdoubleclicked');
                        }
                    }
                }, {
                    region: 'center',
                    border: false,
                    ref: 'displayPane',
                    xtype: 'tabpanel',
                    listeners: {
                        add: function () {
                            if (me.count == 0) {
                                me.fireEvent('parameteravailable');
                            }
                            me.count++;
                        },
                        remove: function () {
                            me.count--;
                            if (me.count == 0) {
                                me.fireEvent('parameterunavailable');
                            }
                        }
                    }
                }]
            });
            this.callParent(arguments);
        },
        getActiveParameter: function () {
            var tab = this.displayPane.getActiveTab();
            return tab.getParameter();
        },
        addPanel: function (id, name, record) {
            var me = this;
            var found = id && this.displayPane.findBy(function (element) {
                if (element.parameter) {
                    return element.parameter.id == id;
                }
                return false;
            });

            if (found.length > 0 && this.displayPane) {
                this.displayPane.activate(found[0]);
            } else {
                var p = new Symphony.Artisan.ParameterPanel({
                    title: name,
                    parameter: record ? record.data : null,
                    border: false,
                    readOnly: me.readOnly,
                    listeners: {
                        save: function () {
                            me.parameterGrid.refresh();
                        },
                        render: function (panel) {
                            if (record) {
                                panel.load(record.data);
                            }
                        }
                    }
                });
                this.displayPane.add(p);
                this.displayPane.doLayout();
                p.show();
            }
        }
    });

})();