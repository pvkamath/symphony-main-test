﻿(function () {
	Symphony.Artisan.LearningObjectEditor = Ext.define('artisan.learningobjecteditor', {
	    alias: 'widget.artisan.learningobjecteditor',
	    extend: 'artisan.optionseditor',
		section: null,
		lastObjectives: '',
		lastIsQuiz: false,
		lastDescription: '',
		lastMaxQuestions: 20,
		lastMarkingMethod: 0,
		lastPassingScore: '',
		lastMaxTime: null,
		lastMinimumPageTime: null,
		lastMaximumQuestionTime: null,
		lastIsSinglePage: null,
		lastFinalContentPage: '',
        labelWidth: 150,
		initComponent: function () {
			var me = this;
			
			this.lastObjectives = this.section.objectives;
			this.lastIsQuiz = this.section.isQuiz;
			this.lastDescription = this.section.description;
			this.lastMaxQuestions = this.section.maxQuestions;
			this.lastMarkingMethod = this.section.markingMethod;
			this.lastPassingScore = this.section.passingScore;
			this.lastMaxTime = this.section.maxTime;
			this.lastMinimumPageTime = this.section.minimumPageTime;
			this.lastMaximumQuestionTime = this.section.maximumQuestionTime;
			this.lastMinLoTime = this.section.minLoTime;
			this.lastIsSinglePage = this.section.isSinglePage;
			//this.lastFinalLearingObjectContentPage = this.section.finalLearingObjectContentPage;
			
			Ext.apply(this, {
			    layout: 'border',
                listeners: {
					beforedestroy: function (comp) {
						if (me.timer) {
							window.clearInterval(me.timer);
						}
					},
					render: function () {
					    me.setSettingMode();

					}
                },
                defaults: {
                    border: false
                },
				items: [{
					region: 'north',
					height: 27,
					xtype: 'toolbar',
					items: [{
						text: 'Revert to Last Save',
						iconCls: 'x-button-revert',
						ref: '../revertButton',
						disabled: true,
						handler: Ext.bind(me.onRevertClick, this)
					}]
				}, {
					region: 'center',
					xtype: 'panel',
					layout: 'fit',
					bodyStyle: 'padding: 15px;',
					items: [{
					    layout: 'form',
					    autoScroll: true,
					    frame: true,
					    bodyStyle: 'padding-right:20px',
						items: [{
							xtype: 'fieldset',
							title: 'General',
							border: true,
							defaults: {
								border: false,
								anchor: '100%',
								labelWidth: me.labelWidth
							},
							items: [{
								name: 'objectives',
								ref: '../../../objectives',
								xtype: 'symphony.spellcheckarea',
								fieldLabel: 'Objectives',
								value: this.section.objectives,
								enableKeyEvents: true,
								listeners: {
									keyup: function (field, event) {
										me.section.objectives = field.getValue();
										me.onContentChanged();
									}
								}
							}]
						}, {
						    xtype: 'fieldset',
						    title: 'Learning Object Mode',
						    ref: '../../learningObjectModeFrame',
						    border: true,
						    defaults: {
						        border: false,
						        anchor: '100%',
						        labelWidth: me.labelWidth
						    },
						    items: [{
						        name: 'learningObjectMode',
						        ref: '../../../normal',
						        xtype: 'radio',
						        fieldLabel: 'Normal',
						        checked: !me.section.isQuiz && !me.section.isSinglePage,
						        afterText: 'This learning object behaves as a regular learning object.',
						        defaultForModes: 'mastery',
						        listeners: {
						            change: function (field, newValue, oldValue) {
						                if (newValue) {
						                    me.section.isQuiz = false;
						                    
						                    me.onContentChanged();
						                }
						                me.setSettingMode();
						            }
						        }
						    }, {
						        name: 'learningObjectMode',
						        ref: '../../../quiz',
						        xtype: 'radio',
						        fieldLabel: 'Quiz',
						        checked: this.section.isQuiz,
						        afterText: 'This learning object will behave as a Quiz. Content pages will be ignored. Students must pass the quiz in order to move on.',
						        disabledForModes: 'mastery',
						        listeners: {
						            change: function (field, newValue, oldValue) {
						                if (newValue) {
						                    
						                    me.section.isQuiz = true;

						                    me.onContentChanged();
						                }
						                me.setSettingMode();
						            }
						        }
						    }, {
						        name: 'isSinglePage',
						        ref: '../../../singlePage',
						        xtype: 'checkbox',
						        fieldLabel: 'Single Page',
						        checked: this.section.isSinglePage,
						        afterText: 'This learning object will will render all pages as a single page.',
						        disabledForModes: 'mastery',
						        listeners: {
						            change: function (field, newValue, oldValue) {
						                me.section.isSinglePage = newValue;
					                    me.onContentChanged();
						                me.setSettingMode();
						            }
						        }
						    }, {
						        xtype: 'checkbox',
		                        fieldLabel: 'Randomize Answers',
		                        boxLabel: 'Will randomize position of answers in each question the first time a question is accessed.',
		                        name: 'isRandomizeAnswers',
		                        ref: '../../../isRandomizeAnswers',
		                        checked: me.section.isRandomizeAnswers,
		                        listeners: {
			                        check: function (field, newValue, oldValue) {
			                            me.section.isRandomizeAnswers = newValue;
			                            me.onContentChanged();
			                        }
			                    }
		                    }]
						}, {
						    xtype: 'fieldset',
						    title: 'Quiz Settings',
						    ref: '../../quizSettings',
						    disabled: !me.section.isQuiz,
                            border: true,
						    defaults: {
						        border: false,
						        anchor: '100%',
						        labelWidth: me.labelWidth
						    },
						    items: [{
						        name: 'passingScore',
						        ref: '../../../passingScore',
						        xtype: 'numberfield',
						        fieldLabel: 'Passing Score',
						        value: me.section.passingScore ? me.section.passingScore : '',
						        minVlue: 1,
						        allowBlank: true,
						        enableKeyEvents: true,
						        help: {
						            title: 'Passing Score',
						            text: 'If set, the value here will override the passing score set at the course root.'
						        },
						        listeners: {
						            keyup: function (field, event) {
						                me.section.passingScore = field.getValue();
						                if (!me.section.passingScore) {
						                    delete me.section.passingScore;
						                }
						                me.onContentChanged();
						            }
						        }
						    }, {
						        name: 'maxQuestions',
						        ref: '../../../maxQuestions',
						        xtype: 'numberfield',
						        fieldLabel: 'Max Questions',
						        value: me.section.maxQuestions ? me.section.maxQuestions : '',
						        minValue: 1,
						        allowBlank: true,
						        enableKeyEvents: true,
						        listeners: {
						            keyup: function (field, event) {
						                me.section.maxQuestions = field.getValue();
						                if (!me.section.maxQuestions) {
						                    delete me.section.maxQuestions;
						                }
						                me.onContentChanged();
						            }
						        }
						    }, {
						        name: 'maxTime',
						        ref: '../../../maxTime',
						        xtype: 'numberfield',
						        fieldLabel: 'Max Time (s)',
						        value: me.section.maxTime ? me.section.maxTime : '',
						        minValue: 0,
						        allowBlank: true,
						        enableKeyEvents: true,
						        help: {
						            title: 'Max Time (Seconds)',
						            text: 'The maximum amount of time in seconds the user has to complete this test before being logged out. Leave blank to allow unlimited time.'
						        },
						        listeners: {
						            keyup: function (field, event) {
						                me.section.maxTime = field.getValue();
						                if (!me.section.maxTime) {
						                    delete me.section.maxTime;
						                }
						                me.onContentChanged();
						            }
						        }
						    }, {
						        name: 'description',
						        ref: '../../../description',
						        xtype: 'symphony.spellcheckarea',
						        fieldLabel: 'Description',
						        value: me.section.description,
						        allowBlank: true,
						        enableKeyEvents: true,
						        listeners: {
						            keyup: function (field, event) {
						                me.section.description = field.getValue();
						                me.onContentChanged();
						            }
						        }
						    }, {
						        xtype: 'checkbox',
						        fieldLabel: 'Randomize Questions',
						        boxLabel: 'Will randomize the order of quiz questions each time the quiz is started.',
						        name: 'isRandomizeQuizQuestions',
						        ref: '../../../isRandomizeQuizQuestions',
						        checked: me.section.isRandomizeQuizQuestions,
						        listeners: {
						            check: function (field, newValue, oldValue) {
						                me.section.isRandomizeQuizQuestions = newValue;
						                me.onContentChanged();
						            }
						        }
						    }, {
						        xtype: 'checkbox',
						        fieldLabel: 'Re-Randomize Order',
						        boxLabel: 'If checked, then each attempt will see a new random set of questions.',
						        name: 'isReRandomizeOrder',
						        ref: '../../../isRandomizeQuestions',
						        checked: me.section.isReRandomizeOrder,
						        listeners: {
						            check: function (field, newValue, oldValue) {
						                me.section.isReRandomizeOrder = newValue;
						                me.onContentChanged();
						            }
						        }
						    }]
						}, {
						    xtype: 'fieldset',
						    title: 'Marking Method',
						    ref: '../../markingMethodFrame',
						    border: true,
						    defaults: {
						        border: false,
						        anchor: '100%',
						        labelWidth: me.labelWidth
						    },
						    items: [{
						        name: 'markingMethod',
						        ref: '../../../defaultMarking',
						        xtype: 'radio',
						        fieldLabel: 'Default',
						        checked: me.section.markingMethod != Symphony.ArtisanCourseMarkingMethod.showAnswers &&
                                         me.section.markingMethod != Symphony.ArtisanCourseMarkingMethod.hideAnswers &&
                                         me.section.markingMethod != Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback,
						        afterText: 'Use global course setting.',
						        listeners: {
						            check: function (field, newValue, oldValue) {
						                if (newValue) {
						                    me.section.markingMethod = 0;
						                    me.onContentChanged();
						                }
						            }
						        }
						    }, {
						        name: 'markingMethod',
						        ref: '../../../showAnswers',
						        xtype: 'radio',
						        fieldLabel: 'Show Answers',
						        checked: this.section.markingMethod == Symphony.ArtisanCourseMarkingMethod.showAnswers ? true : false,
						        afterText: 'Show correct answers after questions are marked.',
						        listeners: {
						            check: function (field, newValue, oldValue) {
						                if (newValue) {
						                    me.section.markingMethod = Symphony.ArtisanCourseMarkingMethod.showAnswers;
						                    me.onContentChanged();
						                }
						            }
						        }
						    }, {
						        name: 'markingMethod',
						        ref: '../../../hideAnswers',
						        xtype: 'radio',
						        fieldLabel: 'Hide Answers',
						        checked: this.section.markingMethod == Symphony.ArtisanCourseMarkingMethod.hideAnswers ? true : false,
						        afterText: 'Hide correct answers after questions are marked.',
						        listeners: {
						            check: function (field, newValue, oldValue) {
						                if (newValue) {
						                    me.section.markingMethod = Symphony.ArtisanCourseMarkingMethod.hideAnswers;
						                    me.onContentChanged();
						                }
						            }
						        }
						    }, {
						        name: 'markingMethod',
						        ref: '../../../hideAnswersAndFeedback',
						        xtype: 'radio',
						        fieldLabel: 'Hide Answers and Feedback',
						        checked: this.section.markingMethod == Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback ? true : false,
						        afterText: 'Hide both correct answers and question feedback after questions are marked.',
						        listeners: {
						            check: function (field, newValue, oldValue) {
						                if (newValue) {
						                    me.section.markingMethod = Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback;
						                    me.onContentChanged();
						                }
						            }
						        }
						    }, {
						        name: 'markingMethod',
						        ref: '../../../hideAllFeedback',
						        xtype: 'radio',
						        fieldLabel: 'Hide All Feedback',
						        checked: this.section.markingMethod == Symphony.ArtisanCourseMarkingMethod.hideAllFeedback ? true : false,
						        afterText: 'Student will not be aware if their answer is correct or incorrect.',
						        listeners: {
						            check: function (field, newValue, oldValue) {
						                if (newValue) {
						                    me.section.markingMethod = Symphony.ArtisanCourseMarkingMethod.hideAllFeedback;
						                    me.onContentChanged();
						                }
						            }
						        }
						    }]
						}, {
						    xtype: 'fieldset',
						    title: 'Course Timers (Seconds)',
						    border: true,
						    defaults: {
						        border: false,
						        anchor: '100%',
						        labelWidth: me.labelWidth
						    },
						    items: [{
						        name: 'minimumPageTime',
						        ref: '../../../minimumPageTime',
						        xtype: 'textfield',
						        fieldLabel: 'Min. Page Time',
						        allowBlank: true,
						        value: me.section.minimumPageTime,
						        enableKeyEvents: true,
						        listeners: {
						            keyup: function (field, event) {
						                me.section.minimumPageTime = field.getValue();
						                if (!me.section.minimumPageTime) {
						                    delete me.section.minimumPageTime;
						                }
						                me.onContentChanged();
						            }
						        },
						        help: {
						            title: 'What is minimum page time?',
						            text: 'This is the required amount of time in seconds that each student must spend on each page. This will override the setting at the course level. Leave this blank to use the course level setting.'
						        },
						        regex: /^\d*$/,
						        regexText: 'Please enter a value greater than or equal to 0.'
						    }, {
						        name: 'maximumQuestionTime',
						        ref: '../../../maximumQuestionTime',
						        xtype: 'textfield',
						        fieldLabel: 'Max. Question Time',
						        allowBlank: true,
						        value: me.section.maximumQuestionTime,
						        enableKeyEvents: true,
						        listeners: {
						            keyup: function (field, event) {
						                me.section.maximumQuestionTime = field.getValue();
						                if (!me.section.maximumQuestionTime) {
						                    delete me.section.maximumQuestionTime;
						                }
						                me.onContentChanged();
						            }
						        },
						        help: {
						            title: 'What is maximum question time?',
						            text: 'This is the amount of time in seconds the student is allowed per question. This will override the setting at the course level. Leave this blank to use the course level setting.'
						        },
						        regex: /^\d*$/,
						        regexText: 'Please enter a value greater than or equal to 0.'
						    }, {
						        name: 'minLoTime',
						        ref: '../../../minLoTime',
						        xtype: 'numberfield',
						        fieldLabel: 'Min. Learning Object Time',
						        value: this.section.minLoTime,
						        minValue: 0,
						        allowBlank: true,
						        enableKeyEvents: true,
						        listeners: {
						            keyup: function (field, event) {
						                me.section.minLoTime = field.getValue();
						                if (!me.section.minLoTime) {
						                    delete me.section.minLoTime;
						                }
						                me.onContentChanged();
						            }
						        },
						        help: {
						            title: 'What is minimum learning object time?',
						            text: 'The minimum amount of time in seconds the user must spend in a learning object in order to advance. Leaving this blank will not enforce any minimum time.'
						        }
						    }]
						}, {
						    xtype: 'fieldset',
						    title: 'Final Content Page',
						    border: true,
						    defaults: {
						        border: false,
						        anchor: '100%'
						    },
						    items: [{
						        xtype: 'symphony.spellcheckarea',
						        border: false,
						        name: 'finalContentPage',
						        value: me.section.finalContentPage,
						        //fieldLabel: 'Content',
						        cls: 'popup-menu-editor',
						        height: 245,
						        enableKeyEvents: true,
						        listeners: {
						            change: function (editor, content) {
						                me.section.finalContentPage = content;
						                me.onContentChanged();
						            }
						        }
						    }]
						}]
					}]
				}]
			});

			this.callParent(arguments);

			this.lastObjectives = this.section.objectives;

			if (Symphony.Artisan.autoSaveInterval != null) {
				me.timer = window.setInterval(function () {
					var newObjectives = me.findField('objectives').getValue();
					if (newObjectives != me.section.objectives) {
						me.section.objectives = newObjectives;
						me.onContentChanged();
					}
					var newDescription = me.findField('description').getValue();
					if (newDescription != me.section.description) {
					    me.section.description = newDescription;
					    me.onContentChanged();
					}
					var newMaxQuestions = me.findField('maxQuestions').getValue();
					if (newMaxQuestions != me.section.maxQuestions) {
					    me.section.maxQuestions = newMaxQuestions;
					    if (!me.section.maxQuestions) {
					        delete me.section.maxQuestions;
					    }
					    me.onContentChanged();
					}

					var newMaxTime = me.findField('maxTime').getValue();
                    if (newMaxTime != me.section.maxTime && (newMaxTime || me.section.maxTime)) {
					    me.section.maxTime = newMaxTime;
					    if (!me.section.maxTime) {
					        delete me.section.maxTime;
					    }
					    me.onContentChanged();
                    }

                    var newMinTime = me.findField('minimumPageTime').getValue();
                    if (newMinTime != me.section.minimumPageTime && (newMinTime || me.section.minimumPageTime)) {
                        me.section.minimumPageTime = newMinTime;
                        if (!me.section.minimumPageTime) {
                            delete me.section.minimumPageTime;
                        }
                        me.onContentChanged();
                    }

                    var newMaxQTime = me.findField('maximumQuestionTime').getValue();
                    if (newMaxQTime != me.section.maximumQuestionTime && (newMaxQTime || me.section.maximumQuestionTime)) {
                        me.section.maximumQuestionTime = newMaxQTime;
                        if (!me.section.maximumQuestionTime) {
                            delete me.section.maximumQuestionTime;
                        }
                        me.onContentChanged();
                    }

					var newPassingScore = me.findField('passingScore').getValue();
					if (newPassingScore != me.section.passingScore) {
					    me.section.passingScore = newPassingScore;
					    if (!me.section.passingScore) {
					        delete me.section.passingScore;
					    }
					    me.onContentChanged();
					}
					
				}, Symphony.Artisan.autoSaveInterval);
			}
		},
		onContentChanged: function () {
		    this.setRevertDisabled(false);
		    this.setSettingMode();
		},
		setSettingMode: function () {
		    this.quizSettings.setDisabled(!this.section.isQuiz);

		    for (var i = 0; i < this.node.childNodes.length; i++) {
		        var pageNode = this.node.childNodes[i];

		        if (this.section.isQuiz === true) {
		            if (pageNode.get('cls').indexOf('quiz') == -1) {
		                pageNode.set('cls', pageNode.get('cls') + ' quiz');
		            }
		        } else {
		            pageNode.set('cls', pageNode.get('cls').replace(' quiz', ''));
		        }
		    }
		},
		onRevertClick: function () {
			this.objectives.setValue(this.lastObjectives);
			
			this.description.setValue(this.lastDescription);

			if (!this.lastMaxQuestions) {
			    this.lastMaxQuestions = '';
			}
			this.maxQuestions.setValue(this.lastMaxQuestions);

			if (!this.lastMaxTime) {
			    this.lastMaxTime = '';
			}
			this.maxTime.setValue(this.lastMaxTime);

			if (!this.lastMinimumPageTime) {
			    this.lastMinimumPageTime = '';
			}
			this.minimumPageTime.setValue(this.lastMinimumPageTime);

			if (!this.lastMaximumQuestionTime) {
			    this.lastMaximumQuestionTime = '';
			}
			this.maximumQuestionTime.setValue(this.lastMaximumQuestionTime);

			if (!this.lastPassingScore) {
			    this.lastPassingScore = '';
			}
			this.passingScore.setValue(this.lastPassingScore);

			switch (this.lastMarkingMethod) {
			    case Symphony.ArtisanCourseMarkingMethod.showAnswers:
			        this.showAnswers.setValue(true);
			        break;
			    case Symphony.ArtisanCourseMarkingMethod.hideAnswers:
			        this.hideAnswers.setValue(true);
			        break;
			    case Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback:
			        this.hideAnswersAndFeedback.setValue(true);
			        break;
			    case Symphony.ArtisanCourseMarkingMethod.hideAllFeedback:
			        this.hideAllFeedback.setValue(true);
			    default:
			        this.defaultMarking.setValue(true);
			}

			if (this.lastIsQuiz) {
			    this.quiz.setValue(true);
			} else {
			    this.normal.setValue(true);
			}

			if (this.lastIsSinglePage) {
			    this.singlePage.setValue(true);
			}

			if (!this.lastMinLoTime) {
			    this.lastMinLoTime = '';
			}
			this.minLoTime.setValue(this.lastMinLoTime);

			this.setRevertDisabled(true);
		},
		setRevertDisabled: function (disable) {
			this.revertButton.setDisabled(disable);
			if (this.node && disable) {
				this.node.set('cls', this.node.get('cls').replace(' node-italic', ''));
			} else {
			    if (this.node.get('cls').indexOf('node-italic') == -1) {
			        this.node.set('cls', this.node.get('cls') + ' node-italic');
			    }
			}
		},
		setSaved: function () {
		    this.lastObjectives = this.section.objectives;
		    this.lastIsQuiz = this.section.isQuiz;
		    this.lastDescription = this.section.description;
		    this.lastMaxQuestions = this.section.maxQuestions;
		    this.lastMaxTime = this.section.maxTime;
		    this.lastMarkingMethod = this.section.markingMethod;
		    this.lastPassingScore = this.section.passingScore;
		    this.lastMinimumPageTime = this.section.minimumPageTime;
		    this.lastMaximumQuestionTime = this.section.maximumQuestionTime;
		    this.lastMinLoTime = this.section.minLoTime;
		    this.lastIsSinglePage = this.section.isSinglePage;
		    this.lastFinalContentPage = this.section.finalContentPage;
		    this.setRevertDisabled(true);
		}
	});

})();