﻿(function () {
	Symphony.Artisan.AnswersGrid = Ext.define('artisan.answersgrid', {
	    alias: 'widget.artisan.answersgrid',
	    extend: 'symphony.localgrid',
		singleCorrect: false,
		answersEditable: true,
		defaultAnswerText: (this.answersEditable ? 'Double click to edit this answer' : ''),
		initComponent: function () {
			var colModel = new Ext.grid.ColumnModel({
				defaults: {
					align: 'center',
					sortable: false
				},
				columns: [
				new Ext.grid.RowNumberer(),
				{
					/*id: 'text',*/
					header: 'Text',
					dataIndex: 'text',
					align: 'left',
					flex: 1
				}, {
				    /*id: 'feedback',*/
				    header: 'Feedback',
				    dataIndex: 'feedback',
				    align: 'left',
				    renderer: function (value) {
				        if (value) {
				            return document.createElement('a').appendChild(
                              document.createTextNode(value)).parentNode.innerHTML
				        }
				        return '';
				    }
				}, {
					header: 'Is Correct',
					dataIndex: 'isCorrect',
					renderer: Symphony.checkRenderer,
					fixed: true,
					width: 80,
                    resizeable: false
				}]
			});
			var me = this;
			Ext.apply(this, {
                border: false,
                enableDragDrop: true,
                bodyStyle: 'padding-bottom: 10px',
				ddGroup: 'artisananswers',
				viewConfig: {
				    forceFit: true,
				    plugins: 'gridviewdragdrop'
				},
				colModel: colModel,
				model: 'artisanAnswer',
                listeners: {
				    celldblclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex; // Get field name
						var value = record.get(fieldName);
						if (fieldName == 'isCorrect') {
							if (me.singleCorrect && !value) {
								// a value is being set to true; set all values to false first
								grid.getStore().each(function (record) {
									record.set(fieldName, false);
									record.commit();
								});
							}
							record.set(fieldName, !value);
							record.commit();
							me.fireEvent('answerschange', me.getAnswers());
						} else if (fieldName == 'text') {
						    me.editAnswer(this, 'Edit Answer', 'Enter your answer below:', value, fieldName, record, me);
						} else if (fieldName == 'feedback') {
						    me.editAnswer(this, 'Edit Feedback', 'Enter your feedback below: (This will override the Incorrect/Correct response set on the question)', value, fieldName, record, me);
						}
					},
					afterrender: {
						single: true,
						fn: function (grid) {
							if (this.answersEditable) {
								// allow sorting
								this.requiredDT = new Ext.dd.DropTarget(grid.getView().getEl(), {
									ddGroup: 'artisananswers',
									notifyDrop: function (dd, e, data) {
										var sm = grid.getSelectionModel();
										var rows = sm.getSelections();
										var cindex = dd.getDragData(e).rowIndex;
										if (sm.hasSelection()) {
											for (i = 0; i < rows.length; i++) {
												grid.store.remove(grid.store.getById(rows[i].id));
												grid.store.insert(cindex, rows[i]);
											}
											sm.selectRecords(rows);
										}

										me.refreshUI();
										me.fireEvent('answerschange', me.getAnswers());
									}
								});
							}
						}
					}
				}
			});
			this.callParent(arguments);
		},
		editAnswer: function(grid, title, msg, value, fieldName, record, me) {
		    if (grid.answersEditable) {
		        Ext.Msg.show({
		            title: title,
		            width: 600,
		            value: value == grid.defaultAnswerText ? '' : value,
		            defaultTextHeight: 120,
		            multiline: true,
		            prompt: true,
		            msg: msg,
		            fn: function (btn, text) {
		                // add and commit
		                if (btn == 'ok') {
		                    record.set(fieldName, text);
		                    record.commit();
		                    me.fireEvent('answerschange', me.getAnswers());
		                }
		            },
		            buttons: Ext.Msg.OKCANCEL
		        });
		    }
		},
		addRow: function () {
			if (!this.autoId) {
				this.autoId = -1;
			}
			// add a new row; if there are no rows, then we flag the answer as correct
			this.store.add({
			    text: this.defaultAnswerText,
                feedback: '',
                isCorrect: false,
                id: this.audoId--
			});
		},
		removeSelectedRow: function () {
			var sm = this.getSelectionModel();
			var rows = sm.getSelections();
			this.store.remove(rows);
			this.refreshUI();
		},
		getAnswers: function () {
			var i = 0,
				result = [];
			this.store.each(function (record) {
				result.push({
					sort: i,
					text: record.get('text'),
					isCorrect: record.get('isCorrect'),
                    feedback: record.get('feedback')
				});
				i++;
			});
			return result;
		},
		refreshUI: function () {
			// forces a re-render, which in turn makes sure the row index updates properly
			this.store.each(function (r) {
				r.commit();
			});
		}
	});

})();