﻿(function () {
	Symphony.Artisan.DeploymentTab = Ext.define('artisan.deploymenttab', {
	    alias: 'widget.artisan.deploymenttab',
	    extend: 'Ext.Panel',
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				layout: 'border',
				items: [{
					split: true,
					collapsible: true,
					region: 'west',
					border: false,
					width: 290,
					title: 'Select a Deployment',
					id: 'artisan.deploymentgrid',
                    xtype: 'artisan.deploymentgrid',
					ref: 'deploymentGrid',
					listeners: {
						cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
							var record = grid.store.getAt(rowIndex);
							if (grid.headerCt.getGridColumns()[columnIndex].id != 'delete') {
								me.addPanel(record);
							}
						},
						addclick: function () {
							me.addPanel(Ext.create('artisanDeployment', {
							    id: 0,
							    courseIds: [],
	                            customerIds: [],
							    name: 'New Deployment',
	                            isUpdate: false,
	                            toArtisan: false,
	                            toSymphony: false,
	                            toMars: false
							}));
						}
					}
				}, {
					region: 'center',
					border: false,
					xtype: 'tabpanel',
					ref: 'displayPane'
				}],
				listeners: {
					// delayed load
					activate: {
						fn: function (panel) {
							panel.find('xtype', 'artisan.deploymentgrid')[0].refresh();
						},
						single: true
					}
				}
			});
			this.callParent(arguments);
		},
		activate: function () {
			this.find('xtype', 'artisan.deploymentgrid')[0].refresh();
		},
		addPanel: function (record) {
			var id = record.get('id');
			var me = this;
			
			var found = id && this.displayPane.findBy(function (element) {
				if (element.deployment) {
					return element.deployment.id == id;
				}
				return false;
			});
			
			if (found && found.length > 0) {
				this.displayPane.activate(found[0]);
			} else {
				var p = new Symphony.Artisan.DeploymentForm({
					title: (record.data ? record.get('name') : record.name),
					deployment: record.data || record,
					border: false,
					closable: true,
				    activate: true, 
					listeners: {
						applied: function () {
							me.deploymentGrid.refresh();
						}
					}
				});
				this.displayPane.add(p);

				setTimeout(function () {
				    me.displayPane.doLayout();
				}, 10);

				p.show();
			}
		}
	});

})();