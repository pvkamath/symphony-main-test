﻿/*
*
*
*  This file appears to no longer be in use. 
*  Ses CourseOptionsEditor.js
*
*/

(function () {
	Symphony.Artisan.CourseForm = Ext.define('artisan.courseform', {
	    alias: 'widget.artisan.courseform',
	    extend: 'Ext.Panel',
		course: null,
		initComponent: function () {
			var me = this;

			Ext.apply(this, {
			    autoScroll: true,
                border: false,
				bodyStyle: 'padding: 15px',
				items: [{
					name: 'general',
					xtype: 'form',
					border: false,
					frame: true,
					labelWidth: 75,
					labelPad: 5,
					defaults: {
						anchor: '100%'
					},
					items: [{
						name: 'name',
						fieldLabel: 'Name',
						allowBlank: false,
						xtype: 'textfield'
					}, {
						name: 'description',
						fieldLabel: 'Description',
						xtype: 'symphony.spellcheckarea'
					}, {
						name: 'keywords',
						fieldLabel: 'Keywords',
						xtype: 'textfield'
					}, {
						name: 'categoryId',
						fieldLabel: 'Category',
						xtype: 'hidden',
						value: 0
					}, {
						xtype: 'symphony.pagedcombobox',
						url: '/services/artisan.svc/themes/', // Artisan theme folders - some may be dynamic
						name: 'themeId',
						fieldLabel: 'Theme',
						bindingName: 'theme.name',
						model: 'artisanTheme',
						valueField: 'id',
						displayField: 'name',
						allowBlank: false,
						emptyText: 'Select a Theme',
						valueNotFoundText: 'Select a Theme',
						listeners: {
						    select: function (combo, records, index) {
						        // change the theme for all open pages
                                // TODO: How we going to show the dynamic theme in preview?
							    me.fireEvent('themechanged', records[0].data);
							    var themeFlavorCombo = me.query('[name=themeFlavorId]')[0];
                                if (themeFlavorCombo) {
							        themeFlavorCombo.setVisible(records[0].get('isDynamic'));
							    }
							}
						}
					}, {
					    xtype: 'symphony.pagedcombobox',
					    url: '/services/customer.svc/themes/', // Not really themes, but groups of settings for a dynamic theme.... A flavor!
					    name: 'themeFlavorId',
					    fieldLabel: 'Theme Flavor',
					    model: 'Theme',
					    valueField: 'id',
					    displayField: 'name',
					    allowBlank: true,
					    emptyText: 'Select a Flavor',
					    valueNotFoundText: 'Select a Flavor',
					    listeners: {
					        select: function(combo, record, index) {
					            // TODO: How are we going to render the dynamic theme...
					            // We will need an endpoint to query and stick it 
					            // in some folder somewhere or just return the css and embed
					            // into the editor. Will ahve to investigate the themechanged event

					        }
					    },
					    bindingName: 'themeFlavor.name'
					}]
				}]
			});
			this.callParent(arguments);
		},
		onRender: function () {
			Symphony.Artisan.CourseForm.superclass.onRender.apply(this, arguments);
			this.load();
		},
		load: function () {
			if (this.course) {
				this.find('name', 'general')[0].bindValues(this.course);
			}
		}
	});

})();