﻿(function () {
    Symphony.Artisan.ParameterSetOptionsPagedGrid = Ext.define('artisan.parametersetoptionspagedgrid', {
        alias: 'widget.artisan.parametersetoptionspagedgrid',
        extend: 'symphony.searchablegrid',
        parameterSet: null,
        selected: 0,
        urlTemplate: '/services/artisan.svc/parametersets/{0}/options',
        parameterSetOptionsInUse: [],
        initComponent: function () {
            var me = this;
            var id = me.parameterSet ? me.parameterSet.id : 0;
            var url = me.urlTemplate.format(id);

            

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'value',*/
                    dataIndex: 'value',
                    header: me.parameterSet.name,
                    editor: new Ext.form.TextField({ allowBlank: false }),
                    flex: 1
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                hideHeaders: false,
                colModel: colModel,
                model: 'parameterSetOption',
                searchColumns: ['value'],
                url: url,
                viewConfig: {
                    forceFit: true
                },
                listeners: {
                    render: function (grid) {
                        grid.store.addListener('load', function () {
                            grid.store.filter([{
                                fn: function (record) {
                                    if (me.parameterSetOptionsInUse.indexOf(record.get('id')) > -1) {
                                        return false;
                                    }
                                    return true;
                                },
                                scope: this
                            }]);

                            var record = grid.store.getById(me.selected);
                            if (record) {
                                grid.getSelectionModel().selectRow(grid.store.indexOf(record), true);
                            }
                        });
                    }
                }
            });

            this.callParent(arguments);
        },
        setData: function (data) {
            var options = { data: data || [] };
            this.store.loadData(options);
        },
        setParameterSet: function (parameterSet) {
            this.parameterSet = parameterSet;
            
            var url = this.urlTemplate.format(parameterSet.id);

            this.store.proxy.url = url;
            this.refresh();
        },
        performSearch: function (searchText) {
            var me = this;

            if (!this.store) { return; }
            if (searchText) {
                var filter = [{
                    fn: function (record) {
                        if (me.parameterSetOptionsInUse.indexOf(record.get('id')) > -1) {
                            return false;
                        }

                        for (var i = 0; i < this.searchColumns.length; i++) {
                            if (record.get(this.searchColumns[i]).toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                                return true;
                            }
                        }
                        return false;
                    },
                    scope: this
                }];

                this.store.filter(filter);
            } else {
                var filter = [{
                    fn: function (record) {
                        if (me.parameterSetOptionsInUse.indexOf(record.get('id')) > -1) {
                            return false;
                        }

                        return true;
                    },
                    scope: this
                }];

                this.store.filter(filter);
            }
        }
    });

})();