﻿(function () {
    Symphony.Artisan.ParameterSetTab = Ext.define('artisan.parametersettab', {
        alias: 'widget.artisan.parametersettab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                border: false,
                items: [{
                    split: true,
                    collapsible: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    title: 'Parameter Sets',
                    itemId: 'artisan.parametersetgrid',
                    xtype: 'artisan.parametersetgrid',
                    ref: 'parameterSetGrid',
                    tbar: {
                        items: [{
                            xtype: 'button',
                            iconCls: 'x-button-add',
                            text: 'Add',
                            handler: function () {
                                me.addPanel(0, "New Parameter Set");
                            }
                        }]
                    },
                    listeners: {
                        cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            var record = grid.getStore().getAt(rowIndex);
                            me.addPanel(record.data.id, record.data.name, record);
                        }
                    }
                }, {
                    region: 'center',
                    border: false,
                    ref: 'displayPane',
                    xtype: 'tabpanel'
                }]
            });
            this.callParent(arguments);
        },
        addPanel: function (id, name, record) {
            var me = this;
            var found = id && this.displayPane.findBy(function (element) {
                if (element.parameterSet) {
                    return element.parameterSet.id == id;
                }
                return false;
            });

            if (found.length > 0 && this.displayPane) {
                this.displayPane.activate(found[0]);
            } else {
                var p = new Symphony.Artisan.ParameterSetPanel({
                    title: name,
                    parameterSet: record ? record.data : null,
                    border: false,
                    listeners: {
                        save: function () {
                            me.parameterSetGrid.refresh();
                        },
                        render: function (panel) {
                            if (record) {
                                panel.load(record.data);
                            }
                        }
                    }
                });
                this.displayPane.add(p);
                this.displayPane.doLayout();
                p.show();
            }
        }
    });

})();