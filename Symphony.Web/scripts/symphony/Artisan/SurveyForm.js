﻿(function () {
	Symphony.Artisan.SurveyForm = Ext.define('artisan.surveyform', {
	    alias: 'widget.artisan.surveyform',
	    extend: 'Ext.Panel',
		content: '',
		lastContent: '',
		reset: false,
		page: null,
		node: null,
		answersEditable: true,
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				frame: true,
				labelAlign: 'top',
				layout: 'form',
				autoScroll: true,
				items: [{
					xtype: 'label',
					cls: 'x-form-item',
					html: 'Directions: Enter the survey text below, then click "Add Item" for each survey item'
				}, {
					name: 'body',
					xtype: 'symphony.spellcheckarea',
					labelAlign: 'top',
					anchor: '100%',
					height: 125,
					enableKeyEvents: true,
					fieldLabel: 'Survey Text',
					value: me.page.html,
					ref: 'questionEditor',
					listeners: {
						keyup: function (field, e) {
							me.fireEvent('contentchange', field.getValue(), me.answersGrid.getAnswers());
						}
					}
				}, {
					xtype: 'label',
					cls: 'x-form-item',
					html: 'Items (drag/drop to reorder; double click to edit):'
				}, {
                    xtype: 'artisan.surveyitemsgrid',
					autoHeight: true,
					singleCorrect: this.singleCorrect,
					answersEditable: this.answersEditable,
					border: true,
					ref: 'answersGrid',
					data: me.page.answers,
					listeners: {
						answerschange: function (answers) {
							me.fireEvent('contentchange', me.questionEditor.getValue(), answers);
						},
						rowclick: function () {
							if (me.removeButton) {
								me.removeButton.enable();
							}
						}
					}
				}, {
					xtype: 'panel',
					layout: 'hbox',
					layoutConfig: {
						pack: 'start'
					},
					items: [{
						xtype: 'button',
						iconCls: 'x-button-add',
						style: 'padding:10px 0',
						text: 'Add Item',
						handler: function () {
							me.answersGrid.addRow();
							me.fireEvent('contentchange', me.questionEditor.getValue(), me.answersGrid.getAnswers());
						}
					}, {
						xtype: 'button',
						iconCls: 'x-button-delete',
						style: 'padding:10px 5px',
						text: 'Remove Selected Item(s)',
						ref: '../removeButton',
						disabled: true,
						handler: function () {
							me.answersGrid.removeSelectedRow();
							me.fireEvent('contentchange', me.questionEditor.getValue(), me.answersGrid.getAnswers());
							this.disable();
						}
					}]
				}]
			});
			this.callParent(arguments);
		},
		getValues: function () {
			return {
				question: this.questionEditor.getValue(),
				answers: this.answersGrid.getAnswers()
			};
		},
		setValue: function (question, answers) {
			this.answersGrid.setData(answers);
			this.questionEditor.setValue(question);
			this.fireEvent('contentchange', question, answers);
		}
	});

})();