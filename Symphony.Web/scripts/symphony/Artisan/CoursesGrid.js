﻿(function () {
	Symphony.Artisan.CoursesGrid = Ext.define('artisan.coursesgrid', {
	    alias: 'widget.artisan.coursesgrid',
	    extend: 'symphony.searchablegrid',
	    hideAddButton: false,
        selectedCategoryId: 0,
        selectedSecondaryCategoryId: 0,
		initComponent: function () {
			var me = this;
			var url = '/services/artisan.svc/courses/';


			var colModel = new Ext.grid.ColumnModel({
				defaults: {
					sortable: true,
					align: 'center',
					renderer: Symphony.Portal.valueRenderer
				},
				columns: [{
					/*id: 'delete',*/
					header: ' ',
					renderer: Symphony.Artisan.deleteCourseRenderer,
                    hidden: !Symphony.User.isArtisanAuthor,
					width: 28
				}, {
					/*id: 'name',*/
					header: 'Name',
					dataIndex: 'name',
					align: 'left',
					renderer: this.nameRenderer,
					flex: 1
				}, {
				    /*id: 'levelIndentText',*/
				    header: 'Category',
				    dataIndex: 'levelIndentText',
                    sortable: false,
				    renderer: function (value, meta, record) {
				        /*console.log(record);
				        if (record.get('categoryId') == 0) {
				            return '';
				        }*/
				        return value;
				    }
				}, {
				    header: 'Modified On',
				    dataIndex: 'modifiedOn',
				    renderer: Symphony.shortDateRenderer
				}]
			});

			Ext.apply(this, {
				tbar: {
					items: [{
						text: 'Add',
						iconCls: 'x-button-add',
						hidden: me.hideAddButton || !Symphony.User.isArtisanAuthor,
						handler: function () {
							me.fireEvent('addclick');
						}
					}]
				},
				idProperty: 'id',
				deferLoad: true,
				url: url,
				colModel: colModel,
				model: 'artisanCourse',
				listeners: {
				    afterrender: function (comp) {

				        me.addDocked({
				            xtype: 'toolbar',
                            layout: 'anchor',
				            dock: 'top',
				            items: [{
				                name: 'categoryId',
				                xtype: 'symphony.pagedcombobox',
				                url: '/services/category.svc/categories/artisan/paged/',
				                model: 'category',
				                bindingName: 'categoryName',
				                clearable: true,
				                ref: 'combo',
				                valueField: 'id',
				                displayField: 'levelIndentText',
				                allowBlank: true,
				                anchor: '100%',
				                emptyText: 'Select a category...',
				                valueNotFoundText: 'Select a category...',
				                listeners: {
				                    focus: function () {
				                        var secondaryCategoryCtrl = me.find('name', 'secondaryCategoryId')[0];
				                        secondaryCategoryCtrl.clearValue()
				                    },
				                    change: function (combo, newValue, oldValue) {
				                        if (!newValue) {
				                            me.updateStore(me.url);
				                            me.selectedCategoryId = 0;
				                        }
				                    },
				                    select: function (combo, records, index) {
				                        var record = records[0];
				                        me.filter({
				                            categoryId: record.get('id')
				                        });
				                        me.selectedCategoryId = record.get('id');
				                        me.refresh();
				                    },
				                    blur: function (combo) {
				                        if (!combo.getValue()) {
				                            me.updateStore(me.url);
				                            me.selectedCategoryId = 0;
				                        }
				                    }
				                }
				            }, {
				                name: 'secondaryCategoryId',
				                xtype: 'symphony.pagedcombobox',
				                clearable: true,
				                url: '/services/category.svc/secondarycategories/artisan/paged/',
				                //recordDef: Ext.data.Record.create(Symphony.Definitions.category),
                                model: 'category',
				                bindingName: 'categoryName',
				                ref: 'secondarycombo',
				                valueField: 'id',
				                allowBlank: true,
                                hidden: true,
				                anchor: '100%',
				                emptyText: 'Select a secondary category...',
				                valueNotFoundText: 'Select a secondary category...',
				                listeners: {
				                    select: function (combo, records, index) {
				                        var record = records[0];
				                        me.filter({
				                            categoryId: 0,
				                            secondaryCategoryId: record.get('id')
				                        });
				                        me.selectedSecondaryCategoryId = record.get('id');
				                        //me.selectedCategoryId = 0;
				                        me.refresh();
				                    },
				                    blur: function (combo) {
				                        if (!combo.getValue()) {
				                            me.updateStore(me.url);
				                            //me.secondaryCategoryId = 0;
				                        }
				                    }
				                }
				            }]
				        });

				        me.store.addListener('load', function () {
				            if (me.selectedCategoryId) {
				                me.filter({
				                    categoryId: me.selectedCategoryId
				                });
				            }
				        });

				        return;
				        var tPanel = comp.getTopToolbar().ownerCt;
				        tPanel.add();

				        

					}
				}
			});
			
			this.callParent(arguments);
		},
		nameRenderer: function (value, meta, record) {
			if (record.data.description) {
				return value + ' <span style="color: #666">- ' + record.data.description;
			}
			return value;
		},
		getSelected: function () {
			return this.getSelectionModel().selections;
		}
	});

})();