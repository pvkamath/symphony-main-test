﻿(function () {
    Symphony.Artisan.ParameterSetOptionsEditorGrid = Ext.define('artisan.parametersetoptionseditorgrid', {
        alias: 'widget.artisan.parametersetoptionseditorgrid',
        extend: 'symphony.editableunpagedgrid',
        parameterSet: null,
        urlTemplate: '/services/artisan.svc/parametersets/{0}/options',
        initComponent: function () {
            var me = this;
            var id = me.parameterSet ? me.parameterSet.id : 0;
            var url = me.urlTemplate.format(id);

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'value',*/
                    dataIndex: 'value',
                    header: 'Value',
                    editor: new Ext.form.TextField({ allowBlank: false }),
                    flex: 1
                },
                { header: ' ', hideable: false, renderer: Symphony.deleteRenderer, width: 28 }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                hideHeaders: false,
                colModel: colModel,
                model: 'parameterSetOption',
                title: 'Options',
                clicksToEdit: 1,
                url: url,
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.getStore().getAt(rowIndex);
                        if (columnIndex === 1) {
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this option?', function (btn) {
                                if (btn == 'yes') {
                                    grid.getStore().remove(record);
                                }
                            });
                        }
                    }
                },
                tbar: [{
                    iconCls: 'x-button-add',
                    text: 'Add',
                    xtype: 'button',
                    handler: function () {
                        me.addValue();
                    }
                }]
            });

            this.callParent(arguments);
        },
        addValue: function () {
            var me = this;
            var w = new Ext.Window({
                width: 600,
                height: 240,
                title: 'Add Values',
                modal: true,
                tbar: [{
                    xtype: 'button',
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function () {
                        var values = w.find('xtype', 'form')[0].getForm().getValues().newValues;
                        var valArray = values.split(',');

                        for (var i = 0; i < valArray.length; i++) {
                            var v = valArray[i].trim();

                            me.store.add({
                                id: 0,
                                parameterSetId: me.parameterSet ? me.parameterSet.id : 0,
                                value: v
                            });
                        }

                        w.close();
                    }
                }],
                items: [{
                    xtype: 'form',
                    frame: false,
                    border: false,
                    cls: 'x-panel-mc',
                    padding: '5px',
                    items: [{
                        cls: 'x-form-item',
                        text: 'Separate multiple values with a comma.',
                        xtype: 'label',
                        style: 'padding: 0px 0px 5px 0px'
                    }, {
                        xtype: 'textarea',
                        name: 'newValues',
                        anchor: '100%',
                        height: 140,
                        hideLabel: true
                    }]
                }]
            }).show();
        },
        setData: function (data) {
            var options = { data: data || [] };
            this.store.loadData(options);
        },
        setParameterSet: function (parameterSet) {
            this.parameterSet = parameterSet;
            
            var url = this.urlTemplate.format(parameterSet.id);

            this.store.proxy.url = url;
            this.refresh();
        }
    });

})();