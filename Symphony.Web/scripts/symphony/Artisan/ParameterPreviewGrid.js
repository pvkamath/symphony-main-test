﻿(function () {
    Symphony.Artisan.ParameterPreviewGrid = Ext.define('artisan.parameterpreviewgrid', {
        alias: 'widget.artisan.parameterpreviewgrid',
        extend: 'symphony.searchablelocalgrid',
        initComponent: function () {
            var me = this;
            

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'name',*/
                    dataIndex: 'name',
                    header: 'Name',
                    flex: 1
                }, {
                    /*id: 'code',*/
                    dataIndex: 'code',
                    header: 'Code'
                }, {
                    /*id: 'parameterSetName',*/
                    dataIndex: 'parameterSetName',
                    header: 'Parameter Set'
                }, {
                    /*id: 'value',*/
                    dataIndex: 'value',
                    header: 'Value',
                    renderer: function (value, meta, record) {
                        if (typeof (value) === 'undefined' || value === '') {
                            return record.get('defaultValue');
                        } else {
                            return value;
                        }
                    }
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                colModel: colModel,
                model: 'parameter',
                searchColumns: ['name']
            });

            this.callParent(arguments);
        }
    });

})();