﻿(function () {
	Symphony.Artisan.CategoryMatchingForm = Ext.define('artisan.categorymatchingform', {
	    alias: 'widget.artisan.categorymatchingform',
	    extend: 'Ext.Panel',
		content: '',
		lastContent: '',
		reset: false,
		page: null,
		node: null,
		answersEditable: true,
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				frame: true,
				labelAlign: 'top',
				layout: 'form',
				autoScroll: true,
				items: [{
					xtype: 'label',
					cls: 'x-form-item',
					html: 'Directions: Enter the question text below then click "Add Category" for each category of matching items.'
				}, {
					name: 'body',
					xtype: 'symphony.spellcheckarea',
					labelAlign: 'top',
					anchor: '100%',
					height: 125,
					enableKeyEvents: true,
					fieldLabel: 'Question',
					value: me.page.html,
					ref: 'questionEditor',
					listeners: {
						keyup: function (field, e) {
							me.fireEvent('contentchange', field.getValue(), me.answersGrid.getAnswers(), me.correctResponse.getValue(), me.incorrectResponse.getValue());
						}
					}
				}, {
					xtype: 'label',
					cls: 'x-form-item',
					html: 'Categories (drag/drop to reorder; double click to edit; CTRL+click to select multiple):'
				}, {
                    xtype: 'artisan.categorymatchgrid',
					autoHeight: true,
					border: true,
					ref: 'answersGrid',
					data: me.page.answers,
					listeners: {
						answerschange: function (answers) {
							me.fireEvent('contentchange', me.questionEditor.getValue(), answers, me.correctResponse.getValue(), me.incorrectResponse.getValue());
						},
						rowclick: function () {
							if (me.removeButton) {
								me.removeButton.enable();
							}
						}
					}
				}].concat(this.answersEditable ? [{
					xtype: 'panel',
					layout: 'hbox',
					layoutConfig: {
						pack: 'start'
					},
					items: [{
						xtype: 'button',
						iconCls: 'x-button-add',
						style: 'padding:10px 0',
						text: 'Add Category',
						handler: function () {
							me.answersGrid.addRow();
							me.fireEvent('contentchange', me.questionEditor.getValue(), me.answersGrid.getAnswers());
						}
					}, {
						xtype: 'button',
						iconCls: 'x-button-delete',
						style: 'padding:10px 5px',
						text: 'Remove Selected Category',
						ref: '../removeButton',
						disabled: true,
						handler: function () {
							me.answersGrid.removeSelectedRow();
							me.fireEvent('contentchange', me.questionEditor.getValue(), me.answersGrid.getAnswers());
							this.disable();
						}
					}]
				}] : []).concat([{
					xtype: 'symphony.spellcheckarea',
					fieldLabel: 'Correct Response',
					value: me.page.correctResponse || 'Correct.',
					anchor: '100%',
					ref: 'correctResponse',
					enableKeyEvents: true,
					listeners: {
						keyup: function (field, e) {
							me.fireEvent('contentchange', me.questionEditor.getValue(), me.answersGrid.getAnswers(), field.getValue(), me.incorrectResponse.getValue());
						}
					}
				}, {
					xtype: 'symphony.spellcheckarea',
					fieldLabel: 'Incorrect Response',
					value: me.page.incorrectResponse || 'Incorrect.',
					anchor: '100%',
					ref: 'incorrectResponse',
					enableKeyEvents: true,
					listeners: {
						keyup: function (field, e) {
							me.fireEvent('contentchange', me.questionEditor.getValue(), me.answersGrid.getAnswers(), me.correctResponse.getValue(), field.getValue());
						}
					}
				}])
			});
			this.callParent(arguments);
		},
		getValues: function () {
			return {
				question: this.questionEditor.getValue(),
				answers: this.answersGrid.getAnswers(),
				correctResponse: this.correctResponse.getValue(),
				incorrectResponse: this.incorrectResponse.getValue()
			};
		},
		setValue: function (question, answers, correctResponse, incorrectResponse) {
			this.answersGrid.setData(answers);
			this.questionEditor.setValue(question);
			this.correctResponse.setValue(correctResponse);
			this.incorrectResponse.setValue(incorrectResponse);
			this.fireEvent('contentchange', question, answers);
		}
	});

})();