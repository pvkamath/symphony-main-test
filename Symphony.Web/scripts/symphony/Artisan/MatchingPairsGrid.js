﻿(function () {
	Symphony.Artisan.MatchingPairsGrid = Ext.define('artisan.matchingpairsgrid', {
	    alias: 'widget.artisan.matchingpairsgrid',
	    extend: 'symphony.localgrid',
		defaultAnswerText: 'Double click to edit this match',
		initComponent: function () {
			var colModel = new Ext.grid.ColumnModel({
				defaults: {
					align: 'center',
					sortable: false
				},
				columns: [
				new Ext.grid.RowNumberer(),
				{
					itemId: 'leftText',
					header: 'Left Text',
					dataIndex: 'text',
					align: 'left',
                    flex: 1,
                    renderer: function(value){
                        var match = Ext.decode(value);
                        return (match && match.leftText ? match.leftText : '');
                    }
				}, {
				    itemId: 'rightText',
					header: 'Right Text',
					dataIndex: 'text',
					align: 'left',
                    flex: 1,
                    renderer: function(value){
                        var match = Ext.decode(value);
                        return (match && match.rightText ? match.rightText : '');
                    }
				}
                ]
			});
			var me = this;
			Ext.apply(this, {
				enableDragDrop: true,
				ddGroup: 'artisananswers',
                viewConfig: {
                    forceFit: true,
                    plugins: 'gridviewdragdrop'
                },
				colModel: colModel,
				model: 'artisanAnswer',
				listeners: {
					celldblclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) { // Get the Record
						var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex; // Get field name
						var columnId = grid.headerCt.getGridColumns()[columnIndex].itemId;
						var value = record.get(fieldName);
                        var match = Ext.decode(value);
						if (columnId == 'leftText') {
							Ext.Msg.show({
								title: 'Edit Match',
								width: 600,
								value: (match && match.leftText ? match.leftText : ''),
								defaultTextHeight: 120,
								multiline: true,
								prompt: true,
								msg: 'Enter your left-side text of the match below:',
								fn: function (btn, text) {
									// add and commit
									if (btn == 'ok') {
                                        match.leftText = text;
										record.set(fieldName, Ext.encode(match));
										record.commit();
										me.fireEvent('answerschange', me.getAnswers());
									}
								},
								buttons: Ext.Msg.OKCANCEL
							});
						}
                        else if (columnId == 'rightText') {
							Ext.Msg.show({
								title: 'Edit Match',
								width: 600,
								value: (match && match.rightText ? match.rightText : ''),
								defaultTextHeight: 120,
								multiline: true,
								prompt: true,
								msg: 'Enter your right-side text of the match below:',
								fn: function (btn, text) {
									// add and commit
									if (btn == 'ok') {
                                        match.rightText = text;
										record.set(fieldName, Ext.encode(match));
										record.commit();
										me.fireEvent('answerschange', me.getAnswers());
									}
								},
								buttons: Ext.Msg.OKCANCEL
							});
						}
					},
					afterrender: {
						single: true,
						fn: function (grid) {
							// allow sorting
							this.requiredDT = new Ext.dd.DropTarget(grid.getView().getEl(), {
								ddGroup: 'artisananswers',
								notifyDrop: function (dd, e, data) {
									var sm = grid.getSelectionModel();
									var rows = sm.getSelections();
									var cindex = dd.getDragData(e).rowIndex;
									if (sm.hasSelection()) {
										for (i = 0; i < rows.length; i++) {
											grid.store.remove(grid.store.getById(rows[i].id));
											grid.store.insert(cindex, rows[i]);
										}
										sm.selectRecords(rows);
									}

									me.refreshUI();
									me.fireEvent('answerschange', me.getAnswers());
								}
							});
						}
					}
				}
			});
			this.callParent(arguments);
		},
		addRow: function () {
			if (!this.autoId) {
				this.autoId = -1;
			}
			this.store.add({
				text: Ext.encode({leftText:'', rightText:''}),
				isCorrect: true,
                id: this.audoId--
			});
		},
		removeSelectedRow: function () {
			var sm = this.getSelectionModel();
			var rows = sm.getSelections();
			this.store.remove(rows);
			this.refreshUI();
		},
		getAnswers: function () {
			var i = 0,
				result = [];
			this.store.each(function (record) {
				result.push({
					sort: i,
					text: record.get('text'),
					isCorrect: record.get('isCorrect')
				});
				i++;
			});
			return result;
		},
		refreshUI: function () {
			// forces a re-render, which in turn makes sure the row index updates properly
			this.store.each(function (r) {
				r.commit();
			});
		}
	});

})();