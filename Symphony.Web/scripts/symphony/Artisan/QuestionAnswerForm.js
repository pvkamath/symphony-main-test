﻿(function () {
    /* all that apply, multiple choice, and true false all work on the same basic principles */
	Symphony.Artisan.QuestionAnswerForm = Ext.define('artisan.questionanswerform', {
	    alias: 'widget.artisan.questionanswerform',
	    extend: 'Ext.Panel',
		content: '',
		lastContent: '',
		reset: false,
		page: null,
		node: null,
		answersEditable: true,
		initComponent: function () {
			var me = this;
			Ext.apply(this, {
				frame: true,
				labelAlign: 'top',
				layout: 'form',
				autoScroll: true,
				items: [{
					xtype: 'label',
					cls: 'x-form-item',
					html: 'Directions: Enter the question text below' + (this.answersEditable ? ', then click "Add Answer" for each additional possible answer and select the correct answer(s) from the resulting list.' : ', then select the correct answer from the list below.')
				}, {
					name: 'body',
					xtype: 'symphony.spellcheckarea',
					labelAlign: 'top',
					anchor: '100%',
					height: 125,
					enableKeyEvents: true,
					fieldLabel: 'Question',
					value: me.page.html,
					ref: 'questionEditor',
					listeners: {
						keyup: function (field, e) {
							me.fireEvent('contentchange', field.getValue(), me.answersGrid.getAnswers(), me.correctResponse.getValue(), me.incorrectResponse.getValue());
						}
					}
				}, {
					xtype: 'label',
					cls: 'x-form-item',
					html: 'Answers' + (this.answersEditable ? ' (drag/drop to reorder; double click to edit; CTRL+click to select multiple):' : '')
				}, {
					//id: 'artisan.answersgrid',
                    xtype: 'artisan.answersgrid',
					autoHeight: true,
					singleCorrect: this.singleCorrect,
					answersEditable: this.answersEditable,
					border: true,
					ref: 'answersGrid',
					data: me.page.answers,
					listeners: {
						answerschange: function (answers) {
							me.fireEvent('contentchange', me.questionEditor.getValue(), answers, me.correctResponse.getValue(), me.incorrectResponse.getValue());
						},
						rowclick: function () {
							if (me.removeButton) {
								me.removeButton.enable();
							}
						}
					}
				}].concat(this.answersEditable ? [{
				    xtype: 'panel',
                    cls: 'x-panel-transparent',
                    border: false,
				    layout: {
				        type: 'hbox',
                        pack: 'start'
				    },
					items: [{
						xtype: 'button',
						iconCls: 'x-button-add',
						style: 'margin:10px 0',
						text: 'Add Answer',
						handler: function () {
							me.answersGrid.addRow();
							me.fireEvent('contentchange', me.questionEditor.getValue(), me.answersGrid.getAnswers());
						}
					}, {
						xtype: 'button',
						iconCls: 'x-button-delete',
						style: 'margin:10px 5px',
						text: 'Remove Selected Answer(s)',
						ref: '../removeButton',
						disabled: true,
						handler: function () {
							me.answersGrid.removeSelectedRow();
							me.fireEvent('contentchange', me.questionEditor.getValue(), me.answersGrid.getAnswers());
							this.disable();
						}
					}]
				}] : []).concat([{
					xtype: 'symphony.spellcheckarea',
					fieldLabel: 'Correct Response',
					value: me.page.correctResponse || 'Correct.',
					anchor: '100%',
					ref: 'correctResponse',
					enableKeyEvents: true,
					listeners: {
						keyup: function (field, e) {
							me.fireEvent('contentchange', me.questionEditor.getValue(), me.answersGrid.getAnswers(), field.getValue(), me.incorrectResponse.getValue());
						}
					}
				}, {
					xtype: 'symphony.spellcheckarea',
					fieldLabel: 'Incorrect Response',
					value: me.page.incorrectResponse || 'Incorrect.',
					anchor: '100%',
					ref: 'incorrectResponse',
					enableKeyEvents: true,
					listeners: {
						keyup: function (field, e) {
							me.fireEvent('contentchange', me.questionEditor.getValue(), me.answersGrid.getAnswers(), me.correctResponse.getValue(), field.getValue());
						}
					}
				}])
			});
			this.callParent(arguments);
		},
		getValues: function () {
			return {
				question: this.questionEditor.getValue(),
				answers: this.answersGrid.getAnswers(),
				correctResponse: this.correctResponse.getValue(),
				incorrectResponse: this.incorrectResponse.getValue()
			};
		},
		setValue: function (question, answers, correctResponse, incorrectResponse) {
			this.answersGrid.setData(answers);
			this.questionEditor.setValue(question);
			this.correctResponse.setValue(correctResponse);
			this.incorrectResponse.setValue(incorrectResponse);
			this.fireEvent('contentchange', question, answers);
		}
	});

})();