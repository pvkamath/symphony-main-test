﻿(function () {
	Symphony.Artisan.SurveyItemsGrid = Ext.define('artisan.surveyitemsgrid', {
	    alias: 'widget.artisan.surveyitemsgrid',
	    extend: 'symphony.localgrid',
		defaultAnswerText: 'Double click to edit this item',
		initComponent: function () {
			var colModel = new Ext.grid.ColumnModel({
				defaults: {
					align: 'center',
					sortable: false
				},
				columns: [
				new Ext.grid.RowNumberer(),
				{
					/*id: 'text',*/
					header: 'Text',
					dataIndex: 'text',
					align: 'left',
					width: 120,
					flex: 1
				}]
			});
			var me = this;
			Ext.apply(this, {
				enableDragDrop: true,
				ddGroup: 'artisananswers',
				colModel: colModel,
				model: 'artisanAnswer',
				viewConfig: {
				    plugins: 'gridviewdragdrop'
				},
				listeners: {
					celldblclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) { // Get the Record
						var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex; // Get field name
						var value = record.get(fieldName);
						if (fieldName == 'text') {
							Ext.Msg.show({
								title: 'Edit Survey Item',
								width: 600,
								value: value == this.defaultAnswerText ? '' : value,
								defaultTextHeight: 120,
								multiline: true,
								prompt: true,
								msg: 'Enter your survey item below:',
								fn: function (btn, text) {
									// add and commit
									if (btn == 'ok') {
										record.set(fieldName, text);
										record.commit();
										me.fireEvent('answerschange', me.getAnswers());
									}
								},
								buttons: Ext.Msg.OKCANCEL
							});
						}
					},
					afterrender: {
						single: true,
						fn: function (grid) {
							// allow sorting
							this.requiredDT = new Ext.dd.DropTarget(grid.getView().getEl(), {
								ddGroup: 'artisananswers',
								notifyDrop: function (dd, e, data) {
									var sm = grid.getSelectionModel();
									var rows = sm.getSelections();
									var cindex = dd.getDragData(e).rowIndex;
									if (sm.hasSelection()) {
										for (i = 0; i < rows.length; i++) {
											grid.store.remove(grid.store.getById(rows[i].id));
											grid.store.insert(cindex, rows[i]);
										}
										sm.selectRecords(rows);
									}

									me.refreshUI();
									me.fireEvent('answerschange', me.getAnswers());
								}
							});
						}
					}
				}
			});
			this.callParent(arguments);
		},
		addRow: function () {
			if (!this.autoId) {
				this.autoId = -1;
			}
			// add a new row; if there are no rows, then we flag the answer as correct
			this.store.add({
				text: this.defaultAnswerText,
				isCorrect: true,
				id: this.audoId--
			});
		},
		removeSelectedRow: function () {
			var sm = this.getSelectionModel();
			var rows = sm.getSelections();
			this.store.remove(rows);
			this.refreshUI();
		},
		getAnswers: function () {
			var i = 0,
				result = [];
			this.store.each(function (record) {
				result.push({
					sort: i,
					text: record.get('text'),
					isCorrect: record.get('isCorrect')
				});
				i++;
			});
			return result;
		},
		refreshUI: function () {
			// forces a re-render, which in turn makes sure the row index updates properly
			this.store.each(function (r) {
				r.commit();
			});
		}
	});

})();