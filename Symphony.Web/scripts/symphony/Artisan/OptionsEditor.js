﻿(function () {
	Symphony.Artisan.OptionsEditor = Ext.define('artisan.optionseditor', {
	    extend: 'Ext.form.FormPanel',

		reset: false,
		node: null,
		initComponent: function () {
		    this.callParent(arguments);

		    this.on('afterrender', function (cmp, eOpts) {
		        if (cmp.course && cmp.course.mode) {
		            cmp.fireEvent('toggleartisanmode', cmp.course.mode, true);
		        } else {
		            cmp.fireEvent('clearartisanmode');
		        }
		    });
		},
		setSaved: function () { }
	});
})();