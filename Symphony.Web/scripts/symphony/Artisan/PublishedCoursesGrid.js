﻿(function () {
	Symphony.Artisan.PublishedCoursesGrid = Ext.define('artisan.publishedcoursesgrid', {
	    alias: 'widget.artisan.publishedcoursesgrid',
	    extend: 'symphony.searchablegrid',
	    height: 530,
        width: 320,
        hidePreviousVersions: false,
		initComponent: function () {
			var me = this;
			var url = '/services/artisan.svc/publishedCourses/';

            var sm = new Ext.selection.CheckboxModel();
			var colModel = new Ext.grid.ColumnModel({
				columns: [

				    {
					    /*id: 'name',*/
					    header: 'Name',
					    dataIndex: 'name',
					    align: 'left',
					    doSort: function (state) {
					        var ds = this.up('tablepanel').store;
					        ds.sort({
					            property: 'artisancourses.name',
					            direction: state
					        });
					    },
					    width: 167 + 28 + 28// extra for missing delete/preview columns
				    },{
					    /*id: 'createdOn',*/
					    header: 'Published On',
					    dataIndex: 'createdOn',
					    align: 'left',
					    width: 75,
					    doSort: function (state) {
					        var ds = this.up('tablepanel').store;
					        ds.sort({
					            property: 'artisancourses.createdon',
					            direction: state
					        });
					    },
					    renderer: Symphony.dateRenderer
				    },{
				       /*id: 'levelIndentText',*/
				        header: 'Category',
				        dataIndex: 'levelIndentText',
				        renderer: Symphony.text,
				        doSort: function (state) {
				            var ds = this.up('tablepanel').store;
				            ds.sort({
				                property: 'artisancourses.createdon',
				                direction: state
				            });
				        },
					    width: 75
				    }
				]
			});
			Ext.apply(this, {
				idProperty: 'id',
				url: url,
				colModel: colModel,
				model: 'artisanCourse',
				selModel: sm,
				plugins: [{ ptype: 'pagingselectpersist' }],
				tbar: {
				    items: [{
				        xtype: 'button',
				        enableToggle: true,
				        text: 'Hide Duplicates',
				        iconCls: 'x-button-eye',
				        toggleHandler: function (button, isPressed) {
				            if (isPressed){
				                me.hidePreviousVersions = true;
				                me.reload();
				            }
				            else{
				                me.hidePreviousVersions = false;
				                me.reload();
				            }
                        }
				    }]
				},
				listeners: {
				    render: function (comp) {
				        var tPanel = comp.getTopToolbar().ownerCt;
				        tPanel.add(new Ext.Toolbar({
				            layout: 'fit',
				            items: [{
				                name: 'categoryId',
				                xtype: 'symphony.pagedcombobox',
				                url: '/services/category.svc/categories/artisan/paged/',
				                model: 'category',
				                bindingName: 'levelIndentText',
				                ref: 'combo',
				                valueField: 'id',
				                displayField: 'levelIndentText',
				                allowBlank: true,
				                flex: 1,
				                emptyText: 'Select a category...',
				                valueNotFoundText: 'Select a category...',
				                listeners: {
				                    keyup: function (combo, e) {
				                        me.performSearch(combo.lastQuery);
				                    },
				                    select: function (combo, record, index) {
				                        me.performSearch(record.data.levelIndentText);
				                    }
				                }
				            }]
				        }));

				        me.store.addListener('load', function () {
				            if (me.selectedCategoryId) {
				                me.filter({
				                    categoryId: me.selectedCategoryId
				                });
				            }
				        });

				    }
				}
			});
			this.callParent(arguments);
		},
		reload: function(){
		    this.store.setBaseParam('hidePrevious',this.hidePreviousVersions);
		    this.store.load();
		},
		previewCourseRenderer: function (value, meta, record) {
		    //return '<a href="#" onclick="Symphony.Artisan.previewCourse({0});"><img src="/images/page_white_magnify.png" /></a>'.format(record.get('id'));
		    return '<a href="#" onclick="alert(\'Preview functionality not yet implemented\');"><img src="/images/page_white_magnify.png" /></a>'.format(record.get('id'));
	    },
		previewCourse: function(id){
            var record = grid.selModel.getSelected();
            var course = record.raw;
            
            Ext.Msg.wait('Please wait while your course is packaged...', 'Please wait...');
            
            Symphony.Ajax.request({
                url: '/services/artisan.svc/courses/package/' + course.id + '?preview=true&deploy=false',
                jsonData: course,
                success: function (result) {
                    Ext.Msg.hide();

                    var w = 800;
                    var h = 600;
                    if (window.screen) {
                        w = window.screen.availWidth;
                        h = window.screen.availHeight;
                    }
                    Ext.Msg.show({
                        title: 'Preview Package',
                        msg: 'Your course has been successfully packaged.<br/><br/><a href="#" onclick="javascript:window.open(\'{0}/API/indexAPI.html?cb={3}\', \'preview\',\'width={1},height={2},top=0,left=0\');">Preview it now.</a>'.format(result.data, w, h, (new Date()).getTime()),
                        buttons: Ext.Msg.OK
                    });
                }
            });
		},
		getSelected: function () {
			return this.getSelectionModel().selections;
		}
	});

})();