﻿(function () {
	Symphony.Artisan.CategoryMatchGrid = Ext.define('artisan.categorymatchgrid', {
	    alias: 'widget.artisan.categorymatchgrid',
	    extend: 'symphony.localgrid',
		defaultAnswerText: 'Double click to edit this category',
		initComponent: function () {
			var colModel = new Ext.grid.ColumnModel({
				defaults: {
					align: 'center',
					sortable: false
				},
				columns: [
				new Ext.grid.RowNumberer(),
				{
					itemId: 'category',
					header: 'Category',
					dataIndex: 'text',
                    flex: 1,
					align: 'left',
                    renderer: function(value){
                        var answer = Ext.decode(value);
                        return (answer && answer.category ? answer.category : '');
                    }
				}, {
					itemId: 'matches',
					header: 'Matches',
					dataIndex: 'text',
					align: 'left',
                    flex: 1,
                    renderer: function(value){
                        var answer = Ext.decode(value);
                        return (answer && answer.matches && answer.matches.length>0 ? answer.matches.join(', ') : '');
                    }
				}
                ]
			});
			var me = this;
			Ext.apply(this, {
				enableDragDrop: true,
				ddGroup: 'artisananswers',
				viewConfig: {
				    forceFit: true,
				    plugins: 'gridviewdragdrop'
                },
				colModel: colModel,
				model: 'artisanAnswer',
				listeners: {
				    celldblclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) { // Get the Record
						var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex; // Get field name
				        var columnId = grid.headerCt.getGridColumns()[columnIndex].itemId;
						var value = record.get(fieldName);
                        var answer = Ext.decode(value);
						if (columnId == 'category') {
							Ext.Msg.show({
								title: 'Edit Category',
								width: 600,
								value: (answer && answer.category ? answer.category : ''),
								defaultTextHeight: 120,
								multiline: true,
								prompt: true,
								msg: 'Enter the category title below:',
								fn: function (btn, text) {
									if (btn == 'ok') {
                                        answer.category = text;
										record.set(fieldName, Ext.encode(answer));
										record.commit();
										me.fireEvent('answerschange', me.getAnswers());
									}
								},
								buttons: Ext.Msg.OKCANCEL
							});
						}
                        else if (columnId == 'matches') {
							Ext.Msg.show({
								title: 'Edit Matches',
								width: 600,
								value: (answer && answer.matches && answer.matches.length>0 ? answer.matches.join('\n') : ''),
								defaultTextHeight: 120,
								multiline: true,
								prompt: true,
								msg: 'Enter the list of matches for this category below: (one per line)',
								fn: function (btn, text) {
									// add and commit
									if (btn == 'ok') {
                                        answer.matches = text.trim().split('\n');
										record.set(fieldName, Ext.encode(answer));
										record.commit();
										me.fireEvent('answerschange', me.getAnswers());
									}
								},
								buttons: Ext.Msg.OKCANCEL
							});
						}
					},
					afterrender: {
						single: true,
						fn: function (grid) {
							// allow sorting
							this.requiredDT = new Ext.dd.DropTarget(grid.getView().getEl(), {
								ddGroup: 'artisananswers',
								notifyDrop: function (dd, e, data) {
									var sm = grid.getSelectionModel();
									var rows = sm.getSelections();
									var cindex = dd.getDragData(e).rowIndex;
									if (sm.hasSelection()) {
										for (i = 0; i < rows.length; i++) {
											grid.store.remove(grid.store.getById(rows[i].id));
											grid.store.insert(cindex, rows[i]);
										}
										sm.selectRecords(rows);
									}

									me.refreshUI();
									me.fireEvent('answerschange', me.getAnswers());
								}
							});
						}
					}
				}
			});
			this.callParent(arguments);
		},
		addRow: function () {
			if (!this.autoId) {
				this.autoId = -1;
			}
			this.store.add({
				text: Ext.encode({category:'', matches:[]}),
				isCorrect: true,
				id: this.audoId--
			});
		},
		removeSelectedRow: function () {
			var sm = this.getSelectionModel();
			var rows = sm.getSelections();
			this.store.remove(rows);
			this.refreshUI();
		},
		getAnswers: function () {
			var i = 0,
				result = [];
			this.store.each(function (record) {
				result.push({
					sort: i,
					text: record.get('text'),
					isCorrect: record.get('isCorrect')
				});
				i++;
			});
			return result;
		},
		refreshUI: function () {
			// forces a re-render, which in turn makes sure the row index updates properly
			this.store.each(function (r) {
				r.commit();
			});
		}
	});

})();