﻿(function () {
	Symphony.Artisan.ContentPageEditor = Ext.define('artisan.contentpageeditor', {
	    alias: 'widget.artisan.contentpageeditor',
	    extend: 'Ext.Panel',
		content: '',
		cssText: '',
		// inline css from the template
		theme: null,
		// the theme object
		lastContent: '',
		reset: false,
		page: null,
		node: null,
		initComponent: function () {
		    var me = this;
		    var idEditor = 'contentTinymceEditor' + mceEditorIdGetter();

			Ext.apply(this, {
				layout: 'border',
				border: false,
				defaults: {
					border: false
				},
				listeners: {
					beforedestroy: function (comp) {
						if (me.timer) {
							window.clearInterval(me.timer);
						}
					}
				},
				items: [{
					region: 'north',
					height: 27,
					xtype: 'toolbar',
					items: [
/*{
                        text: 'Insert Asset',
                        iconCls: 'x-button-asset',
                        ref: '../insertAssetButton',
                        handler: Ext.bind(this.onInsertAssetClick, this)
                    },*/
{
						text: 'Revert to Last Save',
						iconCls: 'x-button-revert',
						ref: '../revertButton',
						disabled: true,
						handler: Ext.bind(this.onRevertClick, this)
					}]
				}, {
				    region: 'center',
				    xtype: 'panel',
				    layout: 'border',
                    border: false,
				    items: [{
				        xtype: 'form',
				        region: 'north',
				        frame: true,
				        height: 36,
				        labelWidth: 200,
                        border: false,
				        items: [{
				            xtype: 'numberfield',
				            name: 'minimumPageTime',
                            value: me.node.page.minimumPageTime,
				            fieldLabel: 'Minimum Page Time (s)',
				            help: {
				                title: 'What is minimum page time?',
				                text: 'This is the amount of time in seconds the student must spend on this page. Leave blank to use the setting configured at the learning object or course.'
				            },
				            enableKeyEvents: true,
				            listeners: {
				                keyup: function (input) {
				                    var v = input.getValue();
				                    if (!v) {
				                        delete me.node.page.minimumPageTime;
				                    } else {
				                        me.node.page.minimumPageTime = v;
				                    }
				                }
				            }
				        }]
				    }, {
				        region: 'center',
				        itemId: 'tinymce',
				        id: idEditor,
				        xtype: 'artisan.tinymce',
                        border: false,
				        content: this.content,
				        themeCssPath: this.theme ? this.theme.cssPath : '',
				        skinCssPath: this.theme ? this.theme.skinCssPath : '',
				        cssText: this.cssText,
				        // this applies to template 1 => css: '.element_media img{ height: 171px; max-width: 316px; }',
				        // this applies to template 12 => css: '.element_media img{ height:171px; max-width:316px }',
				        reset: this.reset,
				        listeners: {
				            contentchange: Ext.bind(this.onContentChange, this)
				        }
				    }]
				}]
			});
			this.callParent(arguments);

			//            if (Symphony.Artisan.autoSaveInterval != null){
			//                me.timer = window.setInterval(function(){
			//                    var content = me.findById('contentTinymceEditor').getContent();
			//                    me.onContentChange(content);
			//                },Symphony.Artisan.autoSaveInterval);
			//            }
		},
/*
        onInsertAssetClick: function() {
            new Symphony.Artisan.AssetPicker({
                title: 'Insert Asset',
                width: 900,
                height: 700,
                modal: true,
                listeners: {
                    assetpicked: function(asset) {
                        var html = new Ext.XTemplate(asset.template).apply(asset);
                        this.getComponent('tinymce').insertContent(html);
                    }.createDelegate(this)
                }
            }).show();
        },*/
		onRevertClick: function () {
			this.getComponent('tinymce').setContent(this.page.lastHtml);
			this.setRevertDisabled(true);
		},
		onContentChange: function (content) {
			this.setRevertDisabled(content == this.page.lastHtml);
			this.fireEvent('contentchange', content);
			this.page.html = content;
		},
		setRevertDisabled: function (disable) {
			this.revertButton.setDisabled(disable);
			if (disable) {
				this.node.set('cls', this.node.get('cls').replace(' node-italic', ''));
			} else {
			    if (this.node.get('cls').indexOf('node-italic') == -1) {
			        this.node.set('cls', this.node.get('cls') + ' node-italic');
			    }
			}
		},
		setSaved: function () {
			this.setRevertDisabled(true);
		},
		setTheme: function (theme) {
		    var editor = this.getComponent('tinymce');
		    if (editor) {
		        if (this.theme) {
		            editor.unloadCss(this.theme.cssPath);
		            editor.unloadCss(this.theme.skinCssPath);
		        }
		        editor.loadCss(theme.cssPath);
		        editor.loadCss(theme.skinCssPath);
		    }
			this.theme = theme;
		}
	});

})();

var mceEditorIdGetter = (function () {
	var mceIdCounter = 1;
	return function () { return mceIdCounter += 1; }
})();
