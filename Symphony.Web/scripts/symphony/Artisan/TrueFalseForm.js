﻿(function () {
	Symphony.Artisan.TrueFalseForm = Ext.define('artisan.truefalseform', {
	    alias: 'widget.artisan.truefalseform',
	    extend: 'artisan.questionanswerform',
		singleCorrect: true,
		answersEditable: false,
		initComponent: function () {
			if (!this.page.answers || this.page.answers.length == 0) {
				this.page.answers = this.page.lastAnswers = [{
					id: -1,
					text: 'True',
					isCorrect: false
				}, {
					id: -2,
					text: 'False',
					isCorrect: false
				}];
			}
			this.callParent(arguments);
		}
	});

})();