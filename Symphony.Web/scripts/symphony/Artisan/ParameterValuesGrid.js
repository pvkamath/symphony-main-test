﻿(function () {
    Symphony.Artisan.ParameterValuesGrid = Ext.define('artisan.parametervaluesgrid', {
        alias: 'widget.artisan.parametervaluesgrid',
        extend: 'symphony.searchablelocalgrid',
        parameter: null,
        parameterSet: null,
        
        initComponent: function () {
            var me = this;
            var id = me.parameter ? me.parameter.id : 0;

            me.ParameterValue = Ext.data.Record.create(Symphony.Definitions.parameterValue);

            var tbar = {
                items: [{
                    iconCls: 'x-button-add',
                    text: 'Add',
                    xtype: 'button',
                    handler: function () {
                        me.updateParameterValue();
                    }
                }]
            };

            var columns = [{
                /*id: 'value',*/
                header: 'Value',
                dataIndex: 'value',
                flex: 1
            }, {
                /*id: 'parameterSetName',*/
                dataIndex: 'parameterSetName',
                header: 'Parameter Set'
            }, {
                /*id: 'parameterSetOptionValue',*/
                dataIndex: 'parameterSetOptionValue',
                header: 'Parameter Set Option'
            }]

            if (!me.readOnly) {
                columns = [{
                    width: 25,
                    hideable: false,
                    /*id: 'edit',*/
                    renderer: function () {
                        return '<img class="edit-parameter" src="/images/pencil.png" ext:qtip="Edit Parameter Value"></img>';
                    }
                }].concat(columns)
                .concat([{ /*id: 'delete',*/ header: ' ', hideable: false, renderer: Symphony.deleteRenderer, width: 28 }]);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: columns
            });

            Ext.apply(this, {
                idProperty: 'id',
                hideHeaders: false,
                colModel: colModel,
                recordDef: me.ParameterValue,
                title: 'Values',
                data: { data: [] },
                pager: {},
                remoteSort: false,
                viewConfig: {
                    forceFit: true
                },
                sortInfo: { field: 'parameterSetOptionValue', direction: "ASC" },
                searchColumns: ['value', 'parameterSetOptionValue'],
                tbar: !me.readOnly ? tbar : {},
                listeners: {
                    render: function (grid) {
                        if (me.readOnly) {
                            grid.getSelectionModel().lock();
                        }
                    },
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var column = grid.getColumnModel().getColumnAt(columnIndex);
                        var record = grid.store.getAt(rowIndex);
                        if (column.id === 'edit') {
                            me.updateParameterValue(record.get('id'), record);
                        } else if (column.id === 'delete') {
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this value?', function (btn) {
                                if (btn == 'yes') {
                                    grid.store.removeAt(rowIndex);
                                    me.fireEvent('deleteparameter', me);
                                }
                            });                           
                        }
                    }
                }
            });

            this.callParent(arguments);
        },
        setParameterSet: function(parameterSet) {
            this.parameterSet = parameterSet;
        },
        updateParameterValue: function (id, record) {
            var me = this;

            if (!me.parameterSet) {
                Ext.Msg.alert('Select Parameter Set', 'You must select a parameter set first.');
                return;
            }

            var currentValues = me.getStore().getRange(0);
            var parameterSetOptionsInUse = [];

            for (var i = 0; i < currentValues.length; i++) {
                if (!(record && record.get('parameterSetOptionId') == currentValues[i].data.parameterSetOptionId)) {
                    parameterSetOptionsInUse.push(currentValues[i].data.parameterSetOptionId);
                }
            }
            
            var w = new Ext.Window({
                title: record ? 'Edit Value' : 'Add Value',
                width: 600,
                height: 445,
                modal: true,
                tbar: {
                    border: false,
                    items: [{
                        xtype: 'button',
                        iconCls: 'x-button-save',
                        text: 'Save',
                        handler: function () {
                            var selectionModel = w.find('xtype', 'artisan.parametersetoptionspagedgrid')[0].getSelectionModel();
                            var value = w.find('name', 'value')[0].getValue();

                            if (!record) {
                                var selections = selectionModel.getSelections();

                                for (var i = 0; i < selections.length; i++) {
                                    var selected = selections[i];

                                    var parameterValue = new me.ParameterValue({
                                        id: (id ? id : 0),
                                        parameterId: 0, // Set on server
                                        parameterSetOptionId: selected.data.id,
                                        parameterSetOptionValue: selected.data.value,
                                        parameterSetName: me.parameterSet.name,
                                        value: value
                                    });

                                    me.store.add(parameterValue);
                                }
                                me.fireEvent('addparameter', me);
                            } else {
                                var selected = selectionModel.getSelected();
                                record.set('parameterSetOptionId', selected.data.id);
                                record.set('parameterSetOptionValue', selected.data.value);
                                record.set('value', value);
                            }

                            w.close();
                        }
                    }]
                },
                items: [{
                    xtype: 'form',
                    border: false,
                    frame: false,
                    cls: 'x-panel-mc',
                    padding: '5px',
                    flex: 1,
                    items: [{
                        cls: 'x-form-item',
                        text: 'Value',
                        xtype: 'label',
                        style: 'padding: 0px 0px 5px 0px'
                    }, {
                        xtype: 'textarea',
                        name: 'value',
                        anchor: '100%',
                        hideLabel: true,
                        value: record ? record.get('value') : ''
                    }]
                }, {
                    xtype: 'artisan.parametersetoptionspagedgrid',
                    border: false,
                    height: 280,
                    hideHeaders: true,
                    selModel: new Ext.grid.RowSelectionModel({ singleSelect: record ? true : false }),
                    parameterSet: me.parameterSet,
                    parameterSetOptionsInUse: parameterSetOptionsInUse,
                    selected: record ? record.get('parameterSetOptionId') : 0
                }]
            }).show();
        },
        performSearch: function (searchText) {
            if (!this.store) { return; }
            if (searchText) {
                var filter = [{
                    fn: function (record) {
                        for (var i = 0; i < this.searchColumns.length; i++) {
                            if (record.get(this.searchColumns[i]).toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                                return true;
                            }
                        }
                        return false;
                    },
                    scope: this
                }];
                
                this.store.filter(filter);
            } else {
                this.store.clearFilter();
            }
        }
    });

})();