﻿(function () {
    Symphony.Artisan.DeploymentInfoGrid = Ext.define('artisan.deploymentinfogrid', {
        alias: 'widget.artisan.deploymentinfogrid',
        extend: 'symphony.simplegrid',
        running: true,
        currentCount: 0, // number of items processed
        repeatDataLoadCount: 0, //Number of times we load the grid where we are stuck on the same item
        maxRepeatData: 10, // After 10 loads where the total has not changed, assume it has stalled and stop checking.
        reloadTimeout: null,
		initComponent: function () {
			var me = this;
			var url = '/services/artisan.svc/deployment/';


			var colModel = new Ext.grid.ColumnModel({
				defaults: {
					sortable: false,
					renderer: Symphony.Portal.valueRenderer
				},
				columns: [{
					/*id: 'date',*/
					header: 'Date',
					dataIndex: 'date',
					renderer: Symphony.dateTimeRenderer
				}, {
					/*id: 'message',*/
					header: 'Message',
					dataIndex: 'message',
					flex: 1
				}, {
					/*id: 'result',*/
					header: 'Result',
					dataIndex: 'result',
					renderer: Symphony.passFailRenderer
				}]
			});

			Ext.apply(this, {
				deferLoad: true,
				url: url,
				colModel: colModel,
				model: 'artisanDeploymentInfo',
				pageSize: 99999,
				viewConfig: {
                    loadMask: false
				},
				listeners: {
				    render: function () {
				        me.store.on('beforeload', function () {
				            me.store.proxy.url = me.url + me.deploymentId;
				         });
				         me.store.on('load', function () {
				             var lastRecordId = me.store.getCount(),
                                 lastRecord = me.store.getAt(lastRecordId - 1);

				             if (me.running) {
				                 clearTimeout(me.reloadTimeout);

				                 me.reloadTimeout = setTimeout(function () {
				                     me.store.load();
				                 }, 2000);
				             }

				             if (lastRecord.data.currentCount == me.currentCount && !lastRecord.data.queued) {
				                 me.repeatDataLoadCount++;
				             } else {
				                 me.repeatDataLoadCount = 0;
				                 me.currentCount = lastRecord.data.currentCount;
				             }

				             if (me.repeatDataLoadCount >= me.maxRepeatData) {
				                 lastRecordId++;

				                 var lastRecord = new me.recordDef({
				                     id: lastRecordId,
				                     message: 'Stalled',
				                     currentCount: lastRecord.data.currentCount,
				                     totalCount: lastRecord.data.totalCount,
				                     running: false,
                                     result: false
				                 });

				                 me.store.add([lastRecord]);
				             }

				             if ((!lastRecord.data.running || lastRecord.data.totalCount == lastRecord.data.currentCount) && !lastRecord.data.queued) {
                                 me.running = false;
				             }

				             me.getView().focusRow(lastRecordId - 1);

				             me.fireEvent('deploymentUpdate', lastRecord);
				         });
				    }
				}
			});

			this.callParent(arguments);
		},
		startLog: function (deploymentId) {
		    clearTimeout(this.reloadTimeout);
		    this.running = true;
		    this.repeatDataLoadCount = 0;
		    this.deploymentId = deploymentId;
		    this.store.removeAll();
		    this.store.proxy.api.read = this.url + this.deploymentId
		    this.store.load();
		}
	});

})();