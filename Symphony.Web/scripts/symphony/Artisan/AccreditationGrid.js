﻿/**
 * Displays an editable grid of accreditations, with an expandable disclaimer
 * editor.
 */
Ext.define('Artisan.AccreditationGrid', {
    extend: 'symphony.simplegrid',
    xtype: 'artisan.accreditationgrid',
    requires: [
        'ArtisanCourseAccreditation',
        'AccreditationStatus',
        'Ext.form.FieldContainer',
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.grid.column.Date',
        'Ext.grid.column.Number',
        'Ext.grid.plugin.RowEditing'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],
    plugins: [{
        pluginId: 'rowediting',
        ptype: 'rowediting',
        errorSummary: false
    }],

    /**
     * {Number} The id of the artisan course to retrieve accreditations for,
     * when the gridType is set to course.
     */
    courseId: null,

    /**
     * {Number} The id of the training program to retrieve accreditations for,
     * when the gridType is set to trainingProgram.
     */
    trainingProgramId: null,

    /**
     * {Number} The id of the accreditation board to retrieve accreditations for,
     * when the gridType is set to boardCourse or boardTrainingProgram.
     */
    accreditationBoardId: null,

    /**
     * {String} When the gridType is set to course, this url will be used to
     * retrieve the artisan course accreditations.
     */
    courseUrlFormat: '/services/Accreditation.svc/accreditations/artisan/{0}',
    
    /**
     * {String} When the gridType is set to trainingProgram, this url will be
     * used to retrieve the training program accreditations.
     */
    trainingProgramUrlFormat: '/services/Accreditation.svc/accreditations/trainingProgram/{0}',

    /**
     * {String} When the gridType is set to boardCoruse, this url will be used to
     * retrieve the artisan course accreditations associated with a board.
     */
    accreditationBoardCourseUrlFormat: '/services/Accreditation.svc/accreditationBoards/{0}/artisanAccreditations/',

    /**
     * {String} When the gridType is set to boardTrainingProgram, this url will
     * be used to retrieve the training program accreditations associated with a
     * board.
     */
    accreditationBoardTrainingProgramUrlFormat: '/services/Accreditation.svc/accreditationBoards/{0}/trainingProgramAccreditations/',

    /**
     * {String} Either "course", "trainingProgram", "boardCourse" or "boardTrainingProgram".
     */
    gridType: 'course',

    deferLoad: true,
    remoteFilter: false,
    viewConfig: {
        autoScroll: false,
        overflowY: 'scroll'
    },

    listeners: {
        selectionchange: function() {
            this.updateDisclaimerBtnState();
        }
    },

    columns: [{
        xtype: 'actioncolumn',
        width: 48,
        cls: 'action-column-spacer',
        items: [{
            icon: '/images/bin.png',
            tooltip: 'Delete the accreditation.',
            getClass: function() {
                // Does nothing except add spacing, very hard to do here without
                // a CSS class since action col items have almost no config
                // options.
                return 'x-action-col-icon-margin';
            },
            handler: function(grid, row, col) {
                var store = grid.getStore(),
                    record = store.getAt(row);

                // Don't bother to confirm if the record hasn't been saved to
                // the server yet.
                if (record.phantom) {
                    record.set('isDeleted', true);
                    store.remove(record);
                    return;
                }

                Ext.Msg.show({
                    title: 'Confirm Removal',
                    msg: 'Remove and archive this accreditation? This will become permanent the next time you save this course.',
                    buttons: Ext.Msg.OKCANCEL,
                    fn: function(choice) {
                        if (choice != 'ok') {
                            return;
                        }

                        record.set('isDeleted', true);
                        store.remove(record);
                    }
                });
            }
        }, {
            icon: Ext.BLANK_IMAGE_URL,
            //tooltip: 'This accreditation does not have a disclaimer. Click the button above to add one.',
            getTip: function(v, meta, record) {
                var me = this,
                    status = record.get('status'),
                    expiryDate = record.get('expiryDate');

                if (status == AccreditationStatus.Pending) {
                    return 'This accreditation is pending. It will not be included in any course packages.';
                } else if (status == AccreditationStatus.Expired) {
                    return 'This accreditation has expired. It will not be included in any course packages.';
                }
                if (new Date().getTime() > expiryDate.getTime()) {
                    return 'This accreditation has expired. It will not be included in any course packages.';
                }
                if (record.get('disclaimer')) {
                    return 'This accreditation does not use the default disclaimer for its accreditation board. Click the button above to modify this.';
                }
                
                return '';
            },
            getClass: function(v, meta, record, rowIdx, colIdx, store, view) {
                var me = this,
                    status = record.get('status'),
                    expiryDate = record.get('expiryDate');

                if (record instanceof TrainingProgramAccreditation) {
                    return 'x-action-col-icon-hidden';
                }

                if (status == AccreditationStatus.Pending) {
                    return 'x-action-col-icon-danger';
                } else if (status == AccreditationStatus.Expired) {
                    return 'x-action-col-icon-danger';
                }

                if (new Date().getTime() > expiryDate.getTime()) {
                    return 'x-action-col-icon-danger';
                }

                // Shows a warning if an accreditation has a non default
                // disclaimer. This will allow users to see at a glance which
                // ones are non-standard.
                if (record.get('disclaimer')) {
                    return 'x-action-col-icon-warning';
                }

                return 'x-action-col-icon-hidden';
            }
        }]
    }, {
        dataIndex: 'accreditationBoardId',
        text: 'Board',
        flex: 1,
        minWidth: 240,

        editor: {
            xtype: 'combo',
            valueField: 'id',
            displayField: 'name',
            lastQuery: '', // prevents double refresh on initial load
            queryParam: 'searchText',
            pageSize: 25,
            allowOnlyWhitespace: false,
            forceSelection: true,

            store: {
                model: 'AccreditationBoard',
                proxy: {
                    type: 'rest',
                    url: '/services/Accreditation.svc/accreditationBoards/',
                    reader: {
                        type: 'json',
                        root: 'data'
                    }
                }
            }
        },

        renderer: function(value, cell, record, row, col, store, view) {
            var me = this,
                editor = view.ownerCt.columns[1].getEditor(),
                store = editor.getStore(),
                model;

            // Force store to load but we only want to do it once, otherwise it
            // will load N times.
            if (!store._loaded) {
                store._loaded = true;
                store.load(function() {
                    view.refresh();
                });
            } else {
                if (value) {
                    return store.getById(value).get('name');
                }
            }
        }
    }, {
        dataIndex: 'professionId',
        text: 'Profession',
        flex: 1,
        minWidth: 240,
        hidden: true,

        editor: {
            xtype: 'combo',
            valueField: 'id',
            displayField: 'name',
            lastQuery: '', // prevents double refresh on initial load
            queryParam: 'searchText',
            pageSize: 25,
            allowOnlyWhitespace: true,
            forceSelection: true,

            store: {
                model: 'profession',
                proxy: {
                    type: 'rest',
                    url: '/services/license.svc/profession/',
                    reader: {
                        type: 'json',
                        root: 'data'
                    }
                }
            }
        },
        
        renderer: function(value, cell, record, row, col, store, view) {
            var me = this,
                editor = view.ownerCt.columns[2].getEditor(),
                store = editor.getStore(),
                model;

            store.on('load', function() {
                var found = false;
                store.each(function(item) {
                    if (item.data.id == null) {
                        found = item;
                    }
                });

                if (found == false) {
                    store.insert(0, { id: null, name: 'Any' });
                } else {
                    store.remove(found);
                    store.insert(0, found);
                }
            });

            // Force store to load but we only want to do it once, otherwise it
            // will load N times.
            if (!store._loaded) {
                store._loaded = true;
                store.load(function() {
                    view.refresh();
                });
            } else {
                if (value) {
                    return store.getById(value).get('name');
                } else {
                    return 'Any';
                }
            }
        }
    }, {
        dataIndex: 'courseName',
        text: 'Course',
        flex: 1,
        minWidth: 140
    }, {
        dataIndex: 'status',
        text: 'Status',
        width: 80,
        editor: {
            xtype: 'combo',
            itemId: 'status-field',
            valueField: 'id',
            displayField: 'name',
            allowOnlyWhitespace: false,
            editable: false,
            queryMode: 'local',

            store: {
                fields: [
                    {type: 'int', name: 'id'},
                    {type: 'string', name: 'name'}
                ],
                data: [
                    {id: 1, name: 'Pending'},
                    {id: 2, name: 'Accredited'},
                    {id: 3, name: 'Expired'}
                ]
            }
        },
        renderer: function(value) {
            return AccreditationStatus.getDisplayValue(value);
        }
    }, {
        dataIndex: 'accreditationCode',
        text: 'Approval Number',
        width: 110,
        editor: {
            xtype: 'textfield',
            maxLength: 32,
            validator: function() {
                var root = this.ownerCt,
                    statusField = root.queryById('status-field'),
                    status = statusField.getValue(),
                    str;

                if (status != AccreditationStatus.Pending) {
                    str = Ext.String.trim(this.getValue());
                    if (str.length < 1) {
                        return 'Acceditations that are not pending require an approval number.';
                    }
                }

                return true;
            }
        }
    }, {
        dataIndex: 'courseNumber',
        text: 'Course Number',
        width: 110,
        editor: {
            xtype: 'textfield',
            maxLength: 32
        }
    }, {
        xtype: 'datecolumn',
        dataIndex: 'startDate_date',
        text: 'Start Date',
        format: 'M d/Y',
        width: 86,
        editor: {
            xtype: 'datefield',
            listeners: {
                change: function(field, value) {
                    var me = this,
                        grid = me.ownerCt.context.grid,
                        record = me.ownerCt.context.record,
                        datePart = Ext.Date.format(value, 'M d/Y'),
                        timePart = Ext.Date.format(record.get('startDate'), 'g:i A');

                    if (!timePart) {
                        var date = new Date(),
                            editor = grid.columns[8].getEditor();
                        
                        editor.suspendEvent('change');
                        editor.setValue(date);

                        record.set('startDate', date);
                        timePart = Ext.Date.format(record.get('startDate'), 'g:i A');
                    }

                    record.set('startDate', Ext.Date.parse(datePart + ' ' + timePart, 'M d/Y g:i A'));

                    // Marks it as dirty for red triangle
                    record.modified.startDate_date = datePart;
                    if (editor) {
                        editor.resumeEvent('change');
                    }
                }
            },

            validator: function() {
                if (!this.ownerCt.context) {
                    return true;
                }

                var root = this.ownerCt,
                    record = root.context.record,
                    status = root.queryById('status-field').getValue(),
                    startDate = record.get('startDate'),
                    expiryDate = record.get('expiryDate');

                if (status != AccreditationStatus.Pending) {
                    if (!startDate) {
                        return 'Acceditations that are not pending require a start date.';
                    }
                }

                if (startDate && expiryDate) {
                    var startDateOnly = Ext.Date.clearTime(startDate, true),
                        expiryDateOnly = Ext.Date.clearTime(expiryDate, true);

                    if (startDateOnly.getTime() > expiryDateOnly.getTime()) {
                        return 'The start date must be before the expiry date.';
                    }
                }

                return true;
            }
        }
    }, {
        xtype: 'datecolumn',
        dataIndex: 'startDate_time',
        text: 'Start Time',
        format: 'g:i A',
        width: 80,
        editor: {
            xtype: 'timefield',
            listeners: {
                change: function(field, value) {
                    var me = this,
                        grid = me.ownerCt.context.grid,
                        record = me.ownerCt.context.record,
                        datePart = Ext.Date.format(record.get('startDate'), 'M d/Y'),
                        timePart = Ext.Date.format(value, 'g:i A');

                    if (!datePart) {
                        var date = new Date(),
                            editor = grid.columns[7].getEditor();
                        
                        editor.suspendEvent('change');
                        editor.setValue(date);

                        record.set('startDate', date);
                        datePart = Ext.Date.format(record.get('startDate'), 'M d/Y');
                    }

                    record.set('startDate', Ext.Date.parse(datePart + ' ' + timePart, 'M d/Y g:i A'));

                    // Marks it as dirty for red triangle
                    record.modified.startDate_time = timePart;
                    if (editor) {
                        editor.resumeEvent('change');
                    }
                }
            },

            validator: function() {
                if (!this.ownerCt.context) {
                    return true;
                }

                var root = this.ownerCt,
                    record = root.context.record,
                    status = root.queryById('status-field').getValue(),
                    startDate = record.get('startDate'),
                    expiryDate = record.get('expiryDate');

                if (status != AccreditationStatus.Pending) {
                    if (!startDate) {
                        return 'Acceditations that are not pending require a start time.';
                    }
                }

                if (startDate && expiryDate) {
                    var startDateOnly = Ext.Date.clearTime(startDate, true),
                        expiryDateOnly = Ext.Date.clearTime(expiryDate, true);

                    // We show a date error if the date is off by a day or more
                    // and we show a time error if it's by less than a day.
                    if (startDateOnly.getTime() == expiryDateOnly.getTime() && startDate.getTime() > expiryDate.getTime()) {
                        return 'The start time must be before the expiry time.';
                    }
                }
                return true;
            }
        }
    }, {
        xtype: 'datecolumn',
        dataIndex: 'expiryDate_date',
        text: 'Expiry Date',
        format: 'M d/Y',
        width: 86,
        editor: {
            xtype: 'datefield',
            listeners: {
                change: function(field, value) {
                    var me = this,
                        grid = me.ownerCt.context.grid,
                        record = me.ownerCt.context.record,
                        datePart = Ext.Date.format(value, 'M d/Y'),
                        timePart = Ext.Date.format(record.get('expiryDate'), 'g:i A');

                    if (!timePart) {
                        var date = new Date(),
                            editor = grid.columns[10].getEditor();
                        
                        editor.suspendEvent('change');
                        editor.setValue(date);

                        record.set('expiryDate', date);
                        timePart = Ext.Date.format(record.get('expiryDate'), 'g:i A');
                    }

                    record.set('expiryDate', Ext.Date.parse(datePart + ' ' + timePart, 'M d/Y g:i A'));

                    // Marks it as dirty for red triangle
                    record.modified.expiryDate_date = datePart;
                    if (editor) {
                        editor.resumeEvent('change');
                    }
                }
            },

            validator: function() {
                if (!this.ownerCt.context) {
                    return true;
                }

                var root = this.ownerCt,
                    record = root.context.record,
                    status = root.queryById('status-field').getValue(),
                    startDate = record.get('startDate'),
                    expiryDate = record.get('expiryDate');

                if (status != AccreditationStatus.Pending) {
                    if (!expiryDate) {
                        return 'Acceditations that are not pending require an expiry date.';
                    }
                }

                if (startDate && expiryDate) {
                    var startDateOnly = Ext.Date.clearTime(startDate, true),
                        expiryDateOnly = Ext.Date.clearTime(expiryDate, true);

                    if (startDateOnly.getTime() > expiryDateOnly.getTime()) {
                        return 'The expiry date must come after the start date.';
                    }
                }

                return true;
            }
        }
    }, {
        xtype: 'datecolumn',
        dataIndex: 'expiryDate_time',
        text: 'Expiry Time',
        format: 'g:i A',
        width: 80,
        editor: {
            xtype: 'timefield',
            listeners: {
                change: function(field, value) {
                    var me = this,
                        record = me.ownerCt.context.record,
                        grid = me.ownerCt.context.grid,
                        datePart = Ext.Date.format(record.get('expiryDate'), 'M d/Y'),
                        timePart = Ext.Date.format(value, 'g:i A');

                    if (!datePart) {
                        var date = new Date(),
                            editor = grid.columns[9].getEditor();
                        
                        editor.suspendEvent('change');
                        editor.setValue(date);

                        record.set('expiryDate', date);
                        datePart = Ext.Date.format(record.get('expiryDate'), 'M d/Y');
                    }

                    record.set('expiryDate', Ext.Date.parse(datePart + ' ' + timePart, 'M d/Y g:i A'));

                    // Marks it as dirty for red triangle
                    record.modified.expiryDate_time = timePart;
                    if (editor) {
                        editor.resumeEvent('change');
                    }
                }
            },

            validator: function() {
                if (!this.ownerCt.context) {
                    return true;
                }

                var root = this.ownerCt,
                    record = root.context.record,
                    status = root.queryById('status-field').getValue(),
                    startDate = record.get('startDate'),
                    expiryDate = record.get('expiryDate');

                if (status != AccreditationStatus.Pending) {
                    if (!expiryDate) {
                        return 'Acceditations that are not pending require an expiry time.';
                    }
                }

                if (startDate && expiryDate) {
                    var startDateOnly = Ext.Date.clearTime(startDate, true),
                        expiryDateOnly = Ext.Date.clearTime(expiryDate, true);

                    // We show a date error if the date is off by a day or more
                    // and we show a time error if it's by less than a day.
                    if (startDateOnly.getTime() == expiryDateOnly.getTime() && startDate.getTime() > expiryDate.getTime()) {
                        return 'The start time must be before the expiry time.';
                    }
                }
                return true;
            }
        }
    }, {
        xtype: 'numbercolumn',
        dataIndex: 'creditHours',
        text: 'Credit Hours',
        format: '0.0',
        width: 80,
        editor: {
            xtype: 'numberfield',
            step: 0.5,
            decimalPrecision: 1,
            allowExponential: false,
            minValue: 0,
            maxValue: 120,

            validator: function() {
                if (!this.ownerCt.context) {
                    return true;
                
                }
                var root = this.ownerCt,
                    status = root.queryById('status-field').getValue(),
                    value;

                if (status != AccreditationStatus.Pending) {
                    value = this.getValue();
                    if (!value && value != 0) {
                        return 'Acceditations that are not pending require the number of credit hours.';
                    }
                }

                return true;
            }
        }
    }, {
        dataIndex: 'creditHoursLabel',
        text: 'Credit Label',
        width: 120,
        editor: {
            xtype: 'textfield',
            maxLength: 32,

            validator: function() {
                var root = this.ownerCt,
                    statusField = root.queryById('status-field'),
                    status = statusField.getValue(),
                    value;

                if (status != AccreditationStatus.Pending) {
                    value = Ext.String.trim(this.getValue());
                    if (value.length < 1) {
                        return 'Acceditations that are not pending require a label for credit hours.';
                    }
                }

                return true;
            }
        }
    }],

    tbar: {
        itemId: 'toolbar',
        items: [{
            text: 'Add',
            itemId: 'add-button',
            iconCls: 'x-button-add',
            handler: function() {
                var root = this.up('[xtype=artisan.accreditationgrid]');

                root.addGridItem();
            }
        }, {
            text: 'Edit Disclaimer',
            itemId: 'disclaimer-button',
            iconCls: 'x-button-add',
            handler: function() {
                var root = this.up('[xtype=artisan.accreditationgrid]');

                root.showDisclaimerEditor();
            }
        }, {
            text: 'Save',
            itemId: 'save-button',
            iconCls: 'x-button-save',
            hidden: true,
            handler: function() {
                var root = this.up('[xtype=artisan.accreditationgrid]');

                root.hideDisclaimerEditor(true);
            }
        }, {
            text: 'Cancel',
            itemId: 'cancel-button',
            iconCls: 'x-button-cancel',
            hidden: true,
            handler: function() {
                var root = this.up('[xtype=artisan.accreditationgrid]');

                root.hideDisclaimerEditor(false);
            }
        }, {
            text: 'Use Default',
            itemId: 'use-default-button',
            iconCls: 'x-button-lock',
            hidden: true,
            handler: function() {
                var root = this.up('[xtype=artisan.accreditationgrid]');

                root.enforceDefaultDisclaimer(true);
            }
        }, {
            text: 'Allow Manual',
            itemId: 'allow-manual-button',
            iconCls: 'x-button-unlock',
            hidden: true,
            handler: function() {
                var root = this.up('[xtype=artisan.accreditationgrid]');

                root.enforceDefaultDisclaimer(false);
            }
        }]
    },

    initComponent: function() {
        var me = this,
            plugin;

        if (me.gridType == 'course') {
            me.model = 'ArtisanCourseAccreditation',
            me.url = Ext.String.format(me.courseUrlFormat, me.courseId);

            Log.debug(Ext.String.format('Configuring proxy with url: {0}', me.url));
        } else if (me.gridType == 'trainingProgram') {
            me.model = 'TrainingProgramAccreditation',
            me.url = Ext.String.format(me.trainingProgramUrlFormat, me.trainingProgramId);

            Log.debug(Ext.String.format('Configuring proxy with url: {0}', me.url));
        } else if (me.gridType =='boardCourse') {
            me.model = 'ArtisanCourseAccreditation',
            me.url = Ext.String.format(me.accreditationBoardCourseUrlFormat, me.accreditationBoardId);

            Log.debug(Ext.String.format('Configuring proxy with url: {0}', me.url));
        } else if (me.gridType = 'boardTrainingProgram') {
            me.model = 'TrainingProgramAccreditation',
            me.url = Ext.String.format(me.accreditationBoardTrainingProgramUrlFormat, me.accreditationBoardId);

            Log.debug(Ext.String.format('Configuring proxy with url: {0}', me.url));
        } else {
            Log.error('No valid grid type specified.');
        }

        me.callParent();

        plugin = me.getPlugin('rowediting');

        if (me.gridType == 'course' || me.gridType =='trainingProgram') {
            me.down('[xtype=pagingtoolbar]').hide();

            me.updateDisclaimerBtnState();

            plugin.on('beforeedit', me.onRowEditingBeforeEdit, me);
            plugin.on('canceledit', me.onRowEditingCancelEdit, me);
            plugin.on('edit', me.onRowEditingEdit, me);
            plugin.on('validateedit', me.onRowEditingValidate, me);

            me.columns[3].hide();

            if (me.trainingProgramId) {
                me.columns[0].setWidth(24);
            }
        } else {
            me.queryById('toolbar').hide();
            me.columns[0].hide();
            me.columns[1].hide();

            if (me.gridType == 'boardCourse') {
                me.columns[3].text = 'Course Name';
                me.columns[3].dataIndex = 'courseName';

                plugin.on('beforeedit', function(plugin, context) {
                    var app = me.up('[xtype=artisan.app]'),
                        tabPanel = app.down('[xtype=artisan.coursestab]');
                    
                    app.activate(0);
                    tabPanel.addPanel(context.record.get('artisanCourseId'), context.record.get('courseName'));

                    return false;
                });
            } else if (me.gridType == 'boardTrainingProgram') {
                me.columns[3].text = 'Training Program Name';
                me.columns[3].dataIndex = 'trainingProgramName';

                plugin.on('beforeedit', function(plugin, context) {
                    // todo maybe: onclick, open training program.

                    return false;
                });
            }
        }
    },

    updateUrlId: function(newId) {
        var me = this,
            proxy = me.getStore().getProxy();

        if (me.gridType == 'course') {
            me.courseId = newId;
            proxy.url = Ext.String.format(me.courseUrlFormat, newId);
            proxy.api.read = Ext.String.format(me.courseUrlFormat, newId);
        } else if (me.gridType == 'trainingProgram') {
            me.trainingProgramId = newId;
            proxy.url = Ext.String.format(me.trainingProgramUrlFormat, newId);
            proxy.api.read = Ext.String.format(me.trainingProgramUrlFormat, newId);
        } else if (me.gridType == 'boardCourse') {
            me.accreditationBoardId = newId;
            proxy.url = Ext.String.format(me.accreditationBoardCourseUrlFormat, newId);
            proxy.api.read = Ext.String.format(me.accreditationBoardCourseUrlFormat, newId);
        } else {
            me.accreditationBoardId = newId;
            proxy.url = Ext.String.format(me.accreditationBoardTrainingProgramUrlFormat, newId);
            proxy.api.read = Ext.String.format(me.accreditationBoardTrainingProgramUrlFormat, newId);
        }
    },

    /**
     * Updates the state of the disclaimer button. It should only be enabled
     * if a grid item is selected.
     * @param {Boolean} forceDisable Forces the button to be disabled. Used when
     * editing a row because technically in the "beforeedit" event, the plugin
     * is not "editing" the row yet. This allows us to force this behavior.
     */
    updateDisclaimerBtnState: function(forceDisable) {
        var me = this,
            plugin = me.getPlugin('rowediting'),
            disclaimerBtn = me.queryById('disclaimer-button'),
            selection = me.getSelectionModel();

        if (forceDisable || plugin.editing || selection.getCount() != 1) {
            Log.debug('Disabling the disclaimer button.');
            disclaimerBtn.disable();
        } else {
            Log.debug('Enabling the disclaimer button.');
            disclaimerBtn.enable();
        }
    },

    /**
     * Adds a new item to the grid.
     */
    addGridItem: function() {
        var me = this,
            plugin = me.getPlugin('rowediting'),
            clazz = Ext.ClassManager.get(me.model),
            store,
            model;

        Log.debug('Adding a new grid item.');

        model = new clazz({
            status: AccreditationStatus.Pending,
            creditHoursLabel: 'Credit Hours'
        });

        store = me.getStore();
        store.add(model);

        Log.debug('Added model to store.', model);

        plugin.startEdit(model, 0);
    },

    /**
     * Shows the disclaimer editor for the currently selected grid item. Hides
     * the grid temporarily and shows a WYSIWYG editor to edit the disclaimer.
     */
    showDisclaimerEditor: function() {
        var me = this,
            selection = me.getSelectionModel(),
            model = selection.selected.getAt(0),
            disclaimer,
            editor;

        Log.debug('Showing disclaimer editor for selection: ', selection);

        me.queryById('add-button').hide();
        me.queryById('disclaimer-button').hide();
        me.queryById('cancel-button').show();
        me.queryById('save-button').show();

        // width height are not too important as the grid panel's layout type is
        // "fit", so it will expand after we hide the table view.
        me._disclaimerPanel = new Ext.container.Container({
            frame: true,
            x: me.getX(),
            y: me.getY(),
            width: me.getWidth(),
            height: me.getHeight(),
            layout: 'fit',
            model: selection.selected.getAt(0),

            items: [{
                xtype: 'symphony.spellcheckarea',
                itemId: 'disclaimer-editor'
            }]
        });

        me.add(me._disclaimerPanel);
        me.headerCt.hide();
        me.getView().hide();

        editor = me._disclaimerPanel.queryById('disclaimer-editor');
        if (model.get('disclaimer') == null) {
            me.queryById('allow-manual-button').show();
            editor.disable();

            disclaimer = model._defaultDisclaimer;
        } else {
            me.queryById('use-default-button').show();
            editor.enable();

            disclaimer = model.get('disclaimer');
        }

        editor.setValue(disclaimer);
    },

    /**
     * Hides the current disclaimer editor and returns to the grid view. If set
     * to save, will commit the data back to the item in the grid.
     * @param {Boolean} commit Whether or not to commit the changes to the model.
     */
    hideDisclaimerEditor: function(commit) {
        var me = this,
            editor = me._disclaimerPanel.queryById('disclaimer-editor');

        Log.debug('Hiding disclaimer editor.');

        me.queryById('add-button').show();
        me.queryById('disclaimer-button').show();
        me.queryById('cancel-button').hide();
        me.queryById('save-button').hide();
        me.queryById('use-default-button').hide();
        me.queryById('allow-manual-button').hide();

        if (commit) {
            // private property of components
            if (!editor.disabled) {
                Log.debug('Committing disclaimer editor changes.');

                value = editor.getValue();
                me._disclaimerPanel.model.set('disclaimer', value);
            } else {
                Log.debug('No changes to commit, using default disclaimer.');

                me._disclaimerPanel.model.set('disclaimer', null);
            }
        } else {
            Log.debug('Abandoning disclaimer editor changes.');
        }

        me.remove(me._disclaimerPanel);
        me.headerCt.show();
        me.getView().show();

        me._disclaimerPanel.destroy();
        editor.destroy();

        delete me._disclaimerPanel;
    },

    /**
     * Sets whether or not to allow manually entering a disclaimer for the
     * accreditation.
     * @param {Boolean} enforce Whether or not to enforce the default disclaimer
     * for the accreditation's parent accreditation board.
     */
    enforceDefaultDisclaimer: function(enforce) {
        var me = this,
            editor = me._disclaimerPanel.queryById('disclaimer-editor'),
            defaultButton = me.queryById('use-default-button'),
            manualButton = me.queryById('allow-manual-button');

        if (enforce) {
            Log.debug('Enforcing default disclaimer on accreditation, disabling input.');

            defaultButton.hide();
            manualButton.show();

            editor.disable();
            editor.setValue(me._disclaimerPanel.model._defaultDisclaimer);
        } else {
            Log.debug('Allowing manual disclaiemr entry on accreditation, enabling input.');

            defaultButton.show();
            manualButton.hide();

            editor.enable();
        }
    },

    /**
     * Fires before a row is edited. Caches current date fields and adds an
     * intercept to disable the row jumping behavior where you can double click
     * on a row to edit it while you're already editing a row.
     */
    onRowEditingBeforeEdit: function(editor, context) {
        var me = this,
            plugin = me.getPlugin('rowediting');

        if (plugin.editing) {
            Log.debug('Tried to edit a row but a row is already being edited.', context);
            return false;
        }

        // Because of the misc hackery we're doing here, we end up committing to
        // the model directly when dealing with these dates. We store them in
        // advance so we can restore them if we cancel.
        context.record._oldStartDate = context.record.get('startDate');
        context.record._oldExpiryDate = context.record.get('expiryDate');

        me.updateDisclaimerBtnState(true);
    },

    /**
     * Fires after a row edit is canceled. Restores the dates and also removes
     * phantom records. Succinctly, if you click "add" to add a new record and
     * then click cancel, the record will be removed from the grid.
     */
    onRowEditingCancelEdit: function(editor, context) {
        var me = this,
            plugin = me.getPlugin('rowediting'),
            store = context.grid.getStore();

        // See below for _phantomCommit.
        if (context.record.phantom && !context.record._phantomCommit) {
            Log.debug('Canceled edit with a phantom model, removing.');
            store.remove(context.record);
        }

        // As mentioned above, we restore the dates on a cancel.
        context.record.set('startDate', context.record._oldStartDate);
        context.record.set('expiryDate', context.record._oldExpiryDate);
    },

    /**
     * Fires after a row has been edited. Marks the row as committed so phantom
     * models aren't removed in the future. ie: The user hit "OK" on the model
     * at least once, so don't remove it automatically when canceling an edit.
     */
    onRowEditingEdit: function(editor, context) {
        var me = this,
            plugin = me.getPlugin('rowediting');

        context.record._phantomCommit = true;

        me.updateDisclaimerBtnState();
    },

    /**
     * Fires before an edit to a row has been confirmed. Returning false cancels
     * the edit. Method is needed to be sure a user hasn't typed invalid input
     * into a combo box and directly clicked on the "Update" button.
     */
    onRowEditingValidate: function(editor, context) {
        var combo,
            board;

        // Context.newValues is where Ext puts the "potential" values for you to
        // inspect. Not really documented anywhere, :)
        if (!context.newValues.accreditationBoardId || context.newValues.accreditationBoardId < 1) {
            return false;
        }

        combo = context.grid.columns[1].getEditor();
        board = combo.getStore().getById(combo.getValue());

        context.record._defaultDisclaimer = board.get('disclaimer');

        // If the user selected a new accreditation board, set the disclaimer to the new board's
        // disclaimer if a custom disclaimer was not entered.
        if (context.newValues.accreditationBoardId != context.originalValues.accreditationBoardId) {
            Log.info('Change in accreditation board, update may occur to disclaimer.');

            if (!context.record.get('disclaimer')) {
                Log.info('No custom disclaimer, using board default.');

                combo = context.grid.columns[1].getEditor();
                board = combo.getStore().getById(combo.getValue());
            } else {
                Log.info('Custom disclaimer present, overriding board default.');
            }
        } else {
            Log.debug('No change in accreditation board, no update to disclaimer will occur.');
        }

        // Paranoid about setting empty to null
        if (!context.record.get('disclaimer')) {
            context.record.set('disclaimer', null);
        }
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});