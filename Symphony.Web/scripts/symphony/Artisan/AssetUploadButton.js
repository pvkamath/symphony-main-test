﻿(function () {
    Symphony.Artisan.AssetUploadButton = Ext.define('artisan.assetuploadbutton', {
        alias: 'widget.artisan.assetuploadbutton',
        extend: 'Ext.ux.SWFUploadButton',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                text: 'Upload',
                iconCls: 'x-button-add',
                uploadUrl: '/Uploaders/ArtisanAssetUploader.ashx',
                name: 'assetUploader',
                listeners: {
                    'queue': function (file) {
                        this.startUpload();
                    },
                    'queueerror': function () {
                        Ext.Msg.alert('Error', 'The file could not be queued');
                    },
                    'uploadstart': function (item) {
                        this.addParameter('customerId', Symphony.User.customerId);

                        Ext.Msg.progress('Please wait...', 'Please wait while your file is uploaded...');
                    },
                    'uploadprogress': function (item, completed, total) {
                        var ratio = parseFloat(completed / total);
                        Ext.Msg.updateProgress(ratio, parseInt(ratio * 100, 10) + '% complete');
                    },
                    'uploaderror': function () {
                        this.error = true;
                        Ext.Msg.hide();
                        Ext.Msg.alert('Upload failed', 'The upload failed. Please make sure it is within the proper file size limitations.');
                    },
                    'uploadsuccess': function (item, response, hasData) {
                        var result = Ext.decode(response);
                        if (result.success) {
                            Ext.Msg.hide();
                            Symphony.Ajax.request({
                                method: 'GET',
                                url: '/services/artisan.svc/assets/' + result.id,
                                success: function (result) {
                                    me.fireEvent('uploadcomplete', result.data);
                                }
                            });
                        } else {
                            Ext.Msg.hide();
                            Ext.Msg.alert('Upload failed', result.error);
                        }
                    }
                }
            });
            Symphony.Artisan.AssetUploadButton.superclass.initComponent.call(this);
        }
    });

})();