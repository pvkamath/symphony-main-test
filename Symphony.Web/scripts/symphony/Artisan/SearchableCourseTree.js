﻿(function () {

	Symphony.Artisan.SearchableCourseTree = Ext.define('artisan.searchablecoursetree', {
	    alias: 'widget.artisan.searchablecoursetree',
	    extend: 'symphony.searchabletree',
		initComponent: function () {
			Ext.apply(this, {
			    rootVisible: true,
			    sortableColumns: false,
                preventHeader: true
			});

			this.callParent(arguments);
		},
		checkHidden: function (text, node) {
			return node.lastChild && node.lastChild.section.pages && node.ui.ctNode.offsetHeight < 3 && node.hasChildNodes() && this.parentFilter(text, node);
		},
		leafFilter: function (text, node) {
			if (!node.page) {
				return false;
			}

			var re = new RegExp(Ext.escapeRe(text), 'i');
			var html = node.page.html;

			if (node.page.pageType == Symphony.ArtisanPageType.content) {
				html = html.replace(/<\/?[^>]+(>|$)/g, '');
			}

			if (node.page.answers && node.page.pageType == Symphony.ArtisanPageType.question) {
				for (var i = 0; i < node.page.answers.length; i++) {
					if (re.test(node.page.answers[i].text)) {
						return true;
					}
				}
			}

			return re.test(html) || re.test(node.page.name) || re.test(node.page.correctResponse) || re.test(node.page.incorrectResponse);
		},
		parentFilter: function (text, node) {
			return true;
		}
	});


})();