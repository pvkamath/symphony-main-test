﻿(function () {
	Symphony.Artisan.PageOptions = Ext.define('artisan.pageoptions', {
	    alias: 'widget.artisan.pageoptions',
	    extend: 'Ext.form.FormPanel',
		type: '',
		course: null,
		page: null,
		initComponent: function () {
			Ext.apply(this, {
				border: false,
				items: [{
					xtype: 'checkbox',
					fieldLabel: 'Calculate Score',
					name: 'calculateScore',
					ref: 'calculateScore',
					checked: this.page ? this.page.calculateScore : '',
					listeners: {
						check: Ext.bind(this.save, this)
					}
				}]
			});

			this.callParent(arguments);
		},
		load: function () {
			if (this.page.calculateScore) {
				this.calculateScore.setValue(this.page.calculateScore);
			}
		},
		save: function (field) {
			this.page.calculateScore = field.getValue();
		}
	});

})();