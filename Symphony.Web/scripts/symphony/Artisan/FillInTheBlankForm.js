﻿(function () {
    Symphony.Artisan.FillInTheBlankForm = Ext.define('artisan.fillintheblankform', {
        alias: 'widget.artisan.fillintheblankform',
        extend: 'Ext.form.FormPanel',
        content: '',
        lastContent: '',
        reset: false,
        page: null,
        node: null,
        replacementText: '__________',
        initComponent: function () {
            
            var me = this;

            me.questionText = '';
            me.correctText = '';
            me.incorrectText = '';

            Ext.apply(this, {
                frame: true,
                labelAlign: 'top',
                items: [{
                    xtype: 'label',
                    cls: 'x-form-item',
                    html: 'Directions: Enter the question text below, placing the word or words to be filled in by the student in double square brackets, like [[this]].'
                }, {
                    name: 'body',
                    xtype: 'symphony.spellcheckarea',
                    labelAlign: 'top',
                    anchor: '100%',
                    height: 175,
                    enableKeyEvents: true,
                    fieldLabel: 'Question',
                    ref: 'questionEditor',
                    value: me.buildQuestion(me.page.html, me.page.answers, me.page.correctResponse, me.page.incorrectResponse),
                    listeners: {
                        keyup: function (field, e) {
                            me.onContentChanged();
                        }
                    }
                }, {
                    xtype: 'label',
                    cls: 'x-form-item',
                    html: 'Preview:'
                }, {
                    xtype: 'panel',
                    bodyStyle: 'padding: 5px',
                    ref: 'preview',
                    cls: 'contentarea-panel',
                    html: me.previewQuestion(me.page.html)
                }, {
                    xtype: 'symphony.spellcheckarea',
                    fieldLabel: 'Correct Response',
                    value: me.page.correctResponse || 'Correct.',
                    anchor: '100%',
                    ref: 'correctResponse',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function (field, e) {
                            me.onContentChanged();
                        }
                    }
                }, {
                    xtype: 'symphony.spellcheckarea',
                    fieldLabel: 'Incorrect Response',
                    value: me.page.incorrectResponse || 'Incorrect.',
                    anchor: '100%',
                    ref: 'incorrectResponse',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function (field, e) {
                            me.onContentChanged();
                        }
                    }
                }],
                listeners: {
                    beforedestroy: function (comp) {
                        if (me.timer) {
                            window.clearInterval(me.timer);
                        }
                    }
                }
            });
            this.callParent(arguments);

            me.timer = window.setInterval(function () {
                var newQuestionText = me.questionEditor.getValue();
                var newIncorrectText = me.incorrectResponse.getValue();
                var newCorrectText = me.incorrectResponse.getValue();

                var contentChanged = false;

                if (newQuestionText != me.questionText) {
                    me.questionText = newQuestionText;
                    contentChanged = true;
                }
                if (newIncorrectText != me.incorrectText) {
                    me.incorrectText = newIncorrectText;
                    contentChanged = true;
                }
                if (newCorrectText != me.correctText) {
                    me.correctText = newCorrectText;
                    contentChanged = true;
                }

                if (contentChanged) {
                    me.onContentChanged(me.questionEditor);
                }
            }, 1000);
        },
        onContentChanged: function() {
            var question = this.previewQuestion(this.questionEditor.getValue());
            var answers = this.extractAnswers(this.questionEditor.getValue());
            var correctResponse = this.correctResponse.getValue();
            var incorrectResponse = this.incorrectResponse.getValue();
            this.preview.update(question);
            this.fireEvent('contentchange', question, answers, correctResponse, incorrectResponse);
        },
		getValues: function () {
			return {
				question: this.previewQuestion(this.questionEditor.getValue()),
				answers: this.extractAnswers(this.questionEditor.getValue()),
				correctResponse: this.correctResponse.getValue(),
				incorrectResponse: this.incorrectResponse.getValue()
			};
		},
		setValue: function (question, answers, correctResponse, incorrectResponse) {
			this.preview.update(this.previewQuestion(question, answers));
			this.questionEditor.setValue(this.buildQuestion(question, answers));
			this.correctResponse.setValue(correctResponse);
			this.incorrectResponse.setValue(incorrectResponse);
			this.fireEvent('contentchange', question, answers, correctResponse, incorrectResponse);
		},
		buildQuestion: function (text, answers) {
			if (answers) {
				for (var i = 0; i < answers.length; i++) {
					//text = text.replace(/\[\[__+?\]\]/, "[[" + answers[i].text + "]]");
					text = text.replace(this.replacementText, "[[" + answers[i].text + "]]");
				}
			}
			return text;
		},
		previewQuestion: function (text) {
			return text.replace(/\[\[.+?\]\]/g, this.replacementText);
		},
		extractAnswers: function (text) {
			var re = /\[\[(.+?)\]\]/g;
			var result = [];
			var match = null;
			var i = 0;
			while (match = (re.exec(text))) {
				result.push({
					sort: i,
					text: match[1],
					isCorrect: true
				});
				i++;
			}
			return result;
		}
	});

})();