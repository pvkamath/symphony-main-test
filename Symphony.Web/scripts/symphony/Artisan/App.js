﻿(function () {
    if (!Symphony.Artisan) {
        Symphony.Artisan = {};
    }
    Symphony.Artisan.App = Ext.define('artisan.app', {
        alias: 'widget.artisan.app',
        extend: 'Ext.Panel',
        select: function (xtype) {
            this.find('xtype', 'tabpanel')[0].activate(this.find('xtype', xtype)[0]);
            return true;
        },
        initComponent: function () {
            var items = [];
            items.push({
                title: 'Courses',
                tabTip: 'Create and edit training courses',
                xtype: 'artisan.coursestab'
            });
            items.push({
                title: 'Assets',
                tabTip: 'Create and edit assets',
                xtype: 'artisan.assetstab',
                autoPlay: true
            });
            if (Symphony.User.canDeployMultiple && Symphony.User.isArtisanPackager) {
                items.push({
                    title: 'Deployment',
                    tabTip: 'Publish and deploy courses to multiple customers',
                    xtype: 'artisan.deploymenttab'
                });
            }
            if (Symphony.Artisan.canManageParameters()) {
                items.push({
                    title: 'Parameters',
                    tabTip: 'Create and manage dynamic placeholder text',
                    xtype: 'artisan.parametertab',
                    cls: Symphony.Modules.getModuleClass(Symphony.Modules.Parameters),
                    hidden: !Symphony.Modules.hasModule(Symphony.Modules.Parameters)
                });
                items.push({
                    title: 'Parameter Sets',
                    tabTip: 'Create and manage parameter sets',
                    xtype: 'artisan.parametersettab',
                    cls: Symphony.Modules.getModuleClass(Symphony.Modules.Parameters),
                    hidden: !Symphony.Modules.hasModule(Symphony.Modules.Parameters)
                });
            }
            Ext.apply(this, {
                requiresActivation: true,
                // activates the first panel on load of the tab
                border: false,
                defaults: {
                    border: false
                },
                layout: 'fit',
                items: [{
                    xtype: 'tabpanel',
                    //activeItem: 0,
                    items: items
                }]
            });
            this.callParent(arguments);
        },
        // manual activate
        activate: function () {
            //this.find('xtype','artisan.coursestab')[0].activate();
            this.find('xtype', 'tabpanel')[0].activate(this.find('xtype', 'artisan.coursestab')[0]);
        }
    });


    Symphony.Artisan.autoSaveInterval = 2000; // in milliseconds, set null to disable

    Symphony.Artisan.deleteCourseRenderer = function (value, meta, record) {
        return '<a href="#" onclick="Symphony.Artisan.deleteCourse({0});"><img src="/images/bin.png" /></a>'.format(record.get('id'));
    };

    Symphony.Artisan.templateRenderer = function (value, meta, record, row, column, store, view, columnCount) {
        var index = column + (row * columnCount);
        var record = store.getAt(index);
        if (!record) {
            return '';
        }
        var tip = '<b>' + record.data.name + '</b><br/>' + record.data.description;
        tip = tip.replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return '<img src="' + record.data.thumbPath + '" ext:qtip="' + tip + '" />';
    };

    Symphony.Artisan.thumbPathRenderer = function (value, meta, record) {
        return '<img src="' + record.data.thumbPath + '" />';
    };

    Symphony.Artisan.nameDescriptionRenderer = function (value, meta, record) {
        return '<div style="font-weight: bold; white-space: normal;">' + record.data.name + '</div><div style="color: #666; white-space: normal;">' + record.data.description + '</div>';
    };

    Symphony.Artisan.deleteCourse = function (courseId) {
        Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this course?', function (btn) {
            if (btn == 'yes') {
                Symphony.Ajax.request({
                    url: '/services/artisan.svc/courses/delete/' + courseId,
                    success: function () {
                        var app = Symphony.App.find('xtype', 'artisan.app')[0];
                        var tab = app.find('xtype', 'artisan.coursestab')[0];
                        // refresh the grid
                        tab.find('xtype', 'artisan.coursesgrid')[0].refresh();
                        // remove the tab, if it's open
                        Ext.each(tab.find('xtype', 'artisan.courseeditor'), function (form) {
                            if (form.courseId == courseId) {
                                form.ownerCt.remove(form);
                            }
                        });

                        var deploymentTab = app.find('xtype', 'artisan.deploymenttab')[0];
                        if (deploymentTab) {
                            var pGrids = deploymentTab.find('xtype', 'artisan.publishedcoursesgrid');
                            if (pGrids && pGrids.length) {
                                for (var i = 0; i < pGrids.length; i++) {
                                    pGrids[i].refresh();
                                }
                            }
                        }
                    }
                });
            }
        });
    };

    Symphony.Artisan.canManageParameters = function() {
        if (!Symphony.Modules.hasModule(Symphony.Modules.Artisan)) {
            return false;
        }

        return Symphony.User.isArtisanAuthor;
    }
    Symphony.Artisan.canOverrideParameters = function () {
        if (!Symphony.Modules.hasModule(Symphony.Modules.Artisan)) {
            return false;
        }

        var u = Symphony.User;
        return (u.isClassroomInstructor || u.isClassroomManager || u.isClassroomSupervisor ||
                u.isTrainingAdministrator || u.isTrainingManager || u.isArtisanAuthor || u.isArtisanPackager)
    }

    Symphony.Artisan.clearArtisanMode = function (component) {
        Symphony.Artisan.toggleArtisanMode(component, "all", false);
    }

    Symphony.Artisan.toggleArtisanMode = function (component, mode, isEnabled) {
        if (!typeof (component.queryBy) == 'function') {
            return;
        }
        mode = mode.toLowerCase();

        if (mode == "all" && isEnabled) {
            return; // Can't enable all modes
        }

        var selector = mode != "all" ? "~=" + mode : "";

        var enabledForMode = component.query("[enabledForModes" + selector + "]");
        var disabledForMode = component.query("[disabledForModes" + selector + "]");
        var defaultForMode = component.query("[defaultForModes" + selector + "]");

        for (var i = 0; i < enabledForMode.length; i++) {
            if (Ext.isFunction(enabledForMode[i].setDisabled)) {
                enabledForMode[i].setDisabled(!isEnabled);
            }
        }
        for (var i = 0; i < disabledForMode.length; i++) {
            var isReadOnly = false;

            // Check for a default value that might be set on a non checkbox/radio field
            if (disabledForMode[i][mode + "Default"] && Ext.isFunction(disabledForMode[i].setValue)) {
                disabledForMode[i].setValue(disabledForMode[i][mode + "Default"]);
                if (Ext.isFunction(disabledForMode[i].setReadOnly) && Ext.isFunction(disabledForMode[i].addCls)) {
                    disabledForMode[i].removeCls(disabledForMode[i].disabledCls);
                    if (isEnabled) {
                        disabledForMode[i].addCls(disabledForMode[i].disabledCls); // Because it should still look disabled, we just want to ensure we can save the default value
                    }
                    disabledForMode[i].setReadOnly(isEnabled);
                    isReadOnly = true;
                }
            }
            if (Ext.isFunction(disabledForMode[i].setDisabled) && !isReadOnly) {
                disabledForMode[i].setDisabled(isEnabled);
                if (isEnabled && disabledForMode[i].isRadio && disabledForMode[i].getValue()) {
                    disabledForMode[i].setValue(false);
                }
            }
        }

        if (isEnabled) {
            for (var i = 0; i < defaultForMode.length; i++) {
                // TODO: only change the value if it's in the disabledForMode list
                // for now, removed; since we clear the value, it'll just error out and force the user to select a valid entry
                // turning this back on will cause full sequential to be un-set when it reloads
                //defaultForMode[i].setValue(true);
                //defaultForMode[i].fireEvent('change', defaultForMode[i], true);
            }
        }
    }
})();