﻿(function () {
	Symphony.Artisan.SectionOptions = Ext.define('artisan.sectionoptions', {
	    alias: 'widget.artisan.sectionoptions',
	    extend: 'Ext.form.FormPanel',
		type: '',
		course: null,
		section: null,
		initComponent: function () {
			Ext.apply(this, {
				border: false,
				items: [{
					xtype: 'symphony.spellcheckarea',
					height: 100,
					width: 100,
					fieldLabel: 'Objectives',
					value: this.section ? this.section.objectives : '',
					ref: 'objectives',
					name: 'objectives',
					listeners: {
						blur: Ext.bind(this.save, this)
					}
				}]
			});

			this.callParent(arguments);
		},
		load: function () {
			this.objectives.setValue('');
			if (this.section.objectives) {
				this.objectives.setValue(this.section.objectives);
			}
		},
		save: function (field) {
			this.section.objectives = field.getValue();
		}
	});

})();