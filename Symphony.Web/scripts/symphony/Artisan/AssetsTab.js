﻿/// <reference path="AssetPanel.js" />
(function () {
    Symphony.Artisan.AssetsTab = Ext.define('artisan.assetstab', {
        alias: 'widget.artisan.assetstab',
        extend: 'Ext.Panel',
        count: 0,
        allowUploads: true,
        autoPlay: false,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                items: [{
                    split: true,
                    collapsible: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    title: 'Select an Asset',
                    itemId: 'artisan.assetsgrid',
                    xtype: 'artisan.assetsgrid',
                    ref: 'assetsGrid',
                    allowUploads: this.allowUploads,
                    listeners: {
                        assetselected: function (record) {
                            me.addPanel(record);
                        },
                        uploadcomplete: function (asset) {
                            var recordDef = Ext.data.Record.create(Symphony.Definitions.artisanAsset);
                            var record = new recordDef(asset);
                            this.refresh();
                            me.addPanel(record);
                        },
                        celldblclick: function (grid, rowIndex, columnIndex, e) {
                            me.fireEvent('assetdoubleclicked');
                        }
                    }
                }, {
                    region: 'center',
                    border: false,
                    //id: 'artisan.assetdisplaypane',
                    itemId: 'artisan.assetdisplaypane',
                    ref: 'displayPane',
                    xtype: 'tabpanel',
                    listeners: {
                        add: function () {
                            if (me.count == 0) {
                                me.fireEvent('assetavailable');
                            }
                            me.count++;
                        },
                        remove: function () {
                            me.count--;
                            if (me.count == 0) {
                                me.fireEvent('assetunavailable');
                            }
                        }
                    }
                }]
            });
            this.callParent(arguments);
        },
        getActiveAsset: function () {
            var tab = this.displayPane.layout.getActiveItem();
            return tab.getAsset();
        },
        addPanel: function (record) {
            var id = record.get('id');
            if (record.data.post) {
                return; // ignore events from the uploader; we save later anyway
            }
            var me = this;
            var found = id && this.displayPane.findBy(function (element) {
                if (element.asset) {
                    return element.asset.id == id;
                }
                return false;
            });

            if (found.length > 0 && this.displayPane) {
                this.displayPane.activate(found[0]);
            } else {
                var p = new Symphony.Artisan.AssetPanel({
                    title: record.get('name'),
                    asset: record.data,
                    autoPlay: this.autoPlay,
                    border: false,
                    listeners: {
                        save: function () {
                            me.assetsGrid.refresh();
                        }
                    }
                });
                this.displayPane.add(p);
                this.displayPane.doLayout();
                p.show();
            }
        }
    });

})();