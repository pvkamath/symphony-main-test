﻿(function () {
    Symphony.Artisan.QuestionPageEditor = Ext.define('artisan.questionpageeditor', {
        alias: 'widget.artisan.questionpageeditor',
        extend: 'Ext.Panel',
        content: '',
        lastContent: '',
        reset: false,
        page: null,
        node: null,
        selectReferenceText: 'Select Reference Page',
        changeReferenceText: 'Change Reference Page',
        initComponent: function () {
            var me = this;
            var questionTypeName = '';
            for (var prop in Symphony.ArtisanQuestionType) {
                if (Symphony.ArtisanQuestionType[prop] == this.page.questionType) {
                    questionTypeName = prop.toLowerCase();
                    break;
                }
            }
            Ext.apply(this, {
                layout: 'border',
                cls: 'contentarea-layout',
                border: false,
                defaults: {
                    border: false
                },
                listeners: {
                    beforedestroy: function (comp) {
                        if (me.timer) {
                            window.clearInterval(me.timer);
                        }
                    }
                },
                items: [{
                    region: 'north',
                    height: 27,
                    xtype: 'toolbar',
                    items: [{
                        text: 'Revert to Last Save',
                        iconCls: 'x-button-revert',
                        ref: '../revertButton',
                        disabled: true,
                        handler: Ext.bind(this.revert, this)
                    }, '-', // same as {xtype: 'tbseparator'}
					{
					text: this.page.referencePageId == 0 ? this.selectReferenceText : this.changeReferenceText,
					xtype: 'button',
					iconCls: 'x-button-content-page',
					ref: '../referencePageButton',
					handler: Ext.bind(this.selectReference, this),
					disabled: (!this.page.courseId || this.page.courseId <= 0),
					tooltip: ((!this.page.courseId || this.page.courseId <= 0) ? "Please save the course first and then close and reopen this page" : "")
	}, ' ', ' ',
					{
					    boxLabel: 'Gradable',
					    xtype: 'checkbox',
					    name: 'isGradable',
					    ctCls: 'gradable',
					    checked: me.page.isGradable,
					    listeners: {
					        check: Ext.bind(this.setGradable, this)
					    }
					}]
                }, {
                    region: 'center',
                    margins: '15',
                    xtype: 'artisan.' + questionTypeName + 'form',
                    ref: 'questionContainer',
                    page: me.page,
                    listeners: {
                        contentchange: Ext.bind(this.onContentChange, this)
                    }
                }]
            });
            this.callParent(arguments);

            if (Symphony.Artisan.autoSaveInterval != null) {
                me.timer = window.setInterval(function () {
                    var values = me.questionContainer.getValues();
                    me.onContentChange(values.question, values.answers, values.correctResponse, values.incorrectResponse);
                }, Symphony.Artisan.autoSaveInterval);
            }
        },
        setGradable: function (field) {
            this.page.isGradable = field.getValue();
        },
        revert: function () {
            this.questionContainer.setValue(this.page.lastHtml, this.page.lastAnswers, this.page.lastCorrectResponse, this.page.lastIncorrectResponse);
        },
        selectReference: function () {
            var win = null,
				loadMask = null,
				me = this,
				selectNode = function (node) {
				    var okBtn = win.query('button')[0];

				    if (node.raw.page) {
				        okBtn.enable();
				        var previewPanel = win.find('name', 'preview')[0];
				        previewPanel.update(node.raw.page.html);
				    } else {
				        okBtn.disable();
				    }
				},
				tree = new Symphony.Artisan.ImporterCourseTree({
				    region: 'west',
				    title: 'Course',
                    split: true,
				    width: 350,
				    showPages: Symphony.ArtisanPageType.content,
				    listeners: {
				        loadcomplete: function () {
				            var child = tree.getRootNode().findChildBy(function (n) {
				                return n.raw && n.raw.page && n.raw.page.id == me.page.referencePageId;
				            }, me, tree);
				            if (child) {
				                tree.getSelectionModel().select(child)
				                selectNode(child);
				            }
				            loadMask.hide();
				        },
				        nodeselected: selectNode
				    }
				});

            win = new Ext.Window({
                title: 'Select a Page',
                width: 1024,
                height: 600,
                layout: 'fit',
                modal: true,
                items: [{
                    layout: 'border',
                    border: false,
                    items: [tree,
					{
					    region: 'center',
					    name: 'preview',
					    border: false,
					    title: 'Page Preview (Un-Themed)'
					}]
                }],
                buttons: [{
                    text: 'OK',
                    disabled: true,
                    handler: function () {
                        var node = tree.getSelectedNode();
                        me.page.referencePageId = node.raw.page.id;
                        me.referencePageButton.setText(me.changeReferenceText);
                        win.close();
                    }
                }, {
                    text: 'Cancel',
                    handler: function () {
                        win.close();
                    }
                }]
            });
            win.show();
            loadMask = new Ext.LoadMask(win, {
                msg: 'Loading...',
                msgCls: 'disabled'
            });
            loadMask.show();
            tree.setCourseId(this.page.courseId);
        },
        onContentChange: function (content, answers, correctResponse, incorrectResponse) {
            var contentChanged = (content != this.page.lastHtml);

            // simple check first
            var answersChanged = (this.page.lastAnswers) ? (answers.length != this.page.lastAnswers.length) : true;

            // ok, they have the same number, let's make sure the content matches
            if (!answersChanged) {
                for (var i = 0; i < this.page.lastAnswers.length; i++) {
                    var currentAnswer = this.page.lastAnswers[i];
                    var incomingAnswer = answers[i];
                    var fields = ['sort', 'text', 'isCorrect'];
                    for (var j = 0; j < fields.length; j++) {
                        var prop = fields[j];
                        if (currentAnswer.hasOwnProperty(prop)) {
                            if (incomingAnswer[prop] != currentAnswer[prop]) {
                                answersChanged = true;
                                break;
                            }
                        }
                    }
                    if (answersChanged) {
                        break;
                    }
                }
            }

            var correctResponseChanged = (correctResponse != this.page.lastCorrectResponse);
            var incorrectResponseChanged = (incorrectResponse != this.page.lastIncorrectResponse);
            this.setRevertDisabled(!contentChanged && !answersChanged && !correctResponseChanged && !incorrectResponseChanged);
            this.page.html = content;
            this.page.answers = answers;
            this.page.correctResponse = correctResponse;
            this.page.incorrectResponse = incorrectResponse;

            this.fireEvent('contentchange', content, answers, correctResponse, incorrectResponse);
        },
        setRevertDisabled: function (disable) {
            this.revertButton.setDisabled(disable);
            if (this.node && this.node.ui && this.node.ui.anchor && this.node.ui.anchor.style) {
                if (disable) {
                    this.node.set('cls', this.node.get('cls').replace(' node-italic', ''));
                } else {
                    if (this.node.get('cls').indexOf('node-italic') == -1) {
                        this.node.set('cls', this.node.get('cls') + ' node-italic');
                    }
                }
            }
        },
        setSaved: function () {
            this.setRevertDisabled(true);
        }
    });

})();