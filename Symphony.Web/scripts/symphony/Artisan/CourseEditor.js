﻿(function () {
    Symphony.Artisan.CourseEditor = Ext.define('artisan.courseeditor', {
        alias: 'widget.artisan.courseeditor',
        extend: 'Ext.Panel',
        course: null,
        cancelButton: true,
        _options: null,
        lastId: 0,
        cls: 'course-editor',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                items: [{
                    split: true,
                    region: 'west',
                    collapsible: true,
                    border: false,
                    width: 300,
                    course: me.course,
                    layout: 'fit',
                    xtype: 'artisan.courseaccordian',
                    ref: 'accordian',
                    listeners: {
                        addpage: Ext.bind(this.addOrEdit, this),
                        editpage: Ext.bind(this.addOrEdit, this),
                        addsection: Ext.bind(this.addOrEdit, this),
                        editsection: Ext.bind(this.addOrEdit, this),
                        editcourse: Ext.bind(this.addOrEdit, this),
                        themechanged: Ext.bind(this.themeChanged, this),
                        deletepage: Ext.bind(this.deletePage, this),
                        deletesection: Ext.bind(this.deleteSection, this),
                        renamepage: Ext.bind(function (page, node) {
                            // rename all other pages that have the same id (for example, pages that are in both a test and content area)
                            var found = this.pageContainer.findBy(function (element) {
                                return element.node && element.node.page && element.node.page.id == page.id;
                            });
                            if (found.length > 0) {
                                for (var i = 0; i < found.length; i++) {
                                    found[i].setTitle(page.name);
                                }
                            }
                        }, this)
                    }
                }, {
                    region: 'center',
                    border: false,
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    course: me.course,
                    ref: 'pageContainer'
                }, {
                    region: 'north',
                    height: 27,
                    border: false,
                    xtype: 'symphony.savecancelbar',
                    hideSaveCancel: !Symphony.User.isArtisanAuthor,
                    cancelButton: this.cancelButton,
                    items: (this.popout ? ['-', {
                        text: 'Pop Out',
                        iconCls: 'x-button-popout',
                        hidden: !Symphony.User.isArtisanAuthor,
                        handler: function () {
                            var win = window.open('/artisan/' + me.course.id, 'Artisan_' + me.course.id);

                            Ext.Msg.show({
                                title: 'Sync Warning',
                                closable: false,
                                msg: 'Changes saved in the newly opened course are NOT reflected here.<br/><br/>You must close and re-open the course in this window to see your changes here.',
                                buttonText: {
                                    ok: 'OK'
                                },
                                icon: Ext.MessageBox.WARNING
                            });
                        }
                    }] : []).concat(['-', {
                        text: 'Duplicate',
                        iconCls: 'x-button-duplicate',
                        hidden: !(Symphony.User.isArtisanAuthor || Symphony.User.isArtisanPackager),
                        handler: function () {
                            me.duplicate();
                        }
                    }, {
                        text: 'Preview',
                        iconCls: 'x-button-preview',
                        handler: function () {
                            me.createPackage(true);
                        }
                    },'-', {
                        text: 'Package',
                        iconCls: 'x-button-package',
                        hidden: !(Symphony.User.isArtisanAuthor || Symphony.User.isArtisanPackager),
                        handler: function () {
                            me.createPackage(false, false);
                        }
                    }, {
                        text: 'Deploy',
                        iconCls: 'x-button-deploy',
                        hidden: !(Symphony.User.isArtisanAuthor),
                        menu: {
                            items: [{
                                text: 'Deploy and Create New Course',
                                iconCls: 'x-button-deploy-create',
                                handler: function () {
                                    me.createPackage(false, true, false);
                                }
                            }, {
                                text: 'Deploy and Update If Possible',
                                iconCls: 'x-button-deploy-update',
                                handler: function () {
                                    me.createPackage(false, true, true);
                                }
                            }, {
                                text: 'Deploy to MARS',
                                iconCls: 'x-button-deploy-mars',
                                handler: function () {
                                    me.createPackage(false, true, true, 'mars');
                                }
                            }]
                        }
                    }, {
                        text: 'Export',
                        iconCls: 'x-button-export',
                        handler: function () {
                            Ext.Msg.show({
                                title: 'Download Package',
                                msg: 'Your course has been successfully exported.<br/><br/><a href="/services/artisan.svc/courses/export/{0}" target="_blank">Download it now.</a>'.format(me.course.id),
                                buttons: Ext.Msg.OK
                            });
                        }
                    }, {
                        text: 'Publish',
                        iconCls: 'x-button-publish',
                        hidden: !Symphony.User.canDeployMultiple || !(Symphony.User.isArtisanAuthor),
                        handler: function () {
                            me.publishCourse();
                        }
                    }, {
                        text: 'Source Import Info',
                        iconCls: 'x-button-openbook',
                        handler: function () {
                            var win = new Ext.Window({
                                layout: 'fit',
                                resizable: false,
                                title: 'Source Import Information',
                                height: 400,
                                width: 500,
                                items: [{
                                    xtype: 'panel',
                                    frame: false,
                                    border: false,
                                    layout: 'fit',
                                    items: [{
                                        xtype: 'artisan.sourceimportinfogrid',
                                        courseId: me.course.id
                                    }]
                                }]
                            }).show();
                        }
                    }]),
                    listeners: {
                        save: function () {
                            //var form = me.find('xtype','artisan.courseoptionseditor')[0];
                            //if(form && !form.isValid()){ return false; }

                            Ext.Msg.wait('Please wait while this course is saved.', 'Saving...');
                            var course = me.course;
                            var layout = me.find('xtype', 'artisan.courselayout')[0];
                            course.sections = layout.getSections();
                            var gridAuthors = me.find('xtype', 'artisan.authorslist')[0];
                            if (gridAuthors && gridAuthors.getStore().data) {
                                var authorsData = gridAuthors.getStore().data.items;
                                course.authors = new Array();
                                for (var i = 0; i < authorsData.length; i++) {
                                    course.authors.push({ id: authorsData[i].data.id });
                                }
                            }
                            var gridSecondCategories = me.find('xtype', 'artisan.secondarycategorieslist')[0];
                            if (gridSecondCategories && gridSecondCategories.getStore().data) {
                                var secondCategoriesData = gridSecondCategories.getStore().data.items;

                                course.secondaryCategories = new Array();
                                for (var i = 0; i < secondCategoriesData.length; i++) {
                                    course.secondaryCategories.push({ name: secondCategoriesData[i].data.name });
                                }
                            }

                            var gridAccreditations = me.find('xtype', 'artisan.accreditationgrid')[0];
                            if (gridAccreditations) {
                                // Using the writer directly is better in this case because the model has fields that
                                // use the serialize property, which is only used by a writer.
                                var store = gridAccreditations.getStore();
                                var writer = store.getProxy().getWriter();
                                var accreditationData = gridAccreditations.getStore().getRange();

                                course.accreditations = [];
                                Ext.each(accreditationData, function(accreditation) {
                                    course.accreditations.push(writer.getRecordData(accreditation));
                                });

                                // Add removed back, they should have isDeleted = true.
                                Ext.each(store.getRemovedRecords(), function(accreditation) {
                                    course.accreditations.push(writer.getRecordData(accreditation));
                                });
                            }

                            course.parameters = JSON.stringify(me.getArtisanParameters(course));

                            if (!me.validateCourse(course)) {
                                return false;
                            }
                            if (!course.minimumPageTime) {
                                course.minimumPageTime = 0;
                            }
                            if (!course.maximumQuestionTime) {
                                course.maximumQuestionTime = 0;
                            }
                            if (!course.maxTime) {
                                course.maxTime = 0;
                            }
                            if (!course.minTime) {
                                course.minTime = 0;
                            }
                            if (!course.minLoTime) {
                                course.minLoTime = 0;
                            }
                            if (!course.minScoTime) {
                                course.minScoTime = 0;
                            }

                            if (!course.passingScore) {
                                delete course.passingScore;
                            }
                            if (!course.inactivityTimeout) {
                                delete course.inactivityTimeout;
                            }

                            me.cleanLastHtml(course);

                            Symphony.Ajax.request({
                                timeout: 240 * 1000,
                                url: '/services/artisan.svc/courses/' + (me.course ? me.course.id : '0'),
                                jsonData: course,
                                success: function (result) {
                                    var accreditations = result.data.accreditations;
                                    // we *update* the course object, so any references are saved
                                    Symphony.merge(me.course, result.data, true);

                                    // Need this because merge re-adds deleted items otherwise.
                                    me.course.accreditations = result.data.accreditations;
                                    me.fireEvent('save', me.course);
                                    me.accordian.setSaved();
                                    me.pageContainer.items.each(function (tab) {
                                        tab.setSaved();
                                    });
                                    Ext.Msg.hide();
                                }
                            });
                        },
                        cancel: Ext.bind(function () {
                            this.ownerCt.remove(this);
                        }, this)
                    }
                }]
            });

            this.callParent(arguments);

            this.pageContainer = this.query('tabpanel')[0];

            // apply classes
            me.applyCompletionClasses();
        },
        cleanLastHtml: function (section) {
            if (section.pages && section.pages.length) {
               for (var i = 0; i < section.pages.length; i++) {
                    if (section.pages[i].lastHtml) {
                        delete section.pages[i].lastHtml;
                    }
                }
            }
            if (section.sections && section.sections.length) {
                for (var j = 0; j < section.sections.length; j++) {
                    this.cleanLastHtml(section.sections[j]);
                }
            }
        },
        applyCompletionClasses: function () {
            var me = this;
            me.removeClass(['inline-questions-completion', 'test-completion', 'navigation-completion', 'is-mastery-enabled']);
            if (me.course.completionMethod == Symphony.ArtisanCourseCompletionMethod.inlineQuestions) {
                me.addClass('inline-questions-completion');
            } else if (me.course.completionMethod == Symphony.ArtisanCourseCompletionMethod.test ||
                       me.course.completionMethod == Symphony.ArtisanCourseCompletionMethod.testForceRestart) {
                me.addClass('test-completion');
            } else if (me.course.completionMethod == Symphony.ArtisanCourseCompletionMethod.navigation || me.course.completionMethod == Symphony.ArtisanCourseCompletionMethod.survey) {
                me.addClass('navigation-completion');
            }

            if (me.course.isMasteryEnabled) {
                me.addClass('is-mastery-enabled');
            }
        },
        publishCourse: function () {
            var me = this;

            var course = me.course;
            var layout = me.find('xtype', 'artisan.courselayout')[0];
            course.sections = layout.getSections();

            course.parameters = JSON.stringify(me.getArtisanParameters(course));

            if (!me.validateCourse(course)) {
                return false;
            }

            Ext.Msg.wait('Please wait while your course is published...', 'Please wait...');

            Symphony.Ajax.request({
                timeout: 240 * 1000,
                url: '/services/artisan.svc/courses/publish/' + (me.course ? me.course.id : '0'),
                jsonData: course,
                success: function (result) {
                    Ext.Msg.hide();
                    Ext.Msg.show({
                        title: 'Course Published',
                        msg: 'Your course has been successfully published.<br /><br />Please visit the "Deployment" tab to manage published courses.',
                        buttons: Ext.Msg.OK
                    });
                }
            });
        },
        duplicate: function () {
            var me = this;

            var course = me.course;
            var layout = me.find('xtype', 'artisan.courselayout')[0];
            course.sections = layout.getSections();

            course.parameters = JSON.stringify(me.getArtisanParameters(course));

            if (!me.validateCourse(course)) {
                return false;
            }

            Ext.Msg.wait('Please wait while your course is duplicated...', 'Please wait...');
            Symphony.Ajax.request({
                timeout: 240 * 1000,
                url: '/services/artisan.svc/courses/duplicate/' + (me.course ? me.course.id : '0'),
                jsonData: course,
                success: function (result) {
                    if (result.success) {
                        Ext.Msg.show({
                            title: 'Course Duplicated',
                            msg: 'Your course has been successfully duplicated.',
                            buttons: Ext.Msg.OK
                        });
                        me.fireEvent('duplicate', result.data);
                    }
                }
            });
        },
        createPackage: function (preview, deploy, update, target, onComplete) {
            var me = this;
            deploy = (deploy ? true : false);
            update = (update ? true : false);
            target = target || 'symphony';

            var course = me.course;

            var layout = me.find('xtype', 'artisan.courselayout')[0];

            course.sections = layout.getSections();

            if (!me.validateCourse(course)) {
                return false;
            }

            Ext.Msg.wait('Gathering course parameters...', 'Please wait...');
            
            var parameters = me.getArtisanParameters(course);
            course.parameters = JSON.stringify(parameters);
            var continuePackaging = function () {
                Ext.Msg.wait('Please wait while your course is packaged...', 'Please wait...');
                Symphony.Ajax.request({
                    timeout: 240 * 1000,
                    url: '/services/artisan.svc/courses/package/' + (me.course ? me.course.id : '0') + '?preview=' + preview + '&deploy=' + deploy + '&update=' + update + '&target=' + target,
                    jsonData: course,
                    success: function (result) {
                        Ext.Msg.hide();
                        if (!onComplete) {
                            if (!preview) {
                                if (deploy) {
                                    var to = (target == 'symphony' ? 'Symphony' : 'MARS');
                                    var ps = (target == 'symphony' ? '<br/><br/><a href="#" onclick="Symphony.Portal.launchOnlineCourseStart({0},{1},0);">Launch it now.</a>'.format(Symphony.User.id, result.data) : '');
                                    Ext.Msg.show({
                                        title: 'Course Deployed',
                                        msg: 'Your course has been successfully deployed to ' + to + '.' + ps,
                                        buttons: Ext.Msg.OK
                                    });
                                } else {
                                    Ext.Msg.show({
                                        title: 'Download Package',
                                        msg: 'Your course has been successfully packaged.<br/><br/><a href="/services/artisan.svc/courses/package/{0}" target="_blank">Download it now.</a>'.format(me.course.id),
                                        buttons: Ext.Msg.OK
                                    });
                                }
                            } else {
                                var w = 800;
                                var h = 600;
                                if (window.screen) {
                                    w = window.screen.availWidth;
                                    h = window.screen.availHeight;
                                }
                                Ext.Msg.show({
                                    title: 'Preview Package',
                                    msg: 'Your course has been successfully packaged.<br/><br/><a href="#" onclick="javascript:window.open(\'{0}/API/indexAPI.html?cb={3}\', \'preview\',\'width={1},height={2},top=0,left=0\');">Preview it now.</a>'.format(result.data, w, h, (new Date()).getTime()),
                                    buttons: Ext.Msg.OK
                                });
                            }
                        } else {
                            onComplete(result.data);
                        }
                    }
                });
            }

            /**
            *
            *  Temporarily disabling parameters 
            *  For some reason this request does not work on chrome on the ocl symphony production environment. 
            *  Works when you directly navigate to the url, and works locally. 
            *  The request never returns in chrome and we see a communication failure message box. 
            *  Even if there are parameters set, it still does not work. 
            *
            *****/

            /*
            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/artisan.svc/parameters?codes=' + parameters.join('|'),
                success: function (args) {
                    Ext.Msg.hide();
                    if (parameters && parameters.length && Symphony.Modules.hasModule(Symphony.Modules.Parameters)) {
                        var w = new Ext.Window({
                            title: 'Set Default Course Parameters',
                            width: 500,
                            height: 500,
                            layout: 'fit',
                            modal: true,
                            border: false,
                            items: [{
                                xtype: 'artisan.parametersetoptionspanel',
                                parameters: args.data,
                                parametersFound: parameters
                            }],
                            buttons: [{
                                text: 'Continue',
                                iconCls: 'x-button-package',
                                handler: function () {
                                    var defaults = w.find('xtype', 'artisan.parametersetoptionspanel')[0].getData();
                                    course.defaultParameters = defaults;
                                    continuePackaging();
                                    w.hide();
                                }
                            }]
                        }).show();
                    } else {
                        continuePackaging();
                    }
                }
            });
            */
            // This is temporary since we have disabled the parameter gathering request. 
            Ext.Msg.hide();
            continuePackaging();
        },
        getArtisanParameters: function (course) {
            var defaultParameters = course.defaultParameters;

            course.parameters = '';
            course.defaultParameters = '';

            var courseJson = JSON.stringify(course);

            course.defaultParameters = defaultParameters;
            var parameters = courseJson.match(/({![a-z0-9_]+})/gi);

            if (parameters && parameters.length) {
                parameters = parameters.filter(function (param, index) {
                    return parameters.indexOf(param) === index;
                });
            }

            return (parameters && parameters.length) ? parameters : [];
        },
        validateCourse: function (course) {
            var count = 0;
            var success = true;

            if (course && course.sections) {
                while (success && count < course.sections.length) {
                    success = this.validateCourseSection(course.sections[count]);
                    count++;
                }
            }

            if (!course.themeId) {
                this.addOrEdit(this.course);
                Ext.Msg.alert('Error', 'A theme is required.');
                return false;
            }

            if (!course.duration && course.isUseAutomaticDuration) {
                course.duration = 0;
            } else if (!course.duration) {
                course.duration = 0;
                course.isUseAutomaticDuration = true;
            }


            if (course.isMasteryEnabled && course.completionMethod != Symphony.ArtisanCourseCompletionMethod.inlineQuestions) {
                this.addOrEdit(this.course);
                Ext.Msg.alert('Error', 'Course must use Inline Question completion mode in order to use mastery mode.');
                return false;
            }

            if (course.isMasteryEnabled && (course.navigationMethod == Symphony.ArtisanCourseNavigationMethod.freeForm || course.navigationMethod == Symphony.ArtisanCourseNavigationMethod.freeFormWithQuiz)) {
                this.addOrEdit(this.course);
                Ext.Msg.alert('Error', 'Course must use sequential or full sequential navigation mode in order to use mastery mode.');
                return false;
            }

            if (course.theme.isDynamic && !course.themeFlavorId) {
                this.addOrEdit(this.course);
                Ext.Msg.alert('Error', 'If a dynamic theme is selected, a theme flavor must also be selected.');
                return false;
            }

            return success;
        },
        validateCourseSection: function (section) {
            var count = 0;
            var success = true;

            if (section && section.sections) {
                while (success && count < section.sections.length) {
                    success = this.validateCourseSection(section.sections[count]);
                    count++;
                }
            } else if (section.pages) {
                if (this.course.isMasteryEnabled && (section.isSinglePage || section.isQuiz)) {
                    var searchableCourseTree = this.query('[xtype="artisan.searchablecoursetree"]')[0];
                    var courseRootNode = searchableCourseTree.getStore().getRootNode();
                    var sectionNode = courseRootNode.findChildBy(function (n) {
                        return n.section && n.section == section;
                    }, this, true);
                    this.addOrEdit(section, sectionNode);
                    Ext.Msg.alert('Error', 'Learning objects must be set to normal mode when mastery is enabled.<br/><br/>This has been fixed for you. Please review the change and try saving again.');
                    return false;
                }
                while (success && count < section.pages.length) {
                    success = this.validateCoursePage(section.pages[count]);
                    count++;
                }
            }

            return success;
        },
        validateCoursePage: function (page) {
            
            var error = false;
            var errorText = '';

            if (page && page.pageType == Symphony.ArtisanPageType.question) {
                if (!page.answers || page.answers.length == 0) {
                    error = true;
                    errorText = 'Your question is missing answers.';
                } else if (page.answers) {
                    var hasCorrectAnswer = false;
                    for (var i = 0; i < page.answers.length; i++) {
                        if (page.answers[i].isCorrect) {
                            hasCorrectAnswer = true;
                            break;
                        }
                    }

                    if (!hasCorrectAnswer) {
                        error = true;
                        errorText = 'Your question is missing a correct answer.';
                    }
                } else if (!page.html) {
                    error = true;
                    errorText = 'Your question is missing text.';
                } else if (!page.correctResponse) {
                    error = true;
                    errorText = 'Your question is missing a correct response.';
                } else if (!page.incorrectResponse) {
                    error = true;
                    errorText = 'Your question is missing an incorrect response.';
                }
            } else if (page && page.pageType == Symphony.ArtisanPageType.survey) {
                if (!page.answers || page.answers.length == 0) {
                    error = true;
                    errorText = 'Your survey is missing items.';
                }
            }

            if (error) {
                this.addOrEdit(page);
                Ext.Msg.alert('Error', errorText);
                return false;
            }

            return true;
        },
        themeChanged: function (theme) {
            // update all the open pages
            this.pageContainer.items.each(function (editor) {
                if (editor.setTheme) {
                    editor.setTheme(theme);
                }
            });
            // and update the course references
            this.course.theme = theme;
        },
        // finds a page and/or section by id; use the type 'page' or 'section', and the appropriate page or section object
        findPageOrSection: function (item, type) {
            var found = this.pageContainer.findBy(function (element) {
                return element.node && element.node[type] && element.node[type].id == item.id;
            });
            return found[0];
        },
        deletePage: function (page) {
            var item = this.findPageOrSection(page, 'page');
            if (item) {
                this.pageContainer.remove(item);
            }
        },
        deleteSection: function (section) {
            var item = this.findPageOrSection(section, 'section');
            if (item) {
                this.pageContainer.remove(item);
            }
        },
        addOrEdit: function (item, node) { //page, node){
            var me = this;
            var found = this.pageContainer.findBy(function (element) {
                if (node) {
                    return element.node == node;
                } else if (element.node && element.node.page) {
                    return element.node.page.id == item.id;
                } else if (element.node && element.node.section) {
                    return element.node.section.id == item.id;
                } else if (element.course) {
                    return element.course.id == item.id;
                } else if (element.page) {
                    return element.page.id == item.id;
                }
                return false;
            });

            if (found.length > 0) {
                this.pageContainer.activate(found[0]);
            } else {
                var tab = null;
                if (!item.id) {
                    item.id = --this.lastId;
                }
                if (item.pageType) {
                    if (item.pageType == Symphony.ArtisanPageType.question) {
                        tab = new Symphony.Artisan.QuestionPageEditor({
                            border: false,
                            page: item,
                            iconCls: 'x-menu-question-page',
                            title: item.name,
                            closable: true,
                            content: item.html,
                            cssText: item.cssText,
                            reset: item.id == 0,
                            node: node
                        });
                    } else if (item.pageType == Symphony.ArtisanPageType.survey) {
                        tab = new Symphony.Artisan.SurveyEditor({
                            border: false,
                            page: item,
                            iconCls: 'x-menu-survey',
                            title: item.name,
                            closable: true,
                            content: item.html,
                            cssText: item.cssText,
                            reset: item.id == 0,
                            node: node
                        });
                    } else {
                        var me = this;
                        tab = new Symphony.Artisan.ContentPageEditor({
                            border: false,
                            page: item,
                            iconCls: 'x-menu-content-page',
                            title: item.name,
                            closable: true,
                            content: item.html,
                            cssText: item.cssText,
                            theme: this.course.theme,
                            reset: item.id == 0,
                            node: node
                        });
                    }
                } else if (item.sectionType) {
                    switch (item.sectionType) {
                        case Symphony.ArtisanSectionType.objectives:
                            tab = new Symphony.Artisan.ObjectivesEditor({
                                border: false,
                                course: this.course,
                                section: item,
                                iconCls: 'x-menu-objectives',
                                title: item.name,
                                closable: true,
                                reset: item.id == 0,
                                node: node
                            });
                            break;
                        case Symphony.ArtisanSectionType.pretest:
                            tab = new Symphony.Artisan.PretestEditor({
                                border: false,
                                section: item,
                                course: this.course,
                                iconCls: 'x-menu-pretest',
                                title: item.name,
                                closable: true,
                                reset: item.id == 0,
                                node: node
                            });
                            break;
                        case Symphony.ArtisanSectionType.posttest:
                            tab = new Symphony.Artisan.PosttestEditor({
                                border: false,
                                section: item,
                                course: this.course,
                                iconCls: 'x-menu-posttest',
                                title: item.name,
                                closable: true,
                                reset: item.id == 0,
                                node: node
                            });
                            break;
                        case Symphony.ArtisanSectionType.survey:
                            // If this is a survey, we need to edit the page inside
                            item = node.surveyPageNode.page;

                            tab = new Symphony.Artisan.SurveyEditor({
                                border: false,
                                page: item,
                                course: this.course,
                                iconCls: 'x-menu-survey',
                                title: item.name,
                                closable: true,
                                reset: item.id == 0,
                                node: node
                            });
                            break;
                        case Symphony.ArtisanSectionType.sco:
                            tab = new Symphony.Artisan.ScoEditor({
                                border: false,
                                section: item,
                                course: this.course,
                                iconCls: 'x-menu-sco',
                                title: item.name,
                                closable: true,
                                reset: item.id == 0,
                                node: node
                            });
                            break;
                        case Symphony.ArtisanSectionType.learningObject:
                            tab = new Symphony.Artisan.LearningObjectEditor({
                                border: false,
                                section: item,
                                course: this.course,
                                iconCls: 'x-menu-learningobject',
                                title: item.name,
                                closable: true,
                                reset: item.id == 0,
                                node: node
                            });
                            break;
                    }
                } else {
                    tab = new Symphony.Artisan.CourseOptionsEditor({
                        border: false,
                        xtype: 'artisan.courseoptionseditor',
                        course: item,
                        iconCls: 'x-treenode-course',
                        title: item.name,
                        closable: true,
                        reset: item.id == 0,
                        node: node,
                        listeners: {
                            themechanged: Ext.bind(this.themeChanged, this),
                            contentchanged: function () {
                                me.applyCompletionClasses();
                            }
                        }
                    });
                }

                // tab should always be defined here
                if (tab) {
                    tab.on('toggleartisanmode', Ext.bind(this.toggleArtisanMode, this));
                    tab.on('clearartisanmode', Ext.bind(this.clearArtisanMode, this));

                    this.pageContainer.add(tab);
                    this.pageContainer.activate(tab);
                }
            }
        },
        clearArtisanMode: function () {
            this.course.mode = '';
            Symphony.Artisan.clearArtisanMode(this);
        },
        toggleArtisanMode: function (mode, isEnabled) {
            // Turn off previous mode
            if (isEnabled && this.course.mode && mode != this.course.mode) {
                toggleArtisanMode(this.mode, false);
            }
            
            if (isEnabled) {
                this.course.mode = mode;
            } else {
                if (mode == this.course.mode) {
                    this.course.mode = '';
                } else {
                    return; // Can't disable a mode that isn't enabled.
                }
            }

            Symphony.Artisan.toggleArtisanMode(this, mode, isEnabled);
            this.applyCompletionClasses();
        }
    });

})();