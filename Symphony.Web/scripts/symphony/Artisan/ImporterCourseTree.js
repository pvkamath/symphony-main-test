﻿(function () {
	Symphony.Artisan.ImporterCourseTree = Ext.define('artisan.importercoursetree', {
	    alias: 'widget.artisan.importercoursetree',
	    extend: 'Ext.tree.TreePanel',
		course: null,
		showPages: false,
		// or 'question' or 'content' types
		buildSpecialSections: false,
		initComponent: function () {
		    var me = this;
		    var store = Ext.create('Ext.data.TreeStore', {
		        model: 'artisanCourse',
		        root: {
		            text: 'Course Root',
		            expanded: true,
		            expandable: true,
                    children: []
		        }
		    });

			Ext.apply(this, {
			    border: false,
                store: store,
                autoScroll: true,
                columns: [{
                    xtype: 'treecolumn',
                    header: '',
                    dataIndex: 'text',
                    flex: 1
                }],
				//loader: new Ext.tree.TreeLoader(),
				listeners: {
					itemclick: function (tree, record, item, index, e, eOpts) {
					    me.fireEvent('nodeselected', record);
					}
				}
			});

			me.load();

			this.callParent(arguments);
		},
		setCourseId: function (id) {
		    var me = this;
			Symphony.Ajax.request({
				method: 'GET',
				url: '/services/artisan.svc/courses/' + id,
				success: function (result) {
				    me.course = result.data;
					me.load();
				}
			});
		},
		load: function () {
			if (this.course && this.course.sections) {
				var include = [];
				for (var i = 0; i < this.course.sections.length; i++) {
					var section = this.course.sections[i];
					var type = section.sectionType;
					if (this.buildSpecialSections || (type != Symphony.ArtisanSectionType.objectives && type != Symphony.ArtisanSectionType.pretest && type != Symphony.ArtisanSectionType.posttest)) {
						include.push(section);
					}
				}
				this.course.sections = include;

			    //this.root.beginUpdate();
				var root = this.store.getRootNode();
				root.removeAll();
				root.appendChild(Symphony.map(this.course.sections, this.buildSectionNode, this));
				//this.root.endUpdate();
				this.fireEvent('loadcomplete');
			}
		},
		buildSectionNode: function (section) {
			var iconCls = '';

			switch (section.sectionType) {
			case Symphony.ArtisanSectionType.objectives:
				iconCls = 'x-menu-objectives';
				break;
			case Symphony.ArtisanSectionType.sco:
				iconCls = 'x-menu-sco';
				break;
			case Symphony.ArtisanSectionType.learningObject:
				iconCls = 'x-menu-learningobject';
				break;
			case Symphony.ArtisanSectionType.pretest:
				iconCls = 'x-menu-pretest';
				break;
			case Symphony.ArtisanSectionType.posttest:
				iconCls = 'x-menu-posttest';
				break;
			}

		    var node = Ext.create('artisanCourse',{
				text: section.name,
				iconCls: iconCls,
				expanded: true,
				expandable: true,
				children: []
			});

			node.section = section;

            // May-2016: Make sure section.sections is an array and has a length
			if (section.sections && section.sections instanceof Array && section.sections.length > 0) {
				node.appendChild(Symphony.map(section.sections, this.buildSectionNode, this));
			} else if (section.pages && this.showPages) {
				var pages = Symphony.map(section.pages, this.buildPageNode, this);
				for (var i = 0; i < pages.length; i++) {
					if (pages[i]) {
						node.appendChild(pages[i]);
					}
				}
			}

			return node;
		},
		buildPageNode: function (page) {
			if (this.showPages === true || this.showPages === page.pageType) {

				var me = this;

				var icon = '';

				if (page.pageType == Symphony.ArtisanPageType.question) {
					icon = '/images/page_green.png';
				} else {
					icon = '/images/page_red.png';
				}

				var node = new Ext.tree.TreeNode({
					text: page.name,
					icon: icon,
					leaf: true,
					editable: false
				});

				node.page = page;

				return node;
			}
			return null;
		},
		isTest: function (section) {
			if (!section) {
				return false;
			}

			return section.sectionType == Symphony.ArtisanSectionType.pretest || section.sectionType == Symphony.ArtisanSectionType.posttest;
		},
		getSelectedId: function () {
			var selected = this.getSelectedNode();
			if (selected.section) {
				return selected.section.id;
			} else if (selected.page) {
				return selected.page.id;
			}
		},
		getSelectedNode: function () {
			return this.getSelectionModel().getSelection()[0];
		}
	});

})();