﻿(function () {
	Symphony.Artisan.ScoEditor = Ext.define('artisan.scoeditor', {
	    alias: 'widget.artisan.scoeditor',
	    extend: 'artisan.optionseditor',
		section: null,
		lastObjectives: '',
		initComponent: function () {
			var me = this;

			this.lastObjectives = this.section.objectives;
			this.lastMinScoTime = this.section.minScoTime;
			this.lastFinalContentPage = this.section.finalContentPage;

			Ext.apply(this, {
				layout: 'border',
				listeners: {
					beforedestroy: function (comp) {
						if (me.timer) {
							window.clearInterval(me.timer);
						}
					}
				},
				defaults: {
                    border: false
				},
				items: [{
					region: 'north',
					height: 27,
					xtype: 'toolbar',
					items: [{
						text: 'Revert to Last Save',
						iconCls: 'x-button-revert',
						ref: '../revertButton',
						disabled: true,
						handler: Ext.bind(me.onRevertClick, this)
					}]
				}, {
					region: 'center',
					xtype: 'panel',
					layout: 'fit',
					bodyStyle: 'padding: 15px;',
					items: [{
						layout: 'form',
						frame: true,
						items: [{
							xtype: 'fieldset',
							title: 'General',
							border: true,
							defaults: {
								border: false,
								anchor: '100%',
								labelWidth: 150
							},
							items: [{
								name: 'objectives',
								ref: '../../../objectives',
								xtype: 'symphony.spellcheckarea',
								fieldLabel: 'Objectives',
								value: this.section.objectives,
								enableKeyEvents: true,
								listeners: {
									keyup: function (field, event) {
										me.section.objectives = field.getValue();
										me.onContentChanged();
									}
								}
							}]
						}, {
						    xtype: 'fieldset',
						    title: 'Course Timers (Seconds)',
						    border: true,
						    defaults: {
						        border: false,
						        anchor: '100%',
						        labelWidth: me.labelWidth
						    },
						    items: [{
						        name: 'minScoTime',
						        ref: '../../../minScoTime',
						        xtype: 'numberfield',
						        fieldLabel: 'Min. SCO Time',
						        value: this.section.minScoTime,
						        minValue: 0,
						        allowBlank: true,
						        enableKeyEvents: true,
						        listeners: {
						            keyup: function (field, event) {
						                me.section.minScoTime = field.getValue();
						                if (!me.section.minScoTime) {
						                    delete me.section.minScoTime;
						                }
						                me.onContentChanged();
						            }
						        },
						        help: {
						            title: 'What is minimum SCO time?',
						            text: 'The minimum amount of time in seconds the user must spend in a SCO in order to advance. Leaving this blank will not enforce any minimum time.'
						        }
						    }]
						}, {
						    xtype: 'fieldset',
						    title: 'Final Content Page',
						    border: true,
						    defaults: {
						        border: false,
						        anchor: '100%',
                                labelWidth: me.labelWidth
						    },
						    items: [{
						        xtype: 'symphony.spellcheckarea',
						        border: false,
						        name: 'finalContentPage',
						        fieldLabel: 'Content',
						        value: me.section.finalContentPage,
						        cls: 'popup-menu-editor',
						        height: 245,
						        enableKeyEvents: true,
						        listeners: {
						            change: function (editor, content) {
						                me.section.finalContentPage = content;
						                me.onContentChanged();
						            }
						        }
						    }]
						}]
					}]
				}]
			});

			this.callParent(arguments);

			this.lastObjectives = this.section.objectives;

			if (Symphony.Artisan.autoSaveInterval != null) {
				me.timer = window.setInterval(function () {
					var newObjectives = me.findField('objectives').getValue();
					if (newObjectives != me.section.objectives) {
						me.section.objectives = newObjectives;
						me.onContentChanged();
					}
				}, Symphony.Artisan.autoSaveInterval);
			}
		},
		onContentChanged: function () {
			this.setRevertDisabled(false);
		},
		onRevertClick: function () {
			this.objectives.setValue(this.lastObjectives);
			this.minScoTime.setValue(this.lastMinScoTime);

			this.setRevertDisabled(true);
		},
		setRevertDisabled: function (disable) {
			this.revertButton.setDisabled(disable);
			if (this.node && disable) {
				this.node.set('cls', this.node.get('cls').replace(' node-italic', ''));
			} else {
			    if (this.node.get('cls').indexOf('node-italic') == -1) {
			        this.node.set('cls', this.node.get('cls') + ' node-italic');
			    }
			}
		},
		setSaved: function () {
		    this.lastObjectives = this.section.objectives;
		    this.lastMinScoTime = this.section.minScoTime;
		    this.lastFinalContentPage = this.section.finalContentPage;

			this.setRevertDisabled(true);
		}
	});

})();