﻿(function () {
    Symphony.Artisan.CoursesTab = Ext.define('artisan.coursestab', {
        alias: 'widget.artisan.coursestab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                items: [{
                    split: true,
                    collapsible: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    title: 'Select a Course',
                    id: 'artisan.coursesgrid',
                    xtype: 'artisan.coursesgrid',
                    listeners: {
                        cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            var record = grid.store.getAt(rowIndex);
                            if (grid.headerCt.getGridColumns()[columnIndex].id != 'delete') {
                                me.addPanel(record.get('id'), record.get('name'));
                            }
                        },
                        addclick: function () {
                            me.addPanel(0, 'New Course');
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    itemId: 'artisan.courseeditorcontainer'
                }],
                listeners: {
                    // delayed load
                    activate: {
                        fn: function (panel) {
                            panel.find('xtype', 'artisan.coursesgrid')[0].refresh();
                        },
                        single: true
                    }
                }
            });
            this.callParent(arguments);
        },
        activate: function () {
            this.find('xtype', 'artisan.coursesgrid')[0].refresh();
        },
        addPanel: function (id, name) {
            var me = this;
            var tabPanel = this.getComponent('artisan.courseeditorcontainer');
            var found = id ? tabPanel.queryBy(function (element) {
                if (!element.course || !element.course.id) {
                    return false;
                }
                return element.course && element.course.id == id;
            }) : null;

            if (found && found.length > 0) {
                tabPanel.activate(found[0]);
            } else {
                if (id) {
                    Ext.Msg.wait('Please wait while this course is loaded...', 'Loading...');
                    Symphony.Ajax.request({
                        method: 'GET',
                        url: '/services/artisan.svc/courses/' + id,
                        success: function (result) {
                            // rebuild the pages so they're references instead of distinct objects
                            var cache = {};
                            var scan = function (data) {
                                if (data.sections) {
                                    for (var i = 0; i < data.sections.length; i++) {
                                        scan(data.sections[i]);
                                    }
                                }
                                if (data.pages) {
                                    for (var i = 0; i < data.pages.length; i++) {
                                        var page = data.pages[i];
                                        if (cache[page.id]) {
                                            data.pages[i] = cache[page.id];
                                        } else {
                                            cache[page.id] = page;
                                        }
                                    }
                                }
                            };

                            // recursively run through the objects
                            // if you find pages that match, delete the 2nd (or 3rd, etc) page
                            // and point it back to the original
                            scan(result.data);
                            
                            me._addPanel(result.data, name);

                            Ext.Msg.hide();
                        }
                    });
                } else {
                    me._addPanel({
                        name: name,
                        id: id
                    }, name);
                }
            }
            tabPanel.doLayout();
        },
        _addPanel: function (course, name) {
            var me = this;
            var tabPanel = this.getComponent('artisan.courseeditorcontainer');
            var panel = tabPanel.add({
                xtype: 'artisan.courseeditor',
                popout: true,
                closable: true,
                activate: true,
                title: name,
                course: course,
                listeners: {
                    save: function (course) {
                        me.find('xtype', 'artisan.coursesgrid')[0].refresh();
                        panel.setTitle(course.name);
                        panel.course = course;
                    },
                    duplicate: function (course) {
                        me.find('xtype', 'artisan.coursesgrid')[0].refresh();
                        me._addPanel(course, course.name);
                    }
                }
            });
            panel.show();
        }
    });

})();