﻿(function () {
	Symphony.Artisan.CourseLayout = Ext.define('artisan.courselayout', {
	    alias: 'widget.artisan.courselayout',
	    extend: 'Ext.Panel',
		course: null,
		initComponent: function () {
			var me = this;

			Ext.apply(this, {
				border: false,
				defaults: {
					border: false
				},
				tbar: [{
					text: 'Add',
					iconCls: 'x-button-add',
					ref: '../addButton',
					disabled: true,
                    hidden: !Symphony.User.isArtisanAuthor,
					menu: [{
						text: 'Add Objectives',
						iconCls: 'x-menu-objectives',
						ref: 'addObjectivesButton',
						handler: Ext.bind(this.onAddObjectivesClick, this)
					}, {
						text: 'Add SCO',
						iconCls: 'x-menu-sco',
						ref: 'addScoButton',
						handler: Ext.bind(this.onAddScoClick, this)
					}, {
						text: 'Add Learning Object',
						iconCls: 'x-menu-learningobject',
						ref: 'addLearningObjectButton',
						handler: Ext.bind(this.onAddLearningObjectClick, this)
					}, {
						text: 'Content Page',
						iconCls: 'x-menu-content-page',
						ref: 'addContentPageButton',
						handler: Ext.bind(this.onAddContentPageClick, this)
					}, {
						text: 'Question Page',
						iconCls: 'x-menu-question-page',
						ref: 'addQuestionPageButton',
						handler: Ext.bind(this.onAddQuestionPageClick, this)
					}, {
						text: 'Add Pretest',
						iconCls: 'x-menu-pretest',
						ref: 'addPretestButton',
						handler: Ext.bind(this.onAddPretestClick, this),
						disabledForModes: 'mastery'
					}, {
						text: 'Add Posttest',
						iconCls: 'x-menu-posttest',
						ref: 'addPosttestButton',
						handler: Ext.bind(this.onAddPosttestClick, this),
						disabledForModes: 'mastery'
					}, {
						text: 'Add Survey',
						iconCls: 'x-menu-survey',
						ref: 'addSurveyButton',
						handler: Ext.bind(this.onAddSurveyClick, this),
						disabledForModes: 'mastery'
					}],
					listeners: {
					    click: function () {
					        //me.onTreeSelectionChange(null, me.tree.root);
					    }
					}
				}, {
					text: 'Rename',
					iconCls: 'x-button-rename',
					ref: '../renameButton',
                    hidden: !Symphony.User.isArtisanAuthor,
					handler: Ext.bind(this.onRenameClick, this)
				}, {
					text: 'Delete',
					iconCls: 'x-button-delete',
					ref: '../deleteButton',
                    hidden: !Symphony.User.isArtisanAuthor,
					handler: Ext.bind(this.onDeleteClick, this)
				}, {
					text: 'Import',
					name: 'import',
					iconCls: 'x-button-import',
                    hidden: !Symphony.User.isArtisanAuthor,
					handler: Ext.bind(this.onImportClick, this)
				}],
				items: [{
					xtype: 'artisan.searchablecoursetree',
                    autoScroll: true,
					split: true,
					border: false,
					ddGroup: 'courselayout',
					enableDD: true,
					plugins: [{
					    ptype: 'cellediting',
					    ignoreNoChange: true,
					    listeners: {
					        beforeedit: Ext.bind(this.onTreeBeforeEdit, this),
					        canceledit: Ext.bind(this.onTreeCancelEdit, this),
					        edit: Ext.bind(this.onTreeAfterEdit, this)
					    }
					}],
					columns: [
                    {
                        xtype: 'treecolumn',
                        text: '',
                        dataIndex: 'text',
                        flex: 1,
                        editor: { xtype: 'textfield', allowBlank: false }
                    }],
					model: 'SimpleArtisanCourseElement',
                    root: {
                        text: 'Course Root',
                        children: [],
                        expanded: true,
                        leaf: false
                    },
                    rootVisible: true,
                    viewConfig: {
                        plugins: {
                            ptype: 'treeviewdragdrop'
                        },
                        listeners: {
                            itemcontextmenu: function (tree, record, item, index, e, eOpts) {
                                if (record.onContextMenu) {
                                    e.stopEvent();
                                    record.onContextMenu(record, e, me);
                                    return false;
                                }
                            },
                            nodedragover: Ext.bind(this.onTreeNodeDragOver, this),
                            beforedrop: Ext.bind(this.beforeNodeDrop, this),
                            startdrag: Ext.bind(function (tree, node, e) {
                                this._dragging = true;
                            }, this),
                            enddrag: Ext.bind(function (tree, node, e) {
                                this._dragging = false;
                            }, this),
                            drop: Ext.bind(this.onTreeMoveNode, this)
                        }
                    },
					listeners: {

						afterrender: function () {
							me.fireEvent('editcourse', me.course, this.root);
						},
						itemclick: function (tree, record) {
                            
						    me.onTreeSelectionChange(tree.getSelectionModel(), record);
						    if (record.section) {
						        me.fireEvent('editsection', record.section, record);
						    } else if (record.page) {
						        me.fireEvent('editpage', record.page, record);
						    } else if (record.getId() == 'root') {
						        me.fireEvent('editcourse', me.course, this.root);
						    }
						}
					}
				}]
			});

			this.callParent(arguments);

			this.tree = this.query('[xtype=artisan.searchablecoursetree]')[0];
			this.tree.root = this.tree.getRootNode();
            //this.tree.root.setText('Course Root'); //this.course.name);
			
            if (this.course && this.course.sections && this.course.sections.length) {
                this.tree.root.appendChild(Symphony.map(this.course.sections, this.buildSectionNode, this));
            }
            
            if (this.containsSection(Symphony.ArtisanSectionType.pretest)) {
				this.addButton.menu.addPretestButton.disable();
			} else {
				this.addButton.menu.addPretestButton.enable();
			}

            if (this.containsSection(Symphony.ArtisanSectionType.survey)) {
				this.addButton.menu.addSurveyButton.disable();
			} else {
				this.addButton.menu.addSurveyButton.enable();
			}

			if (this.containsSection(Symphony.ArtisanSectionType.objectives)) {
				this.addButton.menu.addObjectivesButton.disable();
			} else {
				this.addButton.menu.addObjectivesButton.enable();
			}
		},
		containsSection: function (type) {
			return this.checkForSection(this.course, type);
		},
		checkForSection: function (section, type) {
			if (section && section.sectionType == type) {
				return true;
			} else if (section && section.sections) {
				for (var i = 0; i < section.sections.length; i++) {
					if (this.checkForSection(section.sections[i], type)) {
						return true;
					}
				}
			}
			return false;
		},
		findSectionNodeById: function (node, id) {
			if (node.section && node.section.id == id) {
				return node;
			} else {
				var nodes = node.childNodes;
				for (var i = 0; i < nodes.length; i++) {
					if (nodes[i].section) {
						if (nodes[i].section.id == id) {
							return nodes[i];
						} else if (nodes[i].section.sections) {
							var n = this.findSectionNodeById(nodes[i], id);
							if (n) {
								return n;
							}
						}
					}
				}
			}
		},
		getSections: function () {
			if (!this.tree.root.hasChildNodes()) {
				return [];
			}
			return Symphony.map(this.tree.root.childNodes, this.getSection, this);
		},
		getSection: function (sectionNode) {
			var section = sectionNode.section;
			if (sectionNode.hasChildNodes()) {
				if (sectionNode.firstChild.page) {
					section.pages = Symphony.map(sectionNode.childNodes, function (pageNode) {
						return pageNode.page;
					});
				} else {
					section.sections = Symphony.map(sectionNode.childNodes, this.getSection, this);
				}
			} else if (sectionNode.surveyPageNode) {
			    section.pages = [
                    sectionNode.surveyPageNode.page
			    ];
			} else {
				section.pages = [];
                section.sections = [];
			}
			return section;
		},
		getPages: function (node) {
			if (!node) {
				node = this.tree.root;
			}
			if (node.hasChildNodes()) {
				if (node.firstChild.isLeaf()) {
					return Symphony.map(node.childNodes, function (pageNode) {
						return pageNode.page;
					});
				} else {
					var pages = [];
					for (var i = 0; i < node.childNodes.length; i++) {
						pages = pages.concat(this.getPages(node.childNodes[i]));
					}
					return pages;
				}
			}
			return [];
		},
		getPretestNode: function() {
		    if (!this.tree.root.hasChildNodes()) {
				return [];
			}
		    var topNodes = this.tree.root.childNodes;
		    for(var i=0;i<topNodes.length;i++){
		        if (this.isPretest(topNodes[i]))
		            return topNodes[i];
		    }
		    return null;
		},
		getPosttestNode: function() {
		    if (!this.tree.root.hasChildNodes()) {
				return [];
			}
		    var topNodes = this.tree.root.childNodes;
		    for(var i=0;i<topNodes.length;i++){
		        if (this.isPosttest(topNodes[i]))
		            return topNodes[i];
		    }
		    return null;
		},
		setSaved: function () {
			Ext.each(this.getPages(), function (page) {
				page.lastHtml = page.html;
				page.lastAnswers = page.answers;
				page.lastCorrectResponse = page.correctResponse;
				page.lastIncorrectResponse = page.incorrectResponse;
			});
		},
		buildSectionNode: function (section) {
			var me = this;
			var iconCls = '';
			var nodeCls = '';
            var onContextMenu = function(){};
            
			switch (section.sectionType) {
			    case Symphony.ArtisanSectionType.objectives:
				    iconCls = 'x-menu-objectives';
				    break;
			    case Symphony.ArtisanSectionType.sco:
				    iconCls = 'x-menu-sco';
				    break;
			    case Symphony.ArtisanSectionType.learningObject:
			        iconCls = 'x-menu-learningobject';
			        if (section.isQuiz) {
			            nodeCls = 'quiz'
			        }
				    break;
				    
			    case Symphony.ArtisanSectionType.pretest:
				    iconCls = 'x-menu-pretest';
				    nodeCls = 'tree-pretest';
                    onContextMenu = function(node, e){
                        var contextMenu = new Ext.menu.Menu({
                          items: [{
                            text: 'Copy All to Posttest',
                            iconCls: 'x-menu-posttest',
                            handler: function(){
                                var posttest = me.getPosttestNode();
                                var questions = node.childNodes;
                                for(var i=0;i<questions.length;i++){
		                            if (!me.nodeContainsPage(posttest, questions[i].page.id)){
		                                posttest.appendChild(me.buildPageNode(questions[i].page));
		                            }
		                        }
                            },
                            disabled: (!node.hasChildNodes())
                          }]
                        });
                        contextMenu.showAt(e.xy);
                    }
				    break;
				    
			    case Symphony.ArtisanSectionType.posttest:
				    iconCls = 'x-menu-posttest';
				    nodeCls = 'tree-posttest';
                    onContextMenu = function(node, e){
                        var contextMenu = new Ext.menu.Menu({
                          items: [{
                            text: 'Copy All to Pretest',
                            iconCls: 'x-menu-pretest',
                            handler: function(){
                                var pretest = me.getPretestNode();
                                var questions = node.childNodes;
                                for(var i=0;i<questions.length;i++){
		                            if (!me.nodeContainsPage(pretest, questions[i].page.id)){
		                                pretest.appendChild(me.buildPageNode(questions[i].page));
		                            }
		                        }
                            },
                            disabled: (!node.hasChildNodes())
                          }]
                        });
                        contextMenu.showAt(e.xy);
                    }
				    break;
                case Symphony.ArtisanSectionType.survey:
				    iconCls = 'x-menu-survey';
				    break;
			}

			var node = Ext.create('SimpleArtisanCourseElement', {
			    text: section.name,
			    children: [],
			    expanded: true,
			    leaf: false,
			    listeners: {
			        click: function () {
			            me.fireEvent('editsection', this.section, this);
			        }
			    },
			    iconCls: iconCls,
                cls: nodeCls
			});
			node.onContextMenu = onContextMenu;
			node.section = section;
			if (section.sections) {
			    node.appendChild(Symphony.map(section.sections, this.buildSectionNode, this));
			} else if (section.pages) {
                // survey page needs to be hidden in tree
			    if (section.sectionType == Symphony.ArtisanSectionType.survey) {
			        var surveyPageNode = this.buildPageNode(section.pages[0], true);
			        node.set('leaf', true);
			        node.page = section.pages[0];
			        node.appendChild(surveyPageNode);
			        node.surveyPageNode = surveyPageNode;
                }
                else {
                    node.appendChild(Symphony.map(section.pages, this.buildPageNode, this));
                }
			}

			/*node.eachChild(function (childNode) {
			    //childNode.attributes.cls = childNode.attributes.cls + " " + nodeCls;
			});*/

			return node;
		},
		// NOTE: This only checks 1 level deep
		nodeContainsPage: function(node, pageId){
		    if (!node.hasChildNodes()) {
				return false;
			}
		    var children = node.childNodes;
		    for(var i=0;i<children.length;i++){
		        if (children[i].page && children[i].page.id == pageId)
		            return true;
		    }
		    return false;
		},
		questionNodeContextMenuHandler: function(node, e, tree){
		    var pretest = tree.getPretestNode();
		    var posttest = tree.getPosttestNode();
            var contextMenu = new Ext.menu.Menu({
              items: [{
                text: 'Copy to Pretest',
                iconCls: 'x-menu-pretest',
                handler: function(){
                    if (pretest)
                        pretest.appendChild(tree.buildPageNode(node.page));
                },
                disabled: (!pretest || tree.nodeContainsPage(pretest, node.page.id)),
                hidden: (tree.isInPretest(node))
              },{
                text: 'Copy to Posttest',
                iconCls: 'x-menu-posttest',
                handler: function(){
                    if (posttest)
                        posttest.appendChild(tree.buildPageNode(node.page));
                },
                disabled: (!posttest || tree.nodeContainsPage(posttest, node.page.id)),
                hidden: (tree.isInPosttest(node))
              }]
            });
            contextMenu.showAt(e.xy);
        },
		buildPageNode: function (page, hidden) {
            hidden = hidden || false;
			var me = this;
			var nodeCls = '';
			var icon = '';
            var onContextMenu = function(){};
            
			if (page.pageType == Symphony.ArtisanPageType.question) {
                onContextMenu = me.questionNodeContextMenuHandler;
                icon = '/images/page_green.png';
                nodeCls = 'tree-question';
			} else {
			    icon = '/images/page_red.png';
			    nodeCls = 'tree-page';
			}

			var node = Ext.create('SimpleArtisanCourseElement', {
				text: page.name,
				icon: icon,
                cls: nodeCls,
				leaf: true,
				editable: false,
				hidden: hidden,
				listeners: {
				    click: function () {
						me.fireEvent('editpage', this.page, this);
					}
				}
			});
			node.onContextMenu = onContextMenu;
			node.page = page;
			// when we build a page, we always update the lastHtml and lastAnswers to the current page info
			// so we can detect changes
			node.page.lastHtml = page.html;
			node.page.lastAnswers = page.answers;
			node.page.lastCorrectResponse = page.correctResponse;
			node.page.lastIncorrectResponse = page.incorrectResponse;
			return node;
		},
		onRenameClick: function () {
			var node = this.tree.getSelectionModel().getSelection()[0];
			this.rename(node);
		},
		rename: function (node) {
		    if (node && node != this.tree.root) {
		        this.tree.findPlugin('cellediting').startEdit(node, this.tree.columns[0]);
			}
		},
		onDeleteClick: function () {
		    Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this item?', Ext.bind(function (btn) {
				if (btn == 'yes') {
					var node = this.tree.getSelectionModel().getSelection()[0];
					if (node && node != this.tree.root) {
						node.remove();
					}

					if (node.section && node.section.sectionType == Symphony.ArtisanSectionType.pretest) {
						this.addButton.menu.addPretestButton.enable();
					}
					if (node.section && node.section.sectionType == Symphony.ArtisanSectionType.posttest) {
						this.addButton.menu.addPosttestButton.enable();
					}
                    if (node.section && node.section.sectionType == Symphony.ArtisanSectionType.survey) {
						this.addButton.menu.addSurveyButton.enable();
					}

					if (node.section) {
						this.fireEvent('deletesection', node.section);
					}
					if (node.page) {
						this.fireEvent('deletepage', node.page);
					}
				}
			}, this));
		},
		onAddSectionClick: function (type, text) {
		    var container = this.getSelectedContainerNode();
			var section = {
				name: text,
				//'New Section',
				sectionType: type //Symphony.ArtisanSectionType.normal // default to normal for now, we will need to add support for objectives and survey
			};
			var referenceNode;
			if (container == this.tree.root && type == Symphony.ArtisanSectionType.sco) {
				// if we're adding a SCO to the root, make sure it doesn't go after a post-test
				referenceNode = container.findChildBy(function (n) {
					return n.section && n.section.sectionType == Symphony.ArtisanSectionType.posttest;
				});
				if (!referenceNode) {
				    // Keep the survey at the end as well
				    referenceNode = container.findChildBy(function (n) {
				        return n.section && n.section.sectionType == Symphony.ArtisanSectionType.survey;
				    });
				}
			}

			var node = container.insertBefore(this.buildSectionNode(section), referenceNode);
			node.set('isAddRename', true);

			this.tree.getSelectionModel().select(node);
			this.onTreeSelectionChange(this.tree.getSelectionModel(), node); // trigger button config
			this.fireEvent('addsection', section, node);
			this.rename(node);
		},
		onAddObjectivesClick: function () {
			var section = {
				name: 'Objectives',
				sectionType: Symphony.ArtisanSectionType.objectives
			};
			var root = this.tree.root;
			var node = root.insertBefore(this.buildSectionNode(section), root.firstChild);
			this.addButton.menu.addObjectivesButton.disable();
			this.fireEvent('addsection', section, node);
			this.rename(node);
		},
		onAddScoClick: function () {
			this.onAddSectionClick(Symphony.ArtisanSectionType.sco, 'New SCO');
		},
		onAddLearningObjectClick: function () {
			this.onAddSectionClick(Symphony.ArtisanSectionType.learningObject, 'New Learning Object');
		},
		onAddPretestClick: function () {
			var section = {
				name: 'Pretest',
				sectionType: Symphony.ArtisanSectionType.pretest,
				testType: Symphony.ArtisanTestType.sequential // default to Sequential test type
			};

			// the pretest will always be in the root
			var root = this.tree.root;
			var node;

            // the only section that can be above it is objectives
			if (root.firstChild && root.firstChild.section.sectionType == Symphony.ArtisanSectionType.objectives) {
				node = root.insertBefore(this.buildSectionNode(section), root.firstChild.nextSibling);
			} else {
				node = root.insertBefore(this.buildSectionNode(section), root.firstChild);
			}

			this.addButton.menu.addPretestButton.disable();
			this.fireEvent('addsection', section, node);
		},
		onAddPosttestClick: function () {
			var section = {
				name: 'Posttest',
				sectionType: Symphony.ArtisanSectionType.posttest,
				testType: Symphony.ArtisanTestType.sequential // default to Sequential test type
			};

			// the posttest will always be in the root
            var root = this.tree.root;
			var node;

            // the only section that can be below it is a survey
            if (root.lastChild && root.lastChild.section.sectionType == Symphony.ArtisanSectionType.survey) {
				node = root.insertBefore(this.buildSectionNode(section), root.lastChild);
			} else {
				node = this.tree.root.appendChild(this.buildSectionNode(section));
			}

			this.addButton.menu.addPosttestButton.disable();
			this.fireEvent('addsection', section, node);
		},
        onAddSurveyClick: function () {
			var section = {
				name: 'Survey',
				sectionType: Symphony.ArtisanSectionType.survey,
                leaf: true
			};

			// the survey will always be in the root
			var sectionNode = this.tree.root.appendChild(this.buildSectionNode(section));
			this.addButton.menu.addSurveyButton.disable();

            // survey is a single page within a section
            var page = {
				name: 'Survey',
				html: '',
				courseId: this.course.id,
				pageType: Symphony.ArtisanPageType.survey,
				questionType: 0
            };

            var surveyPageNode = this.buildPageNode(page, true);
            //sectionNode.appendChild(surveyPageNode);
            sectionNode.surveyPageNode = surveyPageNode;

			this.onTreeSelectionChange(this.tree.getSelectionModel(), sectionNode);

			

            // survey is a special case where we display the section in the tree, but edit the page instead
            this.fireEvent('addsection', section, sectionNode);
		},
		onAddContentPageClick: function () {
			this.openTemplatePicker(Symphony.ArtisanPageType.content);
		},
		onAddQuestionPageClick: function () {
			this.openTemplatePicker(Symphony.ArtisanPageType.question);
		},
		openTemplatePicker: function (type) {
			new Symphony.Artisan.TemplatePicker({
				modal: true,
				type: type,
				listeners: {
					templateselect: Ext.bind(this.onTemplateSelect, this)
				}
			}).show();
		},
		onTemplateSelect: function (template) {
			var me = this;
			var container = this.getSelectedContainerNode();
			var page = {
				name: template.pageType == Symphony.ArtisanPageType.question ? 'New Question Page' : 'New Content Page',
				html: template.html,
				cssText: template.cssText,
				lastHtml: template.html,
				lastAnswers: [],
				courseId: this.course.id,
				// new questions don't have any answers yet
				lastCorrectResponse: '',
				lastIncorrectResponse: '',
				correctResponse: '',
				incorrectResponse: '',
				pageType: template.pageType,
				questionType: template.questionType
			};
			var node = container.appendChild(this.buildPageNode(page));
			this.onTreeSelectionChange(this.tree.getSelectionModel(), container); // trigger button config
			this.fireEvent('addpage', page, node);
		},
		onImportClick: function () {
			var me = this;
			new Symphony.Artisan.ImporterWizard({
				course: me.course,
				modal: true,
				listeners: {
					importComplete: function (sectionId, pages) {
						var node = me.findSectionNodeById(me.tree.root, sectionId);
						if (node) {
							for (var i = 0; i < pages.length; i++) {
								node.appendChild(me.buildPageNode(pages[i]));
							}
						}
					},
					importExisting: function (target, source) {
					    var node = target.isRoot() ? me.tree.getRootNode() : me.findSectionNodeById(me.tree.getRootNode(), target.section.id);
						if (source.section) {
							// import a SCO or LO
							// if it's a LO, and the target is also a LO, import all the pages
							if (source.section.sectionType == Symphony.ArtisanSectionType.learningObject && target.section && target.section.sectionType == Symphony.ArtisanSectionType.learningObject) {
								if (source.section.pages) {
									for (var i = 0; i < source.section.pages.length; i++) {
										node.appendChild(me.buildPageNode(source.section.pages[i]));
									}
								}
							} else {
								// can't just append the child, need to insert *after* pre-test/objectives and *before* post-test
								var referenceNode = null;
								if (target.isRoot() || target.section.sectionType == Symphony.ArtisanSectionType.sco) {
									referenceNode = me.tree.getRootNode().findChildBy(function (n) {
										return n.section && n.section.sectionType == Symphony.ArtisanSectionType.posttest;
									});
								}
								node.insertBefore(me.buildSectionNode(source.section), referenceNode);
							}
						} else if (source.page) {
							// import a page
							node.appendChild(me.buildPageNode(source.page));
						}

					}
				}
			}).show();
		},
		getSelectedContainerNode: function () {
			var node = this.tree.getSelectionModel().getSelection()[0];
			if (!node) {
				node = this.tree.root;
			}
			if (node.isLeaf()) {
				node = node.parentNode;
			}
			return node;
		},
		onTreeSelectionChange: function (sm, node) {
		    

			var addSectionButton = this.addButton.menu.addSectionButton;
			var addScoButton = this.addButton.menu.addScoButton;
			var addLearningObjectButton = this.addButton.menu.addLearningObjectButton;
			var addContentPageButton = this.addButton.menu.addContentPageButton;
			var addQuestionPageButton = this.addButton.menu.addQuestionPageButton;
			var addPretestButton = this.addButton.menu.addPretestButton;
			var addPosttestButton = this.addButton.menu.addPosttestButton;
            var addSurveyButton = this.addButton.menu.addSurveyButton;
			var isRoot = (!node || node == this.tree.root);

			if (isRoot) { // root - scos ok, no learning objects, no pages
				this.addButton.enable();
				addScoButton.enable();
				addLearningObjectButton.disable();
				addContentPageButton.disable();
				addQuestionPageButton.disable();
			} else if (this.isSco(node)) { // sco - scos ok, learning objects ok, no pages
				this.addButton.enable();

				// a sco can contain either learning objects or sub-scos, not both
				if (this.hasSco(node)) {
					addScoButton.enable();
					addLearningObjectButton.disable();
				} else if (this.hasLearningObject(node)) {
					addScoButton.disable();
					addLearningObjectButton.enable();
				} else {
					addScoButton.enable();
					addLearningObjectButton.enable();
				}
				addContentPageButton.disable();
				addQuestionPageButton.disable();
			} else if (this.isLearningObject(node)) { //learning obj - no scos, pages ok
				this.addButton.enable();
				addScoButton.disable();
				addLearningObjectButton.disable();
				addContentPageButton.enable();
				addQuestionPageButton.enable();
			} else if (this.isTest(node)) { // test section, no sections, no content, questions ok
				this.addButton.enable();
				addScoButton.disable();
				addLearningObjectButton.disable();
				addContentPageButton.disable();
				addQuestionPageButton.enable();
			} else if (node.isLeaf) { // page - no sections, no pages
				this.addButton.disable();
			}

		    // limit post/pre/objectives/survey
			if (this.hasSectionType(this.tree.root, Symphony.ArtisanSectionType.posttest)) {
			    addPosttestButton.disable();
			} else {
			    addPosttestButton.enable();
			}

			if (this.hasSectionType(this.tree.root, Symphony.ArtisanSectionType.pretest)) {
			    addPretestButton.disable();
			} else {
			    addPretestButton.enable();
			}

			if (this.hasSectionType(this.tree.root, Symphony.ArtisanSectionType.survey)) {
			    addSurveyButton.disable();
			} else {
			    addSurveyButton.enable();
			}

			this.renameButton.setDisabled(isRoot);
			this.deleteButton.setDisabled(isRoot);

			if (!this._dragging) {
				this.fireEvent('nodeselected', node);
			}
		},
		hasSco: function (node) {
			var subSections = this.getSection(node).sections;
			if (!subSections) return false;

			for (var i = 0; i < subSections.length; i++) {
				if (subSections[i].sectionType == Symphony.ArtisanSectionType.sco) return true;
			}
			return false;
		},
		hasLearningObject: function (node) {
			var subSections = this.getSection(node).sections;
			if (!subSections) return false;

			for (var i = 0; i < subSections.length; i++) {
				if (subSections[i].sectionType == Symphony.ArtisanSectionType.learningObject) return true;
			}
			return false;
		},
        // only searches 1 level
		hasSectionType: function (n, t) {
		    var r = false;
		    n.eachChild(function (n) {
		        if (n.section.sectionType == t) {
		            r = true;
		        }
		    });
		    return r;
		},
		isTest: function (node) {
			return this.isPretest(node) || this.isPosttest(node);
		},
		isInTest: function (node) {
			return this.isInPretest(node) || this.isInPosttest(node);
		},
		isOrInTest: function (node) {
			return this.isTest(node) || this.isInTest(node);
		},
		isPretest: function (node) {
			if (!node.section) {
				return false;
			}

			return node.section.sectionType == Symphony.ArtisanSectionType.pretest;
		},
		isInPretest: function (node) {
			var parent = node;
			while (parent = parent.parentNode) {
				if (this.isPretest(parent)) {
					return true;
				}
			}
			return false;
		},
		isOrInPretest: function (node) {
			return this.isPretest(node) || this.isInPretest(node);
		},
		isPosttest: function (node) {
			if (!node.section) {
				return false;
			}

			return node.section.sectionType == Symphony.ArtisanSectionType.posttest;
		},
		isInPosttest: function (node) {
			var parent = node;
			while (parent = parent.parentNode) {
				if (this.isPosttest(parent)) {
					return true;
				}
			}
			return false;
		},
		isOrInPosttest: function (node) {
			return this.isPosttest(node) || this.isInPosttest(node);
		},
		isSco: function (node) {
			if (!node.section) {
				return false;
			}

			return node.section.sectionType == Symphony.ArtisanSectionType.sco;
		},
		isLearningObject: function (node) {
			if (!node.section) {
				return false;
			}

			return node.section.sectionType == Symphony.ArtisanSectionType.learningObject;
		},
		isObjectives: function (node) {
			if (!node.section) {
				return false;
			}

			return node.section.sectionType == Symphony.ArtisanSectionType.objectives;
		},
		isPretest: function (node) {
			if (!node.section) {
				return false;
			}

			return node.section.sectionType == Symphony.ArtisanSectionType.pretest;
		},
		isPosttest: function (node) {
			if (!node.section) {
				return false;
			}

			return node.section.sectionType == Symphony.ArtisanSectionType.posttest;
		},
		isQuestionPage: function (node) {
			if (!node.page) {
				return false;
			}

			return node.page.pageType == Symphony.ArtisanPageType.question;
		},
		isSurvey: function(node) {
		    if (!node.page) {
		        return false;
		    }
		    return node.page.pageType == Symphony.ArtisanPageType.survey;
		},
		isContentPage: function (node) {
			if (!node.page) {
				return false;
			}

			return node.page.pageType == Symphony.ArtisanPageType.content;
		},
		beforeNodeDrop: function (overNode, data, overModel, dropPosition, dropHandlers, eOpts) {
		    var node = data.records[0]; // thing being dropped
		    var target = overModel; // thing dropped on

			if (this.isQuestionPage(node)) {
				//                if(this.isTest(e.target) || this.isInTest(e.target) || this.isTest(node.parentNode) || this.isInTest(node.parentNode)){
				//                    e.dropNode = this.buildPageNode(node.page);
				//                }
				if ((this.isOrInPretest(node.parentNode) && (!this.isOrInTest(target) || this.isOrInPosttest(target))) || (!this.isOrInTest(node.parentNode) && (this.isOrInPretest(target) || this.isOrInPosttest(target))) || (this.isOrInPosttest(node.parentNode) && (this.isOrInPretest(target) || !this.isOrInTest(target)))) {
				    data.records[0] = this.buildPageNode(node.page);
				}
			}
		},
		onTreeNodeDragOver: function (targetNode, position, dragData, e, eOpts) {
		    var source = dragData.records[0]; // the thing we are dropping
		    var target = targetNode; // the thing we are dropping it on.

		    if (this.isObjectives(source) || this.isTest(source)) {
		        return false;
			} else if (this.isTest(target) || this.isInTest(target)) {
			    if (this.isContentPage(source)) {
                	return false;
			    } else if (this.isQuestionPage(source)) {
			    	return true;
				}
			} else if (position != 'append') {
			    if ((position == 'above' && (this.isObjectives(target) || this.isPretest(target))) || (position == 'below' && (this.isPosttest(target) || this.isObjectives(target)))) {
			    	return false;
			    }
			    if (this.isSurvey(source)) {
			        return false;
			    }
                
				return (source.isLeaf() == target.isLeaf());
			} else if (target == this.tree.root) { // root - sections ok, no pages and no LOs
			    return !source.isLeaf() && !this.isLearningObject(source);
			} else if (this.isSco(target) && (this.isSco(source) || this.isLearningObject(source))) { // sco - scos ok, learning objects ok
                return true;
			} else if (this.isLearningObject(target) && (this.isContentPage(source) || this.isQuestionPage(source))) { //learning object - pages ok
			    return true;
			} else if (this.isTest(target) && (this.isQuestionPage(source))) { // test - questions ok
			    return true;
			}
		    return false;
		},
		onTreeMoveNode: function (node, data, overModel, dropPosition, dropHandler) {
		    var oldParent = data.records[0].parentNode;

			if (oldParent.section) {
				if (node.section) {
					oldParent.section.sections.remove(node.section);
				} else if (node.page) {
					oldParent.section.pages.remove(node.page);
				}
			}

			oldParent.expand();
		},
		onTreeCancelEdit: function(editor, e) {
		    this.onTreeEditComplete(e.record);
		},
		onTreeBeforeEdit: function (editor, element, value) {
			return (editor.editNode != this.tree.root);
		},
		onTreeAfterEdit: function (editor, e) {
		    
			if (e.record.get('leaf')) {
				e.record.page.name = e.value;
				
				this.tree.root.cascade(function (node) {
					if (node.page && node.page.id == e.record.page.id) {
					    node.setText(e.record.page.name);
					    node.commit();
					}
				})

				this.fireEvent('renamepage', e.record.page, e.record);
			} else {
			    e.record.section.name = e.value;
			    e.record.commit();
				this.fireEvent('renamesection', e.record.section, e.record);
			}
			this.onTreeEditComplete(e.record);
		},
		onTreeEditComplete: function (record) {
		    if (record.get('isAddRename')) {
                record.set('isAddRename', false);
                this.onTreeSelectionChange(this.tree.getSelectionModel(), record);
            }
        }
	});
})();