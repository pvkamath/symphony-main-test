﻿Symphony.Libraries.LibraryGrid = Ext.define('libraries.librarygrid', {
    alias: 'widget.libraries.librarygrid',
    extend: 'Symphony.SearchableGrid',
    initComponent: function () {
        var me = this;
        var url = '/services/library.svc/libraries/';


        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [
                { header: 'Type', dataIndex: 'displayLibraryItemType', align: 'center', renderer: Symphony.Renderer.displayIconRenderer, width: 40 },
                { header: 'Name', dataIndex: 'name', align: 'left', flex: 1 },
                { header: 'Created', dataIndex: 'displayCreatedOn', renderer: Symphony.Renderer.displayDateRenderer, align: 'center', width: 60 },
                { header: 'Registrations', dataIndex: 'registrationCount', align: 'center', width: 80 },
                { header: 'Items', dataIndex: 'itemCount', align: 'center', width: 40 },
                { header: 'Shared', dataIndex: 'isShared', width: 64, renderer: Symphony.checkRenderer }
            ]
        });

        Ext.apply(this, {
            tbar: {
                items: [{
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function () {
                        me.fireEvent('addclick');
                    }
                }]
            },
            idProperty: 'id',
            autoExpandColumn: 'name',
            url: url,
            colModel: colModel,
            model: 'library',
            deferLoad: true
        });
        this.callParent(arguments);
    }
});
