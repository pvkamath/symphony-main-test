﻿Symphony.Libraries.PortalLibraryItemGrid = Ext.define('libraries.portallibraryitemgrid', {
    alias: 'widget.libraries.portallibraryitemgrid',
    extend: 'Symphony.Libraries.LibraryItemGrid',
    initComponent: function () {
        var me = this;
        me.libraryTypeId = this.libraryRecord ? this.libraryRecord.get('libraryItemTypeId') : null;
        var colModel = {
            columns: [
                {
                    header: 'Name', dataIndex: 'name', align: 'left', flex: 1
                },
                {
                    header: 'Date Added', dataIndex: 'createdOn', align: 'center', renderer: function (value, meta, record) {
                        var displayDate = record.get('displayCreatedOn');

                        return Symphony.Renderer.displayDateRenderer(displayDate);
                    }
                },
                {
                    header: 'Progress', dataIndex: 'item', align: 'center', renderer: function (value, meta, record) {
                        if (me.libraryTypeId == 1) {
                            return value.navigationPercentageDisplay;
                        } else {
                            if (value.percentComplete) {
                                return value.percentComplete + '%';
                            }
                        }
                    }, getSortParam: function () {
                        return 'IsInProgress'
                    }
                },
                {
                     header: me.libraryTypeId == 1 ? 'Score' : 'Completed', dataIndex: 'item', align: 'center', renderer: function (value, meta, record) {
                        if (me.libraryTypeId == 1) {
                            return Symphony.Renderer.displayTextRenderer(value.displayScore) +
                                   Symphony.Renderer.displayIconRenderer(value.displayCertificate);
                        } else {
                            return Symphony.Renderer.displayIconRenderer(value.displayCompleted);
                        }
                     }, getSortParam: function () {
                         return 'IsInProgress'
                     }
                },
                {
                     header: 'Launch', dataIndex: 'launch', align: 'center', renderer: function (value, meta, record) {
                        if (me.libraryTypeId == 1) {
                            return Symphony.Renderer.displayLinkRenderer(record.get('item').displayCourseLink);
                        } else {
                            return Symphony.Portal.trainingProgramRenderer("Launch", meta, record);
                        }
                    }
                },
                {
                    header: 'Favorite', dataIndex: 'displayIsFavorite', align: 'center', renderer: function (value, meta, record) {
                        if (record.get('isLoading')) {
                            meta.css = 'x-mask-loading'
                        }
                        return Symphony.Renderer.displayIconRenderer(value);
                    }, getSortParam: function () {
                        return 'IsFavorite'
                    }
                }
            ]
        };

        Ext.apply(this, {
            tbar: {
                items: !me.hideHeader ? [{
                    xtype: 'button',
                    iconCls: 'x-button-previous',
                    text: 'Back to Categories',
                    handler: function () {
                        me.fireEvent('showCategories');
                    }
                }] : []
            },
            colModel: colModel,
            title: !me.hideHeader ? ((me.libraryTypeId == 1) ? 'Online Courses' : 'Training Programs') : null,
            frame: false,
            border: false,
            deferLoad: true,
            listeners: {
                render: function () {
                    if (me.baseFilter) {
                        Ext.apply(me.activeFilter, me.baseFilter);
                    }

                    me.updateTitle();
                },
                cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e) {
                    var fieldName = grid.getColumnModel().getDataIndex(columnIndex);
                    var data = record.get(fieldName);

                    var courseRecord = Ext.create('course', record.get('item'));

                    if (fieldName == 'launch') {
                        if (me.libraryTypeId == 1) {
                            var courseLink = record.get('item').displayCourseLink;

                            Symphony.LinkHandlers.click(grid, e, courseLink, courseRecord, function () {
                                me.refresh();
                            });
                        } else {
                            Symphony.Portal.showTrainingProgram(record.get('id'), function () {
                                me.refresh();
                            }, true);
                        }
                    } else {
                        Symphony.LinkHandlers.click(grid, e, data, record);
                    }
                }
            }
        });

        this.callParent(arguments);
    },
    updateTitle: function (categoryName) {
        if (!this.hideHeader) {
            this.setTitle(categoryName + ": " + (this.libraryTypeId == 1 ? 'Online Courses' : 'Training Programs'));
        }
    }
});
