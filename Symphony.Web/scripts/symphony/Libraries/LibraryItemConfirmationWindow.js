﻿Symphony.Libraries.LibraryItemConfirmationWindow = Ext.define('libraries.libraryitemconfirmationwindow', {
    alias: 'widget.libraries.libraryitemconfirmationwindow',
    extend: 'Ext.Window',
    initComponent: function () {
        var me = this;
    
        Ext.apply(this, {
            width: 300,
            height: 400,
            title: me.title,
            layout: 'fit',
            border: false,
            items: [{
                xtype: 'panel',
                layout: {
                    type: 'vbox',
                    pack: 'start',
                    align: 'stretch'
                },
                defaults: {
                    flex: 1
                },
                border: true,
                frame: true,
                cls: 'x-window-dlg',
                items: [{
                    xtype: 'panel',
                    border: false,
                    frame: false,
                    cls: 'x-panel-transparent ext-mb-text',
                    html: me.message,
                    flex: me.messageFlex ? me.messageFlex : 0.25
                }, {
                    xtype: 'libraries.libraryitemlocalgrid',
                    flex: 0.75,
                    ref: '../libraryItemGrid'                  
                }]
            }],
            listeners: {
                render: function () {
                    me.libraryItemGrid.setData( me.libraryItems );
                }
            }
        });

        this.callParent(arguments);
    }
});
