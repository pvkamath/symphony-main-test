﻿Symphony.Libraries.LibraryItemsTab = Ext.define('libraries.libraryitemstab', {
    alias: 'widget.libraries.libraryitemstab',
    extend: 'Ext.Panel',
    libraryId: 0,
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            frame: false,
            border: false,
            layout: 'fit',
            defaults: {
                border: false
            },
            items: [{
                xtype: 'libraries.libraryitemgridadmin',
                libraryId: me.libraryId,
                libraryDetails: me.libraryDetails
            }]
        });
        this.callParent(arguments);
    }
});
