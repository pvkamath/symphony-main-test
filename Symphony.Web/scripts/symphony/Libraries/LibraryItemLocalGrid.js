﻿Symphony.Libraries.LibraryItemLocalGrid = Ext.define('libraries.libraryitemlocalgrid', {
    alias: 'widget.libraries.libraryitemlocalgrid',
    extend: 'Symphony.LocalGrid',
    initComponent: function () {
        var me = this;
    

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [
                { flex: 1, header: 'Name', dataIndex: 'name', align: 'left' }
            ]
        });

        Ext.apply(this, {
            idProperty: 'id',
            border: false,
            autoExpandColumn: 'name',
            colModel: colModel,
            model: 'libraryItem'
        });

        this.callParent(arguments);
    }
});
