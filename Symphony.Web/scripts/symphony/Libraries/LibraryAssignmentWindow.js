﻿Symphony.Libraries.LibraryAssignmentWindow = Ext.define('libraries.libraryassignmentwindow', {
    alias: 'widget.libraries.libraryassignmentwindow',
    extend: 'Ext.Window',
    libraryId: 0,
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            layout: 'fit',
            border: false,
            frame: true,
            title: 'Create Registrations',
            modal: true,
            tbar: {
                items: [{
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'x-button-save',
                    handler: function () {
                        me.save();
                    }
                }, {
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        me.close();
                    }
                }]
            },
            items: [{
                xtype: 'panel',
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    pack: 'start'
                },
                border: false,
                frame: true,
                items: [{
                    flex: 0.15,
                    xtype: 'form',
                    border: false,
                    frame: false,
                    ref: '../dates',
                    cls: 'x-panel-transparent',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [{
                        xtype: 'datefield',
                        name: 'startDate',
                        fieldLabel: 'Start Date',
                        allowBlank: false
                    }, {
                        xtype: 'datefield',
                        name: 'endDate',
                        fieldLabel: 'End Date',
                        allowBlank: false
                    }]
                }, {
                    flex: 0.85,
                    xtype: 'courseassignment.userassignmentpanel',
                    bodyStyleOverride: 'padding: 0px',
                    ref: '../assignments',
                    border: false,
                    frame: false,
                    defaults: {
                        border: false,
                        frame: false
                    },
                    listeners: {
                        render: function (panel) {
                            panel.setSelections(Symphony.HierarchyType.user);
                        }
                    }
                }]
            }]
        });
        this.callParent(arguments);
    },
    save: function () {
        var selected = this.assignments.getSelections();
        var dates = this.dates.getValues();

        if (!this.dates.getForm().isValid()) {
            Ext.Msg.alert('Dates Required', 'Start and End date are required when creating library registrations.');
            return;
        }

        if (!selected.length) {
            Ext.Msg.alert('Nothing Selected', 'Select at least once item to create a library registration.');
            return;
        }

        for (var i = 0; i < selected.length; i++) {
            selected[i].startDate = dates.startDate;
            selected[i].endDate = dates.endDate;
        }

        var me = this;

        var loadMask = new Ext.LoadMask(me.getEl(), { msg: "Saving grants..." });
        loadMask.show();

        Symphony.Ajax.request({
            method: 'post',
            url: '/services/library.svc/library/grants/' + this.libraryId + '/',
            jsonData: selected,
            success: function () {
                me.fireEvent("save");
                me.close();
            },
            complete: function (args) {
                loadMask.hide();
            }
        });
    }
});
