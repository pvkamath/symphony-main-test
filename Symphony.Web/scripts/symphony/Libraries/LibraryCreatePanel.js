﻿Symphony.Libraries.LibraryCreatePanel = Ext.define('libraries.librarycreatepanel', {
    alias: 'widget.libraries.librarycreatepanel',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;

        var libraryTypeStore = new Ext.data.ArrayStore({
            fields: ['id', 'text'],
            data: [
                [1, 'Online Course'],
                [2, 'Training Program']
            ]
        });

        Ext.apply(this, {
            layout: 'fit',
            border: false,
            frame: false,
            cls: 'x-panel-transparent',
            items: [{
                xtype: 'form',
                cls: 'x-panel-transparent',
                border: false,
                frame: false,
                ref: 'library',
                padding: 5,
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'textfield',
                    name: 'name',
                    fieldLabel: 'Name',
                    allowEmpty: false
                },
                Symphony.getCombo('libraryItemTypeId', 'Library Type', libraryTypeStore, { value: 1 })
                ]
            }],
            buttons: [{
                xtype: 'button',
                text: 'Cancel',
                iconCls: 'x-button-cancel',
                handler: function () {
                    me.fireEvent('close');
                }
            }, {
                xtype: 'button',
                text: 'Create Library',
                iconCls: 'x-button-save',
                handler: function () {
                    window.x = me;

                    var libraryData = me.library.getValues();
                    var loadMask = new Ext.LoadMask(me.getEl(), { msg: "Creating Library..." });

                    loadMask.show();

                    if (!libraryData.name) {
                        libraryData.name = 'New Library';
                    }

                    Symphony.Ajax.request({
                        method: 'post',
                        url: '/services/library.svc/library/0',
                        jsonData: libraryData,
                        success: function (args) {
                            me.fireEvent('create', args.data);
                            me.fireEvent('close');
                        },
                        complete: function () {
                            loadMask.hide();
                        }
                    });
                }
            }]
        });
        this.callParent(arguments);
    }
});
