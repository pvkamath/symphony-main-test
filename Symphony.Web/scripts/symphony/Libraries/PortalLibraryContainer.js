﻿Symphony.Libraries.PortalLibraryContainer = Ext.define('libraries.portallibrarycontainer', {
    alias: 'widget.libraries.portallibrarycontainer',
    extend: 'Ext.Panel',
    libraryId: 0,
    initComponent: function () {
        var me = this;

        me.resetGlobals();

        Ext.apply(this, {
           layout: 'card',
           activeItem: 0,
           border: false,
           frame: false,
           style: 'border: none',
           bodyStyle: 'border: none',
           defaults: {
               border: false,
               frame: false
           },
           items: [{
               xtype: 'libraries.portallibrarycategorygrid',
               libraryId: this.libraryId,
               baseFilter: this.baseFilter,
               ref: 'libraryCategoryGrid',
               border: false,
               frame: false,
               listeners: {
                   loadCategory: function (categoryId, categoryName) {
                       me.setGlobal('categoryId', categoryId);
                       me.setGlobal('activeItem', 1);
                       me.setGlobal('categoryName', categoryName);

                       me.libraryItemGrid.applyFilter({ categoryId: categoryId });

                       me.globalActiveItem = 1;
                       me.getLayout().setActiveItem(1);
                       me.getLayout().activeItem.updateTitle(categoryName);
                   }
               }
           }, {
                xtype: 'libraries.portallibraryitemgrid',
                libraryId: this.libraryId,
                libraryRecord: this.libraryRecord,
                baseFilter: this.baseFilter,
                ref: 'libraryItemGrid',
                border: false,
                frame: false,
                listeners: {
                    showCategories: function () {
                        me.resetGlobals();

                        me.getLayout().setActiveItem(0);
                    }
                }
            }]
        });
       this.callParent(arguments);
    },
    activateGrid: function () {
        var activeItem = this.getGlobal('activeItem');
        if (!activeItem) {
            activeItem = 0;
            this.setGlobal('activeItem', activeItem);
        }
        this.getLayout().setActiveItem(activeItem);
    },
    applyFilter: function (filter, search) {
        var grid = this.getLayout().activeItem;
        
        grid.store.setBaseParam("searchText", search);

        if (this.getGlobal('categoryId') > 0) {
            Ext.apply(filter, { categoryId: this.getGlobal('categoryId') });
            grid.updateTitle(this.getGlobal('categoryName'))
        } else {
            Ext.apply(filter, { categoryId: 0 });
        }

        grid.applyFilter(filter);

        //grid.store.load();
    },
    getLibraryItemGrid: function () {
        return this.libraryItemGrid;
    },
    resetGlobals: function () {
        this.setGlobal('activeItem', 0);
        this.setGlobal('categoryId', null);
        this.setGlobal('categoryName', null);
    },
    setGlobal: function (param, value) {
        if (!Symphony.Libraries.PortalLibraryContainer.global) {
            Symphony.Libraries.PortalLibraryContainer.global = {};
        }

        Symphony.Libraries.PortalLibraryContainer.global[param] = value;
    },
    getGlobal: function (param) {
        if (Symphony.Libraries.PortalLibraryContainer.global && Symphony.Libraries.PortalLibraryContainer.global[param]) {
            return Symphony.Libraries.PortalLibraryContainer.global[param];
        }

        return null;
    }
});
