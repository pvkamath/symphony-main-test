﻿Symphony.Libraries.LibraryRegistrationsTab = Ext.define('libraries.libraryregistrationstab', {
    alias: 'widget.libraries.libraryregistrationstab',
    extend: 'Ext.Panel',
    libraryId: 0,
    initComponent: function () {

        Ext.apply(this, {
            defaults: {
                border: false
            },
            layout: 'fit',
            items: [{
                xtype: 'libraries.libraryregistrationgrid',
                libraryId: this.libraryId,
                bubbleEvents: ['refreshLibraries']
            }]
        });
        this.callParent(arguments);
    }
});
