﻿Symphony.Libraries.LibrarySearchPanel = Ext.define('libraries.librarysearchpanel', {
    alias: 'widget.libraries.librarysearchpanel',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            border: false,
            frame: false,
            collapsed: true,
            style: 'padding: 0px; border: none;',
            bodyStyle: 'padding: 0px; border: none; ',
            items: [{
                xtype: 'form',
                border: false,
                ref: 'searchForm',
                frame: false,
                cls: 'x-panel-mc',
                style: 'padding: 0px; border: none',
                bodyStyle: 'padding: 0px; border: none;padding: 5px',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'textfield',
                    name: 'search',
                    fieldLabel: 'Search',
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function(cmp, e) {
                            if (e.getKey() == e.ENTER) {
                                me.fireEvent('search', cmp.getValue());
                            }
                        }
                    }
                }, {
                    xtype: 'libraries.librarysearchsuperbox',
                    name: 'authors',
                    ref: '../authors',
                    fieldLabel: 'Authors',
                    displayField: 'fullName',
                    valueField: 'id',
                    model: 'author',
                    emptyText: 'Enter or select authors.',
                    url: '/services/customer.svc/authors/',
                    boxMinHeight: 50,
                    listeners: {
                        change: function () {
                            me.filter();
                        },
                        newitem: function (bs, v) {
                            var newObj = {
                                id: 0,
                                firstName: v,
                                lastName: v,
                                fullName: v
                            };
                            bs.addItem(newObj);
                        }
                    }
                }, {
                    xtype: 'libraries.librarysearchsuperbox',
                    name: 'categories',
                    ref: '../categories',
                    fieldLabel: 'Categories',
                    displayField: 'name',
                    valueField: 'id',
                    model: 'category',
                    emptyText: 'Enter or select categories.',
                    url: '/services/category.svc/secondarycategories/online/paged/',
                    listeners: {
                        change: function () {
                            me.filter();
                        }
                    }
                }, {
                    xtype: 'daterangefield',
                    name: 'date',
                    ref: '../date',
                    fieldLabel: 'Date',
                    listeners: {
                        select: function () {
                            me.filter();
                        }
                    }
                }]
            }]
        });
        this.callParent(arguments);
    },
    filter: function () {
        var formContainer = this.query("[xtype=form]")[0];
        var form = formContainer.getForm();
        var filterValues = form.getValues();

        var categories = formContainer.query("[name=categories]")[0];
        var authors = formContainer.query("[name=authors]")[0];
        var date = formContainer.query("[name=date]")[0];

        filterValues.categories = categories.getRecords();
        filterValues.authors = authors.getRecords();
        filterValues.dateString = date.getValue();

        this.fireEvent('filter', filterValues);
    },
    reset: function () {
        this.fireEvent('reset', { categories: [], authors: [], date: '' });
    }
});
