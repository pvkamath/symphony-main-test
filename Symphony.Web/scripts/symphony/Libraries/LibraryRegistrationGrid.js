﻿Symphony.Libraries.LibraryRegistrationGrid = Ext.define('libraries.libraryregistrationgrid', {
    alias: 'widget.libraries.libraryregistrationgrid',
    extend: 'Symphony.SearchableGrid',
    libraryId: 0,
    initComponent: function () {
        var me = this;
        var url = '/services/library.svc/library/grants/{0}/'.format(me.libraryId); 


        var sm = new Ext.selection.CheckboxModel({
            checkOnly: true,
            sortable: true,
            header: ' ' // kills the select-all checkbox
        });

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                remoteSort: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer,
                sortInfo: { field: 'hierarchyTypeId', direction: 'asc' }
            },
            columns: [

                {  header: 'Type', dataIndex: 'hierarchyTypeId', renderer: Symphony.hierarchyTypeRenderer, width: 35 },
                { flex: 1, header: 'Name', dataIndex: 'levelIndentText', align: 'left' },
                { header: 'Start Date', dataIndex: 'startDate', renderer: Symphony.dateRenderer },
                {  header: 'End Date', dataIndex: 'endDate', renderer: Symphony.dateRenderer }
            ]
        });

        this.selectionPaging = new Ext.ux.grid.RowSelectionPaging();
            
        Ext.apply(this, {
            selModel: sm,
            plugins: [{ ptype: 'pagingselectpersist' }],
            tbar: {
                items: [{
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function () {
                        var w = new Symphony.Libraries.LibraryAssignmentWindow({
                            height: 560,
                            width: 500,
                            layout: 'fit',
                            libraryId: me.libraryId,
                            listeners: {
                                save: function () {
                                    me.refresh();
                                    me.fireEvent('refreshLibraries');
                                }
                            }
                        }).show();
                    }
                }, {
                    text: 'Delete',
                    iconCls: 'x-button-delete',
                    handler: function () {
                        var selected = me.getPlugin('pagingSelectionPersistence').getPersistedSelection();

                        if (selected.length == 0) {
                            Ext.Msg.alert('Nothing to Delete', 'Check the registrations to delete.');
                            return;
                        }

                        var data = [];
                        for (var i = 0; i < selected.length; i++) {
                            data.push({
                                libraryId: me.libraryId,
                                hierarchyNodeId: selected[i].get('hierarchyNodeId'),
                                hierarchyTypeId: selected[i].get('hierarchyTypeId')
                            });
                        }

                        Ext.Msg.show({
                            buttonText: {
                                cancel: '<span class="msg-btn-icon"/><img src="/images/cancel.png"/><span> Cancel</span></span>',
                                ok: '<span class="msg-btn-icon"/><img src="/images/tick.png"/><span> Delete <b>' + selected.length + '</b> Registration' + (selected.length > 1 ? 's' : '') + '</span></span>'
                            },
                            modal: true,
                            icon: Ext.Msg.WARNING,
                            title: 'Delete Registrations?',
                            msg: 'Deleting registrations will revoke access to the library for the associated users.<br/><br/>You are about to delete <b>' + selected.length + '</b> registration' + (selected.length > 1 ? 's' : '') + '.',
                            fn: function (btnId, text, opt) {
                                if (btnId == 'ok') {
                                    var loadMask = new Ext.LoadMask(me.getEl(), { msg: "Deleting registrations..." });
                                    loadMask.show();
                                    Symphony.Ajax.request({
                                        method: 'delete',
                                        url: '/services/library.svc/library/grants/' + me.libraryId + '/',
                                        jsonData: data,
                                        success: function() {
                                            me.refresh();
                                            me.fireEvent('refreshLibraries');
                                            me.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
                                        },
                                        complete: function () {
                                            loadMask.hide();
                                        }
                                    });
                                }
                            }
                        });

                    }
                }]
            },
            border: false,
            frame: true,
            idProperty: 'id',
            autoExpandColumn: 'name',
            url: url,
            colModel: colModel,
            model: 'libraryGrantHierarchy',
            bodyStyle: 'border-top: 0px; border-bottom: 0px'
        });
        this.callParent(arguments);
    }
});
