﻿Symphony.Libraries.LibraryEditor = Ext.define('libraries.libraryeditor', {
    alias: 'widget.libraries.libraryeditor',
    extend: 'Ext.Panel',
    libraryId: 0,
    libraryDetails: null,
    initComponent: function () {
        var me = this;
        console.log(me.libraryDetails);
        Ext.apply(this, {
            layout: 'border',
            defaults: {
                border: false
            },
            items: [{
                region: 'north',
                height: 30,
                items: [{
                    xtype: 'symphony.savecancelbar',
                    disableSaveCancel: me.libraryDetails.isShared,
                    listeners: {
                        save: function () {
                            return me.save();
                        },
                        cancel: function () { this.ownerCt.remove(this); } .createDelegate(this)
                    }
                }]
            }, {
                region: 'center',
                layout: 'fit',
                defaults: {
                    border: false
                },
                items: [{
                    xtype: 'tabpanel',
                    border: false,
                    bodyStyle: 'padding: 15px',
                    activeTab: 0,
                    ref: '../libraryTabs',
                    defaults: {
                        border: false
                    },
                    items: [{
                        title: 'Details', 
                        xtype: 'libraries.librarydetailstab',
                        libraryId: me.libraryId,
                        ref: '../../libraryDetails',
                        libraryDetails: me.libraryDetails
                    }, {
                        title: 'Items',
                        xtype: 'libraries.libraryitemstab',
                        libraryId: me.libraryId,
                        libraryDetails: me.libraryDetails,
                        ref: '../../libraryItems'
                    }, {
                        title: 'Registrations',
                        xtype: 'libraries.libraryregistrationstab',
                        libraryId: me.libraryId,
                        libraryDetails: me.libraryDetails,
                        ref: '../../libraryRegistrations'
                    }]
                }]
            }]
        });
        this.callParent(arguments);
    },
    save: function () {
        var library = this.libraryDetails.getData();
        var me = this;

        if (!library.cost) {
            delete library.cost;
        }

        Symphony.Ajax.request({
            url: '/services/library.svc/library/{0}'.format(me.libraryId),
            method: 'post',
            jsonData: library,
            success: function () {
                me.fireEvent('save');
            }
        });
    }
});
