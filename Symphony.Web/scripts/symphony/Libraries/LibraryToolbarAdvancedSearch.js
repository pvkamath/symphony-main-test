﻿Symphony.Libraries.LibraryToolbarAdvancedSearch = Ext.define('libraries.librarytoolbaradvancedsearch', {
    alias: 'widget.libraries.librarytoolbaradvancedsearch',
    extend: 'Ext.Toolbar',
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            name: 'searchPanel',
            border: false,
            frame: false,
            layout: {
                type: 'fit'
            },
            style: 'padding: 0px; border: none',
            bodyStyle: 'padding: 0px; border: none',
            items: [{
                xtype: 'libraries.librarysearchpanel',
                border: false,
                frame: false,
                style: 'padding: 0px; border: none',
                bodyStyle: 'padding: 0px; border: none',
                listeners: {
                    collapse: function (panel) {
                        me.fireEvent('toggle');
                        panel.reset();
                    },
                    expand: function () {
                        me.fireEvent('toggle');
                    },
                    filter: function (filterParams) {
                        me.fireEvent('filter', filterParams, false);
                    },
                    reset: function (filterParams) {
                        me.fireEvent('filter', filterParams, true);
                    },
                    search: function (searchText) {
                        me.fireEvent('search', searchText);
                    }
                }
            }]
        });

        this.callParent(arguments);
    }
});
