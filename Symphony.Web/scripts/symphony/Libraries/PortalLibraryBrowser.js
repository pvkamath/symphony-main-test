﻿Symphony.Libraries.PortalLibraryGrid = Ext.define('libraries.portallibrarybrowser', {
    alias: 'widget.libraries.portallibrarybrowser',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;
        

        Ext.apply(this, {
            layout: 'border',
            items: [{
                xtype: 'libraries.portallibraryfiltertree',
                region: 'west',
                width: 200,
                listeners: {
                    filterchanged: function (filter) {
                        var libraryItemGrid = me.query('[xtype=libraries.portallibraryitemgrid]')[0];

                        libraryItemGrid.applyFilter(filter);
                    }
                }
            }, {
                xtype: 'libraries.portallibraryitemgrid',
                url: '/services/library.svc/library/items/',
                hideHeader: true,
                region: 'center'
            }]
        });
        this.callParent(arguments);
    }
});
