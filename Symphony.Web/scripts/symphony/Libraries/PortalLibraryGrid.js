﻿Symphony.Libraries.PortalLibraryGrid = Ext.define('libraries.portallibrarygrid', {
    alias: 'widget.libraries.portallibrarygrid',
    extend: 'Symphony.SearchableGrid',
    initComponent: function () {
        var me = this;
        var url = '/services/library.svc/libraries/grants/user/{0}'.format(Symphony.User.id);


        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [
                {
                  header: 'Name', dataIndex: 'name', align: 'left', renderer: function (value) {
                        return '<a href="#">' + value + '</a>'
                    }},
                { header: 'Items', dataIndex: 'itemCount', width: 50, align: 'right' },    
                { header: 'Available Until', dataIndex: 'endDate', width: 60, renderer: Symphony.dateRenderer }
            ]
        });

        Ext.apply(this, {
            idProperty: 'id',
            autoExpandColumn: 'name',
            url: url,
            colModel: colModel,
            model: 'libraryGrantLibrary',
            listeners: {
                cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e) {
                    var record = grid.getStore().getAt(rowIndex);
                    var fieldName = grid.getColumnModel().getDataIndex(columnIndex);
                    var data = record.get(fieldName);

                    if (fieldName == 'name') {
                        var libraryDetailsWindow = new Ext.Window({
                            width: 800,
                            height: 600,
                            title: 'Library: ' + record.get('name'),
                            layout: 'fit',
                            modal: true,
                            items: [{
                                xtype: 'libraries.portallibrarydetailspanel',
                                libraryRecord: record,
                                libraryId: record.get('id'),
                                activeFilter: me.activeFilter
                            }]
                        }).show();
                    }
                }
            }
        });
        this.callParent(arguments);
    }
});
