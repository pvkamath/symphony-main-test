﻿Symphony.Libraries.PortalLibraryFilterTree = Ext.define('libraries.portallibraryfiltertree', {
    alias: 'widget.libraries.portallibraryfiltertree',
    extend: 'symphony.searchabletree',
    initComponent: function () {
        var me = this;
        var url = '/services/library.svc/library/filters/{0}'.format(Symphony.User.id);

        Ext.apply(this, {
            border: false,
            rootVisible: false,
            store: new Ext.data.TreeStore({
                model: 'libraryFilterOption',
                proxy: {
                    type: 'ajax',
                    url: url,
                    reader: new Ext.data.JsonReader({
                        idProperty: this.idProperty || null,
                        totalProperty: 'totalSize',
                        root: 'data'
                    })
                },
                root: { expanded: true, loaded: true }
            }),
            columns: [{
                xtype: 'treecolumn',
                text: 'Filters',
                flex: 1,
                dataIndex: 'displayName'
            }],
            searchField: 'displayName',
            listeners: {
                checkchange: function(node, checked) {
                    var selected = me.getView().getChecked();
                    var filter = {};

                    if (me.currentFilter) { // To clear filter after all elements are unchecked.
                        for (var name in me.currentFilter) {
                            if (me.currentFilter.hasOwnProperty(name)) {
                                filter[name] = null;
                            }
                        }
                    }

                    for (var i = 0; i < selected.length; i++) {
                        if (!filter[selected[i].get('name')]) {
                            filter[selected[i].get('name')] = [];
                        }
                        filter[selected[i].get('name')].push(selected[i].get('value'));
                    }

                    me.currentFilter = filter;

                    me.fireEvent('filterchanged', filter);
                }
            }
        });

        this.callParent(arguments);
    }
});
