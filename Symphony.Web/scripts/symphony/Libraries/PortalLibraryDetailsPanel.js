﻿Symphony.Libraries.PortalLibraryDetailsPanel = Ext.define('libraries.portallibrarydetailspanel', {
    alias: 'widget.libraries.portallibrarydetailspanel',
    extend: 'Ext.Panel',
    libraryId: 0,
    libraryRecord: {},
    filterParams: {},
    initComponent: function () {
        var me = this;

        var detailsTemplate = new Ext.Template([
            '<div class="library-details">',
                '<h2 class="library-name">{name}</h2>',
                '<div class="library-description">',
                    '<p>{description}</p>',
                    '{details}',
                '</div>',
            '</div>'
        ]);

        Ext.apply(this, {
            searchActive: false,
            searchText: '',
            layout: {
                type: 'hbox',
                flex: 1,
                align: 'stretch',
                pack: 'start'
            },border: false,
            frame: false,
            style: 'border: none',
            bodyStyle: 'border: none',
            defaults: {
                border: false,
                frame: false,
                style: 'border: none',
                bodyStyle: 'border: none'
            },
            items: [{
                xtype: 'panel',
                flex: 1,
                border: false,
                frame: false,
                style: 'border: none',
                bodyStyle: 'border: none',
                layout: {
                    type: 'vbox',
                    flex: 1, 
                    align: 'stretch', 
                    pack: 'start'
                },
                defaults: {
                    border: false,
                    frame: false,
                    style: 'border: none',
                    bodyStyle: 'border: none'
                },
                items: [{
                    xtype: 'panel',
                    html: detailsTemplate.apply(me.libraryRecord.data),
                    frame: false,
                    border: false,
                    cls: 'x-panel-mc',
                    flex: 0.25
                }, {
                    xtype: 'tabpanel',
                    flex: 0.75,
                    ref: '../itemGrids',
                    style: 'border: none;',
                    bodyStyle: 'border: none',
                    cls: 'x-panel-mc',
                    defaults: {
                        border: false,
                        frame: false,
                        style: 'border: none',
                        bodyStyle: 'border: none'
                    },
                    tbar: new Ext.Panel({
                        defaults: {
                            border: false,
                            frame: false
                        },
                        border: false,
                        frame: false,
                        items: [{
                            xtype: 'toolbar',
                            items: [{
                                xtype: 'button',
                                text: 'Advanced Search',
                                iconCls: 'x-button-advanced-search',
                                handler: function () {
                                    var topToolbar = me.itemGrids.getDockedItems("panel[dock=top]")[0];
                                    var searchPanel = topToolbar.query('[xtype=libraries.librarysearchpanel]')[0];

                                    searchPanel.toggleCollapse(true);

                                    this.toggle();
                                }
                            }]
                        }, {
                            xtype: 'libraries.librarytoolbaradvancedsearch',
                            border: false,
                            frame: false,
                            listeners: {
                                toggle: function (panel) {
                                    me.doLayout();
                                },
                                filter: function (filterParams, isReset) {
                                    var container = me.findLibraryContainer();
                                    
                                    if (container) {
                                        me.searchActive = true;
                                        
                                        if (isReset) {
                                            me.searchText = '';
                                        }

                                        Ext.apply(me.filterParams, filterParams);

                                        container.applyFilter(me.filterParams, me.searchText);
                                    }
                                },
                                search: function (searchText) {
                                    me.searchActive = true;
                                    me.searchText = searchText;

                                    var container = me.findLibraryContainer();
                                    if (container) {
                                        container.applyFilter(me.filterParams, me.searchText);
                                    }
                                }
                            }
                        }]
                    }),
                    border: false,
                    frame: false,
                    activeTab: 0,
                    style: 'border: none;',
                    bodyStyle: 'border: none;',
                    cls: 'x-panel-mc',
                    items: [{
                        xtype: 'panel',
                        title: 'All',
                        layout: 'fit',
                        border: false,
                        frame: false,
                        style: 'border: none',
                        bodyStyle: 'border: none',
                        listeners: {
                            activate: me.applyFilter.createDelegate(me)
                        },
                        items: [{
                            style: 'border: none',
                            bodyStyle: 'border: none; background: none',
                            border: false,
                            frame: false,
                            xtype: 'libraries.portallibrarycontainer',
                            baseFilter: {},
                            libraryId: me.libraryId,
                            libraryRecord: me.libraryRecord
                        }]
                    }, {
                        xtype: 'panel',
                        title: 'Favorites',
                        layout: 'fit',
                        border: false,
                        frame: false,
                        style: 'border: none',
                        bodyStyle: 'border: none',
                        listeners: {
                            activate: me.applyFilter.createDelegate(me)
                        },
                        items: [{
                            border: false,
                            frame: false,
                            style: 'border: none',
                            bodyStyle: 'border: none',
                            xtype: 'libraries.portallibrarycontainer',
                            baseFilter: { isFavorite: true },
                            libraryId: me.libraryId,
                            libraryRecord: me.libraryRecord
                        }]
                    }, {
                        xtype: 'panel',
                        title: 'In Progress',
                        layout: 'fit',
                        border: false,
                        frame: false,
                        style: 'border: none',
                        bodyStyle: 'border: none',
                        listeners: {
                            activate: me.applyFilter.createDelegate(me)
                        },
                        items: [{
                            border: false,
                            frame: false,
                            style: 'border: none',
                            bodyStyle: 'border: none',
                            xtype: 'libraries.portallibrarycontainer',
                            baseFilter: { isInProgress: true },
                            libraryId: me.libraryId,
                            libraryRecord: me.libraryRecord
                        }]
                    }, {
                        xtype: 'panel',
                        title: 'Not Started',
                        layout: 'fit',
                        border: false,
                        frame: false,
                        style: 'border: none',
                        bodyStyle: 'border: none',
                        listeners: {
                            activate: me.applyFilter.createDelegate(me)
                        },
                        items: [{
                            border: false,
                            frame: false,
                            style: 'border: none',
                            bodyStyle: 'border: none',
                            xtype: 'libraries.portallibrarycontainer',
                            baseFilter: { isNotStarted: true },
                            libraryId: me.libraryId,
                            libraryRecord: me.libraryRecord
                        }]
                    }]
                }]
            }]
        });
        this.callParent(arguments);
    },
    findLibraryContainer: function (item) {
        
        var activeItem = item ? item : this.itemGrids.getActiveTab();

        if (activeItem) {
            var containers = activeItem.find('xtype', 'libraries.portallibrarycontainer');
            if (containers.length) {
                return containers[0];
            }
        }

        return null;
    },
    applyFilter: function (panel) {
        var container = this.findLibraryContainer(panel);
   
        container.activateGrid();


        container.applyFilter(this.filterParams, this.searchText);

    }
});
