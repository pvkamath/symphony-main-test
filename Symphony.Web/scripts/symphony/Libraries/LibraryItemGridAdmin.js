﻿Symphony.Libraries.LibraryItemGridAdmin = Ext.define('libraries.libraryitemgridadmin', {
    alias: 'widget.libraries.libraryitemgridadmin',
    extend: 'Symphony.Libraries.LibraryItemGrid',
    search: '',
    selectedRecords: {},
    initComponent: function () {
        var me = this;
        
        var sm = new Ext.selection.CheckboxModel({
            checkOnly: true,
            sortable: true,
            header: ' ', // kills the select-all checkbox
            listeners: {
                select: function (grid, r, index, opts) {
                    me.selectedRecords[r.get('id')] = {
                        libraryId: me.libraryId,
                        id: r.get('id'),
                        libraryItemTypeId: r.get('libraryItemTypeId'),
                        isInLibrary: r.get('isInLibrary'),
                        name: r.get('name')
                    };
                },
                deselect: function (grid, r, index, opts) {
                    delete me.selectedRecords[r.get('id')];
                }
            }
        });

        var rowSelection = new Ext.selection.RowModel();

        var colModel = {
            columns: [{ header: 'Type', dataIndex: 'displayLibraryItemType', align: 'center', renderer: Symphony.Renderer.displayIconRenderer },
                { header: 'Name', dataIndex: 'name', align: 'left', flex: 1 },
                {
                    header: 'Date Added', dataIndex: 'createdOn', align: 'center', renderer: function (value, meta, record) {
                        if (record.get('isInLibrary')) {
                            return Symphony.Renderer.displayDateRenderer(record.get('displayCreatedOn'));
                        }
                        return '';
                }
                },
                {
                    header: 'Created', dataIndex: 'itemCreatedOn', align: 'center', renderer: function (value, meta, record) {
                        return Symphony.Renderer.displayDateRenderer(record.get('displayItemCreatedOn'));
                    }
                },
                {
                    header: 'Modified', dataIndex: 'itemModifiedOn', align: 'center', renderer: function (value, meta, record) {
                        return Symphony.Renderer.displayDateRenderer(record.get('displayItemModifiedOn'));
                    }
                },
                {
                    header: 'Version', dataIndex: 'courseVersion', align: 'center', render: function (value, meta, record) {
                        if (value) {
                            return value;
                        }
                        return '';
                    }
                },
                { header: 'Favorite', dataIndex: 'displayIsFavorite', align: 'center', renderer: Symphony.Renderer.displayIconRenderer },
                { header: 'In Library', dataIndex: 'displayInLibrary', align: 'center', renderer: Symphony.Renderer.displayIconRenderer }
            ]
        };

        //this.selectionPaging = new Ext.ux.grid.RowSelectionPaging();

        Ext.apply(this, {
            selModel: me.libraryDetails.isShared ? rowSelection : sm,
            frame: true,
            plugins: [{ ptype: 'pagingselectpersist' }],
            tbar: new Ext.Panel({
                defaults: {
                    border: false,
                    frame: false
                },
                border: false,
                frame: false,
                bodyStyle: 'border: none',
                items: [{
                    xtype: 'toolbar',
                    border: false,
                    frame: false,
                    style: 'border: none',
                    bodyStyle: 'border: none',
                    items: [{
                        text: 'Filter: All',
                        name: 'filter',
                        iconCls: 'x-button-filter',
                        textAlign: 'left',
                        cls: 'x-filter',
                        width: 130,
                        disabled: me.libraryDetails.isShared,
                        menu: {
                            items: [{
                                text: 'All',
                                checked: false,
                                group: 'filter',
                                name: 'all',
                                applyFilter: { isShowAll: true, isHideInLibrary: false, libraryItemTypeId: me.libraryDetails ? me.libraryDetails.libraryItemTypeId : null},
                                checkHandler: me.filterChecked.createDelegate(me)
                            }, {
                                text: 'In Library',
                                checked: false,
                                group: 'filter',
                                name: 'inLibrary',
                                applyFilter: { isShowAll: false, isHideInLibrary: false, libraryItemTypeId: me.libraryDetails ? me.libraryDetails.libraryItemTypeId : null},
                                checkHandler: me.filterChecked.createDelegate(me)
                            }, {
                                text: 'Not In Library',
                                checked: false,
                                group: 'filter',
                                name: 'notInLibrary',
                                applyFilter: { isShowAll: true, isHideInLibrary: true, libraryItemTypeId: me.libraryDetails ? me.libraryDetails.libraryItemTypeId : null},
                                checkHandler: me.filterChecked.createDelegate(me)
                            }]
                        }
                    },
                    {
                        xtype: 'button',
                        name: 'advanced',
                        text: 'Advanced Search',
                        iconCls: 'x-button-advanced-search',
                        handler: function () {
                            me.getDockedItems("panel[dock=top]")[0].find('xtype', 'libraries.librarysearchpanel')[0].toggleCollapse(true);
                            this.toggle();
                        }
                    },
                    '->']
                }, {
                    xtype: 'libraries.librarytoolbaradvancedsearch',
                    listeners: {
                        toggle: function (panel) {
                            me.doLayout();
                        },
                        filter: function (filterParams, isReset) {
                            if (isReset) {
                                me.store.setBaseParam('searchText', '');
                            }
                            Ext.apply(me.activeFilter, filterParams);
                            me.searchOptionClicked({ filter: me.activeFilter }, true);
                        },
                        search: function (searchText) {
                            me.performSearch(searchText);
                        }
                    }
                }, {
                    xtype: 'toolbar',
                    disabled: me.libraryDetails.isShared,
                    items: [{
                        xtype: 'button',
                        text: 'Add Selected to Library',
                        iconCls: 'x-button-add',
                        tooltip: {
                            text: 'Adds the selected items to the current library. Select "Not in Library" in the filter menu above to display items that are not in this library.'
                        },
                        handler: function () {
                            var selected = me.getPlugin('pagingSelectionPersistence').getPersistedSelection()

                            if (selected.length == 0) {
                                Ext.Msg.alert('Nothing to add', 'Select at least one item to add to the library.');
                                return;
                            }

                            var data = [];
                            for (var i = 0; i < selected.length; i++) {
                                if (!me.selectedRecords[selected[i].get('id')].isInLibrary) {
                                    data.push(me.selectedRecords[selected[i].get('id')]);
                                }
                            }

                            if (data.length == 0) {
                                Ext.Msg.alert('Nothing to add', 'The items selected are already included in this library.');
                                return;
                            }
                            var loadMask = new Ext.LoadMask(me.getEl(), { msg: "Adding items..." });
                            loadMask.show();
                            Symphony.Ajax.request({
                                method: 'post',
                                url: '/services/library.svc/library/' + me.libraryId + '/items/',
                                jsonData: data,
                                success: function (args) {
                                    
                                    var w = new Symphony.Libraries.LibraryItemConfirmationWindow({
                                        title: 'Items Added',
                                        message: 'The following items have been added to the library:',
                                        messageFlex: 0.1,
                                        libraryItems: args.data,
                                        buttons: [{
                                            xtype: 'button',
                                            iconCls: 'x-button-save',
                                            text: 'Ok',
                                            handler: function () {
                                                w.close();
                                            }
                                        }],
                                        listeners: {
                                            close: function () {
                                                me.clearSelected();
                                            }
                                        }
                                    }).show();

                                },
                                complete: function () {
                                    loadMask.hide();
                                }
                            });
                        }
                    }, {
                        xtype: 'button',
                        text: 'Remove Selected from Library',
                        iconCls: 'x-button-delete',
                        tooltip: {
                            text: 'Removes the selected items from the current library. Select "In Library" in the filter menu above to display items that are current in this library.'
                        },
                        handler: function () {
                            var selected = me.getPlugin('pagingSelectionPersistence').getPersistedSelection();

                            if (selected.length == 0) {
                                Ext.Msg.alert('Nothing to delete', 'Select at least one item to delete from the library.');
                                return;
                            }

                            var data = [];
                            for (var i = 0; i < selected.length; i++) {
                                if (me.selectedRecords[selected[i].get('id')].isInLibrary) {
                                    data.push(me.selectedRecords[selected[i].get('id')]);
                                }
                            }

                            if (data.length == 0) {
                                Ext.Msg.alert('Nothing to delete', 'The items selected are not included in this library.');
                                return;
                            }

                            var w = new Symphony.Libraries.LibraryItemConfirmationWindow({
                                title: 'Delete the following items?',
                                message: 'Deleting items will remove them from the library. Users registered will no longer have access to these items.<br/><br/>You are about to delete the following items:',
                                libraryItems: data,
                                buttons: [{
                                    xtype: 'button',
                                    iconCls: 'x-button-cancel',
                                    text: 'Cancel',
                                    handler: function () {
                                        w.close();
                                    }
                                }, {
                                    xtype: 'button',
                                    iconCls: 'x-button-save',
                                    text: 'Delete <b>{0}</b> item{1}.'.format(data.length, data.length > 1 ? 's' : ''),
                                    handler: function () {
                                        w.close();
                                        var loadMask = new Ext.LoadMask(me.getEl(), { msg: "Deleting items..." });
                                        loadMask.show();
                                        Symphony.Ajax.request({
                                            method: 'delete',
                                            url: '/services/library.svc/library/' + me.libraryId + '/items/',
                                            jsonData: data,
                                            success: function (data) {
                                                me.clearSelected();
                                            },
                                            complete: function () {
                                                loadMask.hide();
                                            }
                                        });
                                    }
                                }]
                            }).show();
                        }
                    }]
                }]
            }),
            colModel: colModel,
            listeners: {
                afterrender: function () {
                    if (me.libraryDetails.isShared) {
                        var inLibrary = me.getDockedItems("panel[dock=top]")[0].find('name', 'filter')[0].menu.items.find(function (item) {
                            return item.name == 'inLibrary';
                        });
                        inLibrary.setChecked(true);
                    } else {
                        var allItems = me.getDockedItems("panel[dock=top]")[0].find('name', 'filter')[0].menu.items.find(function (item) {
                            return item.name == 'all';
                        });
                        allItems.setChecked(true);
                    }
                }
            }
        });

        this.callParent(arguments);
    },
    filterCheckedCallback: function (item) {
        this.getDockedItems("panel[dock=top]")[0].find('name', 'filter')[0].setText('Filter: ' + item.text);
    },
    clearSelected: function() {
        this.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
        this.selectedRecords = {};
        this.refresh();
    }
});
