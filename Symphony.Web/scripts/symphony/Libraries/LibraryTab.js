﻿Symphony.Libraries.LibraryTab = Ext.define('libraries.librarytab', {
    alias: 'widget.libraries.librarytab',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            layout: 'border',
            defaults: {
                border: false
            },
            listeners: {
                refreshLibraries: function () {
                    me.libraryGrid.refresh();
                },
                activate: {
                    fn: function (panel) {
                        panel.query('[xtype=libraries.librarygrid]')[0].refresh();
                    },
                    single: true
                }
            },
            items: [{
                split: true,
                region: 'west',
                xtype: 'libraries.librarygrid',
                width: 400,
                ref: 'libraryGrid',
                listeners: {
                    itemclick: function (grid, record, index, e, eOpts) {
                        //me.addPanel(record.get('id'), record.get('name'), record.get('description'), record.data);
                        Symphony.Ajax.request({
                            url: '/services/library.svc/library/{0}'.format(record.get('id')),
                            method: 'GET',
                            success: function (r) {
                                if (r.success) {
                                    me.addPanel(record.get('id'), record.get('name'), record.get('description'), r.data);
                                } else {
                                    Ext.Msg.alert(r.error);
                                }
                            }
                        });
                        
                    },
                    addclick: function () {
                        var createWindow = new Ext.Window({
                            width: 300,
                            height: 125,
                            layout: 'fit',
                            title: 'Create Library',
                            modal: true,
                            border: false,
                            frame: true,
                            items: [{
                                xtype: 'libraries.librarycreatepanel',
                                listeners: {
                                    create: function (data) {
                                        me.addPanel(data.id, data.name, data.description, data);
                                        me.refresh();
                                    },
                                    close: function () {
                                        createWindow.close();
                                    }
                                }
                            }]
                        }).show();
                    }
                }
            }, {
                region: 'center',
                xtype: 'tabpanel',
                ref: 'libraryView',
                border: false,
                defaults: {
                    border: false
                }
            }]
        });
        this.callParent(arguments);
    },
    addPanel: function (id, name, description, libraryDetails) {
        var me = this;
        var tabPanel = me.libraryView;

        var found = tabPanel.queryBy(function (p) { return p.libraryId == id; })[0];
        if (found) {
            tabPanel.activate(found);
        } else {
            var panel = tabPanel.add({
                xtype: 'libraries.libraryeditor',
                closable: true,
                activate: true,
                //id: id,
                title: name,
                tabTip: description,
                libraryId: id,
                libraryDetails: libraryDetails,
                listeners: {
                    save: function () {
                        me.refresh();
                    }
                }
            });
            panel.show();
        }
        tabPanel.doLayout();
    },
    refresh: function () {
        this.find('xtype', 'libraries.librarygrid')[0].refresh();
    }
});
