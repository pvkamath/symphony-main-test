﻿Symphony.Libraries.LibraryCategoryGrid = Ext.define('libraries.portallibrarycategorygrid', {
    alias: 'widget.libraries.portallibrarycategorygrid',
    extend: 'Symphony.SearchableGrid',
    libraryId: 0,
    initComponent: function () {
        var me = this;
        var url = '/services/library.svc/library/{0}/categories/'.format(me.libraryId);
        
        me.activeFilter = {};


        var colModel = new Ext.grid.ColumnModel(Ext.apply({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [
                {
                    flex: 1, header: 'Name', dataIndex: 'levelIndentText', align: 'left', renderer: function (value) {
                        return '<a href="#">' + value + '</a>';
                    }},
                { header: 'Total', dataIndex: 'totalItems',  align: 'center' }
            ]
        }, me.colModel));

        Ext.apply(this, {
            border: false,
            frame: false,
            bodyStyle: 'padding: 0px; border-bottom: none',
            defaults: {
                border: false,
                frame: false
            },
            idProperty: 'id',
            url: url,
            colModel: colModel,
            model: 'libraryCategory',
            title: 'Categories',
            deferLoad: true,
            listeners: {
                render: function () {
                    if (me.baseFilter) {
                        Ext.apply(me.activeFilter, me.baseFilter);
                        //me.searchOptionClicked({ filter: me.activeFilter }, true);
                    }
                },
                cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e) {
                    var record = grid.getStore().getAt(rowIndex);  // Get the Record
                    var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                    var data = record.get(fieldName);

                    if (fieldName == 'levelIndentText') {
                        me.fireEvent('loadCategory', record.get('id'), data);
                    }
                }
            }
        });

        this.callParent(arguments);
    },
    applyFilter: function (filter) {
        Ext.apply(this.activeFilter, filter);
        this.searchOptionClicked({ filter: this.activeFilter }, true);
    }
});
