﻿Symphony.Libraries.LibraryItemGrid = Ext.define('libraries.libraryitemgrid', {
    alias: 'widget.libraries.libraryitemgrid',
    extend: 'Symphony.SearchableGrid',
    libraryId: 0,
    initComponent: function () {
        var me = this;
        var url = this.url ? this.url : '/services/library.svc/library/{0}/items/'.format(me.libraryId);
        
        me.activeFilter = {};


        var colModel = new Ext.grid.ColumnModel(Ext.apply({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [
                { header: 'Type', dataIndex: 'displayLibraryItemType', align: 'center', renderer: Symphony.Renderer.displayIconRenderer },
                { header: 'Name', dataIndex: 'name', align: 'left', flex: 1 },
                { header: 'Favorite', dataIndex: 'displayIsFavorite',  align: 'center', renderer: Symphony.Renderer.displayIconRenderer },
                { header: 'In Library', dataIndex: 'displayInLibrary', align: 'center', renderer: Symphony.Renderer.displayIconRenderer }
            ]
        }, me.colModel));

        Ext.apply(this, {
            border: false,
            frame: me.frame === false ? false : true,
            bodyStyle: 'padding: 0px; border-bottom: none',
            defaults: {
                border: false,
                frame: false
            },
            url: url,
            colModel: colModel,
            model: 'libraryItem'
        });

        this.callParent(arguments);
    },
    applyFilter: function (filter) {
        Ext.apply(this.activeFilter, filter);
        this.searchOptionClicked({ filter: this.activeFilter }, true);
    },
    filterChecked: function (item, checked) {
        if (checked) {
            var toptoolbar = this.getTopToolbar();

            var f = item.applyFilter;
            Ext.apply(this.activeFilter, f);
            item.filter = this.activeFilter;

            this.searchOptionClicked(item, checked);

            if (typeof(this.filterCheckedCallback) == 'function') {
                this.filterCheckedCallback(item);
            }
        }
    }
});
