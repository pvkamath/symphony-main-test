﻿Symphony.Libraries.LibraryDetailsTab = Ext.define('libraries.librarydetailstab', {
    alias: 'widget.libraries.librarydetailstab',
    extend: 'Ext.Panel',
    libraryId: 0,
    libraryDetails: {},
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            layout: 'fit',
            items: [{
                xtype: 'form',
                border: false,
                frame: true,
                bodyStyle: 'padding: 5px',
                ref: 'detailsForm',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'textfield',
                    name: 'name',
                    fieldLabel: 'Name',
                    allowEmpty: false
                }, {
                    xtype: 'textarea',
                    name: 'description',
                    fieldLabel: 'Description'
                }, {
                    name: 'associatedImageData',
                    fieldLabel: 'Image',
                    xtype: 'associatedimagefield'
                }, {
                    xtype: 'htmleditor',
                    name: 'details',
                    fieldLabel: 'Library Details',
                    ref: '../details'
                }, {
                    xtype: 'numberfield',
                    name: 'cost',
                    fieldLabel: 'Cost'
                }, {
                    xtype: 'textfield',
                    name: 'sku',
                    fieldLabel: 'Sku'
                }]
            }],
            listeners: {
                afterrender: function () {
                    me.load(me.libraryDetails);

                    if (me.libraryDetails.isShared) {
                        var formFields = me.detailsForm.getForm().getFields().items;
                        for (var i = 0; i < formFields.length; i++) {
                            formFields[i].setDisabled(true);
                        }
                    }
                }
            }
        });
        this.callParent(arguments);
    },
    load: function() {
        this.detailsForm.bindValues(this.libraryDetails);
    },
    getData: function () {
        var formData = this.detailsForm.getForm().getValues();
        var detailsEditor = this.find('name', 'details')[0];
        
        Ext.apply(this.libraryDetails, formData);
        return this.libraryDetails;
    }
});
