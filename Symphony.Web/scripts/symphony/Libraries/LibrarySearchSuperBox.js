﻿(function () {
    Symphony.Libraries.LibrarySearchSuperBox = Ext.define('libraries.librarysearchsuperbox', {
        alias: 'widget.libraries.librarysearchsuperbox',
        extend: 'Ext.ux.form.field.BoxSelect',
        selectedItemStore: {},
        model: {},
        url: '',
        emptyText: 'Enter or select items.',
        displayField: '',
        valueField: '',
        initComponent: function () {
            var me = this;
            //var recordDef = Symphony.Definitions.author;

            me.selectedItemStore = new Ext.data.Store({
                remoteSort: false,
                model: me.model,
                reader: new Ext.data.JsonReader({
                    idProperty: 'id'
                }, me.recordDef)
            }); 

            var listeners = {
                beforeadditem: function (bs, v) {
                    //console.log('beforeadditem:', v);
                    //return false;
                },
                additem: function (bs, v, record) {
                    me.selectedItemStore.add(record);
                    me.fireEvent('change');
                },
                beforeremoveitem: function (bs, v) {
                    //console.log('beforeremoveitem:', v);
                    //return false;
                },
                removeitem: function (bs, v, record, item) {
                    var display = item.caption;
                    var index = me.selectedItemStore.findExact(me.displayField, display);

                    me.selectedItemStore.removeAt(index);

                    me.fireEvent('change');
                },
                newitem: function (bs, v) {
                    var newObj = {
                        id: 0,
                        name: v
                    };
                    bs.addItem(newObj);
                }
            };

            if (me.listeners) {
                Ext.apply(listeners, me.listeners);
            }

            Ext.apply(this, {
                mode: 'local',
                triggerAction: 'all',
                queryDelay: 0,
                minChars: 1,
                store: new Ext.data.JsonStore({
                    root: 'data',
                    remote: true,
                    model: me.model,
                    autoLoad: true,
                    proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: me.url
                    })
                }),
                allowBlank: true,
                //msgTarget: 'under',
                allowAddNewData: true,
                    
                //fieldLabel: 'Tags',
                emptyText: me.emptyText,
                resizable: true,
                anchor: '100%',
                displayField: me.displayField,
                valueField: me.valueField,
                //value: 'Sport,Science',
                extraItemCls: 'x-tag' + (me.readOnly ? ' read-only' : ''),
                renderFieldBtns: !me.readOnly,
                backspaceDeletesLastItem: !me.readOnly,
                itemCls: me.readOnly ? 'read-only' : '',
                listeners: listeners
            });
            this.callParent(arguments);
        },
        setValue: function (v) {
            Symphony.Libraries.LibrarySearchSuperBox.superclass.setValue.apply(this, arguments);
        },
        getRecords: function () {
            var items = this.selectedItemStore.getRange();
            var data = [];

            for (var i = 0; i < items.length; i++) {
                data.push(items[i].data);
            }

            return data;
        }
    });

})();