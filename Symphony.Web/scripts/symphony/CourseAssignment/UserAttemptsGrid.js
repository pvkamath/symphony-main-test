﻿
Symphony.CourseAssignment.UserAttemptsGrid = Ext.define('courseassignment.userattemptsgrid', {

    alias: 'widget.courseassignment.userattemptsgrid',

    extend: 'symphony.searchablegrid',
    trainingProgramId: 0,
    onlineCourseId: 0,
    initComponent: function () {
        var me = this;
        var url = '/services/CourseAssignment.svc/trainingprograms/' + this.trainingProgramId + '/onlinecourserollups/' + this.onlineCourseId;

        var lockedOnlineCourseRollup = Symphony.clone(Symphony.Definitions.onlineCourseRollup);
        lockedOnlineCourseRollup.push({ name: 'courseName' });
        lockedOnlineCourseRollup.push({ name: 'testAttemptCount' });

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center'
            },
            columns: [
                {
                    /*id: 'username',*/ header: 'User', dataIndex: 'username', align: 'left', renderer: Symphony.Portal.valueRenderer, width: 125,
                    flex: 1
                },
                { header: 'Attempts', dataIndex: 'attemptCount', renderer: Symphony.Portal.valueRenderer, width: 65 },
                {
                    header: 'Clear Lock',
                    /*id: 'reset',*/
                    width: 60,
                    renderer: function (value, meta, record) {
                        var html = Symphony.qtipRenderer('Clear', 'Clearing a lock allows a student to resume course testing when have reached the maximum attempts for a course. This will effectively set the course attempts to be zero.');
                        return '<a href="#">' + html + '</a>';
                    }
                }, {
                    header: 'Reset',
                    /*id: 'delete',*/
                    width: 60,
                    renderer: function (value, meta, record) {
                        var html = Symphony.qtipRenderer('Reset', 'Resetting a course for a student will remove any saved progress for a course. The student will start at the beginning with the newest version of the course the next time they access the course.');
                        return '<a href="#">' + html + '</a>';
                    }
                }
            ]
        });

        Ext.apply(this, {
            listeners: {
                cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                    var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex;
                    var columnId = grid.headerCt.getGridColumns()[columnIndex].id;
                    var record = grid.getStore().getAt(rowIndex);
                    var onlineCourseRollupId = record.get('id');
                    var action = '';

                    if (columnId == 'reset') {
                        action = 'resetattempts';
                    } else if (columnId == 'delete') {
                        action = 'deleteregistration';
                    }

                    if (action) {
                        Ext.Msg.prompt('Notes Required', 'Please explain the reason for this change:', function (btn, notes) {
                            if (btn == 'ok' && notes.trim().length > 0) {
                                Symphony.Ajax.request({
                                    url: '/services/courseassignment.svc/' + action + '/' + onlineCourseRollupId + '?notes=' + encodeURIComponent(notes) + '&ca=false',
                                    method: 'POST',
                                    success: function () {
                                        me.store.reload();
                                        me.fireEvent('change');
                                    }
                                });
                            } else if (btn == 'ok') {
                                Ext.Msg.hide();
                                Ext.Msg.alert('Notes are required', 'You must enter notes before clearing or resetting a user.');
                            }
                        }, this, true);
                    }
                }
            },
            idProperty: 'id',
            url: url,
            model: 'onlineCourseRollup',
            colModel: colModel
        });
        this.callParent(arguments);
    }
});
