﻿Symphony.CourseAssignment.SessionAssignmentGrid = Ext.define('courseassignment.sessionassignmentgrid', {
    alias: 'widget.courseassignment.sessionassignmentgrid',
    extend: 'symphony.searchablegrid',
    sessionCourseId: null,
    trainingProgramIds: [],
    userIds: [],
    dateRangeString: 'none',
    dateRangeDisplay: '',
    showActive: null,
    showInRangeOnly: null,
    registrationStatusId: null,
    selectedStudents: [],
    url: '/services/courseassignment.svc/sessions/assignedusers/',
    getBaseParams: function() {
        return [
            { name: 'limit', value: 20 },
            { name: 'trainingProgramIds', value: this.trainingProgramIds.join(',') },
            { name: 'sessionCourseId', value: this.sessionCourseId },
            { name: 'dateRange', value: this.dateRangeString },
            { name: 'showActive', value: this.showActive },
            { name: 'showInRangeOnly', value: this.showInRangeOnly },
            { name: 'registrationStatusId', value: this.registrationStatusId },
            { name: 'userIds', value: this.userIds.length > 0 ? this.userIds.join(',') : 'all' }
        ]
    },
    getBaseParamsObject: function() {
        var baseParams = this.getBaseParams(),
            params = {};
        
        for (var i = 0; i < baseParams.length; i++) {
            params[baseParams[i].name] = baseParams[i].value;
        }

        return params;
    },
    setBaseParams: function(store) {
        var baseParams = this.getBaseParams();
        for (var i = 0; i < baseParams.length; i++) {
            store.setBaseParam(baseParams[i].name, baseParams[i].value);
        }
    },
    formatStartEndDate: function (startDate, endDate) {
        var shortStartDate = Symphony.parseDate(startDate).formatSymphony('m/d/Y');
        var startTime = Symphony.parseDate(startDate).formatSymphony('g:i a');

        var shortEndDate = Symphony.parseDate(endDate).formatSymphony('m/d/Y');
        var endTime = Symphony.parseDate(endDate).formatSymphony('g:i a');

        var timeString = "";

        if (shortStartDate == shortEndDate) {
            timeString = shortStartDate + " " + startTime + " to " + endTime;
        } else {
            timeString = shortStartDate + " to " + shortEndDate;
        }

        return timeString;
    },
    formatSessionName: function(name, startDate, endDate) {
        var timeString = this.formatStartEndDate(startDate, endDate);

        return String.format('<span class="session-group{2}">Session: {0}</span><br/><span class="session-time-group">{1}</span>', name, timeString, this.userIds.length ? '' :  'title')
    },
    initComponent: function () {
        var me = this;


        var proxy = new Ext.data.HttpProxy({
            method: 'GET',
            url: me.url,
            reader: new Ext.data.JsonReader({
                idProperty: 'none',
                totalProperty: 'totalSize',
                root: 'data'
            })
        });


        var store = new Ext.data.Store({
            proxy: proxy,
            simpleSort: true,
            baseParams: me.getBaseParamsObject(),
            model: 'sessionAssignedUser',
            groupField: 'ClassID',
            sortInfo: { field: 'Sort', direction: "ASC" },
            listeners: {
                beforeload: function () {
                    me.setBaseParams(me.store);
                },
                load: function () {
                    me.fireEvent('loaded');
                }
            }
        });

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'left',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [{
                /*id: 'fullName',*/
                flex: 1,
                header: 'Full Name',
                dataIndex: 'fullName'
            }, {
                /*id: 'username',*/
                header: 'Username',
                dataIndex: 'username'
            }, {
                /*id: 'RegistrationStatusName',*/
                header: 'Status',
                dataIndex: 'RegistrationStatusName'
            }]
        });

        Ext.apply(this, {
            url: me.url,
            store: store,
            proxy: proxy,
            model: 'sessionAssignedUser',
            cls: 'session-assignment-grid',
            colModel: colModel,
            features: Ext.create('Ext.grid.feature.Grouping', {
                forceFit: true,
                groupHeaderTpl: [
                    '{children:this.renderHeader}',
                    {
                        renderHeader: function(children) {
                            if (children.length) {
                                var record = children[0];
                                var title = (me.userIds.length ? record.get('TrainingProgramName') + '<br/>' : '')

                                if (record.get('ClassID') && record.get('IsInDateRange')) {
                                    var longStartDate = Symphony.parseDate(record.get('MinStartDateTime')).formatSymphony('D, F j, Y g:i a');
                                    var longEndDate = Symphony.parseDate(record.get('MaxStartDateTime')).formatSymphony('D, F j, Y g:i a');

                                    var timeString = me.formatStartEndDate(record.get('MinStartDateTime'), record.get('MaxStartDateTime'));
                        
                                    title += me.formatSessionName(record.get('ClassName'), record.get('MinStartDateTime'), record.get('MaxStartDateTime'));
                        
                                    var qTip = Symphony.qtipRenderer(title, longStartDate + " to " + longEndDate);

                                    return String.format('<span data-classid="{0}">{1}</span>',
                                        record.get('ClassId'),
                                        qTip);
                                } else {
                                    var status = record.get('SessionStatus') ? record.get('SessionStatus') : 0;
                        
                                    switch (status) {
                                        case Symphony.SessionStatus.unregistered:
                                            title += 'Users without registered sessions';
                                            break;
                                        case Symphony.SessionStatus.expired:
                                            title += 'Users with expired sessions outside of this time period';
                                            break;
                                        case Symphony.SessionStatus.active:
                                            title += 'Users with active sessions outside of this time period';
                                            break;
                                        case Symphony.SessionStatus.future:
                                            title += 'Users with future sessions outside of this time period';
                                            break;
                                    }

                                    return title;
                                }
                            }
                        }
                    }
                ]
            }),
            deferLoad: true,
            tbar: {
                items: [{
                    xtype: 'button',
                    text: 'Assign Selected Student To',
                    iconCls: 'x-menu-date-edit',
                    disabled: true,
                    menu: [{
                        items: ['No students selected']
                    }],
                    ref: '../assignStudentBtn' 
                }, {
                    xtype: 'button',
                    text: 'Auto Selected Assign Student',
                    iconCls: 'x-menu-wand',
                    disabled: true,
                    ref: '../autoAssignStudentBtn',
                    handler: function () {
                        me.assignStudents(me.selectedStudents);
                    }
                }]
            },
            listeners: {
                rowclick: function (grid) {
                    var selectionModel = me.getSelectionModel();
                    var selected = selectionModel.getSelections();
                    var tps = {};
                    var tpCount = 0;

                    me.selectedStudents = selected;

                    if (selected.length == 0) {
                        me.assignStudentBtn.setDisabled(true);
                        me.autoAssignStudentBtn.setDisabled(true);
                        return;
                    } else {
                        me.assignStudentBtn.setDisabled(false);
                        me.autoAssignStudentBtn.setDisabled(false);
                    }

                    for (var i = 0; i < selected.length; i++) {
                        var record = selected[i];
                        var tpId = record.get('TrainingProgramID');

                        if (!tps[tpId]) {
                            tps[tpId] = tpId
                            tpCount++;
                        }
                    }

                    if (tpCount > 1) {
                        me.assignStudentBtn.setDisabled(true);
                    }

                    me.assignStudentBtn.setText(selected.length == 1 ? 'Assign Selected Student To' : 'Assign Selected Students To');
                    me.autoAssignStudentBtn.setText(selected.length == 1 ? 'Auto Assign Selected Student' : 'Auto Selected Assign Students');

                    var sessionClassItems = [];
                    var sessionClasses = selected[0].get('SessionClasses');
                    for (var i = 0; i < sessionClasses.length; i++) {
                        sessionClassItems.push({
                            text: me.formatSessionName(sessionClasses[i].className, sessionClasses[i].startDate, sessionClasses[i].endDate),
                            sessionClass: sessionClasses[i],
                            iconCls: 'x-menu-date-move',
                            handler: function () {
                                me.assignStudents(selected, this.sessionClass);
                            }
                        });
                    }

                    me.assignStudentBtn.menu.removeAll();
                    if (sessionClassItems.length > 0) {
                        me.assignStudentBtn.menu.add('<strong class="x-menu-title">Select a Session</strong>');
                        for (var i = 0; i < sessionClassItems.length; i++) {
                            me.assignStudentBtn.menu.add(sessionClassItems[i]);
                        }
                    } else {
                        me.assignStudentBtn.menu.add(
                            { iconCls: 'x-menu-date-error', text: 'No upcoming sessions available.', disabled: true }
                        );
                    }

                    
                },
                rowcontextmenu: function (grid, rowIndex, e) {
                    e.preventDefault();

                    var row = grid.store.getAt(rowIndex);
                    var selectionModel = me.getSelectionModel();
                    var selected = selectionModel.getSelections();
                    var isSelected = false;

                    for (var i = 0; i < selected.length; i++) {
                        var record = selected[i];
                        var tpId = record.get('TrainingProgramID');

                        if (record.get('id') == row.get('id')) {
                            isSelected = true;
                        }
                    }

                    if (!isSelected) {
                        selected = [row];
                        selectionModel.selectRow(rowIndex, false);
                    }

                    var items = [
                        String.format('<strong class="x-menu-title">{0}</strong>', selected.length == 1 ? selected[0].get('firstName') + ' '  + selected[0].get('lastName') : 'Multiple Students'),
                        {
                            iconCls: me.assignStudentBtn.iconCls,
                            disabled: me.assignStudentBtn.disabled,
                            text: me.assignStudentBtn.getText(),
                            menu: me.assignStudentBtn.menu
                        }, {
                            iconCls: me.autoAssignStudentBtn.iconCls,
                            text: me.autoAssignStudentBtn.getText(),
                            handler: me.autoAssignStudentBtn.handler
                        }
                    ];

                    var contextMenu = new Ext.menu.Menu({
                        items: items
                    });

                    contextMenu.showAt(e.xy);
                }
            }
        });
        this.callParent(arguments);

        if (me.trainingProgramIds.length) {
            me.store.load();
        }
    },
    assignStudents: function(selectedStudents, sessionClass) {
        var studentData = [];
        var me = this;

        var url = String.format('/services/classroom.svc/registrations/session/{0}',
                    sessionClass && sessionClass.classId ? sessionClass.classId : 'auto');
           
        for (var i = 0; i < selectedStudents.length; i++) {
            studentData.push(selectedStudents[i].data);
        }

        var loadMask = new Ext.LoadMask(me.getEl(), { msg: "Assigning Students..." });
        loadMask.show();

        Symphony.Ajax.request({
            method: 'POST',
            url: url,
            jsonData: studentData,
            complete: function(args) {
                loadMask.hide();
                me.store.load();
                me.getSelectionModel().clearSelections();
                me.fireEvent('rowclick');
            },
            success: function (args) {
                var resultWin = new Ext.Window({
                    title: 'Results',
                    width: 700,
                    height: 300,
                    layout: 'border',
                    modal: true,
                    buttons: [{
                        xtype: 'button',
                        text: 'Ok',
                        handler: function () { resultWin.close() }
                    }],
                    items: [{
                        xtype: 'panel',
                        html: 'Session registration results:',
                        region: 'north',
                        height: 30,
                        style: 'padding: 5px',
                        border: false,
                        cls: 'x-panel-transparent'
                    }, {
                        xtype: 'courseassignment.sessionassignmentresultsgrid',
                        region: 'center',
                        data: args.data,
                        border: false
                    }]
                }).show();
            }
        });
    },
    setUsers: function(userIds) {
        this.usersIds = userIds;
        this.store.load();
    },
    setTrainingProgramIds: function(trainingProgramIds) {
        this.trainingProgramIds = trainingProgramIds;
        this.store.load();
    },
    setDateRangeVisibility: function(showInRangeOnly) {
        this.showInRangeOnly = showInRangeOnly;
        this.store.load();
    },
    setSessionVisibility: function(showActive) {
        this.showActive = showActive;
        this.store.load();
    },
    setRegistrationStatus: function(registrationStatusId) {
        this.registrationStatusId = registrationStatusId;
        this.store.load();
    },
    setDateRange: function (dateRangeString, dateRangeDisplay) {
        this.dateRangeString = dateRangeString;
        this.dateRangeDisplay = dateRangeDisplay;
        this.store.load();
    },
    clearDateRange: function () {
        this.dateRangeString = 'none';
        this.dateRangeDisplay = '';
        this.store.load();
    }
});
