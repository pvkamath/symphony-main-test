﻿Symphony.CourseAssignment.BulkDownloadTab = Ext.define('courseassignment.bulkdownloadtab', {
    alias: 'widget.courseassignment.bulkdownloadtab',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            layout: 'border',
            listeners: {
                afterrender: function (panel) {
                    panel.doLayout();
                }
            },
            defaults: {
                border: false
            },
            items: [{
                split: true,
                region: 'west',
                xtype: 'courseassignment.selectablecoursesgrid',
                width: 400
            }, {
                region: 'center',
                xtype: 'panel',
                layout: 'border',
                border: false,
                defaults: {
                    border: false
                },
                tbar: {
                    items: [{
                        xtype: 'button',
                        text: 'Download',
                        handler: function () {
                            var selectedCourseIds = me.find('xtype', 'courseassignment.selectablecoursesgrid')[0].getSelectedIds();
                            var courseOverrides = me.find('xtype', 'courseassignment.courseoverridepanel')[0].getValues();
                            var parameterOverrides = me.find('xtype', 'courseassignment.bulkdownloadparameterpanel')[0].getData();

                            if (selectedCourseIds.length > 0) {
                                Ext.Msg.wait('Packaging courses...', 'Please wait...');
                                Symphony.Ajax.request({
                                    url: '/services/courseassignment.svc/downloadcourses/',
                                    jsonData: {
                                        courseIds: selectedCourseIds,
                                        courseOverride: courseOverrides,
                                        parameterOverrides: parameterOverrides
                                    },
                                    success: function (args) {
                                        Ext.Msg.hide();

                                        var fileName = args.data;

                                        fileName = fileName.replace('.zip', '');

                                        Ext.Msg.show({
                                            title: 'Download Package',
                                            msg: 'Your courses have been successfully exported.<br/><br/><a href="/services/courseassignment.svc/downloadcourses/{0}" target="_blank">Download them now.</a>'.format(fileName),
                                            buttons: Ext.Msg.OK
                                        });
                                    }
                                });
                            } else {
                                Ext.Msg.alert("Error", "No courses selected.");
                            }
                        }
                    }]
                },
                items: [{
                    xtype: 'panel',
                    layout: 'fit',
                    region: 'center',
                    defaults: {
                        border: false
                    },
                    items: [{
                        xtype: 'tabpanel',
                        activeTab: 0,
                        bodyStyle: 'padding: 15px',
                        items: [{
                            xtype: 'courseassignment.courseoverridepanel',
                            listeners: {
                                render: function(p){
                                    p.loadData();
                                }
                            }
                        }, {
                            xtype: 'courseassignment.bulkdownloadparameterpanel',
                            title: 'Parameters',
                            parent: me
                        }]
                    }]
                }]
            }]
        });
        this.callParent(arguments);
    }
});
