﻿(function () {
    Symphony.CourseAssignment.TrainingprogramParameterPanel = Ext.define('courseassignment.trainingprogramparameterpanel', {
        alias: 'widget.courseassignment.trainingprogramparameterpanel',
        extend: 'Ext.Panel',
        trainingProgram: {},
        trainingProgramForm: {},
        isLockBySharedTrainingFlag: false,
        activated: false,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                border: false,
                items: [{
                    border: false,
                    xtype: 'artisan.parametersetoptionspanel',
                    isLockBySharedTrainingFlag: me.isLockBySharedTrainingFlag
                }],
                listeners: {
                    activate: function () {
                        var parameterPanel = me.find('xtype', 'artisan.parametersetoptionspanel')[0];
                        parameterPanel.isLockBySharedTrainingFlag = me.isLockBySharedTrainingFlag;
                        var courses = me.trainingProgramForm.find('name', 'assignedcourses')[0].getAssignments();
                        var parameters = [];
                        var previousParameterSetData = parameterPanel.getParameterSetData();
                        var previousParameterData = parameterPanel.getData();
                        var previousValues = {};
                        var tpValues = {};
                        var trainingProgramOverrides = me.trainingProgramForm.parameterOverrides;

                        for (var i = 0; i < previousParameterData.length; i++) {
                            previousValues[previousParameterData[i].parameterId] = previousParameterData[i].value;
                        }

                        for (var i = 0; i < trainingProgramOverrides.length; i++) {
                            tpValues[trainingProgramOverrides[i].parameterId] = trainingProgramOverrides[i];
                            if (!me.activated) {
                                previousParameterSetData['parameterSet' + trainingProgramOverrides[i].parameterSetId] = {
                                    display: trainingProgramOverrides[i].parameterSetOptionValue,
                                    value: trainingProgramOverrides[i].parameterSetOptionId
                                }
                            }

                            if (!me.activated || !previousValues[trainingProgramOverrides[i].parameterSetId]) {
                                previousValues[trainingProgramOverrides[i].parameterId] = trainingProgramOverrides[i].value;
                            }
                        }

                        me.removeAll();

                        courses = courses.requiredCourses
                                    .concat(courses.electiveCourses)
                                    .concat(courses.optionalCourses)
                                    .concat(courses.finalAssessments);
                        
                        for (var i = 0; i < courses.length; i++) {
                            if (courses[i].parameters) {
                                parameters = parameters.concat(JSON.parse(courses[i].parameters));
                            }
                        }
                        
                            Symphony.Ajax.request({
                                method: 'GET',
                                url: '/services/artisan.svc/parameters?codes=' + parameters.join('|'),
                                success: function (args) {
                                    var params = parameters.length ? args.data : [];
                                    for (var i = 0; i < params.length; i++) {
                                        if (previousValues[params[i].id]) {
                                            params[i].value = previousValues[params[i].id];
                                        }
                                    }
                                    var newPanel = new Symphony.Artisan.ParameterSetOptionsPanel({
                                        xtype: 'artisan.parametersetoptionspanel',
                                        isLockBySharedTrainingFlag: me.isLockBySharedTrainingFlag,
                                        border: false,
                                        parameters: params,
                                        parameterSetData: previousParameterSetData,
                                        parametersFound: parameters,
                                        hidden: parameters.length ? false : true,
                                        isTrainingProgram: true
                                    });

                                    me.add(newPanel);
                                    me.doLayout();

                                    if (!parameters.length) {
                                        Ext.Msg.alert("No Parameters", "This training program does not use any Artisan Course Parameters.");
                                    }
                                }
                            });
                      
                        me.activated = true;
                    }
                }
            });

            this.callParent(arguments);
        },
        getParameterOverrides: function () {
            return this.find('xtype', 'artisan.parametersetoptionspanel')[0].getParameterOverrides();
        }
    });

})();

