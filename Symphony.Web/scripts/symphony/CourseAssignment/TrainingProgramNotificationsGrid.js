﻿Ext.define('CourseAssignment.TrainingProgramNotificationsGrid', {
    extend: 'symphony.searchablegrid',
    xtype: 'courseassignment.trainingprogramnotificationsgrid',
    mixins: ['Symphony.mixins.FunctionOwnership'],
    bodyCls: 'trainingProgramNotificationsGrid',
    url: "/services/courseassignment.svc/notificationtemplates/detail",
    model: "notificationTemplate",
    trainingProgram: null,
    remoteFilter: true,
    isAllowGenericFilters: true,
    deferLoad: true,
    previousSelection: null,
    plugins: [{
        ptype: 'rowexpander',
        getRowBodyFeatureData: function(record, idx, rowValues) {
            // Override to fix extjs bug with colspan on rowexpander
            // was being set to previousRowColspan - 1 for each row
            record.data.body = Ext.util.Format.stripTags(record.data.body.replace(/<(style|xml)>(.|\s)*?<\/(style|xml)>/g, ""));
            
            rowValues.rowBodyColspan = this.grid.columns.length;
            rowValues.rowBody = this.getRowBodyContents(record);
            rowValues.rowBodyCls = this.recordsExpanded[record.internalId] ? '' : this.rowBodyHiddenCls;
        },
        rowBodyTpl: new Ext.XTemplate(
            '<div class="messagedetails">',
                '<p><strong>Recipients:</strong> {selectedRecipients:this.formatRecipients}</p>',
                '<p><strong>Subject:</strong> {subject}</p>',
                '<p><strong>Body:</strong> {body}</p>',
            '</div>',
            '<div class="settings">',
                '<p><strong>CC Classroom Supervisors:</strong> {ccClassroomSupervisors:this.formatCheck}</p>',
                '<p><strong>CC Reporting Supervisors:</strong> {ccReportingSupervisors:this.formatCheck}</p>',
            '</div>',
            {
                formatRecipients: function (selectedRecipients) {
                    var recipients = '';

                    for (var i = 0; i < selectedRecipients.length; i++) {
                        recipients += (i > 0 ? ', ' : '') + selectedRecipients[i].displayName;
                    }
                    return recipients;
                },
                formatCheck: function (check) {
                    return Symphony.checkRenderer(check);
                }
            }
        )
    }],
    initComponent: function () {
        var me = this;

        me.initialColumns = [{
            text: 'Name',
            dataIndex: 'displayName',
            flex: 0.25
        }, {
            text: 'Description',
            dataIndex: 'description',
            flex: 0.37
        }, {
            text: 'Schedule',
            dataIndex: 'relativeScheduledMinutes',
            renderer: function (value, meta, record) {
                var scheduleParameter = record.get('scheduleParameter');
                if (scheduleParameter) {
                    if (scheduleParameter.scheduleParameterOption == Symphony.ScheduleParameterOptions.percentage) {
                        return Ext.String.format('{0}% {1}', record.get('percentage'), scheduleParameter.displayName);
                    } else {
                        var beforeOrAfter = value < 0 ? 'before' : 'after';
                        return Ext.String.format('{0} {1} {2}', Symphony.Classroom.durationRendererAllowDays(Math.abs(value)), beforeOrAfter, scheduleParameter.displayName);
                    }
                }
                return "";
            },
            flex: 0.37
        }, {
            text: 'User Created',
            dataIndex: 'isUserCreated',
            width: 75,
            align: 'center',
            renderer: function (value, meta, record) {
                var msg = value ? "This is a user created notification" : "This is not a user created notification";
                return Symphony.checkRenderer(value, msg);
            }
        }, {
            text: 'Scheduled',
            dataIndex: 'isScheduled',
            width: 60,
            align: 'center',
            renderer: function (value, meta, record) {
                var msg = value ? "This is a scheduled notification" : "This is not a scheduled notification";
                return Symphony.checkRenderer(value, msg);
            }
        }, {
            text: 'Enabled',
            dataIndex: 'enabled',
            width: 60,
            align: 'center',
            renderer: function (value, meta, record) {
                var msg = value ? "This notification is enabled" : "This notification is not enabled";

                if (me.isCustomNotificationsEnabled) {
                    value = me.getPlugin("pagingSelectionPersistence").selected[record.get('id')] ? true : false;
                }

                return Symphony.checkRenderer(value, msg);
            }
        }];

        this.plugins.push(new Ext.ux.grid.plugin.PagingSelectionPersistence());

        Ext.apply(this, {
            selModel: new Ext.selection.CheckboxModel({
                checkOnly: true,
                sortable: true
            }),
            columns: me.initialColumns,
            tbar: {
                items: [{
                    xtype: 'checkbox',
                    name: 'isCustomNotificationsEnabled',
                    boxLabel: 'Enable custom notifications',
                    margins: 3,
                    handler: function (chk, checked) {
                        me.setCustomNotificationsEnabled(checked);
                    }
                }, {
                    xtype: 'checkbox',
                    name: 'showSelectedOnly',
                    boxLabel: 'Show Selected Only',
                    margins: 3,
                    handler: function (chk, checked) {
                        me.setSelectedOnly(checked);
                    }
                }]
            },
            listeners: {
                activate: {
                    single: true,
                    fn: function (tab, opts) {

                        // Adding these events here as they need to be added
                        // after the paging selection persistence plugin is enabled.
                        me.selModel.on('select', Ext.bind(me.refreshRowOnSelectChange, me));
                        me.selModel.on('deselect', Ext.bind(me.refreshRowOnSelectChange, me));

                        if (me.trainingProgram && me.trainingProgram.id > 0 && me.trainingProgram.isCustomNotificationsEnabled) {
                            var notificationLoadMask = new Ext.LoadMask(me, { msg: "Loading notifications..." });
                            notificationLoadMask.show();

                            Symphony.Ajax.request({
                                method: 'GET',
                                url: '/services/courseassignment.svc/notificationtemplates/trainingprogram/' + me.trainingProgram.id,
                                success: function (result) {
                                    var customNotificationsCheckBox = me.query('[name=isCustomNotificationsEnabled]')[0];
                                    me.getPlugin('pagingSelectionPersistence').setSelections(result.data);

                                    customNotificationsCheckBox.setValue(true);

                                    me.setCustomNotificationsEnabled(true, true, true);
                                },
                                failure: function() {
                                    me.setCustomNotificationsEnabled(false);
                                },
                                complete: function () {
                                    notificationLoadMask.hide();
                                }
                            });
                        } else {
                            me.setCustomNotificationsEnabled(false);
                        }
                    }
                }
            }
        });

        this.callParent();
    },
    setSelectedOnly: function (isEnabled, suppressReload) {
        
        if (isEnabled) {
            var selectedIds = this.getPlugin('pagingSelectionPersistence').getPersistedSelectionIds();
            this.store.filters.add('selectedIds', new Ext.util.Filter({
                property: 'Id',
                value: selectedIds.join(','),
                comparisonOperator: Symphony.ComparisonOperators['in']
            }));
        } else {
            this.store.filters.removeAtKey('selectedIds');
        }

        if (!suppressReload) {
            this.refresh();
        }
    },
    setCustomNotificationsEnabled: function (isEnabled, suppressReload, isShowSelected) {
        var selectionPlugin = this.getPlugin('pagingSelectionPersistence');
        var showSelectedOnlyChk = this.query('[name=showSelectedOnly]')[0];

        this.isCustomNotificationsEnabled = isEnabled;

        this.columns[0].setVisible(isEnabled);

        if (!isEnabled) {
            this.store.filters.add('enabled', new Ext.util.Filter({
                property: 'Enabled',
                value: true
            }));

            this.store.filters.removeAtKey('selectedIds');

            this.previousSelection = selectionPlugin.getPersistedSelection();
            selectionPlugin.clearPersistedSelection();
            showSelectedOnlyChk.setValue(false);
            showSelectedOnlyChk.setDisabled(true);
        } else {
            this.store.filters.removeAtKey('enabled');
            showSelectedOnlyChk.setDisabled(false);

            if (this.previousSelection) {
                selectionPlugin.setSelections(this.previousSelection);
            }

            var showSelectedOnly = isShowSelected === true ? true : false;

            this.setSelectedOnly(showSelectedOnly, true);
            showSelectedOnlyChk.setValue(showSelectedOnly);
        }

        if (!suppressReload) {
            this.refresh();
        }
    },
    getData: function () {
        if (this.rendered) {
            var customNotificationsCheckBox = this.query('[name=isCustomNotificationsEnabled]')[0];
            if (customNotificationsCheckBox) {
                var notificationTemplates = this.getPlugin('pagingSelectionPersistence').getPersistedSelection();
                var selectedNotificationTemplateData = [];
                for (var i = 0; i < notificationTemplates.length; i++) {
                    selectedNotificationTemplateData.push(notificationTemplates[i].data);
                }

                return {
                    isCustomNotificationsEnabled: customNotificationsCheckBox.getValue(),
                    notificationTemplates: selectedNotificationTemplateData
                }

                return null;
            }
        } else if (this.trainingProgram) {
            return {
                isCustomNotificationsEnabled: this.trainingProgram.isCustomNotificationsEnabled,
                notificationTemplates: this.trainingProgram.notificationTemplates
            }
        } else {
            return null;
        }
    },
    refreshRowOnSelectChange: function (rm, record, index) {
        this.getView().refreshNode(index);
    }
}, function (clazz) {
    Log.debug('Class created: ' + clazz.$className);
});