﻿Symphony.CourseAssignment.TrainingProgramBundleGrid = Ext.define('courseassignment.trainingprogrambundlegrid', {
    alias: 'widget.courseassignment.trainingprogrambundlegrid',
    extend: 'symphony.searchablegrid',
    initComponent: function () {
        var me = this;
        var url = '/services/courseassignment.svc/trainingprogrambundles/';


        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [
                { header: ' ', hideable: false, renderer: Symphony.deleteRenderer, width: 35 },
                {
                    /*id: 'name',*/ header: 'Curriculum Name', dataIndex: 'name', width: 140, align: 'left',
                    flex: 1
                },
                { header: 'Status', dataIndex: 'status', width: 55 },
                { header: 'Sku', dataIndex: 'sku', width: 100 }
            ]
        });

        Ext.apply(this, {
            tbar: {
                items: [{
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function () {
                        me.fireEvent('addclick');
                    }
                }]
            }, 
            idProperty: 'id',
            url: url,
            colModel: colModel,
            model: 'trainingProgramBundle'
        });
        this.callParent(arguments);
    }
});
