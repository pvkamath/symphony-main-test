﻿Symphony.CourseAssignment.SessionAssignmentResultsGrid = Ext.define('courseassignment.sessionassignmentresultsgrid', {
    alias: 'widget.courseassignment.sessionassignmentresultsgrid',
    extend: 'symphony.localgrid',
    initComponent: function () {
        var me = this;

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'left',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [{
                /*id: 'results',*/ name: 'Results', flex: 1, dataIndex: 'id', header: 'Results', renderer: function (value, meta, record) {
                    if (!record.get("ClassID")) {
                        return record.get("firstName") + " " + record.get("lastName") + " was not registered to any sessions.";
                    }
                    return record.get("firstName") + " " + record.get("lastName") + " was registered to the session " + record.get("ClassName") + " with the status " + record.get("RegistrationStatusName").toLowerCase();
                }
            }]
        });

        Ext.apply(this, {
            model: 'sessionAssignedUser',
            colModel: colModel
        });
        this.callParent(arguments);
    }
});
