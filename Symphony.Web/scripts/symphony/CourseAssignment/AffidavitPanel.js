﻿Symphony.CourseAssignment.AffidavitPanel = Ext.define('courseassignment.affidavitpanel', {
    alias: 'widget.courseassignment.affidavitpanel',
    extend: 'Ext.Panel',
    loaded: false,
    config: {},

    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            border: false,
            defaults: { anchor: '100%', border: false, xtype: 'panel', cls: 'x-form-item' },
            border: false,
            autoScroll: true,
            cls: 'x-panel-mc affidavit-form',
            bodyStyle: 'padding: 5px',
            listeners: {
                activate: function() {
                    me.loadAffidavit();
                }
            }
        });
        this.callParent(arguments);
    },
    loadAffidavit: function () {
        var me = this;

        if (!me.loaded) {

            me.loadMask = new Ext.LoadMask(me.getEl(), { msg: "Please wait..." });
            me.loadMask.show();

            Symphony.Ajax.request({
                url: '/services/affidavit.svc/affidavits/' + me.affidavitId,
                method: 'GET',
                success: function (args) {
                    var affidavit = JSON.parse(args.data.affidavitJSON);

                    var affidavitPanel = new Ext.form.FormPanel({
                        items: affidavit.items,
                        layout: 'form',
                        defaults: {
                            border: false
                        },
                        cls: 'x-panel-mc affidavit-form',
                        bodyStyle: 'padding: 5px',
                        name: 'affidavitPanel'
                    });

                    me.config = {
                        width: affidavit.width ? affidavit.width : 500,
                        height: affidavit.height ? affidavit.height : 500,
                        title: me.mode == 'preview' ? 'Affidavit Preview' : 'Final Assessment Affidavit'
                    }
                    me.add(affidavitPanel);
                    me.doLayout();


                    me.fireEvent('updateContainer', {
                        width: me.config.width,
                        height: me.config.height,
                        title: me.config.title,
                        iconCls: 'x-window-script'
                    });

                    if (me.mode != 'preview') {
                        var confirmBtn = me.queryById('confirm');
                        var cancelBtn = me.queryById('cancel');

                        if (!confirmBtn) {
                            confirmBtn = me.find('xtype', 'button')[0];
                        }

                        confirmBtn.setIconCls('x-button-launch-course');
                        if (cancelBtn) {
                            cancelBtn.setIconCls('x-button-cancel');
                        }

                        confirmBtn.addListener('click', function () {
                            var affidavitPanel = me.find('name', 'affidavitPanel')[0],
                                form = affidavitPanel.getForm(),
                                items = form.getFields(),
                                data = form.getValues(),
                                responseJSON = JSON.stringify(data),
                                affidavitResponse = {
                                    userId: Symphony.ActualUser.id,
                                    trainingProgramId: me.trainingProgramId,
                                    courseId: me.courseId,
                                    affidavitId: me.affidavitId,
                                    responseJSON: responseJSON
                                },
                                valid = true;

                            for (var i = 0; i < items.length; i++) {
                                var field = items.itemAt(i);
                                if (!field.getValue()) {
                                    valid = false
                                }
                            }

                            if (!valid) {
                                Ext.Msg.alert("Error", "All fields require a value.");
                                return;
                            }

                            Symphony.Ajax.request({
                                url: '/services/affidavit.svc/affidavits/responses',
                                method: 'post',
                                jsonData: affidavitResponse
                            });

                            // Just going to assume that the affidavit request worked and launch the course
                            me.fireEvent('nextitem');
                        });

                        if (cancelBtn) {
                            cancelBtn.addListener('click', function() {
                                me.ownerCt.close();
                            });
                        }
                    }


                    me.loadMask.hide();
                }
            });
            me.loaded = true;
        }
    }

});
Symphony.CourseAssignment.AffidavitPanel.showAffidavitWindow = function (affidavitId, mode, trainingProgramId, courseId, callback) {
    if (affidavitId > 0) {
        var w = new Ext.Window({
            autoScroll: true,
            modal: true,
            height: 205,
            width: 310,
            listeners: {
                afterrender: function () {
                    var affidavitpanel = w.find('xtype', 'courseassignment.affidavitpanel')[0];
                    affidavitpanel.loadAffidavit();
                }
            },
            items: [{
                xtype: 'courseassignment.affidavitpanel',
                userId: Symphony.ActualUser.id,
                trainingProgramId: trainingProgramId,
                courseId: courseId,
                affidavitId: affidavitId,
                mode: mode,
                listeners: {
                    'updateContainer': function(parameters) {
                        var params = {};
                        Ext.apply(params, parameters, {
                            height: 205,
                            width: 310,
                            title: 'Affidavit',
                            iconCls: 'info'
                        });

                        w.setSize(params.width, params.height);
                        w.setTitle(params.title, params.iconCls);

                        w.center();
                    },
                    afterrender: function () {
                        this.loadAffidavit();
                    }
                }
            }],
            bbar: mode == 'preview' ? {
                items: ['->', {
                    xtype: 'button',
                    iconCls: 'x-button-cancel',
                    text: 'Close',
                    handler: function () {
                        w.close();
                    }
                }]
            } : { }
        });
        w.show();
    }
}

