﻿Symphony.CourseAssignment.LockedUserCoursesGrid = Ext.define('courseassignment.lockedusercoursesgrid', {
    alias: 'widget.courseassignment.lockedusercoursesgrid',
    extend: 'symphony.unpagedgrid',
    userId: 0,
    initComponent: function () {
        var me = this;
        var url = '/services/CourseAssignment.svc/lockedusercourses/' + this.userId + '?lockedOnly=true';

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'left',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [{
                /*id: 'trainingProgramName',*/
                header: 'Training Program',
                width: 200,
                dataIndex: 'trainingProgramName'
            }, {
                /*id: 'courseName',*/
                header: 'Course',
                width: 100,
                dataIndex: 'courseName',
                flex: 1
            },
            { header: 'Attempts', dataIndex: 'testAttemptCount', renderer: Symphony.Portal.valueRenderer, width: 65 },
            {
                header: 'Clear Lock',
                /*id: 'reset',*/
                name: 'reset',
                width: 60,
                renderer: function (value, meta, record) {
                    var html = Symphony.qtipRenderer('Clear', 'Clearing a lock allows a student to resume course testing when have reached the maximum attempts for a course. This will effectively set the course attempts to be zero.');
                    return '<a href="#">'+html+'</a>';
                }
            },
            {
                header: 'Reset',
                /*id: 'delete',*/
                name: 'delete',
                width: 60,
                renderer: function (value, meta, record) {
                    var html = Symphony.qtipRenderer('Reset', 'Resetting a course for a student will remove any saved progress for a course. The student will start at the beginning with the newest version of the course the next time they access the course.');
                    return '<a href="#">' + html + '</a>';
                }
            }]
        });

        Ext.apply(this, {
            idProperty: 'id',
            url: url,
            colModel: colModel,
            model: 'lockedOnlineCourseRollup',
            tbar: {
                items: [{
                    xtype: 'checkbox',
                    boxLabel: 'Show All',
                    listeners: {
                        'check': function (field, state) {
                            var updatedUrl = '';
                            if (state) {
                                updatedUrl = url.replace('lockedOnly=true', 'lockedOnly=false');
                            } else {
                                updatedUrl = url.replace('lockedOnly=false', 'lockedOnly=true');
                            }
                            me.updateStore(updatedUrl);
                        }
                    }
                }]
            },
            listeners: {
                cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                    var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex;
                    var columnName = grid.headerCt.getGridColumns()[columnIndex].name;
                    var record = grid.getStore().getAt(rowIndex);
                    var onlineCourseRollupId = record.get('id');
                    var action = '';

                    if (columnName == 'reset') {
                        action = 'resetattempts';
                    } else if (columnName == 'delete') {
                        action = 'deleteregistration';
                    }
                    if (action) {
                        new Symphony.CourseAssignment.ResetCourseDialog({
                            callback: function () {
                                me.store.reload();
                                me.fireEvent('change');
                            },
                            action: action,
                            onlineCourseRollupId: onlineCourseRollupId
                        }).show();
                    }
                }
            }
        });

        this.callParent(arguments);
    }
});

