﻿Symphony.CourseAssignment.TrainingProgramBundleTab = Ext.define('courseassignment.trainingprogrambundletab', {
    alias: 'widget.courseassignment.trainingprogrambundletab',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            layout: 'border',
            defaults: {
                border: false
            },
            items: [{
                split: true,
                region: 'west',
                xtype: 'courseassignment.trainingprogrambundlegrid',
                width: 400,
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.store.getAt(rowIndex);

                        if (columnIndex === 0) { // delete column
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to remove this training program bundle?', function (btn) {
                                if (btn == 'yes') {
                                    Symphony.Ajax.request({
                                        url: '/services/courseassignment.svc/trainingprogrambundle/' + record.get('id'),
                                        method: 'delete',
                                        success: function () {
                                            Ext.each(me.find('xtype', 'courseassignment.trainingprogrambundleform'), function (form) {
                                                if (form.trainingProgramBundleId == record.get('id')) {
                                                    form.confirmedRemove = true;
                                                    form.ownerCt.remove(form);
                                                }
                                            });
                                            grid.refresh();
                                        }
                                    });
                                }
                            });
                        } else {
                            me.addPanel(record.get('id'));
                        }
                    },
                    addclick: function () {
                        me.addPanel(0, 'New Training Program Bundle', 'New Training Program Bundle');
                    }
                }
            }, {
                region: 'center',
                xtype: 'tabpanel',
                ref: 'bundleView',
                border: false,
                defaults: {
                    border: false
                }
            }]
        });
        this.callParent(arguments);
    },
    addPanel: function (id, name, description) {
        var me = this;
        var tabPanel = me.bundleView;

        var found = tabPanel.queryBy(function (p) { return p.trainingProgramBundleId == id; })[0];
        if (found) {
            tabPanel.activate(found);
        } else {
            var panel = tabPanel.add({
                xtype: 'courseassignment.trainingprogrambundleform',
                closable: true,
                activate: true,
                id: id,
                title: name,
                tabTip: description,
                trainingProgramBundleId: id,
                listeners: {
                    save: function () {
                        me.find('xtype', 'courseassignment.trainingprogrambundlegrid')[0].refresh();
                    }
                }
            });
            panel.show();
        }
        tabPanel.doLayout();
    }
});
