﻿Symphony.CourseAssignment.TemplateHistoryGrid = Ext.define('courseassignment.templatehistorygrid', {
    alias: 'widget.courseassignment.templatehistorygrid',
    extend: 'symphony.searchablegrid',
    notificationTemplateId: 0,
    initComponent: function () {
        var me = this;
        var url = '/services/CourseAssignment.svc/templatehistory/' + this.notificationTemplateId;
        this.exportUrl = '/services/CourseAssignment.svc/templatehistory/export/' + this.notificationTemplateId + '?start=0&limit=10000'; // limit to last 1k records


        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'left',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [{
                /*id: 'createdOn',*/
                header: 'Created',
                dataIndex: 'createdOn',
                width: 130,
                renderer: Symphony.dateTimeRenderer
            }, {
                /*id: 'sender',*/
                header: 'Sender',
                width: 120,
                dataIndex: 'sender'
            }, {
                /*id: 'recipient',*/
                header: 'Recipient',
                width: 120,
                dataIndex: 'recipient'
            }, {
                /*id: 'subject',*/
                header: 'Subject',
                dataIndex: 'subject'
            }, {
                /*id: 'body',*/
                header: 'Body',
                dataIndex: 'body',
                flex: 1
            }]
        });

        var proxy = new Ext.data.HttpProxy({
            method: 'GET',
            url: url,
            reader: new Ext.data.JsonReader({
                idProperty: null,
                totalProperty: 'totalSize',
                root: 'data'
            })
        });

        var pageSize = 25;

        var store = new Ext.data.Store({
            autoDestroy: true,
            model: 'message',
            pageSize: pageSize,
            autoLoad: true,
            proxy: proxy,
            baseParams: { limit: pageSize },
            sortInfo: { field: 'createdOn', direction: 'DESC' },
            groupOnSort: true,
            simpleSort: true,
            //remoteGroup: true,
            groupField: 'createdOn'
        });

        Ext.apply(this, {
            tbar: {
                items: [{
                    text: 'Clear History',
                    iconCls: 'x-button-delete',
                    handler: function () {
                        Ext.Msg.confirm('Are you sure?', 'This will clear the history for this message template. Continue?', function (btn) {
                            if (btn == 'yes') {
                                Symphony.Ajax.request({
                                    url: '/services/courseassignment.svc/templatehistory/' + me.notificationTemplateId,
                                    method: 'DELETE',
                                    success: function () {
                                        me.store.reload();
                                    }
                                });
                            }
                        });
                    }
                }, {
                    text: 'Export',
                    iconCls: 'x-button-export',
                    handler: function () {
                        window.open(me.exportUrl);
                    }
                }]
            },
            store: store,
            features: Ext.create('Ext.grid.feature.Grouping', {
                forceFit: true,
                groupHeaderTpl: [
                    '{children:this.renderHeader}',
                    {
                        renderHeader: function (children) {
                            if (children.length) {
                                var record = children[0],
                                    items = children.length,
                                    date = Symphony.parseDate(record.get('createdOn')).formatSymphony('m/d/y g:i a T');

                                return "Created: " + date + " (" + items + " Item" + (items != 1 ? "s" : "") + ")";
                            }

                            return "No Items";
                        }
                    }
                ]
            }),
            colModel: colModel,
            model: 'message',
            bbar: new Ext.PagingToolbar({
                displayInfo: true,
                displayMsg: '{0} - {1} of {2}',
                emptyMsg: 'No results',
                store: store
            })
        });
        store.on('exception', function (proxy, type, action, options, response, arg) {
            if (response.status == 200) {
                var result = Ext.decode(response.responseText);
                Ext.Msg.alert('Error', result.error);
            } else {
                //TODO: add option to report/add details/etc
                Ext.Msg.alert('Error', 'The store failed to load properly; the server responded with error code ' + response.status);
            }

        });
        this.callParent(arguments);
    }
});
