﻿Symphony.CourseAssignment.UserAttemptsWindow = Ext.define('courseassignment.userattemptswindow', {
    alias: 'widget.courseassignment.userattemptswindow',
    extend: 'Ext.Window',
    trainingProgramId: 0,
    onlineCourseId: 0,
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            modal: true,
            title: 'Manage User Attempts',
            //closeAction: 'hide',
            border: false,
            bodyBorder: false,
            layout: 'border',
            width: 400,
            height: 500,
            items: [{
                ref: 'userAttemptsGrid',
                region: 'center',
                layout: 'fit',
                xtype: 'courseassignment.userattemptsgrid',
                trainingProgramId: this.trainingProgramId,
                onlineCourseId: this.onlineCourseId
            }]
        });
        this.callParent(arguments);
    }
});
