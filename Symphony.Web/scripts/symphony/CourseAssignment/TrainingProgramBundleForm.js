﻿Symphony.CourseAssignment.TrainingProgramBundleForm = Ext.define('courseassignment.trainingprogrambundleform', {
    alias: 'widget.courseassignment.trainingprogrambundleform',
    extend: 'Ext.Panel',
    trainingProgramBundleId: 0,
    trainingProgramBundleStatusStore: new Ext.data.ArrayStore({
        id: 0,
        model: 'simple',
        data: [
            [1, 'In-Development'],
            [2, 'Active'],
            [3, 'Inactive']
        ]
    }),
    trainingProgramBundleContentTypeStore: new Ext.data.ArrayStore({
        id: 0,
        model: 'simple',
        data: [
            [1, 'Prelicense Education'],
            [2, 'Continuing Education'],
            [3, 'Exam Preparation'],
            [4, 'Professional Development']
        ]
    }),
    trainingProgramBundleDeliveryMethodStore: new Ext.data.ArrayStore({
        id: 0,
        model: 'simple',
        data: [
            [1, 'Online Access'],
            [2, 'Physical'],
            [3, 'Download']
        ]
    }),
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            layout: 'border',
            defaults: {
                border: false
            },
            items: [{
                region: 'north',
                height: 30,
                items: [{
                    xtype: 'symphony.savecancelbar',
                    listeners: {
                        save: function () {
                            return me.save(false);
                        },
                        cancel: Ext.bind(function () { this.ownerCt.remove(this); }, this)
                    }
                }]
            }, {
                region: 'center',
                layout: 'fit',
                defaults: {
                    border: false
                },
                items: [{
                    xtype: 'panel',
                    border: false,
                    bodyStyle: 'padding: 15px',
                    activeTab: 0,
                    autoScroll: true,
                    items: [{
                        xtype: 'form',
                        border: false,
                        frame: true,
                        bodyStyle: 'overflow-y:auto;padding-right:10px',
                        name: 'bundle',
                        autoScroll: true,
                        items: [{
                            xtype: 'fieldset',
                            title: 'Curriculum Information',
                            layout: 'form',
                            items: [{
                                xtype: 'panel',
                                layout: 'column',
                                border: false,
                                cls: 'x-panel-transparent',
                                // column defaults
                                defaults: {
                                    columnWidth: 0.5,
                                    border: false,
                                    cls: 'x-panel-transparent',
                                    xtype: 'panel',
                                    layout: 'form',
                                    defaults: { anchor: '100%', xtype: 'textfield' }
                                },
                                items: [{
                                    items: [{
                                        name: 'name',
                                        fieldLabel: 'Name',
                                        allowBlank: false
                                    }, {
                                        name: 'hours',
                                        fieldLabel: 'Hours',
                                        xtype: 'numberfield'
                                    }, {
                                        name: 'cost',
                                        fieldLabel: 'Cost',
                                        xtype: 'numberfield'
                                    }, {
                                        name: 'listPrice',
                                        fieldLabel: 'List Price',
                                        xtype: 'numberfield'
                                    }]
                                }, {
                                    items: [{
                                        name: 'sku',
                                        fieldLabel: '&nbsp;&nbsp;Sku',
                                        allowBlank: false
                                    }, 
                                        Symphony.getCombo('status', '&nbsp;&nbsp;Status', me.trainingProgramBundleStatusStore, {
                                        valueField: 'text',
                                        displayField: 'text'
                                        }),
                                        Symphony.getCombo('deliveryMethod', '&nbsp;&nbsp;Delivery Method', me.trainingProgramBundleDeliveryMethodStore, {
                                            valueField: 'text',
                                            displayField: 'text'
                                        }),
                                        Symphony.getCombo('contentType', '&nbsp;&nbsp;Content Type', me.trainingProgramBundleContentTypeStore, {
                                            valueField: 'text',
                                            displayField: 'text'
                                        })
                                    ]
                                }]
                            }, {
                                xtype: 'hidden',
                                name: 'categoryId'
                            }, {
                                xtype: 'compositefield',
                                anchor: '100%',
                                fieldLabel: 'Category',
                                name: 'categoryCompositeField',
                                items: [{
                                    xtype: 'textfield',
                                    valueField: 'id',
                                    displayField: 'name',
                                    itemId: 'categoryDisplay',
                                    allowBlank: true,
                                    flex: 1,
                                    emptyText: 'Select a category...',
                                    valueNotFoundText: 'Select a category...',
                                    readOnly: true
                                }, {
                                    xtype: 'button',
                                    text: '...',
                                    handler: function (btn) {
                                        var w = Symphony.Categories.getCategoryEditorWindow('trainingprogram', function (node) {
                                            var categoryText = node.attributes.record.get('levelIndentText');
                                            var categoryId = node.attributes.record.get('id');
                                            me.find('name', 'categoryCompositeField')[0].items.items[0].setValue(categoryText);
                                            me.find('name', 'categoryId')[0].setValue(categoryId);
                                            w.close();
                                        }, me.find('name', 'categoryId')[0].getValue()).show();
                                    }
                                }]
                            }, {
                                name: 'description',
                                fieldLabel: 'Description',
                                xtype: 'textarea',
                                anchor: '0'
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Assigned Training Programs',
                            padding: 7,
                            items: [{
                                xtype: 'courseassignment.bundledtrainingprogramsgrid'
                            }]
                        }]
                    }]
                }]
            }]
        });
         
        this.callParent(arguments);
    },
    onRender: function () {
        Symphony.CourseAssignment.TrainingProgramBundleForm.superclass.onRender.apply(this, arguments);
        this.load();
    },
    load: function () {
        var me = this;
        if (this.trainingProgramBundleId) {
            Ext.Msg.wait('Please wait while your training program bundle is loaded...', 'Please wait...');
            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/courseassignment.svc/trainingprogrambundle/' + me.trainingProgramBundleId,
                success: function (result) {
                    // load up the appropriate panels with the results
                    me.trainingProgramBundleId = result.data.id;
                    
                    me.setTitle(result.data.name);

                    // set the basic information
                    me.bindForm(result.data);

                    // add the training programs
                    me.find('xtype', 'courseassignment.bundledtrainingprogramsgrid')[0].setData(result.data.trainingPrograms);

                    if (result.data.categoryId) {
                        me.find('itemId', 'categoryDisplay')[0].setValue(result.data.levelIndentText);
                    }

                    Ext.Msg.hide();
                }
            });
        } else {
            window.setTimeout(function () {
                // default the category
                var cf = me.find('name', 'categoryCompositeField')[0];
                cf.items.itemAt(0).setValue('', true, true);
            }, 1);
        }
    },
    bindForm: function (data) {
        // first, bind the basic information
        this.find('name', 'bundle')[0].bindValues(data);

        var cf = this.find('name', 'categoryCompositeField')[0];

        if (data.categoryId) {
            cf.items.itemAt(0).setValue(data.categoryId, true, true);
        }
        else {
            cf.items.itemAt(0).setValue(0, true, true);
        }
    },
    save: function () {
        // saves a training program bundle
        var me = this;

        var form = me.find('name', 'bundle')[0];

        if (!form.isValid()) {
            Ext.Msg.alert('Errors Detected', 'Your training program bundle has some errors. Please correct and re-save');
            return;
        }
       
        // basic information
        var tpBundle = form.getValues();

        if (!tpBundle.categoryId) {
            tpBundle.categoryId = 0;
        }

        if (!tpBundle.cost) {
            tpBundle.cost = 0;
        }
        
        if (!tpBundle.listPrice) {
            tpBundle.listPrice = 0;
        }

        if (!tpBundle.hours) {
            tpBundle.hours = 0;
        }


        tpBundle.trainingPrograms = me.find('xtype', 'courseassignment.bundledtrainingprogramsgrid')[0].getData();

        // save and process files
        Symphony.Ajax.request({
            url: '/services/courseassignment.svc/trainingprogrambundle/' + me.trainingProgramBundleId,
            jsonData: tpBundle,
            success: function (result) {
                me.trainingProgramBundleId = result.data.id;

                me.load();

                me.setTitle(result.data.name);

                me.fireEvent('save');
            }
        });
    }
});
