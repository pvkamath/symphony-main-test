﻿(function () {
    Symphony.CourseAssignment.CourseOverridePanel = Ext.define('courseassignment.courseoverridepanel', {
        alias: 'widget.courseassignment.courseoverridepanel',
        extend: 'Ext.Panel',
        labelWidth: 120,
        title: 'Artisan Overrides',
        defaults: {
            anchor: '100%'
        },

        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                border: false,
                autoScroll: true,
                items: [{
                    name: 'courseOverrides',
                    xtype: 'form',
                    disabled: this.isShared,
                    border: false,
                    frame: true,
                    items: [{
                        layout: 'column',
                        cls: 'x-panel-transparent',
                        border: false,
                        defaults: {
                            columnWidth: 0.5
                        },
                        items: [{
                            style: 'margin: 5px 5px 10px 5px; padding: 10px;',
                            xtype: 'courseassignment.courseoverridefields'
                        }, {
                            xtype: 'form',
                            name: 'customerCourseOverrides',
                            border: false,
                            frame: false,
                            cls: 'x-panel-transparent',
                            items: {
                                style: 'margin: 5px 5px 10px 5px; padding: 10px;',
                                xtype: 'courseassignment.customeroverridedisplayfields'
                            }
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
            Symphony.on("settings.closed", function () {
                me.loadCustomerData();
            });
        },

        getValues: function () {
            var values = this.find('name', 'courseOverrides')[0].getValues();
            values = Symphony.CourseAssignment.cleanCourseOverrideValues(values);
            return values;
        },
        loadCustomerData: function () {
            var me = this;
            var form = me.find('name', 'courseOverrides')[0];

            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/customer.svc/customers/' + Symphony.User.customerId + '/settings',
                success: function (result) {
                    var settings = result.data;
                    if (settings.retries) {
                        settings.testAttempts = settings.retries + 1;
                    }

                   

                    var fieldset = me.find('xtype', 'courseassignment.customeroverridedisplayfields')[0];
                    var form = me.find('name', 'customerCourseOverrides')[0];

                    form.bindValues(settings);

                    for (var prop in settings) {
                        var fields = fieldset.find('name', prop);
                        if (fields.length > 0) {
                            var field = fields[0];
                            
                            if (field.name == 'themeFlavorOverrideId') {
                                field.setVisible(settings.theme && settings.theme.isDynamic);
                            }
                        }
                    }
                }
            });
        },
        loadData: function (data) {
            var me = this;
            var form = me.find('name', 'courseOverrides')[0];
            var courseOverrideFields = me.find('xtype', 'courseassignment.courseoverridefields')[0];

            if (!data) { // This is a new training program, set blank values for all artisan course overrides
                data = {};
                for (var i = 0; i < courseOverrideFields.items.items.length; i++) {
                    var field = courseOverrideFields.items.items[i];
                    data[field.name] = null;
                }
            }

            if (data.retries) {
                data.testAttempts = data.retries + 1;
            }

            
            // For some reason bind values is not working for 0. This means if you put 
            // 0 in for a timeout to disable it, the next time it loads, the field will
            // be empty and the timeout will revert to original.
            courseOverrideFields.find('name', 'minimumPageTimeOverride')[0].setValue(data.minimumPageTimeOverride);
            courseOverrideFields.find('name', 'maximumQuestionTimeOverride')[0].setValue(data.maximumQuestionTimeOverride);
            courseOverrideFields.find('name', 'inactivityTimeoutOverride')[0].setValue(data.inactivityTimeoutOverride);

            form.bindValues(data);

            for (var prop in data) {
                var fields = courseOverrideFields.find('name', prop);
                if (fields.length > 0) {
                    var field = fields[0];

                    if (field.name == 'themeFlavorOverrideId') {
                        field.setVisible(data.theme && data.theme.isDynamic);
                    }
                }
            }

            me.loadCustomerData();
        }
    });


})();