﻿Symphony.CourseAssignment.TrainingProgramForm = Ext.define('courseassignment.trainingprogramform', {
    alias: 'widget.courseassignment.trainingprogramform',
    extend: 'Ext.Panel',
    trainingProgramId: 0,
    parameterOverrides: [],
    showValidationAlert: false,
    isShared: false,
    isDisabled: false,// set when running within userform window
    isSalesforce: false,// set when running within userform window
    userId: null,// set when running within userform window
    isBundle: false,// set when running within userform window
    initComponent: function () {
        var me = this;

        var categoriesUrl = '/services/courseassignment.svc/categories/';

        var toolbarBtns = ['  '];

        if (!me.isDisabled) {
            toolbarBtns = toolbarBtns.concat([{
                text: 'Duplicate',
                iconCls: 'x-button-duplicate',
                handler: function () {
                    return me.save(true);
                }
            }]);
        }

        if (me.userId > 0) {
            toolbarBtns = toolbarBtns.concat([{
                text: 'Print Certificate',
                iconCls: 'x-button-print',
                handler: function () {
                    me.printCertificate();
                }
            }]);
        }

        toolbarBtns = toolbarBtns.concat([{
            text: 'Preview Certificate',
            iconCls: 'x-button-preview',
            handler: function () {
                me.previewCertificate();
            }
        }]);

        if (me.isDisabled && me.isSalesforce) {
            toolbarBtns = toolbarBtns.concat(['->', {
                text: 'Delete',
                iconCls: 'x-button-delete',
                handler: function () {
                    Symphony.CourseAssignment.unassignTrainingProgram(me.trainingProgramId, me.userId, me.isBundle)
                }
            }])
        }

        Ext.apply(this, {
            layout: 'border',
            defaults: {
                border: false
            },
            items: [{
                region: 'north',
                height: 30,
                items: [{
                    xtype: 'symphony.savecancelbar',
                    saveCancelDisabled: me.isDisabled && !me.isSalesforce,
                    items: toolbarBtns,
                    listeners: {
                        save: function () {
                            return me.save(false);
                        },
                        cancel: Ext.bind(function () { this.ownerCt.remove(this); }, this)
                    }
                }]
            }, {
                region: 'center',
                layout: 'fit',
                defaults: {
                    border: false
                },
                items: [{
                    xtype: 'tabpanel',
                    border: false,
                    bodyStyle: 'padding: 15px',
                    activeTab: 0,
                    ref: '../trainingProgramTabs',
                    listeners: {
                        beforeadd: function (panel, cmp, i) {
                            var type = cmp.getXType();

                            if (!Symphony.Artisan.canOverrideParameters() &&
                                type === 'courseassignment.trainingprogramparameterpanel') {
                                return false;
                            }

                            if (!Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards) && type === 'messageboard.app') {
                                return false;
                            }

                            if (!me.userId && type === 'portal.trainingprogramdetailspanel') {
                                return false;
                            }

                            return true;
                        }
                    },
                    items: [{
                        title: 'Student Progress',
                        xtype: 'portal.trainingprogramdetailspanel',
                        trainingProgramId: me.trainingProgramId,
                        userId: me.userId,
                        width: me.width,
                        ref: '../../studentProgress'
                    }, {
                        title: 'General',
                        name: 'general',
                        xtype: 'form',
                        //defaults: { disabled: true },
                        ref: '../../generalTab',
                        border: false,
                        frame: true,
                        //autoScroll: true,
                        //bodyStyle: 'overflow-x:hidden',
                        bodyStyle: 'overflow-y:auto;padding-right:20px',
                        items: [{
                            xtype: 'fieldset',
                            title: 'Basic Information',
                            disabled: this.isShared,

                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            defaults: {
                                flex: 1,
                                cls: 'x-panel-transparent'
                            },
                            items: [{

                                layout: {
                                    type: 'hbox',
                                    align: 'stretch'
                                },
                                border: false,
                                // column defaults
                                defaults: {
                                    xtype: 'form',
                                    border: false,
                                    flex: 1,
                                    cls: 'x-panel-transparent'
                                },
                                items: [{
                                    //left column
                                    defaults: { anchor: '100%', xtype: 'textfield' },
                                    items: [{
                                        xtype: 'hidden',
                                        name: 'isSalesforce',
                                        value: false
                                    }, {
                                        name: 'isLive',
                                        fieldLabel: 'Enabled',
                                        xtype: 'checkbox',
                                        listeners: {
                                            check: function (box, checked) {
                                                var startDate = me.find('name', 'startDate')[0];
                                                var endDate = me.find('name', 'endDate')[0];
                                                if (checked) {
                                                    startDate.allowBlank = false;
                                                    endDate.allowBlank = false;
                                                } else {
                                                    startDate.allowBlank = true;
                                                    endDate.allowBlank = true;
                                                }
                                            }
                                        }
                                    }, {
                                        name: 'name',
                                        fieldLabel: 'Name',
                                        allowBlank: false
                                    }, {
                                        name: 'internalCode',
                                        fieldLabel: 'Internal Code'
                                    }, {
                                        xtype: 'hidden',
                                        name: 'categoryId',
                                        hidden: true
                                    }, {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        fieldLabel: 'Category',
                                        name: 'categoryCompositeField',
                                        items: [{
                                            xtype: 'textfield',
                                            valueField: 'id',
                                            displayField: 'name',
                                            allowBlank: false,
                                            flex: 1,
                                            emptyText: 'Select a category...',
                                            valueNotFoundText: 'Select a category...',
                                            readOnly: true
                                        }, {
                                            xtype: 'button',
                                            text: '...',
                                            handler: function (btn) {
                                                if (!me.isDisabled || me.isSalesforce) {
                                                    var showRelative = me.userId > 0 ? true : false,
                                                        w = Symphony.Categories.getCategoryEditorWindow('trainingprogram', function (node) {
                                                            var categoryText = node.attributes.record.get('levelIndentText');
                                                            var categoryId = node.attributes.record.get('id');
                                                            me.find('name', 'categoryCompositeField')[0].items.items[0].setValue(categoryText);
                                                            me.find('name', 'categoryId')[0].setValue(categoryId);
                                                            w.close();
                                                        }, me.find('name', 'categoryId')[0].getValue(), showRelative).show();
                                                }
                                            }
                                        }]
                                    }, {
                                        xtype: 'hidden',
                                        name: 'certificatePath'
                                    }, {
                                    xtype: 'hidden',
                                        name: 'certificateTemplateId'
                                    }, {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        fieldLabel: 'Certificate',
                                        name: 'certificateCompositeField',
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Certificates),
                                        items: [{
                                            xtype: 'textfield',
                                            valueField: 'id',
                                            displayField: 'name',
                                            allowBlank: true,
                                            flex: 1,
                                            emptyText: 'Select a certificate...',
                                            valueNotFoundText: 'Select a certificate...',
                                            readOnly: true
                                        }, {
                                            xtype: 'button',
                                            text: '...',
                                            handler: function (btn) {
                                                if (!me.isDisabled || me.isSalesforce) {
                                                    var w = Symphony.Certificates.getCertificateEditorWindow(function (node) {
                                                        var certificateText = node.attributes.record.get('levelIndentText');
                                                        me.find('name', 'certificateCompositeField')[0].items.items[0].setValue(certificateText);

                                                        var certificateTemplateId = node.attributes.record.get('certificateTemplateId');
                                                        if (certificateTemplateId) {
                                                            me.find('name', 'certificateTemplateId')[0].setValue(certificateTemplateId);
                                                            // in the event there was a OLDE cert type, blow away the path value
                                                            me.find('name', 'certificatePath')[0].setValue("");
                                                        }
                                                        else {
                                                        var certificatePath = node.attributes.record.get('path');
                                                        me.find('name', 'certificatePath')[0].setValue(certificatePath);
                                                            // in the event there was a new cert type, blow away the id
                                                            me.find('name', 'certificateTemplateId')[0].setValue(null);
                                                        }
                                                        w.close();
                                                    }, me.find('name', 'certificatePath')[0].getValue()).show();
                                                }
                                            }
                                        }]
                                    }, {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        fieldLabel: 'Session',
                                        name: 'sessionCompositeField',
                                        formItemCls: Symphony.Modules.getModuleClass(Symphony.Modules.Salesforce) + ' session-composite-field',
                                        items: [{
                                            name: 'sessionCourseId',
                                            fieldLabel: 'Session',
                                            xtype: 'symphony.pagedcombobox',
                                            url: '/services/classroom.svc/courses/',
                                            model: 'course',
                                            bindingName: 'sessionCourseName',
                                            valueField: 'id',
                                            displayField: 'name',
                                            allowBlank: true,
                                            flex: 1,
                                            emptyText: 'Select a session course...',
                                            valueNotFoundText: 'Select a session course...',
                                            help: {
                                                title: 'Session Course',
                                                text: 'The course users that purchase this training program through Salesforce will register for.'
                                            },
                                            listeners: {
                                                select: function (cmb, record, index) {
                                                    if (record.get('id') > 0) {
                                                        me.find('name', 'sessionCompositeField')[0].innerCt.items.items[1].setDisabled(false);
                                                    }
                                                }
                                            }
                                        }, {
                                            xtype: 'button',
                                            text: '...',
                                            disabled: true,
                                            handler: function (btn) {
                                                Symphony.CourseAssignment.getSessionAssignmentWindow({
                                                    trainingProgramId: me.trainingProgramId,
                                                    sessionCourseId: me.find('name', 'sessionCompositeField')[0].items.items[0].getValue()
                                                }).show();
                                            }
                                        }]
                                    }]
                                }, {
                                    defaults: { anchor: '100%', xtype: 'textfield' },
                                    labelWidth: 130,
                                    items: [{
                                        name: 'isNewHire',
                                        fieldLabel: '&nbsp;&nbsp;New Hire',
                                        xtype: 'checkbox',
                                        disabled: this.trainingProgramId > 0,
                                        listeners: {
                                            check: function (box, checked) {
                                                var fields = me.find('xtype', 'datefield');
                                                for (var i = 0; i < fields.length; i++) {
                                                    if (checked) {
                                                        fields[i].disable();
                                                        fields[i]._oldValue = fields[i].getValue();
                                                        fields[i].setValue('');
                                                    } else {
                                                        fields[i].enable();
                                                        if (fields[i]._oldValue) {
                                                            fields[i].setValue(fields[i]._oldValue);
                                                        }
                                                    }
                                                }
                                                me.find('name', 'newHireTransitionPeriod')[0].setDisabled(checked);
                                                var card = me.find('name', 'scheduleCard')[0].getLayout();
                                                card.setActiveItem(checked ? 1 : 0);
                                                me.fireEvent('newHire', checked);
                                            }
                                        }
                                    }, {
                                        // SETTERS (values used when loading the combobox initially)
                                        // the name of the value; this is used when binding data to the field
                                        name: 'ownerId',
                                        // the name of the displayed text if nothing is found
                                        bindingName: 'ownerName',

                                        // GETTERS (values used when retrieving new values for the selection)
                                        // the name of the displayed text after a search
                                        displayField: 'fullName',
                                        // the name of the value field after a search
                                        valueField: 'id',

                                        fieldLabel: '&nbsp;&nbsp;Owner',
                                        xtype: 'symphony.pagedcombobox',
                                        model: 'user',
                                        url: '/Services/customer.svc/users/customer/{0}/roles/?roles={1}'.format(Symphony.User.customerId, 'CourseAssignment - Training Administrator,CourseAssignment - Training Manager'),
                                        allowBlank: false

                                    }, {
                                        name: 'cost',
                                        fieldLabel: '&nbsp;&nbsp;Cost',
                                        xtype: 'moneyfield',
                                        formatDelay: 750
                                    }, {
                                        name: 'newHireTransitionPeriod',
                                        fieldLabel: '&nbsp;&nbsp;Transition Period',
                                        help: {
                                            title: 'New Hire Transition Period',
                                            text: 'Only applies to non-"New Hire" Training Programs.<br/><br/>The minumum number of days a Training Program must be available to a user in order to be seen by that user after a transition from "New Hire" to "Normal" status.<br/><br>' +
                                                'NOTE: If left blank, this setting will automatically inherit from the company setting (currently set to ' + Symphony.User.customerNewHireTransitionPeriod + ' days).'
                                        },
                                        allowBlank: true,
                                        allowNegative: true,
                                        allowDecimals: false,
                                        xtype: 'numberfield'
                                    }, {
                                        name: 'sku',
                                        fieldLabel: '&nbsp;&nbsp;SKU',
                                        xtype: 'textfield',
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Salesforce)
                                    }, {
                                        name: 'associatedImageData',
                                        fieldLabel: '&nbsp;&nbsp;Image',
                                        xtype: 'associatedimagefield'
                                    }]
                                }]
                            }, {
                                name: 'description',
                                fieldLabel: 'Description',
                                xtype: 'textarea',
                                anchor: '0'
                            }, {
                                xtype: 'compositefield',
                                fieldLabel: 'Goals and Objectives',
                                allowBlank: true,
                                items: [{
                                    xtype: 'htmleditor',
                                    itemId: 'trainingProgramGoalEditor',
                                    name: 'goalsAndObjectives',
                                    height: 30,
                                    flex: 0.5,
                                    listeners: {
                                    },
                                    onRender: function () {
                                        Ext.form.HtmlEditor.prototype.onRender.apply(this, arguments);
                                        this.height = 115;
                                        delete this.width;
                                    }
                                }]
                            }, {

                                layout: {
                                    type: 'hbox',
                                    align: 'stretch'
                                },
                                // column defaults
                                border: false,
                                defaults: {
                                    columnWidth: 0.5,
                                    flex: 0.5,
                                    border: false,
                                    xtype: 'form',
                                    cls: 'x-panel-transparent'
                                },
                                items: [{
                                    defaults: { anchor: '100%', xtype: 'textfield' },
                                    items: [{
                                        fieldLabel: 'Original Release Date',
                                        xtype: 'datefield',
                                        name: 'originalReleaseDate',
                                        dateOnly: true,
                                        allowBlank: true,
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this),
                                            select: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        name: 'minCourseWork',
                                        fieldLabel: 'Min Course Work',
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram) + ' hour-minute-selector',
                                        help: {
                                            title: 'Min Course Work',
                                            text: 'The minimum amount of time a student must spend in the training program in order to receive credit.'
                                        },
                                        allowBlank: true,
                                        xtype: 'classroom.durationfield',
                                        minValue: 0
                                    }, {
                                        name: 'surveyId',
                                        fieldLabel: 'Survey',
                                        xtype: 'symphony.pagedcombobox',
                                        url: '/services/classroom.svc/onlineSurveys/',
                                        model: 'course',
                                        bindingName: 'surveyName',
                                        valueField: 'id',
                                        displayField: 'name',
                                        emptyText: 'Select a Survey...',
                                        valueNotFoundText: 'Select a Survey...',
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Surveys),
                                        listeners: {
                                            blur: function (combo) {
                                                var mandatorySurvey = me.find('name', 'isSurveyMandatory')[0];
                                                if (!combo.getValue()) {
                                                    mandatorySurvey.setDisabled(true);
                                                } else {
                                                    mandatorySurvey.setDisabled(false);
                                                }
                                            },
                                            select: function (combo, record, index) {
                                                if (record) {
                                                    me.find('name', 'isSurveyMandatory')[0].setDisabled(false);
                                                }
                                            }
                                        }
                                    }, {
                                        name: 'isSurveyMandatory',
                                        fieldLabel: 'Survey Mandatory',
                                        help: {
                                            title: 'Survey Mandatory',
                                            text: 'If checked, the survey will be required before certificate is available',
                                            isCheckbox: true
                                        },
                                        xtype: 'checkbox',
                                        disabled: true,
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Surveys)
                                    }, {
                                        name: 'isCourseTimingDisabled',
                                        fieldLabel: 'Disable Timers',
                                        help: {
                                            title: 'Disable Course Timers',
                                            text: 'Disables all online course timers throughout the entire training program.',
                                            isCheckbox: true
                                        },
                                        xtype: 'checkbox',
                                        disabled: false,
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram)
                                    }, {
                                        name: 'durationCompletion',
                                        fieldLabel: 'Adv. Completion',
                                        xtype: 'fieldcontainer',
                                        //cls: 'duration-composite',
                                        anchor: '100%',
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram),
                                        layout: {
                                            type: 'hbox',
                                            pack: 'start'
                                        },
                                        items: [{
                                            xtype: 'checkbox',
                                            listeners: {
                                                check: function (field) {
                                                    me.find('name', 'durationCompletion')[0].setIsEnabled(field.checked);
                                                }
                                            },
                                            help: {
                                                title: 'Advanced Completion',
                                                text: 'If checked, the training program completion percentage will be calculated based on hours of course work completed out of the total hours of course work in the training program. <br/><br/>If left unchecked, the completion percentage will be number of courses complete out of the number of courses in the training program.',
                                                isCheckbox: true
                                            }
                                        }, {
                                            width: 150,
                                            xtype: 'panel',
                                            cls: 'x-panel-transparent',
                                            border: false,
                                            tpl: new Ext.XTemplate(
                                               '<p class="trainingprogram-duration-display">Duration: {duration} {units}</p>'
                                            ),
                                            data: {
                                                duration: '0',
                                                units: 'hours'
                                            }
                                        }, {
                                            anchor: '100%',
                                            fieldLabel: 'Duration',
                                            xtype: 'classroom.durationfield',
                                            durationWidth: 60,
                                            minValue: 0,
                                            value: 0,
                                            hidden: true,
                                            enabled: false
                                        }],
                                        listeners: {
                                            afterrender: function () {
                                                this.refresh();
                                            }
                                        },
                                        refresh: function () {
                                            if (this.items.items) {
                                                var value = this.getValue();
                                                var unit = Symphony.Classroom.getDurationUnits(value, true);

                                                this.items.items[1].update({ duration: Symphony.Classroom.getDurationValue(value), units: unit });
                                            }
                                        },
                                        getValue: function () {
                                            if (this.items.items) {
                                                return this.items.items[2].getValue();
                                            }
                                        },
                                        setValue: function (value) {
                                            if (this.items.items) {
                                                this.items.items[2].setValue(value);
                                            }
                                        },
                                        setIsEnabled: function (enabled) {
                                            if (this.items.items) {
                                                this.items.items[0].setValue(enabled);
                                            }
                                        },
                                        isEnabled: function () {
                                            if (this.items.items) {
                                                return this.items.items[0].getValue();
                                            }
                                        }
                                    }, {
                                        name: 'allowPrinting',
                                        fieldLabel: 'Allow Printing',
                                        help: {
                                            title: 'Allow Printing',
                                            text: 'Allows printing of all ' + Symphony.Aliases.courses + ' when they\'re completed. Individual settings can still be overridden per ' + Symphony.Aliases.course,
                                            isCheckbox: true
                                        },
                                        xtype: 'checkbox',
                                        disabled: false,
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram)
                                    }]
                                }, {
                                    defaults: { anchor: '100%' },
                                    labelWidth: 130,
                                    items: [{
                                        name: 'maxCourseWork',
                                        fieldLabel: '&nbsp;Max Course Work',
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram),
                                        help: {
                                            title: 'Max Daily Course Work',
                                            text: 'The maximum number of hours allowed per day for online course work within this training program. If left blank, the value will inherit from the category setting.'
                                        },
                                        allowBlank: true,
                                        xtype: 'numberfield',
                                        minValue: 1
                                    }, {
                                        xtype: 'checkbox',
                                        fieldLabel: '&nbsp;&nbsp;Validation',
                                        name: 'isValidationEnabled',
                                        help: {
                                            title: 'Validation Interval',
                                            text: 'If enabled, security questions will be asked at the interval specified (in seconds).',
                                            isCheckbox: true
                                        },
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Validation),
                                        listeners: {
                                            check: function (cbo, checked) {
                                                if (!checked) {
                                                    me.find('name', 'validationInterval')[0].setDisabled(true);
                                                } else {
                                                    me.find('name', 'validationInterval')[0].setDisabled(false);
                                                    if (me.showValidationAlert) {
                                                        Ext.Msg.alert('User Validation', 'Enabling user validation requires that each user has a date of birth, and a social security number set as these will be used to identifiy the current user taking the course.');
                                                    }
                                                }
                                            }
                                        }
                                    }, {
                                        xtype: 'numberfield',
                                        fieldLabel: '&nbsp;&nbsp;Interval',
                                        name: 'validationInterval',
                                        anchor: '100%',
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Validation)
                                    }, {
                                        xtype: 'checkbox',
                                        fieldLabel: '&nbsp; Pre-Test Validation',
                                        name: 'preTestValidation',
                                        help: {
                                            title: 'Validate Pre-Test',
                                            text: 'If enabled, security questions will be asked before the start of exam.',
                                            isCheckbox: true
                                        },
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Validation),
                                        listeners: {
                                            check: function (cbo, checked) {
                                                console.log("checkers");
                                            }
                                        }
                                    }, {
                                        xtype: 'checkbox',
                                        fieldLabel: '&nbsp; Post-Test Validation',
                                        name: 'postTestValidation',
                                        help: {
                                            title: 'Validate Post-Test',
                                            text: 'If enabled, security questions will be asked upon completion of exam.',
                                            isCheckbox: true
                                        },
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Validation),
                                        listeners: {
                                            check: function (cbo, checked) {
                                                console.log("checkers");
                                            }
                                        }
                                    }, {
                                        labelStyle: 'width: 190px',
                                        name: 'disableScheduled',
                                        fieldLabel: '&nbsp;&nbsp;Disable Notifications',
                                        xtype: 'checkbox',
                                        help: {
                                            title: 'Disable Notifications',
                                            text: 'If checked, all scheduled notifications for this training program will be disabled.',
                                            isCheckbox: true
                                        }
                                    }, {
                                        labelStyle: '',
                                        name: 'isNmls',
                                        fieldLabel: '&nbsp;&nbsp;NMLS (CE)',
                                        xtype: 'checkbox'
                                    }]
                                }]
                            }]
                        }, {
                            xtype: 'messageboard.create',
                            name: 'createMessageBoard',
                            hidden: me.trainingProgramId > 0 || !Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards)
                        }, {
                            xtype: 'fieldset',
                            title: 'Course Leaders',
                            cls: Symphony.Modules.getModuleClass(Symphony.Modules.InstructorPortal),
                            padding: 7,
                            items: [{

                                xtype: 'courseassignment.leadersgrid',
                                autoheight: false,
                                name: 'courseleaders',
                                listeners: {
                                    added: function (grid, component, index) {
                                        //  grid.getTopToolbar().hide();
                                    }
                                }
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Accreditations',
                            padding: 7,
                            items: [{
                                xtype: 'artisan.accreditationgrid',
                                autoheight: false,
                                name: 'accreditations',
                                height: 180,
                                gridType: 'trainingProgram',
                                trainingProgramId: this.trainingProgramId
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Scheduling',
                            disabled: this.isShared,
                            name: 'schedule',
                            items: [{
                                xtype: 'panel',
                                name: 'scheduleCard',
                                border: false,
                                cls: 'x-panel-transparent',
                                layout: {
                                    type: 'card',
                                    layoutOnCardChange: true
                                },
                                defaults: {
                                    cls: 'x-panel-transparent'
                                },
                                activeItem: 0,
                                items: [{
                                    //width: 300,
                                    layout: 'form',
                                    border: false,
                                    items: [{
                                        fieldLabel: 'Start Date',
                                        name: 'startDate',
                                        xtype: 'datefield',
                                        dateOnly: true,
                                        labelStyle: 'width:180px',
                                        disabled: this.trainingProgramId > 0,
                                        allowBlank: true, // this will be overridden if the checkbox changes
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this),
                                            select: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        fieldLabel: 'Due Date (for notification only)',
                                        name: 'dueDate',
                                        xtype: 'datefield',
                                        dateOnly: true,
                                        labelStyle: 'width:180px',
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this),
                                            select: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        fieldLabel: 'End Date',
                                        name: 'endDate',
                                        xtype: 'datefield',
                                        dateOnly: true,
                                        labelStyle: 'width:180px',
                                        allowBlank: true, // this will be overridden if the checkbox changes
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this),
                                            select: Ext.bind(this.validateDates, this)
                                        }
                                    }]
                                }, {
                                    layout: 'form',

                                    border: false,
                                    //columnWidth: 1,
                                    defaults: {
                                        labelStyle: 'width:220px'
                                    },
                                    items: [{
                                        fieldLabel: 'Enable Custom New Hire Offsets',
                                        name: 'newHireOffsetEnabled',
                                        xtype: 'checkbox',
                                        allowBlank: true,
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this),
                                            check: function (field, checked) {
                                                var fields = this.ownerCt.find('xtype', 'numberfield');
                                                for (var i = 0; i < fields.length; i++) {
                                                    fields[i].setDisabled(!checked);
                                                    fields[i].allowBlank = !checked;
                                                }
                                            }
                                        }
                                    }, {
                                        fieldLabel: 'Start Offset',
                                        name: 'newHireStartDateOffset',
                                        xtype: 'numberfield',
                                        allowBlank: true,
                                        allowNegative: false,
                                        allowDecimals: false,
                                        maxValue: 365,
                                        minValue: 0,
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        fieldLabel: 'Due Offset',
                                        name: 'newHireDueDateOffset',
                                        xtype: 'numberfield',
                                        allowBlank: true,
                                        allowNegative: false,
                                        allowDecimals: false,
                                        maxValue: 365,
                                        minValue: 0,
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        fieldLabel: 'End Offset',
                                        name: 'newHireEndDateOffset',
                                        xtype: 'numberfield',
                                        allowBlank: true,
                                        allowNegative: false,
                                        allowDecimals: false,
                                        maxValue: 365,
                                        minValue: 0,
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        cls: 'x-panel-transparent',
                                        border: false,
                                        html: '<div class="pure-text"><h4>Instructions</h4>' +
                                        '<p>New hire offsets allow you to schedule a training program based on the user\'s hire date.</p>' +
                                        '<p>For example, if a user was hired on 10/11/2012 and the following offsets are added:</p>' +
                                        '<ul><li>Start Offset: 5 days</li><li>Due Offset: 20 days</li><li>End Offset: 25 days</li></ul>' +
                                        '<p>The schedule for the training program would be as follows:</p>' +
                                        '<ul><li>Start Date: 10/16/2012</li><li>Due Date: 10/31/2012</li><li>End Date: 11/5/2012</li></ul></div>'
                                    }]
                                }]
                            }]
                        }]
                    }, {
                        xtype: 'courseassignment.courseoverridepanel',
                        isShared: this.isShared,
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.AdvancedTrainingProgram)
                    }, {
                        title: 'Assign Users',
                        name: 'assignedusers',
                        xtype: 'courseassignment.trainingprogramusers'
                    }, {
                        title: 'Assign Courses',
                        name: 'assignedcourses',
                        isShared: this.isShared,
                        forceLayout: true,
                        xtype: 'courseassignment.trainingprogramcoursepanel',
                        listeners: {
                            showSettingsMenu: function (proctorRequiredIndicator, activeDate, dueDate, enforceCanMoveForwardIndicator, denyAccessAfterDueDateIndicator, record, grid, e, showProctor) {
                                var form = me.find('name', 'general')[0];
                                var tp = form.getValues();
                                var isNewHire = me.find('name', 'isNewHire')[0].getValue();

                                var beforeExpiresTextBox = "This course is due ";
                                var beforeActiveTextBox = "This course is active ";

                                if ((!tp.startDate || !tp.endDate) && !isNewHire) {
                                    Ext.Msg.alert('Error', 'You must select a start and end date for the Training Program before you can set course dates.');
                                    return;
                                }

                                var minDate = Symphony.parseDate(tp.startDate);
                                var maxDate = Symphony.parseDate(tp.endDate);

                                // populate items conditionally: 
                                var _items = [];

                                if (showProctor) {
                                   _items.push({
                                        xtype: 'checkbox',
                                        disabled: record.get('courseTypeId') == Symphony.CourseType.classroom,
                                        name: 'proctorRequiredIndicator',
                                        boxLabel: 'This course requires a proctor',
                                        checked: proctorRequiredIndicator,
                                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Proctor),
                                        handler: function (chkBox, checked) {
                                            record.set('proctorRequiredIndicator', checked);
                                            record.commit();
                                            grid.refresh();
                                        }
                                    });
                                }

                                _items.push({
                                    xtype: 'checkbox',
                                    cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram),
                                    name: 'enforceCanMoveForwardIndicator',
                                    boxLabel: 'This course requires instructor approval to continue',
                                    checked: enforceCanMoveForwardIndicator,
                                    help: {
                                        title: 'Instructor Approval Required',
                                        text: 'If this is set, the Instructor must flag the student as being able to move forward in the Training Program. <br/><br/>SPECIAL NOTE: if this flag is set on the <b>last</b> course in the Required Courses list, then the Final Assessment will be unavailable until the Instructor approves the student'
                                    },
                                    handler: function (chkBox, checked) {
                                        record.set('enforceCanMoveForwardIndicator', checked);
                                        record.commit();
                                        grid.refresh();
                                    }
                                }, {
                                    xtype: 'checkbox',
                                    cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram),
                                    name: 'denyAccessAfterDueDateIndicator',
                                    boxLabel: 'After the due date occurs, deny access to this course',
                                    checked: denyAccessAfterDueDateIndicator,
                                    handler: function (chkBox, checked) {
                                        record.set('denyAccessAfterDueDateIndicator', checked);
                                        record.commit();
                                        grid.refresh();
                                    }
                                });

                                var w = new Ext.Window({
                                    width: 400,
                                    height: 230,
                                    title: 'Settings',
                                    modal: true,
                                    frame: true,
                                    items: [{
                                        xtype: 'panel',
                                        frame: false,
                                        border: false,
                                        cls: 'x-panel-transparent',
                                        items: [{
                                            xtype: 'form',
                                            labelWidth: 0,
                                            hideLabels: true,
                                            padding: '5px',
                                            border: false,
                                            cls: 'x-panel-transparent',
                                            defaults: {
                                                anchor: '100%'
                                            },
                                            items: _items
                                        },
                                        {
                                            xtype: 'form',
                                            labelWidth: 80,
                                            padding: '5px',
                                            border: false,
                                            cls: 'x-panel-transparent',
                                            items: [
                                                Symphony.getCombo('allowPrinting', 'Allow Printing', Symphony.getYesNoInheritStore(), {
                                                    help: {
                                                        title: 'Allow Printing',
                                                        text: 'Allows printing of this ' + Symphony.Aliases.course + ' when it is completed. ' +
                                                            'Setting to "Default" will follow the Training Program setting, setting to "Yes" or "No" will ' +
                                                            'allow or deny for this specific ' + Symphony.Aliases.course
                                                    }
                                                }),
                                                {
                                                    xtype: 'courseassignment.trainingprogramcoursedatefield',
                                                    fieldLabel: 'Active After',
                                                    anchor: '100%',
                                                    calendarTitle: 'Active After Date',
                                                    beforeHoursTextBox: beforeActiveTextBox,
                                                    afterHoursModes: Symphony.ActiveAfterModesStore,
                                                    value: activeDate,
                                                    minDate: minDate,
                                                    maxDate: dueDate ? dueDate : maxDate,
                                                    recordDateField: 'activeAfterDate',
                                                    recordHoursField: 'activeAfterMinutes',
                                                    recordModeField: 'activeAfterMode',
                                                    record: record,
                                                    hidden: tp.isNewHire,
                                                    onShowMenu: function (menu) {

                                                        var value = Symphony.parseDate(record.get('activeAfterDate')),
                                                            dueDate = Symphony.parseDate(record.get('dueDate')),
                                                            maxDueDate = dueDate != null && dueDate.getFullYear && dueDate.getFullYear() < Symphony.defaultYear ? dueDate : maxDate,
                                                            datePicker = menu.find('xtype', 'datepicker')[0],
                                                            durationField = menu.find('xtype', 'classroom.durationfield')[0],
                                                            modeField = menu.find('name', 'mode')[0];

                                                        if (!value) {
                                                            value = new Date();
                                                        }

                                                        if (datePicker) {
                                                            maxDueDate = maxDueDate.add(Date.MINUTE, maxDueDate.getTimezoneOffset()); // convert utc to local
                                                            value = value.add(Date.MINUTE, value.getTimezoneOffset()); // convert utc to local



                                                            datePicker.setMinDate(minDate);
                                                            datePicker.setMaxDate(maxDueDate);

                                                            if (value && value.getFullYear() < Symphony.defaultYear) {
                                                                datePicker.setValue(value);
                                                            }
                                                        }

                                                        if (durationField && record.get('activeAfterMinutes')) {
                                                            durationField.setValue(record.get('activeAfterMinutes'));
                                                            modeField.setValue(record.get('activeAfterMode'));
                                                        }
                                                    },
                                                    listeners: {
                                                        render: function () {
                                                            var date = Symphony.parseDate(record.get('activeAfterDate')),
                                                                minutes = record.get('activeAfterMinutes'),
                                                                afterHours = Symphony.ActiveAfterModesStore.getById(record.get('activeAfterMode') ? record.get('activeAfterMode') : Symphony.RelativeTimeMode.trainingProgram).get('text');

                                                            if (minutes) {
                                                                this.setDisplayValue(Symphony.Classroom.durationRenderer(minutes) + ' ' + afterHours);
                                                            } else if (date && date.getFullYear() < Symphony.defaultYear) {

                                                                date = date.add(Date.MINUTE, date.getTimezoneOffset()); // convert utc to local

                                                                var displayText = date.format('M j, Y');

                                                                this.setDateRange(date.format('j/n/Y'), date.format('j/n/Y'), displayText);
                                                            }
                                                        }
                                                    }
                                                }, {
                                                    xtype: 'courseassignment.trainingprogramcoursedatefield',
                                                    fieldLabel: 'Due',
                                                    anchor: '100%',
                                                    calendarTitle: 'Due Date',
                                                    beforeHoursTextBox: beforeExpiresTextBox,
                                                    afterHoursModes: Symphony.DueModesStore,
                                                    value: dueDate,
                                                    minDate: activeDate ? activeDate : minDate,
                                                    maxDate: maxDate,
                                                    recordDateField: 'dueDate',
                                                    recordHoursField: 'expiresAfterMinutes',
                                                    recordModeField: 'dueMode',
                                                    record: record,
                                                    hidden: tp.isNewHire,
                                                    onShowMenu: function (menu) {

                                                        var value = Symphony.parseDate(record.get('dueDate')),
                                                            activeAfterDate = Symphony.parseDate(record.get('activeAfterDate'));
                                                        var minDueDate = minDate;
                                                        if (!isNaN(Date.parse(activeAfterDate))) {
                                                            minDueDate = activeAfterDate.getFullYear() < Symphony.defaultYear ? activeAfterDate : minDate;
                                                        }
                                                        var datePicker = menu.find('xtype', 'datepicker')[0],
                                                            durationField = menu.find('xtype', 'classroom.durationfield')[0],
                                                            modeField = menu.find('name', 'mode')[0];

                                                        if (!value) {
                                                            value = new Date();
                                                        }

                                                        if (datePicker) {
                                                            minDueDate = minDueDate.add(Date.MINUTE, minDueDate.getTimezoneOffset()); // convert utc to local
                                                            value = value.add(Date.MINUTE, value.getTimezoneOffset()); // convert utc to local

                                                            datePicker.setMinDate(minDueDate);
                                                            datePicker.setMaxDate(maxDate);

                                                            if (value && value.getFullYear() < Symphony.defaultYear) {
                                                                datePicker.setValue(value);
                                                            }
                                                        }

                                                        if (durationField && record.get('expiresAfterMinutes')) {
                                                            durationField.setValue(record.get('expiresAfterMinutes'));
                                                            modeField.setValue(record.get('dueMode'));
                                                        }
                                                    },
                                                    listeners: {
                                                        render: function () {
                                                            var date = Symphony.parseDate(record.get('dueDate')),
                                                                minutes = record.get('expiresAfterMinutes'),
                                                                afterHours = Symphony.DueModesStore.getById(record.get('dueMode') ? record.get('dueMode') : Symphony.RelativeTimeMode.trainingProgram).get('text');

                                                            if (minutes) {
                                                                this.setDisplayValue(Symphony.Classroom.durationRenderer(minutes) + ' ' + afterHours);
                                                            } else if (date && date.getFullYear() < Symphony.defaultYear) {
                                                                date = date.add(Date.MINUTE, date.getTimezoneOffset()); // convert utc to local

                                                                var displayText = date.format('M j, Y');

                                                                this.setDateRange(date.format('j/n/Y'), date.format('j/n/Y'), displayText);
                                                            }
                                                        }
                                                    }
                                                }]
                                        }, {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            cls: 'x-panel-transparent',
                                            border: false,
                                            items: [{
                                                xtype: 'button',
                                                text: 'OK',
                                                width: 40,
                                                cls: 'daterange-button',
                                                style: 'margin-right: -5px',
                                                handler: function () {

                                                    if (record.get('dueMode') && record.get('activeAfterMode')) {
                                                        if (record.get('dueMode') != Symphony.RelativeTimeMode.course) {
                                                            if (record.get('expiresAfterMinutes') < record.get('activeAfterMinutes')) {
                                                                Ext.Msg.alert('Error', 'The active after date must occur prior to the due date.');
                                                                return;
                                                            }
                                                        }
                                                    }

                                                    w.close();
                                                    grid.refresh();
                                                }
                                            }]
                                        }]
                                    }]
                                })
                                w.show();
                            }

                        }
                    }, {
                        title: 'Documents',
                        name: 'documents',
                        layout: 'fit',
                        border: true,
                        stateId: 'courseassignment.trainingprogramdocumentsgrid',
                        isShared: this.isShared,
                        xtype: 'courseassignment.trainingprogramdocumentsgrid'
                    }, {
                        title: 'Parameters',
                        name: 'parameters',
                        layout: 'fit',
                        border: false,
                        xtype: 'courseassignment.trainingprogramparameterpanel',
                        isLockBySharedTrainingFlag: this.isShared,
                        trainingProgramForm: me,
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Parameters),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.Parameters)
                    }, {
                        title: 'Message Board',
                        name: 'messageboard',
                        xtype: 'messageboard.app',
                        isLockBySharedTrainingFlag: this.isShared,
                        disabled: true,
                        showForumGrid: false,
                        border: true,
                        listeners: {
                            activate: {
                                fn: function (panel) {
                                    var trainingProgram = me.find('name', 'general')[0].getValues();
                                    panel.loadMessageBoardByTrainingProgramId(trainingProgram.name, me.trainingProgramId, null, me.isDisabled ? false : true);
                                },
                                single: true
                            }
                        }
                    }, {
                        xtype: 'book.trainingprogrambookpanel',
                        name: 'books',
                        isLockBySharedTrainingFlag: this.isShared,
                        trainingProgramId: me.trainingProgramId,
                        title: 'Books',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Books),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.Books)
                    }, {
                        title: 'Licenses',
                        name: 'licenses',
                        xtype: 'license.trainingprogramassignmentpanel',
                        isLockBySharedTrainingFlag: this.isShared,
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Licenses),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.Licenses)
                    }, {
                        xtype: 'certificates.metadatapanel',
                        isLockBySharedTrainingFlag: this.isShared,
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Certificates),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.Certificates)
                    }, {
                        title: 'Notifications',
                        name: 'notifications',
                        xtype: 'courseassignment.trainingprogramnotificationsgrid',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.AdvancedTrainingProgram)
                    }]
                }]
            }],
            listeners: {
                newHire: function (newHire) {
                    var coursePanel = me.findByType('courseassignment.trainingprogramcoursepanel')[0];
                    coursePanel.setNewHire(newHire);
                },
                render: function () {
                    if (me.isDisabled) {
                        me.disable();
                    }
                },
                activate: function () {
                    if (me.studentProgress) {
                        me.studentProgress.renderTrainingProgram();
                    }
                }
            }
        });
        this.callParent(arguments);
    },
    validateDates: function () {
        var start = this.find('name', 'startDate')[0];
        var due = this.find('name', 'dueDate')[0];
        var end = this.find('name', 'endDate')[0];

        // start date
        if (start.getValue() && end.getValue() && start.getValue() > end.getValue()) {
            start.markInvalid('The start date must come before the end date.');
        } else if (start.getValue() && due.getValue() && start.getValue() > due.getValue()) {
            start.markInvalid('The start date must come before the due date.');
        } else {
            start.clearInvalid();
        }

        // due date
        if (due.getValue() && start.getValue() && due.getValue() <= start.getValue()) {
            due.markInvalid('The due date must come after the start date.');
        } else if (due.getValue() && end.getValue() && due.getValue() > end.getValue()) {
            due.markInvalid('The due date must be on or before the end date.');
        } else {
            due.clearInvalid();
        }

        // end date
        if (start.getValue() && end.getValue() && end.getValue() <= start.getValue()) {
            end.markInvalid('The end date must come after the start date.');
        } else if (due.getValue() && end.getValue() && end.getValue() < due.getValue()) {
            end.markInvalid('The end date must be on or after the due date.');
        } else {
            end.clearInvalid();
        }

        // set min/max values here so form validation works
        var maxStart = due.getValue() || end.getValue();
        if (maxStart) {
            start.setMaxValue(maxStart.add(Date.DAY, 1));
        }
        var minDue = start.getValue();
        if (minDue) {
            due.setMinValue(minDue.add(Date.DAY, 1));
        }
        var maxDue = end.getValue();
        if (maxDue) {
            due.setMaxValue(maxDue);
        }
        var minEnd = due.getValue(true) || start.getValue(true);
        if (minEnd) {
            if (!due.getValue()) {
                // after the start date
                end.setMinValue(minEnd.add(Date.DAY, 1));
            } else {
                // equal to the due date
                end.setMinValue(minEnd);
            }
        }

        // if the start date is already passed, the date cannot be changed
        var newHire = this.find('name', 'isNewHire')[0];
        var current = new Date();
        if (!start.getValue() || start.getValue() > current) {
            // enable the start field if the TP hasn't started
            // but not if it's a new hire TP
            if (!newHire.getValue()) {
                start.setDisabled(false);
            }
            newHire.setDisabled(false);
        }

        this.find('name', 'newHireTransitionPeriod')[0].setDisabled(this.isDisabled ? true : newHire.getValue());

        var card = this.find('name', 'scheduleCard')[0].getLayout();
        var isNewHire = newHire.getValue();
        card.setActiveItem(isNewHire ? 1 : 0);

        var offsetEnabled = this.find('name', 'newHireOffsetEnabled')[0];
        var fields = offsetEnabled.ownerCt.find('xtype', 'numberfield');
        var isOffsetEnabled = offsetEnabled.getValue();
        for (var i = 0; i < fields.length; i++) {
            fields[i].setDisabled(!isOffsetEnabled);
            fields[i].allowBlank = !isOffsetEnabled;
            if (isOffsetEnabled && fields[i].getValue() === '') {
                fields[i].markInvalid('This field is required if custom new hire offsets are selected.');
            }
        }

        if (isOffsetEnabled) {
            var startOffset = offsetEnabled.ownerCt.find('name', 'newHireStartDateOffset')[0].getValue();
            var dueOffset = offsetEnabled.ownerCt.find('name', 'newHireDueDateOffset')[0].getValue();
            var endOffset = offsetEnabled.ownerCt.find('name', 'newHireEndDateOffset')[0].getValue();
            if (startOffset >= dueOffset) {
                offsetEnabled.ownerCt.find('name', 'newHireStartDateOffset')[0].markInvalid('Start offset must be less than due offset.');
            } else {
                offsetEnabled.ownerCt.find('name', 'newHireStartDateOffset')[0].clearInvalid();
            }
            if (startOffset >= endOffset) {
                offsetEnabled.ownerCt.find('name', 'newHireEndDateOffset')[0].markInvalid('End offset must be greater than start offset.');
            } else {
                offsetEnabled.ownerCt.find('name', 'newHireEndDateOffset')[0].clearInvalid();
            }
            if (dueOffset > endOffset) {
                offsetEnabled.ownerCt.find('name', 'newHireDueDateOffset')[0].markInvalid('Due offset must be less than or equal to end offset.');
            } else {
                offsetEnabled.ownerCt.find('name', 'newHireDueDateOffset')[0].clearInvalid();
            }
        } else {
            offsetEnabled.ownerCt.find('name', 'newHireStartDateOffset')[0].setValue(null);
            offsetEnabled.ownerCt.find('name', 'newHireEndDateOffset')[0].setValue(null);
            offsetEnabled.ownerCt.find('name', 'newHireDueDateOffset')[0].setValue(null);
        }
    },
    afterRender: function () {
        Symphony.CourseAssignment.TrainingProgramForm.superclass.afterRender.apply(this, arguments);
        this.trainingProgramTabs.activate(this.generalTab);
        this.load();
        //this.load();
    },
    load: function () {
        var me = this;
        if (this.trainingProgramId) {
            Ext.Msg.wait('Please wait while your program is loaded...', 'Please wait...');
            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/portal.svc/trainingprogramdetails/' + this.trainingProgramId + '?manage=true',
                success: function (result) {
                    var messageBoard = me.find('xtype', 'messageboard.app')[0];
                    if (messageBoard) {
                        messageBoard.setDisabled(false);
                    }

                    // to prevent shared TP's from stomping the CusomerID, we need to make sure we preserve CustomerID and propagate through the save/update
                    me.trainingProgramCustomerId = result.data.customerId;

                    // load up the appropriate panels with the results
                    me.trainingProgramId = result.data.id;
                    me.parameterOverrides = result.data.trainingProgramParameterOverrides;

                    // Copy over the original survey id so we can show
                    // the correct item in the drop down and not the duplicate
                    result.data.surveyId = result.data.originalSurveyId;
                    result.data.surveyName = result.data.originalSurveyName;

                    // set the basic information
                    me.bindForm(result.data);

                    // set overrides
                    me.find('xtype', 'courseassignment.courseoverridepanel')[0].loadData(result.data);

                    // Set meta data
                    me.find('xtype', 'certificates.metadatapanel')[0].setData(result.data);

                    me.showValidationAlert = !result.data.isValidationEnabled;
                    me.find('name', 'validationInterval')[0].setDisabled(!result.data.isValidationEnabled);

                    // set the training program for the docs
                    me.find('name', 'documents')[0].setTrainingProgram(result.data);

                    // add the selected user groups
                    me.find('name', 'assignedusers')[0].setAssignments(result.data.primaryAssignment, result.data.secondaryAssignment);

                    // add the assigned courses
                    me.find('name', 'assignedcourses')[0].setTrainingProgram(result.data);

                    // add the leaders
                    me.find('name', 'courseleaders')[0].setData(result.data.leaders);

                    // add the licenses
                    me.find('name', 'licenses')[0].setData(result.data.licenses);

                    // set training program data for notifications
                    me.find('name', 'notifications')[0].trainingProgram = result.data;

                    // add books
                    me.find('xtype', 'book.trainingprogrambookpanel')[0].setData(result.data.books);

                    me.find('xtype', 'artisan.accreditationgrid')[0].getStore().loadRawData(result.data.accreditations);

                    if (result.data.categoryId) {
                        var categoryString = result.data.levelIndentText;
                        if (!categoryString) {
                            categoryString = result.data.categoryName;
                        }
                        me.find('name', 'categoryCompositeField')[0].items.items[0].setValue(categoryString);
                    }

                    // new cert trumps old: 
                    if (result.data.certificateTemplateId) {
                        // get the certificate details: 
                        Symphony.Ajax.request({
                            url: '/services/certificate.svc/certificateTemplates/' + result.data.certificateTemplateId,
                            method: "GET",
                            success: function (res) {
                                console.log(res.data);
                                me.find('name', 'certificateCompositeField')[0].items.items[0].setValue(res.data.name);
                            },
                            failure: function () {
                            }
                        });
                    } else if (result.data.certificatePath) {
                        me.find('name', 'certificateCompositeField')[0].items.items[0].setValue(Symphony.Certificates.pathToLevelIndentText(result.data.certificatePath));
                    }

                    if (result.data.sessionCourseId > 0) {
                        var sessionField = me.find('name', 'sessionCompositeField')[0];
                        if (sessionField.rendered) {
                            sessionField.items.items[1].setDisabled(false);
                        } else {
                            sessionField.initialConfig.items[1].disabled = false;
                        }
                    }

                    // documents take care of themselves, so nothing to do there

                    // disable as needed
                    if (Symphony.User.isTrainingManager && !Symphony.User.isTrainingAdministrator && result.data.ownerId != Symphony.User.id) {
                        me.disable();
                    }
                    if (result.data.surveyId) {
                        me.find('name', 'isSurveyMandatory')[0].setDisabled(false);
                    } else {
                        me.find('name', 'surveyId')[0].clearValue();
                    }

                    var durationCompletion = me.find('name', 'durationCompletion')[0];
                    durationCompletion.setIsEnabled(result.data.isAdvancedCompletionPercentage);
                    durationCompletion.setValue(result.data.duration);
                    durationCompletion.refresh();

                    Ext.Msg.hide();

                    me.trainingProgramTabs.setActiveTab(0);
                }
            });
        } else {
            window.setTimeout(function () {
                me.find('name', 'assignedcourses')[0].setTrainingProgram();
                // default the owner to the current user
                var cbo = me.find('name', 'general')[0].find('name', 'ownerId')[0];
                var rec = { id: Symphony.User.id, fullName: Symphony.User.fullName };
                cbo.getStore().insert(0, rec);
                cbo.valueNotFoundText = Symphony.User.fullName;
                cbo.setValue(Symphony.User.id);
                cbo.findRecordByValue(Symphony.User.id);
                // default the category
                var cf = me.find('name', 'categoryCompositeField')[0];

                if (cf.items.itemAt) {
                    cf.items.itemAt(0).setValue('', true, true);
                }
                // set overrides
                me.find('xtype', 'courseassignment.courseoverridepanel')[0].loadData();

                me.validateDates();
            }, 1);
        }
    },
    bindForm: function (data) {
        // first, bind the basic information
        this.find('name', 'general')[0].bindValues(data);

        var cf = this.find('name', 'categoryCompositeField')[0];

        if (data.categoryId) {
            if (cf.items.itemAt && cf.items.itemAt(0)) {
                cf.items.itemAt(0).setValue(data.categoryId, true, true);
            }
        }
        else {
            if (cf.items.itemAt && cf.items.itemAt(0)) {
                cf.items.itemAt(0).setValue(0, true, true);
            }
        }

        // add the schedule
        var schedules = this.find('name', 'schedule')[0].findBy(function (field) { return field.name !== undefined && field.setValue != null; })
        for (var i = 0; i < schedules.length; i++) {
            var item = schedules[i];
            // clear out any min/max settings before we set the value
            // to avoid issues with the invalid indicator
            if (item.xtype == 'datefield') {
                item.setMinValue('01/01/1900');
                item.setMaxValue('01/01/3000');
                item.clearInvalid();

                if (item.name != 'dueDate') {
                    item.allowBlank = !data.isLive;
                }

                var dt = Symphony.parseDate(data[item.name || item.id]);

                if (item.setValue && dt.getFullYear() != Symphony.defaultYear) {
                    item.setValue(dt);
                }
            } else if (item.setValue) {
                item.setValue(data[item.name || item.id]);
            }
        };

        // validate the schedule (sets the min/max dates, etc)
        this.validateDates();
    },
    save: function (duplicate, isCheckForRollup) {
        // saves a training program
        var me = this;

        isCheckForRollup = Symphony.Settings.isCheckForRollupOverride ? true : isCheckForRollup ? true : false;

        // Temporary - Re-enable the rollup completely for now
        // Eventually we need to track this client side to ensure
        // that we have actually changed data that requires a rollup
        // (Assignments, Courses, Start/End dates, Is live toggle
        // see existing rules - CourseAssignmentController around
        // line 1700)
        isCheckForRollup = true;

        // for duplications, require start and end dates
        var previousAllowBlank = this.find('name', 'startDate')[0].allowBlank;
        if (duplicate) {
            if (me.trainingProgramId == 0) {
                Ext.Msg.alert('Invalid Action', 'You cannot duplicate an un-saved training program.');
                return;
            }
            this.find('name', 'startDate')[0].allowBlank = false;
            this.find('name', 'endDate')[0].allowBlank = false;
        }


        var form = me.find('name', 'general')[0];
        
        if (!form.isValid()) {
            Ext.Msg.alert('Errors Detected', 'Your training program has some errors. Please correct and re-save');
            return;
        }
        //            if(!form.isValid()){ 
        //                // so failed duplications don't impact other saves
        //                this.find('name','startDate')[0].allowBlank = previousAllowBlank;
        //                this.find('name','endDate')[0].allowBlank = previousAllowBlank;
        //                return false; 
        //            }

        var users = me.find('name', 'assignedusers')[0];
        var courses = me.find('name', 'assignedcourses')[0];
        var documents = me.find('name', 'documents')[0];
        var parameters = me.find('name', 'parameters')[0];
        //            if(!courses.isValid()){ 
        //                return false; 
        //            }

        // basic information
        var tp = form.getValues();

        // need to null the certTemplateId if it is ""
        if (!tp.certificateTemplateId) {
            tp.certificateTemplateId = null;
        }

        // preserve the TP's customerId
        tp.customerId = me.trainingProgramCustomerId || 0;

        tp.sharedFlag = this.isShared;
        // add course overrides
        Ext.apply(tp, me.find('xtype', 'courseassignment.courseoverridepanel')[0].getValues());

        // add meta data
        tp.metaDataJson = me.find('xtype', 'certificates.metadatapanel')[0].getData();

        tp.leaders = me.find('name', 'courseleaders')[0].getData();

        tp.licenses = me.find('name', 'licenses')[0].getData();

        tp.books = me.find('name', 'books')[0].getData();

        if (parameters) {
            tp.trainingProgramParameterOverrides = parameters.getParameterOverrides();
        } else {
            tp.trainingProgramParameterOverrides = [];
        }

        // some defaults if not specified
        tp.cost = tp.cost || 0;

        // avoid setting it completely if it has a blank value
        var nullable = ['dueDate', 'endDate', 'startDate', 'newHireStartDateOffset', 'newHireEndDateOffset', 'newHireDueDateOffset', 'newHireTransitionPeriod', 'sessionCourseId'];
        for (var i = 0; i < nullable.length; i++) {
            if (!tp[nullable[i]]) {
                delete tp[nullable[i]];
            }
        }

        if (!tp.categoryId) {
            tp.categoryId = 0;
        }

        // users
        var userAssignments = users.getAssignments();
        Ext.apply(tp, userAssignments);

        // courses
        var courseAssignments = courses.getAssignments();
        if (courseAssignments.electiveCourses.length < courseAssignments.minimumElectives) {
            Ext.Msg.alert('Invalid Entry', 'Cannot require ' + courseAssignments.minimumElectives + ' of ' + courseAssignments.electiveCourses.length + ' electives. Please validate course assignment.')
            return;
        }

        if (courseAssignments.optionalCourses.length < 1 && courseAssignments.requiredCourses.length < 1 && courseAssignments.minimumElectives < 1) {
            Ext.Msg.alert('Warning', 'Warning, you don\'t have at least one required course, at least one optional course, or a minimum elective course value of at least one.')
            return;
        }

        // 7/10/12, started this - may want to add later
        /*if (courseAssignments.electiveCourses.length > 0 && courseAssignments.minimumElectives == 0) {
        Ext.Msg.alert('Invalid Entry', 'If you have any elective courses, you must at least require one of them electives. Please validate course assignment.')
        return;
        }*/
        Ext.apply(tp, courseAssignments);

        // duplication info
        if (duplicate) {
            tp.duplicateFromId = me.trainingProgramId;
        }

        if (tp.surveyId === '' || !tp.surveyId) {
            tp.surveyId = 0;
        }

        if (!tp.validationInterval) {
            delete tp.validationInterval;
        }

        if (!tp.maxCourseWork) {
            delete tp.maxCourseWork;
        }

        if (duplicate) {
            tp.sku = null;
        }

        if (!tp.sku) {
            tp.sku = null;
        }


        var notifications = me.find('name', 'notifications')[0].getData();
        if (notifications != null) {
            tp.isCustomNotificationsEnabled = notifications.isCustomNotificationsEnabled;
            tp.notificationTemplates = notifications.notificationTemplates;
        }


        var durationCompletion = me.find('name', 'durationCompletion')[0];
        tp.isAdvancedCompletionPercentage = durationCompletion.isEnabled();
        tp.duration = durationCompletion.getValue();

        //double check proctorFormId value and default to 1 if null:
        if (!tp.proctorFormId) {
            tp.proctorFormId = 1;
        }

        var gridAccreditations = me.find('xtype', 'artisan.accreditationgrid')[0];
        if (gridAccreditations) {
            var store = gridAccreditations.getStore();
            var writer = store.getProxy().getWriter();
            var accreditationData = gridAccreditations.getStore().getRange();

            tp.accreditations = [];
            Ext.each(accreditationData, function(accreditation) {
                tp.accreditations.push(writer.getRecordData(accreditation));
            });

            Ext.each(store.getRemovedRecords(), function(accreditation) {
                tp.accreditations.push(writer.getRecordData(accreditation));
            });
        }
        
        Ext.Msg.wait('Please wait while your training program is saved...', 'Please wait...');

        // save and process files
        Symphony.Ajax.request({
            url: '/services/courseassignment.svc/trainingprograms/' + (duplicate ? 0 : me.trainingProgramId) + '?isCheckForRollup=' + isCheckForRollup,
            jsonData: tp,
            success: function (result) {

                if (duplicate) {
                    me.fireEvent('duplicate', result.data.id, result.data.name, result.data.description);
                } else {

                    var messageBoardCreate = me.find('xtype', 'messageboard.create')[0];
                    if (messageBoardCreate) {
                        messageBoardCreate.setVisible(false);
                    }


                    me.trainingProgramId = result.data.id;

                    me.setTitle(result.data.name);

                    me.find('xtype', 'artisan.accreditationgrid')[0].updateUrlId(result.data.id);
                    // documents
                    if (documents.store.queryBy(function (r) { return !(r.get('id') > 0) }).length > 0) {
                        Ext.Msg.wait('Please wait while your documents are uploaded...', 'Please wait...');

                        // make sure the id is set properly
                        documents.setTrainingProgram({ id: result.data.id });

                        // upload new files
                        documents.upload({
                            trainingProgramId: result.data.id,
                            onComplete: function () {
                                Ext.Msg.hide();
                                me.fireEvent('save');

                                me.load();
                            }
                        });

                    } else {
                        me.fireEvent('save');

                        me.load();
                    }
                    // delete "removed" files from the TP
                }
            }
        });
    },
    disable: function () {
        var me = this;

        me.isDisabled = true;

        var general = me.find('name', 'general')[0];

        var salesforceDisabled = [
            'sku'
        ];

        general.getForm().getFields().each(function (item) {
            if (me.isSalesforce &&
                    item.name != 'sku') {
                return;
            }

            item.disable();
        });

        me.find('name', 'assignedusers')[0].disable();

        me.find('name', 'licenses')[0].disable();

        me.find('name', 'documents')[0].disable();

        if (!me.isSalesforce) { // Allowing users to edit certain sections of a salesforce assigned training program.
            var leadersGrid = general.find('xtype', 'courseassignment.leadersgrid')[0];
            if (leadersGrid) {
                leadersGrid.disable();
            }

            var courseoverrides = me.find('xtype', 'courseassignment.courseoverridepanel')[0];
            if (courseoverrides) {
                courseoverrides.find('xtype', 'form')[0].items.each(function (item) {
                    item.setDisabled(true);
                });
            }

            me.find('xtype', 'certificates.metadatapanel')[0].disable();

            me.find('name', 'assignedcourses')[0].disable();

            me.find('xtype', 'symphony.savecancelbar')[0].find('name', 'save')[0].disable();
        }

    },
    previewCertificate: function () {
        var w = Ext.create('certificates.certificatepreview', {
            isPreview: true,
            trainingProgramId: this.trainingProgramId
        });
        w.show();
    },
    printCertificate: function () {
        var w = Ext.create('certificates.certificatepreview', {
            isPreview: false,
            trainingProgramId: this.trainingProgramId,
            userId: this.userId
        });
        w.show();
    }
});
