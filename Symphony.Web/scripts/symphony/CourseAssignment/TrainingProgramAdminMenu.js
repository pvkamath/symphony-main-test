﻿Symphony.CourseAssignment.TrainingProgramAdminMenu = Ext.define('symphony.courseassignment.resetcoursedialogextforassignments', {
    alias: 'widget.symphony.courseassignment.resetcoursedialogextforassignments',
    extend: 'Ext.menu.Menu',
    trainingProgram: null,
    courseRecord: null,
    userId: null,
    callback: null,
    grid: null,
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            items: [{
                text: 'Reset Attempts<span class="admin-menu-attempt-count">' + me.courseRecord.get('testAttemptCount') + '</span>',
                handler: function () {
                    new Symphony.CourseAssignment.ResetCourseDialog({
                        callback: function (data) {
                            me.closeMenu();
                        },
                        action: 'resetattempts',
                        onlineCourseRollupId: me.courseRecord.get('onlineCourseRollupId')
                    }).show();
                }
            }, {
                text: 'Clear Registration',
                iconCls: 'x-menu-revert',
                handler: function () {
                    new Symphony.CourseAssignment.ResetCourseDialogExtForAssignments({
                        callback: function (data) {
                            me.closeMenu();
                        },
                        action: 'deleteregistration',
                        onlineCourseRollupId: me.courseRecord.get('onlineCourseRollupId')
                    }).show();
                }
            },
            '-',
            {
                text: 'Bump Forward',
                iconCls: 'x-menu-forward',
                handler: function () {
                    me.courseRecord.set('passed', true);
                    me.courseRecord.set('canMoveForwardIndicator', true);
                    me.courseRecord.set('score', 100);

                    me.saveCourseRecord("Bumped Forward", true);
                }
            },
            '-',
            {
                text: 'Change Score',
                name: 'changeScoreMenu',
                ref: 'changeScoreMenu',
                iconCls: 'x-menu-edit',
                menu: {
                    items: [{
                        xtype: 'panel',
                        frame: false,
                        border: false,
                        cls: 'x-panel-transparent',
                        layout: 'form',
                        labelWidth: 50,
                        width: 200,
                        items: [{
                            xtype: 'numberfield',
                            minValue: 0,
                            maxValue: 100,
                            fieldLabel: 'Score',
                            name: 'score',
                            ref: '../../score',
                            value: me.courseRecord.get('score'),
                            anchor: '100%',
                            labelWidth: 50
                        }, {
                            xtype: 'button',
                            text: 'Ok',
                            style: 'float: right',
                            width: 80,
                            handler: function () {
                                me.courseRecord.set('score', me.getScore());
                                me.saveCourseRecord("Changed Score to " + me.getScore());
                                me.hide();
                            }
                        }]
                    }]
                }
            }, {
                text: 'Status',
                iconCls: 'x-menu-course-status',
                menu: {
                    items: [{
                        text: 'Incomplete',
                        group: 'status',
                        checked: me.courseRecord.get('passed') ? false : true,
                        checkHandler: function (item, checked) {
                            if (checked) {
                                me.courseRecord.set('passed', false);
                                me.courseRecord.set('canMoveForwardIndicator', false);
                                me.saveCourseRecord('Changed Passed to false, Changed CanMoveForwardIndicator to false');
                                me.hide();
                            }
                        }
                    }, {
                        text: 'Complete',
                        checked: me.courseRecord.get('passed') && !me.courseRecord.get('canMoveForwardIndicator') ? true : false,
                        group: 'status',
                        checkHandler: function (item, checked) {
                            if (checked) {
                                me.courseRecord.set('passed', true);
                                me.courseRecord.set('canMoveForwardIndicator', false);
                                me.saveCourseRecord('Changed Passed to true, Changed CanMoveForwardIndicator to false');
                                me.hide();
                            }
                        }
                    }, {
                        text: 'Complete and Can Move Forward',
                        checked: me.courseRecord.get('passed') && me.courseRecord.get('canMoveForwardIndicator') ? true : false,
                        group: 'status',
                        checkHandler: function (item, checked) {
                            if (checked) {
                                me.courseRecord.set('passed', true);
                                me.courseRecord.set('canMoveForwardIndicator', true);
                                me.saveCourseRecord('Changed Passed to true, Changed CanMoveForwardIndicator to true');
                                me.hide();
                            }
                        }
                    }]
                }
            }, {
                text: 'Assignments',
                iconCls: 'x-menu-assignments',
                disabled: !me.courseRecord.get('hasAssignments') || !me.courseRecord.get('onlineCourseRollupId'),
                menu: {
                    items: [{
                        text: 'Unmarked',
                        checked: !me.courseRecord.get('isAssignmentMarked'),
                        group: 'assignment',
                        checkHandler: function (item, checked) {
                            if (checked) {
                                me.setAssignmentStatus(Symphony.AssignmentResponseStatus.unmarked);
                            }
                        }
                    }, {
                        text: 'Failed',
                        checked: me.courseRecord.get('isAssignmentMarked') && me.courseRecord.get('isAssignmentRequired'),
                        group: 'assignment',
                        checkHandler: function (item, checked) {
                            if (checked) {
                                me.setAssignmentStatus(Symphony.AssignmentResponseStatus.incorrect);
                            }
                        }
                    }, {
                        text: 'Complete',
                        checked: me.courseRecord.get('isAssignmentMarked') && !me.courseRecord.get('isAssignmentRequired'),
                        group: 'assignment',
                        checkHandler: function (item, checked) {
                            if (checked) {
                                me.setAssignmentStatus(Symphony.AssignmentResponseStatus.correct);
                            }
                        }
                    }]
                }
            }, {
                text: 'Duration',
                ref: 'durationMenu',
                iconCls: 'x-menu-duration',
                menu: {
                    items: [{
                        xtype: 'panel',
                        frame: false,
                        border: false,
                        cls: 'x-panel-transparent',
                        layout: 'form',
                        labelWidth: 50,
                        width: 400,
                        loader: {
                            url: '/services/courseassignment.svc/onlinecourserollup/' + me.courseRecord.get('onlineCourseRollupId') + '/',
                            autoLoad: true,
                            loadMask: true,
                            renderer: function (loader, response, active) {
                                var result = Ext.decode(response.responseText);
                                var rollup = result.data;
                                var cmp = loader.target;

                                // set the proctor code
                                var prLabel = me.down('#proctorLabel');
                                if (rollup && rollup.proctorKey && prLabel) {
                                    prLabel.setText('Proctor Key - ' + rollup.proctorKey);
                                }

                                var durationField = Ext.create('classroom.durationfield', {
                                    name: 'duration',
                                    fieldLabel: 'Duration',
                                    xtype: 'classroom.durationfield',
                                    cls: 'hour-minute-selector',
                                    labelWidth: 50,
                                    width: 250
                                });

                                var btn = Ext.create('Ext.Button', {
                                    text: 'Ok',
                                    style: 'float: right',
                                    width: 80,
                                    handler: function () {
                                        var durationSeconds = durationField.getValue() * 60;

                                        me.courseRecord.set('updateTotalSeconds', durationSeconds);
                                       
                                        me.saveCourseRecord('Set totalSeconds, totalSecondsRounded, and totalSecondsLastAttempt to ' + durationSeconds);

                                        me.hide();
                                    }
                                });

                                cmp.add(durationField);
                                cmp.add(btn);

                                durationField.setValue(rollup && rollup.totalSecondsRounded ? rollup.totalSecondsRounded / 60 : 0);
                            }
                        }
                    }]
                }
            },
            '-',
            {
                text: 'Version',
                ref: 'versionMenu',
                iconCls: 'x-menu-information',
                menu: {
                    items: [{
                        xtype: 'panel',
                        width: 365,
                        frame: false,
                        border: false,
                        layout: {
                            type: 'table',
                            columns: 2
                        },
                        cls: 'version-info x-panel-transparent',
                        defaults: {
                            bodyStyle: 'padding: 5px',
                            border: false,
                            cls: 'x-panel-transparent'
                        },
                        items: [{
                            html: 'Course ID'
                        }, {
                            html: '' + (me.courseRecord.get('id') || me.unknown),
                            width: 150
                        }, {
                            html: 'Training Program ID:'
                        }, {
                            html: '' + (me.trainingProgram.id || me.unknown),
                            width: 150
                        }, {
                            html: 'Delivered Course Publish Date:'
                        }, {
                            items: [{
                                xtype: 'field',
                                value: me.courseRecord.get('deliveredArtisanCourseCreatedOn') ?
                                        Symphony.parseDate(me.courseRecord.get('deliveredArtisanCourseCreatedOn')).formatSymphony('m/d/Y g:i a T') :
                                        me.unknown,
                                readOnly: true,
                                help: {
                                    text: 'Publish date of the course that was delivered to the student.',
                                    title: 'Delivered Course Publish Date:'
                                },
                                width: 150
                            }]
                        }, {
                            html: 'Current Course Publish Date:'
                        }, {
                            items: [{
                                xtype: 'field',
                                value: me.courseRecord.get('currentArtisanCourseCreatedOn') ?
                                        Symphony.parseDate(me.courseRecord.get('currentArtisanCourseCreatedOn')).formatSymphony('m/d/Y g:i a T') :
                                        me.unknown,
                                readOnly: true,
                                help: {
                                    text: 'The most recent publish date for this course in Symphony.',
                                    title: 'Current Course Publish Date:'
                                },
                                width: 150
                            }]
                        }, {
                            html: 'Delivered Online Course Version:'
                        }, {
                            items: [{
                                xtype: 'field',
                                value: (me.courseRecord.get('deliveredOnlineCourseVersion') || me.unknown),
                                readOnly: true,
                                help: {
                                    text: 'Version of the course content that the student received.',
                                    title: 'Delivered Online Course Version:'
                                },
                                width: 150
                            }]
                        }, {
                            html: 'Current Online Course Version:'
                        }, {
                            items: [{
                                xtype: 'field',
                                value: (me.courseRecord.get('currentCourseVersion') || me.unknown),
                                readOnly: true,
                                help: {
                                    text: 'The current version of the course content in Symphony.',
                                    title: 'Current Online Course Version:'
                                },
                                width: 150
                            }]
                        }, {
                            html: 'Delivered Artisan Course ID:'
                        }, {
                            items: [{
                                xtype: 'field',
                                value: (me.courseRecord.get('deliveredArtisanCourseID') || me.unknown),
                                readOnly: true,
                                help: {
                                    text: 'The ID of the Artisan course the student received.',
                                    title: 'Delivered Artisan Course ID:'
                                },
                                width: 150
                            }]
                        }, {
                            html: 'Current Artisan Course ID:'
                        }, {
                            items: [{
                                xtype: 'field',
                                value: (me.courseRecord.get('currentArtisanCourseID') || me.unknown),
                                readOnly: true,
                                help: {
                                    text: 'The ID of the Artisan course that is currently in use in Symphony.',
                                    title: 'Current Artisan Course ID:'
                                },
                                width: 150
                            }]
                        }, {
                            html: 'Playback Symphony Version:'
                        }, {
                            items: [{
                                xtype: 'field',
                                value: (me.courseRecord.get('playbackCoreVersion') || me.unknown),
                                readOnly: true,
                                help: {
                                    text: 'Version of Symphony the student was using during their latest attempt at this course.',
                                    title: 'Playback Symphony Version:'
                                },
                                width: 150
                            }]
                        }, {
                            html: 'Delivered Symphony Version:'
                        }, {
                            items: [{
                                xtype: 'field',
                                value: (me.courseRecord.get('deliveredCoreVersion') || me.unknown),
                                readOnly: true,
                                help: {
                                    text: 'Version of Symphony that generated the course playback files.',
                                    title: 'Delivered Symphony Version:'
                                },
                                width: 150
                            }]
                        }, {
                            html: 'Latest Course Symphony Version:'
                        }, {
                            items: [{
                                xtype: 'field',
                                value: (me.courseRecord.get('currentCoreVersion') || me.unknown),
                                readOnly: true,
                                help: {
                                    text: 'The version of Symphony that generated the most recent version of this course.',
                                    title: 'Latest Course Symphony Version:'
                                },
                                width: 150
                            }]
                        }, {
                            html: 'Current Symphony Version:'
                        }, {
                            items: [{
                                xtype: 'field',
                                readOnly: true,
                                value: (me.courseRecord.get('symphonyCoreVersion') || me.unknown),
                                help: {
                                    text: 'The current version of Symphony.',
                                    title: 'Current Symphony Version:'
                                },
                                width: 150
                            }]
                        }]
                    }]
                }
            },
            '-',
            {
                text: 'View Progress',
                disabled: !me.courseRecord.get('onlineCourseRollupId'),
                ref: 'progressMenu',
                reload: true,
                handler: function () {
                    var progressViewerWindow = new Ext.Window({
                        title: 'Progress',
                        modal: true,
                        border: false,
                        width: 800,
                        height: 600,
                        stateId: 'progressviewerwindow',
                        stateful: true,
                        layout: 'fit',
                        buttons: [{
                            xtype: 'button',
                            text: 'Close',
                            iconCls: 'x-button-cancel',
                            handler: function () {
                                progressViewerWindow.close();
                            }
                        }],
                        listeners: {
                            show: function() {
                                progressViewerWindow.loadMask = new Ext.LoadMask(progressViewerWindow.getEl(), { msg: "Loading progress. Please wait..." });
                                progressViewerWindow.loadMask.show();
                            },
                            beforeshow: function () {
                                Symphony.Ajax.request({
                                    url: String.format('/services/courseassignment.svc/coursestatus/{0}/',
                                            me.courseRecord.get('onlineCourseRollupId')),
                                    method: 'GET',
                                    success: function (args) {

                                        var panelConfig = {
                                            padding: 15,
                                            border: false,
                                            frame: false,
                                            cls: 'x-panel-mc'
                                        }

                                        if (args.data.length) {
                                            var firstRecord = args.data[0],
                                                webPath = firstRecord.webPath,

                                                jqueryPath = webPath + '/js/jquery.js',
                                                artisanUtitliesPath = webPath + '/js/artisan.utilities.js',
                                                lzStringPath = webPath + '/js/lz-string.min.js',
                                                contentPath = webPath + '/content/artisan.coursejson.js',

                                                scriptPaths = [jqueryPath, lzStringPath, artisanUtitliesPath, contentPath];

                                            me.loadScript(scriptPaths, 0, function () {
                                                if (window.Artisan && firstRecord.dataChunk) {

                                                    var dataChunkJSON = firstRecord.dataChunk;

                                                    if (firstRecord.dataChunk.indexOf('gz:') == 0) {
                                                        dataChunkJSON = Artisan.Utilities.decompressJSON(
                                                            firstRecord.dataChunk.replace('gz:', '')
                                                        );
                                                    }

                                                    var dataChunk = JSON.parse(dataChunkJSON);
                                                    var course = Artisan.CourseJson;

                                                    var progressViewer = new Symphony.CourseAssignment.ArtisanProgressViewer({
                                                        course: course,
                                                        dataChunk: dataChunk,
                                                        bookmark: firstRecord.bookmark,
                                                        scormData: args.data,
                                                        listeners: {
                                                            save: function (data) {
                                                                var dataChunk = 'gz:' + Artisan.Utilities.compressJSON(JSON.stringify(data.dataChunk)),
                                                                    bookmark = data.bookmark,
                                                                    loadMask = new Ext.LoadMask(progressViewer, { msg: "Please wait..." });

                                                                loadMask.show();

                                                                Symphony.Ajax.request({
                                                                    url: String.format('/services/courseassignment.svc/coursestatus/{0}/',
                                                                        me.courseRecord.get('onlineCourseRollupId')),
                                                                    method: 'POST',
                                                                    jsonData: {
                                                                        dataChunk: dataChunk,
                                                                        bookmark: bookmark
                                                                    },
                                                                    complete: function (args) {
                                                                        loadMask.hide();
                                                                    },
                                                                    success: function (args) {
                                                                        var first = args.data[0],
                                                                            dcJson = first.dataChunk,
                                                                            bm = first.bookmark;
                                                                        
                                                                        if (dcJson.indexOf('gz:') == 0) {
                                                                            dcJson = Artisan.Utilities.decompressJSON(
                                                                                dcJson.replace('gz:', '')
                                                                            );
                                                                        }

                                                                        progressViewer.updateTree(bm, JSON.parse(dcJson));
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                    progressViewerWindow.loadMask.hide();

                                                    progressViewerWindow.add(progressViewer);
                                                    progressViewerWindow.doLayout();
                                                } else {
                                                    progressViewerWindow.loadMask.hide();
                                                    progressViewerWindow.add(new Ext.Panel(Ext.apply(panelConfig, {
                                                        html: 'Could not load course files.'
                                                    })));
                                                    progressViewerWindow.doLayout();
                                                }
                                            });

                                        } else {
                                            progressViewerWindow.loadMask.hide();
                                            progressViewerWindow.add(new Ext.Panel(Ext.apply(panelConfig, {
                                                html: 'No progress for this user.'
                                            })));
                                            progressViewerWindow.doLayout();
                                        }
                                    }
                                });
                            }
                        }
                    });
                    
                    progressViewerWindow.show();
                }
            },
            '-',
            {
                text: 'Proctor Code',
                itemId: 'proctorLabel',
                hidden: !(me.courseRecord.get('onlineCourseRollupId') && me.courseRecord.get('proctorRequiredIndicator')),
            },
            {
                text: 'Generate New Proctor Code',
                hidden: true, //!me.courseRecord.get('proctorRequiredIndicator'), // IN PROGRESS
                listeners: {
                    click: function () {
                        console.log("gen new proc key");
                        var tpid = me.trainingProgram.id;
                        var uid = me.userId;
                        var courseId = me.courseRecord.get('id');
                        var rollupId = me.courseRecord.get('onlineCourseRollupId');

                        Symphony.Ajax.request({
                            method: 'POST',
                            url: '/services/courseassignment.svc/proctorkey/generate/' + rollupId + '/' + tpid + '/' + courseId + '/' + uid,
                            success: function (data) {
                                var prLabel = me.down('#proctorLabel');
                                if (prLabel) {
                                    prLabel.setText('Proctor Key - ' + data.data.proctorKey);
                                }
                            }
                        });
                    }
                }
            },
            '-',
            {
                text: 'Launch as Student',
                ref: 'launchAsStudent',
                hidden: !Symphony.User.isSalesChannelAdmin,
                handler: function (e) {
                    Ext.Msg.show({
                        title:'Launch course as student?',
                        msg: 'Launching the course as the student will affect the student\'s progress and score in this course. Are you sure you want to launch this course as the student?',
                        buttons: Ext.Msg.YESNO,
                        fn: function(btn) {
                            if (btn == 'yes') {
                                var displayCourseLink = me.courseRecord.get('displayCourseLink');
                                var params = {};

                                for (var i = 0; i < displayCourseLink.queryParams.length; i++) {
                                    if (displayCourseLink.queryParams[i].key == "UserID") {
                                        displayCourseLink.queryParams[i].value = me.userId;
                                    }

                                    params[displayCourseLink.queryParams[i].key] = displayCourseLink.queryParams[i].value;
                                }
                    
                                displayCourseLink.fullUrl = displayCourseLink.fullUrl.replace('UserId|' + Symphony.User.id, 'UserId|' + me.userId);

                                Symphony.LinkHandlers.launch(me.grid, e, displayCourseLink, me.courseRecord, params, me.callback)
                            }
                        },
                        icon: Ext.MessageBox.WARNING
                    });
                }
            }]
        });

        this.callParent(arguments);
    },
    loadScript: function(scripts, index, complete) {
        var script = document.createElement('script'),
            me = this;

        var nextScript = function() {
            index++;
            if (index < scripts.length) {
                me.loadScript(scripts, index, complete);
            } else {
                if (typeof (complete) == 'function') {
                    complete();
                }
            }
        };

        var timedout = false,
            timeout = setTimeout(function () {
                timedout = true;
                nextScript();
            }, 1000);

        script.onload = function () {
            if (!timedout) {
                clearTimeout(timeout);
                nextScript();
            }
        }

        script.setAttribute("type", "text/javascript");
        script.setAttribute("src", scripts[index]);
        script.setAttribute("id", "auto-script-" + index);

        var head = document.getElementsByTagName("head")[0];

        var existingScript = document.getElementById("auto-script-" + index);

        if (existingScript) {
            head.removeChild(existingScript);
        }

        head.appendChild(script)
    },
    setAssignmentStatus: function(status, isBackground) {
        var me = this,
            urlTpl = '/services/assignment.svc/assignment/{0}/{1}/{2}/{3}/',
            responseStatus,
            url;

        me.fireEvent('beforeUpdate', 'assignment', 'Saving assignments. Please wait...');

        switch (status) {
            case Symphony.AssignmentResponseStatus.correct:
                responseStatus = 'correct';
                break;
            case Symphony.AssignmentResponseStatus.incorrect:
                responseStatus = 'incorrect';
                break;
            case Symphony.AssignmentResponseStatus.unmarked:
                responseStatus = 'unmarked';
                break;
        }

        url = String.format(urlTpl, responseStatus, me.trainingProgram.id, me.courseRecord.get('id'), me.userId);

        Ext.Ajax.request({
            method: 'POST',
            url: url,
            success: function (args) {
                me.fireEvent('afterUpdate', 'assignment');
                if (!isBackground) {
                    me.closeMenu();
                }
            }
        });
    },
    saveCourseRecord: function(notes, isBumpForward) {
        var me = this,
            score = me.courseRecord.get('score'),
            url = String.format('/services/courseassignment.svc/trainingprograms/{0}/onlinecourseregistrations/{1}/',
                            this.trainingProgram.id,
                            this.courseRecord.get('id')),
            data;

        me.fireEvent('beforeUpdate', 'record', 'Saving student record. Please wait...');

        if (score > 100) {
            score = 100;
        } else if (score < 0) {
            score = 0;
        } else if (!score) {
            score = 0;
        }

        if (!me.courseRecord.get('onlineCourseRollupId') && score < 1) {
            score = 1; // Trigger a score update to set the highscore date. This will set a valid highscore date for the training program
                       // this will prevent possibilities of duplicates since the rollup will load for the tp afterwards instead of being ignored
                       // due to a null highscore date.
        }

        notes = Symphony.User.id + " - " + new Date().toUTCString() + " - " + notes;

        data = {
            id: me.courseRecord.get('onlineCourseRollupId'),
            score: score,
            userId: me.userId,
            trainingProgramId: me.trainingProgram.id,
            courseId: me.courseRecord.get('id'),
            success: me.courseRecord.get('passed') ? true : false,
            completion: me.courseRecord.get('passed') ? true : false,
            canMoveForwardIndicator: me.courseRecord.get('canMoveForwardIndicator'),
            resetNotes: notes
        };

        if (me.courseRecord.get('updateTotalSeconds') === 0 || me.courseRecord.get('updateTotalSeconds') >= 1) {
            var seconds = me.courseRecord.get('updateTotalSeconds');
            data.totalSeconds = seconds;
            data.totalSecondsRounded = seconds;
            data.totalSecondsLastAttempt = seconds;
        }

        me.courseRecord.set('updateTotalSeconds', null);

        Ext.Ajax.request({
            method: 'post',
            url: url,
            jsonData: [data],
            success: function (args) {
                me.fireEvent('afterUpdate', 'record');

                if (isBumpForward) {
                    me.setAssignmentStatus(Symphony.AssignmentResponseStatus.correct)
                } else {
                    me.closeMenu();
                }
            }
        });
    },
    closeMenu: function () {
        //this.hide();

        if (typeof (this.callback) == 'function') {
            this.callback();
        }
    },
    getScore: function () {
        return this.changeScoreMenu.menu.find('name', 'score')[0].getValue();
    }
});


Symphony.CourseAssignment.ResetCourseDialogExtForAssignments = Ext.define('CourseAssignment.ResetCourseDialogExtForAssignments', {
    extend: 'Ext.Window',

    onlineCourseRollupId: 0,
    callback: null,
    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            title: 'Notes Required',
            action: 'deleteregistration',
            border: false,
            modal: true,
            height: 200,
            width: 400,
            items: [{
                xtype: 'panel',
                frame: true,
                border: false,
                layout: 'form',
                tbar: {
                    items: [{
                        xtype: 'button',
                        text: 'Save',
                        iconCls: 'x-button-save',
                        handler: function () {
                            var notes = me.find('name', 'deletereasonchange')[0].getValue();
                            var resetModules = me.resetmodules.getValue() ? true : false;

                            Symphony.Ajax.request({
                                url: '/services/courseassignment.svc/' + me.action + '/' + me.onlineCourseRollupId + '?notes=' + encodeURIComponent(notes) + '&ca=' + resetModules.toString(),
                                method: 'POST',
                                success: function (args) {
                                    if (typeof (me.callback) == 'function') {
                                        me.callback(args);
                                    }
                                    me.destroy();
                                }
                            });
                        }
                    }, {
                        xtype: 'button',
                        style: 'margin-left: 5px;',
                        text: 'Cancel',
                        iconCls: 'x-button-cancel',
                        handler: function () {
                            me.destroy();
                        }
                    }]
                },
                items: [{
                    name: 'deletereasonchange',
                    labelAlign: 'top',
                    xtype: 'textarea',
                    fieldLabel: 'Please explain the reason for this change',
                    anchor: '100%'
                }, {
                    xtype: 'checkbox',
                    name: 'resetmodules',
                    ref: '../resetmodules',
                    boxLabel: 'Reset assignments'
                }]
            }]
        })
        Symphony.CourseAssignment.ResetCourseDialogExtForAssignments.superclass.initComponent.apply(me, arguments);
    }
});
