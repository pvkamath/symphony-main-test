﻿Symphony.CourseAssignment.BundledTrainingProgramsGrid = Ext.define('courseassignment.bundledtrainingprogramsgrid', {
    alias: 'widget.courseassignment.bundledtrainingprogramsgrid',
    extend: 'symphony.localgrid',
    initComponent: function () {
        var me = this;

        var columns = [
            { itemId: 'deleteTrainingProgram', header: ' ', hideable: false, renderer: Symphony.deleteRenderer, width: 28 },
            {
                /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 100, align: 'left',
                flex: 1
            },
            { /*id: 'sku',*/ header: 'Sku', dataIndex: 'sku', width: 100, align: 'left' },
            {
                /*id: 'cost',*/ header: 'Cost', dataIndex: 'cost', width: 100, align: 'left', renderer: function (value) {
                    return '$' + value.toFixed(2)
                }},
            { /*id: 'startDate',*/ header: 'Start Date', dataIndex: 'startDate', width: 100, renderer: Symphony.pastDefaultDateRenderer },
            { /*id: 'endDate',*/ header: 'End Date', dataIndex: 'endDate', width: 100, renderer: Symphony.pastDefaultDateRenderer }
        ];

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: false,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: columns
        });

        Ext.apply(this, {
            tbar: {
                items: [' ', ' ', {
                    xtype: 'label',
                    text: 'Training Programs (drag/drop to re-order)'
                }, '->', {
                    text: 'Add Training Program',
                    iconCls: 'x-button-add',
                    handler: function () {

                        var sm = new Ext.selection.CheckboxModel();
                        
                        var win = new Ext.Window({
                            items: [{
                                xtype: 'courseassignment.trainingprogramsgrid',
                                deferLoad: false,
                                isHideAdd: true,
                                isHideDelete: true,
                                selModel: sm,
                                plugins: [{ ptype: 'pagingselectpersist' }],
                                tbarItems: [{
                                    xtype: 'button',
                                    iconCls: 'x-button-add',
                                    text: 'Add Training Programs',
                                    handler: function() {
                                        var grid = win.find('xtype', 'courseassignment.trainingprogramsgrid')[0];
                                        var items = grid.getPlugin('pagingSelectionPersistence').getPersistedSelection();
                                        var data = [];
                                        var existing = me.store.getRange();
                                        var existingMap = {};

                                        for (var j = 0; j < existing.length; j++) {
                                            existingMap[existing[j].id] = existing[j];
                                        }
                                        
                                        for (var i = 0; i < items.length; i++) {
                                            if (!existingMap.hasOwnProperty(items[i].data.id)) {
                                                data.push(items[i].data);
                                            }
                                        }
                                        
                                        me.store.loadData(data, true);

                                        win.close();
                                    }
                                }]
                            }],
                            layout: 'fit',
                            modal: true,
                            resizable: false,
                            title: 'Select a Training Program',
                            height: 400,
                            width: 400,
                            listeners: {
                                render: function (w) {
                                    w.find('xtype', 'courseassignment.trainingprogramsgrid')[0].refresh();
                                }
                            }
                        }).show();
                    }
                }]
            },
            listeners: {
                cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                    var record = grid.getStore().getAt(rowIndex);
                    
                    if (grid.getColumnModel().getColumnAt(columnIndex).itemId === 'deleteTrainingProgram') {
                        Ext.Msg.confirm('Are you sure?', 'Are you sure you want to remove this training program?', function (btn) {
                            if (btn == 'yes') {
                                grid.getStore().remove(record);
                            }
                        });
                    }
                },
                afterrender: {
                    single: true,
                    fn: function (grid) {
                        // allow sorting
                        this.requiredDT = new Ext.dd.DropTarget(grid.getView().getEl(), {
                            ddGroup: 'trainingProgramBundleOrder',
                            notifyDrop: function (dd, e, data) {
                                var sm = grid.getSelectionModel();
                                var rows = sm.getSelections();
                                var cindex = dd.getDragData(e).rowIndex;
                                if (sm.hasSelection()) {
                                    for (i = 0; i < rows.length; i++) {
                                        grid.store.remove(grid.store.getById(rows[i].id));
                                        grid.store.insert(cindex, rows[i]);
                                    }
                                    sm.selectRecords(rows);
                                }
                                grid.getView().refresh();
                            }
                        });
                    }
                }
            },
            ddGroup: 'trainingProgramBundleOrder',
            enableDragDrop: true,
            height: 350,
            idProperty: 'id',
            colModel: colModel,
            model: 'trainingProgram',
            viewConfig: {
                plugins: 'gridviewdragdrop'
            }
        });
        this.callParent(arguments);
    },
    getData: function() {
        var results = [];
        var me = this;

        this.store.each(function (record) {
            var item = record.data;

            results.push(item);
        });

        return results;
    }
});
