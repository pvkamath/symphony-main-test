﻿Symphony.CourseAssignment.LeadersGrid = Ext.define('courseassignment.leadersgrid', {
    alias: 'widget.courseassignment.leadersgrid',
    extend: 'symphony.localgrid',
    allowDelete: true,
    allowAdd: true,
    showVideoChatColumn: false,
    initComponent: function () {
        var me = this;

        var columns = [
            { header: ' ', dataIndex: 'userId', renderer: Symphony.Classroom.messageClassInstructorRenderer, width: 28 },
            {
                header: 'Name', dataIndex: 'fullName', align: 'left',
                flex: 1
            },
            {
                text: 'Primary Leader',
                dataIndex: 'isPrimary',
                width: 95,
                xtype: 'checkcolumn',
                renderer: function(value) {
                    return String.format('<input type="checkbox" {0}>', value ? 'checked="true"' : '');
                },
                listeners: {
                    checkchange: function(chk, rowIndex, checked, eOpts ) {
                        if (checked) {
                            me.store.each(function (rec, i) {
                                if (rowIndex != i) {
                                    rec.set('isPrimary', false);
                                    rec.commit();
                                }
                            });
                        }
                        current = me.store.getAt(rowIndex);
                        current.set('isPrimary', checked);
                        current.commit();
                    }
                }
            }
        ];

        if (this.showVideoChatColumn && Symphony.Portal.hasPermission(Symphony.Modules.VideoChat)) {
            columns = [{ header: ' ', dataIndex: 'userId', renderer: Symphony.Classroom.videoChatRenderer, width: 28 }].concat(columns);
        }

        if (this.allowDelete) {
            columns = [{ itemId: 'deleteLeader', header: ' ', hideable: false, renderer: Symphony.deleteRenderer, width: 28 }].concat(columns);
        }

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: columns
        });

        Ext.apply(this, {
            tbar: {
                items: [' ', ' ', {
                    xtype: 'label',
                    text: 'Leaders'
                }, '->', {
                    text: 'Add Leader',
                    iconCls: 'x-button-add',
                    disabled: this.disabled,
                    hidden: !me.allowAdd,
                    handler: function () {
                        var win = new Ext.Window({
                            items: [{
                                stateId: 'classroom.classinstructorselectgrid',
                                xtype: 'classroom.classinstructorselectgrid',
                                border: false,
                                filter: me.getDataAsInstructors(),
                                listeners: {
                                    select: function (grid, record) {
                                        // Convert the instructor to a TP leader
                                        var tpLeader = {
                                            userId: record.get('id'),
                                            firstName: record.get('firstName'),
                                            lastName: record.get('lastName'),
                                            middleName: record.get('middleName'),
                                            fullName: record.get('fullName'),
                                            isPrimary: false
                                        }

                                        me.store.loadData([tpLeader], true);
                                        win.destroy();
                                    }
                                }
                            }],
                            layout: 'fit',
                            modal: true,
                            resizable: false,
                            title: 'Select a Leader',
                            height: 400,
                            width: 400
                        }).show();
                    }
                }]
            },
            listeners: {
                cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                    var record = grid.getStore().getAt(rowIndex);
                    
                    if (grid.getColumnModel().getColumnAt(columnIndex).itemId === 'deleteLeader') {
                        Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this leader?', function (btn) {
                            if (btn == 'yes') {
                                grid.getStore().remove(record);
                            }
                        });
                    }
                }
            },
            height: 150,
            idProperty: 'userId',
            colModel: colModel,
            model: 'trainingProgramLeader'
        });
        this.callParent(arguments);
    },
    getData: function() {
        var results = [];
        var me = this;

        this.store.each(function (record) {
            var item = record.data;

            if (!item.id) {
                delete item.id;
            }

            // Server gets cranky about these fields as they will be empty
            delete item.trainingProgramId;
            delete item.gtmOrganizerStatus;

            results.push(item);
        });
        return results;
    },
    // Need data as class instructors to work with the popup instructor selector
    // This is only for filtering out the list of already assigned leaders
    getDataAsInstructors: function () {
        var results = [];
        var me = this;
        
        this.store.each(function (record) {
            var item = {
                id: 0,
                instructorId: record.data.userId,
                fullName: record.data.fullName,
                classId: 0 
            };
            results.push(item);
        });
        return results;
    },
    disable: function () {
        Symphony.CourseAssignment.LeadersGrid.superclass.disable.call(this);
        this.getTopToolbar().setDisabled(true);
    }
});
