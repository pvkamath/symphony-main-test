﻿Ext.define('CourseAssignment.AccreditationListItem', {
    extend: 'Ext.Container',
    xtype: 'courseassignment.accreditationlistitem',
    requires: [
        'OnlineCourseAccreditation',
        'AccreditationStatus'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    margin: {
        bottom: 8
    },

    items: [{
        xtype: 'displayfield',
        itemId: 'board-name-field',
        cls: 'x-form-display-field-bold'
    }, {
        xtype: 'displayfield',
        itemId: 'profession-field',
        fieldLabel: 'Profession',
        hidden: true
    }, {
        xtype: 'displayfield',
        fieldLabel: 'Credit Hours',
        itemId: 'credit-hours-field'
    }, {
        xtype: 'displayfield',
        fieldLabel: 'Accredited On',
        itemId: 'accreditation-date-field'
    }, {
        xtype: 'symphony.spellcheckarea',
        itemId: 'disclaimer-field',
        fieldLabel: 'Disclaimer',
        cls: 'x-html-editor-full-border',
        readOnly: true
    }],

    initComponent: function() {
        var me = this,
            boardNameField,
            disclaimerField,
            creditHoursField,
            accreditationDateField,
            professionField;

        if (!(me.accreditation instanceof OnlineCourseAccreditation)) {
            me.accreditation = new OnlineCourseAccreditation(me.accreditation);
        }

        me.callParent();

        boardNameField = me.queryById('board-name-field');
        disclaimerField = me.queryById('disclaimer-field');
        creditHoursField = me.queryById('credit-hours-field');
        accreditationDateField = me.queryById('accreditation-date-field');
        professionField = me.queryById('profession-field');

        disclaimerField.toolbar.hide();
        setTimeout(function() {
            // stutter set or it won't assign values properly
            disclaimerField.setValue(me.accreditation.get('disclaimer'));
        }, 100);

        boardNameField.setValue('Accredited by the ' + me.accreditation.get('accreditationBoardName'));
        creditHoursField.setValue(Ext.Number.toFixed(me.accreditation.get('creditHours'), 1));
        accreditationDateField.setValue(Ext.Date.format(me.accreditation.get('startDate'), 'M d/Y'));

        if (me.accreditation.get('professionId')) {
            professionField.show();
            professionField.setValue(me.accreditation.get('professionName'));
        }
    }

}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});