﻿Symphony.CourseAssignment.getSessionAssignmentWindow = function (params) {
    params.xtype = 'courseassignment.sessionassignmentpanel';
    params.region = 'center';

     return new Ext.Window({
         title: 'Student Session Registrations',
         width: 550,
         height: 580,
         layout: 'fit',
         border: true,
         frame: true,
         modal: true,
         items: [params]
     });
}
Symphony.CourseAssignment.SessionAssignmentPanel = Ext.define('courseassignment.sessionassignmentpanel', {

    alias: 'widget.courseassignment.sessionassignmentpanel',

    extend: 'Ext.Panel',
    sessionCourseId: 0,
    trainingProgramId: 0,
    trainingPrograms: [],
    userIds: [],
    loaded: false,
    sessionVisibiliityStore: new Ext.data.ArrayStore({
        id: 0,
        model: 'simple',
        data: [
            [null, 'All'],
            [false, 'Unregistered Sessions'],
            [true, 'Registered Sessions']
        ]
    }),
    dateRangeVisibilityStore: new Ext.data.ArrayStore({
        id: 0,
        model: 'simple',
        data: [
            [null, 'All'],
            [false, 'Show Outside Date Range Only'],
            [true, 'Show Inside Date Range Only']
        ]
    }),
    registrationStatusStore: new Ext.data.ArrayStore({
        id: 0,
        model: 'simple',
        data: [
            [0, 'All'],
            [Symphony.RegistrationStatusType.awaitingApproval, 'Awaiting Approval'],
            [Symphony.RegistrationStatusType.waitList, 'Wait List'],
            [Symphony.RegistrationStatusType.denied, 'Denied'],
            [Symphony.RegistrationStatusType.registered, 'Registered']
        ]
    }),
    trainingProgramStore: new Ext.data.ArrayStore({
        id: 0,
        model: 'simple'
    }),
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            layout: 'border',
            border: false,
            items: [{
                xtype: 'form',
                region: 'north',
                height: me.userIds.length ? 170 : 155,
                border: false,
                frame: false,
                cls: 'x-panel-transparent',
                style: 'padding: 5px',
                labelWidth: 120,
                listeners: {
                    afterlayout: function () {
                        if (me.userIds.length && !me.loaded) {
                            var loadMask = new Ext.LoadMask(me.getEl(), { msg: "Loading..." });
                            loadMask.show();

                            Symphony.Ajax.request({
                                url: String.format('/services/courseassignment.svc/sessions/trainingprograms/?userIds={0}',
                                   me.userIds.join(',')),
                                method: 'get',
                                success: function (args) {
                                    loadMask.hide();
                                    me.trainingPrograms = args.data;

                                    var comboData = [[0,'All']];

                                    var trainingProgramIds = [];
                                    for (var i = 0; i < args.data.length; i++) {
                                        trainingProgramIds.push(args.data[i].id);
                                        comboData.push([args.data[i].id, args.data[i].name]);
                                    }

                                    me.trainingProgramCombo.getStore().loadData(comboData);
                                    me.trainingProgramCombo.setValue(0);

                                    me.sessionAssignmentGrid.setTrainingProgramIds(trainingProgramIds);

                                    me.loaded = true;
                                }
                            });
                        }
                    }
                },
                items: [{
                    xtype: 'fieldset',
                    title: 'Filter Users by Session',
                    
                    items: [{
                        xtype: 'courseassignment.sessiondatefield',
                        fieldLabel: 'Date',
                        anchor: '100%',
                        listeners: {
                            select: function (field, value) {
                                me.sessionAssignmentGrid.setDateRange(value);
                            }
                        }
                    }, Symphony.getCombo('dateRangeVisibility', 'Date Range Visibility', me.dateRangeVisibilityStore, {
                        valueField: 'id',
                        displayField: 'text',
                        value: null,
                        listeners: {
                            select: function (combo, records, index) {
                                me.sessionAssignmentGrid.setDateRangeVisibility(records[0].get('id'));
                            }
                        }
                    }), Symphony.getCombo('sessionVisibility', 'Session Visibility', me.sessionVisibiliityStore, {
                        valueField: 'id',
                        displayField: 'text',
                        value: null,
                        listeners: {
                            select: function (combo, records, index) {
                                me.sessionAssignmentGrid.setSessionVisibility(records[0].get('id'));
                            }
                        }
                    }), Symphony.getCombo('registrationStatus', 'Registration Status', me.registrationStatusStore, {
                        valueField: 'id',
                        displayField: 'text',
                        value: 0,
                        listeners: {
                            select: function (combo, records, index) {
                                me.sessionAssignmentGrid.setRegistrationStatus(records[0].get('id'));
                            }
                        }
                    }), Symphony.getCombo('trainingProgramId', 'Training Program', me.trainingProgramStore, {
                        valueField: 'id',
                        displayField: 'name',
                        hidden: me.userIds.length == 0,
                        value: 0,
                        ref: '../../trainingProgramCombo',
                        listeners: {
                            select: function (combo, record, index) {
                                var trainingProgramIds = [];

                                if (record.get('id') == 0) {
                                    for (var i = 0; i < me.trainingPrograms.length; i++) {
                                        trainingProgramIds.push(me.trainingPrograms[i].id);
                                    }
                                } else {
                                    trainingProgramIds.push(record.get('id'));
                                }

                                me.sessionAssignmentGrid.setTrainingProgramIds(trainingProgramIds);
                            }
                        }
                    })]
                }]
            }, {
                xtype: 'courseassignment.sessionassignmentgrid',
                ref: 'sessionAssignmentGrid',
                sessionCourseId: me.sessionCourseId,
                trainingProgramIds: me.trainingProgramId > 0 ? [me.trainingProgramId] : [],
                userIds: me.userIds,
                region: 'center',
                border: false,
                listeners: {
                    loaded: function () {
                        me.loaded = true;
                    }
                }
            }]
        });
        this.callParent(arguments);
    }
});
