﻿Symphony.CourseAssignment.SelectableCoursesGrid = Ext.define('courseassignment.selectablecoursesgrid', {
    alias: 'widget.courseassignment.selectablecoursesgrid',
    extend: 'courseassignment.coursesgrid',
    initComponent: function () {
        var me = this;
        
        this.selModel = new Ext.selection.CheckboxModel();

        this.plugins = [{ ptype: 'pagingselectpersist' }];
        this.tbar = {};

        this.callParent(arguments);
    },
    getSelectedIds: function () {
        var selected = this.getSelectedCourses();
        var ids = [];

        for (var i = 0; i < selected.length; i++) {
            ids.push(selected[i].get('id'));
        }

        return ids;
    },
    getSelectedCourses: function () {
        return this.getPlugin('pagingSelectionPersistence').getPersistedSelection();
    }
});
