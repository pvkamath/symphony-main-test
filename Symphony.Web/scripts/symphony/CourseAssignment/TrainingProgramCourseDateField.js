﻿Symphony.CourseAssignment.TrainingProgramCourseDateField = Ext.define('courseassignment.trainingprogramcoursedatefield', {
    alias: 'widget.courseassignment.trainingprogramcoursedatefield',
    extend: 'Ext.ux.form.DateRangeField',
    onTriggerClick: function (e) {
        var me = this;
        
        if (this.disabled) {
            return;
        }

        if (this.menu == null) {

            var presetRangeHandler = function () {
                me.setDateRange(this.start, this.end, this.text);
            };

            this.menu = new Ext.menu.Menu({
                hideOnClick: false,
                focusOnSelect: false,
                width: 200,
                items: [
                    {
                        text: 'On Specific Date',
                        menu: {
                            listeners: {
                                show: function (m) {
                                    me.onShowMenu(m);
                                }
                            },
                            width: 260,
                            items: [{
                                xtype: 'panel',
                                cls: 'x-panel-transparent',
                                frame: true,
                                items: [{
                                    xtype: 'panel',
                                    border: false,
                                    layout: 'hbox',
                                    width: 235,
                                    cls: 'x-panel-transparent',
                                    items: [{
                                        xtype: 'panel',
                                        border: false,
                                        cls: 'x-panel-transparent',
                                        items: [{
                                            html: me.calendarTitle,
                                            border: false,
                                            cls: 'daterange-label x-panel-transparent'
                                        }, {
                                            xtype: 'datepicker',
                                            showToday: this.showToday,
                                            cls: 'static-datepicker',
                                            ref: '../../datePicker',
                                            listeners: {
                                                select: function (picker, date) {
                                                    me.selectedDate = date;
                                                }
                                            }
                                        }]
                                    }]
                                }, {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    width: 195,
                                    border: false,
                                    cls: 'x-panel-transparent',
                                    items: [{
                                        xtype: 'button',
                                        text: 'OK',
                                        width: 40,
                                        cls: 'daterange-button',
                                        style: 'margin-right: 3px',
                                        handler: function (btn) {
                                            var date = me.selectedDate;

                                            if (date && date.getFullYear() != Symphony.defaultYear) {

                                                var recordDate = date.add(Date.MINUTE, -date.getTimezoneOffset()); // convert local to utc for DB

                                                me.record.set(me.recordDateField, recordDate.formatSymphony('microsoft'));
                                                me.record.set(me.recordHoursField, null);
                                                me.record.set(me.recordModeField, null);

                                                me.record.commit();

                                                var displayText = date.format('M j, Y');
                                                me.setDateRange(date.format('j/n/Y'), date.format('j/n/Y'), displayText);
                                            }
                                            me.menu.hide();
                                        }
                                    }]
                                }]
                            }]
                        }
                    },
                    {
                        text: 'After Specified Time',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram),
                        menu: {
                            listeners: {
                                show: function (m) {
                                    me.onShowMenu(m);
                                }
                            },
                            items: [{
                                xtype: 'panel',
                                cls: 'x-panel-transparent',
                                frame: true,
                                items: [{
                                    xtype: 'panel',
                                    layout: 'column',
                                    padding: '5px',
                                    cls: 'inline-form x-panel-transparent',
                                    border: false,
                                    items: [{
                                        xtype: 'label',
                                        text: me.beforeHoursTextBox,
                                        cls: 'x-form-item',
                                        labelStyle: 'padding-top: 5px'
                                    }, {
                                        name: 'duration',
                                        fieldLabel: '',
                                        labelWidth: 0,
                                        ref: '../../duration',
                                        allowDays: true,
                                        xtype: 'classroom.durationfield',
                                        style: 'z-index: 99999',
                                        cls: 'hour-minute-selector',
                                        width: 120
                                    },
                                    Symphony.getCombo('mode', '', me.afterHoursModes, {
                                        valueField: 'id',
                                        displayField: 'text',
                                        ref: '../../mode',
                                        style: 'z-index: 99999',
                                        cls: 'x-timereference',
                                        listClass: 'x-menu x-timereference-list',
                                        queryMode: 'local',
                                        triggerAction: 'all',
                                        hideLabel: true,
                                        value: me.afterHoursModes.getById(Symphony.RelativeTimeMode.course) ? Symphony.RelativeTimeMode.course : Symphony.RelativeTimeMode.trainingProgram
                                    })]
                                }, {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    border: false,
                                    cls: 'x-panel-transparent',
                                    items: [{
                                        xtype: 'button',
                                        text: 'OK',
                                        width: 40,
                                        cls: 'daterange-button',
                                        style: 'margin-right: -5px',
                                        handler: function (btn) {
                                            var durationField = btn.ownerCt.ownerCt.ownerCt.duration,
                                                durationValue = durationField.getValue() > 0 ? durationField.getValue() : null,
                                                modeField = btn.ownerCt.ownerCt.ownerCt.mode,
                                                modeValue = modeField.getValue() > 0 ? modeField.getValue() : null,
                                                modeText = modeField.getStore().getById(modeValue).get('text');

                                            

                                            if (durationValue > 0) {
                                                me.record.set(me.recordDateField, null);
                                                me.record.set(me.recordHoursField, durationValue);
                                                me.record.set(me.recordModeField, modeValue);

                                                var displayText = Symphony.Classroom.durationRendererAllowDays(durationValue) + ' ' + modeText;
                                                me.setDisplayValue(displayText);
                                            } else {
                                                me.record.set(me.recordHoursField, null);
                                                me.record.set(me.recordModeField, null);
                                                if (!me.record.get(me.recordDateField)) {
                                                    me.setDisplayValue('');
                                                }
                                            } 

                                            
                                            me.menu.hide();
                                        }
                                    }]
                                }]
                            }]
                        }
                    }, '-', {
                        text: 'Clear',
                        handler: function () {
                            me.record.set(me.recordHoursField, null);
                            me.record.set(me.recordDateField, null);
                            me.record.set(me.modeField, null);
                            me.setDisplayValue('');

                            me.menu.hide();
                        }
                    }
                ]
            });
        }


        this.onFocus();

        this.menu.show(this.el, "tr-br?");

        this.menuEvents('on');

    }
});
