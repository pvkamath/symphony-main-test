﻿(function () {
    Symphony.CourseAssignment.CourseOverrideFields = Ext.define('courseassignment.courseoverridefields', {
        alias: 'widget.courseassignment.courseoverridefields',
        extend: 'Ext.form.FieldSet',
        title: 'Artisan Course Parameters',
        defaults: {
            anchor: '100%',
            labelWidth: 150
        },
        initComponent: function () {
            var me = this;
            Ext.apply(this, {

                items: [
                    Symphony.getCombo('showPretestOverride', 'Show Pretest', Symphony.getYesNoInheritStore()),
                //Symphony.getCombo('showPosttestOverride', 'Show Posttest', Symphony.getYesNoInheritStore()),
                //Symphony.getCombo('showObjectivesOverride', 'Show Objectives', Symphony.getYesNoInheritStore()),
                    Symphony.getCombo('pretestTestOutOverride', 'Test Out', Symphony.getYesNoInheritStore()),
                    {
                        name: 'passingScoreOverride',
                        fieldLabel: 'Passing Score',
                        xtype: 'numberfield',
                        minValue: 0,
                        maxValue: 100
                    },
                    Symphony.getCombo('navigationMethodOverride', 'Navigation', Symphony.getArtisanNavigationMethodStore()),
                    Symphony.getCombo('certificateEnabledOverride', 'Certificate Enabled', Symphony.getYesNoInheritStore(), {
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.Certificates)             
                    }),

                /*{
                name: 'disclaimerOverride',
                fieldLabel: 'Disclaimer',
                xtype: 'textfield'
                }, {
                name: 'contactEmailOverride',
                fieldLabel: 'Contact Email',
                xtype: 'textfield'
                },*/
                    Symphony.getCombo('pretestTypeOverride', 'Pretest Type', Symphony.getArtisanTestTypeStore()),
                    Symphony.getCombo('posttestTypeOverride', 'Posttest Type', Symphony.getArtisanTestTypeStore()),
                    Symphony.getCombo('randomizeQuestionsOverride', 'Randomize Questions', Symphony.getYesNoInheritStore()),
                    Symphony.getCombo('randomizeAnswersOverride', 'Randomize Answers', Symphony.getYesNoInheritStore()),
                    Symphony.getCombo('reviewMethodOverride', 'Review', Symphony.getArtisanReviewMethodStore()),
                    Symphony.getCombo('markingMethodOverride', 'Marking', Symphony.getArtisanMarkingMethodStore()),
                    Symphony.getCombo('inactivityMethodOverride', 'Inactivity', Symphony.getArtisanInactivityMethodStore()),
                    {
                        name: 'inactivityTimeoutOverride',
                        fieldLabel: 'Inactivity Timeout',
                        xtype: 'numberfield',
                        minValue: 0
                    },
                    Symphony.getCombo('isForceLogoutOverride', 'Force Logout', Symphony.getYesNoInheritStore()),
                    , {
                        name: 'minimumPageTimeOverride',
                        fieldLabel: 'Min. Page Time',
                        xtype: 'numberfield',
                        minValue: 0
                    }, {
                        name: 'maximumQuestionTimeOverride',
                        fieldLabel: 'Max. Question Time',
                        xtype: 'numberfield',
                        minValue: 0
                    }, {
                        name: 'maxTimeOverride',
                        xtype: 'numberfield',
                        fieldLabel: 'Max Test Time',
                        minValue: 0
                    }, {
                        name: 'minTimeOverride',
                        xtype: 'numberfield',
                        fieldLabel: 'Min Time',
                        minValue: 0
                    }, {
                        name: 'minLoTimeOverride',
                        xtype: 'numberfield',
                        fieldLabel: 'Min Learning Object Time',
                        minValue: 0
                    }, {
                        name: 'minScoTimeOverride',
                        xtype: 'numberfield',
                        fieldLabel: 'Min SCO Time',
                        minValue: 0
                    }, {
                        fieldLabel: 'Theme',
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/artisan.svc/themes/',
                        name: 'skinOverride',
                        bindingName: 'theme.name',
                        model: 'artisanTheme',
                        valueField: 'id',
                        displayField: 'name',
                        allowBlank: true,
                        emptyText: 'Select a Theme',
                        valueNotFoundText: 'Select a Theme',
                        ref: 'skinCombo',
                        listeners: {
                            select: function (combo, records, eOpts) {
                                me.child('*[ref=flavorCombo]').setVisible(records[0].data.isDynamic)
                            }
                        }
                    }, {
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/themes/', // Not really themes, but groups of settings for a dynamic theme.... A flavor!
                        name: 'themeFlavorOverrideId',
                        fieldLabel: 'Theme Flavor',
                        model: 'Theme',
                        valueField: 'id',
                        displayField: 'name',
                        allowBlank: true,
                        emptyText: 'Select a Flavor',
                        valueNotFoundText: 'Select a Flavor',
                        bindingName: 'themeFlavor.name',
                        ref: 'flavorCombo',
                        hidden: true
                    }, {
                        name: 'testAttempts',
                        fieldLabel: 'Max Test Attempts',
                        xtype: 'numberfield',
                        decimalPrecision: 0,
                        minValue: 1,
                        allowBlank: true
                    }
                ]
            });

            this.callParent(arguments);

            this.child('*[ref=skinCombo]').store.on('load', function () {
                this.insert(0, Ext.create('artisanTheme', {
                    name: '- None -',
                    id: null,
                    description: '',
                    cssPath: '',
                    skinCssPath: ''
                }));
            });

            this.child('*[ref=flavorCombo]').store.on('load', function () {
                this.insert(0, Ext.create('Theme', {
                    name: '- None -',
                    id: null
                }));
            });

        }
    });



    Symphony.CourseAssignment.cleanCourseOverrideValues = function (course) {
        // ugh, poor naming, map to the "retries" field
        if (!course.testAttempts) {
            course.retries = null;
        } else {
            course.retries = (course.testAttempts - 1);
        }
        if (!course.skinOverride) {
            course.skinOverride = null;
        }
        if (!course.posttestTypeOverride) {
            course.posttestTypeOverride = null;
        }
        if (!course.pretestTypeOverride) {
            course.pretestTypeOverride = null;
        }
        if (!course.navigationMethodOverride) {
            course.navigationMethodOverride = null;
        }
        if (!course.passingScoreOverride) {
            course.passingScoreOverride = null;
        }
        if (!course.disclaimerOverride) {
            course.disclaimerOverride = null;
        }
        if (!course.contactEmailOverride) {
            course.contactEmailOverride = null;
        }
        if (!course.retestMode) {
            course.retestMode = 0;
        }
        if (!course.reviewMethodOverride) {
            course.reviewMethodOverride = null;
        }
        if (!course.markingMethodOverride) {
            course.markingMethodOverride = null;
        }
        if (!course.inactivityMethodOverride) {
            course.inactivityMethodOverride = null;
        }
        if (!course.minimumPageTimeOverride && course.minimumPageTimeOverride !== 0) {
            course.minimumPageTimeOverride = null;
        }
        if (!course.maximumQuestionTimeOverride && course.maximumQuestionTimeOverride !== 0) {
            course.maximumQuestionTimeOverride = null;
        }
        if (!course.maxTimeOverride && course.maxTimeOverride !== 0) {
            course.maxTimeOverride = null;
        }

        if (!course.minTimeOverride && course.minTimeOverride !== 0) {
            course.minTimeOverride = null;
        }
        if (!course.minLoTimeOverride && course.minLoTimeOverride !== 0) {
            course.minLoTimeOverride = null;
        }
        if (!course.minScoTimeOverride && course.minScoTimeOverride !== 0) {
            course.minScoTimeOverride = null;
        }

        if (!course.inactivityTimeoutOverride && course.inactivityTimeoutOverride !== 0) {
            course.inactivityTimeoutOverride = null;
        }

        if (!course.themeFlavorOverrideId && course.themeFlavorOverrideId !== 0) {
            course.themeFlavorOverrideId = null;
        }

        return course;
    };
})();