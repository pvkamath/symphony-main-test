﻿Symphony.CourseAssignment.ResetCourseDialog = Ext.define('CourseAssignment.ResetCourseDialog', {
    extend: 'Object',

    action: null,
    onlineCourseRollupId: 0,
    callback: null,
    constructor: function (config) {
        Ext.apply(this, config);
        this.initComponent();
    },
    initComponent: function () {
        Ext.apply(this, {
            title: 'Notes Required',
            msg: 'Please explain the reason for this change:',
            btnClick: function (btn, notes) {
                var me = this;

                if (btn == 'ok' && notes.trim().length > 0) {
                    Symphony.Ajax.request({
                        url: '/services/courseassignment.svc/' + me.action + '/' + me.onlineCourseRollupId + '?notes=' + encodeURIComponent(notes) + '&ca=true',
                        method: 'POST',
                        success: function (args) {
                            if (typeof (me.callback) == 'function') {
                                me.callback(args);
                            }
                        }
                    });
                } else if (btn == 'ok') {
                    Ext.Msg.hide();
                    Ext.Msg.alert('Notes are required', 'You must enter notes before clearing or resetting a user.');
                }
            },
            multiline: true
        });
    },
    show: function () {
        Ext.Msg.prompt(this.title, this.msg, this.btnClick, this, this.multiline);
    }
});

