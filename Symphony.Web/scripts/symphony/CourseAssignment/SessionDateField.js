﻿Symphony.CourseAssignment.SessionDateField = Ext.define('courseassignment.sessiondatefield', {
    alias: 'widget.courseassignment.sessiondatefield',
    extend: 'Ext.ux.form.DateRangeField',
    initComponent: function () {
        var me = this;

        var defaultMenu = me.getDefaultMenuElements()

        Ext.apply(this, {
            menu: new Ext.menu.Menu({
                items: [
                    defaultMenu.day,
                    defaultMenu.week,
                    defaultMenu.month,
                    defaultMenu.year,
                    '-',
                    defaultMenu.dateRange,
                    '-',
                    {
                        text: 'Session Status',
                        menu: {
                            defaults: {
                                handler: function () {
                                    me.setDateRange(this.start, this.end, this.text);
                                }
                            },
                            items: [{
                                text: 'Past Sessions',
                                start: 'past',
                                end: 'past'
                            },
                            {
                                text: 'Active Sessions',
                                start: 'active',
                                end: 'active'
                            },
                            {
                                text: 'Future Sessions',
                                start: 'future',
                                end: 'future'
                            }]
                        }
                    },
                    '-',
                    {
                        text: 'Clear',
                        handler: function () {
                            me.setDateRange('none', 'none', '');
                            me.setDisplayValue('');
                        }
                    }
                ]
            })
        });
        this.callParent(arguments);
    }
});
