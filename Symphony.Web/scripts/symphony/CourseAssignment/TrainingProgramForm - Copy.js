﻿Symphony.CourseAssignment.TrainingProgramForm = Ext.define('courseassignment.trainingprogramform', {
    alias: 'widget.courseassignment.trainingprogramform',
    extend: 'Ext.Panel',
    trainingProgramId: 0,
    initComponent: function () {
        var me = this;

        var categoriesUrl = '/services/courseassignment.svc/categories/';

        Ext.apply(this, {
            layout: 'border',
            defaults: {
                border: false
            },
            items: [{
                region: 'north',
                height: 30,
                items: [{
                    xtype: 'symphony.savecancelbar',
                    items: [{
                        text: 'Duplicate',
                        iconCls: 'x-button-duplicate',
                        handler: function () {
                            return me.save(true);
                        }
                    }],
                    listeners: {
                        save: function () {
                            return me.save(false);
                        },
                        cancel: function () { this.ownerCt.remove(this); } .createDelegate(this)
                    }
                }]
            }, {
                region: 'center',
                layout: 'fit',
                defaults: {
                    border: false
                },
                items: [{
                    xtype: 'tabpanel',
                    border: false,
                    bodyStyle: 'padding: 15px',
                    activeTab: 0,
                    items: [{
                        title: 'General',
                        name: 'general',
                        xtype: 'form',
                        border: false,
                        frame: true,
                        //autoScroll: true,
                        //bodyStyle: 'overflow-x:hidden',
                        bodyStyle: 'overflow-y:auto;padding-right:10px',
                        items: [{
                            xtype: 'fieldset',
                            title: 'Basic Information',
                            items: [{
                                layout: 'column',
                                // column defaults
                                defaults: {
                                    columnWidth: 0.5,
                                    border: false,
                                    xtype: 'panel',
                                    layout: 'form'
                                },
                                items: [{
                                    //left column
                                    defaults: { anchor: '100%', xtype: 'textfield' },
                                    items: [{
                                        name: 'isLive',
                                        fieldLabel: 'Enabled',
                                        xtype: 'checkbox',
                                        listeners: {
                                            check: function (box, checked) {
                                                var startDate = me.find('name', 'startDate')[0];
                                                var endDate = me.find('name', 'endDate')[0];
                                                if (checked) {
                                                    startDate.allowBlank = false;
                                                    endDate.allowBlank = false;
                                                } else {
                                                    startDate.allowBlank = true;
                                                    endDate.allowBlank = true;
                                                }
                                            }
                                        }
                                    }, {
                                        name: 'name',
                                        fieldLabel: 'Name',
                                        allowBlank: false
                                    }, {
                                        name: 'internalCode',
                                        fieldLabel: 'Internal Code'
                                    }, {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        fieldLabel: 'Category',
                                        name: 'categoryCompositeField',
                                        items: [{
                                            xtype: 'symphony.pagedcombobox',
                                            url: categoriesUrl,
                                            model: 'category',
                                            blankValueTitle: 'Default',
                                            ref: 'combo',
                                            name: 'categoryId',
                                            bindingName: 'categoryName',
                                            valueField: 'id',
                                            displayField: 'name',
                                            allowBlank: false,
                                            flex: 1,
                                            emptyText: 'Select a category...',
                                            valueNotFoundText: 'Select a category...'
                                        }, {
                                            xtype: 'button',
                                            text: '...',
                                            handler: function (btn) {
                                                Symphony.EditNameDescriptionPairs("Manage Course Categories", categoriesUrl, function (result) {
                                                    var composite = me.find('name', 'categoryCompositeField')[0];
                                                    var combo = composite.items.itemAt(0);
                                                    // refresh the list if it's already been loaded
                                                    combo.store.load();
                                                    combo.store.on('load', function () {
                                                        // reset the combobox
                                                        for (var i = 0; i < result.data.length; i++) {
                                                            if (result.data[i].id == combo.getValue()) {
                                                                combo.valueNotFoundText = result.data[i].name;
                                                                combo.setValue(0);
                                                                combo.setValue(result.data[i].id);
                                                            }
                                                        }
                                                    }, this, { single: true });
                                                }, { categoryTypeId: Symphony.CategoryType.trainingProgram });
                                            }
                                        }]
                                    }]
                                }, {
                                    defaults: { anchor: '100%', xtype: 'textfield' },
                                    labelWidth: 130,
                                    items: [{
                                        name: 'isNewHire',
                                        fieldLabel: '&nbsp;&nbsp;New Hire',
                                        xtype: 'checkbox',
                                        disabled: this.trainingProgramId > 0,
                                        listeners: {
                                            check: function (box, checked) {
                                                var fields = me.find('xtype', 'datefield');
                                                for (var i = 0; i < fields.length; i++) {
                                                    if (checked) {
                                                        fields[i].disable();
                                                        fields[i]._oldValue = fields[i].getValue();
                                                        fields[i].setValue('');
                                                    } else {
                                                        fields[i].enable();
                                                        if (fields[i]._oldValue) {
                                                            fields[i].setValue(fields[i]._oldValue);
                                                        }
                                                    }
                                                }
                                                me.find('name', 'newHireTransitionPeriod')[0].setDisabled(checked);
                                                var card = me.find('name', 'scheduleCard')[0].getLayout();
                                                card.setActiveItem(checked ? 1 : 0);
                                                me.fireEvent('newHire', checked);
                                            }
                                        }
                                    }, {
                                        // SETTERS (values used when loading the combobox initially)
                                        // the name of the value; this is used when binding data to the field
                                        name: 'ownerId',
                                        // the name of the displayed text if nothing is found
                                        bindingName: 'ownerName',

                                        // GETTERS (values used when retrieving new values for the selection)
                                        // the name of the displayed text after a search
                                        displayField: 'fullName',
                                        // the name of the value field after a search
                                        valueField: 'id',

                                        fieldLabel: '&nbsp;&nbsp;Owner',
                                        xtype: 'symphony.pagedcombobox',
                                        model: 'user',
                                        url: '/Services/customer.svc/users/customer/{0}/roles/?roles={1}'.format(Symphony.User.customerId, 'CourseAssignment - Training Administrator,CourseAssignment - Training Manager'),
                                        allowBlank: false

                                    }, {
                                        name: 'cost',
                                        fieldLabel: '&nbsp;&nbsp;Cost',
                                        xtype: 'moneyfield'
                                    }, {
                                        name: 'newHireTransitionPeriod',
                                        fieldLabel: '&nbsp;&nbsp;Transition Period',
                                        help: {
                                            title: 'New Hire Transition Period',
                                            text: 'Only applies to non-"New Hire" Training Programs.<br/><br/>The minumum number of days a Training Program must be available to a user in order to be seen by that user after a transition from "New Hire" to "Normal" status.<br/><br>' +
                                                'NOTE: If left blank, this setting will automatically inherit from the company setting (currently set to ' + Symphony.User.customerNewHireTransitionPeriod + ' days).'
                                        },
                                        allowBlank: true,
                                        allowNegative: true,
                                        allowDecimals: false,
                                        xtype: 'numberfield'
                                    }]
                                }]
                            }, {
                                name: 'description',
                                fieldLabel: 'Description',
                                xtype: 'textarea',
                                anchor: '0'
                            }, {
                                name: 'disableScheduled',
                                fieldLabel: 'Disable Scheduled Notifications',
                                xtype: 'checkbox'
                            }]
                        }, {
                            xtype: 'fieldset',
                            title: 'Scheduling',
                            name: 'schedule',
                            items: [{
                                xtype: 'panel',
                                name: 'scheduleCard',
                                layout: {
                                    type: 'card',
                                    layoutOnCardChange: true
                                },
                                activeItem: 0,
                                items: [{
                                    //width: 300,
                                    layout: 'form',
                                    items: [{
                                        fieldLabel: 'Start Date',
                                        name: 'startDate',
                                        xtype: 'datefield',
                                        dateOnly: true,
                                        labelStyle: 'width:180px',
                                        disabled: this.trainingProgramId > 0,
                                        allowBlank: true, // this will be overridden if the checkbox changes
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this),
                                            select: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        fieldLabel: 'Due Date (for notification only)',
                                        name: 'dueDate',
                                        xtype: 'datefield',
                                        dateOnly: true,
                                        labelStyle: 'width:180px',
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this),
                                            select: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        fieldLabel: 'End Date',
                                        name: 'endDate',
                                        xtype: 'datefield',
                                        dateOnly: true,
                                        labelStyle: 'width:180px',
                                        allowBlank: true, // this will be overridden if the checkbox changes
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this),
                                            select: Ext.bind(this.validateDates, this)
                                        }
                                    }]
                                }, {
                                    layout: 'form',
                                    //columnWidth: 1,
                                    defaults: {
                                        labelStyle: 'width:220px'
                                    },
                                    items: [{
                                        fieldLabel: 'Enable Custom New Hire Offsets',
                                        name: 'newHireOffsetEnabled',
                                        xtype: 'checkbox',
                                        allowBlank: true,
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this),
                                            check: function (field, checked) {
                                                var fields = this.ownerCt.find('xtype', 'numberfield');
                                                for (var i = 0; i < fields.length; i++) {
                                                    fields[i].setDisabled(!checked);
                                                    fields[i].allowBlank = !checked;
                                                }
                                            }
                                        }
                                    }, {
                                        fieldLabel: 'Start Offset',
                                        name: 'newHireStartDateOffset',
                                        xtype: 'numberfield',
                                        allowBlank: true,
                                        allowNegative: false,
                                        allowDecimals: false,
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        fieldLabel: 'Due Offset',
                                        name: 'newHireDueDateOffset',
                                        xtype: 'numberfield',
                                        allowBlank: true,
                                        allowNegative: false,
                                        allowDecimals: false,
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        fieldLabel: 'End Offset',
                                        name: 'newHireEndDateOffset',
                                        xtype: 'numberfield',
                                        allowBlank: true,
                                        allowNegative: false,
                                        allowDecimals: false,
                                        listeners: {
                                            change: Ext.bind(this.validateDates, this)
                                        }
                                    }, {
                                        html: '<div class="pure-text"><h4>Instructions</h4>' +
                                        '<p>New hire offsets allow you to schedule a training program based on the user\'s hire date.</p>' +
                                        '<p>For example, if a user was hired on 10/11/2012 and the following offsets are added:</p>' +
                                        '<ul><li>Start Offset: 5 days</li><li>Due Offset: 20 days</li><li>End Offset: 25 days</li></ul>' +
                                        '<p>The schedule for the training program would be as follows:</p>' +
                                        '<ul><li>Start Date: 10/16/2012</li><li>Due Date: 10/31/2012</li><li>End Date: 11/5/2012</li></ul></div>'
                                    }]
                                }]
                            }]
                        }]
                    }, {
                        title: 'Assign Users',
                        name: 'assignedusers',
                        xtype: 'courseassignment.trainingprogramusers'
                    }, {
                        title: 'Assign Courses',
                        name: 'assignedcourses',
                        forceLayout: true,
                        xtype: 'courseassignment.trainingprogramcoursepanel',
                        listeners: {
                            setactiveafterdate: function (dt, record, grid, e) {
                                var form = me.find('name', 'general')[0];
                                var tp = form.getValues();

                                if (!tp.startDate || !tp.endDate) {
                                    Ext.Msg.alert('Error', 'You must select a start and end date for the Training Program before you can set course dates.');
                                    return;
                                }

                                var minDate = Symphony.parseDate(tp.startDate);
                                var maxDate = Symphony.parseDate(tp.endDate);
                                var menu = new Ext.menu.DateMenu({
                                    value: dt,
                                    minDate: minDate,
                                    maxDate: maxDate,
                                    dateOnly: true,
                                    handler: function (picker, date) {
                                        if (date) {
                                            // TODO: Make this a util function
                                            // subtract the timezone offset from the date to effectively zero out the time once it's converted to UTC
                                            date = date.add(Date.MINUTE, -date.getTimezoneOffset());
                                        }
                                        record.set('activeAfterDate', date.formatSymphony('microsoft'));
                                        record.commit();
                                        grid.getView().refresh();
                                    }
                                });
                                menu.showAt(e.getXY());

                            },
                            setduedate: function (dt, record, grid, e) {
                                var form = me.find('name', 'general')[0];
                                var tp = form.getValues();
                                if (tp.newHireIndicator) {
                                    Ext.Msg.alert('Error', 'Due dates cannot be set for new hire Training Programs.');
                                    return;
                                }
                                if (!tp.startDate || !tp.endDate) {
                                    Ext.Msg.alert('Error', 'You must select a start and end date for the Training Program before you can set course dates.');
                                    return;
                                }
                                var minDate = Symphony.parseDate(tp.startDate);
                                var maxDate = Symphony.parseDate(tp.endDate);
                                var menu = new Ext.menu.DateMenu({
                                    value: dt,
                                    minDate: minDate,
                                    maxDate: maxDate,
                                    dateOnly: true,
                                    handler: function (picker, date) {
                                        if (date) {
                                            // TODO: Make this a util function
                                            // subtract the timezone offset from the date to effectively zero out the time once it's converted to UTC
                                            date = date.add(Date.MINUTE, -date.getTimezoneOffset());
                                        }
                                        record.set('dueDate', date.formatSymphony('microsoft'));
                                        record.commit();
                                        grid.getView().refresh();
                                    }
                                });
                                menu.showAt(e.getXY());
                            }
                        }
                    }, {
                        title: 'Documents',
                        name: 'documents',
                        layout: 'fit',
                        border: true,
                        stateId: 'courseassignment.trainingprogramdocumentsgrid',
                        xtype: 'courseassignment.trainingprogramdocumentsgrid'
                    }]
                }]
            }],
            listeners: {
                newHire: function (newHire) {
                    var coursePanel = me.findByType('courseassignment.trainingprogramcoursepanel')[0];
                    coursePanel.setNewHire(newHire);
                }
            }
        });
        this.callParent(arguments);
    },
    validateDates: function () {
        var start = this.find('name', 'startDate')[0];
        var due = this.find('name', 'dueDate')[0];
        var end = this.find('name', 'endDate')[0];

        // start date
        if (start.getValue() && end.getValue() && start.getValue() > end.getValue()) {
            start.markInvalid('The start date must come before the end date.');
        } else if (start.getValue() && due.getValue() && start.getValue() > due.getValue()) {
            start.markInvalid('The start date must come before the due date.');
        } else {
            start.clearInvalid();
        }

        // due date
        if (due.getValue() && start.getValue() && due.getValue() <= start.getValue()) {
            due.markInvalid('The due date must come after the start date.');
        } else if (due.getValue() && end.getValue() && due.getValue() > end.getValue()) {
            due.markInvalid('The due date must be on or before the end date.');
        } else {
            due.clearInvalid();
        }

        // end date
        if (start.getValue() && end.getValue() && end.getValue() <= start.getValue()) {
            end.markInvalid('The end date must come after the start date.');
        } else if (due.getValue() && end.getValue() && end.getValue() < due.getValue()) {
            end.markInvalid('The end date must be on or after the due date.');
        } else {
            end.clearInvalid();
        }

        // set min/max values here so form validation works
        var maxStart = due.getValue() || end.getValue();
        if (maxStart) {
            start.setMaxValue(maxStart.add(Date.DAY, 1));
        }
        var minDue = start.getValue();
        if (minDue) {
            due.setMinValue(minDue.add(Date.DAY, 1));
        }
        var maxDue = end.getValue();
        if (maxDue) {
            due.setMaxValue(maxDue);
        }
        var minEnd = due.getValue(true) || start.getValue(true);
        if (minEnd) {
            if (!due.getValue()) {
                // after the start date
                end.setMinValue(minEnd.add(Date.DAY, 1));
            } else {
                // equal to the due date
                end.setMinValue(minEnd);
            }
        }

        // if the start date is already passed, the date cannot be changed
        var newHire = this.find('name', 'isNewHire')[0];
        var current = new Date();
        if (!start.getValue() || start.getValue() > current) {
            // enable the start field if the TP hasn't started
            // but not if it's a new hire TP
            if (!newHire.getValue()) {
                start.setDisabled(false);
            }
            newHire.setDisabled(false);
        }

        this.find('name', 'newHireTransitionPeriod')[0].setDisabled(newHire.getValue());

        var card = this.find('name', 'scheduleCard')[0].getLayout();
        var isNewHire = newHire.getValue();
        card.setActiveItem(isNewHire ? 1 : 0);

        var offsetEnabled = this.find('name', 'newHireOffsetEnabled')[0];
        var fields = offsetEnabled.ownerCt.find('xtype', 'numberfield');
        var isOffsetEnabled = offsetEnabled.getValue();
        for (var i = 0; i < fields.length; i++) {
            fields[i].setDisabled(!isOffsetEnabled);
            fields[i].allowBlank = !isOffsetEnabled;
            if (isOffsetEnabled && fields[i].getValue() === '') {
                fields[i].markInvalid('This field is required if custom new hire offsets are selected.');
            }
        }

        if (isOffsetEnabled) {
            var startOffset = offsetEnabled.ownerCt.find('name', 'newHireStartDateOffset')[0].getValue();
            var dueOffset = offsetEnabled.ownerCt.find('name', 'newHireDueDateOffset')[0].getValue();
            var endOffset = offsetEnabled.ownerCt.find('name', 'newHireEndDateOffset')[0].getValue();
            if (startOffset >= dueOffset) {
                offsetEnabled.ownerCt.find('name', 'newHireStartDateOffset')[0].markInvalid('Start offset must be less than due offset.');
            } else {
                offsetEnabled.ownerCt.find('name', 'newHireStartDateOffset')[0].clearInvalid();
            }
            if (startOffset >= endOffset) {
                offsetEnabled.ownerCt.find('name', 'newHireEndDateOffset')[0].markInvalid('End offset must be greater than start offset.');
            } else {
                offsetEnabled.ownerCt.find('name', 'newHireEndDateOffset')[0].clearInvalid();
            }
            if (dueOffset > endOffset) {
                offsetEnabled.ownerCt.find('name', 'newHireDueDateOffset')[0].markInvalid('Due offset must be less than or equal to end offset.');
            } else {
                offsetEnabled.ownerCt.find('name', 'newHireDueDateOffset')[0].clearInvalid();
            }
        }
    },
    onRender: function () {
        Symphony.CourseAssignment.TrainingProgramForm.superclass.onRender.apply(this, arguments);
        this.load();
    },
    load: function () {
        var me = this;
        if (this.trainingProgramId) {
            Ext.Msg.wait('Please wait while your program is loaded...', 'Please wait...');
            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/portal.svc/trainingprogramdetails/' + this.trainingProgramId + '?manage=true',
                success: function (result) {
                    // load up the appropriate panels with the results
                    me.trainingProgramId = result.data.id;

                    // set the basic information
                    me.bindForm(result.data);

                    // set the training program for the docs
                    me.find('name', 'documents')[0].setTrainingProgram(result.data);

                    // add the selected user groups
                    me.find('name', 'assignedusers')[0].setAssignments(result.data.primaryAssignment, result.data.secondaryAssignment);

                    // add the assigned courses
                    me.find('name', 'assignedcourses')[0].setTrainingProgram(result.data);

                    // documents take care of themselves, so nothing to do there

                    // disable as needed
                    if (Symphony.User.isTrainingManager && !Symphony.User.isTrainingAdministrator && result.data.ownerId != Symphony.User.id) {
                        me.find('name', 'general')[0].getForm().items.each(function (item) {
                            item.disable();
                        });

                        me.find('name', 'assignedusers')[0].disable();

                        me.find('name', 'documents')[0].disable();

                        me.find('name', 'assignedcourses')[0].disable();

                        me.find('xtype', 'symphony.savecancelbar')[0].find('name', 'save')[0].disable();
                    }

                    Ext.Msg.hide();
                }
            });
        } else {
            window.setTimeout(function () {
                me.find('name', 'assignedcourses')[0].setTrainingProgram();
                // default the owner to the current user
                var cbo = me.find('name', 'general')[0].find('name', 'ownerId')[0];
                cbo.valueNotFoundText = Symphony.User.fullName;
                cbo.setValue(Symphony.User.id);

                // default the category
                var cf = me.find('name', 'categoryCompositeField')[0];
                cf.items.itemAt(0).setValue(0, true, true);

                me.validateDates();
            }, 1);
        }
    },
    bindForm: function (data) {
        // first, bind the basic information
        this.find('name', 'general')[0].bindValues(data);

        var cf = this.find('name', 'categoryCompositeField')[0];

        if (data.categoryId) {
            cf.items.itemAt(0).setValue(data.categoryId, true, true);
        }
        else {
            cf.items.itemAt(0).setValue(0, true, true);
        }

        // add the schedule
        var schedules = this.find('name', 'schedule')[0].findBy(function (field) { return field.name !== undefined && field.setValue != null; })
        for (var i = 0; i < schedules.length; i++) {
            var item = schedules[i];
            // clear out any min/max settings before we set the value
            // to avoid issues with the invalid indicator
            if (item.xtype == 'datefield') {
                item.setMinValue('01/01/1900');
                item.setMaxValue('01/01/3000');
                item.clearInvalid();

                if (item.name != 'dueDate') {
                    item.allowBlank = !data.isLive;
                }

                var dt = Symphony.parseDate(data[item.name || item.id]);

                if (item.setValue && dt.getFullYear() != Symphony.defaultYear) {
                    item.setValue(dt);
                }
            } else if (item.setValue) {
                item.setValue(data[item.name || item.id]);
            }
        };

        // validate the schedule (sets the min/max dates, etc)
        this.validateDates();
    },
    save: function (duplicate) {
        // saves a training program
        var me = this;

        // for duplications, require start and end dates
        var previousAllowBlank = this.find('name', 'startDate')[0].allowBlank;
        if (duplicate) {
            if (me.trainingProgramId == 0) {
                Ext.Msg.alert('Invalid Action', 'You cannot duplicate an un-saved training program.');
                return;
            }
            this.find('name', 'startDate')[0].allowBlank = false;
            this.find('name', 'endDate')[0].allowBlank = false;
        }


        var form = me.find('name', 'general')[0];

        if (!form.isValid()) {
            Ext.Msg.alert('Errors Detected', 'Your training program has some errors. Please correct and re-save');
            return;
        }
        //            if(!form.isValid()){ 
        //                // so failed duplications don't impact other saves
        //                this.find('name','startDate')[0].allowBlank = previousAllowBlank;
        //                this.find('name','endDate')[0].allowBlank = previousAllowBlank;
        //                return false; 
        //            }

        var users = me.find('name', 'assignedusers')[0];
        var courses = me.find('name', 'assignedcourses')[0];
        var documents = me.find('name', 'documents')[0];

        //            if(!courses.isValid()){ 
        //                return false; 
        //            }

        // basic information
        var tp = form.getValues();

        // some defaults if not specified
        tp.cost = tp.cost || 0;

        // avoid setting it completely if it has a blank value
        var nullable = ['dueDate', 'endDate', 'startDate', 'newHireStartDateOffset', 'newHireEndDateOffset', 'newHireDueDateOffset', 'newHireTransitionPeriod'];
        for (var i = 0; i < nullable.length; i++) {
            if (!tp[nullable[i]]) {
                delete tp[nullable[i]];
            }
        }

        if (!tp.categoryId) {
            tp.categoryId = 0;
        }

        // users
        var userAssignments = users.getAssignments();
        Ext.apply(tp, userAssignments);

        // courses
        var courseAssignments = courses.getAssignments();
        if (courseAssignments.electiveCourses.length < courseAssignments.minimumElectives) {
            Ext.Msg.alert('Invalid Entry', 'Cannot require ' + courseAssignments.minimumElectives + ' of ' + courseAssignments.electiveCourses.length + ' electives. Please validate course assignment.')
            return;
        }

        if (courseAssignments.requiredCourses.length < 1 && courseAssignments.minimumElectives < 1) {
            Ext.Msg.alert('Warning', 'Warning, you don\'t have at least one required course or a minimum elective course value of at least one.')
            //return;
        }

        // 7/10/12, started this - may want to add later
        /*if (courseAssignments.electiveCourses.length > 0 && courseAssignments.minimumElectives == 0) {
        Ext.Msg.alert('Invalid Entry', 'If you have any elective courses, you must at least require one of them electives. Please validate course assignment.')
        return;
        }*/
        Ext.apply(tp, courseAssignments);

        // duplication info
        if (duplicate) {
            tp.duplicateFromId = me.trainingProgramId;
        }

        // save and process files
        Symphony.Ajax.request({
            url: '/services/courseassignment.svc/trainingprograms/' + (duplicate ? 0 : me.trainingProgramId),
            jsonData: tp,
            success: function (result) {
                if (duplicate) {
                    me.fireEvent('duplicate', result.data.id, result.data.name, result.data.description);
                } else {
                    me.trainingProgramId = result.data.id;

                    me.load();

                    // re-bind the form values, in case the name was modified by a duplication
                    //me.bindForm(result.data);

                    me.setTitle(result.data.name);

                    // documents
                    if (documents.store.query('id', 0).length > 0) {
                        Ext.Msg.wait('Please wait while your documents are uploaded...', 'Please wait...');
                        // make sure the id is set properly
                        documents.setTrainingProgram({ id: result.data.id });

                        // upload new files
                        documents.upload({
                            trainingProgramId: result.data.id,
                            onComplete: function () {
                                Ext.Msg.hide();
                                me.fireEvent('save');
                            }
                        });
                    } else {
                        me.fireEvent('save');
                    }
                    // delete "removed" files from the TP
                }
            }
        });
    }
});
