﻿(function () {
    Symphony.CourseAssignment.OnlineCourseParameterPanel = Ext.define('courseassignment.onlinecourseparameterpanel', {
        alias: 'widget.courseassignment.onlinecourseparameterpanel',
        extend: 'Ext.Panel',
        activated: false,
        artisanCourseId: 0,
        onlineCourseOverrides: [],
        isShared: false,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                border: false,
                disabled: this.isShared,
                items: [{
                    border: false,
                    xtype: 'artisan.parametersetoptionspanel'
                }],
                listeners: {
                    activate: function () {
                        if (!me.activated) {

                            me.removeAll();

                            Symphony.Ajax.request({
                                method: 'GET',
                                url: '/services/artisan.svc/courses/' + me.artisanCourseId,
                                success: function (args) {

                                    var parameters = [];
                                    var defaultParameters = [];

                                    if (args.data.parameters) {
                                        parameters = JSON.parse(args.data.parameters);
                                    }

                                    if (args.data.defaultParametersJson) {
                                        defaultParameters = JSON.parse(args.data.defaultParametersJson);
                                    }

                                    var parameterSetData = {};
                                    var parameterData = {};

                                    // Start with the artisan params
                                    for (var i = 0; i < defaultParameters.length; i++) {
                                        parameterSetData['parameterSet' + defaultParameters[i].parameterSetId] = {
                                            display: defaultParameters[i].parameterSetOptionValue,
                                            value: defaultParameters[i].parameterSetOptionId
                                        }

                                        parameterData[defaultParameters[i].parameterId] = defaultParameters[i].value;
                                    }

                                    // Override with the online course params
                                    for (var i = 0; i < me.onlineCourseOverrides.length; i++) {
                                        parameterSetData['parameterSet' + me.onlineCourseOverrides[i].parameterSetId] = {
                                            display: me.onlineCourseOverrides[i].parameterSetOptionValue,
                                            value: me.onlineCourseOverrides[i].parameterSetOptionId
                                        }

                                        parameterData[me.onlineCourseOverrides[i].parameterId] = me.onlineCourseOverrides[i].value;
                                    }
                                    
                                    Symphony.Ajax.request({
                                        method: 'GET',
                                        url: '/services/artisan.svc/parameters?codes=' + parameters.join('|'),
                                        success: function (args) {
                                            var params = args.data;

                                            for (var i = 0; i < params.length; i++) {
                                                if (parameterData[params[i].id]) {
                                                    params[i].value = parameterData[params[i].id];
                                                }
                                            }
                                            var newPanel = new Symphony.Artisan.ParameterSetOptionsPanel({
                                                xtype: 'artisan.parametersetoptionspanel',
                                                parameters: params,
                                                parameterSetData: parameterSetData,
                                                parametersFound: parameters
                                            });

                                            me.add(newPanel);
                                            me.doLayout();
                                        }
                                    });

                                }
                            });

                            me.activated = true;
                        }
                    }
                }
            });

            this.callParent(arguments);
        },
        getParameterOverrides: function () {
            return this.find('xtype', 'artisan.parametersetoptionspanel')[0].getParameterOverrides();
        }
    });

})();

