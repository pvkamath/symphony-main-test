﻿// Called a list because it's not really a grid.
Ext.define('CourseAssignment.AccreditationList', {
    extend: 'Ext.form.FieldContainer',
    xtype: 'courseassignment.accreditationlist',
    requires: [
        'CourseAssignment.AccreditationListItem',
        'OnlineCourseAccreditation',
        'AccreditationStatus'
    ],
    mixins: ['Symphony.mixins.FunctionOwnership'],

    fieldLabel: 'Accreditations',
    hidden: true,

    items: [{
        xtype: 'container',
        itemId: 'container',
        layout: {
            type: 'vbox',
            align: 'stretch'
        }
    }],

    setAccreditations: function(accreditations) {
        var me = this,
            container = me.queryById('container');

        container.removeAll();

        if (accreditations.length == 0) {
            me.hide();
            return;
        }

        me.show();

        Ext.Array.each(accreditations, function(accreditation) {
            container.add({
                xtype: 'courseassignment.accreditationlistitem',
                accreditation: accreditation
            });
        });
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});