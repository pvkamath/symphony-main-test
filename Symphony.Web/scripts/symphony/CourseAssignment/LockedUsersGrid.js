﻿Symphony.CourseAssignment.LockedUsersGrid = Ext.define('courseassignment.lockedusersgrid', {
    alias: 'widget.courseassignment.lockedusersgrid',
    extend: 'symphony.searchablegrid',
    initComponent: function () {
        var me = this;
        var url = '/services/CourseAssignment.svc/lockedusers/?lockedOnly=true';

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'left',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: [{
                header: 'First Name',
                dataIndex: 'firstName'
            }, {
                /*id: 'lastname',*/
                header: 'Last Name',
                dataIndex: 'lastName',
                flex: 1
            }, {
                /*id: 'lockedCourseCount',*/
                header: 'Locked Courses',
                dataIndex: 'lockedCourseCount',
                renderer: function (v) {
                    return '<a href="#">' + v + '</a>';
                }
            }]
        });

        Ext.apply(this, {
            tbar: {
                items: [{
                    xtype: 'checkbox',
                    boxLabel: 'Show All',
                    listeners: {
                        'check': function (field, state) {
                            var updatedUrl = '';
                            if (state) {
                                updatedUrl = url.replace('lockedOnly=true', 'lockedOnly=false');
                            } else {
                                updatedUrl = url.replace('lockedOnly=false', 'lockedOnly=true');
                            }
                            me.updateStore(updatedUrl);
                        }
                    }
                }]
            },
            idProperty: 'id',
            url: url,
            colModel: colModel,
            model: 'user'
        });

        this.callParent(arguments);
    }
});
