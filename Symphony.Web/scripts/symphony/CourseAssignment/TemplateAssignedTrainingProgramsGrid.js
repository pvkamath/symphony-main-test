﻿Ext.define('CourseAssignment.TemplateAssignedTrainingProgramsGrid', {
    extend: 'symphony.searchablegrid',
    xtype: 'courseassignment.templateassignedtrainingprogramsgrid',
    mixins: ['Symphony.mixins.FunctionOwnership'],
    model: "trainingProgram",
    baseUrl: '/services/courseassignment.svc/trainingprograms/template/',
    initComponent: function () {
        var me = this;

        if (!this.templateId) {
            this.templateId = 0;
        }

        Ext.apply(this, {
            tbar: {
                items: ['Training Programs']
            },
            url: this.baseUrl + this.templateId,
            columns: [{
                dataIndex: 'name',
                text: 'Name',
                flex: 1
            },
            { text: 'Sku', dataIndex: 'sku', flex: 1 },
            { text: 'Enabled', dataIndex: 'isLive', renderer: Symphony.checkRenderer, width: 64 },
            { text: 'Start Date', dataIndex: 'startDate', renderer: Symphony.pastDefaultDateRenderer, width: 64 },
            { text: 'End Date', dataIndex: 'endDate', renderer: Symphony.pastDefaultDateRenderer, width: 64 }]
        });

        this.callParent();
    },
    setTemplateId: function (id) {
        this.templateId = id;
        this.store.proxy.api.read = this.baseUrl + id;
    }
}, function (clazz) {
    Log.debug('Class created: ' + clazz.$className);
});