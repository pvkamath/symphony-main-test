﻿Symphony.CourseAssignment.LockedUsersTab = Ext.define('courseassignment.lockeduserstab', {
    alias: 'widget.courseassignment.lockeduserstab',
    extend: 'Ext.Panel',
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            layout: 'border',
            defaults: {
                border: false
            },
            items: [{
                split: true,
                region: 'center',
                ref: 'grid',
                stateId: 'courseassignment.lockedusersgrid',
                xtype: 'courseassignment.lockedusersgrid',
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex;
                        
                        if (fieldName == 'lockedCourseCount') {
                            var record = grid.getStore().getAt(rowIndex);

                            me.showUserCoursesGrid(record);
                        }
                    }
                }
            }]
        });
        this.callParent(arguments);
    },
    showUserCoursesGrid: function (record) {
        var me = this;
        var w = new Ext.Window({
            height: 600,
            width: 800,
            layout: 'fit',
            modal: true,
            title: record.get('firstName') + ' ' + record.get('lastName'),
            border: false,
            items: [{
                userId: record.get('id'),
                xtype: 'courseassignment.lockedusercoursesgrid',
                listeners: {
                    change: function () {
                        me.grid.refresh();
                    }
                }
            }],
            buttons: [{
                text: 'Close',
                handler: function () {
                    w.close();
                }
            }]
        });
        w.show();
    }
});
