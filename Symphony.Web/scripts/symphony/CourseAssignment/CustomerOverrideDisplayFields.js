﻿(function () {
    Symphony.CourseAssignment.CustomerOverrideDisplayFields = Ext.define('courseassignment.customeroverridedisplayfields', {
        alias: 'widget.courseassignment.customeroverridedisplayfields',
        extend: 'Ext.form.FieldSet',
        defaults: {
            anchor: '100%',
            disabled: true,
            exclude: true,
            labelWidth: 150
        },
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                title: 'Global Parameters' + (Symphony.Portal.hasAnySettingsPermission() ? ' (<a href="#" onclick="(new Symphony.Portal.CustomerSettingsWindow()).show()">Edit</a>)' : ''),
                items: [
                    Symphony.getCombo('showPretestOverride', 'Show Pretest', Symphony.getYesNoInheritStore(), { disabled: true }),
                //Symphony.getCombo('showPosttestOverride', 'Show Posttest', Symphony.getYesNoInheritStore(), { disabled: true }),
                //Symphony.getCombo('showObjectivesOverride', 'Show Objectives', Symphony.getYesNoInheritStore(), { disabled: true }),
                    Symphony.getCombo('pretestTestOutOverride', 'Test Out', Symphony.getYesNoInheritStore(), { disabled: true }),
                    {
                        name: 'passingScoreOverride',
                        fieldLabel: 'Passing Score',
                        xtype: 'numberfield',
                        minValue: 0,
                        maxValue: 100
                    },
                    Symphony.getCombo('navigationMethodOverride', 'Navigation', Symphony.getArtisanNavigationMethodStore()),
                    Symphony.getCombo('certificateEnabledOverride', 'Certificate Enabled', Symphony.getYesNoInheritStore()),
                /*{
                name: 'disclaimerOverride',
                fieldLabel: 'Disclaimer',
                xtype: 'textfield'
                }, {
                name: 'contactEmailOverride',
                fieldLabel: 'Contact Email',
                xtype: 'textfield'
                },*/
                    Symphony.getCombo('pretestTypeOverride', 'Pretest Type', Symphony.getArtisanTestTypeStore()),
                    Symphony.getCombo('posttestTypeOverride', 'Posttest Type', Symphony.getArtisanTestTypeStore()),
                    Symphony.getCombo('randomizeQuestionsOverride', 'Randomize Questions', Symphony.getYesNoInheritStore()),
                    Symphony.getCombo('randomizeAnswersOverride', 'Randomize Answers', Symphony.getYesNoInheritStore()),
                    Symphony.getCombo('reviewMethodOverride', 'Review', Symphony.getArtisanReviewMethodStore()),
                    Symphony.getCombo('markingMethodOverride', 'Marking', Symphony.getArtisanMarkingMethodStore()),
                    Symphony.getCombo('inactivityMethodOverride', 'Inactivity', Symphony.getArtisanInactivityMethodStore()),
                    {
                        name: 'inactivityTimeoutOverride',
                        fieldLabel: 'Inactivity Timeout',
                        xtype: 'numberfield',
                        minValue: 0
                    },
                    Symphony.getCombo('isForceLogoutOverride', 'Force Logout', Symphony.getYesNoInheritStore()),
                    {
                        name: 'minimumPageTimeOverride',
                        fieldLabel: 'Min. Page Time',
                        xtype: 'numberfield',
                        minValue: 0
                    }, {
                        name: 'maximumQuestionTimeOverride',
                        fieldLabel: 'Max. Question Time',
                        xtype: 'numberfield',
                        minValue: 0
                    }, {
                        name: 'maxTimeOverride',
                        xtype: 'numberfield',
                        fieldLabel: 'Max Test Time',
                        minValue: 0
                    }, {
                        name: 'minTimeOverride',
                        xtype: 'numberfield',
                        fieldLabel: 'Min Time',
                        minValue: 0
                    }, {
                        name: 'minLoTimeOverride',
                        xtype: 'numberfield',
                        fieldLabel: 'Min Learning Object Time',
                        minValue: 0
                    }, {
                        name: 'minScoTimeOverride',
                        xtype: 'numberfield',
                        fieldLabel: 'Min SCO Time',
                        minValue: 0
                    }, {
                        fieldLabel: 'Theme',
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/artisan.svc/themes/',
                        name: 'skinOverride',
                        bindingName: 'theme.name',
                        model: 'artisanTheme',
                        valueField: 'id',
                        displayField: 'name',
                        allowBlank: true,
                        emptyText: 'Select a Theme',
                        valueNotFoundText: 'Select a Theme',
                        ref: 'skinCombo',
                        listeners: {
                            select: function (combo, records, eOpts) {
                                me.child('*[ref=flavorCombo]').setVisible(records[0].data.isDynamic)
                            }
                        }
                    }, {
                        xtype: 'symphony.pagedcombobox',
                        url: '/services/customer.svc/themes/', // Not really themes, but groups of settings for a dynamic theme.... A flavor!
                        name: 'themeFlavorOverrideId',
                        fieldLabel: 'Theme Flavor',
                        model: 'Theme',
                        valueField: 'id',
                        displayField: 'name',
                        allowBlank: true,
                        emptyText: 'Select a Flavor',
                        valueNotFoundText: 'Select a Flavor',
                        bindingName: 'themeFlavor.name',
                        ref: 'flavorCombo',
                        hidden: true
                    }, {
                        name: 'testAttempts',
                        fieldLabel: 'Max Test Attempts',
                        xtype: 'numberfield',
                        decimalPrecision: 0,
                        minValue: 1,
                        allowBlank: true
                    }
                ]
            });

            this.callParent(arguments);


            this.child('*[ref=skinCombo]').store.on('load', function () {
                this.insert(0, Ext.create('artisanTheme', {
                    name: '- None -',
                    id: null,
                    description: '',
                    cssPath: '',
                    skinCssPath: ''
                }));
            });

            this.child('*[ref=flavorCombo]').store.on('load', function () {
                this.insert(0, Ext.create('Theme', {
                    name: '- None -',
                    id: null
                }));
            });
            
        }
    });


})();