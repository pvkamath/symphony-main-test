﻿Symphony.CourseAssignment.ArtisanProgressViewer = Ext.define('courseassignment.artisanprogressviewer', {
    alias: 'widget.courseassignment.artisanprogressviewer',
    extend: 'Ext.Panel',
    course: null,
    dataChunk: null,
    bookmark: null,
    scormData: null,
    initComponent: function () {
        var me = this,
            columns = [{
                xtype: 'treecolumn',
                text: 'Course Element',
                dataIndex: 'text',
                flex: 1
            }],
            tbar = Symphony.User.isSalesChannelAdmin ?
            {
                items: {
                    xtype: 'button',
                    iconCls: 'x-button-save',
                    text: 'Save',
                    handler: function () {
                        me.fireEvent('save', me.getUpdatedActivityData());
                    }
                }
            } : null;

        if (Symphony.User.isSalesChannelAdmin) {
            columns = [{
                xtype: 'checkcolumn',
                text: 'Visited',
                dataIndex: 'isVisited',
                width: 75,
                listeners: {
                    checkchange: function (col, rowIndex, checked, eOpts) {
                        var courseTree = me.query('[xtype=artisan.searchablecoursetree]')[0];

                        me.updateNodeCls(courseTree.view.store.getAt(rowIndex));
                    }
                }
            }].concat(columns)
            .concat([{
                text: 'Boomarked',
                dataIndex: 'isBookmarked',
                align: 'center',
                width: 80,
                renderer: function (value, meta, record) {
                    if (record.page) {
                        return Ext.String.format('<input type="radio" name="bookmarked" {0}/>', value ? "checked='checked'" : "");
                    }
                    return "";
                }
            }]);
        }

        Ext.apply(this, {
            width: 250,
            height: 300,
            layout: 'fit',
            border: false,
            frame: false,
            tbar: tbar,
            items: [{
                xtype: 'artisan.searchablecoursetree',
                border: false,
                frame: false,
                course: me.course,
                ref: 'courseTree',
                autoScroll: true,
                columns: columns,
                model: 'ArtisanCourseElement',
                root: {
                    text: 'Course Root',
                    children: [],
                    expanded: true,
                    leaf: false
                },
                rootVisible: true,
                listeners: {
                    afterrender: function () {
                        me.augmentNodes(me.tree.root);
                    },
                    cellclick: function (grid, td, cellindex, record, tr, rowIndex, e, opts) {
                        var inputs = td.getElementsByTagName('input');
                        if (inputs.length) {
                            var firstInput = inputs[0];
                            if (firstInput.type == 'radio') {
                                if (e.target != firstInput) {
                                    firstInput.setAttribute('checked', true);
                                }

                                if (firstInput.name == 'bookmarked' && firstInput.checked) {
                                    me.setBookmarkOnPageNode(grid, record);
                                }
                            } else {
                                me.updateNodeCls(record);
                            }
                        }
                    }
                }
            }]
        });

        this.callParent(arguments);

        this.loadTree();
    },
    updateTree: function (bookmark, dataChunk) {
        this.bookmark = bookmark;
        this.dataChunk = dataChunk;

        this.augmentNodes(this.tree.root);
    },
    loadTree: function () {
        this.tree = this.query('[xtype=artisan.searchablecoursetree]')[0];
        this.tree.root = this.tree.getRootNode();

        this.tree.root.removeAll();

        if (this.course && this.course.sections && this.course.sections.length) {
            var courseLayout = new Symphony.Artisan.CourseLayout();
            this.tree.root.appendChild(Symphony.map(this.course.sections, courseLayout.buildSectionNode, courseLayout));
        }
    },
    updateNodeCls: function (node) {
        var nodeType,
            cls = '';

        if (!node.isRoot()) {
            if (node.get('isVisited')) {
                cls += 'visited-' + node.get('type');
            }

            if (node.get('isBookmarked')) {
                cls += ' bookmarked-' + node.get('type');
            }

            node.set('cls', cls);
        }
    },
    augmentNodes: function (node, parentVisited) {
        var me = this,
            bookmarkArr = me.bookmark ? me.bookmark.split('|') : [-1, -1, -1, -1],
            bookmarkSco = bookmarkArr[0],
            bookmarkLo = bookmarkArr[1],
            bookmarkP = bookmarkArr[2],
            thisParentVisited = false;

        // Don't want any of the artisan defined listeners
        node.clearListeners();

        if (!node.isRoot()) {
            if (node.page) {
                if (me.dataChunk.vp[node.page.id] || (parentVisited && node.page.id == 0)) {
                    node.set('isVisited', true);
                }
                if (bookmarkP == node.page.id) {
                    node.set('isBookmarked', true);
                }
                node.set('type', 'page');
            } else if (node.section) {
                if (node.section.sectionType == Symphony.ArtisanSectionType.learningObject ||
                    node.section.sectionType == Symphony.ArtisanSectionType.pretest ||
                    node.section.sectionType == Symphony.ArtisanSectionType.posttest ||
                    node.section.sectionType == Symphony.ArtisanSectionType.objectives) {
                    if (me.dataChunk.vl[node.section.id]) {
                        node.set('isVisited', true);
                        thisParentVisited = true;
                    }
                    if (bookmarkLo == node.section.id) {
                        node.set('isBookmarked', true);
                    }
                    node.set('type', 'lo');
                } else if (node.section.sectionType == Symphony.ArtisanSectionType.sco ||
                            node.section.sectionType == Symphony.ArtisanSectionType.survey) {

                    if (me.dataChunk.v[node.section.id]) {
                        node.set('isVisited', true);
                    }
                    if (bookmarkSco == node.section.id) {
                        node.set('isBookmarked', true);
                    }
                    node.set('type', 'sco');
                }
            }

            me.updateNodeCls(node);

            node.commit();
        }

        for (var i = 0; i < node.childNodes.length; i++) {
            me.augmentNodes(node.childNodes[i], thisParentVisited);
        }
    },
    setBookmarkOnPageNode: function (grid, node) {
        grid.suspendEvents();

        var me = this,
            nodesToUpdate = [node];

        grid.store.each(function (n) {
            n.set('isBookmarked', false);
        });

        // Get the learning object
        if (node.parentNode) {
            nodesToUpdate.push(node.parentNode);
        }

        // get the sco
        if (node.parentNode.parentNode) {
            nodesToUpdate.push(node.parentNode.parentNode);
        }

        for (var i = 0; i < nodesToUpdate.length; i++) {
            nodesToUpdate[i].set('isBookmarked', true);
        }

        grid.store.each(function (n) {
            me.updateNodeCls(n);
        });

        grid.resumeEvents();
    },
    getUpdatedActivityData: function () {
        var courseTree = this.query('[xtype=artisan.searchablecoursetree]')[0],
            bookmarkArr = this.bookmark ? this.bookmark.split('|') : [-1, -1, -1, -1],
            me = this;

        this.dataChunk.v = {};
        this.dataChunk.vl = {};
        this.dataChunk.vp = {};

        courseTree.view.store.each(function (n) {
            var visitedMap = {},
                nodeId = 0;

            switch (n.get('type')) {
                case 'sco':
                    visitedMap = me.dataChunk.v;
                    nodeId = n.section.id;
                    if (n.get('isBookmarked')) {
                        bookmarkArr[0] = nodeId;
                    }
                    break;
                case 'lo':
                    visitedMap = me.dataChunk.vl;
                    nodeId = n.section.id;
                    if (n.get('isBookmarked')) {
                        bookmarkArr[1] = nodeId;
                    }
                    break;
                case 'page':
                    visitedMap = me.dataChunk.vp;
                    nodeId = n.page.id;
                    if (n.get('isBookmarked')) {
                        bookmarkArr[2] = nodeId;
                    }
                    break;
            }

            if (n.get('isVisited')) {
                visitedMap[nodeId] = 1;
            }
        });

        return {
            dataChunk: this.dataChunk,
            bookmark: bookmarkArr.join('|')
        };
    }
});

