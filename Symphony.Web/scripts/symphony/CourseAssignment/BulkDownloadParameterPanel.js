﻿(function () {
    Symphony.CourseAssignment.BulkDownloadParameterPanel = Ext.define('courseassignment.bulkdownloadparameterpanel', {
        alias: 'widget.courseassignment.bulkdownloadparameterpanel',
        extend: 'Ext.Panel',
        parent: {},
        activated: false,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                border: false,
                layout: 'fit',
                items: [{
                    border: false,
                    ref: 'parameterPanel',
                    xtype: 'artisan.parametersetoptionspanel'
                }],
                listeners: {
                    activate: function () {
                        if (!me.activated) {
                            me.removeAll();

                            Symphony.Ajax.request({
                                method: 'GET',
                                url: '/services/artisan.svc/parameters',
                                success: function (args) {
                                    var params = args.data;
                                    var parametersFound = [];

                                    for (var i = 0; i < params.length; i++) {
                                        parametersFound.push(params[i].code);
                                    }

                                    var newPanel = new Symphony.Artisan.ParameterSetOptionsPanel({
                                        xtype: 'artisan.parametersetoptionspanel',
                                        border: false,
                                        parameters: params,
                                        parametersFound: parametersFound,
                                        isTrainingProgram: false
                                    });

                                    me.add(newPanel);
                                    me.doLayout();

                                }
                            });
                            me.activated = true;
                        }
                    }
                }
            });

            this.callParent(arguments);
        },
        getParameterOverrides: function () {
            return this.find('xtype', 'artisan.parametersetoptionspanel')[0].getParameterOverrides();
        },
        getData: function () {
            return this.find('xtype', 'artisan.parametersetoptionspanel')[0].getData();
        }
    });

})();

