﻿/// Extends grouping feature to allow controls (clickable icons) to 
/// be added to the header of a group. Clicking on the icons will not
/// expand or collapse the grid.
Ext.define('Symphony.ux.GroupingWithControls', {
    extend: 'Ext.grid.feature.Grouping',
    alias: 'feature.groupingwithcontrols',
    onGroupClick: function (view, rowElement, groupName, e) {
        if (e.target.tagName.toLowerCase() == 'a' || e.target.tagName.toLowerCase() == 'img') {
            return;
        }

        this.callParent([view, rowElement, groupName, e]);
    }
});
