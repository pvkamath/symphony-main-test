﻿Ext.override(Ext.form.FormPanel, {
    getValues: function () {
        var values = {};
        this.getForm().getFields().each(function (item) {
            if (item.isDestroyed) {
                return;
            }

            var key = item.name || item.id;
            if (item && item.getValue && !item.exclude) {
                if (item.xtype == 'datefield') {
                    var val = item.getValue();
                    if (val) {
                        values[key] = val.formatSymphony('microsoft');
                    }
                } else if (item.isComposite && !item.isSimpleComposite) {
                    item.items.each(function (subitem) {
                        if (subitem.name) {
                            if (subitem.xtype == 'datefield') {
                                var val = subitem.getValue();
                                if (val) {
                                    values[subitem.name] = val.formatSymphony('microsoft');
                                }
                            }
                            else {
                                values[subitem.name] = subitem.getValue();
                            }
                        }
                    });
                } else if (item.isSuperBoxSelect) {
                    var val = item.getValue();
                    if (val && val.length && val.length > 0) {
                        values[key] = val;
                    } else if (val && val != "" && val.split) {
                        values[key] = val.split(",");
                    } else {
                        values[key] = val;
                    }
                } else if (item.isDateRangeField) {
                    values[key] = item.getValue();
                    values[key + 'Display'] = item.getDisplayValue();
                } else {
                    values[key] = item.getValue();
                }
            }
        });
        return values;
    },
    isValid: function () {
        return this.getForm().isValid();
    },
    findField: function (name) {
        var field = null;
        this.getForm().getFields().each(function (item) {
            if (item.isComposite) {
                item.items.each(function (subitem) {
                    if (!field && subitem.name == name) {
                        field = subitem;
                    }
                });
            } else {
                if (!field && item.name == name) {
                    field = item;
                }
            }
        });
        return field;
    },
    bindValues: function (values, ignoreDirty) {
        var me = this;

        if (!this.getForm().getFields()) {
            console.warn('BIND VALUES - SKIPPED, NO ITEMS');
            return;
        }
        
        this.getForm().getFields().each(function (item) {
            if (item.isDestroyed) {
                return;
            }

            if (item.isComposite && !item.isSimpleComposite) {
                //item.items.each(function (subitem) {
                for (var i = 0; i < item.items.length; i++) {
                    var subitem = item.items[i] || item.items.items[i];

                    if (subitem.isDirty && subitem.isDirty() && ignoreDirty) {
                        return;
                    }
                    var value = values[subitem.name || subitem.id];
                    if (subitem.xtype == 'symphony.pagedcombobox') {
                        if (subitem.bindingName.indexOf('.') > -1) {
                            var parts = subitem.bindingName.split('.');
                            var defaultValue = values[parts[0]];
                            if (defaultValue) {
                                for (var j = 1; j < parts.length; j++) {
                                    if (!defaultValue) {
                                        break;
                                    }
                                    defaultValue = defaultValue[parts[j]];
                                }
                            }
                            subitem.valueNotFoundText = defaultValue || subitem.valueNotFoundText;
                        } else {
                            subitem.valueNotFoundText = values[subitem.bindingName] || subitem.valueNotFoundText;
                        }

                    }
                    if (subitem.xtype == 'datefield') {
                        var dt = subitem.isUseParseDateLong ? Symphony.parseDateLong(value) : Symphony.parseDate(value);
                        if (dt) {
                            if (dt.getFullYear() != Symphony.defaultYear) {
                                subitem.setValue(dt);
                            }
                        } else {
                            subitem.setValue(value);
                        }
                    } else {
                        if (subitem.setValue) {
                            if (value === 0 && subitem.allowBlank) {
                                subitem.setValue('');
                            } else {
                                if (subitem.isXType('combobox')) {
                                    me.bindValueComboBox(values, value, subitem);
                                } else {
                                    subitem.setValue(value);
                                }
                            }
                        }
                    }
                }
                //});
            } else {
                if (item.up() && item.up().isComposite) return;
                if (item.isDirty && item.isDirty() && ignoreDirty) {
                    return;
                }
                var key = item.name || item.id
                var value = values[key];
                if (item.xtype == 'symphony.pagedcombobox') {
                    if (item.bindingName.indexOf('.') > -1) {
                        var parts = item.bindingName.split('.');
                        defaultValue = values[parts[0]];
                        if (defaultValue) {
                            for (var i = 1; i < parts.length; i++) {
                                if (!defaultValue) {
                                    break;
                                }
                                defaultValue = defaultValue[parts[i]];
                            }
                        }
                        item.valueNotFoundText = defaultValue || item.valueNotFoundText;
                    } else {
                        item.valueNotFoundText = values[item.bindingName] || item.valueNotFoundText;
                    }
                }
                if (item.xtype == 'datefield') {
                    var dt = item.isUseParseDateLong ? Symphony.parseDateLong(value) : Symphony.parseDate(value);
                    
                    if (dt) {
                        if (dt.getFullYear() != Symphony.defaultYear) {
                            item.setValue(dt);
                        }
                    } else {
                        item.setValue(value);
                    }
                } else {
                    if (value === 0 && item.allowBlank) {
                        item.setValue('');
                    } else {
                        if (item.isXType('combobox')) {
                            me.bindValueComboBox(values, value, item);
                        } else {
                            item.setValue(value);
                        }
                    }
                }
            }

            if (item.isDateRangeField) {
                item.setDisplayValue(values[key + 'Display']);
            }

            if (item && item.fireEvent) {
                // just being careful since this affects the entire app - in reality, there should always be an item
                // and it should always be able to fire events.
                item.fireEvent('bindvalue', item);
            }

        });
    },
    // Need a few combobox specific rules to apply
    // as the whole bindingName, valueNotFoundText
    // seems to work differently in Ext4
    bindValueComboBox: function (values, value, item) {
        // 1. Ext 3 used == when looking up records in the store
        // Ext 4 uses === so when we have ids that are true/false
        // we need to map them to 1/0 to match ids that exist
        // in a yes/no store. This can be turned off if necessary
        if (!item.isUseFindExact) {
            if (value === false) {
                value = 0;
            } else if (value === true) {
                value = 1;
            }
        }

        // 2. Set the value as normal:
        item.setValue(value);

        // 3. Now check if it worked the way we wanted it to
        if (values[item.bindingName] && item.valueNotFoundText == values[item.bindingName]) {
            // In this case we have a bindingName property. This means whatever text shows up
            // in the combo box display MUST be equal to valueNotFoundText AND the actual value
            // MUST be equal to the value. 
            item.value = value;
            item.setRawValue(item.valueNotFoundText);
        } else if (item.getValue() != value) {
            // The above should catch all cases, but if not, ensure that the 
            // underlying value always ends up as the value supplied. 
            // Otherwise if this combo isn't changed we will lose
            // the value on save.
            item.value = value;
        }
    }
});

(function(){
    var oldSetValue = Ext.form.ComboBox.prototype.setValue;
    Ext.override(Ext.form.ComboBox, {
        setValue: function(v, forceLoad, selectFirstIfNotFound) {
            var cbo = this;
            if (this.mode == 'remote' && forceLoad && this.store.getCount() == 0) {
                this.store.on("load", function () {
                    if(selectFirstIfNotFound && !cbo.bindingName){
                        var r = this.findRecord(this.valueField, v);
                        if(!r){
                            oldSetValue.call(this, this.store.getAt(0).data[this.valueField]);
                        }else{
                            oldSetValue.call(this, v);
                        }
                    }else{
                        oldSetValue.call(this, v);
                    }
                }, this, {single: true});
                this.doQuery(this.allQuery, true);
            }else{
                oldSetValue.call(this, v);
            }
        }
    });
})();