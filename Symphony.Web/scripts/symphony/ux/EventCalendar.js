﻿Ext.ux.EventCalendar = function(config){
    Ext.apply(this, {
        eventData: config.eventData,
        staticLegend: config.staticLegend,
        prevText: config.prevText || '<img src="/resources/images/famfamfam/resultset_previous.png" />',
        nextText: config.nextText || '<img src="/resources/images/famfamfam/resultset_next.png" />'
    });
    Ext.ux.EventCalendar.superclass.constructor.call(this, config);
};

Ext.extend(Ext.ux.EventCalendar, Ext.Panel, {
    initComponent: function(){
        this.container = this.body.dom.el;
        var now = new Date();
        this.buildMonthView(now.getMonth() + 1, now.getFullYear());
        Ext.ux.EventCalendar.superclass.initComponent.apply(this, arguments);
    },
    refresh: function(){
        if(this._lastMonth || this._lastYear){
            this.buildMonthView(this._lastMonth, this._lastYear);
        }
    },
    buildMonthView: function(iMonth, iYear, suppressEvents) {
        iMonth *= 1;
        iYear *= 1;

        var oDay = new WW.Date.Day(1, iMonth, iYear);

        var now = new Date();
        var iDayNow = now.getDate();
        var iMonthNow = now.getMonth() + 1;
        var iYearNow = now.getFullYear();

        var prevMonth = oDay.oMonth.previous();
        var nextMonth = oDay.oMonth.next();

        var calendarKey = {};

        if (!suppressEvents) {
            var e = {};
            this.fire("beforeBuildMonthView", { sender: this, month: iMonth, year: iYear, days: (new Date(iYear, (iMonth), 0)).getDate(), e: e });
            if (e.cancel){
                return;
            }
        }
        this.clearCalendar();

        var container = this.container;

        var div = Builder.node('div', { className: 'calendarControls' });

        var p = Builder.node('p', { className: 'calendarControlsSetMonth' });

        var span = Builder.node('span', { className: 'calendarControlsMonthPrev' });
        span.innerHTML = this.prevText;
        this.setMY(span, prevMonth.iMonth, prevMonth.oYear.iYear);
        this.attachMove(span);
        this.attachHover(span, 'calendarControlsMonthPrevHover');

        p.appendChild(span);

        var span = Builder.node('span', { className: 'calendarControlsMonth' }, oDay.oMonth.data().longName);

        p.appendChild(span);

        var span = Builder.node('span', { className: 'calendarControlsMonthNext' });
        span.innerHTML = this.nextText;
        this.setMY(span, nextMonth.iMonth, nextMonth.oYear.iYear);
        this.attachMove(span);
        this.attachHover(span, 'calendarControlsMonthNextHover');

        p.appendChild(span);

        div.appendChild(p);

        var p = Builder.node('p', { className: 'calendarControlsSetYear' });

        var span = Builder.node('span', { className: 'calendarControlsYearPrev' });
        span.innerHTML = this.prevText;
        this.setMY(span, iMonth, iYear - 1);
        this.attachMove(span);
        this.attachHover(span, 'calendarControlsYearPrevHover');

        p.appendChild(span);

        var span = Builder.node('span', { className: 'calendarControlsYear' }, iYear);

        p.appendChild(span);

        var span = Builder.node('span', { className: 'calendarControlsYearNext' });
        span.innerHTML = this.nextText;
        this.setMY(span, iMonth, iYear + 1);
        this.attachMove(span);
        this.attachHover(span, 'calendarControlsYearNextHover');

        p.appendChild(span);

        div.appendChild(p);

        container.appendChild(div);

        var table = Builder.node('table', { className: 'calendarTable' });

        var thead = Builder.node('thead');

        var tr = Builder.node('tr', { className: 'calendarRow' });

        oDay.dayOfWeekDescriptions.each(function(dayOfWeek) {
            var th = Builder.node('th', { className: dayOfWeek.shortName.toLowerCase() }, dayOfWeek.shortName);
            tr.appendChild(th);
        } .bind(this));

        thead.appendChild(tr);

        table.appendChild(thead);

        var tbody = Builder.node('tbody');

        // build content rows
        do {
            var tr = Builder.node('tr', { className: 'calendarRow' });

            var oWeek = oDay.week();
            oWeek.each(function(oWeekDay) {
                var className = oWeekDay.data().shortName.toLowerCase();
                if (oWeekDay.oMonth.oYear.iYear < iYear || oWeekDay.oMonth.iMonth < iMonth) {
                    className += ' calendarCellPrevMonth';
                } else if (oWeekDay.oMonth.oYear.iYear > iYear || oWeekDay.oMonth.iMonth > iMonth) {
                    className += ' calendarCellNextMonth';
                } else if (oWeekDay.iDay == iDayNow && oWeekDay.oMonth.iMonth == iMonthNow && oWeekDay.oMonth.oYear.iYear == iYearNow) {
                    className += ' calendarCellToday';
                }

                var td = Builder.node('td', { className: className }, [
                                Builder.node('span', { className: 'calendarDate' }, oWeekDay.iDay)
                            ]);

                var events = this.eventData.findAll(function(event) {
                    return (event.day == oWeekDay.iDay && event.month == oWeekDay.oMonth.iMonth && event.year == oWeekDay.oMonth.oYear.iYear)
                } .bind(this));
                if (events.length > 0) {
                    var linkDiv = Builder.node('div', { className: 'calendarEvent' });
                    events.each(function(_event, index) {
                        //build text lines up to 3
                        if (index <= 2) {
                            if (events.length <= 3 || (events.length > 3 && index < 2)) {
                                var p = Builder.node('p', { className: 'eventLine' });
                                var img = Builder.node('img', { className: 'eventImage' });
                                img.src = _event.icon.path;
                                var textSpan = Builder.node('span', { className: 'eventText' });
                                textSpan.innerHTML = _event.text.length < 8 ? _event.text : _event.text.substr(0, 7) + '...';
                                p.appendChild(img);
                                p.appendChild(textSpan);
                                linkDiv.appendChild(p);

                            } else if (index == 2 && events.length > 3) {
                                var p = Builder.node('p', { className: 'elipsisLine' });
                                var textSpan = Builder.node('span', { className: 'elipsisText' });
                                textSpan.innerHTML = '&middot; &middot; &middot;';
                                p.appendChild(textSpan);
                                linkDiv.appendChild(p);
                            }
                        }
                        //build the Calendar Keys
                        calendarKey[_event.icon.path + _event.icon.title] = _event.icon;
                    });
                    td.appendChild(linkDiv);

                    this.attachHover(td, 'calendarCellHover');
                    this.setDMY(td, oWeekDay.iDay, oWeekDay.oMonth.iMonth, oWeekDay.oMonth.oYear.iYear);
                    this.attachDetails(td, true);
                }

                tr.appendChild(td);
            } .bind(this));

            tbody.appendChild(tr);

            var end = (oWeek[6].oMonth.iMonth != iMonth || oWeek[6].iDay == oWeek[6].oMonth.dayCount());
        } while (!end);

        table.appendChild(tbody);

        container.appendChild(table);

        var legendDiv = Builder.node('div', { className: 'calendarLegend' });
        container.appendChild(legendDiv);
        if(!this.staticLegend || this.staticLegend == '') {
            for (var key in calendarKey) {
                var value = calendarKey[key];
                var p = Builder.node('p', { className: 'calendarKeyRow' });
                var img = Builder.node('img');
                img.src = value.path;
                var textSpan = Builder.node('span', { className: 'calendarKeyItem' });
                textSpan.innerHTML = value.title;
                p.appendChild(img);
                p.appendChild(textSpan);
                legendDiv.appendChild(p);
            }
        } else {
            legendDiv.innerHTML = this.staticLegend;
        }
        
        //if (!suppressEvents) {
            this.fire("afterBuildMonthView", { sender: this, month: iMonth, year: iYear, days: (new Date(iYear, (iMonth), 0)).getDate() });
        //}
        
        this._lastMonth = iMonth;
        this._lastYear = iYear;
    },
    attachHover: function(element, className) {
        this.domEvents.get().observe(element, 'mouseover', function(e) {
            var element = Event.element(e);
            element.addClassName(className);
        });
        this.domEvents.get().observe(element, 'mouseout', function(e) {
            var element = Event.element(e);
            element.removeClassName(className);
        });
    },

    attachMove: function(element, once) {
        if (!once) {
            for (var i = 0; i < element.childNodes.length; i++) {
                if (element.childNodes[i].tagName)
                    this.attachMove(element.childNodes[i]);
            }
        }
        var me = this;
        this.domEvents.get().observe(element, 'click', function(e) {
            var element = Event.element(e);
            var iMonth = element.getAttribute('month');
            var iYear = element.getAttribute('year');
            me.buildMonthView(iMonth, iYear);
            Event.stop(e);
        });
    },
    attachDetails: function(element, once) {
        if (!once) {
            for (var i = 0; i < element.childNodes.length; i++) {
                if (element.childNodes[i].tagName)
                    this.attachDetails(element.childNodes[i]);
            }
        }
        var me = this;
        this.domEvents.get().observe(element, 'click', function(e) {
            var element = Event.element(e);
            var iDay = element.getAttribute('day');
            var iMonth = element.getAttribute('month');
            var iYear = element.getAttribute('year');
            me.showDetails(iDay, iMonth, iYear);
        });
    },
    setDMY: function(element, iDay, iMonth, iYear) {
        iDay *= 1;
        iMonth *= 1;
        iYear *= 1;
        element.setAttribute('day', iDay);
        element.setAttribute('month', iMonth);
        element.setAttribute('year', iYear);
        for (var i = 0; i < element.childNodes.length; i++) {
            if (element.childNodes[i].tagName)
                this.setDMY(element.childNodes[i], iDay, iMonth, iYear);
        }
    },
    setMY: function(element, iMonth, iYear) {
        iMonth *= 1;
        iYear *= 1;
        element.setAttribute('month', iMonth);
        element.setAttribute('year', iYear);
        for (var i = 0; i < element.childNodes.length; i++) {
            if (element.childNodes[i].tagName)
                this.setMY(element.childNodes[i], iMonth, iYear);
        }
    },
    showDetails: function(iDay, iMonth, iYear) {
        alert('details');
//        WW.Util.loadResource({
//            url: "/WebWidgetry/widgets/Calendar/EventDetails.js",
//            callback: function() {
//                this.eventDetails = new WebWidgetry.EventDetails("eventDetails_" + id, "", {
//                    parent: this,
//                    windowOptions: {
//                        title: 'Event Details',
//                        modal: true,
//                        height: 200,
//                        width: 350,
//                        resizable: false
//                    },
//                    events: this.eventData,
//                    day: iDay,
//                    month: iMonth,
//                    year: iYear,
//                    prevText: this.prevText,
//                    nextText: this.nextText
//                });
//                this.eventDetails.load();
//            } .bind(this)
//        });
    },
    clearCalendar: function() {
        var container = this.$('calendarContainer');
        if (container.hasChildNodes()) {
            while (container.childNodes.length > 0) {
                Event.stopObserving(container.firstChild);
                container.removeChild(container.firstChild);
            }
        }
    }
});
Ext.reg('eventcalendar', 'Ext.ux.EventCalendar');