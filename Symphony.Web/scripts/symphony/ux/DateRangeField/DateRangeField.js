﻿// Frozen Mountain Software
// Author: John Moss
// Version 1.0.0

Ext.namespace('Ext.ux.form');

Ext.define('Ext.ux.form.DateRangeField', {
    extend: 'Ext.form.TriggerField',
    alias: 'widget.daterangefield',
    editable: false,
    showToday: false,
    triggerCls: 'x-form-date-trigger',
    isDateRangeField: true,



    //format: "m/d/Y",
    //altFormats: "m/d/Y|n/j/Y|n/j/y|m/j/y|n/d/y|m/j/Y|n/d/Y|m-d-y|m-d-Y|m/d|m-d|md|mdy|mdY|d|Y-m-d",
    //invalidText: "{0} is not a valid date range - it must be in the format {1}",

    //// private
    //defaultAutoCreate: { tag: "input", type: "text", size: "10", autocomplete: "off" },

    //// in the absence of a time value, a default value of 12 noon will be used
    //// (note: 12 noon was chosen because it steers well clear of all DST timezone changes)
    //initTime: '12', // 24 hour format

    //initTimeFormat: 'H',

    //// PUBLIC -- to be documented
    //safeParse: function (value, format) {
    //    if (/[gGhH]/.test(format.replace(/(\\.)/g, ''))) {
    //        // if parse format contains hour information, no DST adjustment is necessary
    //        return Date.parseDate(value, format);
    //    } else {
    //        // set time to 12 noon, then clear the time
    //        var parsedDate = Date.parseDate(value + ' ' + this.initTime, format + ' ' + this.initTimeFormat);

    //        if (parsedDate) return parsedDate.clearTime();
    //    }
    //},

    initComponent: function () {
        Ext.ux.form.DateRangeField.superclass.initComponent.call(this);

        this.addEvents(
            /**
             * @event select
             * Fires when a date range is selected.
             * @param {Ext.ux.form.DateRangeField} this
             * @param {string} date The date tange that was selected
             */
            'select'
        );
    },

    initEvents: function () {
        Ext.ux.form.DateRangeField.superclass.initEvents.call(this);
        this.keyNav = new Ext.KeyNav(this.el, {
            "down": function (e) {
                this.onTriggerClick();
            },
            scope: this,
            forceKeyDown: true
        });
    },

    //getErrors: function (value) {
    //    var errors = Ext.ux.form.DateRangeField.superclass.getErrors.apply(this, arguments);

    //    value = this.formatDate(value || this.processValue(this.getRawValue()));

    //    if (value.length < 1) { // if it's blank and textfield didn't flag it then it's valid
    //        return errors;
    //    }

    //    var svalue = value;
    //    value = this.parseDate(value);
    //    if (!value) {
    //        errors.push(String.format(this.invalidText, svalue, this.format));
    //        return errors;
    //    }

    //    return errors;
    //},

    //// private
    //// Provides logic to override the default TriggerField.validateBlur which just returns true
    //validateBlur: function () {
    //    return !this.menu || !this.menu.isVisible();
    //},

    //getValue: function () {
    //    return this.parseDate(Ext.ux.form.DateRangeField.superclass.getValue.call(this)) || "";
    //},

    //setValue: function (date) {
    //    return Ext.ux.form.DateRangeField.superclass.setValue.call(this, this.formatDate(this.parseDate(date)));
    //},

    //// private
    //parseDate: function (value) {
    //    if (!value || Ext.isDate(value)) {
    //        return value;
    //    }

    //    var v = this.safeParse(value, this.format),
    //        af = this.altFormats,
    //        afa = this.altFormatsArray;

    //    if (!v && af) {
    //        afa = afa || af.split("|");

    //        for (var i = 0, len = afa.length; i < len && !v; i++) {
    //            v = this.safeParse(value, afa[i]);
    //        }
    //    }
    //    return v;
    //},

    // private
    onDestroy: function () {
        Ext.destroy(this.menu, this.keyNav);
        Ext.ux.form.DateRangeField.superclass.onDestroy.call(this);
    },

    //// private
    //formatDate: function (date) {
    //    return Ext.isDate(date) ? date.dateFormat(this.format) : date;
    //},

    _value: null,

    formatDateRange: function (startDate, endDate) {
        return startDate + ' - ' + endDate;
    },

    setDateRange: function(startDate, endDate, displayText){
        this._value = startDate + ',' + endDate;

        if (displayText) {
            this.setDisplayValue(displayText);
        }
        else {
            this.setDisplayValue(this.formatDateRange(startDate, endDate));
        }

        this.fireEvent('select', this, this._value);
    },

    getValue: function() {
        return this._value;
    },

    setValue: function(value) {
        this._value = value;

        // TODO: Eventually it would be nice to auto-set the display value from the encoded value
        //this.setDisplayValue(...)
    },

    getDisplayValue: function() {
        return Ext.ux.form.DateRangeField.superclass.getValue.call(this);
    },

    setDisplayValue: function(value) {
        Ext.ux.form.DateRangeField.superclass.setValue.call(this, value);
    },


    /**
     * @method onTriggerClick
     * @hide
     */
    // private
    // Implements the default empty TriggerField.onTriggerClick function to display the date range pickers
    onTriggerClick: function () {
        var me = this;

        if (this.disabled) {
            return;
        }
        if (this.menu == null) {

            var defaultMenu = this.getDefaultMenuElements();

            this.menu = new Ext.menu.Menu({
                items: [
                    defaultMenu.day,
                    defaultMenu.week,
                    defaultMenu.month,
                    defaultMenu.year,
                    '-',
                    defaultMenu.dateRange,
                    defaultMenu.custom,
                    '-',
                    {
                        text: 'Clear',
                        handler: function () {
                            me.setValue('');
                            me.setDisplayValue('');
                        }
                    }
                ]
            });
        }
        this.onFocus();
        //Ext.apply(this.menu.picker, {
        //    format: this.format,
        //    showToday: this.showToday,
        //});
        //this.menu.picker.setValue(this.getValue() || new Date());
        var trigger = this.triggerEl.elements[0];

        this.menu.showAt(trigger.getX(), trigger.getY(), true);
        this.menuEvents('on');
    },

    //private
    menuEvents: function (method) {
        this.menu[method]('select', this.onSelect, this);
        this.menu[method]('hide', this.onMenuHide, this);
        this.menu[method]('show', this.onFocus, this);
    },

    onSelect: function (m, d) {
        this.setValue(d);
        this.fireEvent('select', this, d);
        this.menu.hide();
    },

    onMenuHide: function () {
        this.focus(false, 60);
        this.menuEvents('un');
    },

    getDefaultMenuElements: function (presetRangeHandler) {
        var me = this;

        if (!presetRangeHandler) {
            presetRangeHandler = function () {
                me.setDateRange(this.start, this.end, this.text);
            };
        }

        var menuElements = {
            day: {
                text: 'Day',
                menu: {
                    defaults: {
                        handler: presetRangeHandler
                    },
                    items: [{
                        text: 'Current Day',
                        start: 'C/C/C',
                        end: 'C/C/C'
                    }, {
                        text: 'Previous Day',
                        start: 'C-1/C/C',
                        end: 'C-1/C/C'
                    }, {
                        text: 'Next Day',
                        start: 'C+1/C/C',
                        end: 'C+1/C/C'
                    }]
                }
            },
            week: {
                    text: 'Week',
                    menu: {
                        defaults: {
                            handler: presetRangeHandler
                        },
                        items: [{
                            text: 'Current Week',
                            start: 'FW/C/C',
                            end: 'LW/C/C'
                        }, {
                            text: 'Previous Week',
                            start: 'FW-7/C/C',
                            end: 'LW-7/C/C'
                        }, {
                            text: 'Next Week',
                            start: 'FW+7/C/C',
                            end: 'LW+7/C/C'
                        }, {xtype: 'menuseparator'}, {
                            text: 'Last 7 Days',
                            start: 'C-7/C/C',
                            end: 'C/C/C'
                        }, { xtype: 'menuseparator' }, {
                            text: 'Week-to-Date',
                            start: 'FW/C/C',
                            end: 'C/C/C'
                        }]
                    }
            },
            month: {
                text: 'Month',
                menu: {
                    defaults: {
                        handler: presetRangeHandler
                    },
                    items: [{
                        text: 'Current Month',
                        start: 'FM/C/C',
                        end: 'LM/C/C'
                    }, {
                        text: 'Previous Month',
                        start: 'FM/C-1/C',
                        end: 'LM/C-1/C'
                    }, {
                        text: 'Next Month',
                        start: 'FM/C+1/C',
                        end: 'LM/C+1/C'
                    }, { xtype: 'menuseparator' }, {
                        text: 'Last 30 Days',
                        start: 'C-30/C/C',
                        end: 'C/C/C'
                    }, { xtype: 'menuseparator' }, {
                        text: 'Month-to-Date',
                        start: 'FM/C/C',
                        end: 'C/C/C'
                    }]
                }
            },
            year: {
                text: 'Year',
                menu: {
                    defaults: {
                        handler: presetRangeHandler
                    },
                    items: [{
                        text: 'Current Year',
                        start: 'FM/FY/C',
                        end: 'LM/LY/C'
                    }, {
                        text: 'Previous Year',
                        start: 'FM/FY/C-1',
                        end: 'LM/LY/C-1'
                    }, {
                        text: 'Next Year',
                        start: 'FM/FY/C+1',
                        end: 'LM/LY/C+1'
                    }, { xtype: 'menuseparator' }, {
                        text: 'Last 365 Days',
                        start: 'C-365/C/C',
                        end: 'C/C/C'
                    }, { xtype: 'menuseparator' }, {
                        text: 'Year-to-Date',
                        start: 'FM/FY/C',
                        end: 'C/C/C'
                    }]
                }
            },
            dateRange: {
                text: 'Date Range',
                menu: {
                    items: [{
                        xtype: 'panel',
                        frame: true,
                        border: false,
                        cls: 'x-panel-transparent',
                        items: [{
                            xtype: 'panel',
                            layout: 'hbox',
                            border: false,
                            cls: 'x-panel-transparent',
                            width: 390,
                            items: [{
                                xtype: 'panel',
                                border: false,
                                cls: 'x-panel-transparent',
                                items: [{
                                    html: 'From',
                                    cls: 'daterange-label x-panel-transparent',
                                    border: false
                                }, {
                                    xtype: 'datepicker',
                                    showToday: this.showToday,
                                    cls: 'static-datepicker',
                                    ref: '../../fromDate'
                                }]
                            }, {
                                xtype: 'panel',
                                border: false,
                                cls: 'x-panel-transparent',
                                items: [{
                                    html: 'To',
                                    cls: 'daterange-label x-panel-transparent',
                                    border: false
                                }, {
                                    xtype: 'datepicker',
                                    showToday: this.showToday,
                                    cls: 'static-datepicker',
                                    ref: '../../toDate'
                                }]
                            }]
                        }, {
                            xtype: 'panel',
                            layout: 'hbox',
                            style: 'margin: 20px 10px 10px 10px;',
                            border: false,
                            cls: 'x-panel-transparent',
                            items: [{
                                xtype: 'panel',
                                ref: 'errorPanel',
                                cls: 'x-panel-transparent',
                                border: false
                            }, {
                                xtype: 'button',
                                text: 'OK',
                                width: 40,
                                cls: 'daterange-button',
                                handler: function (btn) {
                                    var fromDatePicker = btn.ownerCt.ownerCt.fromDate;
                                    var toDatePicker = btn.ownerCt.ownerCt.toDate;
                                    var startDate = fromDatePicker.getValue();
                                    var endDate = toDatePicker.getValue();
                                    if (startDate > endDate) {
                                        btn.ownerCt.errorPanel.update('<span style="color:red;">Start date cannot be after the end date.</span>');
                                        return;
                                    }
                                    btn.ownerCt.errorPanel.update("");
                                    var displayText = startDate.format('M j, Y') + ' - ' + endDate.format('M j, Y');
                                    me.setDateRange(startDate.format('j/n/Y'), endDate.format('j/n/Y'), displayText);
                                    me.menu.hide();
                                }
                            }]
                        }]
                    }]
                }
            }, 
            custom: {
                text: 'Custom',
                menu: {
                    items: [{
                        xtype: 'panel',
                        frame: true,
                        items: [{
                            xtype: 'panel',
                            layout: 'hbox',
                            width: 400,
                            border: false,
                            items: [{
                                xtype: 'panel',
                                cls: 'daterange-dateblock',
                                style: 'border-right: 1px solid #ccc;',
                                items: [{
                                    html: 'From',
                                    cls: 'daterange-label'
                                }, {
                                    // Offset fields
                                    hidden: true,
                                    ref: '../../fromOffset',
                                    xtype: 'panel',
                                    style: 'margin-bottom: 5px',
                                    layout: 'hbox',
                                    items: [{
                                        xtype: 'numberfield',
                                        width: 50,
                                        value: 0,
                                        ref: '../../../fromOffsetValue'
                                    }, {
                                        html: 'days',
                                        width: 45,
                                        style: 'margin: 5px 10px'
                                    }, {
                                        xtype: 'combo',
                                        ref: '../../../fromOffsetType',
                                        listClass: 'x-menu',
                                        store: new Ext.data.ArrayStore({
                                            fields: ['id', 'text'],
                                            data: [['before', 'before'], ['after', 'after']]
                                        }),
                                        value: 'before',
                                        valueField: 'id',
                                        displayField: 'text',
                                        queryMode: 'local',
                                        width: 70,
                                        allowBlank: false,
                                        editable: false,
                                        triggerAction: 'all',
                                        typeAhead: false
                                    }]
                                }, {
                                    xtype: 'combo',
                                    ref: '../../fromDropDown',
                                    listClass: 'x-menu',
                                    store: new Ext.data.ArrayStore({
                                        fields: ['code', 'text'],
                                        data: [
                                            ['C{offset}/C/C', 'Today'],
                                            ['FW{offset}/C/C', 'First Day of the Week'],
                                            ['LW{offset}/C/C', 'Last Day of the Week'],
                                            ['FM{offset}/C/C', 'First Day of the Month'],
                                            ['LM{offset}/C/C', 'Last Day of the Month'],
                                            ['FM{offset}/FY/C', 'First Day of the Year'],
                                            ['LM{offset}/LY/C', 'Last Day of the Year']
                                        ]
                                    }),
                                    value: 'C{offset}/C/C',
                                    valueField: 'code',
                                    displayField: 'text',
                                    queryMode: 'local',
                                    cls: 'dynamic-datepicker',
                                    allowBlank: false,
                                    editable: false,
                                    triggerAction: 'all',
                                    typeAhead: false
                                }]
                            }, {
                                xtype: 'panel',
                                cls: 'daterange-dateblock',
                                items: [{
                                    html: 'To',
                                    cls: 'daterange-label'
                                }, {
                                    // Offset fields
                                    hidden: true,
                                    ref: '../../toOffset',
                                    xtype: 'panel',
                                    style: 'margin-bottom: 5px',
                                    layout: 'hbox',
                                    items: [{
                                        xtype: 'numberfield',
                                        width: 50,
                                        value: 0,
                                        ref: '../../../toOffsetValue'
                                    }, {
                                        html: 'days',
                                        width: 45,
                                        style: 'margin: 5px 10px'
                                    }, {
                                        xtype: 'combo',
                                        ref: '../../../toOffsetType',
                                        listClass: 'x-menu',
                                        store: new Ext.data.ArrayStore({
                                            fields: ['id', 'text'],
                                            data: [['before', 'before'], ['after', 'after']]
                                        }),
                                        value: 'before',
                                        valueField: 'id',
                                        displayField: 'text',
                                        queryMode: 'local',
                                        width: 70,
                                        allowBlank: false,
                                        editable: false,
                                        triggerAction: 'all',
                                        typeAhead: false
                                    }]
                                }, {
                                    xtype: 'combo',
                                    ref: '../../toDropDown',
                                    listClass: 'x-menu',
                                    store: new Ext.data.ArrayStore({
                                        fields: ['code', 'text'],
                                        data: [
                                            ['C{offset}/C/C', 'Today'],
                                            ['FW{offset}/C/C', 'First Day of the Week'],
                                            ['LW{offset}/C/C', 'Last Day of the Week'],
                                            ['FM{offset}/C/C', 'First Day of the Month'],
                                            ['LM{offset}/C/C', 'Last Day of the Month'],
                                            ['FM{offset}/FY/C', 'First Day of the Year'],
                                            ['LM{offset}/LY/C', 'Last Day of the Year']
                                        ]
                                    }),
                                    value: 'C{offset}/C/C',
                                    valueField: 'code',
                                    displayField: 'text',
                                    queryMode: 'local',
                                    cls: 'dynamic-datepicker',
                                    allowBlank: false,
                                    editable: false,
                                    triggerAction: 'all',
                                    typeAhead: false
                                }]
                            }]
                        }, {
                            xtype: 'panel',
                            layout: 'hbox',
                            style: 'margin: 20px 10px 10px 10px;',
                            border: false,
                            items: [{
                                xtype: 'checkbox',
                                ref: 'useOffsets',
                                boxLabel: 'Use offsets',
                                handler: function (box, checked) {
                                    var fromOffset = box.ownerCt.ownerCt.fromOffset;
                                    var toOffset = box.ownerCt.ownerCt.toOffset;
                                    fromOffset.setVisible(checked);
                                    toOffset.setVisible(checked);
                                }
                            }, {
                                xtype: 'button',
                                text: 'OK',
                                width: 40,
                                cls: 'daterange-button',
                                handler: function (btn) {
                                    var fromValue = btn.ownerCt.ownerCt.fromDropDown.getValue();
                                    var toValue = btn.ownerCt.ownerCt.toDropDown.getValue();

                                    var fromRawValue = btn.ownerCt.ownerCt.fromDropDown.getRawValue();
                                    var toRawValue = btn.ownerCt.ownerCt.toDropDown.getRawValue();

                                    var toOffsetDisplayString, fromOffsetDisplayString;

                                    var useOffsets = btn.ownerCt.useOffsets.getValue();

                                    if (useOffsets) {
                                        var fromOffsetValue = btn.ownerCt.ownerCt.fromOffsetValue.getValue();
                                        var fromOffsetType = btn.ownerCt.ownerCt.fromOffsetType.getValue();
                                        var fromOffsetString = (fromOffsetType == 'before' ? '-' : '+') + fromOffsetValue;
                                        fromOffsetDisplayString = fromOffsetValue + ' days ' + fromOffsetType + ' ' + fromRawValue;

                                        var toOffsetValue = btn.ownerCt.ownerCt.toOffsetValue.getValue();
                                        var toOffsetType = btn.ownerCt.ownerCt.toOffsetType.getValue();
                                        var toOffsetString = (toOffsetType == 'before' ? '-' : '+') + toOffsetValue;
                                        toOffsetDisplayString = toOffsetValue + ' days ' + toOffsetType + ' ' + toRawValue;

                                        fromValue = fromValue.replace("{offset}", fromOffsetString);
                                        toValue = toValue.replace("{offset}", toOffsetString);
                                    }
                                    else {
                                        fromValue = fromValue.replace("{offset}", "");
                                        toValue = toValue.replace("{offset}", "");

                                        fromOffsetDisplayString = fromRawValue;
                                        toOffsetDisplayString = toRawValue;
                                    }

                                    me.setDateRange(fromValue, toValue, fromOffsetDisplayString + ' - ' + toOffsetDisplayString);
                                    me.menu.hide();
                                }
                            }]
                        }]
                    }]
                }
            }
        }

        return menuElements;
    }

    //// private
    //beforeBlur: function () {
    //    var v = this.parseDate(this.getRawValue());
    //    if (v) {
    //        this.setValue(v);
    //    }
    //}
});