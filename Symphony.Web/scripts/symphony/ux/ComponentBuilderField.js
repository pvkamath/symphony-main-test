﻿Ext.define('Symphony.ComponentBuilderFieldItem', {
    extend: 'Ext.data.Model',

    fields: [{
        type: 'int',
        name: 'type'
    }, {
        type: 'string',
        name: 'name'
    }, {
        type: 'string',
        name: 'htmlContent'
    }, {
        type: 'string',
        name: 'codeName'
    }, {
        type: 'boolean',
        name: 'showCancelButton'
    }, {
        type: 'string',
        name: 'userFieldName'
    }, {
        model: 'userDataField',
        name: 'dataField'
    }, {
        type: 'int',
        name: 'userDataFieldId'
    }, {
        type: 'boolean',
        name: 'allowBlank'
    }]
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});

Ext.define('Symphony.ComponentBuilderFieldItemStore', {
    extend: 'Ext.data.Store',
    alias: 'store.componentbuilderfielditems',
    model: 'Symphony.ComponentBuilderFieldItem'
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});

Ext.define('Symphony.ComponentBuilderFieldType', {
    extend: 'Ext.data.Model',

    statics: {
        LABEL: Symphony.FieldType.label,
        TEXT_FIELD: Symphony.FieldType.textField,
        DATE_FIELD: Symphony.FieldType.dateField,
        CONFIRMATION_BUTTON: Symphony.FieldType.confirmationButton,
        CHECKBOX_FIELD: Symphony.FieldType.checkboxField,
        SYMPHONY_USER_FIELD: Symphony.FieldType.symphonyUserField, // Field on the user model flagged with CertificateField attribute
        SYMPHONY_DATA_FIELD: Symphony.FieldType.symphonyDataField, // Defined field that is used in the user profile for a 
        PROCTOR_CODE_FIELD: Symphony.FieldType.proctorCodeField
    },

    fields: [{
        type: 'int',
        name: 'id'
    }, {
        type: 'string',
        name: 'name'
    }]
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});

Ext.define('Symphony.ComponentBuilderFieldTypeStore', {
    extend: 'Ext.data.Store',
    alias: 'store.componentbuilderfieldtypes',
    model: 'Symphony.ComponentBuilderFieldItem',
    
    data: [
        { id: Symphony.ComponentBuilderFieldType.LABEL, name: 'Label' },
        { id: Symphony.ComponentBuilderFieldType.TEXT_FIELD, name: 'Text Field' },
        { id: Symphony.ComponentBuilderFieldType.DATE_FIELD, name: 'Date Field' },
        { id: Symphony.ComponentBuilderFieldType.CONFIRMATION_BUTTON, name: 'Confirmation Button' },
        { id: Symphony.ComponentBuilderFieldType.CHECKBOX_FIELD, name: 'Checkbox Field' },
        // any fields on the user model that have been flagged with the "CertificateField" attribute
        { id: Symphony.ComponentBuilderFieldType.SYMPHONY_USER_FIELD, name: 'Symphony User Field' },
        // In symphony this is a data field, in the case of license ui this has been referred to a profile field
        { id: Symphony.ComponentBuilderFieldType.SYMPHONY_DATA_FIELD, name: 'Symphony Profile Field' },
        { id: Symphony.ComponentBuilderFieldType.PROCTOR_CODE_FIELD, name: 'Proctor Code Field' }
    ]
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});

Ext.define('Symphony.ComponentBuilderUserField', {
    extend: 'Ext.data.Model',
    fields: [{
        type: 'string',
        name: 'code'
    }, {
        type: 'string',
        name: 'name'
    }]
}, function (clazz) {
    Log.debug('Class created: ' + clazz.$className);
});

Ext.define('Symphony.ComponentBuilderSymphonyUserFieldStore', {
    extend: 'Ext.data.Store',
    alias: 'store.componentbuildersymphonyuserfields',
    model: 'Symphony.ComponentBuilderUserField',
    constructor: function(config)
    {
        var data = [];
        for (var i = 0; i < Symphony.UserCertificateFields.length; i++) {
            data.push({ code: Symphony.UserCertificateFields[i], name: Symphony.toHumanCase(Symphony.UserCertificateFields[i]) });
        }

        Ext.apply(config, {
            data: data
        });

        Symphony.ComponentBuilderSymphonyUserFieldStore.superclass.constructor.call(this, config);
    }
});

Ext.define('Symphony.ComponentBuilderField', {
    extend: 'Ext.Container',
    xtype: 'symphony.componentbuilderfield',
    mixins: [
        'Ext.form.field.Field',
        'Symphony.mixins.FunctionOwnership'
    ],
    requires: [
        'Ext.grid.Panel'
    ],

    // For allowing only certain ComponentBuilderFieldItems
    // Defaulting to the original five items to ensure that 
    // existing components that use this editor will behave 
    // the same as before. Since the Symphony User/Data fields
    // aren't required for Affidavits or Proctor forms we
    // won't confuse those editors with them. 
    fieldItemFilter: [
        Symphony.ComponentBuilderFieldType.LABEL,
        Symphony.ComponentBuilderFieldType.TEXT_FIELD,
        Symphony.ComponentBuilderFieldType.DATE_FIELD,
        Symphony.ComponentBuilderFieldType.CONFIRMATION_BUTTON,
        Symphony.ComponentBuilderFieldType.CHECKBOX_FIELD
    ],
    isSizeRequired: true, // allow disabling width/height fields
    isConfirmationRequired: true, // allow ignoring the confirmation button requirement
    isShowRequiredColumn: false, // allow setting required fields
    /**
     * {Symphony.ComponentBuilderFieldItem} The currently selected component.
     */
    currentComponent: null,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    assumeOwnership: ['components-grid', 'name-field', 'type-field', 'add-button', 'preview-button'],

    items: [{
        xtype: 'numberfield',
        itemId: 'width-field',
        fieldLabel: 'Width',
        value: 700,
        allowOnlyWhitespace: false,
        allowDecimals: false,
        enableKeyEvents: true,
        listeners: {
            keyup: function() {
                var me = this.up('[xtype=symphony.componentbuilderfield]'),
                    value = me.getValue();

                Log.debug('User changed width to: ' + value);

                me._changed = true;
            }
        }
    }, {
        xtype: 'numberfield',
        itemId: 'height-field',
        fieldLabel: 'Height',
        value: 400,
        allowOnlyWhitespace: false,
        allowDecimals: false,
        enableKeyEvents: true,
        listeners: {
            keyup: function() {
                var me = this.up('[xtype=symphony.componentbuilderfield]'),
                    value = me.getValue();

                Log.debug('User changed height to: ' + value);

                me._changed = true;
            }
        }
    }, {
        xtype: 'container',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },

        defaults: {
            height: 400,
            margin: {
                top: 8,
                bottom: 8
            }
        },
        items: [{
            xtype: 'grid',
            itemId: 'components-grid',
            title: 'Components',
            flex: 3,
            enableColumnMove: false,
            enableColumnHide: false,
            enableColumnResize: false,
            store: {
                type: 'componentbuilderfielditems'
            },
            viewConfig: {
                markDirty: false,
                plugins: {
                    ptype: 'gridviewdragdrop',
                    dragText: 'Drag and drop to reorder components'
                }
            },

            listeners: {
                itemclick: function(grid, model, item, index, e) {
                    var me = this.up('[xtype=symphony.componentbuilderfield]');

                    Log.debug('User clicked a component in the components grid.');

                    if (e.target.tagName == 'IMG') {
                        Log.debug('Click was on an action column item. Canceling.');
                        return;
                    }

                    me.selectComponent(model);
                }
            },

            columns: {
                defaults: {
                    flex: 1,
                    sortable: false
                },
                items: [{
                  xtype: 'actioncolumn',
                    width: 36,
                    flex: 0,
                    cls: 'action-column-spacer',
                    items: [{
                        icon: '/images/bin.png',
                        tooltip: 'Delete the component.',
                        getClass: function() {
                            return 'x-action-col-icon-margin';
                        },
                        handler: function(grid, row, col) {
                            var me = this.up('[xtype=symphony.componentbuilderfield]'),
                                store = grid.getStore(),
                                record = store.getAt(row);

                            store.remove(record);
                            me. deselectComponent();
                            me._changed = true;

                            return false;
                        }
                    }]
                }, {
                    dataIndex: 'type',
                    text: 'Type',
                    flex: 1,
                
                    renderer: function (v) {
                        switch (v) {
                            case Symphony.ComponentBuilderFieldType.LABEL: return 'Label';
                            case Symphony.ComponentBuilderFieldType.TEXT_FIELD: return 'Text Field';
                            case Symphony.ComponentBuilderFieldType.DATE_FIELD: return 'Date Field';
                            case Symphony.ComponentBuilderFieldType.CONFIRMATION_BUTTON: return 'Confirmation Button';
                            case Symphony.ComponentBuilderFieldType.CHECKBOX_FIELD: return 'Checkbox Field';
                            case Symphony.ComponentBuilderFieldType.SYMPHONY_USER_FIELD: return 'Symphony User Field';
                            case Symphony.ComponentBuilderFieldType.SYMPHONY_DATA_FIELD: return 'Symphony Profile Field';
                            case Symphony.ComponentBuilderFieldType.PROCTOR_CODE_FIELD: return 'Proctor Code Field';
                        }
                    }
                }, {
                    dataIndex: 'name',
                    text: 'Name',
                    flex: 1
                }, {
                    text: 'Required?',
                    dataIndex: 'allowBlank',
                    width: 95,
                    xtype: 'checkcolumn',
                    renderer: function (value, meta, record) {
                        var me = this.up('[xtype=symphony.componentbuilderfield]');
                        if (me.isShowRequiredColumn) {
                            switch (record.get('type')) {
                                case Symphony.ComponentBuilderFieldType.TEXT_FIELD:
                                case Symphony.ComponentBuilderFieldType.DATE_FIELD:
                                case Symphony.ComponentBuilderFieldType.CHECKBOX_FIELD:
                                case Symphony.ComponentBuilderFieldType.SYMPHONY_USER_FIELD:
                                case Symphony.ComponentBuilderFieldType.SYMPHONY_DATA_FIELD:
                                case Symphony.ComponentBuilderFieldType.PROCTOR_CODE_FIELD:
                                    return String.format('<input type="checkbox" {0}>', !value ? 'checked="true"' : '');
                                    break;
                            }
                        }
                        return "";
                    },
                    listeners: {
                        checkchange: function (col, rowIndex, checked, eOpts) {
                            var store = col.up('grid'),
                                current = store.getAt(rowIndex);
                            current.set('allowBlank', !checked);
                        }
                    }
                }]
            },

            tbar: [{
                xtype: 'button',
                itemId: 'add-button',
                iconCls: 'x-button-add',
                text: 'Add',

                handler: function() {
                    var me = this.up('[xtype=symphony.componentbuilderfield]');

                    Log.debug('User clicked button to add a new component.');

                    me.addComponent();
                    me.isValid();
                }
            }, {
                xtype: 'button',
                itemId: 'preview-button',
                iconCls: 'x-button-search',
                text: 'Preview',

                handler: function() {
                    var me = this.up('[xtype=symphony.componentbuilderfield]');

                    Log.debug('User clicked button to show a preview.');

                    me.preview();
                }
            }, '->', {
                xtype: 'tbtext',
                itemId: 'error-text',
                text: '',
                style: 'color: red !important; font-weight: bold !important;'
            }]
        }, {
            xtype: 'form',
            itemId: 'properties-pane',
            title: 'Properties',
            flex: 5,
            bodyPadding: 12,
            labelWidth: 160,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            items: [{
                xtype: 'combobox',
                itemId: 'type-field',
                name: 'type',
                fieldLabel: 'Type',
                queryMode: 'local',
                valueField: 'id',
                displayField: 'name',
                forceSelection: true,
                editable: false,
                store: {
                    type: 'componentbuilderfieldtypes'
                },
                disabled: true,
                excludeForm: true,

                listeners: {
                    beforerender: function () {
                        var me = this.up('[xtype=symphony.componentbuilderfield]');
                        if (Ext.isArray(me.fieldItemFilter) && me.fieldItemFilter.length > 0) {
                            this.store.filter({
                                filterFn: function (item) {
                                    if (Ext.Array.contains(me.fieldItemFilter, item.get('id'))) {
                                        return true;
                                    }
                                    return false;
                                }
                            });
                        }
                    },
                    select: function() {
                        var me = this.up('[xtype=symphony.componentbuilderfield]'),
                            value = this.getValue();

                        Log.debug('Type was modified, updating model with new type: ' + value);

                        me.currentComponent.set('type', value);
                        me.updateFields(value);
                        me.isValid();
                    }
                }
            }, {
                xtype: 'textfield',
                itemId: 'name-field',
                name: 'name',
                fieldLabel: 'Name',
                enableKeyEvents: true,
                disabled: true,
                excludeForm: true,

                listeners: {
                    keyup: function() {
                        var me = this.up('[xtype=symphony.componentbuilderfield]');

                        Log.debug('Name was modified, updating model with new name: ' + this.getValue());

                        me.currentComponent.set('name', this.getValue());
                        me.isValid();
                    }
                }
            }, {
                xtype: 'combobox',
                itemId: 'symphony-user-field',
                name: 'userFieldName',
                fieldLabel: 'Symphony User Field',
                queryMode: 'local',
                valueField: 'code',
                displayField: 'name',
                forceSelection: true,
                editable: false,
                store: {
                    type: 'componentbuildersymphonyuserfields'
                },
                disabled: true,
                hidden: true,
                excludeForm: true,
                listeners: {
                    select: function () {
                        var me = this.up('[xtype=symphony.componentbuilderfield]');

                        Log.debug('Symphony user field was modified, updating model with new name: ' + this.getRawValue());

                        me.currentComponent.set('name', this.getRawValue());
                        me.currentComponent.set('userFieldName', this.getValue());
                        me.currentComponent.set('codeName', this.getValue());

                        me.isValid();
                    }
                }
            }, {
                xtype: 'symphony.pagedcombobox',
                model: 'userDataField',
                fieldLabel: 'Symphony Profile Field',
                itemId: 'symphony-data-field',
                url: '/services/customer.svc/user/datafields/0',
                hidden: true,
                disabled: true,
                excludeForm: true,
                editable: false,
                displayField: 'displayName',
                bindingName: 'name',
                valueField: 'userDataFieldId',
                name: 'userDataFieldId',
                listeners: {
                    select: function () {
                        var me = this.up('[xtype=symphony.componentbuilderfield]');

                        Log.debug('Symphony user field was modified, updating model with new name: ' + this.getRawValue());

                        me.currentComponent.set('name', this.getRawValue());
                        me.currentComponent.set('dataField', this.store.findRecord('userDataFieldId', this.getValue()));
                        me.isValid();
                    }
                }
            }, {
                xtype: 'htmleditor',
                itemId: 'html-content-field',
                name: 'htmlContent',
                fieldLabel: 'Content',
                enableKeyEvents: true,
                hidden: true,
                disable: true,
                excludeForm: true,

                listeners: {
                    // See initComponent, have to wait for boxready before binding anything.
                    // Such are the joys of the htmleditor component :)
                }
            }, {
                xtype: 'textfield',
                itemId: 'code-name-field',
                name: 'codeName',
                fieldLabel: 'Code Name',
                enableKeyEvents: true,
                hidden: true,
                disable: true,
                excludeForm: true,

                listeners: {
                    keyup: function() {
                        var me = this.up('[xtype=symphony.componentbuilderfield]');

                        Log.debug('Code name was modified, updating model with new code name: ' + this.getValue());

                        me.currentComponent.set('codeName', this.getValue());
                        me.isValid();
                    }
                }
            }, {
                xtype: 'checkboxfield',
                itemId: 'show-cancel-button-field',
                name: 'showCancelButton',
                fieldLabel: 'Cancel Button',
                hidden: true,
                excludeForm: true,

                listeners: {
                    change: function() {
                        var me = this.up('[xtype=symphony.componentbuilderfield]');

                        Log.debug('Show cancel button was modified, updating model with new setting: ' + (this.getValue() ? 'true' : 'false'));

                        me.currentComponent.set('showCancelButton', this.getValue());
                        me.isValid();
                    }
                }
            }]
        }]
    }],

    initComponent: function() {
        var me = this,
            htmlContentField,
            form;

        me.callParent(arguments);

        form = me.queryById('properties-pane');
        form = form.getForm();

        // remove size fields if not needed
        if (!me.isSizeRequired) {
            me.queryById('width-field').setVisible(false);
            me.queryById('height-field').setVisible(false);

            me.queryById('components-grid').margin = 0;
            me.queryById('properties-pane').margin = 0;
        }

        // Remove required column if not needed
        if (!me.isShowRequiredColumn) {
            var allowBlankColumnIndex = -1,
                grid = me.queryById('components-grid'),
                columns = grid.columns;

            for (var i = 0; i < columns.length; i++) {
                if (columns[i].dataIndex === 'allowBlank') {
                    allowBlankColumnIndex = i;
                    break;
                }
            }
            if (allowBlankColumnIndex > -1) {
                columns[allowBlankColumnIndex].hide();
            }
        }

        // This is a bit tricky to explain, first read the section at the bottom
        // of shared.js, search for "monitor".
        //
        // After reading that, it will be clear that what we're doing here is
        // changing the default monitor selector here so that we can actually
        // grab data from the form fields marked as excludeForm.
        form.monitor.selector = '[isFormField]';

        htmlContentField = me.queryById('html-content-field');
        htmlContentField.on('boxready', Ext.Function.createOwned(me, function() {
            htmlContentField.on('change', Ext.Function.createOwned(me, function() {
                if (!me.currentComponent) {
                    // Occurs on delete.
                    return;
                }

                Log.debug('Html content was modified, updating model with new content.');

                me.isValid();
                me.currentComponent.set('htmlContent', htmlContentField.getValue());
            }, me, { buffer: 400 }));
        }));
    },

    /**
     * Adds a new component to the grid.
     */
    addComponent: function() {
        var me = this,
            componentsGrid = me.queryById('components-grid'),
            component;

        Log.info('Adding a new component to the grid.');

        component = new Symphony.ComponentBuilderFieldItem({
            type: 1,
            name: 'New Component'
        });
        componentsGrid.getStore().add(component);

        me.selectComponent(component);
    },

    /**
     * Marks a component as selected and displays its properties in the properties
     * pane so that they can be edited.
     * @param {Symphony.ComponentBuilderFieldItem} component The component field
     * that is selected in the components grid.
     */
    selectComponent: function(component) {
        var me = this,
            propertiesPane = me.queryById('properties-pane'),
            typeField = me.queryById('type-field'),
            nameField = me.queryById('name-field'),
            htmlContentField = me.queryById('html-content-field'),
            codeNameField = me.queryById('code-name-field'),
            showCancelBtnField = me.queryById('show-cancel-button-field'),
            symphonyUserField = me.queryById('symphony-user-field'),
            symphonyDataField = me.queryById('symphony-data-field'),
            data;

        Log.info('Selected a component to edit.', component);
        me.currentComponent = component;

        typeField.enable();
        nameField.enable();
        htmlContentField.enable();
        codeNameField.enable();
        showCancelBtnField.enable();
        symphonyUserField.enable();
        symphonyDataField.enable();


        data = component.getData();
        Log.debug('Retrieving component data.', data);

        // The html content field is bound to change instead of keydown, so if
        // this is removed, the setValues call can corrupt model data by setting
        // the values of the wrong component.
        
        htmlContentField.suspendEvents();
        propertiesPane.bindValues(data);
        htmlContentField.resumeEvents();

        me.updateFields(data.type);

        
    },

    /**
     * Deselects all components and disables all property pane input.
     */
    deselectComponent: function() {
        var me = this,
            componentsGrid = me.queryById('components-grid'),
            propertiesPane = me.queryById('properties-pane'),
            typeField = me.queryById('type-field'),
            nameField = me.queryById('name-field'),
            htmlContentField = me.queryById('html-content-field'),
            codeNameField = me.queryById('code-name-field'),
            showCancelBtnField = me.queryById('show-cancel-button-field'),
            model = new Symphony.ComponentBuilderFieldItem();

        Log.info('Deselecting all components.');

        typeField.disable();
        nameField.disable();

        propertiesPane.getForm().setValues(model.getData());

        componentsGrid.getSelectionModel().deselectAll();

        me.currentComponent = null;
        me.updateFields(0);
    },

    /**
     * Updates the propety fields that are displayed.
     * @property {Number} type The new type of component, which will determine
     * which property fields are shown.
     */
    updateFields: function(type) {
        var me = this,
            typeField = me.queryById('type-field'),
            nameField = me.queryById('name-field'),
            htmlContentField = me.queryById('html-content-field'),
            codeNameField = me.queryById('code-name-field'),
            showCancelBtnField = me.queryById('show-cancel-button-field'),
            symphonyUserField = me.queryById('symphony-user-field'),
            symphonyDataField = me.queryById('symphony-data-field');

        Log.info('Updating visibility of fields for type: ' + type);
        console.log(type);
        console.log(Symphony.ComponentBuilderFieldType.LABEL);
        switch (type) {
            case Symphony.ComponentBuilderFieldType.LABEL:
                Log.debug('Showing html label fields.');

                htmlContentField.show();
                codeNameField.hide();
                showCancelBtnField.hide();
                symphonyUserField.hide();
                symphonyDataField.hide();
                nameField.show();
                break;

            case Symphony.ComponentBuilderFieldType.TEXT_FIELD:
                Log.debug('Showing text input fields.');

                htmlContentField.hide();
                codeNameField.show();
                showCancelBtnField.hide();
                symphonyUserField.hide();
                symphonyDataField.hide();
                nameField.show();
                break;

            case Symphony.ComponentBuilderFieldType.DATE_FIELD:
                Log.debug('Showing date input fields.');

                htmlContentField.hide();
                codeNameField.show();
                showCancelBtnField.hide();
                symphonyUserField.hide();
                symphonyDataField.hide();
                nameField.show();
                break;

            case Symphony.ComponentBuilderFieldType.CONFIRMATION_BUTTON:
                Log.debug('Showing confirmation button fields.');

                htmlContentField.hide();
                codeNameField.hide();
                showCancelBtnField.show();
                symphonyUserField.hide();
                symphonyDataField.hide();
                nameField.show();
                break;

            case Symphony.ComponentBuilderFieldType.CHECKBOX_FIELD:
                Log.debug('Showing checkbox input fields.');

                htmlContentField.hide();
                codeNameField.show();
                showCancelBtnField.hide();
                symphonyUserField.hide();
                symphonyDataField.hide();
                nameField.show();
                break;
            case Symphony.ComponentBuilderFieldType.SYMPHONY_USER_FIELD:
                Log.debug('Showing symphony user field inputs');

                htmlContentField.hide();
                codeNameField.hide();
                showCancelBtnField.hide();
                symphonyUserField.show();
                symphonyDataField.hide();
                nameField.hide();
                break;
            case Symphony.ComponentBuilderFieldType.SYMPHONY_DATA_FIELD:
                Log.debug('Showing symphony data field inputs');

                htmlContentField.hide();
                codeNameField.hide();
                showCancelBtnField.hide();
                symphonyUserField.hide();
                symphonyDataField.show();
                nameField.hide();
                break;
            case Symphony.ComponentBuilderFieldType.PROCTOR_CODE_FIELD:
                Log.debug('Showing Proctor Code field inputs');
                htmlContentField.hide();
                showCancelBtnField.hide();
                symphonyUserField.hide();
                symphonyDataField.hide();
                codeNameField.hide();

                nameField.show();

                // set the default value on the codename:
                codeNameField.setValue('Code');
                break;
            default:
                htmlContentField.hide();
                codeNameField.hide();
                symphonyUserField.hide();
                symphonyDataField.hide();
                nameField.show();
                break;
        }
    },

    /**
     * Shows a preview.
     */
    preview: function() {
        Log.error('You must override the preview method.');
        // stub, override.
    },

    /**
     * Determines whether the component is in a valid state.
     */
    isValid: function() {
        var me = this,
            componentsGrid = me.queryById('components-grid'),
            heightField = me.queryById('height-field'),
            widthField = me.queryById('width-field'),
            errorText = me.queryById('error-text'),
            hasButton = false,
            err = [];

        height = Ext.Number.from(heightField.getValue());
        width = Ext.Number.from(widthField.getValue());

        errorText.update('');

        if (!width) {
            if (me.isSizeRequired) {
                err.push('The width is invalid.');
            }
            width = 700;
        }
        if (!height) {
            if (me.isSizeRequired) {
                err.push('The height is invalid.');
            }
            height = 400;
        }

        componentsGrid.getStore().each(function (item) {
            switch (item.get('type')) {
                case Symphony.ComponentBuilderFieldType.CONFIRMATION_BUTTON:
                    hasButton = true;
                    break;
                case Symphony.ComponentBuilderFieldItem.SYMPHONY_DATA_FIELD:
                    if (!item.get('dataField')) {
                        err.push('Symphony Profile Fields require a field to be selected.');
                    }
                    break;
                case Symphony.ComponentBuilderFieldItem.SYMPHONY_USER_FIELD:
                    if (!item.get('userFieldName')) {
                        err.push('Symphony User Fields require a field to be selected.');
                    }
                    break;
            }
        });

        if (!hasButton && me.isConfirmationRequired) {
            err.push('You must add a confirmation button.');
        }

        if (err.length == 0) {
            return true;
        } else {
            errorText.update(err[0]);
            me.markInvalid(err);
            return false;
        }
    },

    /**
     * Gets the JSON representation of the current components.
     */
    getValue: function () {
        var me = this,
            componentsGrid = me.queryById('components-grid'),
            heightField = me.queryById('height-field'),
            widthField = me.queryById('width-field'),
            items = [],
            height,
            width,
            cfg;

        height = Ext.Number.from(heightField.getValue());
        width = Ext.Number.from(widthField.getValue());

        Log.debug('Getting value.');

        if (!width) {
            Log.error('Invalid width, defaulting to 700');
            width = 700;
        }
        if (!height) {
            Log.error('Invalid height, defaulting to 400.');
            height = 400;
        }

        cfg = {
            width: width,
            height: height,
            items: items
        };

        componentsGrid.getStore().each(Ext.Function.createOwned(me, function(item) {
            var type = item.get('type'),
                item;

            switch (type) {
                case Symphony.ComponentBuilderFieldType.LABEL:
                    item = {
                        _name: item.get('name'),
                        xtype: 'component',
                        html: item.get('htmlContent'),
                        type: type,
                        margin: {
                            left: 10,
                            bottom: 10
                        }
                    };

                    Log.debug('Adding label: ', item);
                    items.push(item);
                    break;

                case Symphony.ComponentBuilderFieldType.TEXT_FIELD:
                    item = {
                        _name: item.get('name'),
                        xtype: 'textfield',
                        name: item.get('codeName'),
                        fieldLabel: item.get('name'),
                        type: type,
                        allowBlank: item.get('allowBlank') === false ? false : true,
                        anchor: '-10'
                    };

                    Log.debug('Adding text field: ', item);
                    items.push(item);
                    break;

                case Symphony.ComponentBuilderFieldType.DATE_FIELD:
                    item = {
                        _name: item.get('name'),
                        xtype: 'datefield',
                        name: item.get('codeName'),
                        fieldLabel: item.get('name'),
                        type: type,
                        allowBlank: item.get('allowBlank') === false ? false : true,
                        anchor: '-10'
                    };

                    Log.debug('Adding date field: ', item);
                    items.push(item);
                    break;

                case Symphony.ComponentBuilderFieldType.CONFIRMATION_BUTTON:
                    var showCancel = item.get('showCancelButton');
                    item = {
                        _name: item.get('name'),
                        _showCancelButton: item.get('showCancelButton'),
                        xtype: 'container',
                        margin: {
                            top: 6,
                            bottom: 6
                        },
                        layout: {
                            align: 'middle',
                            pack: 'center',
                            type: 'hbox'
                        },
                        items: [{
                            xtype: 'button',
                            itemId: 'confirm',
                            cls: 'x-btn-large',
                            text: item.get('name'),
                            minWidth: 90
                        }],
                        type: type
                    };
                    
                    if (showCancel) {
                        item.items.unshift({
                            xtype: 'button',
                            itemId: 'cancel',
                            cls: 'x-btn-large',
                            text: 'Cancel',
                            minWidth: 90,
                            margin: { right: 12 }
                        });
                    }

                    Log.debug('Adding confirmation button: ', item);
                    items.push(item);
                    break;

                case Symphony.ComponentBuilderFieldType.CHECKBOX_FIELD:
                    item = {
                        _name: item.get('name'),
                        xtype: 'checkboxgroup',
                        labelPad: 12,
                        margin: { left: 0 },
                        allowBlank: item.get('allowBlank') === false ? false : true,
                        items: [{
                            xtype: 'checkbox',
                            name: 'confirm',
                            name: item.get('codeName'),
                            boxLabel: item.get('name')
                        }],
                        type: type
                    };

                    Log.debug('Adding checkbox field: ', item);
                    items.push(item);
                    break;
                case Symphony.ComponentBuilderFieldType.SYMPHONY_USER_FIELD:
                    item = {
                        _name: item.get('name'),
                        name: item.get('codeName'),
                        fieldLabel: item.get('name'),
                        userFieldName: item.get('codeName'),
                        codeName: item.get('codeName'), // to deserilize on server with code name
                        displayName: item.get('displayName'), // to deserialize on server with display name. 
                        isSymphonyField: true,
                        anchor: '-10',
                        type: type,
                        allowBlank: item.get('allowBlank') === false ? false : true
                    }
                    Log.debug('Adding user field: ', item);
                    items.push(item);
                    break;
                case Symphony.ComponentBuilderFieldType.SYMPHONY_DATA_FIELD:
                    var dataField = item.get('dataField'),
                        cfg;
                    
                    if (dataField) {
                        item = {
                            _name: dataField.get('displayName'),
                            name: dataField.get('codeName'),
                            codeName: dataField.get('codeName'), // to deserilize on server with code name
                            xtype: dataField.get('xtype'),
                            displayName: dataField.get('displayName'), // to deserialize on server with display name. 
                            fieldLabel: dataField.get('displayName'),
                            userDataFieldId: dataField.get('userDataFieldId'),
                            isSymphonyField: true,
                            anchor: '-10',
                            validator: dataField.get('validator'),
                            config: dataField.get('config'),
                            type: type,
                            allowBlank: item.get('allowBlank') === false ? false : true
                        }

                        Log.debug('Adding user data field: ', item);
                        items.push(item);
                    } else {
                        Log.error('dataField property was not set for item: ', item);
                    }
                    break;

                case Symphony.ComponentBuilderFieldType.PROCTOR_CODE_FIELD:
                    item = {
                        _name: item.get('name'),
                        xtype: 'textfield',
                        name: 'Code', // this filed name is used internally in the Proctor code validation. 
                        fieldLabel: item.get('name'),
                        type: type,
                        allowBlank: item.get('allowBlank') === false ? false : true,
                        anchor: '-10'
                    };
                    items.push(item);
                    break;
            }
        }));

        value = Ext.encode(cfg);
        Log.debug('Calculated JSON value is ' + value);

        return Ext.encode(cfg);
    },

    /**
     * Sets the current components based on a JSON value.
     */
    setValue: function(value) {
        var me = this,
            componentsGrid = me.queryById('components-grid'),
            heightField = me.queryById('height-field'),
            widthField = me.queryById('width-field'),
            cfg;

        Log.debug('Assigning value: ', value);

        cfg = Ext.decode(value, true);
        if (!cfg) {
            Log.error('Value should be JSON.');
            return;
        }

        if (!cfg.items) {
            Log.error('Value is in an invalid format.');
            return;
        }

        heightField.setValue(cfg.height);
        widthField.setValue(cfg.width);

        Ext.Array.each(cfg.items, Ext.Function.createOwned(me, function(item) {
            var model = new Symphony.ComponentBuilderFieldItem({
                name: item._name ? item._name : item.fieldLabel
            });

            // 2016/10/20 NOTE: We may want to switch on item.type instead, as this is the enumerated 
            // type we want anyway and allows better component reuse, i.e., in the case of 
            // the proctor code which is a strict super of textfield
            switch (item.xtype) {
                case 'component':
                    Log.debug('Adding label:', item);

                    model.set('type', Symphony.ComponentBuilderFieldType.LABEL);
                    model.set('htmlContent', item.html);
                    break;

                case 'textfield':
                    Log.debug('Adding text field:', item);

                    model.set('type', item.type);
                    model.set('codeName', item.name);
                    model.set('allowBlank', item.allowBlank);
                    break;

                case 'datefield':
                    Log.debug('Adding date field:', item);

                    model.set('type', Symphony.ComponentBuilderFieldType.DATE_FIELD);
                    model.set('codeName', item.name);
                    model.set('allowBlank', item.allowBlank);
                    break;

                case 'container':
                    Log.debug('Adding confirmation button:', item);

                    model.set('type', Symphony.ComponentBuilderFieldType.CONFIRMATION_BUTTON);
                    model.set('showCancelButton', item._showCancelButton);
                    model.set('allowBlank', item.allowBlank);
                    break;

                case 'checkboxgroup':
                    Log.debug('Adding checkbox field:', item);

                    model.set('type', Symphony.ComponentBuilderFieldType.CHECKBOX_FIELD);
                    model.set('codeName', item.name);
                    model.set('allowBlank', item.allowBlank);
                    break;
                default:
                    // Assume component label
                    Log.debug('Adding default label:', item);

                    model.set('type', Symphony.ComponentBuilderFieldType.LABEL);
                    model.set('htmlContent', item.html);
                    break;
            }

            if (item.isSymphonyField) {
                Log.debug('Found symphony field:', item);

                model.set('type', item.userDataFieldId ?
                                        Symphony.ComponentBuilderFieldType.SYMPHONY_DATA_FIELD :
                                        Symphony.ComponentBuilderFieldType.SYMPHONY_USER_FIELD);
                
                // Since details of the symphony field are dependent on the field selected, we
                // need to add all the data to the model so on save without selecting a new
                // type, we retain the data. 
                model.set('codeName', item.name);
                model.set('userFieldName', item.userFieldName);
                model.set('userDataFieldId', item.userDataFieldId);
                model.set('xtype', item.xtype);
                model.set('fieldLabel', item.fieldLabel);
                model.set('name', item.fieldLabel);
                model.set('allowBlank', item.allowBlank);

                // Store all info that is embeded in the item based on the type of symphony field selected
                var dataField = Ext.create('userDataField', {
                    codeName: item.name,
                    displayName: item.fieldLabel,
                    userDataFieldId: item.userDataFieldId,
                    validator: item.validator,
                    config: item.config,
                    xtype: item.xtype
                });
                model.set('dataField', dataField);

            }

            componentsGrid.getStore().add(model);
        }));
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});