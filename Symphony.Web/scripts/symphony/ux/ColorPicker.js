﻿Ext.define('Ext.ux.ColorPicker', {
    extend: 'Ext.panel.Panel',
    xtype: 'colorpicker2',
    frame: true,

    layout: 'hbox',
    padding: 16,
    maxWidth: 570,

    statics: {
        /**
         * Forces a field's value to be between min and max.
         * 
         * @param {Ext.form.field.Base} field The field to bound.
         * @param {Number} min The minimum allowable value.
         * @param {Number} max The maximum allowable value.
         */
        boundField: function(field, min, max) {
            v = field.getValue();

            field.suspendEvent('change');

            if (v > max) {
                field.setValue(max);
            } else if (v < min) {
                field.setValue(min);
            } else if (v == null || v.length == 0) {
                field.setValue(min);
            }

            field.resumeEvent('change');
        }
    },

    listeners: {
        boxready: function() {
            this._rendered = true;

            if (!this.getHex()) {
                this.setValue('#ffffff');
            }

            this.addMouseListeners();

            // We set the value usually before the component is rendered, so we can't update these
            // widgets until the component is actually drawn.
            this.updateHsvWidgets();
            this.updateSwatch();
        }
    },
    items: [{
        xtype: 'container',
        style: 'position: relative',
        width: 260,
        height: 264,
        padding: 8,
        style: '-webkit-user-select: none; -moz-user-select: none; -o-user-select: none; -khtml-user-select: none; -ms-user-select: none;',

        items: [{
            xtype: 'component',
            itemId: 'swatch',
            width: 256,
            height: 256,
            style: 'position: absolute; left: 4px; top: 4px;'
        }, {
            xtype: 'image',
            itemId: 'saturation-value-widget',
            width: 256,
            height: 256,
            style: 'position: absolute; left: 4px; top: 4px;',
            src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAPPlJREFUeNrsXVuSG8kNRG3MwX1J+wj2h8N22N6HtHqUPyxOFEEkkEChm5S2O0IxM2R3kzNiApkJFGrMOaeITPn/4X2NHpvEc5nHmOeO/Pd145zM48xjlXOe/XP1nK6/a+WcI/5lP9vRYwzevMfuvv4k13Ed1/GHPd7+TwCu4+zjhf7uQ32Vjp+//X7RNZnH7h5Xf7/B/p7s3907Dz2nH0c/r4+jx9Z/t8e+fv169/3t5y9fvsjnz59FRN6//v777/Lbb7+JiMh///tfERH5+9//LiIif/nLX0RE5E9/+pNcDOA6ruNiAFcmTmZLOlM1ZcDwnDlnNWPPA/9eLfdgzo8YgfH3gfdmMzmb1dev6LH1ua9fvz5keZ3x16z/6dMn+f3330VE5OPHjyIi8ttvv8nPP/8sIiL/+te/RETkb3/7m4iI/PnPf35/rxcDuI7ruBjAD5vFx8ZtrWvnSe/7zPtTHkCBYTDXoMxMaf3lOX3+3M3o1v29zI6yOtL1q56//bwygNu/L1++3GX9m87//PmzfPr06V3v3xjATff/8ssv79r/n//8p4iI/PWvf73zAi4GcB3XcTGA12cABcf3kIx9ciZff89hvEZa84O/4+z43Yjzhpft0f+x9biRwQeh4weZ7a2MPplMf6t8WI/r7O/p/S9fvrxn/5UBrE7/p0+f3hnAx48f5cOHDw8M4D//+Y+IiPzjH/+48wJuzOBiANdxHRcDeB0GYGm6Z2X1hsxOOf5OVpxHvEeHSbCMAWXj6Heb0XsOtPoATOjudZysPohM/6DZrYxvZfXlsamz/HLOuGV6S+9rl9/S/SLynv1X5//GAH799dd3BvDvf//7LuPfqgK3c58aAAKwzye9lyyovXLf3PnbJg08BpDI1JuV17fAGf3fAqruNhAt95pMY40OXsvXEQHfAzqi/Na/G8ANmj812NegoA2/NQB8+vTpoey3BoAb9b99/fnnn+WXX355Dwbrc7dS4SUBruM6LglwXrIFmeGVKP0IsvtslgnI/ILZW/0NZ/V3Nf4v6LKf0eY7k7TeNTgdSj8AA3goAwZ0flQzP2raiRjASv1vJt+35+by81gNP8v4WxnALZNbEuDXX399z/i3527n3+55MYDruI6LAZySgHWUP/xFi81B5YxK/v6RMTgzrxWUBr2MvWbBlAew3Kui7wf6PZkGHP050gwgaMtdfYHpZP8RmHww268/e0afZfh9/fpVPn/+PFcGICKDZQDaA/jw4cND5r95BisDOCwAsFT1YMCXDcadNQIGGHYDS9gH8O01K69jBickAZKddmYQImj9WF/HAfbwAoAC7mBo/i0mKvAPJAN0EEB9/Cv1Zx3/bwFh6ABwMwBvgP7w4cNdMLh91cC/Xb8YkJcEuI7ruCTAsZT/sOxPdMfNjXsxjGJYGXFXljAMglivztbtZ5DF3eudnx/kBcjgkBVY56vsPkHWHxYDSNbz13/TYgQo86OVfGu5j2QA8unTp7lk74EkgM72v//++51kuL3W+vWIAODqygNAfyTgh33Z7AxQtw9TpR030visMz8Q5X/E72TuE3oAqLbv1ewNHQ8fB627bosu0viA8k+1qGdY4PcCwA2Et59vrr8RAN5BfAsGIjI+fvx4B/b1++X8O81/+7pKgM4AME4C/jgJ8ClDznmP6XtapTLj+ckGiUDjw/ekfp/IkKOyvZfVvcweaP1BlPQesngG9J7RdwsIt2CwPn/LuCgArNn/BtDb91bzzzeQTxEZFtjXrxr4t9dc/6aXB3Ad13F5AK+Z/Sur16q6m8yoEtDkyr3Qaji3d95r5FkyHLwHuh5keK9sNwDdf7gHS/edTD83GIB28qc6bzAuP9L7C/WfKxNYJcC6yk/P80Pr/dcZf2t2//jx41w9AYvu68y/vp+WAHBSCbGL7m8ZawBksxp0PJOUoPNZE284pp0XrIZD+aOa/kDXaOlAmH0PINWBQQ3gGEQA0B7AVB18I1Pu09p/CQQiImOVAZYEiDwA7QestfwV7OtXbfqtQ0guCXAd13Ed2xKg3fVXhtUu3Q8ZBEPPo4wfSBZolLHXWk0+yTKdJycGWIE3EG0PGAHsxxe7B3+QVH84VB9WAfS1iX9Tu/wWCwDZXzf7vMuCVQZoBmCZgMjU+/z5800GiKiOwfW+FvVf/192AsB4UeB3gZ76/YBcGUI4/qg9mGjPHV5wIhbgsBTeA+9Er4H0/bw3BFwPgKX53z7YsMtP03sJuvoA5X+4ngG/MeHnLhBoD2AF7w3wlqZfJYKI3PkB1gxBTwI83QPYaRnOmoSkIVgt1U32ddC6CKT9rcYgxtQjy3TIxLPA6t0fMYYRgH2itfvI7NMtu0Dz6001hi4HSq3Fd1rDPbxmH20Afvny5Q6wlgegH9MrA/X5t2M9H2X+rQDQafw1Zf0q8LPgtVx/ipZ7mR1c4zrwyKQjga6vRSYiauKZQA6gRqCJKH+U7Q3zD2X6sMa/BIB3UETBQLv8KvPfuf0kA1j/zdUgRABHpp5mDFYAQJn/MgGv4zquoywBWow/q1Z9QuYvZf3IBHTGY01i5xmXJQTjtczfAWj+GZh5kcYfiNqL3bk3DCo/jPfxwABWTb/efB2aobL5QIzgdv7KAECmn6jXP1P3t2b6GxJgzdTzZuJpA8/K+uv3K8X/8OHDXD8nKwNZWU9bFaDBO6iCfwf4lQadmQhG0SXDVgHx1BxC97u9+4axZ5qATg3fCiDD0e3TCggE3Tc9gMXtn5Y0cPr6J9MHYJl+lvtP1P0H8gCscd/fQD5XY9CSAPp7HRA0uPU8gm0PoHvRUJZBsEGDNQaJBTaTeC+DYBP0OnplyEWluxGwBres55mAjs7XmXs6rAAO4VBZPHL8H85F56GxXXoBjxir+YDxd2f4ke7/XMt9OgAswz90tn8wBnXHoOUp6Gy/BgAP+BUG0EH9q/cYyTp8FfhRBcBc5uxJjajTTrh6vbuKLuH6D+N719RzSnhWyY81/KbKTmPH7f8mGSJDb2hWIE59X8sDa6GP829+A+eIGIBaDPRgDCJTTweY2/erFEAB4DIBr+M6rqPmAZzcOEQxhmLmZ2v/FQo/CQ/DMwkZY9BsBAp6+Sf6myndPS1GYDEMpNuBvh+gtj8trwDpeX0fXeIDht6IjL9owAdb+lsy9F1G10wA0f2bHEA6f11XgOi+Zf7tSoCdAFDZKKPSice69EwVgB2ayRiAQ3sYAYWPwMyafN59UB/AQC5/4PCHpp828Ay67+n/9dy7gGE5/LoiwFYBNM0Hgz6mqI5A3QCkNPrUtB5VCBbDb4paUag1vxUAVi8AAf8pjUAHgp/N+izw3dc32nFnxvxztruKsjNTxpvR4ExLp2eeNxjCXZOOPscBewh0Q9dbbv+DY4+m+OhzwDLf6U35Yct/xuDPibS9p+kto88LAOzWZ9kAsNP3n702C34262e69ibReecBfzhdfyPI4nfBysnyMIM3Az5iCLBH3zD9hlXaQ0D36L8hARgT0Fzgg4w/Ae2+FhvQwNcZ/bYWYL2nxwB0ZSAKAJrur+sDvEBwShkwU2loBj9T/2cqACPwANzgocqGaXrvBR5juGbk6mcAPwz9/pC5kcOvZcJNE2v6rqVE0PDzwD4Mem+5/Lr0Nwntz7r/UzMBpw/gjtpbo8KN/QNNhrBmfabkt2UCntAxyDKFQXTlsY0/TL3dkxdR8GB0/fDofwBqL2BAyh5l+NWcA9nfbMrRgCXMPcQMRtTNp/yC4S3pvQUaUPefAqb86k09ke7XkgDV6a3FQChg3P6tpUH9vN6eDAUALyhcZcDruI6rDPhU828EuptmCZ40KNL+3cwfle+mJxVQ116QuSNG4GV8/TsNp9HngbojY3Bdk691KWjiCTP9yjbWjj2HCQw0288z/lj3X5X+hrUaUJ17l9m9MeLa1bckQJUBHBUAqIEY2UDRAP5M6+5k5Afo2JsbHsDM6nd9kaHf2YBw13Zruf2opGdJDiuIeLIABQpF1y0DbyDtf/v35csXpvtvRnP+URBQ5b9pUXYP2FEA+LYnwMPfBJmAziSnc0zAxE47TKDoAj9lBlYzOnMt6QEwmd762w1rNV8QEIbu7QdtvNOp8U9nUs9Ek34YD0CB2RzqkTEBWfNP/IEfd0HAyvC3c6IAgCoDVpZHAYM1ASsMIG3kZeg8cR5zv5RRF7ECJqOjoEPO4wuztlH+M4OM4eIjB9/K9BHl9zr9YI3fyf4m4CMJoOb7DUTvGRPQ2t3Hof2u8TfnfO/3d7oBTZqvwW41/VhZfjUEvU1Ob1+/bSCik1ReAlT0f+d9Nxp2mKzv0fnJlAWNrB4GDHL+PiztEWW9FANgKb+h76cOEMjlX68BC3/uAA0kwDDAYWZ1zwNY9b+A7j+LDejzdAZnaH5Uw7e2F1uzvAV6NuOfEgC6sn9wwg74Iy9gsFIgat4h9L3HIFijz/QJnIx+xwDQ+YDyow4+c50+CATrh9rStqipZ1To/fo6zJw/a+iHgF4AKwis11s036L7VmkQ6Xyr9Md0/ZUbgZr1PJXZI9ov/rSdCPxhYEiae2H2Thp3aBrPRMwBZXSPAVgSIkn5LdCv70fP4bNMvOH19q/3Qkt6PQ9gAeRAtJ/s/oOZ3mMCN7DengezCszsjpz+myEYVQFekQFQmn7jecYPmJEcYNhAsLhnOMGClQBRIw+6D6PpHxgAyugrKBHlt7K8Kt9pbT9VteGBrmv672h91wNYQDk00JH2Fzz6G/b/6yCwUnYUJNbnLM/AChRWJrcCAIvLt5O0evrc7C+SDQyECUhXBaLJvIBuuxJAAxCszEPVgmG5+rsMQJl3qAyIXP9p+QRa12tNHz2OKLyxwcdgVv+hcmDU+qu1vKXfke7XYI+uYWv+pzMAwtGfBEOoZP8I/BVGQNX5IxlgZWVgCAwwkHM4JiAq21kAh+8NOfaeSajBDZzpqYOG1cev3+/KNsCafvZxas5/xAY0yBFTsPwC7errHgH9/lbPgHH6Edhb1wI0mXpUBi9k+G7wwyzuGHwUEwhq95PoAzCzMgtwRNmNzF0JEGbvvuCRXtOZ8xfW9dWOPdbOv0NpaW/Q53Saf6x7Rc89gBz1CGipoasBKABYjUFsA9BRJmCY2Q+i96lWXkIOsFLAc/DpQAHYxIxKfoyLrwG6yQCiqoBVERiIESCweoC3yoDOgh6TJVig9Xb8QUzAcvKtIGDRfF0etIzBlQVEOp+tAjxbAlQGcoY9945Gn0xQIJz88P5OEw9st2UB7bAAS4NbkgCagogBWABH1wYO/zRW800tAfTv4lF9WVqF9Vp/sdf43x4fFthRyc8o97nBxwK5/psYUuCBkXiGH6L8ycR4TADoOCfJDLKSIHTmGcfe0e4zyNhQArAgd0zASPO7AYIIDiaY1bXW/bwe/akbgZbrTUagOwT16wQm4UAtv9byX6tnwGECiFXA0p7V2hvpfS8AdDMAtgWYLut5II7KcplGnUZGwLCB4ey4A0uDTs1/OE0+aUB7Lj7JAKw6v9X5F7IELxAYjT3QBAOz/AYq/2nzzZr9DxqApicHrMVG1n0sU481/NbmIOt53RqMDq8l+K2gx9uzezL7p+r1O4zA689vkAFHZ3wdWNBiH4riW46+rhJELMFy95dtvSa6V7TsN/IAbkahp/9ROTBy+iPmsH5Okd5HAPeyfNT+21IGPDgAVLN/S1DwavVMow/S71EwQK8RbLXt6Xkm4yNARybfVI1AE83780xAi9oDxz9iCF4Z0AT3wgZGsPYftQqHxqOl4fXPmiEwhh8KABkf4JkBgN1th3HiqVLhJvhhyY4ANVvuY+g+vH9DxqcZgAStvQQjsMBTkgBOx9/UgQEM/LQ8gMkGAUT7rTKe1QdgyRBG72uazyTEbEdgSwDYyf4HUf/UuVFACNp44bVM0AB+gNXnr1kAlfG9sh5gAKYnAEw/q6X34RyjD8CUBYwEQIFB0WvKA2CDgFMKdP0Ai/pbXX1I76Ms70mAqO7fyQBGIbtXQZ5hC+lAQWr7MKNvaH0vOKCAEZp2Yrf2TovCI0aw0nfP9DNW9T20/qJzgsoADAwWkB3qPjzAZ4KA9xhq2UXBwaP7XiPP+vw6PjwrBZ7FALI1zAzQZQf8hPY2zytofVT3hwafk+FLGT/DCKzefyvbq7q3aRSqLjgm05tlQOu+mgksi3cG04wUVQGsDG0FjCijswGgqvNbGoE2AkA06npWjD8C7GygYMEfGYWhJ0Bm+UHS/SjDuxk/+hkwALPsZzEE1OYrj22/E6wQfAB0kIEnuRZAewBWyS7FDjzzDy3scZjQQ3OQ9gdQQNGYyAaMwxnAZvZ3O/S6Hwv+eLDmn2AC1mIiq57/sBiI6PEv/5xhANbzkSxAK/+QSeg0EA0LqJFDH+0HwD6GyoDBtYIMRFbvV4D9MhJgIwCEHXrsYxU2gFbkeVk9YBAPQC7Se8Q+BlkCnMFIr/B8xBCMQDKQKejIBC9QDEvfoz6ANcvvmICVYGFl72wAiBb2sCPAjzIBsyCvyAKW+suBASEq3zGlQQv8riSwWAJzvnF95PKHGR6ZfF42l8eWXghqQ4cPwA5gfwDhJYyK/o/AbQEYNQIhuh8FDAvQXSxgKwBEtf8soBuz/1ZACJz+B93tVAeiLB85/BSd1y69+rnMCIzndUnPze6ALYSgdhp+TADrUWEKvC57yGbz6BwEXnQ/L9vrx3VPwG73bTUAZEZ/V4Ee3qcrIDhsIKL4aHOPGWzXnXH4EXh1o5I1Vy9kBI0Z33pOywKrl4DZ1UezBWbox4NEWAMNa+7pbF4JFOh7ZO6xONKfq2xwOIoBhGW8LKVngZ2o4TNsIAS/ASqXshuZOzQGEb237kVk+YrrP4myX7Tib+hsv75/IAFQxyACMaT2lgeAyoGGeVgCvK71MxQ+ag+OgkDmaOsEPNn8o135ipmXMfeMzM3qfXN6TxQoInAvgPayvskQrCxuMIDplAE92j8QsC397AUFoM3njgfgBQ4vUDBSwKLyuiqC6D6zvLerKvC2rlneDADZqT/WVl7ulJ0A2EKcM52avpmZHbNvWNRallVty8leq68YPfjD+PmBKjLTYZ3NPIdakz5UNhoZKqt+HkvQGGu9Xgcj67qv/0ezqGtlzvmTCjDz2w49633X9QBzGRv207fv5/I6U00Cmus5y/N3P6vpwO+PrfdQc/7n+nprAF4fWwL4nCrar4/dOv+Wz9M01qM83GN9+vb1DAawQ//Rmvlw918C/FEPf5klOHMA0FBR8TI+8ABEZ3uDZXg0H5p8gPK7hp4C8x0rcWTDw3UWA0C7/QQOP5oQVKL/FVMQ9QFEpTuv+WenWvdMDyAMChLv9EODndDvmYAxnP7+qAQYBRUIVA/s4DrL9Z8oSABTcBAlPQRuuPuPoeHd65ChZ/UHqKGgMAhoD4As65WDggdyqxNwfYwt9T19OXCxxEdLCmIyLwPuHXbAeAheOy/yBkbiPC+bP2h8BuBewECMwuvq0wECgRu08j78jo5h5waMAKSMBxAGBTLr38mrqBEImYTR2K/dEuBWAGhY4JPN9DSzaPo509nntfN6IBarh8ALEoHBZ5l2DyZhROlXQMt9H/9Dl58VIABrGE4zznvkXDK6ZS4O0MDDtBi7ZmdFCqAmH8KPgRWAXVd/lwF4swCzC3msbblNTQ8eCxfrHPwz2yAUNfJ43oBpEqIyYFTW09c4df0pdivvRFUCqypgBQjEGpAEWF8n0vqZ7I4CTUXvB+zHmvPvsoDlZ/EeszoGdZlQf1at81EjEWQAuxSjIh3YGmeS7rM/RxkcsgSS5ntBwzwvkAqMTDAlAxsgIomAAoQyLkciUzPnuS3CHnC91/Y68jKz+ditu3Z28uk+TgsAm/TfBCHT7EO6+OK5/yxL0KB23H0I5EJ2N+8b1Pmn1+pLuP7W93qt/0PGZ1gCA3Qvu3trDjyqThh8JZaAAov1ucv0+7f1ATw5ALCr87ayeTE4hKU/pxNQGPA7Gt8KLCFggb5PewARwFGXn2YMwSy96XQUjgyNT3gAJZBb1J7sh7j7bOr7Ae8kBHpn8jUbgaoBoFLmI+4jTPbPgt1oHvJ28EUNQUyDz3T0/l2TT9DE45lPFp2NGnmiRiC9eYW+/0+q8mA19AzHKFvP0fLiq2rmQdfenvsJNQCpc83H9c/qHmZTkHfu0qCkA+5cG4NQo4++j9ETMFXCWX+eVsOP8RxuBGoIANVrouwfAZwq97HneudFciGSBhWt70mHJbswmh9KACbDLx1vIZVf70nS/ffXQgwiIwFQ9vY8ACQjrGDGlAHR+VUcdVYHdgNAeS1/BuykSYg2+qA6AQlqX6nru8C29D2QBdbjocnnlAU9CeDez1jYM0E3Hg12ZALqIAACCFy5qK53uxAR3WdAH5UBrXtZ0iBbHtQjxJ7uAXjbg7FdfonHMhmdLukRQIY9AF5QIE1AL1howIlRtdBlQgrMyiMYTqMPusdDG7DhAbhg9wxFB8RsGZD5XpgAYX02dRcf0uyscYc6A3ezvvXcaRLgAPpfBTw9C5C4hs7qbLY3HPzpNAJRgAeUX6yVeVZGZwKCM/K7JAGsTO4sHYbrE3R2jjI/Mui8rO3d05MBXpaPsHTozkAnBIAt+t8EeLa+zzT2hPq/I9tHgEdlPlAWFOXaiycJQKOQJx/QIp8SKzCAbD6OZIJqx6WHm2RBj/oCPBmgTb4z+wPSVQBisxBJuv2Mj8D258O190y1wCgDCsrY6j/YnMYCXH9xHP1pgBndj7rOCAhhQ4t2+S2Hf875k2YTmn0Yj3/1nteuvnU/7fSrzThW53xd3itzzp/0UmBdIVivYaoEXhXAaKy6c/s1y4tcfV1V+Pa45/i3VgHoXYAK2T6j7cNs7zn00XNOPZ4+h3iMuQfT+CPGnn4CdhUSYxJP+nl9f5TtUdnTWwsQZXrLVDOC2ASdiaYhh7KyN7zDu19E2SOvQP+MWEHCIAzHg711aPnENe48QTRk1KH7QvYDhGB2Ao97jnNPqNsJR1+QfjfAhQCraTkMGEACiC77OQFhGBnNncQLmoUmMBin9hmiIIDKiCgIoNKgReE9KWAA/sGlDxqFpCIFogVHrxIAslo/o+GlkuGDUmBYCXA0/IwyO3jMquk/BDnHA3Bd/cgEROVADRIEHG/0tjG2azq+gGsqrr8TUwaMQMuwCEc6UaxiV/sf4QfsBIDsCLAU/Q9A3Qly5CMwff9hOzDKwoFcQDX9yPR7MOmcRh+P8iMTUTfruJmcKQMGzT2hAbhUAaIJRQPRf8/Ai1iA1Sfg4cErJ1YXCHlS4dUZAAvQTMZnzEOXSQQuPwVe5/XhY0FVYEY+QdTUo4Oj4yHArBw0BkWZ/OF7JnB4dB2wFxg8ovO9JiOPBVjane33r4C/bWOQxiqAuV22pcuDjElLB1bTe005oGqge++9iTywggCqAFaHnjmtx8sqRhZHGV6WHgR3aq0yt1B2e68CgCqBV3e33H34mLEe4CfLrf+2cGAs/f6iJceXL1++Lg696+p7Pf/R99++Pjj4VjXg9lUPBTXuK+h5wyNjqgHpKkArO2BKfRJM6kkAPnwPmb3V1HuABqHDDOjHgvvRGVxJBFg1MCb6WhQfmoaMB8Bke2d4ibffAKzf6wAYZfKoKYiREpo5ePLhWbMAWiVAQwBID/PI+APM96S+H6DURlN9rbUjY9Cj9KAKAAHsVA1CCSBqNDlhCroMCOj3iVp+qxLAMgIjEDPUPTunjwV81OG3M4zn9ABQ1P9psFdLfgy4xW/fZQHs9gEEZmEYOIKyX+QJDDDyCzEGCvAz2JBT/d+FmT7DItAqvgLw6Wm9QXWBLuH9UAyAXAMgni7f0PjpYJKRAE5vf1oCeA6+AWaxtv4CnYIhwFc5g4JGJBE6JUDUmhvdh6mNs9t2MT0GQSCxyrvUSr4KI36KCUh0DkY7+2R/2Sqtp1x69R9vudgu0JfHzQU7TOaMPsSIBluAD0w9tIR1qEDzNQgI5r3U65iGn+oPQMbgQxuwGMM+jH6DuWwAKpr1oBbgjAkoIu7uPsAAvDPp9MAPZAwCU+/B8COMwVYTMLXcl4xmmTX8njzIVgPofgHPK/AYAFHmk0QfwPS6ABlPgMjEVhlQLG0f9AEIY/h53Xm7EsBq+Im+j6g8Gj6Cav+7K/mYxUUv0weQDAD0EJBEzZ919GETESsBiGlBVEBADUfAF9AdcA+uveHYhwECmYJG89Bg+gASDn60sEmY8x1t7rb+Zuk/Y+ap0WpU006nH9C6FkDI/nwWtIJ79ukegEy/AOkT0J1+kdaXRCegBUzARkLTz+oLcAIE2urbAziqGngGnxVMQu+ArQJYGZfxCFhD0PpseoYhc0Tviw0I3jqCVgawWdqjlgujQFGg/KEkcADOBo/IoXezOQgWHlMwKwMOoN06vwVwiyqj+wPH32sFdu9jgTVRCoRO/uqrMJWAKLBEGZ9tFTbuL2cyBNoE3GhNZLU7Mu3cjG4YeJ4hWCkFovuICmYDNeF4zynDD9HfB2PRAMndOWznnzbsnA/sXedfZBJa59w69hRrgSagDpZrN588Tga2guydwYdMPnRNZAgKmAtgmN6mYagNOm0agvNu309FstAW4FsmoLt8FwA0PD+i79H9NjJ+NM23PPufYQ9e+7Gj79F8AJM1IMofUXbQKAQlATIJvYwPzhHBXX9Utmd0fsUDiBbuALmjWYBEBiDrC2SHgDLn0xJgc2efLbpfBTUJemFYQGUvQMFbeEHDDzULkT6BZ/pB085gKWLtheCN0zIyI7NbDzIGRfCafqjPs9UDz5xDWj8yFxm9323kndII1BAAaMOPKTXugJq8JjqfAnQUUKa/AaibceVx8Y9VautkADCgeF8NlvMAGA/owZz+98U9+lxVgx8s8L3VgN4KwUwwyBqDRwWHXQ8AUv7sPRDYmLp+EujeNVYGl8BfEOBso2WwVn16OqCFFNb6wKNRVYFngDS91u/m/D+DXXiNP9YqP/dnuZ8XaDbzOJpcv/b0vAKrycfQ5NMYRjLRKj3rcWuVn7G6D+p+fQ+t741mo71GoIIJiICN3jBL/1kGETECpiQnRAOQV+6j6L3FAIKfTU8BBLjQ9Sc0vVniY6m8p/eZeyHtjeSHJw9QpmcYQfSZYzIxkgHB47LDKJ4iAViwFpp8tgCeOI8JFoyEiLoDxWkmGlm6bVH6zPns89HPWb1vAQ29hgdOC0hoZFhGCnj0nJUJRzb0VK9/agAolA6PBn1k8rHBApUlLcf/IXOjNl2nbh+ZetsMQF/f8RU104DKgC5thlWBSD5ZTIQFeea8akA4Y1ZASwDYKPnRawgqwz12ggMzBISoEEjS8We/mpobyYMOBhBVAcBQUUZWSGQKKpNtsowBlGwFmYURe9jBAXovqBMwGzDKOwN1NAKBFX8ZvQ6BGRiEjJHnNgpJvCJwCm4NFgGjw8C1EpiCUNMmMpoVwKY2/QITUNaNP1CzUGTeGYbdVwFLjUWt3PPOMxqHxGrascxDtLIPGYKWCRg1BgUm3QxW9mnD76GhR5uKyOBrNQEluesPysiOMVgx9bKUP8UMQJMPda6zki+d8cnrrB2KUMWAkQACQBZ1PyJzj/IO9HVeeVAFJlfDV/Q+Ky9QA1E2Q3cYftl7tKwGzO4klDH8Mk1ACcofBo6iBIDz+5zz0XgvSN/F7haEkoRoJDJNPIOtIAYSrQWAgcjrBbB0ejAPwV0ZyJiI2SpBptEnW9/3jMzTPYCsPNhpBQ76AHYZATT1kAGIAgtzrvBdewLMQmEYgWEUhoYawTost958PZTx1w8wGGIqEWsw9HKmxOfew7om6wVUuggtvc9OM2o1AQ+aCBTu35eRBWTWp4DtPAdHhDs+g7eaT2evaS3qAXpdgLanMqFnLiE9bz2PGoGMJh+xjMnbZ0trdM9HWO+t9L7oRULL8z95i3jW4GM07Axnmo/pBaDrkM53nn/wB1YIBYuBMo+1eACwmae6y0mhKjAI/yBkBkxAiSQA6AIsMQDBawY8D0D3DwiZ0VFfgUV7w/sZQcvzHUScTUKiLB0sSS5JAOQ5sJIg8gsyz2/q/tLW4mdKgHAOYEEeZP0ACRp76OcFb+gpgtcTDOD4CwgEpn6PND+i6E5PvWf6eY0+ZtUDGXfBIh0K1Bag1MrRsNwXPc+Yekg2sFuCeRg7oonoJTyAAitgqwblrB94AKFcyFwftBl7PgTFCIJGIDeoBItwIkaBjEEJtH24VNfJwlH2d1kA4/ozup9gA6Ej/9SNQZq2Bstkd88wk6asj+i6BzizRZe4Z/iBFntbbx3tTQ8BUWKnQcbbBdfbOsw1J0FNP3uOpeG/fvsdIfuwpgIb/2/Tq0JEvQKAtU2nHflh+y/HGzB9gPX/1vIMnDo/9AeI3oA2DwCBVAL6njYLNzK7EE1CEoz4DiWA5xs4E32jABL1B+jXHoFp6Wr8qInHKvtJsJ+AB8LAMA2pNTD4XIofLephs3+W/ndQfDRgZJdBHDYV+EivAAUEBuQO6KZThokkANTwRHDwjEJBMsNrBQamoKvxo3IcQfnZCb1esDPLg8xEXm/IKandQ1CygI98AHmh48ix4Ex5j6H8Qmb5THmQuUYM+SCe4RfIGuSsi9dIFDACAavpQhMvArhn+snjzk3C+ASGLHH3P9R/c6d8536u9AwCIXfjifwCJlgwfQIEW5CjAkfb9uDMKO9sUHDYwfD0eSaLE2zAW/8vbB8AsaHIQw3e8QCs3n4xgohYU3HBOULW+d3zI58A9QYI6MlfwOv1AYTyw9LzUXuyPO6yAz0Bx0eA+l/rfbQ9HPIBkt+f4gFUMnyYbXflAFkSlEjDExJAkCwgGAC8J+kBuD0HTk1+Bs/DbBhtOoqCVMQ8LLlBfA6iVY2pzxIzi6Chdi9e78H3LgFaMjxZBgzvzfTXO2U8SgJ4Jh4yAa2uuwisjibPmoIuXReiESj4naOyoNfRSGd6ZsfeCNhev0HWrPNAnglqP1QAaOgfGGRZriVQRGVAsgrg7TzM7jNIg8tjMJYpmA0Inkk3ua3I3d+D/dxoIBP+xd3g0ExNvsISOrr9zgoGWx5AJet3PMaU+litH3gA1DnMJpJGpvd8hNA7QK68o9G998J4Am5lAmnsTJBwvAK0PmGytf2KB1DxCIDmf3iO2L037AU4zQPopP07jzVn+Uy2Tp/LMADhdw6m6XUyo7vBbPP+FZlAZb8uhnp0ln1mh19WAjzMuFcRY4DnNIVD58/CY+bWX2yWr2R9RseTOpky7jLGoFMqaw8IHfcXo6GHlAkepTfLjahD0JIJ3SyAYAmCKgagwiPeecb9RH+vzwfPiYiMt8rU3rMp/242r5zHsgH2MSRZiI5B5EdsafqjAsZOkGD1f5e3dFaWfmUmcOcBvFoASMqTl/AAEjKglQEQa/o7ND8yMql6PhEk7l7L8wGY15bHHY/Ykeb0+LKK/k9ofVPPe/fJegEvxQCiLr0A2J1ZvyoBJDHV5QgG4AF0m+Ib/3ctDMCY289kfpreZ6oAmYwdNQXJd3C8mgQI+wAagM2wgMx9R5TBAehQlmbAyJbY2iTALr3Xi3a8Ud2VPgAW0GzWr5YGjQAp2eBwxp6ArxAATJBvlAFdoKMgUgA3tb7Aei9MPR+wiFInoN7jsBIQlt8n3QkoxIKgaicgAjYxbZit/7fX9qPg8CN7AGWwk8AO5UDWE2Be0+uW8z7sgBV4Wh2uRSDr/tT5qFGJXA/gue+Q7Vj63dDr7lRjVAUwFnaltL9Tv8/6Aw+63nouaA/emf93vgcgxD4CJNilovOtciLDHKz/wApLCFZHugExsxqQlQ3O60YZPPIQ4ESeKJNHDMBjDkzmXP0C7RMwn+Od7G+V7XaZwhFy4MgAUHL5BQwGLdD+SsUAmZLsTsCoOjAdFxtSd2+GATFezB0j5kkAhqozut0zDq3afgR45lyW+lcovjeZify8v/Y8gAaQWyOKsjKBDRopwBeCg8k8gkoBDBzOPcPsnD0H9QV4gCWnBtOmYOYc671FwGQd/mwm3g0IWXZQYBXSGURaPAAhNvkMSnpb1yQze+QBiBBDNqMqwBII6ak7yDxzZg6uvz/ahefBS/D6BBAL8UzGoD5/dz1YL+Cyk9sD3kzAyOFfgwRpGE6v1GdpcScgwD6AxDXmdS/hATDuarFH2y0DknsUhJWABF2njEJSGrAegtdvkJ4K7F2vDcNkNpdCJSFkAJHbz9w7ur6S2RkZwOr3714CkAAfiY1FB7lTEAPqVOBAewtEI8+CmYM02L1A4U0sYkw8cbbxyt4v8gwi2ZHQ7tGW39A09Oh2RpfvBoss7X/avgDJADAsrR/Uaks+QAKwIWizgYOoKmRHinlA82r2KOOL5Bt3pGLqZXYGirR6RhYhsEXBJAMsD8iZYEFUJsq7+GRek71flwfgAd0195jegAp9Lzz3AMLKqkTGB7Bq81ZpzJAiSMNbdX53gQ3zFbAC8fr3vceX+j5a2y/RaDNi9BfThZjW/tFzWt8jjW7MA4B7/xljyNh1/8/1AEi9P4JqAf08yQIqgSK8JvIBSOoOfQ9nXgBL0UWIVuFshmcan/TjigF4wYpiAF5Wjc4FWZPeHbgj6z+D8p/lAWT0ftYrSOl+UhLADUoSQUK/n2ibsTDbeQDNSgumTMeagpHxthMkkM73SoGsHvdA7/UK7JzfBfSjggQdAMTp7Avq6tQ9iPdRAnKCPUTaPxMk3JLYLrjF79QTxvQja/bSzQiYIOFtChrtGciA1WMMrPbPnn9WnT97D9oDIM6JnHu2zg8bi7xs7Bl8EjfJmGO4gIaHIEPZUx5796M9BN1srGv/ht4OPQLiKyqJmpp/+RylWIcTaEewHsAMTmg+v9UKHPkGCe3vnS/oPoYmp2r5qGeA9ANqHkBWJjB6n5UKG4NEWEkA2QLRTfgw1izBDCTq2Sd2BJoJebHzVYIgV874QVD06u+wB6FS2oskwhEHK2+e7gFsBoAy3c9S9wzIvWs3z4GMAQWLACRb4N29/qivFtgtueEZghm9vgN61hg8A9CdI8ZeNQAwpuI2yJNZ2suwmXbkAer3EuhvOQLURwUHCyQs2BGbYAAHxqSngkUW4MxrvupxWAAITENk6MHS3s6qQjZAOFuVe97B8AZ6RIzB2gjVqhN7QYJlEgdn7hDcDNgzbrsXICqBIfNYNWtnKhRdxuCRJqBbGSAzPLpHKAMCcw/dizIQVXvpdMynO38AgcEyFhngIAPRayiywKlfM/I7AiYiQZkR+iNqj3vGBDTP1WAnpyXTxp/XAITuAxYKScYgVEHHNfXAWoPzTcDEUmF6jz9SBlCZvMgIwkBi/CfQH2DvXCvjS7CHAcEaKJNOv68gm1tuvTeRp8oAvGoIvC9jDkY0P9s70MEYXnItQEMAYJ1+Rgakm4UqwQJJgAjobMAgKwPRRqRRVUCc9QQMxReSgXi99CmwW2sGvMAA/AZqpSEL2t3nX9UXiAIAHPDh0Xl0PlkaZPoAwoCReb4IbmHpa3BP8XZHInccohhBokzXEUCoc6waPgN2tiMvEyRYvR5l6uxyYDYwZP0A5vVbGoHYYSGJoSIjMg+F2GqM1P2pLA60tbBAEKJxKGOAAS1v+gKVr9bfQW/Y4RmhxDlMcBNxBm+imY6MT4DYwe7zng8QNPJojc8O/bAeQ4A7zwMgDT+aLVRbhiNfIHLmiZFeIsEkIutDvZPxveDjVCZoBpHJ5EecE+n4xECR0nksI+jyAY52/Fs8AAvE2WEhjDFIthxH7CJiDOwQUCGzm5DlwcyoMga80eYnEhmLTFbNBIjsOdYEoMwwEgRUhlUxjICRAbvTg3eOavCoBIAMWGnHPyMXClo+8hkYkFvvkw1GTGb35McIqgIRwK3g7bKOQHKEAYJsDKKvi1z6pI5OAzYKPgncvJQR2BIAHM2eDQBuZ14C1BQL8AJGFFg8cBZ6FjLGoDCm3YasCIehSrAEN1MGzO4czAaQqlHYMc+faVMuYOyQQJI2AUlQD2VAeMYazSwyOr5g+IUlLLY/gKWi4m/4CQ08VaYbWZMwMv0SBp4V9Lx7hsFPBRSJgpxlmulmI0feQcBnA4T1fkhjbkaBzvExMoZgjwlYyeoEmAWBFVDbjElHB5nE7sWeoUdt/kF4DYx3wOjcrCHHvgdXMkRBKcMcmKB8RvMMA37jHHHOO9XwO1ICbPsAnYuHEvSdKT+GjIEtR0YmYHaOwQ5gva7CjKGXBLgwoCbYF1WSI8zGFKArVL1yXtfBvN6PHgB2gkB4HblYaAvISQ+gFCDYclu2PNf9WKTPI0AfAf7v/XiZAOAAblfnlwMIUa2gMz0LZLLEZ0moFCC1ltwNIFkJ0A30oGqwnTU3PYG2zULbA0CXCYhKTgCgJuXNLDXuMv7EXoYb0coQ0LsmYMKgy9S6qyZguFiKNRMNgy5TGmUDBsvqjjIBU0YgESzEMwhfwgQ8QAa4lYSdpiGPbUQ7BEli1eGmCViSDqBakZYAgjchFUnM1ssExExZj1ill9rEI+MjdJcBz/YGWiQAm8Gzuw2x/QSkzqevCTJ7uPWX4FWMFROQ9g9YwFckAAlU2QEyawIiE7GyJXkV5B00/gj6v3PPnQBQHt5ZlQFFnY+yK3NNuUsPsBeKlib6DcpBgvQQWsEdbWXmlfUy3sZOmbBiAu4Yh882Gs8OAOHy4szEITKjV2QBQ+8zVYAMSMNsbFB1tOCKBXwqIJABI2QdTFmPCTpZMFbofoUxZHcHfsZuwmwAGJahwG4GkhwZlmEFFRaQ1fdMGZCh9dG5GRMs0vBW0M0AfjtgIDDu7v7jnUuu7PNWCaZpdSaDZ6n6DrVn31e5CoDAa30IszsONZYImS5CxmkWIgt6hhrbo06DJFMFiCoLyLGPdklO7KSUuo4NemrDEImqNpUyHdHNRw0wMT7jzDy/aYBaNisAPVUA73ym5m+UQjIsgM3otFkoufUCLBPJ3nOAHncJPALWbaczeqUcxwRIMiiGwTKb4XeAy2Tk3X0BveXN340HkJQGNJ1P6vpUQGAqBkf1ICQDguknFMEaUvxsUMnq9p06e2fJLgNqViJ0zA/8Lk3AanmPAB3FEioBQfrGhB0dEBCA76oW1YCwC/hqtu8CfGXqD7NhCJPBjwD0WdWBrQCQ0fwHyICyLEiwglcLArRHYXUcJkpyFOVnHP1OYLPgr9T7M8EgC/gK/T+rarAbAGjKf5AMoFlAlRVU/IWmxUiZIMMCeIsBMAHkSGDvZOqdbN8VOF6N/m9XAZKUP9v0gxhGNHs/cy7cTMObV0A4xSlTilnIwupRxsdo+j4dDFkvBbGudQvyqEJjZEdzNx3trFc2LAkCR+T0I7efKX1STv9hVYBsxmcDRSANQingaHyGRXjz9BgJsP1BJ/T9YQyi6XvXJ6i69cz13s/FqTvbo70Cp//76ATsMAi7pEHlZyEXFnlZTmodhrtA3JYRScAeFSDM+1YMuk5KXgV/ZbvyVzy+lwCQHiiSXJnIAjZ8Trjlz9vBggWocLMTDwsQiR17S+CvVAKasrn8CMdbZkvv4LnJ6PfEY5GH0MoCsiZhJQg4f+tdsD2bAbRdU6X6UUY+M9szVN/q6nNmLIrk2qFFghItCgAvzQSSU4V2g0LmXqHZCHR8xS94iQBRBSdB38PXKppx4wjw/2gM4JUCQIUFlIJENQgE7ImtVrCVChbQhzOAjoCxKQFEyA69HfBXjL1XNPpeLgBIX5mwkvWZYFPd8jvr7gvh8LPSIRWANrN5eD+Cyqevi8psO+C3jgrVZ47K9uTfbQBAznmCBYT+QIMUyDj8O/5BSgJUpUOUNZMZm8rmVSrvadROHR+BrcPVZ7Zn32EXhwSAtRFoMwCYMwN2WQBL24NMjO4h2UYhsRuCMlmoi76W/8/YjVHY56KAtBhT3TJKij8zZtzIAJsNQMnsPwuBIdUcdAYDyCwayjCGDiZQNQ5F+GnHKQaQycpFyr9tshHsgmIiEQModOWlQMmAsZL5HYb7cl7AKQEgIwOygaHAKjqCQMbIywI7BG8xSKQz5i64iSBSuhdTWahQ+i5Dr6NScFaweCuU5Nw5fQ1m4O4y4woz6DATI2YQZvvOINFEnUsA3AlAGQOx07zbyPTt7cGVo7rX4FtXTR6AoDvbZ8277HW7zUaekVfxKrbB2qSZzwwq9PWVTF8dSc6Cl12NuBEsRBqZQXcASMsASc4RbOwhKAeB4MNKmZa7XY0HBZEWRnDGz2AVnxwF/m5K7oHZWQb9EhKg3QxM0PQUkA8MAi0gTVQV/jA/H5XVu8GfcfN/qEagEwOA5zccHgSK48pb2EKy5PndAPmIrF7V1WeA/xlz/l8lAKCeAHdBEbkBSNY7qAYGdF0W0B2ygmmI2r1nN7DfrzmKrrOGYicbONLh9wLGUWsQTmUARUMwZf5lAgMTBEgGkm0yqp6TBWnmGha0RwSPrnPCDHwE0L+3/v+WAEC67x1A78j4p/gEXaA/KeCcAdpUJu567aOyfAfQX3pz0Gx232ABWaB3mIJPMxCr4OzK9pvU//BzngX0o8DfvT/BqzIAqmFIcusDImaxS/u3g0BR89OB4SDpUAk6R9F0OQromeEZR7n4nePELSn2PQWAEOiCFy50SIGKHGDAzTjzOwCvBosuRtCVzdOgTt4rvL/1OAJUFvyd1P+ZfsEzA0C3T3DU49AIlI0xaBuj0o66/5HZ/LD7Bxm+FCTOCgovawIK3+/PnOdS+goLeEIQyHoNUgwsnY+lwHT0/TuyOZs9O+h9J/irz5E7CoXywDvvrWPlX3ICbwcLeFoQyICFNRIPDgSvAuhyNrfOZYdqXpm/xgDODACeWRgxjDawI5p/YHCQTh8h+9oMA3hCMDgF5MU+/Grm915Hnh04TgkAstEzUDQFK2DPALsjOHjnbgeXAOAuAzgiQBTo+iEgr3ThHVHWI5Lm8+YBFKh7CHByH8CKl1DxCrrYAAuuEnNIAq8T4Onr9divwvuvgnnrHkfo9wi8rzQwpCsAdLGAVwkC6cVJiccz4Mj8zu59dwFOvF92R96uICHSsPjmFcF/pqdwZgBoWc9cbSDyaH8S1F2BoA3cG/KgDMTiPTroewr4ndf8CKbfUwOA7JcNI5ZQNQ272UXaH9gE95GgL2fl7L0PMuG6KX85MHSV9V4yAGSMvga5UJUKVabQzgaeISWe8Hg2gz8t6+8EhrN0/xEMpDMAtJqGRweBIlMoB4KsBEje62Ufrxhwm/eqGn3yTPC/1GKgEwJAl2ewEwSOYAqVe1YYQSfz2ALxZiamJQBB6b/7zP5yHoDkR4Bnzh9drca3/6tqti+AjMrg3RKgCuyGjL79Og0SoDWzZzcpAcAX6WvVDYNI1iNgzn87wr1vlgKZYLFrLkqV+u9c2wT4bQA3ALslICSe27n2JSj9sysKb/PcEeCv4BtIFCh2GMEBQeQpweAZzx2ZtRPAl1fU80cFiqMCQArY3bKBlRZHmohHMoajgsELAPsQuk5m9BaqfhSdP+r4Sa7jOq7jD3u8HdjBlzYQWRZA3Dczz2DnPtTzR3oLzVm97b5Hs4IOZpCk9NvyoULnj2YKb/NJI8B3vQNpriJ0VBo6ZEEBlKcFi93no9dOgroD+C2g7poZ+LIMQPaW82avHUewhoQvQGV02Sw7nhEsXuH5s0CNViNmX6eo5Q8B/06wYK99m8ePAD+0iiC948s62QUdLDYye+oeBwC6DLaOe7CZvABo9n4i32nmPzUAFBnEUcyhO2B0gJQJGBFQu86JwNp1zpmZvBXQZ2TylxgI0hgAykEgkbXTWZ4AZea8rJ/ABoMjwX4m4LcBXQAze78M8LNZX+SFNwm5yoDXcR1/4OPtoIad7nscyQS66X7pvGcwgmewgsx5R2XzozP5ZuZPX7dzj7cjV/w1y4GSL1AJBE8KBt2BoxPsW4DfAOjp99wFZJW+v+Ry4AMDwFYQKLCIo68pAbch6z8T7BlwZtjAU3X7mdc8G/ylANAkBXaDwBaoD2QFW+dnAsJR8qIhu7fdtwH0lWx/GvA7wV99H5cJeB3X8Qc+3uYx23adySi2jMUqG8hq+gMYwZakaD5/WwrsXFN5jZ2s35j5y9f/SAGgQw7sBpJKMNim+5UgUgkKZwSGSnCoXtNE2Xf0+hZwX6kr8G0Dc2P2R4G2UuMzg8EOqzgoKLQC/Wyw72T5ZtB3AL896+8ElLfdFt8GwB5537ZgsBsQnhEUdhjDznUHgH0H8E8F/QnA37rvZQJex3X8gY+3g7framEC3WzgSYygnRXseA8dWX5HQnRl+4MyfmfWb8/8LxcADvIDjpYb7QGhSyp0BIaO4NAVJDoCRdc9jgL80XS/U/dfEuA6ruM6WhnAWSygWxZ0M4LDmcHJ2b3zPk/J9Edn/GfQ/dP2BnwBmn4maDv1+pGBoTNAdFD4Thp+hP4+GvCnafwj5MXbQVg9Kks/MyB0A/gIMB8B7CMAfiTQ2wH5LMCfwS4uD+A6ruPyAI7NyiezgKNp/FkZ/MhsfnRmPzrDH5YRj5YPz9L6zwwAZ1Dzlg/8ge/rSACfBeYzgX0o8DrKmie9r8PfyyUBruM6LgnwvCz8ZDZwNpVvzXhdQ1kOzqinZ9lnSIem93r6+7oYwHVcx8UAXivrvhgrKGWOs5uiTszeT8monU1UL6Txn/5e314Ua+PJwHo6OJ/VTPUiAP5uQJ38PV/ud7gkwHVcxyUBvqvj1Wj4Hz0z/1AZ/EeVHxcDuI7ruI4fggE8LevMH/OPNX60X+isNSA/wnExgOu4jj/wMcYY8/oz/HGP+YNTwOu4GMB1XMd1gON/AwCwtGiXiZ6F/gAAAABJRU5ErkJggg=='
        }, {
            xtype: 'image',
            itemId: 'saturation-value-pointer',
            width: 8,
            height: 8,
            style: 'position: absolute; left: 0; top: 0;',
            src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAnklEQVR42pWSPQoCMRCFHwhJZWG911oIokdYyBUXArZCrmK1VXwjTxkDG3TgK+af+QEk58s1kIWsZBOrbAFeaJjInbQdzDf5ylWOG0kkiiRbU0yAWpqhkBM6MZt8FrPAKQk7ok6votBwpsRBQlTM9mvCUTEPuKHmQcL8Xoop+Y+hc7/WomoHV7l8rdUdrg4OVz+H614j6x2a65j9azwBGk+my+T7uM8AAAAASUVORK5CYII='
        }]
    }, {
        xtype: 'container',
        height: 264,
        width: 40,
        margin: {
            left: 24
        },
        style: '-webkit-user-select: none; -moz-user-select: none; -o-user-select: none; -khtml-user-select: none; -ms-user-select: none;',

        items: [{
            xtype: 'image',
            itemId: 'hue-widget',
            height: 256,
            width: 20,
            style: 'position: absolute; left: 6px; top: 3px;',
            src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAACyCAIAAADNrnipAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpENUEyNzU3OUNBODIxMUUzOUFEQURDRTRCMUIyRjZGNiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpENUEyNzU3QUNBODIxMUUzOUFEQURDRTRCMUIyRjZGNiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkQ1QTI3NTc3Q0E4MjExRTM5QURBRENFNEIxQjJGNkY2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkQ1QTI3NTc4Q0E4MjExRTM5QURBRENFNEIxQjJGNkY2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+lm1TXgAABVxJREFUeNqkWr2rHUUUP8esUe99IjEIBhEjIprCEFJICgvTWKiNCFpoo6D/UhotxEIUbFSwMYKFgiim8CONDwV5In6Lee9pIvk5e+9+zO6e35nZ2S0ee2fnN+fMmfM9TyE3iqg0D9p3bP5q+yLDn82LQnYiZPdBJ4ObuRjgw8DxIcEpF9N1m08BfFf0WSbcTheN5kHuG04yH3vzAfyQ0GdEMNp/S/nh4WZynp7y+ZEY2A6nfAXwk0Oymsd/c1TPSu4z3lcAv5gx1RZHJb2GiSNY85mCHcUaf92CTXmMuJgyUoPX+RKaquerrkmID35zIp7pscP8GsDvcdnCHwzgD8lWTYUf/AzgT6X0CeAv+SGrqwIawLsTNTRPaOotavDeRBOQsfPGJH9lntUiOHDMAXyFbI8hBwK7mnG2VNooP6prAtODUV6jLelBO81xs2wn+ucStn92wb709AcCzokA+m00B6YSWvF6O6hfbX7OONx43c8XCKz6x2XS9+DVIYn8knoZg5k3YrKsDsxsw1XS7lMNTuYyzFwbtstCRsO2cMfl+LdeYImcyzrRhrLv6Sjb++7B+OaVEFhCPQ9S+R9z39rptrpIIZLrj2rWgxwlSez5MNPjWELR03nOwNaCBzZOnwnJyeLrn/e0PkyspMaUUx8x7owcICyVFhJja/CxUcUysRAhTNV/d6JAl69bzRJH20EnHWDyV23Z1olt+nn+Bn8E3v5ctMrNyKigbKmo3AY7KIDrl3bDd4DCUlyo3I15aRBiyvcj4aN5SqlyekFOIueQqjeJZdSv55dQfgL5mxx9UnkG87zeQMNegJdITOue6FyquibLKbwthalkVSKoiHKO17GsrWpKSXXrWKLeKhdS6ukZxutwLNbvY6i8vUTD3odttywRiUSj8hHyz2a0uspnS9j+GqRe475BOsq7SyjvoahfsAX/5rJt5o/SKcnfKGpzbKf8i5Kct9Ht60sEhv8yekNiBtIAPuQlgvqtqgD+K68ZZggggH8p78QJ9lI9MHH2vMu7SqYTQ0z5mzx/LyblL0ixhyTzAfwJL9/Y4XXZFy7yOnQaWgeDAfyuFY78grTtuAreKm8gCl5zA6nXPQ3gCxkE7ZZWJXKQXVyMJRrAhxkBzq6yAng/pydsFpox5dltniCwl5e4oeczkkd7JICfXqIkj8/pZWHkwx4lZpiu4wP4XIGcO8pn8k7ViIYB/CDpyDjFYU/5ZJnf3YJPEI/h0ezAt1v2kHODU4N38roT1sWP4CZeeCa8QgAf4YmEEJfUUr4BMwqa0QQ9Ctq9Uu61m7L/VtDbimSJp8dB7QAkGvTTTiBZJts7qkfuxdy8L1r0FHI9vQE+C3oqSF0kVbdkOD6WctRgFiyUJDjdSwMW/2KKLFeteR1iWlO8dE+5oMlTrbiGOM3xBrwui3Fbttd2XU0zv1jg1aqkMon2XN73XOdpmH0hsOJ2K2462WuY4+md4mqgnv5pGbq9yg5QhsFfKi+rRC8j7W5ZKNDvMC/tG3z6EWmPQ/uev2NGkjyyVr2yRGBXke4R0DBwPV1h8yIa1wr7WRvwfqr1yIWu+MOyDNbxHXocxU+WmxFXb/o9f5/yt0q63zXly0XRtaF8KeOehUxQfJyZ7hkBUfFBynHw5RTvpII6txvFGxmqyBqIeIVfbsMpQuuXqq9DkWlK/aJV/a8VSUsgelrJfqojxFPYFuxf9JNOsOIlN1tl6zbg59yEGd4VmOIpN2108xXFYykHwnsCikfIVEl5GA3gs8l0qJ5F7PnUHKepI4Gd5DcYqfxTm1Jyyxu15BHFbs/HZvRnRywo1hmKTcb/F2AAe20YVEpQ65oAAAAASUVORK5CYII='
        }, {
            xtype: 'image',
            itemId: 'hue-pointer',
            width: 32,
            height: 9,
            style: 'position: absolute; left: 0; top: 0;',
            src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAJCAYAAABT2S4KAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKtmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarZZ3UJPZGsbf7/vSCwQSIiAl9I4U6dJrAAXpYCMkIQRCCCFBwIbK4gqsBRURrOiKiIJrAWQtiAXbItj7giwq6rpYsKFy/2AJ99659487c9+ZM/Obd555znnPOX88ALQ2nkwmQRkA2VKFPCY0gJOUnMIhPgIcMEANzIHB4+fJ/KOjI+E/FwLw4TYgAAA37HgymQT+t9IQCPP4AEg0AKQJ8vjZAMhRAGQ7XyZXAGBxAGC6UCFTAGAFAMCSJyWnAGAVAMASjfNOAGCljfNRAGDJ42ICAbCLACQajycXAVBvAgAnny9SAFDfA4CDVCCWAtBMAcCHn8ETANAEAGCbnZ0jAKCtAwDLtH/yEf2LZ5rKk8cTqXh8FgAAIAWJ82QSXiH8vytbopzYwwAAaHlZsREAYA6AFPB5wbETnCHkRk6wTBEQM8FiBTdOpVGGxU+wMivef4KzciJUemnarCiVf15gygQXZcQlTrBAGBQ8wfKcGJU+Lz82eFIfOGuCM3nh0RPMkwNMsFASGjN55mjVOaWSWapZ0uUhKo0wb3JeRUZcmIrlcSpNujiEq5pXHjbpL4lWecqVMap7EErjVZ4CXpDqbiEOMkAJUhCAEOSQBjkgAQVwIAjEkAcykAAPCgEUwgIFAEBgjqxQLhZlKDj+MplEaMvhSvn2thwnB0dnSEpO4Yw/4Ts2IACAsC9P9nI7ADzKABDRZI9nAnD8KQDzw2TP5O34Pz3Zw1fK88d7OAAAPFBAHVigAwZgApZgB07gCl7gB8EQDlEQB8kwH/iQAdkgh4WwGJZDKZTDOtgENbADdsM+OAiHoRVOwBm4AFegB27BA+iDQXgJw/ABRhEEISJ0hInoIIaIGWKDOCHuiA8SjEQiMUgykoqIECmiRBYjK5FypBKpQXYhDcgvyHHkDHIJ6UXuIf3IEPIW+YJiKA1lofqoOToNdUf90Qg0Dp2HitBctAgtQdeg1WgdegBtQc+gV9BbaB/6Eh3BAKNibMwIs8PcsUAsCkvB0jE5thQrw6qwOqwJa8e6sBtYH/YK+4wj4Jg4Ds4O54ULw8Xj+Lhc3FJcBa4Gtw/XgjuHu4Hrxw3jvuPpeD28Dd4Tz8Un4UX4hfhSfBV+L/4Y/jz+Fn4Q/4FAILAJFgQ3QhghmZBJWESoIGwjNBM6CL2EAcIIkUjUIdoQvYlRRB5RQSwlbiEeIJ4mXicOEj+RqCRDkhMphJRCkpJWkKpI+0mnSNdJz0ijZAbZjOxJjiILyIXkteQ95HbyNfIgeZSiQbGgeFPiKJmU5ZRqShPlPOUh5R2VSjWmelBnU8XUYmo19RD1IrWf+pmmSbOmBdLm0pS0NbR6WgftHu0dnU43p/vRU+gK+hp6A/0s/TH9kxpTzV6NqyZQW6ZWq9aidl3ttTpZ3UzdX32+epF6lfoR9WvqrxhkhjkjkMFjLGXUMo4z7jBGNJgajhpRGtkaFRr7NS5pPNckapprBmsKNEs0d2ue1RxgYkwTZiCTz1zJ3MM8zxxkEVgWLC4rk1XOOsjqZg1raWpN10rQKtCq1Tqp1cfG2OZsLlvCXss+zL7N/jJFf4r/FOGU1VOaplyf8lF7qraftlC7TLtZ+5b2Fx2OTrBOls56nVadR7o4XWvd2boLdbfrntd9NZU11Wsqf2rZ1MNT7+uhetZ6MXqL9HbrXdUb0TfQD9WX6W/RP6v/yoBt4GeQabDR4JTBkCHT0MdQbLjR8LThC44Wx58j4VRzznGGjfSMwoyURruMuo1GjS2M441XGDcbPzKhmLibpJtsNOk0GTY1NJ1puti00fS+GdnM3SzDbLNZl9lHcwvzRPNV5q3mzy20LbgWRRaNFg8t6Za+lrmWdZY3rQhW7lZZVtuseqxRaxfrDOta62s2qI2rjdhmm02vLd7Ww1ZqW2d7x45m52+Xb9do12/Pto+0X2Hfav96mum0lGnrp3VN++7g4iBx2OPwwFHTMdxxhWO741snaye+U63TTWe6c4jzMuc25zfTbaYLp2+ffteF6TLTZZVLp8s3VzdXuWuT65CbqVuq21a3O+4s92j3CveLHniPAI9lHic8Pnu6eio8D3v+5WXnleW13+v5DIsZwhl7Zgx4G3vzvHd59/lwfFJ9dvr0+Rr58nzrfJ/4mfgJ/Pb6PfO38s/0P+D/OsAhQB5wLOBjoGfgksCOICwoNKgsqDtYMzg+uCb4cYhxiCikMWQ41CV0UWhHGD4sImx92B2uPpfPbeAOh7uFLwk/F0GLiI2oiXgSaR0pj2yfic4Mn7lh5sNZZrOks1qjIIobtSHqUbRFdG70r7MJs6Nn185+GuMYszimK5YZuyB2f+yHuIC4tXEP4i3jlfGdCeoJcxMaEj4mBiVWJvYlTUtaknQlWTdZnNyWQkxJSNmbMjIneM6mOYNzXeaWzr09z2JewbxL83XnS+afXKC+gLfgSCo+NTF1f+pXXhSvjjeSxk3bmjbMD+Rv5r8U+Ak2CoaE3sJK4bN07/TK9Ocib9EG0VCGb0ZVxitxoLhG/CYzLHNH5sesqKz6rDFJoqQ5m5Sdmn1cqinNkp7LMcgpyOmV2chKZX25nrmbcoflEfK9eUjevLw2BUshU1xVWip/UPbn++TX5n9amLDwSIFGgbTgaqF14erCZ0UhRT8vwi3iL+pcbLR4+eL+Jf5Ldi1FlqYt7Vxmsqxk2WBxaPG+5ZTlWct/W+GwonLF+5WJK9tL9EuKSwZ+CP2hsVStVF56Z5XXqh0/4n4U/9i92nn1ltXfywRll8sdyqvKv1bwKy7/5PhT9U9ja9LXdK91Xbt9HWGddN3t9b7r91VqVBZVDmyYuaFlI2dj2cb3mxZsulQ1vWrHZspm5ea+6sjqti2mW9Zt+VqTUXOrNqC2eave1tVbP24TbLu+3W970w79HeU7vuwU77y7K3RXS515XdVuwu783U/3JOzp+tn954a9unvL936rl9b37YvZd67BraFhv97+tY1oo7Jx6MDcAz0Hgw62Ndk17WpmN5cfgkPKQy9+Sf3l9uGIw51H3I80HTU7uvUY81hZC9JS2DLcmtHa15bc1ns8/Hhnu1f7sV/tf60/YXSi9qTWybWnKKdKTo2dLjo90iHreHVGdGagc0Hng7NJZ2+em32u+3zE+YsXQi6c7fLvOn3R++KJS56Xjl92v9x6xfVKy1WXq8d+c/ntWLdrd8s1t2ttPR497b0zek9d971+5kbQjQs3uTev3Jp1q/d2/O27d+be6bsruPv8nuTem/v590cfFD/EPyx7xHhU9Vjvcd3vVr8397n2newP6r/6JPbJgwH+wMs/8v74OljylP606pnhs4bnTs9PDIUM9byY82Lwpezl6KvSPzX+3Pra8vXRv/z+ujqcNDz4Rv5m7G3FO5139e+nv+8ciR55/CH7w+jHsk86n/Z9dv/c9SXxy7PRhV+JX6u/WX1r/x7x/eFY9tiYjCfnAQAABgBoejrA23oAejIAsweAojaegf/O7shkiv9vPJ6TAQDAFaDeDyC+GCCyA2B7B4BZMQCtAyAaAOL8AHV2Vq2/Ky/d2WnciyYHwH8aG3unD0BsB/gmHxsb3TY29m0PAHYPoCN3PHsDABAYAJUWWg5m0ssjN3D/noH/ATng/qyl8sCSAAAAIGNIUk0AAG11AABzoAAA/N0AAINkAABw6AAA7GgAADA+AAAQkOTsmeoAAADjSURBVHjatJNBjsMgEATL+SO8w85LfPIp/oAfaLKabrMHshtvlFvYlpAQI1RNzzDUWsk5V54atm3jv/TKIqVUz0op1ZQStdbu6x3rAlBK4et+Z98L63oDqDnn7i9f1xv73lilFAAuAIogFNiBbJZl6Woi51yXZUE2dmMp4mkgLCJESFjGNvM8A9Qe8HmesY1lQg+WdU5ARDRXkrDNOI5tSD7XMI4jtpHU0o5AcTIQ0qMobPWEv5hoDKml/WvgcIvdx8E0XbvCT196mKYrPg5sc9jtMKX02uvhzeUeg/iz/cP6HgB6thu6ZTRojAAAAABJRU5ErkJggg=='
        }]
    }, {
        xtype: 'container',
        height: '100%',
        layout: {
            type: 'vbox',
            pack: 'center'
        },

        defaults: {
            defaults: {
                labelWidth: 40,
                width: 100,
                labelAlign: 'right',
                labelSeparator: '',
                allowDecimals: false,
                allowExponential: false,
                allowOnlyWhitespace: false,
                autoStripChars: true,
                margin: {
                    bottom: 8
                }
            }
        },
        items: [{
            xtype: 'container',
            layout: 'hbox',

            items: [{
                xtype: 'numberfield',
                itemId: 'red-field',
                fieldLabel: 'R',
                minValue: 0,
                maxValue: 255,
                listeners: {
                    blur: function() {
                        Ext.ux.ColorPicker.boundField(this, 0, 255);
                    },
                    change: function() {
                        var me = this.up('[xtype=colorpicker2]');
                        
                        me.recalculateHsv();
                        me.updateHsvWidgets();
                        me.updateSwatch();
                        me.updateHex();
                        me.fireChangeEvent();
                    }
                }
            }, {
                xtype: 'numberfield',
                itemId: 'hue-field',
                fieldLabel: 'H',
                minValue: 0,
                maxValue: 360,
                listeners: {
                    blur: function() {
                        Ext.ux.ColorPicker.boundField(this, 0, 360);
                    },
                    change: function() {
                        var me = this.up('[xtype=colorpicker2]');
                        
                        me.recalculateRgb();
                        me.updateHsvWidgets();
                        me.updateSwatch();
                        me.updateHex();
                        me.fireChangeEvent();
                    }
                }
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',

            items: [{
                xtype: 'numberfield',
                itemId: 'green-field',
                fieldLabel: 'G',
                minValue: 0,
                maxValue: 255,
                listeners: {
                    blur: function() {
                        Ext.ux.ColorPicker.boundField(this, 0, 255);
                    },
                    change: function() {
                        var me = this.up('[xtype=colorpicker2]');
                        
                        me.recalculateHsv();
                        me.updateHsvWidgets();
                        me.updateSwatch();
                        me.updateHex();
                        me.fireChangeEvent();
                    }
                }
            }, {
                xtype: 'numberfield',
                itemId: 'saturation-field',
                fieldLabel: 'S',
                minValue: 0,
                maxValue: 100,
                listeners: {
                    blur: function() {
                        Ext.ux.ColorPicker.boundField(this, 0, 100);
                    },
                    change: function() {
                        var me = this.up('[xtype=colorpicker2]');
                        
                        me.recalculateRgb();
                        me.updateHsvWidgets();
                        me.updateSwatch();
                        me.updateHex();
                        me.fireChangeEvent();
                    }
                }
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',

            items: [{
                xtype: 'numberfield',
                itemId: 'blue-field',
                fieldLabel: 'B',
                minValue: 0,
                maxValue: 255,
                listeners: {
                    blur: function() {
                        Ext.ux.ColorPicker.boundField(this, 0, 255);
                    },
                    change: function() {
                        var me = this.up('[xtype=colorpicker2]');
                        
                        me.recalculateHsv();
                        me.updateHsvWidgets();
                        me.updateSwatch();
                        me.updateHex();
                        me.fireChangeEvent();
                    }
                }
            }, {
                xtype: 'numberfield',
                itemId: 'value-field',
                fieldLabel: 'V',
                minValue: 0,
                maxValue: 100,
                listeners: {
                    blur: function() {
                        Ext.ux.ColorPicker.boundField(this, 0, 100);
                    },
                    change: function() {
                        var me = this.up('[xtype=colorpicker2]');
                        
                        me.recalculateRgb();
                        me.updateHsvWidgets();
                        me.updateSwatch();
                        me.updateHex();
                        me.fireChangeEvent();
                    }
                }
            }]
        }, {
            xtype: 'textfield',
            itemId: 'hex-field',
            labelWidth: 40,
            width: 200,
            labelSeparator: '',
            labelAlign: 'right',
            fieldLabel: 'Hex',
            readOnly: true,
            margin: {
                top: 24
            }
        }, {
            xtype: 'button',
            width: 200,
            text: 'Done',
            margin: {
                top: 24
            },
            handler: function() {
                var me = this.up('[xtype=colorpicker2]');

                me.fireChangeEvent();
                me.hide();
            }
        }]
    }],

    /**
     * Binds the necessary mouse events for the saturation/value and hue widgets.
     */
    addMouseListeners: function() {
        var me = this,
            body = Ext.getBody(),
            saturationValueWidget = me.queryById('saturation-value-widget'),
            hueWidget = me.queryById('hue-widget');

        // These both work the same way. We bind a mousedown event to catch initial clicks on the
        // widgets.
        //
        // After that, however, we bind a mouseup event to the body. This ensures that the event
        // fires properly if we drag the cursor outside of the element.
        //
        // The mousemove event also occurs on the body. The resulting behavior is that if you drag
        // the cursor outside of the widget, then it can continue to move, but is bounded by the
        // edge of the component.
        //
        // If the mousemove event was bound to the widget itself, then it the cursor would stop
        // moving when the mouse left the widget. This looks choppy and makes it difficult to select
        // extreme values (ie: 0/100).
        //
        // The one scenario not covered is if the user drags the mouse out of the window.
        saturationValueWidget.el.on('mousedown', function(e) {
            me._saturationValueClick = true;
            me.setSaturationValueWidgetXY(e.browserEvent.offsetX, e.browserEvent.offsetY);

            me.mon(body, 'mousemove', me.onSaturationValueMouseMove, me);
            me.mon(body, 'mouseup', me.onSaturationValueMouseUp, me);

            e.preventDefault();
        });

        hueWidget.el.on('mousedown', function(e) {
            me._hueClick = true;
            me.setHueWidgetY(e.browserEvent.offsetY);

            me.mon(body, 'mousemove', me.onHueMouseMove, me);
            me.mon(body, 'mouseup', me.onHueMouseUp, me);

            e.preventDefault();
        });
    },

    /**
     * Fires when a mouse up event triggers after clicking on the saturation/value widget. Releases
     * the cursor.
     */
    onSaturationValueMouseUp: function(e) {
        var me = this,
            body = Ext.getBody();

        me._saturationValueClick = false;

        me.mun(body, 'mousemove', me.onSaturationValueMouseMove);
        me.mun(body, 'mouseup', me.onSaturationValueMouseUp);
    },

    /**
     * Fires when a mouse move event triggers after clicking on the saturation/value widget. Moves
     * the cursor to the current location of the mouse.
     */
    onSaturationValueMouseMove: function(e) {
        var me = this,
            widget = me.queryById('saturation-value-widget'),
            widgetXY = widget.getXY(),
            widgetX = widgetXY[0],
            widgetY = widgetXY[1],
            x = e.browserEvent.clientX,
            y = e.browserEvent.clientY;

        if (x < widgetX) {
            x = widgetX;
        } else if (x > widgetX + 255) {
            x = widgetX + 255;
        }

        if (y < widgetY) {
            y = widgetY;
        } else if (y > widgetY + 255) {
            y = widgetY + 255;
        }

        if (me._saturationValueClick) {
            me.setSaturationValueWidgetXY(x - widgetX, y - widgetY);
        }
    },

    /**
     * Fires when a mouse up event triggers after clicking on the hue widget. Releases the cursor.
     */
    onHueMouseUp: function(e) {
        var me = this,
            body = Ext.getBody();

        me._hueClick = false;

        me.mun(body, 'mousemove', me.onHueMouseMove);
        me.mun(body, 'mouseup', me.onHueMouseUp);
    },

    /**
     * Fires when a mouse move event triggers after clicking on the hue widget. Moves the cursor
     * to the current location of the mouse.
     */
    onHueMouseMove: function(e) {
        var me = this,
            hue = me.queryById('hue-widget'),
            hueY = hue.getY(),
            y = e.browserEvent.clientY;

        if (y < hueY) {
            y = hueY;
        } else if (y > hueY + 255) {
            y = hueY + 255;
        }

        if (me._hueClick) {
            me.setHueWidgetY(y - hueY);
        }
    },

    /**
     * Selects a point at (x, y) on the saturation/value widget, updating the rest of the UI.
     * 
     * @param {Number} x The x coordinate of the point that was selected on the widget.
     * @param {Number} y The y coordinate of the point that was selected on the widget.
     */
    setSaturationValueWidgetXY: function(x, y) {
        var me = this,
            widget = me.queryById('saturation-value-widget'),
            pointer = me.queryById('saturation-value-pointer'),
            sRatio,
            vRatio;

        pointer.setLocalXY(x, y);

        // Small diagram to aid understanding. Saturation is on the x axis and value is on the y
        // axis.
        //
        // v=1 ____________
        //    |            |
        //    |            |
        //    |            |
        //    |            |
        // v=0|____________|
        //    s=0          s=1

        sRatio = x / (widget.getWidth() - 1);
        vRatio = y / (widget.getHeight() - 1); 

        me.setHsvSaturation(sRatio);
        me.setHsvValue(1 - vRatio);
        me.recalculateRgb();
        me.updateSwatch();
        me.updateHex();
        me.fireChangeEvent();
    },
    
    /**
     * Selects a y coordinate on the hue widget, updating the rest of the UI.
     * 
     * @param {Number} y The y coordinate of the point that was selected on the widget.
     */
    setHueWidgetY: function(y) {
        var me = this,
            widget = me.queryById('hue-widget'),
            pointer = me.queryById('hue-pointer'),
            hRatio;

        pointer.setLocalY(y);
        
        hRatio = y / (widget.getHeight() - 1);

        me.setHsvHue(1 - hRatio);
        me.recalculateRgb();
        me.updateSwatch();
        me.updateHex();
        me.fireChangeEvent();
    },

    /**
     * Gets the value of the HSV hue field.
     * 
     * @return {Number} The HSV hue, between 0 and 1.
     */
    getHsvHue: function() {
        var me = this,
            field = me.queryById('hue-field'),
            h = Ext.Number.from(field.getValue() / 360);

        if (h > 1) { 
            h = 1;
        } else if (h < 0) {
            h = 0;
        }

        return h;
    },

    /**
     * Sets the HSV hue field.
     * 
     * @param {Number} h An HSV hue, between 0 and 1.
     */
    setHsvHue: function(h) {
        var me = this,
            field = me.queryById('hue-field');

        h = Math.round(h * 360);

        field.suspendEvent('change');
        field.setValue(h);
        field.resumeEvent('change');
    },

    /**
     * Gets the value of the HSV saturation field.
     * 
     * @return {Number} The HSV saturation, between 0 and 1.
     */
    getHsvSaturation: function() {
        var me = this,
            field = me.queryById('saturation-field'),
            s = Ext.Number.from(field.getValue() / 100);

        if (s > 1) { 
            s = 1;
        } else if (s < 0) {
            s = 0;
        }

        return s;
    },

    /**
     * Sets the HSV satuation field.
     * 
     * @param {Number} s An HSV satuation, between 0 and 1.
     */
    setHsvSaturation: function(s) {
        var me = this,
            field = me.queryById('saturation-field');

        s = Math.round(s * 100);

        field.suspendEvent('change');
        field.setValue(s);
        field.resumeEvent('change');
    },

    /**
     * Gets the value of the HSV value field.
     * 
     * @return {Number} The HSV value, between 0 and 1.
     */
    getHsvValue: function() {
        var me = this,
            field = me.queryById('value-field'),
            v = Ext.Number.from(field.getValue() / 100);

        if (v > 1) { 
            v = 1;
        } else if (v < 0) {
            v = 0;
        }

        return v;
    },

    /**
     * Sets the HSV value field.
     * 
     * @param {Number} v An HSV value, between 0 and 1.
     */
    setHsvValue: function(v) {
        var me = this,
            field = me.queryById('value-field');

        v = Math.round(v * 100);

        field.suspendEvent('change');
        field.setValue(v);
        field.resumeEvent('change');
    },

    /**
     * Gets the value of the RGB red field.
     * 
     * @return {Number} The RGB red value, between 0 and 255.
     */
    getRgbRed: function() {
        var me = this,
            field = me.queryById('red-field');

        return Ext.Number.from(field.getValue());
    },

    /**
     * Sets the RGB red field.
     * 
     * @param {Number} r An RGB red value, between 0 and 255.
     */
    setRgbRed: function(r) {
        var me = this,
            field = me.queryById('red-field');

        r = Math.round(r);

        field.suspendEvent('change');
        field.setValue(r);
        field.resumeEvent('change');
    },

    /**
     * Gets the value of the RGB green field.
     * 
     * @return {Number} The RGB green value, between 0 and 255.
     */
    getRgbGreen: function() {
        var me = this,
            field = me.queryById('green-field');

        return Ext.Number.from(field.getValue());
    },

    /**
     * Sets the RGB green field.
     * 
     * @param {Number} b An RGB green value, between 0 and 255.
     */
    setRgbGreen: function(g) {
        var me = this,
            field = me.queryById('green-field');

        g = Math.round(g);

        field.suspendEvent('change');
        field.setValue(g);
        field.resumeEvent('change');
    },

    /**
     * Gets the value of the RGB blue field.
     * 
     * @return {Number} The RGB blue value, between 0 and 255.
     */
    getRgbBlue: function() {
        var me = this,
            field = me.queryById('blue-field');

        return Ext.Number.from(field.getValue());
    },

    /**
     * Sets the RGB blue field.
     * 
     * @param {Number} b An RGB blue value, between 0 and 255.
     */
    setRgbBlue: function(b) {
        var me = this,
            field = me.queryById('blue-field');

        b = Math.round(b);

        field.suspendEvent('change');
        field.setValue(b);
        field.resumeEvent('change');
    },

    /**
     * Gets the hex value of the field.
     * 
     * @return {String} The hex value.
     */
    getHex: function() {
        var me = this,
            field = me.queryById('hex-field');

        return field.getValue();
    },

    /**
     * Sets the hex field.
     * 
     * @param {String} hex A hex value, without the preceding hash.
     */
    setHex: function(hex) {
        var me = this,
            field = me.queryById('hex-field');

        field.suspendEvent('change');
        field.setValue(hex);
        field.resumeEvent('change');
    },

    /**
     * Recalculates the values of the RGB fields based on the HSV values.
     */
    recalculateRgb: function() {
        var me = this,
            h = me.getHsvHue(),
            s = me.getHsvSaturation(),
            v = me.getHsvValue(),
            rgb = me.convertHsvToRgb(h, s, v);

        me.setRgbRed(rgb.r);
        me.setRgbGreen(rgb.g);
        me.setRgbBlue(rgb.b);
    },

    /**
     * Recalculates the values of the HSV fields based on the RGB values.
     */
    recalculateHsv: function() {
        var me = this,
            r = me.getRgbRed(),
            g = me.getRgbGreen(),
            b = me.getRgbBlue(),
            hsv = me.convertRgbToHsv(r, g, b);

        me.setHsvHue(hsv.h);
        me.setHsvSaturation(hsv.s);
        me.setHsvValue(hsv.v);
    },

    /**
     * Updates the color of the swatch underneath the saturation/vlaue widget based on the current
     * RGB values.
     */
    updateSwatch: function() {
        var me = this,
            swatch = me.queryById('swatch'),
            h = me.getHsvHue(),
            rgb = me.convertHsvToRgb(h, 1, 1),
            hex = me.convertRgbToHex(rgb.r, rgb.g, rgb.b);

        swatch.getEl().dom.style.background = hex;
    },

    /**
     * Updates the hex field based on the current RGB values.
     */
    updateHex: function() {
        var me = this,
            hexField = me.queryById('hex-field'),
            r = me.getRgbRed(),
            g = me.getRgbGreen(),
            b = me.getRgbBlue(),
            hex = me.convertRgbToHex(r, g, b);

        hexField.setRawValue(hex);
    },

    /**
     * Updates the hue/saturation/value widgets pointers' positions when the form values change.
     */
    updateHsvWidgets: function() {
        if (!this._rendered) {
            return;
        }

        var me = this,
            saturationValueWidget = me.queryById('saturation-value-widget'),
            saturationValuePointer = me.queryById('saturation-value-pointer'),
            hueWidget = me.queryById('hue-widget'),
            huePointer = me.queryById('hue-pointer'),
            h = me.getHsvHue(),
            s = me.getHsvSaturation(),
            v = me.getHsvValue();
        
        huePointer.setLocalY(hueWidget.getHeight() * (1 - h));
        saturationValuePointer.setLocalXY(saturationValueWidget.getWidth() * s, saturationValueWidget.getHeight() * (1 - v));
    },

    /**
     * Converts an HSV color value to an RGB color value.
     * 
     * @param {Number} h The hue, between 0 and 1.
     * @param {Number} s The saturation, between 0 and 1.
     * @param {Number} v The value, between 0 and 1.
     * 
     * @return {
     *      {Number} r The red value, between 0 and 255.
     *      {Number} g The green value, between 0 and 255.
     *      {Number] b The blue value, between 0 and 255.
     * }
     */
    convertHsvToRgb: function(h, s, v) {
        var r, g, b, C, X, m;

        C = v * s;
        X = C * (1 - Math.abs((h * 6) % 2 - 1));
        m = v - C;

        if (h >= 5/6) {
            r = C;
            g = 0;
            b = X;
        } else if (h >= 4/6) {
            r = X;
            g = 0;
            b = C;
        } else if (h >= 3/6) {
            r = 0;
            g = X;
            b = C;
        } else if (h >= 2/6) {
            r = 0;
            g = C;
            b = X;
        } else if (h >= 1/6) {
            r = X;
            g = C;
            b = 0;
        } else {
            r = C;
            g = X;
            b = 0;
        }

        return {
            r: Math.round((r + m) * 255),
            g: Math.round((g + m) * 255),
            b: Math.round((b + m) * 255)
        }
    },

    /**
     * Converts an RGB color value to an HSV color value.
     * 
     * @param {Number} r The red value, between 0 and 255.
     * @param {Number} g The green value, between 0 and 255.
     * @param {Number} b The blue value, between 0 and 255.
     * 
     * @return {
     *      {Number} h The hue, between 0 and 1.
     *      {Number} s The saturation, between 0 and 1.
     *      {Number] v The value, between 0 and 1.
     * }
     */
    convertRgbToHsv: function(r, g, b) {
        var me = this,
            rRatio = r / 255,
            gRatio = g / 255,
            bRatio = b / 255,
            Cmax, Cmin, d, h, s, v;

        Cmax = Math.max(rRatio, gRatio, bRatio);
        Cmin = Math.min(rRatio, gRatio, bRatio);
        d = Cmax - Cmin;

        v = Cmax;

        if (d == 0) {
            h = 0;
            s = 0;
        } else {
            if (Cmax == rRatio) {
                h = ((gRatio - bRatio) / d) + (gRatio < bRatio ? 6 : 0);
            } else if (Cmax == gRatio) {
                h = ((bRatio - rRatio) / d) + 2;
            } else {
                h = ((rRatio - gRatio) / d) + 4;
            }

            h = h * 60 / 360;
            s = d / Cmax;
        }

        return {
            h: h,
            s: s,
            v: v
        };
    },

    /**
     * Converts an RGB color value to a hex color string.
     * 
     * @param {Number} r The red value, from 0 to 255.
     * @param {Number} g The green value, from 0 to 255.
     * @param {Number} b The blue value, from 0 to 255.
     * 
     * @return {String} A #hex3 string for the color represented by the rgb coordinates that were
     * provided. 
     */
    convertRgbToHex: function(r, g, b) {
        var me = this,
            r = (r).toString(16),
            g = (g).toString(16)
            b = (b).toString(16);

        r = (r.length == 2 ? r : '0' + r);
        g = (g.length == 2 ? g : '0' + g);
        b = (b.length == 2 ? b : '0' + b);

        return '#' + r + g + b;
    },

    /**
     * Converts a hex string to an RGB color object.
     * 
     * @param {String} A #hex3 string to convert.
     * 
     * @return {
     *      {Number} r The red value, from 0 to 255.
     *      {Number} g The green value, from 0 to 255.
     *      {Number} b The blue value, from 0 to 255.
     * }
     */
    convertHexToRgb: function(hex) {
        var me = this,
            r,
            g,
            b;        

        if (hex.length == 7) {
            hex = hex.substr(1);
        }

        if (hex.length != 6) {
            Log.error('Unknown value: ' + hex);
            hex = 'ffffff';
        }

        r = parseInt(hex.substr(0, 2), 16);
        g = parseInt(hex.substr(2, 2), 16);
        b = parseInt(hex.substr(4, 2), 16);

        return {
            r: r,
            g: g,
            b: b
        };
    },

    /**
     * Fires a change event with the new hex value.
     */
    fireChangeEvent: function() {
        var me = this,
            hexField = me.queryById('hex-field'),
            hex = hexField.getValue();

        me.fireEvent('change', me.value, hex);
    },

    /**
     * Sets the picker value.
     * 
     * @param {String} v The new picker value, as a hex string.
     */
    setValue: function(v) {
        if (!v) {
            // The picker is sometimes initialized to empty string or something so don't do anything
            // in these cases.
            return;
        }

        var me = this,
            rgb = me.convertHexToRgb(v),
            hsv = me.convertRgbToHsv(rgb.r, rgb.g, rgb.b);

        me.setRgbRed(rgb.r);
        me.setRgbGreen(rgb.g);
        me.setRgbBlue(rgb.b);
        me.setHsvHue(hsv.h);
        me.setHsvSaturation(hsv.s);
        me.setHsvValue(hsv.v);

        me.updateHex();
        me.updateHsvWidgets();
    }
});