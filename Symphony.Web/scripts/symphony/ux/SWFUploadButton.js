
Ext.define('Ext.ux.SWFUploadButton', {
    extend: 'Ext.Button',
    alias: 'widget.swfuploadbutton',
    uploadUrl: '/uploader.aspx',
    flashUrl: '/scripts/swfupload/Flash/swfupload.swf',
    filePostName: 'data',
    fileTypes: '*.*',
    fileTypesDescription: 'All Files',
    fileSizeLimit: 1024 * 60, // 30 mb
    fileUploadLimit: 0,
    fileQueueLimit: 0,
    
    initComponent: function(){
        Ext.ux.SWFUploadButton.superclass.initComponent.call(this);
        this.addEvents(
            'dialogopen',
            'queue',
            'queueerror',
            'dialogclose',
            'uploadstart',
            'uploadprogress',
            'uploadsuccess',
            'uploaderror',
            'uploadcomplete',
            'allcomplete'
        );
    },
    destroy: function(){
        if(this.swfu){
            this.swfu.destroy();
        }
        Ext.ux.SWFUploadButton.superclass.destroy.call(this);
    },
    afterRender : function(){
        Ext.ux.SWFUploadButton.superclass.afterRender.call(this);
        // grab the middle element of the button
        var el = $('#' + this.id + ' .x-btn-wrap'); //Ext.query('#' + this.id + ' .x-btn-mc')[0];

        var middle = Ext.get($('#' + this.id + ' .x-btn-wrap')[0]); //Ext.get(Ext.id(el));
        
        // set position relative on the container
        middle.setStyle({
            position: 'relative'
        });
        
        // create 2 divs; the first will be positioned absolutely over the button,
        // and the second will be replaced by swfupload
        var id = Ext.id();
        var div = middle.insertFirst({
            tag: 'div',
            children: [{
                tag: 'div'
            }],
            id: id
        });
        
        // set the appropriate height and width for the button; really, we should
        // handle the resize event and make appropriate adjustments to this
        var height = middle.getHeight(), width = middle.getWidth();
        div.setStyle({ 
            position: 'absolute',
            height: height + 'px',
            width: width + 'px'
        });
        
        
        var relay = function(event, originalArguments){
            // we can't just do a "concat" because the stupid arguments property isn't a real array
            var finalArguments = [event];
            for(var i = 0; i < originalArguments.length; i++){
                finalArguments.push(originalArguments[i]);
            }
            this.fireEvent.apply(this, finalArguments);
        }.createDelegate(this);

        // http://demo.swfupload.org/Documentation/
        this.swfu = new SWFUpload({
            // Backend settings
            upload_url: this.uploadUrl,
            file_post_name: this.filePostName,

            // Flash file settings
            file_size_limit : this.fileSizeLimit,
            file_types : this.fileTypes,
            file_types_description : this.fileTypesDescription,
            file_upload_limit : this.fileUploadLimit,
            file_queue_limit : this.fileQueueLimit,

            // Event handler settings
            //swfupload_loaded_handler : swfUploadLoaded,
			
            file_dialog_start_handler: function(){ relay('dialogopen', arguments); },
            file_queued_handler : function(){ relay('queue', arguments); },
            file_queue_error_handler : function(){ relay('queueerror', arguments); },
            file_dialog_complete_handler : function(){ relay('dialogclose', arguments); },
			
            upload_start_handler :function(){ relay('uploadstart', arguments); },
            upload_progress_handler : function(){ relay('uploadprogress', arguments); },
            upload_error_handler : function(){ relay('uploaderror', arguments); },
            upload_success_handler : function(){ relay('uploadsuccess', arguments); },
            upload_complete_handler : function(){ relay('uploadcomplete', arguments); },

            // Button Settings
            button_placeholder_id : id,
            button_width: width,
            button_height: height,
            button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT,
            button_cursor : SWFUpload.CURSOR.HAND,
			
            // Flash Settings
            flash_url : this.flashUrl,
            
            // Debug settings
            debug: false
        });
        
    },
    uploadAll: function (callback) {
        var swfu = this.swfu, me = this;
        if(swfu.getStats().files_queued > 0){
            swfu.startUpload();
            this.on('uploadcomplete', function () {
			    if (swfu.getStats() && swfu.getStats().files_queued > 0) {
				    swfu.startUpload();
			    }else{
			        me.fireEvent('allcomplete');
			        if(callback){
			            callback();
			        }
			    }
            });
        }else{
            me.fireEvent('allcomplete');
            if(callback){
	            callback();
	        }
        }
    },
    startUpload: function (fileId) {
        this.swfu.startUpload(fileId);
    },
    removeFile: function (fileId) {
        this.swfu.cancelUpload(fileId);
    },
    addParameter: function (name, value) {
        this.swfu.addPostParam(name, value);
    },
    removeParameter: function (name) {
        this.swfu.removePostParam(name);
    },
    addFileParameter: function (fileId, name, value) {
        this.swfu.addFileParam(fileId, name, value);
    },
    removeFileParameter: function (fileId, name) {
        this.swfu.removeFileParam(fileId, name);
    },
    disable: function () {
        if(!this.rendered){
            this.on('afterrender', this.disable, this, { single: true });
            return;
        }
        Ext.ux.SWFUploadButton.superclass.disable.call(this);
        try{
            this.swfu.setButtonDisabled(false);
        }catch(e){}
    },
    enable: function () {
        if(!this.rendered){
            this.on('afterrender', this.enable, this, { single: true });
            return;
        }
        Ext.ux.SWFUploadButton.superclass.enable.call(this);
        try{
            this.swfu.setButtonDisabled(false);
        }catch(e){}
    },
    cancel: function (file, triggerError) {
        // cancels the first file in the queue; if we need the ability to cancel specifics, this method can take a file id
        this.swfu.cancelUpload(file, triggerError || false);
    },
    getStats: function () {
        return this.swfu.getStats();
    }
});