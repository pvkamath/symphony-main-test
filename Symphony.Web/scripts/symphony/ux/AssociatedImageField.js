﻿Ext.define('Ext.form.AssociatedImageField', {
    extend: 'Ext.form.field.Base',
    alias: 'widget.associatedimagefield',

    maxFileSize: 1024 * 300, // 300k max by default

    dataUrl: null,

    thumbnailWidth: 100,
    thumbnailHeight: 100,

    maxFiles: 1,

    /**
     * @config {Boolean} editable
     * If set to true, a dialog box will be presented to the user allowing them to edit the image.
     */
    editable: false,

    /**
     * @config {Boolean} noPreviewResize
     * If set to true, the preview will not be resized into a thumbnail. The preview will instead be
     * a full-size render of the image.
     */
    noPreviewResize: false,

    noDataText: '<span class="x-form-text x-form-textarea">Drag/drop an image here.<br/><br/>For best results, use square images.<span>',

    // Add specialkey listener
    initComponent: function () {
        var me = this,
            removeBtn;

/*        removeBtn = new Ext.button.Button({
            iconCls: 'x-button-cross',
            floating: true,
            ownerCt: me
        });
        removeBtn.show();*/

        Ext.apply(this, {
            fieldSubTpl: [
                '<div class="associated-image-field">',
                    '<div class="associated-image-instructions"><span class="x-form-text x-form-textarea">DROP HERE</span></div>',
                    '<div class="associated-image-no-data">' + me.noDataText + '</div>',
                    '<div class="associated-image-field-preview"></div>',
                    '<div class="associated-image-upload-progress">',
                        '<div class="associated-image-upload-progress-inner">',
                        '</div>',
                    '</div>',
                '</div>'
            ]
        });

        this.on('afterrender', function () {
            try{
                var dz = this.dz = new Dropzone(me.el.dom, {
                    editable: me.editable,
                    noPreviewResize: me.noPreviewResize,
                    thumbnailWidth: me.thumbnailWidth,
                    thumbnailHeight: me.thumbnailHeight,
                    maxFileSize: me.maxFileSize,
                    url: me.getUrl(),
                    previewsContainer: $(me.el.dom).find('.associated-image-field-preview')[0]
                });

                dz.on('thumbnail', function (f, dataUrl) {
                    me.setValue(dataUrl);
                    me.ownerCt.doLayout();
                    me.isValid();
                    me.fireEvent('thumbnail', f, dataUrl);
                });

                dz.on('addedfile', function (f) {
                    if (!me.editable) {
                        $(me.el.dom).find('.associated-image-no-data').hide();
                        $(me.el.dom).find('.associated-image-field-preview').show();

                        me.fireEvent('addedfile', f);
                    } else {
                        var window = new Ext.form.AssociatedImageFieldEditor({
                            dz: dz,
                            dataUrl: me.dz.getDataUri(),

                            callback: function(dataUrl) {
                                me.setValue(dataUrl);
                                me.ownerCt.doLayout();
                                me.isValid();

                                $(me.el.dom).find('.associated-image-no-data').hide();
                                $(me.el.dom).find('.associated-image-field-preview').show();

                                me.fireEvent('addedfile', f);
                            }
                        });
                        window.show();
                    }
                });

                dz.on('removedfile', function (f) {
                    $(me.el.dom).find('.associated-image-no-data').show();
                    $(me.el.dom).find('.associated-image-field-preview').hide();

                    me.fireEvent('removedfile', f);
                });

                dz.on('uploadprogress', function (file, percent, bytes) {
                    $(me.el.dom).find('.associated-image-upload-progress-inner').css({
                        width: percent + '%'
                    });
                });

                // default = hide preview
                $(me.el.dom).find('.associated-image-no-data').show();
                $(me.el.dom).find('.associated-image-field-preview').hide();
            
                if (me.dataUrl) {
                    me.setValue(me.dataUrl);
                }
            } catch (Exception) {
                // ignore it
            }
        });

        this.callParent();
    },
    setAssociationId: function (v) {
        if (!this.dz) {
            throw new Error('The field must be rendered before setting the association id explicitly.');
        }
        me.associationId = v;
        this.dz.options.url = this.getUrl();
    },
    getUrl: function () {
        return '/Uploaders/AssociatedImageUploader.ashx?association={0}&associationId={1}'.format(this.association, this.associationId)
    },
    upload: function () {
        this.dz.uploadFiles();
    },
    getValue: function () {
        return this.dataUrl;
    },
    getSubmitValue: function () {
        return this.dataUrl;
    },
    setValue: function (dataUrl) {
        var me = this;
        if (!this.rendered) {
            this.on('afterrender', function () {
                me.setValue(dataUrl);
            }, me, { single: true });
            return;
        }

        try{
            if (!dataUrl) {
                $(this.el.dom).find('.associated-image-no-data').show();
                $(this.el.dom).find('.associated-image-field-preview').hide();

            } else {
                $(this.el.dom).find('.associated-image-no-data').hide();

                var container = $(this.el.dom).find('.associated-image-field-preview');

                container.show();


                // http://en.wikipedia.org/wiki/Base64
                // we're going with base64 is ~37% larger, so multiply by .73 to get the original size
                // we're going rough here, doesn't matter if it's exact

                container[0].innerHTML = '';
                container.append([
                    '<div class="dz-preview">',
                        '<div class="dz-image">',
                            '<img data-dz-thumbnail alt="" src="{0}">',
                            '<img data-dz-remove src="/images/cross.png" class="image-delete-btn">',
                        '</div>',
                        '<div class="dz-details">',
                            '<div class="dz-size">{1}</div>',
                            '<div class="dz-filename">{2}</div>',
                            '<div class="dz-error">{3} </div>',
                        '</div>',
                    '</div>'
                ].join('').format(dataUrl, this.dz.filesize(dataUrl.length), 'filename.png', me._error));

                var removeBtn = $(this.el.dom).find('.image-delete-btn');
                removeBtn[0].addEventListener('click', function() {
                    me.setValue('');
                    me.ownerCt.doLayout();
                    me.fireEvent('deleted');
                });

                // Something in event flow prevents a resize from happening correctly. You can see
                // this if you have two associated images in a row and you add an error to the
                // top one.
                setTimeout(function() {
                    me.ownerCt.doLayout();
                }, 1);
            }
        } catch (err) {
            if (console.error) {
                console.error(err);
            } else if (console.log) {
                console.log(err);
            }
        }
        this.dataUrl = dataUrl;
    },

    _error: '',
    isValid: function() {
        var me = this,
            lengthRelative;
        
        if (me.dataUrl) {
            lengthRelative = me.dataUrl.length;

            if (me.dataUrl.length > me.maxFileSize) {
                me.updateError(Ext.String.format('Image is too large (max {0}).', me.dz.filesize(me.maxFileSize)));
                me.addCls('dz-invalid');

                return false;
            }

            me.updateError('');
            me.removeCls('dz-invalid');
        }

        return true;
    },

    updateError: function(error) {
        var me = this,
            el = me.getEl(),
            dom = el.query('.dz-error')[0];

        if (dom) {
            me._error = error;
            dom.innerHTML = error;
        }
    }
});

//TODO: file type validation
//TODO: dimensions - allow forced
//TODO: automatic image generation?