﻿// Custom proxy to allow proper serialization of filters
// The regular proxy only uses property/value when 
// serializing filters. 
//
// This will override the encodeFilter function to use
// the serialized version of the filters which pulls from
// initialConfig. This is probably what this function
// should have used in the first place. But leaving in 
// the option in case the filter is not an actual 
// Ext.util.Filter.
Ext.define('Symphony.ux.GenericFilterAjaxProxy', {
    extend: 'Ext.data.proxy.Ajax',
    encodeFilters: function(filters) {
        var min = [],
            length = filters.length,
            i = 0;

        for (; i < length; i++) {
            /* 
                If the filter has the serialize function, use it.
            */
            if (Ext.isFunction(filters[i].serialize)) {
                min[i] = filters[i].serialize(); 
            } else {
                // No serialize function, revert to default Ext implementation.
                min[i] = {
                    property: filters[i].property,
                    value   : filters[i].value
                };
            }
        }
        return this.applyEncoding(min);
    }
});