﻿Ext.define('Symphony.ux.TemplateParameterTree', {
    extend: 'symphony.searchabletree',
    alias: 'widget.symphony.templateparametertree',
    rootVisible: false,
    autoScroll: true,
    model: 'simple',
    root: {
        text: 'Template Parameters'
    },
    addTemplateParameter: function (target, text) {
        if (!text) {
            var node = this.getSelectionModel().getSelection()[0];
            if (!node || node.disabled || node.childNodes.length > 0) {
                Ext.Msg.alert('Invalid Parameter', 'You cannot add the selected parameter; try using one of it\'s child parameters.');
                return;
            }
            text = this.getTemplateParameterText(node);
        }

        if (target.xtype == 'htmleditor') {
            target.insertAtCursor(text);
        }  else if (target.xtype == 'symphony.ckeditorfield') {
            target.insert(text);
        } else {
            this.insertAtCaret(target.el.dom, text);
        }
    },
    insertAtCaret: function (el, text) {
        if (el.tagName != 'input') {
            var fly = Ext.fly(el),
                input = fly.down('input');
            if (input) {
                el = input.el.dom;
            }
        }

        if (document.selection) {
            el.focus();
            sel = document.selection.createRange();
            sel.text = text;
            el.focus();
        }
        else if (el.selectionStart || el.selectionStart == '0') {
            var startPos = el.selectionStart;
            var endPos = el.selectionEnd;
            var scrollTop = el.scrollTop;
            el.value = el.value.substring(0, startPos) + text + el.value.substring(endPos, el.value.length);
            el.focus();
            el.selectionStart = startPos + text.length;
            el.selectionEnd = startPos + text.length;
            el.scrollTop = scrollTop;
        } else {
            el.value += text;
            el.focus();
        }
    },
    getTemplateParameterText: function (node) {
        
        var me = this;
        if (node.disabled) {
            return false;
        }
        var nodes = [node];
        var parent = node;
        

        while ((parent = parent.parentNode) != null && parent.getDepth() > 0) {
            nodes.push(parent);
        }
        nodes.reverse();

        var cleanPath = function (s) {
            return s.replace('//', '').replace(/\//g, '.').replace(me.getRootNode().raw.text, '').replace('..', '');
        };

        var totalPath = [];
        var path = '{!';
        var parentCollection = null;
        var collectionCount = 0;
        Ext.each(nodes, function (node) {
            if (node.raw.isCollection) {
                var subPath = '';
                if (parentCollection != null) {
                    // nested collection
                    var currentPath = path.replace('{!', '');
                    subPath = currentPath + (node.raw.code ? node.raw.code : node.raw.text);
                } else {
                    // top-level collection
                    subPath = cleanPath(node.getPath('text'));
                }
                // track the foreach
                totalPath.push('{!foreach ' + subPath + '}');
                // track the parent
                parentCollection = node;
                // reset the path...we're starting one further
                // down in the tree on the next iteration
                path = '{!' + node.raw.singularText + '.';
                collectionCount++;
            } else {
                path += (node.raw.code ? node.raw.code : node.raw.text) + '.';
            }
        });
        // add the last element to the array
        totalPath.push(path.replace(/\.$/, '') + '}');

        // close out any collections
        for (var i = 0; i < collectionCount; i++) {
            totalPath.push("{!end}");
        }

        return totalPath.join('\r\n');
    },
    displayTemplateParameters: function (data) {
        var duplicatedData = JSON.parse(JSON.stringify(data));
        
        var store = Ext.create('Ext.data.TreeStore', {
            root: duplicatedData,
            proxy: {
                type: 'memory'
            },
            model: 'simple'
        });

        this.setRootNode(store.getRootNode());

        this.getRootNode().expandChildren();
    }
});