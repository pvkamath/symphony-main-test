(function(){
    var oldRender = Ext.form.Field.prototype.onRender;
    var oldFieldSetRenderer = Ext.form.FieldSet.prototype.initComponent;

    var renderTooltip = function (cmp) {
        var id = Ext.id(),
            cls = 'symphony-helpfield-icon';

        if (cmp.help.isCheckbox) {
            cls += ' is-checkbox';
        }

        cls += ' ' + cmp.xtype.replace(/\./g, '-');

        if (cmp.inputEl) {
            Ext.DomHelper.append(cmp.inputEl.parent(), {
                tag: 'div',
                id: id,
                cls: cls
            });

            Ext.QuickTips.register({
                autoHide: false,
                target: id,
                title: cmp.help.title || '',
                text: cmp.help.text || '',
                dismissDelay: 0
            });

            cmp.on('destroy', function () {
                Ext.QuickTips.unregister(id);
            });
        } else {
            cmp.title += Ext.DomHelper.createHtml({
                tag: 'span',
                id: id,
                cls: cls,
                'data-qtitle': cmp.help.title || '',
                'data-qtip': cmp.help.text || ''
            });
        }

        
    }

    Ext.override(Ext.form.FieldSet, {
        initComponent: function () {
            if (this.help) {
                renderTooltip(this);
            }
            oldFieldSetRenderer.apply(this, arguments);
        }
    });

    Ext.override(Ext.form.Field, {
        onRender: function(){
            oldRender.apply(this, arguments);
            
            if(this.help){
                renderTooltip(this);
            }
        }
    });
})();