﻿Ext.define('Symphony.ux.CKEditorField', {
    extend: 'Ext.form.field.TextArea',
    xtype: 'symphony.ckeditorfield',
    mixins: ['Symphony.mixins.FunctionOwnership'],

    /**
     * @config {Number} contentWidth
     * The width of the content area, in pixels.
     */
    contentWidth: 670,

    /**
     * @config {Number} contentHeight
     * The height of the content area, in pixels.
     */
    contentHeight: 910,

    _editor: null,
    _isLoaded: false,

    listeners: {
        boxready: function() {
            var me = this;

            // Loaded in SymphonyMaster.Master as a script tag.
            if (!CKEDITOR) {
                Log.error('CKEditor is not loaded.');
                return;
            }

            this.editorId = me.getInputId();

            this._editor = CKEDITOR.replace(this.editorId, {
                uiColor: '#99bce8',
                resize_enabled: false,

                imageBrowser_listUrl: '/scripts/symphony/Certificates/CertificateImagesJSON.ashx',

                toolbarGroups: [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph', 'imagebrowser'] },
                    { name: 'styles', groups: [ 'styles' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    { name: 'colors', groups: [ 'colors' ] },
                    { name: 'tools', groups: [ 'tools' ] },
                    { name: 'others', groups: [ 'others'] },
                    { name: 'about', groups: ['about'] }
                ],
                removeButtons: 'NewPage,Save,Preview,Print,Templates,Replace,Find,Form,Checkbox,Radio,TextField,Textarea,Select,ImageButton,HiddenField,Button,Language,BidiRtl,BidiLtr,Anchor,Flash,Smiley,PageBreak,Iframe,About,Cut,Copy,Paste,PasteText,PasteFromWord,SelectAll,Maximize,SpecialChar,Styles,Scayt'

            });

            this._editor.on('loaded', Ext.Function.createOwned(me, function () {
                // the 'me' here is NOT the same 'me' as the boxready 'me'. +1 for clarity. 
                me._isLoaded = true;
                me.handleResize();

                /*
                    The block below *should* load a saved bg image to the editor, but
                    'loaded' fires before the iframe is fully loaded and this throws an error.
                    So we Poll!!
                */

                var root = me.up('[xtype=certificates.certificatetemplatepanel]');
                var poller = setInterval(function () {
                    try {
                        if (root && root.model) {
                            var bgImagePath = root.model.get("bgImagePath");
                            if (bgImagePath) {
                                me.setBackgroundImage(bgImagePath);
                                clearInterval(poller);
                                return;
                            }
                        }
                    } catch (e) {
                        console.log("Load BG Failed: ", e);
                    }
                }, 100); // to see the error, set an interval of <= 1
            }));
        },

        resize: function() {
            Log.debug('Handling resize event on CKEditor.');
            this.handleResize();
        }
    },

    /**
     * Commits data from the CKEditor instance back to the textarea element. Used before saving
     * so that the data can be obtained automatically by ExtJS forms.
     */
    commit: function() {
        Log.info('Committing data from CKEditor back to textarea element.');
        this._editor.updateElement();
    },

    insert: function(str) {
        this._editor.insertHtml(str);
    },

    getData: function () {
        return this._editor.getData();
    },

    /**
     * Handles resize events. This is necessary because the height of the editor can change if a
     * resizes causes toolbar buttons to spill onto a new line.
     */
    handleResize: function() {
        var me = this;

        if (!me._isLoaded) {
            Log.debug('Delaying resize event, CKEditor has not loaded.');

            setTimeout(function() {
                me.handleResize();
            },1);

            return;
        }

        var el = me._editor.container.$,
            contentEl = Ext.DomQuery.selectNode('#' + el.id + ' .cke_contents'),
            iframeEl = Ext.DomQuery.selectNode('#' + el.id + ' .cke_wysiwyg_frame'),
            marginX;

        // Suspend or infinite loop occurs. This keeps the height constant when
        // the toolbar on ckeditor changes.
        me.suspendEvent('resize');
        me._editor.resize(me.getWidth() - me.labelWidth - 10, me.getHeight());
        //me.setHeight(el.offsetHeight);
        me.resumeEvent('resize');

        marginX = Math.floor((el.offsetWidth - me.contentWidth) / 2);
        marginY = Math.floor((el.offsetHeight - me.contentHeight) / 2);

        if (marginX < 0) {
            contentEl.style.overflowX = 'scroll';
            marginX = 0;
        } else {
            contentEl.style.overflowX = 'hidden';
        }

        if (marginY < 0) {
            contentEl.style.overflowY = 'scroll';
            marginY = 0;
        } else {
            contentEl.style.overflowY = 'hidden';
        }

        iframeEl.style.backgroundColor = '#999999';
        iframeEl.style.paddingLeft = marginX + 'px';
        iframeEl.style.paddingRight = marginX + 'px';
        iframeEl.style.paddingTop = marginY + 'px';
        iframeEl.style.paddingBottom = marginY + 'px';
        iframeEl.style.boxSizing = "border-box";

        iframeEl.style.width = '100%';
        iframeEl.style.height = '100%';

        // get the doc in the iframe, too
        var iframeContent = iframeEl.contentDocument.getElementsByTagName('html')[0];
        iframeContent.style.height = "100%";
        iframeContent.style.width = "100%";
        iframeContent.style.boxSizing = "border-box";
    },

    /**
     * Sets the width and height of the content.
     * @param {Number} w The width of the content.
     * @param {Number} h The height of the content.
     */
    setContentSize: function(w, h) {
        var me = this,
            msg;

        if (!w || w < 100) {
            msg ='Content width must be at least 100.';
            
            Log.error(msg);
            Ext.Error.raise(msg);
        }

        if (!h || h < 100) {
            msg ='Content height must be at least 100.';
            
            Log.error(msg);
            Ext.Error.raise(msg);
        }

        me.contentWidth = w;
        me.contentHeight = h;

        me.handleResize();
    },

    setBackgroundImage: function (imagePath) {
        //console.log("imagePath", imagePath);

        this.backgroundImagePath = imagePath;
        var currentEditor = CKEDITOR.instances[this.editorId];

        var docBody = currentEditor.document.getBody();
        if (imagePath) {
            docBody.setStyle("background-image", "url(" + imagePath + ")");
            docBody.setStyle("background-size", "cover");
            docBody.setStyle("background-repeat", "no-repeat");
            docBody.setStyle("background-position", "center center");
        } else {
            docBody.setStyle("background", "#FFF");
        }
    }
});