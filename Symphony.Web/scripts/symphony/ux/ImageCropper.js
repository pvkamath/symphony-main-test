﻿/**
 * Licence:
 * You can use the Code as you like, only the URL http//www.thomas-lauria.de/ has to be in the used Files / Code 
 * @author Thomas Lauria
 * http://www.thomas-lauria.de
 */

Ext.ns('Ext.ux');
Ext.define('Ext.ux.ImageCropper', {
    requires: ['Ext.Img'],
    extend: 'Ext.Component',
    alias: 'widget.symphony.imagecropper',
    minWidth: 50,
    minHeight: 50,
    quadratic: false,
    cropData: null,
    preserveRatio: true,
    autoEl: {
        tag: 'div',
        children: [{
            tag: 'div',
            cls: 'image-crop-wrapper',
            style: {
                background: '#ffffff',
                opacity: 0.5,
                position: 'absolute',
                '-ms-filter': "'progid:DXImageTransform.Microsoft.Alpha(Opacity=50)'",
                'filter': 'alpha(opacity=50)',
                '-moz-opacity': 0.5,
                '-khtml-opacity': 0.5
            }
        }]
    },
    initComponent: function () {
        this.preserveRatio = this.quadratic || this.preserveRatio;
        this.callParent(arguments);
        this.images = [];

        this.maxWidth = this.width;
        this.maxHeight = this.height;

        this.cropData = this.cropData || [{
            x: 30,
            y: 30,
            height: 175,
            width: 175
        }];
    },
    getResultPosition: function (imageEl) {
        var me = this,
           parent = me.getBox(),
           img = imageEl.getBox(),
           res = {
               x: (img.x - parent.x),
               y: (img.y - parent.y),
               width: img.width,
               height: img.height
           };
        imageEl.setStyle({
            'background-position': (-res.x) + 'px ' + (-res.y) + 'px'
        });
        return res;
    },
    /**
     * @return Object array
     */
    getCropData: function () {
        var cropData = [];
        for (var i = 0; i < this.images.length; i++) {
            cropData.push(this.getResultPosition(this.images[i].el));
        }
        return cropData;
    },
    onRender: function (ct, position) {
        var me = this,
           height = me.height,
           width = me.width;

        me.wrap = me.el.down('.image-crop-wrapper');

        me.wrap.setSize(me.width, me.height);

        me.el.setStyle({
            background: 'url(' + me.src + ') no-repeat left top',
            'background-size': me.width + 'px ' + me.height + 'px'
        });
        if (me.quadratic) {
            if (height > width) {
                height = width;
            }
            else {
                width = height;
            }
        }

        this.initialX = this.cropData[0].x;
        this.initialY = this.cropData[0].y;
        this.initialWidth = this.cropData[0].width;
        this.initialHeight = this.cropData[0].height;

        for (var i = 0; i < this.cropData.length; i++) {
            this.createResizable(this.cropData[i].x, this.cropData[i].y, this.cropData[i].width, this.cropData[i].height);
        }

        Ext.create('Ext.Img', {
            renderTo: me.id,
            src: me.src,
            listeners: {
                afterrender: function (img) {
                    img.mon(img.getEl(), 'load', function (e) {
                        me.setImage(me.src, img.getWidth(), img.getHeight());
                        img.hide();
                    });
                }
            }
        });

        this.callParent();
    },
    setImage: function (src, imgWidth, imgHeight) {
        // determine scale
        var resultWidth = this.maxWidth;
        var resultHeight = this.maxHeight;

        if (imgWidth > imgHeight) {
            resultHeight = (imgHeight / imgWidth) * resultWidth;
        } else if (imgHeight > imgWidth) {
            resultWidth = (imgWidth / imgHeight) * resultHeight;
        } 

        for (var i = 0; i < this.images.length; i++) {
            this.images[i].el.setStyle({ 'background': 'url(' + src + ')', 'background-size': resultWidth + 'px ' + resultHeight + 'px' });
            this.getResultPosition(this.images[i].el);
        }

        this.setWidth(resultWidth);
        this.setHeight(resultHeight);
        
        this.src = src;
        this.el.setStyle({
            background: 'url(' + src + ') no-repeat left top',
            width: resultWidth + 'px',
            height: resultHeight + 'px',
            'background-size': resultWidth + 'px ' + resultHeight + 'px'
        });
    },
    setCropData: function (dims) {
        // expect { x, y, width, height } format
        //TODO: sets
        if (dims.length != this.images.length) {
            throw new Error('Dimensions and sets lengths must be equal when setting crop data.');
        }
        for (var i = 0; i < dims.length; i++) {
            this.images[i].resizer.getEl().setLeftTop(dims[i].x, dims[i].y);
            // adjust the resizable size
            this.images[i].resizer.resizeTo(dims[i].width, dims[i].height);

            this.getResultPosition(this.images[i].el);
        }
    },
    addCropper: function () {
        this.createResizable(this.initialX, this.initialY, this.initialWidth, this.initialHeight);
    },
    reset: function () {
        // remove all but the first one
        for (var i = 1; i < this.images.length; i++) {
            var image = this.images[i];
            image.destroy();
        }

        this.images = [this.images[0]];

        this.setCropData([{
            width: this.initialWidth,
            height: this.initialHeight,
            x: this.initialX,
            y: this.initialY
        }]);

    },
    createResizable: function (x, y, width, height) {
        var me = this;

        me.images.push(Ext.create('Ext.Img', {
            opacity: 1.0,
            renderTo: me.el,
            resizable: {
                pinned: true,
                preserveRatio: me.preserveRatio
            },
            draggable: {
                constrain: true,
                constrainTo: me.el,
                listeners: {
                    dragstart: function (draggable, e, opts) {
                        var img = Ext.fly(e.target);
                        img.setStyle({
                            'background': 'transparent'
                        });
                    },
                    dragend: function (draggable, e, opts) {
                        var img = Ext.fly(e.target);
                        var res = me.getResultPosition(img);

                        img.setStyle({
                            'background-image': 'url(' + me.src + ')',
                            'background-size': me.width + 'px ' + me.height + 'px',
                            'background-repeat': 'no-repeat'
                        });

                        me.fireEvent('changeCrop', me, res);
                        me.fireEvent('moveCrop', me, res);
                        me.fireEvent('change', me, me.getCropData());
                    }
                }
            },
            constrainTo: me.el,
            src: Ext.BLANK_IMAGE_URL,
            height: height,
            width: width,
            x: x,
            y: y,
            style: {
                cursor: 'move',
                position: 'absolute',
                background: 'url(' + me.src + ') no-repeat left top',
                'background-size': me.width + 'px ' + me.height + 'px'
            },
            listeners: {
                resize: function () {
                    res = me.getResultPosition(this.el);
                    me.fireEvent('changeCrop', me, res);
                    me.fireEvent('resizeCrop', me, res);

                    me.fireEvent('change', me, me.getCropData());
                }
            }
        }));
    },
    repositionCropperImage: function () {
        var ebox = this.getEl().getBox();
        for (var i = 0; i < this.sets.length; i++) {
            var set = this.sets[i];
            var rbox = set.resizable.getEl().getBox();
            // +6 is to account for the padding around the resizer
            set.cropperImage.setStyle({
                left: -(rbox.x - ebox.x + 6) + 'px',
                top: -(rbox.y - ebox.y + 6) + 'px'
            });
        }
    }
});