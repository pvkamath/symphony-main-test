﻿Ext.define('Ext.form.AssociatedImageFieldEditor', {
    extend: 'Ext.Window',
    xtype: 'associatedimagefieldeditor',
    modal: true,

    title: 'Drag the sides to resize your image',

    items: [{
        xtype: 'image',
        itemId: 'image',
        cls: 'unselectable'
    }],

    listeners: {
        boxready: function() {
            var me = this,
                image = me.queryById('image');

            me.setHeight(image.getHeight() + 21 + 12 + 26);
            me.setWidth(image.getWidth() + 12);
        },
        
        resize: function(me, width, height) {
            var imgWidth = width - 12,
                imgHeight = height - 21 - 12 - 26;

            me.resizeImage(imgWidth, imgHeight);
        }
    },

    tbar: [{
        xtype: 'button',
        itemId: 'revert-button',
        iconCls: 'x-button-revert',
        text: 'Revert',

        handler: function() {
            var me = this.up('[xtype=associatedimagefieldeditor]');
            me.revertImage();
        }
    }, '->', {
        xtype: 'button',
        itemId: 'save-button',
        iconCls: 'x-button-save',
        text: 'Save',

        handler: function() {
            var me = this.up('[xtype=associatedimagefieldeditor]')
                image = me.queryById('image');

            me.close();
            me.callback(me._value);
        }
    }],

    initComponent: function() {
        var me = this,
            image;

        me.callParent(arguments);

        me._value = me.dz.getDataUri();

        image = me.queryById('image');
        image.setSrc(me._value);
    },
    
    revertImage: function() {
        var me = this;

        me.resizeImage();
    },

    resizeImage: function(width, height) {
        var me = this,
            image = me.queryById('image');

        me._value = me.dz.getDataUri(width, height);
        image.setSrc(me._value);

        me.suspendEvent('resize');
        me.setHeight(image.getHeight() + 21 + 12 + 26);
        me.setWidth(image.getWidth() + 12);
        me.resumeEvent('resize');

        // There is some bug where the window is getting moved after a resize. Unsure what
        // event to listen for here, so this is okay for now.
        setTimeout(function() {
            me.setX(Math.round((window.innerWidth - me.getWidth()) / 2));
            me.setY(Math.round((window.innerHeight - me.getHeight()) / 2));
        }, 1);
    }
});