﻿Ext.define('Ext.ux.MoneyField', {
    extend: 'Ext.form.TextField',
    alias: 'widget.moneyfield',
    minValue: 0,
    initComponent: function(){
        this.maskRe = /[0-9\$\.]/;

        if (!this.formatDelay) {
            this.formatDelay = 500;
        }

        var formatFieldTask = new Ext.util.DelayedTask(function(newValue) {
            this.setValue(newValue);
        })

        Ext.apply(this, {
            listeners: {
                change: function (field, newValue, oldValue) {
                    formatFieldTask.delay(this.formatDelay, null, this, [newValue]);
                }
            }
        });
        Ext.ux.MoneyField.superclass.initComponent.apply(this, arguments);
    },
    setValue: function(newValue){
        var oldValue = this.getValue();
        if(this.allowBlank && newValue == ''){
            Ext.ux.MoneyField.superclass.setValue.call(this, '');
            return;
        }
        newValue = Ext.util.Format.usMoney((newValue + '').replace(/[^0-9\.]/g, ''));
        if(newValue == '$NaN.00'){
            Ext.ux.MoneyField.superclass.setValue.call(this, oldValue);
        }else{
            Ext.ux.MoneyField.superclass.setValue.call(this, newValue); 
        }
    },
    getValue: function(){
         var text = Ext.ux.MoneyField.superclass.getValue.call(this); 
         return (text + '').replace(/[^0-9\.]/g, '');
    }
});