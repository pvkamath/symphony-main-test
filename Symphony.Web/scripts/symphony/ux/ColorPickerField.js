﻿Ext.define('Ext.ux.ColorPickerField', {
    extend: 'Ext.form.field.Picker',
    xtype: 'colorpickerfield',
    requires: [
        'Ext.ux.ColorPicker'
    ],

    editable: false,
    fieldStyle: {
        cursor: 'pointer'
    },
    listeners: {
        boxready: function() {
            // Need to refresh after the component is drawn or it will be white.
            this.refreshColors();
        }
    },

    initComponent: function() {
        var me = this;

        me.callParent(arguments);

        var cmp = Ext.create('widget.component', {
            floating: true,
            border: true,
            width: 20,
            height: 20,
            style: {
                backgroundColor: 'red'
            }
        });
    },

    /**
     * Creates the picker.
     */
    createPicker: function() {
        var me = this;
        
        me.picker = new Ext.ux.ColorPicker({
            xtype: 'colorpicker2',
            floating: true,
            value: '#ff0eff'
        });

        me.mon(me.picker, {
            change: function(picker, newValue, oldValue) {
                me.setValue(newValue);
                me.refreshColors();
            }
        });

        return me.picker;
    },

    /**
     * Gets an appropriate foreground text color, given a background color.
     * 
     * @param {String} hex The background color to match, a #hex3 string.
     * 
     * @return {String} A #hex3 string that will match with the background color.
     */
    getTextColor: function(hex) {
        var r, g, b, a;

        if (hex.length == 7 && hex[0] == '#') {
            hex = hex.substr(1);
        }
        if (hex.length != 6) {
            Log.error('Unknown format: ' + hex);
            return '#000000';
        }

        r = parseInt(hex.substr(0, 2), 16);
        g = parseInt(hex.substr(2, 2), 16);
        b = parseInt(hex.substr(4, 2), 16);

        // perceptive luminance, search on s/o if curious.
        a = 1 - (0.299 * r + 0.587 * g + 0.144 * b) / 255;

        if (a < 0.5) {
            return '#000000';
        } else {
            return '#ffffff';
        }
    },

    setValue: function(v) {
        var me = this,
            picker = me.getPicker();

        me.callParent(arguments);

        picker.setValue(v);
        me.refreshColors();
    },

    /**
     * Based on the field's current value, refreshes the colors of the field.
     */
    refreshColors: function() {
        var me = this,
            v = me.getValue();

        if (!me.inputEl) {
            return;
        }

        me.inputEl.dom.style.background = v;
        me.inputEl.dom.style.color = me.getTextColor(v);
    }
});