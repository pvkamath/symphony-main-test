﻿Symphony.Instructors.TrainingProgramDetailsPanel = Ext.define('instructors.trainingprogramdetailspanel', {
    alias: 'widget.instructors.trainingprogramdetailspanel',
    extend: 'Ext.Panel',
    defaultStartDate: null,
    defaultDueDate: null,
    initComponent: function () {
        var me = this;

        var scheduleDateRenderer = function (value, meta, record, rowIndex, alternateValue) {

            if (!value || Symphony.parseDate(value).getFullYear() == Symphony.defaultYear) {
                if (alternateValue && record.get(alternateValue)) {
                    var minutes = record.get(alternateValue),
                        after = alternateValue == 'activeAfterMinutes' ? ' after training program start' : ' after course launch',
                        returnText = Symphony.Classroom.durationRenderer(minutes) + after;

                    return returnText;
                } else {
                    var defaultDate;

                    if (alternateValue == 'activeAfterMinutes') {
                        defaultDate = me.defaultStartDate;
                    } else if (alternateValue = 'expiresAfterMinutes') {
                        defaultDate = me.defaultDueDate;
                    }

                    if (defaultDate.getFullYear() != Symphony.defaultYear) {
                        return Symphony.dateRenderer(defaultDate);
                    }
                }

                return 'N/A';
            }

            return Symphony.dateRenderer(value);
        }

        var tpColModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: scheduleDateRenderer
            },
            columns: [
                { header: 'Start Date', dataIndex: 'startDate'},
                { header: 'Due Date', dataIndex: 'dueDate' },
                { header: 'End Date', dataIndex: 'endDate' }
            ]
        });

        var ocColModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: scheduleDateRenderer
            },
            columns: [
                {
                    header: 'Active After Date', dataIndex: 'activeAfterDate', renderer: function (value, meta, record, rowIndex) {
                        return scheduleDateRenderer(value, meta, record, rowIndex, 'activeAfterMinutes');
                    }},
                {
                    header: 'Due Date', dataIndex: 'dueDate', renderer: function (value, meta, record, rowIndex) {
                        return scheduleDateRenderer(value, meta, record, rowIndex, 'expiresAfterMinutes');
                    }}
            ]
        });


        Ext.apply(this, {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'symphony.localgrid',
                flex: 0.5,
                title: 'Training Program Schedule',
                name: 'trainingProgramSchedule',
                colModel: tpColModel,
                model: 'trainingProgram',
                border: false
                /*viewConfig: {
                    forceFit: true
                }*/
            }, {
                xtype: 'symphony.localgrid',
                title: 'Online ' + Symphony.Aliases.course + ' Schedule',
                name: 'onlineCourseSchedule',
                border: false,
                flex: 0.5,
                autoScroll: true,
                colModel: ocColModel,
                model: 'course'
                /*viewConfig: {
                    forceFit: true
                }*/
            }]
        });


        this.callParent(arguments);
    },
    setData: function (data) {
        var allCourses = data.requiredCourses.concat(data.optionalCourses, data.electiveCourses, data.finalAssessments),
            tpSchedule = this.find('name', 'trainingProgramSchedule')[0],
            ocSchedule = this.find('name', 'onlineCourseSchedule')[0],
            course;

        for (var i = 0; i < allCourses.length; i++) {
            if (allCourses[i].courseTypeId === Symphony.CourseType.online &&
                    allCourses[i].id === this.courseId) {
                course = allCourses[i];
                break;
            }
        }

        var startDate = Symphony.parseDate(data.startDate);
        var dueDate = Symphony.parseDate(data.dueDate);
        var endDate = Symphony.parseDate(data.endDate);

        if (dueDate.getFullYear() == Symphony.defaultYear) {
            dueDate = endDate;
        }

        this.defaultDueDate = dueDate;
        this.defaultStartDate = startDate;
        
        tpSchedule.setData([data]);
        ocSchedule.setData([course]);
    }
});
