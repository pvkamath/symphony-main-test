﻿(function () {
    if (!Symphony.Instructors) {
        Symphony.Instructors = {};
    }

    

    Symphony.Instructors.App = Ext.define('instructors.app', {
        alias: 'widget.instructors.app',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                border: false,
                items: [{
                    split: true,
                    collapsible: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    title: 'My ' + Symphony.Aliases.courses,
                    xtype: 'instructors.instructedclassesgrid',
                    readOnly: true,
                    ref: 'mycoursesgrid',
                    listeners: {
                        rowclick: function (grid, rowIndex, e) {
                            if (e.target.getAttribute('symphony:method')) {
                                Symphony.Portal.linkHandler(grid, rowIndex, e);
                            } else {
                                var record = grid.store.getAt(rowIndex);
                                var panelId = record.data.courseTypeId + "_" + record.data.id + "_" + record.data.trainingProgramId;

                                me.addPanel(panelId, record.data.name, record);
                            }
                        },
                        groupclick: function (grid, groupField, groupValue, e) {
                            if (e.target.getAttribute('symphony:method')) {
                                Symphony.Portal.linkHandler(grid, 0, e);
                            }
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    itemId: 'instructors.classcontainer'
                }],
                listeners: {
                    activate: {
                        fn: function (panel) {
                            panel.find('xtype', 'instructors.instructedclassesgrid')[0].refresh();
                        },
                        single: true
                    }
                }
            });

            this.callParent(arguments);
        },
        addPanel: function (id, name, record) {
            var me = this;
            var tabPanel = this.query('[itemId=instructors.classcontainer]')[0];
            //get all the classforms
            var classForm = tabPanel.query('[itemId=' + id + ']'); // ('xtype', 'classroom.classform');
            if (classForm.length) {
                tabPanel.activate(classForm[0]);
            } else {
                var panel = tabPanel.add({
                    xtype: 'instructors.classform',
                    closable: true,
                    activate: true,
                    itemId: id,
                    title: name,
                    tabTip: name,
                    classId: record.data.id,
                    classTypeId: record.data.courseTypeId,
                    trainingProgramId: record.data.trainingProgramId
                });
                panel.show();
            }
            tabPanel.doLayout();
        }
    });

})();