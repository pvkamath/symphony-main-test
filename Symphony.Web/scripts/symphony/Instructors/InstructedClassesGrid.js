﻿Symphony.Instructors.InstructedClassesGrid = Ext.define('instructors.instructedclassesgrid', {
    alias: 'widget.instructors.instructedclassesgrid',
    extend: 'symphony.searchablegrid',
    readOnly: false,
    initComponent: function () {
        var me = this;
        var url = '/services/classroom.svc/instructedClasses/';

        var proxy = new Ext.data.HttpProxy({
            method: 'GET',
            url: url,
            reader: new Ext.data.JsonReader({
                idProperty: 'none',
                totalProperty: 'totalSize',
                root: 'data'
            })
        });


        var store = new Ext.data.Store({
            //remoteSort: true, // Remote sort will be set on refresh so we can avoid having this store automatically load 
            proxy: proxy,
            //baseParams: { limit: 20 },
            model: 'instructedClass',
            //sorters: [{ field: 'groupByTrainingProgramId', direction: "ASC" }],
            sortInfo: {
                field: 'trainingprogramname', direction: "ASC"
            },
            groupField: 'groupByTrainingProgramId'
        });

        me.store = store;

        var columns = [
            {
                header: ' ',
                dataIndex: 'courseTypeId', width: 30, align: 'center',
                renderer: Symphony.Portal.courseTypeRenderer
            }, // course type icons
            {
                /*id: 'name',*/ header: 'Name', dataIndex: 'name', width: 140, align: 'left',
                flex: 1,
                doSort: function (state) {
                    var ds = this.up('tablepanel').store;
                    ds.sort({
                        property: 'trainingprogramname',
                        direction: state
                    });
                }
            },
            { /*id: 'messageBoard',*/ header: '', dataIndex: 'messageBoardId', width: 30, fixed: true, align: 'right', renderer: Symphony.Portal.MessageBoardRenderer }
        ]

        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: columns
        });

        Ext.apply(this, {
            idProperty: 'id',
            cls: 'groupedCourseGrid',
            border: false,
            deferLoad: true,
            url: url,
            colModel: colModel,
            model: 'instructedClass',
            store: store,
            minColumnWidth: 30,
            features: Ext.create('Ext.grid.feature.Grouping', {
                collapsible: true,
                forceFit: true,
                showGroupName: false,
                enableGroupingMenu: false,
                enableNoGroups: false,

                groupHeaderTpl: [
                    '{children:this.renderHeader}',
                    {
                        renderHeader: function (children) {
                            if (children.length) {
                                var record = children[0];
                                if (record.data.trainingProgramName) {
                                    return String.format(
                                        '<span class="instructed-class-group {0}"><div class="icons">{1}{2}</div><div class="header">{3}</div></span>',
                                        record.get('hasUnmarkedAssignments') ? 'unmarked' : 'marked',
                                        Symphony.Portal.LaunchAssignmentsRenderer(record.get('groupByTrainingProgramId'), {}, { data: { trainingProgramId: record.data.groupByTrainingProgramId, courseId: 0, qTip: record.get('hasUnmarkedAssignments') ? 'View Assignments (Grading Required)' : '' } }),
                                        Symphony.Portal.MessageBoardRenderer(record.get('groupByTrainingProgramId'), {}, { data: { messageBoardId: record.data.trainingProgramMessageBoardId } }),
                                        Symphony.qtipRenderer(record.data.trainingProgramName)
                                    );
                                } else {
                                    return 'Classes I Instruct'
                                }
                            }
                        }
                    }
                ]
            })
        });
        this.callParent(arguments);
    },
    refresh: function () {
        this.store.remoteSort = true;
        this.callParent();
    }
});
