﻿Symphony.Instructors.OnlineCourseStudentGrid = Ext.define('instructors.onlinecoursestudentgrid', {
    alias: 'widget.instructors.onlinecoursestudentgrid',
    extend: 'symphony.searchablegrid',
    courseId: 0,
    trainingProgramId: 0,
    showScores: true,
    isAutoRefreshRunning: false,
    initComponent: function () {
        var me = this;
        me.url = '/services/courseassignment.svc/trainingprograms/' + this.trainingProgramId + '/onlinecourseregistrations/' + this.courseId + '/';
        
        var proxy = new Ext.data.HttpProxy({
            method: 'GET',
            url: me.url,
            reader: new Ext.data.JsonReader({
                idProperty: 'none',
                totalProperty: 'totalSize',
                root: 'data'
            })
        });
        
        var store = new Ext.data.Store({
            simpleSort: true,
            proxy: proxy,
            //baseParams: { limit: 20 },
            model: 'onlineCourseRollup',
            sortInfo: {
                field: 'startTime', direction: 'DESC'
            },
            remoteGroup: true,
            remoteSort: true,
            groupField: 'classId'
        });

        me.store = store;

        var columns = [
            {
                /*id: 'registrant',*/ header: 'Registrant', dataIndex: 'fullName', align: 'left',
                flex: 1
            }
        ];

        var videoChatColumn = [{ header: ' ', dataIndex: 'userId', renderer: Symphony.Classroom.videoChatRenderer, width: 28 }];
        var messageColumn = [{ header: ' ', dataIndex: 'userId', renderer: Symphony.Classroom.messageStudentRenderer, width: 28 }];
        
        var scoreColumns = [{ /*id: 'score',*/ header: 'Score', dataIndex: 'score', align: 'center', width: 180 },
            { /*id: 'attemptCount',*/ header: 'Attempts', dataIndex: 'attemptCount', align: 'center', width: 180 },
            {
                type: 'checkbox', /*id: 'canMoveForward',*/ header: 'Can Move Forward', dataIndex: 'canMoveForwardIndicator', width: 120, renderer: function (value) {
                    if (!value) {
                        return '<input type="checkbox"></input>';
                    }
                    return '<input type="checkbox" checked="checked"></input>';
                }
            }];

        var timeColumn = [{
            /*id: 'remaining',*/ header: 'Time Remaining', dataIndex: 'remaining', align: 'center', width: 180, renderer: function (value, meta, record) {
                var expiresAfterMinutes = record.get("expiresAfterMinutes"),
                    unit = Symphony.Classroom.getDurationUnits(expiresAfterMinutes),
                    display;

                if (expiresAfterMinutes <= 0) {
                    return '-';
                }

                if (value <= 0) {
                    return 'Expired';
                }

                if (unit != "minutes" && value >= 60) {
                    value = Math.ceil(value / 60) * 60;
                }

                display = Symphony.Classroom.durationRenderer(value);


                return display;
                
            }
        }, {
            /*id: 'extend',*/
            header: '',
            width: 55,
            dataIndex: 'extend',
            renderer: function (value, meta, record) {
                var id = Ext.id();
                if (record.get("expiresAfterMinutes") <= 0) {
                    return '';
                }
                return '<a href="#" id="' + id + '">Extend Time</a>';
            }
        }];

        if (me.showScores) {
            columns = messageColumn.concat(columns).concat(scoreColumns);
            if (Symphony.Portal.hasPermission(Symphony.Modules.VideoChat)) {
                columns = videoChatColumn.concat(columns);
            }
        }

        columns = columns.concat(timeColumn);
        
        var colModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                align: 'center',
                renderer: Symphony.Portal.valueRenderer
            },
            columns: columns
        });



        Ext.apply(this, {
            store: store,
            features: Ext.create('Ext.grid.feature.Grouping', {
                forceFit: true,
                showGroupName: false,
                groupHeaderTpl: [
		            '{children:this.renderHeader}',
		            {
		                renderHeader: function (children) {
		                    if (children.length) {
		                        var record = children[0];
		                        if (record.get('classId') > 0) {

		                            var longDate = Symphony.parseDate(record.get('classStartTime')).formatSymphony('D, F j, Y g:i a');
		                            var shortDate = Symphony.parseDate(record.get('classStartTime')).formatSymphony('m/d/Y');
		                            var title = String.format('{0} - {1}', record.get('className'), shortDate)
		                            var qTip = Symphony.qtipRenderer(title, longDate);

		                            return String.format('<span data-classid="{0}">{1}</span>',
                                        record.get('classId'),
                                        qTip);
		                        }

		                        return 'No Session';
		                    }
		                }
		            }
                ]
            }),
            tbar: {
                items: [{
                    xtype: 'button',
                    iconCls: 'x-button-save',
                    text: 'Save Student Status',
                    handler: function () {
                        var data = me.getData(),
                            postData = [];

                        for (var i = 0; i < data.length; i++) {
                            postData.push(data[i].data);
                        }

                        me.save(postData);
                    }
                }]
            },
            searchColumns: ['fullName'],
            idProperty: 'userId',
            url: me.url,
            colModel: colModel,
            model: 'onlineCourseRollup',
            listeners: {
                cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                    var column = grid.getColumnModel().getColumnAt(columnIndex),
                        record = grid.getStore().getAt(rowIndex);

                    if (column.type && column.type === 'checkbox' && e.target.tagName.toLowerCase() === 'input') {
                        record.data.canMoveForwardIndicator = e.target.checked;
                    }

                    if (column.id == 'extend') {
                        var menu = new Ext.menu.Menu({
                            items: [{
                                text: 'Extend time',
                                menu: {
                                    items: [{
                                        xtype: 'panel',
                                        frame: true,
                                        items: [{
                                            xtype: 'panel',
                                            layout: 'column',
                                            padding: '5px',
                                            cls: 'inline-form',
                                            items: [{
                                                xtype: 'label',
                                                text: "Extend course time by ",
                                                cls: 'x-form-item',
                                                labelStyle: 'padding-top: 5px'
                                            }, {
                                                name: 'duration',
                                                fieldLabel: '',
                                                labelWidth: 0,
                                                ref: '../../duration',
                                                xtype: 'classroom.durationfield',
                                                style: 'z-index: 99999',
                                                cls: 'hour-minute-selector',
                                                width: 120
                                            }]
                                        }, {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            border: false,
                                            items: [{
                                                xtype: 'button',
                                                text: 'OK',
                                                width: 40,
                                                cls: 'daterange-button',
                                                style: 'margin-right: -5px',
                                                handler: function (btn) {
                                                    var durationField = btn.ownerCt.ownerCt.ownerCt.duration,
                                                        durationValue = durationField.getValue() > 0 ? durationField.getValue() : null,
                                                        trainingProgramId = record.get('trainingProgramId'),
                                                        userId = record.get('userId'),
                                                        courseId = record.get('courseId');

                                                    Symphony.Ajax.request({
                                                        url: '/services/courseassignment.svc/trainingprograms/extension/' + trainingProgramId + '/' + courseId + '/' + userId + '/' + durationValue,
                                                        method: 'POST',
                                                        success: function () {
                                                            me.refresh();
                                                        }
                                                    });

                                                    menu.hide();
                                                }
                                            }]
                                        }]
                                    }]
                                }
                            }, {
                                text: 'Lock course',
                                handler: function() {
                                    var trainingProgramId = record.get('trainingProgramId'),
                                        userId = record.get('userId'),
                                        courseId = record.get('courseId');

                                    Symphony.Ajax.request({
                                        url: '/services/courseassignment.svc/trainingprograms/extension/' + trainingProgramId + '/' + courseId + '/' + userId,
                                        method: 'DELETE',
                                        success: function () {
                                            me.refresh();
                                        }
                                    });

                                    menu.hide();
                                }
                            }]
                        });
                        menu.showAt(e.target.getX(), e.target.getY());
                    }
                },
                render: function () {
                    me.startAutoRefresh();
                },
                destroy: function () {
                    me.stopAutoRefresh();
                }
            }
        });
        this.callParent(arguments);
    },
    getData: function (args) {
        return this.store.getRange();
    },
    getAllData: function (callback) {
        // get all data without paging
        Symphony.Ajax.request({
            method: 'GET',
            url: this.url,
            success: function (args) {
                callback(args);
            }
        });
    },
    save: function (data) {
        var me = this;

        Symphony.Ajax.request({
            url: me.url,
            method: 'post',
            jsonData: data,
            success: function (result) {
                me.refresh();
            }
        });        
    },
    startAutoRefresh: function () {
        this.isAutoRefreshRunning = true;
        this.autoRefresh();
    },
    stopAutoRefresh: function () {
        this.isAutoRefreshRunning = false;
    },
    autoRefresh: function () {
        var me = this;
        if (me.isAutoRefreshRunning) {
            try {
                me.store.reload();
                setTimeout(function () {
                    me.autoRefresh();
                }, 60000);
            } catch (e) {
                // The grid was probably destroyed
            }
        }
    }
});
