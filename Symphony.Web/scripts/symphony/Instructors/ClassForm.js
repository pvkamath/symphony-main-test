﻿(function () {
    Symphony.Instructors.ClassForm = Ext.define('instructors.classform', {
        alias: 'widget.instructors.classform',
        extend: 'Ext.Panel',
        classId: 0,
        classTypeId: 0,
        trainingProgramId: 0,

        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                border: false,
                defaults: {
                    border: false
                },
                listeners: {
                    render: {
                        fn: function () {
                            var attendanceGrid = me.find('xtype', 'classroom.attendancegrid')[0];
                            if (attendanceGrid) {
                                var attendanceToolbar = attendanceGrid.getTopToolbar();
                                var button = new Ext.Button({
                                    text: 'Save',
                                    iconCls: 'x-button-save',
                                    handler: function () {
                                        me.save();
                                    }
                                });

                                attendanceToolbar.insert(0, '-');
                                attendanceToolbar.insert(0, button);
                            }
                        },
                        single: true
                    },
                    beforeadd: function (panel, cmp, i) {
                        var type = cmp.getXType(),
                            hiddenComponents = [];

                        hiddenComponents[Symphony.CourseType.online] = [
                            'classroom.attendancegrid',
                            'classroom.registrationtrees',
                            'classroom.coursedocumentsgrid',
                            'classroom.classschedulegrid',
                            'classroom.classresourcesgrid'
                        ];

                        hiddenComponents[Symphony.CourseType.classroom] = [
                            'instructors.onlinecoursestudentgrid',
                            'instructors.trainingprogramdetailspanel'
                        ];

                        if (hiddenComponents[me.classTypeId].indexOf(type) > -1) {
                            return false;
                        }

                        if (type === 'messageboard.app' && !Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards)) {
                            return false;
                        }
                        return true;
                    }
                },
                items: [{
                    region: 'center',
                    layout: 'fit',
                    ref: 'main',
                    defaults: {
                        border: false
                    },
                    items: [{
                        xtype: 'tabpanel',
                        border: false,
                        activeTab: 0,
                        ref: '../classTabs',
                        bubbleEvents: ['beforeadd'], // because we only want one place to decide which controls should be shown
                        listeners: {
                            render: function () {
                                me.load();
                            },
                            beforeadd: function (panel, cmp, i) {
                                if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                    return false;
                                }
                            }

                        },
                        items: [{
                            title: 'General',
                            name: 'general',
                            xtype: 'panel',
                            autoScroll: true,
                            border: false,
                            bodyCssClass: 'x-panel-mc',
                            layout: {
                                type: 'vbox',
                                defaultMargins: {
                                    top: 5,
                                    right: 5,
                                    left: 5,
                                    bottom: 5
                                },
                                align: 'stretch'
                            },
                            padding: 0,
                            margins: {
                                left: 0,
                                right: 0,
                                top: 0,
                                bottom: 0
                            },
                            tbar: {
                                xtype: 'classroom.classcontactbar',
                                border: false,
                                instructorsGridType: me.classTypeId === Symphony.CourseType.online ? 'courseassignment.leadersgrid' : 'classroom.classinstructorsgrid',
                                studentsGridType: me.classTypeId === Symphony.CourseType.online ? 'instructors.onlinecoursestudentgrid' : 'classroom.registrationtrees'
                            },
                            defaults: {
                                border: false,
                                xtype: 'panel',
                                layout: 'fit',
                                flex: 1
                            },
                            bubbleEvents: ['beforeadd'], // Because we only want one place to manage which controls should and shouldn't be shown
                            listeners: {
                                
                                    beforeadd: function (panel, cmp, i) {
                                        if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                            return false;
                                        }
                                    }
                            },
                            items: [{
                                xtype: 'panel',
                                layout: {
                                    type: 'hbox',
                                    defaultMargins: {
                                        top: 5,
                                        right: 5,
                                        left: 5,
                                        bottom: 5
                                    },
                                    align: 'stretch'
                                },
                                flex: 0.3,
                                defaults: {
                                    border: false,
                                    xtype: 'panel',
                                    layout: 'fit',
                                    flex: 1
                                },
                                bubbleEvents: ['beforeadd'], // because we only want one place to decide which controls should be shown
                                listeners: {
                                    
                                        beforeadd: function (panel, cmp, i) {
                                            if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                                return false;
                                            }
                                        }
                                },
                                items: [{
                                    xtype: me.classTypeId === Symphony.CourseType.online ? 'courseassignment.leadersgrid' : 'classroom.classinstructorsgrid',
                                    name: 'instructorsgrid',
                                    flex: 0.2,
                                    autoheight: false,
                                    allowDelete: false,
                                    title: me.classTypeId === Symphony.CourseType.online ? 'Leaders' : 'Instructors',
                                    showVideoChatColumn: true,
                                    listeners: {
                                        added: function (grid, component, index) {
                                            grid.getTopToolbar().hide();
                                        },
                                        beforeadd: function (panel, cmp, i) {
                                            if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                                return false;
                                            }
                                        }
                                    },
                                    style: "overflow-x: hidden"
                                }, {
                                    xtype: 'classroom.classschedulegrid',
                                    flex: 0.2,
                                    autoheight: false,
                                    allowDelete: false,
                                    title: 'Schedule',
                                    listeners: {
                                        added: function (grid, component, index) {
                                            grid.getTopToolbar().hide();
                                        },
                                        beforeadd: function (panel, cmp, i) {
                                            if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                                return false;
                                            }
                                        }
                                    }
                                }, {
                                    xtype: 'classroom.classresourcesgrid',
                                    flex: 0.2,
                                    autoheight: false,
                                    allowDelete: false,
                                    title: 'Resources',
                                    listeners: {
                                        added: function (grid, component, index) {
                                            grid.getTopToolbar().hide();
                                        },
                                        beforeadd: function (panel, cmp, i) {
                                            if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                                return false;
                                            }
                                        }
                                    }
                                }, {
                                    xtype: 'instructors.trainingprogramdetailspanel',
                                    flex: 0.2,
                                    courseId: me.classId, // When it's an online course, the course id of the online course will be in the classId field.
                                    trainingProgramId: me.trainingProgramId,
                                    listeners: {
                                        
                                        beforeadd: function (panel, cmp, i) {
                                                if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                                    return false;
                                                }
                                            }
                                    }
                                }]
                            }, {
                                flex: 0.7,
                                title: 'Students',
                                name: 'classroom.attendancegrid',
                                classId: me.classId,
                                courseCompletionTypeId: me.courseCompletionTypeId,
                                itemId: 'classroom.attendancegrid.' + me.classId,
                                xtype: 'classroom.attendancegrid',
                                showMessageColumn: true,
                                showVideoChatColumn: true,
                                showCanMoveForwardColumn: true,
                                listeners: {
                                
                                    beforeadd: function (panel, cmp, i) {
                                        if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                                            return false;
                                        }
                                    }
                                }
                            }, {
                                flex: 0.7,
                                title: 'Students',
                                name: 'instructors.onlinecoursestudentgrid',
                                courseId: me.classId, // When it's an online course, the course id of the online course will be in the classId field.
                                trainingProgramId: me.trainingProgramId,
                                itemId: 'instructors.onlinecoursestudentgrid.' + me.classId,
                                xtype: 'instructors.onlinecoursestudentgrid',
                                listeners: {
                                
                beforeadd: function (panel, cmp, i) {
                    if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                        return false;
                    }
                }
            }
                            }]
                        }, {
                            title: 'Documents',
                            name: 'files',
                            xtype: me.classTypeId === Symphony.CourseType.online ? 'courseassignment.trainingprogramdocumentsgrid' : 'classroom.coursedocumentsgrid',
                            trainingProgramId: me.trainingProgramId,
                            listeners: {
                                render: function (documentsGrid) {
                                    documentsGrid.getTopToolbar().hide();
                                },
                                activate: {
                                    fn: function (documentsGrid) {
                                        if (documentsGrid.setCourseId) {
                                            documentsGrid.setCourseId(me.klass.courseId);
                                        }
                                    },
                                    single: true
                                },  
                beforeadd: function (panel, cmp, i) {
                    if (!me.fireEvent('beforeadd', panel, cmp, i)) {
                        return false;
                    }
                }

                            }
                        }, {
                            title: 'Registrations and Waiting List',
                            name: 'classroom.registrationtrees',
                            xtype: 'classroom.registrationtrees',
                            readOnly: true,
                            listeners: {

                                beforeadd: function (panel, cmp, i) {
                                    me.fireEvent('beforeadd', panel, cmp, i);
                                }
                            }
                        }, {
                            title: 'Message Board',
                            name: 'messageboard',
                            xtype: 'messageboard.app',
                            ref: '../../messageBoardTab',
                            disabled: true,
                            hidden: true,
                            showForumGrid: false,
                            listeners: {
                                activate: {
                                    fn: function (panel) {
                                        var messageBoardType = me.classTypeId === Symphony.CourseType.classroom ?
                                                                Symphony.MessageBoardType.classroom :
                                                                Symphony.MessageBoardType.online;

                                        if (me.classTypeId === Symphony.CourseType.online) {
                                            me.find('name', 'instructors.onlinecoursestudentgrid')[0].getAllData(function (args) {
                                                panel.loadMessageBoardByClassId(messageBoardType, me.classId, me.klass.name, me.trainingProgramId, args.data, true);
                                            });
                                        } else if (me.classTypeId === Symphony.CourseType.classroom) {
                                            var registrations = me.find('name', 'classroom.attendancegrid')[0].getData();
                                            panel.loadMessageBoardByClassId(messageBoardType, me.classId, me.klass.name, me.trainingProgramId, registrations, true);
                                        }
                                    },
                                    single: true
                                }, 
                                    beforeadd: function (panel, cmp, i) {
                                        me.fireEvent('beforeadd', panel, cmp, i);
                                    }
                                
                            }
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        load: function () {
            var me = this;
            
            me.loadMask = new Ext.LoadMask(me.main.getEl(), { msg: "Please wait..." });
            me.loadMask.show();

            if (me.classId && me.classTypeId === Symphony.CourseType.classroom) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/classroom.svc/classes/' + me.classId,
                    success: function (result) {
                        me.loadData(result.data);
                    }
                });
            } else if (me.classId && me.classTypeId === Symphony.CourseType.online) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/courseassignment.svc/courses/' + me.classId,
                    success: function (result) {
                        Symphony.Ajax.request({
                            method: 'GET',
                            url: '/services/portal.svc/trainingprogramdetails/' + me.trainingProgramId,
                            success: function (tpresult) {
                                me.loadData(result.data, tpresult.data);
                            }
                        });
                    }
                });
            }
        },
        loadData: function (data, tpData) {
            // load up the appropriate panels with the results
            var me = this;
            me.klass = data;
            me.trainingProgram = tpData;

            if (me.messageBoardTab) {
                me.messageBoardTab.setDisabled(false);
            }

            var instructorsGrid = me.find('name', 'instructorsgrid')[0]

            // first, bind the basic information
            if (me.trainingProgramId > 0 && me.classTypeId === Symphony.CourseType.online) {
                instructorsGrid.setData(tpData.leaders);
                me.find('xtype', 'instructors.trainingprogramdetailspanel')[0].setData(tpData)
            } else {
                instructorsGrid.setData(data.classInstructorList);
                me.find('xtype', 'classroom.classresourcesgrid')[0].setData(data.resourceList);
                if (me.klass.id > 0) {
                    me.find('xtype', 'classroom.classschedulegrid')[0].setClass(data);

                    // add the registrations and waiting list
                    me.find('name', 'classroom.registrationtrees')[0].setData(data.registrationList);

                    // add the attendance and scores
                    me.find('name', 'classroom.attendancegrid')[0].setData(data.registrationList);
                }

                // set the capacity based on the room
                me.find('name', 'classroom.registrationtrees')[0].setCapacity(data.capacityOverride);

                // set the completion type based on the course
                me.find('name', 'classroom.attendancegrid')[0].setCourseCompletionType(data.courseCompletionTypeId);
            }

            me.loadMask.hide();

        },
        save: function () {
            var me = this;
            var registrationList = me.find('xtype', 'classroom.attendancegrid')[0];
            var registrations = registrationList.getData();

            Symphony.Ajax.request({
                url: '/services/classroom.svc/classes/' + me.classId + '/registrations',
                jsonData: { registrationList: registrations },
                success: function (result) {
                    me.loadData(result.data);
                }
            });
        },
        clearButtons: function (grid) {
            var toolbar = grid.getTopToolbar();
            var buttons = toolbar.findByType('button');
            
            for (var i = 0; i < buttons.length; i++) {
                toolbar.remove(buttons[i]);
            }
        }
    });

})();