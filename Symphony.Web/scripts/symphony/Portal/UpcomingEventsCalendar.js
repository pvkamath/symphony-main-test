﻿Symphony.Portal.UpcomingEventsCalendar = Ext.define('portal.upcomingeventscalendar', {
    alias: 'widget.portal.upcomingeventscalendar',
    extend: 'classroom.trainingcalendarmonthgrid',
    initComponent: function () {
        Ext.apply(this, {
            cls: 'event-calendar',
            enableDragDrop: false,
            showFilters: false
        });
        this.callParent(arguments);
        this.on('rowclick', Ext.bind(Symphony.Portal.linkHandler, this));
    },
    updateUrl: function () {
        var url = '/services/portal.svc/upcomingevents/calendar/' + this.year + '/' + this.month + '/' + (this.showAll ? false : true);
        this.url = url;
        if (this.updateStore) {
            this.updateStore(url);
        }
    },
    setShowAll: function (showAll) {
        this.showAll = showAll;
        this.updateUrl();
    },
    renderDate: function (value) {
        if (value.events && value.events.length > 0) {
            var html = [];
            var recordDef = Ext.data.Record.create(Symphony.Definitions.upcomingEvent);
            for (var i = 0; i < value.events.length; i++) {
                var event = value.events[i];
                var record = new recordDef(event);
                record.raw = event;
                var date = Symphony.shortTimeRenderer(event.startDate, true);
                var options = Symphony.Portal.upcomingEventOptionsRenderer(event.eventName, null, record, 0, 0, this.store);
                var content = Symphony.Portal.upcomingEventRenderer(event.eventName, null, record, 0, 0, this.store, true);
                var qtip = '';
                if (options != '-') {
                    qtip = Symphony.htmlEscape(event.eventName + '<br/><em style="font-style:italic">' + options + '</em>');
                }
                var finalEvent = this.renderEventContent(date, content + '<br/><em style="font-style:italic">Options</em>: ' + options, qtip, event.eventId, '');
                html.push(finalEvent);
            }

            return html.join('');
        }
        return '';
    }
});
