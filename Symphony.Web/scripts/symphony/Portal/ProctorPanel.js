﻿Symphony.Portal.ProctorPanel = Ext.define('portal.proctorpanel', {
    alias: 'widget.portal.proctorpanel',
    extend: 'Ext.Panel',
    loaded: false,
    config: {},

    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            border: false,
            autoScroll: true,
            cls: 'x-panel-mc affidavit-form',
            bodyStyle: 'padding: 5px',
            defaults: {
                xtype: 'panel',
                anchor: '100%',
                border: false,
                cls: 'x-form-item'
            },
            listeners: {
                activate: function() {
                    me.loadProctorForm();
                }
            }
        });

        me.callParent(arguments);
    },

    loadProctorForm: function () {
        var me = this;

        if (me.loaded) {
            return;
        }
        me.loaded = true;

        me.loadMask = new Ext.LoadMask(me.getEl(), { msg: "Please wait..." });
        me.loadMask.show();

        Symphony.Ajax.request({
            url: '/services/proctor.svc/proctorForms/' + me.proctorFormId,
            method: 'GET',
            success: function (args) {
                var proctorForm = JSON.parse(args.data.proctorJSON);

                var proctorPanel = new Ext.form.FormPanel({
                    items: proctorForm.items,
                    layout: 'form',
                    defaults: {
                        border: false
                    },
                    cls: 'x-panel-mc affidavit-form',
                    bodyStyle: 'padding: 5px',
                    name: 'proctorPanel'
                });

                me.config = {
                    width: proctorForm.width ? proctorForm.width : 500,
                    height: proctorForm.height ? proctorForm.height : 500,
                    title: me.mode == 'preview' ? 'Proctor Preview' : 'Proctor Required'
                }
                me.add(proctorPanel);
                me.doLayout();

                me.fireEvent('updateContainer', {
                    width: me.config.width,
                    height: me.config.height,
                    title: me.config.title,
                    iconCls: 'x-window-script'
                });

                me.loadMask.hide();

                var confirmBtn = me.queryById('confirm');
                var cancelBtn = me.queryById('cancel');

                if (!confirmBtn) {
                    confirmBtn = me.find('xtype', 'button')[0];
                }

                confirmBtn.setIconCls('x-button-launch-course');
                if (cancelBtn) {
                    cancelBtn.setIconCls('x-button-cancel');
                }

                if (me.mode == 'preview') {
                    return;
                }

                confirmBtn.addListener('click', function () {
                    var proctorPanel = me.find('name', 'proctorPanel')[0],
                        form = proctorPanel.getForm(),
                        items = form.getFields(),
                        data = form.getValues(),
                        responseJSON = JSON.stringify(data),
                        proctorResponse = {
                            userId: Symphony.ActualUser.id,
                            courseId: me.onlineCourseId,
                            trainingProgramId: me.trainingProgramId,
                            proctorFormId: me.proctorFormId,
                            responseJSON: responseJSON
                        },
                        valid = true;

                    for (var i = 0; i < items.length; i++) {
                        var field = items.itemAt(i);
                        if (!field.getValue()) {
                            valid = false
                        }
                    }

                    if (!valid) {
                        Ext.Msg.alert("Error", "All fields require a value.");
                        return;
                    }

                    Symphony.Ajax.request({
                        url: '/services/proctor.svc/proctorForms/responses',
                        method: 'post',
                        jsonData: proctorResponse
                    });

                    // Just going to assume that the affidavit request worked and launch the course
                    me.fireEvent('nextitem');
                });

                if (cancelBtn) {
                    cancelBtn.addListener('click', function() {
                        me.fireEvent('exit');
                    });
                }
            }
        });
    }
});

Symphony.Portal.ProctorPanel.showProctorWindow = function (proctorFormId, mode, trainingProgramId, onlineCourseId, callback) {
    if (!proctorFormId || proctorFormId <= 0) {
        return;
    }

    var w = new Ext.Window({
        autoScroll: true,
        modal: true,
        height: 205,
        width: 310,
        listeners: {
            afterrender: function () {
                var proctorPanel = w.find('xtype', 'portal.proctorpanel')[0];
                proctorPanel.loadProctorForm();
            }
        },

        items: [{
            xtype: 'portal.proctorpanel',
            userId: Symphony.ActualUser.id,
            onlineCourseId: onlineCourseId,
            trainingProgramId: trainingProgramId,
            proctorFormId: proctorFormId,
            mode: mode,

            listeners: {
                'updateContainer': function (parameters) {
                    var params = {};
                    Ext.apply(params, parameters, {
                        height: 205,
                        width: 310,
                        title: 'Proctor Required',
                        iconCls: 'info'
                    });

                    w.setSize(params.width, params.height);
                    w.setTitle(params.title, params.iconCls);

                    w.center();
                }
            }
        }],
        bbar: mode == 'preview' ? {
            items: ['->', {
                xtype: 'button',
                iconCls: 'x-button-cancel',
                text: 'Close',
                handler: function () {
                    w.close();
                }
            }]
        } : {}
    });
    w.show();
};