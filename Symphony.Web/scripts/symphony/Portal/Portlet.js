﻿Ext.define('portal.portlet', {
    extend: 'Ext.app.Portlet',
    alias: 'widget.portal.portlet',
    
    initComponent: function () {
        this.cls = this.cls + ' x-portlet';
        this.callParent(arguments);

        var componentsWithStores = this.query('[store]');

        for (var i = 0; i < componentsWithStores.length; i++) {
            var store = componentsWithStores[i].store;
            if (!store.hasBeforePortletStoreLoadListener) {
                store.on('beforeload', this.beforePortletStoreLoad, this)
                store.hasBeforePortletStoreLoadListener = true;
            }
        }
    },
    beforePortletStoreLoad: function (store) {
        return this.isVisible();
    },
    initPortlet: function () {
        var me = this;
        setTimeout(function () {
            me.setVisible(true);
            if (typeof (me.initStore) === 'function') {
                me.initStore();
            }
            if (typeof (me.setChecked) === 'function') {
                me.setChecked(true);
            }
        }, 100);
    }
});