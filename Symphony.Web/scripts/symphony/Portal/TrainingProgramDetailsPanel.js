﻿(function () {
    Symphony.Portal.TrainingProgramDetailsPanel = Ext.define('portal.trainingprogramdetailspanel', {
        alias: 'widget.portal.trainingprogramdetailspanel',
        extend: 'Ext.Panel',
        trainingProgramId: 0,
        trainingProgram: null,
        loaded: false,
        summaryWidth: 116, 
        gridWidth: 0, // to be calculated based on window width
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                items: Ext.isIE8 ? [{
                    xtype: 'panel',
                    layout: {
                        type: 'vbox',
                        pack: 'center',
                        align: 'center',
                        padding: 10
                    },
                    items: [{
                        xtype: 'panel',
                        border: false,
                        layout: {
                            type: 'hbox',
                            pack: 'center',
                            align: 'center'
                        },
                        items: {
                            border: false,
                            xtype: 'panel',
                            html: 'Please wait...'
                        }
                    }]
                }] : [],
                tbar: {
                    hidden: !me.isShowBackButton,
                    items: [{
                        xtype: 'button',
                        iconCls: 'x-button-previous',
                        text: 'Return to Library',
                        handler: function () {
                            me.returnToLibrary();
                        }
                    }]
                },
                border: false,
                layout: 'fit',
                listeners: {
                    afterrender: this.load,
                    beforeremove: function () {
                        me.loaded = false;
                    }
                }
            });

            this.callParent(arguments);
        },
        load: function () {
            var me = this;
            var mask = new Ext.LoadMask(this, { msg: "Please wait..." });
            var url = '/services/portal.svc/trainingprogramdetails/' + this.trainingProgramId;

            if (me.userId > 0) {
                url += '/' + me.userId;
            }

            if (!Ext.isIE8) {
                mask.show();
            }

            Symphony.Ajax.request({
                method: 'GET',
                url: url,
                callback: function () {
                    mask.hide();
                },
                success: function (result) {
                    me.renderTrainingProgram(result.data);

                    if (result.data.sessionCourseId > 0) {
                        var tp = result.data;
                        var now = new Date();
                        var timeUntilStartInMs = Symphony.parseDate(tp.startDate) - now;
                        var timeUntilEndInMs = Symphony.parseDate(tp.endDate) - now;

                        if (Symphony.Portal.TrainingProgramDetailsPanel.untilSessionStartTimeout) {
                            clearTimeout(Symphony.Portal.TrainingProgramDetailsPanel.untilSessionStartTimeout);
                        }

                        if (Symphony.Portal.TrainingProgramDetailsPanel.untilSessionEndTimeout) {
                            clearTimeout(Symphony.Portal.TrainingProgramDetailsPanel.untilSessionEndTimeout);
                        }

                        if (timeUntilStartInMs > 0 && tp.isSessionRegistered && timeUntilStartInMs < 2147483647) {
                            Symphony.Portal.TrainingProgramDetailsPanel.untilSessionStartTimeout = setTimeout(function () {
                                if (me && me.loaded && !me.isDestroyed) {
                                    me.load();
                                } 
                            }, timeUntilStartInMs);
                        }

                        if (timeUntilEndInMs > 0 && tp.isSessionRegistered && timeUntilEndInMs < 2147483647) {
                            Symphony.Portal.TrainingProgramDetailsPanel.untilSessionEndTimeout = setTimeout(function () {
                                if (me && me.loaded && !me.isDestroyed) {
                                    me.load();
                                }
                            }, timeUntilEndInMs)
                        }
                    }

                }
            });
        },
        renderTrainingProgram: function (tp) {
            var tpData = tp ? tp : this.trainingProgram;

            if (!tpData) {
                // Returning - means the function
                // was called before the tp data
                // was first loaded. Which is ok,
                // because this will get called 
                // after data loaded.
                return;
            }

            var courseAccordian = this.find('name', 'courseAccordion');
            var activeItemIndex = 0;

            if (courseAccordian.length) {
                courseAccordian[0].items.each(function (item, index) {
                    if (!item.collapsed) {
                        activeItemIndex = index;
                    }
                });
            }

            this.removeAll();
            this.addItems(tpData, activeItemIndex);
            this.loaded = true;
            this.trainingProgram = tpData;
        },
        removeItems: function () {
            this.removeAll();
        },
        addItems: function (trainingProgram, activeItemIndex) {
            var me = this;
            var tp = trainingProgram;

            tp.description = tp.description || 'No description provided.';
            var messageBoardLinkId = "messageBoardLinkButton-" + tp.id + "-" + tp.messageBoardId;
            if (_ui(this)) {
                tp.dueDateString = Symphony.Renderer.displayDateRenderer(tp.displayDueDate);
                _log("Cleanup - Symphony.Portal.TrainingProgramDetailsPanel.addItems - no need for ui toggle.");
            } else {
                var dt = Symphony.parseDate(tp.dueDate, !tp.isUsingSessions);
                if (dt.getFullYear() == Symphony.defaultYear || dt.getFullYear() == 2040) {
                    dt = Symphony.parseDate(tp.endDate, true);
                }

                tp.dueDateString = dt.formatSymphony('m/d/Y');

                if (tp.isUsingSessions) {
                    if (tp.isSessionRegistered) {
                        tp.dueDateString += dt.formatSymphony(' g:i a');
                    } else {
                        tp.dueDateString = 'Session registration required.';
                    }
                }
            }

            var topTemplate = new Ext.Template([
                    '<div class="training-program-view">',
                        '<div class="title">{name}</div>',
                        '<div class="description"><span>Description: </span>{description}</div>',
                        '<div class="due"><span>Due: </span>{dueDateString}</div>',
                        '<div class="message-board-link-button" id="' + messageBoardLinkId + '"></div>',
                    '</div>'
            ]);

            var items = [{
                layout: 'border',
                defaults: {
                    border: false,
                    bodyStyle: 'padding:10px',
                    plain: true
                },
                cls: 'tp-summary',
                xtype: 'container',
                style: 'background-color: transparent !important',
                listeners: {
                    afterrender: function () {
                        if (tp.messageBoardId && Symphony.Portal.hasPermission(Symphony.Modules.MessageBoards)) {

                            if (me.find('name', 'messageboardbtn').length == 0) {

                                setTimeout(function () {
                                    new Ext.Panel({
                                        border: false,
                                        renderTo: messageBoardLinkId,
                                        layout: {
                                            type: 'hbox',
                                            pack: 'center'
                                        },
                                        items: [{
                                            xtype: 'button',
                                            text: 'View Message Board',
                                            iconCls: 'x-button-message-board',
                                            name: 'messageboardbtn',
                                            handler: function () {
                                                Symphony.Portal.launchMessageBoard(tp.messageBoardId);
                                            }
                                        }]
                                    });
                                    me.doLayout();
                                }, 10);
                            }
                        }
                    }
                },
                items: [{
                    region: 'north',
                    height: 132,
                    html: topTemplate.apply(tp)
                }, {
                    region: 'center',
                    xtype: 'panel',
                    layout: {
                        type: 'hbox',
                        pack: 'start',
                        align: 'stretch',
                        defaultMargins: 5
                    },
                    defaults: {
                        border: false,
                        frame: false
                    },
                    items: [{
                        flex: 1,
                        xtype: 'panel',
                        style: 'background-color: transparent !important',
                        layout: 'border',
                        defaults: {
                            border: false
                        },
                        items: [{
                            xtype: 'panel',
                            region: 'center',
                            style: 'background-color: transparent !important',
                            layout: 'accordion',
                            //border: true,
                            name: 'courseAccordion',
                            activeItem: activeItemIndex,
                            layoutConfig: {
                                titleCollapse: true, /* clicking the title to expand/collapse */
                                animate: true
                            },
                            items: [{
                                title: 'Required ' + Symphony.Aliases.courses,
                                itemId: 'portal.trainingprogramrequiredcoursesgrid',
                                xtype: 'portal.trainingprogramcoursesgrid',
                                data: tp.requiredCourses,
                                trainingProgram: tp,
                                showMessageBoardColumn: true,
                                required: true,
                                targetUserId: me.userId,
                                cls: 'accordion-panel-first', /* fixes the top border of the accordion */
                                listeners: {
                                    'registrationchanged': function () {
                                        me.load();
                                    }
                                }
                            }, {
                                title: 'Elective ' + Symphony.Aliases.courses,
                                itemId: 'portal.trainingprogramelectivecoursesgrid',
                                xtype: 'portal.trainingprogramcoursesgrid',
                                trainingProgram: tp,
                                showMessageBoardColumn: true,
                                enabled: _ui(this) ? tp.isElectivesAvailable : (tp.enforceRequiredOrder ? Symphony.Portal.requiredComplete(tp) : true),
                                disabledMessage: _ui(this) ? Symphony.Renderer.displayModelRenderer(tp.displayIsElectivesAvailable) : 'Elective ' + Symphony.Aliases.courses + ' are disabled until all required ' + Symphony.Aliases.courses + ' are complete.',
                                data: tp.electiveCourses,
                                targetUserId: me.userId,
                                listeners: {
                                    'registrationchanged': function () {
                                        me.load();
                                    }
                                }
                            }, {
                                title: 'Optional ' + Symphony.Aliases.courses,
                                itemId: 'portal.trainingprogramoptionalcoursesgrid',
                                xtype: 'portal.trainingprogramcoursesgrid',
                                trainingProgram: tp,
                                showMessageBoardColumn: true,
                                enabled: _ui(this) ? tp.isOptionalAvailable : ( tp.enforceRequiredOrder ? Symphony.Portal.requiredComplete(tp) && Symphony.Portal.electiveComplete(tp) : true),
                                disabledMessage: _ui(this) ? Symphony.Renderer.displayModelRenderer(tp.displayIsOptionalAvailable) : 'Optional ' + Symphony.Aliases.courses + ' are disabled until all required ' + Symphony.Aliases.courses + ' are complete and minimum elective requirements are met.',
                                data: tp.optionalCourses,
                                targetUserId: me.userId,
                                listeners: {
                                    'registrationchanged': function () {
                                        me.load();
                                    }
                                }
                            }]
                        }, {
                            region: 'south',
                            items: [{
                                title: 'Final Assessment',
                                itemId: 'portal.trainingprogramfinalcoursesgrid',
                                xtype: 'portal.trainingprogramcoursesgrid',
                                cls: 'final-assessment',
                                collapsible: false,
                                hideMaskWhenEmpty: true,
                                trainingProgram: tp,
                                showMessageBoardColumn: true,
                                enabled: _ui(this) ? tp.isFinalAvailable : Symphony.Portal.isFinalEnabled(tp),
                                disabledMessage: _ui(this) ? Symphony.Renderer.displayModelRenderer(tp.displayIsFinalAvailable) : Symphony.Portal.getFinalAssessmentDisabledMessage(tp),
                                data: tp.finalAssessments,
                                targetUserId: me.userId,
                                border: false,
                                listeners: {
                                    'registrationchanged': function () {
                                        me.load();
                                    }
                                }
                            }, {
                                title: 'Surveys',
                                itemId: 'portal.trainingprogramsurveysgrid',
                                xtype: 'portal.trainingprogramcoursesgrid',
                                cls: 'final-assessment',
                                collapsible: false,
                                hideMaskWhenEmpty: true,
                                trainingProgram: tp,
                                showMessageBoardColumn: true,
                                enabled: _ui(this) ? tp.isAccreditationSurveyAvailable : Symphony.Portal.isAccreditationSurveyEnabled(tp),
                                disabledMessage: _ui(this) ? Symphony.Renderer.displayModelRenderer(tp.displayIsAccreditationSurveyAvailable) : Symphony.Portal.getAccreditationSurveyDisabledMessage(tp),
                                data: tp.accreditationSurveys,
                                targetUserId: me.userId,
                                border: false,
                                listeners: {
                                    'registrationchanged': function() {
                                        me.load();
                                    }
                                }
                            }]
                        }]
                    }, {
                        width: 90,
                        xtype: 'portal.trainingprogramsummary',
                        name: 'summary',
                        data: tp,
                        targetUserId: me.userId,
                        listeners: {
                            'surveytaken': function () {
                                me.load();
                            }
                        }
                    }, {
                        flex: 1,
                        layout: 'fit',
                        name: 'filesAndLeaders',
                        items: [{
                            xtype: 'panel',
                            layout: 'border',
                            border: false,
                            bodyStyle: 'background: white',
                            items: [{
                                border: true,
                                title: 'Training Program Leaders',
                                cls: 'training-program-leaders',
                                xtype: 'courseassignment.leadersgrid',
                                name: 'courseleaders',
                                cls: Symphony.Modules.getModuleClass(Symphony.Modules.InstructorPortal),
                                region: 'north',
                                allowDelete: false,
                                showVideoChatColumn: true,
                                hidden: Symphony.Portal.hasPermission(Symphony.Modules.InstructorPortal) ? false : true,
                                height: 250,
                                margins: {
                                    bottom: 5
                                },
                                data: tp.leaders,
                                listeners: {
                                    added: function (grid, component, index) {
                                        grid.getTopToolbar().hide();
                                    }
                                }
                            }, {
                                border: true,
                                title: 'Training Program Files',
                                cls: 'training-program-files',
                                itemId: 'portal.trainingprogramfilesgrid',
                                xtype: 'portal.trainingprogramfilesgrid',
                                data: tp.files,
                                region: 'center'
                            }]
                        }]
                    }]
                }]
            }];
            this.add(items);
            this.doLayout();

        }
    });


})();