﻿(function () {
    Ext.define('portal.professionselector', {
        alias: 'widget.portal.professionselector',
        extend: 'Ext.form.field.ComboBox',
        initComponent: function () {
            var me = this;
            var professionStoreData = [];

            if (this.professions) {
                for (var i = 0; i < this.professions.length; i++) {
                    professionStoreData.push([this.professions[i].name, this.professions[i].id]);
                }
            }

            var store = new Ext.data.ArrayStore({
                fields: ['name', 'id'],
                data: professionStoreData
            });
            
            Ext.apply(this, {
                store: store,
                allowBlank: false,
                triggerAction: 'all',
                queryMode: 'local',
                forceSelection: true,
                editable: false,
                store: store,
                valueField: 'id',
                displayField: 'name'
            });

            this.callParent(arguments);
        }
    });
})();