﻿(function () {
    Symphony.Portal.SessionWarningPanel = Ext.define('portal.sessionwarningpanel', {
        alias: 'widget.portal.sessionwarningpanel',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                html: '<img class="msg-box-icon" src="/images/error_64x64.png"/><p class="msg-box-text">Warning! You are about to launch this course in Review Mode. Your progress will not be recorded!</p>',
                border: false,
                padding: '5px 5px 5px 5px',
                cls: 'x-panel-mc',
                footerCssClass: 'x-panel-mc',
                buttons: [{
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function() {
                        me.fireEvent('exit');
                    }
                }, {
                    xtype: 'button',
                    text: 'Launch in Review Mode',
                    iconCls: 'x-button-next',
                    handler: function() {
                        me.fireEvent('nextitem');
                    }
                }],
                listeners: {
                    activate: function () {
                        me.fireEvent('updateContainer', {
                            width: 300,
                            height: 150,
                            iconCls: 'x-window-critical',
                            title: 'Launch in Review Mode?'
                        });
                    }
                }
            });
            this.callParent(arguments);
        }
    });

})();