﻿(function () {
    Ext.define('portal.disclaimer', {
        alias: 'widget.portal.disclaimer',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                border: false,
                html: this.html,
                anchor: '100%'
            });

            this.callParent(arguments);
        }
    });
})();