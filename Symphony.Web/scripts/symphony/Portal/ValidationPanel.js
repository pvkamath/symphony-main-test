﻿(function () {
    Symphony.Portal.ValidationPanel = Ext.define('portal.validationpanel', {
        alias: 'widget.portal.validationpanel',
        extend: 'Ext.Panel',
        border: false,
        
        modal: true,
        resizable: false,
        bodyBorder: false,
        
        userId: null,
        customerId: null,
        trainingProgramId: null,
        onlineCourseId: null,
        affidavitId: null,
        launchCourse: false,
        userData: null,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                border: false,
                bodyBorder: false,
                cls: 'x-panel-mc',
                items: [{
                    xtype: 'form',
                    border: false,
                    bodyBorder: false,
                    frame: false,
                    monitorValid: true,
                    defaults: {
                        anchor: '100%'
                    },
                    padding: '5px 5px 5px 5px',
                    listeners: {
                        clientvalidation: function (form, valid) {
                            me.buttons[1].setDisabled(!valid);
                        }
                    },
                    labelWidth: 160,
                    items: [{
                        xtype: 'label',
                        cls: 'x-form-item',
                        text: 'Please enter your personal details. These will be used to ensure only you are taking online ' + Symphony.Aliases.courses.toLowerCase() + '. You will only be prompted to enter this information once. While taking courses you will be periodically asked to confirm these details.',
                        height: 65
                    }, {
                        xtype: 'textfield',
                        name: 'ssn',
                        fieldLabel: 'Social Security Number',
                        allowBlank: false
                    }, {
                        name: 'dob',
                        fieldLabel: 'Date of Birth (d/m/y)',
                        allowBlank: false,
                        xtype: 'datefield',
                        dateOnly: true,
                        format: 'd/m/Y',
                        submitFormat: 'd/m/Y'
                    }]
                }],
                listeners: {
                    activate: function () {
                        me.fireEvent('updateContainer', {
                            height: 207,
                            width: 410,
                            title: 'User Validation Required',
                            iconCls: 'x-window-error'
                        });
                    },
                    render: function () {
                        Symphony.Ajax.request({
                            url: '/services/customer.svc/users/' + me.userId + '/customer/' + me.customerId,
                            method: 'GET',
                            success: function (args) {
                                me.userData = args.data;
                            }
                        });
                    }
                },
                buttons: [{
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        me.fireEvent('exit');
                    }
                }, {
                    xtype: 'button',
                    text: 'Save',
                    name: 'saveBtn',
                    iconCls: 'x-button-save',
                    handler: function () {
                        var data = me.find('xtype', 'form')[0].getForm().getValues();
                        data.userId = me.userId;

                        Ext.apply(me.userData, data);

                        Symphony.Ajax.request({
                            url: '/services/customer.svc/users/' + me.userId + '/customer/' + me.customerId,
                            jsonData: me.userData
                        });

                        me.fireEvent('nextitem');
                       

                    },
                    margins: '0 15'
                }],
                footerCssClass: 'x-panel-mc'
            });
            this.callParent(arguments);
        }
    });

})();