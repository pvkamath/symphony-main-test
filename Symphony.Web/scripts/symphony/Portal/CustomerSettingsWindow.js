﻿(function () {
    Symphony.Portal.CustomerSettingsWindow = Ext.define('portal.customersettingswindow', {
        alias: 'widget.portal.customersettingswindow',
        extend: 'Ext.Window',
        border: false,
        height: 500,
        width: 600,
        modal: true,
        resizable: false,
        bodyBorder: false,
        title: 'Company Settings',
        iconCls: 'x-menu-gear',
        //closeAction: 'hide',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'fit',
                items: [{
                    xtype: 'form',
                    ref: 'settingsForm',
                    border: false,
                    layout: 'fit',
                    items: [{
                        xtype: 'tabpanel',
                        activeTab: 1,
                        border: false,
                        defaults: {
                            frame: true,
                            border: false
                        },
                        items: [{
                            title: 'Student Mgmt',
                            iconCls: 'x-tab-user',
                            disabled: true
                        }, {
                            title: 'Course Mgmt',
                            iconCls: 'x-tab-openbook',
                            autoScroll: true,
                            items: [{
                                xtype: 'courseassignment.courseoverridefields'
                            }]
                        }, {
                            title: 'Meetings',
                            iconCls: 'x-tab-meeting',
                            disabled: true
                        }]
                    }],
                    listeners: {
                        afterrender: function () {
                            Symphony.Ajax.request({
                                method: 'GET',
                                url: '/services/customer.svc/customers/' + Symphony.User.customerId + '/settings',
                                success: function (result) {
                                    var settings = result.data;
                                    if (settings.retries) {
                                        settings.testAttempts = settings.retries + 1;
                                    }
                                    me.settingsForm.bindValues(settings);

                                    var themeFlavorField = me.settingsForm.find('name', 'themeFlavorOverrideId')[0];
                                    themeFlavorField.setVisible(settings.theme && settings.theme.isDynamic);
                                }
                            });
                        }
                    }
                }],
                buttons: [{
                    xtype: 'button',
                    text: 'Save',
                    handler: function () {
                        var settings = me.settingsForm.getValues();

                        settings = Symphony.CourseAssignment.cleanCourseOverrideValues(settings);

                        Symphony.Ajax.request({
                            url: '/services/customer.svc/customers/' + Symphony.User.customerId + '/settings',
                            jsonData: settings,
                            success: function (result) {
                                me.close();
                            },
                            failure: function (result) {
                                Ext.Msg.alert('Error', result.error);
                            }
                        });
                    },
                    margins: '0 15'
                }, {
                    xtype: 'button',
                    text: 'Cancel',
                    handler: function () {
                        me.close();
                    },
                    margins: '0 15'
                }]
            });
            this.callParent(arguments);

            this.on('close', function () {
                Symphony.fire('settings.closed');
            });
        }
    });

})();