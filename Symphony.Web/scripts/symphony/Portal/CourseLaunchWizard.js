﻿(function () {
    Symphony.Portal.CourseLaunchWizard = Ext.define('portal.courselaunchwizard', {
        alias: 'widget.portal.courselaunchwizard',
        extend: 'Ext.Window',
        border: false,
        height: 205,
        width: 310,
        modal: true,
        resizable: false,
        bodyBorder: false,
        title: 'Launch Course',
        layout: 'fit',
        submitData: {},
        launchSteps: [],
        launchLink: {},
        useNewVersion: false,
        minWidth: 320,
        minHeight: 100,
        preventClose: false,
        initComponent: function () {
            var me = this;
            if (_ui(this) || me.useNewVersion) {
                var items = [];
                var steps = me.launchSteps && me.launchSteps.length ? me.launchSteps.length : 0;

                for (var i = 0; i < steps; i++) {
                    var item = me.launchSteps[i].content ? JSON.parse(Symphony.Renderer.displayTextRenderer(me.launchSteps[i].content.value)) : {};
                    var launchStep = Ext.apply(me.launchSteps[i], item, {
                        buttons: [],
                        nextOverride: '',
                        stepIndex: i,
                        items: [{
                            xtype: 'panel'
                        }]
                    });
                    if (launchStep.contentUrl) {
                        // We will load the launch step on activate
                        launchStep.items = [{
                            xtype: 'panel',
                            listeners: {
                                render: function (panel) {
                                    var mask = new Ext.LoadMask(panel.body, { msg: "Please wait..." });
                                    mask.show();
                                }
                            }
                        }]

                        launchStep.loadLaunchStep = function () {
                            me.loadLaunchStep(this.initialConfig);
                            launchStep.loadLaunchStep = null;
                        }
                    } else {
                        me.parseLaunchStep(launchStep);
                    }

                    if (launchStep.isBlocker) {
                        launchStep.buttons.push({
                            xtype: 'button',
                            text: 'Ok',
                            handler: function () {
                                me.exit();
                            }
                        });
                    } else {
                        if (!this.preventClose) {
                            launchStep.buttons.push({
                                xtype: 'button',
                                text: 'Cancel',
                                iconCls: 'x-button-cancel',
                                handler: function () {
                                    me.exit();
                                }
                            });
                        }

                        if (i == me.launchSteps.length - 1) {
                            launchStep.isLast = true;
                            launchStep.buttons.push({
                                xtype: 'button',
                                text: launchStep.nextOverride ? launchStep.nextOverride : 'Launch Course',
                                iconCls: 'x-button-next',
                                handler: function () {
                                    // Process the form without a callback
                                    // On the final form, we cannot wait for a callback
                                    // on a request when we launch the course. Otherwise
                                    // in some browsers, the popup blocker will catch
                                    // the course launch popup. 
                                    //
                                    // Process form will still return false if the form is
                                    // invalid. 
                                    //
                                    // If the form is valid, we are assuming success and 
                                    // launching the course. 
                                    //
                                    // We need to always ensure that the last launch step
                                    // has no bearing on the course itself.
                                    //
                                    // Thus far, no launch steps actually have any bearing
                                    // on the course. They are only for recording data
                                    // about the launch. (Profession used, affidavit, proctor used...)
                                    if (!me.processForm()) {
                                        return;
                                    }
                                    
                                    me.launch();
                                }
                            });
                        } else {
                            launchStep.buttons.push({
                                xtype: 'button',
                                text: launchStep.nextOverride ? launchStep.nextOverride : 'Next',
                                iconCls: 'x-button-next',
                                handler: function () {
                                    // In some cases, the next launch step is dependent on the 
                                    // result of the previous
                                    // Specifically when selecting the profession.
                                    // We need to ensure the profession is saved first
                                    // then next is fired. The next launch step after
                                    // selecting a profession will be the accreditation disclaimer
                                    // this will ensure the accreditation disclaimers match 
                                    // with the profession. 
                                    me.processForm(function() {
                                        me.next();
                                    });
                                }
                            });
                        }
                    }

                    items.push(Ext.apply({
                        xtype: 'form',
                        layout: 'form',
                        cls: 'x-panel-transparent',
                        padding: 10,
                        footerCssClass: 'x-panel-transparent',
                        border: false,
                        autoScroll: true,
                        jsonFieldName: me.launchSteps[i].jsonFieldName,
                        defaults: {
                            xtype: 'panel',
                            anchor: '100%',
                            border: false,
                            header: false,
                            cls: 'x-panel-transparent'
                        },
                        header: false,
                        listeners: {
                            activate: function () {
                                var activeItem = me.launchWizard.getLayout().activeItem;
                                var mask = new Ext.LoadMask(this, { msg: "Loading..." });

                                if (Ext.isFunction(activeItem.loadLaunchStep)) {
                                    mask.show();
                                    activeItem.loadLaunchStep();

                                    activeItem.updateContainer = function (item) {
                                        mask.hide();
                                        activeItem.updateContainer= null;
                                        me.updateContainer(item);
                                    }
                                } else {
                                    me.updateContainer(activeItem);
                                }
                            }
                        }
                    }, launchStep));

                }

                Ext.apply(this, {
                    closable: !this.preventClose,
                    listeners: {
                        beforeshow: function () {
                            if (items.length == 0) {
                                me.launch();
                                return false;
                            }
                        }
                    },
                    items: [{
                        xtype: 'panel',
                        layout: 'card',
                        activeItem: 0,
                        ref: 'launchWizard',
                        items: items
                    }]
                });
                _log("CLEANUP! - Symphony.Portal.CourseLaunchWizard - Totally different for new ui. The entire init function has been rewritten. Clear out the old!");
            } else {
                Ext.apply(this, {
                    listeners: {
                        beforeshow: function () {
                            if (me.launchWizard.items.length == 0) {
                                me.launch();
                                return false;
                            }
                        }
                    },
                    items: [{
                        xtype: 'panel',
                        layout: 'card',
                        activeItem: 0,
                        ref: 'launchWizard',
                        listeners: {
                            beforeadd: function (panel, cmp, i) {
                                return !cmp.hidden;
                            }
                        },
                        items: [{
                            xtype: 'panel',
                            name: 'courseworkalert',
                            hidden: me.isCourseWorkAllowed,
                            html: 'Cannot launch ' + Symphony.Aliases.course.toLowerCase() + '. You have spent more than the daily allowed time doing online ' + Symphony.Aliases.course.toLowerCase() + ' work for this ' + Symphony.Aliases.course.toLowerCase() + '.',
                            cls: 'x-panel-mc',
                            padding: '5px 5px 5px 5px',
                            footerCssClass: 'x-panel-mc',
                            listeners: {
                                activate: function () {
                                    me.updateContainer({
                                        width: 250,
                                        height: 120,
                                        title: 'Course Unavailable',
                                        iconCls: 'x-window-error'
                                    });
                                }
                            },
                            buttons: [{
                                xtype: 'button',
                                text: 'Ok',
                                handler: function () {
                                    me.exit();
                                }
                            }]
                        }, {
                            xtype: 'portal.sessionwarningpanel',
                            name: 'sessionexpired',
                            hidden: !me.isReviewMode,
                            userId: me.userId,
                            onlineCourseId: me.onlineCourseId,
                            trainingProgramId: me.trainingProgramId,
                            customerId: me.customerId,
                            listeners: {
                                nextitem: function () {
                                    me.next();
                                },
                                exit: function () {
                                    me.exit();
                                },
                                updateContainer: function (parameters) {
                                    me.updateContainer(parameters);
                                }
                            }
                        }, {
                            xtype: 'portal.validationpanel',
                            name: 'validationrequired',
                            hidden: me.isUserValidationAllowed,
                            userId: me.userId,
                            onlineCourseId: me.onlineCourseId,
                            trainingProgramId: me.trainingProgramId,
                            customerId: me.customerId,
                            listeners: {
                                nextitem: function () {
                                    me.next();
                                },
                                exit: function () {
                                    me.exit();
                                },
                                updateContainer: function (parameters) {
                                    me.updateContainer(parameters);
                                }
                            }
                        }, {
                            xtype: 'portal.proctorpanel',
                            name: 'proctorrequired',
                            hidden: !me.proctorRequiredIndicator,
                            userId: me.userId,
                            onlineCourseId: me.onlineCourseId,
                            trainingProgramId: me.trainingProgramId,
                            customerId: me.customerId,
                            proctorFormId: me.proctorFormId,
                            listeners: {
                                nextitem: function () {
                                    me.next();
                                },
                                exit: function () {
                                    me.exit();
                                },
                                updateContainer: function (parameters) {
                                    me.updateContainer(parameters);
                                }
                            }
                        }, {
                            xtype: 'courseassignment.affidavitpanel',
                            name: 'affidavitrequired',
                            hidden: me.affidavitId == 0,
                            userId: me.userId,
                            courseId: (me.syllabusTypeId == Symphony.SyllabusType['final'] ? me.onlineCourseId : 0),
                            trainingProgramId: me.trainingProgramId,
                            customerId: me.customerId,
                            affidavitId: me.affidavitId,
                            mode: 'live',
                            listeners: {
                                nextitem: function () {
                                    me.next();
                                },
                                exit: function () {
                                    me.exit();
                                },
                                updateContainer: function (parameters) {
                                    me.updateContainer(parameters);
                                }
                            }
                        }]
                    }]
                })
            }
            this.callParent(arguments);
        },
        loadLaunchStep: function (launchStep) {
            var me = this;
            launchStep.loading = true;
            Symphony.Ajax.request({
                url: launchStep.contentUrl,
                method: 'get',
                success: function (result) {
                    if (result.data.isSkip && !launchStep.isLast) {
                        me.next();
                    } else {
                        var item = JSON.parse(Symphony.Renderer.displayTextRenderer(result.data.content));
                        launchStep = Ext.apply(launchStep, item);
                        me.parseLaunchStep(launchStep);
                    }
                }
            });
        },
        parseLaunchStep: function (launchStep) {
            var title = launchStep.title.value;
            var iconCls = launchStep.iconCls ? launchStep.iconCls : '';
            
            for (var j = 0; j < launchStep.items.length; j++) {
                var anchor = parseInt(launchStep.items[j].anchor, 10);
                var actual = 94;

                if (anchor < 0) {
                    actual = actual + anchor;
                }
                
                launchStep.items[j].anchor = actual + "%";

                if (launchStep.items[j].items) {
                    for (var x = 0; x < launchStep.items[j].items.length; x++) {
                        if (launchStep.items[j].items[x].xtype == 'button') {
                            launchStep.items[j].hidden = true;
                            launchStep.nextOverride = launchStep.items[j].items[x].text;
                        }
                    }
                }
            }

            if (this.rendered) {
                var currentSteps = this.query('form');
                if (currentSteps[launchStep.stepIndex]) {
                    var currentStep = currentSteps[launchStep.stepIndex];

                    currentStep.height = launchStep.height;
                    currentStep.width = launchStep.width;
                    currentStep.title = launchStep.title.value;
                    currentStep.removeAll(true);

                    for (var x = 0; x < launchStep.items.length; x++) {
                        var alias = 'widget.' + (launchStep.items[x].xtype ? launchStep.items[x].xtype : 'panel');
                        var xclass = Ext.ClassManager.getNameByAlias(alias);
                        currentStep.add(Ext.create(xclass, launchStep.items[x]));
                    }
                    currentStep.doLayout();
                    currentStep.loading = false;

                    if (Ext.isFunction(currentStep.updateContainer)) {
                        currentStep.updateContainer(currentStep);
                    }
                }
            } else {
                launchStep.loading = false;
                if (this.items && this.items.items && this.items.items.length) {
                    Ext.apply(this.items.items[launchStep.stepIndex], launchStep);
                }
            }
        },
        updateContainer: function (activeItem) {
            var parameters = {
                width: activeItem.width && activeItem.width > this.minWidth ? activeItem.width : this.minWidth,
                height: activeItem.height && activeItem.height > this.minHeight ? activeItem.height : this.minHeight,
                title: activeItem.title.value ? activeItem.title.value : activeItem.title,
                iconCls: activeItem.iconCls
            };

            var params = {};
            Ext.apply(params, parameters, {
                height: 205,
                width: 310,
                title: 'Launch Course',
                iconCls: 'info'
            });

            var maxWidth = Symphony.App.container.getWidth() - 50;
            var maxHeight = Symphony.App.container.getHeight() - 50;

            params.width = params.width < maxWidth ? params.width : maxWidth;
            params.height = params.height < maxHeight ? params.height : maxHeight;

            this.setSize(params.width, params.height);
            this.setTitle(params.title, params.iconCls);

            this.center();
        },
        processForm: function (callback) {
            if (!_ui(this) && !this.useNewVersion) { return; }

            var container = this.launchWizard.getLayout().activeItem;
            var form = container.getForm ? container.getForm() : null;
            var data = form && form.getValues ? form.getValues() : {};

            var invalidFields = [];

            var isWaitForCallback = false;

            if (form && container.jsonFieldName) {
                if (!form.isValid()) {
                    Ext.Msg.alert("Error", "Some required fields are missing.");
                    return false;
                }

                if (container.postUrl) {
                    var postData = {};

                    isWaitForCallback = true;

                    postData[container.jsonFieldName] = JSON.stringify(data);

                    this.submitForm(
                        container.postUrl,
                        postData,
                        callback
                    );
                }
            }

            // In case this form doesn't actually post anything, fire the callback right away
            if (!isWaitForCallback && Ext.isFunction(callback)) {
                callback();
            }

            return true;
        },
        submitForm: function(url, data, callback) {
            if (!_ui(this) && !this.useNewVersion) { return; }

            Symphony.Ajax.request({
                method: 'post',
                url: url,
                jsonData: data,
                success: function () {
                    if (Ext.isFunction(callback)) {
                        callback();
                    }
                }
            });
        },
        exit: function() {
            this.close();
        },
        next: function () {
            var activeItem = this.launchWizard.getLayout().activeItem;
            var activeIndex = this.launchWizard.items.indexOf(activeItem);
            var nextIndex = activeIndex + 1;

            if (nextIndex < this.launchWizard.items.length) {
                this.launchWizard.getLayout().setActiveItem(nextIndex);
            } else {
                this.launch();
            }
        },
        launch: function () {
            if (_ui(this) || this.useNewVersion) {
                if (!this.preventClose) {
                    this.exit();
                }
                Symphony.Portal.launchOnlineCourse_NewUI(this.launchLink, this.bookmark);
                _log("Cleanup - LaunchCourseWizard Launch function - rewritten, use this version only");
            } else {
                this.exit();
                Symphony.Portal.launchOnlineCourseStart(this.userId, this.onlineCourseId, this.trainingProgramId, this.bookmark);
            }
        }
    });

})();