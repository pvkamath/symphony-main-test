﻿Ext.define('portal.portalpanel', {
    extend: 'Ext.app.PortalPanel',
    alias: 'widget.portal.portalpanel',
    moveItem: function (panel, row, col) {
        var columns = this.query('[xtype=portalcolumn]'),
            column = columns[col];

        if (column && column.insert) {
            column.insert(row, panel);
        }
    },
    applyConfiguration: function (widgetConfigArray) {
        if (!widgetConfigArray || !widgetConfigArray.length) {
            var portlets = this.query('portlet');
            for (var i = 0; i < portlets.length; i++) {
                if (typeof (portlets[i].initPortlet) === 'function') {
                    portlets[i].initPortlet();
                }
            }
        } else {
            for (var i = 0; i < widgetConfigArray.length; i++) {
                var config = widgetConfigArray[i],
                    elmID = config.widgetName.replace(/ /g, '').toLowerCase(),
                    newPosition = this.calcPosition(config.widgetOrderNumber),
                    portlet = this.query("[itemId=portal." + elmID + "]")[0];

                if (!config.widgetIsVisible) {
                    continue;
                } else {
                    if (typeof (portlet.initPortlet) === 'function') {
                        portlet.initPortlet();
                    }
                }

                if (portlet) {
                    this.moveItem(portlet, newPosition.row, newPosition.column);
                }
            }
        }

        this.doLayout();
    },
    calcPosition: function (p) {
        return { column: (p % 2) ? 0 : 1, row: Math.floor(p / 2) };
    }
});
