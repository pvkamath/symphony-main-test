﻿(function () {
    return false;
    window.janrainLogin = true;

    if (typeof window.janrain !== 'object') window.janrain = {};
    window.janrain.settings = {};
    window.janrain.settings.capture = {};

        /* _______________ can edit below this line _______________ */

    janrain.settings.packages = ['capture'];
    janrain.settings.capture.clientId = 'ar99k2muauf3g5zs46akag8rn4phez8p';
    janrain.settings.capture.appid = 'btywcng9mhetspnbr5t4tqbedf';
    janrain.settings.appUrl = 'https://oncourselearning-dev.rpxnow.com/';
    janrain.settings.capture.flowName = 'default';
    janrain.settings.capture.captureServer = 'https://oncourselearning-dev.janraincapture.com';
    janrain.settings.capture.redirectUri = 'http://localhost:58599/Handlers/JanrainHandler.ashx',
    janrain.settings.capture.confirmModalClose = true;
    janrain.settings.capture.responseType = 'token';
    janrain.settings.capture.modalBorderOpacity = 1;
    janrain.settings.capture.modalBorderWidth = 5;
    janrain.settings.capture.modalBorderRadius = 20;
    janrain.settings.capture.modalCloseHtml = 'X';
    janrain.settings.capture.modalBorderColor = '#7AB433';
    janrain.settings.capture.noModalBorderInlineCss = true;
    //janrain.settings.capture.stylesheets = ['stylesheets/your.css', 'stylesheets/your_other.css'];
    //janrain.settings.capture.conditionalIEStylesheets = ['stylesheets/your-ie.css'];

        /* _______________ can edit above this line _______________ */

    function isReady() { janrain.ready = true; };
    if (document.addEventListener) {
        document.addEventListener("DOMContentLoaded", isReady, false);
    } else {
        window.attachEvent('onload', isReady);
    }

    /*var e = document.createElement('script');
    e.type = 'text/javascript';
    e.id = 'janrainAuthWidget';

    e.src = 'https://d29usylhdk1xyu.cloudfront.net/load/oncourselearning-dev';

    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(e, s);*/
    

})();

