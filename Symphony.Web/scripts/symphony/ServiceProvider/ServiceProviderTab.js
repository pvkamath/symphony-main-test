﻿ (function () {       
    Symphony.ServiceProvider.ServiceProviderTab = Ext.define('serviceprovider.serviceprovidertab', {
        alias: 'widget.serviceprovider.serviceprovidertab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(me, {
                layout: 'border',
                border: false,
                items: [{
                    split: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    xtype: 'serviceprovider.serviceprovidergrid',
                    listeners: {
                        rowclick: function (grid, rowIndex, e) {
                            var record = grid.getStore().getAt(rowIndex);
                            me.addPanel(record.get('id'), record.get('name'), record);
                        },
                        addServiceProvider: function () {
                            me.addPanel(0, 'New Service Provider');
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    //layout: 'fit',
                    enableTabScroll: true,
                    xtype: 'tabpanel',
                    ref: 'tabPanel'
                }],
                addPanel: function (serviceProviderId, serviceProviderName, record) {
                    var me = this;
                        
                    var tabPanel = me.tabPanel;
                    var found = tabPanel.find('serviceProviderId', serviceProviderId);

                    if (!found.length) {
                        var panel = tabPanel.add({
                            xtype: 'serviceprovider.serviceproviderpanel',
                            closable: true,
                            activate: true,
                            record: record,
                            title: serviceProviderName,
                            licenseId: serviceProviderId,
                            border: false,
                            listeners: {
                                save: function () {
                                    me.find('xtype', 'serviceprovider.serviceprovidergrid')[0].refresh();
                                },
                                cancel: function () {
                                    tabPanel.remove(panel);
                                }
                            }
                        });
                        panel.show();
                    } else {
                        tabPanel.setActiveTab(found[0]);
                    }
                    tabPanel.doLayout();
                }
            });
            Symphony.ServiceProvider.ServiceProviderTab.superclass.initComponent.apply(me, arguments);
        }
    });

} ());
