﻿(function () {
    Symphony.ServiceProvider.ServiceProviderGrid = Ext.define('serviceprovider.serviceprovidergrid', {
        alias: 'widget.serviceprovider.serviceprovidergrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/serviceprovider.svc/serviceprovider/';

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'name',*/
                    header: 'Name',
                    dataIndex: 'name',
                    align: 'left'
                }, {
                    /*id: 'url',*/
                    dataIndex: 'url',
                    header: 'Url',
                    align: 'left',
                    width: 30,
                    flex: 1
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                colModel: colModel,
                url: url,
                model: 'serviceProvider',
                viewConfig: {
                    forceFit: true
                },
                tbar: {
                    items: [{
                        xtype: 'button',
                        iconCls: 'x-button-add',
                        text: 'Add Service Provider',
                        handler: function () {
                            me.fireEvent('addServiceProvider');
                        }
                    }]
                }
            });

            this.callParent(arguments);
        }
    });

})();