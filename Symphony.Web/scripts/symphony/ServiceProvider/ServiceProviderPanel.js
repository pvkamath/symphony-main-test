﻿(function () {
    Symphony.ServiceProvider.ServiceProviderPanel = Ext.define('serviceprovider.serviceproviderpanel', {
        alias: 'widget.serviceprovider.serviceproviderpanel',
        extend: 'Ext.Panel',
        serviceProviderId: 0,
        record: null,
        methodStore: new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [
                ['urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect', 'Redirect'],
                ['urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST', 'Post'],
                ['urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact', 'Artifact']
            ]
        }),
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                border: false,
                layout: 'fit',
                defaults: {
                    border: false
                },
                bodyStyle: 'padding: 15px',
                tbar: [{
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'x-button-save',
                    handler: function () {
                        me.save();
                    }
                }, {
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        me.fireEvent('cancel');
                    }
                }],
                items: [{
                    name: 'serviceprovider',
                    xtype: 'form',
                    border: false,
                    frame: true,
                    bodyStyle: 'overflow-y:auto; padding-right:10px',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Service Provider Info',
                        defaults: { anchor: '100%', xtype: 'textfield' },
                        items: [{ 
                            name: 'name',
                            fieldLabel: 'Name',
                            allowBlank: false
                        }, {
                            name: 'nameIdentifier',
                            fieldLabel: 'Name Identifier',
                            help: 'The unique ID for this service provider; used to look up the incoming request based on the <issuer> block'
                        }, {
                            name: 'url',
                            fieldLabel: 'Url',
                            allowBlank: false
                        }, {
                            name: 'assertionUrl',
                            fieldLabel: 'Assertion Url'
                        }, {
                            name: 'artifactUrl',
                            fieldLabel: 'Artifact Url',
                            allowBlank: false
                        }, {
                            name: 'logoutUrl',
                            fieldLabel: 'Logout Url',
                            allowBlank: false
                        },
                        Symphony.getCombo('method', 'Method', me.methodStore, {
                            valueField: 'id',
                            displayField: 'text'
                        }), {
                            name: 'isRedirectArtifact',
                            xtype: 'checkbox',
                            fieldLabel: 'Redirect Artifact',
                            allowBlank: false,
                            help: 'If set and the method is artifact, the artifact will be sent via Redirect instead of POST'
                        }, {
                            name: 'isDisableTheme',
                            xtype: 'checkbox',
                            fieldLabel: 'Disable Theme',
                            help: 'If set, the theme parameters on the customer will be ignored when serialized to a SAML attribute.'
                        }]
                    }]
                }],
                listeners: {
                    afterrender: function () {
                        me.loadData(me.record);
                    }
                }
            });

            this.callParent(arguments);
        },
        save: function () {
            // saves a training program bundle
            var me = this;

            var form = me.find('name', 'serviceprovider')[0];

            if (!form.isValid()) {
                Ext.Msg.alert('Errors Detected', 'Your service provider has some errors. Please correct and re-save');
                return;
            }
       
            var serviceProvider = form.getValues();

            Symphony.Ajax.request({
                url: '/services/serviceprovider.svc/serviceprovider/' + me.serviceProviderId,
                jsonData: serviceProvider,
                success: function (result) {
                    var recordType = Ext.data.Record.create(Symphony.Definitions.serviceProvider);
                    me.record = new recordType(result.data);
                    
                    me.loadData(me.record.data);

                    me.serviceProviderId = me.record.get('id');
                    me.setTitle(result.data.name);

                    me.fireEvent('save');
                }
            });
        },
        loadData: function(record) {
            if (record && record.data) {
                this.find('name', 'serviceprovider')[0].bindValues(record.data);
                this.serviceProviderId = record.get('id');
            }
        }
    });


}(window));
