﻿(function () {
    Ext.Loader.setConfig({
        enabled: false
    });
    Ext.onReady(function () {
        var href = window.location.href;
        var id = href.substring(href.lastIndexOf('/') + 1);
        
        var mbApp = Ext.create('messageboard.app', {
            messageBoardId: id,
            showForumGrid: false
        });
        Symphony.App = new Ext.Viewport({
            layout: 'fit',
            items: [mbApp]
        });
        mbApp.loadMessageBoardById(id);
    });
})();