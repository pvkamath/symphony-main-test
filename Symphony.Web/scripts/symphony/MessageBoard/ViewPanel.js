﻿(function () {
    Symphony.MessageBoard.ViewPanel = Ext.define('messageboard.viewpanel', {
        alias: 'widget.messageboard.viewpanel',
        extend: 'Ext.Panel',
        initComponent: function () {
            Ext.apply(this, {
                itemDef: this.itemDef,
                itemId: this.itemId,
                recordId: this.recordId,
                border: false,
                layout: 'fit',
                items: [{
                    xtype: this.itemDef.xtype,
                    recordId: this.recordId,
                    record: this.record,
                    parentRecord: this.parentRecord,
                    canSticky: this.canSticky,
                    isInstructor: this.isInstructor,
                    classData: this.classData,
                    className: this.className,
                    messageBoardType: this.messageBoardType
                }]
            });

            this.callParent(arguments);
        },
        refresh: function () {
            this.query('panel[recordId=' + this.recordId + ']')[0].refresh();
        }
    });

})();

