﻿(function () {
    Symphony.MessageBoard.Form = Ext.define('messageboard.form', {
        alias: 'widget.messageboard.form',
        extend: 'Ext.form.Panel',
        inline: false,
        messageBoardId: 0,
        messageBoardData: null,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                border: false,
                cls: 'x-panel-transparent',
                listeners: {
                    afterrender: function () {
                        if (me.messageBoardId > 0) {
                            var mask = new Ext.LoadMask(me, { msg: "Loading..." });
                            mask.show();
                            Symphony.Ajax.request({
                                url: '/services/messageboard.svc/messageBoard/' + me.messageBoardId,
                                method: 'GET',
                                success: function (result) {
                                    me.messageBoardData = result.data;
                                    me.bindValues(result.data);
                                },
                                complete: function () {
                                    mask.hide();
                                }
                            });
                        }
                    }
                },
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Create a message board on save',
                    name: 'isCreateMessageBoard',
                    hidden: !this.inline
                }, {
                    xtype: 'textfield',
                    name: this.inline ? 'messageBoardName' : 'name',
                    fieldLabel: 'Name',
                    anchor: '100%',
                    allowBlank: this.inline
                }, {
                    xtype: 'checkbox',
                    name: 'isDisableTopicCreation',
                    fieldLabel: 'Disable Topics',
                    boxLabel: 'Prevents students from creating new topics.'
                }]
            });

            this.callParent(arguments);
        },
        save: function () {
            var me = this,
                messageBoard = me.messageBoardData,
                mask = new Ext.LoadMask(me, { msg: "Saving..." });

            Ext.apply(messageBoard, me.getValues());

            messageBoard.id = me.messageBoardId;

            mask.show();

            Symphony.Ajax.request({
                url: '/services/messageboard.svc/messageBoard/' + me.messageBoardId,
                method: 'POST',
                jsonData: messageBoard,
                success: function (result) {
                    me.fireEvent('saved', result.data);
                },
                complete: function () {
                    mask.hide();
                }
            });
        }
    });

})();

