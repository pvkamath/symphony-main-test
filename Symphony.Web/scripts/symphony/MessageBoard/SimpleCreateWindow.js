﻿(function () {
    Symphony.MessageBoard.SimpleCreateWindow = Ext.define('messageboard.simplecreatewindow', {
        alias: 'widget.messageboard.simplecreatewindow',
        extend: 'Ext.Panel',
        baseUrl: '/services/messageboard.svc/messageBoard/0',
        initComponent: function () {
            var me = this;
            

            Ext.apply(this, {
                border: false,
                layout: {
                    type: 'vbox',
                    pack: 'center',
                    align: 'center'
                },
                bubbleEvents: ['loadForum'],
                listeners: {
                    render: function (panel) {
                        var parent = panel.findParentByType('[xtype=messageboard.viewpanel]'),
                            name = parent.find('name', 'name')[0];
                        
                        parent.setTitle('');

                        setTimeout(function () {
                            panel.doLayout();
                        }, 200);

                        name.setValue(me.record.data.name);
                    }
                },
                items: [{
                    xtype: 'panel',
                    flex: 0.3,
                    border: false,
                    frame: false,
                    height: 1
                }, {
                    xtype: 'form',
                    height: 180,
                    frame: true,
                    title: 'Message Board Not Found',
                    border: false,
                    width: 400,
                    layout: {
                        type: 'vbox',
                        pack: 'start',
                        align: 'stretch',
                        defaultMargins: 10
                    },
                    tbar: {
                        items: [{
                            name: 'send',
                            text: 'Create',
                            iconCls: 'x-button-save',
                            handler: function () {
                                var messageboard = me.find('xtype', 'form')[0].getValues();
                                
                                Ext.apply(me.record.data, messageboard);

                                Symphony.Ajax.request({
                                    url: me.baseUrl,
                                    jsonData: me.record.data,
                                    method: 'POST',
                                    success: function (result) {
                                        me.hide();
                                        me.fireEvent('loadForum', result);
                                    }
                                });

                            }
                        }]
                    },
                    items: [{
                        xtype: 'label',
                        text: 'A message board was not found for this class. Provide a name for this message board to create one now.',
                        cls: 'x-form-item',
                        flex: 0.3
                    }, {
                        xtype: 'messageboard.form',
                        flex: 0.7
                    }]
                }, {
                    xtype: 'panel',
                    flex: 0.3,
                    height: 110,
                    border: false,
                    frame: false
                }]
            });

            this.callParent(arguments);
        }
    });

})();

