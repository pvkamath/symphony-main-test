﻿(function () {
    Symphony.MessageBoard.Create = Ext.define('messageboard.create', {
        alias: 'widget.messageboard.create',
        extend: 'Ext.form.FieldSet',
        initComponent: function () {
            Ext.apply(this, {
                title: 'Message Board',
                items: [{
                    xtype: 'messageboard.form',
                    labelWidth: 200,
                    inline: true
                }]
            });

            this.callParent(arguments);
        }
    });

})();

