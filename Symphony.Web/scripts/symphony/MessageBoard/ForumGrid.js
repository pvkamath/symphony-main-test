﻿(function () {
    Symphony.MessageBoard.ForumGrid = Ext.define('messageboard.forumgrid', {
        alias: 'widget.messageboard.forumgrid',
        extend: 'symphony.simplegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/messageboard.svc/messageBoard/';

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    width: 20,
                    hidden: !Symphony.MessageBoard.IsManager,
                    renderer: function () {
                        return '<img class="edit-messageboard" src="/images/pencil.png" ext:qtip="Edit Message Board"></img>';
                    }
                }, {
                    /*id: 'name',*/
                    header: 'Name',
                    dataIndex: 'name',
                    align: 'left',
                    renderer: function (value, p, record) {
                        var d = record.data;
                        var messageBoardType = {};

                        for (var i = 0; i < Symphony.MessageBoard.Types.length; i++) {
                            if (Symphony.MessageBoard.Types[i].id === record.data.messageBoardType) {
                                messageBoardType = Symphony.MessageBoard.Types[i];
                            }
                        }

                        var courseName = "";
                        if (messageBoardType.text && d.courseName) {
                            courseName = String.format('<span class="coursename">{0}: {1}</span>', messageBoardType.text, d.courseName);
                        }

                        return String.format(
                            '<span class="forumName">{0}</span><br/>{1}</span><br/>'
                        , d.name, courseName);
                    },
                    flex: 1
                }, {
                    /*id: 'posts',*/
                    dataIndex: 'posts',
                    header: 'Posts',
                    align: 'center',
                    width: 30
                }, {
                    /*id: 'lastUpdated',*/
                    dataIndex: 'lastUpdated',
                    header: 'Updated',
                    align: 'right',
                    width: 45,
                    renderer: function (value, p, record) {
                        var d = record.data;
                        if (d.posts > 0) {
                            return String.format(
                                '<div class="lastPost">' +
                                    '<span class="date">{0}</span><br/><span class="author">by {1}</span>' +
                                '</div>',
                                Symphony.shortDateRenderer(d.lastUpdated), d.lastPostBy
                            );
                        } else {
                            return "";
                        }
                    }
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                colModel: colModel,
                url: url,
                model: 'messageBoard',
                cls: 'messageboards',
                bubbleEvents: ['loadForum'],
                viewConfig: {
                    forceFit: true
                },
                tbar: {
                    items: [{
                        xtype: 'button',
                        iconCls: 'x-button-add',
                        hidden: !Symphony.MessageBoard.IsManager,
                        text: 'New Message Board',
                        handler: function () {
                            me.editMessageBoard(null, me);
                        }
                    }]
                },
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        if (columnIndex === 0) {
                            var record = grid.getStore().getAt(rowIndex);
                            me.editMessageBoard(record, me);
                        } else {
                            me.fireEvent('loadForum', grid.getStore().getAt(rowIndex));
                        }
                    }
                }
            });

            this.callParent(arguments);
        },
        editMessageBoard: function (record, me) {
            var w = new Ext.Window({
                title: record ? 'Edit Message Board' : 'New Message Board',
                width: 540,
                height: 460,
                layout: 'fit',
                border: false,
                modal: true,
                items: [{
                    xtype: 'messageboard.forumpanel',
                    record: record,
                    callback: function (result) {
                        me.refresh();
                        w.close();
                    }
                }]
            }).show();
        }
    });

})();