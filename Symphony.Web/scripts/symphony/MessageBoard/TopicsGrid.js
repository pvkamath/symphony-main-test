﻿(function () {
    Symphony.MessageBoard.TopicsGrid = Ext.define('messageboard.topicsgrid', {
        alias: 'widget.messageboard.topicsgrid',
        extend: 'symphony.simplegrid',
        baseUrl: '/services/messageboard.svc/topic/',
        url: '',
        initComponent: function () {
            var me = this;
            me.url = me.baseUrl + me.recordId;
            var columns = {
                items: [{
                    header: 'Topic',
                    dataIndex: 'title',
                    align: 'left',
                    renderer: function (value, p, record) {
                        var d = record.data;
                        return String.format(
                            '<div class="topic">' +
                                '<span class="title">{0}</span><br/>' +
                                '<span class="createdOn">Created by {1}, {2}</span><br/>' +
                                '<p class="preview {0}">{1}</p>'.format(Symphony.MessageBoard.IsManager ? 'hasDeleteColumn' : '', record.get('preview') ? record.get('preview') : '') +
                            '</div>',
                            value, d.createdBy, Symphony.shortDateRenderer(d.createdOn));
                    },
                    flex: 1
                }, {
                    dataIndex: 'posts',
                    header: 'Posts',
                    align: 'center',
                    width: 60
                }, {
                    dataIndex: 'lastPost',
                    header: 'Last Post Date',
                    align: 'right',
                    renderer: function (value, p, record) {
                        var d = record.data;
                        if (d.lastPostBy) {
                            return String.format(
                                '<div class="lastPost">' +
                                    '<span class="date">{0}</span><br/><span class="author">by {1}</span>' +
                                '</div>',
                                Symphony.longDateRenderer(d.lastPost), d.lastPostBy
                            );
                        }
                        return '';
                    },
                    width: 200
                }]
            };


            if (Symphony.MessageBoard.IsManager) {
                columns.items = [{
                    dataIndex: 'isDeleted',
                    header: ' ',
                    renderer: Symphony.MessageBoard.deleteTopicRenderer,
                    width: 28,
                    fixed: true
                }].concat(columns.items);
            }


            Ext.apply(this, {
                border: false,
                forceFit: false,
                columns: columns,
                url: me.url,
                model: 'messageBoardTopic',
                cls: 'topics-list',
                tbar: [{
                    xtype: 'button',
                    text: 'New Topic',
                    iconCls: 'x-button-add',
                    hidden: (me.record.data.isDisableTopicCreation && !Symphony.MessageBoard.IsManager),
                    handler: function () {
                        me.fireEvent('editTopic', me.record, false, me.refreshTopics, me);
                    }
                }, {
                    xtype: 'button',
                    text: 'Message Board Settings',
                    iconCls: 'x-button-gear',
                    hidden: !Symphony.MessageBoard.IsManager,
                    handler: function () {
                        me.fireEvent('editSettings');
                    }
                }, '->', {
                    xtype: 'button',
                    text: 'Get Link',
                    iconCls: 'x-button-link',
                    handler: function () {
                        var text = window.location.protocol + '//' + window.location.host + '/messageboard/' + me.recordId;
                        window.prompt('Copy to the clipboard using: Ctrl+C, Enter', text);
                    }
                }],
                bubbleEvents: ['loadTopic', 'editTopic', 'deleteTopic', 'restoreTopic', 'editSettings'],
                listeners: {
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex;
                        var record = grid.getStore().getAt(rowIndex);
                        if (fieldName == 'isDeleted') {
                            if (!record.data.isDeleted) {
                                me.fireEvent('deleteTopic', record, me.refreshTopics, me);
                            } else {
                                me.fireEvent('restoreTopic', record, me.refreshTopics, me);
                            }
                        } else {
                            me.fireEvent('loadTopic', record, me.record);
                        }
                    }
                }
            });

            this.callParent(arguments);

            console.warn("For some reason adding getRowClass to the view config returns blank rows. However, adding the function afterwards appears to work. Look into this.");
            this.getView().getRowClass = function (record, rowIndex, rowParams, store) {
                var cls = 'topic-row';
                if (record.data.isSticky) {
                    cls += ' sticky';
                }
                if (record.data.isLocked) {
                    cls += ' locked';
                }
                if (record.data.isDeleted) {
                    cls += ' deleted';
                }

                return cls;
            }

        },
        refreshTopics: function (result, me) {
            var store = me.getStore();
            var originalStoreUrl = store.proxy.url;
            var refreshListener = function () {
                // Highlight a new topic, and restore
                // the url to the original
                var newRecordIndex = store.find('id', result.data.id);
                var record = store.getAt(newRecordIndex);
                var rowEl = me.getView().getNodeByRecord(record);

                rowEl.scrollIntoView();
                Ext.fly(rowEl).highlight();

                store.removeListener('load', refreshListener);
            }

            // We have added a new topic
            // send the topic id to the server to switch pages
            // to automatically show the topic after creating
            // ---- This isn't working - It doesn't update the pager when changed.
            //store.proxy.url = store.proxy.url + '/' + result.data.id;
            //store.proxy.url = store.proxy.url;
            if (result && result.data) {
                store.addListener('load', refreshListener);
            }


            me.refresh();
        }
    });

})();