﻿(function () {
    Symphony.MessageBoard.PostPanel = Ext.define('messageboard.postpanel', {
        alias: 'widget.messageboard.postpanel',
        extend: 'Ext.Panel',
        baseUrl: '/services/messageboard.svc/post/',
        topicUrl: '/services/messageboard.svc/topic/',
        initComponent: function () {
            var me = this;

            var buttonTitle;
            if (me.isEdit) {
                buttonTitle = 'Update Post';
            } else if (me.isReply) {
                buttonTitle = 'Create Reply';
            } else {
                buttonTitle = 'Create Post';
            }

            Ext.apply(this, {
                border: false,
                items: [{
                    xtype: 'form',
                    frame: true,
                    items: [{
                        xtype: 'htmleditor',
                        name: 'content',
                        hideLabel: true,
                        height: 310,
                        anchor: '100%'
                    }, {
                        xtype: 'checkbox',
                        name: 'isPrivate',
                        labelSeparator: '',
                        hideLabel: true,
                        boxLabel: 'Make this a private message.'
                    }]
                }],
                listeners: {
                    afterrender: function (grid) {
                        var isPrivate = me.find('name', 'isPrivate')[0];

                        if (me.record && me.isEdit) {
                            // editing a post, need to use the post userid since admins might be editing
                            me.find('xtype', 'form')[0].bindValues(me.record.data);

                            if (me.record.data.postId) { // We are editing a reply to an existing post
                                isPrivate.setDisabled(true);
                                // replies can only be private if the owner of the parent is not equal to
                                // the owner of the reply
                                Symphony.Ajax.request({
                                    method: 'GET',
                                    url: me.baseUrl + me.record.data.topicId + '/' + me.record.data.postId,
                                    success: function (args) {
                                        isPrivate.setDisabled(args.data.userId === me.record.data.userId);
                                    }
                                });

                            } else { // We are editing a regular post
                                // Regular posts cannot be private if the creator of the post is also the creator of the topic
                                // or the post is the first post in the topic
                                isPrivate.setDisabled(me.topic.data.userId === me.record.data.userId || me.record.data.isFirst);

                            }
                        } else {
                            // Creating a new post from the current user
                            if (me.isReply) {
                                var isPrivateDisabled = me.record.data.userId === Symphony.User.id;

                                isPrivate.setDisabled(isPrivateDisabled);

                                if (!isPrivateDisabled) {
                                    if (me.record.data.isPrivate) {
                                        isPrivate.setValue(true);
                                    }
                                }
                            } else { // Regular post
                                // regular posts can only be private if they are not the first post, and the owner
                                // of the topic is not the creator of the post
                                isPrivate.setDisabled(me.topic.data.userId === Symphony.User.id || me.record.data.isFirst);
                            }
                        }
                    }
                },
                buttons: [{
                    name: 'send',
                    text: buttonTitle,
                    handler: function () {
                        var form = me.find('xtype', 'form')[0].getForm();
                        var formData = form.getValues();
                        var data, id;

                        if (me.isEdit) {
                            id = me.record.data.id;
                            data = me.record.data;
                            data.content = formData.content;
                            data.isPrivate = formData.isPrivate === 'on' ? true : false;
                        } else {
                            id = 0;
                            data = formData;
                            if (me.isReply) {
                                data.topicId = me.record.data.topicId;
                                data.postId = me.record.data.id;
                            } else {
                                data.topicId = me.record.data.id;
                            }
                            data.isPrivate = formData.isPrivate === 'on' ? true : false;
                        }

                        Symphony.Ajax.request({
                            url: me.baseUrl + id,
                            jsonData: data,
                            method: 'POST',
                            success: function (result) {
                                if (typeof me.callback === 'function') {
                                    me.callback(result);
                                }
                            }
                        });
                    }
                }]
            });

            this.callParent(arguments);
        }
    });

})();

