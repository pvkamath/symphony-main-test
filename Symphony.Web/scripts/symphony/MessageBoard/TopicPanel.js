﻿(function () {
    Symphony.MessageBoard.TopicPanel = Ext.define('messageboard.topicpanel', {
        alias: 'widget.messageboard.topicpanel',
        extend: 'Ext.Panel',
        baseUrl: '/services/messageboard.svc/topic/',
        initComponent: function () {
            var me = this;

            var items = [{
                xtype: 'textfield',
                name: 'title',
                fieldLabel: 'Title',
                anchor: '100%',
                value: me.isEdit ? me.record.data.title : ''
            }];

            if (me.isEdit) {
                items.push({
                    xtype: 'checkbox',
                    name: 'isLocked',
                    labelSeparator: '',
                    hideLabel: true,
                    boxLabel: 'Lock this topic.',
                    anchor: '100',
                    checked: me.isEdit ? me.record.data.isLocked : false
                });
            } else {
                items.push({
                    xtype: 'htmleditor',
                    name: 'content',
                    hideLabel: true,
                    height: 300,
                    value: '',
                    anchor: '100%'
                });
            }

            items.push({
                xtype: 'checkbox',
                name: 'isSticky',
                disabled: !me.canSticky,
                labelSeparator: '',
                hideLabel: true,
                boxLabel: 'Make this a sticky topic.',
                checked: me.isEdit ? me.record.data.isSticky : false
            });

            Ext.apply(this, {
                border: false,
                cls: me.isEdit ? 'edit-topic-panel' : 'new-topic-panel',
                layout: 'fit',
                items: [{
                    xtype: 'form',
                    frame: true,
                    items: items
                }],
                buttons: [{
                    name: 'send',
                    text: me.isEdit ? 'Update Topic' : 'Create Topic',
                    handler: function () {
                        var form = me.find('xtype', 'form')[0].getForm();
                        var formData = form.getValues();
                        var data;
                        var id = me.isEdit ? me.record.data.id : 0;

                        if (me.isEdit) {
                            data = me.record.data;
                            data.title = formData.title;
                            data.isSticky = formData.isSticky === 'on' ? true : false;
                            data.isLocked = formData.isLocked === 'on' ? true : false;
                        } else {
                            data = form.getValues();
                            data.messageBoardId = me.record.data.id;
                            data.newTopicPost = formData.content;
                            data.isSticky = formData.isSticky === 'on' ? true : false;
                        }

                        Symphony.Ajax.request({
                            url: me.baseUrl + id,
                            jsonData: data,
                            method: 'POST',
                            success: function (result) {
                                if (typeof me.callback === 'function') {
                                    me.callback(result);
                                }
                            }
                        });
                    }
                }]
            });

            this.callParent(arguments);
        }
    });

})();

