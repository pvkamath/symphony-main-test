﻿(function () {
    Symphony.MessageBoard.ViewWindow = Ext.define('messageboard.viewwindow', {
        alias: 'widget.messageboard.viewwindow',
        extend: 'Ext.Window',
        initComponent: function () {
            var me = this;

            me.width = me.width || 750;
            me.height = me.height || 650;

            Ext.apply(this, {
                title: 'Message Board',
                width: me.width || 750,
                height: me.height || 650,
                layout: 'fit',
                modal: false,
                minimizable: true,
                listeners: {
                    minimize: function(window, opts) {
                        me.width = window.getWidth();
                        window.collapse(false);
                        window.setWidth(200);
                        window.alignTo(Ext.getBody(), 'bl-bl');
                    }
                },
                tools: [{
                    id: 'restore',
                    handler: function (evt, toolEl, owner, tool) {
                        if (me.collapsed) {
                            me.expand(false);
                            me.setWidth(me.width);
                            me.center();
                        }
                    }
                }],
                items: [{
                    xtype: 'messageboard.app',
                    showForumGrid: false,
                    border: false
                }]
            });

            this.callParent(arguments);
        },
        loadMessageBoardById: function (messageBoardId, trainingProgramId, showCanMoveForwardButtons) {
            this.find('xtype', 'messageboard.app')[0].loadMessageBoardById(messageBoardId, trainingProgramId, showCanMoveForwardButtons);
        }
    });

})();

