﻿(function () {
    Symphony.MessageBoard.ForumPanel = Ext.define('messageboard.forumpanel', {
        alias: 'widget.messageboard.forumpanel',
        extend: 'Ext.Panel',
        baseUrl: '/services/messageboard.svc/messageBoard/',
        isNameSet: false,
        assignedToRecord: {},
        assignedToFieldName: '',
        isEdit: false,
        formLoaded: false,
        initComponent: function () {
            var me = this;

            me.isEdit = me.record ? true : false;

            var messageBoardTypeStore = new Ext.data.JsonStore({
                fields: ['id', 'text', 'fieldName'],
                data: { data: Symphony.MessageBoard.Types },
                root: 'data',
                idProperty: 'id'
            });

            var typeCombo = Symphony.getCombo('type', '', messageBoardTypeStore, {
                anchor: '100%',
                hideLabel: true,
                allowBlank: false,
                displayField: 'text',
                valueField: 'id',
                listeners: {
                    select: function (cbo, record, index) {
                        me.gridPanel.layout.setActiveItem(index);

                        var activeItem = me.gridPanel.getComponent(index);

                        me.assignedToFieldName = record.data.fieldName;

                        activeItem.setDisabled(false);

                        if (activeItem.getStore().getCount() === 0) {
                            activeItem.refresh();
                        }
                    }
                }
            });
            var gridHeight = 300;

            Ext.apply(this, {
                border: false,
                items: [{
                    xtype: 'form',
                    frame: true,
                    items: [{
                        xtype: 'textfield',
                        name: 'name',
                        fieldLabel: 'Name',
                        anchor: '100%',
                        enableKeyEvents: true,
                        allowBlank: false,
                        listeners: {
                            keyup: function (field, e) {
                                if (field.getValue() !== '') {
                                    me.isNameSet = true;
                                } else {
                                    me.isNameSet = false;
                                }
                            }
                        }
                    }, {
                        xtype: 'label',
                        text: 'Assign this message board to a(n):'
                    },
                    typeCombo,
                    {
                        xtype: 'label',
                        name: 'assignedTo',
                        text: 'Assigned to: '
                    },
                    {
                        xtype: 'panel',
                        layout: 'card',
                        ref: '../gridPanel',
                        listeners: {
                            afterrender: function (panel) {
                                panel.layout.setActiveItem(0);
                            }
                        },
                        items: [{
                            xtype: 'courseassignment.trainingprogramsgrid',
                            height: gridHeight,
                            title: "Training Programs",
                            border: true,
                            disabled: true,
                            viewConfig: {
                                forceFit: true
                            },
                            selModel: new Ext.grid.RowSelectionModel({
                                singleSelect: true
                            }),
                            listeners: {
                                render: function (grid) {
                                    me.removeToolbarButtons(grid);
                                    //grid.refresh();                       
                                },
                                rowclick: function (grid, rowIndex, e) {
                                    me.setCourse(grid, rowIndex, e);
                                }
                            }
                        }, {
                            xtype: 'classroom.classesgrid',
                            height: gridHeight,
                            title: "Classes",
                            disabled: true,
                            viewConfig: {
                                forceFit: true
                            },
                            selModel: new Ext.grid.RowSelectionModel({
                                singleSelect: true
                            }),
                            listeners: {
                                render: function (grid) {
                                    me.removeToolbarButtons(grid);
                                    //grid.refresh();                             
                                },
                                rowclick: function (grid, rowIndex, e) {
                                    me.setCourse(grid, rowIndex, e);
                                }
                            }
                        }, {
                            xtype: 'courseassignment.coursesgrid',
                            height: gridHeight,
                            title: "Online Courses",
                            disabled: true,
                            viewConfig: {
                                forceFit: true
                            },
                            selModel: new Ext.grid.RowSelectionModel({
                                singleSelect: true
                            }),
                            listeners: {
                                render: function (grid) {
                                    me.removeToolbarButtons(grid);
                                    // grid.refresh();                             
                                },
                                rowclick: function (grid, rowIndex, e) {
                                    me.setCourse(grid, rowIndex, e);
                                }
                            }
                        }]
                    }]
                }],
                buttons: [{
                    name: 'send',
                    text: me.isEdit ? 'Update Message Board' : 'Create Message Board',
                    handler: function () {
                        var form = me.find('xtype', 'form')[0].getForm();
                        var formData = form.getValues();
                        var id = me.isEdit ? me.record.data.id : 0;

                        if (!form.isValid()) {
                            Ext.Msg.alert("Error", "Some values are invalid. Please check your input.");
                            return;
                        }

                        formData.messageBoardType = parseInt(formData.messageBoardType, 10);

                        messageBoardTypeStore.each(function (record) {
                            if (record.data.id === formData.messageBoardType) {
                                if (me.assignedToRecord[record.data.fieldName]) {
                                    formData[record.data.fieldName] = me.assignedToRecord[record.data.fieldName].data.id;
                                } else {
                                    me.setAssignedToText("<i style='color: red'>Please assign a course/training program</i>");
                                }
                            } else {
                                formData[record.data.fieldName] = null;
                            }
                        });

                        Symphony.Ajax.request({
                            url: me.baseUrl + id,
                            jsonData: formData,
                            method: 'POST',
                            success: function (result) {
                                if (typeof me.callback === 'function') {
                                    me.callback(result);
                                }
                            }
                        });

                    }
                }],
                listeners: {
                    afterlayout: function (panel) {
                        var formElement = me.find('xtype', 'form')[0];
                        var form = formElement.getForm();

                        if (me.record && !me.formLoaded) {
                            var combo = formElement.find('xtype', 'combo')[0];
                            var store, record, index;

                            form.setValues(me.record.data);

                            store = combo.getStore();
                            record = store.getById(combo.getValue());
                            index = store.indexOf(record);

                            if (record) {
                                combo.fireEvent('select', combo, record, index)

                                me.assignedToRecord[record.data.fieldName] = {
                                    data: {
                                        id: me.record.data[record.data.fieldName],
                                        name: me.record.data.courseName
                                    }
                                }

                                me.setAssignedToText(me.record.data.courseName);
                            }

                            me.formLoaded = true;
                        }
                    }
                }
            });

            this.callParent(arguments);
        },
        setCourse: function (grid, rowIndex, e) {
            var title = this.find('name', 'name')[0];

            var record = grid.getStore().getAt(rowIndex);
            var data = record.data;

            if (!this.isNameSet) {
                title.setValue(data.name);
            }

            this.assignedToRecord[this.assignedToFieldName] = record;
            this.setAssignedToText(data.name);

        },
        setAssignedToText: function (name) {
            var assignedTo = this.find('name', 'assignedTo')[0].getEl();

            assignedTo.update('Assigned to: ' + name);
        },
        removeToolbarButtons: function (grid) {
            var removeButtons = ['Add', 'Quick Add', 'Upload New']
            var toolbar = grid.getTopToolbar();
            var btns = toolbar.findByType('button');

            for (var i = 0; i < btns.length; i++) {
                if (removeButtons.indexOf(btns[i].getText()) >= 0) {
                    toolbar.remove(btns[i]);
                }
            }
        }
    });

})();

