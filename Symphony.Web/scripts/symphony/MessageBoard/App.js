﻿(function () {
    if (!Symphony.MessageBoard) {
        Symphony.MessageBoard = {};
    }

    Symphony.MessageBoard.Items = {
        post: {
            id: 1,
            xtype: 'messageboard.postsgrid'
        },
        topic: {
            id: 2,
            xtype: 'messageboard.topicsgrid'
        },
        create: {
            id: 3,
            xtype: 'messageboard.simplecreatewindow'
        }
    }

    // Helpers for hiding elements in the UI
    // Same rules apply in the controller

    // These roles have the ability to edit/create message boards
    Symphony.MessageBoard.IsManager = (Symphony.User.isClassroomManager ||
                                      Symphony.User.isTrainingAdministrator ||
                                      Symphony.User.isTrainingManager);

    Symphony.MessageBoard.Types = [
        { id: Symphony.MessageBoardType.trainingProgram, text: 'Training Program', fieldName: 'trainingProgramId' },
        { id: Symphony.MessageBoardType.classroom, text: 'Class', fieldName: 'classId' },
        { id: Symphony.MessageBoardType.online, text: 'Online Course', fieldName: 'onlineCourseId' }
    ];

    Symphony.MessageBoard.deleteTopicRenderer = function (value, meta, record) {
        return value ? 
            '<div class="grid-restore-topic" ext:qtip="Restore Topic"></div>' :
            '<div class="grid-delete-topic" ext:qtip="Delete Topic"></div>';
    };

    Symphony.MessageBoard.App = Ext.define('messageboard.app', {
        alias: 'widget.messageboard.app',
        extend: 'Ext.Panel',
        messageBoardId: 0,
        showForumGrid: true,
        isLockBySharedTrainingFlag: false,
        canSticky: false,
        isInstructor: false,
        forumLoaded: false,
        messageBoardType: 0,
        initComponent: function () {
            var me = this;
            
            Ext.apply(this, {
                layout: 'border',
                listeners: {
                    loadTopic: function (record, parentRecord) {
                        me.addPanel(record.data.id, record.data.title, Symphony.MessageBoard.Items.post, record, parentRecord);
                    },
                    loadForum: function (record) {
                        // Even though we have the data we need, we don't know if this messageboard includes
                        // the isInstructor flag since that is only set when getting single message boards
                        // Make a new call to get the full forum details
                        me.loadMessageBoardById(record.data.id);
                    },
                    editPost: function (record, isEdit, isReply, callback, scope) {
                        me.editPost(record, isEdit, isReply, callback, scope);
                    },
                    editTopic: function (record, isEdit, callback, scope) {
                        me.editTopic(record, isEdit, callback, scope);
                    },
                    deletePost: function(record, callback, scope) {
                        me.deletePost(record, callback, scope);
                    },
                    restorePost: function(record, callback, scope) {
                        me.restorePost(record, callback, scope);
                    },
                    deleteTopic: function(record, callback, scope) {
                        me.deleteTopic(record, callback, scope);
                    },
                    restoreTopic: function(record, callback, scope) {
                        me.restoreTopic(record, callback, scope);
                    },
                    beforeadd: function (app, cmp, index) {
                        if (cmp && cmp.getXType && cmp.getXType() === 'messageboard.forumgrid' && !me.showForumGrid) {
                            return false;
                        }
                        return true;
                    },
                    editSettings: function () {
                        me.editSettings();
                    }
                },
                items: [{
                    split: true,
                    collapsible: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    title: 'Select a Message Board',
                    itemId: 'messageboardforumgrid',
                    xtype: 'messageboard.forumgrid',
                    bubbleEvents: ['loadForum']
                }, {
                    region: 'center',
                    border: false,
                    enableTabScroll: true,
                    layout: 'card',
                    itemId: 'messageboardpanel',
                    xtype: 'panel'
                }]
            });

            this.callParent(arguments);

            this.mainpanel = this.query('#messageboardpanel')[0];
        },
        loadMessageBoardById: function (messageBoardId, trainingProgramId, showCanMoveForwardButtons, courseTypeId) {
            var me = this;
            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/messageboard.svc/messageBoard/' + messageBoardId,
                success: function (args) {
                    var record = { data: args.data };

                    // Try to load the class data so we can show the canMoveForward buttons
                    // If we find some, great, load it in, if not we still show the message board anyway
                    if (showCanMoveForwardButtons) { // Only add if specifically requested
                        switch (args.data.messageBoardType) {
                            case Symphony.MessageBoardType.classroom:
                                Symphony.Ajax.request({
                                    url: '/services/classroom.svc/classes/' + args.data.classId,
                                    method: 'get',
                                    success: function (classData) {
                                        me.classData = classData.data.registrationList;
                                        me.className = args.data.className;
                                        me.launchMesageBoardPanel(messageBoardId, record);
                                    },
                                    failure: function () {
                                        // don't care if something failed, launch the messageboard anyway, it just wont have canMoveForward buttons
                                        me.launchMesageBoardPanel(messageBoardId, record);
                                    }
                                });
                                break;
                            case Symphony.MessageBoardType.online:
                                if (trainingProgramId) {
                                    Symphony.Ajax.request({
                                        url: '/services/courseassignment.svc/trainingprograms/' + trainingProgramId + '/onlinecourseregistrations/' + args.data.onlineCourseId + '/',
                                        method: 'get',
                                        success: function (classData) {
                                            me.classData = classData.data;
                                            me.className = args.data.onlineCourseName;
                                            me.launchMesageBoardPanel(messageBoardId, record);
                                        },
                                        failure: function () {
                                            // don't care if something failed, launch the messageboard anyway, it just wont have canMoveForward buttons
                                            me.launchMesageBoardPanel(messageBoardId, record);
                                        }
                                    })
                                } else {
                                    // We can't determine the class data at this point with out the training program id so we wont bother with the canMoveForward buttons
                                    me.launchMesageBoardPanel(messageBoardId, record);
                                }
                                break;
                            case Symphony.MessageBoardType.trainingProgram:
                                // Since we don't know which class this is for, we don't load any class data
                                me.launchMesageBoardPanel(messageBoardId, record);
                                break;
                        }
                    } else { // wasn't from a grid that requested the buttons to be added, just show the message board panel
                        me.launchMesageBoardPanel(messageBoardId, record);
                    }
                },
                failure: function () {
                    var parent = me.findParentByType('[xtype=messageboard.viewwindow]');
                    Ext.Msg.alert("Error", "Could not load message board.");

                    if (parent) {
                        parent.close();
                    }
                }
            });
        },
        launchMesageBoardPanel: function(messageBoardId, record) {
            var me = this;
            me.messageBoardType = record.data.messageBoardType;

            me.addPanel(messageBoardId, record.data.name, Symphony.MessageBoard.Items.topic, record);
            me.messageBoardId = messageBoardId;

            var containingWindow = me.findParentByType('[xtype=messageboard.viewwindow]');
            if (containingWindow) {
                var panel = me.find('xtype', 'messageboard.viewpanel')[0];

                containingWindow.setTitle(record.data.name);

                panel.getHeader().hide();
            }
        },
        loadMessageBoardByTrainingProgramId: function(name, trainingProgramId, classData, allowCreate) {
            var me = this;
            me.classData = classData;
            me.className = name;

            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/messageboard.svc/messageBoard/' + Symphony.MessageBoardType.trainingProgram + '/' + trainingProgramId,
                success: function (args) {
                    record = { data: args.data };

                    if (record.data.id !== 0) {
                        me.addPanel(record.data.id, record.data.name, Symphony.MessageBoard.Items.topic, record);
                        me.messageBoardId = record.data.id;
                    } else {
                        me.showMessageBoardNotFound(Symphony.MessageBoardType.trainingProgram, trainingProgramId, name, record, allowCreate);
                    }
                }
            });
        },
        loadMessageBoardByClassId: function(messageBoardType, classId, name, trainingProgramId, classData, allowCreate) {
            var me = this;
            me.classData = classData;
            me.className = name;

            Symphony.Ajax.request({
                method: 'GET',
                url: '/services/messageboard.svc/messageBoard/' + messageBoardType + '/' + classId,
                success: function (args) {
                    var record = { data: args.data };

                    if (record.data.id !== 0) {
                        me.addPanel(record.data.id, record.data.name, Symphony.MessageBoard.Items.topic, record);
                        me.messageBoardId = record.data.id;
                    } else {
                        // Allow creating a message board at the class level if it does not exist
                        me.showMessageBoardNotFound(messageBoardType, classId, name, record, allowCreate);
                    }
                }
            });
        },
        showMessageBoardNotFound: function (messageBoardType, classId, name, record, allowCreate) {
            record.data.messageBoardType = messageBoardType;
            for (var i = 0; i < Symphony.MessageBoard.Types.length; i++) {
                var type = Symphony.MessageBoard.Types[i];

                if (type.id === messageBoardType) {
                    record.data[type.fieldName] = classId;
                } else {
                    record.data[type.fieldName] = null;
                }
            }
            
            record.data.name = name;

            //if (this.isLockBySharedTrainingFlag) return;
            if (allowCreate) {
                this.addPanel(classId, "Create Message Board", Symphony.MessageBoard.Items.create, record, null, true);
            } else {
                Ext.Msg.alert("Message Board Not Found", "A message board is not available for this class.");
            }
        },
        addPanel: function (recordId, name, item, record, parentRecord, hideTopHeader) {
            var me = this;
            
            me.forumLoaded = true;

            if (typeof (hideTopHeader) === 'undefined') {
                hideTopHeader = false;
            }
            
            var found = me.mainpanel.queryBy(function (component, container) {
                if (component.recordId && component.itemDef) {
                    if (component.recordId == recordId && component.itemDef.id == item.id) {
                        return true;
                    }
                }
                return false;
            });

            if (typeof (record.data.isInstructor) !== "undefined") {
                me.canSticky = record.data.isInstructor || Symphony.MessageBoard.IsManager;
                me.isInstructor = record.data.isInstructor;
            } else if (typeof (parentRecord.data.isInstructor) !== "undefined") {
                me.canSticky = parentRecord.data.isInstructor || Symphony.MessageBoard.IsManager;
                me.isInstructor = parentRecord.data.isInstructor;
            }


            if (found.length) {
                me.mainpanel.layout.setActiveItem(found[0]);
                found[0].refresh();
            } else {
                var args = { itemDef: item, recordId: recordId, record: record };
                this.fireEvent('beforeadd', args);

                var itemId = 'panel-' + recordId + '-' + item.id;

                var panel = Ext.create('messageboard.viewpanel', {
                    itemDef: item,
                    recordId: recordId,
                    itemId: itemId,
                    record: record,
                    parentRecord: parentRecord,
                    title: name,
                    canSticky: me.canSticky,
                    isInstructor: me.isInstructor,
                    classData: me.classData ? me.classData : null,
                    className: me.className ? me.className : null,
                    messageBoardType: me.messageBoardType
                });
                me.mainpanel.add(panel);
                me.mainpanel.doLayout();
                panel.show();

                me.mainpanel.layout.setActiveItem(itemId);

                this.fireEvent('add', args);
            }
        },
        editTopic: function (record, isEdit, callback, scope) {
            var w = new Ext.Window({
                width: 600,
                height: isEdit ? 152 : 435,
                title: isEdit ? 'Edit Topic' : 'New Topic',
                layout: 'fit',
                border: false,
                items: [{
                    xtype: 'messageboard.topicpanel',
                    record: record,
                    canSticky: this.canSticky,
                    isInstructor: this.isInstructor,
                    isEdit: isEdit,
                    callback: function (result) {
                        if (typeof callback === 'function') {
                            callback(result, scope);
                        }
                        w.close();
                    }
                }]
            }).show();
        },
        editPost: function (record, isEdit, isReply, callback, postPanel) {
            var title;
            if (isEdit) {
                title = "Edit Post";
            } else if (isReply) {
                title = "New Reply";
            } else {
                title = "New Post";
            }

            var w = new Ext.Window({
                width: 600,
                height: 407,
                modal: true,
                title: title,
                layout: 'fit',
                border: false,
                items: [{
                    xtype: 'messageboard.postpanel',
                    record: record,
                    topic: postPanel.record,
                    isEdit: isEdit,
                    isReply: isReply,
                    canSticky: this.canSticky,
                    isInstructor: this.isInstructor,
                    messageBoardId: this.messageBoardId,
                    callback: function (result) {
                        if (typeof callback === 'function') {
                            callback(result, postPanel);
                        }
                        w.close();
                    }
                }]
            }).show();
        },
        deletePost: function (record, callback, scope) {
            Symphony.Ajax.request({
                method: 'delete',
                url: '/services/messageboard.svc/post/' + record.get('id'),
                success: function (args) {
                    if (typeof callback === 'function') {
                        callback(args, scope);
                    }
                }
            });
        },
        restorePost: function (record, callback, scope) {
            record.data.isDeleted = false;
            Symphony.Ajax.request({
                method: 'post',
                url: '/services/messageboard.svc/post/' + record.get('id'),
                jsonData: record.data,
                success: function (args) {
                    if (typeof callback === 'function') {
                        callback(args, scope);
                    }
                }
            });
        },
        deleteTopic: function (record, callback, scope) {
            Symphony.Ajax.request({
                method: 'delete',
                url: '/services/messageboard.svc/topic/' + record.get('id'),
                success: function (args) {
                    if (typeof callback === 'function') {
                        callback(args, scope);
                    }
                }
            });
        },
        restoreTopic: function (record, callback, scope) {
            record.data.isDeleted = false;
            Symphony.Ajax.request({
                method: 'post',
                url: '/services/messageboard.svc/topic/' + record.get('id'),
                jsonData: record.data,
                success: function (args) {
                    if (typeof callback === 'function') {
                        callback(args, scope);
                    }
                }
            });
        },
        editSettings: function () {
            var me = this;

            var w = new Ext.Window({
                width: 400,
                height: 140,
                modal: true,
                title: 'Edit Message Board',
                layout: 'fit',
                border: false,
                bodyPadding: 10,
                tbar: [{
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'x-button-save',
                    handler: function () {
                        this.up('window').down('[xtype=messageboard.form]').save()
                    }
                }],
                items: [{
                    xtype: 'messageboard.form',
                    messageBoardId: this.messageBoardId,
                    listeners: {
                        saved: function (data) {
                            // title could be in the containing window, or a subpanel
                            var messageBoardPanel = me.down('[xtype=messageboard.viewpanel]'),
                                messageBoardWindow = me.up('[xtype=messageboard.viewwindow]');

                            if (messageBoardWindow) {
                                messageBoardWindow.setTitle(data.name);
                            } else {
                                messageBoardPanel.setTitle(data.name);
                            }

                            this.up('window').close();
                        }
                    }
                }]
            }).show();
        }
    });

})();