﻿(function () {
    Symphony.MessageBoard.PostsGrid = Ext.define('messageboard.postsgrid', {
        alias: 'widget.messageboard.postsgrid',
        extend: 'symphony.simplegrid',
        baseUrl: '/services/messageboard.svc/post/',
        topicUrl: '/services/messageboard.svc/topic/',
        url: '',
        isTopicEditable: null,
        initComponent: function () {
            var me = this;

            me.url = me.baseUrl + me.recordId;
            
            me.isTopicEditable = (Symphony.User.id === me.record.data.userId ||
                                 me.isInstructor ||
                                 Symphony.MessageBoard.IsManager);

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'info',*/
                    resizable: false,
                    width: 80,
                    renderer: function (value, p, record) {
                        var d = record.data;
                        var posts = d.posts + ' ' + (d.posts === 1 ? 'post' : 'posts');
                        var id = Ext.id();
                        
                        setTimeout(function () {
                            me.addStatusControl(id, record);
                        }, 10);

                        return String.format(
                            '<div class="info {4}">' +
                                '<p class="username">{0}</p>' +
                                '<p class="user-img"></p>' +
                                '<p class="jobrole">{1}</p>' +
                                '<p class="posts">{2}</p>' +
                                '<div class="status"><div id="{3}"></div></div>' +
                            '</div>'
                            , d.username, d.jobRole || '', posts, id, d.isDeleted ? 'deleted' : '');
                    }
                }, {
                    /*id: 'content',*/
                    flex: 1,
                    dataIndex: 'content',
                    align: 'left',
                    renderer: function (value, p, record) {
                        var d = record.data;
                        var id = Ext.id();
                        var privateIcon = '<img class="private" src="/images/eye.png" ext:qtip="This message is private and can only be seen by administrators and the creator of this thread."/>';
                        setTimeout(function () {
                            me.addPostControls(me, id, record);
                        }, 10);
                        // If the post is a reply to something, display what the reply is in response too.
                        var replySummary = "";
                        if (d.postId > 0) {
                            var replyContent = record.get('replyContent'),
                                replyUsername = record.get('replyUsername');

                            replyContent = replyContent.length > 700 ? replyContent.substring(0, 700) + "..." : replyContent;

                            replySummary = "In reply to " + replyUsername + "'s post: \"" + replyContent + "\"";
                            
                        }
                        var isHidden = (replySummary.length === 0) ? "hidden" : "";
                        
                        return String.format(
                            '<div class="post {4}">' +
                                '<div class="nurp"></div>' +
                                '<div class="controls"><div id="{2}"></div></div>' +
                                '<p class="date">{0}{3}</p>' +
                                '<p class="reply-summary ' + isHidden + '" id="reply' + d.id + '">{5}</p>' +
                                '<p class="content">{1}</p>' +
                            '</div>'
                        , Symphony.longDateRenderer(d.createdOn), d.content, id, d.isPrivate ? privateIcon : '', d.isDeleted ? 'deleted' : '', replySummary);
                    }
                }]
            });

            Ext.apply(this, {
                disableSelection: true,
                trackMouseOver: false,
                idProperty: 'id',
                border: false,
                colModel: colModel,
                url: me.url,
                cls: 'posts',
                ref: 'forum',
                model: 'messageBoardPost',
                bubbleEvents: ['loadForum', 'editPost', 'editTopic', 'saveStudentStatus', 'deletePost', 'restorePost'],
                tbar: {
                    items: [{
                        xtype: 'button',
                        iconCls: 'x-button-previous',
                        text: 'Back to ' + me.parentRecord.data.name,
                        listeners: {
                            click: function (btn, e) {
                                me.fireEvent('loadForum', me.parentRecord);
                            }
                        }
                    },
                    '->',
                    {
                        xtype: 'button',
                        iconCls: 'x-button-pencil',
                        text: 'Edit Topic',
                        hidden: !me.isTopicEditable,
                        listeners: {
                            click: function (btn, e) {
                                me.fireEvent('editTopic', me.record, true, me.refreshTopic, me);
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        name: 'lockBtn',
                        hidden: !me.isTopicEditable,
                        listeners: {
                            click: function (btn, e) {
                                var data = me.record.data;
                                data.isLocked = !me.record.data.isLocked;

                                Symphony.Ajax.request({
                                    url: me.topicUrl + me.record.data.id,
                                    jsonData: data,
                                    method: 'POST',
                                    success: function (result) {
                                        me.refreshTopic(result, me);
                                    }
                                });
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        name: 'stickyBtn',
                        hidden: !me.canSticky,
                        listeners: {
                            click: function (btn, e) {
                                var data = me.record.data;
                                data.isSticky = !me.record.data.isSticky;

                                Symphony.Ajax.request({
                                    url: me.topicUrl + me.record.data.id,
                                    jsonData: data,
                                    method: 'POST',
                                    success: function (result) {
                                        me.refreshTopic(result, me);
                                    }
                                });
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        iconCls: 'x-button-new-message',
                        text: 'New Reply',
                        name: 'newReplyBtn',
                        cls: 'x-button-new-message',
                        listeners: {
                            click: function (btn, e) {
                                me.fireEvent('editPost', me.record, false, false, me.refreshPosts, me);
                            }
                        }
                    }]

                },
                listeners: {
                    /*viewready: function (grid) {
                        setTimeout(function () {
                            grid.removeListener('afterrender', this);
                            grid.fireEvent('resize', grid, grid.getSize().width);
                        }, 100);
                    },*/
                    resize: function (grid, adjWidth, adjHeight, rawWidth, rawHeight) {
                        console.warn("Resize listener on post grid removed. Posts may not fit in grid properly.");

                        /*var element = grid.getGridEl();

                        var tables = element.select('.post-row > table');
                        var originalPosts = element.select('.post-row.orig');
                        var originalPostContent = element.select('.post-row.orig table td.x-grid3-td-content');
                        var replies = element.select('.post-row.reply');
                        var replyContent = element.select('.post-row.reply table td.x-grid3-td-content');

                        var width = adjWidth * 0.97;
                        var mainPostWidth = width - 97;
                        var replyWidth = mainPostWidth - 80;

                        tables.setWidth('100%');
                        originalPosts.setWidth(width);
                        originalPostContent.setWidth(mainPostWidth);
                        replies.setWidth(mainPostWidth);
                        replyContent.setWidth(replyWidth);*/
                    },
                    added: function () {
                        var store = me.getStore();
                        store.addListener('load', function() {
                            me.fireEvent('resize', me, me.getSize().width);
                        });
                        me.refreshTopic(me.record, me);
                    }
                }
            });


            this.callParent(arguments);

            console.warn("Having to add getRowClass on grid after creation instead of in view config. Look into this.");

            this.getView().getRowClass = function (record, rowIndex, rp, ds) {
                var cls = 'post-row';
                if (record.data.postId) {
                    cls += ' reply';
                } else {
                    cls += ' orig';
                }
                return cls;
            }
        },
        setTopicId: function (id) {
            this.topicId = id;
            this.updatePosts();
        },
        updatePosts: function () {
            var store = this.getStore();
            this.url = this.baseUrl + this.topicId;
            store.proxy.url = this.url;
            store.load();
        },
        addStatusControl: function (id, record) {
            var me = this;
            if (me.classData) {
                for (var i = 0; i < me.classData.length; i++) {
                    if (me.classData[i].registrantId === record.data.userId ||
                        me.classData[i].userId === record.data.userId) {

                        if (!me.classData[i].registrationStatusId || me.classData[i].registrationStatusId === Symphony.RegistrationStatusType.registered)

                        var menuTextTemplate = 'This student {0} move forward from {1}.';
                        
                        var statusButton = new Ext.Button({
                            xtype: 'button',
                            text: 'Status',
                            width: 74,
                            tooltip: String.format(menuTextTemplate, (me.classData[i].canMoveForwardIndicator ? 'can' : 'cannot')),
                            menu: {
                                items: [{
                                    text: String.format(menuTextTemplate, 'cannot', me.className),
                                    checked: !me.classData[i].canMoveForwardIndicator,
                                    group: 'canMoveForwardIndicator',
                                    checkHandler: function (item, checked) {
                                        if (checked) {
                                            me.classData[i].canMoveForwardIndicator = false;
                                            me.saveStudentStatus()
                                            statusButton.setTooltip(String.format(menuTextTemplate, (me.classData[i].canMoveForwardIndicator ? 'can' : 'cannot'), me.className));
                                        }
                                    }
                                }, {
                                    text: String.format(menuTextTemplate, 'can', me.className),
                                    checked: me.classData[i].canMoveForwardIndicator,
                                    group: 'canMoveForwardIndicator',
                                    checkHandler: function (item, checked) {
                                        if (checked) {
                                            me.classData[i].canMoveForwardIndicator = true;
                                            me.saveStudentStatus();
                                            statusButton.setTooltip(String.format(menuTextTemplate, (me.classData[i].canMoveForwardIndicator ? 'can' : 'cannot'), me.className));
                                        }
                                    }
                                }]
                            }
                        }).render(document.body, id);

                        break;
                    }
                }
            }
        },
        addPostControls: function (me, id, record) {

            if (record.data.isDeleted) {
                if (Symphony.MessageBoard.IsManager) {
                    new Ext.Button({
                        xtype: 'button',
                        text: '',
                        iconCls: 'x-button-restore-message',
                        width: 24,
                        tooltip: 'Restore Message',
                        handler: function () {
                            Ext.MessageBox.confirm('Restore this message?', 'Do you really want to restore this message? Posts that this message is a reply to will be restored as well.', function (btn) {
                                if (btn == 'yes') {
                                    me.fireEvent('restorePost', record, me.refreshPosts, me);
                                }
                            });
                        }
                    }).render(document.body, id);
                }
            } else {
                if (Symphony.MessageBoard.IsManager ||
                    me.IsInstructor) {

                    new Ext.Button({
                        xtype: 'button',
                        text: '',
                        iconCls: 'x-button-delete-message',
                        width: 24,
                        tooltip: 'Delete Message',
                        handler: function () {
                            Ext.MessageBox.confirm('Delete this message?', 'Do you really want to delete this message? Replies to this message will be deleted as well.', function (btn) {
                                if (btn == 'yes') {
                                    me.fireEvent('deletePost', record, me.refreshPosts, me);
                                }
                            });
                        }
                    }).render(document.body, id);
                }

                if (record.data.userId === Symphony.User.id ||
                    Symphony.MessageBoard.IsManager ||
                    me.IsInstructor) {

                    new Ext.Button({
                        xtype: 'button',
                        text: '',
                        iconCls: 'x-button-edit-message',
                        width: 24,
                        tooltip: 'Edit Message',
                        handler: function () {
                            me.fireEvent('editPost', record, true, false, me.refreshPosts, me);
                        }
                    }).render(document.body, id)

                }

                new Ext.Button({
                    xtype: 'button',
                    text: '',
                    iconCls: 'x-button-new-message',
                    width: 24,
                    tooltip: 'Reply to this Message',
                    handler: function () {
                        me.fireEvent('editPost', record, false, true, me.refreshPosts, me);
                    }
                }).render(document.body, id);
            }
        },
        refreshPosts: function (result, me) {
            var store = me.getStore();

            var refreshListener = function () {
                var newRecordIndex = store.find('id', result.data.id);
                var gridEl = me.getGridEl();
                var rowEl = me.getView().getRow(newRecordIndex);

                rowEl.scrollIntoView(gridEl, false);
                Ext.fly(rowEl).highlight();

                //Gotta highlight that nurp too or it looks funny
                var nurp = new Ext.Element(rowEl).select('.nurp').elements[0];
                Ext.fly(nurp).highlight(false, {
                    attr: 'border-right-color'
                });

                store.removeListener('load', refreshListener);
            }

            store.addListener('load', refreshListener);
            me.refresh();
        },
        refreshTopic: function (result, me) {
            var isLocked = result.data.isLocked;
            var isSticky = result.data.isSticky;
            var title = result.data.title;
            var toolbar = me.getTopToolbar();

            var stickyBtn = toolbar.find('name', 'stickyBtn')[0];
            var lockBtn = toolbar.find('name', 'lockBtn')[0];
            var newReplyBtn = toolbar.find('name', 'newReplyBtn')[0];

            stickyBtn.setText(isSticky ? 'Un-Sticky' : 'Sticky');
            lockBtn.setText(isLocked ? 'Unlock Thread' : 'Lock Thread');

            stickyBtn.setIconCls(isSticky ? 'x-button-un-sticky' : 'x-button-sticky');
            lockBtn.setIconCls(isLocked ? 'x-button-unlock' : 'x-button-close-thread');

            if (isLocked) {
                me.addCls('locked')
                if (newReplyBtn) {
                    newReplyBtn.hide();
                };
            } else {
                me.removeCls('locked');
                if (newReplyBtn) {
                    newReplyBtn.show();
                }
            }
        },
        saveStudentStatus: function () {
            var me = this;
            if (me.classData && me.classData.length > 0) {
                if (me.messageBoardType == Symphony.MessageBoardType.classroom) {
                    var classId = me.classData[0].classId;
                    Symphony.Ajax.request({
                        url: '/services/classroom.svc/classes/' + classId + '/registrations',
                        type: 'post',
                        jsonData: { registrationList: me.classData },
                        success: function () {
                            me.refreshGrids(classId);
                        }
                    });
                } else if (me.messageBoardType == Symphony.MessageBoardType.online) {
                    var trainingProgramId = me.classData[0].trainingProgramId;
                    var courseId = me.classData[0].courseId;

                    Symphony.Ajax.request({
                        url: '/services/courseassignment.svc/trainingprograms/' + trainingProgramId + '/onlinecourseregistrations/' + courseId + '/',
                        type: 'post',
                        jsonData: me.classData,
                        success: function () {
                            me.refreshGrids(-1);
                        }
                    });
                }
            }
        },
        refreshGrids: function (classId) {
            var main = Ext.getCmp("symphonymain");
            var instructorApp = main.find('xtype', 'instructors.app');
            var me = this;
            if (instructorApp.length > 0) {
                var classForms = instructorApp[0].find('xtype', 'instructors.classform');
                for (var i = 0; i < classForms.length; i++) {
                    var onlineStudentGrids = classForms[i].find('xtype', 'instructors.onlinecoursestudentgrid');
                    var attendanceGrids = classForms[i].find('xtype', 'classroom.attendancegrid');

                    for (var x = 0; x < onlineStudentGrids.length; x++) {
                        onlineStudentGrids[x].refresh();
                    }

                    for (var y = 0; y < attendanceGrids.length; y++) {
                        if (attendanceGrids[y].classId == classId) {
                            attendanceGrids[y].setData(me.classData);
                        }
                    }
                }
            }
        }
    });

})();
