﻿/**
 * AffidavitTab is a panel that wraps a grid of affidavits and a sub tab panel
 * that displays affidavit forms.
 */
Ext.define('Affidavits.AffidavitTab', {
    extend: 'Ext.Panel',
    xtype: 'affidavits.affidavittab',
    requires: [
        'AffidavitFinalExam',
        'Affidavits.AffidavitGrid',
        'Ext.tab.Panel'
    ],
    uses: 'Affidavits.AffidavitPanel',
    mixins: ['Symphony.mixins.FunctionOwnership'],

    layout: 'border',

    items: [{
        xtype: 'affidavits.affidavitgrid',
        itemId: 'grid',
        region: 'west',
        title: 'Select an affidavit',

        border: false,
        split: true,
        width: 290,

        listeners: {
            addgriditem: function(model) {
                var me = this,
                    root = me.up('[xtype=affidavits.affidavittab]');

                root.addPanel(model);
            },

            opengriditem: function(model) {
                var me = this,
                    root = me.up('[xtype=affidavits.affidavittab]');

                root.addPanel(model);
            }
        }
    }, {
        xtype: 'tabpanel',
        itemId: 'tab-panel',
        region: 'center',
        border: false
    }],

    initComponent: function() {
        var me = this;

        me.callParent();

        me.on('activate', function() {
            var grid = me.queryById('grid');

            Log.debug('Affidavit tab activated, refreshing grid.');
            grid.refresh();
        });
    },

    addPanel: function(affidavit) {
        var me = this,
            grid = me.queryById('grid'),
            tabPanel = me.queryById('tab-panel'),
            editor;

        Log.info('Opening a new affidavit editor.');

        editor = tabPanel.items.findBy(function(el, key) {
            if (el.model && !el.model.phantom) {
                return el.model.getId() == affidavit.getId();
            }

            return false;
        });

        if (!editor) {
            Log.debug('Editor is not open, opening an editor for the model.', affidavit);

            editor = tabPanel.add(new Affidavits.AffidavitPanel({
                model: affidavit,
                closable: true
            }));
            editor.on('change', function() {
                grid.refresh();
            });
        } else {
            Log.debug('Editor is already open for the model, switching focus.', editor);
        }

        tabPanel.activate(editor);
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});