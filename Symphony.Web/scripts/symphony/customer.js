(function () {
    var customerTabs = [{
        title: 'Users',
        layout: 'fit',
        tabTip: 'Create and manage users.',
        iconCls: 'x-user',
        xtype: 'customer.usersgrid',
        name: 'customer.userstab',
        listeners: {
            activate: {
                fn: function (grid) {
                    grid.refresh();
                },
                single: true
            }
        }
    }, {
        title: Symphony.Aliases.jobRoles,
        layout: 'fit',
        tabTip: 'Create and manage {0}'.format(Symphony.Aliases.jobRoles),
        xtype: 'customer.hierarchytab',
        iconCls: 'x-jobrole',
        hierarchyName: 'jobRole',
        editxtype: 'customer.jobroleform',
        name: 'customer.jobrolestab'
    }, {
        title: Symphony.Aliases.locations,
        layout: 'fit',
        tabTip: 'Create and manage {0}'.format(Symphony.Aliases.locations),
        xtype: 'customer.hierarchytab',
        iconCls: 'x-location',
        hierarchyName: 'location',
        editxtype: 'customer.locationform',
        name: 'customer.locationstab'
    }, {
        title: Symphony.Aliases.audiences,
        layout: 'fit',
        tabTip: 'Create and manage {0}'.format(Symphony.Aliases.audiences),
        xtype: 'customer.hierarchytab',
        iconCls: 'x-audience',
        hierarchyName: 'audience',
        editxtype: 'customer.audienceform',
        name: 'customer.audiencestab'
    }, {
        title: 'Import',
        tabTip: 'Import your data',
        xtype: 'customer.importpanel',
        name: 'customer.importstab'
    }, {
        title: 'Export',
        tabTip: 'Export your data',
        xtype: 'customer.exportpanel',
        name: 'customer.exportstab'
    }];

    Symphony.Customer.enableEditing = Symphony.User.isCustomerAdministrator;

    Symphony.Customer.deleteGTMOrganizerRenderer = function (value, meta, record) {
        return '<a href="#" onclick="Symphony.Customer.deleteGTMOrganizer({0});"><img src="/images/bin.png" /></a>'.format(record.get('id'));
    };

    Symphony.Customer.deleteGTMOrganizer = function (gtmOrganizerId) {
        Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this GTM organizer?', function (btn) {
            if (btn == 'yes') {
                Symphony.Ajax.request({
                    url: '/services/customer.svc/gtmorganizers/delete/' + gtmOrganizerId,
                    success: function () {
                        Ext.getCmp('gtmorganizersgrid').refresh();
                    }
                });
            }
        });
    };

    if (Symphony.User.isSalesChannelAdmin) {
        Symphony.Customer.App = Ext.define('customer.app', {
            alias: 'widget.customer.app',
            extend: Symphony.getQueryParam('tabs') == 'false' ? 'Ext.Panel' : 'Ext.TabPanel',
            layout: 'card',
            select: function (xtype) {
                var tabs = this.find('xtype', xtype);
                if (tabs.length > 0) {
                    this.setActiveTab(tabs[0]);
                    return true;
                }
                return false;
            },
            initComponent: function () {
                Ext.apply(this, {
                    requiresActivation: true,
                    border: false,
                    defaults: {
                        border: false
                    },
                    items: [{
                        title: 'Sales Channel Administration',
                        xtype: 'customer.saleschannelpanel',
                        name: 'customer.saleschanneltab'
                    }, {
                        title: 'User Administration',
                        xtype: 'customer.useradministrationtab',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedAdministration),
                        hidden: false //!Symphony.Modules.hasModule(Symphony.Modules.AdvancedAdministration)
                    }, {
                        title: 'Customer Administration',
                        xtype: 'customer.customeradministrationtab',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedAdministration),
                        hidden: false// !Symphony.Modules.hasModule(Symphony.Modules.AdvancedAdministration)
                    }, {
                        title: 'Networks',
                        xtype: 'network.networktab',
                        name: 'customer.networktab'
                    }, {
                        title: 'Application Administration',
                        xtype: 'customer.applicationtiletabs',
                        name: 'customer.applicationtab'
                    }, {
                        title: 'Service Providers',
                        xtype: 'serviceprovider.serviceprovidertab',
                        name: 'customer.serviceprovidertab'
                    }, {
                        title: 'Themes',
                        tabTip: 'Define themes to style a customer\'s portal',
                        xtype: 'customer.themetab'
                    }, {
                        title: 'Accreditation Boards',
                        tabTip: 'Define organizations that accredit courses',
                        xtype: 'artisan.accreditationboardtab'
                    }, {
                        title: 'Affidavits',
                        xtype: 'affidavits.affidavittab',
                        name: 'customer.affidavittab',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedAdministration),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.AdvancedAdministration)
                    }, {
                        title: 'Proctor Forms',
                        xtype: 'proctors.proctorformtab',
                        name: 'customer.proctorformtab',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedAdministration),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.AdvancedAdministration)
                    }, {
                        title: 'Certificate Templates',
                        xtype: 'certificates.certificatetemplatetab',
                        name: 'customer.certificatetemplatetab',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedAdministration),
                        hidden: !Symphony.Modules.hasModule(Symphony.Modules.AdvancedAdministration)
                    }]
                });
                this.callParent(arguments);
            }
        });
    } else {
        Symphony.Customer.App = Ext.define('customer.app', {
            extend: 'Ext.tab.Panel',
            alias: 'widget.customer.app',
            select: function (xtype) {
                var tabs = this.find('xtype', xtype);
                if (tabs.length > 0) {
                    this.setActiveTab(tabs[0]);
                    return true;
                }
                return false;
            },
            initComponent: function () {
                Ext.apply(this, {
                    requiresActivation: true,
                    border: false,
                    defaults: {
                        border: false
                    },
                    items: customerTabs
                });
                this.callParent(arguments);
            }
        });
    }

    Symphony.Customer.SalesChannelPanel = Ext.define('customer.saleschannelpanel', {
        alias: 'widget.customer.saleschannelpanel',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                items: [{
                    region: 'west',
                    split: true,
                    collapsible: true,
                    border: false,
                    width: 300,
                    layout: 'border',
                    defaults: {
                        border: false
                    },
                    items: [{
                        region: 'north',
                        height: 27,
                        xtype: 'toolbar',
                        items: [{
                            text: 'Manage Collaboration',
                            iconCls: 'x-button-meeting',
                            handler: function () {
                                var w = new Ext.Window({
                                    title: 'Manage Collaboration',
                                    height: 350,
                                    width: 400,
                                    layout: 'fit',
                                    modal: true,
                                    items: [{
                                        layout: 'fit',
                                        border: false,
                                        xtype: 'customer.gtmorganizersgrid',
                                        id: 'gtmorganizersgrid',
                                        listeners: {
                                            open: function (gtmOrganizerId) {
                                                var w2 = new Ext.Window({
                                                    title: (gtmOrganizerId ? 'Edit' : 'Add') + ' GTM Organizer',
                                                    height: 225,
                                                    width: 400,
                                                    layout: 'fit',
                                                    modal: true,
                                                    items: [{
                                                        gtmOrganizerId: gtmOrganizerId,
                                                        xtype: 'customer.gtmorganizerform',
                                                        border: false,
                                                        listeners: {
                                                            cancel: function () {
                                                                w2.destroy();
                                                            },
                                                            save: function (data) {
                                                                Ext.getCmp('gtmorganizersgrid').refresh();
                                                                w2.destroy();
                                                            }
                                                        }
                                                    }]
                                                });
                                                w2.show();
                                            }
                                        }
                                    }],
                                    listeners: {
                                        // delayed load
                                        activate: {
                                            fn: function (panel) {
                                                panel.find('xtype', 'customer.gtmorganizersgrid')[0].refresh();
                                            },
                                            single: true
                                        }
                                    }
                                });
                                w.show();
                            }
                        }]
                    }, {
                        region: 'center',
                        layout: 'fit',
                        id: 'customer.saleschanneltree',
                        xtype: 'customer.saleschanneltree',
                        //enableDD: Symphony.Customer.enableEditing,
                        listeners: {
                            saleschannelclick: function (parentId, salesChannelId, salesChannelName) {
                                var container = me.find('name', 'center-content')[0];
                                var panel = container.queryBy(function (subPanel) {
                                    return subPanel.salesChannelId == salesChannelId;
                                })[0];
                                if (!panel) {
                                    panel = new Symphony.Customer.SalesChannelEditor({
                                        closable: true,
                                        title: salesChannelName,
                                        salesChannelId: salesChannelId,
                                        salesChannelName: salesChannelName,
                                        parentId: parentId
                                    });

                                    container.add(panel);
                                    container.doLayout();
                                }
                                container.layout.setActiveItem(panel);
                            },
                            rename: function (id, name) {
                                var container = me.find('name', 'center-content')[0];
                                var panel = container.findBy(function (subPanel) {
                                    return subPanel.salesChannelId == id;
                                })[0];
                                if (panel) {
                                    panel.setTitle(name);
                                }
                            },
                            dragdrop: function (panel, node, dd, e) {
                                // when the item is dropped, save its new parent
                                var parentId = node.parentNode.attributes.value;
                                var id = node.attributes.value;
                                var name = node.attributes.text;
                                var tree = me.find('xtype', 'customer.saleschanneltree')[0];
                                Symphony.Ajax.request({
                                    url: '/services/customer.svc/saleschannels/' + id + '?reparent=true',
                                    jsonData: { id: id, name: name, parentId: parentId },
                                    success: function (result) {
                                        // refresh to ensure we have the latest data
                                        tree.refresh();
                                    },
                                    failure: function (result) {
                                        if (result.status != 200) {
                                            Ext.Msg.alert('Error', result.statusText);
                                        } else {
                                            Ext.Msg.alert('Error', result.error);
                                        }
                                        // refresh to make everything go back the way it was
                                        tree.refresh();
                                    }
                                });
                            }
                        }
                    }]
                }, {
                    region: 'center',
                    layout: 'fit',
                    border: false,
                    items: [{
                        /* nested item per ExtJS docs for border layouts */
                        xtype: 'tabpanel',
                        //layout: 'fit',
                        border: false,
                        name: 'center-content'
                    }]
                }],
                listeners: {
                    activate: {
                        fn: function (panel) {
                            panel.query('[xtype=customer.saleschanneltree]')[0].store.load();
                        },
                        single: true
                    }
                }
            });
            this.callParent(arguments);
        }
    });


    Symphony.Customer.GTMOrganizersGrid = Ext.define('customer.gtmorganizersgrid', {
        alias: 'widget.customer.gtmorganizersgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this;
            var url = '/services/customer.svc/gtmorganizers';


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { header: ' ', renderer: Symphony.Customer.deleteGTMOrganizerRenderer, width: 28, align: 'center' },
                    {
                        /*id: 'email',*/ header: 'Email', dataIndex: 'email',
                        flex: 1
                    },
                    { /*id: 'organizerKey',*/ header: 'Organizer Key', dataIndex: 'organizerKey', width: 130 }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('open', 0);
                        }
                    }]
                },
                pageSize: 10,
                idProperty: 'id',
                deferLoad: true,
                url: url,
                colModel: colModel,
                model: 'gtmOrganizer',
                listeners: {
                    itemdblclick: function (grid, record, item, rowIndex, e) {
                        me.fireEvent('open', record.get('id'));
                    }
                }
            });
            this.callParent(arguments);
        }
    });


    Symphony.Customer.GTMOrganizerForm = Ext.define('customer.gtmorganizerform', {
        alias: 'widget.customer.gtmorganizerform',
        extend: 'Ext.Panel',
        gtmOrganizerId: 0,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 27,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        items: [],
                        listeners: {
                            save: function () {
                                var frm = me.find('xtype', 'form')[0];
                                if (!frm.isValid()) { return false; }

                                var gtmOrganizer = frm.getValues();

                                Symphony.Ajax.request({
                                    url: '/services/customer.svc/gtmorganizers/' + me.gtmOrganizerId,
                                    jsonData: gtmOrganizer,
                                    success: function (result) {
                                        me.fireEvent('save', result.data);
                                    }
                                });
                            },
                            cancel: function () {
                                me.fireEvent('cancel');
                            }
                        }
                    }]
                }, {
                    region: 'center',
                    layout: 'fit',
                    defaults: {
                        border: false
                    },
                    items: [{
                        border: false,
                        bodyStyle: 'padding: 15px',
                        items: [{
                            name: 'general',
                            xtype: 'form',
                            border: false,
                            frame: true,
                            items: [{
                                xtype: 'fieldset',
                                title: 'Basic Information',
                                defaults: {
                                    border: false,
                                    xtype: 'panel',
                                    layout: 'form'
                                },
                                items: [{
                                    defaults: { anchor: '100%', xtype: 'textfield' },
                                    items: [{
                                        name: 'organizerKey',
                                        fieldLabel: 'Organizer Key',
                                        allowBlank: false,
                                        xtype: 'numberfield',
                                        allowDecimals: false,
                                        allowNegative: false
                                    }, {
                                        name: 'email',
                                        fieldLabel: 'Email',
                                        allowBlank: false
                                    }, {
                                        name: 'password',
                                        fieldLabel: 'Password',
                                        allowBlank: false
                                    }]
                                }]
                            }]
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        onRender: function () {
            Symphony.Customer.GTMOrganizerForm.superclass.onRender.apply(this, arguments);
            this.load();
        },
        load: function () {
            var me = this;
            if (this.gtmOrganizer) {
                me.find('name', 'general')[0].bindValues(this.gtmOrganizer);
            } else if (this.gtmOrganizerId) {
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/customer.svc/gtmorganizers/' + this.gtmOrganizerId,
                    success: function (result) {
                        // load up the details
                        me.find('name', 'general')[0].bindValues(result.data);
                    }
                });
            }
        }
    });


    Symphony.Customer.SalesChannelTree = Ext.define('customer.saleschanneltree', {
        alias: 'widget.customer.saleschanneltree',
        extend: 'symphony.tabletree',
        editable: true,
        deferLoad: true,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                deferLoad: me.deferLoad,
                columns: [
                    {
                        xtype: 'treecolumn',
                        text: 'Sales Channels',
                        dataIndex: 'name',
                        flex: 1
                    }
                ],
                tbar: me.editable ? {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            var node = me.getSelectionModel().getSelection()[0];
                            var parentId = node ? node.attributes.value : 0;

                            var saleschannelAttributeWindow = new Symphony.Customer.SalesChannelAttributeEditor({
                                salesChannelId: 0, parentSalesChannelId: parentId,
                                listeners: {
                                    save: function (id, newname) {
                                        me.renameSalesChannel(id, newname);
                                        this.close();
                                    },
                                    cancel: function () {
                                        this.close();
                                    }
                                }
                            });

                            saleschannelAttributeWindow.show();
                        }
                    }, {
                        text: 'Edit',
                        disabled: true,
                        name: 'rename',
                        iconCls: 'x-button-edit',
                        handler: function () {
                            var node = me.getSelectionModel().getSelection()[0];

                            var id = node.get('id');
                            var parentId = node.get('parentId') == 'root' ? 0 : node.get('parentId');
                            
                            var saleschannelAttributeWindow = new Symphony.Customer.SalesChannelAttributeEditor({
                                salesChannelId: id, parentSalesChannelId: parentId,
                                listeners: {
                                    save: function (id, newname) {
                                        me.renameSalesChannel(id, newname);
                                        this.close();
                                    },
                                    cancel: function () {
                                        this.close();
                                    }
                                }
                            });

                            saleschannelAttributeWindow.show();
                        }
                    }]
                } : null,
                url: '/services/customer.svc/saleschannels/',
                model: 'salesChannel',
                performSearch: function (text) {
                    Ext.each(this.hiddenNodes, function (n) {
                        n.ui.show();
                    });

                    if (!text) {
                        this.clearFilter();
                        return;
                    }
                    this.expandAll();

                    var channels = [];
                    var me = this;

                    Ext.Msg.wait("We are fetching your search results", "Please wait...");

                    // We want to filter based on customer data, since that is more meaningful
                    // So go and fetch the customer data here, then filter it based on the passed search text
                    Symphony.Ajax.request({
                        url: '/services/customer.svc/customers/?searchText=' + text, // Start with only the customer data that actually matches our search!
                        method: 'GET',
                        success: function (result) {
                            var customers = result.data;
                            var salesChannelSearchTerms = {} // to prevent dups
                            var salesChannelSearchTermArray = [];

                            // Just get the subdomains that match the customers while filtering for duplicates
                            for (var i = 0; i < customers.length; i++) {
                                if (!salesChannelSearchTerms[customers[i].salesChannelName]) {
                                    salesChannelSearchTerms[customers[i].salesChannelName] = true;
                                    salesChannelSearchTermArray.push(customers[i].salesChannelName);
                                }
                            }

                            // Add on the original search term as a subdomain so we can search those too
                            if (!salesChannelSearchTerms[text]) {
                                salesChannelSearchTermArray.push(text);
                            }

                            // and filter!
                            me.filterByArray(salesChannelSearchTermArray, "name");
                            
                            Ext.Msg.hide();
                            me.fireEvent('searchComplete');
                        }
                    });
                },
                renameSalesChannel: function (id, newname) {
                    me.fireEvent('rename', id, newname);
                    me.refresh();
                }
            });

            Ext.apply(this.listeners, {
                itemclick: function (tree, record, index, e, eOpts) {
                    var parent = record.get('parentId');
                    parent = parent == 'root' ? 0 : parent;
                    me.fireEvent('saleschannelclick', parent, record.get('id'), record.get('name'));
                    if (me.editable) {
                        me.getDockedItems('toolbar[dock="top"]')[0].find('name', 'rename')[0].enable();
                    }
                }
            });

            this.callParent(arguments);
        }
    });

    Symphony.Customer.SalesChannelAttributeEditor = Ext.define('symphony.customer.saleschannelattributeeditor', {
        alias: 'widget.symphony.customer.saleschannelattributeeditor',
        extend: 'Ext.Window',
        salesChannelId: 0,
        parentSalesChannelId: 0,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                tbar: {
                    xtype: 'symphony.savecancelbar',
                    border: false,
                    listeners: {
                        save: function () {
                            var newsaleschannelname = me.find('name', 'name')[0].getValue();
                            var newsaleschannelcode = me.find('name', 'code')[0].getValue();
                            var newisloginenabled = me.find('name', 'isLoginEnabled')[0].getValue();
                            var logo = me.find('name', 'logo')[0].getValue();
                            var description = me.find('name', 'description')[0].getValue();
                            var themeId = me.find('name', 'themeId')[0].getValue() || 0;

                            Symphony.Ajax.request({
                                url: '/services/customer.svc/saleschannels/' + me.salesChannelId,
                                jsonData: {
                                    id: me.salesChannelId, parentId: me.parentSalesChannelId, name: newsaleschannelname,
                                    code: newsaleschannelcode, isLoginEnabled: newisloginenabled,
                                    themeId: themeId || 0,
                                    logo: logo,
                                    description: description
                                },
                                success: function (result) {
                                    me.fireEvent('save', me.salesChannelId, newsaleschannelname);
                                }
                            })
                        },
                        cancel: function () { me.fireEvent('cancel'); }
                    }
                },
                items: [{
                    xtype: 'form',
                    name: 'saleschannelform',
                    border: true,
                    frame: true,
                    bodyStyle: 'padding: 5px',
                    defaults: { anchor: '100%', xtype: 'textfield' },
                    items: [{
                        name: 'name',
                        fieldLabel: 'Name',
                        allowBlank: false
                    }, {
                        name: 'code',
                        fieldLabel: 'Code',
                        allowBlank: true
                    }, {
                        fieldLabel: 'Logo',
                        xtype: 'associatedimagefield',
                        name: 'logo',
                        noPreviewResize: true
                    }, {
                        xtype: 'combobox',
                        name: 'themeId',
                        fieldLabel: 'Theme',
                        displayField: 'name',
                        valueField: 'id',
                        lastQuery: '',
                        forceSelection: true,
                        queryParam: 'searchText',
                        pageSize: 10,
                        emptyText: 'Select a theme',
                        store: {
                            autoLoad: true,
                            type: 'themes',
                            listeners: {
                                load: function () {
                                    this.insert(0, new Theme({
                                        id: null,
                                        name: 'None'
                                    }));
                                }
                            }
                        }
                    }, {
                        name: 'description',
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        help: {
                            title: 'Description',
                            text: 'Company description, used for generic details about the company.'
                        },
                        allowBlank: true
                    }, {
                        xtype: 'checkbox',
                        name: 'isLoginEnabled',
                        fieldLabel: 'Is Login Enabled',
                        allowBlank: true
                    }]
                }],
                modal: true,
                resizable: false,
                title: 'Sales Channel',
                width: 400,
                layout: 'fit',
                listeners: {
                    render: function () {
                        if (me.salesChannelId == 0) return;
                        Symphony.Ajax.request({
                            method: 'GET',
                            url: '/services/customer.svc/saleschannel/' + me.salesChannelId,
                            success: function (result) {
                                me.find('name', 'saleschannelform')[0].bindValues(result.data);
                            }
                        });
                    }
                }
            });
            this.callParent(arguments);
        }
    });


    Symphony.Customer.SalesChannelEditor = Ext.define('customer.saleschanneleditor', {
        alias: 'widget.customer.saleschanneleditor',
        extend: 'Ext.Panel',
        salesChannelId: 0,
        parentId: 0,
        salesChannelName: '',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                border: false,
                layout: 'fit',
                items: [{
                    xtype: 'tabpanel',
                    activeItem: 0,
                    flex: 1,
                    border: false,
                    items: [{
                        title: 'Companies',
                        //id: 'customer.customersgrid',
                        xtype: 'customer.customersgrid',
                        layout: 'fit',
                        salesChannelId: this.salesChannelId,
                        listeners: {
                            open: function (customerId, salesChannelId, customerName) {
                                var grid = this;
                                var tab = me.find('xtype', 'tabpanel')[0];
                                var panel = tab.findBy(function (p) {
                                    return p.customerId == customerId && p.editor == true;
                                })[0];

                                if (!panel) {
                                    panel = new Symphony.Customer.CustomerEditor({
                                        editor: true,
                                        closable: true,
                                        title: customerName,
                                        salesChannelId: salesChannelId,
                                        customerId: customerId,
                                        customerName: customerName,
                                        listeners: {
                                            edit: function () {
                                                grid.refresh();
                                            }
                                        }
                                    });
                                    tab.add(panel);
                                    tab.doLayout();
                                }
                                tab.activate(panel);
                            }
                        }
                    }, {
                        title: 'Administrators',
                        //id: 'customer.administratorsgrid',
                        xtype: 'customer.administratorsgrid',
                        salesChannelId: me.salesChannelId,
                        layout: 'fit'
                    }]
                }]
            });
            this.callParent(arguments);
        }
    });


    Symphony.Customer.CustomersGrid = Ext.define('customer.customersgrid', {
        alias: 'widget.customer.customersgrid',
        extend: 'symphony.searchablegrid',
        salesChannelId: 0,
        initComponent: function () {
            var me = this;
            var url = '/services/customer.svc/customers/saleschannel/' + this.salesChannelId;


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    width: 90,
                    renderer: Symphony.checkRenderer
                },
                columns: [
                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name', align: 'left', renderer: Symphony.Portal.valueRenderer,
                        flex: 1
                    },
                    { header: 'Sub-Domain', dataIndex: 'subdomain', renderer: Symphony.Portal.valueRenderer },
                    { header: 'User Limit', dataIndex: 'userLimit', width: 70, renderer: Symphony.Portal.valueRenderer },
                    { header: 'Self-Registration', dataIndex: 'selfRegistrationIndicator' },
                    { header: 'Evaluation', dataIndex: 'evaluationIndicator', width: 80 },
                    {
                        header: 'Eval. End Date',
                        dataIndex: 'evaluationEndDate',
                        renderer: function (value, meta, record) {
                            var dt = Symphony.parseDate(value);
                            if (dt && dt.getFullYear() != Symphony.defaultYear) {
                                return Symphony.dateRenderer(value);
                            }
                            return '';
                        }
                    },
                    { header: 'Suspended', dataIndex: 'suspendedIndicator', width: 80 },
                    { header: 'Quick Query', dataIndex: 'quickQueryIndicator', width: 80 },
                    { header: 'Report Builder', dataIndex: 'reportBuilderIndicator', width: 80 }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Add Company',
                        iconCls: 'x-button-add',
                        handler: function () {
                            Symphony.Customer.EditCompany(0, me.salesChannelId, Ext.bind(me.refresh, me));
                        }
                    }, '-', '<span style="font-style:italic">Double click a company to edit</span>']
                },
                listeners: {
                    itemdblclick: function (grid, record) {
                        me.fireEvent('open', record.get('id'), me.salesChannelId, record.get('name'));
                    }
                },
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'customer'
            });
            this.callParent(arguments);
        }
    });


    Symphony.Customer.BasicCustomersGrid = Ext.define('customer.basiccustomersgrid', {
        alias: 'widget.customer.basiccustomersgrid',
        extend: 'symphony.searchablegrid',
        salesChannelId: -1,
        initComponent: function () {
            var me = this;
            var url = '/services/customer.svc/customers/saleschannel/' + this.salesChannelId;


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    width: 145,
                    renderer: Symphony.checkRenderer
                },
                columns: [
                    { /*id: 'name',*/ header: 'Name', dataIndex: 'name', align: 'left', renderer: Symphony.Portal.valueRenderer },
                    { header: 'Sub-Domain', dataIndex: 'subdomain', renderer: Symphony.Portal.valueRenderer }
                ]
            });

            Ext.apply(this, {
                listeners: {
                    rowclick: function (grid) {
                        var record = grid.getSelectionModel().getSelection()[0];
                        me.fireEvent('customerselect', record.get('id'), me.salesChannelId, record.get('name'));
                    }
                },
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'customer'
            });
            this.callParent(arguments);
        },
        setSalesChannel: function (salesChannelId) {
            this.salesChannelId = salesChannelId;
            this.updateStore('/services/customer.svc/customers/saleschannel/' + this.salesChannelId);
        }
    });


    Symphony.Customer.AdministratorsGrid = Ext.define('customer.administratorsgrid', {
        alias: 'widget.customer.administratorsgrid',
        extend: 'symphony.searchablegrid',
        salesChannelId: 0,
        initComponent: function () {
            var me = this;
            var url = '/services/customer.svc/administrators/saleschannel/' + this.salesChannelId;


            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.valueRenderer
                },
                columns: [
                    { /*id: 'username',*/ header: 'Username', dataIndex: 'username' },
                    { /*id: 'firstName',*/ header: 'First Name', dataIndex: 'firstName' },
                    { /*id: 'lastName',*/ header: 'Last Name', dataIndex: 'lastName' },
                    { /*id: 'isAccountExec',*/ header: 'Account Executive', dataIndex: 'isAccountExec', align: 'center', renderer: Symphony.checkRenderer },
                    { /*id: 'isCustomerCare',*/ header: 'Customer Care Rep', dataIndex: 'isCustomerCare', width: 120, align: 'center', renderer: Symphony.checkRenderer }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Add User',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.edit(0, 0, me.salesChannelId);
                        }
                    }, {
                        text: 'Edit User',
                        name: 'editButton',
                        iconCls: 'x-button-edit',
                        disabled: true,
                        handler: function () {
                            var data = me.getSelectionModel().getSelection()[0].data;
                            me.edit(data.id, data.customerId, me.salesChannelId);
                        }
                    }]
                },
                viewConfig: {
                    listeners: {
                        'refresh': function () {
                            me.getDockedItems('toolbar[dock="top"]')[0].find('name', 'editButton')[0].disable();
                        }
                    }
                },
                listeners: {
                    rowselect: function () {
                        me.getDockedItems('toolbar[dock="top"]')[0].find('name', 'editButton')[0].enable();
                    },
                    itemdblclick: function (grid, record, item, rowIndex, e) {
                        var data = record.data;
                        me.edit(data.id, data.customerId, me.salesChannelId);
                    },
                    itemcontextmenu: function (view, record, item, i, e) {
                        var id = record.get('id');
                        var isAccountExec = record.get('isAccountExec'), isCustomerCare = record.get('isCustomerCare');
                        var menu = new Ext.menu.Menu({
                            items: [
                                {
                                    text: isAccountExec ? 'Make this user not an account exec' : 'Make this user an account exec',
                                    handler: function () {
                                        Symphony.Ajax.request({
                                            url: '/services/customer.svc/users/' + id + '/toggleaccountexec/',
                                            success: function () {
                                                me.refresh();
                                            }
                                        });
                                    }
                                },
                                {
                                    text: isCustomerCare ? 'Make this user not a customer care rep' : 'Make this user a customer care rep',
                                    handler: function () {
                                        Symphony.Ajax.request({
                                            url: '/services/customer.svc/users/' + id + '/togglecustomercare/',
                                            success: function () {
                                                me.refresh();
                                            }
                                        });
                                    }
                                }
                            ]
                        });
                        menu.showAt(e.getXY());
                        e.stopEvent();
                    }
                },
                idProperty: 'id',
                url: url,
                colModel: colModel,
                model: 'user'
            });
            this.callParent(arguments);
        },
        edit: function (userId, customerId, salesChannelId) {
            var me = this;

            var w = new Ext.Window({
                title: (userId ? 'Edit' : 'Add') + ' an Administrator',
                height: 260,
                width: 340,
                layout: 'fit',
                modal: true,
                items: [{
                    layout: 'fit',
                    border: false,
                    userId: userId,
                    customerId: customerId,
                    salesChannelId: salesChannelId,
                    xtype: 'customer.administratorform',
                    listeners: {
                        cancel: function () {
                            w.destroy();
                        },
                        save: function (data) {
                            me.refresh();
                            w.destroy();
                        }
                    }
                }]
            });
            w.show();
        }
    });


    Symphony.Customer.EditCompany = function (customerId, salesChannelId, callback) {
        var w = new Ext.Window({
            title: (customerId ? 'Edit' : 'Add') + ' a Company',
            height: 635,
            width: 800,
            layout: 'fit',
            modal: true,
            items: [{
                layout: 'fit',
                border: false,
                customerId: customerId,
                salesChannelId: salesChannelId,
                xtype: 'customer.customerform',
                listeners: {
                    cancel: function () {
                        w.destroy();
                    },
                    save: function (data) {
                        callback(data);
                        w.destroy();
                    }
                }
            }]
        });
        w.show();
    };

    Symphony.Customer.AdministratorForm = Ext.define('customer.administratorform', {
        alias: 'widget.customer.administratorform',
        extend: 'Ext.Panel',
        userId: 0,
        salesChannelId: 0,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                tbar: {
                    xtype: 'symphony.savecancelbar',
                    items: [{
                        text: 'Unlock',
                        iconCls: 'x-button-unlock',
                        handler: function () {
                            Symphony.Ajax.request({
                                url: '/services/customer.svc/users/' + me.userId + '/customer/' + me.customerId + '/reset/',
                                success: function (args) {
                                    Ext.Msg.alert('Reset Complete', 'The user\'s account has been successfully unlocked.');
                                }
                            });
                        }
                    }],
                    listeners: {
                        save: function () {
                            var form = me.find('xtype', 'form')[0];

                            if (!form.isValid()) { return false; }

                            var administrator = form.getValues();
                            administrator.salesChannelId = me.salesChannelId;

                            Symphony.Ajax.request({
                                url: '/services/customer.svc/administrators/' + me.userId,
                                jsonData: administrator,
                                success: function (result) {
                                    me.fireEvent('save', result.data);
                                }
                            });
                        },
                        cancel: function () { me.fireEvent('cancel'); }
                    }
                },
                items: [{
                    border: false,
                    bodyStyle: 'padding: 15px',
                    items: [{
                        border: false,
                        xtype: 'form',
                        frame: true,
                        labelWidth: 125,
                        defaults: { anchor: '100%', xtype: 'textfield' },

                        items: [{
                            name: 'username',
                            fieldLabel: 'Username',
                            allowBlank: false
                        }, {
                            name: 'password',
                            fieldLabel: 'Password',
                            allowBlank: true
                        }, {
                            name: 'firstName',
                            fieldLabel: 'First Name',
                            allowBlank: false
                        }, {
                            name: 'lastName',
                            fieldLabel: 'Last Name',
                            allowBlank: false
                        }, {
                            name: 'email',
                            fieldLabel: 'Email',
                            allowBlank: false
                        }, {
                            name: 'telephoneNumber',
                            fieldLabel: 'Phone Number',
                            allowBlank: false
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        onRender: function () {
            if (this.userId) {
                var me = this;
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/customer.svc/administrators/' + this.userId,
                    success: function (result) {
                        me.find('xtype', 'form')[0].bindValues(result.data);
                    }
                });
            }
            Symphony.Customer.AdministratorForm.superclass.onRender.apply(this, arguments);
        }
    });




    Symphony.Customer.CustomerEditor = Ext.define('customer.customereditor', {
        alias: 'widget.customer.customereditor',
        extend: 'Ext.Panel',
        customerId: 0,
        customerName: '',
        salesChannelId: 0,
        initComponent: function () {
            var me = this;
            var tabs = Symphony.clone(customerTabs);

            if (me.isCustomerAdministration) {
                tabs = [{
                    title: 'Details',
                    xtype: 'customer.customerform',
                    isCustomerAdministration: true,
                    customerId: me.customerId,
                    salesChannelId: me.salesChannelId,
                    iconCls: 'x-menu-gear',
                    layout: 'fit',
                    listeners: {
                        save: function (data) {
                            me.fireEvent('save', data);
                        }
                    }
                }].concat(tabs);
            }

            // set the customer id so when we're working in a sales channel, we know which customer we're editing
            Ext.each(tabs, function (tab) {
                tab.customerId = me.customerId;
            });
            Ext.apply(this, {
                layout: 'fit',
                tbar: !me.isCustomerAdministration ? {
                    items: [{
                        text: 'Edit Company Properties',
                        iconCls: 'x-button-edit',
                        handler: function () {
                            Symphony.Customer.EditCompany(me.customerId, me.salesChannelId, function (args) {
                                me.setTitle(args.name);
                                me.fireEvent('edit', args);
                            });
                        }
                    }]
                } : null,
                border: false,
                items: [{
                    border: false,
                    xtype: 'tabpanel',
                    activeItem: 0,
                    items: tabs
                }]
            });
            this.callParent(arguments);
        }
    });



    Symphony.Customer.HierarchyTab = Ext.define('customer.hierarchytab', {
        alias: 'widget.customer.hierarchytab',
        extend: 'Ext.Panel',
        customerId: 0,
        //hierarchyName: 'location',
        //editxtype: 'customer.locationform',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                tbar: {
                    items: !Symphony.Customer.enableEditing ? [] : [{
                        text: 'Add ' + Symphony.Aliases[me.hierarchyName],
                        iconCls: 'x-button-add',
                        handler: function () {
                            var tree = me.find('xtype', 'customer.hierarchytree')[0];
                            var node = tree.getSelectionModel().getSelection()[0];
                            var parentId = 0;
                            if (node) {
                                parentId = node.getId();
                            }
                            me.edit(me.customerId, 0, parentId);
                        }
                    }, {
                        text: 'Edit ' + Symphony.Aliases[this.hierarchyName],
                        name: 'edit',
                        disabled: true,
                        iconCls: 'x-button-edit',
                        handler: function () {
                            var tree = me.find('xtype', 'customer.hierarchytree')[0];
                            var node = tree.getSelectionModel().getSelection()[0];
                            if (!node) { return; }
                            var record = node.attributes.record;
                            me.edit(me.customerId, record.get('id'), record.get('parentId'));
                        }
                    }, {
                        text: 'Delete ' + Symphony.Aliases[this.hierarchyName],
                        name: 'delete',
                        disabled: true,
                        iconCls: 'x-button-delete',
                        handler: function () {
                            var tree = me.find('xtype', 'customer.hierarchytree')[0];
                            var node = tree.getSelectionModel().getSelection()[0];
                            if (!node) { return; }
                            var record = node.attributes.record;
                            me.del(record.get('id'), record.get('name'));
                        }
                    }]
                },
                layout: 'border',
                items: [{
                    border: false,
                    region: 'west',
                    xtype: 'customer.hierarchytree',
                    iconCls: this.iconCls,
                    hierarchyName: this.hierarchyName,
                    title: this.title,
                    customerId: this.customerId,
                    split: true,
                    width: 300,
                    listeners: {
                        click: function (record, node, e) {
                            //if (!node.attributes.value) { return; }
                            var id = record.get('id');
                            me.find('xtype', 'customer.hierarchyusers')[0].setFilter(id);
                            me.find('xtype', 'customer.hierarchytiles')[0].setFilter(id);
                            me.find('xtype', 'license.genericassignmenttab')[0].setFilter(id, me.hierarchyName.toLowerCase());

                            // Because we are re-using the license generic tab, this is handled differently than the above tabs:
                            var licenseTab = {
                                title: 'Licenses',
                                border: false,
                                url: '/services/license.svc/reports/' + me.entityType + '/' + me.entityId,
                                xtype: 'license.genericassignmenttab'
                            };

                            var tabPanel = Ext.getCmp('companyInfoTabPanel');

                            Ext.each(['edit', 'delete'], function (name) {
                                var item = me.getDockedItems('toolbar[dock="top"]')[0].find('name', name)[0];
                                if (item) {
                                    item.enable();
                                }
                            });
                        },
                        selectioncleared: function () {
                            me.find('xtype', 'customer.hierarchyusers')[0].setFilter(0);
                            me.find('xtype', 'customer.hierarchytiles')[0].setFilter(0);
                            Ext.each(['edit', 'delete'], function (name) {
                                var item = me.getDockedItems('toolbar[dock="top"]')[0].find('name', name)[0];
                                if (item) {
                                    item.disable();
                                }
                            });
                        }
                    }
                }, {
                    region: 'center',
                    xtype: 'tabpanel',
                    id: 'companyInfoTabPanel',
                    activeItem: 0,
                    border: false,
                    items: [{
                        xtype: 'customer.hierarchyusers',
                        border: false,
                        title: 'Users',
                        customerId: this.customerId,
                        hierarchyName: this.hierarchyName
                    }, {
                        title: 'Tiles',
                        border: false,
                        customerId: this.customerId,
                        hierarchyName: this.hierarchyName,
                        xtype: 'customer.hierarchytiles'
                    }, {
                        title: 'Licenses',
                        id: 'companyInfoLicenseTabPanel',
                        border: false,
                        hierarchyName: this.hierarchyName,
                        url: '/services/license.svc/reports/' + me.entityType + '/' + me.entityId,
                        xtype: 'license.genericassignmenttab'
                    }
                    ]
                }],
                listeners: {
                    activate: {
                        fn: function (panel) {
                            panel.query('[xtype=customer.hierarchytree]')[0].refresh();
                        },
                        single: true
                    }
                }
            });
            this.callParent(arguments);
        },
        edit: function (customerId, itemId, parentId) {
            var me = this;
            var height = (me.hierarchyName == 'location' ? 315 : me.hierarchyName == 'audience' ? 200 : 155);
            var w = new Ext.Window({
                title: (itemId ? 'Edit' : 'Add') + ' a ' + Symphony.Aliases[me.hierarchyName],
                height: height,
                width: 500,
                layout: 'fit',
                modal: true,
                items: [{
                    layout: 'fit',
                    border: false,
                    itemId: itemId,
                    parentId: parentId,
                    customerId: customerId,
                    xtype: me.editxtype,
                    listeners: {
                        cancel: function () {
                            w.destroy();
                        },
                        save: function () {
                            me.find('xtype', 'customer.hierarchytree')[0].refresh();
                            w.destroy();
                        }
                    }
                }]
            });
            w.show();
        },
        del: function (id, name) {
            var me = this;
            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete the ' + Symphony.Aliases[me.hierarchyName] + ' "' + name + '"?', function (btn) {
                if (btn == 'yes') {
                    Symphony.Ajax.request({
                        url: '/services/customer.svc/' + me.hierarchyName + 's/delete/' + id,
                        success: function (result) {
                            me.find('xtype', 'customer.hierarchytree')[0].refresh();
                        }
                    });
                }
            });
        }
    });


    Symphony.Customer.LocationForm = Ext.define('customer.locationform', {
        alias: 'widget.customer.locationform',
        extend: 'Ext.Panel',
        customerId: 0,
        itemId: 0,
        parentId: 0,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                tbar: {
                    xtype: 'symphony.savecancelbar',
                    listeners: {
                        save: function () {
                            var form = me.find('xtype', 'form')[0];

                            if (!form.isValid()) { return false; }

                            var location = form.getValues();

                            location.parentId = me.parentId == 'root' ? 0 : me.parentId;
                            location.stateId = location.stateId || 0;
                            location.state = location.state || '';

                            Symphony.Ajax.request({
                                url: '/services/customer.svc/locations/' + me.itemId + '/customer/' + me.customerId,
                                jsonData: location,
                                success: function (result) {
                                    me.fireEvent('save', result.data);
                                }
                            });
                        },
                        cancel: function () { me.fireEvent('cancel'); }
                    }
                },
                items: [{
                    border: false,
                    bodyStyle: 'padding: 15px',
                    items: [{
                        border: false,
                        xtype: 'form',
                        frame: true,
                        labelWidth: 145,
                        defaults: { anchor: '100%', xtype: 'textfield' },
                        items: [{
                            name: 'name',
                            fieldLabel: 'Name',
                            allowBlank: false
                        }, {
                            name: 'internalCode',
                            fieldLabel: 'Internal Code',
                            allowBlank: true
                        }, {
                            name: 'address1',
                            fieldLabel: 'Address 1',
                            allowBlank: true
                        }, {
                            name: 'address2',
                            fieldLabel: 'Address 2',
                            allowBlank: true
                        }, {
                            name: 'city',
                            fieldLabel: 'City',
                            allowBlank: true
                        }, {
                            name: 'state',
                            fieldLabel: 'State',
                            xtype: 'symphony.pagedcombobox',
                            url: '/services/classroom.svc/states/',
                            model: 'state',
                            bindingName: 'state',
                            valueField: 'description',
                            displayField: 'description',
                            allowBlank: true,
                            emptyText: 'Select a State...',
                            valueNotFoundText: 'Select a State...'
                        }, {
                            name: 'postalCode',
                            fieldLabel: 'Zip Code',
                            allowBlank: true
                        }, {
                            name: 'telephoneNumber',
                            fieldLabel: 'Telephone Number',
                            allowBlank: true
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        onRender: function () {
            if (this.itemId) {
                var me = this;
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/customer.svc/locations/' + this.itemId + '/customer/' + this.customerId,
                    success: function (result) {
                        me.find('xtype', 'form')[0].bindValues(result.data);
                    }
                });
            }
            Symphony.Customer.LocationForm.superclass.onRender.apply(this, arguments);
        }
    });


    Symphony.Customer.JobRoleForm = Ext.define('customer.jobroleform', {
        alias: 'widget.customer.jobroleform',
        extend: 'Ext.Panel',
        customerId: 0,
        itemId: 0,
        parentId: 0,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                tbar: {
                    xtype: 'symphony.savecancelbar',
                    listeners: {
                        save: function () {
                            var form = me.find('xtype', 'form')[0];

                            if (!form.isValid()) { return false; }

                            var jobRole = form.getValues();
                            jobRole.parentId = me.parentId == 'root' ? 0 : me.parentId;

                            Symphony.Ajax.request({
                                url: '/services/customer.svc/jobroles/' + me.itemId + '/customer/' + me.customerId,
                                jsonData: jobRole,
                                success: function (result) {
                                    me.fireEvent('save', result.data);
                                }
                            });
                        },
                        cancel: function () { me.fireEvent('cancel'); }
                    }
                },
                items: [{
                    border: false,
                    bodyStyle: 'padding: 15px',
                    items: [{
                        border: false,
                        xtype: 'form',
                        frame: true,
                        labelWidth: 125,
                        defaults: { anchor: '100%', xtype: 'textfield' },
                        items: [{
                            name: 'name',
                            fieldLabel: 'Name',
                            allowBlank: false
                        }, {
                            name: 'internalCode',
                            fieldLabel: 'Internal Code',
                            allowBlank: true
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        onRender: function () {
            if (this.itemId) {
                var me = this;
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/customer.svc/jobroles/' + this.itemId + '/customer/' + this.customerId,
                    success: function (result) {
                        me.find('xtype', 'form')[0].bindValues(result.data);
                    }
                });
            }
            Symphony.Customer.JobRoleForm.superclass.onRender.apply(this, arguments);
        }
    });


    Symphony.Customer.AudienceForm = Ext.define('customer.audienceform', {
        alias: 'widget.customer.audienceform',
        extend: 'Ext.Panel',
        customerId: 0,
        itemId: 0,
        parentId: 0,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                tbar: {
                    xtype: 'symphony.savecancelbar',
                    listeners: {
                        save: function () {
                            var form = me.find('xtype', 'form')[0];

                            if (!form.isValid()) { return false; }

                            var jobRole = form.getValues();
                            jobRole.parentId = me.parentId == 'root' ? 0 : me.parentId;

                            Symphony.Ajax.request({
                                url: '/services/customer.svc/audiences/' + me.itemId + '/customer/' + me.customerId,
                                jsonData: jobRole,
                                success: function (result) {
                                    me.fireEvent('save', result.data);
                                }
                            });
                        },
                        cancel: function () { me.fireEvent('cancel'); }
                    }
                },
                items: [{
                    border: false,
                    bodyStyle: 'padding: 15px',
                    items: [{
                        border: false,
                        xtype: 'form',
                        frame: true,
                        labelWidth: 125,
                        defaults: { anchor: '100%', xtype: 'textfield' },
                        items: [{
                            name: 'name',
                            fieldLabel: 'Name',
                            allowBlank: false
                        }, {
                            name: 'internalCode',
                            fieldLabel: 'Internal Code',
                            allowBlank: true
                        }, {
                            name: 'salesforcePartnerCode',
                            fieldLabel: 'Salesforce Partner Code',
                            allowBlank: true
                        }]
                    }]
                }]
            });
            this.callParent(arguments);
        },
        onRender: function () {
            if (this.itemId) {
                var me = this;
                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/customer.svc/audiences/' + this.itemId + '/customer/' + this.customerId,
                    success: function (result) {
                        me.find('xtype', 'form')[0].bindValues(result.data);
                    }
                });
            }
            Symphony.Customer.AudienceForm.superclass.onRender.apply(this, arguments);
        }
    });


    Symphony.Customer.HierarchyTree = Ext.define('customer.hierarchytree', {
        alias: 'widget.customer.hierarchytree',
        extend: 'symphony.tabletree',
        customerId: 0,
        hierarchyName: 'location',
        title: 'Location',
        plugins: { ptype: 'bufferedrenderer' },
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                viewConfig: Symphony.Customer.enableEditing ? {
                    plugins: {
                        ptype: 'treeviewdragdrop'
                    },
                    listeners: {
                        drop: function (node, data, overModel, dropPosition, opts) {
                            
                            var id = data.records[0].getId();
                            var parent = data.records[0].parentNode;
                            var parentId = 0;

                            if (parent && !parent.isRoot()) {
                                parentId = parent.getId();
                            }

                            var name = data.records[0].get('name');

                            Symphony.Ajax.request({
                                url: '/services/customer.svc/' + me.hierarchyName.toLowerCase() + 's/' + id + '/customer/' + me.customerId + '?reparent=true',
                                jsonData: { id: id, name: name, parentId: parentId },
                                success: function (result) {
                                    // refresh to ensure we have the latest data
                                    me.refresh();
                                },
                                failure: function (result) {
                                    if (result.status != 200) {
                                        Ext.Msg.alert('Error', result.statusText);
                                    } else {
                                        Ext.Msg.alert('Error', result.error);
                                    }
                                    // refresh to make everything go back the way it was
                                    me.refresh();
                                }
                            });
                        }
                    }
                } : null,
                rootVisible: false,
                rootText: this.title, 
                iconCls: this.iconCls,
                columns: [{
                    xtype: 'treecolumn', //this is so we know which column will show the tree
                    text: this.title,
                    flex: 2,
                    sortable: true,
                    dataIndex: 'name'
                }],
                url: '/services/customer.svc/' + this.hierarchyName.toLowerCase() + 's/customer/' + this.customerId,
                model: this.hierarchyName == 'location' ? 'locationModel' : this.hierarchyName,
                deferLoad: true
            });
            this.callParent(arguments);
        }
    });


    Symphony.Customer.HierarchyPermTree = Ext.define('customer.hierarchypermtree', {
        alias: 'widget.customer.hierarchypermtree',
        extend: 'symphony.tabletree',
        customerId: 0,
        userId: 0,
        lockControl: false,
        hierarchyName: 'location',
        title: 'Location',
        checkedElements: [],
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                enableDD: Symphony.Customer.enableEditing,
                rootVisible: true,
                rootText: this.title,
                iconCls: this.iconCls,
                url: '/services/customer.svc/' + this.hierarchyName.toLowerCase() + 's/customer/' + this.customerId,
                //recordDef: Ext.data.Record.create(Symphony.Definitions[this.hierarchyName]),
                model: this.hierarchyName == 'location' ? 'locationModel' : this.hierarchyName,
                columns: [{
                    dataIndex: 'name',
                    xtype: 'treecolumn',
                    flex: 1
                }],
                listeners: {
                    dragdrop: function (panel, node, dd, e) {
                        // when the item is dropped, save its new parent
                        var parentId = node.parentNode.attributes.value;
                        var id = node.attributes.value;
                        var name = node.attributes.text;
                        Symphony.Ajax.request({
                            url: '/services/customer.svc/' + me.hierarchyName.toLowerCase() + 's/' + id + '/customer/' + me.customerId + '?reparent=true',
                            jsonData: { id: id, name: name, parentId: parentId },
                            success: function (result) {
                                // refresh to ensure we have the latest data
                                me.refresh();
                            },
                            failure: function (result) {
                                if (result.status != 200) {
                                    Ext.Msg.alert('Error', result.statusText);
                                } else {
                                    Ext.Msg.alert('Error', result.error);
                                }
                                // refresh to make everything go back the way it was
                                me.refresh();
                            }
                        });
                    },
                    treedataloaded: function () {
                        if (this.disabled) return;
                        if (me.userId > 0) {
                            me.loadUserData();
                        }
                    }
                }
            });
            this.callParent(arguments);
        },
        loadUserData: function () {
            var me = this;
            var repprmtype = 0;
            switch (this.hierarchyName.toLocaleLowerCase()) {
                case 'location':
                    repprmtype = 2;
                    break;
                case 'jobrole':
                    repprmtype = 1;
                    break;
                case 'audience':
                    repprmtype = 3;
                    break;
            }
            Symphony.Ajax.request({
                url: '/services/customer.svc/usersreportprm/' + me.userId + '/customer/' + me.customerId + '/prmtype/' + repprmtype,
                method: 'GET',
                success: function (result) {
                    var mdata = result.data;
                    me.checkedElements.length = 0;
                    for (var i = 0; i < mdata.length; i++) {
                        me.checkedElements.push(mdata[i].objectId);
                    }
                    me.getRootNode().cascadeBy(function (n) {
                        if (me.checkedElements.indexOf(n.data.id) > -1) {
                            n.set('checked', true);
                            }
                    });
                },
                failure: function (result) {
                    if (result.status != 200) {
                        Ext.Msg.alert('Error', result.statusText);
                    } else {
                        Ext.Msg.alert('Error', result.error);
                    }
                    // refresh to make everything go back the way it was
                }
            });
        }
    });


    Symphony.Customer.HierarchyUsers = Ext.define('customer.hierarchyusers', {
        alias: 'widget.customer.hierarchyusers',
        extend: 'symphony.searchablegrid',
        customerId: 0,
        hierarchyId: 0,
        hierarchyName: 'location',
        initComponent: function () {
            var me = this;
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { /*id: 'firstName',*/ header: 'First Name', dataIndex: 'firstName' },
                    { /*id: 'lastName',*/ header: 'Last Name', dataIndex: 'lastName' },
                    { /*id: 'supervisor',*/ header: 'Classroom Supervisor', dataIndex: 'supervisor' },
                    { /*id: 'reportingSupervisor',*/ header: 'Reporting Supervisor', dataIndex: 'reportingSupervisor' },
                    { /*id: 'status',*/ header: 'Status', dataIndex: 'status', align: 'center' },
                    { /*id: 'location',*/ header: Symphony.Aliases.location, dataIndex: 'location' },
                    { /*id: 'jobRole',*/ header: Symphony.Aliases.jobRole, dataIndex: 'jobRole' },
                    {
                        /*id: 'audiences',*/ header: Symphony.Aliases.audiences, dataIndex: 'audiences',
                        flex: 1
                    }
                ]
            });
            Ext.apply(this, {
                tbar: {
                    items: !Symphony.Customer.enableEditing ? [] : [{
                        text: 'Add User to this ' + Symphony.Aliases[this.hierarchyName],
                        iconCls: 'x-button-add',
                        name: 'add',
                        disabled: true,
                        handler: Ext.bind(this.selectUsers, this)
                    },
                    {
                        text: 'Remove User from this ' + Symphony.Aliases[this.hierarchyName],
                        iconCls: 'x-button-delete',
                        name: 'delete',
                        disabled: true,
                        handler: function () {
                            var results = [];
                            Ext.each(me.getSelectionModel().getSelections(), function (record) {
                                results.push(record.get('id'));
                            });
                            me.deleteUserMap(results);
                        }
                    }]
                },
                deferLoad: true, // no need to load the data until a location is selected
                colModel: colModel,
                url: '/ignoreme', // url is required for Ext to work, but we're setting it in the beforeload
                model: 'user',
                viewConfig: {
                    listeners: {
                        'refresh': function () {
                            var item = me.getDockedItems('toolbar[dock="top"]')[0].find('name', 'delete')[0];
                            if (item) { item.disable(); }
                        }
                    }
                },
                listeners: {
                    rowselect: function () {
                        var item = me.getDockedItems('toolbar[dock="top"]')[0].find('name', 'delete')[0];
                        if (item) { item.enable(); }
                    },
                    itemdblclick: function (grid, record, item, rowIndex, e) {
                        var data = record.data;
                        me.edit(data.customerId, data.id);
                    }
                }
            });
            this.callParent(arguments);

            this.store.on('beforeload', function (store) {
                store.getProxy().api.read = '/services/customer.svc/users/customer/' + this.customerId + '/' + this.hierarchyName.toLowerCase() + '/' + this.hierarchyId;
            }, this);
        },
        edit: function (customerId, userId) {
            var me = this;
            Symphony.Customer.showUserEditWindow(userId, customerId, function () {
                me.refresh();
            });
        },
        setFilter: function (id) {
            this.hierarchyId = id;
            var addBtn = this.getDockedItems('toolbar[dock="top"]')[0].find('name', 'add')[0];
            if (id != 0) {
                this.refresh();
                if (addBtn) { addBtn.enable(); }
            } else {
                if (addBtn) { addBtn.disable(); }
            }
        },
        getUserIds: function () {
            var results = [];
            this.store.each(function (r) {
                results.push(r.get('id'));
            });
            return results;
        },
        selectUsers: function () {
            var me = this;
            var w = new Ext.Window({
                title: 'Add Users',
                height: 410,
                width: 500,
                layout: 'fit',
                modal: true,
                items: [{
                    layout: 'fit',
                    border: false,
                    customerId: me.customerId,
                    hierarchyName: me.hierarchyName,
                    hierarchyId: me.hierarchyId,
                    filter: me.getUserIds(),
                    xtype: 'customer.filtereduserlist',
                    listeners: {
                        cancel: function () {
                            w.destroy();
                        },
                        save: function () {
                            me.refresh();
                            this.clearSelections();
                            w.destroy();
                        }
                    }
                }]
            });
            w.show();
        },
        deleteUserMap: function (userIds) {
            var me = this;
            var word = userIds.length > 1 ? 'users' : 'user';
            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to remove the selected ' + word + ' from this ' + Symphony.Aliases[me.hierarchyName] + '?', function (btn) {
                if (btn == 'yes') {
                    Symphony.Ajax.request({
                        url: '/services/customer.svc/' + me.hierarchyName + 's/' + me.hierarchyId + '/map/delete/',
                        jsonData: userIds,
                        success: function () {
                            me.refresh();
                        }
                    });
                }
            });
        }
    });


    Symphony.Customer.showUserEditWindow = function (userId, customerId, callback) {
        var w = new Symphony.Customer.UserEditWindow({
            stateful: true,
            stateId: 'usereditwindow',
            customerId: customerId,
            userId: userId,
            listeners: {
                save: function () {
                    if (typeof (callback) === 'function') {
                        callback();
                    }
                }
            }
        });
        w.show();
    },

    Symphony.Customer.UserEditWindow = Ext.define('customer.usereditwindow', {
        alias: 'widget.customer.usereditwindow',
        extend: 'Ext.Window',
        customerId: null,
        userId: null,
        initComponent: function () {
            var me = this;

            me.width = Ext.getBody().getWidth();
            me.height = Ext.getBody().getHeight();

            me.width = Math.min(me.width, 1200);
            me.height = Math.min(me.height, 800);

            me.width = Math.max(me.width, 600);
            me.height = Math.max(me.height, 400);

            // IE7: IE was unable to automatically determine what the tab indexes should
            // be for the user form when it was shown in a window. As a result, you could
            // not tab from input field to input field.
            //
            // Enable the ExtJS focus manager for this instead, which captures the tab
            // key and uses its own "next field" logic.
            //
            // To avoid affecting other components, turn this on when the modal is shown,
            // and turn it off when the modal is hidden.
            //
            // In case we globally enable the focus manager at some point, we add a flag
            // here so that we don't accidentally disable it in that case.
            if (!Ext.FocusManager.enabled) {
                this._enabledFocusMgr = true;
                Ext.FocusManager.enable();
            }

            this.on('destroy', function() {
                if (this._enabledFocusMgr) {
                    Ext.FocusManager.disable();
                    delete this._enabledFocusMgr;
                }
            }, this, {single: true});

            Ext.apply(this, {
                title: (me.userId ? 'Edit' : 'Add') + ' a User',
                height: me.height,
                width: me.width,
                layout: 'fit',
                modal: true,
               
                listeners: {
                    show: function (w) {
                        Symphony.App.addListener('resize', w.resizeUserEditWindow, w);
                    },
                    hide: function (w) {
                        Symphony.App.removeListener('resize', w.resizeUserEditWindow, w);
                    }
                },
                items: [{
                    id: 'usereditor',
                    layout: 'fit',
                    border: false,
                    userId: me.userId,
                    customerId: me.customerId,
                    xtype: 'customer.userform',
                    bodyCls: 'x-panel-default-framed',
                    bodyStyle: 'padding: 0px',
                    width: me.width,
                    listeners: {
                        cancel: function () {
                            me.destroy();
                        },
                        save: function () {
                            me.fireEvent('save');
                            me.destroy();
                        }
                    }
                }]
            });
            this.callParent(arguments);
        },
        resizeUserEditWindow: function () {
            this.width = Ext.getBody().getWidth();
            this.height = Ext.getBody().getHeight();

            this.width = Math.min(this.width, 1200);
            this.height = Math.min(this.height, 800);

            this.width = Math.max(this.width, 600);
            this.height = Math.max(this.height, 400);

            this.setWidth(this.width);
            this.setHeight(this.height);

            this.center();
        }
    });


    Symphony.Customer.FilteredUserList = Ext.define('customer.filtereduserlist', {
        alias: 'widget.customer.filtereduserlist',
        extend: 'symphony.searchablegrid',
        customerId: 0,
        hierarchyName: 'location',
        hierarchyId: 0,
        initComponent: function () {
            var me = this;

            var sm = new Ext.selection.CheckboxModel({
                checkOnly: true,
                sortable: true,
                header: ' ' // kills the select-all checkbox
            });
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [

                    { header: 'First Name', dataIndex: 'firstName' },
                    {
                        /*id: 'lastName',*/ header: 'Last Name', dataIndex: 'lastName',
                        flex: 1
                    }
                ]
            });
            
            Ext.apply(this, {
                tbar: {
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        listeners: {
                            save: function () {
                                
                                var data = [];
                                var selections = me.getSelections();
                                for (var i = 0; i < selections.length; i++) {
                                    data.push(selections[i].get('id'));
                                }

                                Symphony.Ajax.request({
                                    url: '/services/customer.svc/' + me.hierarchyName + 's/' + me.hierarchyId + '/map/',
                                    jsonData: data,
                                    success: function () {
                                        me.fireEvent('save', me.getSelections());
                                    }
                                });
                            },
                            cancel: function () {
                                me.fireEvent('cancel');
                            }
                        }
                    }]
                },
                idProperty: 'id',
                url: '/ignoreme', // set in the beforeload filter
                colModel: colModel,
                model: 'user',
                selModel: sm,
                plugins: [{ ptype: 'pagingselectpersist' }]
            });
            this.callParent(arguments);
            this.store.on('beforeload', this.selectedUserFilter, this);
        },
        selectedUserFilter: function (store, options) {
            store.getProxy().api.read = '/services/customer.svc/users/customer/' + this.customerId + '/unassigned/' + this.hierarchyName + '/' + this.hierarchyId;
            store.getProxy().actionMethods.read = 'GET';
            store.getProxy().jsonData = this.filter;
        },

        getSelections: function () {
            return this.getPlugin('pagingSelectionPersistence').getPersistedSelection();
        },
        setSelections: function (selections) {
            this.getPlugin('pagingSelectionPersistence').setSelections(selections);
        },
        clearSelections: function () {
            this.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
        },
        onRender: function () {
            Symphony.Customer.FilteredUserList.superclass.onRender.apply(this, arguments);

            // after the load
            this.setSelections(this.selections || []);
        }
    });


    Symphony.Customer.UsersGrid = Ext.define('customer.usersgrid', {
        alias: 'widget.customer.usersgrid',
        extend: 'symphony.searchablegrid',
        customerId: 0,
        initComponent: function () {
            var me = this;
            var url = '/services/customer.svc/users/customer/' + this.customerId;

            var columns = {
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                items: [
                    { /*id: 'username',*/ header: Symphony.Aliases.username, dataIndex: 'username' },
                    { /*id: 'firstName',*/ header: 'First Name', dataIndex: 'firstName' },
                    { /*id: 'lastName',*/ header: 'Last Name', dataIndex: 'lastName' },
                    { /*id: 'supervisor',*/ header: 'Classroom Supervisor', dataIndex: 'supervisor' },
                    { /*id: 'secondarySupervisor',*/ header: 'Secondary Supervisor', dataIndex: 'secondarySupervisor' },
                    { /*id: 'reportingSupervisor',*/ header: 'Reporting Supervisor', dataIndex: 'reportingSupervisor' },
                    { /*id: 'employeeNumber',*/ header: 'Employee Number', dataIndex: 'employeeNumber' },
                    { /*id: 'status',*/ header: 'Status', dataIndex: 'status' },
                    { /*id: 'nmlsNumber',*/ header: 'NMLS Number', dataIndex: 'nmlsNumber', hidden: Symphony.Modules.hasModule(Symphony.Modules.Licenses) },
                    { /*id: 'location',*/ header: Symphony.Aliases.location, dataIndex: 'location' },
                    { /*id: 'jobRole',*/ header: Symphony.Aliases.jobRole, dataIndex: 'jobRole' },
                    {
                        /*id: 'audiences',*/ header: Symphony.Aliases.audiences, dataIndex: 'audiences',
                        flex: 1
                    }
                ]
            };

            if (Symphony.User.isSalesChannelAdmin) {
                columns.items.push({
                    header: 'View Transcript',
                    xtype: 'actioncolumn',
                    width: 100,
                    items: [{
                        icon: '/images/script.png',
                        tooltip: 'View Transcript',
                        handler: function (grid, rowIndex, colIndex, item, e, record) {
                            Symphony.Portal.showTranscriptWindow(record.get('fullName'), record.get('id'));
                        }
                    }]
                });
            }

            Ext.apply(this, {
                searchMenu: {
                    items: [{
                        text: 'Active',
                        group: 'searchGroup',
                        filter: { status: Symphony.UserStatusType.active },
                        checked: false,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }, {
                        text: 'Inactive',
                        group: 'searchGroup',
                        filter: { status: Symphony.UserStatusType.inactive },
                        checked: false,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }, {
                        text: 'All',
                        group: 'searchGroup',
                        checked: true,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }]
                },
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.edit(me.customerId, 0);
                        }
                    }, {
                        text: 'Edit',
                        name: 'editbtn',
                        iconCls: 'x-button-edit',
                        disabled: true,
                        handler: function () {
                            var data = me.getSelectionModel().getSelection()[0].data;
                            me.edit(data.customerId, data.id);
                        }
                    }, {
                        text: 'Delete',
                        name: 'deletebtn',
                        iconCls: 'x-button-delete',
                        disabled: true,
                        handler: function () {
                            var data = me.getSelectionModel().getSelection()[0].data;
                            me.del(data.customerId, data.id);
                        }
                    }]
                },
                url: url,
                model: 'user',
                columns: columns,
                deferLoad: true,
                viewConfig: {
                    listeners: {
                        'refresh': function () {
                            me.getDockedItems('toolbar[dock="top"]')[0].find('name', 'editbtn')[0].disable();
                            me.getDockedItems('toolbar[dock="top"]')[0].find('name', 'deletebtn')[0].disable();
                        }
                    }
                },
                listeners: {
                    rowselect: function () {
                        me.getDockedItems('toolbar[dock="top"]')[0].find('name', 'editbtn')[0].enable();
                        me.getDockedItems('toolbar[dock="top"]')[0].find('name', 'deletebtn')[0].enable()
                    },
                    itemdblclick: function (grid, record, item, index, e) {
                        var data = record.data;
                        me.edit(data.customerId, data.id);
                    }
                }
            });
            this.callParent(arguments);
        },
        edit: function (customerId, userId) {
            var me = this;
            Symphony.Customer.showUserEditWindow(userId, customerId, function () {
                me.refresh();
            });
        },
        del: function (customerId, userId) {
            var me = this;
            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this user?', function (btn) {
                if (btn == 'yes') {
                    Symphony.Ajax.request({
                        url: '/services/customer.svc/users/' + userId + '/customer/' + customerId + '/delete/',
                        success: function () {
                            me.refresh();
                        }
                    });
                }
            });
        }
    });


    Symphony.Customer.BasicUsersGrid = Ext.define('customer.basicusersgrid', {
        alias: 'widget.customer.basicusersgrid',
        extend: 'symphony.searchablegrid',
        customerId: 0,
        initComponent: function () {
            var me = this;
            var url = '/services/customer.svc/users/customer/' + this.customerId;

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { /*id: 'username',*/ header: Symphony.Aliases.username, dataIndex: 'username' },
                    { /*id: 'firstName',*/ header: 'First Name', dataIndex: 'firstName' },
                    { /*id: 'lastName',*/ header: 'Last Name', dataIndex: 'lastName' }
                ]
            });
            Ext.apply(this, {
                deferLoad: true,
                searchMenu: {
                    items: [{
                        text: 'Active',
                        group: 'searchGroup',
                        filter: { status: Symphony.UserStatusType.active },
                        checked: false,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }, {
                        text: 'Inactive',
                        group: 'searchGroup',
                        filter: { status: Symphony.UserStatusType.inactive },
                        checked: false,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }, {
                        text: 'All',
                        group: 'searchGroup',
                        checked: true,
                        checkHandler: Ext.bind(this.searchOptionClicked, this)
                    }]
                },
                url: url,
                model: 'user',
                colModel: colModel,
                listeners: {
                    rowselect: function (sm, rowIndex, record) {
                        // fire userselect with userId
                        me.fireEvent('userselect', record.get("id"));
                    }
                }
            });
            this.callParent(arguments);
        },
        setCustomer: function (customerId) {
            this.customerId = customerId;
            this.updateStore('/services/customer.svc/users/customer/' + this.customerId);
        }
    });


    Symphony.Customer.ImportPanel = Ext.define('customer.importpanel', {
        alias: 'widget.customer.importpanel',
        extend: 'Ext.Panel',
        getImportType: function () {
            return this.find('xtype', 'combo')[0].getValue()
        },
        initComponent: function () {
            var panel = this;

            Ext.apply(this, {
                layout: 'border',
                border: false,
                items: [{
                    region: 'north',
                    border: false,
                    xtype: 'toolbar',
                    height: 30,
                    items: ['New Import:',
                    {
                        xtype: 'combo',
                        queryMode: 'local',
                        editable: false,
                        forceSelection: true,
                        displayField: 'text',
                        valueField: 'id',
                        triggerAction: 'all',
                        store: Symphony.ImportTypeStore,
                        value: 0,
                        blankText: 'Select an Import Type',
                        valueNotFoundText: 'Select an Import Type'
                    }, {
                        xtype: 'swfuploadbutton',
                        text: 'Select a File...',
                        iconCls: 'x-button-add',
                        // the current user's domain
                        uploadUrl: '/Uploaders/ImportUploader.ashx?customer=' + Symphony.User.customerDomain,
                        fileTypes: '*.csv;*.xls,*;xlsx',
                        fileTypesDescription: 'Comma Separated Values and Excel',
                        fileQueueLimit: 2,
                        preview: true,
                        listeners: {
                            'dialogclose': function (selectedCount) {
                                if (this.getStats().files_queued > 1) {
                                    this.cancel();
                                }
                            },
                            'queue': function (file) {
                                this.ownerCt.find('name', 'startimport')[0].enable();
                                this.ownerCt.find('name', 'previewimport')[0].enable();
                                this.setText('File selected (' + file.name + ')');
                            },
                            'queueerror': function () {
                                Ext.Msg.alert('Error', 'The file could not be queued');
                            },
                            'uploadstart': function (item) {
                                this.addParameter('ASP.NET_SessionId', Symphony.Settings.sessionId);
                                this.addParameter('.ASPXAUTH', Symphony.Settings.aspxauth);
                                this.addParameter('importType', panel.getImportType());
                                this.addParameter('customerId', panel.customerId || Symphony.User.customerId);
                                this.addParameter('userId', Symphony.User.id);
                                this.addParameter('preview', this.preview);

                                Ext.Msg.progress('Please wait...', 'Please wait while your file is uploaded...');
                            },
                            'uploadprogress': function (item, completed, total) {
                                var ratio = parseFloat(completed / total);
                                Ext.Msg.updateProgress(ratio, parseInt(ratio * 100, 10) + "% complete");
                            },
                            'uploaderror': function () {
                                this.error = true;
                                Ext.Msg.hide();
                                Ext.Msg.alert('Upload failed', 'The upload failed. Please make sure it is within the proper file size limitations.');
                            },
                            'uploadsuccess': function (item, response, hasData) {
                                var result = Ext.decode(response);
                                this.working = false;
                                if (result.success) {
                                    Ext.Msg.hide();
                                    Ext.Msg.alert('Upload Complete', 'The upload has started; you may need to periodically refresh the "Import History" grid to check the import progress.', function () {
                                        panel.find('xtype', 'customer.importhistorygrid')[0].refresh();
                                    });

                                } else {
                                    Ext.Msg.hide();
                                    Ext.Msg.alert('Upload Failed', result.error);
                                }
                                this.ownerCt.find('name', 'startimport')[0].disable();
                                this.ownerCt.find('name', 'previewimport')[0].disable();
                                this.setText('Select a File...');
                                this.enable();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Preview Import',
                        iconCls: 'x-button-import-preview',
                        disabled: true,
                        name: 'previewimport',
                        handler: function () {
                            if (!panel.getImportType()) {
                                Ext.Msg.alert('Select an Import Type', 'You must select an import type first.');
                                return;
                            }
                            var btn = this.ownerCt.find('xtype', 'swfuploadbutton')[0];
                            btn.preview = true;
                            btn.startUpload();
                        }
                    }, {
                        xtype: 'button',
                        text: 'Start Import',
                        iconCls: 'x-button-import-start',
                        disabled: true,
                        name: 'startimport',
                        handler: function () {
                            if (!panel.getImportType()) {
                                Ext.Msg.alert('Select an Import Type', 'You must select an import type first.');
                                return;
                            }
                            var me = this;
                            Ext.Msg.confirm('Apply Import?',
                                'This will run the import <b>and apply the results</b>. Continue?', function (btn) {
                                    if (btn == 'yes') {
                                        var btn = me.ownerCt.find('xtype', 'swfuploadbutton')[0];
                                        btn.preview = false;
                                        btn.startUpload();
                                    }
                                });
                        }
                    }]
                }, {
                    region: 'center',
                    layout: 'fit',
                    margins: '5',
                    id: 'customer.importhistorygrid',
                    xtype: 'customer.importhistorygrid',
                    customerId: this.customerId || Symphony.User.customerId,
                    tbar: {
                        items: ['Import History']
                    }
                }]
            });
            this.callParent(arguments);
        }
    });


    Symphony.Customer.downloadImportResults = function (customerId, importId) {
        var f = document.createElement('iframe');
        f.src = '/services/customer.svc/importresults/customer/' + customerId + '/' + importId + '/';
        f.style.display = 'none';
        document.body.appendChild(f);
    };

    Symphony.Customer.editSessionAssignments = function (userIds) {

        Symphony.CourseAssignment.getSessionAssignmentWindow({
            userIds: userIds
        }).show();
    };

    Symphony.Customer.ImportHistoryGrid = Ext.define('customer.importhistorygrid', {
        alias: 'widget.customer.importhistorygrid',
        extend: 'symphony.searchablegrid',
        customerId: 0,
        initComponent: function () {
            var me = this;
            var url = '/services/customer.svc/importhistory/customer/' + me.customerId;


            var importDateTimeRenderer = function (value, meta, record) {
                var dt = Symphony.parseDate(value);
                if (dt.getFullYear() == 2078) {
                    return '';
                }
                return Symphony.dateTimeRenderer(value, meta, record);
            };

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'center',
                    width: 135,
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    {
                        /*id: 'status',*/ header: 'Status', dataIndex: 'status', width: 100, align: 'left',
                        flex: 1
                    },
                    { header: 'Description', dataIndex: 'description', width: 100, align: 'left' },
                    { header: 'Start Date', dataIndex: 'startDate', renderer: importDateTimeRenderer },
                    { header: 'Last Updated', dataIndex: 'lastUpdated', renderer: importDateTimeRenderer },
                    { header: 'End Date', dataIndex: 'endDate', renderer: importDateTimeRenderer },
                    {
                        header: 'Download', dataIndex: 'resultsFile', sortable: false, width: 75, renderer:
                          function (value, meta, record) {
                              return '<a href="#">Download</a>';
                          }
                    },
                    {
                        header: 'Sessions', dataIndex: 'sessionUserIDs', sortable: false, width: 110, renderer:
                          function (value, meta, record) {
                              if (value) {
                                  return '<a href="#">Edit</a>';
                              }
                          }
                    }
                ]
            });

            Ext.apply(this, {
                autoHeight: false,
                listeners: {
                    rowclick: function (grid) {
                        me.fireEvent('select', grid.getSelectionModel().getSelection()[0].data);
                    },
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.getStore().getAt(rowIndex);  // Get the Record
                        var fieldName = grid.headerCt.getGridColumns()[columnIndex].dataIndex; // Get field name

                        switch (fieldName) {
                            case 'resultsFile':
                                var id = record.get('id');
                                Symphony.Customer.downloadImportResults((me.customerId || Symphony.User.customerId), id);
                                break;
                            case 'sessionUserIDs':
                                if (!record.get(fieldName)) {
                                    return;
                                }

                                var sessionUserIdStrings = record.get(fieldName).split(',');
                                var sessionUserIdInts = [];

                                for (var i = 0; i < sessionUserIdStrings.length; i++) {
                                    var userId = parseInt(sessionUserIdStrings[i], 10);

                                    if (userId > 0) {
                                        sessionUserIdInts.push(userId);
                                    }
                                }

                                if (sessionUserIdInts.length == 0) {
                                    Ext.Msg.alert("No Sessions", "No sessions were handled in this import.");
                                    return;
                                }

                                Symphony.Customer.editSessionAssignments(sessionUserIdInts);
                                break;
                        }

                    }
                },
                url: url,
                colModel: colModel,
                model: 'importHistory'
            });

            this.callParent(arguments);
        }
    });


    Symphony.Customer.ExportPanel = Ext.define('customer.exportpanel', {
        alias: 'widget.customer.exportpanel',
        extend: 'Ext.Panel',
        customerId: 0,
        exportData: function (type) {
            var f = document.createElement('iframe');
            f.src = '/services/customer.svc/export/customer/' + (this.customerId || Symphony.User.customerId) + '/' + type + '/';
            f.style.display = 'none';
            document.body.appendChild(f);
        },
        initComponent: function () {
            Ext.apply(this, {
                layout: 'border',
                border: false,
                items: [{
                    region: 'north',
                    border: false,
                    height: 30,
                    xtype: 'toolbar',
                    items: [{
                        xtype: 'button',
                        text: 'Users',
                        iconCls: 'x-button-user',
                        handler: Ext.bind(this.exportData, this, [Symphony.ImportType.user])
                    }, {
                        xtype: 'button',
                        text: Symphony.Aliases.jobRoles,
                        iconCls: 'x-button-jobrole',
                        handler: Ext.bind(this.exportData, this, [Symphony.ImportType.jobRole])
                    }, {
                        xtype: 'button',
                        text: Symphony.Aliases.locations,
                        iconCls: 'x-button-location',
                        handler: Ext.bind(this.exportData, this, [Symphony.ImportType.location])
                    }, {
                        xtype: 'button',
                        text: Symphony.Aliases.audiences,
                        iconCls: 'x-button-audience',
                        handler: Ext.bind(this.exportData, this, [Symphony.ImportType.audience])
                    }]
                }, {
                    region: 'center',
                    border: false
                }]
            });
            this.callParent(arguments);
        }
    });



})();