﻿(function () {
    Symphony.Network.NetworkRelationshipPanel = Ext.define('network.networkrelationshipeditorpanel', {
        alias: 'widget.network.networkrelationshipeditorpanel',
        extend: 'Ext.form.Panel',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                frame: false,
                border: false,
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    pack: 'start',
                    padding: '5 0 0 0'
                },
                defaults: {
                    border: false,
                    margin: '5 5 5 5'
                },
                items: [{
                    xtype: 'fieldset',
                    height: 20,
                    layout: 'form',
                    items: {
                        xtype: 'panel',
                        border: false,
                        layout: {
                            type: 'hbox',
                            align: 'stretch',
                            pack: 'start'
                        },
                        defaults: {
                            labelWidth: 180
                        },
                        items: [{
                            xtype: 'symphony.pagedcombobox',
                            name: 'sourceCustomerId',
                            fieldLabel: 'Customer',
                            displayField: 'name',
                            valueField: 'id',
                            url: '/services/customer.svc/customers/',
                            model: 'customer',
                            matchFieldWidth: false,
                            queryMode: 'remote',
                            bindingName: 'sourceCustomerName',
                            flex: 1,
                            padding: '0 5 0 0',
                            disabled: this.record.get('detailId') > 0,
                            listeners: {
                                select: function (cmb, records, e) {
                                    var r = records[0];
                                    me.updateRelationship([
                                        { field: 'sourceCustomerName', val: r.get('name') },
                                        { field: 'sourceCustomerId', val: r.get('id') }
                                    ]);
                                }
                            }
                        }, {
                            xtype: 'symphony.pagedcombobox',
                            name: 'destinationCustomerId',
                            fieldLabel: 'Customer',
                            displayField: 'name',
                            valueField: 'id',
                            url: '/services/customer.svc/customers/',
                            model: 'customer',
                            matchFieldWidth: false,
                            queryMode: 'remote',
                            bindingName: 'destinationCustomerName',
                            flex: 1,
                            padding: '0 0 0 5',
                            disabled: this.record.get('detailId') > 0,
                            listeners: {
                                select: function (cmb, records, e) {
                                    var r = records[0];
                                    me.updateRelationship([
                                        { field: 'destinationCustomerName', val: r.get('name') },
                                        { field: 'destinationCustomerId', val: r.get('id') }
                                    ]);
                                }
                            }
                        }]
                    }
                }, {
                    height: 80,
                    xtype: 'fieldset',
                    title: 'Resources',
                    border: true,
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    defaults: {
                        layout: 'form',
                        flex: 1,
                        border: false
                    },
                    items: [{
                        cls: 'x-panel-transparent',
                        items: [{
                            xtype: 'checkbox',
                            fieldLabel: '',
                            hideEmptyLabel: true,
                            boxLabel: 'Allow Training Program Sharing',
                            name: 'allowTrainingProgramSharing',
                            listeners: {
                                change: function (chk, newValue, oldVal, e) {
                                    me.updateRelationship([{ field: chk.name, val: newValue }]);
                                }
                            }
                        }, {
                            xtype: 'checkbox',
                            fieldLabel: '',
                            hideEmptyLabel: true,
                            boxLabel: 'Allow Reporting',
                            name: 'allowReporting',
                            listeners: {
                                change: function (chk, newValue, oldVal, e) {
                                    me.updateRelationship([{ field: chk.name, val: newValue }]);
                                }
                            }
                        }]
                    }, {
                        cls: 'x-panel-transparent',
                        items: [{
                            xtype: 'checkbox',
                            fieldLabel: '',
                            hideEmptyLabel: true,
                            boxLabel: 'Allow Library Sharing',
                            name: 'allowLibraries',
                            listeners: {
                                change: function (chk, newValue, oldVal, e) {
                                    me.updateRelationship([{ field: chk.name, val: newValue }]);
                                }
                            }
                        }]
                    }]
                }, {
                    xtype: 'tabpanel',
                    flex: 1,
                    margin: '0',
                    plain: true,
                    defaults: {
                        border: false
                    },
                    items: [{
                        title: 'Training Programs',
                        xtype: 'network.sharedtrainingprogramsgrid',
                        customerId: me.record.get('sourceCustomerId'),
                        listeners: {
                            persistedselectionchange: function (grid, selected, e) {
                                var selections = grid.getSelections();
                                var selectedIds = [];
                                for (var i = 0; i < selections.length; i++) {
                                    selectedIds.push(selections[i].get('id'));
                                }

                                me.updateRelationship([{ field: 'trainingProgramIds', val: selectedIds }]);
                            }
                        }
                    }, {
                        title: 'Libraries',
                        xtype: 'network.sharedlibrariesgrid',
                        customerId: me.record.get('sourceCustomerId'),
                        entityType: Symphony.EntityType.library,
                        isNetworkSharedEntityGrid: true,
                        listeners: {
                            persistedselectionchange: function (grid, selected, e) {
                                var grids = me.query('grid[isNetworkSharedEntityGrid]');
                                var networkSharedEntities = [];
                                for (var i = 0; i < grids.length; i++) {
                                    var selections = grids[i].getSelections();
                                    for (var j = 0; j < selections.length; j++) {
                                        networkSharedEntities.push({
                                            entityId: selections[j].get('id'),
                                            entityTypeId: grids[i].entityType,
                                            customerNetworkDetailId: me.record.get('detailId')
                                        });
                                    }
                                }

                                me.updateRelationship([{ field: 'networkSharedEntities', val: networkSharedEntities }]);
                            }
                        }
                    }]
                }],
                listeners: {
                    afterrender: {
                        fn: function () {
                            me.loadData();
                        },
                        single: true
                    }
                }
            });

            this.callParent(arguments);
        },
        loadData: function () {
            this.bindValues(this.record.data);

            var tpGrid = this.query('[xtype=network.sharedtrainingprogramsgrid]')[0];
            var libGrid = this.query('[xtype=network.sharedlibrariesgrid]')[0];

            var networkSharedEntities = this.record.get('networkSharedEntities');
            if (networkSharedEntities && networkSharedEntities.length) {
                var entityIds = Ext.Array.pluck(Ext.Array.filter(networkSharedEntities, function (item) {
                    return item.entityTypeId == Symphony.EntityType.library
                }), 'entityId');

                libGrid.setSelections(entityIds);
            }

            // TODO: Make training programs behave as network shared entities.
            var networkSharedTrainingPrograms = this.record.get('trainingProgramIds');
            if (networkSharedTrainingPrograms && networkSharedTrainingPrograms.length) {
                tpGrid.setSelections(networkSharedTrainingPrograms);
            }
        },
        updateData: function(data, record) {
            this.detailId = data.detailId;
            var combos = this.query('[xtype=symphony.pagedcombobox]');
            for (var i = 0; i < combos.length; i++) {
                combos[i].setDisabled(true);
            }

            if (record) {
                this.record = record;
            }
        },
        updateRelationship: function (data) {
            for (var i = 0; i < data.length; i++) {
                this.record.set(data[i].field, data[i].val);
            }

            this.fireEvent('relationshipChanged', this.record);

            var tpGrid = this.query('[xtype=network.sharedtrainingprogramsgrid]')[0];
            var libGrid = this.query('[xtype=network.sharedlibrariesgrid]')[0];

            if (tpGrid.customerId != this.record.get('sourceCustomerId')) {
                tpGrid.setCustomerId(this.record.get('sourceCustomerId'));
            }
            if (libGrid.customerId != this.record.get('sourceCustomerId')) {
                libGrid.setCustomerId(this.record.get('sourceCustomerId'));
            }

            this.setTitle(this.record.get('sourceCustomerName') + ' to ' + this.record.get('destinationCustomerName'));
        }
    })
})();
