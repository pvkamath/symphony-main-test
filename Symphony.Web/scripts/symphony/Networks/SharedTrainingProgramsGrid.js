﻿(function () {
    Symphony.Network.SharedTrainingProgramsGrid = Ext.define('network.sharedtrainingprogramsgrid', {
        alias: 'widget.network.sharedtrainingprogramsgrid',
        extend: 'symphony.searchablegrid',
        customerId: 0,
        data: [],
        tpdata: [],
        initComponent: function () {
            var me = this;
            this.urlTemplate = '/services/courseassignment.svc/trainingproholdbycustomer/{0}/';
            this.url = String.format(this.urlTemplate, me.customerId);
            
            var sm = new Ext.selection.CheckboxModel({
                checkOnly: true,
                id: 'checker',
                header: ' ',
                listeners: {
                    selectionchange: function (sm, selected, e) {
                        me.fireEvent('persistedselectionchange', me, selected, e);
                    }
                }
            });

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { header: 'Program Name', dataIndex: 'name', minWidth: 140, align: 'left', flex: 1 },
                    { header: 'New Hire', dataIndex: 'isNewHire', renderer: Symphony.checkRenderer, width: 64 },
                    { header: 'Enabled', dataIndex: 'isLive', renderer: Symphony.checkRenderer, width: 64 },
                    { header: 'End Date', dataIndex: 'endDate', renderer: Symphony.pastDefaultDateRenderer, width: 64 },
                    { header: 'Sku', dataIndex: 'sku', width: 64 }
                ]
            });

            Ext.apply(this, {
                selModel: sm,
                colModel: colModel,
                data: this.data,
                model: 'trainingProgram',
                plugins: [{ ptype: 'pagingselectpersist' }],
                listeners: {
                    afterrender: function () {
                        if (me.tpdata && me.tpdata.length) {
                            me.setSelections(me.tpdata);
                        }
                    }
                }
            });

            this.callParent();
        },
        clearSelections: function () {
            this.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
        },
        getSelections: function () {
            var selection = this.getPlugin('pagingSelectionPersistence').getPersistedSelection();
            var selectedTps = [];

            for (var i = 0; i < selection.length; i++) {
                var selected = selection[i];
                selectedTps.push(selected);
            }

            return selectedTps;
        },
        setSelections: function (data) {
            this.getPlugin('pagingSelectionPersistence').setSelections(data);
        },
        setCustomerId: function (id) {
            this.customerId = id;
            this.updateStore(String.format(this.urlTemplate, id));
        }
    });
})();