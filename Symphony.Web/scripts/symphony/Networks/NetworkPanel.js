(function () {
    Symphony.Network.NetworkPanel = Ext.define('network.networkpanel', {
        alias: 'widget.network.networkpanel',
        extend: 'Ext.Panel',
        networkId: 0,
        record: null,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                border: false,
                frame: false,
                layout: 'fit',
                bodyPadding: 10,
                defaults: {
                    border: false
                },
                tbar: [{
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'x-button-save',
                    handler: function () {
                        me.save();
                    }
                }, {
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        me.fireEvent('cancel');
                    }
                }],
                items: [{
                    xtype: 'panel',
                    layout: 'border',
                    frame: true,
                    border: false,
                    items: [{
                        region: 'north',
                        height: 50,
                        xtype: 'form',
                        cls: 'x-panel-transparent',
                        name: 'network',
                        border: false,
                        frame: false,
                        layout: {
                            type: 'vbox',
                            align: 'stretch',
                            pack: 'start'
                        },
                        items: [{
                            xtype: 'fieldset',
                            title: 'Network Info',
                            defaults: { xtype: 'textfield' },
                            cls: 'x-panel-transparent',
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start',
                                padding: '0 0 7 2'
                            },
                            items: [{
                                flex: 1,
                                name: 'name',
                                fieldLabel: 'Name',
                                allowBlank: false,
                                padding: '0 5 0 0'
                            }, {
                                flex: 1,
                                name: 'codeName',
                                fieldLabel: 'Code Name',
                                allowBlank: false,
                                padding: '0 0 0 5'
                            }]
                        }]
                    }, {
                        region: 'center',
                        xtype: 'panel',
                        title: 'Relationships',
                        layout: 'fit',
                        items: [{
                            name: 'networkrelationshippanel',
                            xtype: 'network.networkrelationshippanel',
                            allRelationshipInfo: []
                        }]
                    }]
                }],
                listeners: {
                    afterrender: function () {
                        me.loadData(me.record);
                    },
                    addRelationshipElement: function (relationship) {
                        var locgrid = me.find('name', 'networkrelationshippanel')[0];
                        var actRec = Ext.create('networkCustomerRelationship', relationship);
                        locgrid.getStore().add(actRec);
                    }
                }
            });

            this.callParent(arguments);
        },
        getSharedTrainingPrograms: function (sourceCustomerId, destinationCustomerId, relationList) {
            for (var i = relationList.length - 1; i >= 0; i--) {
                if (relationList[i].sourceCustomerId == sourceCustomerId && relationList[i].sourceCustomerId == sourceCustomerId) {
                    return relationList[i].trainingProgramIds;
                }
            }
            return [];
        },
        save: function () {
            // saves a network
            var me = this;
            var form = me.find('name', 'network')[0];
            var relationshipPanel = me.query('[xtype=network.networkrelationshippanel]')[0];

            if (!form.isValid()) {
                Ext.Msg.alert('Errors Detected', 'Your information has some errors. Please correct and re-save');
                return;
            }

            var network = form.getValues();
            
            network.customerNetworkDetails = relationshipPanel.getRelationshipData();

            if (network.customerNetworkDetails === false) {
                return;
            }

            network.customerNetwork = [];

            Symphony.Ajax.request({
                url: '/services/network.svc/network/' + me.networkId,
                jsonData: network,
                success: function (result) {
                    var record = Ext.create('network', result.data);

                    me.loadData(record);

                    me.networkId = record.get('id');
                    me.setTitle(result.data.name);

                    me.fireEvent('save');
                }
            });
        },
        loadData: function (record) {

            var me = this;

            if (record && record.data) {
                this.find('name', 'network')[0].bindValues(record.data);
                this.networkId = record.get('id');
                
                var networkRelationshipPanel = me.find('name', 'networkrelationshippanel')[0];
                if (!record.get('customerNetworkDetails')) {

                    Symphony.Ajax.request({
                        method: 'GET',
                        url: '/services/network.svc/network/' + me.networkId,
                        success: function (result) {
                            networkRelationshipPanel.setData(result.data.customerNetworkDetails);
                        }
                    })
                } else {
                    networkRelationshipPanel.setData(record.get('customerNetworkDetails'));
                }
            }
        }
    });
}(window));
