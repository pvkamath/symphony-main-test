﻿(function () {
    Symphony.Network.NetworkGrid = Ext.define('network.networkgrid', {
        alias: 'widget.network.networkgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {

            var me = this;
            var url = '/services/network.svc/networks/';

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'name',*/
                    header: 'Name',
                    dataIndex: 'name',
                    align: 'left',
                    flex: 0.6
                }, {
                    /*id: 'codeName',*/
                    dataIndex: 'codeName',
                    header: 'Code',
                    align: 'left',
                    flex: 0.4
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                colModel: colModel,
                url: url,
                model: 'network',
                viewConfig: {
                    forceFit: true
                },
                tbar: {
                    items: [{
                        xtype: 'button',
                        iconCls: 'x-button-add',
                        text: 'Add Network',
                        handler: function () {
                            me.fireEvent('addNetwork');
                        }
                    }]
                }
            });

            this.callParent(arguments);
        }
    });

})();