﻿(function () {
    Symphony.Network.SharedTrainingProgramsGrid = Ext.define('network.sharedlibrariesgrid', {
        alias: 'widget.network.sharedlibrariesgrid',
        extend: 'symphony.searchablegrid',
        customerId: 0,
        data: [],
        libdata: [],
        initComponent: function () {
            var me = this;
            this.urlTemplate = '/services/library.svc/libraries/customer/{0}/';
            this.url = String.format(this.urlTemplate, me.customerId);

            var sm = new Ext.selection.CheckboxModel({
                checkOnly: true,
                id: 'checker',
                header: ' ',
                listeners: {
                    selectionchange: function (sm, selected, e) {
                        me.fireEvent('persistedselectionchange', me, selected, e);
                    }
                }
            });

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { header: 'Type', dataIndex: 'displayLibraryItemType', align: 'center', renderer: Symphony.Renderer.displayIconRenderer, width: 40 },
                    { header: 'Name', dataIndex: 'name', align: 'left', flex: 1 },
                    { header: 'Created', dataIndex: 'displayCreatedOn', renderer: Symphony.Renderer.displayDateRenderer, align: 'center', width: 60 },
                    { header: 'Registrations', dataIndex: 'registrationCount', align: 'right', width: 80 },
                    { header: 'Items', dataIndex: 'itemCount', width: 50, align: 'right', width: 40 }
                ]
            });

            Ext.apply(this, {
                selModel: sm,
                colModel: colModel,
                data: this.data,
                model: 'library',
                plugins: [{ ptype: 'pagingselectpersist' }],
                listeners: {
                    afterrender: function () {
                        if (me.libdata && me.libdata.length) {
                            me.setSelections(me.libdata);
                        }
                    }
                }
            });

            this.callParent();
        },
        clearSelections: function () {
            this.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
        },
        getSelections: function () {
            var selection = this.getPlugin('pagingSelectionPersistence').getPersistedSelection();
            var selectedLibs = [];

            for (var i = 0; i < selection.length; i++) {
                var selected = selection[i];
                selectedLibs.push(selected);
            }

            return selectedLibs;
        },
        setSelections: function (data) {
            this.getPlugin('pagingSelectionPersistence').setSelections(data);
        },
        setCustomerId: function (id) {
            this.customerId = id;
            this.updateStore(String.format(this.urlTemplate, id));
        }
    });
})();