﻿(function () {
    Symphony.Network.NetworkTab = Ext.define('network.networktab', {
        alias: 'widget.network.networktab',
        extend: 'Ext.Panel',
        allcustomers: [],
        initComponent: function () {
            var me = this;
            //me.loadAllCustomersInfo();
            Ext.apply(me, {
                layout: 'border',
                border: false,
                items: [{
                    split: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    xtype: 'network.networkgrid',
                    listeners: {
                        rowclick: function (grid, rowIndex, e) {
                            var record = grid.getStore().getAt(rowIndex);
                            me.addPanel(record.get('id'), record.get('name'), record);
                        },
                        addNetwork: function () {
                            me.addPanel(0, 'New Network');
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    enableTabScroll: true,
                    xtype: 'tabpanel',
                    ref: 'tabPanel'
                }],
                addPanel: function (networkId, networkName, record) {
                    var me = this;

                    var tabPanel = me.tabPanel;
                    var found = tabPanel.find('networkId', networkId);

                    if (!found.length) {
                        var panel = tabPanel.add({
                            xtype: 'network.networkpanel',
                            closable: true,
                            activate: true,
                            record: record,
                            title: networkName,
                            licenseId: networkId,
                            customerDetails: me.allcustomers,
                            border: false,
                            listeners: {
                                save: function () {
                                    me.find('xtype', 'network.networkgrid')[0].refresh();
                                },
                                cancel: function () {
                                    tabPanel.remove(panel);
                                }
                            }
                        });
                        panel.show();
                    } else {
                        me.tabPanel.setActiveTab(found[0].id);
                    }
                    tabPanel.doLayout();
                }
            });
            Symphony.Network.NetworkTab.superclass.initComponent.apply(me, arguments);
        }
    });

}());
