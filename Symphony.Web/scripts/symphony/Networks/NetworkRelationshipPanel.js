﻿(function () {
    Symphony.Network.NetworkRelationshipPanel = Ext.define('network.networkrelationshippanel', {
        alias: 'widget.network.networkrelationshippanel',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;

            this.relationshipData = [];

            Ext.apply(this, {
                layout: 'border',
                border: false,
                frame: false,
                tbar: {
                    items: [{
                        xtype: 'button',
                        text: 'Add Relationship',
                        iconCls: 'x-button-add',
                        handler: Ext.bind(me.addNewRelationship, me)
                    }]
                },
                items: [{
                    region: 'west',
                    xtype: 'network.networkrelationshipgrid',
                    width: 250,
                    split: true,
                    border: false,
                    selModel: Ext.create('Ext.selection.RowModel', {
                        listeners: {
                            selectionchange: function (sm, records, e) {
                                if (records && records.length) {
                                    var record = records[0];
                                    me.addRelationshipEditorPanel(record);
                                }
                            }
                        }
                    }),
                    listeners: {
                        afterRender: {
                            fn: function () {
                                this.setData(me.relationshipData);
                            },
                            single: true
                        },
                        deleterelationship: function (record) {
                            me.removeRelationshipEditorPanel(record);
                        }
                    }
                }, {
                    region: 'center',
                    xtype: 'panel',
                    layout: 'fit',
                    border: false,
                    items: [{
                        xtype: 'tabpanel',
                        name: 'relationshipTabPanel',
                        border: false
                    }]
                }]
            });

            this.callParent(arguments);
        },
        getGrid: function () {
            var grid = this.query('[xtype=network.networkrelationshipgrid]')[0];
            return grid;
        },
        getStore: function () {
            return this.getGrid().getStore();
        },
        setData: function (data) {
            this.relationshipData = data;
            var store = this.getStore(),
                oldRecords;

            if (store) {
                oldRecords = store.getRange();
                store.removeAll();
                store.loadData(data);
            }

            var clientIdMap = {};
            for (var i = 0; i < data.length; i++) {
                if (data[i].clientId <= -1 || data[i].clientId >= 1) {
                    clientIdMap[data[i].clientId] = data[i];
                }
            }

            var tabPanel = this.query('[name=relationshipTabPanel]')[0];
            var panels = tabPanel.query('[xtype=network.networkrelationshipeditorpanel]');

            for (var j = 0; j < panels.length; j++) {
                var newData = clientIdMap[panels[j].detailId];

                if (newData) {
                    var newIndex = store.find('detailId', newData.detailId),
                        newRecord;

                    if (newIndex >= 0) {
                        newRecord = store.getAt(newIndex);
                    } else {
                        console.error('Could not find new record for relationship. Reloading this panel before continuing is recommended.');
                    }

                    panels[j].updateData(newData, newRecord);
                }
            }

        },
        getRelationshipData: function () {
            return this.getGrid().getRelationshipData();
        },
        addNewRelationship: function() {
            this.getGrid().addNewRelationship();
        },
        relationshipChanged: function(record) {
            var grid = this.getGrid(),
                view = grid.getView(),
                store = grid.getStore(),
                index;

            index = store.indexOf(record);
            if (index < 0) {
                Ext.Error.raise('Could not find relationship in store. Id may have been changed while saving a new relationship.');
            }

            view.refreshNode(index);
        },
        removeRelationshipEditorPanel: function (record) {
            var tabPanel = this.query('[name=relationshipTabPanel]')[0];
            if (record && record.get('detailId')) {
                var panel = tabPanel.query('[detailId=' + record.get('detailId') + ']');
                if (panel.length) {
                    tabPanel.remove(panel[0]);
                }
            }
        },
        addRelationshipEditorPanel: function (record) {
            if (!record) {
                return;
            }

            var me = this;
            var tabPanel = me.query('[name=relationshipTabPanel]')[0];
            var id = record.get('detailId');
            var found = tabPanel.query('[detailId=' + id + ']');

            if (!found.length) {
                var panel = tabPanel.add({
                    xtype: 'network.networkrelationshipeditorpanel',
                    closable: true,
                    activate: true,
                    record: record,
                    title: record.get('sourceCustomerName') + ' to ' + record.get('destinationCustomerName'),
                    detailId: id,
                    border: false,
                    listeners: {
                        relationshipChanged: Ext.bind(me.relationshipChanged, me)
                    }
                });
                panel.show();
            } else {
                tabPanel.setActiveTab(found[0].id);
            }
            tabPanel.doLayout();
        }
    })
})();
