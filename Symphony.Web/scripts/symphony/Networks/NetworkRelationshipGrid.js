﻿(function () {
    Symphony.Network.NetworkRelationshipGrid = Ext.define('network.networkrelationshipgrid', {
        alias: 'widget.network.networkrelationshipgrid',
        extend: 'symphony.localgrid',
        model: 'networkCustomerRelationship',
        cls: 'network-relationship-grid',
        columns: [{
            text: 'Relationship',
            flex: 1,
            xtype: 'templatecolumn',
            tpl: new Ext.XTemplate(
                '<p>',
                    '<strong>Source:</strong><br/>',
                    '{sourceCustomerName}',
                '</p>',
                '<p>',
                    '<strong>Destination:</strong><br/>',
                    '{destinationCustomerName}',
                '</p>'
            )
        }, {
            text: 'Resources',
            xtype: 'actioncolumn',
            width: 75,
            tdCls: 'hide-disabled-action-column vertical-align-action-column',
            align: 'right',
            items: [{
                icon: '/images/script.png',
                iconCls: 'x-action-col-icon-margin',
                tooltip: 'Shares Reports',
                isDisabled: function(v, rowIndex, colIndex, item, record) {
                    return !record.get('allowReporting');
                }
            }, {
                icon: '/images/book_open.png',
                iconCls: 'x-action-col-icon-margin',
                tooltip: 'Shares Libraries',
                isDisabled: function (v, rowIndex, colIndex, item, record) {
                    return !record.get('allowLibraries');
                }
            }, {
                icon: '/images/application_cascade.png',
                iconCls: 'x-action-col-icon-margin',
                tooltip: 'Shares Training Programs',
                isDisabled: function (v, rowIndex, colIndex, item, record) {
                    return !record.get('allowTrainingProgramSharing');
                }
            }]
        }],
        initComponent: function () {
            var me = this;

            this.newItemId = 0;

            this.columns = [{
                text: '',
                width: 40,
                xtype: 'actioncolumn',
                align: 'center',
                tdCls: 'vertical-align-action-column',
                items: [{
                    icon: '/images/bin.png',
                    iconCls: 'x-action-col-icon-margin',
                    tooltip: 'Delete Relationship',
                    handler: function (view, rowIndex, colIndex, item, e, record) {
                        e.stopEvent();
                        Ext.Msg.confirm('Delete Relationship?', 'Are you sure you want to delete the relationship between ' + record.get('sourceCustomerName') + ' and ' + record.get('destinationCustomerName') + '?',
                            function (btn) {
                                if (btn == "yes") {
                                    me.store.remove(record);
                                    me.fireEvent('deleterelationship', record);
                                }
                            });
                    }
                }]
            }].concat(this.columns);

            this.callParent(arguments);
        },
        getRelationshipData: function() {
            var records = this.getStore().getRange();

            for (var i = 0; i < records.length; i++) {
                var isError = false;
                var errorMessage = "";
                
                if (!(records[i].get('sourceCustomerId') >= 1 && records[i].get('destinationCustomerId') >= 1)) {
                    isError = true;
                    errorMessage = 'Source customer and destination customer must be set.';
                } else if (records[i].get('sourceCustomerId') == records[i].get('destinationCustomerId')) {
                    isError = true;
                    errorMessage = 'Source customer and destination customer cannot be the same customer.';
                }

                if (isError) {
                    Ext.Msg.alert('Error', errorMessage);
                    this.getSelectionModel().select(records[i]);
                    return false;
                }
            }

            return Ext.Array.pluck(records, 'data');
        },
        addNewRelationship: function () {
            this.newItemId--;

            var relationship = Ext.create(this.model, {
                sourceCustomerName: '-',
                destinationCustomerName: '-',
                detailId: this.newItemId,
                sourceCustomerId: 0,
                destinationCustomerId: 0,
                networkId: 0,
                trainingProgramIds: [],
                networkSharedEntities: [],
                allowLibraries: false,
                allowReporting: false,
                allowTrainingProgramSharing: false
            });

            this.store.add(relationship);

            this.getSelectionModel().select(relationship);

        }
    })
})();
