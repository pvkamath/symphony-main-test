﻿Symphony.Categories.getCategoryEditorWindow = function (categoryType, afterSelect, categoryId, showRelative) {
    var relativeCategoryId = showRelative ? categoryId : null;

     return new Ext.Window({
         title: 'Select a Category',
         width: 700,
         height: 500,
         layout: 'border',
         border: false,
         modal: true,
         items: [{
             region: 'center',
             xtype: 'categories.categoryeditorpanel',
             selectedCategoryId: categoryId,
             categoryType: categoryType,
             relativeCategoryId: relativeCategoryId,
             readOnly: showRelative,
             afterSelect: function (node) {
                 afterSelect(node);
             }
         }]
     });
}
Symphony.Categories.CategoryEditorPanel = Ext.define('categories.categoryeditorpanel', {

    alias: 'widget.categories.categoryeditorpanel',

    extend: 'Ext.Panel',
    categoryType: '',
    selectedNode: null,
    selectedCategoryId: null,
    relativeCategoryId: null,
    readOnly: false,
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            layout: 'border',
            items: [{
                border: false,
                region: 'west',
                xtype: 'categories.categoryeditortree',
                categoryType: me.categoryType,
                iconCls: me.iconCls,
                title: "Categories",
                customerId: me.customerId,
                relativeCategoryId: me.relativeCategoryId,
                split: true,
                width: 300,
                ref: 'categorytree',
                readOnly: me.readOnly,
                listeners: {
                    itemdblclick: function (tree, record, item, index, e) {
                        if (typeof (me.afterSelect) === 'function') {
                            me.afterSelect(record);
                        }
                    },
                    itemclick: function (tree, record, item, index, e, opts) {
                        if (record && record.get('id') >= 0) {
                            me.selectedNode = record;
                            me.selectedCategoryId = record.get('categoryId');
                            me.addPanel(record.get('id'), record.get('name'), record);
                        }
                    },
                    del: function (node) {
                        var found = me.mainpanel.findBy(function (component, container) {
                            if (component.recordId) {
                                if (component.recordId == node.attributes.record.get('id')) {
                                    return true;
                                }
                            }
                            return false;
                        });

                        if (found.length) {
                            me.mainpanel.remove(found[0]);
                        }
                    },
                    startEdit: function () {
                        me.selectBtn.setDisabled(true);
                    },
                    endEdit: function () {
                        me.selectBtn.setDisabled(false);
                    },
                    append: function (tree, parent, node, index)
                    {
                        if (node.attributes.record && node.attributes.record.get('id') && node.attributes.record.get('id') == me.selectedCategoryId) {
                            var selectNodeFunction = function () {
                                me.categorytree.selectPath(node.getPath());
                                me.categorytree.fireEvent('click', node);
                            };

                            if (node.rendered) {
                                selectNodeFunction();
                            } else {
                                node.addListener('render', selectNodeFunction);
                            }
                        }
                    },
                    afterrender: function (cmp) {
                        me.categorytree = cmp;
                    }
                }
            }, {
                border: false,
                region: 'center',
                region: 'center',
                enableTabScroll: true,
                xtype: 'tabpanel',
                name: 'mainpanel',
                listeners: {
                    afterrender: function (cmp) {
                        me.mainpanel = cmp;
                    }
                }
            }],
            bbar: {
                items: ['->', {
                    xtype: 'button',
                    iconCls: 'x-button-save',
                    text: 'Select',
                    listeners: {
                        afterrender: function (cmp) {
                            me.selectBtn = cmp;
                        }
                    },
                    handler: function () {
                        var node = me.selectedNode;
                        if (node) {
                            if (node.get('id') > 0) {
                                if (typeof (me.afterSelect) === 'function') {
                                    me.afterSelect(node);
                                }
                            } else {
                                Ext.Msg.alert("Save the Category", "Please save the category first.");
                            }
                        } else {
                            Ext.Msg.alert("Select a Category", "Please select a category from the tree.");
                        }
                    }
                }]
            }
        });
        this.callParent(arguments);
    }, 
    addPanel: function (recordId, name, record) {
        var me = this;

        var found = me.mainpanel.queryBy(function (cmp) {
            if (cmp.recordId && cmp.recordId == recordId) {
                return true;
            }
            return false;
        });

        if (found.length) {
            me.mainpanel.layout.setActiveItem(found[0].id);
            me.mainpanel.doLayout();
            found[0].show();
        } else {
            var args = { recordId: recordId };
            this.fireEvent('beforeadd', args);

            var panel = Ext.create('categories.categoryform', {
                categoryType: me.categoryType,
                recordId: recordId,
                record: record,
                title: name,
                node: record,
                readOnly: me.readOnly,
                closable: true,
                listeners: {
                    save: function (data) {
                        var leaf = record.get('leaf'),
                            checked = record.get('checked');

                        me.selectedNode = record;

                        me.selectedNode.data = args.data;
                        me.selectedNode.set('leaf', leaf);
                        me.selectedNode.set('checked', checked);
                        me.selectedNode.commit();
                    },
                    activate: function () {
                        me.selectedNode = record;
                        me.selectedCategoryId = record.get('id');
                    }
                }
            });
            me.mainpanel.add(panel);
            me.mainpanel.doLayout();
            panel.show();

            me.mainpanel.layout.setActiveItem(panel.id);

            this.fireEvent('add', args);
        }
    }
});
