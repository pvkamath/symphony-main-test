﻿Symphony.Categories.CategoryForm = Ext.define('categories.categoryform', {
    alias: 'widget.categories.categoryform',
    extend: 'Ext.Panel',
    categoryType: '',
    record: null,
    readOnly: false,
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            tbar: {
                xtype: 'symphony.savecancelbar',
                disabled: me.readOnly,
                listeners: {
                    save: function () {
                        var form = me.find('xtype', 'form')[0];

                        if (!form.isValid()) { return false; }

                        var category = form.getValues();
                        Ext.apply(me.record.data, category);
                        if (!me.record.data.maxCourseWork) {
                            delete me.record.data.maxCourseWork;
                        }

                        if (me.record.data.parentId == "root") {
                            me.record.data.parentId = 0;
                        }

                        Symphony.Ajax.request({
                            url: '/services/category.svc/categories/{0}/{1}'.format(me.categoryType, me.record.data.id),
                            method: 'POST',
                            jsonData: me.record.data,
                            success: function (args) {
                                var leaf = record.get('leaf'),
                                    checked = record.get('checked');

                                me.record.data = args.data;
                                me.record.set('leaf', left);
                                me.record.set('checked', checked);
                                me.record.commit();

                                form.bindValues(args.data);
                                me.setTitle(args.data.name);

                                me.fireEvent('save', me.record.data);
                            }
                        });
                    },
                    cancel: function () { me.ownerCt.remove(me); }
                }
            },
            frame: false,
            bodyCssClass: 'x-panel-mc',
            border: false,
            bodyStyle: 'padding: 10px',
            items: [{
                border: false,
                
                items: [{
                    border: false,
                    xtype: 'form',
                    frame: false,
                    labelWidth: 125,
                    defaults: { anchor: '100%', xtype: 'textfield' },
                    listeners: {
                        afterrender: function (cmp) {
                            cmp.bindValues(me.record.data);
                        }
                    },
                    items: [{
                        name: 'name',
                        fieldLabel: 'Name',
                        allowBlank: false
                    }, {
                        name: 'description',
                        fieldLabel: 'Description',
                        xtype: 'textarea',
                        allowBlank: true
                    }, {
                        name: 'maxCourseWork',
                        cls: Symphony.Modules.getModuleClass(Symphony.Modules.AdvancedTrainingProgram),
                        fieldLabel: 'Max Course Work',
                        help: {
                            title: 'Max Course Work',
                            text: 'The maximum number of hours allowed per day for online course work. If left blank, the value will inherit from the company setting.'
                        },
                        allowBlank: true,
                        xtype: 'numberfield',
                        minValue: 1
                    }]
                }]
            }]
        });
        this.callParent(arguments);
    }
});
