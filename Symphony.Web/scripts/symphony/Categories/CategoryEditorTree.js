﻿(function () {
    Symphony.Categories.CategoryEditorTree = Ext.define('categories.categoryeditortree', {
        alias: 'widget.categories.categoryeditortree',
        extend: 'symphony.tabletree',
        categoryType: '',
        relativeCategoryId: null,
        afterSelect: null,
        readOnly: false,
        initComponent: function () {
            var me = this;

            var categoryUrl = me.relativeCategoryId > 0 ?
                '/services/category.svc/categories/{0}/relativesof/{1}/'.format(me.categoryType, me.relativeCategoryId) :
                '/services/category.svc/categories/{0}/'.format(me.categoryType);
            
            Ext.apply(this, {
                plugins: [{
                    ptype: 'cellediting',
                    clicksToEdit: 2,
                    listeners: {
                        beforeedit: Ext.bind(this.onTreeBeforeEdit, this),
                        canceledit: Ext.bind(this.onTreeCancelEdit, this),
                        edit: Ext.bind(this.onTreeAfterEdit, this)
                    }
                }],
                tbar: {
                    items: [{
                        xtype: 'button',
                        text: 'Add',
                        iconCls: 'x-button-add',
                        ref: '../addBtn',
                        disabled: me.readOnly,
                        listeners: {
                            afterrender: function (cmp) {
                                me.addBtn = cmp;
                            }
                        },
                        handler: function () {
                            var node = me.getSelectionModel().getSelection()[0];

                            if (!node) {
                                node = me.getRootNode();
                            }

                            var categoryNode = Ext.create('category', {
                                name: "New Category",
                                description: " ",
                                id: 0,
                                parentId: node.isRoot ? 0 : node.get('id'),
                                expanded: true,
                                expandable: true,
                                leaf: true
                            });

                            node.appendChild(categoryNode);
                            node.set('leaf', false);
                            node.expand();
                            me.getSelectionModel().select(categoryNode);
                            me.editingPlugin.startEdit(categoryNode, 0);
                            
                            me.addBtn.setDisabled(true);
                        }
                    }, {
                        xtype: 'button',
                        text: 'Delete',
                        iconCls: 'x-button-delete',
                        ref: '../deleteBtn',
                        disabled: me.readOnly,
                        handler: function () {
                            var node = me.getSelectionModel().getSelection()[0];

                            if (node) {
                                Ext.Msg.confirm('Delete Category?', 'Are you sure you want to delete this category?', function (btn) {
                                    if (btn === 'yes') {
                                        Symphony.Ajax.request({
                                            method: 'DELETE',
                                            url: '/services/category.svc/categories/{0}/{1}'.format(me.categoryType, node.attributes.record.get('id')),
                                            success: function () {
                                                node.remove();
                                                me.fireEvent('del', node);
                                            }
                                        });
                                    }
                                })
                            }
                        }
                    }]
                },
                url: categoryUrl,
                model: 'category',
                rootVisible: true,
                rootText: 'Categories',
                columns: [
                    {
                        xtype: 'treecolumn',
                        text: 'Category',
                        dataIndex: 'name',
                        flex: 1,
                        sortable: true,
                        editor: { xtype: 'textfield', allowBlank: false }
                    }
                ]
            });
            this.callParent(arguments);
        },
        onTreeBeforeEdit: function (editor, element, value) {
            this.fireEvent('startEdit');
            return (element.record != this.getRootNode());
        },
        onTreeAfterEdit: function (editor, e, opts) {
            var me = this;
            var record = e.record;

            if (!record.data.maxCourseWork) {
                delete record.data.maxCourseWork;
            }

            if (record.data.parentId == 'root') {
                record.data.parentId = 0;
            }

            Symphony.Ajax.request({
                url: '/services/category.svc/categories/{0}/{1}'.format(me.categoryType, record.get('id')),
                method: 'POST',
                jsonData: record.data,
                success: function (args) {
                    var leaf = record.get('leaf'),
                        checked = record.get('checked');

                    record.data = args.data;
                    record.set('leaf', leaf);
                    record.set('checked', checked);
                    record.commit();

                    me.fireEvent('itemclick', me, record);
                    me.fireEvent('endEdit');
                    me.addBtn.setDisabled(false);
                    
                },
                failure: function (args) {
                    Ext.Msg.alert('Error', args.error);
                    me.fireEvent('endEdit');
                    record.remove();
                    me.addBtn.setDisabled(false);
                }
            });
        },
        onTreeCancelEdit: function (editor, e, opts) {
            if (!e.record.get('id')) {
                e.record.remove();
            }
            me.addBtn.setDisabled(false);
        }
    });

})();