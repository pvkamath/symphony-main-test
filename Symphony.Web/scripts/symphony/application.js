﻿//if (window.Symphony.User.isSalesChannelAdmin) {
// Only going to bother creating and running the function if the user is a
//     a sales chanel admin. 
(function (window, undefined) {
    //Globals
    var Ext = window.Ext,
            Symphony = window.Symphony,
            customer = Symphony.Customer,
            salesChannelAdmin = customer.App,
            studentManagmentAdmin,
            applicationTileAdmin,
            applicationGrid,
            applicationPanel,
            applicationDefinition,
            applicationStore,
            tileDefinition,
            tileStore,
            tileGrid,
            tileForm,
            tileTab,
            filteredTileList,
            userApplicationGrid,
            userApplicationDefinition,
            userApplicationStore,
            userApplicationTileDefinition,
            userApplicationTileStore,
            userTileGrid,
            userTileForm,
            userAssignments,
            hierarchytiles,
            userAssignmentDialog,
            userAssignmentGrid,
            userAssignmentList,
            userPermissionsHierarchy;

    //#region Application & Tile admin TabPanel
    Symphony.Customer.ApplicationTypeAdmin = Ext.define('customer.applicationtiletabs', {
        alias: 'widget.customer.applicationtiletabs',
        extend: 'Ext.TabPanel',
        initComponent: function () {
            Ext.apply(this, {
                requiresActivation: true,
                border: false,
                defaults: {
                    border: false
                },
                activeItem: 0,
                items: [{
                    title: 'Applications',
                    layout: 'fit',
                    xtype: 'customer.applicationtab'
                }, {
                    title: 'Tiles',
                    layout: 'fit',
                    xtype: 'customer.tiletab'
                }
                ]
            });
            this.callParent(arguments);
        }
    })

    //#endregion

    


    function deleteRenderer(value, meta, record) {
        return '<img src="/images/bin.png" ext:qtip="Delete"/>';
    };
    Symphony.Customer.ApplicationGrid = Ext.define('customer.applicationgrid', {
        alias: 'widget.customer.applicationgrid',
        extend: 'symphony.searchablegrid',
        initComponent: function () {
            var me = this,
                url = '/services/customer.svc/applications/?',
                colModel = new Ext.grid.ColumnModel({
                    defaults: {
                        sortable: true,
                        align: 'center',
                        renderer: Symphony.Portal.valueRenderer
                    },
                    columns: [{
                        header: ' ',
                        hideable: false,
                        renderer: deleteRenderer,
                        width: 35,
                        sortable: false
                    }, {
                        /*id: 'name',*/
                        header: 'Name',
                        dataIndex: 'name',
                        width: 140,
                        align: 'left',
                        flex: 1
                    }
                    ]
                });
            Ext.apply(me, {
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }
                    ]
                },
                idProperty: 'id',
                deferLoad: true,
                url: url,
                colModel: colModel,
                model: 'ApplicationModel'
            });
            Symphony.Customer.ApplicationGrid.superclass.initComponent.apply(me, arguments);
        }
    });


    Symphony.Customer.ApplicationTab = Ext.define('customer.applicationtab', {
        alias: 'widget.customer.applicationtab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(me, {
                layout: 'border',
                border: false,
                items: [{
                    split: true,    
                    region: 'west',
                    border: false,
                    width: 300,
                    stateId: 'customer.applicationgrid',
                    xtype: 'customer.applicationgrid',
                    listeners: {
                        cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            
                            var record = grid.getStore().getAt(rowIndex), panel;
                            if (columnIndex === 0) {
                                Ext.Msg.confirm('Are you sure?', 'Deleting this Application will remove All associated Tiles, Assignments and Credentials. It Cannot be undone.', function (btn) {
                                    if (btn == 'yes') {
                                        Symphony.Ajax.request({
                                            url: '/services/customer.svc/applications/' + record.get('id') + '/delete',
                                            success: function (result) {
                                                var panel = me.findPanel(record.data.id);
                                                if (!!panel) {
                                                    panel.confirmedRemove = true;
                                                    panel.ownerCt.remove(panel);
                                                }
                                                grid.getStore().reload();
                                            }
                                        });
                                    }
                                });
                            }
                            else {
                                me.addPanel(record);
                            }
                        },
                        addclick: function () {
                            var defaultData = {
                                name: "New Application",
                                authenticationUri: "http://www.example.com",
                                passUserNamePass: true
                            },

                            record = Ext.create('ApplicationModel', defaultData);
                            me.addPanel(record);
                        }
                    }
                }, {
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    itemId: 'customer.applicationcontainer',
                    border: false,
                    listeners: {
                        beforeremove: function (container, component) {
                            if (!component.confirmedRemove) {
                                Ext.Msg.confirm('Are you sure?', 'Any unsaved changes will be lost. Are you sure?', function (btn) {
                                    if (btn == 'yes') {
                                        component.confirmedRemove = true;
                                        container.remove(component);
                                    }
                                });
                                return false;
                            }
                        }
                    }
                }],
                listeners: {
                    activate: {
                        fn: function (panel) {
                            panel.query('[xtype=customer.applicationgrid]')[0].refresh();
                        },
                        single: true
                    }
                }
            });
            Symphony.Customer.ApplicationTab.superclass.initComponent.apply(me, arguments);
        },
        addPanel: function (record) {
            var me = this,
                        tabPanel = this.getComponent('customer.applicationcontainer'),
                        id = 'application_' + record.data.id,
                        name = record.data.name,
                        found = me.findPanel(record.data.id);
            if (found) {
                tabPanel.activate(found);
            }
            else {
                var panel = tabPanel.add({
                    xtype: 'customer.applicationform',
                    closable: true,
                    activate: true,
                    data: record.data,
                    tabId: id,
                    applicationId: record.data.id,
                    title: name,
                    listeners: {
                        save: function () {
                            var grid = me.find('xtype', 'customer.applicationgrid')[0];
                            grid.refresh();
                        }
                    }
                });
                panel.show();
            }
            tabPanel.doLayout();
        },
        findPanel: function (id) {
            var tabPanel = this.getComponent('customer.applicationcontainer'),
                tabId = 'application_' + id;
            return tabPanel.queryBy(function (p) {
                return p.tabId == tabId;
            })[0];
        }

    });


    Symphony.Customer.ApplicationForm = Ext.define('customer.applicationform', {
        alias: 'widget.customer.applicationform',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this,
            appUrl = ''; //'/services/applications.svc/applications/'
            Ext.apply(me, {
                layout: 'fit',
                border: false,
                defaults: {
                    cls: 'x-panel-transparent'
                },
                tbar: {
                    xtype: 'symphony.savecancelbar',
                    items: [{
                        text: 'Duplicate',
                        iconCls: 'x-button-duplicate',
                        handler: function () {
                            return me.save(true);
                        }
                    }],
                    listeners: {
                        save: function () {
                            return me.save(false);
                        },
                        cancel: function () {
                            me.ownerCt.remove(me);
                        }
                    }
                },
                items: [{
                    layout: 'fit',
                    padding: 15,
                    border: false,
                    defaults: {
                        border: false,
                        cls: 'x-panel-transparent'
                    },
                    items: [{
                        xtype: 'form',
                        name: 'form',
                        frame: true,
                        border: false,
                        bodyStyle: 'overflow-y:auto;padding:5px',
                        defaults: {
                            anchor: '100%'
                        },
                        items: [{
                            fieldLabel: 'Name',
                            name: 'name',
                            allowBlank: false,
                            width: '100%',
                            xtype: 'textfield'
                        }, {
                            fieldLabel: 'Authorization URI',
                            name: 'authenticationUri',
                            xtype: 'textfield'
                        }, {
                            xtype: 'checkbox',
                            boxLabel: 'Send User Name and Password',
                            name: 'passUserNamePass'
                        }]
                    }]
                }]
            });
            Symphony.Customer.ApplicationForm.superclass.initComponent.apply(me, arguments);
            me.applicationId = me.data.id;
            me.find('name', 'form')[0].bindValues(me.data);
        },
        save: function (duplicate) {
            var me = this,
                        form = me.find('name', 'form')[0],
                        application;
            if (!form.isValid()) {
                return false;
            }
            application = form.getValues();
            application.id = duplicate ? 0 : (me.applicationId || 0);
            Symphony.Ajax.request({
                url: '/services/customer.svc/applications/' + application.id + '/',
                jsonData: application,
                success: function (result) {
                    me.fireEvent('save', result.data);
                    me.setTitle(result.data.name);
                    me.applicationId = result.data.id;
                    me.tabId = 'application_' + result.data.id;
                }
            });
        }
    });

    //#endregion

    //#region Tile


    Symphony.Customer.TileGrid = Ext.define('customer.tilegrid', {
        alias: 'widget.customer.tilegrid',
        extend: 'symphony.simplegrid',
        initComponent: function () {
            var me = this,
                        url = '/services/customer.svc/tiles/?',
                        colModel = new Ext.grid.ColumnModel({
                            defaults: {
                                sortable: true,
                                align: 'center',
                                renderer: Symphony.Portal.valueRenderer
                            },
                            columns: [{
                                header: ' ',
                                hideable: false,
                                renderer: deleteRenderer,
                                width: 35,
                                sortable: false
                            }, {
                                /*id: 'name',*/
                                header: 'Name',
                                dataIndex: 'name',
                                width: 140,
                                align: 'left',
                                flex: 1
                            }
                            ]
                        });
            Ext.apply(me, {
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('addclick');
                        }
                    }
                    ]
                },
                idProperty: 'id',
                deferLoad: true,
                url: url,
                colModel: colModel,
                model: 'TileModel'
            });
            Symphony.Customer.TileGrid.superclass.initComponent.apply(me, arguments);
        }
    });


    Symphony.Customer.TileTab = Ext.define('customer.tiletab', {
        alias: 'widget.customer.tiletab',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(me, {
                layout: 'border',
                items: [{
                    split: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    stateId: 'customer.tilegrid',
                    xtype: 'customer.tilegrid',
                    listeners: {
                        cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                            var record = grid.getStore().getAt(rowIndex);
                            if (columnIndex === 0) {
                                Ext.Msg.confirm('Are you sure?', 'Deleting this Tile will remove All Assignments (Credenials and Applications are not deleted). It Cannot be undone.', function (btn) {
                                    if (btn == 'yes') {
                                        Symphony.Ajax.request({
                                            url: '/services/customer.svc/tiles/' + record.get('id') + '/delete',
                                            success: function (result) {
                                                var panel = me.findPanel(record.data.id);
                                                if (!!panel) {
                                                    panel.confirmedRemove = true;
                                                    panel.ownerCt.remove(panel);
                                                }
                                                grid.getStore().reload();
                                            }
                                        });

                                    }
                                });
                            }
                            else {
                                me.addPanel(record);
                            }
                        },
                        addclick: function () {
                            var defaultData = {
                                name: "New tile",
                                authenticationUri: "http://www.example.com",
                                passUserNamePass: true
                            },

                            record = Ext.create('TileModel', defaultData);
                            me.addPanel(record);
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    itemId: 'customer.tilecontainer',
                    listeners: {
                        beforeremove: function (container, component) {
                            if (!component.confirmedRemove) {
                                Ext.Msg.confirm('Are you sure?', 'Any unsaved changes will be lost. Are you sure?', function (btn) {
                                    if (btn == 'yes') {
                                        component.confirmedRemove = true;
                                        container.remove(component);
                                    }
                                });
                                return false;
                            }
                        }
                    }
                }
                ],
                listeners: {
                    activate: {
                        fn: function (panel) {
                            panel.query('[xtype=customer.tilegrid]')[0].refresh();
                        },
                        single: true
                    }
                }
            });
            Symphony.Customer.TileTab.superclass.initComponent.apply(me, arguments);
        },
        addPanel: function (record) {
            var me = this,
                        tabPanel = this.getComponent('customer.tilecontainer'),
                        id = 'tile_' + record.data.id,
                        name = record.data.name,
                        found = tabPanel.queryBy(function (p) {
                            return p.itemId == id;
                        })[0];
            if (found) {
                tabPanel.activate(found);
            }
            else {
                var panel = tabPanel.add({
                    xtype: 'customer.tileform',
                    closable: true,
                    activate: true,
                    data: record.data,
                    tabId: id,
                    tileId: record.data.id,
                    title: name,
                    listeners: {
                        save: function () {
                            var grid = me.find('xtype', 'customer.tilegrid')[0];
                            grid.refresh();
                        }
                    }
                });
                panel.show();
            }
            tabPanel.doLayout();
        },
        findPanel: function (id) {
            var tabPanel = this.getComponent('customer.tilecontainer'),
                tabId = 'tile_' + id;
            return tabPanel.queryBy(function (p) {
                return p.tabId == tabId;
            })[0];
        }
    });


    Symphony.Customer.TileForm = Ext.define('customer.tileform', {
        alias: 'widget.customer.tileform',
        extend: 'Ext.Panel',
        store: tileStore,
        initComponent: function () {
            var me = this,
                        appUrl = ''; //'/services/tiles.svc/tiles/'
            Ext.apply(me, {
                layout: 'fit',
                border: false,
                defaults: {
                    border: false
                },
                dockedItems: [{
                    xtype: 'symphony.savecancelbar',
                    items: [{
                        text: 'Duplicate',
                        iconCls: 'x-button-duplicate',
                        hidden: me.popup ? true : false,
                        handler: function () {
                            return me.save(true);
                        }
                    }, {
                        text: 'Get Url',
                        iconCls: 'x-button-gear',
                        handler: function () {
                            if (me.tileId > 0) {
                                var url = 'http://' + window.location.hostname + '/services/customer.svc/applications/applinks/' + me.tileId + '?jsonp=?';
                                Ext.Msg.show({
                                    title: 'Url',
                                    width: 500,
                                    msg: 'The Url for this tile is:',
                                    prompt: true,
                                    value: url
                                });
                            }
                        }
                    }],
                    listeners: {
                        save: function () {
                            var result = me.save(false);

                            if (result) {
                                if (typeof (me.completeCallback) === 'function') {
                                    me.completeCallback();
                                }
                            }

                            return result;
                        },
                        cancel: function () {
                            me.ownerCt.remove(me);

                            if (typeof (me.completeCallback) === 'function') {
                                me.completeCallback();
                            }
                        }
                    }
                }],
                items: [{
                    layout: 'fit',
                    defaults: {
                        border: false
                    },
                    padding: 15,
                    items: [{
                        xtype: 'form',
                        name: 'form',
                        border: false,
                        frame: true,
                        
                        bodyStyle: 'overflow-y:auto;padding: 5px',
                        items: [{
                            fieldLabel: 'Name',
                            name: 'name',
                            allowBlank: false,
                            width: 300,
                            xtype: 'textfield'
                        }, {
                            fieldLabel: 'Destination URI',
                            name: 'tileUri',
                            width: 300,
                            xtype: 'textfield'
                        }, {
                            fieldLabel: 'Application',
                            name: 'applicationId',
                            xtype: 'symphony.pagedcombobox',
                            url: '/services/customer.svc/applications/?',
                            model: 'ApplicationModel',
                            bindingName: 'applicationName',
                            ref: 'combo',
                            valueField: 'id',
                            displayField: 'name',
                            allowBlank: false,
                            flex: 1,
                            emptyText: 'Select an application...',
                            valueNotFoundText: 'Select an application...'
                        }
                        ]
                    }
                    ]
                }
                ]
            });
            Symphony.Customer.TileForm.superclass.initComponent.apply(me, arguments);
            me.tileId = me.data.id;
            me.find('name', 'form')[0].bindValues(me.data);
        },
        save: function (duplicate) {
            var me = this,
                        form = me.find('name', 'form')[0],
                        tile;
            if (!form.isValid()) {
                return false;
            }
            tile = form.getValues();

            tile.id = duplicate ? 0 : (me.tileId || 0);
            Symphony.Ajax.request({
                url: '/services/customer.svc/tiles/' + tile.id + '/',
                jsonData: tile,
                success: function (result) {
                    me.fireEvent('save', result.data);
                    me.setTitle(result.data.name);
                    me.tileId = result.data.id;
                    me.tabId = 'tile_' + result.data.id;
                }
            });

            return true;
        }
    });

    //#endregion

    function credentialStateRenderer(value, meta, record) {
        var src = {
            'none': '<img src="/images/application_form_disabled.png">',
            'missing': '<img src="/images/application_form_error.png">',
            'complete': '<img src="/images/application_form.png">',
            'default': ''
        };
        return src[value] || src['default'];
    };

    function deleteTileRenderer(value, meta, record) {
        var data = record.data;
        if (!data.isAssignedByJobRole && !data.isAssignedByLocation && !data.isAssignedByAudience) {
            return '<img src="/images/bin.png" />';
        }
        else {
            return '&nbsp;';
        }
    };

    function audienceStateRenderer(value) {
        return '<img src="/images/group' + (!!value ? '' : '_disabled') + '.png">'
    };

    function jobRoleStateRenderer(value) {
        return '<img src="/images/building' + (!!value ? '' : '_disabled') + '.png">'
    };

    function placeStateRenderer(value) {
        return '<img src="/images/map' + (!!value ? '' : '_disabled') + '.png">'
    };
    
    Symphony.Customer.UserApplicationGrid = Ext.define('customer.userapplicationgrid', {
        alias: 'widget.customer.userapplicationgrid',
        extend: 'symphony.simplegrid',
        initComponent: function () {
            var me = this,
            url = '/services/customer.svc/applications/users/' + me.userId,
            //recordDef = Ext.data.Record.create(userApplicationDefinition),
            colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false,
                    align: 'center',
                    hideable: false,
                    menuDisabled: true,
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'name',*/
                    header: 'Name',
                    dataIndex: 'name',
                    width: 140,
                    align: 'left',
                    menuDisabled: false,
                    sortable: true,
                    flex: 1
                }, {
                    /*id: 'credentialState',*/
                    header: '',
                    dataIndex: 'credentialState',
                    width: 30,
                    renderer: credentialStateRenderer
                }]
            });
            Ext.apply(me, {
                tbar: {
                    items: [{
                        text: 'Assign Tile to User',
                        iconCls: 'x-button-add',
                        handler: function () {
                            me.fireEvent('assign');
                        }
                    }
                    ]
                },
                idProperty: 'id',
                deferLoad: true,
                store: userApplicationStore,
                url: url,
                colModel: colModel,
                model: 'UserApplication'
            });
            Symphony.Customer.UserApplicationGrid.superclass.initComponent.apply(me, arguments);
            me.getStore().reload();
        }
    });


    Symphony.Customer.UserTileGrid = Ext.define('customer.usertilegrid', {
        alias: 'widget.customer.usertilegrid',
        extend: 'symphony.simplegrid',
        initComponent: function () {
            var me = this,
                appId = me.applicationId,
                userId = me.userId,
                url = '/services/customer.svc/applications/' + appId + '/users/' + userId + '/', //'/services/applications.svc/applications/'
                //recordDef = Ext.data.Record.create(userApplicationTileDefinition),
                colModel = new Ext.grid.ColumnModel({
                    defaults: {
                        sortable: false,
                        align: 'center',
                        hideable: false,
                        menuDisabled: true,
                        renderer: Symphony.Portal.valueRenderer
                    },
                    columns: [{
                        header: '',
                        width: 30,
                        renderer: deleteTileRenderer
                        /*id: 'doDelete'*/
                    }, {
                        /*id: 'name',*/
                        header: 'Name',
                        dataIndex: 'name',
                        width: 180,
                        align: 'left',
                        menuDisabled: false,
                        sortable: true
                    }, {
                        header: jobRoleStateRenderer(true),
                        width: 30,
                        renderer: jobRoleStateRenderer,
                        dataIndex: 'isAssignedByJobRole'
                        //id: 'isAssignedByJobRole'
                    }, {
                        header: placeStateRenderer(true),
                        width: 30,
                        renderer: placeStateRenderer,
                        dataIndex: 'isAssignedByLocation'
                        //id: 'isAssignedByLocation'
                    }, {
                        header: audienceStateRenderer(true),
                        width: 30,
                        renderer: audienceStateRenderer,
                        dataIndex: 'isAssignedByAudience'
                        //id: 'isAssignedByAudience'
                    }]
                });
            Ext.apply(me, {
                idProperty: 'id',
                deferLoad: true,
                //autoExpandColumn: 'name',
                store: userApplicationTileStore,
                url: url,
                colModel: colModel,
                model: 'UserApplicationTile'
            });
            Symphony.Customer.UserTileGrid.superclass.initComponent.apply(me, arguments);
            me.getStore().reload();
        },
        listeners: {
            cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                if (columnIndex > 0) {
                    return
                }
                var record = grid.getStore().getAt(rowIndex);
                var tileId = record.get('id');
                var userId = record.get('userID');
                Ext.Msg.confirm('Are you sure?', 'Are you sure you want to remove the selected "' + record.get('name') + '" tile from this user?', function (btn) {
                    if (btn == 'yes') {
                        Symphony.Ajax.request({
                            url: '/services/customer.svc/tiles/users/' + userId + '/map/delete/',
                            jsonData: [tileId],
                            success: function () {
                                grid.store.reload();
                            }
                        });
                    }
                });
            }
        }
    });



    Symphony.Customer.CredentialForm = Ext.define('customer.applicationcredentialform', {
        alias: 'widget.customer.applicationcredentialform',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this,
                data = me.data,
                userId = me.userId
            appUrl = '/services/customer.svc/applications/' + data.id + '/users/' + userId + '/';
            Ext.apply(me, {
                layout: 'border',
                defaults: {
                    border: false
                },
                items: [{
                    region: 'north',
                    height: 27,
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        itemId: 'customer.userapplication.credentialstoolbar',
                        items: [{
                            text: 'Edit Credentials',
                            name: 'edit',
                            iconCls: 'x-button-edit',
                            handler: function () {
                                return me.setState('edit');
                            }
                        }],
                        listeners: {
                            save: function () {
                                return me.save(false);
                            },
                            cancel: function () {
                                me.reset()
                                me.setState('show');
                            }
                        }
                    }]
                }, {
                    region: 'center',
                    layout: 'fit',
                    defaults: {
                        border: false
                    },
                    items: [{
                        xtype: 'form',
                        itemId: 'customer.userapplication.credentials',
                        border: false,
                        frame: true,
                        bodyStyle: 'overflow-y:auto;padding-right:10px',
                        items: [{
                            name: 'credentialUserName',
                            xtype: 'textfield',
                            fieldLabel: 'Username',
                            allowBlank: false,
                            enableKeyEvents: true,
                            listeners: {
                                keyup: function () {
                                    me.buttonState();
                                }
                            }
                        }, {
                            name: 'credentialPassword',
                            fieldLabel: 'Password',
                            xtype: 'textfield',
                            inputType: Symphony.User.hasPasswordMask ? 'password' : 'text',
                            allowBlank: false,
                            enableKeyEvents: true,
                            listeners: {
                                keyup: function () {
                                    me.buttonState();
                                }
                            }
                        }]
                    }]
                }]
            });
            Symphony.Customer.CredentialForm.superclass.initComponent.apply(me, arguments);
            me.credentialForm = me.find('itemId', 'customer.userapplication.credentials')[0];
            me.credentialToolbar = me.find('itemId', 'customer.userapplication.credentialstoolbar')[0];
            me.credentialForm.bindValues(me.data);
        },
        buttonState: function () {
            var fields = this.credentialForm.getForm().getFields(),
                        data = this.data,
                        key, i, enable = false,
                        val, savebutton;
            fields.each(function (item) {
                if (item && item.getValue) {
                    enable = enable || !(!item.isValid() || item.getValue() === data[item.name]);
                }
            });
            savebutton = this.credentialToolbar.find('name', 'save') || [];
            i = savebutton.length;
            while (i--) {
                savebutton[i].setDisabled(!enable);
            }
        },
        reset: function () {
            this.credentialForm.bindValues(this.data);
        },
        save: function () {
            var me = this,
                data = me.data,
                userId = me.findParentBy(function (parent) { return !!parent.userId }).userId,
                formData = me.credentialForm.getValues(),
                jsonData = {};

            if (formData.credentialUserName !== data.credentialUserName) {
                jsonData.userName = formData.credentialUserName;
            }
            if (formData.credentialPassword !== data.credentialPassword) {
                jsonData.pass = formData.credentialPassword;
            }

            Symphony.Ajax.request({
                url: '/services/customer.svc/applications/' + data.id + '/users/' + userId + '/credentials/' + data.credentialId,
                jsonData: jsonData,
                success: function (result) {
                    data.credentialUserName = formData.credentialUserName;
                    data.credentialPassword = result.data.pass;
                    data.credentialId = result.data.id;
                    data.id = result.data.applicationId;
                    me.fireEvent('save', me.data);
                    me.reset();
                    me.setState('show');
                }
            });
        },
        setState: function (state) {
            var form = this,
                        toolbarButtons, formfields, item, i, disabled = (state === 'show'),
                        stateButtonNames = {
                            'new': 'save',
                            'show': 'edit',
                            'edit': 'save cancel'
                        };
            toolbarButtons = !!form.credentialToolbar ? form.credentialToolbar.items : [];
            formfields = !!form.credentialForm ? form.credentialForm.getForm().getFields() : [];
            i = toolbarButtons.length;
            while (i--) {
                item = toolbarButtons.get(i);
                item.setVisible(stateButtonNames[state].indexOf(item.name) > -1);
            }

            i = formfields.length;
            while (i--) {
                formfields.get(i).setDisabled(disabled);
            }

            form.buttonState();
        }
    });


    Symphony.Customer.UserTileGridForm = Ext.define('customer.usertilegridform', {
        alias: 'widget.customer.usertilegridform',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this,
                store = me.store,
                data = me.data;

            Ext.apply(me, {
                xtype: 'panel',
                layout: 'border',
                items: [{
                    region: 'north',
                    height: 97,
                    itemId: 'customer.applicationcredentialform',
                    border: false,
                    xtype: 'customer.applicationcredentialform',
                    data: data
                }, {
                    region: 'center',
                    itemId: 'usertilegrid',
                    border: false,
                    userId: me.userId,
                    applicationId: me.applicationId,
                    xtype: 'customer.usertilegrid'
                }] //tiles
            });
            Symphony.Customer.UserTileGridForm.superclass.initComponent.apply(me, arguments);
            me.getComponent('customer.applicationcredentialform').reset();
            me.addEvents('setstate', 'resetform');
        },
        refresh: function() {
            var grid = this.getComponent('usertilegrid');
            grid.getStore().reload();
        },
        listeners: {
            setstate: function (state) {
                this.getComponent('customer.applicationcredentialform').setState(state);
            },
            resetform: function () {
                this.getComponent('customer.applicationcredentialform').reset();
            }
        }
    });



    Symphony.Customer.UserTileForm = Ext.define('customer.usertileform', {
        alias: 'widget.customer.usertileform',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;
            Ext.apply(me, {
                layout: 'border',
                items: [{
                    split: true,
                    border: false,
                    region: 'west',
                    width: 150,
                    userId: me.userId,
                    stateId: 'customer.userapplicationgrid',
                    xtype: 'customer.userapplicationgrid',
                    listeners: {
                        rowclick: function (grid, rowIndex, e) {
                            me.addPanel(grid.getStore().getAt(rowIndex))
                        },
                        assign: function () {
                            var applicationGrid = this;
                            var parent = applicationGrid.findParentBy(function (i) { return !!i.customerId });
                            var customerId = parent.customerId;
                            var w = new Ext.Window({
                                title: 'Add Tiles',
                                height: 410,
                                width: 500,
                                layout: 'fit',
                                modal: true,
                                items: [{
                                    layout: 'fit',
                                    border: false,
                                    customerId: customerId,
                                    userId: applicationGrid.userId,
                                    xtype: 'customer.userassignmentgrid',
                                    listeners: {
                                        cancel: function () {
                                            w.destroy();
                                        },
                                        save: function () {
                                            applicationGrid.refresh();
                                            me.fireEvent('refreshAll');
                                            w.destroy();
                                        }
                                    }
                                }]
                            });
                            w.show();
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    xtype: 'tabpanel',
                    enableTabScroll: true,
                    itemId: 'customer.user.applicationTabs',
                    listeners: {
                        beforeremove: function (container, component) {
                            //console.log(arguments);
                        }
                    }
                }],
                listeners: {
                    refreshAll: function () {
                        var tabPanel = me.getComponent('customer.user.applicationTabs');
                        tabPanel.items.each(function (p) {
                            if (Ext.isFunction(p.refresh)) {
                                p.refresh();
                            }
                        });
                    }
                }
            });
            Symphony.Customer.UserTileForm.superclass.initComponent.apply(me, arguments);
        },
        addPanel: function (record) {
            var me = this,
                    tabPanel = this.getComponent('customer.user.applicationTabs'),
                    grid = this.find('xtype', 'customer.userapplicationgrid')[0],
                    id = 'assignment_' + record.data.id,
                    userId = this.userId,
                    name = record.get('name'),
                    panel, credentialState,
                    found = tabPanel.queryBy(function (p) {
                        return p.tileId == id;
                    })[0];
            if (found) {
                tabPanel.activate(found);
            }
            else {
                credentialState = record.get('credentialState');
                if (credentialState === 'none') {
                    //tileGrid only
                    panel = tabPanel.add({
                        title: record.get('name'),
                        tileId: id,
                        userId: userId,
                        applicationId: record.get('id'),
                        border: false,
                        closable: true,
                        xtype: 'customer.usertilegrid'
                    });
                }
                else {
                    panel = tabPanel.add({
                        tileId: id,
                        userId: userId,
                        grid: grid,
                        applicationId: record.get('id'),
                        title: record.get('name'),
                        data: record.data,
                        border: false,
                        closable: true,
                        xtype: 'customer.usertilegridform'
                    });
                    panel.fireEvent('setState', credentialState === 'complete' ? 'show' : 'new');
                }
                panel.show();
            }
            tabPanel.doLayout();
        }
    });


    Symphony.Customer.UserPermissionsHierarchyPanel = Ext.define('customer.userhierarchypermissions', {
        alias: 'widget.customer.userhierarchypermissions',
        extend: 'Ext.Panel',
        customerId: 0,
        userId: 0,
        initComponent: function () {
            var me = this;
            var parent = me.findParentBy(function (i) { return !!i.customerId });
            //var customerId = parent.customerId;
            var customerId = me.customerId;
            var userId = me.userId;

            Ext.apply(this, {
                layout: 'fit',
                items: [{
                    xtype: 'panel',
                    frame: false,
                    border: false,
                    cls: 'x-panel-transparent',
                    name: 'userpermhierarchypanel',
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        defaultMargins: 2
                    },
                    items: [{
                        xtype: 'panel',
                        name: 'userhierarchytreelocationscontainer',
                        flex: 0.33,
                        border: true,
                        layout: 'fit',
                        frame: false,
                        items: [{
                            title: Symphony.Aliases.locations,
                            xtype: 'customer.hierarchypermtree',
                            name: 'userhierarchytreelocations',
                            iconCls: 'x-location',
                            animate: false,
                            checkboxes: true,
                            border: false,
                            hierarchyName: 'location',
                            customerId: customerId,
                            userId: userId
                        }, {
                            xtype: 'panel',
                            name: 'userhierarchytreelocpanel',
                            frame: false,
                            border: false
                        }]
                    }, {
                        xtype: 'panel',
                        name: 'userhierarchytreeaudiencecontainer',
                        flex: 0.33,
                        border: true,
                        layout: 'fit',
                        frame: false,
                        items: [{
                            title: Symphony.Aliases.audience,
                            xtype: 'customer.hierarchypermtree',
                            name: 'userhierarchytreeaudience',
                            checkboxes: true,
                            animate: false,
                            iconCls: 'x-audience',
                            hierarchyName: 'audience',
                            border: false,
                            customerId: customerId,
                            userId: userId
                        }, {
                            xtype: 'panel',
                            name: 'userhtreeaudiencecont',
                            frame: false,
                            border: false
                        }]
                    }, {
                        xtype: 'panel',
                        name: 'userhierarchytreejobrolescontainer',
                        flex: 0.33,
                        border: true,
                        layout: 'fit',
                        frame: false,
                        items: [{
                            title: Symphony.Aliases.jobRoles,
                            xtype: 'customer.hierarchypermtree',
                            name: 'userhierarchytreejobroles',
                            checkboxes: true,
                            animate: false,
                            iconCls: 'x-jobrole',
                            hierarchyName: 'jobRole',
                            border: false,
                            customerId: customerId,
                            userId: userId
                        }, {
                            xtype: 'panel',
                            name: 'userhierarchytreejobr',
                            frame: false,
                            border: false
                        }]
                    }]
                }],
                listeners: {
                    beforeshow: function () {
                        var parent = me.findParentBy(function (i) { return !!i.customerId });
                        var reportingAnalysFlag = parent.find('name', 'reportingAnalyst')[0].checked;
                        var reportingJobRoleAnalystFlag = parent.find('name', 'reportingJobRoleAnalyst')[0].checked;
                        var reportingLocationAnalystFlag = parent.find('name', 'reportingLocationAnalyst')[0].checked;
                        var reportingAudienceAnalystFlag = parent.find('name', 'reportingAudienceAnalyst')[0].checked;
                        if (!reportingAnalysFlag) {
                            if (!reportingLocationAnalystFlag) {
                                this.find('name', 'userhierarchytreelocations')[0].checkboxes = false;
                                this.find('name', 'userhierarchytreelocations')[0].disabled = true;
                                this.find('name', 'userhierarchytreelocpanel')[0].disabled = true;
                            }

                            if (!reportingAudienceAnalystFlag) {
                                this.find('name', 'userhierarchytreeaudience')[0].checkboxes = false;
                                this.find('name', 'userhierarchytreeaudience')[0].disabled = true;
                                this.find('name', 'userhtreeaudiencecont')[0].disabled = true;
                            }

                            if (!reportingJobRoleAnalystFlag) {
                                this.find('name', 'userhierarchytreejobroles')[0].checkboxes = false;
                                this.find('name', 'userhierarchytreejobroles')[0].disabled = true;
                                this.find('name', 'userhierarchytreejobroles')[0].userhierarchytreejobr = true;
                            }
                        }
                    }
                }
            });
            Symphony.Customer.UserPermissionsHierarchyPanel.superclass.initComponent.apply(me, arguments);
        }
    });


    Symphony.Customer.HierarchyTilesGrid = Ext.define('customer.hierarchytiles', {
        alias: 'widget.customer.hierarchytiles',
        extend: 'symphony.searchablegrid',
        hierarchyId: 0,
        hierarchyName: 'location',
        initComponent: function () {
            var me = this;
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                        {
                            /*id: 'name',*/ header: 'Tile Name', dataIndex: 'name',
                            flex: 1
                        }
                ]
            });
            Ext.apply(this, {
                tbar: {
                    items: !Symphony.Customer.enableEditing ? [] : [{
                        text: 'Add Tile to this ' + Symphony.Aliases[this.hierarchyName],
                        iconCls: 'x-button-add',
                        name: 'add',
                        disabled: true,
                        handler: Ext.bind(this.selectTiles, this)
                    },
                        {
                            text: 'Remove Tile from this ' + Symphony.Aliases[this.hierarchyName],
                            iconCls: 'x-button-delete',
                            name: 'delete',
                            disabled: true,
                            handler: function () {
                                var results = [];
                                Ext.each(me.getSelectionModel().getSelections(), function (record) {
                                    results.push(record.get('id'));
                                });
                                me.deleteTileMap(results);
                            }
                        }]
                },
                deferLoad: true, // no need to load the data until a location is selected
                colModel: colModel,
                url: '/ignoreme', // url is required for Ext to work, but we're setting it in the beforeload
                model: 'TileModel',
                viewConfig: {
                    listeners: {
                        'refresh': function () {
                            var item = me.getTopToolbar().find('name', 'delete')[0];
                            if (item) { item.disable(); }
                        }
                    }
                },
                listeners: {
                    rowselect: function () {
                        var item = me.getTopToolbar().find('name', 'delete')[0];
                        if (item) { item.enable(); }
                    },
                    itemdblclick: function (grid, record, item, rowIndex, e) {
                        var data = record.data;
                        me.edit(data.id, data);
                    }
                }
            });
            Symphony.Customer.HierarchyTilesGrid.superclass.initComponent.apply(me, arguments);

            this.store.on('beforeload', function (store) {
                store.proxy.api.read = '/services/customer.svc/tiles/customer/' + this.customerId + '/' + this.hierarchyName.toLowerCase() + '/' + this.hierarchyId;
            }, this);
        },
        deleteTileMap: function (tileIds) {
            var me = this;
            var word = tileIds.length > 1 ? 'tiles' : 'tile';
            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to remove the selected ' + word + ' from this ' + Symphony.Aliases[me.hierarchyName] + '?', function (btn) {
                if (btn == 'yes') {
                    Symphony.Ajax.request({
                        url: '/services/customer.svc/tiles/' + me.hierarchyName + 's/' + me.hierarchyId + '/map/delete/',
                        jsonData: tileIds,
                        success: function () {
                            me.refresh();
                        }
                    });
                }
            });
        },
        refresh: function () {
            this.store.reload();
        },
        edit: function (tileId, data) {
            var me = this;
            var w = new Ext.Window({
                title: 'Edit Tile',
                width: 450,
                height: 160,
                layout: 'fit',
                items: [{
                    border: false,
                    xtype: 'customer.tileform',
                    layout: 'fit',
                    id: tileId,
                    tileId: tileId,
                    data: data,
                    completeCallback: function () {
                        w.close();
                        me.refresh();
                    },
                    popup: true
                }]
            });
            w.show();
        },
        selectTiles: function () {
            var me = this;
            var w = new Ext.Window({
                title: 'Add Tiles',
                height: 410,
                width: 500,
                layout: 'fit',
                modal: true,
                items: [{
                    layout: 'fit',
                    border: false,
                    customerId: me.customerId,
                    hierarchyName: me.hierarchyName,
                    hierarchyId: me.hierarchyId,
                    filter: me.getTileIds(),
                    xtype: 'customer.filteredtilesgrid',
                    listeners: {
                        cancel: function () {
                            w.destroy();
                        },
                        save: function () {
                            me.refresh();
                            w.destroy();
                        }
                    }
                }]
            });
            w.show();
        },
        getTileIds: function () {
            var ids = [];
            this.store.each(function (record) {
                ids.push(record.get('id'));
            });
            return ids;
        },
        setFilter: function (id) {
            this.hierarchyId = id;
            var addBtn = this.getTopToolbar().find('name', 'add')[0];
            if (id != 0) {
                this.refresh();
                if (addBtn) { addBtn.enable(); }
            } else {
                if (addBtn) { addBtn.disable(); }
            }
        }
    });



    Symphony.Customer.FilteredTilesGrid = Ext.define('customer.filteredtilesgrid', {
        alias: 'widget.customer.filteredtilesgrid',
        extend: 'symphony.searchablegrid',
        customerId: 0,
        hierarchyName: 'location',
        hierarchyId: 0,
        initComponent: function () {
            var me = this;

            var sm = new Ext.selection.CheckboxModel({
                checkOnly: true,
                sortable: true,
                header: ' ' // kills the select-all checkbox
            });
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [

                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name',
                        flex: 1
                    }
                ]
            });

            Ext.apply(this, {
                tbar: {
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        listeners: {
                            save: function () {
                                Symphony.Ajax.request({
                                    url: '/services/customer.svc/tiles/' + me.hierarchyName + 's/' + me.hierarchyId + '/map/',
                                    jsonData: me.getSelections(),
                                    success: function () {
                                        me.fireEvent('save', me.getSelections());
                                    }
                                });
                            },
                            cancel: function () {
                                me.fireEvent('cancel');
                            }
                        }
                    }]
                },
                idProperty: 'id',
                url: '/ignoreme', // set in the beforeload filter
                colModel: colModel,
                model: 'TileModel',
                selModel: sm,
                plugins: [{ ptype: 'pagingselectpersist' }]
            });
            this.callParent(arguments);
            this.store.on('beforeload', this.selectedTileFilter, this);
        },
        selectedTileFilter: function (store, options) {
            store.proxy.api.read = '/services/customer.svc/tiles/customer/' + this.customerId + '/unassigned/' + this.hierarchyName + '/' + this.hierarchyId;
            store.proxy.method = 'GET';
            store.proxy.jsonData = this.filter;
        },
        getSelections: function () {
            return this.getPlugin('pagingSelectionPersistence').getPersistedSelectionIds()
        },
        setSelections: function (value) {
            this.getPlugin('pagingSelectionPersistence').setSelections(value);
        },
        clearSelections: function () {
            this.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
        },
        onRender: function () {
            Symphony.Customer.FilteredTilesGrid.superclass.onRender.apply(this, arguments);

            // after the load
            this.setSelections(this.selections || []);
        }
    });


    Symphony.Customer.UserAssignmentGrid = Ext.define('customer.userassignmentgrid', {
        alias: 'widget.customer.userassignmentgrid',
        extend: 'symphony.simplegrid',
        customerId: 0,
        initComponent: function () {
            var me = this;

            var sm = new Ext.selection.CheckboxModel({
                checkOnly: true,
                sortable: true,
                header: ' ' // kills the select-all checkbox
            });
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [

                    {
                        /*id: 'name',*/ header: 'Name', dataIndex: 'name',
                        flex: 1
                    }
                ]
            });

            Ext.apply(this, {
                userId: me.userId,
                tbar: {
                    items: [{
                        xtype: 'symphony.savecancelbar',
                        listeners: {
                            save: function () {
                                Symphony.Ajax.request({
                                    url: '/services/customer.svc/tiles/users/' + me.userId + '/map/',
                                    jsonData: me.getSelections(),
                                    success: function () {
                                        me.fireEvent('save', me.getSelections());
                                    }
                                });
                            },
                            cancel: function () {
                                me.fireEvent('cancel');
                            }
                        }
                    }]
                },
                idProperty: 'id',
                url: '/ignoreme', // set in the beforeload filter
                colModel: colModel,
                model: 'TileModel',
                selModel: sm,
                plugins: [{ ptype: 'pagingselectpersist' }],
                listeners: {
                    afterrender: function () {
                        me.setSelections(this.selections || []);
                    }
                }
            });
            this.callParent(arguments);
            this.store.on('beforeload', this.selectedTileFilter, this);
        },
        selectedTileFilter: function (store, options) {
            store.proxy.api.read = '/services/customer.svc/tiles/unassigned/users/' + this.userId + '/';
            store.proxy.method = 'GET';
            store.proxy.jsonData = this.filter;
        },
        getSelections: function () {
            return this.getPlugin('pagingSelectionPersistence').getPersistedSelectionIds()
        },
        setSelections: function (value) {
            this.getPlugin('pagingSelectionPersistence').setSelections(value);
        },
        clearSelections: function () {
            this.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
        }
    });


}(window));
//}