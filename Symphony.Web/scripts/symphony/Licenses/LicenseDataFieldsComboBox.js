﻿(function () {
    Symphony.License.LicenseDataFieldsComboBox = Ext.define('license.datafieldscombo', {
        alias: 'widget.license.datafieldscombo',
        extend: 'Ext.ux.form.field.BoxSelect',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                queryMode: 'local',
                triggerAction: 'all',
                queryDelay: 0,
                minChars: 1,
                store: new Ext.data.JsonStore({
                    root: 'data',
                    remote: true,
                    fields: Symphony.Definitions.userDataField,
                    autoLoad: true,
                    proxy: new Ext.data.HttpProxy({
                        method: 'GET',
                        url: '/services/customer.svc/user/datafields/0'
                    })
                }),
                allowBlank: true,
                //msgTarget: 'under',
                allowAddNewData: true,
                    
                //fieldLabel: 'Tags',
                emptyText: 'Enter or select data fields',
                resizable: true,
                anchor: '100%',
                displayField: 'displayName',
                valueField: 'userDataFieldId',
                //value: 'Sport,Science',
                extraItemCls: 'x-tag' + (me.readOnly ? ' read-only' : ''),
                renderFieldBtns: !me.readOnly,
                backspaceDeletesLastItem: !me.readOnly,
                itemCls: me.readOnly ? 'read-only' : '',
                listeners: {
                    beforeadditem: function (bs, v) {
                        //console.log('beforeadditem:', v);
                        //return false;
                    },
                    additem: function (bs, v) {
                        //console.log('additem:', v);
                    },
                    beforeremoveitem: function (bs, v) {
                        //console.log('beforeremoveitem:', v);
                        //return false;
                    },
                    removeitem: function (bs, v) {
                        //console.log('removeitem:', v);
                    },
                    newitem: function (bs, v) {
                        var newObj = {
                            id: v,
                            name: v
                        };
                        bs.addItem(newObj);
                    }
                }
            });
            this.callParent(arguments);
        },
        setValue: function (v) {
            Symphony.License.LicenseDataFieldsComboBox.superclass.setValue.apply(this, arguments);
        }
    });

})();