(function (window, undefined) {
    Symphony.License.LicensePanel = Ext.define('license.licensepanel', {
        alias: 'widget.license.licensepanel',
        extend: 'Ext.Panel',
        deferLoad: false,
        editable: true,
        record: null,
        licenseId: 0,
        customerId: 0,
        licenseExpirationRuleId: 0,
        initComponent: function () {
            var me = this;

            var tbar = {
                items: [{
                    xtype: 'button',
                    iconCls: 'x-button-save',
                    text: 'Save License',
                    handler: function () {

                        var assignmentsUserData = { assignmentsUserDataList: [], licenseAssignmentTypes: [] };

                        var assignmentsUserDataTab = me.find('name', 'userassignmentstab')[0];
                        if (assignmentsUserDataTab) {
                            var assignmentsUserDataTabStore = assignmentsUserDataTab.getStore();
                            if (assignmentsUserDataTab.rendered == true) {
                                assignmentsUserData.licenseAssignmentTypes.push('user');
                            }
                            assignmentsUserDataTabStore.each(function (r) {
                                if (r.get('parentObject') == '') {

                                    var customerId = r.get('customerId') ? r.get('customerId') :
                                                        me.customerId ? me.customerId : Symphony.User.customerId;

                                    assignmentsUserData.assignmentsUserDataList.push({
                                        id: 0,
                                        customerId: customerId,
                                        customerName: '',
                                        assignmentStatus: '',
                                        assignmentStatusId: r.get('assignmentStatusId'),
                                        locationId: 0,
                                        jobRoleId: 0,
                                        audienceId: 0,
                                        userId: r.get('userId'),
                                        firstName: r.get('firstName'),
                                        lastName: r.get('lastName'),
                                        startDate: Symphony.parseDate(r.get('startDate')).formatSymphony('microsoft'),
                                        expiryDate: Symphony.parseDate(r.get('expiryDate')).formatSymphony('microsoft')
                                    });
                                }
                            });
                        }

                        var assignmentLocationTab = me.find('name', 'locationassignmenttab')[0];
                        if (assignmentLocationTab) {
                            var assignmentLocationTabStore = assignmentLocationTab.getStore();
                            if (assignmentLocationTab.rendered == true) {
                                assignmentsUserData.licenseAssignmentTypes.push('location');
                            }
                            assignmentLocationTabStore.each(function (r) {

                                var customerId = r.get('customerId') ? r.get('customerId') :
                                                        me.customerId ? me.customerId : Symphony.User.customerId;

                                assignmentsUserData.assignmentsUserDataList.push({
                                    id: 0,
                                    customerId: customerId,
                                    customerName: '',
                                    assignmentStatus: '',
                                    assignmentStatusId: r.get('assignmentStatusId'),
                                    locationId: r.get('locationId'),
                                    jobRoleId: 0,
                                    audienceId: 0,
                                    userId: 0,
                                    startDate: Symphony.parseDate(r.get('startDate')).formatSymphony('microsoft'),
                                    expiryDate: Symphony.parseDate(r.get('expiryDate')).formatSymphony('microsoft')
                                });
                            });
                        }

                        var assignmentJobRoleDataTab = me.find('name', 'jobroleassignmenttab')[0];
                        if (assignmentJobRoleDataTab) {
                            var assignmentJobRoleDataTabStore = assignmentJobRoleDataTab.getStore();
                            if (assignmentJobRoleDataTab.rendered == true) {
                                assignmentsUserData.licenseAssignmentTypes.push('jobrole');
                            }
                            assignmentJobRoleDataTabStore.each(function (r) {

                                var customerId = r.get('customerId') ? r.get('customerId') :
                                                        me.customerId ? me.customerId : Symphony.User.customerId;

                                assignmentsUserData.assignmentsUserDataList.push({
                                    id: 0,
                                    customerId: customerId,
                                    customerName: '',
                                    assignmentStatus: '',
                                    assignmentStatusId: r.get('assignmentStatusId'),
                                    locationId: 0,
                                    jobRoleId: r.get('jobRoleId'),
                                    audienceId: 0,
                                    userId: 0,
                                    startDate: Symphony.parseDate(r.get('startDate')).formatSymphony('microsoft'),
                                    expiryDate: Symphony.parseDate(r.get('expiryDate')).formatSymphony('microsoft')
                                });
                            });
                        }

                        var assignmentAudienceDataTab = me.find('name', 'audienceassignmenttab')[0];
                        if (assignmentAudienceDataTab) {
                            var assignmentAudienceDataTabStore = assignmentAudienceDataTab.getStore();
                            if (assignmentAudienceDataTabStore.rendered == true) {
                                assignmentsUserData.licenseAssignmentTypes.push('audience');
                            }
                            assignmentAudienceDataTabStore.each(function (r) {

                                var customerId = r.get('customerId') ? r.get('customerId') :
                                                        me.customerId ? me.customerId : Symphony.User.customerId;

                                assignmentsUserData.assignmentsUserDataList.push({
                                    id: 0,
                                    customerId: customerId,
                                    customerName: '',
                                    assignmentStatus: '',
                                    assignmentStatusId: r.get('assignmentStatusId'),
                                    locationId: 0,
                                    jobRoleId: 0,
                                    audienceId: r.get('audienceId'),
                                    userId: 0,
                                    startDate: Symphony.parseDate(r.get('startDate')).formatSymphony('microsoft'),
                                    expiryDate: Symphony.parseDate(r.get('expiryDate')).formatSymphony('microsoft')
                                });
                            });
                        }

                        var generalTab = me.find('name', 'licensegeneraltabinfo')[0];
                        var componentBuilder = me.query('[xtype=symphony.componentbuilderfield]')[0];
                        
                        var formFieldJson = componentBuilder.getValue();


                        generalTab.save(assignmentsUserData, formFieldJson);

                    }
                }, {
                    xtype: 'button',
                    iconCls: 'x-button-cancel',
                    text: 'Cancel',
                    handler: function () {
                        me.fireEvent('cancel');
                    }
                }]
            };

            var licenseAssignmentStatuses = new Ext.data.ArrayStore({
                id: 0,
                fields: ['id', 'text'],
                data: [['1', 'Pending Course Work'], ['2', 'Pending Regulatory'], ['3', 'Regulatory Approved'], ['4', 'Assigned'], ['5', 'Revoked']]
            });

            Ext.apply(this, {
                border: false,
                tbar: tbar,
                layout: 'fit',
                licenseAssignmentStatuses: licenseAssignmentStatuses,
                defaults: {
                    border: false
                },
                items: [{
                    name: 'licensecontainertabpanel',
                    xtype: 'tabpanel',
                    frame: false,
                    border: false,
                    activeTab: 0,
                    listeners: {
                        save: function () {
                            var panels = this.query('panel');

                            for (var i = 0; i < panels.length; i++) {
                                if (panels[i].refresh) {
                                    panels[i].refresh();
                                }
                            }
                        }
                    },
                    items: [{
                        title: 'General',
                        xtype: 'license.generaltab',
                        name: 'licensegeneraltabinfo',
                        editable: me.editable,
                        frame: false,
                        border: false,
                        ref: 'generalTab',
                        bubbleEvents: ['save'],
                        record: me.record,
                        listeners: {
                            'licenseExpirationRuleChanged': function (ruleId) {
                                me.licenseExpirationRuleId = ruleId;

                                isExpirationDaysDisabled = true;
                                if (ruleId == Symphony.LicenseExpirationRule.daysAfterStart) {
                                    isExpirationDaysDisabled = false;
                                }

                                var form = me.find('xtype', 'license.generaltab')[0]
                                            .find('xtype', 'form')[0]
                                            .find('name', 'expirationRuleAfterDays')[0]
                                            .setDisabled(isExpirationDaysDisabled);

                            },
                            'customerchanged': function (customerId) {
                                me.customerId = customerId;
                            },
                            licenseloaded: function(data) {
                                var componentBuilder = me.query('[xtype=symphony.componentbuilderfield]')[0];

                                me.customerId = data.customerId;
                                componentBuilder.setValue(data.certificateFormFieldJson);
                            },
                            save: function (result) {
                                me.fireEvent('refreshTree');
                                me.licenseId = result.data.id;
                                me.setTitle(result.data.name);
                            }
                        }
                    }, {
                        xtype: 'panel',
                        title: 'Form Fields',
                        frame: false,
                        border: false,
                        layout: 'fit',
                        editable: me.editable,
                        items: [{
                            disabled: !me.editable,
                            xtype: 'symphony.componentbuilderfield',
                            fieldItemFilter: [
                                Symphony.ComponentBuilderFieldType.LABEL,
                                Symphony.ComponentBuilderFieldType.SYMPHONY_USER_FIELD,
                                Symphony.ComponentBuilderFieldType.SYMPHONY_DATA_FIELD
                            ],
                            isSizeRequired: false,
                            isConfirmationRequired: false,
                            isShowRequiredColumn: true
                        }]
                    }, {
                        xtype: 'panel',
                        tbar: {
                            hidden: !me.editable,
                            items: [{
                                xtype: 'button',
                                text: 'Assign License to User',
                                iconCls: 'x-button-add',
                                handler: function () {
                                    me.addUserAssignment();
                                }
                            }]
                        },
                        title: 'User Licenses',
                        frame: false,
                        border: false,
                        layout: 'fit',
                        editable: me.editable,
                        items: [{
                            xtype: 'license.userassignmenttab',
                            url: '/services/license.svc/licenseUserAssignments/' + me.licenseId + '/',
                            name: 'userassignmentstab',
                            licenseId: me.licenseId,
                            border: false,
                            listeners: {
                                itemdblclick: function (grid, record, item, rowIndex, e) {
                                    var parentObject = grid.getStore().getAt(rowIndex).get('parentObject');
                                    if (parentObject != '') {
                                        Ext.Msg.alert('Alert', 'You can not edit this record, since this user belong to ' + parentObject);
                                        return;
                                    }
                                    me.editUserAssignment(grid, rowIndex);
                                }
                            },
                            editable: me.editable
                        }]
                    }, {
                        xtype: 'panel',
                        tbar: {
                            hidden: !me.editable,
                            items: [{
                                xtype: 'button',
                                text: 'Assign License to {0}'.format(Symphony.Aliases.jobRole),
                                iconCls: 'x-button-add',
                                handler: function () {
                                    me.addAssignment({
                                        assignmentTab: 'jobroleassignmenttab',
                                        assignmentPanel: 'addjobroleassignmentpanel',
                                        entityType: 'jobRole', // sometimes the app wants 'jobrole' and sometimes it wants 'jobRole'. default to caml and toLowerCase where needed
                                        entityIdField: 'jobRoleId',
                                        entityNameField: 'jobRoleName'
                                    });
                                }
                            }]
                        },
                        title: '{0} Licenses'.format(Symphony.Aliases.jobRole),
                        frame: false,
                        border: false,
                        layout: 'fit',
                        editable: me.editable,
                        items: [{
                            xtype: 'license.genericassignmenttab',
                            name: 'jobroleassignmenttab',
                            nameColumnTitle: 'Job Role',
                            url: '/services/license.svc/licenseJobRoleAssignments/' + me.licenseId + '/',
                            licenseExpirationRuleId: me.licenseExpirationRuleId,
                            licenseId: me.licenseId,
                            customerId: me.customerId,
                            border: false,
                            listeners: {
                                itemdblclick: function (grid, record, item, rowIndex, e) {
                                    me.editAssignment(grid, rowIndex, {
                                        assignmentTab: 'jobroleassignmenttab',
                                        assignmentPanel: 'addjobroleassignmentpanel',
                                        entityType: 'jobRole',
                                        entityIdField: 'jobroleId',
                                        entityNameField: 'jobroleName'
                                    });
                                }
                            },
                            editable: me.editable
                        }]
                    }, {
                        xtype: 'panel',
                        tbar: {
                            hidden: !me.editable,
                            items: [{
                                xtype: 'button',
                                text: 'Assign License to {0}'.format(Symphony.Aliases.locations),
                                iconCls: 'x-button-add',
                                handler: function () {
                                    me.addAssignment({
                                        assignmentTab: 'locationassignmenttab',
                                        assignmentPanel: 'addlocationassignmentpanel',
                                        entityType: 'location',
                                        entityIdField: 'locationId',
                                        entityNameField: 'locationName'
                                    });
                                }
                            }]
                        },
                        title: '{0} Licenses'.format(Symphony.Aliases.locations),
                        frame: false,
                        border: false,
                        layout: 'fit',
                        editable: me.editable,
                        items: [{
                            xtype: 'license.genericassignmenttab',
                            name: 'locationassignmenttab',
                            nameColumnTitle: 'Locations',
                            url: '/services/license.svc/licenseLocationAssignments/' + me.licenseId + '/',
                            licenseId: me.licenseId,
                            customerId: me.customerId,
                            licenseExpirationRuleId: me.licenseExpirationRuleId,
                            border: false,
                            listeners: {
                                itemdblclick: function (grid, record, item, rowIndex, e) {
                                    me.editAssignment(grid, rowIndex, {
                                        assignmentTab: 'locationassignmenttab',
                                        assignmentPanel: 'addlocationassignmentpanel',
                                        entityType: 'location',
                                        entityIdField: 'locationId',
                                        entityNameField: 'locationName'
                                    });
                                }
                            },
                            editable: me.editable
                        }]
                    },{
                        xtype: 'panel',
                        tbar: {
                            hidden: !me.editable,
                            items: [{
                                xtype: 'button',
                                text: 'Assign License to {0}'.format(Symphony.Aliases.audience),
                                iconCls: 'x-button-add',
                                handler: function () {
                                    me.addAssignment({
                                        assignmentTab: 'audienceassignmenttab',
                                        assignmentPanel: 'addaudienceassignmentpanel',
                                        entityType: 'audience', 
                                        entityIdField: 'audienceId', 
                                        entityNameField: 'audienceName'
                                    });
                                }
                            }]
                        },
                        title: '{0} Licenses'.format(Symphony.Aliases.audience),
                        frame: false,
                        border: false,
                        layout: 'fit',
                        editable: me.editable,
                        items: [{
                            xtype: 'license.genericassignmenttab',
                            name: 'audienceassignmenttab',
                            customerId: me.customerId,
                            licenseId: me.licenseId,
                            nameColumnTitle: "Audience",
                            url:  '/services/license.svc/licenseAudienceAssignments/' + me.licenseId + '/',
                            licenseExpirationRuleId: me.licenseExpirationRuleId,
                            border: false,
                            listeners: {
                                itemdblclick: function (grid, record, item, rowIndex, e) {
                                    me.editAssignment(grid, rowIndex, {
                                        assignmentTab: 'audienceassignmenttab',
                                        assignmentPanel: 'addaudienceassignmentpanel',
                                        entityType: 'audience', 
                                        entityIdField: 'audienceId', 
                                        entityNameField: 'audienceName'
                                    });
                                }
                            },
                            editable: me.editable
                        }]
                    }]
                }]
            });

            this.callParent(arguments);
        },
        addUserAssignment: function (mode) {
            var me = this;
            var win = new Ext.Window({
                tbar: [{
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function () {
                        var licenseStartDate = win.find('name', 'startDate')[0];
                        if (licenseStartDate.getValue() == '') {
                            Ext.Msg.alert('Error', 'Please provide Start Date');
                            return;
                        }

                        var licenseExpiryDate = win.find('name', 'expiryDate')[0];
                        var expiryDate;

                        if (me.licenseExpirationRuleId === 1) {
                            if (licenseExpiryDate.getValue() === '') {
                                Ext.Msg.alert('Error', 'Please provide Expiry Date');
                                return;
                            }

                            expiryDate = licenseExpiryDate.getValue();
                        } else {
                            expiryDate = new Date();
                        }

                        var licenseAssignmentStatus = win.find('name', 'licenseAssignmentStatuses')[0];
                        var locGridUserList = win.find('name', 'assignmentuserlist')[0];
                        var selectedUsersRows = locGridUserList.getPlugin('pagingSelectionPersistence').getPersistedSelection();

                        var locGridUserAssginemntTab = me.find('name', 'userassignmentstab')[0];

                        for (var i = 0; i < selectedUsersRows.length; i++) {
                            var oneuserAssignment = {
                                customerId: selectedUsersRows[i].get('customerId'),
                                startDate: licenseStartDate.getValue().formatSymphony('microsoft'),
                                expiryDate: expiryDate.formatSymphony('microsoft'),
                                assignmentStatusId: licenseAssignmentStatus.getValue(),
                                assignmentStatus: licenseAssignmentStatus.lastSelectionText,
                                firstName: selectedUsersRows[i].get('firstName'),
                                lastName: selectedUsersRows[i].get('lastName'),
                                userId: selectedUsersRows[i].get('id'),
                                parentObject: '',
                                renewalSubmittedDate: '',
                                expiresOnDate: expiryDate.formatSymphony('microsoft'),
                                daysUntilExpiry: '',
                                renewByDate: '',
                                entityName: selectedUsersRows[i].get('firstName') + ' ' + selectedUsersRows[i].get('lastName')
                            };
                            var recordDef = Ext.data.Record.create(Symphony.Definitions.licenseAssignments);
                            var actRecAssignment = new recordDef(oneuserAssignment);
                            locGridUserAssginemntTab.getStore().add(actRecAssignment);
                        }
                        win.destroy();
                    }
                }, {
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        win.destroy();
                    }
                }],
                items: [{
                    xtype: 'license.userassignmentpanel',
                    licenseAssignmentStatuses: me.licenseAssignmentStatuses,
                    border: false,
                    frame: false,
                    customerId: me.customerId,
                    licenseExpirationRuleId: me.licenseExpirationRuleId,
                    name: 'adduserassignmentpanel',
                    layout: 'fit'
                }],
                layout: 'fit',
                modal: true,
                resizable: true,
                title: 'Assign License to User',
                width: 600,
                height: 680
            }).show();
        },
        editUserAssignment: function (grid, rowIndex) {
            var me = this;
            var record = grid.getStore().getAt(rowIndex);
            var editUserFirstName = record.get('firstName');
            var editUserLastName = record.get('lastName');
            var dataAssignment = {
                firstName: editUserFirstName,
                lastName: editUserLastName,
                startDate: Symphony.parseDate(record.get('startDate')),
                expiryDate: Symphony.parseDate(record.get('expiryDate')),
                assignmentStatusId: record.get('assignmentStatusId'),
                licenseAssignmentId: record.get('licenseAssignmentId')
            };
            var win = new Ext.Window({
                tbar: [{
                    text: 'Update',
                    iconCls: 'x-button-edit',
                    handler: function () {
                        var licenseStartDate = win.find('name', 'startDate')[0];
                        if (licenseStartDate.getValue() == '') {
                            Ext.Msg.alert('Error', 'Please provide Start Date');
                            return;
                        }
                        var licenseExpiryDate = win.find('name', 'expiryDate')[0];
                        var expiryDate;

                        if (me.licenseExpirationRuleId === 1) {
                            if (licenseExpiryDate.getValue() === '') {
                                Ext.Msg.alert('Error', 'Please provide Expiry Date');
                                return;
                            }
                            expiryDate = licenseExpiryDate.getValue();
                        
                        } else {
                            expiryDate = new Date();
                        }

                        var licenseAssignmentStatus = win.find('name', 'licenseAssignmentStatuses')[0];
                        var locGridUserList = win.find('name', 'assignmentuserlist')[0];

                        var locGridUserAssignmentTab = me.find('name', 'userassignmentstab')[0];

                        var oneUserAssignmentUpdate = {
                            customerId: me.customerId,
                            startDate: licenseStartDate.getValue().formatSymphony('microsoft'),
                            expiryDate: expiryDate.formatSymphony('microsoft'),
                            assignmentStatusId: licenseAssignmentStatus.getValue(),
                            assignmentStatus: licenseAssignmentStatus.lastSelectionText
                        };
                        var recordDef = Ext.data.Record.create(Symphony.Definitions.licenseAssignments);
                        var actRecAssignment = new recordDef(oneUserAssignmentUpdate);

                        locGridUserAssignmentTab.updateRecord(actRecAssignment);

                        // file uploads: 
                        var swf = Ext.ComponentQuery.query('[name=licenseAssignmentUploader]')[0];

                        if (swf && swf.rendered) {
                            swf.addParameter('licenseAssignmentId', dataAssignment.licenseAssignmentId);
                            swf.on('allcomplete', function () {
                                //console.log("SWF UPlOAD COMPLETE:", arguments);
                                win.destroy();
                            });
                            swf.uploadAll();
                        } else
                            win.destroy(); {
                        }

                    }
                }, {
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        win.destroy();
                    }
                }],
                items: [{
                    xtype: 'license.userassignmentpanel',
                    licenseAssignmentStatuses: me.licenseAssignmentStatuses,
                    licenseExpirationRuleId: me.licenseExpirationRuleId,
                    layout: 'fit',
                    border: false,
                    frame: false,
                    mode: 'edit',
                    customerId: me.customerId,
                    data: dataAssignment,
                    name: 'edituserassignmentpanel'
                }],
                modal: true,
                resizable: true,
                layout: 'fit',
                title: 'Edit User\'s License',
                width: 600,
                height: 680
            }).show();

        },

        addAssignment: function (entityInfo) {
            var me = this;
            var win = new Ext.Window({
                tbar: [{
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function () {
                        var licenseStartDate = win.find('name', 'startDate')[0];
                        var licenseExpiryDate = win.find('name', 'expiryDate')[0];
                        var expiryDate;

                        if (me.licenseExpirationRuleId === 1) {
                            if (licenseExpiryDate.getValue() === '') {
                                Ext.Msg.alert('Error', 'Please provide Expiry Date');
                                return;
                            }
                            expiryDate = licenseExpiryDate.getValue();
                        } else {
                            expiryDate = new Date();
                        }

                        var licenseAssignmentStatus = win.find('name', 'licenseAssignmentStatuses')[0];

                        var locgrid = me.find('name', entityInfo.assignmentTab)[0];

                        var recordDef = Ext.data.Record.create(Symphony.Definitions.licenseAssignments);

                        var tree = win.find('xtype', 'courseassignment.trainingprogramhierarchylist')[0];
                        var nodes = tree.getView().getChecked();

                        for (var i = 0; i < nodes.length; i++) {
                            var oneAssignment = {
                                customerId: me.customerId,
                                startDate: licenseStartDate.getValue().formatSymphony('microsoft'),
                                expiryDate: expiryDate.formatSymphony('microsoft'),
                                assignmentStatusId: licenseAssignmentStatus.getValue(),
                                assignmentStatus: licenseAssignmentStatus.lastSelectionText,
                                entityId: nodes[i].get('id'),

                                entityType: entityInfo.entityType,

                                entityName: nodes[i].get('name'),
                                renewalSubmittedDate: '',
                                expiresOnDate: expiryDate.formatSymphony('microsoft'),
                                daysUntilExpiry: '',
                                renewByDate: ''
                            };

                            // Preserving old data contract, tho moving toward reuse and generification
                            oneAssignment[entityInfo.entityIdField] = nodes[i].get('id');
                            oneAssignment[entityInfo.entityNameField] = nodes[i].get('name');

                            var actRecAssignment = new recordDef(oneAssignment);
                            locgrid.getStore().add(actRecAssignment);
                        }

                        win.destroy();
                    }
                }, {
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        win.destroy();
                    }
                }],
                items: [{
                    xtype: 'license.genericassignmentpanel',
                    licenseAssignmentStatuses: me.licenseAssignmentStatuses,
                    licenseExpirationRuleId: me.licenseExpirationRuleId,
                    frame: false,
                    border: false,
                    name: me.assignmentPanel,
                    customerId: me.customerId,
                    urlsource: '/services/customer.svc/',

                    // not a fan of the pluralization, makes for weird concats
                    urlFragment: (entityInfo.entityType).toLowerCase() + 's/customer/' + (me.customerId ? me.customerId : Symphony.User.customerId),

                    entityType: entityInfo.entityType,
                    layout: 'fit'
                }],
                modal: true,
                layout: 'fit',
                resizable: true,
                title: 'Assign License to {0}'.format(Symphony.Aliases[entityInfo.entityType]),
                width: 600,
                height: 600
            }).show();
        },
        editAssignment: function (grid, rowIndex, entityInfo) {
            var me = this;

            var rec = grid.getStore().getAt(rowIndex);
            var selectedNodeId = rec.get(entityInfo.entityIdField);
            var assignmentData = {
                startDate: rec.get('startDate'),
                expiryDate: rec.get('expiryDate'),
                assignmentStatusId: rec.get('assignmentStatusId'),
                licenseAssignmentId: rec.get('licenseAssignmentId')
            };

            var win = new Ext.Window({
                tbar: [{
                    text: 'Update',
                    iconCls: 'x-button-edit',
                    handler: function () {
                        var licenseStartDate = win.find('name', 'startDate')[0];
                        var licenseExpiryDate = win.find('name', 'expiryDate')[0];
                        var licenseAssignmentStatus = win.find('name', 'licenseAssignmentStatuses')[0];

                        var locgrid = me.find('name', entityInfo.assignmentTab)[0];

                        var recordDef = Ext.data.Record.create(Symphony.Definitions.licenseAssignments);

                        var oneAssignment = {
                            customerId: me.customerId,
                            startDate: licenseStartDate.getValue().formatSymphony('microsoft'),
                            expiryDate: licenseExpiryDate.getValue().formatSymphony('microsoft'),
                            assignmentStatusId: licenseAssignmentStatus.getValue(),
                            assignmentStatus: licenseAssignmentStatus.lastSelectionText
                        };
                        var actRecAssignment = new recordDef(oneAssignment);
                        locgrid.updateRecord(actRecAssignment);

                        // file uploads: 
                        var swf = Ext.ComponentQuery.query('[name=licenseAssignmentUploader]')[0];

                        if (swf && swf.rendered) {
                            swf.addParameter('licenseAssignmentId', assignmentData.licenseAssignmentId);
                            swf.on('allcomplete', function () {
                                //console.log("SWF UPlOAD COMPLETE:", arguments);
                                win.destroy();
                            });
                            swf.uploadAll();
                        } else
                            win.destroy(); {
                        }


                    }
                }, {
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        win.destroy();
                    }
                }],
                items: [{
                    xtype: 'license.genericassignmentpanel',
                    licenseAssignmentStatuses: me.licenseAssignmentStatuses,
                    licenseExpirationRuleId: me.licenseExpirationRuleId,
                    selectedNodeId: selectedNodeId,
                    data: assignmentData,
                    frame: false,
                    mode: 'edit',
                    border: false,
                    customerId: me.customerId,
                    urlSource: '/services/customer.svc/',

                    name: entityInfo.assignmentPanel,
                    urlFragment: (entityInfo.entityType).toLowerCase() + 's/customer/' + (me.customerId ? me.customerId : Symphony.User.customerId),
                    entityType: entityInfo.entityType,
                    
                    layout: 'fit'
                }],
                modal: true,
                layout: 'fit',
                resizable: true,
                title: 'Assign License to {0}'.format(Symphony.Aliases[entityInfo.entityType]),
                width: 600,
                height: 600
            }).show();
        }
    });


    //User
    Symphony.License.UserAssignmentPanel = Ext.define('license.userassignmentpanel', {
        alias: 'widget.license.userassignmentpanel',
        extend: 'Ext.Panel',
        mode: '',
        customerId: 0,
        licenseAssignmentStatuses: {},
        data: {},
        initComponent: function () {
            var me = this;

            var itemsList = [{
                xtype: 'fieldset',
                height: 100,
                region: 'north',
                border: false,
                frame: false,
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'datefield',
                    name: 'startDate',
                    value: me.data.startDate,
                    fieldLabel: 'Start Date'
                }, {
                    xtype: 'datefield',
                    name: 'expiryDate',
                    disabled: me.licenseExpirationRuleId !== 1,
                    hidden: me.licenseExpirationRuleId !== 1,
                    fieldLabel: 'Expiry Date',
                    value: me.data.expiryDate || Date.now()
                }, Symphony.getCombo('licenseAssignmentStatuses', 'Status', me.licenseAssignmentStatuses, { value: me.data.assignmentStatusId }, {
                    valueField: 'id',
                    displayField: 'text'
                })]
            }];
            
            if (me.mode === 'edit') {
                itemsList.push({
                    name: 'notescontainertabpanel',
                    xtype: 'tabpanel',
                    border: false,
                    cls: 'x-panel-transparent',
                    activeTab: 0,
                    region: 'center',
                    items: [{
                        title: 'Notes',
                        xtype: 'license.assignmentnotespanel',
                        data: me.data
                    }, {
                        title: 'Documents',
                        xtype: 'license.assignmentdocumentspanel',
                        data: me.data
                    }]
                });
            } else {
                itemsList.push({
                    xtype: 'license.userlist',
                    name: 'assignmentuserlist',
                    region: 'center',
                    border: false,
                    customerId: me.customerId,
                    editUser: me.data,
                    mode: me.mode,
                    value: []
                })
            }

            Ext.apply(me, {
                items: [{
                    xtype: 'panel',
                    layout: 'border',
                    frame: false,
                    border: false,
                    items: itemsList
                }]
            });
            this.callParent(arguments);
        }
    });

    Symphony.License.UserList = Ext.define('license.userlist', {
        alias: 'widget.license.userlist',
        extend: 'Symphony.SearchableGrid',
        editUser: {},
        customerId: 0,
        singleSelect: false,
        mode: '',
        initComponent: function () {

            var me = this;

            var url = '/Services/customer.svc/users/customer/{0}'.format((me.customerId ? me.customerId : Symphony.User.customerId));

            var sm = new Ext.selection.CheckboxModel({
                checkOnly: !me.singleSelect,
                sortable: true,
                header: ' ', // kills the select-all checkbox
                mode: me.singleSelect ? 'SINGLE' : 'MULTI'
            });

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [
                    { id: 'firstName', header: 'First Name', dataIndex: 'firstName' },
                    { id: 'lastName', header: 'Last Name', dataIndex: 'lastName' }
                ]
            });

            Ext.apply(this, {
                autoExpandColumn: 'lastName',
                idProperty: 'id',
                url: url,
                disabled: me.mode == 'edit' ? true : false,
                colModel: colModel,
                model: 'user',
                selModel: sm,
                plugins: [{ ptype: 'pagingselectpersist' }],
                tbar: {
                    items: [{
                        xtype: 'checkbox',
                        boxLabel: 'Display Only Selected Users',
                        hidden: me.singleSelect,
                        listeners: {
                            check: function (checkbox, checked) {
                                if (checked) {
                                    me.store.on('beforeload', me.selectedUserFilter, me);
                                } else {
                                    me.store.proxy.url = me.url;
                                    me.store.proxy.method = 'GET';
                                    me.store.proxy.jsonData = null;
                                    me.store.un('beforeload', me.selectedUserFilter, me);
                                }
                                me.refresh();
                            }
                        }
                    }]
                },
                listeners: {
                    'render': function () {
                        if (me.mode == 'edit') {
                            me.store.on('beforeload', function () {
                                var recordDef = Ext.data.Record.create(Symphony.Definitions.user);
                                var actRecAssignment = new recordDef({ firstName: me.editUser.firstName, lastName: me.editUser.lastName });
                                me.store.add(actRecAssignment);
                                return false;
                            });
                        }
                    }
                }
            });
            this.callParent(arguments);
        },
        selectedUserFilter: function (store, options) {
            store.proxy.url = '/services/courseassignment.svc/specificusers/';
            store.proxy.method = 'POST';
            store.proxy.jsonData = this.selectionPaging.getSelections();
        },
        getSelections: function () {
            var selectedIds = this.getPlugin('pagingSelectionPersistence').getPersistedSelection(), results = [];
            for (var i = 0; i < selectedIds.length; i++) {
                results.push({ hierarchyTypeId: Symphony.HierarchyType.user, hierarchyNodeId: selectedIds[i] });
            }
            return results;
        },
        setSelections: function (value) {
            this.getPlugin('pagingSelectionPersistence').setSelections(value);
        },
        clearSelections: function () {
            this.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
        }
    });
    
    Symphony.License.AssignmentNotesPanel = Ext.define('license.assignmentnotespanel', {
        alias: 'widget.license.assignmentnotespanel',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                border: false,
                frame: false,
                cls: 'x-panel-transparent',
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    pack: 'start'
                },
                items: [{
                    xtype: 'panel',
                    height: 70,
                    border: false,
                    frame: false,
                    cls: 'x-panel-transparent',
                    layout: {
                        type: 'hbox',
                        defaultMargins: 5 
                    },
                    items: [{
                        xtype: 'textarea',
                        name: 'notebody',
                        emptyText: 'Add a note to this assignment',
                        flex: 5
                    }, {
                        xtype: 'button',
                        text: 'Save Note',
                        iconCls: 'x-button-add',
                        handler: function () {
                            var noteBody = Ext.ComponentQuery.query('[name=notebody]')[0];
                            if (!noteBody.value) {
                                return;
                            }

                            // save note
                            Symphony.Ajax.request({
                                url: '/services/license.svc/notes',
                                jsonData: {
                                    licenseAssignmentId: me.data.licenseAssignmentId,
                                    body: noteBody.value
                                },
                                success: function (data) {
                                    console.log("SAVE NOTE SUCCESS: ", data, arguments);
                                    // clear the note element 
                                    noteBody.setValue("");
                                    // .. and reload the notes list
                                    var notesPanel = Ext.getCmp('licenseAssignmentNotePanel');

                                    notesPanel ? notesPanel.refresh() : console.warn('Cannot find notes grid');
                                },
                                failure: function (err) {
                                    // if error, show message and set focus
                                    console.log("SAVE NOTE FAILED: ", err, arguments);
                                }
                            });
                        },
                        flex: 1
                    }]
                }, {
                    xtype: 'license.notespanel',
                    id: 'licenseAssignmentNotePanel',
                    data: me.data,
                    border: false,
                    flex: 1
                }]
            });

            this.callParent(arguments);
        }
    });

    Symphony.License.AssignmentDocumentsPanel = Ext.define('license.assignmentdocumentspanel', {
        alias: 'widget.license.assignmentdocumentspanel',
        extend: 'Ext.Panel',
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'fit',
                border: false,
                items: [{
                    xtype: 'license.documentspanel',
                    border: false,
                    data: me.data,
                    listeners: {
                        assignmentDocumentUploaded: function () {
                            this.refresh();
                        },
                        afterlayout: function (panel) {
                            if (me.data.licenseAssignmentId == 0) {
                                panel.getEl().mask("This license assignment must be saved prior to uploading documents. Click \"Save License\" to apply all of your current license assignment changes.", "disabled-mask");
                            }
                        }
                    }
                }]
            });

            this.callParent(arguments);
        }
    });

    // generic assignment tab panel
    Symphony.License.GenericAssignmentPanel = Ext.define('license.genericassignmentpanel', {
        alias: 'widget.license.genericassignmentpanel',
        extend: 'Ext.Panel',
        mode: '',
        border: false,
        customerId: 0,
        selectedNodeId: 0,
        licenseExpirationRuleId: 0,
        data: {},
        editUser: {},
        licenseAssignmentStatuses: {},
        initComponent: function () {
            var me = this;

            var itemsList = [{
                xtype: 'fieldset',
                height: 100,
                region: 'north',
                border: false,
                frame: false,
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'datefield',
                    name: 'startDate',
                    value: me.data.startDate ? Symphony.parseDate(me.data.startDate).formatSymphony('m/d/Y') : '',
                    fieldLabel: 'Start Date'
                }, {
                    xtype: 'datefield',
                    name: 'expiryDate',
                    disabled: me.licenseExpirationRuleId !== 1,
                    hidden: me.licenseExpirationRuleId !== 1,
                    fieldLabel: 'Expiry Date',
                    value: me.data.expiryDate ? Symphony.parseDate(me.data.expiryDate).formatSymphony('m/d/Y') : Date.now()
                }, Symphony.getCombo('licenseAssignmentStatuses', 'Status', me.licenseAssignmentStatuses, { value: me.data.assignmentStatusId }, {
                    valueField: 'id',
                    displayField: 'text'
                })]
            }];

            if (me.mode === 'edit') {
                itemsList.push({
                    name: 'notescontainertabpanel',
                    xtype: 'tabpanel',
                    frame: false,
                    border: false,
                    activeTab: 0,
                    region: 'center',
                    cls: 'x-panel-transparent',
                    items: [{
                        title: 'Notes',
                        xtype: 'license.assignmentnotespanel',
                        data: me.data
                    }, {
                        title: 'Documents',
                        xtype: 'license.assignmentdocumentspanel',
                        data: me.data
                    }]
                });
            } else {
                var entityListView = {
                    title: Symphony.Aliases[me.entityType],
                    tabTip: 'Create and manage {0}'.format(Symphony.Aliases[me.entityType]),
                    region: 'center',
                    urlsource: me.urlsource,
                    urlFragment: me.urlFragment,
                    disabled: me.mode === 'edit',
                    hidden: me.mode === 'edit',
                    value: [],
                    border: false,
                    customerId: me.customerId,
                    iconCls: 'x-location',
                    hierarchyName: me.hierarchyName,
                    listeners: {
                        'treedataloaded': function (tree) {
                            if (me.selectedNodeId > 0) {
                                tree.setStateCheckedOnNode(me.selectedNodeId);
                            }
                        }
                    }
                };

                entityListView.xtype = me.entityType === 'user' ? 'license.userlist' : 'courseassignment.trainingprogramhierarchylist';

                itemsList.push(entityListView);
            }

            Ext.apply(me, {
                items: [{
                    xtype: 'panel',
                    layout: 'border',
                    border: false,
                    frame: false,
                    items: itemsList
                }]
            });
            this.callParent(arguments);
        }
    });


}(window));
