﻿(function () {
    Symphony.License.LicenseUserFieldsComboBox = Ext.define('license.userfieldscombo', {
        alias: 'widget.license.userfieldscombo',
        extend: 'Ext.ux.form.field.BoxSelect',
        initComponent: function () {
            var data = [];
            var me = this;

            for (var i = 0; i < Symphony.UserCertificateFields.length; i++) {
                data.push([Symphony.UserCertificateFields[i], Symphony.toHumanCase(Symphony.UserCertificateFields[i])]);
            }
            var store = new Ext.data.ArrayStore({
                fields: ['id', 'name'],
                data: data
            });
            Ext.apply(this, {
                queryMode: 'local',
                triggerAction: 'all',
                queryDelay: 0,
                minChars: 1,
                store: store,
                allowBlank: true,
                //msgTarget: 'under',
                allowAddNewData: false,
                    
                //fieldLabel: 'Tags',
                emptyText: 'Enter or select user fields',
                resizable: true,
                anchor: '100%',
                displayField: 'name',
                valueField: 'id',
                //value: 'Sport,Science',
                extraItemCls: 'x-tag' + (me.readOnly ? ' read-only' : ''),
                renderFieldBtns: !me.readOnly,
                backspaceDeletesLastItem: !me.readOnly,
                itemCls: me.readOnly ? 'read-only' : '',
                listeners: {
                    beforeadditem: function (bs, v) {
                        //console.log('beforeadditem:', v);
                        //return false;
                    },
                    additem: function (bs, v) {
                        //console.log('additem:', v);
                    },
                    beforeremoveitem: function (bs, v) {
                        //console.log('beforeremoveitem:', v);
                        //return false;
                    },
                    removeitem: function (bs, v) {
                        //console.log('removeitem:', v);
                    }
                }
            });
            this.callParent(arguments);
        },
        setValue: function (v) {
            Symphony.License.LicenseUserFieldsComboBox.superclass.setValue.apply(this, arguments);
        }
    });

})();