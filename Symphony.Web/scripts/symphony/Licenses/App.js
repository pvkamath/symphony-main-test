﻿(function () {
    Symphony.License.App = Ext.define('licenses.app', {
        alias: 'widget.licenses.app',
        extend: 'Ext.Panel',
        editable: true,
        initComponent: function () {
            var me = this;
            Ext.apply(me, {
                layout: 'border',
                border: false,
                items: [{
                    split: true,
                    region: 'west',
                    border: false,
                    width: 250,
                    xtype: 'license.licensetree',
                    editable: me.editable,
                    listeners: {
                        addrecord: function (record) {
                            me.addPanel(0, 'New License', record);
                        },
                        itemclick: function (tree, record, item, index, e, eOpts) {
                            me.addPanel(record.get('id'), record.get('name'), record);
                            me.fireEvent('selectedNode', record);
                        },
                        'delete': function (selectedNode) {
                            if (!selectedNode) {
                                Ext.Msg.alert('Error', 'Please select a license.');
                                return;
                            }
                            var id = selectedNode.attributes.record.get('id');

                            // loop through the tab items and remove the deleted license's panel

                            // NOTE May 2016 - Using .query() or .find() (etc.) seems to only 
                            // return the lowest desendents matching the query (licenseId=id) :(
                            // for now, loop through the items.items b/c it works. 
                            var tabItems = me.tabPanel.items.items; 
                            for (var i = 0; i < tabItems.length; i++) {
                                if (tabItems[i].licenseId === id) {
                                    me.tabPanel.remove(tabItems[i]);
                                    break;
                                }
                            }
                        }
                    }
                }, {
                    border: false,
                    region: 'center',
                    //layout: 'fit',
                    enableTabScroll: true,
                    xtype: 'tabpanel',
                    ref: 'tabPanel'
                }],
                addPanel: function (licenseId, licenseName, record) {
                    var me = this;

                    var tabPanel = me.tabPanel;
                    var found = tabPanel.find('licenseId', licenseId);

                    if (!found.length) {
                        // Clear any old panels, then add new panel
                        //var f;
                        //while (f = tabPanel.items.first()) {
                        //    tabPanel.remove(f, true);
                        //}

                        var panel = tabPanel.add({
                            xtype: 'license.licensepanel',
                            closable: true,
                            activate: true,
                            record: record,
                            title: licenseName,
                            licenseId: licenseId,
                            customerId: 0,
                            editable: me.editable,
                            border: false,
                            listeners: {
                                refreshTree: function () {
                                    me.refreshLicenses();
                                },
                                'cancel': function () {
                                    tabPanel.remove(panel);
                                }
                            }
                        });
                        panel.show();
                    } else {
                        tabPanel.setActiveTab(found[0]);
                    }
                    tabPanel.doLayout();
                }
            });
            this.callParent();

            this.on('activate', function (panel) {
                panel.refreshLicenses();
            }, this, { single: true });
        },
        refreshLicenses: function () {
            var licenseTree = this.query('[xtype=license.licensetree]')[0];
            if (licenseTree) {
                licenseTree.refresh();
            }
        }
    });
})();
