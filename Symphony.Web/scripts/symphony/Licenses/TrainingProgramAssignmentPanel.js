﻿(function () {
    Symphony.License.TrainingProgramAssignmentPanel = Ext.define('license.trainingprogramassignmentpanel', {
        alias: 'widget.license.trainingprogramassignmentpanel',
        extend: 'Ext.Panel',
        trainingProgramId: 0,
        isLockBySharedTrainingFlag: false,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'fit',
                border: true,
                tbar: {
                    items: [{
                        xtype: 'button',
                        disabled: this.isLockBySharedTrainingFlag,
                        text: 'Add License',
                        iconCls: 'x-button-add',
                        handler: function () {
                            Symphony.License.getLicenseSelectorWindow(function (node) {
                                me.licenseAssignmentGrid.editLicense(node.attributes.record);
                            });

                        }
                    }]
                },
                items: [{
                    xtype: 'license.trainingprogramassignmentgrid',
                    border: false,
                    disabled: this.isLockBySharedTrainingFlag,
                    ref: 'licenseAssignmentGrid',
                    readOnly: me.readOnly,
                    listeners: {
                        rowclick: function (grid, rowIndex, e) {
                            me.fireEvent('rowclick', grid, rowIndex, e);
                        },
                        itemdblclick: function (grid, record, item, rowIndex, e) {
                            me.fireEvent('itemdblclick', grid, rowIndex, e, record);
                        }
                    }
                }]
            });

            this.callParent(arguments);
        },
        getData: function () {
            var results = [];
            var items = this.query('[xtype=license.trainingprogramassignmentgrid]')[0].store.data.items;

            for (var i = 0; i < items.length; i++) {
                if (items[i].data.parentId === 'root') {
                    items[i].data.parentId = 0;
                }

                results.push(items[i].data);
            }

            return results;
        },
        setData: function (data) {
            this.query("[xtype=license.trainingprogramassignmentgrid]")[0].setData(data);
        }
    });


})();