﻿(function (window, undefined) {

    Symphony.License.GeneralTab = Ext.define('license.generaltab', {
        alias: 'widget.license.generaltab',
        extend: 'Ext.Panel',
        deferLoad: false,
        editable: true,
        record: null,
        customerId: 0,
        initComponent: function () {
            
            var me = this;

            Ext.apply(this, {
                layout: 'fit',
                autoScroll: true,
                border: false,
                items: [{
                    name: 'general',
                    xtype: 'form',
                    bodyStyle: 'background-color: #DFE8F6',
                    border: false,
                    frame: true,
                    items: [{
                        xtype: 'fieldset',
                        title: 'Basic Information' + (!me.editable ? ' (Read Only)' : ''),
                        defaults: {
                            border: false,
                            cls: 'x-panel-transparent'
                        },
                        items: [{
                            layout: 'column',
                            cls: 'x-panel-transparent',
                            border: false,
                            bodyStyle: 'padding-bottom: 10px',
                            // column defaults
                            defaults: {
                                columnWidth: 0.5,
                                border: false,
                                xtype: 'form',
                                cls: 'x-panel-transparent',
                                layout: 'anchor'
                            },
                            items: [{
                                //left column
                                defaults: { anchor: '95%', xtype: 'textfield' },
                                items: [{
                                    name: 'licenseId',
                                    fieldLabel: 'ID',
                                    allowBlank: false,
                                    xtype: 'hidden',
                                    value: me.record.get('id')
                                }, {
                                    name: 'name',
                                    fieldLabel: 'Name',
                                    allowBlank: false,
                                    readOnly: !me.editable,
                                    value: me.record.get('name')
                                }, {
                                    xtype: 'radio',
                                    name: 'expirationRuleId',
                                    fieldLabel: 'Expiration Rules',
                                    inputValue: Symphony.LicenseExpirationRule.individual,
                                    afterText: 'Specify expiry date for each individual license',
                                    handler: Ext.bind(me.changeExpirationRule, me),
                                    readOnly: !me.editable
                                }, {
                                    xtype: 'radio',
                                    name: 'expirationRuleId',
                                    inputValue: Symphony.LicenseExpirationRule.daysAfterStart,
                                    hideEmptyLabel: false,
                                    afterText: 'Specify number of days after Start date',
                                    handler: Ext.bind(me.changeExpirationRule, me),
                                    readOnly: !me.editable
                                }, {
                                    xtype: 'numberfield',
                                    hideEmptyLabel: false,
                                    name: 'expirationRuleAfterDays',
                                    allowBlank: false,
                                    readOnly: !me.editable
                                }, {
                                    xtype: 'radio',
                                    inputValue: Symphony.LicenseExpirationRule.endOfYear,
                                    name: 'expirationRuleId',
                                    hideEmptyLabel: false,
                                    afterText: 'Expiry at the end of the year',
                                    handler: Ext.bind(me.changeExpirationRule, me),
                                    readOnly: !me.editable
                                }]
                            }, {
                                //Right column
                                items: [{
                                    name: 'customerId',
                                    bindingName: 'customerName',
                                    valueNotFoundText: 'All',
                                    emptyText: 'All',
                                    xtype: 'symphony.pagedcombobox',
                                    url: '/services/customer.svc/customers/',
                                    recordDef: Ext.data.Record.create(Symphony.Definitions.customer),
                                    model: 'customer',
                                    fieldLabel: 'Customer',
                                    fieldWidth: 140,
                                    allowBlank: true,
                                    forceSelection: true,
                                    anchor: '100%',
                                    displayField: 'name',
                                    valueField: 'id',
                                    listeners: {
                                        select: function (cbo, record, index) {
                                            if (Ext.isArray(record)) {
                                                record = record[0];
                                            }

                                            me.customerId = record.get('id');
                                            me.fireEvent('customerchanged', record.get('id'));
                                        }
                                    },
                                    readOnly: !me.editable
                                }, {
                                    xtype: 'symphony.pagedcombobox',
                                    name: 'accreditationBoardId',
                                    bindingName: 'accreditationBoard',
                                    readOnly: !me.editable,
                                    url: '/services/Accreditation.svc/accreditationBoards/',
                                    model: 'AccreditationBoard',
                                    fieldLabel: 'Accredited By',
                                    fieldWidth: 140,
                                    clearable: true,
                                    allowBlank: true,
                                    forceSelection: true,
                                    anchor: '100%',
                                    displayField: 'name',
                                    valueField: 'id'
                                }, {
                                    name: 'jurisdictionId',
                                    bindingName: 'jurisdiction',
                                    xtype: 'symphony.pagedcombobox',
                                    fieldLabel: 'Jurisdiction',
                                    fieldWidth: 140,
                                    allowBlank: false,
                                    readOnly: !me.editable,
                                    url: '/services/license.svc/jurisdiction/',
                                    model: 'jurisdiction',
                                    allowBlank: false,
                                    forceSelection: false,
                                    emptyText: 'Enter or select a jurisdiction',
                                    anchor: '100%',
                                    displayField: 'name',
                                    valueField: 'id'
                                }, {
                                    name: 'professionId',
                                    bindingName: 'profession',
                                    xtype: 'symphony.pagedcombobox',
                                    fieldLabel: 'Profession',
                                    fieldWidth: 140,
                                    allowBlank: false,
                                    readOnly: !me.editable,
                                    url: '/services/license.svc/profession/',
                                    model: 'profession',
                                    allowBlank: false,
                                    forceSelection: false,
                                    emptyText: 'Enter or select a profession',
                                    anchor: '100%',
                                    displayField: 'name',
                                    valueField: 'id'
                                }]
                            }]
                        }, {
                            xtype: 'panel',
                            layout: 'form',
                            // Full width column
                            defaults: { anchor: '100%', xtype: 'textfield', columnWidth: 1 },
                            items: [{
                                name: 'description',
                                fieldLabel: 'Description',
                                allowBlank: true,
                                xtype: 'textarea',
                                readOnly: !me.editable,
                                value: me.record.get('description')
                            }, {
                                name: 'hasUserLicenseNumber',
                                fieldLabel: 'Request License Number',
                                xtype: 'checkbox',
                                readOnly: !me.editable,
                                disabled: !me.editable
                            }, {
                                name: 'renewalLeadTimeInDays',
                                fieldLabel: 'Renewal Lead Time (in Days)',
                                value: me.record.get('renewalLeadTimeInDays')
                            }]
                        }]
                    }],
                    listeners: {                            
                        rename: function (id, name) {
                            var container = me.find('name', 'center-content')[0];
                            var panel = container.findBy(function (subPanel) {
                                return subPanel.salesChannelId == id;
                            })[0];
                            if (panel) {
                                panel.setTitle(name);
                            }
                        }
                    }
                }]
            });
            this.callParent(arguments);

            this.load();
        },
        load: function() {
            var me = this;
            
            if (this.record.get('id') > 0) {
                Ext.Msg.wait('Please wait while your license is loaded...', 'Please wait...');

                Symphony.Ajax.request({
                    method: 'GET',
                    url: '/services/license.svc/license/' + this.record.get('id'),
                    success: function (result) {
                        me.fireEvent('licenseloaded', result.data);
                        Ext.Msg.hide();
                        me.find('name', 'general')[0].bindValues(result.data);
                    }
                });
            } 
        },
        save: function (assignmentData, formFieldJson) {

            var me = this;
                            
            var form = me.find('name', 'general')[0].getForm();
            
            if (!form.isValid()) {
                Ext.Msg.alert('Errors Detected', 'Your license has some errors. Please correct and re-save');
                return;
            }

            var data = form.getValues();

            data.certificateFormFieldJson = formFieldJson;

            data.licenseAssignments = assignmentData.assignmentsUserDataList;
            data.licenseAssignmentTypes = assignmentData.licenseAssignmentTypes;

            data.customerId = me.find('name', 'customerId')[0].value;
            if (data.customerId == '') {
                data.customerId = 0;
            };
            data.parentId = me.record.get('parentId');

            if (!data.parentId) {
                data.parentId = 0;
            }

            if (data.parentId == 'root') {
                data.parentId = 0;
            }


            data.jurisdiction = data.jurisdictionId;
            delete data.jurisdictionId;

            data.profession = data.professionId;
            delete data.professionId;

            if (data.expirationRuleId != Symphony.LicenseExpirationRule.daysAfterStart) {
                data.expirationRuleAfterDays = null;
            }
            
            if (data.hasUserLicenseNumber == 'on') {
                data.hasUserLicenseNumber = true;
            } else {
                data.hasUserLicenseNumber = false;
            }

            // null renewal time if empty string
            if (!data.renewalLeadTimeInDays) {
                data.renewalLeadTimeInDays = null;
            }

            // Set default value for Accreditation Board. 
            if (!data.accreditationBoardId) {
                data.accreditationBoardId = -1; // Using -1 as the default to help ID empty values in the future.
            }
            
            Ext.Msg.wait('Please wait while your license is saved...', 'Please wait...');

            // save and process files
            Symphony.Ajax.request({
                url: '/services/license.svc/license/' + this.record.get('id'),
                jsonData: data,
                success: function (result) {
                    Ext.Msg.hide();
                    me.record.set('id', result.data.id)
                    me.fireEvent('save', result);
                }
            });
        },
        getValues: function () {
            var me = this;

            if (!me.form.isValid()) {
                return;
            }

            var result = me.form.getValues();
            var dt = Symphony.parseDate(result.date);
            var hours = result.time.split(':')[0];
            var minutes = result.time.split(':')[1].split(' ')[0];
            var amPm = result.time.split(':')[1].split(' ')[1];
            if (amPm == 'PM' && hours != '12') {
                hours = hours + 12;
            }
            else if (amPm == 'AM' && hours == '12') {
                hours = 0;
            }
            dt = dt.add(Date.HOUR, hours);
            dt = dt.add(Date.MINUTE, minutes);

            result.date = dt.formatSymphony('microsoft');
            return result;
        },
        changeExpirationRule: function (opt, checked) {
            if (checked) {
                this.fireEvent('licenseExpirationRuleChanged', opt.inputValue);
            }
        }
    });



} (window));
