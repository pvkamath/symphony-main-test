﻿(function () {

    // Ad hoc delete handler
    function DeleteRecord(recordId, callback) {
        Ext.Ajax.request({
            url: "/services/license.svc/assignment/delete/" + recordId,
            method: "DELETE",
            success: function (data) {
                console.log("DELETE SUCCESS:", data);
                callback(null, data);
            },
            failure: function (error) {
                console.log("DELETE failure:", error);
                callback(error);
            }
        });
    }

    Symphony.License.UserAssignmentTab = Ext.define('license.userassignmenttab', {
        alias: 'widget.license.userassignmenttab',
        extend: 'symphony.editableunpagedgrid',
        initComponent: function () {
            var me = this;

            var url = me.url;

            var cache = {};

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left'
                },
                columns: [
                    { header: 'ID', dataIndex: 'id', hidden: true },
                    { dataIndex: 'deleteUser', renderer: Symphony.deleteRenderer, header: '', width: 30, hidden: !me.editable },
                    { header: 'License',  dataIndex: 'licenseName' },
                    { header: 'Start Date', dataIndex: 'startDate', renderer: function (value) { return Symphony.parseDate(value).formatSymphony('m/d/Y'); } },
                    { header: 'Expiry Date', dataIndex: 'expiresOnDate', renderer: function (value) { return Symphony.parseDate(value).formatSymphony('m/d/Y'); } },
                    { header: 'Days Until Expiry', dataIndex: 'daysUntilExpiry' },
                    { header: 'User Id', dataIndex: 'userId', hidden: true },
                    { header: 'First Name', dataIndex: 'firstName' },
                    { header: 'Last Name', dataIndex: 'lastName' },
                    { header: 'Assignment Status Id', dataIndex: 'assignmentStatusId', hidden: true },
                    { header: 'Assignment Status', dataIndex: 'assignmentStatus', width: 150 },
                    { header: 'Belongs To', dataIndex: 'parentObject' },
                    { header: 'Renew Before', dataIndex: 'renewByDate', renderer: function (value) { return Symphony.parseDate(value).formatSymphony('m/d/Y'); } }
                ]
            });

            Ext.apply(this, {
                url: url,
                colModel: colModel,
                model: 'licenseAssignments',
                layout: 'fit',
                tbar: false ? { // hiding the button for now. - NB Apr 15, 2016
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                            console.log("LICENSE PANEL (USERS)");
                        }
                    }]
                } : {},
                listeners: {
                    datagridloaded: function () {
                        var reccnt = this.getStore().getCount();
                        for (var i = 0; i < reccnt; i++) {
                            if (this.getStore().getAt(i).get('parentObject') == '') continue;
                            this.getView().getRow(i).style.backgroundColor = '#EFEFEF';
                        }

                    },
                    cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e) {
                        if (record.get('parentObject') != '') return;
                        if (grid.getColumnModel().getColumnAt(columnIndex).dataIndex == 'deleteUser') {
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete?', function (btn) {
                                if (btn == 'yes') {
                                    DeleteRecord(record.data.licenseAssignmentId, function (err, data) {
                                        if (err) {
                                            console.error("Delete failed: ", err);
                                            return;
                                        }
                                        grid.getStore().remove(record);
                                        console.log("Delete success: ", data);
                                    });
                                }
                            });
                        }
                    }
                }
            });

            this.callParent(arguments);
        },
        updateRecord: function (record) {
            var curEditRecord = this.getSelectionModel().getSelection()[0];
            curEditRecord.set('assignmentStatusId', record.data.assignmentStatusId);
            curEditRecord.set('assignmentStatus', record.data.assignmentStatus);
            curEditRecord.set('startDate', record.data.startDate);
            curEditRecord.set('expiryDate', record.data.expiryDate);
            curEditRecord.commit();
        }
    });

    // Generic License Assignment Panel
    Symphony.License.GenericAssignmentTab = Ext.define('license.genericassignmenttab', {
        alias: 'widget.license.genericassignmenttab',
        extend: 'symphony.editableunpagedgrid',
        licenseId: 0,
        initComponent: function () {
            var me = this;

            var cache = {};
            var url = me.url;

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left'
                },
                columns: [
                    { header: 'ID', dataIndex: 'id', hidden: true },
                    { dataIndex: 'deleteAssignment', renderer: Symphony.deleteRenderer, header: '', width: 30, hidden: !me.editable },
                    { header: 'Start Date', dataIndex: 'startDate', renderer: function (value) { return Symphony.parseDate(value).formatSymphony('m/d/Y'); } },
                    { header: 'Expiry Date', dataIndex: 'expiresOnDate', renderer: function (value) { return Symphony.parseDate(value).formatSymphony('m/d/Y'); } },
                    { header: 'Days Until Expiry', dataIndex: 'daysUntilExpiry' },
                    { header: me.nameColumnTitle, dataIndex: 'entityName' },
                    { header: 'Assignment Status Id', dataIndex: 'assignmentStatusId', hidden: true },
                    { header: 'Assignment Status', dataIndex: 'assignmentStatus', width: 150 },
                    { header: 'Renew Before', dataIndex: 'renewByDate', renderer: function (value) { return Symphony.parseDate(value).formatSymphony('m/d/Y'); } }
                ]
            });

            Ext.apply(this, {
                url: url,
                colModel: colModel,
                model: 'licenseAssignments',
                listeners: {
                    cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e) {
                        if (grid.getColumnModel().getColumnAt(columnIndex).dataIndex == 'deleteAssignment') {
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete?', function (btn) {
                                if (btn == 'yes') {
                                    DeleteRecord(record.data.licenseAssignmentId, function (err, data) {
                                        if (err) {
                                            console.error("Delete failed: ", err);
                                            return;
                                        }
                                        grid.getStore().remove(record);
                                        //console.log("Delete success: ", data);
                                    });
                                }
                            });
                        }
                    }
                }
            });

            this.callParent(arguments);

            // To utilize this view in the customer section, we need to allow for dynamic URL creation
            // based on the selction in the tree view. NOTE: this gets fired on selection change AND on tab select :(
            this.store.on('beforeload', function (store) {
                if (this.entityType && this.entityId) {
                    store.getProxy().api.read = '/services/license.svc/reports/users/' + this.entityType + "/" + this.entityId;;
                }
            }, this);
        },
        updateRecord: function (record) {
            var curEditRecord = this.getSelectionModel().getSelection()[0];
            curEditRecord.set('assignmentStatusId', record.data.assignmentStatusId);
            curEditRecord.set('assignmentStatus', record.data.assignmentStatus);
            curEditRecord.set('startDate', record.data.startDate);
            curEditRecord.set('expiryDate', record.data.expiryDate);
            curEditRecord.commit();
        },
        // This is an odd name as it is not a filter it is always an id,
        // but taken from customer.js:customer.hierarchyusers
        setFilter: function (entityId, entityType) {
            //console.log("'SetFilter': ", entityId, entityType);
            this.entityType = entityType;
            this.entityId = entityId;
            this.refresh();
        }
    });

    Symphony.License.NotesPanel = Ext.define('license.notespanel', {
        alias: 'widget.license.notespanel',
        extend: 'symphony.editableunpagedgrid',

        initComponent: function () {
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left'
                },
                columns: [ {
                    header: '',
                    dataIndex: 'id',
                    hidden: true
                },{
                    header: 'Date',
                    dataIndex: 'modified',
                    renderer: function (value, meta, record) {
                        return Symphony.dateRenderer(value);
                    },
                    align: 'center'
                },{
                    header: 'Body',
                    dataIndex: 'body',
                    flex: 1
                }]
            });

            var me = this;

            var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToMoveEditor: 1,
                autoCancel: false
            });

            Ext.apply(this, {
                url: '/services/license.svc/notes/' + me.data.licenseAssignmentId,
                colModel: colModel,
                model: 'licenseAssignmentNote',

               /* 
                * The edit should happen inline, but the rowediting is failing. 
                * Will revisit in a fuuture iteration
                
                tbar: {
                    items: [{
                        text: 'Add note',
                        iconCls: 'x-button-add',
                        handler: function () {
                            rowEditing.cancelEdit();
                            var store = me.store;

                            store.insert(0, {
                                id: '',
                                modified: 0,
                                body: ''
                            });

                            rowEditing.startEdit(0, 1);
                        }
                    }]
                },
                */
                layout: 'fit',
                viewConfig: {},
                listeners: {
                    rowselect: function (grid, rowIndex, e) {},
                    itemdblclick: function (grid, record, item, rowIndex, e) {}
                }
            });
            this.callParent(arguments);
        }
    });

    Symphony.License.DocumentsPanel = Ext.define('license.documentspanel', {
        alias: 'widget.license.documentspanel',
        extend: 'symphony.searchablegrid',

        initComponent: function () {
            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left'
                },
                columns: [{
                    header: 'Date',
                    dataIndex: 'modified',
                    renderer: function (value, meta, record) {
                        return Symphony.dateRenderer(value);
                    },
                    align: 'center'
                }, {
                    header: 'Document Name',
                    dataIndex: 'filename',
                    flex: 1
                }, {
                    header: 'File Type',
                    dataIndex: 'filetype'
                },{
                    header: 'Uploaded?',
                    renderer: function (value, meta, record) {
                        return record.data.id ? 'Yes' : 'Pending';
                    }
                }]
            });

            var me = this;
            Ext.apply(this, {
                url: '/services/license.svc/assignments/documents/' + me.data.licenseAssignmentId,
                colModel: colModel,
                model: 'licenseAssignmentDocument',
                tbar: {
                    items: [{
                        xtype: 'swfuploadbutton',
                        uploadUrl: '/Uploaders/LicenseAssignmentUploader.ashx',
                        text: 'Choose a File:',
                        name: 'licenseAssignmentUploader',
                        iconCls: 'x-button-add',
                        listeners: {
                            'queue': function (file) {
                                //console.log("Queueing", file);
                                me.store.add({
                                    id: null,
                                    filename: file.name,
                                    filetype: file.type,
                                    modified: "/Date(" + Date.now() + ")/"
                                });
                            },
                            'uploadstart': function (item) {
                                Ext.Msg.progress('Please wait...', 'Please wait while your file is uploaded...');
                            },
                            'uploadprogress': function (item, completed, total) {
                                var ratio = parseFloat(completed / total);
                                Ext.Msg.updateProgress(ratio, parseInt(ratio * 100, 10) + "% complete");
                            },
                            'allcomplete': function () {
                            },
                            'uploaderror': function () {
                                this.error = true;
                                Ext.Msg.hide();
                                Ext.Msg.alert('Upload failed', 'The upload failed. Please make sure it is within the proper file size limitations.');
                            },
                            'uploadsuccess': function (item, response, hasData) {
                                var result = Ext.decode(response);
                                if (result.success) {
                                    me.fireEvent('assignmentDocumentUploaded');
                                    Ext.Msg.hide();
                                } else {
                                    Ext.Msg.hide();
                                    Ext.Msg.alert('Upload failed', result.error);
                                }
                            }
                        }
                    }]
                },
                viewConfig: {},
                listeners: {
                    rowselect: function (grid, rowIndex, e) {},
                    itemdblclick: function (grid, record, item, rowIndex, e) { }
                }
            });
            this.callParent(arguments);
        }
    });

    Symphony.License.UserLicensePanel = Ext.define('license.userlicensepanel', {
        alias: 'widget.license.userlicensepanel',
        extend: 'Ext.Panel',
        userId: 0,
        customerId: 0,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                tbar: {
                    items: [{
                        text: 'Add',
                        iconCls: 'x-button-add',
                        handler: function () {
                           // console.log("LICENSE PANEL (USERS)");
                        }
                    }]
                },
                items: {
                    xtype: 'tabpanel',
                    activeItem: 0,
                    border: false,
                    cls: 'x-panel-transparent',
                    defaults: {
                        border: false,
                        frame: false,
                        autoScroll: true,
                        cls: 'x-panel-transparent',
                        bodyPadding: 5
                    },
                    items: [{
                        title: 'Licenses',
                        xtype: 'license.userassignmenttab',
                        userId: me.userId,
                        url: '/services/license.svc/reports/user/' + me.userId
                    }]
                }
            })
        }
    });

}(window));
