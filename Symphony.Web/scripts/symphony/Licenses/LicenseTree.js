﻿    (function (window, undefined) {   

        Symphony.License.LicenseTree = Ext.define('license.licensetree', {
            alias: 'widget.license.licensetree',
            extend: 'symphony.tabletree',
            editable: true,
            deferLoad: true,
            enableDD: true,
            nodeTextRenderer: function (record, parentNode) {
                return record.get('name') + ' (' + (record.get('jurisdiction') || '-') + ', ' + (record.get('profession') || '-') + ')';
            },
            initComponent: function () {
                var me = this;

                Ext.apply(this, {
                    deferLoad: me.deferLoad,
                    viewConfig: {
                        plugins: 'treeviewdragdrop',
                        listeners: {
                            drop: function (node, data, overModel, dropPosition, eOpts) {
                                // when the item is dropped, save its new parent
                                var parentId = overModel.get('id');
                                var id = data.records[0].get('id');
                                var name = data.records[0].get('text');
                                Symphony.Ajax.request({
                                    url: '/services/license.svc/license/' + id + '?reparent=true',
                                    jsonData: { id: id, name: name, parentId: parentId },
                                    success: function (result) {
                                        // refresh to ensure we have the latest data
                                        me.refresh();

                                    },
                                    failure: function (result) {
                                        if (result.status != 200) {
                                            Ext.Msg.alert('Error', result.statusText);
                                        } else {
                                            Ext.Msg.alert('Error', result.error);
                                        }
                                        // refresh to make everything go back the way it was
                                        me.refresh();
                                    }
                                });
                            }
                        }
                    },
                    columns: [{
                        dataIndex: 'name',
                        xtype: 'treecolumn',
                        text: 'Licenses',
                        flex: 1,
                        renderer: function (value, meta, record) {
                            return record.get('name') + ' (' + (record.get('jurisdiction') || '-') + ', ' + (record.get('profession') || '-') + ')';
                        }
                    }],
                    tbar: me.editable ? {
                        items: [{
                            text: 'Add',
                            iconCls: 'x-button-add',
                            handler: function () {
                                var node = me.getSelectionModel().getSelection()[0];
                                //console.log(node);
                                var parentId = (typeof node === 'undefined' || node === null) ? 0 : node.attributes.value;
                                var license = Ext.data.Record.create(Symphony.Definitions.license);
                                var record = new license({ id: 0, parentId: parentId });
                                
                                me.fireEvent('addrecord', record);
                            }
                        }, {
                            text: 'Delete',
                            name: 'delete',
                            iconCls: 'x-button-delete',
                            handler: function () {
                                var node = me.getSelectionModel().getSelection()[0];
                                var id = node.get('id');

                                Ext.Msg.show({
                                    title: 'Delete?',
                                    msg: 'Are you sure you want to delete this license?',
                                    buttons: Ext.Msg.YESNO,
                                    animEl: 'elId',
                                    icon: Ext.MessageBox.QUESTION,
                                    fn: function() {
                                        Symphony.Ajax.request({
                                            url: '/services/license.svc/license/' + id,
                                            method: 'DELETE',
                                            jsonData: { id: id},
                                            success: function (result) {
                                                me.fireEvent('delete', node);
                                                me.refresh();
                                            }
                                        });
                                    }                              
                                });
                            }
                        }]
                    } : null,
                    url: '/services/license.svc/license/',
                    model: 'license',
                    listeners: {
                        
                    }
                });
                this.callParent(arguments);
            }
        });


} (window));
