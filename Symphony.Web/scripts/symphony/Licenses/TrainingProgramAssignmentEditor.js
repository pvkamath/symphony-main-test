﻿(function () {
    Symphony.License.TrainingProgramAssignmentEditor = Ext.define('license.trainingprogramassignmenteditor', {
        alias: 'widget.license.trainingprogramassignmenteditor',
        extend: 'Ext.Window',
        record: null,
        initComponent: function () {
            var me = this;

            Ext.apply(this, {
                layout: 'fit',
                border: false,
                modal: true,
                width: 300,
                height: 130,
                title: 'Edit Assignment',
                items: [{
                    xtype: 'form',
                    border: false,
                    frame: true,
                    bodyStyle: 'overflow-y:auto; padding-right:10px',
                    items: [{
                        xtype: 'textfield',
                        name: 'approvalCode',
                        fieldLabel: 'Approval Code',
                        value: me.record ? me.record.get('approvalCode') : ''
                    }, {
                        xtype: 'checkbox',
                        name: 'isPrimary',
                        fieldLabel: 'Primary License',
                        checked: me.record ? me.record.get('isPrimary') : ''
                    }]
                }],
                buttons: [{
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        me.close();
                    }
                }, {
                    xtype: 'button',
                    text: 'Ok',
                    iconCls: 'x-button-finish',
                    handler: function () {
                        if (typeof (me.callback) === 'function') {
                            var values = me.find('xtype', 'form')[0].getForm().getValues();
                            me.callback(values);
                        }
                        me.close();
                    }
                }]
            });

            this.callParent(arguments);
        }
    });


})();