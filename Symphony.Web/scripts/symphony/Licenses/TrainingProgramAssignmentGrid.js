﻿(function () {
    Symphony.License.TrainingProgramAssignmentGrid = Ext.define('license.trainingprogramassignmentgrid', {
        alias: 'widget.license.trainingprogramassignmentgrid',
        extend: 'symphony.searchablelocalgrid',
        trainingProgramId: 0,
        initComponent: function () {
            var url = '/services/license.svc/trainingprogram/' + this.trainingProgramId;
            var me = this;

            var columns = [
                { /*id: 'name',*/ header: 'Name', dataIndex: 'name' },
                //{ id: 'licenseType', header: 'License Type', dataIndex: 'licenseType' },
                { /*id: 'jurisdiction',*/ header: 'Jurisdiction', dataIndex: 'jurisdiction' },
                { /*id: 'profession',*/ header: 'Profession', dataIndex: 'profession' },
                { /*id: 'approvalCode',*/ header: 'Approval Code', dataIndex: 'approvalCode' },
                { /*id: 'isPrimary',*/ header: 'Primary License', dataIndex: 'isPrimary', align: 'center', renderer: Symphony.checkRenderer }
            ];

            if (!me.readOnly) {
                columns = [
                    { /*id: 'deleteLicense',*/ header: ' ', hideable: false, renderer: Symphony.deleteRenderer, width: 28, name: 'deleteLicense' }
                ].concat(columns)
                .concat([{
                    /*id: 'editLicense',*/ header: ' ', hideable: false, width: 28,
                    name: 'editLicense',
                    renderer: function () {
                        return '<a href="#"><img class="edit-license" src="/images/pencil.png" ext:qtip="Edit Approval Code"/></a>';
                    }
                }]);
            }

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left'
                },
                columns: columns
            });

            Ext.apply(this, {
                url: url,
                model: 'license',
                colModel: colModel,
                listeners: {
                    rowcontextmenu: function (grid, rowIndex, e) {
                        if (!me.readOnly) {
                            var record = grid.store.getAt(rowIndex);
                            var id = record.get('id');
                            var menu = new Ext.menu.Menu({
                                items: [
                                    {
                                        text: 'Make this the primary license',
                                        handler: function () {
                                            me.setPrimary(record);
                                        }
                                    }
                                ]
                            });
                            menu.showAt(e.getXY());
                            e.stopEvent();
                        }
                    },
                    cellclick: function(grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.getStore().getAt(rowIndex);
                        var columnId = grid.getColumnModel().getColumnAt(columnIndex).name;
                        
                        if (columnId === 'deleteLicense') {
                            Ext.Msg.confirm('Are you sure?', 'Are you sure you want to delete this license?', function (btn) {
                                if (btn == 'yes') {
                                    grid.getStore().remove(record);
                                    me.handleDefaultPrimary();
                                }
                            });
                        } else if (columnId === 'editLicense') {
                            me.editLicense(record, true);
                        }

                    }
                }
            });

            this.callParent(arguments);
        },
        clearPrimary: function() {
            var store = this.getStore();
            store.each(function (r) {
                r.set('isPrimary', false);
                r.commit();
            });
        },
        setPrimary: function (record) {
            this.clearPrimary();

            record.set('isPrimary', true);
            record.commit();
        },
        handleDefaultPrimary: function() {
            var store = this.getStore();
            var first = null;
            var hasPrimary = false;

            store.each(function (r) {
                if (!first) {
                    first = r;
                }

                if (r.get('isPrimary')) {
                    hasPrimary = true;
                }
            });

            if (!hasPrimary && first && first.isModel) {
                this.setPrimary(first);
            }
        },
        editLicense: function (record, isUpdate) {
            var me = this;

            var w = new Symphony.License.TrainingProgramAssignmentEditor({
                record: record,
                callback: function (values) {
                    record.data.approvalCode = values.approvalCode;
                    record.data.isPrimary = (values.isPrimary === 'on' ? true : false);
                    
                    
                    if (!isUpdate) {
                        if (record.data.isPrimary) {
                            me.clearPrimary();
                        }
                        me.store.loadData([record.data], true);
                    } else {
                        record.commit();
                        if (record.data.isPrimary) {
                            me.setPrimary(record);
                        }
                    }

                    me.handleDefaultPrimary();
                }
            });

            w.show();
        }
    });

})();