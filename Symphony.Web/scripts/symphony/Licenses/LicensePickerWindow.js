﻿(function (window, undefined) {
    Symphony.License.getLicenseSelectorWindow = function (afterSelect) {
        var w = new Symphony.License.LicensePickerWindow({
            afterSelect: afterSelect
        });
        w.show();
    }
    Symphony.License.LicensePickerWindow = Ext.define('license.licensepickerwindow', {
        alias: 'widget.license.licensepickerwindow',
        extend: 'Ext.Window',
        initComponent: function () {
            var me = this;
            me.selectedNode = null;

            Ext.apply(this, {
                title: 'Select a License',
                width: 900,
                height: 550,
                layout: 'border',
                border: false,
                modal: true,
                items: [{
                    editable: false,
                    region: 'center',
                    xtype: 'licenses.app',
                    border: false,
                    listeners: {
                        selectedNode: function (node) {
                            me.selectedNode = node;
                        }
                    }
                }],
                buttons: [{
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'x-button-cancel',
                    handler: function () {
                        me.close()
                    }
                }, {
                    xtype: 'button',
                    text: 'Add License',
                    iconCls: 'x-button-add',
                    handler: function () {
                        if (!me.selectedNode) {
                            Ext.Msg.alert('Error', 'Please select a license.');
                            return;
                        }

                        if (typeof (me.afterSelect) === 'function') {
                            me.afterSelect(me.selectedNode);
                        }

                        me.close();
                    }
                }],
                listeners: {
                    show: function () {
                        me.query('[xtype=licenses.app]')[0].refreshLicenses();
                    }
                }
            });
            this.callParent();
        }
    })
} (window));
