﻿/**
 * Log level enumeration for Symphony.LogLevel.
 */
Ext.define('Symphony.LogLevel', {
    alternateClassName: 'LogLevel',

    statics: {
        NONE: 0,
        ERROR: 1,
        WARN: 2,
        INFO: 3,
        DEBUG: 4
    }
});

/**
 * Logger singleton for outputting logs. Safe wrapper around the console that
 * can print messages of varying severity. 
 */
Ext.define('Symphony.Logger', {
    alternateClassName: 'Log',
    requires: 'Symphony.LogLevel',
    singleton: true,

    logLevel: LogLevel.NONE,

    debug: function() {
        var me = this;

        if (!console) {
            return;
        }
        if (me.logLevel < LogLevel.DEBUG) {
            return;
        }

        var args = Array.prototype.slice.call(arguments);

        var caller = me.formatCaller(me.debug.caller);
        if (caller) {
            args.unshift(caller);
        }

        me.log(console.debug || console.log, args);
    },

    info: function() {
        var me = this;

        if (!console) {
            return;
        }
        if (me.logLevel < LogLevel.INFO) {
            return;
        }

        var args = Array.prototype.slice.call(arguments);

        var caller = me.formatCaller(me.info.caller);
        if (caller) {
            args.unshift(caller);
        }

        me.log(console.info || console.log, args);
    },

    warn: function() {
        var me = this;

        if (!console) {
            return;
        }
        if (me.logLevel < LogLevel.WARN) {
            return;
        }

        var args = Array.prototype.slice.call(arguments);

        var caller = me.formatCaller(me.warn.caller);
        if (caller) {
            args.unshift(caller);
        }

        me.log(console.warn || console.log, args);
    },

    error: function() {
        var me = this;

        if (!console) {
            return;
        }
        if (me.logLevel < LogLevel.ERROR) {
            return;
        }

        var args = Array.prototype.slice.call(arguments);

        var caller = me.formatCaller(me.error.caller);
        if (caller) {
            args.unshift(caller);
        }

        me.log(console.error || console.log, args);
    },

    writeLine: function() {
        var me = this;

        if (!console) {
            return;
        }
        if (me.logLevel <= LogLevel.NONE) {
            return;
        }

        console.log.apply(console, arguments);
    },

    watchStore: function(owner, store) {
        var me = this;

        if (store._isWatched) {
            return;
        }
        if (!console) {
            return;
        }
        if (me.logLevel < LogLevel.DEBUG) {
            return;
        }

        store._isWatched = true;

        store.on('beforeload', Ext.Function.createOwned(owner, function(store, operation) {
            store._loadTimestamp = new Date();
            store._loadOperation = operation;

            Log.debug(Ext.String.format('Performing store({0}) load operation.', store.model.$className), operation);
        }));

        store.on('beforeprefetch', Ext.Function.createOwned(owner, function(store, operation) {
            store._prefetchTimestamp = new Date();
            store._prefetchOperation = operation;

            Log.debug(Ext.String.format('(Performing store({0}) prefetch operation.', store.model.$className), operation);
        }));

        store.on('load', Ext.Function.createOwned(owner, function(store, records, successful) {
            var elapsed = Ext.Date.getElapsed(store._loadTimestamp),
                elapsedStr = Ext.Number.toFixed(elapsed / 1000, 3) + 's';

            if (successful) {
                Log.debug(Ext.String.format('Store({0}) load operation completed in {1} and returned {2} records.', store.model.$className, elapsedStr, records.length), store._loadOperation);
            } else {
                Log.error(Ext.String.format('Store({0}) load operation failed in {1}.', store.model.$className, elapsedStr), store._loadOperation);
            }
        }));

        store.on('prefetch', Ext.Function.createOwned(owner, function(store, records, successful) {
            var elapsed = Ext.Date.getElapsed(store._prefetchTimestamp),
                elapsedStr = Ext.Number.toFixed(elapsed / 1000, 3) + 's';

            if (successful) {
                Log.debug(Ext.String.format('Store({0}) prefetch operation completed in {1} and returned {2} records.', elapsedStr, records.length), records, store._loadOperation);
            } else {
                Log.error(Ext.String.format('Store({0}) prefetch operation failed in {1}.', store.model.$className, elapsedStr), store._loadOperation);
            }
        }));
    },

    watchModel: function(owner, model) {
        var me = this,
            saveFn;

        if (model._isWatched) {
            return;
        }
        if (!console) {
            return;
        }
        if (me.logLevel < LogLevel.DEBUG) {
            return;
        }

        model._isWatched = true;

        saveFn = model.save;
        model.save = Ext.Function.createOwned(owner, function(options) {
            var callbackFn = Ext.Function.createOwned(owner, function(records, operation) {
                var elapsed = Ext.Date.getElapsed(model._saveTimestamp),
                    elapsedStr = Ext.Number.toFixed(elapsed / 1000, 3) + 's';

                if (operation.success) {
                    Log.debug(Ext.String.format('Model({0}) save operation completed in {1}.', model.$className, elapsedStr), operation);
                } else {
                    Log.error(Ext.String.format('Model({0}) save operation failed in {1}.', model.$className, elapsedStr), operation);
                }
            });

            model._saveTimestamp = new Date();
            Log.debug(Ext.String.format('Performing model({0}) save operation.', model.$className), options);

            if (options.callback) {
                options.callback = Ext.Function.createSequence(options.callback, callbackFn);
            } else {
                options.callback = callbackFn;
            }

            saveFn.call(model, options);
        });

        destroyFn = model.destroy;
        model.destroy = Ext.Function.createOwned(owner, function(options) {
            var callbackFn = Ext.Function.createOwned(owner, function(records, operation) {
                var elapsed = Ext.Date.getElapsed(model._destroyTimestamp),
                    elapsedStr = Ext.Number.toFixed(elapsed / 1000, 3) + 's';

                if (operation.success) {
                    Log.debug(Ext.String.format('Model({0}) destroy operation completed in {1}.', model.$className, elapsedStr), operation);
                } else {
                    Log.error(Ext.String.format('Model({0}) destroy operation failed in {1}.', model.$className, elapsedStr), operation);
                }
            });

            model._destroyTimestamp = new Date();
            Log.debug(Ext.String.format('Performing model({0}) destroy operation.', model.$className), options);

            if (options.callback) {
                options.callback = Ext.Function.createSequence(options.callback, callbackFn);
            } else {
                options.callback = callbackFn;
            }

            destroyFn.call(model, options);
        });

        model.load = Ext.Function.createOwned(owner, function(config) {
            var me = this,
                operation,
                scope,
                callback;

            config = Ext.apply({}, config);
            config = Ext.applyIf(config, {
                action: 'read',
                id: me.getId()
            });

            operation = new Ext.data.Operation(config);
            scope = config.scope || this;

            model._loadTimestamp = new Date();
            Log.debug(Ext.String.format('Performing model({0}) load operation.', model.$className), operation);

            callback = Ext.Function.createOwned(owner, function(operation) {
                var record = null,
                    success = operation.wasSuccessful(),
                    elapsed = Ext.Date.getElapsed(model._loadTimestamp),
                    elapsedStr = Ext.Number.toFixed(elapsed / 1000, 3) + 's';
                
                if (success) {
                    record = operation.getRecords()[0];
                    // If the server didn't set the id, do it here
                    if (!record.hasId()) {
                        record.setId(me.getId());
                    }
                    me.set(record.getData());

                    Ext.callback(config.success, scope, [record, operation]);
                    Log.debug(Ext.String.format('Model({0}) load operation completed in {1}.', model.$className, elapsedStr), operation);
                } else {
                    Ext.callback(config.failure, scope, [record, operation]);
                    Log.error(Ext.String.format('Model({0}) load operation failed in {1}.', model.$className, elapsedStr), operation);
                }
                Ext.callback(config.callback, scope, [record, operation, success]);
            });

            me.getProxy().read(operation, callback, this);
        });
    },

    log: function(fn, args) {
        fn.apply(console, args);
    },

    formatCaller: function(caller) {
        if (!caller) {
            return null;
        } 
        
        if (caller.$owner) {
            return '[' + caller.$owner.$className.replace('Symphony.', '') + ']';
        } else if (caller.$name) {
            return '[??::' + caller.$name + ']';
        } else {
            return null;
        }
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);
});

/**
 * Mixin that changes the owner of a component's functions. Ownership data is
 * not normally available on event handlers.
 * 
 * Basically, this mixin allows the logger to determine what the current class
 * context is when in an event handler. So, if you click a toolbar button on a
 * grid, it can print out [Symphony.CoolGrid] button clicked!
 * 
 * Without this mixin there is no such context.
 */
Ext.define('Symphony.mixins.FunctionOwnership', {
    onClassMixedIn: function(clazz) {
        var initFn = clazz.prototype.initComponent;

        clazz.prototype.initComponent = function() {
            var me = this;

            if (initFn) {
                initFn.call(me);
            }

            var fn = function(itemId) {
                var item,
                    eventData,
                    key;
                
                if (me.queryById) {
                    item = me.queryById(itemId);
                }
                if (itemId == null) {
                    item = me;
                }
                if (item == null) {
                    Log.error('Can\'t find item with itemId ' + itemId + '.');
                }

                if (item.handler) {
                    item.handler.$owner = me;
                }
                
                for (key in item.events) {
                    eventData = item.events[key];
                    if (!eventData.listeners) {
                        continue;
                    }

                    Ext.each(eventData.listeners, function(listener) {
                        listener.fn.$owner = me;
                    });
                }
            };

            if (this.assumeOwnership) {
                Ext.each(this.assumeOwnership, fn);
            }
            fn(null);
        };
    }
}, function(clazz) {
    Log.debug('Class created: ' + clazz.$className);

    Ext.Function.createOwned = function(owner, fn) {
        fn.$owner = owner;
        return fn;
    };

    Log.debug('Added method Ext.Function.createOwned.');
});