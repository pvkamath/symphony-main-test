﻿/**
 * DebugMenu is a menu that can perform a number of operations on a component,
 * such as printing out information about items in its hierarchy and inspecting
 * items in its store.
 * 
 * Review the button definitions below for more information.
 */
Ext.define('Symphony.DebugMenu', {
    extend: 'Ext.Window',

    width: 435,

    buttons: [{
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAhFJREFUeNpi/P//PwMlgImBQsACY6xiZPQAUi1Q7vF/DAy5v4GMH1BFbBBam5GBIRjINALibWH//8+CGwBU3BJRUmL84/v3/zdPn5a8fuqUJdBzJjB5ILuTgY3N7fevX++AhvQChbaDxBlhYbCAkTENyErTNjeXVNTWlvr57dvvM/v23Xn96lUsPx9fjqiUlN2DGzeuAzVnszIwPATSDFFAvXAD5jMyMvxiYDAGOj0cyHUyd3RU4hEQ4L937dozdg4OhhsXL54DOjcdqPkFK8hmHAaA/fwXaBBQwUw9Q0N1BiYm9tvXrt36/v27DiPEK2AgDjUAHgaPENEiChSsVZKSkv7x8SPbradPbwjy8vJ/+/Pn0pffv/VAlvwB4lfo0fgDEpAgyRoFUVFTMWZmsWtPn97++PPnxPtv3pxlZ2XlAspt+4crHYAMAJqeLsHL663GxCT15NOnF29//lwIdPJcoMagR9++beVhYZEHstt+Q9SiGgD0t/xPBoZUaw4O+e+/fn098/HjeSB/zieg3D+Iy/Kf/PmzFmiREVCzI4YBQMW5hlxckjx//7Js/fTpPpBfD1T0HqYQakgd0IAzQDmPH9BEiGyAgw07u+SlX7/ePv/7dwdQ49nvQPHvEDm4IUCX1gD5r4De0ENJSNHApPwfkpTfMgPDAhhlD5BSIQNa4CkCxSSW/f9/nHHAcyNAgAEAxF3igbzwV7kAAAAASUVORK5CYII=',
        tooltip: 'Closes the window.',
        
        // Nothing fancy here; closes the window.
        handler: function() {
            this.up('[xtype=window]').close();
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:application/octet-stream;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAA+VBMVEX///8AAAAAAAAAAAAAAABpgCqBjEiCjUmBjEh8jEF8jEFfeB+Dm0SRpVZjfCOXpl11ijqKokxfeB9cdR1cdR2SpliYp11vhTFcdR2FjE2PpVJvhTGIn0qNpE9jfCOEnUZ1ijqNolCLo0x8pxCkzjGSvx2AqxGEsBOZxiWNuhmbyCat2jiWwStRaBeo1TR7nCaeyi7P91+izzCaxSxXbxlcdR1VbRhQZhdZchpTaxiItRV5og+35EPj/3av1z/3+sr7/a6TvCrv/JTE7lGu2zrC7E/t/ZLc/IHV+HuQuSni/Ij7/a36/KBPZBabyCev3Duaxybf/YX7/aFN8WUGAAAAI3RSTlMACQEVBBs25zAtMBXa6RXxJ9kPEs/s7SHJBuck5OIb3yrn3pUcndcAAAClSURBVHheVY1FYsNQEMW+KczMBTMzO8xUuv9h4tR1fqPVSIs3IKUPnsllM0+e78yrBQCplc/TRfv14d23iz7TfwbDPydGqzVFUdvdmPj1ycsp+uA47iv6fq/EXiztP0WBpmlB3BzqDdBs2batKjzPK2p89ZIVzwiCwPDgW0tjWVazYHBMhmFMBwZfIklS8mGQl2EYLuVUUdw93nFxNAkIdk3AEABumfwXtP7AAGMAAAAASUVORK5CYII=',
        tooltip: 'Opens a debug window with this component\'s parent component.',
        
        // Moves up one component in the hierarchy and opens a new window. This
        // is useful when you are working with components with a lot of child
        // components, such as a grid.
        handler: function() {
            var window = this.up('[xtype=window]');

            var win = Ext.create('Symphony.DebugMenu', {
                x: window.getX(),
                y: window.getY(),
                component: window.component.ownerCt
            });
            win.show();

            window.close();
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/gif;base64,R0lGODlhEAAQANUzAISQnf////P2+ZMAAMopAP/M/8DL1oWQnDZBTL/J0v+DIP9MDP9sFP8zAP8pAPv8/c/X37zG0OLo79rg5uDl69be5qa0wmt0fuPo7ICKluzw8/z9/vv8/LO/zH2Ik9Pb4/b4+niCjfn6+/P19+js8N/l7OXp7P3+/sXO1/X3+vj6/O/y9XB6hNjg53N8h2ZueP/+/vDz9+rv9P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAADMALAAAAAAQABAAAAaewNkMQCwahUhAYMlsApAHgXEKSByE0cBDIJNEIihIy4oVCECkj8HSqZQg5FkGcE6v229ARuiRUosJHkIhAgEbKjFeGEsrJgGDZiIjFAYTMAQES5gzLnSSlAEEBQWYowQsfkUBAwoKo60DF4UnGykSiwMMugwDM7ICDxwalaELxgsEQi/AwgahBQ4NDQ6kQgjX16vRAwPTvUjgM9xC40EAOw==',
        tooltip: 'Clears the store by removing all models from it.',

        // Empties the component's store if it has a store. Works on anything
        // that has a getStore method.
        handler: function() {
            var window = this.up('[xtype=window]'),
                store;

            if (!window.component.getStore) {
                Ext.Error.raise('This component does not have a store.');
                return;
            }

            store = window.component.getStore();
            store.removeAll();
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/gif;base64,R0lGODlhEAAQANU9AISQnfv6+vP2+f///zZBTP//ABKuHgqjDYWQnABeAABMABBoEPv8/dbe5sDL1rO/zKa0wgBkAODl6/n6++zw8/P19wBaAHiCjfz9/mt0ftPb47zG0Ojs8Pb4+vv8/Ons7+Lo7wBGAMDDx4CKln2IkwBJAHB6hABFAO/x9FrNLWZuePHz9QBcAOPn6+Lm6gBTAL/J0nN8h/j6/MXO1wBEAABjAABlAP3+/srO0/Dz9/X3+urv9L7AxP///wAAAAAAACH5BAEAAD0ALAAAAAAQABAAAAaawF4PQCwahUjAYMlsApAIgXEKgCGE0QFDsANtNrNPwHXtRQUdjsYBeTQCkQBOOAKg1a2APpBYBIQkUkVwLBYWLwULQhcCAxgyOXwFkwUpiT2MAhMVEnwGn6CJMXabnTUHqKkKPSaCRAElISc0CQarPRmNNxg6K3sBNgp/uAICDB4UbG4BwiJIKsbIyg0oATxIQgTa29zY3t/eQQA7',
        tooltip: 'Prints out a list of models in the store.',

        // Prints out a list of models in the component's. Useful for debugging
        // grid renderers, as you can see the raw model data.
        handler: function() {
            var window = this.up('[xtype=window]'),
                store;

            if (!window.component.getStore) {
                Ext.Error.raise('This component does not have a store.');
                return;
            }

            store = window.component.getStore();
            store.data.each(function(model) {
                if (model.raw) {
                    Log.writeLine(model.getId(), model.raw);
                } else if (model.data) {
                    Log.writeLine(model.getId(), model.data);
                } else {
                    Log.writeLine(model.getId(), model);
                }
            });
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/gif;base64,R0lGODlhEAAQANUmAF0+AJReAP///5NdAP/IBv/uAP/4Sf+nKv//AP/sdP+4FP+gLZNhAP+eHv/TAP/RAP+qI/+rJ/+1BpdkAP/3AJdjAP/ta//EAP+pKf+cH5NiAP/SAP+oGv/sAP/CCv+9D/+yHP+yGv/OAP/rRf/rcv/1MP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAACYALAAAAAAQABAAAAZfQEYlMCgaj4GJBiBoOp9PgAlgsIwQncdFkmlwDlLT1JAoUTae0CICFrupJITjA8G03W+DQaQ43PF5BCB/gG4BBQQBhYWHiYuAjYqPhoiSkyaRl2IAlZqbjp5Tlp5heEEAOw==',
        tooltip: 'Prints out a list of all the filters applied to the store.',

        // Prints out a list of filters that are applied to the store. Doesn't
        // really work right now.
        handler: function() {
            var window = this.up('[xtype=window]'),
                store;

            if (!window.component.getStore) {
                Ext.Error.raise('This component does not have a store.');
                return;
            }

            store = window.component.getStore();
            Log.writeLine(store.filters);
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACOklEQVR42pWTX0hTURzHv7etB3vJElkvRYYEQRE9ZhBExVJZzunc3V3bdeY/2BJByoeCCOpBShCdYWmtaXduTedsmCuI6sEFQRBBQRQ1iqJZPaxC13bvbufcy/ZQUNv38ON3/n0/5xzOOQzLWg8DGCSxC6XpJYk+hgCW6/T6ym1VVSW53yUSWIjFUozDwcvtTicueS/jXvxhUeZDe/fjVJsbE14vFMBxnoe+i0WXrRUVGyogSdLfLgbQarT4+PkTboT8uHslgGs+nwpwOhyo7bbC3doJzRoNZFIKLiI5l0NGzCKdTuPHyk+E79zG4tg0vJOTKoC32VDvOgabsQXatVrFQBGiKCKTySCd+aXUGYZBNpvF/aVHWBi9CZ8gqAA7x8HgtqP+oJ6sySgmuiI9Cid4IHAudT8EIEoiHj99gqhnClN+vwqwsSwaenjs2blbmZRXR/g6xk1thUyHJCmHZy+eY37YByEQIAC7Q7ZaLGjsdWJH9Xb0LAYwXMsWcl60PVJnVQCv3r7G3JAX08EgGDsBWMxmuEfP42xsFgMHjOh/EFHyn6L9VOeONMHjOoNgKKQCWpqbYe5rx6ZK3T/vnx5PyklY/voFocEJ3JqZUQFNJhPYk50oX1/+30ckyzJS31MIXLyK2XBYBTQajeD6u7GurKyol7iyugr/wBjmIhEV0GAw4ML4EN58SBQFqN68Fac7ejEfjYJ+pm/7amo26nS6osx5JZNJLMXjKQo4StojJLaURADekzjxG+oc9NIN5uENAAAAAElFTkSuQmCC',
        tooltip: 'Attaches monitors to the store\'s reader and writer to enable detailed analysis.',

        // This is for debugging tricky issues with how readers process data.
        // This function inserts a debugger statement into the reader's read
        // function, so you can step through as it decodes. It also adds an
        // exception handler to the proxy so errors show up in the console
        // instead of being silent.
        handler: function() {
            var window = this.up('[xtype=window]'),
                store,
                proxy,
                reader;

            if (!window.component.getStore) {
                Ext.Error.raise('This component does not have a store.');
                return;
            }

            store = window.component.getStore();
            proxy = store.getProxy();
            if (!proxy) {
                Ext.Error.raise('This component does not have a proxy.');
                return;
            }
            
            proxy.on('exception', function(reader, response, error) {
                Log.error('An error occured in a store\'s proxy.');
                Log.writeLine('    error      : ', error.message);
                Log.writeLine('    statusCode : ', response.status, response);
            });

            reader = proxy.getReader();
            if (!reader) {
                Ext.Error.raise('This component does not have a reader.');
                return;
            }

            reader.on('exception', function(reader, response, error) {
                Log.error('An error occured in a store\'s reader.');
                Log.writeLine('    error      : ', error.message);
                Log.writeLine('    statusCode : ', response.status, response);
            });

            var fn = reader.read;
            reader.read = function(response) {
                debugger;
                return fn.call(reader, response);
            };

            store.load();
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAADcklEQVR42mWTbWxTZRTH//fe3tt729uu7da9ONoNFmrYS3CbzgQ+OIgN3YsmLm7hm8QIIcEsflL2RROMAY0RP0iyaAzEoH7A7QNS1zESUAIypXNvtmNxnaPTvbTr2t7e9rLee+tDMVP0fHqePP9zzu+c5xwK/7VdA90w2noYB98l2O1mUEBuMyVrCdkPJTWMyOkr/5ZT26fGD0Qo6qWy9hZfzb4nUVppAUtpoB9oyGkFxBMKln+aR/z6ZAA804vZNzP/BGgdNEFVQ8+8drCmed8OlKYlHHDQcDs4eJwiQmsShu5l8FuJDXMTK/h58NoSDGw9gsezjwI0nQt43/AeatnvRuh2BN/01oIThf9V9/qXYdBNbswFVzB29uooZk74KDz1WWejb4//pVfbcOdeGguRDZxqYFFdIeLs7XU0V/MY8NbCyDH4jmT/eEqGu7EK48OTmA2Euygc/Pr8sZMHjsQoDkpOBavriP0RQ5oRUFHrxMJsFCN9LuxxWXFzLoaLUX2b6NMz1y9Q5a+Mrr1wYn+5nHwAwVBARgXiMoWMTmM6FEefNY0v+luIvICewTCcdaVFZ80m4ttzt9Ypd/8PucPH2vjNeBYLSQ3LKRV5DdginVfnolj98Gkip9H7yTSGYkY8/2wl3E5DMcjYxQmFanhnIvfy4QbeP51GLg9wDAWTyGL87ioud1jRQRw+v/Unjo5IaGurQkbKQ2ALqNkhIhwIK9SugeBac3d9eXQlS0p49ClbJKMSS+JqXwXKrDy8H4WxXF2JKhNBJy1Q1AJc5DJ5JbROlfTfPd/a4TkisDQ0VXtYKgw8g/VVGTOTqzCZaPAmAc0tFdDy+sNnMAaG0OoIjsxfoExHxzv3trv9dR4HJIJH0WS6KAqLS0kc9xix22HEuz+mwblsEBlCQBAsFg4L8wlM3bjfVWS2nfw14H1x5yELT9AVDWtZHeUbaXzV90SxpLyUQecdHU4rC4GjISk6xi4vjibPNPiKAexvzZjMZaZQ+3NVNWVElJA13I8kMdThgMPM4r2bCfzC8nDZOcTTedz4fmVJjmfrN99vym4vU+nb8yJtYS+17nX4drsEEAhEfpdgMTLY4jjUOQl2VEZwKhHQpXzvxilP5vFt/Nvspxe7SxzGHrvF0GU2c2YyDlCVLTmW0vyphDK8ObDzsXX+C6LyYkG4TGlfAAAAAElFTkSuQmCC',
        tooltip: 'Prints out information about the store.',

        // Prints out detailed information about the store, its proxy and its
        // reader.
        handler: function() {
            var window = this.up('[xtype=window]'),
                store,
                proxy,
                reader;

            if (!window.component.getStore) {
                Ext.Error.raise('This component does not have a store.');
                return;
            }

            store = window.component.getStore();
            proxy = store.getProxy();

            Log.writeLine(store.$className);
            Log.writeLine('    model    :', (store.model ? store.model.$className : '(none)'));
            Log.writeLine('    pageSize :', store.pageSize);

            if (proxy) {
                reader = proxy.getReader();

                Log.writeLine('    proxy :', (proxy.type ? proxy.type : '(ext3)'));
                if (proxy.type == 'ajax' || proxy.type == 'rest') {
                    Log.writeLine('        url             : ', proxy.url);
                    Log.writeLine('        model           : ', (proxy.model ? proxy.model.$className : '(none)'));
                    Log.writeLine('        headers         : ', proxy.headers);
                    Log.writeLine('        noCache         : ', proxy.noCache);
                    Log.writeLine('        timeout         : ', proxy.timeout);
                    Log.writeLine('        idParam         : ', proxy.idParam);
                    Log.writeLine('        pageParam       : ', proxy.pageParam);
                    Log.writeLine('        startParam      : ', proxy.startParam);
                    Log.writeLine('        limitParam      : ', proxy.limitParam);
                    Log.writeLine('        filterParam     : ', proxy.filterParam);
                    Log.writeLine('        sortParam       : ', proxy.sortParam);
                    Log.writeLine('        groupParam      : ', proxy.groupParam);
                    Log.writeLine('        extraParams     : ', proxy.extraParams);
                }
                if (proxy.type =='rest') {
                    Log.writeLine('        appendId        : ', proxy.appendId);
                }

                Log.writeLine('        reader : ' , (reader.type ? reader.type : '(ext3)'));
                Log.writeLine('            root               : ', reader.root); // check
                Log.writeLine('            successProperty    : ', reader.successProperty);
                Log.writeLine('            totalProperty      : ', reader.totalProperty);
                Log.writeLine('            implicitIncludes   : ', reader.implicitIncludes);

                if (reader.type == 'json') {
                    Log.writeLine('            useSimpleAccessors : ', proxy.useSimpleAccessors);
                }

                Log.writeLine('            rawData            : ', reader.rawData);
                if (reader.type == 'json') {
                    Log.writeLine('            jsonData           : ', reader.jsonData);
                }

            } else {
                Log.writeLine('    proxy: (null)');
            }
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gA+Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2ODApLCBkZWZhdWx0IHF1YWxpdHkK/9sAQwAIBgYHBgUIBwcHCQkICgwUDQwLCwwZEhMPFB0aHx4dGhwcICQuJyAiLCMcHCg3KSwwMTQ0NB8nOT04MjwuMzQy/9sAQwEJCQkMCwwYDQ0YMiEcITIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIy/8AAEQgAEAAQAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A9R1jxuLNZG0+zNzDFJ5T3DNtj3/3R/e6Umj+NxeLG2oWZtoZJPKW5Vt0e/8Aun+71rT1Hw3bXVq9vBFCkDkM0GNq7h0ZSPut2zg5HajTvDdta2qW88ULwISywY3LuPVmJ+83boMDtQB//9k=',
        tooltip: 'Hides the component.',

        // Nothing fancy, hides the component. Used when some component is laid
        // out incorrectly and you want to inspect what's beneath it.
        handler: function() {
            var window = this.up('[xtype=window]');

            window.component.hide();
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA50lEQVQ4jd3SrUpEURQF4M/LFKMTRERBLIoPYDCJj2AaRDBYDYPJMG8hYhKT1W4wiKjoG4iY/AmjBvEHBActK1yHe+26YXHYa++19j6Hw5+PgQqugTnMYjzcDS5wis86swbW0vxVgzu00/sjRnH2i7Af59GAYVyl8IJ1nNSI2nhOfo0ROAjxhJmYvlUYfKQ2jYdwhwUm8Y5uAPcVb3Sbs4tHvEarmfu0sJ2mFnql6T0spbaJ5azf7J+yiw4KzGMnWAi3gb2yoOgzWMUgjrPeVjCBIwxhpSyo+kgwhkVMJb/Efukd/lN8A2EKSWfnFrzqAAAAAElFTkSuQmCC',
        tooltip: 'Shows the component.',

        // See above.
        handler: function() {
            var window = this.up('[xtype=window]');

            window.component.show();
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAaZJREFUeNqcUz1MwkAU/noUEwPlR0SDMRDCojDp4KiTQTajxsHJzcHRHWNi3Bw1cdPdEBkEDBPubogmLgqRSAwREx0Ue+ddKaVQdOC7XO/a995373v3KgX2r94+v1XPDwUYAImP/8D4EB4yARxDtoYsgt/3EhgE7t2sRxYnCxyeb+G2fs3Zba1U+kESJhUx3zx21k4gYuW2b6lewPbmIhglupYeKYxpBBKhOD7N63I4gZlejMprtXXUHxUIjge67AbBzGQcR2d5EMnesrP+BJSVNN8OAWt5rs8msTF3wAmIkTFjrA8BBVW/dB/WyaBYvEH5uaoLAYKhEJxOBbXai3Z1hmj+8PtGMTbh0zPQ5ZQrZSyvrGqshBCk02n4/X4sJeJQVbVTKV7cXDaj7wFiTpBSirv7B201f3t8KhtTs5mUyZbmcCuW0jkcDkstLATaNfOcvN4RbRVSxBR7xek0AoQ8MwyC2PQULlIpo3+i0SgUxYVc5rL7RjlpJBzuvLuSGTYoRKw83Pwo8J9ioUmZpXPR7jmpp6P5u51IELHCFOHTg8HQ+BVgAGTo295GrHVqAAAAAElFTkSuQmCC',
        tooltip: 'Performs a manual refresh of the component\'s layout.',

        // Triggers a manual refresh of the layout. Useful when determining if
        // a UI layout issue is the result of a race condition. If you trigger
        // a refresh and the layout looks correct, then this is likely the case.
        handler: function() {
            var window = this.up('[xtype=window]');

            if (window.component.doLayout) {
                window.component.doLayout();
            } else if (window.component.updateLayout) {
                window.component.updateLayout();
            } else {
                Ext.Error.raise('Cannot find layout drawing method.');
            }
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAghJREFUeNpi/P//PwM2sK+BdRqQymTADdYA8XRGbAYANdcJKlo36kcuwKn79Y0dDNfWFzBgGADU7M3KJbTFNGk1A9PfSwx/P+9HkWcVTWd4deMAw80dvQz///9zZkHTzAxytoZ3GwML22+GXy92oWhmE89meHl1N8Ot3ZNAmm2dGn4fYUFzWa+0cay3kJw6w89nHWiac4Ca98A0mwM1nwKJMyHZnsMtopKvbJ/O8OvtapAiOGYRDmd4fnkHw81dE8B8IKgAqncFMcBhAORoMTGzXjVOWM3AwfmJ4debJQwM//+iuIBDto6BkVUSzH5zazfD7V1NDD8/v5SBeSFT2amCgYOPg4HhHyPYryDw89kEuAH/fn9g+P3pIcPdA7MYXt86sh4UhUBvPIW7ACiQgxzvKo7pDBKaZgz/fj5gYOY2AmliuHtwDsPv7x+bgBrrYeqwReNkKcPIHJAB//+8Z/j15S3DHaDGt3dPrITaehBFA8gAGN5bz9J+YZHL/98fT/7/9WbT/8eHC/4fbOX/DxSvRlaHjJE1V5+YovX/+5vD/z/dn/f/3DxTkMb5QGyGSzPcAKAim0OdYv+/vtj3/8H+rP8HWnhAmivwaYRhRqBCTiYWjm/qHo0Mz84vYvj49Op0qF8vMxABQNFowCOuAUzbdQz//vx0wAgkQgDqBUuQS4hxMjoGCDAAIqx2ssQjWqMAAAAASUVORK5CYII=',
        tooltip: 'Prints out a list of the component\'s managed listeners.',

        // Prints out a list of listeners on the component. Slightly less useful
        // as it also includes ones that are attached by the framework.
        handler: function() {
            var listeners = this.up('[xtype=window]').component.managedListeners;

            if (!listeners) {
                Log.writeLine('No managed listeners.');
            }

            Ext.each(listeners, function(listener) {
                Log.writeLine(listener.ename);

                if (listener.item.dom) {
                    Log.writeLine('    target : (dom) ' + listener.item.dom.id);
                } else {
                    Log.writeLine('    target : (cmp) ' + listener.item.$className);
                }

                Log.writeLine('    scope  : ' + (listener.scope != null ? listener.scope.$className : '(null)'));
            });
        }
    }, {
        width: 24,
        minWidth: 24,
        height: 22,
        icon: 'data:image/gif;base64,R0lGODlhEAAQAKIAAFx6qmB+rWaDsZGlx2yKt3KQunaUvv///yH5BAEAAAcALAAAAAAQABAAAAMqeLrc/jA6QOUCAxzDCxFBQ2mWQpYHOUojpn2haFbRWao01GYHXOeoICQBADs=',
        tooltip: 'Prints out a hierarchy of the component\'s children.',

        // Prints out a tree of this component's items and the items of all its
        // children. Useful to get a structured view of the component and how it
        // relates to other components in the hierarchy.
        handler: function() {
            var containerProperties = ['dockedItems', 'items'];
            var printRec = function(cmp, depth) {
                var baseStr = Ext.String.repeat(' ', depth);
                var propStr = baseStr + '  ';

                if (!depth) {
                    depth = 0;
                }

                Log.writeLine(baseStr + cmp.$className);
                Ext.each(containerProperties, function(property) {
                    if (!cmp[property]) {
                        return;
                    }

                    Log.writeLine(propStr + property);

                    if (cmp[property].each) {
                        cmp[property].each(function(item) {
                            printRec(item, depth + 4);
                        });
                    } else if (Ext.isArray(cmp[property])) {
                        for (var i = 0; i < cmp[property].length; i++) {
                            printRec(cmp[property][i], depth + 4);
                        }
                    }
                });
            };

            printRec(this.up('[xtype=window]').component);
        }
    }],

    defaults: {
        border: false,
        bodyPadding: 10
    },
    items: [{
        xtype: 'form',
        itemId: 'identityForm',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        
        items: [{
            xtype: 'textfield',
            name: 'className',
            fieldLabel: 'Class Name',
            readOnly: true
        }, {
            xtype: 'textfield',
            name: 'xtype',
            fieldLabel: 'XType',
            readOnly: true
        }, {
            xtype: 'textfield',
            name: 'id',
            fieldLabel: 'DOM Id',
            readOnly: true
        }]
    }],

    initComponent: function() {
        var me = this,
            identityForm,
            identityModel;

        me.callParent();

        identityForm = me.queryById('identityForm'),
        identityModel = {
            className: me.component.$className,
            xtype: me.component.xtype,
            id: me.component.id
        };

        me.setTitle(me.component.$className);

        identityForm.getForm().setValues(identityModel);
    }
});

/**
 * Debug menu mixin. Applying this as a mixin to a component will allow you to
 * bring up a debug menu for the component using shift and right click.
 * 
 * Would probably make more sense semantically as a plugin but oh well.
 */
Ext.define('Symphony.mixins.DebugMenu', {
    requires: 'Symphony.DebugMenu',

    /**
     * Undocumented ExtJS 4+ feature. Fires after the framework mixes in the
     * mixin's methods.
     * @param {Ext.Class} clazz Class that the mixin is applied to.
     */
    onClassMixedIn: function(clazz) {
       var initFn = clazz.prototype.initComponent;

        // Very briefly, we are wrapping the component's initComponent method
        // with our own initialization logic. The only thing we need to do is
        // bind a contextmenu handler after the component is drawn.
        clazz.prototype.initComponent = function() {
            var me = this;

            me.on('boxready', function() {
                var el = this.getEl().dom;

                el.addEventListener('contextmenu', function(e) {
                    var y = e.clientY;

                    if (!e.shiftKey) {
                        return;
                    }

                    e.stopPropagation();
                    e.preventDefault();

                    var win = Ext.create('Symphony.DebugMenu', {
                        x: e.clientX,
                        y: e.clientY,
                        component: me
                    });
                    win.show();

                    if (y + win.getHeight() > window.innerHeight) {
                        win.setY(window.innerHeight - win.getHeight());
                    }
                
                    return false;
                });
            });

            if (initFn) {
                initFn.call(me);
            }
        };
    }
});

// Applies this to all components. Careful. Uncomment to use debug menu by
// right clicking on a component.
//Ext.Component.mixin('debugmenu', Symphony.mixins.DebugMenu);
