﻿(function () {
    var registrationChanged = function (grid) {
        var publiccoursesgrid = Symphony.App.query('[itemId=portal.publiccoursesgrid]')[0];
        var upcomingeventsgrid = Symphony.App.query('[itemId=portal.upcomingeventsgrid]')[0];

        if (publiccoursesgrid) {
            publiccoursesgrid.refresh();
        }

        if (upcomingeventsgrid) {
            upcomingeventsgrid.refresh();
        }

        grid.fireEvent('registrationChanged');
    }

    Symphony.LinkHandlers = {
        simpleClick: function (grid, rowIndex, columnIndex, e) {
            var record = grid.getStore().getAt(rowIndex);
            var fieldName = grid.getColumnModel().getDataIndex(columnIndex);
            var data = record.get(fieldName);

            if (data) {
                Symphony.LinkHandlers.click(grid, e, data, record);
            }
        },
        click: function (grid, e, displayItem, record, callback) {
            // Allow links, and links with spans inside, and links with spans with imgs inside.
            // Just looking up two levels to see if there is a link.
            var target = e.target;

            if (target.tagName != 'A') {
                if (target.parentElement.tagName == 'A') {
                    target = target.parentElement;
                } else if (target.parentElement.parentElement && target.parentElement.parentElement.tagName == 'A') {
                    target = target.parentElement.parentElement;
                } 
            }
			
            // Could be an icon containing a link, so look for the proper item
            var link = displayItem.link ? displayItem.link : displayItem;

            if (target && target.tagName == 'A') {
                var methods = target.className.match(/method_([a-zA-Z]+)/);
                if (methods && methods.length > 1) {
                    var method = methods[1];

                    if (Symphony.LinkHandlers[method]) {
                        e.stopEvent();

                        // extract query params
                        var params = {};
                        if (link.queryParams) {
                            for (var i = 0; i < link.queryParams.length; i++) {
                                params[link.queryParams[i].key] = link.queryParams[i].value;
                            }
                        }

                        Symphony.LinkHandlers[method](grid, e, link, record, params, callback);
                        return true;
                    }
                }
            }

            return false;
        },
        launch: function (grid, e, link, record, params, callback, preventClose) {
            var launchSteps = record.get('displayLaunchStepsV2');
            var courseId = params.CourseID ? params.CourseID : 0;
            var userId = params.UserID ? params.UserID : 0;
            var trainingProgramId = params.TrainingProgramID ? params.TrainingProgramID : 0;

            if (Symphony.Portal && Symphony.Portal.onCourseCompletion) {
                Symphony.Portal.onCourseCompletion(function () {
                    if (typeof (callback) == 'function') {
                        callback();
                    }
                    registrationChanged(grid);
                });
            }

            var launchWizard = new Symphony.Portal.CourseLaunchWizard({
                launchSteps: launchSteps,
                launchLink: link,
                courseId: courseId,
                trainingProgramId: trainingProgramId,
                userId: userId,
                bookmark: link.bookmark ? link.bookmark : "",
                useNewVersion: true,
                preventClose: preventClose
            }).show();
        },
        register: function (grid, e, link, record, params, callback) {
            var classId = params.ClassID ? params.ClassID : 0;
            var courseId = params.CourseID ? params.CourseID : 0;
            var minDate = params.MinDate ? new Date(params.MinDate) : new Date();
            var maxDate = params.MaxDate ? new Date(params.MaxDate) : new Date();

            if (classId > 0) {
                Symphony.Portal.registerClass(classId, function () {
                    if (typeof (callback) == 'function') {
                        callback();
                    }
                });
            } else {
                var window = new Symphony.Portal.ClassRegistrationWindow({
                    minDate: minDate,
                    maxDate: maxDate,
                    courseId: courseId,
                    url: link.fullUrl,
                    listeners: {
                        'close': function () {
                            if (typeof (callback) == 'function') {
                                callback();
                            }
                            registrationChanged(grid);
                        }
                    }
                });
                window.show();
            }
        },
        togglefavorite: function (grid, e, link, record, params, callback) {
            var displayIcon = record.get('displayIsFavorite');
            
            record.set('isLoading', true);
            record.commit();

            Symphony.Ajax.request({
                url: link.fullUrl,
                method: link.baseUrl.method,
                complete: function (args) {
                    var result = Ext.decode(args.responseText);
                    if (result.success) {
                        record.set('displayIsFavorite', result.data);
                        switch (link.baseUrl.method) {
                            case 'post':
                                record.set('isFavorite', true);
                                break;
                            case 'delete':
                                record.set('isFavorite', false);
                                break;
                        }
                    }
                    record.set('isLoading', false);
                    record.commit();
                }
            });
        },
        unregister: function (grid, e, link, record, params, callback) {
            var url = link.fullUrl;

            Symphony.Ajax.request({
                url: url,
                success: function (result) {
                    if (typeof (callback) == 'function') {
                        callback();
                    }
                    registrationChanged(grid);
                }
            });
        },
        classdetails: function (grid, e, link, record, params, callback) {
            var id = params['ClassID'] ? params['ClassID'] : 0;
            
            Symphony.Portal.viewClassDetails(id);
        },
        launchgtm: function (grid, e, link, record, params, callback) {
            var url = link.fullUrl;
            var id = params['EventID'] ? params['EventID'] : 0;
            
            Symphony.Portal.launchGTM(id, url);
        },
        launchassignments: function (grid, e, link, record, params, callback) {
            var trainingProgramId = params.TrainingProgramID ? params.TrainingProgramID : 0;
            var userId = params.UserID ? params.UserID : Symphony.User.id;

            Symphony.Portal.launchAssignments(trainingProgramId, record.get('id'), userId, record);
        },
        launchcertificate: function(grid, e, link, record, params, callback) {
            var w = (790),
               h = (564),
               t = ((screen.height - h) / 2),
               l = ((screen.width - w) / 2);

            window.open(link.absoluteUrl, 'CertificateWindow', 'height=' + h + ',width=' + w + ',top=' + t + ',left=' + l + ',toolbar=no,menubar=no,status=no,location=no,scrollbars=yes,resizable=yes');
        }


    };
})();