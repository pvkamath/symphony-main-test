﻿/// IE patches
/*
* https://www.sencha.com/forum/showthread.php?286865-Messagebox-in-IE11-lt-br-gt-tag-causes-wrapping-and-text-cut-off
* there seems to be a bug with calculating the messagebox width in IE. 
* It appears to be off by 1px causing a wrap to occur when not expected, this causes some text to be cut off. 
* This will render a container off screen to get the width and adjust the width using the 1px offset.
* Affects >= IE7
*/

Ext.grid.plugin.DragDrop.override({
    init : function(view) {
        view.on('afterrender', this.onViewRender, this, {single: true}); // "afterrender" instead "render"
    }
});

// Ext 4.2.1 does not have the IE11 check, this is from > 4.2.1 IE check
var docMode = document.documentMode;
Ext.isIE = isIE = !Ext.isOpera && (/msie/.test(Ext.userAgent) || /trident/.test(Ext.userAgent))
Ext.isIE11 = Ext.isIE && ((/trident\/7\.0/.test(Ext.userAgent) && docMode != 7 && docMode != 8 && docMode != 9 && docMode != 10) || docMode == 11);

if (Ext.isIE) {
    Ext.override(Ext.window.MessageBox, {
        onShow: function () {
            this.callParent(arguments);

            var offset = 1;
            var helperEl = Ext.fly(this.helperElId) || Ext.fly(Ext.DomHelper.createDom({
                tag: 'div',
                id: this.helperElId,
                style: {
                    position: 'absolute',
                    left: '-1000px',
                    top: '-1000px',
                    'font-size': '11px',
                    'line-height': '13px',
                    'font-family': 'arial, tahoma, helvetica, sans-serif'
                }
            }, Ext.getBody()));

            if (this.msg && (this.msg.getWidth() !== (helperEl.dom.clientWidth + offset))) {
                helperEl.update(this.msg.value);
                this.msg.setWidth(Ext.Number.constrain(helperEl.dom.clientWidth + offset, this.minWidth, this.maxWidth));
            };

            this.center();
        }
    });
}
if (Ext.isIE && (Ext.isIE11)) {
    Ext.override(Ext.form.field.HtmlEditor, {
        insertAtCursor: function (text) {
            if (!this.activated) {
                return;
            }
            if (Ext.isIE && !Ext.isIE11) {
                this.win.focus();
                var doc = this.getDoc(), r = doc.selection.createRange();
                if (r) {
                    r.pasteHTML(text);
                    this.syncValue();
                    this.deferFocus();
                }
            } else if (Ext.isIE11) {
                this.win.focus();
                var doc = this.getDoc(), r = doc.getSelection().getRangeAt(0);
                if (r) {
                    var fragment = r.createContextualFragment(text);

                    doc.body.appendChild(fragment);

                    this.syncValue();
                    this.deferFocus();
                }
            }
            else {
                this.win.focus();
                this.execCmd('InsertHTML', text);
                this.deferFocus();
            }
        }
    })
}

/// END IE Patches





/* ##### UI MIGRATION SECTION #####
    * Just temporary functions for migrating client side ui logic
    * to the server.
    *
    * Making thse global so we can use them in any file. 
    *
    * Remove once ui is stablized.
    */
// For logging deprecated messages
var _log = function (msg) {
    if (console && console.info && Symphony.Settings.isServerSideUiLogicLogging) {
        console.info("UI MIGRATE: " + msg + "\r\n" + new Error('Call Stack:').stack.replace('Error:', ''));
    }
};

var _getArgs = function (args) {
    if (args && args.xtype) {
        return args.xtype;
    } else if (args && args.id) {
        return args.id;
    }
    return "[undetermined]";
};

// Quick access to ui setting
var _ui = function (args) {
    if (args && _getArgs(args)) {
        _log("ui toggle detected in " + _getArgs(args) + " update the code to always assume _ui() = true");
    }
    return Symphony.Settings.isServerSideUiLogicPortal
};

// allow flagging funciton as deprecated
// and displays console logs to indicate
// functions in use. 
var _deprecate = function (args) {
    if (_ui()) {
        var func = _getArgs(args) ? _getArgs(args) : "[Unknown function]";

        _log(func + " has been deprecated, but is still in use. Please resolve.");
    }
};
// Handler for deprected events 
// In the case where we have an old symphony handler still assigned 
// use this one instead when _ui(this) is on.
var _deprecatedHandler = function (args) {
    if (_ui()) {
        if (args && _getArgs(args)) {
            _log(_getArgs(args) + " has a deprecated handler assigned. Remove.");
        }
    }
};

var _getDeprecatedHandler = function (args) {
    if (_ui()) {
        if (args && _getArgs(args)) {
            _log(_getArgs(args) + " was assigned a deprecated handler. Remove.");
        }
    }
    return _deprecatedHandler;
}

var _nullHandler = function (args) {

};
/*
* ##### END UI MIGRATION SECTION #####
*/

/*
* Ext JS 3 to 4 upgrade - Adding a bunch of hacks to deal with ext4
*/
// Messagebox hack
// For some reason, in some cases, calling Ext.Msg.hide() after a Ext.Msg.wait() call, 
// it will fail "unable to call function endDrag of undefined"
// Going to override and add a check for dd first, and just add a stub function to ignore it
Ext.override(Ext.window.MessageBox, {
    hide: function (cmp) {
        if (!this.dd) {
            this.dd = {
                endDrag: function () {
                    // Have no idea why this doesn't exist
                    console.warn("Ext.window.MessageBox tried to call endDrag, but dd didn't exist. Look into why!");
                }
            }
        }

        this.callParent(arguments);
    }
});
// Allow using tabpanel.activate
Ext.override(Ext.tab.Panel, {
    activate: function (cmp) {
        this.setActiveTab(cmp);
    }
});
// Since treenodes are now just a record, we don't need to go node.attributes.record
// node is actually a record. This places the attributes field on the record so 
// we don't have to change all tree usage.
// also allow the option to use setText;
var dataModelConstructor = Ext.data.Model.prototype.constructor;
Ext.data.Model.prototype.constructor = function () {
    
    dataModelConstructor.apply(this, arguments);
    this.attributes = {
        record: this
    }
    this.setText = function (text) {
        this.set('text', text);
        this.commit();
    }
}
// Compnent hacks.
Ext.override(Ext.Component, {
    initComponent: function() {
        var me = this;

        // *******
        // initComponent hack
        // Merge the listeners that were passed in through the config
        // with the listeners that are defined by the base object.
        // Otherwise the listeners used by the base object would 
        // always override the listeners set in the config. 
        if (me.initialConfig && me.initialConfig.listeners) {
            var listeners = me.initialConfig.listeners;
            
            if (me.listeners) {
                Ext.apply(listeners, me.listeners);
            }
            if (me.initialConfig.listeners.rowclick) {
                me.initialConfig.listeners.itemclick = function (cmp, record, item, index, e, eOpts) {
                    me.initialConfig.listeners.rowclick(cmp, index, e);
                }
            }
            me.listeners = listeners;
        }

        me.callParent();


        /***********************/
        /* Adding ref support to extjs4
        * This is ugly.........
        * In ExtJS3 this gets done directly on init compontent, so the references
        * would be available immediately. In extjs4 init component does not have
        * any way to travel up the tree... there is no owner concept at this point. 
        * render/afterrender are too late, especially for the Artisan course editor.
        * 
        * Added seems to be called soon enough. For some reason, overriding the onAdded
        * function does not work for all components... have no idea why, but for example
        * it wouldn't work with a button within a toolbar. 
        * 
        * However, the added event still gets fired... (why onAdded doesn't work still a mystery...)
        * 
        * So this injects an added listener to every component, if there is a ref, it will trace
        * up the tree to find the specified parent. 
        *
        * Another issue... with items inside a toolbar, up() will no longer get to the container of the toolbar!!!!
        *.. Ugh, luckily there is an "isContained" property.... and no, it isn't a boolean! it's the reference
        * to the container of the toolbar! WEIRD!
        * So if we need to go up another level, and the result of up is undefined, then check if there is
        * an isContained property. If there is, set that as the next parent.
        *
        *........ Ext........ ugh......*/
        var oldAdded = function(cmp, container, pos, eOpts) {
            
        };
        if (me.listeners && me.listeners.added) {
            oldAdded = me.listeners.added;
        }

        if (!me.listeners) {
            me.listeners = {};
        }

        me.listeners.added = function (cmp, container, pos, eOpts) {
            if (cmp.ref) {
                var parts = cmp.ref.split('/');
                var name = parts[parts.length - 1];
                var parent = cmp;

                for (var i = 0; i < parts.length; i++) {
                    if (parent && parent.up && parent.up()) {
                        parent = parent.up();
                    } else if (parent.isContained && parent.isContained) {
                        parent = parent.isContained;
                    }
                }

                parent[name] = cmp;
            }
            oldAdded(cmp, container, pos, eOpts);
        }
        /*** End the ref nonsense!!!* */

        if (me.listeners) {
            if (me.listeners.rowclick) {
                me.listeners.itemclick = function (cmp, record, item, index, e, eOpts) {
                    me.listeners.rowclick(cmp, index, e);
                }
            }
            me.on(me.listeners);
            me.listeners = null;
        }
        me.enableBubble(me.bubbleEvents);
            

    },
    // Map find to query so we don't have to update all finds
    find: function (prop, value) {
        return this.query("[{0}={1}]".format(prop, value));
    },
    // Add find by type
    findByType: function(t) {
        return this.find('xtype', t);
    },
    // Map findby to queryby so we don't have to update all findBys
    findBy: function (f) {
        var queryInner = function (element) {
            if (f(element)) { // force all findBy functions to return true or false!!
                return true;
            }
            return false;
        }
        return this.queryBy(queryInner);
    }
});
Ext.override(Ext.data.Store, {
    setBaseParam: function (parameter, value) {
        this.getProxy().extraParams[parameter] = value;
    }
});
Ext.override(Ext.data.TreeStore, {
    setBaseParam: function (parameter, value) {
        this.getProxy().extraParams[parameter] = value;
    }
});
// Restore shortcut
Ext.TaskMgr = Ext.TaskManager;

/*
* End Ext hacks
*/

// Adding a findExact function for treeStore
// We have a case where a tree store is used in a BoxSelect field
// TreeStore does not have findExact and BoxSelect expects this.
// This returns the actual ID not the index!! 
Ext.override(Ext.data.TreeStore, {
    findExact: function (fieldName, value, startIndex) {
        var root = this.getRootNode(),
            index = 0,
            found = false,
            foundNode = null;

        if (!startIndex) {
            startIndex = 0;
        }

        root.cascadeBy(function (n) {
            if (startIndex >= index && !found) {
                if (n.get(fieldName) === value) {
                    found = true;
                    foundNode = n;
                }
            }
            index++;
        });

        if (foundNode) {
            return foundNode.getId();
        }
        return -1;
    },
    // BoxSelect uses getAt afterwards
    // to return the node from the tree
    // BoxSelect assumes it's receiving an index
    // and grabbing the node by the index. 
    // In this case, we are just translating
    // to the id since we don't really have
    // an index. 
    getAt: function (id) {
        return this.getNodeById(id);
    },
    queryBy: function (fn) {
        var records = [],
            collection = new Ext.util.MixedCollection(),
            index = 0;

        this.getRootNode().cascadeBy(function (n) {
            if (fn(n, index)) {
                records.push(n);
            }
        });

        collection.addAll(records);

        return collection;
    },
    removeFilter: function (toRemove, applyFilters) {
        var me = this;

        if (!me.remoteFilter && me.isFiltered()) {
            if (toRemove instanceof Ext.util.Filter) {
                me.filters.remove(toRemove);
            } else {
                me.filters.removeAtKey(toRemove);
            }

            if (applyFilters !== false) {

                // Not gone down to zero filters - re-filter Store
                if (me.filters.length) {
                    me.filter();
                }

                    // No filters left - let clearFilter do its thing.
                else {
                    me.clearFilter();
                }
            } else {
                me.fireEvent('filterchange', me, me.filters.items);
            }
        }
    }
});


(function () {
    /// Allows a store to post request data
    /// and maintain the get params for paging
    Ext.define('Ext.ux.data.proxy.JsonPostProxy', {
        extend: 'Ext.data.proxy.Ajax',
        alias: 'proxy.jsonpostproxy',
        actionMethods: {
            read: 'POST'
        },
        buildRequest: function (operation) {
            if (this.method) {
                this.actionMethods.read = this.method;
            } else {
                this.actionMethods.read = 'POST';
            }

            var request = this.callParent(arguments);
            request.jsonData = this.jsonData;
            if (this.overrideUrl) {
                request.url = this.overrideUrl;
            }

            return request;
        },
        applyEncoding: function (value) {
            return value;
        }
    });


    var oldDateFormat = Date.prototype.format;
    Date.prototype.formatSymphony = function (format) {
        if (format == 'microsoft') {
            // format to MS style
            return '/Date(' + this.getTime() + ')/';
        }
        return Ext.Date.format(this, format);
    };
    var _id = 0;
    var dateRegex = /\/Date\(-?(\d+[\-|\+]?\d{0,4})\)\//;
    var dateRegexAllowNegative = /\/Date\((-?\d+[\-|\+]?\d{0,4})\)\//;
    Symphony.quoteRegex = /\"/g;
    Symphony.singleQuoteRegex = /\'/g;

    Symphony.htmlEscape = function (str) {
        return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    };

    Symphony.escapeQuotes = function (str) {
        return str.replace(Symphony.quoteRegex, '\\"').replace(Symphony.singleQuoteRegex, "\\'");
    };

    Symphony.toHumanCase = function (text) {
        var result = text.replace(/([A-Z])/g, " $1");
        return (result.charAt(0).toUpperCase() + result.slice(1)).trim();
    };

    Symphony.map = function (array, fn, scope) {
        var result = [];
        for (var i = 0; i < array.length; i++) {
            result.push(fn.call(scope || this, array[i]));
        }
        return result;
    }

    Symphony.getIndexById = function (arr, id) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].id === id) {
                return i;
            }
        }
        return null;
    };

    Symphony.getIndexByProperty = function (arr, prop, val) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][prop] === val) {
                return i;
            }
        }
        return null;
    };

    Symphony.log = function (msg, params) {
        if (window && window.console) {
            if (params) {
                window.console.log(msg, params);
            } else {
                window.console.log(msg);
            }
        }
    };

    var urlParams;
    (window.onpopstate = function () {
        var match,
            pl = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query = window.location.search.substring(1);

        urlParams = {};
        while (match = search.exec(query))
            urlParams[decode(match[1])] = decode(match[2]);
    })();

    Symphony.getQueryParam = function (name) {
        return urlParams[name];
    };
    

    // does a full merge of two objects, including merging sub-objects and sub-arrays
    Symphony.merge = function (dest, src, useArrayIds) {
        useArrayIds = useArrayIds || false;
        for (var prop in src) {
            if (Ext.isObject(src[prop])) {
                if (!dest[prop]) {
                    dest[prop] = {};
                }
                Symphony.merge(dest[prop], src[prop], useArrayIds);
            } else if (Ext.isArray(src[prop])) {
                if (!Ext.isArray(dest[prop])) {
                    dest[prop] = [];
                }
                for (var i = 0; i < src[prop].length; i++) {
                    if (useArrayIds && (prop == "sections" || prop == "pages")) {
                        var index = i;
                        var mergeId = src[prop][i].clientId || src[prop][i].id;
                        if (mergeId) {
                            index = Symphony.getIndexById(dest[prop], mergeId);
                            if (index == null) {
                                continue;
                                //index = i; // if it fails, we assume it's because this is a new item, with negative values
                            }
                        }
                        Symphony.merge(dest[prop][index], src[prop][i], useArrayIds);

                        // after the merge, we don't need the temp client id anymore
                        if (src[prop][i].clientId) {
                            delete src[prop][i].clientId;
                        }
                    }
                    else {
                        Symphony.merge(dest[prop][i], src[prop][i], useArrayIds);
                    }
                }
            } else {
                dest[prop] = src[prop];
            }
        }
        return dest;
    };

    // Move position of an array element
    // http://stackoverflow.com/questions/5306680/move-an-array-element-from-one-array-position-to-another
    // Allows negative indexes too (to reference from the end of the array)
    Array.prototype.move = function (old_index, new_index) {
        while (old_index < 0) {
            old_index += this.length;
        }
        while (new_index < 0) {
            new_index += this.length;
        }
        if (new_index >= this.length) {
            var k = new_index - this.length;
            while ((k--) + 1) {
                this.push(undefined);
            }
        }
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    };


    var spellChecker;

    String.prototype.format = function () {
        var string = this;
        for (var i = 0; i < arguments.length; i++) {
            string = string.replace("{" + i + "}", arguments[i]);
        }
        return string;
    };

    String.prototype.replaceAll = function (token, newToken, ignoreCase) {
        var str, i = -1, _token;
        // ensure we're working w/ strings
        newToken = newToken + '';
        token = token + '';
        if ((str = this.toString()) && typeof token === "string") {
            _token = ignoreCase === true ? token.toLowerCase() : undefined;
            while ((i = (
            _token !== undefined ?
                str.toLowerCase().indexOf(
                            _token,
                            i >= 0 ? i + newToken.length : 0
                ) : str.indexOf(
                            token,
                            i >= 0 ? i + newToken.length : 0
                )
        )) !== -1) {
                str = str.substring(0, i)
                    .concat(newToken)
                    .concat(str.substring(i + token.length));
            }
        }
        return str;
    };

    // Array filter polyfill
    // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // Adds support for array filter in IE8
    if (!Array.prototype.filter) {
        Array.prototype.filter = function (fun /*, thisArg */) {
            "use strict";

            if (this === void 0 || this === null)
                throw new TypeError();

            var t = Object(this);
            var len = t.length >>> 0;
            if (typeof fun !== "function")
                throw new TypeError();

            var res = [];
            var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
            for (var i = 0; i < len; i++) {
                if (i in t) {
                    var val = t[i];

                    // NOTE: Technically this should Object.defineProperty at
                    //       the next index, as push can be affected by
                    //       properties on Object.prototype and Array.prototype.
                    //       But that method's new, and collisions should be
                    //       rare, so use the more-compatible alternative.
                    if (fun.call(thisArg, val, i, t))
                        res.push(val);
                }
            }

            return res;
        };
    }

    Ext.override(Ext.Panel, {
        setTabTip: function (tip) {
            //Ext.get(this.tabEl).child('span.x-tab-strip-text', true).qtip = tip;
        }
    });

    var oldSetValue = Ext.form.ComboBox.prototype.setValue;
    Ext.override(Ext.form.ComboBox, {
        setValue: function(v){
            oldSetValue.apply(this, arguments);
            this.fireEvent('set', this);
        }
    });

    // Enable state management for all "state aware" components
    Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
        expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 180)) // 180 days
    }));

    Ext.QuickTips.init();
    if (!window.Symphony) {
        window.Symphony = {};
    }

    Symphony.Modules.hasModule = function (m) {
        return (Symphony.Settings.modules.indexOf(m) > -1);
    };
    Symphony.Modules.hideDisabledModules = function () {
        var body = Ext.getBody();

        for (var prop in Symphony.Modules) {
            if (Symphony.Modules.hasOwnProperty(prop) && Ext.isString(Symphony.Modules[prop])) {
                var disabledClassName = prop.toLowerCase() + '-disabled';

                if (!Symphony.Modules.hasModule(Symphony.Modules[prop])) {
                    body.addCls(disabledClassName);
                } else {
                    body.removeCls(disabledClassName);
                }
            }
        }
    };
    Symphony.Modules.getModuleClass = function (m) {
        return m.toLowerCase() + '-module';
    };

    Ext.define('TimeStoreModel', {
        extend: 'Ext.data.Model',
        fields: [
            'hour',
            'minute',
            'text'
        ]
    });

    Symphony = Ext.apply(Symphony, {
        // the year the parser comes back with when it reads a non-null column that has a default
        // value; the year in the db is actually 1/1/1900, but since the JS parser doesn't handle them
        // propertly, it ends up with 2039.
        defaultYear: 2039,
        getCustomer: function () {
            return Symphony.User.customerDomain;
        },
        getUserTimezoneKey: function () {
            var zone = (new Date()).formatSymphony('T');
            // GTM only deals with standard time, not daylight savings
            if (zone.charAt(1) == 'D') {
                zone = zone.charAt(0) + 'S' + zone.charAt(2);
            }
            for (var i = 0; i < Symphony.TimeZones.length; i++) {
                if (Symphony.TimeZones[i].zone == zone) {
                    return Symphony.TimeZones[i].gtmId;
                }
            }
        },
        clone: function (o) {
            if (!o || 'object' !== typeof o) {
                return o;
            }
            if ('function' === typeof o.clone) {
                return o.clone();
            }
            var c = '[object Array]' === Object.prototype.toString.call(o) ? [] : {};
            var p, v;
            for (p in o) {
                if (o.hasOwnProperty(p)) {
                    v = o[p];
                    if (v && 'object' === typeof v) {
                        c[p] = Symphony.clone(v);
                    }
                    else {
                        c[p] = v;
                    }
                }
            }
            return c;
        },
        keys: function (o, f) {
            var keys = [];
            for (var p in o) {
                if (o.hasOwnProperty(p)) {
                    if (f && f(p, o[p], o)) {
                        keys.push(p);
                    } else if (!f) {
                        keys.push(p);
                    }
                }
            }
            return keys;
        },
        PortalLayout: {},
        Customer: {},
        Classroom: {},
        Network: {},
        Book: {},
        CourseAssignment: {},
        Categories: {},
        Certificates: {},
        Assignments: {},
        Libraries: {},
        ServiceProvider: {},
        Date: {},
        Settings: Ext.apply({
            defaultPageSize: 20,
            defaultComboPageSize: 10
        }, Symphony.Settings || {}),
        PermissionText: {
            "Artisan - Author": "Authors online courses.  Access to Artisan Assets, Learning Objects, Assemble Courses and Review tabs.",
            "Artisan - Packager": "Packages online courses.  Access to Artisan Configure and Deploy tab.",
            "Artisan - Viewer": "Reviews previously packaged courses.  Access to Artisan Review tab.",

            "Customer - Administrator": "All functions under the Student Management and Reporting tabs.",
            "Customer - Manager": "View only for Student Management and Reporting functions. Can reset user passwords.",
            "Customer - Limited Administrator": "All functions under the Student Management and Reporting tabs with dataview limited as specified.",
            "Customer - Limited Manager": "View only for Student Management and Reporting functions. Can reset user passwords with dataview limited as specified.",
            "Customer - User": "May take online courses, and register for classroom and virtual courses.",
            "Customer - Job Role Manager": "View only Student Management functions for users in the active user's job role.",
            "Customer - Location Manager": "View only Student Management functions for users in the active user's location.",

            "Classroom - ILT Manager": "All functions under Classroom Training tab, including approvals of student requests.",
            "Classroom - Instructor": "Eligible to be assigned as an instructor for classroom courses.  Enters student attendance and scores.",
            "Classroom - Resource Manager": "Eligible to be assigned as a resource manager for a given venue. Manages resources for classes within that venue.",
            "Classroom - Supervisor": "Eligible to be assigned as a student’s supervisor.  Receives and approves student training requests.",
            "Classroom - User": "The XX role provides full access to all features of the XX.", /*removed*/

            "CourseAssignment - Training Administrator": "All functions under the Course Management tab.",
            "CourseAssignment - Training Manager": "All functions under the Course Management tab, limited to editing their own Training Programs.",
            "CourseAssignment - Help Desk": "The XX role provides full access to all features of the XX.", /*removed*/

            "Collaboration - GTM Organizer": "Can initiate online meetings and invite internal and external users.",
            "Collaboration - GTW Organizer": "Can host and instruct virtual classes.",

            "Reporting - Analyst": "Can view reports.",
            "Reporting - Report Developer": "Can develop reports.",
            "Reporting - Job Role Analyst": "Can view reports for users in the active user's job role.",
            "Reporting - Location Analyst": "Can view reports for users in the active user's location.",
            "Reporting - Audience Analyst": "Can view reports for users in the active user's audience.",
            "Reporting - Supervisor": "" /* TODO: This text needs to be written */
        },
        TimeZones: [
            { zone: 'AST', gtmId: 59, displayName: 'Atlantic Standard Time' },
            { zone: 'EST', gtmId: 61, displayName: 'Eastern Standard Time' },
            { zone: 'CST', gtmId: 64, displayName: 'Central Standard Time' },
            { zone: 'MST', gtmId: 65, displayName: 'Mountain Standard Time' },
            { zone: 'PST', gtmId: 67, displayName: 'Pacific Standard Time' },
            { zone: 'AKST', gtmId: 68, displayName: 'Alaska Standard Time' },
            { zone: 'HST', gtmId: 69, displayName: 'Hawaiian Standard Time' }
        ],
        TimeStore: Ext.create('Ext.data.ArrayStore', {
            model: 'TimeStoreModel',
            type: 'array',
            data: [
                [0, 0, '12:00 AM'],
                [0, 15, '12:15 AM'],
                [0, 30, '12:30 AM'],
                [0, 45, '12:45 AM'],
                [1, 0, '1:00 AM'],
                [1, 15, '1:15 AM'],
                [1, 30, '1:30 AM'],
                [1, 45, '1:45 AM'],
                [2, 0, '2:00 AM'],
                [2, 15, '2:15 AM'],
                [2, 30, '2:30 AM'],
                [2, 45, '2:45 AM'],
                [3, 0, '3:00 AM'],
                [3, 15, '3:15 AM'],
                [3, 30, '3:30 AM'],
                [3, 45, '3:45 AM'],
                [4, 0, '4:00 AM'],
                [4, 15, '4:15 AM'],
                [4, 30, '4:30 AM'],
                [4, 45, '4:45 AM'],
                [5, 0, '5:00 AM'],
                [5, 15, '5:15 AM'],
                [5, 30, '5:30 AM'],
                [5, 45, '5:45 AM'],
                [6, 0, '6:00 AM'],
                [6, 15, '6:15 AM'],
                [6, 30, '6:30 AM'],
                [6, 45, '6:45 AM'],
                [7, 0, '7:00 AM'],
                [7, 15, '7:15 AM'],
                [7, 30, '7:30 AM'],
                [7, 45, '7:45 AM'],
                [8, 0, '8:00 AM'],
                [8, 15, '8:15 AM'],
                [8, 30, '8:30 AM'],
                [8, 45, '8:45 AM'],
                [9, 0, '9:00 AM'],
                [9, 15, '9:15 AM'],
                [9, 30, '9:30 AM'],
                [9, 45, '9:45 AM'],
                [10, 0, '10:00 AM'],
                [10, 15, '10:15 AM'],
                [10, 30, '10:30 AM'],
                [10, 45, '10:45 AM'],
                [11, 0, '11:00 AM'],
                [11, 30, '11:15 AM'],
                [11, 30, '11:30 AM'],
                [11, 45, '11:45 AM'],

                [12, 0, '12:00 PM'],
                [12, 15, '12:15 PM'],
                [12, 30, '12:30 PM'],
                [12, 45, '12:45 PM'],
                [13, 0, '1:00 PM'],
                [13, 15, '1:15 PM'],
                [13, 30, '1:30 PM'],
                [13, 45, '1:45 PM'],
                [14, 0, '2:00 PM'],
                [14, 15, '2:15 PM'],
                [14, 30, '2:30 PM'],
                [14, 45, '2:45 PM'],
                [15, 0, '3:00 PM'],
                [15, 15, '3:15 PM'],
                [15, 30, '3:30 PM'],
                [15, 45, '3:45 PM'],
                [16, 0, '4:00 PM'],
                [16, 15, '4:15 PM'],
                [16, 30, '4:30 PM'],
                [16, 45, '4:45 PM'],
                [17, 0, '5:00 PM'],
                [17, 15, '5:15 PM'],
                [17, 30, '5:30 PM'],
                [17, 45, '5:45 PM'],
                [18, 0, '6:00 PM'],
                [18, 15, '6:15 PM'],
                [18, 30, '6:30 PM'],
                [18, 45, '6:45 PM'],
                [19, 0, '7:00 PM'],
                [19, 15, '7:15 PM'],
                [19, 30, '7:30 PM'],
                [19, 45, '7:45 PM'],
                [20, 0, '8:00 PM'],
                [20, 15, '8:15 PM'],
                [20, 30, '8:30 PM'],
                [20, 45, '8:45 PM'],
                [21, 0, '9:00 PM'],
                [21, 15, '9:15 PM'],
                [21, 30, '9:30 PM'],
                [21, 45, '9:45 PM'],
                [22, 0, '10:00 PM'],
                [22, 15, '10:15 PM'],
                [22, 30, '10:30 PM'],
                [22, 45, '10:45 PM'],
                [23, 0, '11:00 PM'],
                [23, 30, '11:15 PM'],
                [23, 30, '11:30 PM'],
                [23, 45, '11:45 PM']
            ]
        }),
        Definitions: {
            artisanAsset: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'path' },
                { name: 'assetTypeId' },
                { name: 'template' },
                { name: 'previewTemplate' },
                { name: 'keywords' },
                { name: 'isPublic' },
                { name: 'width' },
                { name: 'height' },
                { name: 'alternateHtml' },
                { name: 'hasAlternateHtml' }
            ],
            artisanAssetType: [
                { name: 'id' },
                { name: 'name' },
                { name: 'extensions' },
                { name: 'template' },
                { name: 'hasAlternateHtml' }
            ],
            artisanTheme: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'cssPath' },
                { name: 'skinCssPath' },
                { name: 'isDynamic' }
            ],
            artisanSourceImportInfo: [
                { name: 'id' },
                { name: 'name' },
                { name: 'value' }
            ],
            assignment: [
                { name: 'sectionId' },
                { name: 'courseName' },
                { name: 'courseId' },
                { name: 'trainingProgramId' },
                { name: 'attempt' },
                { name: 'date' },
                { name: 'name' },
                { name: 'userId' },
                { name: 'numQuestions' },
                { name: 'totalQuestions' },
                { name: 'numCorrect' },
                { name: 'score' },
                { name: 'isMarked' },
                { name: 'isPassed' },
                { name: 'isOld' },
                { name: 'artisanCourseID'},
                { name: 'isOldArtisanCourse' },
                { name: 'groupId' },
                // Display
                { name: 'displayAssignmentStatus' },
                { name: 'displaySubmitDate' }
            ],
            assignmentResponse: [
                { name: 'id' },
                { name: 'userId' },
                { name: 'trainingProgramId' },
                { name: 'courseId' },
                { name: 'pageId' },
                { name: 'sectionId' },
                { name: 'attempt' },
                { name: 'date' },
                { name: 'question' },
                { name: 'response' },
                { name: 'correctResponse' },
                { name: 'responseStatus' },
                { name: 'instructorFeedback' }
            ],
            publicDocument: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'uploaded' },
                { name: 'categoryName' }
            ],
            category: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'parentId' },
                { name: 'levelText' },
                { name: 'levelIndentText' },
                { name: 'categoryKey' },
                { name: 'maxCourseWork' }
            ],
            author: [
                { name: 'id' },
                { name: 'email' },
                { name: 'firstName' },
                { name: 'lastName' },
                { name: 'fullName' }
            ],
            secondaryCategory: [
                { name: 'id' },
                { name: 'name' }
            ],
            certificateTemplate: [
                { name: 'id' },
                { name: 'name' },
                { name: 'path' },
                { name: 'levelIndentText' },
                { name: 'certificateTemplateId' },
                { name: 'parentId' }
            ],
            trainingProgram: [
                { name: 'id' },
                { name: 'associatedImageData' },
                { name: 'name' },
                { name: 'categoryName' },
                { name: 'dueDate' },
                { name: 'startDate' },
                { name: 'endDate' },
                { name: 'isNewHire' },
                { name: 'isLive' },
                { name: 'ownerId' },
                { name: 'internalCode' },
                { name: 'description' },
                { name: 'cost' },
                { name: 'courseCount' },
                { name: 'completed' },
                { name: 'requiredCourses' },
                { name: 'electiveCourses' },
                { name: 'finalAssessments' },
                { name: 'dateCompleted' },
                { name: 'categoryId' },
                { name: 'disableScheduled' },
                { name: 'messageBoardId' },
                { name: 'trainingProgramParameterOverrides' },
                { name: 'surveyId' },
                { name: 'originalSurveyName' },
                { name: 'originalSurveyId' },
                { name: 'surveyName' },
                { name: 'isSurveyMandatory' },
                { name: 'surveyTaken' },
                { name: 'surveysRequired' },
                { name: 'userStartTime' },
                { name: 'affidavitId' },
                { name: 'trainingProgramAffidavitId' },
                { name: 'isFinalAffidavitComplete' },
                { name: 'isTrainingProgramAffidavitComplete' },
                { name: 'affidavitName' },
                { name: 'proctorFormId' },
                { name: 'proctorFormName' },
                { name: 'certificatePath' },
                { name: 'assignmentsRequired' },
                { name: 'metaDataJson' },
                { name: 'sku' },
                { name: 'isCourseWorkAllowed' },
                { name: 'purchasedFromBundleName' },
                { name: 'percentComplete' },
                { name: 'isCourseTimingDisabled' },
                { name: 'isSalesforce' },
                { name: 'hasUserStarted' },
                { name: 'isNmls' },
                { name: 'licenses' },
                { name: 'isUsingSessions' },
                { name: 'sessionCourseId' },
                { name: 'isSessionRegistered' },
                { name: 'isSessionActive' },
                { name: 'courseUnlockModeId' },
                { name: 'sharedFlag' },
                { name: 'isRequiredComplete' },
                { name: 'isElectiveComplete' },
                { name: 'isElectivesAvailable' },
                { name: 'isOptionalAvailable' },
                { name: 'isFinalAvailable' },
                { name: 'isAccreditationSurveyAvailable' },
                { name: 'isAssignmentsComplete' },
                { name: 'assignmentCount' },
                { name: 'isActive' },
                { name: 'allowPrinting' },
                { name: 'isMinCourseWorkComplete' },
                { name: 'minCourseWork' },
                { name: 'totalSeconds' },
                { name: 'accreditationSurveys' },
                { name: 'isAccreditationSurveysComplete', type: 'bool' },
                // Display Fields
                { name: 'displayCompleted' },
                { name: 'displayDueDate' },
                { name: 'displaySurvey' },
                { name: 'displayAccreditationSurvey' },
                { name: 'displayIsElectivesAvailable' },
                { name: 'displayIsOptionalAvailable' },
                { name: 'displayIsFinalAvailable' },
                { name: 'displayIsAccreditationSurveyAvailable' },
				{ name: 'originalReleaseDate' },
                { name: 'goalsAndObjectives' }
            ],
            licenseAssignments: [
                { name: 'customer' },
                { name: 'customerId' },
                { name: 'name' },
                { name: 'licenseId' },
                { name: 'licenseName' },
                { name: 'assignmentStatus' },
                { name: 'assignmentStatusId' },
                { name: 'userId' },
                { name: 'firstName' },
                { name: 'lastName' },
                { name: 'locationId' },
                { name: 'locationName' },
                { name: 'jobRoleId' },
                { name: 'jobRoleName' },
                { name: 'audienceId' },
                { name: 'audienceName' },
                { name: 'startDate' },
                { name: 'expiryDate' },
                { name: 'parentObject' },
                { name: 'licenseAssignmentId' },
                { name: 'parentObject' },
                { name: 'renewalSubmittedDate' },
                { name: 'expiresOnDate' },
                { name: 'daysUntilExpiry' },
                { name: 'renewByDate' },
                { name: 'entityName' }
            ],
            licenseAssignmentNote: [
                { name: 'id' },
                { name: 'licenseAssignmentId' },
                { name: 'modified' },
                { name: 'body' }
            ],
            licenseAssignmentDocument: [
                { name: 'id' },
                { name: 'licenseAssignmentId' },
                { name: 'filename' },
                { name: 'filetype' },
                { name: 'modified' }
            ],
            license: [
                { name: 'id' },
                { name: 'parentId' },
                { name: 'name' },
                { name: 'licenseType' },
                { name: 'accreditationBoard' },
                { name: 'accreditationBoardId' },
                { name: 'jurisdiction' },
                { name: 'profession' },
                { name: 'approvalCode' },
                { name: 'description' },
                { name: 'dataFields' },
                { name: 'isPrimary' },
                { name: 'expirationRuleId' },
                { name: 'expirationRuleAfterDays' },
                { name: 'renewalLeadTimeInDays' }
            ],
            trainingProgramBundle: [
                { name: 'id' },
                { name: 'name' },
                { name: 'sku' },
                { name: 'description' },
                { name: 'customerId' },
                { name: 'categoryId' },
                { name: 'status' },
                { name: 'levelIndentText' }
            ],
            trainingProgramLeader: [
               { name: 'id' },
               { name: 'trainingProgramId' },
               { name: 'userId' },
               { name: 'firstName' },
               { name: 'middleName' },
               { name: 'lastName' },
               { name: 'fullName' },
               { name: 'gtmOrganizerStatus' },
               { name: 'isPrimary' }
            ],
            instructedClass: [
                { name: 'id' },
                { name: 'name' },
                { name: 'lockedIndicator' },
                { name: 'isVirtual' },
                { name: 'minClassDate' },
                { name: 'courseTypeId' },
                { name: 'trainingProgramId' },
                { name: 'trainingProgramName' },
                { name: 'requiredSyllabusOrder' },
                { name: 'syllabusTypeId' },
                { name: 'messageBoardId' },
                { name: 'trainingProgramMessageBoardId' },
                { name: 'groupByTrainingProgramId' },
                { name: 'hasUnmarkedAssignments' }
            ],
            userAssignment: [
                { name: 'UserID', readOnly: true },
                { name: 'customerId' },
                { name: 'firstName' },
                { name: 'lastName' },
                { name: 'fullName', readOnly: true },
                { name: 'ClassID' },
                { name: 'ClassName' },
                { name: 'ClassStartTime' },
                { name: 'MaxGradedDate' },
                { name: 'MaxSubmitDate' },
                { name: 'AssignmentsAttempted' },
                { name: 'AssignmentsMarked' },
                { name: 'AssignmentsCompleted' },
                { name: 'AssignmentsInCourse' },
                { name: 'IsAssignmentsFailed' },
                { name: 'IsAssignmentsComplete' },
                { name: 'IsNew' }
            ],
            user: [
                { name: 'id', readOnly: true },
                { name: 'customerId' },
                { name: 'firstName' },
                { name: 'lastName' },
                { name: 'fullName', readOnly: true },
                { name: 'supervisor', readOnly: true },
                { name: 'secondarySupervisor', readOnly: true },
                { name: 'reportingSupervisor', readOnly: true },
                { name: 'username' },
                { name: 'status' },
                { name: 'location', readOnly: true },
                { name: 'jobRole', readOnly: true },
                { name: 'audiences', readOnly: true },
                { name: 'ssn' },
                { name: 'nmlsNumber' },
                { name: 'employeeNumber' },
                { name: 'telephoneNumber' },
                { name: 'isAccountExec' },
                { name: 'isCustomerCare' },
                { name: 'timeZone' },
                { name: 'lockedCourseCount' },
                { name: 'isAuthor' },
                { name: 'associatedImageData' },
                { name: 'customerName' },
                { name: 'salesChannelName' },
                { name: 'salesChannelId' },
                { name: 'email' }
            ],
            hierarchy: [
                { name: 'id' },
                { name: 'name' },
                { name: 'parentId' }
            ],
            course: [
                { name: 'id' },
                { name: 'associatedImageData' },
                { name: 'name' },
                { name: 'courseTypeId' },
                { name: 'internalCode' },
                { name: 'description' },
                { name: 'courseObjective' },
                { name: 'score' },
                { name: 'startDate' },
                { name: 'endDate' },
                { name: 'dueDate' },
                { name: 'activeAfterDate' },
                { name: 'hasStarted' },
                { name: 'hasEnded' },
                { name: 'classId' },
                { name: 'passed' },
                { name: 'registrationId' },
                { name: 'registrationStatusId' },
                { name: 'attendedIndicator' },
                { name: 'publicIndicator' },
                { name: 'credit' },
                { name: 'perStudentFee' },
                { name: 'courseCompletionTypeId' },
                { name: 'courseCompletionTypeDescription' },
                { name: 'onlineCourseId' },
                { name: 'onlineCourseName' },
                { name: 'presentationTypeId' },
                { name: 'presentationTypeDescription' },
                { name: 'publicIndicator' },
                { name: 'createdOn' },
                { name: 'modifiedOn' },
                { name: 'version' },
                { name: 'webinarKey' },
                { name: 'launchUrl' },
                { name: 'isStartingSoon' },
                { name: 'categoryId' },
                { name: 'categoryName' },
                { name: 'numberOfDays' },
                { name: 'dailyDuration' },
                { name: 'artisanCourseId' },
                { name: 'messageBoardId' },
                { name: 'proctorRequiredIndicator' },
                { name: 'parameters' },
                { name: 'canMoveForwardIndicator' },
                { name: 'surveyId' },
                { name: 'surveyName' },
                { name: 'surveyTaken' },
                { name: 'isSurveyMandatory' },
                { name: 'levelIndentText' },
                { name: 'enforceCanMoveForwardIndicator' },
                { name: 'certificateEnabled' },
                { name: 'activeAfterMinutes' },
                { name: 'expiresAfterMinutes' },
                { name: 'isRelativeActiveAfter' },
                { name: 'isRelativeDueDate' },
                { name: 'minutesUntilActive' },
                { name: 'minutesUntilExpired' },
                { name: 'hasLaunched' },
                { name: 'activeAfterUnits' },
                { name: 'expiresAfterUnits' },
                { name: 'certificatePath' },
                { name: 'isAssignmentMarked' },
                { name: 'isAssignmentRequired' },
                { name: 'hasAssignments' },
                { name: 'metaDataJson' },
                { name: 'isUserValidationAllowed' },
                { name: 'isCourseWorkAllowed' },
                { name: 'affidavitId' },
                { name: 'hasTrainingProgramStarted' },
                { name: 'denyAccessAfterDueDateIndicator' },
                { name: 'activeAfterMode' },
                { name: 'dueMode' },
                { name: 'isUsingSession' },
                { name: 'onlineCourseRollupId' },
                { name: 'completed' },
                { name: 'attemptCount' },
                { name: 'testAttemptCount' },
                { name: 'assignmentsInCourse' },
                { name: 'assignmentsMarked' },
                { name: 'assignmentsCompleted' },
                { name: 'duration' },
                { name: 'navigationPercentage' },
                { name: 'navigationPercentageDisplay' },
                { name: 'isUseAutomaticDuration' },
                { name: 'automaticDuration' },
                { name: 'isDisplayAssignmentLaunchLink' },
                { name: 'allowPrinting' },
                { name: 'totalSeconds' },
                { name: 'totalSecondsRounded' },
                { name: 'totalSecondsLastAttempt' },
                { name: 'isSurvey', type: 'bool' },
                //Versioning
                { name: 'symphonyCoreVersion' },
                { name: 'playbackCoreVersion' },
                { name: 'deliveredCoreVersion' },
                { name: 'deliveredOnlineCourseVersion' },
                { name: 'deliveredArtisanCourseID' },
                { name: 'deliveredArtisanCourseCreatedOn' },
                { name: 'currentCoreVersion' },
                { name: 'currentCourseVersion' },
                { name: 'currentArtisanCourseID' },
                { name: 'currentArtisanCourseCreatedOn' },
                //Display
                { name: 'displayCourseLink' },
                { name: 'displayLaunchSteps' },
                { name: 'displayLaunchStepsV2' },
                { name: 'displayScore' },
                { name: 'displayCertificate' },
                { name: 'displayDueDate' },
                { name: 'displayPrint' },
                { name: 'displayAssignmentStatus' },
                { name: 'displayAssignmentsIcon' },
                { name: 'displayAssignmentLaunchText' },
                { name: 'displayPrintIcon' }
            ],
            artisanCourse: [
                { name: 'id' },
                { name: 'contentType' },
                { name: 'name' },
                { name: 'text' },
                { name: 'description' },
                { name: 'keywords' },
                { name: 'createdOn' },
                { name: 'levelIndentText' },
                { name: 'categoryId' },
                { name: 'modifiedOn' }
            ],
            artisanTemplate: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'html' },
                { name: 'thumbPath' },
                { name: 'pageType' },
                { name: 'questionType' },
                { name: 'cssText' }
            ],
            artisanQuestionType: [
                { name: 'id' },
                { name: 'name' },
                { name: 'displayName' }
            ],
            artisanAnswer: [
                { name: 'id' },
                { name: 'pageId' },
                { name: 'text' },
                { name: 'sort' },
                { name: 'isCorrect' },
                { name: 'feedback' }
            ],
            artisanDeployment: [
                { name: 'id' },
                { name: 'courseIds' },
                { name: 'customerIds' },
                { name: 'name' },
                { name: 'notes' },
                { name: 'isUpdate' },
                { name: 'isUpdateOnly' },
                { name: 'isAutoUpdate' },
                { name: 'toArtisan' },
                { name: 'toSymphony' },
                { name: 'toMars' },
                { name: 'createdOn' },
                { name: 'running' },
                { name: 'queued' }
            ],
            artisanDeploymentInfo: [
                { name: 'message' },
                { name: 'result' },
                { name: 'courseId' },
                { name: 'courseName' },
                { name: 'customerId' },
                { name: 'customerName' },
                { name: 'date' },
                { name: 'currentCount' },
                { name: 'totalCount' },
                { name: 'running' },
                { name: 'queued' }
            ],
            upcomingEvent: [
                { name: 'eventId' },
                { name: 'eventName' },
                { name: 'groupId' },
                { name: 'eventType' },
                { name: 'startDate' },
                { name: 'endDate' },
                { name: 'webinarRegistrationId' },
                { name: 'webinarKey' },
                { name: 'isAdminRegistration' },
                { name: 'webinarLaunchUrl' },
                { name: 'joinUrl' },
                { name: 'userId' },
                { name: 'messageBoardId' },
                // Display
                { name: 'displayCourseLink' },
                { name: 'displayStartDate' },
                { name: 'displayEventLink' },
                { name: 'displayEventType' }
            ],
            presentationType: [
                { name: 'id' },
                { name: 'description' }
            ],
            courseCompletionType: [
                { name: 'id' },
                { name: 'description' },
                { name: 'codeName' }
            ],
            transcriptEntry: [
                { name: 'courseId' },
                { name: 'courseName' },
                { name: 'courseTypeId' },
                { name: 'classId' },
                { name: 'registrationId' },
                { name: 'registrationStatusId' },
                { name: 'passOrFail' },
                { name: 'passed' },
                { name: 'completeOrIncomplete' },
                { name: 'completed' },
                { name: 'onlineTestId' },
                { name: 'surveyId' },
                { name: 'isPublic' },
                { name: 'score' },
                { name: 'attemptDate' },
                { name: 'attemptTime' },
                { name: 'attemptCount' },
                { name: 'startDate' },
                { name: 'endDate' },
                { name: 'isAssignmentMarked' },
                { name: 'isAssignmentRequired' },
                { name: 'trainingProgramId' },
                { name: 'trainingProgramName' },
                { name: 'isHistorical' },
                { name: 'isTrainingProgramRollupComplete' }
            ],
            klass: [
                { name: 'classId' },
                { name: 'courseId' },
                { name: 'className' },
                { name: 'classDescription' },
                { name: 'classRoomName' },
                { name: 'venueName' },
                { name: 'dates' },
                { name: 'startDate' },
                { name: 'numberOfDays' },
                { name: 'dailyDuration' },
                { name: 'registrationClosed' },
                { name: 'webinarKey' },
                { name: 'isAdminRegistration' },
                //Display
                { name: 'displayStartDate' },
                { name: 'displayLocation' },
                { name: 'displayRegisterLink' }
            ],
            classroomClass: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'internalCode' },
                { name: 'courseId' },
                { name: 'courseName' },
                { name: 'timeZoneId' },
                { name: 'timeZoneDescription' },
                { name: 'capacityOverride' },
                { name: 'lockedIndicator' },
                { name: 'additionalInstructions' },
                { name: 'notes' },
                { name: 'objective' },
                { name: 'isVirtual' },
                { name: 'surveyId' },
                { name: 'venueId' },
                { name: 'venueName' },
                { name: 'venueCity' },
                { name: 'roomId' },
                { name: 'roomName' },
                { name: 'location' },
                { name: 'minClassDate' },
                { name: 'maxClassDate' },
                { name: 'classDates' },
                { name: 'classInstructors' },
                { name: 'numberOfDays' },
                { name: 'dailyDuration' },
                { name: 'approvalRequired' },
                { name: 'metaDataJson' },
                { name: 'isAdminRegistration' }
            ],
            room: [
                { name: 'id' },
                { name: 'name' },
                { name: 'internalCode' },
                { name: 'roomCost' },
                { name: 'capacity' },
                { name: 'networkAccessIndicator' },
                { name: 'lockedIndicator' },
                { name: 'venueId' }
            ],
            venue: [
                { name: 'id' },
                { name: 'name' },
                { name: 'internalCode' },
                { name: 'city' },
                { name: 'stateId' },
                { name: 'stateName' },
                { name: 'stateDescription' },
                { name: 'timeZoneId' },
                { name: 'timeZoneDescription' }
            ],
            message: [
                { name: 'id' },
                { name: 'senderId' },
                { name: 'sender' },
                { name: 'recipientId' },
                { name: 'recipient' },
                { name: 'subject' },
                { name: 'body' },
                { name: 'isRead' },
                { name: 'createdOn' },
                { name: 'priority' }
            ],
            notificationTemplate: [
                { name: 'id' },
                { name: 'subject' },
                { name: 'body' },
                { name: 'displayName' },
                { name: 'description' },
                { name: 'isScheduled' },
                { name: 'enabled' },
                { name: 'scheduleParameterId' },
                { name: 'scheduleParameter' },
                { name: 'selectedRecipients' },
                { name: 'ccClassroomSupervisors' },
                { name: 'ccReportingSupervisors' },
                { name: 'percentage' },
                { name: 'relativeScheduledMinutes' },
                { name: 'isUserCreated' }
            ],
            meeting: [
                { name: 'id' },
                { name: 'startDateTime' },
                { name: 'endDateTime' },
                { name: 'gtmMeetingId' },
                { name: 'gtmOrganizerId' },
                { name: 'meetingId' },
                { name: 'subject' },
                { name: 'joinUrl' },
                { name: 'meetingType' },
                { name: 'maxParticipants' },
                { name: 'timeZoneKey' },
                { name: 'passwordRequired' },
                { name: 'conferenceCallInfo' },
                { name: 'internalInvitees' },
                { name: 'externalInvitees' },
                { name: 'isEditable' },
                { name: 'isOrganizer' }
            ],
            trainingProgramFile: [
                { name: 'id' },
                { name: 'filename' },
                { name: 'createdOn' },
                { name: 'title' },
                { name: 'description' },
                { name: 'percentUploaded' },
                { name: 'swfFileId' }
            ],
            courseFile: [
                { name: 'id' },
                { name: 'filename' },
                { name: 'createdOn' },
                { name: 'title' },
                { name: 'description' },
                { name: 'percentUploaded' },
                { name: 'swfFileId' }
            ],
            state: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' }
            ],
            timeZone: [
                { name: 'id' },
                { name: 'description' }
            ],
            classInstructor: [
                { name: 'id' },
                { name: 'classId' },
                { name: 'instructorId' },
                { name: 'firstName' },
                { name: 'middleName' },
                { name: 'lastName' },
                { name: 'fullName' },
                { name: 'gtmOrganizerStatus' },
            /*{name: 'canHostVirtual' },*/
                { name: 'collaborator' }
            ],
            registration: [
                { name: 'id' },
                { name: 'classId' },
                { name: 'registrantId' },
                { name: 'registrationTypeId' },
                { name: 'registrationStatusId' },
                { name: 'classCapacity' },
                { name: 'firstName' },
                { name: 'middleName' },
                { name: 'lastName' },
                { name: 'fullName' },
                { name: 'score' },
                { name: 'attendedIndicator' },
                { name: 'modifiedOn' },
                { name: 'canMoveForwardIndicator' }
            ],
            classResource: [
                { name: 'id' },
                { name: 'classId' },
                { name: 'resourceId' },
                { name: 'name' },
                { name: 'cost' },
                { name: 'venueId' },
                { name: 'venueName' },
                { name: 'roomId' },
                { name: 'roomName' },
                { name: 'location' }
            ],
            classDate: [
                { name: 'id' },
                { name: 'classId' },
                { name: 'startDateTime' },
                { name: 'duration' }
            ],
            venueResourceManager: [
                { name: 'id' },
                { name: 'resourceManagerId' },
                { name: 'venueId' },
                { name: 'firstName' },
                { name: 'middleName' },
                { name: 'lastName' },
                { name: 'fullName' }
            ],
            resource: [
                { name: 'id' },
                { name: 'name' },
                { name: 'cost' },
                { name: 'venueId' },
                { name: 'roomId' }
            ],
            locationModel: [
                { name: 'id' },
                { name: 'parentId' },
                { name: 'name' },
                { name: 'levelText' },
                { name: 'levelIndentText' }
            ],
            jobRole: [
                { name: 'id' },
                { name: 'parentId' },
                { name: 'name' },
                { name: 'levelText' },
                { name: 'levelIndentText' }
            ],
            audience: [
                { name: 'id' },
                { name: 'parentId' },
                { name: 'name' },
                { name: 'levelText' },
                { name: 'levelIndentText' }
            ],
            locationx: [
                { name: "address1" },
                { name: "address2" },
                { name: "city" },
                { name: "id" },
                { name: "internalCode" },
                { name: "name" },
                { name: "parentId" },
                { name: "postalCode" },
                { name: "salesforcePartnerCode" },
                { name: "state" },
                { name: "telephoneNumber" }
            ],
            userStatus: [
                { name: 'id' },
                { name: 'description' }
            ],
            
            salesChannel: [
                { name: 'id' },
                { name: 'parentId' },
                { name: 'name' }
            ],
            customer: [
                { name: 'id' },
                { name: 'salesChannelId' },
                { name: 'salesChannelName' },
                { name: 'parentId' },
                { name: 'name' },
                { name: 'evaluationIndicator' },
                { name: 'evaluationEndDate' },
                { name: 'suspendedIndicator' },
                { name: 'userLimit' },
                { name: 'selfRegistrationIndicator' },
                { name: 'selfRegistrationPassword' },
                { name: 'lockoutCount' },
                { name: 'subdomain' },
                { name: 'locationId' },
                { name: 'quickQueryIndicator' },
                { name: 'reportBuilderIndicator' },
                { name: 'customerCareRep' },
                { name: 'accountExecutive' },
                { name: 'isSalesforceCapable' },
                { name: 'email' },
                { name: 'associatedImageData' },
                { name: 'isExternalSystemLoginEnabled' },
                { name: 'themeId', type: 'int', useNull: true },
                { name: 'themeTypeId', type: 'int' }
            ],
            importHistory: [
                { name: 'id' },
                { name: 'startDate' },
                { name: 'description' },
                { name: 'endDate' },
                { name: 'status' },
                { name: 'statusCode' },
                { name: 'lastUpdated' },
                { name: 'errored' },
                { name: 'userIds' },
                { name: 'sessionUserIDs' },
                { name: 'sessionUserJson' }
            ],
            gtmOrganizer: [
                { name: 'id' },
                { name: 'organizerKey' },
                { name: 'email' },
                { name: 'password' }
            ],
            pendingRegistration: [
                { name: 'classId' },
                { name: 'registrationId' },
                { name: 'name' },
                { name: 'studentName' },
                { name: 'startDate' },
                { name: 'location' },
                { name: 'status' }
            ],
            onlineCourseRollup: [
                { name: 'id' },
                { name: 'userId' },
                { name: 'courseId' },
                { name: 'trainingProgramId' },
                { name: 'score' },
                { name: 'attemptCount' },
                { name: 'username' },
                { name: 'firstName' },
                { name: 'lastName' },
                { name: 'middleName' },
                { name: 'fullName' },
                { name: 'canMoveForwardIndicator' },
                { name: 'startTime' },
                { name: 'expiresAfterMinutes' },
                { name: 'remaining' },
                { name: 'classId' },
                { name: 'className' },
                { name: 'classStartTime' },
                { name: 'success' },
                { name: 'completion' },
                { name: 'proctorKey' }
            ],
            reportType: [
                { name: 'id' },
                { name: 'name' },
                { name: 'userNotes' },
                { name: 'isEnabled' },
                { name: 'codeName' }
            ],
            reportJob: [
                { name: 'id' },
                { name: 'name' },
                { name: 'isScheduled' },
                { name: 'userNotes' },
                { name: 'group' },
                { name: 'description' },
                { name: 'jobType' }
            ],
            reportAudience: [
                { name: 'audiencekey' },
                { name: 'name' },
                { name: 'customerkey' },
                { name: 'audienceLevelDetail' },
                { name: 'audienceId' },
                { name: 'parentId' },
                { name: 'customerId' }
            ],
            reportLocation: [
                { name: 'locationkey' },
                { name: 'Name' },
                { name: 'customerkey' },
                { name: 'locationLevelDetail' },
                { name: 'locationId' },
                { name: 'parentLocationId' },
                { name: 'customerId' }
            ],
            reportJobRole: [
                { name: 'jobRolekey' },
                { name: 'Name' },
                { name: 'customerkey' },
                { name: 'jobRoleLevelDetail' },
                { name: 'jobRoleId' },
                { name: 'parentJobRoleId' },
                { name: 'customerId' }
            ],
            reportTrainingProgram: [
                { name: 'trainingProgramkey' },
                { name: 'name' },
                { name: 'customerkey' },
                { name: 'trainingProgramId' },
                { name: 'isCurrentIndicator' },
                { name: 'customerId' }
            ],
            reportCourse: [
                { name: 'coursekey' },
                { name: 'coursekeyType' },
                { name: 'name' },
                { name: 'customerkey' },
                { name: 'thirdPartyIndicator' },
                { name: 'courseId' },
                { name: 'customerId' }
            ],
            reportUser: [
                { name: 'userkey' },
                { name: 'firstName' },
                { name: 'lastName' },
                { name: 'middleName' },
                { name: 'customerkey' },
                { name: 'userId' },
                { name: 'isImported' },
                { name: 'customerId' },
                { name: 'fullName' }
            ],
            report: [
                { name: 'id' },
                { name: 'ownerId' },
                { name: 'customerId' },
                { name: 'name' },
                { name: 'reportTypeId' },
                { name: 'notifyWhenReady' },
                { name: 'downloadCSV' },
                { name: 'downloadXLS' },
                { name: 'downloadPDF' },
                { name: 'parameters' },
                { name: 'scheduleType' },
                { name: 'scheduleHour' },
                { name: 'scheduleDaysOfWeek' },
                { name: 'scheduleDayOfMonth' },
                { name: 'scheduleMonth' },
                { name: 'isFavorite' },
                { name: 'ownerName' },
                { name: 'reportTypeCode' },
                { name: 'nextRunTime' },
                { name: 'reportTemplateId' },
                { name: 'xmlParameters' },
                { name: 'isSymphonyIdMode' }
            ],
            reportQueue: [
                { name: 'id' },
                { name: 'queueCommand' },
                { name: 'parameters' },
                { name: 'reportType' },
                { name: 'isEnabled' },
                { name: 'jobId' },
                { name: 'reportId' },
                { name: 'status' },
                { name: 'statusId' },
                { name: 'startedOn' },
                { name: 'finishedOn' },
                { name: 'reportName' },
                { name: 'downloadCSV' },
                { name: 'downloadXLS' },
                { name: 'downloadPDF' },
                { name: 'nextRunTime' },
                { name: 'errorMessage' },
                { name: 'statusDataLength' }
            ],
            reportQueueEntry: [
                { name: 'id' },
                { name: 'reportId' },
                { name: 'queueId' },
                { name: 'status' },
                { name: 'statusId' },
                { name: 'startedOn' },
                { name: 'finishedOn' },
                { name: 'reportName' },
                { name: 'downloadCSV' },
                { name: 'downloadXLS' },
                { name: 'downloadPDF' },
                { name: 'nextRunTime' }
            ],
            reportTemplate: [
                { name: 'id' },
                { name: 'name' },
                { name: 'codeName' },
                { name: 'description' },
                { name: 'isEnabled' },
                { name: 'reportSystem' },
                { name: 'reportSystemParameters' },
                { name: 'reportEntities' }
            ],
            reportEntity: [
                { name: 'id' },
                { name: 'name' },
                { name: 'xtype' },
                { name: 'config' },
                { name: 'order' },
                { name: 'description' },
                { name: 'isDeprecated' },
                { name: 'tagName' },
                { name: 'duplicated' }
            ],
            reportProcedure: [
                { name: 'fullName' },
                { name: 'codeName' }
            ],
            messageBoard: [
                { name: 'id' },
                { name: 'name' },
                { name: 'createdOn' },
                { name: 'modifiedOn' },
                { name: 'createdBy' },
                { name: 'modifiedBy' },
                { name: 'messageBoardType' },
                { name: 'trainingProgramId' },
                { name: 'classId' },
                { name: 'onlineCourseId' },
                { name: 'courseName' },
                { name: 'posts' },
                { name: 'lastUpdated' },
                { name: 'lastPostBy' },
                { name: 'isInstructor' }
            ],
            messageBoardPost: [
                { name: 'id' },
                { name: 'topicId' },
                { name: 'userId' },
                { name: 'postId' },
                { name: 'isPrivate' },
                { name: 'content' },
                { name: 'createdOn' },
                { name: 'modifiedOn' },
                { name: 'createdBy' },
                { name: 'modifiedBy' },
                { name: 'username' },
                { name: 'posts' },
                { name: 'jobRole' },
                { name: 'replies', type: 'object' },
                { name: 'isFirst' },
                { name: 'isDeleted' },
                { name: 'replyUsername' },
                { name: 'replyContent' }
            ],
            messageBoardTopic: [
                { name: 'id' },
                { name: 'messageBoardId' },
                { name: 'userId' },
                { name: 'title' },
                { name: 'isSticky' },
                { name: 'isLocked' },
                { name: 'createdOn' },
                { name: 'modifiedOn' },
                { name: 'createdBy' },
                { name: 'modifiedBy' },
                { name: 'posts' },
                { name: 'lastPost' },
                { name: 'lastPostBy' },
                { name: 'preview' },
                { name: 'isDeleted' }
            ],
            parameter: [
                { name: 'id' },
                { name: 'parameterSetId' },
                { name: 'name' },
                { name: 'code' },
                { name: 'description' },
                { name: 'defaultValue' },
                { name: 'parameterSetName' },
                { name: 'template' },
                { name: 'value' }
            ],
            parameterValue: [
                { name: 'id' },
                { name: 'parameterId' },
                { name: 'parameterSetOptionId' },
                { name: 'parameterSetName' },
                { name: 'parameterSetOptionValue' },
                { name: 'isDefault' },
                { name: 'value' }
            ],
            parameterSet: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' }
            ],
            parameterSetOption: [
                { name: 'id' },
                { name: 'parameterSetId' },
                { name: 'value' }
            ],
            keyValuePair: [
                { name: 'key' },
                { name: 'value' }
            ],
            jurisdiction: [
                { name: 'id' },
                { name: 'name' }
            ],
            profession: [
                { name: 'id' },
                { name: 'name' }
            ],
            accreditationBoardProfession: [
                { name: 'id' },
                { name: 'name' },
                { name: 'isAssigned', type: 'bool' },
                { name: 'professionId', type: 'int' },
                { name: 'accreditationBoardId', type: 'int' }
            ],
            licenseDataField: [
                { name: 'id' },
                { name: 'name' }
            ],
            licenseUserField: [
                { name: 'id' },
                { name: 'name' }
            ],
            serviceProvider: [
                { name: 'id' },
                { name: 'name' },
                { name: 'url' },
                { name: 'artifactUrl' },
                { name: 'logoutUrl' },
                { name: 'method' },
                { name: 'nameIdentifier' },
                { name: 'assertionUrl' },
                { name: 'isRedirectArtifact' },
                { name: 'isDisableTheme' }
            ],
            externalSystem: [
                { name: 'id' },
                { name: 'systemName' },
                { name: 'systemCodeName' },
                { name: 'loginUrl' },
                { name: 'isRequired' }
            ],
            network: [
				{ name: 'id' },
				{ name: 'name' },
				{ name: 'codeName' },
                { name: 'customerNetwork' },
                { name: 'customerNetworkDetails' }
            ],
            networkCustomer: [
                { name: 'id' },
                { name: 'name' }
            ],
            networkCustomerRelationship: [
                { name: 'sourceCustomerId' },
                { name: 'sourceCustomerName' },
                { name: 'destinationCustomerId' },
                { name: 'destinationCustomerName' },
                { name: 'allowTrainingProgramSharing' },
                { name: 'allowReporting' },
                { name: 'allowLibraries' },
                { name: 'detailId' },
                { name: 'networkId' },
                { name: 'networkSharedEntities' },
                { name: 'trainingProgramIds' }
            ],
            book: [
                { name: 'id' },
                { name: 'associatedImageData' },
                { name: 'name' },
                { name: 'author' },
                { name: 'publisher' },
                { name: 'isbn' },
                { name: 'typeFormat' }
            ],
            //Libraries
            library: [
                { name: 'id' },
                { name: 'name' },
                { name: 'description' },
                { name: 'cost' },
                { name: 'details' },
                { name: 'sku' },
                { name: 'customerId' },
                { name: 'itemCount' },
                { name: 'registrationCount' },
                { name: 'createdOn' },
                { name: 'modifiedOn' },
                { name: 'displayCreatedOn' },
                { name: 'associatedImageData' },
                { name: 'libraryItemTypeId' },
                { name: 'isShared' },
                //Display
                { name: 'displayLibraryItemType' }
            ],
            libraryItem: [
                { name: 'id' },
                { name: 'libraryItemId' },
                { name: 'libraryId' },
                { name: 'name' },
                { name: 'description' },
                { name: 'categoryId' },
                { name: 'duration' },
                { name: 'libraryItemTypeId' },
                { name: 'createdOn' },
                { name: 'courseTypeId' },
                { name: 'categoryName' },
                { name: 'levelIndentText' },
                { name: 'isInLibrary' },
                { name: 'isFavorite' },
                { name: 'item' },
                { name: 'itemCreatedOn' },
                { name: 'itemModifiedOn' },
                { name: 'courseVersion' },
                // Display
                { name: 'displayInLibrary' },
                { name: 'displayIsFavorite' },
                { name: 'displayLibraryItemType' },
                { name: 'displayCreatedOn' },
                { name: 'displayItemCreatedOn' },
                { name: 'displayItemModifiedOn' },
                { name: 'isLoading' }
            ],
            libraryFilterOption: [
                { name: 'name' },
                { name: 'value' },
                { name: 'displayName' }
            ],
            libraryGrant: [
                { name: 'gridId' },
                { name: 'startDate' },
                { name: 'endDate' }
            ],
            libraryGrantHierarchy: [
                { name: 'id' },
                { name: 'hierarchyNodeId' },
                { name: 'hierarchyTypeId' },
                { name: 'locationId' },
                { name: 'jobRoleId' },
                { name: 'audienceId' },
                { name: 'userId' },
                { name: 'name' },
                { name: 'parentId' },
                { name: 'startDate' },
                { name: 'endDate' },
                { name: 'levelIndentText' }
            ],
            userDataField: [
                { name: 'userDataFieldId' },
                { name: 'customerId' },
                { name: 'xtype' },
                { name: 'config' },
                { name: 'codeName' },
                { name: 'displayName' },
                { name: 'categoryCodeName' },
                { name: 'categoryDisplayName' },
                { name: 'validator' },
                { name: 'isGlobal' }
            ],
            userDataCategory: [
                { name: 'categoryCodeName' },
                { name: 'categoryDisplayName' }
            ],
            classroomRegistration: [
                { name: 'qtip' },
                { name: 'registrationStatusTypeId' },
                { name: 'text' },
                { name: 'value' },
                { name: 'item' }
            ]
        }
    });
    // Definition extensions
    Symphony.Definitions.sessionAssignedUser = [
        { name: 'RegistrationStatusId' },
        { name: 'RegistrationStatusName' },
        { name: 'ClassID' },
        { name: 'ClassName' },
        { name: 'MinStartDateTime' },
        { name: 'MaxStartDateTime' },
        { name: 'SessionStatus' },
        { name: 'TrainingProgramID' },
        { name: 'TrainingProgramName' },
        { name: 'IsInDateRange' },
        { name: 'Sort' },
        { name: 'SessionClasses' },
        { name: 'CreatedOn' },
        { name: 'ModifiedOn' }
    ].concat(Symphony.Definitions.user);

    Symphony.Definitions.sessionClass = [
        { name: 'trainingProgramId' }
    ].concat(Symphony.Definitions['classroomClass']);

    Symphony.Definitions.libraryGrantLibrary = Symphony.Definitions.libraryGrant.concat(Symphony.Definitions.library);
    Symphony.Definitions.libraryCategory = Symphony.Definitions.category.concat([
        { name: 'items' },
        { name: 'totalItems' },
        { name: 'totalRows' }
    ]);

    Symphony.Definitions.transcriptEntryCourse = Symphony.Definitions.course.concat([
        { name: 'courseId' },
        { name: 'courseName' },
        { name: 'trainingProgramId' },
        { name: 'trainingProgramName' },
        { name: 'isHistorical' },
        { name: 'isTrainingProgramRollupComplete' },
        { name: 'attemptTime' },
        { name: 'attemptDate' },
        { name: 'displayTrainingProgramCertificate' },
        { name: 'isSessionCourse' }
    ]);

    /// Temporary
    /// Rebuild all of the existing definitions as ExtJS4 definitions
    var x = [];
    for (var name in Symphony.Definitions) {
        if (Symphony.Definitions.hasOwnProperty(name) && typeof (Symphony.Definitions[name]) == 'object') {
            Ext.define(name, {
                extend: 'Ext.data.Model',
                fields: Symphony.Definitions[name],
                idProperty: name == "reportEntity" || name == "userAssignment" ? 'none' : 'id'
            });
        }
    }

    // old code, should be moved up into the model definitions
    Symphony.Definitions.lockedOnlineCourseRollup = Symphony.clone(Symphony.Definitions.onlineCourseRollup);
    Symphony.Definitions.lockedOnlineCourseRollup.push({ name: 'username' });
    Symphony.Definitions.lockedOnlineCourseRollup.push({ name: 'courseName' });
    Symphony.Definitions.lockedOnlineCourseRollup.push({ name: 'trainingProgramName' });
    Symphony.Definitions.lockedOnlineCourseRollup.push({ name: 'testAttemptCount' });

    Ext.define('lockedOnlineCourseRollup', {
        extend: 'Ext.data.Model',
        fields: Symphony.Definitions.lockedOnlineCourseRollup,
        idProperty: 'id'
    });
    
    Ext.define('test', {
        extend: 'Ext.data.Model',
        fields: [{ name: 'test' }]
    });

    Ext.define('simple', {
        extend: 'Ext.data.Model',
        fields: [{ name: 'id' }, { name: 'text' }]
    });

    Ext.define('scheduledClassFilter', {
        extend: 'Ext.data.Model',
        fields: [{ name: 'code' }, { name: 'text' }]
    });

    Ext.define('DaysOfTheWeekModel', {
        extend: 'Ext.data.Model',
        fields: [
            'sunday',
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday'
        ]
    });

    Ext.define('ApplicationModel', {
        extend: 'Ext.data.Model',
        fields: [
            'id',
            'name',
            'authenticationUri',
            'passUserNamePass'
        ]
    });

    Ext.define('TileModel', {
        extend: 'Ext.data.Model',
        fields: [
            'id',
            'name',
            'tileUri',
            'applicationId',
            'applicationName'
        ]
    });

    Ext.define('UserApplication', {
        extend: 'Ext.data.Model',
        fields: [
            'id',
            'name',
            'tileCount',
            'credentialId',
            'credentialState',
            'credentialUserName',
            'credentialPassword'
       ]
    });

    Ext.define('UserApplicationTile', {
        extend: 'Ext.data.Model',
        fields: [
            "id",
            "name",
            "tileUri",
            "applicationId",
            "applicationName",
            "credentialID",
            "credentialState",
            "credentialPassword",
            "credentialUserName",
            "isAssignedByAudience",
            "isAssignedByJobRole",
            "isAssignedByLocation",
            "isAssignedByUser",
            "userID"
        ]
    });

    Ext.define('SimpleArtisanCourseElement', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'id',
            type: 'int'
        }, {
            name: 'text',
            type: 'string'
        }, {
            name: 'isVisited',
            type: 'bool'
        }, {
            name: 'isBookmarked',
            type: 'bool'
        }, {
            name: 'type',
            type: 'string'
        }]
    });

    Ext.define('MediaPlayer', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'name',
            type: 'string'
        }]
    });

    Ext.define('customer', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'rest',
            url: '/services/customer.svc/customers/',
            reader: {
                type: 'json',
                root: 'data',
                totalProperty: 'totalSize'
            }
        },

        fields: [
            { name: 'id' },
            { name: 'salesChannelId' },
            { name: 'salesChannelName' },
            { name: 'parentId' },
            { name: 'name' },
            { name: 'evaluationIndicator' },
            { name: 'evaluationEndDate' },
            { name: 'suspendedIndicator' },
            { name: 'userLimit' },
            { name: 'selfRegistrationIndicator' },
            { name: 'selfRegistrationPassword' },
            { name: 'lockoutCount' },
            { name: 'subdomain' },
            { name: 'locationId' },
            { name: 'quickQueryIndicator' },
            { name: 'reportBuilderIndicator' },
            { name: 'customerCareRep' },
            { name: 'accountExecutive' },
            { name: 'isSalesforceCapable' },
            { name: 'email' },
            { name: 'associatedImageData' },
            { name: 'isExternalSystemLoginEnabled' },
            { name: 'themeId', type: 'int', useNull: true },
            { name: 'themeTypeId', type: 'int' }
        ]
    }, function(clz) {
        window.customer = clz;
    });

    Ext.define('Theme', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'rest',
            url: '/services/Customer.svc/themes/',
            reader: {
                type: 'json',
                root: 'data',
                totalProperty: 'totalSize'
            }
        },
        fields: [{
            name: 'id',
            type: 'int',
            useNull: true
        }, {
            name: 'name',
            type: 'string'
        }, {
            name: 'codeName',
            type: 'string'
        }, {
            name: 'description',
            type: 'string'
        }, {
            name: 'primaryColor',
            type: 'string'
        }, {
            name: 'secondaryColor',
            type: 'string'
        }, {
            name: 'tertiaryColor',
            type: 'string'
        }, {
            name: 'quaternary',
            type: 'string'
        }, {
            name: 'primaryImage',
            type: 'string'
        }, {
            name: 'secondaryImage',
            type: 'string'
        }, {
            name: 'isDeleted',
            type: 'bool'
        }]
    }, function(clazz) {
        window.theme = Theme;
    });

    Ext.define('CertificateTemplate', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'rest',
            url: '/services/certificate.svc/certificateTemplates/',
            reader: {
                type: 'json',
                root: 'data',
                totalProperty: 'totalSize'
            }
        },

        fields: [{
            name: 'id',
            type: 'int',
            useNull: true
        }, {
            name: 'customerId',
            type: 'int',
            useNull: true
        }, {
            name: 'certificateType',
            type: 'int'
        }, {
            name: 'name',
            type: 'string'
        }, {
            name: 'description',
            type: 'string'
        }, {
            name: 'width',
            type: 'int'
        }, {
            name: 'height',
            type: 'int'
        }, {
            name: 'path',
            type: 'string'
        }, {
            name: 'parentId',
            type: 'int',
            useNull: true
        }, {
            name: 'indentLevelText',
            type: 'string'
        }, {
            name: 'content',
            type: 'string'
        }, {
            name: 'isDeleted',
            type: 'bool'
        }, {
            name: 'bgImageId',
            type: 'int'
        }, {
            name: 'bgImage',
            type: 'string'
        }, {
            name: 'bgImagePath',
            type: 'string'
        }]
    });

    Ext.define('AccreditationBoard', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'rest',
            url: '/services/Accreditation.svc/accreditationBoards/',
            reader: {
                type: 'json',
                root: 'data',
                totalProperty: 'totalSize'
            }
        },
        fields: [{
            name: 'id',
            type: 'int'
        }, {
            name: 'name',
            type: 'string'
        }, {
            name: 'description',
            type: 'string'
        }, {
            name: 'primaryContact',
            type: 'string'
        }, {
            name: 'phone',
            type: 'string'
        }, {
            name: 'disclaimer',
            type: 'string',
            useNull: true
        }, {
            name: 'logo',
            type: 'string'
        }, {
            name: 'professions'
        }, {
            name: 'surveys'
        }]
    }, function(clazz) {
        Log.debug('Class created: ' + clazz.$className);
    });

    Ext.define('AccreditationBoardSurvey', {
        extend: 'Ext.data.Model',

        fields: [{
            name: 'id',
            type: 'int'
        }, {
            name: 'rank',
            type: 'int'
        }, {
            name: 'name',
            type: 'string'
        }, {
            name: 'category',
            type: 'string'
        }, {
            name: 'isChecked',
            type: 'bool'
        }]
    });

    Ext.define('CustomersStore', {
        extend: 'Ext.data.Store',
        alias: 'store.customers',
        model: 'customer'
    });

    Ext.define('ThemesStore', {
        extend: 'Ext.data.Store',
        alias: 'store.themes',
        model: 'Theme'
    });

    Ext.define('AccreditationBoardSurveysStore', {
        extend: 'Ext.data.Store',
        alias: 'store.accreditationBoardSurveys',
        model: 'AccreditationBoardSurvey'
    });

    Ext.define('AccreditationBoardProfessionsStore', {
        extend: 'Ext.data.Store', 
        alias: 'store.accreditationBoardProfessions',
        model: 'accreditationBoardProfession' 
    });

    Ext.define('CertificateTypesStore', {
        extend: 'Ext.data.Store',
        alias: 'store.certificateTypes',
        model: 'simple',
        data: [
            { id: 1, text: 'Training Program' },
            { id: 2, text: 'Classroom Course' },
            { id: 3, text: 'Online Course' }
        ]
    });

    Ext.define('ProfessionsStore', {
        extend: 'Ext.data.Store', 
        alias: 'store.professions',
        model: 'profession',

        proxy: {
            type: 'rest',
            url: '/services/license.svc/profession',
            reader: {
                type: 'json',
                root: 'data'
            }
        }
    });

    // Add new methods to the date object for parsing dates formatted in\
    // weird MS-style. 
    Ext.override(Ext.Date, {
        convertDate: function(value, record) {
            if (!value) {
                return null;
            }

            // Try to parse like it's a .NET date. The framework doesn't provide
            // a separate method that only works with a reader, so we have to
            // wrap the functionality here.
            //
            // If it's a .NET date return the original value. Don't return a 
            // Date object here, let ExtJS handle it because it performs some
            // additional adjustments related to timezones.
            var parts = /\/Date\((.*)([+-])(\d{2})(\d{2})\)\//.exec(value);
            if (!parts || parts.length == 0) {
                return value;
            }

            var date = new Date(Ext.Number.from(parts[1])),
                modifyFn;

            // This is the hours/minutes of timezone offset. We don't need to
            // do anything with this currently as it will display in the users
            // timezone anyways. Tricky part is saving when we need to make
            // sure it's in UTC.
            parts[3] = Ext.Number.from(parts[3]);
            parts[4] = Ext.Number.from(parts[4]);

            return date;
        },
        
        // Note that if you want to use this, you have to make sure that the
        // model's writer performs the serialization, or this won't get called.
        // ie: this isn't invoked if you call getData().
        //
        // You can do it manually like:
        //     var writer = model/store.getProxy().getWriter();
        //     var data = writer.getRecordData(writer);
        serializeDate: function(value, record) {
            var offsetTime;

            if (Ext.isDate(value)) {
                offsetTime = value.getTime() - value.getTimezoneOffset() * 60 * 1000;
                return '\/Date(' + offsetTime.toString() + ')\/';
            } else {
                return null;
            }
        }
    });

    Ext.define('ArtisanCourseAccreditation', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'rest',
            url: '/services/Accreditation.svc/accreditations/artisan',
            reader: {
                type: 'json',
                root: 'data'
            }
        },
        fields: [{
            name: 'accreditationBoardId',
            type: 'int'
        }, {
            name: 'artisanCourseId',
            type: 'int'
        }, {
            name: 'professionId',
            type: 'int',
            useNull: true
        }, {
            name: 'courseName',
            type: 'string',
            persist: false
        }, {
            name: 'status',
            type: 'int'
        }, {
            name: 'startDate',
            type: 'date',
            convert: Ext.Date.convertDate,
            serialize: Ext.Date.serializeDate
        }, {
            name: 'startDate_date',
            type: 'date',
            persist: false,
            convert: function(value, model) {
                return model.get('startDate');
            }
        }, {
            name: 'startDate_time',
            type: 'date',
            persist: false,
            convert: function(value, model) {
                return model.get('startDate');
            }
        }, {
            name: 'expiryDate',
            type: 'date',
            convert: Ext.Date.convertDate,
            serialize: Ext.Date.serializeDate
        }, {
            name: 'expiryDate_date',
            type: 'date',
            persist: false,
            convert: function(value, model) {
                return model.get('expiryDate');
            }
        }, {
            name: 'expiryDate_time',
            type: 'date',
            persist: false,
            convert: function(value, model) {
                return model.get('expiryDate');
            }
        }, {
            name: 'creditHours',
            type: 'float',
            useNull: true
        }, {
            name: 'creditHoursLabel',
            type: 'string'
        }, {
            name: 'disclaimer',
            type: 'string',
            useNull: true
        }, {
            name: 'accreditationCode',
            type: 'string'
        }, {
            name: 'courseNumber',
            type: 'string',
            useNull: true
        }, {
            name: 'isDeleted',
            type: 'boolean',
            defaultValue: false
        }]
    }, function(clazz) {
        Log.debug('Class created: ' + clazz.$className);
    });

    Ext.define('AffidavitFinalExam', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'rest',
            url: '/services/affidavit.svc/affidavits/',
            reader: {
                type: 'json',
                root: 'data'
            }
        },
        fields: [{
            name: 'id',
            type: 'int'
        }, {
            name: 'name',
            type: 'string'
        }, {
            name: 'description',
            type: 'string',
            useNull: true
        }, {
            name: 'affidavitJSON',
            type: 'string'
        }, {
            name: 'isDeleted',
            type: 'boolean',
            defaultValue: false
        }]
    }, function(clazz) {
        Log.debug('Class created: ' + clazz.$className);
    });

    window.affidavitFinalExam = AffidavitFinalExam;

    Ext.define('ProctorForm', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'rest',
            url: '/services/proctor.svc/proctorForms/',
            reader: {
                type: 'json',
                root: 'data'
            }
        },
        fields: [{
            name: 'id',
            type: 'int'
        }, {
            name: 'name',
            type: 'string'
        }, {
            name: 'description',
            type: 'string',
            useNull: true
        }, {
            name: 'proctorJSON',
            type: 'string'
        }, {
            name: 'isDeleted',
            type: 'boolean',
            defaultValue: false
        }]
    }, function(clazz) {
        Log.debug('Class created: ' + clazz.$className);
    });

    Ext.define('OnlineCourseAccreditation', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'rest',
            url: '/services/Accreditation.svc/accreditations/courseassignment',
            reader: {
                type: 'json',
                root: 'data'
            }
        },
        fields: [{
            name: 'accreditationBoardId',
            type: 'int'
        }, {
            name: 'onlineCourseId',
            type: 'int'
        }, {
            name: 'artisanAccreditationId',
            type: 'int'
        }, {
            name: 'professionId',
            type: 'int',
            useNull: true
        }, {
            name: 'accreditationBoardName',
            type: 'string'
        }, {
            name: 'professionName',
            type: 'string'
        }, {
            name: 'startDate',
            type: 'date',
            convert: Ext.Date.convertDate,
            serialize: Ext.Date.serializeDate
        }, {
            name: 'expiryDate',
            type: 'date',
            convert: Ext.Date.convertDate,
            serialize: Ext.Date.serializeDate
        }, {
            name: 'creditHours',
            type: 'float',
            useNull: true
        }, {
            name: 'creditHoursLabel',
            type: 'string'
        }, {
            name: 'disclaimer',
            type: 'string'
        }, {
            name: 'accreditationCode',
            type: 'string'
        }, {
            name: 'courseNumber',
            type: 'string',
            useNull: true
        }, {
            name: 'isDeleted',
            type: 'boolean',
            defaultValue: false
        }]
    }, function(clazz) {
        Log.debug('Class created: ' + clazz.$className);
    });

    Ext.define('AccreditationStatus', {
        statics: {
            Pending: 1,
            Accredited: 2,
            Expired: 3,

            getDisplayValue: function(value) {
                try {
                    if (Ext.isString(value)) {
                        value = Ext.Number.from(value);
                    }

                    switch (value) {
                        case AccreditationStatus.Pending: return 'Pending';
                        case AccreditationStatus.Accredited: return 'Accredited';
                        case AccreditationStatus.Expired: return 'Expired';
                        default: return 'Unknown (' + value + ')';
                    }
                } catch(e) {
                    Log.error(e);
                    return 'Error';
                }
            }
        }
    }, function(clazz) {
        Log.debug('Class created: ' + clazz.$className);
    });

    Ext.define('TrainingProgramAccreditation', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'rest',
            url: '/services/Accreditation.svc/accreditations/trainingProgram',
            reader: {
                type: 'json',
                root: 'data'
            }
        },
        fields: [{
            name: 'accreditationBoardId',
            type: 'int'
        }, {
            name: 'trainingProgramId',
            type: 'int'
        }, {
            name: 'trainingProgramName',
            type: 'string',
            persist: false
        }, {
            name: 'status',
            type: 'int'
        }, {
            name: 'startDate',
            type: 'date',
            convert: Ext.Date.convertDate,
            serialize: Ext.Date.serializeDate
        }, {
            name: 'startDate_date',
            type: 'date',
            persist: false,
            convert: function(value, model) {
                return model.get('startDate');
            }
        }, {
            name: 'startDate_time',
            type: 'date',
            persist: false,
            convert: function(value, model) {
                return model.get('startDate');
            }
        }, {
            name: 'expiryDate',
            type: 'date',
            convert: Ext.Date.convertDate,
            serialize: Ext.Date.serializeDate
        }, {
            name: 'expiryDate_date',
            type: 'date',
            persist: false,
            convert: function(value, model) {
                return model.get('expiryDate');
            }
        }, {
            name: 'expiryDate_time',
            type: 'date',
            persist: false,
            convert: function(value, model) {
                return model.get('expiryDate');
            }
        }, {
            name: 'creditHours',
            type: 'float',
            useNull: true
        }, {
            name: 'creditHoursLabel',
            type: 'string'
        }, {
            name: 'disclaimer',
            type: 'string',
            useNull: true
        }, {
            name: 'accreditationCode',
            type: 'string'
        }, {
            name: 'courseNumber',
            type: 'string',
            useNull: true
        }, {
            name: 'isDeleted',
            type: 'boolean',
            defaultValue: false
        }]
    }, function(clazz) {
        Log.debug('Class created: ' + clazz.$className);
    });

    Symphony.newId = function () {
        // create a unique identifier
        return "symphony_" + _id++;
    };


    //TODO: load this from the database so we use the appropriate customer-specific values
    Symphony.HierarchyTypeStoreData = [
        [Symphony.HierarchyType.location, Symphony.Aliases.location],
        [Symphony.HierarchyType.jobRole, Symphony.Aliases.jobRole],
        [Symphony.HierarchyType.audience, Symphony.Aliases.audience],
        [Symphony.HierarchyType.user, 'User']
    ];
    Symphony.ImportTypeStore = new Ext.data.ArrayStore({
        model: 'simple',
        data: [
            [Symphony.ImportType.user, 'Users'],
            [Symphony.ImportType.jobRole, Symphony.Aliases.jobRoles],
            [Symphony.ImportType.location, Symphony.Aliases.locations],
            [Symphony.ImportType.audience, Symphony.Aliases.audiences],
            [Symphony.ImportType.order, 'Orders']
        ]
    });
    Symphony.RegistrationStatusTypeStore = new Ext.data.ArrayStore({
        model: 'simple',
        data: [
            [Symphony.RegistrationStatusType.awaitingApproval, 'Awaiting Approval'],
            [Symphony.RegistrationStatusType.waitList, 'Wait List'],
            [Symphony.RegistrationStatusType.denied, 'Denied'],
            [Symphony.RegistrationStatusType.registered, 'Registered']
        ]
    });
    Symphony.LimitedCourseCompletionTypeStore = new Ext.data.ArrayStore({
        model: 'simple',
        data: [
            [Symphony.CourseCompletionType.attendance, 'Attendance'],
            [Symphony.CourseCompletionType.numeric, 'Numeric'],
            [Symphony.CourseCompletionType.letter, 'Letter']
        ]
    });
    Symphony.SsoTypeStore = new Ext.data.ArrayStore({
        model: 'simple',
        data: [
            [Symphony.SsoType.saml, 'SAML'],
            [Symphony.SsoType.janrain, 'Janrain']
        ]
    });
    Symphony.SsoLoginUiTypeStore = new Ext.data.ArrayStore({
        model: 'simple',
        data: [
            [Symphony.SsoLoginUiType.standard, 'Standard'],
            [Symphony.SsoLoginUiType.mixed, 'Mixed'],
            [Symphony.SsoLoginUiType.forced, 'Forced']
        ]
    });
    Symphony.ActiveAfterModesStore = new Ext.data.ArrayStore({
        id: 0,
        model: 'simple',
        data: [
            [Symphony.RelativeTimeMode.trainingProgram, 'after first course launch.'],
            [Symphony.RelativeTimeMode.session, 'after session start.'],
            [Symphony.RelativeTimeMode.previousCourseEnd, 'after previous course end.']
        ]
    }),
    Symphony.DueModesStore = new Ext.data.ArrayStore({
        id: 0,
        model: 'simple',
        data: [
            [Symphony.RelativeTimeMode.course, 'after course start.'],
            [Symphony.RelativeTimeMode.trainingProgram, 'after first course launch.'],
            [Symphony.RelativeTimeMode.session, 'after session start.'],
            [Symphony.RelativeTimeMode.finalUnlocked, 'after final unlock.']
        ]
    }),
    Symphony.PresentationType = {
        'Class': true,
        'Lecture': true,
        'Seminar': true,
        'Vendor Training': true,
        'Third Party Training': true,
        'Conference': true,
        'Forum': true,
        'Meeting': true,
        'Webinar': true
    };

    /* a couple generic renderers */
    Symphony.parseDate = function (value, addLocalOffset) {
        if (!value) { return value; }
        var ticksAndZone = value.match(dateRegex)[1];
        var delim = ticksAndZone.indexOf('-') > -1 ? '-' : '+';
        var parts = ticksAndZone.split(delim);
        var ticks = parseInt(parts[0], 10);
        var dt = new Date(ticks);
        if (addLocalOffset) {
            var offset = dt.getTimezoneOffset();
            dt = Ext.Date.add(dt, Date.MINUTE, offset);
        }
        return dt;
    };
	Symphony.parseDateLong = function (value, addLocalOffset) {
        if (!value) { return value; }
        var ticksAndZone = value.match(dateRegexAllowNegative)[1];
        var delim = ticksAndZone.lastIndexOf('-') > 0 ? '-' : '+';
        var parts = ticksAndZone.split(delim);
        var tickString = parts.length == 3 && delim == '-' ?  '-'+parts[1] : parts[0];
        var ticks = parseInt(tickString, 10);
        var dt = new Date(ticks);
        if (addLocalOffset) {
            var offset = dt.getTimezoneOffset();
            dt = dt.add(Date.MINUTE, offset);
        }
        return dt;
    };
    Symphony.dateRenderer = function (value) {
        if (!value) {
            return '';
        }
        var dt = null;
        if (Ext.isString(value)) {
            dt = Symphony.parseDate(value);
        } else {
            dt = value;
        }
        var dateString = dt.formatSymphony('m/d/Y');
        return Symphony.qtipRenderer(dateString);
    };
    Symphony.dateTimeRenderer = function (value) {
        if (!value) {
            return '';
        }
        var dateString = Symphony.parseDate(value).formatSymphony('m/d/Y g:i a T');
        return Symphony.qtipRenderer(dateString);
    };
    Symphony.naturalTimeRendererTZ = function (value) {
        if (!value) {
            return '';
        }
        var dateString = Symphony.parseDate(value).formatSymphony('g:i a T');
        return Symphony.qtipRenderer(dateString);
    };
    Symphony.naturalDateRenderer = function (value) {
        if (!value) {
            return '';
        }
        var dateString = Symphony.parseDate(value).formatSymphony('M j, Y');
        return Symphony.qtipRenderer(dateString);
    };
    Symphony.pastDefaultDateRenderer = function (value) {
        if (value && Symphony.parseDate(value).getFullYear() != Symphony.defaultYear) {
            var dateString = Symphony.parseDate(value, true).formatSymphony('m/d/y');
            return Symphony.qtipRenderer(dateString);
        } else {
            return '';
        }
    };
    Symphony.longDateRenderer = function (value) {
        if (!value) {
            return '';
        }
        var dateString = Symphony.parseDate(value).formatSymphony('D, F j, Y g:i a');
        return Symphony.qtipRenderer(dateString);
    };

    Symphony.shortDateRenderer = function (value) {
        if (!value) {
            return '';
        }
        var dateString = Symphony.parseDate(value).formatSymphony('m/d/y');
        return Symphony.qtipRenderer(dateString);
    };
    Symphony.naturalTimeRenderer = function (value, hideTip) {
        if (!value) {
            return '';
        }
        var timeString = Symphony.parseDate(value).formatSymphony('g:i a');
        if (hideTip) { return timeString; }
        return Symphony.qtipRenderer(timeString);
    };
    Symphony.naturalTimeRendererTZ = function (value, hideTip) {
        if (!value) {
            return '';
        }
        var timeString = Symphony.parseDate(value).formatSymphony('g:i a T');
        if (hideTip) { return timeString; }
        return Symphony.qtipRenderer(timeString);
    };
    Symphony.shortTimeRenderer = function (value, hideTip) {
        if (!value) {
            return '';
        }
        var timeString = Symphony.parseDate(value).formatSymphony('g:ia');
        if (hideTip) { return timeString; }
        return Symphony.qtipRenderer(timeString);
    };
    Symphony.timeRenderer = function (value, hideTip) {
        if (!value) {
            return '';
        }
        var timeString = Symphony.parseDate(value).formatSymphony('g:i:s A T');
        if (hideTip) { return timeString; }
        return Symphony.qtipRenderer(timeString);
    };
    Symphony.checkRenderer = function (value, qtip) {
        if (!value) {
            return '<img src="/images/bullet_black.png" ' + Symphony.generateQtipAttr(qtip) + '/>';
        }
        return '<img src="/images/tick.png" ' + Symphony.generateQtipAttr(qtip) + '/>';
    };
    Symphony.passFailRenderer = function (value, qtip) {
        if (!value) {
            return '<img src="/images/cross.png" ' + Symphony.generateQtipAttr(qtip) + '/>';
        }
        return '<img src="/images/tick.png" ' + Symphony.generateQtipAttr(qtip) + '/>';
    };
    Symphony.starRenderer = function (value) {
        if (!value) {
            return '<img src="/images/bullet_white.png" />';
        }
        return '<img src="/images/star.png" />';
    };
    Symphony.newRenderer = function (value, qtip) {
        if (value) {
            return '<img src="/images/new.png" ' + Symphony.generateQtipAttr(qtip) + '/>';
        }
    };
    Symphony.undoRenderer = function (qtip) {
        return '<img src="/images/arrow_undo.png" ' + Symphony.generateQtipAttr(qtip) + '/>';
    };
    Symphony.errorRenderer = function (qtip) {
        return '<img src="/images/error.png" ' + Symphony.generateQtipAttr(qtip) + '/>';
    };
    Symphony.globalRenderer = function (value, qtip) {
        if (value) {
            return '<img src="/images/world.png" ' + Symphony.generateQtipAttr(qtip) + '/>';
        }
        return '<img src="/images/bullet_white.png" />';
    };
    Symphony.hierarchyTypeRenderer = function (hierarchyTypeId) {
        var template = '<img src="{0}" {1}/>';
        switch (hierarchyTypeId) {
            case Symphony.HierarchyType.audience:
                return template.format('/images/group.png', Symphony.generateQtipAttr(Symphony.Aliases.audience));
                break;
            case Symphony.HierarchyType.user:
                return template.format('/images/user.png', Symphony.generateQtipAttr('User'));
                break;
            case Symphony.HierarchyType.location:
                return template.format('/images/map.png', Symphony.generateQtipAttr(Symphony.Aliases.location));
                break;
            case Symphony.HierarchyType.jobRole:
                return template.format('/images/building.png', Symphony.generateQtipAttr(Symphony.Aliases.jobRole));
                break;
        }
    };
    Symphony.qtipRenderer = function (title, qtip, cls) {
        if (arguments.length == 1) {
            qtip = title;
        }
        qtip = Symphony.cleanQtip(qtip);
        return '<span ' + (cls ? 'class="' + cls + '" ' : '') + Symphony.generateQtipAttr(qtip) + '>' + title + '</span>';
    };
    Symphony.cleanQtip = function (qtip) {
        var value = qtip;
        if (qtip && qtip.value) {
            value = qtip.value;
        }
        if (value && value.replace) {
            // escape quotes
            value = value.replace(Symphony.quoteRegex, '&quot;');
        }
        return value;
    }
    Symphony.generateQtipAttr = function (qtip) {
        if (qtip) {
            return 'data-qtip="' + Symphony.cleanQtip(qtip) + '"';
        }
        return '';
    }

    Symphony.quickStore = function (items) {
        var result = [];
        for (var i = 0; i < items.length; i++) {
            var inner = [];
            if (!Ext.isObject(items[i])) {
                inner.push(items[i]);
                inner.push(items[i]);
            } else {
                for (var prop in items[i]) {
                    if (items[i].hasOwnProperty(prop)) {
                        inner.push(prop);
                        inner.push(items[i][prop]);
                    }
                }
            }
            result.push(inner);
        }
        return new Ext.data.SimpleStore({ fields: ['id', 'name'], data: result });
    };

    Symphony.PagedComboBox = Ext.define('symphony.pagedcombobox', {
        alias: 'widget.symphony.pagedcombobox',
        extend: 'Ext.form.ComboBox',
        
        initComponent: function () {
            var me = this;

            var proxy = this.networkProxy || new Ext.data.HttpProxy({
                method: 'GET',
                url: this.url,
                reader: new Ext.data.JsonReader({
                    idProperty: this.idProperty || null,
                    totalProperty: 'totalSize',
                    root: 'data'
                })
            });
            this.pageSize = this.pageSize || Symphony.Settings.defaultComboPageSize;

            if (!this.model) {

                console.warn("NO MODEL SUPPLIED FOR COMBO! THIS MUST BE RESOLVED");
                this.model = 'test';
            }

            var store = this.store || new Ext.data.Store({
                simpleSort: true,
                proxy: proxy,
                pageSize: this.pageSize,
                //baseParams: { limit: this.pageSize },
                model: this.model,
                remoteFilter: this.remoteFilter === true ? true : false
            });

            store.on('beforeload', function (store, operation) {
                // Stores for comboboxes appear to include a default filter
                // that just has the display name, but no value to filter by.
                // There is a parameter on the filter "isDiabled=true"
                // yet it still appears in query string "filter=[{property: 'displayPropertyName'}]"
                // We have a filter parameter on some requests that map to one of our
                // filter models. When this parameter gets decoded, it created a default
                // empty filter, which if applied to the request serverside,
                // it will filter out the resources we want. 
                if (!me.allowDisabledFilters && operation.filters && operation.filters.length) {

                    var enabledFilters = [];

                    for (var i = 0; i < operation.filters.length; i++) {
                        if (!operation.filters[i].disabled) {
                            enabledFilters.push(operation.filters[i])
                        }
                    }
                   
                    operation.filters = enabledFilters;
                }
            });

            if (this.blankValueTitle) {
                console.warn("FIX THIS FOR ADDING A BLANK VALUE!!");
                /*store.on('load', function () {
                    var obj = {};
                    obj[me.displayField] = me.blankValueTitle;
                    obj[me.valueField] = 0;
                    //store.insert(0, new me.recordDef(obj));
                }, { single: true });*/
            }

            if (!this.hasOwnProperty("forceSelection")) {
                this.forceSelection = true;
            }

            var listConfig = this.listConfig ? this.listConfig : {
                minWidth: 250
            }

            Ext.apply(this, {
                queryParam: 'searchText',
                triggerAction: 'all',
                loadMask: true,
                minChars: 0,
                queryMode: 'remote',
                displayField: this.displayField || 'name',
                valueField: this.valueField || 'id',
                store: store,
                trigger2Cls: this.clearable ? 'x-form-clear-trigger' : '',
                listConfig: listConfig
            });
            this.callParent(arguments);

            var me = this;
            if (me.clearable) {
                me.on('specialkey', this.onSpecialKeyDown, me);
                me.on('select', function (me, rec) {
                    //me.onShowClearTrigger(true);
                }, me);
                //me.on('afterrender', function () { me.onShowClearTrigger(false); }, me);
            }
        },
        setUrl: function (url) {
            this.url = url;
            if (this.store) {
                this.store.proxy.api.read = url;
                this.store.reload();
            }
        },

        /**
        * @private onSpecialKeyDown
        * eventhandler for special keys
        */
        onSpecialKeyDown: function (obj, e, opt) {
            if (e.getKey() == e.ESC) {
                this.clear();
            }
        },

        /**
        * @override onTrigger2Click
        * eventhandler
        */
        onTrigger2Click: function (args) {
            this.clear();
        },

        /**
        * @private clear
        * clears the current search
        */
        clear: function () {
            var me = this;
            me.fireEvent('beforeclear', me);
            me.valueNotFoundText = null;
            me.clearValue();
            //me.onShowClearTrigger(false);
            me.fireEvent('clear', me);
        }
    });


    Symphony.TimeZoneCombo = Ext.define('symphony.timezonecombo', {
        alias: 'widget.symphony.timezonecombo',
        extend: 'Ext.form.ComboBox',
        initComponent: function () {
            Ext.apply(this, {
                triggerAction: 'all',
                queryMode: 'local',
                displayField: 'displayName',
                valueField: 'id',
                store: this.generateStore()
            });

            this.callParent(arguments);
        },
        generateStore: function () {
            var zones = [];
            Ext.each(Symphony.TimeZones, function (tz) {
                zones.push([tz.displayName, tz.zone, tz.gtmId]);
            });
            var store = new Ext.data.ArrayStore({
                fields: ['displayName', 'zone', 'id'],
                data: zones
            });
            return store;
        }
    });


    // the timezone combobox above uses the GoToMeeting zones, etc, this one uses the .NET ones
    Symphony.NetTimeZoneCombo = Ext.define('symphony.nettimezonecombo', {
        alias: 'widget.symphony.nettimezonecombo',
        extend: 'Ext.form.ComboBox',
        initComponent: function () {
            Ext.apply(this, {
                triggerAction: 'all',
                queryMode: 'local',
                displayField: 'displayName',
                valueField: 'zoneId',
                store: this.generateStore()
            });

            this.callParent(arguments);
        },
        generateStore: function () {
            var zones = [];
            Ext.each(Symphony.NetTimeZones, function (tz) {
                zones.push([tz.displayName, tz.zoneId]);
            });
            var store = new Ext.data.ArrayStore({
                fields: ['displayName', 'zoneId'],
                data: zones
            });
            return store;
        }
    });



    Symphony.getCombo = function (name, label, store, options) {
        return Ext.apply({
            xtype: 'combo',
            name: name,
            allowBlank: true,
            fieldLabel: label,
            anchor: '100%',
            triggerAction: 'all',
            queryMode: 'local',
            forceSelection: false,
            editable: false,
            store: store,
            valueField: 'id',
            displayField: 'text'
        }, options);
    };

    Symphony.getYesNoInheritStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [
                [null, '- Default -'],
                [0, 'No'],
                [1, 'Yes']
            ]
        });
    };

    Symphony.getArtisanTestTypeStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [
                [null, '- Default -'],
                [Symphony.ArtisanTestType.gameBoard, 'Game Board'],
                [Symphony.ArtisanTestType.gameWheel, 'Game Wheel'],
                [Symphony.ArtisanTestType.sequential, 'Sequential'],
                [Symphony.ArtisanTestType.random, 'Random']
            ]
        });
    };

    Symphony.getRetestModeStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [
                [Symphony.RetestMode.alwaysAllow, 'Always Allow'],
                [Symphony.RetestMode.onlyOnFailure, 'Only Allow on Failure']
            ]
        });
    };

    Symphony.getArtisanNavigationMethodStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [
                [null, '- Default -'],
                [Symphony.ArtisanCourseNavigationMethod.sequential, 'Sequential'],
                [Symphony.ArtisanCourseNavigationMethod.fullSequential, 'Full Sequential'],
                [Symphony.ArtisanCourseNavigationMethod.freeForm, 'Free Form']
            ]
        });
    };

    Symphony.getArtisanReviewMethodStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [
                [null, '- Default -'],
                [Symphony.ArtisanCourseReviewMethod.showReview, 'Show Review'],
                [Symphony.ArtisanCourseReviewMethod.hideReview, 'Hide Review']
            ]
        });
    };

    Symphony.getArtisanMarkingMethodStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [
                [null, '- Default -'],
                [Symphony.ArtisanCourseMarkingMethod.showAnswers, 'Show Correct Answers'],
                [Symphony.ArtisanCourseMarkingMethod.hideAnswers, 'Hide Correct Answers'],
                [Symphony.ArtisanCourseMarkingMethod.hideAnswersAndFeedback, 'Hide Correct Answers and Feedback']
            ]
        });
    };

    Symphony.getArtisanInactivityMethodStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [
                [null, '- Default -'],
                [Symphony.ArtisanCourseTimeoutMethod.showAlert, 'Show alert prior to logout'],
                [Symphony.ArtisanCourseTimeoutMethod.automatic, 'Do not show alert prior to logout']
            ]
        });
    };

    Symphony.getArtisanContentTypeStore = function () {
        return new Ext.data.ArrayStore({
            id: 0,
            model: 'simple',
            data: [
                [Symphony.ArtisanContentType.unknown, '- Default -'],
                [Symphony.ArtisanContentType.video, 'Video'],
                [Symphony.ArtisanContentType.audio, 'Audio']
            ]
        });
    };

    /* basic grids */
    Symphony.LocalGrid = Ext.define('symphony.localgrid', {
        alias: 'widget.symphony.localgrid',
        extend: 'Ext.grid.GridPanel',
        initComponent: function () {
            var proxy = this.networkProxy || new Ext.data.MemoryProxy({
                reader: new Ext.data.JsonReader({
                    idProperty: this.idProperty || null
                })
            });

            if (!this.model) {

                console.warn("NO MODEL SUPPLIED FOR GRID! THIS MUST BE RESOLVED");
                this.model = 'test';
            }
            var store = new Ext.data.Store({
                remoteSort: false,
                model: this.model,
                proxy: proxy,
                sorters: this.sortInfo
            });

            Ext.apply(this, {
                loadMask: true,
                store: store,
                colModel: this.colModel
            });
            this.callParent(arguments);
        },
        refresh: function () {
            this.store.loadData(this.data || []);
        },
        onRender: function () {
            this.setData(this.data);
            Symphony.LocalGrid.superclass.onRender.apply(this, arguments);
        },
        setData: function (data) {
            this.data = data;
            this.store.loadData(data || []);
        },
        remove: function (name, value) {
            // if only one value is specified, use the 'id' field
            if (arguments.length == 1) {
                value = name;
                name = 'id';
            }
            var idx = this.store.find(name, value);
            this.store.removeAt(idx);
        },
        disable: function () {
            if (!this.rendered) {
                this.on('afterrender', function () {
                    this.disable();
                }, this, { single: true });
            } else {
                if (!this.disabledDiv) {
                    this.disabledDiv = div = document.createElement('div');
                    div.style.width = '100%';
                    div.style.height = '100%';
                    div.style.position = 'absolute';
                    div.style.left = 0;
                    div.style.top = 0;
                    div.style.backgroundColor = '#eee';
                    Ext.get(div).setOpacity(0.4);
                    this.body.dom.appendChild(div);
                }
            }
        },
        enable: function () {
            if (this.rendered) {
                this.body.dom.removeChild(this.disabledDiv);
            }
            delete this.disabledDiv;
        }
    });


    Symphony.UnpagedGrid = Ext.define('symphony.unpagedgrid', {
        alias: 'widget.symphony.unpagedgrid',
        extend: 'Ext.grid.GridPanel',
        initComponent: function () {
            var proxy = this.networkProxy || new Ext.data.HttpProxy({
                method: 'GET',
                url: this.url,
                reader: new Ext.data.JsonReader({
                    idProperty: this.idProperty || null,
                    root: 'data'
                })
            });

            if (!this.model) {
                console.warn("NO MODEL SUPPLIED FOR GRID! THIS MUST BE RESOLVED");
                this.model = 'test';
                /*var names = [];
                for (var i = 0; i < this.colModel.config.length; i++) {
                    var dataIndex = this.colModel.config[i].dataIndex;
                    if (dataIndex) {
                        names.push({ name: dataIndex });
                    }
                }
                this.recordDef = Ext.data.Record.create(names);*/
                
            }

            var store = this.store || new Ext.data.Store({
                remoteSort: false,
                proxy: proxy,
                model: this.model
            });

            Ext.apply(this, {
                loadMask: this.hasOwnProperty('loadMask') ? this.loadMask : true,
                store: store,
                colModel: this.colModel
            });
            this.callParent(arguments);
        },
        refresh: function () {
            this.store.reload();
        },
        onRender: function () {
            this.store.on('exception', function (proxy, type, action, options, response, arg) {
                if (response.status == 200) {
                    var result = Ext.decode(response.responseText);
                    Ext.Msg.alert('Error', result.error);
                } else {
                    //TODO: add option to report/add details/etc
                    Ext.Msg.alert('Error', 'The store failed to load properly; the server responded with error code ' + response.status);
                }

            });
            Symphony.UnpagedGrid.superclass.onRender.apply(this, arguments);

            window.setTimeout(Ext.bind(function () {
                if (!this.deferLoad) {
                    this.store.load();
                }
            }, this), 1);
        },
        updateStore: function (url) {
            this.url = url;
            if (this.store) {
                this.store.proxy.url = url;
                this.store.reload();
            }
        }
    });


    Symphony.EditableUnpagedGrid = Ext.define('symphony.editableunpagedgrid', {
        alias: 'widget.symphony.editableunpagedgrid',
        extend: 'Ext.grid.GridPanel',
        initComponent: function () {
            var proxy = this.networkProxy || new Ext.data.HttpProxy({
                method: 'GET',
                url: this.url,
                reader: new Ext.data.JsonReader({
                    idProperty: this.idProperty || null,
                    root: 'data'
                })
            });

            if (!this.model) {
                var names = [];

                console.warn("NO MODEL SUPPLIED FOR GRID! THIS MUST BE RESOLVED");
                this.model = 'test';
                /*for (var i = 0; i < this.colModel.config.length; i++) {
                    var dataIndex = this.colModel.config[i].dataIndex;
                    if (dataIndex) {
                        names.push({ name: dataIndex });
                    }
                }
                this.recordDef = Ext.data.Record.create(names);*/
                
            }

            var store = this.store || new Ext.data.Store({
                remoteSort: false,
                proxy: proxy,
                model: this.model
            });

            Ext.apply(this, {
                loadMask: true,
                store: store,
                colModel: this.colModel,
                plugins: [{
                    ptype: 'cellediting',
                    clicksToEdit: this.clicksToEdit
                }]
            });
            this.callParent(arguments);
        },
        refresh: function () {
            this.store.reload();
        },
        onRender: function () {
            this.store.on('exception', function (proxy, type, action, options, response, arg) {
                if (response.status == 200) {
                    var result = Ext.decode(response.responseText);
                    Ext.Msg.alert('Error', result.error);
                } else {
                    //TODO: add option to report/add details/etc
                    Ext.Msg.alert('Error', 'The store failed to load properly; the server responded with error code ' + response.status);
                }

            });
            Symphony.EditableUnpagedGrid.superclass.onRender.apply(this, arguments);

            window.setTimeout(Ext.bind(function () {
                if (!this.deferLoad) {
                    this.store.load();
                }
            }, this), 1);
        },
        disable: function () {
            if (!this.rendered) {
                this.on('afterrender', function () {
                    this.disable();
                }, this, { single: true });
            } else {
                if (!this.disabledDiv) {
                    this.disabledDiv = div = document.createElement('div');
                    div.style.width = '100%';
                    div.style.height = '100%';
                    div.style.position = 'absolute';
                    div.style.left = 0;
                    div.style.top = 0;
                    div.style.backgroundColor = '#eee';
                    Ext.get(div).setOpacity(0.4);
                    this.body.dom.appendChild(div);
                }
            }
        },
        enable: function () {
            if (this.rendered) {
                this.body.dom.removeChild(this.disabledDiv);
            }
            delete this.disabledDiv;
        },
        startEditing: function (rowIndex, colIndex) {
            this.findPlugin('cellediting').startEditByPosition({ row: rowIndex, column: colIndex });
        }
    });


    Symphony.SimpleGrid = Ext.define('symphony.simplegrid', {
        alias: 'widget.symphony.simplegrid',
        extend: 'Ext.grid.GridPanel',
        initComponent: function () {
            var me = this;

            var proxy;
            if (this.networkProxy) {
                proxy = this.networkProxy;
            } else {
                var proxyConfig = {
                    api: {
                        read: this.url
                    },
                    timeout: 60000,

                    simpleSortMode: true,
                    reader: new Ext.data.JsonReader({
                        idProperty: this.idProperty || null,
                        totalProperty: 'totalSize',
                        root: 'data'
                    })
                };

                if (this.isAllowGenericFilters) {
                    proxy = new Symphony.ux.GenericFilterAjaxProxy(proxyConfig);
                } else {
                    proxy = this.method == 'POST' ? new Ext.ux.data.proxy.JsonPostProxy(proxyConfig) : new Ext.data.HttpProxy(proxyConfig);
                }

                
            }

            this.pageSize = this.pageSize || Symphony.Settings.defaultPageSize;

            if (!this.model) {
                console.warn("NO MODEL SUPPLIED FOR GRID! THIS MUST BE RESOLVED");
                this.model = 'test';
                /*var names = [];
                for (var i = 0; i < this.colModel.config.length; i++) {
                    var dataIndex = this.colModel.config[i].dataIndex;
                    if (dataIndex) {
                        names.push({ name: dataIndex });
                    }
                }
                this.recordDef = Ext.data.Record.create(names);*/
                
            }

            // Remote sort forces autoLoad to be true
            // so take it off the store config if it is
            // enabled. Apply it before load. 
            var isApplyRemoteSort = this.store && this.store.remoteSort || !this.store;

            var store = this.store || new Ext.data.Store({
                proxy: proxy,
                model: this.model,
                baseParams: { limit: this.pageSize },
                sorters: this.sortInfo,
                pageSize: this.pageSize,
                remoteFilter: this.remoteFilter === true ? true : false,
                listeners: {
                    load: function () {
                        me.dataloadedcomplete();
                    }
                }
            });

            store.remoteSort = false;

            store.on('beforeload', function () {
                if (isApplyRemoteSort) {
                    store.remoteSort = true;
                }
            });

            var pager = this.pager || new Ext.PagingToolbar({
                displayInfo: (this.displayPagingInfo === false ? false : true),
                displayMsg: '{0} - {1} of {2}',
                emptyMsg: 'No results',
                store: store
            });

            Ext.apply(this, {
                loadMask: true,
                store: store,
                bbar: pager,
                colModel: this.colModel
            });

            this.callParent(arguments);

            var me = this;
            this.getSelectionModel().on('select', function (sm, record, rowIndex, eOpts) {
                me.fireEvent('rowselect', me, rowIndex, record);
            });
        },
        refresh: function () {
            this.store.reload();
        },
        onRender: function () {
            Symphony.SimpleGrid.superclass.onRender.apply(this, arguments);
            window.setTimeout(Ext.bind(function () {
                if (!this.deferLoad) {
                    this.store.load({ params: { start: 0 } });
                }
                this.store.on('exception', function (proxy, type, action, options, response, arg) {
                    if (response.status == 200) {
                        var result = Ext.decode(response.responseText);
                        Ext.Msg.alert('Error', result.error);
                    } else {
                        //TODO: add option to report/add details/etc
                        Ext.Msg.alert('Error', 'The store failed to load properly; the server responded with error code ' + response.status);
                    }

                });
            }, this), 1);
        },
        disable: function () {
            if (!this.rendered) {
                this.on('afterrender', function () {
                    this.disable();
                }, this, { single: true });
            } else {
                if (!this.disabledDiv) {
                    this.disabledDiv = div = document.createElement('div');
                    div.style.width = '100%';
                    div.style.height = '100%';
                    div.style.position = 'absolute';
                    div.style.left = 0;
                    div.style.top = 0;
                    div.style.backgroundColor = '#eee';
                    Ext.get(div).setOpacity(0.4);
                    this.body.dom.appendChild(div);
                }
            }
        },
        enable: function () {
            if (this.rendered && this.disabledDiv) {
                this.body.dom.removeChild(this.disabledDiv);
            }
            delete this.disabledDiv;
        },
        dataloadedcomplete: function () {
            this.fireEvent('datagridloaded');
        }
    });

    Symphony.SearchableGrid = Ext.define('symphony.searchablegrid', {
        alias: 'widget.symphony.searchablegrid',
        extend: 'symphony.simplegrid',
        performSearch: function (searchText) {
            // set it as a "base" parameter so it applies to pagination and refreshes too
            this.store.setBaseParam('searchText', searchText);
            //this.store.setBaseParam('pageIndex', 0);
            this.store.currentPage = 1;
            this.store.load({
                page: 1,
                start: 0
            });
        },
        initComponent: function () {
            var grid = this;

            var searchConfig = [
                '->',
                {
                    xtype: 'textfield',
                    name: 'searchText',
                    emptyText: 'Enter search text',
                    listeners: {
                        specialkey: function (txt, e) {
                            if (e.getKey() == e.ENTER) {
                                grid.performSearch(txt.getValue());
                            }
                        }
                    }
                },
                {
                    xtype: grid.searchMenu ? 'splitbutton' : 'button',
                    ref: '../searchButton',
                    iconCls: 'x-button-search',
                    menu: grid.searchMenu,
                    handler: function (b, e) {
                        var txt = b.findParentByType('toolbar').find('name', 'searchText')[0];
                        grid.performSearch(txt.getValue());
                    }
                }
            ];

            if (this.tbar && this.tbar.isXType && this.tbar.isXType('panel')) {
                var toolbars = this.tbar.find('xtype', 'toolbar');
                if (toolbars.length) {
                    var toolbarItems = [];
                    for (var i = 0; i < searchConfig.length; i++) {
                        if (typeof (searchConfig[i]) == "object") {
                            toolbarItems.push(searchConfig[i]);
                        }
                    }
                    toolbars[0].add(new Ext.Panel({
                        layout: 'hbox',
                        width: 180,
                        cls: 'x-panel-mc',
                        bodyStyle: 'border: none; padding-top: 0px;',
                        style: 'border: none; padding-top: 0px;',
                        border: false,
                        frame: false,
                        defaults: {
                            border: false
                        },
                        items: toolbarItems
                    }));
                }
            } else {
                var currentItems = this.tbar ? this.tbar.items || [] : [];
            
                this.tbar = {
                    hideBorders: true,
                    items: currentItems.concat(searchConfig)
                }
            }

            this.callParent(arguments);
        },
        addSearchMenuItems: function (items) {
            if (this.searchButton && this.searchButton.menu) {
                this.searchButton.menu.add(items);
            }
        },
        searchOptionClicked: function (item, checked) {
            var me = this;
            if (me._filter) {
                me.store.proxy.url = me.url;
                me.store.un('beforeload', me._filter);
            }

            if (checked) {
                if (item.filter) {
                    me._filter = Ext.bind(me.filter, me, [item.filter]);
                    me.store.on('beforeload', me._filter);
                }
                me.refresh();
            }
        },
        checkSearchOptionClicked: function (item, checked) {
            var me = this;

            if (me._filter) {
                me.store.proxy.url = me.url;
                me.store.un('beforeload', me._filter);
            }

            var filter = [];
            for (var i = 0; i < me.searchButton.menu.items.length; i++) {
                var menuItem = me.searchButton.menu.items.items[i];
                if (menuItem.checked) {
                    filter.push(menuItem.filter);
                }
            }
            if (filter.length > 0) {
                me._filter = Ext.bind(me.filter, me, [filter]);
                me.store.on('beforeload', me._filter);
            }
            me.refresh();
        },
        filter: function (filter) {
            var f = Ext.encode(filter);
            this.store.proxy.api.read = this.url + '?filter=' + f;
        },
        updateStore: function (url) {
            this.url = url;
            if (this.store) {
                this.store.proxy.api.read = url;
                this.store.reload();
            }
        }
    });


    Symphony.SearchableLocalGrid = Ext.define('symphony.searchablelocalgrid', {
        alias: 'widget.symphony.searchablelocalgrid',
        extend: 'symphony.localgrid',
        performSearch: function (searchText) {
            // set it as a "base" parameter so it applies to pagination and refreshes too
            if (!this.store) { return; }
            if (searchText) {
                var filter = [];
                for (var i = 0; i < this.searchColumns.length; i++) {
                    filter.push({
                        property: this.searchColumns[i],
                        value: searchText
                    });
                }
                this.store.filter(filter);
            } else {
                this.store.clearFilter();
            }
        },
        initComponent: function () {
            var grid = this;
            var currentItems = this.tbar ? this.tbar.items || [] : [];
            Ext.apply(this, {
                tbar: {
                    hideBorders: true,
                    items: currentItems.concat([
                        '->',
                        {
                            xtype: 'textfield',
                            name: 'searchText',
                            emptyText: 'Enter search text',
                            listeners: {
                                specialkey: function (txt, e) {
                                    if (e.getKey() == e.ENTER) {
                                        grid.performSearch(txt.getValue());
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-button-search',
                            handler: function (b, e) {
                                var txt = b.findParentByType('toolbar').find('name', 'searchText')[0];
                                grid.performSearch(txt.getValue());
                            }
                        }
                    ])
                }
            });
            this.callParent(arguments);
        }
    });


    /* end basic grids */

    // A simple editor for name/description pairs
    // it assumes the "save" url is the original url with a "save/" tacked on the end
    // and that the original url can be used without any parameters to get the complete list
    // of items to be edited
    Symphony.EditNameDescriptionPairs = function (title, url, callback, options) {
        options = options || {};

        var w = new Ext.Window({
            title: 'Manage Categories',
            modal: true,
            width: 500,
            height: 500,
            layout: 'fit',
            items: [{
                tbar: [{
                    xtype: 'button',
                    text: 'Add',
                    iconCls: 'x-button-add',
                    handler: function () {
                        var Category = Ext.data.Record.create(Symphony.Definitions.category);
                        var category = new Category({ name: 'New Category', description: 'New Category' });
                        var grid = w.grid;
                        grid.store.add(category);
                        grid.startEditing(grid.store.getCount() - 1, 0);
                    }
                }],
                border: false,
                xtype: 'symphony.editableunpagedgrid',
                url: url,
                layout: 'fit',
                ref: 'grid',
                model: 'category',
                clicksToEdit: 1,
                colModel: new Ext.grid.ColumnModel({
                    defaults: {
                        sortable: true,
                        renderer: Symphony.Portal.valueRenderer
                    },
                    columns: [
                        { /*id: 'name',*/ header: 'Name', dataIndex: 'name', editor: new Ext.form.TextField({ allowBlank: false }) },
                        {
                            /*id: 'description',*/ header: 'Description', dataIndex: 'description', editor: new Ext.form.TextField({ allowBlank: false }),
                            flex: 1
                        }
                    ]
                })
            }],
            buttons: [{
                text: 'Save',
                iconCls: 'x-button-save',
                handler: function () {
                    w.grid.stopEditing();
                    var records = w.grid.store.getModifiedRecords();
                    var results = [];
                    for (var i = 0; i < records.length; i++) {
                        var len = results.push(records[i].data);
                        if (options.categoryTypeId) {
                            results[len - 1].categoryTypeId = options.categoryTypeId;
                        }
                    }
                    Symphony.Ajax.request({
                        url: url + 'save/',
                        jsonData: results,
                        success: function (result) {
                            w.close();
                            if (callback) {
                                callback(result);
                            }
                        }
                    });
                }
            }, {
                text: 'Cancel',
                iconCls: 'x-button-cancel',
                handler: function () {
                    w.close();
                }
            }]
        }).show();
    };

    /* start tree */
    Symphony.SearchableTree = Ext.define('symphony.searchabletree', {
        alias: 'widget.symphony.searchabletree',
        extend: 'Ext.tree.Panel',
        idColumn: 'id',
        parentColumn: 'parentId',
        displayColumn: 'name',
        autoScroll: true,
        containerScroll: true,
        animate: true,
        cascadeChecks: true,
        rootVisible: false,
        rootText: 'Root',
        initComponent: function () {
            var currentItems = this.tbar ? this.tbar.items || [] : [];
            var tree = this;

            var store = this.store || Ext.create('Ext.data.TreeStore', {
                model: this.model,
                proxy: {
                    type: 'memory'
                },
                root: this.root || Ext.create(this.model, {
                    text: this.rootText,
                    children: this.data || []
                }),
                autoLoad: !this.deferLoad
            });

            delete this.root;

            if (this.checkboxes) {
                store.on('load', function () {
                    var root = store.getRootNode();
                    root.cascadeBy(function (node) {
                        if (typeof (node.checked) == 'undefined') {
                            node.data.checked = false;
                        }
                    });
                    tree.fireEvent('treedataloaded', tree);
                });
            }

            Ext.apply(this, {
                hiddenNodes: [],
                rootVisible: this.rootVisible,
                store: store,
                tbar: !this.isHideSearch ? {
                    hideBorders: true,
                    items: currentItems.concat([
                        '->',
                        {
                            xtype: 'textfield',
                            name: 'searchText',
                            emptyText: 'Enter search text',
                            listeners: {
                                specialkey: function (txt, e) {
                                    if (e.getKey() == e.ENTER) {
                                        tree.performSearch(txt.getValue());
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-button-search',
                            handler: function (b, e) {
                                var txt = b.findParentByType('toolbar').find('name', 'searchText')[0];
                                tree.performSearch(txt.getValue());
                            }
                        }
                    ])
                } : null,
                listeners: Ext.apply({
                    beforeselect: function(tree, record, index,  opts) {
                        if (record.get('cls') == 'x-disabled') {
                            return false;
                        }
                    },
                    checkchange: function (node, checked) {
                        if (!tree.cascadeChecks) { return; }

                        if (node.get('cls') == 'x-disabled') {
                            node.set('checked', !checked);
                            return false;
                        }

                        tree.cascadeChecks = false;
                        node.cascade(function (subnode) {
                            subnode.set('checked', checked);

                            if (subnode != node) {
                                if (checked) {
                                    subnode.set('cls', 'x-disabled');
                                } else {
                                    subnode.set('cls', '');
                                }
                            }
                        });
                        node.disabled = false;
                        node.set('disabled', false);
                        tree.cascadeChecks = true;
                    }
                }, this.listeners || {})
            });

            this.callParent(arguments);
        },
        disableSubnodes: function () {
            if (!this.cascadeChecks) { return; }
            this.cascadeChecks = false;

            var cascadeNode = function (node) {
                for (var i = 0; i < node.childNodes.length; i++) {
                    var thisNode = node.childNodes[i];

                    if (thisNode.parentNode && thisNode.parentNode.data.checked) {
                        // node is checked, check and disable subnodes
                        thisNode.cascade(function (subnode) {
                            subnode.data.checked = true;
                            subnode.disable();
                        });
                    } else {
                        cascadeNode(thisNode);
                    }
                }
            };
            cascadeNode(this.root);

            this.cascadeChecks = true;
        },
        performSearch: function (text) {
            Ext.each(this.hiddenNodes, function (n) {
                n.ui.show();
            });
            if (!text) {
                this.clearFilter();
                return;
            }
            this.expandAll();
            
            this.filterByText(text);
        },
        filterByText: function (text) {
            if (this.searchField) {
                this.filterBy(text, this.searchField);
            } else {
                this.filterBy(text, 'text');
            }

        },
        /**
         * Filter the tree on a string, hiding all nodes except those which match and their parents.
         * @param The term to filter on.
         * @param The field to filter on (i.e. 'text').
         */
        filterBy: function (text, by) {
            this.filterByArray([text], by);
        },
        /**
         * Filter the tree on a array of strings, hiding all nodes except those which match one of the strings and their parents.
         * @param The array of terms to filter on.
         * @param The field to filter on (i.e. 'text').
         */
        filterByArray: function(textArr, by) {
            this.clearFilter();

            var view = this.getView(),
                me = this,
                nodesAndParents = [];

            // Find the nodes which match the search term, expand them.
            // Then add them and their parents to nodesAndParents.
            this.getRootNode().cascadeBy(function (tree, view) {
                var currNode = this;

                for (var i = 0; i < textArr.length; i++) {

                    if (currNode && (currNode.data[by] && currNode.data[by].toString().toLowerCase().indexOf(textArr[i].toLowerCase()) > -1
                                     || me.leafFilter && me.leafFilter(textArr[i], currNode))) {
                        me.expandPath(currNode.getPath());

                        while (currNode.parentNode) {
                            nodesAndParents.push(currNode.id);
                            currNode = currNode.parentNode;
                        }
                    }
                }
            }, null, [me, view]);

            // Hide all of the nodes which aren't in nodesAndParents
            this.getRootNode().cascadeBy(function (tree, view) {
                var uiNode = view.getNodeByRecord(this);

                if (uiNode && !Ext.Array.contains(nodesAndParents, this.id) && !this.isRoot()) {
                    Ext.get(uiNode).setDisplayed('none');
                }
            }, null, [me, view]);
        },
        clearFilter: function () {
            var view = this.getView();

            this.getRootNode().cascadeBy(function (tree, view) {
                var uiNode = view.getNodeByRecord(this);

                if (uiNode) {
                    Ext.get(uiNode).setDisplayed('table-row');
                }
            }, null, [this, view]);
        }
    });

    Symphony.TableReader = Ext.define('Symphony.TableReader', {
        extend: 'Ext.data.reader.Json',
        alias: 'reader.symphony.tablereader',
        getResponseData: function (response) {
            var data = this.callParent([response]);
            
            var records = [];
            var recordDictionary = {};

            // because refresh is dreadfully slow and memory hogging, we can override by forcing everything into a root node.
            if (this.rootText && data.records.length > 0) {
                var fakeRoot = Ext.create(data.records[0].$className, {});
                fakeRoot.set('name', this.rootText);
                fakeRoot.set('id', 0);
                data.records.unshift(fakeRoot);
            }

            for (var i = 0; i < data.records.length; i++) {
                var record = data.records[i];

                record.data.expanded = true;
                record.data.loaded = true;
                record.data.icon = '/images/leaf.gif';

                if (recordDictionary[record.get('parentId')]) {
                    var parentRecord = recordDictionary[record.get('parentId')];

                    parentRecord.appendChild(record);
                    parentRecord.data.leaf = false;
                    parentRecord.data.icon = "";
                } else {
                    records.push(record);
                }

                recordDictionary[record.get('id')] = record;
            }

            data.records = records;
            return data;
        }
    });

    Symphony.TableTree = Ext.define('symphony.tabletree', {
        alias: 'widget.symphony.tabletree',
        extend: 'symphony.searchabletree',
        rootText: null,
        initComponent: function () {
            var proxy = new Ext.data.HttpProxy({
                method: 'GET',
                url: this.url,
                reader: new Symphony.TableReader({
                    idProperty: this.idProperty || null,
                    root: 'data',
                    rootText: this.rootText
                })
            });

            if (!this.model) {
                console.warn("NO MODEL SUPPLIED FOR GRID! THIS MUST BE RESOLVED");
                this.model = 'test';
                /*var names = [];
                for (var i = 0; i < this.colModel.config.length; i++) {
                    var dataIndex = this.colModel.config[i].dataIndex;
                    if (dataIndex) {
                        names.push({ name: dataIndex });
                    }
                }
                this.recordDef = Ext.data.Record.create(names);*/
            }
            
            var store = this.store = new Ext.data.TreeStore({
                model: this.model,
                proxy: proxy,
                autoLoad: !this.deferLoad,
                root: {
                    expanded: true,
                    loaded: this.deferLoad,
                    text: this.rootText,
                    name: this.rootText,
                    children: []
                }
            });

            
            Ext.apply(this, {
                data: null,
                store: this.store
            });
            this.callParent(arguments);

        },
        refresh: function () {
            this.setLoading(true);
            this.store.on('load', function () {
                this.setLoading(false);
            }, this, { single: true });
            this.store.load();
        },
        performSearch: function (text) {
            this.store.setBaseParam('searchText', text);
            this.refresh();
        },
        isChecked: function (rec) {
            return false;
        }
    });

    Symphony.SpellCheckArea = Ext.define('symphony.spellcheckarea', {
        alias: 'widget.symphony.spellcheckarea',
        extend: 'Ext.form.field.HtmlEditor',
        initComponent: function () {
            this.callParent(arguments);
            return;
            // for now, ignore; causes issues with paste
            if (!spellChecker) {
                spellChecker = new LiveSpellInstance();
                spellChecker.Fields = 'ALL'; //fields;
                spellChecker.ServerModel = 'aspx';
                spellChecker.ActivateAsYouType();
            }
        }
    });


    Symphony.SpellCheckField = Ext.define('symphony.spellcheckfield', {
        alias: 'widget.symphony.spellcheckfield',
        extend: 'Ext.form.TextField',
        //height: 20,
        initComponent: function () {
            this.callParent(arguments);
        }
    });


    // Very simple combobox to act like html select box
    Symphony.DropDownBox = Ext.define('symphony.dropdownbox', {
        alias: 'widget.symphony.dropdownbox',
        extend: 'Ext.form.ComboBox',
        allowBlank: false,
        editable: false,
        triggerAction: 'all',
        typeAhead: false,
        initComponent: function () {
            //            Ext.apply(this, { 
            //            });
            this.callParent(arguments);
            //        },
            //        setValue : function(value){
            //            Symphony.DropDownBox.superclass.setValue.call(this, value);
            //            if (this.)
            //            return this;
        }
    });

    /*
    * AssetGrid
    */
    Symphony.AssetsGrid = Ext.define('symphony.assetsgrid', {
        alias: 'widget.symphony.assetsgrid',
        extend: 'symphony.searchablegrid',
        allowUploads: true,
        initComponent: function () {
            var me = this;
            var url = this.assetsUrl;

            var colModel = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true,
                    align: 'left',
                    renderer: Symphony.Portal.valueRenderer
                },
                columns: [{
                    /*id: 'name',*/
                    header: 'Preview',
                    align: 'center',
                    width: 65,
                    renderer: function (value, meta, record) {
                        return new Ext.XTemplate('<img src="{relativeUrl}" width="100%" height="100%" />').apply(record.raw);
                    }
                }, {
                    /*id: 'description',*/
                    header: 'Description',
                    renderer: function (value, meta, record) {
                        return new Ext.XTemplate('<b>{name}</b><br/>{description}').apply(record.raw);
                    },
                    flex: 1
                }]
            });

            Ext.apply(this, {
                idProperty: 'id',
                deferLoad: true,
                hideHeaders: true,
                url: url,
                colModel: colModel,
                model: 'artisanAsset',
                searchMenu: {
                    items: []
                },
                tbar: {
                    items: this.allowUploads ? [{
                        xtype: 'symphony.assetuploadbutton',
                        assetsUploadUrl: me.assetsUploadUrl,
                        templateId: me.templateId,
                        listeners: {
                            'uploadcomplete': function (data) {
                                me.fireEvent('uploadcomplete', data);
                            }
                        }
                    }] : []
                },
                listeners: {
                    cellclick: function (grid, td, columnIndex, record, tr, rowIndex, e, eOpts) {
                        var record = grid.getStore().getAt(rowIndex); // Get the Record
                        me.fireEvent('assetselected', record);
                    },
                    uploadcomplete: function (asset) {
                        var recordDef = Ext.data.Record.create(Symphony.Definitions.artisanAsset);
                        var record = new recordDef(asset);
                        this.refresh();
                    }
                }
            });

            this.callParent(arguments);
        },
        onRender: function () {
            Symphony.AssetsGrid.superclass.onRender.apply(this, arguments);

            var me = this;
            // defer this jazz so it doesn't affect main page load time
            window.setTimeout(function () {
                // get list of file types
                var searchItems = [];
                Symphony.Ajax.request({
                    method: 'GET',
                    url: me.assetsUrl,
                    success: function (result) {
                        for (i = 0; i < result.data.length; i++) {
                            searchItems.push({
                                hideOnClick: false,
                                text: result.data[i].filename,
                                checked: true,
                                filter: {
                                    assetTypeId: result.data[i].filename
                                },
                                checkHandler: Ext.bind(me.checkSearchOptionClicked, me)
                            });
                        }
                        me.addSearchMenuItems(searchItems);
                        // pre-set the filter for the grid and refresh it
                        me.checkSearchOptionClicked();
                    }
                });
            }, 1);
        }
    });

    // Start AssetPicker
    Symphony.AssetPicker = Ext.define('symphony.assetpicker', {
        alias: 'widget.symphony.assetpicker',
        extend: 'Ext.Window',
        allowUpload: false,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'fit',
                defaults: {
                    border: false
                },
                items: [{
                    xtype: 'symphony.assetstab',
                    ref: 'assetsTab',
                    assetsUrl: me.assetsUrl,
                    assetsUploadUrl: me.assetsUploadUrl,
                    allowUpload: this.allowUpload,
                    listeners: {
                        assetavailable: function () {
                            me.okBtn.enable();
                        },
                        assetunavailable: function () {
                            me.okBtn.disable();
                        },
                        assetdoubleclicked: Ext.bind(this.onOkClick, this)
                    }
                }],
                // buttons are placed in the footer's "items" array, so the ref has one more layer to go through
                buttons: [{
                    text: 'OK',
                    ref: '../okBtn',
                    disabled: true,
                    handler: Ext.bind(this.onOkClick, this)
                }, {
                    text: 'Cancel',
                    ref: '../cancelBtn',
                    handler: Ext.bind(this.onCancelClick, this)
                }]
            });
        },
        onOkClick: function () {
            this.fireEvent('assetpicked', this.assetsTab.getActiveAsset());
            this.hide();
        },
        onCancelClick: function () {
            this.hide();
        }
    });

    Symphony.AssetPicker.getInstance = function (options, callback) {
        Symphony.AssetPicker._instanceCallback = callback;
        if (Symphony.AssetPicker._instance == null) {
            Symphony.AssetPicker._instance = new Symphony.AssetPicker({
                title: 'Insert Asset',
                width: 900,
                height: 600,
                cls: 'maxindex',
                modal: true,
                closeAction: 'hide',
                assetsUploadUrl: options.assetsUploadUrl,    // handler to save the asset
                assetsUrl: options.assetsUrl,
                listeners: {
                    assetpicked: function (asset) {
                        Symphony.AssetPicker._instanceCallback(asset);
                    }
                }
            });
        }
        Ext.apply(Symphony.AssetPicker._instance, options || {});
        return Symphony.AssetPicker._instance;
    };
    // END AssetPicker

    // AssetsTab
    Symphony.AssetsTab = Ext.define('symphony.assetstab', {
        alias: 'widget.symphony.assetstab',
        extend: 'Ext.Panel',
        count: 0,
        allowUploads: true,
        autoPlay: false,
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                layout: 'border',
                items: [{
                    split: true,
                    collapsible: true,
                    region: 'west',
                    border: false,
                    width: 300,
                    title: 'Select an Asset',
                    xtype: 'symphony.assetsgrid',
                    assetsUrl: me.assetsUrl,
                    assetsUploadUrl: me.assetsUploadUrl,
                    ref: 'assetsGrid',
                    allowUploads: this.allowUploads,
                    listeners: {
                        assetselected: function (record) {
                            //me.addPanel(record);
                        },
                        celldblclick: function (grid, rowIndex, columnIndex, e) {
                            me.fireEvent('assetdoubleclicked');
                        }
                    }
                }]
            });
        },
        getActiveAsset: function () {
            var tab = this.displayPane.layout.getActiveItem();
            return tab.getAsset();
        }
    });
    // END AssetsTab

    Symphony.AssetUploadButton = Ext.define('symphony.assetuploadbutton', {
        alias: 'widget.symphony.assetuploadbutton',
        extend: 'Ext.ux.SWFUploadButton',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                text: 'Upload',
                iconCls: 'x-button-add',
                uploadUrl: me.assetsUploadUrl,
                name: 'assetUploader',
                listeners: {
                    'queue': function (file) {
                        this.startUpload();
                    },
                    'queueerror': function () {
                        Ext.Msg.alert('Error', 'The file could not be queued');
                    },
                    'uploadstart': function (item) {
                        this.addParameter('id', me.templateId);

                        Ext.Msg.progress('Please wait...', 'Please wait while your file is uploaded...');
                    },
                    'uploadprogress': function (item, completed, total) {
                        var ratio = parseFloat(completed / total);
                        Ext.Msg.updateProgress(ratio, parseInt(ratio * 100, 10) + '% complete');
                    },
                    'uploaderror': function () {
                        this.error = true;
                        Ext.Msg.hide();
                        Ext.Msg.alert('Upload failed', 'The upload failed. Please make sure it is within the proper file size limitations.');
                    },
                    'uploadsuccess': function (item, response, hasData) {
                        var result = Ext.decode(response);
                        if (result.success) {
                            Ext.Msg.hide();
                            Symphony.Ajax.request({
                                method: 'GET',
                                url: me.assetsUrl + result.id,
                                success: function (result) {
                                    me.fireEvent('uploadcomplete', result.data);
                                }
                            });
                        } else {
                            Ext.Msg.hide();
                            Ext.Msg.alert('Upload failed', result.error);
                        }
                    }
                }
            });
            Symphony.AssetUploadButton.superclass.initComponent.call(this);
        }
    });

    Symphony.AssetLinkButton = Ext.define('symphony.assetlinkbutton', {
        alias: 'widget.symphony.assetlinkbutton',
        extend: 'Ext.Button',
        initComponent: function () {
            var me = this;
            Ext.apply(this, {
                text: 'Link',
                iconCls: 'x-button-link',
                xtype: 'button',
                listeners: {
                    click: function () {
                        Ext.Msg.prompt('Enter a URL', 'Please enter a valid asset URL:', function (btn, value) {
                            if (btn == 'ok') {
                                Ext.Msg.wait('Please wait while your asset is created...');
                                Symphony.Ajax.request({
                                    method: 'POST',
                                    url: me.assetsUrl,
                                    jsonData: {
                                        url: value
                                    },
                                    success: function (result) {
                                        me.fireEvent('linkcomplete', result.data);
                                        Ext.Msg.hide();
                                    }
                                });
                            }
                        });
                    }
                }
            });
            // Symphony.Artisan.AssetLinkButton.superclass.initComponent.call(this);
        }
    });
    /*
    * END AssetGrid
    */

    Symphony.Ajax = {
        request: function (options) {
            options = Ext.apply({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            }, options || {});

            var defaultFailure = true;
            if (options.failure) {
                defaultFailure = false;
            }

            options.failure = options.failure || function (args) {
                var msg = 'The request failed.';
                if (args.status != 200) {
                    msg += ' The error was: ' + args.status + ': ' + args.statusText;
                }
                Ext.Msg.alert('Error', msg);
            };

            var success = options.success || function () { };
            options.success = function (args) {
                var result = Ext.decode(args.responseText);
                if (!result.success) {
                    if (!defaultFailure) {
                        options.failure(result);
                    } else {
                        Ext.Msg.alert('Error', result.error);
                    }
                } else {
                    success(result);
                }

                if (typeof (options.complete) == 'function') {
                    options.complete(args);
                }
            };
            Ext.Ajax.request(options);
        }
    };

    Ext.override(Ext.form.DateField, {
        getValue: function (overrideDateOnly) {
            var date = this.parseDate(Ext.form.DateField.superclass.getValue.call(this)) || "";
            if (date && this.dateOnly && !overrideDateOnly) {
                // TODO: Make this a util function
                // subtract the timezone offset from the date to effectively zero out the time once it's converted to UTC
                date = date.add(Date.MINUTE, date.getTimezoneOffset());
            }
            return date;

        },
        setValue: function (date) {

            if (date && !date.getTimezoneOffset) {
                // For some reason the date was passed in as a string so try to parse it
                date = this.parseDate(date);
            }

            if (date && this.dateOnly) {
                date = date.add(Date.MINUTE, date.getTimezoneOffset());
            }
            return Ext.form.DateField.superclass.setValue.call(this, this.formatDate(this.parseDate(date)));
        }
    });

    
    Ext.override(Ext.form.Checkbox, {
        initComponent: function () {
            if (this.afterText) {
                console.warn("After text does not exist on radio. Use boxLabel instead");
                this.boxLabel = this.afterText;
            }
            /*
            * Box label hack
            * Checkboxes rendered without box labels would end up being nudged down a few pixels. 
            * this was enough to cause the checkbox to be noticeably out of alignment with the
            * field label, and potentially be partially cut off if rendered at the bottom of a 
            * container. This just ensures there is always a blank box label rendered so checkboxes
            * will always render in the same position.
            */
            if (!this.boxLabel) {
                this.boxLabel = "&nbsp";
            }
            this.callParent(arguments);
        }
    });
    console.warn("Removed this render override as it was breaking boxlabels... review the conditions for which this was required.");
    
    // fix for checkboxes not rendering under certain conditions in IE
    Ext.override(Ext.form.Checkbox, {
        onRender: function (ct, position) {
            Ext.form.Checkbox.superclass.onRender.call(this, ct, position);
            if (this.inputValue !== undefined) {
                this.el.dom.value = this.inputValue;
            }
            /*this.wrap = this.el.wrap({ cls: 'x-form-check-wrap' });
            if (this.boxLabel) {
                this.wrap.createChild({ tag: 'label', htmlFor: this.el.id, cls: 'x-form-cb-label', html: this.boxLabel });
            }*/
            if (this.checked) {
                this.setValue(true);
            } else {
                this.checked = this.el.dom.checked;
            }
            /*if (Ext.isIE && !Ext.isStrict) {
                this.wrap.repaint();
            }
            this.resizeEl = this.positionEl = this.wrap;
            if (this.afterText) {
                var container = this.el.parent().dom;
                var span = document.createElement('span');
                span.innerHTML = this.afterText;
                container.appendChild(span);
            }*/
        }
    });

    Symphony.parseQueryString = function (queryString) {
        var parts = {};
        var pairs = queryString.split('&');
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split('=');
            parts[pair[0]] = pair[1];
        }
        return parts;
    }

    Ext.override(Ext.ux.form.field.BoxSelect, {
        isSuperBoxSelect: true
    });

    // adds a "postfix" function to columns used in a grouping view
    // that lets us modify the group template without creating a new group
    Ext.override(Ext.grid.GroupingView, {
        doRender: function (cs, rs, ds, startRow, colCount, stripe) {
            if (rs.length < 1) {
                return '';
            }

            if (!this.canGroup() || this.isUpdating) {
                return Ext.grid.GroupingView.superclass.doRender.apply(this, arguments);
            }

            var groupField = this.getGroupField(),
                colIndex = this.cm.findColumnIndex(groupField),
                g,
                gstyle = 'width:' + this.getTotalWidth() + ';',
                cfg = this.cm.config[colIndex],
                groupRenderer = cfg.groupRenderer || cfg.renderer,
                prefix = this.showGroupName ? (cfg.groupName || cfg.header) + ': ' : '',
                groups = [],
                curGroup, i, len, gid;


            for (i = 0, len = rs.length; i < len; i++) {
                var rowIndex = startRow + i,
                    r = rs[i],
                    gvalue = r.data[groupField],
                    postfix = cfg.postfix ? cfg.postfix(r, groupRenderer, rowIndex, colIndex, ds, cfg) : '';

                g = this.getGroup(gvalue, r, groupRenderer, rowIndex, colIndex, ds);
                if (!curGroup || curGroup.group != g) {
                    gid = this.constructId(gvalue, groupField, colIndex);
                    // if state is defined use it, however state is in terms of expanded
                    // so negate it, otherwise use the default.
                    this.state[gid] = !(Ext.isDefined(this.state[gid]) ? !this.state[gid] : this.startCollapsed);
                    curGroup = {
                        group: g,
                        gvalue: gvalue,
                        text: prefix + g + postfix,
                        groupId: gid,
                        startRow: rowIndex,
                        rs: [r],
                        cls: this.state[gid] ? '' : 'x-grid-group-collapsed',
                        style: gstyle
                    };
                    groups.push(curGroup);
                } else {
                    curGroup.rs.push(r);
                }
                r._groupId = gid;
            }

            var buf = [];
            for (i = 0, len = groups.length; i < len; i++) {
                g = groups[i];
                this.doGroupStart(buf, g, cs, ds, colCount);
                buf[buf.length] = Ext.grid.GroupingView.superclass.doRender.call(
                        this, cs, g.rs, ds, g.startRow, colCount, stripe);

                this.doGroupEnd(buf, g, cs, ds, colCount);
            }
            return buf.join('');
        }
    });
})();

Ext.override(Ext.AbstractComponent, {
    find: function (prop, val) {
        var items = this.down('*[' + prop + '="' + val + '"]');
        if (items == null) {
            return [];
        }
        return [items];
    }
});

// Override to change the monitor selector.
//
// The monitor is an object that runs in the background of every form panel and
// tracks which form elements are added or removed to the form panel. This is
// how the "getValues" method works.
//
// In Ext 4.2.0, this causes issues with nested forms because the monitor will
// parse the nested forms' form items as well. If the parent form and nested
// form both have a field with the same "name" property, this will cause
// getValues to return an array of data, which is almost surely not expected.
//
// Ext 4.2.2 changes the selector so that if you add the excludeForm property to
// a form item, it won't be included. This change is future-proof, if you
// upgrade to Ext 4.2.2, you can safely remove this.
//
// http://docs.sencha.com/extjs/4.2.2/source/Basic.html#Ext-form-Basic-method-getFields
var formCtor = Ext.form.Basic.prototype.constructor;

Ext.form.Basic.prototype.constructor = function(owner, config) {
    formCtor.call(this, owner, config);

    this.monitor.selector = '[isFormField]:not([excludeForm])';
};

// Finds nested components in a config block before they are initialized. Useful if you want to set
// a value on a config property in initComponent before the component is initialized. Supports
// multiple levels of searching using a dot to separate itemIds, see example below.
//
// Example:
// var cfg = {
//     xtype: 'container',
//     itemId: 'bigContainer',
//
//     items: [{
//         itemId: 'childOne'
//     }, {
//         itemId: 'childTwo'
//     }]
// }
//
// var cfgOne = Symphony.Object.findByItemId(cfg, 'bigContainer.childOne');
// cfgOne.modelId = 100;
//
// Very basic, can possibly be extended to a more "xpath"-y syntax at some point.
//
Symphony.Object = {
    findByItemId: function(cfg, itemId) {
        var parts = itemId.split('.'),
            curItemId = parts.shift();

        if (!curItemId.length || curItemId.length < 1) {
            Log.error('Cannot parse empty/null itemId: ' + itemId);
            return null;
        }

        if (!cfg.items) {
            Log.error(curItemId + ' does not have an items property.');
            return null;
        }

        for (var i = 0; i < cfg.items.length; i++) {
            if (cfg.items[i].itemId == curItemId) {
                if (parts.length == 0) {
                    return cfg.items[i];
                } else {
                    return Symphony.Object.findByItemId(cfg.items[i], parts.join('.'));
                }
            }
        }

        return null;
    }
};

// Bug in 4.2.0
// https://www.sencha.com/forum/showthread.php?259118-4.2.0-GA-GridViewDragDrop-TreeViewDragDrop-causes-scrolling-on-selection
// 
// Fixed in 4.2.2, can remove then.
Ext.override(Ext.view.DragZone, {
    onItemMouseDown: function(view, record, item, index, e) {
        if (!this.isPreventDrag(e, record, item, index)) {
            // Since handleMouseDown prevents the default behavior of the event, which
            // is to focus the view, we focus the view now.  This ensures that the view
            // remains focused if the drag is cancelled, or if no drag occurs.
            if (view.focusRow) {
                view.focusRow(record);
            }
            this.handleMouseDown(e);
        }
    }
});
