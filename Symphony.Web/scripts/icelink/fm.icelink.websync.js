
/*
 * Title: IceLink WebSync Extension for JavaScript
 * Version: 2.4.10
 * Copyright Frozen Mountain Software 2011+
 */

(function(name, definition) {
    if (typeof module != 'undefined') module.exports = definition();
    else if (typeof define == 'function' && typeof define.amd == 'object') define(definition);
    else this[name] = definition();
}('fm.icelink.websync', function() {

if (typeof global !== 'undefined' && !global.window) { global.window = global; global.document = { cookie: '' }; }

if (!window.fm) { throw new Error("fm must be loaded before fm.icelink.websync."); }

if (!window.fm.icelink) { throw new Error("fm.icelink must be loaded before fm.icelink.websync."); }

if (!window.fm.icelink.websync) { window.fm.icelink.websync = {}; }

var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

var __hasProp = {}.hasOwnProperty;

var __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

/*<span id='cls-fm.icelink.websync.joinConferenceCompleteArgs'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.joinConferenceCompleteArgs
 <div>
 Arguments for join-conference complete callbacks.
 </div>

@extends fm.websync.baseCompleteArgs
*/


fm.icelink.websync.joinConferenceCompleteArgs = (function(_super) {

  __extends(joinConferenceCompleteArgs, _super);

  joinConferenceCompleteArgs.prototype.__isRejoin = false;

  function joinConferenceCompleteArgs() {
    this.getIsRejoin = __bind(this.getIsRejoin, this);
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      joinConferenceCompleteArgs.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    joinConferenceCompleteArgs.__super__.constructor.call(this);
  }

  /*<span id='method-fm.icelink.websync.joinConferenceCompleteArgs-getIsRejoin'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets whether the join call was automatically
  	 invoked following a stream failure.
  	 </div>
  
  	@function getIsRejoin
  	@return {Boolean}
  */


  joinConferenceCompleteArgs.prototype.getIsRejoin = function() {
    return this.__isRejoin;
  };

  return joinConferenceCompleteArgs;

})(fm.websync.baseCompleteArgs);


/*<span id='cls-fm.icelink.websync.joinConferenceFailureArgs'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.joinConferenceFailureArgs
 <div>
 Arguments for join-conference failure callbacks.
 </div>

@extends fm.websync.baseFailureArgs
*/


fm.icelink.websync.joinConferenceFailureArgs = (function(_super) {

  __extends(joinConferenceFailureArgs, _super);

  joinConferenceFailureArgs.prototype.__conferenceChannel = null;

  joinConferenceFailureArgs.prototype.__isRejoin = false;

  function joinConferenceFailureArgs() {
    this.getIsRejoin = __bind(this.getIsRejoin, this);

    this.getConferenceChannel = __bind(this.getConferenceChannel, this);
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      joinConferenceFailureArgs.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    joinConferenceFailureArgs.__super__.constructor.call(this);
  }

  /*<span id='method-fm.icelink.websync.joinConferenceFailureArgs-getConferenceChannel'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the ID of the conference that failed to be joined.
  	 </div>
  
  	@function getConferenceChannel
  	@return {String}
  */


  joinConferenceFailureArgs.prototype.getConferenceChannel = function() {
    return this.__conferenceChannel;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceFailureArgs-getIsRejoin'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets whether the join call was automatically
  	 invoked following a stream failure.
  	 </div>
  
  	@function getIsRejoin
  	@return {Boolean}
  */


  joinConferenceFailureArgs.prototype.getIsRejoin = function() {
    return this.__isRejoin;
  };

  return joinConferenceFailureArgs;

})(fm.websync.baseFailureArgs);


/*<span id='cls-fm.icelink.websync.joinConferenceSuccessArgs'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.joinConferenceSuccessArgs
 <div>
 Arguments for join-conference success callbacks.
 </div>

@extends fm.websync.baseSuccessArgs
*/


fm.icelink.websync.joinConferenceSuccessArgs = (function(_super) {

  __extends(joinConferenceSuccessArgs, _super);

  joinConferenceSuccessArgs.prototype.__conferenceChannel = null;

  joinConferenceSuccessArgs.prototype.__isRejoin = false;

  function joinConferenceSuccessArgs() {
    this.getIsRejoin = __bind(this.getIsRejoin, this);

    this.getConferenceChannel = __bind(this.getConferenceChannel, this);
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      joinConferenceSuccessArgs.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    joinConferenceSuccessArgs.__super__.constructor.call(this);
  }

  /*<span id='method-fm.icelink.websync.joinConferenceSuccessArgs-getConferenceChannel'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the ID of the conference that was joined.
  	 </div>
  
  	@function getConferenceChannel
  	@return {String}
  */


  joinConferenceSuccessArgs.prototype.getConferenceChannel = function() {
    return this.__conferenceChannel;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceSuccessArgs-getIsRejoin'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets whether the join call was automatically
  	 invoked following a stream failure.
  	 </div>
  
  	@function getIsRejoin
  	@return {Boolean}
  */


  joinConferenceSuccessArgs.prototype.getIsRejoin = function() {
    return this.__isRejoin;
  };

  return joinConferenceSuccessArgs;

})(fm.websync.baseSuccessArgs);


/*<span id='cls-fm.icelink.websync.leaveConferenceCompleteArgs'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.leaveConferenceCompleteArgs
 <div>
 Arguments for leave-conference complete callbacks.
 </div>

@extends fm.websync.baseCompleteArgs
*/


fm.icelink.websync.leaveConferenceCompleteArgs = (function(_super) {

  __extends(leaveConferenceCompleteArgs, _super);

  function leaveConferenceCompleteArgs() {
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      leaveConferenceCompleteArgs.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    leaveConferenceCompleteArgs.__super__.constructor.call(this);
  }

  return leaveConferenceCompleteArgs;

})(fm.websync.baseCompleteArgs);


/*<span id='cls-fm.icelink.websync.leaveConferenceArgs'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.leaveConferenceArgs
 <div>
 Arguments for a client leaving an IceLink conference.
 </div>

@extends fm.websync.baseInputArgs
*/


fm.icelink.websync.leaveConferenceArgs = (function(_super) {

  __extends(leaveConferenceArgs, _super);

  leaveConferenceArgs.prototype._conferenceChannel = null;

  leaveConferenceArgs.prototype._onComplete = null;

  leaveConferenceArgs.prototype._onFailure = null;

  leaveConferenceArgs.prototype._onSuccess = null;

  /*<span id='method-fm.icelink.websync.leaveConferenceArgs-fm.icelink.websync.leaveConferenceArgs'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Initializes a new instance of the <see cref="fm.icelink.websync.leaveConferenceArgs">fm.icelink.websync.leaveConferenceArgs</see> class.
  	 </div>
  	@function fm.icelink.websync.leaveConferenceArgs
  	@param {String} conferenceChannel The conference ID.
  	@return {}
  */


  function leaveConferenceArgs() {
    this.setOnSuccess = __bind(this.setOnSuccess, this);

    this.setOnFailure = __bind(this.setOnFailure, this);

    this.setOnComplete = __bind(this.setOnComplete, this);

    this.setConferenceChannel = __bind(this.setConferenceChannel, this);

    this.getOnSuccess = __bind(this.getOnSuccess, this);

    this.getOnFailure = __bind(this.getOnFailure, this);

    this.getOnComplete = __bind(this.getOnComplete, this);

    this.getConferenceChannel = __bind(this.getConferenceChannel, this);

    var conferenceChannel;
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      leaveConferenceArgs.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    conferenceChannel = arguments[0];
    leaveConferenceArgs.__super__.constructor.call(this);
    this.setConferenceChannel(conferenceChannel);
  }

  /*<span id='method-fm.icelink.websync.leaveConferenceArgs-getConferenceChannel'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the conference ID.
  	 </div>
  
  	@function getConferenceChannel
  	@return {String}
  */


  leaveConferenceArgs.prototype.getConferenceChannel = function() {
    return this._conferenceChannel;
  };

  /*<span id='method-fm.icelink.websync.leaveConferenceArgs-getOnComplete'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the callback to invoke after <see cref="fm.icelink.websync.leaveConferenceArgs.onSuccess">fm.icelink.websync.leaveConferenceArgs.onSuccess</see> or <see cref="fm.icelink.websync.leaveConferenceArgs.onFailure">fm.icelink.websync.leaveConferenceArgs.onFailure</see>.
  	 </div>
  
  	@function getOnComplete
  	@return {fm.singleAction}
  */


  leaveConferenceArgs.prototype.getOnComplete = function() {
    return this._onComplete;
  };

  /*<span id='method-fm.icelink.websync.leaveConferenceArgs-getOnFailure'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the callback to invoke if the request fails.
  	 </div>
  
  	@function getOnFailure
  	@return {fm.singleAction}
  */


  leaveConferenceArgs.prototype.getOnFailure = function() {
    return this._onFailure;
  };

  /*<span id='method-fm.icelink.websync.leaveConferenceArgs-getOnSuccess'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the callback to invoke if the request succeeds.
  	 </div>
  
  	@function getOnSuccess
  	@return {fm.singleAction}
  */


  leaveConferenceArgs.prototype.getOnSuccess = function() {
    return this._onSuccess;
  };

  /*<span id='method-fm.icelink.websync.leaveConferenceArgs-setConferenceChannel'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the conference ID.
  	 </div>
  
  	@function setConferenceChannel
  	@param {String} value
  	@return {void}
  */


  leaveConferenceArgs.prototype.setConferenceChannel = function() {
    var value;
    value = arguments[0];
    return this._conferenceChannel = value;
  };

  /*<span id='method-fm.icelink.websync.leaveConferenceArgs-setOnComplete'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the callback to invoke after <see cref="fm.icelink.websync.leaveConferenceArgs.onSuccess">fm.icelink.websync.leaveConferenceArgs.onSuccess</see> or <see cref="fm.icelink.websync.leaveConferenceArgs.onFailure">fm.icelink.websync.leaveConferenceArgs.onFailure</see>.
  	 </div>
  
  	@function setOnComplete
  	@param {fm.singleAction} value
  	@return {void}
  */


  leaveConferenceArgs.prototype.setOnComplete = function() {
    var value;
    value = arguments[0];
    return this._onComplete = value;
  };

  /*<span id='method-fm.icelink.websync.leaveConferenceArgs-setOnFailure'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the callback to invoke if the request fails.
  	 </div>
  
  	@function setOnFailure
  	@param {fm.singleAction} value
  	@return {void}
  */


  leaveConferenceArgs.prototype.setOnFailure = function() {
    var value;
    value = arguments[0];
    return this._onFailure = value;
  };

  /*<span id='method-fm.icelink.websync.leaveConferenceArgs-setOnSuccess'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the callback to invoke if the request succeeds.
  	 </div>
  
  	@function setOnSuccess
  	@param {fm.singleAction} value
  	@return {void}
  */


  leaveConferenceArgs.prototype.setOnSuccess = function() {
    var value;
    value = arguments[0];
    return this._onSuccess = value;
  };

  return leaveConferenceArgs;

})(fm.websync.baseInputArgs);


/*<span id='cls-fm.icelink.websync.joinConferenceArgs'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.joinConferenceArgs
 <div>
 Arguments for a client joining an IceLink conference.
 </div>

@extends fm.websync.baseInputArgs
*/


fm.icelink.websync.joinConferenceArgs = (function(_super) {

  __extends(joinConferenceArgs, _super);

  joinConferenceArgs.prototype._conference = null;

  joinConferenceArgs.prototype._conferenceChannel = null;

  joinConferenceArgs.prototype._onComplete = null;

  joinConferenceArgs.prototype._onFailure = null;

  joinConferenceArgs.prototype._onSuccess = null;

  joinConferenceArgs.prototype._rejoin = false;

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-fm.icelink.websync.joinConferenceArgs'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Initializes a new instance of the <see cref="fm.icelink.websync.joinConferenceArgs">fm.icelink.websync.joinConferenceArgs</see> class.
  	 </div>
  	@function fm.icelink.websync.joinConferenceArgs
  	@param {String} conferenceChannel The conference channel.
  	@param {fm.icelink.conference} conference The conference
  	@return {}
  */


  function joinConferenceArgs() {
    this.setRejoin = __bind(this.setRejoin, this);

    this.setOnSuccess = __bind(this.setOnSuccess, this);

    this.setOnFailure = __bind(this.setOnFailure, this);

    this.setOnComplete = __bind(this.setOnComplete, this);

    this.setConferenceChannel = __bind(this.setConferenceChannel, this);

    this.setConference = __bind(this.setConference, this);

    this.getRejoin = __bind(this.getRejoin, this);

    this.getOnSuccess = __bind(this.getOnSuccess, this);

    this.getOnFailure = __bind(this.getOnFailure, this);

    this.getOnComplete = __bind(this.getOnComplete, this);

    this.getConferenceChannel = __bind(this.getConferenceChannel, this);

    this.getConference = __bind(this.getConference, this);

    var conference, conferenceChannel;
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      joinConferenceArgs.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    conferenceChannel = arguments[0];
    conference = arguments[1];
    joinConferenceArgs.__super__.constructor.call(this);
    this.setConferenceChannel(conferenceChannel);
    this.setConference(conference);
  }

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-getConference'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the conference.
  	 </div>
  
  	@function getConference
  	@return {fm.icelink.conference}
  */


  joinConferenceArgs.prototype.getConference = function() {
    return this._conference;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-getConferenceChannel'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the conference channel.
  	 </div>
  
  	@function getConferenceChannel
  	@return {String}
  */


  joinConferenceArgs.prototype.getConferenceChannel = function() {
    return this._conferenceChannel;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-getOnComplete'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the callback to invoke after <see cref="fm.icelink.websync.joinConferenceArgs.onSuccess">fm.icelink.websync.joinConferenceArgs.onSuccess</see> or <see cref="fm.icelink.websync.joinConferenceArgs.onFailure">fm.icelink.websync.joinConferenceArgs.onFailure</see>.
  	 </div>
  
  	@function getOnComplete
  	@return {fm.singleAction}
  */


  joinConferenceArgs.prototype.getOnComplete = function() {
    return this._onComplete;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-getOnFailure'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the callback to invoke if the request fails.
  	 </div>
  
  	@function getOnFailure
  	@return {fm.singleAction}
  */


  joinConferenceArgs.prototype.getOnFailure = function() {
    return this._onFailure;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-getOnSuccess'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the callback to invoke if the request succeeds.
  	 </div>
  
  	@function getOnSuccess
  	@return {fm.singleAction}
  */


  joinConferenceArgs.prototype.getOnSuccess = function() {
    return this._onSuccess;
  };

  joinConferenceArgs.prototype.getRejoin = function() {
    return this._rejoin;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-setConference'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the conference.
  	 </div>
  
  	@function setConference
  	@param {fm.icelink.conference} value
  	@return {void}
  */


  joinConferenceArgs.prototype.setConference = function() {
    var value;
    value = arguments[0];
    return this._conference = value;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-setConferenceChannel'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the conference channel.
  	 </div>
  
  	@function setConferenceChannel
  	@param {String} value
  	@return {void}
  */


  joinConferenceArgs.prototype.setConferenceChannel = function() {
    var value;
    value = arguments[0];
    return this._conferenceChannel = value;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-setOnComplete'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the callback to invoke after <see cref="fm.icelink.websync.joinConferenceArgs.onSuccess">fm.icelink.websync.joinConferenceArgs.onSuccess</see> or <see cref="fm.icelink.websync.joinConferenceArgs.onFailure">fm.icelink.websync.joinConferenceArgs.onFailure</see>.
  	 </div>
  
  	@function setOnComplete
  	@param {fm.singleAction} value
  	@return {void}
  */


  joinConferenceArgs.prototype.setOnComplete = function() {
    var value;
    value = arguments[0];
    return this._onComplete = value;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-setOnFailure'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the callback to invoke if the request fails.
  	 </div>
  
  	@function setOnFailure
  	@param {fm.singleAction} value
  	@return {void}
  */


  joinConferenceArgs.prototype.setOnFailure = function() {
    var value;
    value = arguments[0];
    return this._onFailure = value;
  };

  /*<span id='method-fm.icelink.websync.joinConferenceArgs-setOnSuccess'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the callback to invoke if the request succeeds.
  	 </div>
  
  	@function setOnSuccess
  	@param {fm.singleAction} value
  	@return {void}
  */


  joinConferenceArgs.prototype.setOnSuccess = function() {
    var value;
    value = arguments[0];
    return this._onSuccess = value;
  };

  joinConferenceArgs.prototype.setRejoin = function() {
    var value;
    value = arguments[0];
    return this._rejoin = value;
  };

  return joinConferenceArgs;

})(fm.websync.baseInputArgs);


/*<span id='cls-fm.icelink.websync.leaveConferenceFailureArgs'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.leaveConferenceFailureArgs
 <div>
 Arguments for leave-conference failure callbacks.
 </div>

@extends fm.websync.baseFailureArgs
*/


fm.icelink.websync.leaveConferenceFailureArgs = (function(_super) {

  __extends(leaveConferenceFailureArgs, _super);

  leaveConferenceFailureArgs.prototype.__conferenceChannel = null;

  function leaveConferenceFailureArgs() {
    this.getConferenceChannel = __bind(this.getConferenceChannel, this);
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      leaveConferenceFailureArgs.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    leaveConferenceFailureArgs.__super__.constructor.call(this);
  }

  /*<span id='method-fm.icelink.websync.leaveConferenceFailureArgs-getConferenceChannel'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the ID of the conference that failed to be left.
  	 </div>
  
  	@function getConferenceChannel
  	@return {String}
  */


  leaveConferenceFailureArgs.prototype.getConferenceChannel = function() {
    return this.__conferenceChannel;
  };

  return leaveConferenceFailureArgs;

})(fm.websync.baseFailureArgs);


/*<span id='cls-fm.icelink.websync.leaveConferenceSuccessArgs'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.leaveConferenceSuccessArgs
 <div>
 Arguments for leave-conference success callbacks.
 </div>

@extends fm.websync.baseSuccessArgs
*/


fm.icelink.websync.leaveConferenceSuccessArgs = (function(_super) {

  __extends(leaveConferenceSuccessArgs, _super);

  leaveConferenceSuccessArgs.prototype.__conferenceChannel = null;

  function leaveConferenceSuccessArgs() {
    this.getChannel = __bind(this.getChannel, this);
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      leaveConferenceSuccessArgs.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    leaveConferenceSuccessArgs.__super__.constructor.call(this);
  }

  /*<span id='method-fm.icelink.websync.leaveConferenceSuccessArgs-getChannel'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the ID of the conference that was left.
  	 </div>
  
  	@function getChannel
  	@return {String}
  */


  leaveConferenceSuccessArgs.prototype.getChannel = function() {
    return this.__conferenceChannel;
  };

  return leaveConferenceSuccessArgs;

})(fm.websync.baseSuccessArgs);


/*<span id='cls-fm.icelink.websync.clientExtensions'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.clientExtensions
 <div>
 Extension methods for <see cref="fm.websync.client">fm.websync.client</see> instances.
 </div>
*/

fm.icelink.websync.clientExtensions = (function() {

  clientExtensions._argsKey = null;

  clientExtensions._candidateTag = null;

  clientExtensions._clientKey = null;

  clientExtensions._conferenceChannelKey = null;

  clientExtensions._instanceChannelPrefix = null;

  clientExtensions._joinStateKey = null;

  clientExtensions._leaveStateKey = null;

  clientExtensions._offerAnswerTag = null;

  clientExtensions._onLinkCandidateEvent = null;

  clientExtensions._onLinkOfferAnswerEvent = null;

  clientExtensions._unsubscribeCleanupEvent = null;

  function clientExtensions() {
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      clientExtensions.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
  }

  clientExtensions.getClient = function() {
    var conference;
    conference = arguments[0];
    return conference.getDynamicValue(fm.icelink.websync.clientExtensions._clientKey);
  };

  clientExtensions.getConference = function() {
    var client, conferenceChannel;
    client = arguments[0];
    conferenceChannel = arguments[1];
    return fm.global.tryCast(client.getDynamicValue(fm.icelink.websync.clientExtensions.getConferenceKey(conferenceChannel)), fm.icelink.conference);
  };

  clientExtensions.getConferenceChannel = function() {
    var conference;
    conference = arguments[0];
    return conference.getDynamicValue(fm.icelink.websync.clientExtensions._conferenceChannelKey);
  };

  clientExtensions.getConferenceKey = function() {
    var conferenceChannel;
    conferenceChannel = arguments[0];
    return fm.stringExtensions.concat("fm.icelink.websync.conference", conferenceChannel);
  };

  clientExtensions.getInstanceChannel = function() {
    var conferenceChannel, instanceId;
    conferenceChannel = arguments[0];
    instanceId = arguments[1];
    return fm.stringExtensions.concat(fm.icelink.websync.clientExtensions._instanceChannelPrefix, conferenceChannel, "/", instanceId);
  };

  clientExtensions.getInstanceChannelFromArray = function() {
    var channels, str, _i, _len, _var0;
    channels = arguments[0];
    _var0 = channels;
    for (_i = 0, _len = _var0.length; _i < _len; _i++) {
      str = _var0[_i];
      if (fm.stringExtensions.startsWith(str, fm.icelink.websync.clientExtensions._instanceChannelPrefix)) {
        return str;
      }
    }
    return null;
  };

  clientExtensions.getInstanceIdRecordKey = function() {
    var conferenceChannel;
    conferenceChannel = arguments[0];
    return fm.stringExtensions.concat("fm.icelink.websync.instance", conferenceChannel);
  };

  /*<span id='method-fm.icelink.websync.clientExtensions-joinConference'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Joins an IceLink conference.
  	 </div>
  	@function joinConference
  	@param {fm.websync.client} client The WebSync client.
  	@param {fm.icelink.websync.joinConferenceArgs} joinArgs The arguments.
  	@return {fm.websync.client} The WebSync client.
  */


  clientExtensions.joinConference = function() {
    var args, args3, args4, bindArgs, client, joinArgs, str, _var0;
    client = arguments[0];
    joinArgs = arguments[1];
    if (fm.stringExtensions.isNullOrEmpty(joinArgs.getConferenceChannel())) {
      throw new Error("Conference channel cannot be null.");
    }
    _var0 = joinArgs.getConference();
    if (_var0 === null || typeof _var0 === 'undefined') {
      throw new Error("Conference cannot be null.");
    }
    str = client.getInstanceId().toString();
    joinArgs.setDynamicValue(fm.icelink.websync.clientExtensions._joinStateKey, new fm.icelink.websync.joinState());
    client.startBatch();
    args3 = new fm.websync.bindArgs([new fm.websync.record(fm.icelink.websync.clientExtensions.getInstanceIdRecordKey(joinArgs.getConferenceChannel()), fm.serializer.serializeString(str))]);
    args3.setRequestUrl(joinArgs.getRequestUrl());
    args3.setSynchronous(joinArgs.getSynchronous());
    args3.setOnSuccess(clientExtensions.onBindSuccess);
    args3.setOnFailure(clientExtensions.onBindFailure);
    args3.setDynamicProperties(joinArgs.getDynamicProperties());
    bindArgs = args3;
    bindArgs.setDynamicValue(fm.icelink.websync.clientExtensions._argsKey, joinArgs);
    bindArgs.copyExtensions(joinArgs);
    client.bind(bindArgs);
    args4 = new fm.websync.subscribeArgs([joinArgs.getConferenceChannel(), fm.icelink.websync.clientExtensions.getInstanceChannel(joinArgs.getConferenceChannel(), str)]);
    args4.setRequestUrl(joinArgs.getRequestUrl());
    args4.setSynchronous(joinArgs.getSynchronous());
    args4.setOnSuccess(clientExtensions.onSubscribeSuccess);
    args4.setOnFailure(clientExtensions.onSubscribeFailure);
    args4.setOnReceive(clientExtensions.onSubscribeReceive);
    args4.setDynamicProperties(joinArgs.getDynamicProperties());
    args = args4;
    fm.websync.subscribers.subscribeArgsExtensions.setOnClientSubscribe(args, clientExtensions.onClientSubscribe);
    fm.websync.subscribers.subscribeArgsExtensions.setOnClientUnsubscribe(args, clientExtensions.onClientUnsubscribe);
    args.setDynamicValue(fm.icelink.websync.clientExtensions._argsKey, joinArgs);
    args.copyExtensions(joinArgs);
    client.subscribe(args);
    client.endBatch();
    return client;
  };

  /*<span id='method-fm.icelink.websync.clientExtensions-leaveConference'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Leaves an IceLink conference.
  	 </div>
  	@function leaveConference
  	@param {fm.websync.client} client The WebSync client.
  	@param {fm.icelink.websync.leaveConferenceArgs} leaveArgs The arguments.
  	@return {fm.websync.client} The WebSync client.
  */


  clientExtensions.leaveConference = function() {
    var args3, args4, client, instanceId, leaveArgs, unbindArgs, unsubscribeArgs;
    client = arguments[0];
    leaveArgs = arguments[1];
    if (fm.stringExtensions.isNullOrEmpty(leaveArgs.getConferenceChannel())) {
      throw new Error("conferenceChannel cannot be null.");
    }
    instanceId = client.getInstanceId().toString();
    leaveArgs.setDynamicValue(fm.icelink.websync.clientExtensions._leaveStateKey, new fm.icelink.websync.leaveState());
    client.startBatch();
    args3 = new fm.websync.unsubscribeArgs([leaveArgs.getConferenceChannel(), fm.icelink.websync.clientExtensions.getInstanceChannel(leaveArgs.getConferenceChannel(), instanceId)]);
    args3.setRequestUrl(leaveArgs.getRequestUrl());
    args3.setSynchronous(leaveArgs.getSynchronous());
    args3.setOnSuccess(clientExtensions.onUnsubscribeSuccess);
    args3.setOnFailure(clientExtensions.onUnsubscribeFailure);
    args3.setDynamicProperties(leaveArgs.getDynamicProperties());
    unsubscribeArgs = args3;
    unsubscribeArgs.setDynamicValue(fm.icelink.websync.clientExtensions._argsKey, leaveArgs);
    unsubscribeArgs.copyExtensions(leaveArgs);
    client.unsubscribe(unsubscribeArgs);
    args4 = new fm.websync.unbindArgs([new fm.websync.record(fm.icelink.websync.clientExtensions.getInstanceIdRecordKey(leaveArgs.getConferenceChannel()))]);
    args4.setRequestUrl(leaveArgs.getRequestUrl());
    args4.setSynchronous(leaveArgs.getSynchronous());
    args4.setOnSuccess(clientExtensions.onUnbindSuccess);
    args4.setOnFailure(clientExtensions.onUnbindFailure);
    args4.setDynamicProperties(leaveArgs.getDynamicProperties());
    unbindArgs = args4;
    unbindArgs.setDynamicValue(fm.icelink.websync.clientExtensions._argsKey, leaveArgs);
    unbindArgs.copyExtensions(leaveArgs);
    client.unbind(unbindArgs);
    client.endBatch();
    return client;
  };

  clientExtensions.onBindFailure = function() {
    var dynamicValue, e;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    return dynamicValue.getDynamicValue(fm.icelink.websync.clientExtensions._joinStateKey).updateBindFailure(e);
  };

  clientExtensions.onBindSuccess = function() {
    var dynamicValue, e;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    return dynamicValue.getDynamicValue(fm.icelink.websync.clientExtensions._joinStateKey).updateBindSuccess(e);
  };

  clientExtensions.onClientSubscribe = function() {
    var conference, dynamicValue, e, _var0;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    conference = fm.icelink.websync.clientExtensions.getConference(e.getClient(), dynamicValue.getConferenceChannel());
    _var0 = conference;
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      return conference.link(e.getSubscribedClient().getClientId().toString(), e.getSubscribedClient().getBoundRecords());
    }
  };

  clientExtensions.onClientUnsubscribe = function() {
    var conference, dynamicValue, e, _var0;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    conference = fm.icelink.websync.clientExtensions.getConference(e.getClient(), dynamicValue.getConferenceChannel());
    _var0 = conference;
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      return conference.unlink(e.getUnsubscribedClient().getClientId().toString(), "Remote client unsubscribed.");
    }
  };

  clientExtensions.onLinkCandidate = function() {
    var client, conferenceChannel, e, instanceId, record, _var0, _var1, _var2;
    e = arguments[0];
    client = fm.icelink.websync.clientExtensions.getClient(e.getConference());
    _var0 = client;
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      conferenceChannel = fm.icelink.websync.clientExtensions.getConferenceChannel(e.getConference());
      _var1 = conferenceChannel;
      if (_var1 !== null && typeof _var1 !== 'undefined') {
        record = fm.icelink.websync.baseLinkArgsExtensions.getPeerClient(e).getBoundRecords()[fm.icelink.websync.clientExtensions.getInstanceIdRecordKey(conferenceChannel)];
        _var2 = record;
        if (_var2 !== null && typeof _var2 !== 'undefined') {
          instanceId = fm.serializer.deserializeString(record.getValueJson());
          return client.publish(new fm.websync.publishArgs(fm.icelink.websync.clientExtensions.getInstanceChannel(conferenceChannel, instanceId), e.getCandidate().toJson(), fm.icelink.websync.clientExtensions._candidateTag));
        }
      }
    }
  };

  clientExtensions.onLinkOfferAnswer = function() {
    var client, conferenceChannel, e, instanceId, record, _var0, _var1, _var2;
    e = arguments[0];
    client = fm.icelink.websync.clientExtensions.getClient(e.getConference());
    _var0 = client;
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      conferenceChannel = fm.icelink.websync.clientExtensions.getConferenceChannel(e.getConference());
      _var1 = conferenceChannel;
      if (_var1 !== null && typeof _var1 !== 'undefined') {
        record = fm.icelink.websync.baseLinkArgsExtensions.getPeerClient(e).getBoundRecords()[fm.icelink.websync.clientExtensions.getInstanceIdRecordKey(conferenceChannel)];
        _var2 = record;
        if (_var2 !== null && typeof _var2 !== 'undefined') {
          instanceId = fm.serializer.deserializeString(record.getValueJson());
          return client.publish(new fm.websync.publishArgs(fm.icelink.websync.clientExtensions.getInstanceChannel(conferenceChannel, instanceId), e.getOfferAnswer().toJson(), fm.icelink.websync.clientExtensions._offerAnswerTag));
        }
      }
    }
  };

  clientExtensions.onSubscribeFailure = function() {
    var dynamicValue, e, state;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    state = dynamicValue.getDynamicValue(fm.icelink.websync.clientExtensions._joinStateKey);
    state.updateSubscribeFailure(e);
    if (state.getBindSuccess()) {
      throw new Error("Join conference failed (bind succeeded, subscribe failed).");
    }
    return fm.icelink.websync.clientExtensions.raiseJoinFailure(dynamicValue, e, e.getIsResubscribe());
  };

  clientExtensions.onSubscribeReceive = function() {
    var conference, dynamicValue, e, _var0;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    conference = fm.icelink.websync.clientExtensions.getConference(e.getClient(), dynamicValue.getConferenceChannel());
    _var0 = conference;
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      if (e.getTag() === fm.icelink.websync.clientExtensions._offerAnswerTag) {
        return conference.receiveOfferAnswer(fm.icelink.offerAnswer.fromJson(e.getDataJson()), e.getPublishingClient().getClientId().toString(), e.getPublishingClient().getBoundRecords());
      } else {
        if (e.getTag() === fm.icelink.websync.clientExtensions._candidateTag) {
          return conference.receiveCandidate(fm.icelink.candidate.fromJson(e.getDataJson()), e.getPublishingClient().getClientId().toString());
        }
      }
    }
  };

  clientExtensions.onSubscribeSuccess = function() {
    var dynamicValue, e, state;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    state = dynamicValue.getDynamicValue(fm.icelink.websync.clientExtensions._joinStateKey);
    state.updateSubscribeSuccess(e);
    fm.icelink.websync.clientExtensions.setConferenceChannel(dynamicValue.getConference(), dynamicValue.getConferenceChannel());
    fm.icelink.websync.clientExtensions.setClient(dynamicValue.getConference(), e.getClient());
    fm.icelink.websync.clientExtensions.setConference(e.getClient(), dynamicValue.getConferenceChannel(), dynamicValue.getConference());
    if (!state.getBindSuccess()) {
      throw new Error("Join conference failed (subscribe succeeded, bind failed).");
    }
    return fm.icelink.websync.clientExtensions.raiseJoinSuccess(dynamicValue, e);
  };

  clientExtensions.onUnbindFailure = function() {
    var dynamicValue, e, state;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    state = dynamicValue.getDynamicValue(fm.icelink.websync.clientExtensions._leaveStateKey);
    state.updateUnbindFailure(e);
    if (state.getUnsubscribeSuccess()) {
      throw new Error("Leave failed (unsubscribe succeeded, unbind failed).");
    }
    return fm.icelink.websync.clientExtensions.raiseLeaveFailure(dynamicValue, e);
  };

  clientExtensions.onUnbindSuccess = function() {
    var dynamicValue, e, state;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    state = dynamicValue.getDynamicValue(fm.icelink.websync.clientExtensions._leaveStateKey);
    state.updateUnbindSuccess(e);
    if (!state.getUnsubscribeSuccess()) {
      throw new Error("Leave failed (unbind succeeded, unsubscribe failed).");
    }
    return fm.icelink.websync.clientExtensions.raiseLeaveSuccess(dynamicValue, e);
  };

  clientExtensions.onUnsubscribeFailure = function() {
    var dynamicValue, e;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    return dynamicValue.getDynamicValue(fm.icelink.websync.clientExtensions._leaveStateKey).updateUnsubscribeFailure(e);
  };

  clientExtensions.onUnsubscribeSuccess = function() {
    var dynamicValue, e;
    e = arguments[0];
    dynamicValue = e.getDynamicValue(fm.icelink.websync.clientExtensions._argsKey);
    return dynamicValue.getDynamicValue(fm.icelink.websync.clientExtensions._leaveStateKey).updateUnsubscribeSuccess(e);
  };

  clientExtensions.raiseJoinFailure = function() {
    var args2, args3, args4, e, exception, isRejoin, joinArgs, onComplete, onFailure, p, retry, _var0, _var1;
    joinArgs = arguments[0];
    e = arguments[1];
    isRejoin = arguments[2];
    retry = false;
    onFailure = joinArgs.getOnFailure();
    _var0 = onFailure;
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      args2 = new fm.icelink.websync.joinConferenceFailureArgs();
      args2.__conferenceChannel = joinArgs.getConferenceChannel();
      args2.__isRejoin = isRejoin;
      args2.setRetry(e.getRetry());
      args2.setClient(e.getClient());
      args2.setException(e.getException());
      args2.setTimestamp(e.getTimestamp());
      args2.setDynamicProperties(joinArgs.getDynamicProperties());
      p = args2;
      p.copyExtensions(e);
      try {
        onFailure(p);
      } catch (exception1) {
        exception = exception1;
        if (!e.getClient().raiseUnhandledException(exception)) {
          fm.asyncException.asyncThrow(exception, "Client -> Join -> OnFailure");
        }
      } finally {

      }
      retry = p.getRetry();
    }
    onComplete = joinArgs.getOnComplete();
    _var1 = onComplete;
    if (_var1 !== null && typeof _var1 !== 'undefined') {
      args4 = new fm.icelink.websync.joinConferenceCompleteArgs();
      args4.__isRejoin = isRejoin;
      args4.setClient(e.getClient());
      args4.setTimestamp(e.getTimestamp());
      args4.setDynamicProperties(joinArgs.getDynamicProperties());
      args3 = args4;
      args3.copyExtensions(e);
      try {
        onComplete(args3);
      } catch (exception2) {
        exception = exception2;
        if (!e.getClient().raiseUnhandledException(exception)) {
          fm.asyncException.asyncThrow(exception, "Client -> Join -> OnComplete");
        }
      } finally {

      }
    }
    if (retry) {
      joinArgs.setIsRetry(true);
      return fm.icelink.websync.clientExtensions.joinConference(e.getClient(), joinArgs);
    }
  };

  clientExtensions.raiseJoinSuccess = function() {
    var args2, args3, args4, e, exception, joinArgs, onComplete, onSuccess, p, _var0, _var1;
    joinArgs = arguments[0];
    e = arguments[1];
    joinArgs.getConference().addOnLinkCandidate(fm.icelink.websync.clientExtensions._onLinkCandidateEvent);
    joinArgs.getConference().addOnLinkOfferAnswer(fm.icelink.websync.clientExtensions._onLinkOfferAnswerEvent);
    e.getClient().addOnUnsubscribeSuccess(fm.icelink.websync.clientExtensions._unsubscribeCleanupEvent);
    onSuccess = joinArgs.getOnSuccess();
    _var0 = onSuccess;
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      args2 = new fm.icelink.websync.joinConferenceSuccessArgs();
      args2.__conferenceChannel = joinArgs.getConferenceChannel();
      args2.__isRejoin = e.getIsResubscribe();
      args2.setClient(e.getClient());
      args2.setTimestamp(e.getTimestamp());
      args2.setDynamicProperties(e.getDynamicProperties());
      p = args2;
      p.copyExtensions(e);
      try {
        onSuccess(p);
      } catch (exception1) {
        exception = exception1;
        if (!e.getClient().raiseUnhandledException(exception)) {
          fm.asyncException.asyncThrow(exception, "Client -> Join -> OnSuccess");
        }
      } finally {

      }
    }
    onComplete = joinArgs.getOnComplete();
    _var1 = onComplete;
    if (_var1 !== null && typeof _var1 !== 'undefined') {
      args4 = new fm.icelink.websync.joinConferenceCompleteArgs();
      args4.__isRejoin = e.getIsResubscribe();
      args4.setClient(e.getClient());
      args4.setTimestamp(e.getTimestamp());
      args4.setDynamicProperties(joinArgs.getDynamicProperties());
      args3 = args4;
      args3.copyExtensions(e);
      try {
        return onComplete(args3);
      } catch (exception2) {
        exception = exception2;
        if (!e.getClient().raiseUnhandledException(exception)) {
          return fm.asyncException.asyncThrow(exception, "Client -> Join -> OnComplete");
        }
      } finally {

      }
    }
  };

  clientExtensions.raiseLeaveFailure = function() {
    var args2, args3, args4, e, exception, leaveArgs, onComplete, onFailure, p, retry, _var0, _var1;
    leaveArgs = arguments[0];
    e = arguments[1];
    retry = false;
    onFailure = leaveArgs.getOnFailure();
    _var0 = onFailure;
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      args2 = new fm.icelink.websync.leaveConferenceFailureArgs();
      args2.__conferenceChannel = leaveArgs.getConferenceChannel();
      args2.setRetry(e.getRetry());
      args2.setClient(e.getClient());
      args2.setException(e.getException());
      args2.setTimestamp(e.getTimestamp());
      args2.setDynamicProperties(leaveArgs.getDynamicProperties());
      p = args2;
      p.copyExtensions(e);
      try {
        onFailure(p);
      } catch (exception1) {
        exception = exception1;
        if (!e.getClient().raiseUnhandledException(exception)) {
          fm.asyncException.asyncThrow(exception, "Client -> Leave -> OnFailure");
        }
      } finally {

      }
      retry = p.getRetry();
    }
    onComplete = leaveArgs.getOnComplete();
    _var1 = onComplete;
    if (_var1 !== null && typeof _var1 !== 'undefined') {
      args4 = new fm.icelink.websync.leaveConferenceCompleteArgs();
      args4.setClient(e.getClient());
      args4.setTimestamp(e.getTimestamp());
      args4.setDynamicProperties(leaveArgs.getDynamicProperties());
      args3 = args4;
      args3.copyExtensions(e);
      try {
        onComplete(args3);
      } catch (exception2) {
        exception = exception2;
        if (!e.getClient().raiseUnhandledException(exception)) {
          fm.asyncException.asyncThrow(exception, "Client -> Leave -> OnComplete");
        }
      } finally {

      }
    }
    if (retry) {
      leaveArgs.setIsRetry(true);
      return fm.icelink.websync.clientExtensions.leaveConference(e.getClient(), leaveArgs);
    }
  };

  clientExtensions.raiseLeaveSuccess = function() {
    var args2, args3, args4, e, exception, leaveArgs, onComplete, onSuccess, p, _var0, _var1;
    leaveArgs = arguments[0];
    e = arguments[1];
    onSuccess = leaveArgs.getOnSuccess();
    _var0 = onSuccess;
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      args2 = new fm.icelink.websync.leaveConferenceSuccessArgs();
      args2.__conferenceChannel = leaveArgs.getConferenceChannel();
      args2.setClient(e.getClient());
      args2.setTimestamp(e.getTimestamp());
      args2.setDynamicProperties(leaveArgs.getDynamicProperties());
      p = args2;
      p.copyExtensions(e);
      try {
        onSuccess(p);
      } catch (exception1) {
        exception = exception1;
        if (!e.getClient().raiseUnhandledException(exception)) {
          fm.asyncException.asyncThrow(exception, "Client -> Leave -> OnSuccess");
        }
      } finally {

      }
    }
    onComplete = leaveArgs.getOnComplete();
    _var1 = onComplete;
    if (_var1 !== null && typeof _var1 !== 'undefined') {
      args4 = new fm.icelink.websync.leaveConferenceCompleteArgs();
      args4.setClient(e.getClient());
      args4.setTimestamp(e.getTimestamp());
      args4.setDynamicProperties(leaveArgs.getDynamicProperties());
      args3 = args4;
      args3.copyExtensions(e);
      try {
        return onComplete(args3);
      } catch (exception2) {
        exception = exception2;
        if (!e.getClient().raiseUnhandledException(exception)) {
          return fm.asyncException.asyncThrow(exception, "Client -> Leave -> OnComplete");
        }
      } finally {

      }
    }
  };

  clientExtensions.setClient = function() {
    var client, conference;
    conference = arguments[0];
    client = arguments[1];
    return conference.setDynamicValue(fm.icelink.websync.clientExtensions._clientKey, client);
  };

  clientExtensions.setConference = function() {
    var client, conference, conferenceChannel;
    client = arguments[0];
    conferenceChannel = arguments[1];
    conference = arguments[2];
    return client.setDynamicValue(fm.icelink.websync.clientExtensions.getConferenceKey(conferenceChannel), conference);
  };

  clientExtensions.setConferenceChannel = function() {
    var conference, conferenceChannel;
    conference = arguments[0];
    conferenceChannel = arguments[1];
    return conference.setDynamicValue(fm.icelink.websync.clientExtensions._conferenceChannelKey, conferenceChannel);
  };

  clientExtensions.unsetClient = function() {
    var conference;
    conference = arguments[0];
    return conference.unsetDynamicValue(fm.icelink.websync.clientExtensions._clientKey);
  };

  clientExtensions.unsetConference = function() {
    var client, conferenceChannel;
    client = arguments[0];
    conferenceChannel = arguments[1];
    return client.unsetDynamicValue(fm.icelink.websync.clientExtensions.getConferenceKey(conferenceChannel));
  };

  clientExtensions.unsetConferenceChannel = function() {
    var conference;
    conference = arguments[0];
    return conference.unsetDynamicValue(fm.icelink.websync.clientExtensions._conferenceChannelKey);
  };

  clientExtensions.unsubscribeCleanup = function() {
    var conference, e, str, _i, _len, _results, _var0, _var1;
    e = arguments[0];
    _var0 = e.getChannels();
    _results = [];
    for (_i = 0, _len = _var0.length; _i < _len; _i++) {
      str = _var0[_i];
      conference = fm.icelink.websync.clientExtensions.getConference(e.getClient(), str);
      _var1 = conference;
      if (_var1 !== null && typeof _var1 !== 'undefined') {
        conference.removeOnLinkCandidate(fm.icelink.websync.clientExtensions._onLinkCandidateEvent);
        conference.removeOnLinkOfferAnswer(fm.icelink.websync.clientExtensions._onLinkOfferAnswerEvent);
        conference.unlinkAll("Local client unsubscribed.");
        fm.icelink.websync.clientExtensions.unsetConference(e.getClient(), str);
        fm.icelink.websync.clientExtensions.unsetClient(conference);
        fm.icelink.websync.clientExtensions.unsetConferenceChannel(conference);
        _results.push(e.getClient().removeOnUnsubscribeSuccess(fm.icelink.websync.clientExtensions._unsubscribeCleanupEvent));
      } else {
        _results.push(void 0);
      }
    }
    return _results;
  };

  fm.websync.client.prototype.getConference = function() {
    var conferenceChannel;
    conferenceChannel = arguments[0];
    Array.prototype.splice.call(arguments, 0, 0, this);
    return fm.icelink.websync.clientExtensions.getConference.apply(this, arguments);
  };

  /*<span id='method-fm.icelink.websync.clientExtensions-joinConference'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Joins an IceLink conference.
  	 </div>
  	@function joinConference
  	@param {fm.icelink.websync.joinConferenceArgs} joinArgs The arguments.
  	@return {fm.websync.client} The WebSync client.
  */


  fm.websync.client.prototype.joinConference = function() {
    var joinArgs;
    joinArgs = arguments[0];
    Array.prototype.splice.call(arguments, 0, 0, this);
    return fm.icelink.websync.clientExtensions.joinConference.apply(this, arguments);
  };

  /*<span id='method-fm.icelink.websync.clientExtensions-leaveConference'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Leaves an IceLink conference.
  	 </div>
  	@function leaveConference
  	@param {fm.icelink.websync.leaveConferenceArgs} leaveArgs The arguments.
  	@return {fm.websync.client} The WebSync client.
  */


  fm.websync.client.prototype.leaveConference = function() {
    var leaveArgs;
    leaveArgs = arguments[0];
    Array.prototype.splice.call(arguments, 0, 0, this);
    return fm.icelink.websync.clientExtensions.leaveConference.apply(this, arguments);
  };

  clientExtensions._joinStateKey = "fm.icelink.websync.joinState";

  clientExtensions._leaveStateKey = "fm.icelink.websync.leaveState";

  clientExtensions._argsKey = "fm.icelink.websync.args";

  clientExtensions._clientKey = "fm.icelink.websync.client";

  clientExtensions._conferenceChannelKey = "fm.icelink.websync.conferenceChannel";

  clientExtensions._instanceChannelPrefix = "/fm.icelink.websync.instance/";

  clientExtensions._offerAnswerTag = "fm.icelink.websync.offeranswer";

  clientExtensions._candidateTag = "fm.icelink.websync.candidate";

  clientExtensions._onLinkOfferAnswerEvent = clientExtensions.onLinkOfferAnswer;

  clientExtensions._onLinkCandidateEvent = clientExtensions.onLinkCandidate;

  clientExtensions._unsubscribeCleanupEvent = clientExtensions.unsubscribeCleanup;

  return clientExtensions;

}).call(this);


/*<span id='cls-fm.icelink.websync.baseLinkArgsExtensions'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.baseLinkArgsExtensions
 <div>
 Extension methods for <see cref="fm.icelink.baseLinkArgs">fm.icelink.baseLinkArgs</see> instances.
 </div>
*/

fm.icelink.websync.baseLinkArgsExtensions = (function() {

  function baseLinkArgsExtensions() {
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      baseLinkArgsExtensions.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
  }

  /*<span id='method-fm.icelink.websync.baseLinkArgsExtensions-getPeerClient'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the WebSync peer client.
  	 </div>
  	@function getPeerClient
  	@param {fm.icelink.baseLinkArgs} baseLinkArgs The base link arguments.
  	@return {fm.icelink.websync.peerClient} The WebSync peer client.
  */


  baseLinkArgsExtensions.getPeerClient = function() {
    var baseLinkArgs;
    baseLinkArgs = arguments[0];
    return new fm.icelink.websync.peerClient(new fm.guid(baseLinkArgs.getPeerId()), baseLinkArgs.getPeerState());
  };

  /*<span id='method-fm.icelink.websync.baseLinkArgsExtensions-getPeerClient'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the WebSync peer client.
  	 </div>
  	@function getPeerClient
  	@return {fm.icelink.websync.peerClient} The WebSync peer client.
  */


  fm.icelink.baseLinkArgs.prototype.getPeerClient = function() {
    Array.prototype.splice.call(arguments, 0, 0, this);
    return fm.icelink.websync.baseLinkArgsExtensions.getPeerClient.apply(this, arguments);
  };

  return baseLinkArgsExtensions;

}).call(this);




fm.icelink.websync.joinState = (function(_super) {

  __extends(joinState, _super);

  joinState.prototype._bindFailureArgs = null;

  joinState.prototype._bindSuccess = false;

  joinState.prototype._bindSuccessArgs = null;

  joinState.prototype._subscribeFailureArgs = null;

  joinState.prototype._subscribeSuccess = false;

  joinState.prototype._subscribeSuccessArgs = null;

  function joinState() {
    this.updateSubscribeSuccess = __bind(this.updateSubscribeSuccess, this);

    this.updateSubscribeFailure = __bind(this.updateSubscribeFailure, this);

    this.updateBindSuccess = __bind(this.updateBindSuccess, this);

    this.updateBindFailure = __bind(this.updateBindFailure, this);

    this.setSubscribeSuccessArgs = __bind(this.setSubscribeSuccessArgs, this);

    this.setSubscribeSuccess = __bind(this.setSubscribeSuccess, this);

    this.setSubscribeFailureArgs = __bind(this.setSubscribeFailureArgs, this);

    this.setBindSuccessArgs = __bind(this.setBindSuccessArgs, this);

    this.setBindSuccess = __bind(this.setBindSuccess, this);

    this.setBindFailureArgs = __bind(this.setBindFailureArgs, this);

    this.getSubscribeSuccessArgs = __bind(this.getSubscribeSuccessArgs, this);

    this.getSubscribeSuccess = __bind(this.getSubscribeSuccess, this);

    this.getSubscribeFailureArgs = __bind(this.getSubscribeFailureArgs, this);

    this.getBindSuccessArgs = __bind(this.getBindSuccessArgs, this);

    this.getBindSuccess = __bind(this.getBindSuccess, this);

    this.getBindFailureArgs = __bind(this.getBindFailureArgs, this);
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      joinState.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    joinState.__super__.constructor.call(this);
  }

  joinState.prototype.getBindFailureArgs = function() {
    return this._bindFailureArgs;
  };

  joinState.prototype.getBindSuccess = function() {
    return this._bindSuccess;
  };

  joinState.prototype.getBindSuccessArgs = function() {
    return this._bindSuccessArgs;
  };

  joinState.prototype.getSubscribeFailureArgs = function() {
    return this._subscribeFailureArgs;
  };

  joinState.prototype.getSubscribeSuccess = function() {
    return this._subscribeSuccess;
  };

  joinState.prototype.getSubscribeSuccessArgs = function() {
    return this._subscribeSuccessArgs;
  };

  joinState.prototype.setBindFailureArgs = function() {
    var value;
    value = arguments[0];
    return this._bindFailureArgs = value;
  };

  joinState.prototype.setBindSuccess = function() {
    var value;
    value = arguments[0];
    return this._bindSuccess = value;
  };

  joinState.prototype.setBindSuccessArgs = function() {
    var value;
    value = arguments[0];
    return this._bindSuccessArgs = value;
  };

  joinState.prototype.setSubscribeFailureArgs = function() {
    var value;
    value = arguments[0];
    return this._subscribeFailureArgs = value;
  };

  joinState.prototype.setSubscribeSuccess = function() {
    var value;
    value = arguments[0];
    return this._subscribeSuccess = value;
  };

  joinState.prototype.setSubscribeSuccessArgs = function() {
    var value;
    value = arguments[0];
    return this._subscribeSuccessArgs = value;
  };

  joinState.prototype.updateBindFailure = function() {
    var bindFailureArgs;
    bindFailureArgs = arguments[0];
    this.setBindSuccess(false);
    return this.setBindFailureArgs(bindFailureArgs);
  };

  joinState.prototype.updateBindSuccess = function() {
    var bindSuccessArgs;
    bindSuccessArgs = arguments[0];
    this.setBindSuccess(true);
    return this.setBindSuccessArgs(bindSuccessArgs);
  };

  joinState.prototype.updateSubscribeFailure = function() {
    var subscribeFailureArgs;
    subscribeFailureArgs = arguments[0];
    this.setSubscribeSuccess(false);
    return this.setSubscribeFailureArgs(subscribeFailureArgs);
  };

  joinState.prototype.updateSubscribeSuccess = function() {
    var subscribeSuccessArgs;
    subscribeSuccessArgs = arguments[0];
    this.setSubscribeSuccess(true);
    return this.setSubscribeSuccessArgs(subscribeSuccessArgs);
  };

  return joinState;

})(fm.object);




fm.icelink.websync.leaveState = (function(_super) {

  __extends(leaveState, _super);

  leaveState.prototype._unbindFailureArgs = null;

  leaveState.prototype._unbindSuccess = false;

  leaveState.prototype._unbindSuccessArgs = null;

  leaveState.prototype._unsubscribeFailureArgs = null;

  leaveState.prototype._unsubscribeSuccess = false;

  leaveState.prototype._unsubscribeSuccessArgs = null;

  function leaveState() {
    this.updateUnsubscribeSuccess = __bind(this.updateUnsubscribeSuccess, this);

    this.updateUnsubscribeFailure = __bind(this.updateUnsubscribeFailure, this);

    this.updateUnbindSuccess = __bind(this.updateUnbindSuccess, this);

    this.updateUnbindFailure = __bind(this.updateUnbindFailure, this);

    this.setUnsubscribeSuccessArgs = __bind(this.setUnsubscribeSuccessArgs, this);

    this.setUnsubscribeSuccess = __bind(this.setUnsubscribeSuccess, this);

    this.setUnsubscribeFailureArgs = __bind(this.setUnsubscribeFailureArgs, this);

    this.setUnbindSuccessArgs = __bind(this.setUnbindSuccessArgs, this);

    this.setUnbindSuccess = __bind(this.setUnbindSuccess, this);

    this.setUnbindFailureArgs = __bind(this.setUnbindFailureArgs, this);

    this.getUnsubscribeSuccessArgs = __bind(this.getUnsubscribeSuccessArgs, this);

    this.getUnsubscribeSuccess = __bind(this.getUnsubscribeSuccess, this);

    this.getUnsubscribeFailureArgs = __bind(this.getUnsubscribeFailureArgs, this);

    this.getUnbindSuccessArgs = __bind(this.getUnbindSuccessArgs, this);

    this.getUnbindSuccess = __bind(this.getUnbindSuccess, this);

    this.getUnbindFailureArgs = __bind(this.getUnbindFailureArgs, this);
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      leaveState.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    leaveState.__super__.constructor.call(this);
  }

  leaveState.prototype.getUnbindFailureArgs = function() {
    return this._unbindFailureArgs;
  };

  leaveState.prototype.getUnbindSuccess = function() {
    return this._unbindSuccess;
  };

  leaveState.prototype.getUnbindSuccessArgs = function() {
    return this._unbindSuccessArgs;
  };

  leaveState.prototype.getUnsubscribeFailureArgs = function() {
    return this._unsubscribeFailureArgs;
  };

  leaveState.prototype.getUnsubscribeSuccess = function() {
    return this._unsubscribeSuccess;
  };

  leaveState.prototype.getUnsubscribeSuccessArgs = function() {
    return this._unsubscribeSuccessArgs;
  };

  leaveState.prototype.setUnbindFailureArgs = function() {
    var value;
    value = arguments[0];
    return this._unbindFailureArgs = value;
  };

  leaveState.prototype.setUnbindSuccess = function() {
    var value;
    value = arguments[0];
    return this._unbindSuccess = value;
  };

  leaveState.prototype.setUnbindSuccessArgs = function() {
    var value;
    value = arguments[0];
    return this._unbindSuccessArgs = value;
  };

  leaveState.prototype.setUnsubscribeFailureArgs = function() {
    var value;
    value = arguments[0];
    return this._unsubscribeFailureArgs = value;
  };

  leaveState.prototype.setUnsubscribeSuccess = function() {
    var value;
    value = arguments[0];
    return this._unsubscribeSuccess = value;
  };

  leaveState.prototype.setUnsubscribeSuccessArgs = function() {
    var value;
    value = arguments[0];
    return this._unsubscribeSuccessArgs = value;
  };

  leaveState.prototype.updateUnbindFailure = function() {
    var unbindFailureArgs;
    unbindFailureArgs = arguments[0];
    this.setUnbindSuccess(false);
    return this.setUnbindFailureArgs(unbindFailureArgs);
  };

  leaveState.prototype.updateUnbindSuccess = function() {
    var unbindSuccessArgs;
    unbindSuccessArgs = arguments[0];
    this.setUnbindSuccess(true);
    return this.setUnbindSuccessArgs(unbindSuccessArgs);
  };

  leaveState.prototype.updateUnsubscribeFailure = function() {
    var unsubscribeFailureArgs;
    unsubscribeFailureArgs = arguments[0];
    this.setUnsubscribeSuccess(false);
    return this.setUnsubscribeFailureArgs(unsubscribeFailureArgs);
  };

  leaveState.prototype.updateUnsubscribeSuccess = function() {
    var unsubscribeSuccessArgs;
    unsubscribeSuccessArgs = arguments[0];
    this.setUnsubscribeSuccess(true);
    return this.setUnsubscribeSuccessArgs(unsubscribeSuccessArgs);
  };

  return leaveState;

})(fm.object);


/*<span id='cls-fm.icelink.websync.peerClient'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.peerClient
 <div>
 Details about a WebSync peer client.
 </div>

@extends fm.object
*/


fm.icelink.websync.peerClient = (function(_super) {

  __extends(peerClient, _super);

  peerClient.prototype._boundRecords = null;

  peerClient.prototype._clientId = null;

  /*<span id='method-fm.icelink.websync.peerClient-fm.icelink.websync.peerClient'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Initializes a new instance of the <see cref="fm.icelink.websync.peerClient">fm.icelink.websync.peerClient</see> class.
  	 </div>
  	@function fm.icelink.websync.peerClient
  	@param {fm.guid} clientId The WebSync client ID.
  	@param {Object} boundRecords The WebSync bound records.
  	@return {}
  */


  function peerClient() {
    this.setClientId = __bind(this.setClientId, this);

    this.setBoundRecords = __bind(this.setBoundRecords, this);

    this.getClientId = __bind(this.getClientId, this);

    this.getBoundRecords = __bind(this.getBoundRecords, this);

    var boundRecords, clientId;
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      peerClient.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    clientId = arguments[0];
    boundRecords = arguments[1];
    peerClient.__super__.constructor.call(this);
    this.setClientId(clientId);
    this.setBoundRecords(boundRecords);
  }

  /*<span id='method-fm.icelink.websync.peerClient-getBoundRecords'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the WebSync bound records.
  	 </div>
  
  	@function getBoundRecords
  	@return {Object}
  */


  peerClient.prototype.getBoundRecords = function() {
    return this._boundRecords;
  };

  /*<span id='method-fm.icelink.websync.peerClient-getClientId'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the WebSync client ID.
  	 </div>
  
  	@function getClientId
  	@return {fm.guid}
  */


  peerClient.prototype.getClientId = function() {
    return this._clientId;
  };

  /*<span id='method-fm.icelink.websync.peerClient-setBoundRecords'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the WebSync bound records.
  	 </div>
  
  	@function setBoundRecords
  	@param {Object} value
  	@return {void}
  */


  peerClient.prototype.setBoundRecords = function() {
    var value;
    value = arguments[0];
    return this._boundRecords = value;
  };

  /*<span id='method-fm.icelink.websync.peerClient-setClientId'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sets the WebSync client ID.
  	 </div>
  
  	@function setClientId
  	@param {fm.guid} value
  	@return {void}
  */


  peerClient.prototype.setClientId = function() {
    var value;
    value = arguments[0];
    return this._clientId = value;
  };

  return peerClient;

})(fm.object);




fm.icelink.websync.startState = (function(_super) {

  __extends(startState, _super);

  startState.prototype._bindFailureArgs = null;

  startState.prototype._bindSuccess = false;

  startState.prototype._bindSuccessArgs = null;

  startState.prototype._subscribeFailureArgs = null;

  startState.prototype._subscribeSuccess = false;

  startState.prototype._subscribeSuccessArgs = null;

  function startState() {
    this.updateSubscribeSuccess = __bind(this.updateSubscribeSuccess, this);

    this.updateSubscribeFailure = __bind(this.updateSubscribeFailure, this);

    this.updateBindSuccess = __bind(this.updateBindSuccess, this);

    this.updateBindFailure = __bind(this.updateBindFailure, this);

    this.setSubscribeSuccessArgs = __bind(this.setSubscribeSuccessArgs, this);

    this.setSubscribeSuccess = __bind(this.setSubscribeSuccess, this);

    this.setSubscribeFailureArgs = __bind(this.setSubscribeFailureArgs, this);

    this.setBindSuccessArgs = __bind(this.setBindSuccessArgs, this);

    this.setBindSuccess = __bind(this.setBindSuccess, this);

    this.setBindFailureArgs = __bind(this.setBindFailureArgs, this);

    this.getSubscribeSuccessArgs = __bind(this.getSubscribeSuccessArgs, this);

    this.getSubscribeSuccess = __bind(this.getSubscribeSuccess, this);

    this.getSubscribeFailureArgs = __bind(this.getSubscribeFailureArgs, this);

    this.getBindSuccessArgs = __bind(this.getBindSuccessArgs, this);

    this.getBindSuccess = __bind(this.getBindSuccess, this);

    this.getBindFailureArgs = __bind(this.getBindFailureArgs, this);
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      startState.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    startState.__super__.constructor.call(this);
  }

  startState.prototype.getBindFailureArgs = function() {
    return this._bindFailureArgs;
  };

  startState.prototype.getBindSuccess = function() {
    return this._bindSuccess;
  };

  startState.prototype.getBindSuccessArgs = function() {
    return this._bindSuccessArgs;
  };

  startState.prototype.getSubscribeFailureArgs = function() {
    return this._subscribeFailureArgs;
  };

  startState.prototype.getSubscribeSuccess = function() {
    return this._subscribeSuccess;
  };

  startState.prototype.getSubscribeSuccessArgs = function() {
    return this._subscribeSuccessArgs;
  };

  startState.prototype.setBindFailureArgs = function() {
    var value;
    value = arguments[0];
    return this._bindFailureArgs = value;
  };

  startState.prototype.setBindSuccess = function() {
    var value;
    value = arguments[0];
    return this._bindSuccess = value;
  };

  startState.prototype.setBindSuccessArgs = function() {
    var value;
    value = arguments[0];
    return this._bindSuccessArgs = value;
  };

  startState.prototype.setSubscribeFailureArgs = function() {
    var value;
    value = arguments[0];
    return this._subscribeFailureArgs = value;
  };

  startState.prototype.setSubscribeSuccess = function() {
    var value;
    value = arguments[0];
    return this._subscribeSuccess = value;
  };

  startState.prototype.setSubscribeSuccessArgs = function() {
    var value;
    value = arguments[0];
    return this._subscribeSuccessArgs = value;
  };

  startState.prototype.updateBindFailure = function() {
    var bindFailureArgs;
    bindFailureArgs = arguments[0];
    this.setBindSuccess(false);
    return this.setBindFailureArgs(bindFailureArgs);
  };

  startState.prototype.updateBindSuccess = function() {
    var bindSuccessArgs;
    bindSuccessArgs = arguments[0];
    this.setBindSuccess(true);
    return this.setBindSuccessArgs(bindSuccessArgs);
  };

  startState.prototype.updateSubscribeFailure = function() {
    var subscribeFailureArgs;
    subscribeFailureArgs = arguments[0];
    this.setSubscribeSuccess(false);
    return this.setSubscribeFailureArgs(subscribeFailureArgs);
  };

  startState.prototype.updateSubscribeSuccess = function() {
    var subscribeSuccessArgs;
    subscribeSuccessArgs = arguments[0];
    this.setSubscribeSuccess(true);
    return this.setSubscribeSuccessArgs(subscribeSuccessArgs);
  };

  return startState;

})(fm.object);




fm.icelink.websync.stopState = (function(_super) {

  __extends(stopState, _super);

  stopState.prototype._unbindFailureArgs = null;

  stopState.prototype._unbindSuccess = false;

  stopState.prototype._unbindSuccessArgs = null;

  stopState.prototype._unsubscribeFailureArgs = null;

  stopState.prototype._unsubscribeSuccess = false;

  stopState.prototype._unsubscribeSuccessArgs = null;

  function stopState() {
    this.updateUnsubscribeSuccess = __bind(this.updateUnsubscribeSuccess, this);

    this.updateUnsubscribeFailure = __bind(this.updateUnsubscribeFailure, this);

    this.updateUnbindSuccess = __bind(this.updateUnbindSuccess, this);

    this.updateUnbindFailure = __bind(this.updateUnbindFailure, this);

    this.setUnsubscribeSuccessArgs = __bind(this.setUnsubscribeSuccessArgs, this);

    this.setUnsubscribeSuccess = __bind(this.setUnsubscribeSuccess, this);

    this.setUnsubscribeFailureArgs = __bind(this.setUnsubscribeFailureArgs, this);

    this.setUnbindSuccessArgs = __bind(this.setUnbindSuccessArgs, this);

    this.setUnbindSuccess = __bind(this.setUnbindSuccess, this);

    this.setUnbindFailureArgs = __bind(this.setUnbindFailureArgs, this);

    this.getUnsubscribeSuccessArgs = __bind(this.getUnsubscribeSuccessArgs, this);

    this.getUnsubscribeSuccess = __bind(this.getUnsubscribeSuccess, this);

    this.getUnsubscribeFailureArgs = __bind(this.getUnsubscribeFailureArgs, this);

    this.getUnbindSuccessArgs = __bind(this.getUnbindSuccessArgs, this);

    this.getUnbindSuccess = __bind(this.getUnbindSuccess, this);

    this.getUnbindFailureArgs = __bind(this.getUnbindFailureArgs, this);
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      stopState.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    stopState.__super__.constructor.call(this);
  }

  stopState.prototype.getUnbindFailureArgs = function() {
    return this._unbindFailureArgs;
  };

  stopState.prototype.getUnbindSuccess = function() {
    return this._unbindSuccess;
  };

  stopState.prototype.getUnbindSuccessArgs = function() {
    return this._unbindSuccessArgs;
  };

  stopState.prototype.getUnsubscribeFailureArgs = function() {
    return this._unsubscribeFailureArgs;
  };

  stopState.prototype.getUnsubscribeSuccess = function() {
    return this._unsubscribeSuccess;
  };

  stopState.prototype.getUnsubscribeSuccessArgs = function() {
    return this._unsubscribeSuccessArgs;
  };

  stopState.prototype.setUnbindFailureArgs = function() {
    var value;
    value = arguments[0];
    return this._unbindFailureArgs = value;
  };

  stopState.prototype.setUnbindSuccess = function() {
    var value;
    value = arguments[0];
    return this._unbindSuccess = value;
  };

  stopState.prototype.setUnbindSuccessArgs = function() {
    var value;
    value = arguments[0];
    return this._unbindSuccessArgs = value;
  };

  stopState.prototype.setUnsubscribeFailureArgs = function() {
    var value;
    value = arguments[0];
    return this._unsubscribeFailureArgs = value;
  };

  stopState.prototype.setUnsubscribeSuccess = function() {
    var value;
    value = arguments[0];
    return this._unsubscribeSuccess = value;
  };

  stopState.prototype.setUnsubscribeSuccessArgs = function() {
    var value;
    value = arguments[0];
    return this._unsubscribeSuccessArgs = value;
  };

  stopState.prototype.updateUnbindFailure = function() {
    var unbindFailureArgs;
    unbindFailureArgs = arguments[0];
    this.setUnbindSuccess(false);
    return this.setUnbindFailureArgs(unbindFailureArgs);
  };

  stopState.prototype.updateUnbindSuccess = function() {
    var unbindSuccessArgs;
    unbindSuccessArgs = arguments[0];
    this.setUnbindSuccess(true);
    return this.setUnbindSuccessArgs(unbindSuccessArgs);
  };

  stopState.prototype.updateUnsubscribeFailure = function() {
    var unsubscribeFailureArgs;
    unsubscribeFailureArgs = arguments[0];
    this.setUnsubscribeSuccess(false);
    return this.setUnsubscribeFailureArgs(unsubscribeFailureArgs);
  };

  stopState.prototype.updateUnsubscribeSuccess = function() {
    var unsubscribeSuccessArgs;
    unsubscribeSuccessArgs = arguments[0];
    this.setUnsubscribeSuccess(true);
    return this.setUnsubscribeSuccessArgs(unsubscribeSuccessArgs);
  };

  return stopState;

})(fm.object);


/*<span id='cls-fm.icelink.websync.signalProvider'>&nbsp;</span>
*/

/**
@class fm.icelink.websync.signalProvider
 <div>
 A WebSync-based provider for signalling used during the peer-to-peer handshake process.
 </div>

@extends fm.icelink.signalProvider
*/


fm.icelink.websync.signalProvider = (function(_super) {

  __extends(signalProvider, _super);

  signalProvider.prototype.__isStarted = false;

  signalProvider.prototype.__isStarting = false;

  signalProvider.prototype.__isStopping = false;

  signalProvider.prototype._candidateTag = null;

  signalProvider.prototype._client = null;

  signalProvider.prototype._offerAnswerTag = null;

  signalProvider.prototype._processOnNotifyEvent = null;

  signalProvider.prototype._stateLock = null;

  /*<span id='method-fm.icelink.websync.signalProvider-fm.icelink.websync.signalProvider'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Initializes a new instance of the <see cref="fm.icelink.websync.signalProvider">fm.icelink.websync.signalProvider</see> class.
  	 </div>
  	@function fm.icelink.websync.signalProvider
  	@param {String} conferenceId The ID of the conference.
  	@param {fm.websync.client} client The client.
  	@return {}
  */


  function signalProvider() {
    this.stop = __bind(this.stop, this);

    this.start = __bind(this.start, this);

    this.setClient = __bind(this.setClient, this);

    this.sendOfferAnswerSuccess = __bind(this.sendOfferAnswerSuccess, this);

    this.sendOfferAnswerFailure = __bind(this.sendOfferAnswerFailure, this);

    this.sendOfferAnswer = __bind(this.sendOfferAnswer, this);

    this.sendCandidateSuccess = __bind(this.sendCandidateSuccess, this);

    this.sendCandidateFailure = __bind(this.sendCandidateFailure, this);

    this.sendCandidate = __bind(this.sendCandidate, this);

    this.processOnNotify = __bind(this.processOnNotify, this);

    this.getIsStopping = __bind(this.getIsStopping, this);

    this.getIsStarting = __bind(this.getIsStarting, this);

    this.getIsStarted = __bind(this.getIsStarted, this);

    this.getClient = __bind(this.getClient, this);

    var client, conferenceId;
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      signalProvider.__super__.constructor.call(this);
      fm.util.attachProperties(this, arguments[0]);
      return;
    }
    conferenceId = arguments[0];
    client = arguments[1];
    signalProvider.__super__.constructor.call(this, conferenceId);
    this.__isStarting = false;
    this.__isStopping = false;
    this.__isStarted = false;
    this._stateLock = new fm.object();
    this.setClient(client);
    this._offerAnswerTag = fm.stringExtensions.concat("fm.icelink.offerAnswer-", this.getConferenceId());
    this._candidateTag = fm.stringExtensions.concat("fm.icelink.candidate-", this.getConferenceId());
    this._processOnNotifyEvent = this.processOnNotify;
  }

  /*<span id='method-fm.icelink.websync.signalProvider-getClient'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets the client.
  	 </div>
  
  	@function getClient
  	@return {fm.websync.client}
  */


  signalProvider.prototype.getClient = function() {
    return this._client;
  };

  /*<span id='method-fm.icelink.websync.signalProvider-getIsStarted'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets whether the provider is started.
  	 </div>
  
  	@function getIsStarted
  	@return {Boolean}
  */


  signalProvider.prototype.getIsStarted = function() {
    return this.__isStarted;
  };

  /*<span id='method-fm.icelink.websync.signalProvider-getIsStarting'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets whether the provider is starting.
  	 </div>
  
  	@function getIsStarting
  	@return {Boolean}
  */


  signalProvider.prototype.getIsStarting = function() {
    return this.__isStarting;
  };

  /*<span id='method-fm.icelink.websync.signalProvider-getIsStopping'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Gets whether the provider is stopping.
  	 </div>
  
  	@function getIsStopping
  	@return {Boolean}
  */


  signalProvider.prototype.getIsStopping = function() {
    return this.__isStopping;
  };

  signalProvider.prototype.processOnNotify = function() {
    var e, notifyingClient, peerId, _var0;
    e = arguments[0];
    notifyingClient = e.getNotifyingClient();
    _var0 = notifyingClient;
    if ((_var0 !== null && typeof _var0 !== 'undefined') && (notifyingClient.getClientId() !== null)) {
      peerId = notifyingClient.getClientId().toString();
      if (e.getTag() === this._offerAnswerTag) {
        return this.raiseOfferAnswer(new fm.icelink.receiveOfferAnswerArgs(fm.icelink.offerAnswer.fromJson(e.getDataJson()), peerId));
      } else {
        if (e.getTag() === this._candidateTag) {
          return this.raiseCandidate(new fm.icelink.receiveCandidateArgs(fm.icelink.candidate.fromJson(e.getDataJson()), peerId));
        }
      }
    }
  };

  /*<span id='method-fm.icelink.websync.signalProvider-sendCandidate'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sends a candidate to a peer.
  	 </div>
  	@function sendCandidate
  	@param {fm.icelink.sendCandidateArgs} args The arguments.
  	@return {void}
  */


  signalProvider.prototype.sendCandidate = function() {
    var args, notifyArgs;
    args = arguments[0];
    notifyArgs = new fm.websync.notifyArgs(fm.parseAssistant.parseGuid(args.getPeerId()), args.getCandidate().toJson(), this._candidateTag);
    notifyArgs.setOnSuccess(this.sendCandidateSuccess);
    notifyArgs.setOnFailure(this.sendCandidateFailure);
    return this.getClient().notify(notifyArgs);
  };

  signalProvider.prototype.sendCandidateFailure = function() {
    var e;
    e = arguments[0];
    return fm.log.error(fm.stringExtensions.format("Could not send candidate for conference '{0}' to remote WebSync client.", this.getConferenceId()), e.getException());
  };

  signalProvider.prototype.sendCandidateSuccess = function() {
    var e;
    e = arguments[0];
    return fm.log.debug(fm.stringExtensions.format("Sent candidate for conference '{0}' to remote WebSync client.", this.getConferenceId()));
  };

  /*<span id='method-fm.icelink.websync.signalProvider-sendOfferAnswer'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Sends an offer/answer to a peer.
  	 </div>
  	@function sendOfferAnswer
  	@param {fm.icelink.sendOfferAnswerArgs} args The arguments.
  	@return {void}
  */


  signalProvider.prototype.sendOfferAnswer = function() {
    var args, notifyArgs;
    args = arguments[0];
    notifyArgs = new fm.websync.notifyArgs(fm.parseAssistant.parseGuid(args.getPeerId()), args.getOfferAnswer().toJson(), this._offerAnswerTag);
    notifyArgs.setOnSuccess(this.sendOfferAnswerSuccess);
    notifyArgs.setOnFailure(this.sendOfferAnswerFailure);
    return this.getClient().notify(notifyArgs);
  };

  signalProvider.prototype.sendOfferAnswerFailure = function() {
    var e;
    e = arguments[0];
    return fm.log.error(fm.stringExtensions.format("Could not send offer/answer for conference '{0}' to remote WebSync client.", this.getConferenceId()), e.getException());
  };

  signalProvider.prototype.sendOfferAnswerSuccess = function() {
    var e;
    e = arguments[0];
    return fm.log.debug(fm.stringExtensions.format("Sent offer/answer for conference '{0}' to remote WebSync client.", this.getConferenceId()));
  };

  signalProvider.prototype.setClient = function() {
    var value;
    value = arguments[0];
    return this._client = value;
  };

  /*<span id='method-fm.icelink.websync.signalProvider-start'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Starts the provider.
  	 </div>
  	@function start
  	@param {fm.icelink.startArgs} startArgs The arguments.
  	@return {void}
  */


  signalProvider.prototype.start = function() {
    var args2, p, startArgs, _var0, _var1;
    startArgs = arguments[0];
    if (this.getIsStopping()) {
      throw new Error("Signal provider is currently stopping.");
    }
    if (this.getIsStarting()) {
      throw new Error("Signal provider is already starting.");
    }
    if (this.getIsStarted()) {
      throw new Error("Signal provider is already started.");
    }
    this.__isStarting = true;
    this.getClient().addOnNotify(this._processOnNotifyEvent);
    this.__isStarting = false;
    this.__isStarted = true;
    _var0 = startArgs.getOnSuccess();
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      p = new fm.icelink.startSuccessArgs();
      p.setSignalProvider(this);
      p.setDynamicProperties(startArgs.getDynamicProperties());
      startArgs.getOnSuccess()(p);
    }
    _var1 = startArgs.getOnComplete();
    if (_var1 !== null && typeof _var1 !== 'undefined') {
      args2 = new fm.icelink.startCompleteArgs();
      args2.setSignalProvider(this);
      args2.setDynamicProperties(startArgs.getDynamicProperties());
      return startArgs.getOnComplete()(args2);
    }
  };

  /*<span id='method-fm.icelink.websync.signalProvider-stop'>&nbsp;</span>
  */


  /**
  	 <div>
  	 Stops the provider.
  	 </div>
  	@function stop
  	@param {fm.icelink.stopArgs} stopArgs The arguments.
  	@return {void}
  */


  signalProvider.prototype.stop = function() {
    var args2, p, stopArgs, _var0, _var1;
    stopArgs = arguments[0];
    if (this.getIsStarting()) {
      throw new Error("Signal provider is currently starting.");
    }
    if (this.getIsStopping()) {
      throw new Error("Signal provider is already stopping.");
    }
    if (!this.getIsStarted()) {
      throw new Error("Signal provider is already stopped.");
    }
    this.__isStopping = true;
    this.getClient().removeOnNotify(this._processOnNotifyEvent);
    this.__isStopping = false;
    this.__isStarted = false;
    _var0 = stopArgs.getOnSuccess();
    if (_var0 !== null && typeof _var0 !== 'undefined') {
      p = new fm.icelink.stopSuccessArgs();
      p.setSignalProvider(this);
      p.setDynamicProperties(stopArgs.getDynamicProperties());
      stopArgs.getOnSuccess()(p);
    }
    _var1 = stopArgs.getOnComplete();
    if (_var1 !== null && typeof _var1 !== 'undefined') {
      args2 = new fm.icelink.stopCompleteArgs();
      args2.setSignalProvider(this);
      args2.setDynamicProperties(stopArgs.getDynamicProperties());
      return stopArgs.getOnComplete()(args2);
    }
  };

  return signalProvider;

})(fm.icelink.signalProvider);


var methodName, _fn, _i, _len, _ref;

_ref = ['joinConference', 'leaveConference'];
_fn = function(methodName) {
  var method;
  method = fm.websync.client.prototype[methodName];
  return fm.websync.client.prototype[methodName] = function() {
    var obj;
    if (arguments.length === 1 && fm.util.isPlainObject(arguments[0])) {
      obj = arguments[0];
      return method.call(this, new fm.icelink.websync[methodName + 'Args'](obj));
    } else {
      return method.apply(this, arguments);
    }
  };
};
for (_i = 0, _len = _ref.length; _i < _len; _i++) {
  methodName = _ref[_i];
  _fn(methodName);
}


return fm.icelink.websync;
}));