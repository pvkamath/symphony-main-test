﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<% 
    Symphony.Core.Controllers.UserController.Logout(HttpContext.Current);
	
    if (!string.IsNullOrEmpty(Page.Request["Redirect"]))
    {
        Page.Response.Redirect(Page.Request["Redirect"]);
    }
    
%>