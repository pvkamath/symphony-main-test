﻿<%@ Page Title="Welcome to Symphony" MasterPageFile="~/SymphonyMaster.Master" Language="C#" AutoEventWireup="true" CodeBehind="PorSchoolsImport.aspx.cs" Inherits="Symphony.Web._Default" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    
    <script type="text/javascript">
        function msToTime(duration) {
            var milliseconds = parseInt((duration % 1000) / 100)
                , seconds = parseInt((duration / 1000) % 60)
                , minutes = parseInt((duration / (1000 * 60)) % 60)
                , hours = parseInt((duration / (1000 * 60 * 60)) % 24);

            hours = (hours < 10) ? "0" + hours : hours;
            minutes = (minutes < 10) ? "0" + minutes : minutes;
            seconds = (seconds < 10) ? "0" + seconds : seconds;

            return hours + ":" + minutes + ":" + seconds;
        }

        $(document).ready(function () {
            $.ajax({
                method: 'get',
                url: '/services/artisan.svc/importproschool/',
                success: function (args) {
                    var batchId = args.data;
                    var start = $.now();

                    var pph = 0;
                    var sph = 0;
                    var cph = 0;

                    var lastPagesProcessed = 0;
                    var lastSectionsProcessed = 0;
                    var lastCoursesProcessed = 0;

                    var downloadTime = 0;

                    var interval = setInterval(function () {
                        $.ajax({
                            method: 'get',
                            url: '/services/artisan.svc/artisanimportjob/' + batchId,
                            success: function (batchArgs) {
                                var currentTime = $.now();
                                var batch = batchArgs.data;
                                var duration = currentTime - start;
                                var hours = (duration - downloadTime) / 1000 / 60 / 60;

                                if (batch.section != "Downloading" && downloadTime == 0) {
                                    downloadTime = duration;
                                }


                                if (lastPagesProcessed != batch.pagesProcessed) {
                                    pph = batch.pagesProcessed / hours;
                                    lastPagesProcessed = batch.pagesProcessed;
                                }
                                if (lastSectionsProcessed != batch.sectionsProcessed) {
                                    sph = batch.sectionsProcessed / hours;
                                    lastSectionsProcessed = batch.sectionsProcessed;
                                }
                                if (lastCoursesProcessed != batch.coursesProcessed) {
                                    cph = batch.coursesProcessed / hours;
                                    lastCoursesProcessed = batch.coursesProcessed;
                                }
                                
                                $('#course').html(batch.course);
                                $('#section').html(batch.section);
                                $('#coursesProcessed').html(batch.coursesProcessed);
                                $('#sectionsProcessed').html(batch.sectionsProcessed);
                                $('#pagesProcessed').html(batch.pagesProcessed);
                                $('#courseId').html(batch.courseId);
                                $('#running').html(batch.running ? 'Yes' : 'No');
                                $('#started').html(Symphony.dateRenderer(batch.createdOn));
                                $('#cph').html(Math.round(cph));
                                $('#pph').html(Math.round(pph));
                                $('#sph').html(Math.round(sph));
                                $('#elapsed').html(msToTime(duration));
                                $('#errors').html(batch.errors);
                                $('#pathIssues').html(batch.pathIssues);

                                $('#downloadTime').html(downloadTime > 0 ? msToTime(downloadTime) : msToTime(duration));

                                $('#lineCount').html(batch.lines);
                                $('#sectionCount').html(batch.sectionCount);
                                $('#courseCount').html(batch.courseCount);

                                $('#linePercent').html(Math.round((batch.linesProcessed / batch.lines) * 100) + "%");
                                $('#sectionPercent').html(Math.round((batch.sectionsProcessed / batch.sectionCount) * 100) + "%");
                                $('#coursePercent').html(Math.round((batch.coursesProcessed / batch.courseCount) * 100) + "%");

                                if (!batch.running) {
                                    clearInterval(interval);
                                }
                            }
                        });
                    }, 1000);

                }
            });
        });
    </script>
    
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <table border="1" width="100%">
        <tr>
            <td width="15%">Course</td>
            <td width="15%">Section</td>
            <td width="5%">Courses Processed</td>
            <td width="5%">Sections Processed</td>
            <td width="5%">Pages Processed</td>
            <td width="10%">Course ID</td>
            <td width="5%">Running</td>
            <td width="10%">Started</td>
            <td width="5%">Courses Per Hour</td>
            <td width="5%">Sections Per Hour</td>
            <td width="5%">Pages Per Hour</td>
            <td width="15%">Elapsed</td>
        </tr>
        <tr>
            <td id="course"></td>
            <td id="section"></td>
            <td id="coursesProcessed"></td>
            <td id="sectionsProcessed"></td>
            <td id="pagesProcessed"></td>
            <td id="courseId"></td>
            <td id="running"></td>
            <td id="started"></td>
            <td id="cph"></td>
            <td id="sph"></td>
            <td id="pph"></td>
            <td id="elapsed"></td>
            
        </tr>
    </table>
    <br /><br />
    <div>
        Download Time: <span id="downloadTime"></span><br />
        Line Count: <span id="lineCount"></span> <span id="linePercent"></span><br />
        Section Count: <span id="sectionCount"></span> <span id="sectionPercent"></span><br />
        Course Count: <span id="courseCount"></span> <span id="coursePercent"></span><br />
        
    </div>
    <br /><br />
    Error Messages:
    <div id="errors"></div>

    <br /><br />
    Path Issues:
    <div id="pathIssues"></div>
</asp:Content>
